﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace TrendParser
{
    public class BinaryPacketReader
    {
        private byte[] buffer;
        private int bufferIndex;

        public BinaryPacketReader(byte[] buffer)
        {
            this.buffer = buffer;
            this.bufferIndex = 0;
        }

        /// <summary>
        /// Creates a reader from all bytes of a file
        /// </summary>
        /// <param name="fileName"></param>
        public BinaryPacketReader(string fileName)
        {
            if (File.Exists(fileName))
                buffer = File.ReadAllBytes(fileName);
        }

        public Int32 NextInt32()
        {
            Int32 value = BitConverter.ToInt32(buffer, bufferIndex);
            bufferIndex += 4;
            return value;
        }

        public uint NextUInt32()
        {
            uint value = BitConverter.ToUInt32(buffer, bufferIndex);
            bufferIndex += 4;
            return value;
        }

        public Int32 Next2SComplimentInt24()
        {
            Int32 value = Bit24ToInt32(new byte[] { buffer[bufferIndex + 2], buffer[bufferIndex + 1], buffer[bufferIndex] }); // Bytes need to be inverted
            bufferIndex += 3;
            return value;
        }

        public Int16 NextInt16()
        {
            Int16 value = BitConverter.ToInt16(buffer, bufferIndex);
            bufferIndex += 2;
            return value;
        }

        public ushort NextUInt16()
        {
            ushort value = BitConverter.ToUInt16(buffer, bufferIndex);
            bufferIndex += 2;
            return value;
        }

        public byte NextInt8()
        {
            byte value = buffer[bufferIndex];
            bufferIndex++;
            return value;
        }

        public string NextString(int length)
        {
            string value = Encoding.ASCII.GetString(buffer, bufferIndex, length);
            bufferIndex += length;
            return value;
        }

        public int RemainingBytes => buffer.Length - bufferIndex;

        public static Int32 Bit24ToInt32(byte[] byteArray)
        {
            Int32 result = ((0xFF & byteArray[2]) << 16) | ((0xFF & byteArray[1]) << 8) | (0xFF & byteArray[0]);
            if ((result & 0x00800000) > 0)
                result = (int)((uint)result | 0xFF000000);
            else
                result = (int)((uint)result & 0x00FFFFFF);
            return result;
        }

        public string ReadString(int charCount)
        {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < charCount; i++)
            {
                byte c = NextInt8();

                if (c == 0)
                    continue;

                char ch = Convert.ToChar(c);
                builder.Append(ch);
            }

            return builder.ToString();
        }

        public bool ReadBoolean()
        {
            var bit = NextInt8();
            return bit == 1;
        }

        public byte[] ReadBytes(int count)
        {
            byte[] bytes = new byte[count];
            for (int i = 0; i < count; i++)
            {
                bytes[i] = NextInt8();
            }

            return bytes;
        }
    }
}
