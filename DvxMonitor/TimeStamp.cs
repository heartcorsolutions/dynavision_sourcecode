﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TrendParser
{
    /// <summary>
    /// 36 bytes
    /// </summary>
    public class TimeStamp 
    {
        public uint Month { get; set; }
        public uint Day { get; set; }
        public uint Year { get; set; }

        public uint Hours { get; set; }
        public uint Minutes { get; set; }
        public uint Seconds { get; set; }

        public TimeStamp()
        {

        }

        public TimeStamp(BinaryPacketReader reader)
        {
            Seconds = reader.NextUInt32();
            Minutes = reader.NextUInt32();
            Hours = reader.NextUInt32();

            Day = reader.NextUInt32();
            Month = reader.NextUInt32();
            Year = reader.NextUInt32() + 1994;

            //padding
            reader.NextUInt32();
            reader.NextUInt32();
            reader.NextUInt32();
        }

        public bool IsValid()
        {
            if (Year < 1994)
                return false;
            if (Month <= 0 || Month > 12)
                return false;
            if (Day <= 0 || Day > 31)
                return false;

            if (Hours <= -1 || Hours > 24)
                return false;
            if (Minutes <= -1 || Minutes > 60)
                return false;
            if (Seconds <= -1 || Seconds > 60)
                return false;

            return true;
        }
 
        [JsonIgnore]
        public DateTime DateTime => new DateTime((int)Year, (int)Month, (int)Day, (int)Hours, (int)Minutes, (int)Seconds).ToLocalTime();
    }
}
