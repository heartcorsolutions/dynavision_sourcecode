﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrendParser
{
    /// <summary>
    /// 68 bytes
    /// 40 + 28
    /// </summary>
    public class DeviceStatus
    {
        public TrendHeader Header { get; set; }

        #region Format1

        public uint SdCardSize { get; set; }
        public uint SdCardFree { get; set; }
        public uint BatteryStatus { get; set; }
        public uint NumberOfReboots { get; set; }
        public uint WifiAttempts { get; set; }
        public uint LTEAttemps { get; set; }

        #endregion

        #region Format2

        public byte ConnectionType { get; set; }

        #endregion


        public DeviceStatus()
        {

        }

        public DeviceStatus(BinaryPacketReader reader)
        {
            Header = new TrendHeader(reader);
            switch (Header.FormatStamp)
            {
                case 1:
                    ReadFormat1(reader);
                    break;
                default:
                case 2:
                case 3:
                case 4:
                    ReadFormat1(reader);
                    ReadFormat2(reader);
                    break;
            }
        }

        private void ReadFormat1(BinaryPacketReader reader)
        {
            SdCardSize = reader.NextUInt32();
            SdCardFree = reader.NextUInt32();
            BatteryStatus = reader.NextUInt32();
            NumberOfReboots = reader.NextUInt32();
            WifiAttempts = reader.NextUInt32();
            LTEAttemps = reader.NextUInt32();
        }

        private void ReadFormat2(BinaryPacketReader reader)
        {
            ConnectionType = (byte) reader.NextUInt32();
        }
    }
}
