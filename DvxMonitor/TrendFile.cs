﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrendParser
{
    public class TrendFile
    {
        public DeviceStatus Status { get; set; }
        public List<TrendValues> Values { get; set; }

        //public TrendFile(BinaryReader reader)
        //{
        //    Status = new DeviceStatus(reader);

        //    Values = new List<TrendValues>();
        //    for (int i = 0; i < 30; i++)
        //    {
        //        Values.Add(new TrendValues(reader));
        //    }
        //}

        public TrendFile(string fileName)
        {
            var reader = new BinaryPacketReader(fileName);
            Status = new DeviceStatus(reader);

            Values = new List<TrendValues>();
            for (int i = 0; i < 30; i++)
            {
                Values.Add(new TrendValues(reader));
            }
        }
    }
}
