﻿using System;
using System.IO;
using System.Text;
using TechMedic.Model;
using TrendParser;

namespace TechMedic.Util
{
    public class WriteConfig
    {
        private const string EcgChannelOption = "0x00";
        private const string OffChannelOption = "0x11";
        private const string BoolOffOption = "0x00";
        private const string BoolOnOption = "0x01";
        private const string SquareWaveChannelOption = "0x15";
        private const string MvvdChannelOption = "0x13";
        private const string Wtc1OnOption = "0x0B";
        private const string Wtc1OffOption = "0x03";
        private const string Wtc2OnOption = "0xD4";
        private const string Wtc2OffOption = "0x14";

        private const string LOFF_enabled = "0xF3";
        private const string LOFF_disabled = "0xF2";

        private const string ChannelConfigFileName = "ads1298r.config";
        //private const string WifiConfigFileName = "wifi.config";
        private const string CommonConfigFileName = "common.config";
        private const string RtcConfigFileName = "rtc.config";

        public static bool WriteBinaryCommonConfig(string folderPath, DvxCommonSettingsModel settings)
        {
            string filePath = Path.Combine(folderPath, CommonConfigFileName);
            settings.RetriesOnFail = Math.Max(settings.RetriesOnFail, (byte)30);

            try
            {
                using (BinaryWriter writer = new BinaryWriter(File.Open(filePath, FileMode.Create)))
                {
                    WriteInt32(writer, settings.SerialNumber);
                    WriteIp(writer, settings.ServerIp);
                    WriteInt16(writer, settings.FtpPort);
                    WriteInt16(writer, settings.UdpPort);
                    WriteString(writer, settings.FtpUsername);
                    WriteString(writer, settings.FtpPassword);
                    WriteString(writer, settings.StudyFolder);
                    WriteString(writer, settings.WifiSsid);
                    WriteString(writer, settings.WifiPassword);
                    WriteString(writer, settings.CellularApnAddress);
                    WriteString(writer, settings.CellularName);
                    WriteString(writer, settings.CellularPassword);
                    WriteInt16(writer, settings.CellularPin);
                    WriteInt16(writer, settings.PostEventInterval);
                    WriteInt16(writer, settings.HolterInterval);
                    writer.Write(settings.UseVibration);
                    WriteInt8(writer, settings.RetriesOnFail);
                    WriteInt16(writer, 2700);
                    WriteInt16(writer, 4095);
                    WriteInt8(writer, settings.ConnectionMode);
                    WriteInt8(writer, settings.QrsChannel);
                    WriteInt8(writer, settings.RespirationChannel);
                    WriteInt8(writer, settings.GpsMode);
                    WriteInt32(writer, settings.DeviceMode);
                    //TODO: Ntp Server Address
                    WriteIp(writer, settings.ServerIp);
                    WriteInt32(writer, settings.NtpPort);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }

            return true;
        }

        public static bool WriteChannelConfig(string folderPath, int channelCount, int sampleRate)
        {
            string filePath = Path.Combine(folderPath, ChannelConfigFileName);
            StringBuilder fileBuilder = new StringBuilder();

            //TODO: dont hardcode samplerate
            fileBuilder.Append(GetLine("0x85", "CONFIG1"));
            fileBuilder.Append(GetLine("0x10", "CONFIG2"));
            fileBuilder.Append(GetLine("0xCC", "CONFIG3"));
            fileBuilder.Append(GetLine(LOFF_disabled, "LOFF"));

            for (int i = 0; i < 8; i++)
            {
                if (i < channelCount)
                    fileBuilder.Append(GetLine("0x00", $"CH{i + 1}SET"));
                else
                    fileBuilder.Append(GetLine("0x11", $"CH{i + 1}SET"));
            }

            fileBuilder.Append(GetLine("0x02", "RLD_SENSP"));
            fileBuilder.Append(GetLine("0x02", "RLD_SENSN"));
            fileBuilder.Append(GetLine("0x00", "LOFF_SENSP"));
            fileBuilder.Append(GetLine("0x00", "LOFF_SENSN"));
            fileBuilder.Append(GetLine("0x00", "LOFF_FLIP"));
            fileBuilder.Append(GetLine("0x0F", "GPIO"));
            fileBuilder.Append(GetLine("0x00", "PACE"));
            fileBuilder.Append(GetLine("0x20", "RESP"));
            fileBuilder.Append(GetLine("0x22", "CONFIG4"));

            bool useWtc = channelCount >= 5;

            fileBuilder.Append(GetLine(useWtc ? Wtc1OnOption : Wtc1OffOption, "WCT1"));
            fileBuilder.Append(GetLine(useWtc ? Wtc2OnOption : Wtc2OffOption, "WCT2"));

            fileBuilder.Append(GetLine("0x0" + channelCount, "nr_of_ch"));

            fileBuilder.Append(GetLine(useWtc ? BoolOnOption : BoolOffOption, "1705"));
            try
            {
                File.WriteAllText(filePath, fileBuilder.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
            return true;
        }

        public static bool WriteRtcConfig(string folderPath)
        {
            string filePath = Path.Combine(folderPath, RtcConfigFileName);
            DateTime utc = DateTime.UtcNow;

            try
            {
                using (BinaryWriter writer = new BinaryWriter(File.Open(filePath, FileMode.Create)))
                {
                    //RTC Time
                    writer.Write((byte)utc.Hour);
                    writer.Write((byte)utc.Minute);
                    writer.Write((byte)utc.Second);
                    writer.Write((byte)0);
                    writer.Write(0);
                    writer.Write(0);
                    writer.Write(0);
                    writer.Write(0);

                    //RTC Date
                    writer.Write((byte)utc.DayOfWeek);
                    writer.Write((byte)utc.Month);
                    writer.Write((byte)utc.Day);
                    writer.Write((byte)(utc.Year - 1994));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }

            return true;
        }

        private static string GetLine(string setting, string settingName)
        {
            return (setting + " :" + settingName + "\n");
        }

        private static void WriteIp(BinaryWriter writer, string ip)
        {
            if (ParseIp(ip, out byte[] ipParts))
            {
                for (int i = ipParts.Length - 1; i >= 0; i--)
                {
                    writer.Write(ipParts[i]);
                }
            }
            else
            {
                writer.Write(new byte[4]);
            }
        }
        private static string ReadIp(BinaryReader reader)
        {
            byte[] ipParts = reader.ReadBytes(4);
            string ip = ipParts[3] + "." + ipParts[2] + "." + ipParts[1] + "." + ipParts[0];
            return ip;
        }

        private static string ReadIp(BinaryPacketReader reader)
        {
            byte[] ipParts = reader.ReadBytes(4);
            string ip = ipParts[3] + "." + ipParts[2] + "." + ipParts[1] + "." + ipParts[0];
            return ip;
        }

        private static bool ParseIp(string ip, out byte[] output)
        {
            bool success = true;
            byte[] ipParts = new byte[4];

            string[] parts = ip.Split('.');
            if (parts.Length != 4)
            {
                success = false;
            }
            else
            {
                for (int i = 0; i < parts.Length; i++)
                {
                    ipParts[i] = byte.Parse(parts[i]);
                    if (byte.TryParse(parts[i], out byte result))
                    {
                        ipParts[i] = result;
                    }
                    else
                    {
                        success = false;
                    }
                }
            }

            output = ipParts;
            return success;
        }

        private static void WriteString(BinaryWriter writer, string value)
        {
            if (!string.IsNullOrEmpty(value) && value.Length <= 32)
            {
                value = value.PadRight(32, '\0');
                byte[] bytes = Encoding.ASCII.GetBytes(value);

                writer.Write(bytes);
            }
            else
            {
                byte[] bytes = new byte[32];

                writer.Write(bytes);
            }
        }

        private static void WriteInt32(BinaryWriter writer, int value)
        {
            writer.Write(value);
        }

        private static void WriteInt32(BinaryWriter writer, uint value)
        {
            int val = (int)value;
            writer.Write(val);
        }

        private static void WriteInt16(BinaryWriter writer, short value)
        {
            writer.Write(value);
        }

        private static void WriteInt16(BinaryWriter writer, int value)
        {
            short val = (short)value;
            writer.Write(val);
        }

        private static void WriteInt8(BinaryWriter writer, byte value)
        {
            writer.Write(value);
        }

        public static DvxCommonSettingsModel AddSettings(DvxCommonSettingsModel serverSettings, DvxCommonSettingsModel usbSettings)
        {
            usbSettings.ServerIp = serverSettings.ServerIp;
            usbSettings.FtpPort = serverSettings.FtpPort;
            usbSettings.UdpPort = serverSettings.UdpPort;
            usbSettings.FtpUsername = serverSettings.FtpUsername;
            usbSettings.FtpPassword = serverSettings.FtpPassword;
            usbSettings.CellularApnAddress = serverSettings.CellularApnAddress;
            usbSettings.CellularName = serverSettings.CellularName;
            usbSettings.CellularPassword = serverSettings.CellularPassword;
            usbSettings.CellularPin = serverSettings.CellularPin;
            return usbSettings;
        }

        #region readConfigs

        public static bool ReadCommonConfig(string folder, out DvxCommonSettingsModel settings)
        {
            string filePath = Path.Combine(folder, "common.config");
            File.SetAttributes(filePath, FileAttributes.Normal);
            var model = new DvxCommonSettingsModel();

            if (File.Exists(filePath))// && new FileInfo(filePath).Length >= 277)
            {
                var reader = new BinaryPacketReader(filePath);

                model.SerialNumber = reader.NextUInt32();
                model.ServerIp = ReadIp(reader);
                model.FtpPort = reader.NextUInt16();
                model.UdpPort = reader.NextUInt16(); //UDP Port
                model.FtpUsername = reader.ReadString(32).Replace("\0", "");
                model.FtpPassword = reader.ReadString(32).Replace("\0", "");
                reader.ReadString(32);
                model.WifiSsid = reader.ReadString(32).Replace("\0", "");
                model.WifiPassword = reader.ReadString(32).Replace("\0", "");
                model.CellularApnAddress = reader.ReadString(32).Replace("\0", "");
                model.CellularName = reader.ReadString(32).Replace("\0", "");
                model.CellularPassword = reader.ReadString(32).Replace("\0", "");
                model.CellularPin = reader.NextUInt16();
                model.PostEventInterval = reader.NextUInt16();
                model.HolterInterval = reader.NextUInt16();
                model.UseVibration = reader.ReadBoolean();
                model.RetriesOnFail = reader.NextInt8();
                reader.NextUInt16();
                reader.NextUInt16();
                model.ConnectionMode = reader.NextInt8();

                settings = model;
                return true;
            }

            settings = new DvxCommonSettingsModel();
            return false;
        }

        public static bool ReadChannelCount(string folderPath, out int channelCount)
        {
            var filePath = Path.Combine(folderPath, "ads1298r.config");
            File.SetAttributes(filePath, FileAttributes.Normal);
            if (!File.Exists(filePath))
            {
                channelCount = 0;
                return false;
            }

            using (var reader = new StreamReader(filePath))
            {
                for (int i = 0; i < 23; i++) reader.ReadLine();

                string channels = reader.ReadLine()?.Substring(2, 2);
                if (int.TryParse(channels, out int result))
                {
                    channelCount = result;
                    return true;
                }
            }

            channelCount = -1;
            return false;
        }

        #endregion
    }
}
