﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TechMedic.Model
{
    [Table("d_dvx_common_settings")]
    public class DvxCommonSettingsModel
    {
        [NotMapped] public const byte ConnectionMode4G = 2;
        [NotMapped] public const byte ConnectionModeWifi = 1;

        [NotMapped] public const uint DeviceModeEcg = 0;
        [NotMapped] public const uint DeviceModeSentec = 1;

        [Key] public int d_primary_key { get; set; }

        [Column("server_ip")] public string ServerIp { get; set; }

        [Column("ftp_port")] public int FtpPort { get; set; }

        [Column("udp_port")] public int UdpPort { get; set; }

        [Column("ftp_username")] public string FtpUsername { get; set; }

        [Column("ftp_password")] public string FtpPassword { get; set; }

        [Column("cellular_apn")] public string CellularApnAddress { get; set; }

        [Column("cellular_username")] public string CellularName { get; set; }

        [Column("cellular_password")] public string CellularPassword { get; set; }

        [Column("cellular_pin")] public int CellularPin { get; set; }

        [NotMapped] public uint SerialNumber { get; set; }

        [NotMapped] public string WifiSsid { get; set; } = string.Empty;

        [NotMapped] public string WifiPassword { get; set; } = string.Empty;

        [NotMapped] public string StudyFolder { get; set; }

        [NotMapped] public int PostEventInterval { get; set; }

        [NotMapped] public int HolterInterval { get; set; }

        [NotMapped] public bool UseVibration { get; set; }

        [NotMapped] public byte RetriesOnFail { get; set; }

        [NotMapped] public byte ConnectionMode { get; set; } = ConnectionModeWifi + ConnectionMode4G;

        [NotMapped] public byte QrsChannel { get; set; } = 1;

        [NotMapped] public byte RespirationChannel { get; set; } = 0;

        [NotMapped] public byte GpsMode { get; set; } = 1; //1 is on

        [NotMapped] public uint DeviceMode { get; set; } = DeviceModeEcg;

        [NotMapped] public uint NtpAddress { get; set; }

        [NotMapped] public uint NtpPort { get; set; } = 123;
    }
}