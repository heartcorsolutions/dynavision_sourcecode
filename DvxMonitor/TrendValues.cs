﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TrendParser
{
    public class TrendValues
    {
        /// <summary>
        /// 76 bytes
        /// 40 + 36
        /// </summary>
        public TrendHeader Header { get; set; }

        #region Format1

        public uint Spo2 { get; set; }
        public uint HeartRate { get; set; }
        public uint Temperature { get; set; }
        public uint Respiration { get; set; }
        public int GpsLong { get; set; }
        public int GpsLat { get; set; }
        public byte HeartCondition { get; set; }

        public DateTime LastGpsLocation { get; set; } = DateTime.MinValue;

        #endregion

        #region Format2

        public uint PulseRate { get; set; }
        public byte ManualEvent { get; set; }

        #endregion

        #region Format3

        /// <summary>
        /// Udp, Transmission-interval, Holter
        /// </summary>
        public byte DeviceTransmissionMode { get; set; }

        #endregion

        #region Format4

        public uint GpsSatellites { get; set; }
        public uint GpsHorizontalDilution { get; set; }

        #endregion

        [JsonIgnore] public string FileName { get; set; }

        public TrendValues()
        {

        }

        public TrendValues(BinaryPacketReader reader)
        {
            Header = new TrendHeader(reader);
            switch (Header.FormatStamp)
            {
                case 1:
                    ReadFormat1(reader);
                    break;
                case 2:
                case 3:
                    ReadFormat1(reader);
                    ReadFormat2(reader);
                    break;
                default:
                case 4:
                    ReadFormat1(reader);
                    ReadFormat2(reader);
                    ReadFormat4(reader);
                    break;
            }
        }

        private void ReadFormat1(BinaryPacketReader reader)
        {
            Spo2 = reader.NextUInt32();
            HeartRate = reader.NextUInt32();
            Temperature = reader.NextUInt32();
            Respiration = reader.NextUInt32();
            GpsLong = reader.NextInt32();
            GpsLat = reader.NextInt32();

            if (Header.FormatStamp >= 3)
            {
                HeartCondition = reader.NextInt8();
                DeviceTransmissionMode = reader.NextInt8();
                reader.NextUInt16();
            }
            else
            {
                HeartCondition = (byte)reader.NextInt32();
            }
        }

        private void ReadFormat2(BinaryPacketReader reader)
        {
            PulseRate = reader.NextUInt32();
            ManualEvent = (byte)reader.NextInt32();
        }

        private void ReadFormat4(BinaryPacketReader reader)
        {
            GpsSatellites = reader.NextUInt32();
            GpsHorizontalDilution = reader.NextUInt32();
        }

        [JsonIgnore] public bool IsValidGps => !(GpsLat == int.MaxValue || GpsLong == int.MaxValue);
    }
}
