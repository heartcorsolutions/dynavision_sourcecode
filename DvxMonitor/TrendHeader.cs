﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrendParser
{
    /// <summary>
    /// 40 bytes
    /// </summary>
    public class TrendHeader
    {
        public TimeStamp TimeStamp { get; set; }
        public uint FormatStamp { get; set; }

        public TrendHeader()
        {

        }

        public TrendHeader(BinaryPacketReader reader)
        {
            FormatStamp = reader.NextUInt32();
            TimeStamp = new TimeStamp(reader);
        }
    }
}
