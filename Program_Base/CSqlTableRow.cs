﻿// CSqlTableRow 
// by Simon Vlaar 
// can be used by Techmedic Development B.V
// created 16 August 2016
//
// Basic functions for a talbe row in a MySql database
// 

using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Program_Base;
using System.Windows.Forms;

/* find all column names -> use to make a valid bit mask and to check if a variable is defined in the dBase 
 * *
select TABLE_NAME,COLUMN_NAME, ORDINAL_POSITION from information_schema.columns
where table_schema = 'nl0001soft' order by table_name,ordinal_position
*/


namespace Program_Base
{
    public enum DSqlCompareDateTime
    {
        Second, Minute, Hour, Day, Month, Year
    }
    public enum DTableRowActiveState
    {
        Disabled, Enabled, Locked, Archived, NrTableRowStates
    }
    public enum DSqlSslMode
    {
        Default = 0,    // use default = PREFERRED
        // --ssl-mode=
        DISABLED, // Establish an unencrypted connection.
        PREFERRED,  // Establish an encrypted connection if the server supports encrypted connections, 
                    // falling back to an unencrypted connection if an encrypted connection cannot be established. 
                    // This is the default if --ssl-mode is not specified.
        REQUIRED,   // Establish an encrypted connection if the server supports encrypted connections. 
                    // The connection attempt fails if an encrypted connection cannot be established.
        VERIFY_CA,  // Like REQUIRED, but additionally verify the server Certificate Authority (CA) certificate against the configured CA certificates. 
                    // The connection attempt fails if no valid matching CA certificates are found.
        VERIFY_IDENTITY // Like VERIFY_CA, but additionally perform host name identify verification by checking 
                    // the server's Common Name identity in the certificate that the server sends to the client. 
                    // The client verifies the Common Name against the host name the client uses for connecting to the server, 
                    // and the connection fails if there is a mismatch. For encrypted connections, this option helps prevent man-in-the-middle attacks. 
                    // This is like the legacy --ssl-verify-server-cert option.
                    // Note Host name identity verification does not work with self-signed certificates created automatically by the server, or manually using mysql_ssl_rsa_setup 
    }

    public class CBitSet32
    {
        public const int _cNrValidBits = 31;    // do not use the highest bit to prevent sign problems in sql etc
        public UInt32 _mBitSet;

        public CBitSet32()
        {
            _mBitSet = 0;
        }
        public void mClear()
        {
            _mBitSet = 0;
        }
        public void mCopyTo(ref CBitSet32 ArTo)
        {
            if (ArTo != null)
            {
                ArTo._mBitSet = _mBitSet;
            }
        }
        public bool mbHasPermission(UInt32 APermisionFlags)
        {
            return (_mBitSet & APermisionFlags) == _mBitSet;
        }
        public void mSetBits(UInt32 ABitSet)
        {
            _mBitSet |= ABitSet;
        }
        public void mSetOneBit(UInt16 ABitNr)
        {
            if (ABitNr <= _cNrValidBits)
            {
                _mBitSet |= (1U << ABitNr);
            }
        }
        public void mResetOneBit(UInt16 ABitNr)
        {
            if (ABitNr <= _cNrValidBits)
            {
                _mBitSet &= ~(1U << ABitNr);
            }
        }
        public bool mbIsBitSet(UInt16 ABitNr)
        {
            UInt32 mask = 1U << ABitNr;
            return (_mBitSet & mask) == mask;
        }
        public bool mbIsEmpty()
        {
            return _mBitSet == 0;
        }
        public bool mbIsNotEmpty()
        {
            return _mBitSet != 0;
        }
        public bool mbIsEqual(CBitSet64 ABitSet2)
        {
            bool bEqual = _mBitSet == 0;

            if (ABitSet2 != null)
            {
                bEqual = _mBitSet == ABitSet2._mBitSet;
            }
            return bEqual;
        }

        public bool mbIsNotEqual(CBitSet64 ABitSet2)
        {
            bool bNotEqual = _mBitSet != 0;

            if (ABitSet2 != null)
            {
                bNotEqual = _mBitSet != ABitSet2._mBitSet;
            }
            return bNotEqual;
        }

        public void mOrSet(CBitSet32 ABitSet2)
        {
            if (ABitSet2 != null)
            {
                _mBitSet |= ABitSet2._mBitSet;
            }
        }
        public void mAndSet(CBitSet32 ABitSet2)
        {
            if (ABitSet2 != null)
            {
                _mBitSet &= ABitSet2._mBitSet;
            }
        }
        public static CBitSet64 sMakeOrSet(CBitSet32 ABitSet1, CBitSet32 ABitSet2)
        {
            CBitSet64 result = new CBitSet64();
            if (result != null)
            {
                if (ABitSet1 != null)
                {
                    result._mBitSet = ABitSet1._mBitSet;
                    if (ABitSet2 != null)
                    {
                        result._mBitSet |= ABitSet2._mBitSet;
                    }
                }
                else if (ABitSet2 != null)
                {
                    result._mBitSet = ABitSet2._mBitSet;
                }

            }
            return result;
        }
        public static CBitSet32 sMakeCopy(CBitSet32 ABitSet1)
        {
            CBitSet32 result = new CBitSet32();
            if (result != null)
            {
                if (ABitSet1 != null)
                {
                    result._mBitSet = ABitSet1._mBitSet;
                }
            }
            return result;
        }
        public static CBitSet32 sMakeAndSet(CBitSet32 ABitSet1, CBitSet32 ABitSet2)
        {
            CBitSet32 result = new CBitSet32();
            if (result != null)
            {
                if (ABitSet1 != null)
                {
                    result._mBitSet = ABitSet1._mBitSet;
                    if (ABitSet2 != null)
                    {
                        result._mBitSet &= ABitSet2._mBitSet;
                    }
                }
                else if (ABitSet2 != null)
                {
                    result._mBitSet = ABitSet2._mBitSet;
                }
            }
            return result;
        }
    }
    public class CBitSet64
    {
        public const int _cNrValidBits = 63;// do not use the highest bit to prevent sign problems in sql etc
        public UInt64 _mBitSet;

        public CBitSet64()
        {
            _mBitSet = 0;
        }
        public void mClear()
        {
            _mBitSet = 0;
        }
        public void mCopyTo(ref CBitSet64 ArTo)
        {
            if (ArTo != null)
            {
                ArTo._mBitSet = _mBitSet;
            }
        }
        public bool mbHasPermission(UInt64 APermisionFlags)
        {
            return (_mBitSet & APermisionFlags) == _mBitSet;
        }
        public void mSetBits(UInt64 ABitSet)
        {
            _mBitSet |= ABitSet;
        }
        public void mSetOneBit(UInt16 ABitNr)
        {
            if (ABitNr <= _cNrValidBits)
            {
                _mBitSet |= (1UL << ABitNr);
            }
        }
        public void mResetOneBit(UInt16 ABitNr)
        {
            if (ABitNr <= _cNrValidBits)
            {
                _mBitSet &= ~(1UL << ABitNr);
            }
        }
        public bool mbIsBitSet(UInt16 ABitNr)
        {
            UInt64 mask = 1UL << ABitNr;
            return (_mBitSet & mask) == mask;
        }
        public bool mbIsEmpty()
        {
            return _mBitSet == 0;
        }
        public bool mbIsNotEmpty()
        {
            return _mBitSet != 0;
        }
        public bool mbIsEqual(CBitSet64 ABitSet2)
        {
            bool bEqual = _mBitSet == 0;

            if (ABitSet2 != null)
            {
                bEqual = _mBitSet == ABitSet2._mBitSet;
            }
            return bEqual;
        }

        public bool mbIsNotEqual(CBitSet64 ABitSet2)
        {
            bool bNotEqual = _mBitSet != 0;

            if (ABitSet2 != null)
            {
                bNotEqual = _mBitSet != ABitSet2._mBitSet;
            }
            return bNotEqual;
        }

        public void mOrSet( CBitSet64 ABitSet2)
        {
            if( ABitSet2 != null )
            {
                _mBitSet |= ABitSet2._mBitSet;
            }
        }
        public void mAndSet(CBitSet64 ABitSet2)
        {
            if (ABitSet2 != null)
            {
                _mBitSet &= ABitSet2._mBitSet;
            }
        }
        public static CBitSet64 sMakeOrSet(CBitSet64 ABitSet1, CBitSet64 ABitSet2)
        {
            CBitSet64 result = new CBitSet64();
            if( result != null )
            {
                if (ABitSet1 != null)
                {
                    result._mBitSet = ABitSet1._mBitSet;
                    if (ABitSet2 != null)
                    {
                        result._mBitSet |= ABitSet2._mBitSet;
                    }
                }
                else if (ABitSet2 != null)
                {
                    result._mBitSet = ABitSet2._mBitSet;
                }

            }
            return result;
        }
        public static CBitSet64 sMakeCopy(CBitSet64 ABitSet1)
        {
            CBitSet64 result = new CBitSet64();
            if (result != null)
            {
                if (ABitSet1 != null)
                {
                    result._mBitSet = ABitSet1._mBitSet;
                }
            }
            return result;
        }
        public static CBitSet64 sMakeAndSet(CBitSet64 ABitSet1, CBitSet64 ABitSet2)
        {
            CBitSet64 result = new CBitSet64();
            if (result != null)
            {
                if (ABitSet1 != null)
                {
                    result._mBitSet = ABitSet1._mBitSet;
                    if (ABitSet2 != null)
                    {
                        result._mBitSet &= ABitSet2._mBitSet;
                    }
                }
                else if (ABitSet2 != null)
                {
                    result._mBitSet = ABitSet2._mBitSet;
                }

            }
            return result;
        }
    }

    public class CSqlDBaseConnection
    {
        private static uint sSqlGlobalRowLimit = 10000;

        private MySqlConnection mSqlConnection;
        private string mSqlServer;
        private int mSqlPort = 0;
        private DSqlSslMode mSqlSslMode = 0;
        private string mSqlDataBase;
        private string mSqlUser;
        private string mSqlPassword;
        private uint mSqlRowLimit = sSqlGlobalRowLimit;
        private DFloatDecimal mDecimal = DFloatDecimal.Dot;
        private static double mFloatComparePrecision = 0.0001;
        private static double mFloatCompareValid = 1e6;
        private static double mFloatInValid = 1e8;

        // output towards mySql in string format
        // !! sqlReader.GetValue(index).ToString() => returns string in program local format
        private string mSqlDateFormat = "yyyy-MM-dd";
        private string mSqlTimeFormat = "HH:mm:ss";
        private string mSqlDateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff";
        private TimeSpan mSqlTimeCorrection = TimeSpan.Zero;          // NotUsed add this time offset to get the correct time at the server

        private const string _cCaseName = "CSqlConnection";

        int _mOpenLN = 0;       // store openConnection location for debug where it is not closed
        string _mOpenFN = "";
        string _mOpenCN = "";

        private CSqlCmd _mCmdActive = null;
            int _mCmdLN = 0;       // store GetSqlCmd location for debug where it is not closed
        string _mCmdFN = "";
        string _mCmdCN = "";

        private string mSqlTrueText = "true";
        private string mSqlFalseText = "false";
        private string mSqlTrueLetter = "T";
        private string mSqlFalseLetter = "F";

        private void mSqlSetTrueFalse(string ATrueText, string AFalseText)
        {
            if (String.IsNullOrEmpty(ATrueText) == false)
            {
                mSqlTrueText = ATrueText;
                mSqlTrueLetter = mSqlTrueText.Substring(0, 1);

            }
            if (String.IsNullOrEmpty(AFalseText) == false)
            {
                mSqlFalseText = AFalseText;
                mSqlFalseLetter = mSqlFalseText.Substring(0, 1);
            }
        }
        public string mBoolSqlString(bool AbBool)
        {
            return AbBool ? mSqlTrueText : mSqlFalseText;
        }
        public CSqlDBaseConnection()
        {
            mSqlConnection = null;
            mSqlTimeCorrection = TimeSpan.Zero;
        }
        ~CSqlDBaseConnection()
        {
            mSqlDisposeConnection();
        }
        public DFloatDecimal mGetDecimal()
        {
            return mDecimal;
        }
        public uint mGetSqlRowLimit()
        {
            return mSqlRowLimit;
        }
        public static void sSetGlobalSqlRowLimit(UInt32 AGlobalRowLimit)
        {
            sSqlGlobalRowLimit = AGlobalRowLimit > 1 ? AGlobalRowLimit : 1;
        }
        public string mSqlDateTimeSqlString(DateTime ADateTime)
        {
            if( ADateTime > DateTime.MinValue )
            {
                DateTime dt = ADateTime - mSqlTimeCorrection;       // correct for set offset
                return "'" + dt.ToString(mSqlDateTimeFormat, CultureInfo.InvariantCulture) + "'"; //yyyy-MM-dd HH:mm:ss") + "'";
            }
            return mSqlZeroDateTimeSqlString();
        }
        public string mSqlDateSqlString(DateTime ADateTime)
        {
            if (ADateTime > DateTime.MinValue)
            {
                return "'" + ADateTime.ToString(mSqlDateFormat, CultureInfo.InvariantCulture) + "'"; // "yyyy-MM-dd") + "'";
            }
            return mSqlZeroDateSqlString();
        }
        public string mSqlFloatSqlString( float ArFloat )
        {
            string s = ArFloat.ToString();
            return CProgram.sCorrectToDecimal(mDecimal, s);
        }
        public string mSqlDoubleSqlString(double ArDouble)
        {
            string s = ArDouble.ToString();
            return CProgram.sCorrectToDecimal(mDecimal, s);
        }
        public string mSqlCompareDoubleSqlString( string AName, double AValue )
        {
            return "ABS(" + AName + "-" + mSqlDoubleSqlString(AValue) + ")<=" + mSqlDoubleSqlString(mFloatComparePrecision);
        }
        public string mSqlDoubleIsValidSqlString(string AName)
        {
            return "ABS(" + AName + ")<=" + mSqlDoubleSqlString(mFloatCompareValid);
        }
        public string mSqlDoubleIsZeroSqlString(string AName)
        {
            return "ABS(" + AName + ")<=" + mSqlDoubleSqlString(mFloatComparePrecision);
        }
        public string mSqlDoubleNotZeroSqlString(string AName)
        {
            return "(ABS(" + AName + ")>=" + mSqlDoubleSqlString(mFloatComparePrecision) + " AND ABS(" + AName + ")<=" + mSqlDoubleSqlString(mFloatCompareValid) +")";
        }
        public string mSqlZeroDateSqlString()
        {
            return "'0000-00-00'";
        }
        public string mSqlZeroDateTimeSqlString()
        {
            return "'0000-00-00 00:00:00'";
        }
        public string mSqlBoolSqlString(bool AbBool)
        {
            return AbBool ? mSqlTrueText : mSqlFalseText;
        }
        public string mSqlCompareTypeSqlString( DSqlCompareDateTime ACompareAs )
        {
            switch( ACompareAs )
            {
                case DSqlCompareDateTime.Second: return "Second";
                case DSqlCompareDateTime.Minute: return "Minute";
                case DSqlCompareDateTime.Hour: return "Hour";
                case DSqlCompareDateTime.Day: return "Day";
                case DSqlCompareDateTime.Month: return "Month";
                case DSqlCompareDateTime.Year: return "Year";
            }
            return null;
        }

        public string mSqlDateTimeComparSqlString( string AName, DateTime ADateTime, DSqlCompareDateTime ACompareAs, float AWithin)
        {
            return "TIMESTAMPDIFF(" + mSqlCompareTypeSqlString(ACompareAs) + "," + AName + "," + mSqlDateTimeSqlString(ADateTime) + ")<=" + mSqlFloatSqlString(AWithin); 
        }
        public string mSqlStringContainsSqlString(string AName, string AContainsSqlString)
        {
            return "INSTR(" + AName + "," + AContainsSqlString + ")>0";
        }
        public string mSqlStringStartsWithSqlString(string AName, string AStartsWith)
        {
            return "INSTR(" + AName + "," + mSqlConvertToSqlString(AStartsWith) + ")=1"; 
        }
        public string mSqlConvertToSqlString(string AString)     // return string in "'"  with special chars as escape sequence
        {
            /*
             * Table 10.1 Special Character Escape Sequences
            Escape Sequence Character Represented by Sequence
            \0  An ASCII NUL(X'00') character
            \' 	A single quote (“'”) character
            \" 	A double quote (“"”) character
            \b A backspace character
            \n A newline(linefeed) character
            \r A carriage return character
            \t A tab character
            \Z ASCII 26(Control + Z); see note following the table
            \\ 	A backslash (“\”) character
            \% A “%” character; see note following the table
            \_ A “_” character; see note following the table
            */
            string s = "'";
            if (AString != null)
            {
                int n = AString.Length;
                char c;

                for (int i = 0; i < n; ++i)
                {
                    c = AString[i];
                    if( c == '\'' ) { s += "\\'"; }
                    else if (c == '\"') { s += "\\\""; }
                    else if (c == '\b') { s += "\\b"; }
                    else if (c == '\n') { s += "\\n"; }
                    else if (c == '\r') { s += "\\r"; }
                    else if (c == '\t') { s += "\\t"; }
                    else if (c == '\\') { s += "\\\\"; }
                    else if (c == '%') { s += "\\%"; }
                    else if (c == '_') { s += "\\_"; }
                    else { s += c; }
                }
            }
            return s + "'";
        }

        public static string sSqlLimitSqlString( string AString, UInt16 ASqlMaxLen )
        {

            /*
             * Table 10.1 Special Character Escape Sequences
            Escape Sequence Character Represented by Sequence
            \0  An ASCII NUL(X'00') character
            \' 	A single quote (“'”) character
            \" 	A double quote (“"”) character
            \b A backspace character
            \n A newline(linefeed) character
            \r A carriage return character
            \t A tab character
            \Z ASCII 26(Control + Z); see note following the table
            \\ 	A backslash (“\”) character
            \% A “%” character; see note following the table
            \_ A “_” character; see note following the table
            */
            string s = "";
            string result = "";
            if (AString != null)
            {
                int n = AString.Length;
                char c;

                for (int i = 0; i < n; ++i)
                {
                    c = AString[i];
                    if (c == '\'') { s += "\\'"; }
                    else if (c == '\"') { s += "\\\""; }
                    else if (c == '\b') { s += "\\b"; }
                    else if (c == '\n') { s += "\\n"; }
                    else if (c == '\r') { s += "\\r"; }
                    else if (c == '\t') { s += "\\t"; }
                    else if (c == '\\') { s += "\\\\"; }
                    else if (c == '%') { s += "\\%"; }
                    else if (c == '_') { s += "\\_"; }
                    else { s += c; }
                    if (s.Length > ASqlMaxLen)
                    {
                        break;
                    }
                    result += c;
                }
            }
            return result;  // AString limited to the max length of the SQL stored string

        }

        public MySqlCommand mGetNewSqlCommand()
        {
            MySqlCommand cmd = null;

            if(mSqlConnection != null )
            {
                cmd = mSqlConnection.CreateCommand();
            }
            return cmd;
        }
        public MySqlConnection mGetSqlConnection()
        {
            return mSqlConnection;
        }
        public void mSqlDisposeConnection()
        {
            if (mSqlConnection != null)
            {
                try
                {
                    mSqlConnection.Close();
                    mSqlConnection.Dispose();
                }
                catch (Exception Ex)
                {
                    CProgram.sLogException("Failed closing connection to " + mSqlServer, Ex);
                }
            }
        }
        public bool mbSqlCheckSetup()
        {
            return mSqlPort > 0 && String.IsNullOrEmpty(mSqlServer) == false && false == String.IsNullOrEmpty(mSqlDataBase) && String.IsNullOrEmpty(mSqlUser) == false;
        }
        public bool mbSqlSetupServer(string AServer, int APort, DSqlSslMode ASslMode, string ADataBase, string AUser, string APassword)
        {
            mSqlDisposeConnection();
            mSqlServer = AServer;
            mSqlPort = APort;
            mSqlDataBase = ADataBase.Trim().ToLower();
            mSqlSslMode = ASslMode;
            mSqlUser = AUser;
            mSqlPassword = APassword;

            return mbSqlCheckSetup();
        }

        public string mGetSslModeCmd()
        {
            string sslCmd = "";

            if (mSqlSslMode > DSqlSslMode.Default)
            {
                sslCmd = ";Ssl Mode=" + mSqlSslMode.ToString();
            }
            return sslCmd;
        }

        public string mSqlGetServerName()
        {
            return mSqlServer;
        }
        public string mSqlGetDBaseName()
        {
            return mSqlDataBase;
        }
        public DSqlSslMode mGetSqlSslMode()
        {
            return mSqlSslMode;
        }
        public bool mbSqlCreateConnection()
        {
            bool bOk = false;

            if (mSqlConnection != null)
            {
                // connection already exists
                bOk = true;
            }
            else if (false == mbSqlCheckSetup())
            {
                CProgram.sLogError( "Bad Sql connection params to " + mSqlServer);
            }
            else
            {
                string connString = @"SERVER=" + mSqlServer + ";Port=" + mSqlPort.ToString() + mGetSslModeCmd();

                connString += ";DATABASE=" + mSqlDataBase + ";UID=" + mSqlUser + ";password=" + mSqlPassword + ";";

                connString += "AllowZeroDateTime=true;";    // returns DateTime.MinValue if null
//                connString += "ConnectionTimeout=30;";      // increase connection timeout from 15 to 30 seconds
                connString += "ConvertZeroDateTime=true;";    // returns DateTime.MinValue if null
//                connString += "DefaultCommandTimeout=60;";      // increase command timeout from 30 to 60 seconds
                connString += "Keepalive=15;";              // kep alive

                try
                {
                    mSqlConnection = new MySqlConnection(connString);

                    if (mSqlConnection != null)
                    {
                        mSqlConnection.Open();
                        CProgram.sLogLine(" Connected to SQL server" + mSqlServer + " . " + mSqlDataBase
                            + ( mSqlSslMode > DSqlSslMode.Default ?  ", sql mode = " + mSqlSslMode.ToString() : "" ));
                        bOk = true;
                    }
                }
                catch (Exception e)
                {
                    bOk = false;
                    CProgram.sLogException( "Failed connection to SQL server" + mSqlServer, e);
                    mSqlConnection = null;
                }
                finally
                {
                    mSqlCloseConnection();
                }
            }
            return bOk;
        }
        public void mSqlCloseConnection()
        {
            mCloseSqlCmd();
            // keep Sql connection open
            /*          if (mSqlConnection != null)
                      {
                          try
                          {
                              mSqlConnection.Close();
                          }
                          catch (Exception Ex)
                          {
                              CProgram.sLogException( "Failed closing connection to SQL server" + mSqlServer, Ex);
                          }

                      }
            */
        }

        public void mSqlForceCloseConnection()
        {
            if (mSqlConnection != null)
            {
                try
                {
                    mSqlConnection.Close();
                }
                catch (Exception Ex)
                {
                    CProgram.sLogException("Failed closing connection to SQL server" + mSqlServer, Ex);
                }

            }
        }

        public void mSqlLogOpen()
        {
            if( mSqlConnection != null)
            {
                string state = mSqlConnection.State.ToString();
                string lnString = "";

                if( _mOpenLN == 0 )
                {
                    lnString = "not opened";
                }
                else
                {
                    CClassFuncLine ln = new CClassFuncLine(_mOpenLN, _mOpenFN, _mOpenCN);

                    lnString = _mOpenLN > 0 ? "opened at " : " previousley opened at ";

                    if (ln != null)
                    {
                        lnString += ln.mGetString();
                    }
                }
                CProgram.sLogLine("dBase " + mSqlGetDBaseName() + " state= " + state + " " + lnString);
            }
        }

        public bool mbSqlOpenConnection([System.Runtime.CompilerServices.CallerLineNumber]int _AutoLN = 0,
                    [System.Runtime.CompilerServices.CallerMemberName]string _AutoFN = "",
                    [System.Runtime.CompilerServices.CallerFilePath] string _AutoCN = "")
        {
            bool bOk = false;

            if (mSqlConnection == null)
            {
                CProgram.sLogError("No connection present for SQL server" + mSqlServer);
            }
            else
            {
                try
                {
                    if (mSqlConnection.State == ConnectionState.Open)
                    {
                        // do not care if it is already open
                        bOk = true;
//                        mSqlLogOpen();
//                        CProgram.sLogLine("Open called when still open!", _AutoLN, _AutoFN, _AutoCN);
                    }
                    else
                    {
                        mSqlConnection.Open();

                        ConnectionState state = mSqlConnection.State;

                        bOk = state == ConnectionState.Open;
                        if (!bOk)
                        {
                            CProgram.sLogLine("dBase " + mSqlGetServerName() + "#" + mSqlGetDBaseName() + " is " + state.ToString());
                        }
                        else
                        {
                            _mOpenLN = _AutoLN;
                            _mOpenFN = _AutoFN;
                            _mOpenCN = _AutoCN;
                        }
                    }
                }
                catch (Exception Ex)
                {
                    CProgram.sLogException("Failed opening connection to SQL server" + mSqlServer, Ex);
                }
            }
            return bOk;
        }

        public CSqlCmd mCreateSqlCmd(CSqlDataTableRow ATable,
                    [System.Runtime.CompilerServices.CallerLineNumber]int _AutoLN = 0,
                    [System.Runtime.CompilerServices.CallerMemberName]string _AutoFN = "",
                    [System.Runtime.CompilerServices.CallerFilePath] string _AutoCN = "")
        {
            if (mSqlConnection == null)
            {
                CProgram.sLogError("SqlConnection not set ", _AutoLN, _AutoFN, _AutoCN);
                return null;
            }
            if( ATable == null)
            {
                CProgram.sLogError("Table = NULL", _AutoLN, _AutoFN, _AutoCN);
                return null;
            }
            if (false == ATable.mbCheckTableName())
            {
                CProgram.sLogError("Tabel name is not ok " + ATable.mGetDbTableName(), _AutoLN, _AutoFN, _AutoCN);
                return null;
            }
            if (_mCmdActive != null)
            {
                if (_mCmdActive.mbIsCmdReaderOpen())
                {
                    CClassFuncLine ln = new CClassFuncLine(_mOpenLN, _mOpenFN, _mOpenCN);

                    string lnString = "";

                    if (ln != null)
                    {
                        lnString = ln.mGetString();
                    }

                    CProgram.sLogLine("dBase " + mSqlGetDBaseName() + "reader{ " + lnString + "}  stil open!", _AutoLN, _AutoFN, _AutoCN);
                }
                mCloseSqlCmd();
            }
            _mCmdActive = new CSqlCmd(this, ATable);
            if( _mCmdActive != null)
            {
                _mCmdLN = _AutoLN;
                _mCmdFN = _AutoFN;
                _mCmdCN = _AutoCN;
            }

            return _mCmdActive;
        }

        public void mCloseSqlCmd(bool AbForce = false)
        {
            if (_mCmdActive != null)
            {
                CSqlCmd active = _mCmdActive;

                //_mCmdActive = null; // prevent recursive loop
                if (active != null && AbForce)
                {
                    active.mCloseCmd();
                }
             }
        }

        public double mGetFloatPrecision()
        {
            return mFloatComparePrecision;
        }
        public double mGetFloatInvalid()
        {
            return mFloatInValid;
        }
        public bool mbFloatCompareEqual(double AFloat1, double AFloat2)
        {
            return Math.Abs(AFloat1 - AFloat2) <= mFloatComparePrecision;
        }
        public bool mbFloatIsValid(double AFloat)
        {
            return Math.Abs(AFloat) < mFloatCompareValid;
        }
        public bool mbFloatIsZero(double AFloat)
        {
            return Math.Abs(AFloat) < mFloatComparePrecision;
        }
        public bool sbFloatIsNotZero(double AFloat)
        {
            double d = Math.Abs(AFloat);
            return d < mFloatCompareValid && d >= mFloatComparePrecision;
        }

        public bool mbParseFloat(string AString, ref float ArFloat)
        {
            string s = "";
            int n = AString == null ? 0 : AString.Length;
            for( int i = 0; i < n; ++i )
            {
                char c = AString[i];

                s += (c == '.' || c == ',') ? '.' : c;
            }
            return CProgram.sbParseFloat(DFloatDecimal.Dot, s, ref ArFloat);
        }
        public bool mbParseDouble(string AString, ref double ArDouble)
        {
            string s = "";
            int n = AString == null ? 0 : AString.Length;
            for (int i = 0; i < n; ++i)
            {
                char c = AString[i];

                s += (c == '.' || c == ',') ? '.' : c;
            }
            return CProgram.sbParseDouble(DFloatDecimal.Dot, s, ref ArDouble);
        }

        public bool mbParseProgramDtUtc( string AString, out DateTime ArUtc )  // program format
        {
            bool bOk = false;

            DateTime utc = DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Utc);

            bOk = CProgram.sbParseDtUtc(AString, out utc);
            if (bOk)
            {
                utc +=  mSqlTimeCorrection;
            }
            ArUtc = utc;
            return bOk; 
        }
        public bool mbParseProgramDtLocal(string AString, out DateTime ArDateTime) // program format
        {
            bool bOk = false;
            DateTime dt = DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Local);

            bOk = CProgram.sbParseDtLocal(AString, out dt);
            if (bOk)
            {
                dt += mSqlTimeCorrection;
            }
            ArDateTime = dt;

            return bOk;
        }

        public bool mbSqlParseToString(string AInString, ref string ArOutString) // parse string and convert escape chars to control chars
        {
            /*
             * Table 10.1 Special Character Escape Sequences
            Escape Sequence Character Represented by Sequence
            \0  An ASCII NUL(X'00') character
            \' 	A single quote (“'”) character
            \" 	A double quote (“"”) character
            \b A backspace character
            \n A newline(linefeed) character
            \r A carriage return character
            \t A tab character
            \Z ASCII 26(Control + Z); see note following the table
            \\ 	A backslash (“\”) character
            \% A “%” character; see note following the table
            \_ A “_” character; see note following the table
            */
            bool bOk = false;

            if( AInString != null /*&& ArOutString != null*/ )
            {
                string s = "";

                int n = AInString.Length;
                char c;

                for (int i = 0; i < n; ++i)
                {
                    c = AInString[i];
                    if( c == '\\')
                    {                       // escape char
                        if( ++i < n )
                        {
                            c = AInString[i];
                            if (c == '\'') { s += "'"; }
                            else if (c == '\"') { s += '\"'; }
                            else if (c == 'b') { s += '\b'; }
                            else if (c == 'n') { s += '\n'; }
                            else if (c == 'r') { s += '\r'; }
                            else if (c == 't') { s += '\t'; }
                            else if (c == '\\') { s += '\\'; }
                            else if (c == '%') { s += '%'; }
                            else if (c == '_') { s += '_'; }
                            else s += c;
                        }
                    }
                    else { s += c; }
                }
                ArOutString = s;
                bOk = true; 
            }
            return bOk;
        }

        public bool mbParseBool(string AString, ref bool ArBool)
        {
            bool bOk = false;

            if (AString != null)
            {
                if (0 == AString.CompareTo(mSqlTrueText) || 0 == AString.CompareTo(mSqlTrueLetter) || 0 == AString.CompareTo("1"))
                {
                    ArBool = true;
                    bOk = true;
                }
                else if (0 == AString.CompareTo(mSqlFalseText) || 0 == AString.CompareTo(mSqlFalseLetter) || 0 == AString.CompareTo("0"))
                {
                    ArBool = false;
                    bOk = true;
                }
            }
            return bOk;
        }
    }

    //==========================================================
    // CSqlDatTableRow
    //=========================
    public enum DSqlDataType
    {
        SqlError,      // return value when not ok or variable does not exist  
        SqlOther,       // unknow typ,      compare as string ('x' = 'y' )
        Sqlbool,        // bool             compare use as bool (x)
        SqlKey,         // key index,       compare as integer  (x=y)
        SqlIX,          // index,           comapre as integer  (x=y)
        SqlInteger,         // UInt??, Unt??    compare as integer  (x=y) 
        SqlFloat,       // float/double     compare with precision (ABs( x-y) < 0.0001 (presition) ) // should not be used much for searching
        SqlDate,        // date             compare as string (date difference do not occure, otherwise (x-y)< 1h)
        SqlDateTime,    // DateTime         compare time difference (TIMEDIFF(SEC,X-Y)< 1sec)
        SqlString,      // string           compare as string ('x' = 'y' )
        SqlEncryptInt,  // encrypted Int24  can only compare with 0 and equal
        SqlEncryptString // rypted string   can only compare equal
    }

    public enum DSqlDataCmd
    {
        GetType,                // return DSqlDataType + toDo : AName
        ShowString,            // return variable to user printed string
        SqlString,              // return variable to string
        ParseString,            // parse value from string
        WhereLower,             // retun where string for testing if db-value is lower then value               (string: starts width)
        WhereLowerEqual,        // retun where string for testing if db-value is lower or equal then value
        WhereEqual,             // retun where string for where testing if db-value is equal to value
        WhereHigherEqual,       // retun where string for where testing if db-value is higher or equal then value
        WhereHigher,            // retun where string for where testing if db-value is higher or equal then value   (string: contains)
        WhereNotEqual,          // retun where string for where testing if db-value is equal to value          
        WhereValid,             // return where string for testing if db-value is true or valid( not null, int != 0, string has value, float != 0.000) 
        WhereNotValid,          // return where string for testing if db-value is false or null or not valid(  int = 0, string has value, float = 0.000) 
    }

    public enum DSqlDataTableColum
    {
        CreatedUTC = 54,
        CreatedBy_IX,
        CreatedDevice_IX,
        CreatedProgID,
        ChangedUTC,
        ChangedBy_IX,
        ChangedDevice_IX,
        ChangedProgID,
        Active,
        Index_KEY              // should be 63  0x80000000000000000
    }

    public class CSqlCountItem
    {
        public string _mVarValue = "";
        public UInt32 _mCount = 0;

        public bool mbGetVarUInt32(out UInt32 ArVar)
        {
            UInt32 val = 0;
            bool bOk = _mVarValue != null && _mVarValue.Length > 0 && UInt32.TryParse(_mVarValue, out val);

            ArVar = val;
            return bOk;
        }
        public bool mbGetVarUInt16(out UInt16 ArVar)
        {
            UInt16 val = 0;
            bool bOk = _mVarValue != null && _mVarValue.Length > 0 && UInt16.TryParse(_mVarValue, out val);

            ArVar = val;
            return bOk;
        }
    }

    // CSqlEnumTableRow
    // CSqlLanguageTableRow
    public class CSqlDataTableRow
    {
        private CSqlDBaseConnection mSqlConnection;
        private string mTableName;

        // standard variables for each Table
        public UInt32 mIndex_KEY;
        public bool mbActive;
        public DateTime mCreatedUTC;
        public UInt32 mCreatedBy_IX;
        public UInt32 mCreatedDevice_IX;
        public UInt32 mCreatedProgID;
        public DateTime mChangedUTC;
        public UInt32 mChangedBy_IX;
        public UInt32 mChangedDevice_IX;
        public UInt32 mChangedProgID;

        public const uint cMaxNrColums = 64;    // flags is a Int64 
        public const uint cStartStandardColums = 54;    // flags is a Int64 
        public  UInt64 mMaskIndexKey = 0;               // used in select to get the index_Key
        public UInt64 mMaskActive = 0;                  // used in where
        public UInt64 mMaskCreated = 0;                 // used at insert together with MaskPrimary
        public UInt64 mMaskChanged = 0;                 // used at updateChanged
        public UInt64 mMaskPrimairyInsert = 0;         // setup by derived class 
        public UInt64 mMaskValid = 0;
        public UInt64 mMaskAll = 0;


/*        public CSqlDataTableRow()
        {
            mSqlConnection = null;
            mTableName = "?DataTableRow?";

            mInitTableMasks( 0, 0 );
        }
*/        public CSqlDataTableRow(CSqlDBaseConnection ASqlConnection, string ATableName)
        {
            mSqlConnection = ASqlConnection;
            mTableName = ATableName;

            mInitTableMasks( 0, 0 );
        }
        public void mSetTableNameDirect( string ATableName)
        {
            mTableName = ATableName;
        }
        public void mSetSqlConnectionTable(CSqlDBaseConnection ASqlConnection, string ATableName)
        {
            mSqlConnection = ASqlConnection;
            mTableName = ATableName;
        }

        public void mSetSqlConnection(CSqlDBaseConnection ASqlConnection)
        {
            mSqlConnection = ASqlConnection;
            if (mTableName == null || mTableName.Length == 0)
            {
                CProgram.sLogError("Tabel name is not ok");
            }  
        }

        public void mInitTableMasks( UInt64 AMaskPrimaryInsert, UInt64 AMaskValidDerived)
        {
            mMaskIndexKey = sGetMask( (UInt16)DSqlDataTableColum.Index_KEY );
            mMaskActive = sGetMask((UInt16)DSqlDataTableColum.Active);
            mMaskCreated = sGetMaskRange((UInt16)DSqlDataTableColum.CreatedUTC, (UInt16)DSqlDataTableColum.CreatedProgID);
            mMaskChanged = sGetMaskRange((UInt16)DSqlDataTableColum.ChangedUTC, (UInt16)DSqlDataTableColum.Active);
            mMaskValid = AMaskValidDerived | mMaskIndexKey | sGetMask( (UInt16)DSqlDataTableColum.Active ); // always load active and index
            mMaskAll = sGetMaskRange((UInt16)DSqlDataTableColum.ChangedUTC, (UInt16)DSqlDataTableColum.Index_KEY);
            mMaskAll |= mMaskValid;
            mMaskPrimairyInsert = AMaskPrimaryInsert;         // setup by derived class 

            mIndex_KEY = 0;
            mbActive = false;
            mCreatedUTC = DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Utc);
            mCreatedBy_IX = 0;
            mCreatedDevice_IX = 0;
            mCreatedProgID = 0;
            mChangedUTC = mCreatedUTC; ;
            mChangedBy_IX = 0;
            mChangedDevice_IX = 0;
            mChangedProgID = 0;
        }

        public void mInitTableVarRange( UInt16 ANrDerivedVars, UInt64 AMaskPrimaryInsert )
        {
            UInt64 mask = ANrDerivedVars == 0 ? 0 : sGetMaskRange(0, (UInt16)(ANrDerivedVars - 1));
            mInitTableMasks(AMaskPrimaryInsert, mask);
        }
        public string mGetTableNameOnly()
        {
            return mTableName;
        }
        public bool mbCheckTableName()
        {
            return mTableName != null && mTableName.Length > 0;
        }

        public string mGetDbTableName()         // returns dBase.tableName
        {
            string s = "$";

            if (mSqlConnection != null)
            {
                s = mSqlConnection.mSqlGetDBaseName();
            }
            s += "." + mTableName;
            return s;
        }
        public CSqlDBaseConnection mGetSqlConnection()
        {
            return mSqlConnection;
        }

        public static UInt64 sGetMask( UInt16 AFlagNr )
        {
            UInt64 mask = 0;

            if( AFlagNr < cMaxNrColums )
            {
                UInt64 bit = 0x01;
                mask = bit << AFlagNr;
            }
            return mask;
        }

        public static UInt64 sGetEmptyPrimairyMask()
        {
            return sGetMask((UInt16)DSqlDataTableColum.CreatedProgID);  // prog ID is always available
        }

        public static UInt64 sGetMaskRange(UInt16 AFirstFlagNr, UInt16 ALastFlagNr)
        {
            UInt64 mask = 0;
            UInt64 bit = 0x01;

            for ( UInt16 i = AFirstFlagNr; i <= ALastFlagNr; ++i )
            {
                if (i < cMaxNrColums)
                {
                    mask |= bit << i;
                }
            }
            return mask;
        }

        public UInt64 mGetValidMask(bool AbIncludeStandard, bool AbTestValid = false)
        {
            string name = "";
            string sqlString = "";
            UInt64 validMask = 0;
            UInt64 mask = 0x01;
            uint n = AbIncludeStandard ? cMaxNrColums : cStartStandardColums;
            bool bValid;

            for (UInt16 i = 0; i < n; ++i)
            {
                if (DSqlDataType.SqlError != mVarGetSet(DSqlDataCmd.GetType, i, ref name, ref sqlString, out bValid))
                {
                    if( bValid || false == AbTestValid )
                    {
                        validMask |= mask;
                    }
                }
                mask <<= 1;
            }
            return validMask;
        }

        public void mSetCreated2CurrentUser()
        {
            // collect user en program info
            mCreatedUTC = CProgram.sGetUtcNow();
            mCreatedBy_IX = CProgram.sGetProgUserIX();    
            mCreatedDevice_IX = CProgram.sGetDeviceIX();
            mCreatedProgID = CProgram.sGetProgID();
            mChangedUTC = mCreatedUTC; 
            mChangedBy_IX = mCreatedBy_IX;
            mChangedDevice_IX = mCreatedDevice_IX;
            mChangedProgID = mCreatedProgID;
        }
        public void mSetChanged2CurrentUser()
        {
            // collect user en program info
            mChangedUTC = CProgram.sGetUtcNow();
            mChangedBy_IX = CProgram.sGetProgUserIX();
            mChangedDevice_IX = CProgram.sGetDeviceIX();
            mChangedProgID = CProgram.sGetProgID();
        }
        // derived class must define mCreateNewRow() returning a new derived class
        public virtual CSqlDataTableRow mCreateNewRow()
        {
  //          CSqlDataTableRow row = null;
            throw (new Exception("mCreateNewRow() not defined for table " + mTableName));

//            return row;
        }
        public virtual bool mbCopyTo(CSqlDataTableRow ATo) // derived  public override void mbCopyTo() must call base.mbCopyTo(ATo)
        {
            bool bOk = false;
            if (ATo != null)
            {
                bOk = true;
                ATo.mIndex_KEY = mIndex_KEY;
                ATo.mbActive = mbActive;
                ATo.mCreatedUTC = mCreatedUTC;
                ATo.mCreatedBy_IX = mCreatedBy_IX;
                ATo.mCreatedDevice_IX = mCreatedDevice_IX;
                ATo.mCreatedProgID = mCreatedProgID;
                ATo.mChangedUTC = mChangedUTC;
                ATo.mChangedBy_IX = mChangedBy_IX;
                ATo.mChangedDevice_IX = mChangedDevice_IX;
                ATo.mChangedProgID = mChangedProgID;
            }
            return bOk;
        }

        public bool mbCopyFrom(CSqlDataTableRow AFrom)
        {

            bool bOk = false;

            if (AFrom != null)
            {
                bOk = AFrom.mbCopyTo(this);
            }
            return bOk;
        }

        public static CSqlDataTableRow sCreateCopy(CSqlDataTableRow AFrom)
        {
            CSqlDataTableRow copy = null;

            if( AFrom != null)
            {
                copy = AFrom.mCreateNewRow();

                if( copy != null )
                {
                    if (false == AFrom.mbCopyTo(copy))
                    {
                        copy = null;
                    }
                }
            }

            return copy;

        }
        public  CSqlDataTableRow mCreateCopy()
        {
            CSqlDataTableRow copy = mCreateNewRow();

            if (copy != null)
            {
                if (false == mbCopyTo(copy))
                {
                    copy = null;
                }
            }
            return copy;
        }

        public virtual void mClear() // derived  public override void mClear() must call base.mClear()
        {
            mIndex_KEY = 0;
            mbActive = false;
            mCreatedUTC = DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Utc);
            mCreatedBy_IX = 0;
            mCreatedDevice_IX = 0;
            mCreatedProgID = 0;
            mChangedUTC = mCreatedUTC; ;
            mChangedBy_IX = 0;
            mChangedDevice_IX = 0;
            mChangedProgID = 0;
        }

        // template derived class with functions incl mGetSetVar
        // mVarGetSet defines which variable and table colum name belongs to a flag, must be defined with override in derived class
        public virtual DSqlDataType mVarGetSet(DSqlDataCmd  ACmd, UInt16 AVarIndex, ref string AVarSqlName, ref string AStringValue, out bool ArbValid)
        {
           ArbValid = false;

           switch ((DSqlDataTableColum)AVarIndex)
            {
                case DSqlDataTableColum.CreatedUTC:
                    return mSqlGetSetUTC(ref mCreatedUTC, "CreatedUTC", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);

                case DSqlDataTableColum.CreatedBy_IX:
                    return mSqlGetSetUInt32(ref mCreatedBy_IX, "CreatedBy_IX", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);

                case DSqlDataTableColum.CreatedDevice_IX:
                    return mSqlGetSetUInt32(ref mCreatedDevice_IX, "CreatedDevice_IX", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);

                case DSqlDataTableColum.CreatedProgID:
                    return mSqlGetSetUInt32(ref mCreatedProgID, "CreatedProgID", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);

                case DSqlDataTableColum.ChangedUTC:
                    return mSqlGetSetUTC(ref mChangedUTC, "ChangedUTC", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);

                case DSqlDataTableColum.ChangedBy_IX:
                    return mSqlGetSetUInt32(ref mChangedBy_IX, "ChangedBy_IX", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);

                case DSqlDataTableColum.ChangedDevice_IX:
                    return mSqlGetSetUInt32(ref mChangedDevice_IX, "ChangedDevice_IX", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);

                case DSqlDataTableColum.ChangedProgID:
                    return mSqlGetSetUInt32(ref mChangedProgID, "ChangedProgID", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);

                case DSqlDataTableColum.Active:
                    return mSqlGetSetBool(ref mbActive, "Active", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);

                case DSqlDataTableColum.Index_KEY:
                    return mSqlGetSetUInt32(ref mIndex_KEY, mTableName + "_KEY", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                                                 // colum name of index must be <tableName + "_KEY"          
            }
            return DSqlDataType.SqlError;
        }

        public CSqlDataTableRow mCreateNewCopy()
        {
            CSqlDataTableRow row = null;

            try
            {
                row = mCreateNewRow();

                if (row != null)
                {
                    mbCopyTo(row);
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed CreateNewRow(" + mTableName + ")", ex);
            }
            return row;
        }

        public DSqlDataType mVarGetInfo( UInt16 AVarIndex, out string AName, out string ASqlString )
        {
            bool bValid;

            AName = "";
            ASqlString = "";
            return mVarGetSet(DSqlDataCmd.GetType, AVarIndex, ref AName, ref ASqlString, out bValid);
        }
        public DSqlDataType mVarGetType(UInt16 AVarIndex)
        {
            bool bValid;

            string name = "";
            string sqlString = "";
            return mVarGetSet(DSqlDataCmd.SqlString, AVarIndex, ref name, ref sqlString, out bValid);
        }
        public string mVarGetName(UInt16 AVarIndex, bool AbTestValid = false)
        {
            string name = "";
            string sqlString = "";
            bool bValid;

            bool b = DSqlDataType.SqlError != mVarGetSet(DSqlDataCmd.SqlString, AVarIndex, ref name, ref sqlString, out bValid);
            
            if(b && AbTestValid)
            {
                b = bValid;
            }
            return b ? name : null;
        }
        public string mVarGetShowString( UInt16 AVarIndex, bool AbTestValid = false)
        {
            string name = "";
            string sqlString = "";
            bool bValid;

            bool b = DSqlDataType.SqlError != mVarGetSet(DSqlDataCmd.ShowString, AVarIndex, ref name, ref sqlString, out bValid);

            if (b && AbTestValid)
            {
                b = bValid;
            }
            return b ? sqlString : null;

        }
        public string mVarGetSqlText(UInt16 AVarIndex, DSqlDataCmd ASqlDataCmd, bool AbTestValid = false)
        {
            string name = "";
            string sqlString = "";
            bool bValid;

            bool b = DSqlDataType.SqlError != mVarGetSet(ASqlDataCmd, AVarIndex, ref name, ref sqlString, out bValid);

            if (b && AbTestValid)
            {
                b = bValid;
            }
            return b ? sqlString : null;
        }

        public bool mVarParse(UInt16 AVarIndex, string AText)
        {
            bool bValid;

            string name = "";
            return DSqlDataType.SqlError != mVarGetSet(DSqlDataCmd.ParseString, AVarIndex, ref name, ref AText, out bValid);
        }
        public bool mVarParseObject(UInt16 AVarIndex, Object AObject)
        {
            bool bValid;

            string text = "";
            string name = "";

            if(AObject != null)
            {
                text = AObject.ToString();
            }
            return DSqlDataType.SqlError != mVarGetSet(DSqlDataCmd.ParseString, AVarIndex, ref name, ref text, out bValid);
        }

        public bool mbGetVarNames(out String AListNames, UInt64 AVarFlags, string ASeperator, bool AbTestValid = false)
        {
            bool bOk = true;
            string list = "";
            string name = "";
            string sqlString = "";         
            UInt64 mask = 0x01;

            for (UInt16 i = 0; i < cMaxNrColums; ++i)
            {
                if (0 != (AVarFlags & mask))
                {
                    bool bValid;
                    DSqlDataType result = mVarGetSet(DSqlDataCmd.GetType, i, ref name, ref sqlString, out bValid);

                    if (DSqlDataType.SqlError == result )
                    {
                        CProgram.sLogError("Failed get variable name for index " + i.ToString());
                        bOk = false;
                    }
                    else if( bValid || false == AbTestValid)
                    {
                        if( list.Length > 0 )
                        {
                            list += ASeperator;
                        }
                        list += name;
                    }
                }
                mask <<= 1;
            }
            AListNames = list;
            return bOk;
        }

        public bool mbGetVarSetList(out String AListSets, UInt64 AVarFlags, string ASeperator, bool AbTestValid = false)
        {
            bool bOk = true;
            string list = "";
            string name = "";
            string sqlString = "";
            UInt64 mask = 0x01;

            for (UInt16 i = 0; i < cMaxNrColums; ++i)
            {
                if (0 != (AVarFlags & mask))
                {
                    bool bValid;

                    DSqlDataType result = mVarGetSet(DSqlDataCmd.SqlString, i, ref name, ref sqlString, out bValid);
                    if (DSqlDataType.SqlError == result)
                    {
                        CProgram.sLogError("Failed get variable name=value for index " + i.ToString());
                        bOk = false;
                    }
                    else if( bValid || false == AbTestValid)
                    {
                        if (list.Length > 0)
                        {
                            list += ASeperator;
                        }
                        list += name + "=" + sqlString;
                    }
                }
                mask <<= 1;
            }
            AListSets = list;
            return bOk;
        }
        public bool mbGetVarValues(out String AListSets, UInt64 AVarFlags, string ASeperator, bool AbTestValid = false)
        {
            bool bOk = true;
            string list = "";
            string name = "";
            string sqlString = "";
            UInt64 mask = 0x01;

            for (UInt16 i = 0; i < cMaxNrColums; ++i)
            {
                if (0 != (AVarFlags & mask))
                {
                    bool bValid;

                    DSqlDataType result = mVarGetSet(DSqlDataCmd.SqlString, i, ref name, ref sqlString, out bValid);
                    if (DSqlDataType.SqlError == result)
                    {
                        CProgram.sLogError("Failed get variable name=value for index " + i.ToString());
                        bOk = false;
                    }
                    else if( bValid || false == AbTestValid)
                    {
                        if (list.Length > 0)
                        {
                            list += ASeperator;
                        }
                        list += sqlString;
                    }
                }
                mask <<= 1;
            }
            AListSets = list;
            return bOk;
        }

        public bool mbGetVarWhereList(out String AListSets, UInt64 AVarFlags, bool AbTestValid = false)
        {
            bool bOk = true;
            string list = "";
            string name = "";
            string sqlString = "";
            UInt64 mask = 0x01;

            for (UInt16 i = 0; i < cMaxNrColums; ++i)
            {
                if (0 != (AVarFlags & mask))
                {
                    name = "";
                    sqlString = "";
                    bool bValid;

                    DSqlDataType result = mVarGetSet(DSqlDataCmd.WhereEqual, i, ref name, ref sqlString, out bValid);
                    if (DSqlDataType.SqlError == result)
                    {
                        CProgram.sLogError("Failed get variable WHERE name=value for index " + i.ToString());
                        bOk = false;
                    }
                    else if( name == null || name.Length == 0)
                    {
                        CProgram.sLogError("Failed name is empty for get variable WHERE name=value for index " + i.ToString());
                        bOk = false;
                    }
                    else if( bValid || false == AbTestValid)
                    {
                        if (list.Length > 0)
                        {
                            list += " AND ";
                        }
                        list += sqlString;
                    }
                }
                mask <<= 1;
            }
            AListSets = list;
            return bOk;
        }

        public bool mbParseValues( UInt64 AVarFlags, Object[] AValues )
        {
            bool bOk = false;
            UInt16 index = 0, nrValues = 0;
            string name = "";
            string text;
            UInt64 mask = 0x01;
            bool bValid;

            if( AValues != null )
            {
                nrValues = (UInt16)AValues.Length;

                if( nrValues > 0 )
                {
                    bOk = true;
                    for (UInt16 i = 0; i < cMaxNrColums; ++i)
                    {
                        if (0 != (AVarFlags & mask) && index < nrValues )   // last values can be null and not stored in array
                        {
                            text = "";
                            if( AValues[index] != null)
                            {
                                text = AValues[index].ToString();
                            }
                            DSqlDataType result = mVarGetSet(DSqlDataCmd.ParseString, i, ref name, ref text, out bValid);
                            if (DSqlDataType.SqlError == result)
                            {
                                CProgram.sLogError("Failed parsing variable[" + index + "] for index " + i.ToString());
                                bOk = false;
                            }
                            ++index; // next variable
                        }
                        mask <<= 1;
                    }
                }
            }
            return bOk;
        }
        public string mSqlFloatSqlString( float AFloat )
        {
            return mSqlConnection == null ? null : mSqlConnection.mSqlFloatSqlString(AFloat);
        }
        public string mSqlDoubleSqlString(double ADouble)
        {
            return mSqlConnection == null ? null : mSqlConnection.mSqlDoubleSqlString(ADouble);
        }
        public string mSqlBoolSqlString(bool AbBool)
        {
            return mSqlConnection == null ? null : mSqlConnection.mSqlBoolSqlString(AbBool);
        }
        public string mSqlDateTimeSqlString(DateTime ADateTime)
        {
            return mSqlConnection == null ? null : mSqlConnection.mSqlDateTimeSqlString(ADateTime);
        }
        public string mSqlDateSqlString(DateTime ADateTime)
        {
            return mSqlConnection == null ? null : mSqlConnection.mSqlDateSqlString(ADateTime);
        }

        public string mSqlStringSqlString( string AString )
        {
            return mSqlConnection == null ? null : mSqlConnection.mSqlConvertToSqlString(AString);
        }
        public string mSqlCompareDoubleSqlString(string AName, double AValue)
        {
            return mSqlConnection == null ? null : mSqlConnection.mSqlCompareDoubleSqlString(AName, AValue);
        }
        public string mSqlDoubleIsValidSqlString(string AName)
        {
            return mSqlConnection == null ? null : mSqlConnection.mSqlDoubleIsValidSqlString(AName);
        }
        public string mSqlDoubleIsZeroSqlString(string AName)
        {
            return mSqlConnection == null ? null : mSqlConnection.mSqlDoubleIsZeroSqlString(AName);
        }
        public string mSqlDoubleNotZeroSqlString(string AName)
        {
            return mSqlConnection == null ? null : mSqlConnection.mSqlDoubleNotZeroSqlString(AName);
        }
        public string mSqlDateTimeCompareSqlString(string AName, DateTime ADateTime, DSqlCompareDateTime ACompareAs, float AWithin)
        {
            return mSqlConnection == null ? null : mSqlConnection.mSqlDateTimeComparSqlString(AName, ADateTime, ACompareAs, AWithin);
        }

        public string mSqlStringContainsSqlString( string AName, string AContains)
        {
            return mSqlConnection == null ? null : mSqlConnection.mSqlStringContainsSqlString(AName, AContains);
        }
        public string mSqlStringStartsWithSqlString(string AName, string AStartsWith)
        {
            return mSqlConnection == null ? null : mSqlConnection.mSqlStringStartsWithSqlString(AName, AStartsWith);
        }

        public static string sGetActiveChar( UInt16 AState)
        {
            if( AState < (UInt16)DTableRowActiveState.NrTableRowStates)
            {
                switch( (DTableRowActiveState)AState)
                {
                    case DTableRowActiveState.Disabled:   return "🚫";
                    case DTableRowActiveState.Enabled:    return ".";
                    case DTableRowActiveState.Locked:     return "🔒";
                    case DTableRowActiveState.Archived:   return "🛡";
                }
            }
            return "?";
        }

        // get/set variables

        /* c# standard types
         Type		range 		description
         sbyte		-128 to 127	Signed 8-bit integer
         byte		0 to 255	Unsigned 8-bit integer
         char		U+0000 to U+ffff	Unicode 16-bit character
         short		-32,768 to 32,767	Signed 16-bit integer
         ushort		0 to 65,535	Unsigned 16-bit integer
         int		-2,147,483,648 to 2,147,483,647	Signed 32-bit integer
         uint		0 to 4,294,967,295	Unsigned 32-bit integer
         long		-9,223,372,036,854,775,808 to 9,223,372,036,854,775,807	Signed 64-bit integer
         ulong		0 to 18,446,744,073,709,551,615	Unsigned 64-bit integer
         float		±1.5e−45 to ±3.4e38	7 digits
         double		±5.0e−324 to ±1.7e308	15-16 digits
         String		char (U+0000 to U+ffff) array
         DateTime	Date + Time
         Date		bestaat niet gebruik 
         DateTimeOffset
         TimeSpan

                   mySql types             use standard
                   TINYINT                 sbyte
                   TYNYINT UNSIGNED        byte
                   BOOL                    bool
                   SMALLINT                Int16
                   SMALLINT UNSIGNED       UInt16
                   INT                     Int32
                   INT UNSIGNED            UInt32
                   FLOAT                   float
                   DOUBLE                  double
                   DATE                    DateTime
                   TIMESTAMP               DateTime    // if possible  store DateTime in UTC!!
                                           TimeSpan -> float sec
                   CHAR(m)                 string      // fixed max length text
                   TINYTEXT                string      // smal text with variable length
                   TEXT                    string      // 
         */
        // used in CSqlTableRow
        // todo omzetten naar DSqlDataType mSqlGetSetXXXX(ref bool ArVar, string AName, DSqlDataCmd ACmd, string AParameter, out string ArStringValue)
        public DSqlDataType mSqlGetSetBool(ref bool ArVar, string AName, DSqlDataCmd ACmd, ref string ArSqlName, ref string ArStringValue, out bool ArbValid)
        {
            DSqlDataType dataType = DSqlDataType.Sqlbool;
            bool bOk = false;
            bool bValid = true;

            if (ArSqlName != null)         // Set ArSqlName if not null
            {
                ArSqlName = AName;
            }
            if (ACmd == DSqlDataCmd.GetType)
            {
                // to do if AParameter ArStringValue = AName;
                bOk = true;
            }
            else if (mSqlConnection != null)
            {
                if (ACmd == DSqlDataCmd.ParseString)
                {
                    bOk = mSqlConnection.mbParseBool(ArStringValue, ref ArVar);
                }
                else if (ArStringValue != null)
                {                  
                    string v = mSqlBoolSqlString(ArVar); // variable value
                    string s = null;

                    switch (ACmd)
                    {
                        case DSqlDataCmd.SqlString:          s = v; break;
                        case DSqlDataCmd.ShowString:        s = CProgram.sBoolToProgString(ArVar); break;
                        case DSqlDataCmd.WhereLower:        s = ArVar ? AName + "=" + mSqlBoolSqlString(false) : mSqlBoolSqlString(true); break;
                        case DSqlDataCmd.WhereLowerEqual:   s = mSqlBoolSqlString(true);  break;
                        case DSqlDataCmd.WhereEqual:        s = AName + "=" + v; break;
                        case DSqlDataCmd.WhereHigherEqual:  s = mSqlBoolSqlString(true); break;
                        case DSqlDataCmd.WhereHigher:       s = ArVar ? mSqlBoolSqlString(false) : AName + "=" + mSqlBoolSqlString(true); break;
                        case DSqlDataCmd.WhereNotEqual:     s = AName + "!=" + v; break;
                        case DSqlDataCmd.WhereValid:        s = AName; break;
                        case DSqlDataCmd.WhereNotValid:     s = "!" + AName; break;
                    }
                    if (s != null && s.Length > 0)
                    {
                        ArStringValue = s;
                        bOk = true;
                    }
                }
            }
            ArbValid = bValid;
            return bOk ? dataType : DSqlDataType.SqlError;
        }
        public DSqlDataType mSqlGetSetSByte(ref sbyte ArVar, string AName, DSqlDataCmd ACmd, ref string ArSqlName, ref string ArStringValue, out bool ArbValid)
        {
            DSqlDataType dataType = DSqlDataType.SqlInteger;
            bool bOk = false;
            bool bValid = ArVar != 0;

            if (ArSqlName != null)         // Set ArSqlName if not null
            {
                ArSqlName = AName;
            }
            if (ACmd == DSqlDataCmd.GetType)
            {
                bOk = true;
            }
            else if (mSqlConnection != null )
            {
                if (ACmd == DSqlDataCmd.ParseString)
                {
                    bOk = sbyte.TryParse(ArStringValue, out ArVar);
                }
                else if (ArStringValue != null)
                {
                    string v = ArVar.ToString(); // variable value
                    string s = null;

                    switch (ACmd)
                    {
                        case DSqlDataCmd.SqlString:         s = v; break;
                        case DSqlDataCmd.ShowString:        s = v; break;
                        case DSqlDataCmd.WhereLower:        s = AName + "<" + v; break;
                        case DSqlDataCmd.WhereLowerEqual:   s = AName + "<=" + v; break;
                        case DSqlDataCmd.WhereEqual:        s = AName + "=" + v; break;
                        case DSqlDataCmd.WhereHigherEqual:  s = AName + ">=" + v; break;
                        case DSqlDataCmd.WhereHigher:       s = AName + ">=" + v; break;
                        case DSqlDataCmd.WhereNotEqual:     s = AName + "!=" + v; break;
                        case DSqlDataCmd.WhereValid:        s = AName + "!=0"; break;
                        case DSqlDataCmd.WhereNotValid:     s = AName + "=0"; break;
                    }
                    if( s != null && s.Length > 0 )
                    {
                        ArStringValue = s;
                        bOk = true;
                    }
                }
            }
            ArbValid = bValid;
            return bOk ? dataType : DSqlDataType.SqlError;
        }
        public DSqlDataType mSqlGetSetUByte(ref byte ArVar, string AName, DSqlDataCmd ACmd, ref string ArSqlName, ref string ArStringValue, out bool ArbValid)
        {
            DSqlDataType dataType = DSqlDataType.SqlInteger;
            bool bOk = false;
            bool bValid = ArVar != 0;

            if (ArSqlName != null)         // Set ArSqlName if not null
            {
                ArSqlName = AName;
            }
            if (ACmd == DSqlDataCmd.GetType)    // returning type is possible without a connection
            {
                bOk = true;
            }
            else if (mSqlConnection != null)
            {
                if (ACmd == DSqlDataCmd.ParseString)
                {
                    bOk = byte.TryParse(ArStringValue, out ArVar);
                }
                else if (ArStringValue != null)
                {
                    string v = ArVar.ToString(); // variable value
                    string s = null;

                    switch (ACmd)
                    {
                        case DSqlDataCmd.SqlString: s = v; break;
                        case DSqlDataCmd.ShowString: s = v; break;
                        case DSqlDataCmd.WhereLower: s = AName + "<" + v; break;
                        case DSqlDataCmd.WhereLowerEqual: s = AName + "<=" + v; break;
                        case DSqlDataCmd.WhereEqual: s = AName + "=" + v; break;
                        case DSqlDataCmd.WhereHigherEqual: s = AName + ">=" + v; break;
                        case DSqlDataCmd.WhereHigher: s = AName + ">=" + v; break;
                        case DSqlDataCmd.WhereNotEqual: s = AName + "!=" + v; break;
                        case DSqlDataCmd.WhereValid: s = AName + "!=0"; break;
                        case DSqlDataCmd.WhereNotValid: s = AName + "=0"; break;
                    }
                    if (s != null && s.Length > 0)
                    {
                        ArStringValue = s;
                        bOk = true;
                    }
                }
            }
            ArbValid = bValid;
            return bOk ? dataType : DSqlDataType.SqlError;
        }
        public DSqlDataType mSqlGetSetYear(ref UInt16 ArVar, string AName, DSqlDataCmd ACmd, ref string ArSqlName, ref string ArStringValue, out bool ArbValid)
        {
            if(ArVar > 0 )
            {
                if( ArVar <= 1900 )
                {
                    ArVar = 0;
                }
                else if( ArVar > 2155 )
                {
                    ArVar = 0;
                }
            }
            return mSqlGetSetUInt16(ref ArVar, AName, ACmd, ref ArSqlName, ref ArStringValue, out ArbValid);
        }

        public DSqlDataType mSqlGetSetUInt16(ref UInt16 ArVar, string AName, DSqlDataCmd ACmd, ref string ArSqlName, ref string ArStringValue, out bool ArbValid)
        {
            DSqlDataType dataType = DSqlDataType.SqlInteger;
            bool bOk = false;
            bool bValid = ArVar != 0;

            if (ArSqlName != null)         // Set ArSqlName if not null
            {
                ArSqlName = AName;
            }
            if (ACmd == DSqlDataCmd.GetType)    // returning type is possible without a connection
            {
                bOk = true;
            }
            else if (mSqlConnection != null)
            {
               if (ACmd == DSqlDataCmd.ParseString)
                {
                    bOk = UInt16.TryParse(ArStringValue, out ArVar);
                }
                else if (ArStringValue != null)
                {
                    string v = ArVar.ToString(); // variable value
                    string s = null;

                    switch (ACmd)
                    {
                        case DSqlDataCmd.SqlString: s = v; break;
                        case DSqlDataCmd.ShowString: s = v; break;
                        case DSqlDataCmd.WhereLower: s = AName + "<" + v; break;
                        case DSqlDataCmd.WhereLowerEqual: s = AName + "<=" + v; break;
                        case DSqlDataCmd.WhereEqual: s = AName + "=" + v; break;
                        case DSqlDataCmd.WhereHigherEqual: s = AName + ">=" + v; break;
                        case DSqlDataCmd.WhereHigher: s = AName + ">=" + v; break;
                        case DSqlDataCmd.WhereNotEqual: s = AName + "!=" + v; break;
                        case DSqlDataCmd.WhereValid: s = AName + "!=0"; break;
                        case DSqlDataCmd.WhereNotValid: s = AName + "=0"; break;
                    }
                    if (s != null && s.Length > 0)
                    {
                        ArStringValue = s;
                        bOk = true;
                    }
                }
            }
            ArbValid = bValid;
            return bOk ? dataType : DSqlDataType.SqlError;
        }
        public DSqlDataType mSqlGetSetInt16(ref Int16 ArVar, string AName, DSqlDataCmd ACmd, ref string ArSqlName, ref string ArStringValue, out bool ArbValid)
        {
            DSqlDataType dataType = DSqlDataType.SqlInteger;
            bool bOk = false;
            bool bValid = ArVar != 0;

            if (ArSqlName != null)         // Set ArSqlName if not null
            {
                ArSqlName = AName;
            }
            if (ACmd == DSqlDataCmd.GetType)    // returning type is possible without a connection
            {
                bOk = true;
            }
            else if (mSqlConnection != null)
            {
                if (ACmd == DSqlDataCmd.ParseString)
                {
                    bOk = Int16.TryParse(ArStringValue, out ArVar);
                }
                else if (ArStringValue != null)
                {
                    string v = ArVar.ToString(); // variable value
                    string s = null;

                    switch (ACmd)
                    {
                        case DSqlDataCmd.SqlString: s = v; break;
                        case DSqlDataCmd.ShowString: s = v; break;
                        case DSqlDataCmd.WhereLower: s = AName + "<" + v; break;
                        case DSqlDataCmd.WhereLowerEqual: s = AName + "<=" + v; break;
                        case DSqlDataCmd.WhereEqual: s = AName + "=" + v; break;
                        case DSqlDataCmd.WhereHigherEqual: s = AName + ">=" + v; break;
                        case DSqlDataCmd.WhereHigher: s = AName + ">=" + v; break;
                        case DSqlDataCmd.WhereNotEqual: s = AName + "!=" + v; break;
                        case DSqlDataCmd.WhereValid: s = AName + "!=0"; break;
                        case DSqlDataCmd.WhereNotValid: s = AName + "=0"; break;
                    }
                    if (s != null && s.Length > 0)
                    {
                        ArStringValue = s;
                        bOk = true;
                    }
                }
            }
            ArbValid = bValid;
            return bOk ? dataType : DSqlDataType.SqlError;
        }
        public DSqlDataType mSqlGetSetInt32(ref Int32 ArVar, string AName, DSqlDataCmd ACmd, ref string ArSqlName, ref string ArStringValue, out bool ArbValid)
        {
            DSqlDataType dataType = DSqlDataType.SqlInteger;
            bool bOk = false;
            bool bValid = ArVar != 0;

            if (ArSqlName != null)         // Set ArSqlName if not null
            {
                ArSqlName = AName;
            }
            if (ACmd == DSqlDataCmd.GetType)    // returning type is possible without a connection
            {
                bOk = true;
            }
            else if (mSqlConnection != null)
            {
                if (ACmd == DSqlDataCmd.ParseString)
                {
                    bOk = Int32.TryParse(ArStringValue, out ArVar);
                }
                else if (ArStringValue != null)
                {
                    string v = ArVar.ToString(); // variable value
                    string s = null;

                    switch (ACmd)
                    {
                        case DSqlDataCmd.SqlString: s = v; break;
                        case DSqlDataCmd.ShowString: s = v; break;
                        case DSqlDataCmd.WhereLower: s = AName + "<" + v; break;
                        case DSqlDataCmd.WhereLowerEqual: s = AName + "<=" + v; break;
                        case DSqlDataCmd.WhereEqual: s = AName + "=" + v; break;
                        case DSqlDataCmd.WhereHigherEqual: s = AName + ">=" + v; break;
                        case DSqlDataCmd.WhereHigher: s = AName + ">=" + v; break;
                        case DSqlDataCmd.WhereNotEqual: s = AName + "!=" + v; break;
                        case DSqlDataCmd.WhereValid: s = AName + "!=0"; break;
                        case DSqlDataCmd.WhereNotValid: s = AName + "=0"; break;
                    }
                    if (s != null && s.Length > 0)
                    {
                        ArStringValue = s;
                        bOk = true;
                    }
                }
            }
            ArbValid = bValid;
            return bOk ? dataType : DSqlDataType.SqlError;
        }
        public DSqlDataType mSqlGetSetUInt32(ref UInt32 ArVar, string AName, DSqlDataCmd ACmd, ref string ArSqlName, ref string ArStringValue, out bool ArbValid)
        {
            DSqlDataType dataType = DSqlDataType.SqlInteger;
            bool bOk = false;
            bool bValid =  ArVar != 0;
            
            if (ArSqlName != null)         // Set ArSqlName if not null
            {
                ArSqlName = AName;
            }
            if (ACmd == DSqlDataCmd.GetType)    // returning type is possible without a connection
            {
                bOk = true;
            }
            else if (mSqlConnection != null)
            {
                if (ACmd == DSqlDataCmd.ParseString)
                {
                    bOk = UInt32.TryParse(ArStringValue, out ArVar);
                }
                else if (ArStringValue != null)
                {
                    string v = ArVar.ToString(); // variable value
                    string s = null;

                    switch (ACmd)
                    {
                        case DSqlDataCmd.SqlString: s = v; break;
                        case DSqlDataCmd.ShowString: s = v; break;
                        case DSqlDataCmd.WhereLower: s = AName + "<" + v; break;
                        case DSqlDataCmd.WhereLowerEqual: s = AName + "<=" + v; break;
                        case DSqlDataCmd.WhereEqual: s = AName + "=" + v; break;
                        case DSqlDataCmd.WhereHigherEqual: s = AName + ">=" + v; break;
                        case DSqlDataCmd.WhereHigher: s = AName + ">=" + v; break;
                        case DSqlDataCmd.WhereNotEqual: s = AName + "!=" + v; break;
                        case DSqlDataCmd.WhereValid: s = AName + "!=0"; break;
                        case DSqlDataCmd.WhereNotValid: s = AName + "=0"; break;
                    }
                    if (s != null && s.Length > 0)
                    {
                        ArStringValue = s;
                        bOk = true;
                    }
                }
            }
            ArbValid = bValid;
            return bOk ? dataType : DSqlDataType.SqlError;
        }
        public DSqlDataType mSqlGetSetInt64(ref Int64 ArVar, string AName, DSqlDataCmd ACmd, ref string ArSqlName, ref string ArStringValue, out bool ArbValid)
        {
            DSqlDataType dataType = DSqlDataType.SqlInteger;
            bool bOk = false;
            bool bValid = ArVar != 0;

            if (ArSqlName != null)         // Set ArSqlName if not null
            {
                ArSqlName = AName;
            }
            if (ACmd == DSqlDataCmd.GetType)    // returning type is possible without a connection
            {
                bOk = true;
            }
            else if (mSqlConnection != null)
            {
                if (ACmd == DSqlDataCmd.ParseString)
                {
                    bOk = Int64.TryParse(ArStringValue, out ArVar);
                }
                else if (ArStringValue != null)
                {
                    string v = ArVar.ToString(); // variable value
                    string s = null;

                    switch (ACmd)
                    {
                        case DSqlDataCmd.SqlString: s = v; break;
                        case DSqlDataCmd.ShowString: s = v; break;
                        case DSqlDataCmd.WhereLower: s = AName + "<" + v; break;
                        case DSqlDataCmd.WhereLowerEqual: s = AName + "<=" + v; break;
                        case DSqlDataCmd.WhereEqual: s = AName + "=" + v; break;
                        case DSqlDataCmd.WhereHigherEqual: s = AName + ">=" + v; break;
                        case DSqlDataCmd.WhereHigher: s = AName + ">=" + v; break;
                        case DSqlDataCmd.WhereNotEqual: s = AName + "!=" + v; break;
                        case DSqlDataCmd.WhereValid: s = AName + "!=0"; break;
                        case DSqlDataCmd.WhereNotValid: s = AName + "=0"; break;
                    }
                    if (s != null && s.Length > 0)
                    {
                        ArStringValue = s;
                        bOk = true;
                    }
                }
            }
            ArbValid = bValid;
            return bOk ? dataType : DSqlDataType.SqlError;
        }
        public DSqlDataType mSqlGetSetUInt64(ref UInt64 ArVar, string AName, DSqlDataCmd ACmd, ref string ArSqlName, ref string ArStringValue, out bool ArbValid)
        {
            DSqlDataType dataType = DSqlDataType.SqlInteger;
            bool bOk = false;
            bool bValid = ArVar != 0;

            if (ArSqlName != null)         // Set ArSqlName if not null
            {
                ArSqlName = AName;
            }
            if (ACmd == DSqlDataCmd.GetType)    // returning type is possible without a connection
            {
                bOk = true;
            }
            else if (mSqlConnection != null)
            {
                if (ACmd == DSqlDataCmd.ParseString)
                {
                    bOk = UInt64.TryParse(ArStringValue, out ArVar);
                }
                else if (ArStringValue != null)
                {
                    string v = ArVar.ToString(); // variable value
                    string s = null;

                    switch (ACmd)
                    {
                        case DSqlDataCmd.SqlString: s = v; break;
                        case DSqlDataCmd.ShowString: s = v; break;
                        case DSqlDataCmd.WhereLower: s = AName + "<" + v; break;
                        case DSqlDataCmd.WhereLowerEqual: s = AName + "<=" + v; break;
                        case DSqlDataCmd.WhereEqual: s = AName + "=" + v; break;
                        case DSqlDataCmd.WhereHigherEqual: s = AName + ">=" + v; break;
                        case DSqlDataCmd.WhereHigher: s = AName + ">=" + v; break;
                        case DSqlDataCmd.WhereNotEqual: s = AName + "!=" + v; break;
                        case DSqlDataCmd.WhereValid: s = AName + "!=0"; break;
                        case DSqlDataCmd.WhereNotValid: s = AName + "=0"; break;
                    }
                    if (s != null && s.Length > 0)
                    {
                        ArStringValue = s;
                        bOk = true;
                    }
                }
            }
            ArbValid = bValid;
            return bOk ? dataType : DSqlDataType.SqlError;
        }
        public DSqlDataType mSqlGetSetBitSet32(ref CBitSet32 ArVar, string AName, DSqlDataCmd ACmd, ref string ArSqlName, ref string ArStringValue, out bool ArbValid)
        {
            DSqlDataType dataType = DSqlDataType.SqlInteger;
            bool bOk = false;
            bool bValid = ArVar._mBitSet != 0U;

            if (ArSqlName != null)         // Set ArSqlName if not null
            {
                ArSqlName = AName;
            }
            if (ACmd == DSqlDataCmd.GetType)    // returning type is possible without a connection
            {
                bOk = true;
            }
            else if (mSqlConnection != null)
            {
                if (ACmd == DSqlDataCmd.ParseString)
                {
                    bOk = UInt32.TryParse(ArStringValue, out ArVar._mBitSet);
                }
                else if (ArStringValue != null)
                {
                    string v = ArVar._mBitSet.ToString(); // variable value
                    string s = mCmdBitSetString( AName, ACmd, v, bValid );

                    if (s != null && s.Length > 0)
                    {
                        ArStringValue = s;
                        bOk = true;
                    }
                }
            }
            ArbValid = bValid;
            return bOk ? dataType : DSqlDataType.SqlError;
        }

        private string mCmdBitSetString( string AName, DSqlDataCmd ACmd, string AValue, bool AbValid)
        {
            string s = "";

            switch (ACmd)
            {
                case DSqlDataCmd.SqlString: s = AValue; break;
                case DSqlDataCmd.ShowString: s = AValue; break;
                case DSqlDataCmd.WhereLower: s = "(" + AName + " & " + AValue + " != " + AValue + ")"; break;  // bits requested not present in var
                case DSqlDataCmd.WhereLowerEqual: s = AbValid ? "(" + AName + " & " + AValue + " != " + AValue + " )" : AName + "=0"; break; // bits not present in var
                case DSqlDataCmd.WhereEqual: s = AbValid ? "(" + AName + "!= 0 AND (" + AName + " & " + AValue + " = " + AName + "))" : AName + "=0"; break;    // all bits present in var
                case DSqlDataCmd.WhereHigherEqual: s = AbValid ? "(" + AName + " = 0 OR (" + AName + " & " + AValue + " = " + AName + "))" : AName + "!=0"; break;    // var empty or all bits present in var
                case DSqlDataCmd.WhereHigher: s = "(" + AName + " = 0 OR (" + AName + " & " + AValue + " = " + AName + "))"; break;    // var empty or all bits present in var
                case DSqlDataCmd.WhereNotEqual: s = "(" + AName + " & " + AValue + " != " + AName+ ")"; break;    // BitSet empty / not present
                case DSqlDataCmd.WhereValid: s = AName + "!=0"; break; // BitSet not empty / present
                case DSqlDataCmd.WhereNotValid: s = AName + "=0"; break; // BitSet empty / not present
            }
            return s;
        }

        public DSqlDataType mSqlGetSetBitSet64(ref CBitSet64 ArVar, string AName, DSqlDataCmd ACmd, ref string ArSqlName, ref string ArStringValue, out bool ArbValid)
        {
            DSqlDataType dataType = DSqlDataType.SqlInteger;
            bool bOk = false;
            bool bValid = ArVar._mBitSet != 0UL;

            if (ArSqlName != null)         // Set ArSqlName if not null
            {
                ArSqlName = AName;
            }
            if (ACmd == DSqlDataCmd.GetType)    // returning type is possible without a connection
            {
                bOk = true;
            }
            else if (mSqlConnection != null)
            {
                if (ACmd == DSqlDataCmd.ParseString)
                {
                    bOk = UInt64.TryParse(ArStringValue, out ArVar._mBitSet);
                }
                else if (ArStringValue != null)
                {
                    string v = ArVar._mBitSet.ToString(); // variable value
                    string s = mCmdBitSetString(AName, ACmd, v, bValid);
                    if (s != null && s.Length > 0)
                    {
                        ArStringValue = s;
                        bOk = true;
                    }
                }
            }
            ArbValid = bValid;
            return bOk ? dataType : DSqlDataType.SqlError;
        }


        public DSqlDataType mSqlGetSetFloat(ref float ArVar, string AName, DSqlDataCmd ACmd, ref string ArSqlName, ref string ArStringValue, out bool ArbValid)
        {
            DSqlDataType dataType = DSqlDataType.SqlFloat;
            bool bOk = false;
            bool bValid = mSqlConnection == null ? false : mSqlConnection.sbFloatIsNotZero(ArVar);

            if (ArSqlName != null)         // Set ArSqlName if not null
            {
                ArSqlName = AName;
            }
            if (ACmd == DSqlDataCmd.GetType)    // returning type is possible without a connection
            {
                bOk = true;
            }
            else if (mSqlConnection != null)
            {
                if (ACmd == DSqlDataCmd.ParseString)
                {
                    bOk = mSqlConnection.mbParseFloat(ArStringValue, ref ArVar);
                }
                else if (ArStringValue != null)
                {
                    string v = mSqlConnection.mSqlFloatSqlString( ArVar ); // variable value
                    string s = null;

                    switch (ACmd)
                    {
                        case DSqlDataCmd.SqlString: s = v; break;
                        case DSqlDataCmd.ShowString: s = CProgram.sShowFloat( ArVar, 6 ); break;
                        case DSqlDataCmd.WhereLower: s = AName + "<" + v; break;
                        case DSqlDataCmd.WhereLowerEqual: s = AName + "<=" + v; break;
                        case DSqlDataCmd.WhereEqual: s = mSqlCompareDoubleSqlString(AName,ArVar); break;
                        case DSqlDataCmd.WhereHigherEqual: s = AName + ">=" + v; break;
                        case DSqlDataCmd.WhereHigher: s = AName + ">=" + v; break;
                        case DSqlDataCmd.WhereNotEqual: s = AName + "!=" + v; break;
                        case DSqlDataCmd.WhereValid: s = mSqlDoubleIsValidSqlString( AName ); break;
                        case DSqlDataCmd.WhereNotValid: s = mSqlDoubleIsValidSqlString( AName ); break;
                    }
                    if (s != null && s.Length > 0)
                    {
                        ArStringValue = s;
                        bOk = true;
                    }
                }
            }
            ArbValid = bValid;
            return bOk ? dataType : DSqlDataType.SqlError;
        }
        public DSqlDataType mSqlGetSetDouble(ref double ArVar, string AName, DSqlDataCmd ACmd, ref string ArSqlName, ref string ArStringValue, out bool ArbValid)
        {
            DSqlDataType dataType = DSqlDataType.SqlFloat;
            bool bOk = false;

            bool bValid = mSqlConnection == null ? false : mSqlConnection.sbFloatIsNotZero( ArVar );

            if (ArSqlName != null)         // Set ArSqlName if not null
            {
                ArSqlName = AName;
            }
            if (ACmd == DSqlDataCmd.GetType)    // returning type is possible without a connection
            {
                bOk = true;
            }
            else if (mSqlConnection != null)
            {
                if (ACmd == DSqlDataCmd.ParseString)
                {
                    bOk = mSqlConnection.mbParseDouble(ArStringValue, ref ArVar);
                }
                else if (ArStringValue != null)
                {
                    string v = mSqlConnection.mSqlDoubleSqlString(ArVar); // variable value
                    string s = null;

                    switch (ACmd)
                    {
                        case DSqlDataCmd.SqlString: s = v; break;
                        case DSqlDataCmd.ShowString: s = CProgram.sShowDouble(ArVar, 6); break;
                        case DSqlDataCmd.WhereLower: s = AName + "<" + v; break;
                        case DSqlDataCmd.WhereLowerEqual: s = AName + "<=" + v; break;
                        case DSqlDataCmd.WhereEqual: s = mSqlCompareDoubleSqlString(AName, ArVar); break;
                        case DSqlDataCmd.WhereHigherEqual: s = AName + ">=" + v; break;
                        case DSqlDataCmd.WhereHigher: s = AName + ">=" + v; break;
                        case DSqlDataCmd.WhereNotEqual: s = AName + "!=" + v; break;
                        case DSqlDataCmd.WhereValid: s = mSqlDoubleIsValidSqlString(AName); break;
                        case DSqlDataCmd.WhereNotValid: s = mSqlDoubleIsValidSqlString(AName); break;
                    }
                    if (s != null && s.Length > 0)
                    {
                        ArStringValue = s;
                        bOk = true;
                    }
                }
            }
            ArbValid = bValid;
            return bOk ? dataType : DSqlDataType.SqlError;
        }
        public DSqlDataType mSqlGetSetString(ref string ArVar, string AName, DSqlDataCmd ACmd, ref string ArSqlName, ref string ArStringValue, UInt16 ASizeLimit, out bool ArbValid)
        {
            DSqlDataType dataType = DSqlDataType.SqlString;
            bool bOk = false;

            bool bValid = false;

            if (ArVar != null)
            {
                bValid = ArVar.Length > 0;
            }
            if (ArSqlName != null)         // Set ArSqlName if not null
            {
                ArSqlName = AName;
            }
            if (ACmd == DSqlDataCmd.GetType)
            {
                bOk = true;
            }
            else if (mSqlConnection != null)
            {
                if (ACmd == DSqlDataCmd.ParseString)
                {
                    bOk = mSqlConnection.mbSqlParseToString( ArStringValue, ref ArVar);
                }
                else if (ArStringValue != null)
                {
                    string v = ArVar;
                    string s = null;

                    if( v != null && v.Length > ASizeLimit )
                    {
                        v = v.Substring(0, ASizeLimit);
                    }
                    v = mSqlStringSqlString(v);
                    switch (ACmd)
                    {
                        case DSqlDataCmd.SqlString:          s = v; break;
                        case DSqlDataCmd.ShowString:        s = ArVar; break;
                        case DSqlDataCmd.WhereLower:        s = mSqlStringStartsWithSqlString( AName, v ); break;
                        case DSqlDataCmd.WhereLowerEqual:   s = mSqlStringStartsWithSqlString(AName, v); break;
                        case DSqlDataCmd.WhereEqual:        s = AName + "=" + v; break;
                        case DSqlDataCmd.WhereHigherEqual:  s = mSqlStringContainsSqlString(AName, v); break;
                        case DSqlDataCmd.WhereHigher:       s = mSqlStringContainsSqlString(AName, v); break;
                        case DSqlDataCmd.WhereNotEqual:     s = AName + "!=" + v; break;
                        case DSqlDataCmd.WhereValid:        s = "LENGTH(" + AName + ")>0"; break;
                        case DSqlDataCmd.WhereNotValid:     s = "LENGTH(" + AName + ")=0"; break;
                    }
                    if (s != null && s.Length > 0)
                    {
                        ArStringValue = s;
                        bOk = true;
                    }
                }
            }
            ArbValid = bValid;
            return bOk ? dataType : DSqlDataType.SqlError;
        }
        public DSqlDataType mSqlGetSetDate(ref DateTime ArVar, string AName, DSqlDataCmd ACmd, ref string ArSqlName, ref string ArStringValue, out bool ArbValid) // forces to Local time
        {
            //mSqlDateTimeCompareSecSqlString(AName, ADateTime, ACompareAs, AWithin);
            DSqlDataType dataType = DSqlDataType.SqlDateTime;
            bool bOk = false;
            bool bValid = false;

            if (ArVar != null)
            {
                bValid = ArVar != DateTime.MinValue;
            }

            if (ArSqlName != null)         // Set ArSqlName if not null
            {
                ArSqlName = AName;
            }
            if (ACmd == DSqlDataCmd.GetType)    // returning type is possible without a connection
            {
                bOk = true;
            }
            else if (mSqlConnection != null)
            {
                if (ACmd == DSqlDataCmd.ParseString)
                {
                    bOk = mSqlConnection.mbParseProgramDtLocal(ArStringValue, out ArVar);  // got string in program local format
                }
                else if (ArStringValue != null)
                {
                    string v = mSqlDateSqlString( ArVar ); // variable value
                    string s = null;

                    switch (ACmd)
                    {
                        case DSqlDataCmd.SqlString: s = v; break;
                        case DSqlDataCmd.ShowString: s = CProgram.sDateTimeToString( ArVar ); break;
                        case DSqlDataCmd.WhereLower: s = AName + "<" + v; break;
                        case DSqlDataCmd.WhereLowerEqual: s = AName + "<=" + v; break;
                        case DSqlDataCmd.WhereEqual: s = AName + "=" + v; break;
                        case DSqlDataCmd.WhereHigherEqual: s = AName + ">=" + v; break;
                        case DSqlDataCmd.WhereHigher: s = AName + ">=" + v; break;
                        case DSqlDataCmd.WhereNotEqual: s = AName + "!=" + v; break;
                        case DSqlDataCmd.WhereValid: s = AName + "!=0"; break;
                        case DSqlDataCmd.WhereNotValid: s = AName + "=0"; break;
                    }
                    if (s != null && s.Length > 0)
                    {
                        ArStringValue = s;
                        bOk = true;
                    }
                }
            }
            ArbValid = bValid;
            return bOk ? dataType : DSqlDataType.SqlError;
         }
        public DSqlDataType mSqlGetSetDateTime(ref DateTime ArVar, string AName, DSqlDataCmd ACmd, ref string ArSqlName, ref string ArStringValue, out bool ArbValid) // forces to Local time
        {
            DSqlDataType dataType = DSqlDataType.SqlDateTime;
            bool bOk = false;

            bool bValid = false;

            if (ArVar != null)
            {
                bValid = ArVar != DateTime.MinValue;
            }
            if (ArSqlName != null)         // Set ArSqlName if not null
            {
                ArSqlName = AName;
            }
            if (ACmd == DSqlDataCmd.GetType)    // returning type is possible without a connection
            {
                bOk = true;
            }
            else if (mSqlConnection != null)
            {
                if (ACmd == DSqlDataCmd.ParseString)
                {
                    bOk = mSqlConnection.mbParseProgramDtLocal(ArStringValue, out ArVar );  // got string in program local format
                }
                else if (ArStringValue != null)
                {
                    if( ArVar != null && ArVar.Kind != DateTimeKind.Local )
                    {
                        CProgram.sLogError(AName + " not DateTime.Local");
                        bOk = false;
                        bValid = false;
                    }
                    else
                    {
                        string v = mSqlDateTimeSqlString(ArVar); // variable value
                        string s = null;

                        switch (ACmd)
                        {
                            case DSqlDataCmd.SqlString: s = v; break;
                            case DSqlDataCmd.ShowString: s = CProgram.sDateTimeToString(ArVar); break;
                            case DSqlDataCmd.WhereLower: s = AName + "<" + v; break;
                            case DSqlDataCmd.WhereLowerEqual: s = AName + "<=" + v; break;
                            case DSqlDataCmd.WhereEqual: s = mSqlDateTimeCompareSqlString(AName, ArVar, DSqlCompareDateTime.Second, 1); break;
                            case DSqlDataCmd.WhereHigherEqual: s = AName + ">=" + v; break;
                            case DSqlDataCmd.WhereHigher: s = AName + ">=" + v; break;
                            case DSqlDataCmd.WhereNotEqual: s = "( NOT " + mSqlDateTimeCompareSqlString(AName, ArVar, DSqlCompareDateTime.Second, 1) + ")"; break;
                            case DSqlDataCmd.WhereValid: s = AName + "!='0000-"; break;
                            case DSqlDataCmd.WhereNotValid: s = AName + "=0"; break;
                        }
                        if (s != null && s.Length > 0)
                        {
                            ArStringValue = s;
                            bOk = true;
                        }
                        bOk = ArStringValue != null && ArStringValue.Length > 0;
                    }
                }
            }
            ArbValid = bValid;
            return bOk ? dataType : DSqlDataType.SqlError;
        }
        public DSqlDataType mSqlGetSetUTC(ref DateTime ArVar, string AName, DSqlDataCmd ACmd, ref string ArSqlName, ref string ArStringValue, out bool ArbValid) // forces to UTC time
        {
            DSqlDataType dataType = DSqlDataType.SqlDateTime;
            bool bOk = false;
            bool bValid = false;

            if (ArVar != null)
            {
                bValid = ArVar != DateTime.MinValue;
            }
            if (ArSqlName != null)         // Set ArSqlName if not null
            {
                ArSqlName = AName;
            }
            if (ACmd == DSqlDataCmd.GetType)    // returning type is possible without a connection
            {
                bOk = true;
            }
            else if (mSqlConnection != null)
            {
                if (ACmd == DSqlDataCmd.ParseString)
                {
                    bOk = mSqlConnection.mbParseProgramDtUtc(ArStringValue, out ArVar); // got string in program local format
                }
                else if (ArStringValue != null )
                {
                    if (ArVar != null && ArVar.Kind != DateTimeKind.Utc)
                    {
                        CProgram.sLogError(AName + " not DateTime.Utc");
                        bOk = false;
                        bValid = false;
                    }
                    else
                    {
                        string v = mSqlDateTimeSqlString(ArVar); // variable value
                        string s = null;

                        switch (ACmd)
                        {
                            case DSqlDataCmd.SqlString: s = v; break;
                            case DSqlDataCmd.ShowString: s = CProgram.sDateTimeToString(ArVar); break;
                            case DSqlDataCmd.WhereLower: s = AName + "<" + v; break;
                            case DSqlDataCmd.WhereLowerEqual: s = AName + "<=" + v; break;
                            case DSqlDataCmd.WhereEqual: s = mSqlDateTimeCompareSqlString(AName, ArVar, DSqlCompareDateTime.Second, 1); break;
                            case DSqlDataCmd.WhereHigherEqual: s = AName + ">=" + v; break;
                            case DSqlDataCmd.WhereHigher: s = AName + ">=" + v; break;
                            case DSqlDataCmd.WhereNotEqual: s = "( NOT " + mSqlDateTimeCompareSqlString(AName, ArVar, DSqlCompareDateTime.Second, 1) + ")"; break;
                            case DSqlDataCmd.WhereValid: s = AName + "!=" + mSqlConnection.mSqlZeroDateTimeSqlString(); break;
                            case DSqlDataCmd.WhereNotValid: s = AName + "=" + mSqlConnection.mSqlZeroDateTimeSqlString(); break;
                        }
                        if (s != null && s.Length > 0)
                        {
                            ArStringValue = s;
                            bOk = true;
                        }
                        bOk = ArStringValue != null && ArStringValue.Length > 0;
                    }

                }
            }
            ArbValid = bValid;
            return bOk ? dataType : DSqlDataType.SqlError;
        }
        public DSqlDataType mSqlGetSetEncryptedInt(ref CEncryptedInt ArVar, string AName, DSqlDataCmd ACmd, ref string ArSqlName, ref string ArStringValue, out bool ArbValid)
        {
            DSqlDataType dataType = DSqlDataType.SqlEncryptInt;
            bool bOk = false;
            bool bValid = false;

            if (ArVar != null)
            {
                bValid = ArVar.mbNotZero();
            }

            if (ArSqlName != null)         // Set ArSqlName if not null
            {
                ArSqlName = AName;
            }
            if (ACmd == DSqlDataCmd.GetType)    // returning type is possible without a connection
            {
                bOk = true;
            }
            else if (mSqlConnection != null)
            {
                if( ArVar == null )
                {
                    CProgram.sLogError(AName + " = null");
                    bOk = false;
                }
                else if (ACmd == DSqlDataCmd.ParseString)
                {
                    Int32 var = 0;
                    bOk = Int32.TryParse(ArStringValue, out var);

                    ArVar.mbSetEncrypted(var);                  
                }
                else if (ArStringValue != null)
                {
                    string v = ArVar == null ? "" : ArVar.mGetEncryptedInt().ToString(); // variable value
                    string s = null;

                    switch (ACmd)
                    {
                        case DSqlDataCmd.SqlString: s = v; break;
                        case DSqlDataCmd.ShowString: s = v; break;
                        //                        case DSqlDataCmd.WhereLower: s = AName + "<" + v; break;            // nonsense
                        //                        case DSqlDataCmd.WhereLowerEqual: s = AName + "<=" + v; break;
                        case DSqlDataCmd.WhereEqual: s = AName + "=" + v; break;
//                        case DSqlDataCmd.WhereHigherEqual: s = AName + ">=" + v; break;
//                        case DSqlDataCmd.WhereHigher: s = AName + ">=" + v; break;
                        case DSqlDataCmd.WhereNotEqual: s = AName + "!=" + v; break;
                        case DSqlDataCmd.WhereValid: s = AName + "!=0"; break;
                        case DSqlDataCmd.WhereNotValid: s = AName + "=0"; break;
                    }
                    if (s != null && s.Length > 0)
                    {
                        ArStringValue = s;
                        bOk = true;
                    }
                }
            }
            ArbValid = bValid;
            return bOk ? dataType : DSqlDataType.SqlError;
        }
        public DSqlDataType mSqlGetSetEncryptedString(ref CEncryptedString ArVar, string AName, DSqlDataCmd ACmd, ref string ArSqlName, ref string ArStringValue, UInt16 ASizeLimit, out bool ArbValid)
        {
            DSqlDataType dataType = DSqlDataType.SqlEncryptString;
            bool bOk = false;
            bool bValid = false;

            if (ArVar != null)
            {
                bValid = ArVar.mbNotEmpty();
            }

            if (ArSqlName != null)         // Set ArSqlName if not null
            {
                ArSqlName = AName;
            }
            if (ACmd == DSqlDataCmd.GetType)    // returning type is possible without a connection
            {
                bOk = true;
            }
            else if (mSqlConnection != null)
            {
                if (ArVar == null)
                {
                    CProgram.sLogError(AName + " = null");
                    bOk = false;
                }
                else if (ACmd == DSqlDataCmd.ParseString)
                {
                    string var = "";
                    bOk = mSqlConnection.mbSqlParseToString(ArStringValue, ref var);
                    ArVar.mbSetEncrypted(var);
                }
                else if (ArStringValue != null)
                {
                    string v = ArVar.mGetEncrypted();
                    string s = null;

                    if (v.Length > ASizeLimit)
                    {
                        v = v.Substring(0, ASizeLimit);
                    }
                    v = mSqlStringSqlString(v);

                    switch (ACmd)
                    {
                        case DSqlDataCmd.SqlString: s = v; break;
                        case DSqlDataCmd.ShowString: s = ArVar.mDecrypt(); break;
                        case DSqlDataCmd.WhereLower: s = mSqlStringStartsWithSqlString(AName, v); break;    // encryption keeps the string searchable when encryption level stays the same
                        case DSqlDataCmd.WhereLowerEqual: s = mSqlStringStartsWithSqlString(AName, v); break;
                        case DSqlDataCmd.WhereEqual: s = AName + "=" + v; break;
                        case DSqlDataCmd.WhereHigherEqual: s = mSqlStringContainsSqlString(AName, v); break;
                        case DSqlDataCmd.WhereHigher: s = mSqlStringContainsSqlString(AName, v); break;
                        case DSqlDataCmd.WhereNotEqual: s = AName + "!=" + v; break;
                        case DSqlDataCmd.WhereValid: s = "LENGTH(" + AName + ")>0"; break;
                        case DSqlDataCmd.WhereNotValid: s = "LENGTH(" + AName + ")=0"; break;
                    }
                    if (s != null && s.Length > 0)
                    {
                        ArStringValue = s;
                        bOk = true;
                    }
                }
            }
            ArbValid = bValid;
            return bOk ? dataType : DSqlDataType.SqlError;
        }
        public DSqlDataType mSqlGetSetStringSet(ref CStringSet ArVar, string AName, DSqlDataCmd ACmd, ref string ArSqlName, ref string ArStringValue, UInt16 ASizeLimit, out bool ArbValid)
        {
            DSqlDataType dataType = DSqlDataType.SqlString;
            bool bOk = false;
            bool bValid = false;

            if (ArVar != null)
            {
                bValid = ArVar.mbIsNotEmpty();
            }

            if (ArSqlName != null)         // Set ArSqlName if not null
            {
                ArSqlName = AName;
            }
            if (ACmd == DSqlDataCmd.GetType)    // returning type is possible without a connection
            {
                bOk = true;
            }
            else if (mSqlConnection != null)
            {
               if (ACmd == DSqlDataCmd.ParseString)
                {
                    string var = "";
                    bOk = mSqlConnection.mbSqlParseToString(ArStringValue, ref var);

                    if (ArVar == null)
                    {
                        ArVar = new CStringSet();
                    }
                    ArVar.mSetSet(var);
                }
                else if (ArStringValue != null)
                {
                    string v = ArVar == null ? "" : ArVar.mGetSet();
                    string s = null;

                    if (v != null && v.Length > ASizeLimit)
                    {
                        v = v.Substring(0, ASizeLimit);
                    }
                    v = mSqlStringSqlString(v);

                    switch (ACmd)
                    {
                        case DSqlDataCmd.SqlString: s = v; break;
                        case DSqlDataCmd.ShowString: s = ArVar.mGetSet(); break;
                        case DSqlDataCmd.WhereLower: s = mSqlStringStartsWithSqlString(AName, v); break;    // encryption keeps the string searchable when encryption level stays the same
                        case DSqlDataCmd.WhereLowerEqual: s = mSqlStringStartsWithSqlString(AName, v); break;
                        case DSqlDataCmd.WhereEqual: s = AName + "=" + v; break;
                        case DSqlDataCmd.WhereHigherEqual: s = mSqlStringContainsSqlString(AName, v); break;
                        case DSqlDataCmd.WhereHigher: s = mSqlStringContainsSqlString(AName, v); break;
                        case DSqlDataCmd.WhereNotEqual: s = AName + "!=" + v; break;
                        case DSqlDataCmd.WhereValid: s = "LENGTH(" + AName + ")>0"; break;
                        case DSqlDataCmd.WhereNotValid: s = "LENGTH(" + AName + ")=0"; break;
                    }
                    if (s != null && s.Length > 0)
                    {
                        ArStringValue = s;
                        bOk = true;
                    }
                }
            }
            ArbValid = bValid;
            return bOk ? dataType : DSqlDataType.SqlError;
        }

        public CSqlCmd mCreateSqlCmd([System.Runtime.CompilerServices.CallerLineNumber]int _AutoLN = 0,
             [System.Runtime.CompilerServices.CallerMemberName]string _AutoFN = "",
             [System.Runtime.CompilerServices.CallerFilePath] string _AutoCN = "")
        {
            if (mSqlConnection == null)
            {
                CProgram.sLogError("SqlConnection not set", _AutoLN, _AutoFN, _AutoCN);
                return null;
            }
            return mSqlConnection.mCreateSqlCmd(this, _AutoLN, _AutoFN, _AutoCN);
        }

        public CSqlCmd mCreateSqlCmd(CSqlDBaseConnection ASqlConnection,
            [System.Runtime.CompilerServices.CallerLineNumber]int _AutoLN = 0,
              [System.Runtime.CompilerServices.CallerMemberName]string _AutoFN = "",
              [System.Runtime.CompilerServices.CallerFilePath] string _AutoCN = "")
        {
            if( mSqlConnection != null)
            {
                mSqlConnection.mCloseSqlCmd();
            }
            mSqlConnection = ASqlConnection;
            if (mSqlConnection == null)
            {
                CProgram.sLogError("SqlConnection not set", _AutoLN, _AutoFN, _AutoCN);
                return null;
            }
            return mSqlConnection.mCreateSqlCmd(this, _AutoLN, _AutoFN, _AutoCN);
        }

        public bool mbDoSqlInsert()    // save this into dBase
        {
            return mbDoSqlInsert(mMaskAll);
        }

        public bool mbDoSqlInsert(UInt64 ASaveVarMask)    // save this into dBase
        {
            bool bOk = false;
            CSqlCmd cmd = null;

            try
            {
                if (mSqlConnection != null && mSqlConnection.mbSqlOpenConnection())
                {
                    cmd = mCreateSqlCmd();

                    if (cmd == null)
                    {
                        CProgram.sLogLine("Failed to init cmd " + mGetDbTableName());
                    }
                    else if (false == cmd.mbPrepareInsertCmd(ASaveVarMask, true, true))
                    {
                        CProgram.sLogLine("Failed to prepare SQL row in table " + mGetDbTableName());
                    }
                    else if (cmd.mbExecuteInsertCmd())  // insert this into dBase Table and receives new Index
                    {
                        UInt32 index = mIndex_KEY;   // return new key in this
                        bOk = index != 0;
//                        CProgram.sLogLine("Successfull insert in " + mGetDbTableName() + "[" + index.ToString() + "]");
                    }
                    else
                    {
                        CProgram.sLogLine("Failed to execute insert SQL row in table " + mGetDbTableName());
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed insert Row in table " + mGetDbTableName(), ex);
                bOk = false;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.mCloseCmd();
                }
                if (mSqlConnection != null )
                {
                    mSqlConnection.mSqlCloseConnection();
                 }
            }
            return bOk;
        }
        public bool mbDoSqlUpdate(UInt64 AUpdateVarMask, bool AbActive, UInt64 AForceUpdatePkMask = 0)    // update this into dBase (uses mIndexKey)
        {
            bool bOk = false;
            CSqlCmd cmd = null;

            try
            {
                if (mSqlConnection != null && mSqlConnection.mbSqlOpenConnection())
                {
                    cmd = mCreateSqlCmd();

                    if (cmd == null)
                    {
                        CProgram.sLogLine("Failed to init cmd in SqlUpdate " + mGetDbTableName() + mIndex_KEY.ToString());
                    }
                    else if (false == cmd.mbPrepareUpdateCmd(true, AUpdateVarMask, true, AbActive, AForceUpdatePkMask ))
                    {
                        CProgram.sLogLine("Failed to prepare update SQL row in table " + mGetDbTableName() + mIndex_KEY.ToString());
                    }
                    else if (cmd.mbExecuteUpdateCmd())
                    {
                        bOk = true;
//                        CProgram.sLogLine("Successfull updated in " + mGetDbTableName() + "[" + mIndex_KEY.ToString() + "]");
                    }
                    else
                    {
                        CProgram.sLogLine("Failed to insert SQL row in table " + mGetDbTableName() + mIndex_KEY.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed update Row in table " + mGetDbTableName() + "IX=" + mIndex_KEY.ToString(), ex);
                bOk = false;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.mCloseCmd();
                }
                if (mSqlConnection != null)
                {
                    mSqlConnection.mSqlCloseConnection();
                 }
            }
            return bOk;
        }
        public bool mbDoSqlSelectIndex(UInt32 ACurrentIndex, UInt64 ALoadVarMask )  // load index from dBase into this
        {
            bool bOk = false;

            CSqlCmd cmd = null;

            try
            {
                if( ACurrentIndex == 0 )
                {
                    // undefined index
                    mClear();
                }
                else if (mSqlConnection != null && mSqlConnection.mbSqlOpenConnection())
                {

                    UInt64 mask = ALoadVarMask | mMaskIndexKey;
                    mClear();

                    cmd = mCreateSqlCmd();
                    mIndex_KEY = ACurrentIndex;

                    if (cmd == null)
                    {
                        CProgram.sLogLine("Failed to init cmd SqlSelectIndex SQL row[" + ACurrentIndex.ToString() + "] in table " + mGetDbTableName());
                    }
                    else if (false == cmd.mbPrepareSelectCmd(true, mask))
                    {
                        CProgram.sLogLine("Failed to prepare select index SQL row["+ ACurrentIndex.ToString() + "] in table " + mGetDbTableName());
                    }
                    else 
                    {
                        string whereString;

                        mbGetVarWhereList(out whereString,  mMaskIndexKey);

                        cmd.mWhereSetString(whereString);

                        if (cmd.mbExecuteSelectCmd())
                        {
                            bool bError = false;
                            mClear();
  
                            if (cmd.mbReadOneRow(out bError))
                            {
                                bOk = false == bError && mIndex_KEY == ACurrentIndex;
                            }
                            if (bOk)
                            {
//                                CProgram.sLogLine("Successful select index SQL row[" + mIndex_KEY.ToString() + "] in table " + mGetDbTableName());
                            }
                            else
                            {
                                CProgram.sLogLine("Failed to read row select index SQL row[" + ACurrentIndex.ToString() 
                                    + "] in table " + mGetDbTableName() + " bE=" + bError.ToString() + " IX= " + mIndex_KEY.ToString());
                            }
                        }
                        else
                        {
                            CProgram.sLogLine("Failed to execute select index SQL row[" + ACurrentIndex.ToString() + "] in table " + mGetDbTableName());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed select Row[" + ACurrentIndex.ToString() + "] in table " + mGetDbTableName(), ex);
                bOk = false;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.mCloseCmd();
                }
                if (mSqlConnection != null)
                {
                    mSqlConnection.mSqlCloseConnection();
                }
            }
            if( false == bOk && mIndex_KEY != 0)
            {
                CProgram.sLogLine("Failed to read row select index SQL row[" + ACurrentIndex.ToString()
    + "] in table " + mGetDbTableName() + " reset IX= " + mIndex_KEY.ToString());
                mIndex_KEY = 0;
            }
            return bOk;
        }

        public bool mbDoSqlTestSelectIndex(out bool AbError, UInt32 ACurrentIndex, UInt64 ALoadVarMask)  // load index from dBase into this
        {
            bool bOk = false;

            CSqlCmd cmd = null;
            bool bError = true;

            try
            {
                if (ACurrentIndex == 0)
                {
                    // undefined index
                    mClear();
                }
                else if (mSqlConnection != null && mSqlConnection.mbSqlOpenConnection())
                {

                    UInt64 mask = ALoadVarMask | mMaskIndexKey;
                    mClear();

                    cmd = mCreateSqlCmd();
                    mIndex_KEY = ACurrentIndex;

                    if (cmd == null)
                    {
                        CProgram.sLogLine("Failed to init cmd");
                    }
                    else if (false == cmd.mbPrepareSelectCmd(true, mask))
                    {
                        CProgram.sLogLine("Failed to prepare select index SQL row[" + ACurrentIndex.ToString() + "] in table " + mGetDbTableName());
                    }
                    else
                    {
                        string whereString;

                        mbGetVarWhereList(out whereString, mMaskIndexKey);

                        cmd.mWhereSetString(whereString);

                        if (cmd.mbExecuteSelectCmd())
                        {
                            mClear();

                            if (cmd.mbReadOneRow(out bError))
                            {
                                bOk = false == bError && mIndex_KEY == ACurrentIndex;
                            }

/*                            if (bOk)
                            {
                                CProgram.sLogLine("Successful select index SQL row[" + mIndex_KEY.ToString() + "] in table " + mGetDbTableName());
                            }
                            else
                            {
                                CProgram.sLogLine("Failed to prepare select index SQL row[" + ACurrentIndex.ToString() + "] in table " + mGetDbTableName());

                            }
 */                       }
                        else
                        {
                            CProgram.sLogLine("Failed to execute select index SQL row[" + ACurrentIndex.ToString() + "] in table " + mGetDbTableName());
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                if( cmd != null)
                {
                    cmd.mLogCmdString("SqlTestSelectIndex");
                }
                CProgram.sLogException("Failed select Row[" + ACurrentIndex.ToString() + "]  in table " + mGetDbTableName(), ex);
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.mCloseCmd();
                }
                if (mSqlConnection != null)
                {
                    mSqlConnection.mSqlCloseConnection();
                }
            }
            if (false == bOk && mIndex_KEY != 0)
            {
                CProgram.sLogLine("Failed to read row select index SQL row[" + ACurrentIndex.ToString()
    + "] in table " + mGetDbTableName() + " reset IX= " + mIndex_KEY.ToString());
                mIndex_KEY = 0;
            }

            AbError = bError;
            return bOk;

        }


        public bool mbDoSqlSelectList(out bool AbError, out List<CSqlDataTableRow> AList, UInt64 ALoadVarMask, 
            UInt64 AWhereEqualVarMask, bool AbActiveOnly, string AWhereString,
            DSqlSort ASortOrder = DSqlSort.None, UInt16 ASortVar=0, UInt32 ALimitNrRows = 0)
        {
            bool bOk = false;
            bool bError = false;
            CSqlCmd cmd = null;

            AList = null;

            try
            {
                if (mSqlConnection != null && mSqlConnection.mbSqlOpenConnection())
                {
                    UInt64 maskLoad = mMaskIndexKey | ALoadVarMask;
                    UInt64 maskWhere = AWhereEqualVarMask;

                    cmd = mCreateSqlCmd();

                    if(AbActiveOnly)
                    {
                        mbActive = true;
                        maskWhere |= mMaskActive;
                    }
                    if (cmd == null)
                    {
                        CProgram.sLogLine("Failed to init cmd for table " + mGetDbTableName());
                    }
                    else if (false == cmd.mbPrepareSelectCmd(true, maskLoad))
                    {
                        CProgram.sLogLine("Failed to prepare selectlist SQL rows in table " + mGetDbTableName());
                    }
                    else
                    {
                        string whereString;

                        if( ASortOrder != DSqlSort.None )
                        {
                            cmd.mbSetOrderBy(ASortVar, ASortOrder);
                        }

                        mbGetVarWhereList(out whereString, maskWhere);

                        cmd.mWhereSetString(whereString);
                        if( AWhereString != null && AWhereString.Length > 0)
                        {
                            cmd.mWhereAddString(AWhereString);
                        }
                        if(ALimitNrRows > 0)
                        {
                            cmd.mSetLimit(ALimitNrRows, 0);
                        }
 /*                       if( cmd.mGetWhereString() == null || cmd.mGetWhereString().Length == 0)
                        {
                            cmd.mWhereAddString(AWhereString);
                        }
*/                        AList = cmd.mExecuteSelectListCmd( out bError);

                        if ( AList != null )
                        {
                            bOk = true;

//                            CProgram.sLogLine("Successful select list SQL rows n=" + AList.Count.ToString() + " in table " + mGetDbTableName() + (bError ? "!list error!": ""));
                         }
                        else
                        { 
                            CProgram.sLogLine("Failed to select list SQL rows in table " + mGetDbTableName());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed to select list SQL rows in table " + mGetDbTableName(), ex);
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.mCloseCmd();
                }
                if (mSqlConnection != null)
                {
                    mSqlConnection.mSqlCloseConnection();
                }
            }

            AbError = bError;
            return bOk;
        }
        public bool mbDoSqlSelectList(out bool AbError, out List<CSqlDataTableRow> AList, UInt64 ALoadVarMask,
            string AWhereString, DSqlSort ASortOrder = DSqlSort.None, UInt16 ASortVar = 0, UInt32 ALimitNrRows = 0)
        {
            bool bOk = false;
            bool bError = false;
            CSqlCmd cmd = null;

            AList = null;

            try
            {
                if (mSqlConnection != null && mSqlConnection.mbSqlOpenConnection() && AWhereString != null && AWhereString.Length > 0)
                {
                    UInt64 maskLoad = mMaskIndexKey | ALoadVarMask;

                    cmd = mCreateSqlCmd();

                    if (cmd == null)
                    {
                        CProgram.sLogLine("Failed to init cmd for table " + mGetDbTableName());
                    }
                    else if (false == cmd.mbPrepareSelectCmd(true, maskLoad))
                    {
                        CProgram.sLogLine("Failed to prepare selectlist SQL rows in table " + mGetDbTableName());
                    }
                    else
                    {
                        if (ASortOrder != DSqlSort.None)
                        {
                            cmd.mbSetOrderBy(ASortVar, ASortOrder);
                        }

                        cmd.mWhereSetString(AWhereString);
                        if (ALimitNrRows > 0)
                        {
                            cmd.mSetLimit(ALimitNrRows, 0);
                        }

                        AList = cmd.mExecuteSelectListCmd(out bError);

                        if (AList != null)
                        {
                            bOk = true;
                            //                            CProgram.sLogLine("Successful select list SQL rows n=" + AList.Count.ToString() + " in table " + mGetDbTableName() + (bError ? "!list error!": ""));
                        }
                        else
                        {
                            CProgram.sLogLine("Failed to select list SQL rows in table " + mGetDbTableName());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed to select list SQL rows in table " + mGetDbTableName(), ex);
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.mCloseCmd();
                }
                if (mSqlConnection != null)
                {
                    mSqlConnection.mSqlCloseConnection();
                }
            }

            AbError = bError;
            return bOk;
        }

        public bool mbListFillComboBox( ComboBox AComboBox, List<CSqlDataTableRow> AList, UInt16 AVarNr, UInt32 ACursorIndex, string AFirstLine )
        {
            bool bOk = false;

            if (AComboBox != null && AComboBox.Items != null)
            {
                string cursor = "";

                AComboBox.Items.Clear();

                if(AFirstLine != null && AFirstLine.Length > 0 )
                {
                    AComboBox.Items.Add(AFirstLine);
                }
                if (AList != null)
                {
                    foreach( CSqlDataTableRow row in AList )
                    {
                        string label = row.mVarGetShowString(AVarNr);

                        if( ACursorIndex == row.mIndex_KEY)
                        {
                            cursor = label;
                            bOk = true;
                        }
                        if( label == null || label.Length == 0 )
                        {
                            CProgram.sLogError("Empty label in dBase" + mGetDbTableName() + " for variable nr " + AVarNr);
                        }
                        else
                        {
                            AComboBox.Items.Add(label);
                        }
                    }
                }
                AComboBox.Text = cursor != null && cursor.Length > 0 ? cursor : AFirstLine;
            }
            return bOk;
        }
        public bool mbListFillStringList(ref List<string>ArStringList, List<CSqlDataTableRow> AList, UInt16 AVarNr, UInt32 ACursorIndex, out UInt16 ArSeqInList)
        {
            bool bOk = false;
            UInt16 seqInList = 0;

            if (ArStringList != null )
            {
                 if (AList != null)
                {
                    UInt16 i = 0;
                    foreach (CSqlDataTableRow row in AList)
                    {
                        string label = row.mVarGetShowString(AVarNr);

                        if (ACursorIndex == row.mIndex_KEY)
                        {
                            seqInList = i;
                            bOk = true;
                        }
                        if (label == null || label.Length == 0)
                        {
                            CProgram.sLogError("Empty label in dBase" + mGetDbTableName() + " for variable nr " + AVarNr);
                        }
                        else
                        {
                            ArStringList.Add(label);
                        }
                        ++i;
                    }
                    bOk = true;
                }
            }
            ArSeqInList = seqInList;
            return bOk;
        }


        public bool mbListParseComboBox(ComboBox AComboBox, List<CSqlDataTableRow> AList, UInt16 AVarNr, ref UInt32 ArIndex)
        {
            bool bOk = false;

            if( AComboBox != null && AList != null )
            {
                UInt32 index = mGetIndexKeyFromList(AList, AVarNr, AComboBox.Text);

                if( index > 0 )
                {
                    ArIndex = index;
                    bOk = true;
                }
            }

            return bOk;
        }

        public bool mbDoSqlCount(out bool AbError, out UInt32 ArTotalCount, 
                    UInt64 AWhereEqualVarMask, bool AbActiveOnly, string AWhereString )
        {
            bool bOk = false;
            bool bError = false;
            CSqlCmd cmd = null;
            UInt32 totalCount = 0;

            try
            {
                if (mSqlConnection != null && mSqlConnection.mbSqlOpenConnection())
                {
                    UInt64 maskWhere = AWhereEqualVarMask;

                    cmd = mCreateSqlCmd();

                    if (AbActiveOnly)
                    {
                        mbActive = true;
                        maskWhere |= mMaskActive;
                    }
                    if (cmd == null)
                    {
                        CProgram.sLogLine("Failed to init cmd for table " + mGetDbTableName());
                    }
                    else if (false == cmd.mbPrepareCountCmd(0))
                    {
                        CProgram.sLogLine("Failed to prepare count cmd in table " + mGetDbTableName());
                    }
                    else
                    {
                        string whereString;

                        mbGetVarWhereList(out whereString, maskWhere);

                        cmd.mWhereSetString(whereString);
                        if (AWhereString != null && AWhereString.Length > 0)
                        {
                            cmd.mWhereAddString(AWhereString);
                        }
                        bError = cmd.mbExecuteCountCmd(out totalCount);

                        if( false == bError )
                        {
                            bOk = true;
                            //                            CProgram.sLogLine("Successful count list SQL rows n=" + AList.Count.ToString() + " in table " + mGetDbTableName() + (bError ? "!list error!": ""));
                        }
                        else
                        {
                            CProgram.sLogLine("Failed to count SQL rows in table " + mGetDbTableName());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed to count SQL rows in table " + mGetDbTableName(), ex);
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.mCloseCmd();
                }
                if (mSqlConnection != null)
                {
                    mSqlConnection.mSqlCloseConnection();
                }
            }

            AbError = bError;
            ArTotalCount = totalCount;
            return bOk;
        }

        public bool mbDoSqlCountList(out bool AbError, out UInt32 ArTotalCount, out List<CSqlCountItem> AList, UInt16 ACountByVar,
            UInt64 AWhereEqualVarMask, bool AbActiveOnly, string AWhereString,
             DSqlSort ASortOrder = DSqlSort.None)
        {
            bool bOk = false;
            bool bError = false;
            CSqlCmd cmd = null;
            UInt32 totalCount = 0;

            AList = null;

            try
            {
                if (mSqlConnection != null && mSqlConnection.mbSqlOpenConnection())
                {
                    UInt64 maskWhere = AWhereEqualVarMask;

                    cmd = mCreateSqlCmd();

                    if (AbActiveOnly)
                    {
                        mbActive = true;
                        maskWhere |= mMaskActive;
                    }
                    if (cmd == null)
                    {
                        CProgram.sLogLine("Failed to init cmd for table " + mGetDbTableName());
                    }
                    else if (false == cmd.mbPrepareCountCmd(ACountByVar))
                    {
                        CProgram.sLogLine("Failed to prepare count SQL rows in table " + mGetDbTableName());
                    }
                    else
                    {
                        string whereString;

                        if (ASortOrder != DSqlSort.None)
                        {
                            cmd.mbSetOrderBy(ACountByVar, ASortOrder);
                        }

                        mbGetVarWhereList(out whereString, maskWhere);

                        cmd.mWhereSetString(whereString);
                        if (AWhereString != null && AWhereString.Length > 0)
                        {
                            cmd.mWhereAddString(AWhereString);
                        }
                        AList = cmd.mExecuteCountListCmd(out bError, out totalCount);

                        if (AList != null)
                        {
                            bOk = true;
                            //                            CProgram.sLogLine("Successful count list SQL rows n=" + AList.Count.ToString() + " in table " + mGetDbTableName() + (bError ? "!list error!": ""));
                        }
                        else
                        {
                            CProgram.sLogLine("Failed to count list SQL rows in table " + mGetDbTableName());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed to count list SQL rows in table " + mGetDbTableName(), ex);
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.mCloseCmd();
                }
                if (mSqlConnection != null)
                {
                    mSqlConnection.mSqlCloseConnection();
                }
            }

            AbError = bError;
            ArTotalCount = totalCount;
            return bOk;
        }

        public UInt32 mGetIndexKeyFromList(List<CSqlDataTableRow> AList, UInt16 AVarNr, string ALabel)
        {
            UInt32 index = 0;

            if (AList != null)
            {
                foreach (CSqlDataTableRow row in AList)
                {
                    string label = row.mVarGetShowString(AVarNr);

                    if (ALabel == label)
                    {
                        index = row.mIndex_KEY;
                        break;
                    }
                }
            }
            return index;
        }
        public CSqlDataTableRow mGetNodeFromList(List<CSqlDataTableRow> AList, UInt16 AVarNr, string ALabel)
        {
            CSqlDataTableRow index = null;

            if (AList != null)
            {
                foreach (CSqlDataTableRow row in AList)
                {
                    string label = row.mVarGetShowString(AVarNr);

                    if (ALabel == label)
                    {
                        index = row;
                        break;
                    }
                }
            }
            return index;
        }
        public CSqlDataTableRow mGetNodeFromList(List<CSqlDataTableRow> AList, UInt32 AIndex)
        {
            CSqlDataTableRow index = null;

            if (AList != null)
            {
                foreach (CSqlDataTableRow row in AList)
                {
                    if (AIndex == row.mIndex_KEY)
                    {
                        index = row;
                        break;
                    }
                }
            }
            return index;
        }
        public bool mbLoadIndexFromList(List<CSqlDataTableRow> AList, UInt32 AIndex)
        {
            bool bFound = false;

            if (AList != null)
            {
                foreach (CSqlDataTableRow row in AList)
                {
                    if (AIndex == row.mIndex_KEY)
                    {
                        bFound = mbCopyFrom( row );

                        break;
                    }
                }
            }
            return bFound;
        }


        // sql where commands

        public string mSqlWhereAND(string AValue1, string AValue2)
        {
            string s = "";

            if (AValue1 == null || AValue1.Length == 0)
            {
                s = AValue2 == null ? "" : AValue2;
            }
            else if (AValue2 == null || AValue2.Length == 0)
            {
                s = AValue1;
            }
            else
            {
                s = AValue1 + " AND " + AValue2;
            }
            return s;
        }
        public string mSqlWhereOR(string AValue1, string AValue2)
        {
            string s = "";

            if (AValue1 == null || AValue1.Length == 0)
            {
                s = AValue2 == null ? "" : AValue2;
            }
            else if (AValue2 == null || AValue2.Length == 0)
            {
                s = AValue1;
            }
            else
            {
                s = AValue1 + " OR " + AValue2;
            }
            return s;
        }

        public string mSqlWhereCompound(string AValue)
        {
            string s = "";

            if (AValue != null || AValue.Length > 0)
            {
                s = "(" + AValue + ")";
            }
            return s;
        }
/*        public string mSqlWhereBool(bool AbValue)
        {
        }
        public string mSqlWhereInt32(Int32 AValue)
        {
        }
        public string mSqlWhereUInt32(UInt32 AValue)
        {
        }
        public string mSqlWhereString(string AValue)
        {
        }
*/        public string mSqlWhereCmpVar(DSqlDataCmd ACmpCmd, UInt16 AVarNr)
        {
            return mVarGetSqlText(AVarNr, ACmpCmd);
        }

        public void mSqlWhereAND(ref string ArCurrentWhere, string AValue2)
        {
            if (ArCurrentWhere == null || ArCurrentWhere.Length == 0)
            {
                ArCurrentWhere = AValue2 == null ? "" : AValue2;
            }
            else if (AValue2 != null && AValue2.Length > 0)
            { 
                 ArCurrentWhere += " AND " + AValue2;
            }
        }
        public void mSqlWhereOR(ref string ArCurrentWhere, string AValue2)
        {
            if (ArCurrentWhere == null || ArCurrentWhere.Length == 0)
            {
                ArCurrentWhere = AValue2 == null || AValue2.Length == 0 ? "" : "(" + AValue2 + ")";
            }
            else if (AValue2 == null || AValue2.Length == 0)
            {
                ArCurrentWhere = "(" + ArCurrentWhere + ")";
            }
            else
            {
                ArCurrentWhere = "(" + ArCurrentWhere + ") OR (" + AValue2 + ")";
            }
        }
        public void mSqlWhereAndCmpVar(ref string ArCurrentWhere, DSqlDataCmd ACmpCmd, UInt16 AVarNr, bool AbTestValid)
        {
            string s = mVarGetSqlText(AVarNr, ACmpCmd, AbTestValid);

            mSqlWhereAND(ref ArCurrentWhere, s);
        }
        public void mSqlWhereAndCmpDT(ref string ArCurrentWhere, UInt16 AVarNr, DateTime AMinValue, DateTime AMaxValue)
        {
            if( AMinValue > DateTime.MinValue && AMaxValue < DateTime.MaxValue )
            {
                string name = mVarGetName(AVarNr);
                string minStr = mSqlDateSqlString(AMinValue);
                string maxStr = mSqlDateSqlString(AMaxValue);
                mSqlWhereAND(ref ArCurrentWhere, name + ">=" + minStr + " AND " + name + "<=" + maxStr );
            }
        }
        public void mSqlWhereAndSearchString(ref string ArCurrentWhere, UInt16 AVarNr, bool AbTestValid = true)
        {
            string s = mVarGetSqlText(AVarNr, DSqlDataCmd.WhereHigherEqual, AbTestValid);

            mSqlWhereAND(ref ArCurrentWhere, s);
        }
        public void mSqlWhereAndSearchEqual(ref string ArCurrentWhere, UInt16 AVarNr, bool AbTestValid = true)
        {
            string s = mVarGetSqlText(AVarNr, DSqlDataCmd.WhereEqual, AbTestValid);

            mSqlWhereAND(ref ArCurrentWhere, s);
        }
 
    }





    // ==========================================================================================================
    // CSqlCmd
    // ========================

    public enum DSqlCommand
    {
        None,
        Insert,     // insert new row in table          
        Update,
        Select,
        Count,
        Delete,
        UtcTime,
        LocalTime,
        User,
        Other
    }
    /* mySql command:
 DELETE [LOW_PRIORITY] [QUICK] [IGNORE] FROM tbl_name [PARTITION (partition_name,...)] [WHERE where_condition] [ORDER BY ...] [LIMIT row_count]
 INSERT [LOW_PRIORITY | DELAYED | HIGH_PRIORITY] [IGNORE] [INTO] tbl_name [PARTITION (partition_name,...)] [(col_name,...)] 
            {VALUES | VALUE} ({expr | DEFAULT},...),(...),... [ ON DUPLICATE KEY UPDATE col_name=expr [, col_name=expr] ... ] 
INSERT [LOW_PRIORITY | DELAYED | HIGH_PRIORITY] [IGNORE] [INTO] tbl_name [PARTITION (partition_name,...)] SET col_name={expr | DEFAULT}, ...
        [ ON DUPLICATE KEY UPDATE col_name=expr[, col_name=expr] ... ]
UPDATE [LOW_PRIORITY] [IGNORE] table_reference SET col_name1={expr1|DEFAULT} [, col_name2={expr2|DEFAULT}] ...
        [WHERE where_condition] [ORDER BY ...] [LIMIT row_count]  
        
SELECT [DISTINCT] COUNT(*) FROM table  WHERE student.student_id=course.student_id 
            

SELECT [ALL | DISTINCT | DISTINCTROW ] [HIGH_PRIORITY] [MAX_STATEMENT_TIME = N] [STRAIGHT_JOIN] [SQL_SMALL_RESULT] [SQL_BIG_RESULT] [SQL_BUFFER_RESULT]
                [SQL_CACHE | SQL_NO_CACHE] [SQL_CALC_FOUND_ROWS] select_expr [, select_expr ...]
                [FROM table_references [PARTITION partition_list] [WHERE where_condition] [GROUP BY {col_name | expr | position} [ASC | DESC], ... [WITH ROLLUP]]
                [HAVING where_condition] [ORDER BY {col_name | expr | position} [ASC | DESC], ...]
                [LIMIT {[offset,] row_count | row_count OFFSET offset}] [PROCEDURE procedure_name(argument_list)]
                [INTO OUTFILE 'file_name' [CHARACTER SET charset_name]export_options| INTO DUMPFILE 'file_name'| INTO var_name [, var_name]][FOR UPDATE | LOCK IN SHARE MODE]]

        ORDER BY id DESC
        using command only as:
DELETE FROM tbl_name WHERE where_condition [LIMIT row_count];
INSERT INTO tbl_name SET col_name=expr, ...;
UPDATE  table_reference SET col_name1=expr1, col_name2=expr2... WHERE where_condition [ORDER BY ...] [LIMIT row_count]  
SELECT [DISTINCT] select_expr [, select_expr ...] FROM table_references [WHERE where_condition] [GROUP BY {col_name | expr | position} [ASC | DESC]]
                [ORDER BY {col_name | expr | position} [ASC | DESC], ...] [LIMIT row_count OFFSET offset] ;
SELECT [DISTINCT] COUNT(*) FROM table  WHERE student.student_id=course.student_id;

SELECT Now();   
SELECT UTC_TIMESTAMP();

     * */
    public enum DSqlSort
    {
        None,
        Ascending,
        Desending
    }
    public class CSqlCmd
    {
        private CSqlDBaseConnection mSqlConnection;
        private CSqlDataTableRow mSqlTable;
        private MySqlCommand mMySqlCommand;     //creste only at execute command
        private MySqlDataReader mMySqlReader;
        private UInt32 mNrRowsRead;

        private DSqlCommand mCommand;
        UInt64 mCmdVarFlags;
        private string mCmdString;
        private string mNamesList;
        private string mSetVarList;
        private string mWhereString;
        private string mOrderBy;
        private string mGroupBy;
        //private uint mWhereParmLevel;   // '(' Parenthis level as bit mask 
//        private uint mWhereContains;    // contains condition on parentis level
        private uint mLimit;
        private uint mLimitStart;
        private bool mbDistinct;

        public CSqlCmd( CSqlDBaseConnection ASqlConnection, CSqlDataTableRow ATable)
        {
            mSqlConnection = ASqlConnection;
            mSqlTable = ATable;
            mMySqlCommand = null;

            mClear();
        }

        ~CSqlCmd()
        {
            mCloseCmd();
        }

        public UInt64 mGetCmdVarFlags()
        {
            return mCmdVarFlags;
        }

        public void mClear()
        {
            mMySqlCommand = null;
            mMySqlReader = null;
            mNrRowsRead = 0;

            mCommand = DSqlCommand.None;
            mCmdVarFlags = 0; 
            mNamesList = "";
            mSetVarList = "";
            mWhereString = "";
            mOrderBy = "";
            mGroupBy = "";
//            mWhereParmLevel = 0;   // '(' Parenthis level as bit mask 
//            mWhereContains = 0;    // contains condition on parentis level
            mLimit = 0;
            mLimitStart = 0;
            mbDistinct = false;

        }
        public bool mbCheckSql( )
        {
            return mSqlConnection != null && mSqlTable != null;
                //&& mMySqlCommand != null  && mMySqlCommand != null;
        }
        public CSqlDBaseConnection mGetSqlConnection()
        {
            return mSqlConnection;
        }
        public CSqlDataTableRow mGetSqlTable()
        {
            return mSqlTable;
        }
       public UInt32 mGetNrRowsRead()
        {
            return mNrRowsRead;
        }

        public string mGetSqlDbTableName()
        {
            string s = "!.!";

            if( mSqlConnection != null && mSqlTable != null )
            {
                // added to lower for Unix/Linux case sensitive versions of mySql
                s = "`" + mSqlConnection.mSqlGetDBaseName() + "`." + mSqlTable.mGetTableNameOnly().ToLower();  
            }
            return s;
        }
        public void mCloseCmd()      // make sure SqlConnection is closed at end of command!
        {
            try
            {
                if (mMySqlReader != null)
                {
                    try
                    {
                        if (mMySqlReader != null && false == mMySqlReader.IsClosed)
                        {
                            if (mMySqlReader != null)
                            {
                                mMySqlReader.Close();
                            }
                        }
                        if (mMySqlReader != null)
                        {
                            mMySqlReader.Dispose();
                        }
                    }
                    catch (Exception ex)
                    {
                        // just disregard, do not log on screen any more
                        mMySqlReader = null;
                    }
                }
                if (mMySqlCommand != null)
                {
                    mMySqlCommand.Dispose();
                }
            }
            catch ( Exception ex )
            {
                CProgram.sLogException("error closeCmd " + mGetSqlDbTableName(), ex);
            }
 
            mMySqlCommand = null;      
            mMySqlReader = null;       // close command and result followd by a close connection
            if( mSqlConnection != null)
            {
                try
                {
                    mSqlConnection.mCloseSqlCmd();
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("error closeCmd2 " + mGetSqlDbTableName(), ex);
                }
            }
        }
        public bool mbIsCmdReaderOpen()
        {
            return mMySqlReader != null && mMySqlReader.IsClosed == false;
        }

        public bool mbIsCmdOpen()
        {
            return mMySqlReader != null || mMySqlCommand != null;
        }

        public bool mbCheckMakeCmd( DSqlCommand ACommand )
        {
            bool bOk = false;
            if( mCommand != ACommand )
            {
                CProgram.sLogError("SqlComand(" + ACommand.ToString() + ") not equal to set command "+ mCommand.ToString());
            }
            else if(mbCheckSql())
            {
                bOk = mbMakeCmdString();

                if( bOk )
                {
//                    bOk = mSqlConnection.mbSqlCreateConnection();
                }
            }
            return bOk;
            
        }
        public bool mbMakeCmdString()
        {
            bool bOk = false;

            if(mbCheckSql())
            {
                switch (mCommand)
                {
                    case DSqlCommand.None:
                        bOk = false;    // no command set
                        break;
                    case DSqlCommand.Insert:
                        // INSERT INTO tbl_name SET col_name=expr, ...;
                        if( mSetVarList.Length > 0)
                        {
                            mCmdString = "INSERT INTO " + mGetSqlDbTableName() + " SET " + mSetVarList + ";";
                            bOk = true;
                        }

                        break;
                    case DSqlCommand.Update:
                        //  UPDATE table_reference SET col_name1 = expr1, col_name2 = expr2...
                        //          WHERE where_condition[ORDER BY...][LIMIT row_count]
                        if (mSetVarList.Length > 0 && mWhereString.Length > 0) 
                        {
                            mCmdString = "UPDATE " + mGetSqlDbTableName() + " SET " + mSetVarList 
                                + " WHERE "+ mWhereString  + mOrderBy + mGetLimitString() + ";";
                            bOk = true;
                        }
                        break;
                    case DSqlCommand.Select:
                        // SELECT[DISTINCT] select_expr[, select_expr...] FROM table_references[WHERE where_condition] 
                        // [GROUP BY {col_name | expr | position} [ASC | DESC]] 
                        // [ORDER BY {col_name | expr | position} [ASC | DESC], ...] [LIMIT row_count OFFSET offset] ;
                        if (mNamesList.Length > 0 )
                        {
                            mCmdString = "SELECT " + mNamesList + " FROM " + mGetSqlDbTableName() + mGroupBy;

                            if (mWhereString != null && mWhereString.Length > 0)
                            {
                                mCmdString += " WHERE " + mWhereString;
                            }
                            mCmdString += mOrderBy + mGetLimitString() + ";";
                            bOk = true;
                        }
                        break;
                    case DSqlCommand.Count:
                        // SELECT COUNT(*) FROM <table> WHERE <test>;
                        // SELECT <var>, COUNT(*) FROM <table> WHERE <test> GROUP BY <var>
                        if (mNamesList.Length > 0)
                        {
                            mCmdString = "SELECT " + mNamesList + ", count(*) FROM " + mGetSqlDbTableName();

                            if (mWhereString != null && mWhereString.Length > 0)
                            {
                                mCmdString += " WHERE " + mWhereString;
                            }
                            mCmdString += " GROUP BY " + mNamesList;
                            mCmdString += mOrderBy + mGetLimitString() + ";";
                            bOk = true;
                        }
                        else
                        {
                            mCmdString = "SELECT count(*) FROM " + mGetSqlDbTableName();

                            if (mWhereString != null && mWhereString.Length > 0)
                            {
                                mCmdString += " WHERE " + mWhereString;
                            }
                            mCmdString += ";";
                            bOk = true;
                        }

                        break;
                    case DSqlCommand.Delete:
                        break;
                    case DSqlCommand.UtcTime:
                        break;
                    case DSqlCommand.LocalTime:
                        break;
                    case DSqlCommand.User:
                        break;
                    case DSqlCommand.Other:
                        break;
                }
            }
            return bOk && mCmdString != null && mCmdString.Length > 0;
        }
        public bool mbPrepareUserCommand(string ASqlCommand)
        {
            mClear();

            mCmdString = ASqlCommand;
            mCommand = DSqlCommand.User;
            return true;
        }
        public bool mbPrepareInsertCmd(UInt64 AVarFlags, bool AbUseStandard, bool AbActiveState)       // setup for creation of row, need the 
        {
            // INSERT INTO tbl_name SET col_name=expr, ...;
            bool bOk = false;
            UInt64 flags = AVarFlags;

            mClear();
            mCommand = DSqlCommand.Insert;
            if (mbCheckSql())
            {
                UInt64 indexMask = mSqlTable.mMaskIndexKey;
                if (AbUseStandard)
                {
                    flags |= mSqlTable.mMaskCreated; // add create state etc
                    flags |= mSqlTable.mMaskChanged;
                    mSqlTable.mSetCreated2CurrentUser();
                    mSqlTable.mbActive = AbActiveState;
                }
                if ((flags & indexMask) != 0) flags ^= indexMask;   // diable indexKey because it is auto created
                mCmdVarFlags = flags;                               // store flags for testing later
                bOk = mSqlTable.mbGetVarSetList(out mSetVarList, flags, ", ");
            }
            if (bOk == false) mCommand = DSqlCommand.None;
            return bOk;
        }
        public bool mbExecuteInsertCmd()       // creation of row
        {
            // INSERT INTO tbl_name SET col_name=expr, ...;
            bool bOk = false;
            string name = "";
            string where = "";
            string cmd = "";

            try
            {
                if( mSqlTable.mMaskPrimairyInsert == 0 && mSqlTable != null)
                {
                    CProgram.sLogError("Table " + mGetSqlDbTableName() + " does not have a primary key");
                }
                else if (mbCheckMakeCmd(DSqlCommand.Insert)//) // should be open already
                    &&  mSqlConnection.mbSqlOpenConnection())
                {
                   mMySqlCommand = mSqlConnection.mGetNewSqlCommand();

                    if(mMySqlCommand == null)
                    {
                        CProgram.sLogError("No new SqlCommand for table " + mGetSqlDbTableName());
                    }
                    else
                    {
                        mMySqlCommand.CommandText = mCmdString;
                        mMySqlReader = mMySqlCommand.ExecuteReader();

                        if (mMySqlReader != null)
                        {
                            bOk = mMySqlReader.RecordsAffected == 1;

                            if (false == bOk)
                            {
                                mLogCmdString("$ExecInsCmd");
                                CProgram.sLogError("Sql execute failed return != 1 cmd=" + mCmdString);
                            }
                            else
                            {
                                // insert worked now get the index
                                UInt64 flags = mSqlTable.mMaskPrimairyInsert;
                                UInt64 maskCreated = CSqlDataTableRow.sGetMask((UInt16)DSqlDataTableColum.CreatedUTC)
                                    | CSqlDataTableRow.sGetMask((UInt16)DSqlDataTableColum.CreatedDevice_IX)
                                    | CSqlDataTableRow.sGetMask((UInt16)DSqlDataTableColum.CreatedProgID);      // should be unique enough to find inserted row

                                //                               if ((mCmdVarFlags & maskCreatedUTC) != 0)
                                {
                                    flags |= maskCreated;
                                }

                                name = mSqlTable.mVarGetName((UInt16)DSqlDataTableColum.Index_KEY);
                                bOk = mSqlTable.mbGetVarWhereList(out where, flags);

                                if (false == bOk)
                                {
                                    mLogCmdString("@ExecInsCmd");
                                    CProgram.sLogError("Sql execute failed where =" + where + "for name=" + name + " table=" + mGetSqlDbTableName());
                                }
                                else
                                {
                                    try
                                    {
                                        if (mMySqlReader != null && false == mMySqlReader.IsClosed)
                                        {
                                            mMySqlReader.Close();
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        // just disregard, do not log on screen any more
                                        mMySqlReader = null;
                                    }

                                    //sv20170104                                   mSqlConnection.mSqlCloseConnection();

                                    cmd = "SELECT " + name + " FROM " + mGetSqlDbTableName() + " WHERE " + where 
                                        + " ORDER BY "+ name +" DESC LIMIT 1;"; // there should only be one
//                                    + " LIMIT 1;"; // there should only be one

                                    //sv20170104 use same                                    mSqlConnection.mbSqlOpenConnection();

                                    mMySqlCommand.CommandText = cmd;
                                    mMySqlReader = mMySqlCommand.ExecuteReader();

                                    bOk = mMySqlReader != null && mMySqlReader.HasRows && mMySqlReader.FieldCount >= 1;
                                    if (false == bOk)
                                    {
                                        CProgram.sLogError("Sql execute failed cmd=" + cmd);
                                    }
                                    else
                                    {
                                        mMySqlReader.Read();
                                        uint index = mMySqlReader.GetUInt32(0);
                                        bOk = index > 0;
                                        if (bOk)
                                        {
                                            mSqlTable.mIndex_KEY = index;
                                        }
                                        else
                                        {
                                            mLogCmdString("$ExecInsCmd");
                                            CProgram.sLogError("$ExecInsCmd quiry=" + cmd);
                                            CProgram.sLogError("Insert into dBase table " + mGetSqlDbTableName() + " returned index 0!");
                                        }
                                    }
                                }
                            }
                        }
                    }
                 }
            }
            catch (Exception ex)
            {
                string tblName = mGetSqlDbTableName();
                int pos = tblName.IndexOf('_');
                if( pos >= 0 )
                {
                    tblName = tblName.Substring(pos + 1);
                }
                mLogCmdString("$ExecInsCmd");
                CProgram.sLogError("$ExecInsCmd quiry=" + cmd);
                CProgram.sLogException("Insert into dBase table " + tblName + " failed", ex);
 
                bOk = false;
            }
            finally
            {
                if( mSqlConnection != null )
                {
                    mSqlConnection.mSqlCloseConnection();
                }
            }
            return bOk;
        }
        public bool mDoInsertCmd(UInt64 AStoreVarFlags, bool AbUseStandard, bool AbActiveState)       // creation of row with Prinairy, update with stored flags
        {
            bool bOk = false;

            if (mbCheckSql())
            {
                bOk = mbPrepareInsertCmd(AStoreVarFlags, AbUseStandard, AbActiveState);
                if( bOk )
                {
                    bOk = mbExecuteInsertCmd();
                }
            }
            mCloseCmd();
            return bOk;
        }
        public bool mbPrepareUpdateCmd(bool AbIndexOnly, UInt64 AVarFlags, bool AbUseStandard, 
            bool AbActiveState, UInt64 AForceUpdatePkMask )       //  update record indicated by Index_Key with given variables from flag 
        {
            //  UPDATE table_reference SET col_name1 = expr1, col_name2 = expr2...WHERE where_condition[ORDER BY...][LIMIT row_count]
            bool bOk = false;
            UInt64 flags = AVarFlags;

            mClear();
            mCommand = DSqlCommand.Update;
            if (mbCheckSql())
            {
                UInt64 indexMask = mSqlTable.mMaskIndexKey;
                UInt64 primaryMask = mSqlTable.mMaskPrimairyInsert | indexMask;
                
                if (AbUseStandard)
                {
                    flags |= mSqlTable.mMaskChanged; // add create state etc
                    mSqlTable.mSetChanged2CurrentUser();
                    mSqlTable.mbActive = AbActiveState;
                }
                flags &= UInt64.MaxValue ^ primaryMask;     // never update primairy values
                if( 0 != AForceUpdatePkMask )
                {
                    if( false == AbIndexOnly )
                    {
                        CProgram.sLogError("SQL Update missing IndexOnly with PK:" + AForceUpdatePkMask.ToString("X") + " table " + mGetSqlDbTableName());
                        mCommand = DSqlCommand.None;
                        return false;
                    }
                    if( 0 != (AForceUpdatePkMask & indexMask ))
                    {
                        CProgram.sLogError("SQL Update IndexKey in PK:" + AForceUpdatePkMask.ToString("X") + " table " + mGetSqlDbTableName());
                        mCommand = DSqlCommand.None;
                        return false;
                    }
                    flags |= AForceUpdatePkMask;
                }

                mCmdVarFlags = flags;                               // store flags for testing later
                bOk = mSqlTable.mbGetVarSetList(out mSetVarList, flags, ", ");

                if( AbIndexOnly)
                {
                    mSetLimit(1, 0);
                    mWhereString = mSqlTable.mVarGetSqlText((UInt16)DSqlDataTableColum.Index_KEY, DSqlDataCmd.WhereEqual);
                    bOk = mWhereString.Length > 0;
                }

            }
            if (bOk == false) mCommand = DSqlCommand.None;
            return bOk;
        }
        public void mWhereSetString(string AString)
        {
            mWhereString = AString;
        }
        public void mWhereAddString(string AString)
        {
            if (mWhereString.Length > 0) mWhereString += " AND ";
            mWhereString += AString;
        }
        public string mGetWhereString()
        {
            return mWhereString;
        }
        public string mGetCmdString()
        {
            return mCmdString;
        }
        public bool mWhereAddCmp( UInt16 AVarNr, DSqlDataCmd ACompareCmd )
        {
            bool bOk = false;

            if (mbCheckSql())
            {
                string text = mSqlTable.mVarGetSqlText( AVarNr, ACompareCmd );

                if (mWhereString.Length > 0) mWhereString += " AND ";
                mWhereString += text;

            }
            return bOk;
        }
        public void mSetLimit( UInt32 ALimit, UInt32 ALimitStart )
        {
            mLimit = ALimit;
            mLimitStart = ALimitStart;
        }
        public bool mbSetOrderBy( UInt16 AOrderByVariable, DSqlSort ASortOrder)
        {
            // [ORDER BY {col_name | expr | position}[ASC | DESC], ...]
            bool bOk = false;
            mOrderBy = "";

            if( ASortOrder == DSqlSort.None )
            {
                bOk = true;
            }
            else if( mSqlTable != null )
            {
                string name = mSqlTable.mVarGetName(AOrderByVariable);

                if( name.Length > 0 )
                {
                    mOrderBy = " ORDER BY " + name;

                    if( ASortOrder == DSqlSort.Ascending )
                    {
                        mOrderBy += " ASC";
                    }
                    else
                    {
                        mOrderBy += " DESC";
                    }
                    bOk = true;
                }
            }
            return bOk;

        }
        public string mGetLimitString()
        {
            // [LIMIT row_count OFFSET offset]
            string s = "";
            UInt32 limit = mLimit;

            if( limit == 0 && mSqlConnection != null )
            {
                limit = mSqlConnection.mGetSqlRowLimit();
            }
            if( limit > 0 )
            {
                s = " LIMIT " + limit.ToString();
                if (mLimitStart > 0)
                {
                    s += " OFFSET " + mLimitStart.ToString();
                }
            }
            return s;
        }
        public bool mbExecuteUpdateCmd()       // execute update record
        {
            bool bOk = false;
            try
            {
                if (mSqlTable.mMaskPrimairyInsert == 0 && mSqlTable != null)
                {
                    CProgram.sLogError("Table " + mGetSqlDbTableName() + " does not have a primary key");
                }
                else if (mbCheckMakeCmd(DSqlCommand.Update)
                     && mSqlConnection.mbSqlOpenConnection())
                {
                    mMySqlCommand = mSqlConnection.mGetNewSqlCommand();

                    if (mMySqlCommand == null)
                    {
                        CProgram.sLogError("No new SqlCommand for table " + mGetSqlDbTableName());
                    }
                    else
                    {
                        mMySqlCommand.CommandText = mCmdString;
                        mMySqlReader = mMySqlCommand.ExecuteReader();

                        if (mMySqlReader != null)
                        {
                            if( mLimit == 1 && mMySqlReader.RecordsAffected > 1)
                            {
                                CProgram.sLogError("SQL rows effected " + mMySqlReader.RecordsAffected + " != 1 for table " + mGetSqlDbTableName());
                            }
                            bOk = mMySqlReader.RecordsAffected >= 1;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                mLogCmdString("ExecUpdate");
                CProgram.sLogException("Update dBase table row failed " + mGetSqlDbTableName(), ex);
            }
            finally
            {
                mSqlConnection.mSqlCloseConnection();
            }
            return bOk;
        }
        public bool mDoUpdateCmd(bool AbIndexOnly, UInt64 AVarFlags, bool AbUseStandard, 
            bool AbActiveState, UInt64 AForceUpdatePkMask = 0)       // update record indicated by Index_Key with given variables from flag
        {                                                                                               // updates changed by and sets state
            bool bOk = false;

            if (mbCheckSql())
            {
                bOk = mbPrepareUpdateCmd(AbIndexOnly, AVarFlags, AbUseStandard, AbActiveState, AForceUpdatePkMask);
                if (bOk)
                {
                    bOk = mbExecuteUpdateCmd();
                }
            }
            return bOk;
        }
        public bool mbPrepareSelectCmd(bool AbRetrieveIndex, UInt64 AVarRetrieveFlags)       //  retrieve record with given variables indicated by where string
        {
            // SELECT[DISTINCT] select_expr[, select_expr...] FROM table_references[WHERE where_condition] 
            // [GROUP BY {col_name | expr | position} [ASC | DESC]] 
            // [ORDER BY {col_name | expr | position} [ASC | DESC], ...] [LIMIT row_count OFFSET offset] ;
            //  SELECT[DISTINCT] select_expr[, select_expr...] FROM table_references[WHERE where_condition] 
            //  [GROUP BY {col_name | expr | position} [ASC | DESC]]
            //  [ORDER BY {col_name | expr | position} [ASC | DESC], ...] [LIMIT row_count OFFSET offset] ;
            bool bOk = false;
            UInt64 flags = AVarRetrieveFlags;

            mClear();
            mCommand = DSqlCommand.Select;
            if (mbCheckSql())
            {
                if( AbRetrieveIndex)
                {
                    flags |= mGetSqlTable().mMaskIndexKey; 
                }
                mCmdVarFlags = flags;                               // store flags for testing later
                bOk = mSqlTable.mbGetVarNames(out mNamesList, flags, ", ");
            }
            if (bOk == false) mCommand = DSqlCommand.None;
            return bOk;
        }
        public bool mbPrepareCountCmd( UInt16 AVar)       //  retrieve record with given variables indicated by where string
        {
            // SELECT COUNT(*) FROM <table> WHERE <test>;
            // SELECT <var>, COUNT(*) FROM <table> WHERE <test> GROUP BY <var>
            bool bOk = false;
            bool bByVar = AVar > 0;

            mClear();
            mCommand = DSqlCommand.Count;
            if (mbCheckSql())
            {
                if( bByVar )
                {
                    mCmdVarFlags = CSqlDataTableRow.sGetMask(AVar);                               // store flags for testing later
                    bOk = mSqlTable.mbGetVarNames(out mNamesList, mCmdVarFlags, ", ");
                }
                else
                {
                    bOk = true;
                }
            }
            if (bOk == false) mCommand = DSqlCommand.None;
            return bOk;
        }
        public void mSetDistinct( bool AbDistinct )
        {
            mbDistinct = AbDistinct;
        }
        public void mLogCmdString(string AHeader,
                               [System.Runtime.CompilerServices.CallerLineNumber]int _AutoLN = 0,
                    [System.Runtime.CompilerServices.CallerMemberName]string _AutoFN = "",
                    [System.Runtime.CompilerServices.CallerFilePath] string _AutoCN = "")
        {
            CProgram.sLogError(AHeader + " sqlCmd:" + mCmdString, _AutoLN, _AutoFN, _AutoCN);
        }
        public bool mbExecuteSelectCmd()       // execute retrieve record with given variables indicated by where string
        {                                       // generates data 
            bool bOk = false;
            try
            {
                mNrRowsRead = 0;
                if (mSqlTable.mMaskPrimairyInsert == 0 && mSqlTable != null)
                {
                    CProgram.sLogError("Table " + mGetSqlDbTableName() + " does not have a primary key");
                }
                else if (mbCheckMakeCmd(DSqlCommand.Select)
                    && mSqlConnection.mbSqlOpenConnection())
                {
                    mMySqlCommand = mSqlConnection.mGetNewSqlCommand();

                    if (mMySqlCommand == null)
                    {
                        CProgram.sLogError("No new SqlCommand for table " + mGetSqlDbTableName());
                    }
                    else
                    {
                        mMySqlCommand.CommandText = mCmdString;
                        mMySqlReader = mMySqlCommand.ExecuteReader(CommandBehavior.CloseConnection);

                        if (mMySqlReader != null)
                        {
                            //  mMySqlReader.RecordsAffected = -1;
                            int n = mMySqlReader.FieldCount;
                            bOk = /*mMySqlReader.HasRows &&*/ n > 0;    // can be empty
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                mLogCmdString("ExecSelect");
                CProgram.sLogException("Select dBase table row failed " + mGetSqlDbTableName(), ex);
            }
            finally
            {
                if (bOk == false)       // do not close connection otherwise reader is disposed
                {
                    mSqlConnection.mSqlCloseConnection();
                }
            }
            return bOk;
        }
        public bool mbDoSelectCmd(UInt64 AWhereEqualVarFlags, bool AbRetrieveIndex, UInt64 ARetrieveVarFlags)       //  retrieve record with given variables indicated by where var is equal to current variable
        {
            bool bOk = false;

            if (mbCheckSql())
            {
                if (mbPrepareSelectCmd(AbRetrieveIndex, ARetrieveVarFlags))
                {
                    if (mSqlTable.mbGetVarWhereList(out mWhereString, AWhereEqualVarFlags))
                    {
                        bOk = mbExecuteSelectCmd();
                    }
                }
            }
            if (bOk == false)
            {
                mCloseCmd();
            }

            return bOk;
        }
        public List<CSqlDataTableRow> mExecuteSelectListCmd(out bool AbReadError)       // execute retrieve record with given variables indicated by where string
        {                                                            // generates data 
            List<CSqlDataTableRow> list = null;
            bool bError = true;

            try
            {
                CSqlDataTableRow row;
                UInt16 nrValues;
                UInt16 index = 0;
                string name = "";
                string text;
                UInt64 mask;
                Object value;
                bool bValid;

                mNrRowsRead = 0;
                if( mSqlTable == null )
                {
                    CProgram.sLogError("Table is not defined");

                }
                else if (mSqlTable.mMaskPrimairyInsert == 0 )
                {
                    CProgram.sLogError("SqlCmd: Table " + mGetSqlDbTableName() + " does not have a primary key");
                }
                else if (mbCheckMakeCmd(DSqlCommand.Select)
                     && mSqlConnection.mbSqlOpenConnection()) //should already be open
                {
                    using (MySqlCommand sqlCommand = mSqlConnection.mGetNewSqlCommand())
                    {
                        if (sqlCommand == null)
                        {
                            CProgram.sLogError("No new SqlCommand for table " + mGetSqlDbTableName());
                        }
                        else
                        {
                            sqlCommand.CommandText = mCmdString;
                            using (MySqlDataReader sqlReader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection))
                            {
                                if (sqlReader != null && sqlReader.FieldCount > 0)
                                {
                                    list = new List<CSqlDataTableRow>();

                                    if( list != null )
                                    {
                                        if (mCmdVarFlags != 0)
                                        {
                                            try
                                            {
                                                bError = false;
                                                while (sqlReader.Read())
                                                {

                                                    #region read into new CSqlDataTable and add to list
                                                    nrValues = (UInt16)sqlReader.FieldCount;

                                                    if (nrValues > 0)
                                                    {
                                                        row = mSqlTable.mCreateNewRow();
                                                        if (row == null)
                                                        {
                                                            bError = true;
                                                            CProgram.sLogError("SqlCmd: Table " + mGetSqlDbTableName() + "[" + mNrRowsRead.ToString() + "] failed to create");
                                                        }
                                                        else
                                                        {
                                                            mask = 0x01;
                                                            index = 0;
                                                            for (UInt16 i = 0; i < CSqlDataTableRow.cMaxNrColums; ++i)
                                                            {
                                                                if (0 != (mCmdVarFlags & mask) && index < nrValues)   // last values can be null and not stored in array
                                                                {
                                                                    text = "";
                                                                    try
                                                                    {
                                                                        if (false == sqlReader.IsDBNull(index))
                                                                        {
                                                                            value = sqlReader.GetValue(index);       // a null value in db can not be converted to a value of given type
                                                                            if (value != null)
                                                                            {
                                                                                text = value.ToString();    // on date time the ToString returns program format
                                                                            }
                                                                        }

                                                                        if (text.Length > 0)   // try to read if there is a valid string
                                                                        {
                                                                            DSqlDataType result = row.mVarGetSet(DSqlDataCmd.ParseString, i, ref name, ref text, out bValid);
                                                                            if (DSqlDataType.SqlError == result)
                                                                            {
                                                                                CProgram.sLogError("SqlCmd: Table " + mGetSqlDbTableName() + "[" + mNrRowsRead.ToString() 
                                                                                    + "] Failed parsing variable[" + index + "] for index " + i.ToString() + " '" + text + "'");
                                                                                bError = true;
                                                                            }
                                                                        }
                                                                    }
                                                                    catch (Exception )
                                                                    {
                                                                        // just accept that conversion errors result in variable is not read 
                                                                        //                                            CProgram.sLogException("Read SQL variable[" + index + "] with mask " + mask.ToString("X") + " failed.", ex);
                                                                    }
                                                                    ++index; // next variable
                                                                }
                                                                mask <<= 1;
                                                            }
                                                            list.Add(row);
                                                            ++mNrRowsRead;
                                                        }
                                                    }
                                                    #endregion
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                mLogCmdString("ExecSelectList["+list.Count.ToString()+"]");
                                                string s = "";
                                                try
                                                {
                                                    s = sqlReader.ToString();
                                                }
                                                catch( Exception)
                                                {

                                                }
                                                if( s != null && s.Length > 0)
                                                {
                                                    CProgram.sLogLine("SqlRow=" + s);

                                                }
                                                CProgram.sLogException("Read row from Select result failed. " + mGetSqlDbTableName(), ex);
                                                bError = true;
                                            }
                                        }
                                    }
                                    try
                                    {
                                        if (false == sqlReader.IsClosed)
                                        {
                                            sqlReader.Close();
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        // just disregard, do not log on screen any more
                                        mMySqlReader = null;
                                    }

                                }

                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                mLogCmdString("ExecSelectList");
                CProgram.sLogException("Select dBase table row failed " + mGetSqlDbTableName(), ex);
            }
            finally
            {
                mSqlConnection.mSqlCloseConnection();
            }
            AbReadError = bError;
            return list;
        }
        public List<CSqlDataTableRow> mDoSelectListCmd(out bool AbReadError, UInt64 AWhereEqualVarFlags, bool AbRetrieveIndex, UInt64 ARetrieveVarFlags)       //  retrieve record with given variables indicated by where var is equal to current variable
        {
            List<CSqlDataTableRow> list = null;
            bool bError = true;

             if (mbCheckSql())
            {
                if( mbPrepareSelectCmd(AbRetrieveIndex, ARetrieveVarFlags))
                {
                    if( mSqlTable.mbGetVarWhereList(out mWhereString, AWhereEqualVarFlags))
                    {
                        list = mExecuteSelectListCmd(out bError);
                    }
                }
            }
            mCloseCmd();
            AbReadError = bError;

            return list;
        }

        public bool mbExecuteCountCmd(out UInt32 ArTotalCount)       // execute retrieve record with given variables indicated by where string
        {
            UInt32 totalCount = 0;
            bool bError = true;

            try
            {
                bool bByVar = mCmdVarFlags != 0;
                UInt16 nrValues;
                string text;
                Object value;

                mNrRowsRead = 0;
                if (mSqlTable == null)
                {
                    CProgram.sLogError("Table is not defined");

                }
                else if (mSqlTable.mMaskPrimairyInsert == 0)
                {
                    CProgram.sLogError("SqlCmd: Table " + mGetSqlDbTableName() + " does not have a primary key");
                }
                else if( bByVar)
                {
                    CProgram.sLogError("SqlCmd: Table " + mGetSqlDbTableName() + " count command called with var defined");
                }
                else if (mbCheckMakeCmd(DSqlCommand.Count)
                     && mSqlConnection.mbSqlOpenConnection()) //should already be open
                {
                    using (MySqlCommand sqlCommand = mSqlConnection.mGetNewSqlCommand())
                    {
                        if (sqlCommand == null)
                        {
                            CProgram.sLogError("No new SqlCommand for table " + mGetSqlDbTableName());
                        }
                        else
                        {
                            sqlCommand.CommandText = mCmdString;
                            using (MySqlDataReader sqlReader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection))
                            {
                                if (sqlReader != null && sqlReader.FieldCount > 0)
                                {
                                    try
                                    {
                                        if (sqlReader.Read())
                                        {

                                            #region read into new CSqlDataTable and add to list
                                            nrValues = (UInt16)sqlReader.FieldCount;

                                            if (nrValues >= 1)
                                            {
                                                text = "";
                                                try
                                                {
                                                    if (false == sqlReader.IsDBNull(0))
                                                    {
                                                        value = sqlReader.GetValue(0);       // a null value in db can not be converted to a value of given type
                                                        if (value != null)
                                                        {
                                                            text = value.ToString();    // on date time the ToString returns program format
                                                        }
                                                    }
                                                    if (text.Length > 0)   // try to read if there is a valid string
                                                    {
                                                        if (UInt32.TryParse(text, out totalCount))
                                                        {
                                                            bError = false;
                                                        }
                                                        else
                                                        {
                                                            CProgram.sLogError("SqlCmd: Table " + mGetSqlDbTableName() + " Failed parsing count");
                                                            bError = true;
                                                        }
                                                    }
                                                }
                                                catch (Exception)
                                                {
                                                    // just accept that conversion errors result in variable is not read 
                                                }
                                                ++mNrRowsRead;
                                            }
                                        }
                                        #endregion

                                    }
                                    catch (Exception ex)
                                    {
                                        mLogCmdString("ExecCount = " + totalCount);
                                        string s = "";
                                        try
                                        {
                                            s = sqlReader.ToString();
                                        }
                                        catch (Exception)
                                        {

                                        }
                                        if (s != null && s.Length > 0)
                                        {
                                            CProgram.sLogLine("SqlRow=" + s);

                                        }
                                        CProgram.sLogException("Read row from Count result failed." + mGetSqlDbTableName(), ex);
                                        bError = true;
                                    }
                                }
                                try
                                {
                                    if (false == sqlReader.IsClosed)
                                    {
                                        sqlReader.Close();
                                    }
                                }
                                catch (Exception ex)
                                {
                                    // just disregard, do not log on screen any more
                                    mMySqlReader = null;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                mLogCmdString("ExecCount");
                CProgram.sLogException("Count dBase table row failed " + mGetSqlDbTableName(), ex);
            }
            finally
            {
                mSqlConnection.mSqlCloseConnection();
            }
            ArTotalCount = totalCount;
            return bError;
        }
        public List<CSqlCountItem> mExecuteCountListCmd(out bool AbReadError, out UInt32 ArTotalCount)       // execute retrieve record with given variables indicated by where string
        {
            List<CSqlCountItem> list = null;
            UInt32 totalCount = 0;
            bool bError = true;

            try
            {
                bool bByVar = mCmdVarFlags != 0;
                CSqlCountItem item;
                UInt16 nrValues;
                string text;
                Object value;

                mNrRowsRead = 0;
                if (mSqlTable == null)
                {
                    CProgram.sLogError("Table is not defined");

                }
                else if (mSqlTable.mMaskPrimairyInsert == 0)
                {
                    CProgram.sLogError("SqlCmd: Table " + mGetSqlDbTableName() + " does not have a primary key");
                }
                else if (false == bByVar)
                {
                    CProgram.sLogError("SqlCmd: Table " + mGetSqlDbTableName() + " count list command called with no var defined");
                }
                else if (mbCheckMakeCmd(DSqlCommand.Count)
                     && mSqlConnection.mbSqlOpenConnection()) //should already be open
                {
                    using (MySqlCommand sqlCommand = mSqlConnection.mGetNewSqlCommand())
                    {
                        if (sqlCommand == null)
                        {
                            CProgram.sLogError("No new SqlCommand for table " + mGetSqlDbTableName());
                        }
                        else
                        {
                            sqlCommand.CommandText = mCmdString;
                            using (MySqlDataReader sqlReader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection))
                            {
                                if (sqlReader != null && sqlReader.FieldCount > 0)
                                {
                                    list = new List<CSqlCountItem>();

                                    if (list != null)
                                    {
                                        if (mCmdVarFlags != 0)
                                        {
                                            try
                                            {
                                                bError = false;
                                                while (sqlReader.Read())
                                                {

                                                    #region read into new CSqlDataTable and add to list
                                                    nrValues = (UInt16)sqlReader.FieldCount;

                                                    if (nrValues >= 2)
                                                    {
                                                        item = new CSqlCountItem();
                                                        if (item == null)
                                                        {
                                                            bError = true;
                                                            CProgram.sLogError("SqlCmd: Table " + mGetSqlDbTableName() + "[" + mNrRowsRead.ToString() + "] failed to create CountItem");
                                                        }
                                                        else
                                                        {
                                                            text = "";
                                                            try
                                                            {
                                                                if (false == sqlReader.IsDBNull(0))
                                                                {
                                                                    value = sqlReader.GetValue(0);       // a null value in db can not be converted to a value of given type
                                                                    if (value != null)
                                                                    {
                                                                        text = value.ToString();    // on date time the ToString returns program format
                                                                    }
                                                                }
                                                                item._mVarValue = text;
                                                                if (false == sqlReader.IsDBNull(1))
                                                                {
                                                                    value = sqlReader.GetValue(1);       // a null value in db can not be converted to a value of given type
                                                                    if (value != null)
                                                                    {
                                                                        text = value.ToString();    // on date time the ToString returns program format
                                                                    }
                                                                }

                                                                if (text.Length > 0)   // try to read if there is a valid string
                                                                {
                                                                    if (UInt32.TryParse(text, out item._mCount))
                                                                    {
                                                                        list.Add(item);
                                                                        totalCount += item._mCount;
                                                                    }
                                                                    else
                                                                    {
                                                                        CProgram.sLogError("SqlCmd: Table " + mGetSqlDbTableName() + "[" + mNrRowsRead.ToString()
                                                                            + "] Failed parsing count");

                                                                        bError = true;
                                                                    }
                                                                }
                                                            }
                                                            catch (Exception)
                                                            {
                                                                // just accept that conversion errors result in variable is not read 
                                                            }
                                                            ++mNrRowsRead;
                                                        }
                                                    }
                                                    #endregion
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                mLogCmdString("ExecCountList[" + list.Count.ToString() + "] = " + totalCount);
                                                string s = "";
                                                try
                                                {
                                                    s = sqlReader.ToString();
                                                }
                                                catch (Exception)
                                                {

                                                }
                                                if (s != null && s.Length > 0)
                                                {
                                                    CProgram.sLogLine("SqlRow=" + s);

                                                }
                                                CProgram.sLogException("Read row from Count result failed." + mGetSqlDbTableName(), ex);
                                                bError = true;
                                            }
                                        }
                                    }
                                    try
                                    {
                                        if (false == sqlReader.IsClosed)
                                        {
                                            sqlReader.Close();
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        // just disregard, do not log on screen any more
                                        mMySqlReader = null;
                                    }

                                }

                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                mLogCmdString("ExecCountList");
                CProgram.sLogException("Count dBase table row failed " + mGetSqlDbTableName(), ex);
            }
            finally
            {
                mSqlConnection.mSqlCloseConnection();
            }
            AbReadError = bError;
            ArTotalCount = totalCount;
            return list;
        }
        public bool mbDoRetrieveIndex( UInt32 AIndex, UInt64 ARetrieveVarFlags)       //  retrieve record with given variables indicated by where var is equal to current variable
        {
            bool bOk = false;

            try
            {
                if (mbCheckSql())
                {
                    mSqlTable.mIndex_KEY = AIndex;
                    if (mbPrepareSelectCmd(true, ARetrieveVarFlags))
                    {
                        UInt64 flags = mSqlTable.mMaskIndexKey;

                        if (mSqlTable.mbGetVarWhereList(out mWhereString, flags))
                        {
                            if (mbExecuteSelectCmd())
                            {
                                bool bError;

                                if (mbReadOneRow(out bError))
                                {
                                    bOk = true;
                                }
                            }
                        }
                    }
                }

            }
            catch ( Exception ex)
            {
                CProgram.sLogException("Failed reading record " + mGetSqlDbTableName() + "[" + AIndex.ToString() + "]", ex);
            }
            mCloseCmd();

            return bOk;
        }

        public bool mbReadOneRow(out bool AbReadError)
        {
            return mbReadIntoRow(out AbReadError, mSqlTable);
        }

        public bool mbReadIntoRow(out bool AbReadError, CSqlDataTableRow ADataTableRow)
        {
            bool bOk = false;
            bool bError = true;

            if (mbCheckSql() && ADataTableRow != null && mMySqlReader != null)
            {
                UInt16 nrValues = (UInt16)mMySqlReader.FieldCount;
                UInt16 index = 0;
                string name = "";
                string text;
                UInt64 mask = 0x01;
                Object value;
                bool bValid;

                if (mCmdVarFlags != 0)
                {
                    try
                    {
                        bError = false;
                        if (mMySqlReader.Read())
                        {
                            ++mNrRowsRead;
                            nrValues = mMySqlReader == null ? (UInt16)0 : (UInt16)mMySqlReader.FieldCount;

                            if (nrValues > 0)
                            {

                                bOk = true;
                                for (UInt16 i = 0; i < CSqlDataTableRow.cMaxNrColums; ++i)
                                {
                                    if (0 != (mCmdVarFlags & mask) && index < nrValues)   // last values can be null and not stored in array
                                    {
                                        text = "";
                                        try
                                        {// strange that mMySqlReader == null occurs within loop

                                            if (mMySqlReader != null && false == mMySqlReader.IsDBNull( index  ))
                                            {
                                                value = mMySqlReader.GetValue(index);       // a null value in db can not be converted to a value of given type
                                                if (value != null)
                                                {
                                                    text = value.ToString();
                                                }
                                            }

                                            if (text.Length > 0)   // try to read if there is a valid string
                                            {
                                                DSqlDataType result = ADataTableRow.mVarGetSet(DSqlDataCmd.ParseString, i, ref name, ref text, out bValid);
                                                if (DSqlDataType.SqlError == result)
                                                {
                                                    CProgram.sLogError("Failed parsing variable[" + index + "] for index " + i.ToString() + "  " + mGetSqlDbTableName());
                                                    bOk = false;
                                                }
                                            }
                                        }
                                        catch (Exception )
                                        {
                                            // just accept that conversion errors result in variable is not read 
//                                            CProgram.sLogException("Read SQL variable[" + index + "] with mask " + mask.ToString("X") + " failed.", ex);
                                        }
                                        ++index; // next variable
                                    }
                                    mask <<= 1;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        mLogCmdString("readIntoRow");
                        string s = "";
                        try
                        {
                            if (mMySqlReader != null)
                            {
                                s = mMySqlReader.ToString();
                            }
                        }
                        catch (Exception)
                        {

                        }
                        if (s != null && s.Length > 0)
                        {
                            CProgram.sLogLine("SqlRow=" + s);

                        }
                        CProgram.sLogException("Read row from Select result failed. " + mGetSqlDbTableName(), ex);
                        bError = true;
                    }
                }
            }
            AbReadError = bError;
            return bOk;
        }

        public bool mbPrepareDeleteCmd()
        {
            // DELETE FROM tbl_name WHERE where_condition[LIMIT row_count];
            bool bOk = false;

            if (mbCheckSql())
            {
            }
            return bOk;

        }
        public bool mbPrepareCountCmd(UInt64 AVarRetrieveFlag)
        {
            // SELECT [DISTINCT] COUNT(*) FROM table  WHERE student.student_id=course.student_id;
            bool bOk = false;

            if (mbCheckSql())
            {
            }
            return bOk;


        }

        public bool mbDoAskNow(out DateTime ADateTime)
        {
            // SELECT Now();
            bool bOk = false;

            ADateTime = DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Local);
                
            if (mbCheckSql())
            {
            }
            return bOk;

        }
        public bool mbDoAskUtc(out DateTime ADateTime)
        {
            //SELECT UTC_TIMESTAMP();
            bool bOk = false;

            ADateTime = DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Utc);
            if (mbCheckSql())
            {
            }
            return bOk;
        }
    }
}
