﻿// ReqTextForm
// by Simon Vlaar 
// can be used by Techmedic Development B.V
// created 19 Maart 2018

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Program_Base
{
 
    public partial class ReqTextForm : Form
    {
		private static ReqTextForm _sReqTextForm = null;

		public string _mStringValue, _mStringOrg;
        public bool _mbResult = false;

        public ReqTextForm()
        {
            InitializeComponent();

            textBoxValue.Focus();
        }

        public static bool sbReqText(string ATitle, string AVarName, ref string ArVar, UInt16 AMaxNrChar = 0)
        {
            bool bOk = false;

            if (_sReqTextForm == null)
            {
                _sReqTextForm = new ReqTextForm();
            }
            if (_sReqTextForm != null)
            {
                string title = CProgram.sMakeProgTitle(ATitle, false, true);

                _sReqTextForm.Text = title;
                _sReqTextForm.mInitText(AVarName, ArVar, AMaxNrChar);
                //                _sReqTextForm.textBoxValue.SelectAll();
                _sReqTextForm.buttonFromClipboard.Visible = true;

                _sReqTextForm.textBoxValue.Select(0, 0);
                _sReqTextForm.ShowDialog();

                bOk = _sReqTextForm.mbGetTextResult(ref ArVar);
            }
            return bOk;
        }
        public static bool sbShowText(string ATitle, string AVarName, string ArVar)
        {
            bool bOk = false;

            if (_sReqTextForm == null)
            {
                _sReqTextForm = new ReqTextForm();
            }
            if (_sReqTextForm != null)
            {
                string title = CProgram.sMakeProgTitle(ATitle, false, true);

                _sReqTextForm.Text = title;
                _sReqTextForm.mInitText(AVarName, ArVar, 0);

                _sReqTextForm.textBoxValue.ReadOnly = true;
                _sReqTextForm.buttonToClipboard.Visible = true;

                _sReqTextForm.textBoxValue.Select(0,0);
                _sReqTextForm.buttonFromClipboard.Visible = false;
                _sReqTextForm.buttonUndo.Visible = false;
                _sReqTextForm.buttonClear.Visible = false;

                _sReqTextForm.ShowDialog();
                bOk = _sReqTextForm._mbResult;
            }
            return bOk;
        }


        private void buttonCancel_Click(object sender, EventArgs e)
        {
            _mbResult = false;
            Close();
        }

        private void buttonUndo_Click(object sender, EventArgs e)
        {
            textBoxValue.Text = _mStringOrg;
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
			_mStringValue = textBoxValue.Text;
			_mbResult = true;
			if ( _mbResult)
            {
                Close();
            }
        }

        private void textBoxValue_KeyPress(object sender, KeyPressEventArgs e)
        {
        }

        private void ReqTextForm_Shown(object sender, EventArgs e)
        {
            textBoxValue.Focus();

        }

        public void mInitText( string AVarName, string AText, UInt16 AMaxNrChar )
        {
            _mStringOrg = AText;
            _mStringValue = AText;
            _mbResult = false;

            labelName.Text = AVarName + " = ";
			textBoxValue.MaxLength = AMaxNrChar == 0 ? 65000 : AMaxNrChar;
            textBoxValue.Text = AText;
            toolTip.SetToolTip(textBoxValue, "");
            textBoxValue.Focus();

        }
        public bool mbGetTextResult( ref string ArText)
        {
            if( _mbResult )
            {
                ArText = _mStringValue;
            }
            return _mbResult;
        }

		private void buttonClear_Click( object sender, EventArgs e )
		{
			textBoxValue.Text = "";
		}

		private void panel15_Paint( object sender, PaintEventArgs e )
		{
		
		}

		private void buttonClipboard_Click( object sender, EventArgs e )
		{
            try
            {
                textBoxValue.Text = Clipboard.GetText();
            }
            catch (Exception)
            {

            }
        }

        private void panel10_Paint( object sender, PaintEventArgs e )
		{
		}

        private void buttonToClipboard_Click(object sender, EventArgs e)
        {
            try
            {
                Clipboard.SetText(textBoxValue.Text);
            }
            catch (Exception)
            {

            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
        }
    }
}
