﻿// CEncryptString 
// by Simon Vlaar 
// can be used by Techmedic Development B.V
// created 24 August 2016
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Program_Base
{
    public enum DEncryptLevel
    {
        None,           // no encryption
        L1_Program,        // encrypt with program hash code
        L1_HashCode,       // encrypt program and table Index and Hash code
        L1_HashTable,      // encrypt with program, hash and extra hash table
                            // Level2 add bit swap function
        L2_Program,        // encrypt with program hash code
        L2_HashCode,       // encrypt program and table Index and Hash code
        L2_HashTable,      // encrypt with program, hash and extra hash table
    }

    public class CEncryptionTable
    {
        private static string mTableName;       // Organisation Name
        private static Int32 mTableIndex;       // Organisation index
        private static UInt16 mTableSize;
        private static byte[] mHashTable;

        public CEncryptionTable()
        {
            mTableName = "";
            mTableIndex = 0;
            mTableSize = 0;
            mHashTable = null;
        }
    }

    public class CEncryptedString
    {
        private string mName;       // name of 
        private DEncryptLevel mNeededLevel;
        private string mEncrypted;
        private UInt16 mMinSize; 

        public CEncryptedString()
        {
            mName = "";
            mNeededLevel = DEncryptLevel.L1_HashTable;
            mEncrypted = "";
        }

        public CEncryptedString(string AName, DEncryptLevel ALevelNeeded, UInt16 AMinSize = 0)
        {
            mName = AName;
            mNeededLevel = ALevelNeeded;
            mEncrypted = "";
            mMinSize = AMinSize;
        }

        public void mClear()
        {
            mEncrypted = "";
        }

        // returns true if value has changed and must be stored

        public bool mbEncrypt(string AString)
        {
            mEncrypted = AString;

            return true;
        }
        public bool mbSetEncryptLevel(string AName, DEncryptLevel ALevelNeeded, UInt16 AMinSize = 0)
        {
            return true;
        }
        public void mbSetEncryptEmpty(string AName, DEncryptLevel ALevelNeeded)
        {
            mName = AName;
            mNeededLevel = ALevelNeeded;
            mEncrypted = null;
        }
        public static CEncryptedString sMakeCopy(CEncryptedString AFrom)    // makes a new copy
        {
            CEncryptedString copy = null;

            if (AFrom != null)
            {
                copy = new CEncryptedString();

                if (copy != null)
                {
                    copy.mName = AFrom.mName;
                    copy.mNeededLevel = AFrom.mNeededLevel;
                    copy.mMinSize = AFrom.mMinSize;
                    copy.mEncrypted = String.Copy(AFrom.mEncrypted);
                }
            }
            return copy;
        }
   
        public CEncryptedString mCreateCopy()
        {
            CEncryptedString copy = new CEncryptedString();

            if (copy != null)
            {
                copy.mName = mName;
                copy.mNeededLevel = mNeededLevel;
                copy.mMinSize = mMinSize;
                copy.mEncrypted = String.Copy(mEncrypted);
            }

            return copy;
        }
        public bool mbCopyTo(ref CEncryptedString ATo)    // makes a new copy
        {
            bool bOk = false;

            if (ATo == null)
            {
                ATo = new CEncryptedString();
            }
            if (ATo != null)
            {
                ATo.mName = mName;
                ATo.mNeededLevel = mNeededLevel;
                ATo.mMinSize = mMinSize;
                ATo.mEncrypted = String.Copy(mEncrypted);
                bOk = true;
            }
            return bOk;
        }

        public bool mbCopyFrom( CEncryptedString AFrom)    // makes a new copy
        {
            bool bOk = false;

            if (AFrom != null)
            {
                mName = AFrom.mName;
                mNeededLevel = AFrom.mNeededLevel;
                mMinSize = AFrom.mMinSize;
                mEncrypted = AFrom.mEncrypted;
                bOk = true;
            }
            return bOk;
        }


        public void mbSetEmpty()
        {
            mEncrypted = null;
        }
        public string mDecrypt()
        {
            return mEncrypted;
        }
        public bool mbUpgradeLevel(DEncryptLevel ALevelNeeded, UInt16 AMinSize = 0 )
        {
            return true;
        }
        public string mGetEncrypted()
        {
            return mEncrypted;
        }
        public bool mbSetEncrypted(string AEncrypted)
        {
            mEncrypted = AEncrypted;
            return true;
        }
        public bool mbNotEmpty()
        {
            return mEncrypted != null && mEncrypted.Length > 0;
        }
        public bool mbIsEmpty()
        {
            return mEncrypted == null || mEncrypted.Length == 0;
        }
        public bool mbIsEqual(CEncryptedString ATest)
        {
            bool bEqual = mEncrypted == null || mEncrypted.Length == 0;

            if (ATest != null)
            {
                if (bEqual)
                {
                    bEqual = ATest.mEncrypted == null || ATest.mEncrypted.Length == 0;
                }
                else
                {
                    bEqual = ATest.mEncrypted == mEncrypted;    // same encryption results in same string
                }
            }
            return bEqual;
        }
        public override string ToString()
        {
            //            return "!illegal use of toString!";
            //return mDecrypt();      // unportunatly c# makes an auto ToString
            return "!ES=" + mEncrypted + "!";      // unportunatly c# makes an auto ToString
        }
        public static bool sbCmpSearchString(CEncryptedString ATest, string AIncludes)
        {
            bool bIncludes = true;

            if (AIncludes != null && AIncludes.Length > 0)
            {
                bIncludes = ATest != null && false == ATest.mbIsEmpty();

                if (bIncludes)
                {
                    bIncludes = CProgram.sbCmpSearchString(ATest.mDecrypt(), AIncludes);
                }
            }
            return bIncludes;
        }
        public static bool sbCmpSearchString(CEncryptedString ATest, CEncryptedString AIncludes)
        {
            bool bIncludes = true;

            if (AIncludes != null && false == AIncludes.mbIsEmpty())
            {
                bIncludes = sbCmpSearchString(ATest, AIncludes.mDecrypt());
            }
            return bIncludes;
        }
        public void mAddLine(string AAddLine)
        {
            if (AAddLine != null && AAddLine.Length > 0)
            {
                string text = mDecrypt();
                if (text != null && text.Length > 0)
                {
                    text = text + "\r\n" + AAddLine;
                }
                else
                {
                    text = AAddLine;
                }
                mbEncrypt(text);
            }
        }
        public void mAddText(ref string ArText, string AAddText, string ASeperator)
        {
            if (AAddText != null && AAddText.Length > 0)
            {
                string text = mDecrypt();
                if (text != null && text.Length > 0)
                {
                    text = text + ASeperator + AAddText;
                }
                else
                {
                    text = AAddText;
                }
                mbEncrypt(text);
             }
        }

    }

    public class CEncryptedInt  // encryption is not yet made
    {
        private string mName;       // name of encrypted int
        private DEncryptLevel mNeededLevel;
        Int32 mEncrypted;

        private const Int32 cModeLevelMask = 0x7C000000;
        private const UInt16 cNodeLevelShift = 26;
        // encryption is not yet made

       // returns if value has changed and must be stored
        public CEncryptedInt()
        {
            mName = "";
            mNeededLevel = DEncryptLevel.L1_HashTable;
            mEncrypted = 0;
        }
        public CEncryptedInt(string AName, DEncryptLevel ALevelNeeded )
        {
            mName = AName;
            mNeededLevel = ALevelNeeded;
            mEncrypted = 0;
         }
        public void mClear()
        {
            mEncrypted = 0;
        }
        public static CEncryptedInt sCreateCopy(CEncryptedInt AFrom)    // makes a new copy
        {
            CEncryptedInt copy = null;

            if (AFrom != null)
            {
                copy = new CEncryptedInt();

                if (copy != null)
                {
                    copy.mName = AFrom.mName;
                    copy.mNeededLevel = AFrom.mNeededLevel;
                    copy.mEncrypted = AFrom.mEncrypted;
                }
            }
            return copy;
        }
        public bool mbIsEqual(CEncryptedInt ATest)    // makes a new copy
        {
            bool bEqual = false;

            if (ATest != null)
            {
                bEqual = mDecrypt() == ATest.mDecrypt();
            }
            return bEqual;
        }
        public bool mbIsNotEqual(CEncryptedInt ATest)    // makes a new copy
        {
            bool bEqual = false;

            if (ATest != null)
            {
                bEqual = mDecrypt() == ATest.mDecrypt();
            }
            return false == bEqual;
        }

        public CEncryptedInt mCreateCopy()
        {
            CEncryptedInt copy = new CEncryptedInt();

            if (copy != null)
            {
                copy.mName = mName;
                copy.mNeededLevel = mNeededLevel;
                copy.mEncrypted = mEncrypted;
            }

            return copy;
        }
        public bool mbCopyTo(ref CEncryptedInt ATo)    // makes a new copy
        {
            bool bOk = false;

            if (ATo == null)
            {
                ATo = new CEncryptedInt();
            }
            if (ATo != null)
            {
                ATo.mName = mName;
                ATo.mNeededLevel = mNeededLevel;
                ATo.mEncrypted = mEncrypted;
                bOk = true;
            }
            return bOk;
        }

        public bool mbCopyFrom(CEncryptedInt AFrom)    // makes a new copy
        {
            bool bOk = false;

            if (AFrom != null)
            {
                mName = AFrom.mName;
                mNeededLevel = AFrom.mNeededLevel;
                mEncrypted = AFrom.mEncrypted;
                bOk = true;
            }
            return bOk;
        }

        // function return true when encryption level is increased
        public bool mEncrypt(Int32 AInt)
        {
            mEncrypted = AInt;
            return true;
        }
        public bool mbSetEncryptLevel(string AName, DEncryptLevel ALevelNeeded)
        {
            mName = AName;
            mNeededLevel = ALevelNeeded;
            return true;
        }
        public void mbSetEncryptZero(string AName, DEncryptLevel ALevelNeeded)
        {
            mName = AName;
            mNeededLevel = ALevelNeeded;
            mEncrypted = 0;
        }
        public void mbSetZero()
        {
            mEncrypted = 0;
        }
        public Int32 mDecrypt()
        {
            return mEncrypted;
        }
        public bool mbUpgradeLevel(DEncryptLevel ALevelNeeded)
        {
            // if( ALevelNeeded )

            return true;
        }
        bool mbIsEncryptOk()
        {
            return true;
        }
        public DEncryptLevel mGetEncryptionLevel()
        {
            DEncryptLevel level = DEncryptLevel.None;

/*            if (0 == (AInt & 0x80000000))
            {   // positive int
                bOk = (AInt & 0x7C000000) == 0;
            }
            else
            {   // negative
                bOk = (AInt & 0x7C000000) == 0x7C000000;
            }
*/
            return level;

        }
        public static DEncryptLevel sGetEncryptionLevel( UInt32 AInt)
        {
            DEncryptLevel level = DEncryptLevel.None;
/*            UInt32 modeLevel = AInt & 0x7C000000;

            if (0 == (AInt & 0x80000000))
            {   // positive int
                if( modeLevel != 0 )
                {
                    level = (DEncryptLevel)(modeLevel >> )
                }
            }
            else
            {   // negative
                bOk = (AInt & 0x7C000000) == 0x7C000000;
            }
*/            return level;

        }
        public bool mbValidInt( Int32 AInt )    // returns if int is within valid range
        {
            bool bOk;

            if( 0 == (AInt & 0x80000000 ))
            {   // positive int
                bOk = (AInt & 0x7C000000) == 0;
            }
            else
            {   // negative
                bOk = (AInt & 0x7C000000) == 0x7C000000;
            }
            return bOk;
        }
        public Int32 mGetEncryptedInt()
        {
            return mEncrypted;
        }
        public bool mbSetEncrypted(Int32 AEncrypted)
        {
            mEncrypted = AEncrypted;
            return true;
        }
        public bool mbNotZero()
        {
            return mEncrypted != 0;
        }
        public bool mbIsZero()
        {
            return mEncrypted != 0;
        }

        public bool mbEncryptDate( DateTime ADate )
        {
            Int32 oldValue = mEncrypted;

            if( ADate != null && ADate > DateTime.MinValue)
            {
                mEncrypted = (Int32)CProgram.sCalcYMD( ADate );
            }
            else
            {
                mEncrypted = 0;
            }
            return mEncrypted != oldValue;
        }
        public DateTime mDecryptToDate()
        {
            Int32 dateInt = mDecrypt();
            DateTime dt = DateTime.MinValue;
            bool bOk = false;

            if (dateInt != 0)
            {
                if (dateInt < 0)
                {
                    CProgram.sLogError("DecrytToDate: Invalid value " + dateInt.ToString() + " for Date");
                }
                if (dateInt > 99991231)
                {
                    CProgram.sLogError("DecrytToDate Invalid value " + dateInt.ToString() + " for Date");
                    dt = DateTime.MaxValue;
                }
                else
                {
                    try
                    {
                        CProgram.sbSplitYMD((UInt32)dateInt, out dt);
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogError("DecrytToDate Invalid value " + dateInt.ToString() + " for Date");
                        dt = DateTime.MinValue;
                    }
                }
            }
            return dt;
        }
        public UInt16 mDecryptYear()
        {
            Int32 dateInt = mDecrypt();
            UInt16 year = 0;
            bool bOk = false;

            if (dateInt != 0)
            {
                if (dateInt < 0)
                {
                    CProgram.sLogError("DecrytYear: Invalid value " + dateInt.ToString() + " for Date");
                }
                if (dateInt > 99991231)
                {
                    CProgram.sLogError("DecrytYear Invalid value " + dateInt.ToString() + " for Date");
                    year = 9999;
                }
                else
                {
                    try
                    {
                        year = CProgram.sGetYear( (UInt32)dateInt);
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogError("DecrytYear Invalid value " + dateInt.ToString() + " for Date");
                        year = 0;
                    }
                }
            }
            return year;
        }
        public override string ToString()
        {
            //            return "!illegal use of toString!";
            return "!EI=" + mEncrypted.ToString() + "!";      // unfortunatly c# makes an auto ToString
        }

    }
}
