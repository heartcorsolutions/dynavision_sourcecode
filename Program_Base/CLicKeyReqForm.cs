﻿// CLicKeyForm
// by Simon Vlaar 
// can be used by Techmedic Development B.V
// created 28 August 2016
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Program_Base
{
     public partial class CLicKeyReqForm : Form
    {
        private string mHardwareKey;
        public string mLicenseKey;
        public bool mbAccept = false;
        public bool mbLicenseText = false;
        public bool mbInstructText = false;
        public DLicensePC mLicensePc = DLicensePC.SinglePC;
        public string mLicensePath = null;

        Image mLicImage = null;

        public CLicKeyReqForm( string AHardwareKey, string ATitle, string ALicenseKey, DLicensePC ALicensePc = DLicensePC.SinglePC, string ALicensePath = null)
        {
            InitializeComponent();
            mHardwareKey = AHardwareKey == null ? "" : AHardwareKey.Trim();
            textBoxHwKey.Text = mHardwareKey;
            mLicensePc = ALicensePc;
            mLicensePath = ALicensePath;

            string licenseFile = Path.Combine(CProgram.sGetProgDir(), "licinstruct.rtf");

            richTextInstruct.Text = "";
            try
            {
                richTextInstruct.LoadFile(licenseFile);
            }
            catch (Exception)
            {
                richTextInstruct.Text = "";
            }
            mbInstructText = richTextInstruct.Text.Length > 10;

            licenseFile = Path.Combine(CProgram.sGetProgDir(), "license.rtf");

            richTextBoxLicense.Text = "";
            try
            {
                richTextBoxLicense.LoadFile(licenseFile);
            }
            catch ( Exception)
            {
                richTextBoxLicense.Text = "";
            }

            mbLicenseText = richTextBoxLicense.Text.Length > 10;
            if( false == mbLicenseText)
            {
                richTextBoxLicense.Text = "License text file is missing!";
            }
            textBoxLicKey.Text = ALicenseKey;
            if( CLicKeyDev.sbSetCheckLicKey(ALicenseKey))
            {
                CLicKeyDev.sbSetProgramOrgDevice();
                textBoxLicKey.BackColor = Color.LightGreen;
            }
            try
            {
                mLicImage = Image.FromFile(Path.Combine(CProgram.sGetProgDir(), "license.png"));
                panelImage.BackgroundImage = mLicImage;
                if (mLicImage != null) panelImage.Size = new Size( 5 + mLicImage.Width, 10 );
            }
            catch( Exception)
            {
            }

            mUpdateCenterDevice();
            Text = CProgram.sMakeProgTitle(ATitle, true, false);
        }

        private void mUpdateCenterDevice()
        {
            int daysLeft = CLicKeyDev.sGetDaysLeft();
            string center = "Center = " + CLicKeyDev.sGetOrgLabel();
            string device = "Device = " + CLicKeyDev.sGetDeviceID().ToString();
            string dirText = "";

            string hardware = CLicKeyDev.sGetHardwareStr();

            if( hardware.Contains("00000000-00000000-00000000-00000000" ))
            {
                labelSandBox.Text = "No hardware detected, run program outside sandbox container of Anti-virus / in Administration mode!";
            }
            else
            {
                labelSandBox.Text = "";
            }
            if(CLicKeyDev.sbDeviceIsMultiPC())
            {
                device += " MultiPC";
            }
            switch (mLicensePc)
            {
                case DLicensePC.SinglePC:
                    dirText = "1Dir = " + mLicensePath; 
                    break;
                case DLicensePC.MultiPcPrefered:
                    dirText = "mDir = " + mLicensePath;
                    break;
                case DLicensePC.MultiPCAllowed:
                    dirText = "aDir = " + mLicensePath;
                    break;
            }

            labelCenterName.Text = center;
            labelDevice.Text = device;
            labelDir.Text = dirText;

            labelDaysLeft.Text = daysLeft >= 0 ? "Days left on licence = " + daysLeft.ToString()
                            : "License not ok or Expired";
            labelDaysLeft.BackColor = daysLeft > 14 ? Color.LightGreen : (daysLeft < 0 ? Color.Red : Color.Orange);
            string version = "", warn = "";
            switch (CProgram.sGetProgVersionLevel())
            {
                case DProgramVersionLevel.Alpha:
                    version = "ALPHA TEST";
                    warn = "Internal use only!";
                    break;
                case DProgramVersionLevel.Beta:
                    version = "BETA TEST";
                    warn = "Try carefully!";
                    break;
                case DProgramVersionLevel.Demo:
                    version = "DEMO - no save!";
                    warn = "Do not patient info!";
                    break;
            }
            labelVersionLevel.Text = version;
            labelTest2.Text = warn;
        }
        private void button2_Click(object sender, EventArgs e)
        {
            mbAccept = true;
            mLicenseKey = textBoxLicKey.Text;
            Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonClipboard_Click(object sender, EventArgs e)
        {
            uint devID = CProgram.sGetDeviceID();
            string demo = (devID & 0x80000000 ) == 0 ? "" : "D";
            string s = "Name: \r\ne-mail: \r\ntel: +\r\nOraganisation: \r\nIP: \r\nHardware-ID=\r\n";
            devID &= 0x7FFFFFFF;
            s += mHardwareKey;
            s += "\r\ndev= " + demo + devID.ToString() + " org= " + CProgram.sGetOrganisationLabel()
                + " prog= " + CProgram.sGetProgName() + " " + CProgram.sGetProgVersion() 
                + "\r\n"; 

            Clipboard.SetText(s);
            labelCopyClipboard.Visible = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
        }

        private void mSetCheckLicKey( string ALicKey )
        {
            string licKey = CLicKeyDev.sStripText(ALicKey);
                
            textBoxLicKey.Text = licKey;

            if (licKey != null && licKey.Length > 8)
            {
                if (CLicKeyDev.sbSetCheckLicKey(licKey))
                {
                    mUpdateCenterDevice();
                    textBoxLicKey.BackColor = Color.LightGreen;
                }
                else
                {
                    textBoxLicKey.BackColor = Color.LightYellow;
                }
            }
        }

        private void buttonLoadLic_Click(object sender, EventArgs e)
        {
            mSetCheckLicKey(Clipboard.GetText());
        }

        private void buttonAccept_Click(object sender, EventArgs e)
        {
            string licKey = textBoxLicKey.Text;

            try
            {
                if (mLicImage == null)
                {
                    CProgram.sPromptWarning(true, "Licence request", "License image file is missing!");
                }
                else if (false == mbInstructText)
                {
                    CProgram.sPromptWarning(true, "Licence request", "License text file is missing!");
                }
                else if (false == mbLicenseText)
                {
                    CProgram.sPromptWarning(true, "Licence request", "License text file is missing!");
                }
                else if (licKey.Length < 10)
                {
                    CProgram.sPromptWarning(true, "Licence request", "License key is missing!");
                }
                else if (false == CLicKeyDev.sbSetCheckLicKey(licKey))
                {
                    CProgram.sPromptWarning(true, "Licence request", "License key is not correct!");
                }
                else if (false == CLicKeyDev.sbSetProgramOrgDevice())
                {
                    CProgram.sPromptWarning(true, "Licence request", "License key is not compatible!");
                }
                else
                {
                    mLicenseKey = licKey;
                    mbAccept = true;
                    Close();
                }
            }
            catch ( Exception ex)
            {
                CProgram.sLogException( "Test LicKey Failed" , ex );
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void richTextBoxLicense_LinkClicked(object sender, LinkClickedEventArgs e)
        {
            string link = e.LinkText;

            if( link.StartsWith( "http://")
                || link.StartsWith("https://"))
            {
                System.Diagnostics.Process.Start(link);
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void richTextInstruct_LinkClicked(object sender, LinkClickedEventArgs e)
        {
            string link = e.LinkText;

            if (link.StartsWith("http://")
                || link.StartsWith("https://"))
            {
                System.Diagnostics.Process.Start(link);
            }

        }

        private void textBoxHwKey_DoubleClick(object sender, EventArgs e)
        {
            Clipboard.SetText(mHardwareKey);
            labelCopyClipboard.Visible = true;
        }
    }
}
