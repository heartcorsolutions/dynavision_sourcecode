﻿// CProgram 
// by Simon Vlaar 
// can be used by Techmedic Development B.V
// created 1 November August 2016
//
// Set of string values concatenated with a seperator:  ;<val1>;val2;v<val3>;
// The string starts with aseperator so that the exact value can be found 
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Program_Base
{
    public class CStringSet
    {
 
        private string _mStringSet;
        private const char _cSeperatorChar = ';';

        public bool mbIsEmpty()
        {
            return _mStringSet == null || _mStringSet.Length <= 2;
        }
        public bool mbIsNotEmpty()
        {
            return _mStringSet != null && _mStringSet.Length > 2;   // has at least ;;
        }

        public void mSetSet(string ASet)
        {
            if( ASet == null || ASet.Length == 0 )
            {
                _mStringSet = null;
            }
            else
            {
                if( ASet[0] != _cSeperatorChar )    // make sure that the set starts with ;
                {
                    _mStringSet = _cSeperatorChar + ASet;
                }
                else
                {
                    _mStringSet = ASet;
                }
                int len = _mStringSet.Length;
                if( _mStringSet[len-1] != _cSeperatorChar)  // make sure that the set ends with ;
                {
                    _mStringSet += _cSeperatorChar;
                }
            }
        }
        public void mSetSet(CStringSet ASet)
        {
            if (ASet == null )
            {
                _mStringSet = null;
            }
            else
            {
                _mStringSet = ASet._mStringSet;
            }
        }

        public string mGetSet()
        {
            return _mStringSet;
        }
        public void mClear()
        {
            _mStringSet = "";
        }
        public static CStringSet sCreateCopy(CStringSet AFrom)    // makes a new copy
        {
            CStringSet copy = null;

            if (AFrom != null)
            {
                copy = new CStringSet();

                if (copy != null)
                {
                    copy._mStringSet = AFrom._mStringSet;
                }
            }
            return copy;
        }

        public CStringSet mCreateCopy()
        {
            CStringSet copy = new CStringSet();

            if (copy != null)
            {
                copy._mStringSet = _mStringSet;
            }

            return copy;
        }
        public bool mbCopyTo(ref CStringSet ATo)    // makes a new copy
        {
            bool bOk = false;

            if (ATo == null)
            {
                ATo = new CStringSet();
            }
            if (ATo != null)
            {
                ATo._mStringSet = _mStringSet;
                bOk = true;
            }
            return bOk;
        }

        public bool mbCopyFrom(CStringSet AFrom)    // makes a new copy
        {
            bool bOk = false;

            if (AFrom != null)
            {
                _mStringSet = AFrom._mStringSet;
                bOk = true;
            }
            return bOk;
        }
        public bool mbContainsValue(string AValue)
        {
            bool bFound = false;

            if (AValue != null && AValue.Length > 0 && _mStringSet != null && _mStringSet.Length > 0)
            {
                bFound = _mStringSet.IndexOf(_cSeperatorChar + AValue + _cSeperatorChar) >= 0;
            }

            return bFound;
        }

        public UInt16 mCount()
        {
            UInt16 n = 0;
            int len;

            if (_mStringSet != null && (len = _mStringSet.Length) > 1)
            {
                for (int i = 1; i < len; ++i)   // skip first and count ;
                {
                    if (_mStringSet[i] == _cSeperatorChar)
                    {
                        ++n;
                    }
                }
            }
            return n;
        }

        public int mIndexOf(string AValue)
        {
            int index = -1;

            if (AValue != null && AValue.Length > 0 && _mStringSet != null && _mStringSet.Length > 0)
            {
                int pos = _mStringSet.IndexOf(_cSeperatorChar + AValue + _cSeperatorChar);
                if (pos >= 0)
                {
                    index = 0;

                    while (--pos > 0)
                    {

                        if (_mStringSet[pos] == _cSeperatorChar)
                        {
                            ++index;
                        }
                    }
                }
            }
            return index;
        }
        public string mGetValue(UInt16 AIndex)
        {
            string value = null;
 
            int len;

            if (_mStringSet != null && (len = _mStringSet.Length) > 1)
            {
                UInt16 n = 0;
                int i = 0;
                while (n < AIndex && ++i < len)
                {
                    if (_mStringSet[i] == _cSeperatorChar)
                    {
                         ++n;
                    }
                }
                // i is at ';'
                ++i;
                if (n == AIndex)
                {
                    for (int j = i; j < len; ++j)
                    {

                        if (_mStringSet[j] == _cSeperatorChar)
                        {
                            value = _mStringSet.Substring(i, j - i);
                            break;
                        }
                    }
                }
            }
            return value;
        }
        public string mGetShowSet(string AGlue)
        {
            string showString = "";
            UInt16 nSet = mCount();

           for (UInt16 i = 0; i < nSet; ++i)
           {
                if( i > 0 )
                {
                    showString += AGlue;
                }
                showString += mGetValue(i);
            }
            return showString;
        }
        public bool mbRemoveValue(string AValue)
        {
            bool bRemoved = false;
            int lValue, lSet;

            if (AValue != null && (lValue = AValue.Length) > 0 && _mStringSet != null && (lSet = _mStringSet.Length) > 0)
            {
                int pos = _mStringSet.IndexOf(_cSeperatorChar + AValue + _cSeperatorChar);
                if (pos >= 0)
                {
                    string s = "";

                    if (pos > 0)
                    {
                        s = _mStringSet.Substring(0, pos);
                    }
                    pos += lValue+1;
                    if (pos < lSet)
                    {
                        s += _mStringSet.Substring(pos);
                    }
                    _mStringSet = s;
                    bRemoved = true;
                }
            }
            return bRemoved;
        }
        public void mAddValue(string AValue)
        {
            if (AValue != null && AValue.Length > 0)
            {
                string s = AValue + _cSeperatorChar;

                if (_mStringSet == null || _mStringSet.Length <= 0)
                {
                    _mStringSet = _cSeperatorChar + s;
                }
                else if (_mStringSet.IndexOf(_cSeperatorChar + s) < 0)
                {
                    _mStringSet += s;

                }
            }
        }

        public bool mbEquals(CStringSet ASet)
        {
            if (ASet == null || ASet.mbIsEmpty())
            {
                return mbIsEmpty();
            }
            if (mbIsEmpty())
            {
                return false;
            }
            return _mStringSet == ASet._mStringSet;
        }


        public bool mbContainsSet(CStringSet ASet)
        {
            if (ASet == null || ASet.mbIsEmpty())
            {
                return false;
            }
            if (mbIsEmpty())
            {
                return false;
            }

            UInt16 nSet = ASet.mCount();
            string code;
            UInt16 nFound = 0;

            for (UInt16 i = 0; i < nSet; ++i)
            {
                code = ASet.mGetValue(i);
                if (mbContainsValue(code))
                {
                    ++nFound;
                }
                else
                {
                    break;
                }
            }

            return nFound == nSet;
        }
        public void mAddSet(CStringSet ASet)
        {
            if (ASet != null &&  ASet.mbIsNotEmpty())
            {
                if (mbIsEmpty())
                {
                    _mStringSet = ASet._mStringSet;
                }
                else
                {
                    UInt16 nSet = ASet.mCount();
                    string code;

                    for (UInt16 i = 0; i < nSet; ++i)
                    {
                        code = ASet.mGetValue(i);
                        mAddValue(code);
                    }
                }
            }
        }
        public void mSortSet(CStringSet ASortSequenceSet)
        {
            if (ASortSequenceSet != null && false == ASortSequenceSet.mbIsEmpty())
            {
                if (mbIsNotEmpty())
                {
                    string newSet = "" + _cSeperatorChar;
                    UInt16 nSet = ASortSequenceSet.mCount();
                    string code;

                    for (UInt16 i = 0; i < nSet; ++i)
                    {
                        code = ASortSequenceSet.mGetValue(i);
                        if (mbContainsValue(code))      // add codes in order that are also in current stringSet
                        {
                            newSet += code + _cSeperatorChar;
                        }
                    }
                    nSet = mCount();
                    for (UInt16 i = 0; i < nSet; ++i)
                    {
                        code = mGetValue(i);
                        if (false == newSet.Contains(_cSeperatorChar + code + _cSeperatorChar))  // add codes from current set if they are not already there
                        {
                            newSet += code + _cSeperatorChar;
                        }
                    }
                    _mStringSet = newSet;
                }
            }
        }
    }
}