﻿// ReqBoolForm
// by Simon Vlaar 
// can be used by Techmedic Development B.V
// created 10 September 2016
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Program_Base
{
 
    public partial class ReqBoolForm : Form
    {
        public bool mbBoolResult;
        public bool mbBoolOrg;
        public bool mbResult;

        public ReqBoolForm( )
        {
            InitializeComponent();

            mbResult = mbBoolOrg = false; ;
            mbResult = false;

        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            mbResult = false;
            Close();
        }

        private void buttonUndo_Click(object sender, EventArgs e)
        {
            checkBox.Checked = mbBoolOrg;
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            mbBoolResult = checkBox.Checked;
            mbResult = true;

            Close();
            
        }

        public void mInitBool(bool AbStart, string AText )
        {
            checkBox.Text = AText;
            checkBox.Checked = AbStart;
            mbBoolOrg = mbBoolResult = AbStart;
            
            mbResult = false;
        }

        public bool mbGetBoolResult( ref bool  ArbBool)
        {
            if( mbResult )
            {
                ArbBool = checkBox.Checked;
            }
            return mbResult;
        }

        private void ReqBoolForm_Load(object sender, EventArgs e)
        {

        }
    }
}
