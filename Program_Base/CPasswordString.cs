﻿// CPasswordString 
// by Simon Vlaar 
// can be used by Techmedic Development B.V
// created 7 Maart 2018
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Program_Base
{

    public class CPasswordString
    {
        private string _mName;       // name of 
		private string _mCenter;
        private string _mEncrypted;
        private UInt16 _mSize;
		private UInt16 _mPosMask;
		private const Int32 _cGridMask = 0x005555;

		private string _mDebugEncrypt;
		private string _mDebugDecrypt;

		public CPasswordString()
        {
            _mName = "";
			_mCenter = "";
            _mEncrypted = "";

			 _mSize = 0;
			_mPosMask = 0;
		}

		public CPasswordString(string AName, string ACenter, UInt16 AMinSize = 0)
        {
            _mName = AName;
			_mCenter = ACenter;

			_mEncrypted = "";
            mSetSize( AMinSize );
        }

		private void mSetSize( UInt16 ASize )
		{
			_mSize = 16;
			_mPosMask = 0xF;
			while( ASize > _mSize )
			{
				_mSize *= 2;
				_mPosMask <<= 1;
				_mPosMask |= 1;
			}
		}

        public void mClear()
        {
            _mEncrypted = "";
			mSetSize( 0 );
		}

		private UInt32 mCalcSeed()
		{
			UInt32 seed = 0x78451256;
			UInt32 v;

			int i, shift = 303;
			char c;

			for( i = 0; i < _mName.Length; ++i )
			{
				c = _mName[i];
				v = (UInt32)((c & 0x0FF) * shift);
				seed += v;
				shift <<= 1;
				if( 0 != ( i & 1 ))
				{
					shift |= 1;
				}
			}
			shift = 107;
			for (i = 0; i < _mCenter.Length; ++i)
			{
				c = _mCenter[i];
				v = (UInt32)((c & 0x0FF) * shift);
				seed -= v;
				shift <<= 1;
				if (0 == (i & 1))
				{
					shift |= 1;
				}
			}
			if (0 == (seed & 0xFF000000)) seed |= 0x13000000;
			if (0 == (seed & 0x00FF0000)) seed |= 0x00570000;
			if (0 == (seed & 0x0000FF00)) seed |= 0x00005100;
			if (0 == (seed & 0x000000FF)) seed |= 0x000000C2;
			return seed;
		}

        // returns true if value has changed and must be stored

        public bool mbEncrypt(string AString)
        {
			bool bOk = _mName != null && _mName.Length > 0 && _mCenter != null && _mCenter.Length > 0;
			_mEncrypted = "!!error encrypt!!";

            bool bDebug = false;
			_mDebugEncrypt = "";

			try
			{
				if (bOk)
				{
					_mEncrypted = "";

					if (AString != null && AString.Length > 0)
					{
						UInt32 m, seed = mCalcSeed();
						UInt16 i, c, v, n = (UInt16)AString.Length;
						string str = AString;
						UInt32 posXorMask = _mPosMask & seed;
						Int32 iBuffer;
						bool bWideChar = false;
						Int32 checkSum = 0x31827364;

						if (posXorMask == 0)
						{
							posXorMask = (_mPosMask & (seed >> 4));
						}
						if (0 == posXorMask)
						{
							posXorMask = 0x5;
						}
						if (n > _mSize)
						{
							mSetSize( n );
						}
						posXorMask = _mPosMask & seed;
                        if (bDebug) _mDebugEncrypt = "Encrypt(" + AString + ") len= " + n.ToString() + "\r\n";
                        if (bDebug) _mDebugEncrypt += "size=" + _mSize.ToString() + ", posMask = " + _mPosMask.ToString( "X2" ) + ", seed= " + seed.ToString() + ", posXorMask = " + posXorMask.ToString( "X2" ) + ")\r\n";

						while (str.Length < _mSize)
						{
							str += "\t" + _mName + AString + _mCenter;
						}
                        if (bDebug) _mDebugEncrypt += "str= " + str + "\r\n";
						UInt16[] buffer = new UInt16[_mSize];

						for (i = 0; i < _mSize; ++i)
						{
							c = (UInt16)str[i];
							if (0 != (c & 0x00FF00))
							{
								bWideChar = true;
							}
							m = (seed >> (i & 0xF)) & 0x0FF; // only change bottom byte of char
							v = (UInt16)(c ^ m);
							iBuffer = i & 0xFFFF0;		// only shuffle bottom 4 bits
							iBuffer |= (i & 5) << 1;
							iBuffer |= (i >> 1) & 5;
							iBuffer ^= (int)posXorMask;

							iBuffer &= _mPosMask;
							buffer[iBuffer] = v;

                            if (bDebug) _mDebugEncrypt += i.ToString( "D3" ) + ": " + (char)(c) + "=" + c.ToString( "X4" ) + "[" + iBuffer.ToString( "X3" ) + "]=" + v.ToString( "X4" ) + ", m= " + m.ToString( "X2" ) + "\r\n";

							checkSum += v << (iBuffer & 0x0F);
						}
						_mEncrypted = checkSum.ToString("X8") + "-";
						if (bWideChar)
						{
							for (i = 0; i < _mSize; ++i)
							{
								v = buffer[i];
								_mEncrypted += v.ToString( "X4" );
							}
						}
						else
						{
							_mEncrypted += "$";
							for (i = 0; i < _mSize; ++i)
							{
								v = buffer[i];
								if (0 != (v & 0x0FF00))
								{
									CProgram.sLogError( "Error PW Encrypt at " + i.ToString() );
								}
								if( v == 0)
								{
									string aaa = v.ToString();
								}
								_mEncrypted += v.ToString( "X2" );
							}
						}
                        if (bDebug) _mDebugEncrypt += "checksum=" + checkSum.ToString();
                        if (bDebug) _mDebugEncrypt += "encrypted=" + _mEncrypted;
					}
				}
			}
			catch ( Exception ex)
			{
				CProgram.sLogException( "Error PW Encrypt", ex );
				_mEncrypted = "!!ex encrypt!!";
				bOk = false;
			}
			return bOk;
        }

        public bool mbIsEncrypted()
        {
            string decrypted;
            bool bOk = mbDecrypt(out decrypted, true);

            return bOk;
        }
        public string mDecrypt()
        {
            string decrypted = "";

            bool bOk = mbDecrypt(out decrypted, false);

            return bOk ? decrypted : "";
        }

        public bool mbDecrypt( out string ArDecrypted, bool AbTestOnly )
		{
			bool bOk = _mName != null && _mName.Length > 0 && _mCenter != null && _mCenter.Length > 0;
			string decrypted = "!!error decrypt!!";
			string encrypted = _mEncrypted == null ? "" : _mEncrypted.Trim();
            bool bDebug = false;

			_mDebugDecrypt = "";
			try
			{
				if (bOk && encrypted != null && encrypted.Length > 0)
				{
					decrypted = "!!error decrypt!!";
                    bOk = false;

					if (encrypted.Length > 12 && encrypted[8] == '-')
					{
						string chkStr = encrypted.Substring( 0, 8 );
						int chkSum = Convert.ToInt32( chkStr, 16 );

						encrypted = encrypted.Substring( 9 );

						UInt32 m, seed = mCalcSeed();
						UInt16 i, c, v, n = (UInt16)encrypted.Length;
						UInt32 posXorMask = _mPosMask & seed;
						Int32 iBuffer, j;
						Int32 checkSum = 0x31827364;

						bool bWideChar = encrypted[0] != '$';
						Int32 sizeHex = bWideChar ? 4 : 2;
						Int32 jOffset = bWideChar ? 0 : 1;

						if (posXorMask == 0)
						{
							posXorMask = (_mPosMask & (seed >> 4));
						}
						if (0 == posXorMask)
						{
							posXorMask = 0x5;
						}

						i = (UInt16)((n - jOffset) / sizeHex); // Estimated nr of buffer items

						if (i > _mSize)
						{
							mSetSize( i );  // resize buffer so that string can fit
						}
						posXorMask = _mPosMask & seed;
						if( bDebug ) _mDebugDecrypt = "decrypt(" + encrypted + ") + " + n.ToString() + "\r\n";
                        if (bDebug) _mDebugDecrypt += "size=" + _mSize.ToString() + ", posMask = " + _mPosMask.ToString( "X2" ) + ", seed= " + seed.ToString() + ", posXorMask = " + posXorMask.ToString( "X2" ) + ")\r\n";

						UInt16[] buffer = new UInt16[_mSize];
						string str;
						//int k;
						bool bAdd = true;

						for (i = 0; i < _mSize; ++i)
						{
							j = jOffset + (i * sizeHex);
							c = 0;
							v = 0;
							str = "";
							if (j < n)
							{
								str = encrypted.Substring( j, sizeHex );

								try
								{
									v = Convert.ToUInt16( str, 16 );
								}
								catch (Exception)
								{
								}
							}
							checkSum += v << (i & 0x0F);

							buffer[i] = v;
						}

						if (checkSum != chkSum)
						{
                            if (bDebug) _mDebugDecrypt += "Checksum not ok " + chkSum.ToString() + "!=" + checkSum.ToString();
						}
                        else if (AbTestOnly )
                        {
                            // just test checksum
                            bOk = true;
                        }
						else
						{
							decrypted = "";
							for (i = 0; i < _mSize; ++i)
							{
								iBuffer = i & 0xFFFF0;      // only shuffle bottom 4 bits
								iBuffer |= (i & 5) << 1;
								iBuffer |= (i >> 1) & 5;
								iBuffer ^= (int)posXorMask;
								iBuffer &= _mPosMask;
/*
								k = i ^ (int)posXorMask;
								iBuffer = (k & _cGridMask) << 1;
								iBuffer |= (k >> 1) & _cGridMask;
	*/							
								v = buffer[iBuffer];

								m = (seed >> (i & 0xF)) & 0x0FF; // only change bottom byte of char
								c = (UInt16)(v ^ m);

								if (bAdd)
								{
									if (c < 32)
									{
										bAdd = false;  // '\t'or invalid char
									}
									else
									{
										decrypted += (char)c;
									}
								}
                                if (bDebug) _mDebugDecrypt += "[" + i.ToString( "D3" ) + "] =" + (char)(c)  + "= " + c.ToString( "X4" ) + " [" + iBuffer.ToString( "X3" ) + "]=" + v.ToString( "X4" ) + ", m= " + m.ToString( "X2" ) + "\r\n";
							}
                            if (bDebug) _mDebugDecrypt += "decrypted=" + decrypted;
                            bOk = true;
						}
					}
				}
			}
			catch (Exception ex)
			{
				CProgram.sLogException( "Error PW decrypt", ex );
				decrypted = "!!ex decrypt!!";
				bOk = false;
			}
            ArDecrypted = decrypted;
            return bOk;
		}
		public void mbSetEncryptEmpty( string AName)
        {
            _mName = AName;
            _mEncrypted = null;
			mSetSize( 0 );
		}
		public static CPasswordString sMakeCopy(CPasswordString AFrom)    // makes a new copy
        {
            CPasswordString copy = null;

            if (AFrom != null)
            {
                copy = new CPasswordString();

                if (copy != null)
                {
                    copy._mName = AFrom._mName;
                    copy._mCenter = AFrom._mCenter;
                    copy._mSize = AFrom._mSize;
					copy._mPosMask = AFrom._mPosMask;
                    copy._mEncrypted = String.Copy(AFrom._mEncrypted);
                }
            }
            return copy;
        }
   
        public CPasswordString mCreateCopy()
        {
            CPasswordString copy = new CPasswordString();

            if (copy != null)
            {
                copy._mName = _mName;
				copy._mCenter = _mCenter;
				copy._mSize = _mSize;
				copy._mPosMask = _mPosMask;
	            copy._mEncrypted = String.Copy(_mEncrypted);
            }

            return copy;
        }
        public bool mbCopyTo(ref CPasswordString ATo)    // makes a new copy
        {
            bool bOk = false;

            if (ATo == null)
            {
                ATo = new CPasswordString();
            }
            if (ATo != null)
            {
                ATo._mName = _mName;
                ATo._mCenter = _mCenter;
				ATo._mSize = _mSize;
				ATo._mPosMask = _mPosMask;
                ATo._mEncrypted = String.Copy(_mEncrypted);
                bOk = true;
            }
            return bOk;
        }

        public bool mbCopyFrom( CPasswordString AFrom)    // makes a new copy
        {
            bool bOk = false;

            if (AFrom != null)
            {
                _mName = AFrom._mName;
                _mCenter = AFrom._mCenter;
				_mCenter = AFrom._mCenter;
				_mSize = AFrom._mSize;
                _mEncrypted = AFrom._mEncrypted;
                bOk = true;
            }
            return bOk;
        }


        public void mbSetEmpty()
        {
            _mEncrypted = null;
			mSetSize(0);
        }
        public string mGetEncrypted()
        {
            return _mEncrypted;
        }
        public bool mbSetEncrypted(string AEncrypted)
        {
            _mEncrypted = AEncrypted;
            return true;
        }
        public bool mbNotEmpty()
        {
            return _mEncrypted != null && _mEncrypted.Length > 0;
        }
        public bool mbIsEmpty()
        {
            return _mEncrypted == null || _mEncrypted.Length == 0;
        }
        public override string ToString()
        {
            //            return "!illegal use of toString!";
            //return mDecrypt();      // unportunatly c# makes an auto ToString
            return "!PW=" + _mEncrypted + "!";      // unfortunatly c# makes an auto ToString
        }
        public static bool sbCmpSearchString(CPasswordString ATest, string AIncludes)
        {
            bool bIncludes = true;

            if (AIncludes != null && AIncludes.Length > 0)
            {
                bIncludes = ATest != null && false == ATest.mbIsEmpty();

                if (bIncludes)
                {
                    bIncludes = CProgram.sbCmpSearchString(ATest.mDecrypt(), AIncludes);
                }
            }
            return bIncludes;
        }
        public static bool sbCmpSearchString(CPasswordString ATest, CPasswordString AIncludes)
        {
            bool bIncludes = true;

            if (AIncludes != null && false == AIncludes.mbIsEmpty())
            {
                bIncludes = sbCmpSearchString(ATest, AIncludes.mDecrypt());
            }
            return bIncludes;
        }
    }
}
