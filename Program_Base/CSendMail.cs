﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading;

namespace Program_Base
{

    enum DSendMailState
    {
        New,
        Cued,
        Sending,
        Error,
        Succeded,
        Disposed
    }
	public class CSendMail
	{
		public static List<CSendMail> _sSendMailList = null;

		public static string _sMailSmtpServer = "127.0.0.1";
		public static UInt16 _sMailSmtpPort = 25;
		public static bool _sbMailSmtpSsl = true;
		public static string _sMailSmtpUser = null;
		public static CPasswordString _sMailSmtpPassword = null;
		public static UInt32 _sMailSmtpMinTimeSec = 20;
		public static UInt16 _sMailSmtpNrThreads = 1;
		public static bool _sbMailLogAll = false;

		private static SmtpClient _sSmtpClient = null;  // first smtp client
		public static UInt32 _sMailSendCounter = 0;
		public static UInt32 _sMailSendOkCounter = 0;
		public static UInt32 _sMailSendErrorCounter = 0;
		//
		public string _mMailLabel = null;
		public UInt32 _mMailSeqNr = 0;
		DSendMailState _mState;

		private Thread _mThread = null;

		public DateTime _mCreatedUTC;
		public DateTime _mCuedUTC;
		public DateTime _mStartSendUTC;
		public UInt32 _mMaxTimeSec;
		public DateTime _mEndSendUTC;
		public string _mErrorResult;
		public string _mSendResult;
		public bool _mbLogDone = false;

		public MailMessage _mMailMessage = null;

		public CSendMail( string AMailLabel, MailMessage AMailMessage, UInt32 AMaxTimeSec )
		{
			mClear();
			_mMailLabel = AMailLabel;
			_mMailMessage = AMailMessage;
			_mMaxTimeSec = _sMailSmtpMinTimeSec + AMaxTimeSec;
			_mCreatedUTC = DateTime.UtcNow;
			_mMailSeqNr = ++_sMailSendCounter;

		}

		private void mClear()
		{
			_mMailLabel = null;
			_mState = DSendMailState.New;

			_mThread = null;
			_mCreatedUTC = DateTime.MinValue;
			_mStartSendUTC = DateTime.MinValue;
			_mMaxTimeSec = _sMailSmtpMinTimeSec + 120;
			_mEndSendUTC = DateTime.MinValue;
			_mErrorResult = "";
			_mSendResult = "";
			_mbLogDone = false;
			_mMailMessage = null;
		}

		public void mDoSendMail()
		{
            SmtpClient smtpClient = null;

            try
			{
				if (_mMailMessage == null )
				{
					_mErrorResult += "No data!";
					_mState = DSendMailState.Error;
				}
				else if (_mState != DSendMailState.New && _mState != DSendMailState.Cued)
				{
					_mErrorResult += "Wrong state to start mailing!";
					_mState = DSendMailState.Error;

				}
				else
				{
                    smtpClient = sCreateSmtpClient();

                    if (smtpClient != null)
                    {


                        _mStartSendUTC = DateTime.UtcNow;
                        _mState = DSendMailState.Sending;
                        smtpClient.Send(_mMailMessage);
                        _mEndSendUTC = DateTime.UtcNow;
                        _mState = DSendMailState.Succeded;
                    }
					_mSendResult = "OK";
				}
			}
			catch (Exception ex)
			{
				_mSendResult += "Error";
				_mErrorResult += ex.ToString();
				_mState = DSendMailState.Error;
			}
            try
            {
                if (smtpClient != null)
                {
                    smtpClient.Dispose();
                }
            }
            catch (Exception ex)
            {
            }

        }
        public void mDisposeData()
		{

			try
			{
				if (_mThread != null)
				{
					if (_mThread.IsAlive)
					{
						_mThread.Abort();
					}
					_mThread = null;
				}
				if (_mMailMessage != null)
				{
					foreach (Attachment attachment in _mMailMessage.Attachments)
					{
						attachment.Dispose();
					}

					_mMailMessage.Dispose();
					_mMailMessage = null;

				}
				_mState = DSendMailState.Disposed;
			}
			catch (Exception ex)
			{
				CProgram.sLogException( "Dispose CSendData", ex );
			}
		}

		public bool mbStartThread()
		{
			bool bOk = false;

			try
			{
				if (_mThread != null)
				{
					CProgram.sLogError( "Aready started sending mail: " + _mMailLabel + " " + _mState.ToString() );
				}
				else
				{
					_mThread = new Thread( new ThreadStart( this.mDoSendMail ) );

					if (_mThread != null)
					{
						_mState = DSendMailState.Cued;
						//					if (AbStartNow)
						{
							_mThread.Start();
						}
						bOk = true;
					}
				}
			}

			catch (Exception ex)
			{
				_mSendResult += "Error Start";
				_mErrorResult += ex.ToString();
				_mState = DSendMailState.Error;
			}
			return bOk;
		}

		public bool mbStopThread( string AReason )
		{
			bool bOk = false;
			try
			{
				if (_mThread != null)
				{
					if (_mThread.IsAlive)
					{
						_mThread.Abort();
						_mSendResult += AReason + " Abort";
						_mState = DSendMailState.Error;
					}
				}
				bOk = true;
			}
			catch (Exception ex)
			{
				_mSendResult += "Error Stop " + AReason;
				_mErrorResult += ex.ToString();
				_mState = DSendMailState.Error;
			}
			return bOk;
		}
		public bool mbCheckSendTime( DateTime AUtcNow )
		{
			bool bOk = _mState == DSendMailState.Sending;

			if (bOk)
			{
				if (_mStartSendUTC != DateTime.MinValue)
				{

					double t = (AUtcNow - _mStartSendUTC).TotalSeconds;

					if (t > _mMaxTimeSec)
					{
						mbStopThread( "Timeout " + t.ToString( "0.0" ) );
						bOk = false;
					}
				}
				else
				{
					mbStopThread( "start time" );
					bOk = false;
				}
			}
			return bOk;
		}
		/*        public bool mbIsTreadFinished()
				{
					bool bOk = false;
					try
					{
						if( _mState == DSendMailState.)
					}

					catch (Exception ex)
					{
						CProgram.sLogException( "", ex );
					}
					return bOk;

				}
		*/
		public void mbLogState( string AHeader, bool AbForceLog )
		{
			if (AbForceLog || _sbMailLogAll)
			{
				string s = AHeader + ": Mail" + _mMailSeqNr.ToString() + "{" + _mMailLabel + ":" + _mState.ToString();

				if (_mCreatedUTC != DateTime.MinValue)
				{
					double t = (DateTime.UtcNow - _mCreatedUTC).TotalSeconds;

					s += ", age " + t.ToString( "0.000" ) + "sec";
				}
				CProgram.sLogLine( s + "}" );
			}
		}
		public void mbLogResultTread( string AHeader, bool AbLogError, bool AbOnce = true )
		{
			if (false == _mbLogDone || false == AbOnce || _sbMailLogAll)
			{
				string s = AHeader + ": Mail" + _mMailSeqNr.ToString() + "{" + _mMailLabel + ":" + _mState.ToString();

				if (_mStartSendUTC != DateTime.MinValue)
				{
					if (_mEndSendUTC != DateTime.MinValue)
					{
						double t = (_mEndSendUTC - _mStartSendUTC).TotalSeconds;

						s += ", end " + t.ToString( "0.000" ) + "sec";
					}
					else
					{
						double t = (DateTime.UtcNow - _mStartSendUTC).TotalSeconds;

						s += ", now " + t.ToString( "0.000" ) + "sec";
					}
				}
				else if (_mCreatedUTC != DateTime.MinValue)
				{
					double t = (DateTime.UtcNow - _mCreatedUTC).TotalSeconds;

					s += ", cued " + t.ToString( "0.000" ) + "sec";
				}

				bool bLog = _mSendResult != null && _mSendResult.Length > 0;

				if (bLog)
				{
					s += ", " + _mSendResult;
				}

				if (_mErrorResult != null && _mErrorResult.Length > 0 && AbLogError)
				{
					bLog = true;
					s += ", err " + _mErrorResult;
				}
				if (bLog || false == AbOnce || _sbMailLogAll)
				{
					s += "}";
					if (_mState == DSendMailState.Error)
					{
						CProgram.sLogError( s );
					}
					else
					{
						CProgram.sLogLine( s );
					}
					_mbLogDone = true;
				}
			}
		}

		// static management functions 

		public static void sDisableSmtpServer()
		{
			_sMailSmtpServer = null;
			_sMailSmtpPort = 25;
			_sbMailSmtpSsl = true;
			_sMailSmtpUser = null;
			_sMailSmtpPassword = null;
			_sMailSendOkCounter = 0;
			_sMailSendErrorCounter = 0;
		}

		//
		public static bool sbSetSmtpServer( string ASmtpServer, UInt16 ASmtpPort, bool AbUseSsl, string ASmtpUser,
			CPasswordString ASmtpPassword, UInt32 ASmtpMinTimeSec, UInt16 ANrThreads )
		{
			bool bOk = false;
			sDisableSmtpServer();

			if (ASmtpServer != null && ASmtpServer.Length > 2 && ASmtpPort > 0)
			{
				if (ASmtpPassword == null || ASmtpPassword.mbIsEmpty())
				{
					bOk = true;
				}
				else
				{
					string decrypted;
					if (ASmtpPassword.mbDecrypt( out decrypted, false ))
					{
						bOk = true;
					}
				}
				if (bOk)
				{
					_sMailSmtpServer = ASmtpServer;
					_sMailSmtpPort = ASmtpPort;
					_sbMailSmtpSsl = AbUseSsl;
					_sMailSmtpUser = ASmtpUser;
					_sMailSmtpPassword = ASmtpPassword;
					_sMailSmtpMinTimeSec = ASmtpMinTimeSec;
					_sMailSmtpNrThreads = ANrThreads < 1 ? (UInt16)1 : ANrThreads;
					bOk = true;
				}
			}
			return bOk;
		}
		public static bool sbCheckSmtpClient()
		{
            bool bOk = _sSmtpClient != null;

			if (bOk == false && _sMailSmtpServer != null && _sMailSmtpServer.Length > 0)
			{
				try
				{

					SmtpClient smtpClient = new SmtpClient( _sMailSmtpServer );

					if (smtpClient != null)
					{
						smtpClient.EnableSsl = _sbMailSmtpSsl;
						smtpClient.Port = _sMailSmtpPort;
						if (_sMailSmtpUser != null && _sMailSmtpUser.Length > 0 && _sMailSmtpPassword != null)
						{
                            string pw;

                            if( false == _sMailSmtpPassword.mbDecrypt(out pw, false))
                            {
                                CProgram.sLogError("smtp password decrypt error");
                            }
                            smtpClient.Credentials = new
								   NetworkCredential( _sMailSmtpUser, pw );
						}
					}
					_sSmtpClient = smtpClient;

					if (_sSendMailList == null)
					{
						_sSendMailList = new List<CSendMail>();
					}
					bOk = smtpClient != null && _sSendMailList != null;
				}
				catch (Exception ex)
				{
					CProgram.sLogException( "Create SmtpClient", ex );
				}
			}
			return bOk;
		}
        
        public static SmtpClient sCreateSmtpClient()
        {
            SmtpClient smtpClient = null;

            if (_sMailSmtpServer != null && _sMailSmtpServer.Length > 0)
            {
                try
                {

                    smtpClient = new SmtpClient(_sMailSmtpServer);

                    if (smtpClient != null)
                    {
                        smtpClient.EnableSsl = _sbMailSmtpSsl;
                        smtpClient.Port = _sMailSmtpPort;
                        if (_sMailSmtpUser != null && _sMailSmtpUser.Length > 0 && _sMailSmtpPassword != null)
                        {
                            string pw;

                            if (false == _sMailSmtpPassword.mbDecrypt(out pw, false))
                            {
                                CProgram.sLogError("smtp password decrypt error");
                            }
                            smtpClient.Credentials = new
                                   NetworkCredential(_sMailSmtpUser, pw);
                        }
                    }
                    if (_sSendMailList == null)
                    {
                        _sSendMailList = new List<CSendMail>();
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Create SmtpClient", ex);
                }
            }
            return smtpClient;
        }

        public static bool sbStartSendMail( string AMailLabel, MailMessage AMailMessage, bool AbStartNow, UInt32 AMaxTimeSec, bool AbRunDirect = false )
		{
			bool bOk = AMailMessage != null;

			if (bOk)

			{
				CSendMail newMail = null;
				try
				{
                    if (AMailLabel == null || AMailLabel.Length == 0 || AMailMessage == null)
                    {
                        CProgram.sLogError("Start Send Mail called with null values: " + AMailLabel);
                    }
                    else if (sbCheckSmtpClient())
					{
						newMail = new CSendMail( AMailLabel, AMailMessage, AMaxTimeSec );

						if (newMail != null)
						{

							if (AbRunDirect)
							{
								newMail.mbLogState( "Created Direct", false );
								newMail.mDoSendMail();
								newMail.mbLogResultTread( "Run", true, false );
								bOk = newMail._mState == DSendMailState.Succeded;
								newMail.mDisposeData();
							}
							else if (AbStartNow)
							{
								newMail.mbLogState( "Created Run", false );
								bOk = newMail.mbStartThread();
							}
							else
							{
								newMail._mState = DSendMailState.Cued;
								newMail.mbLogState( "Created Cue", false );
								bOk = true; // in cue
							}

                            _sSendMailList.Add(newMail);
                        }
					}
					else
					{
						CProgram.sLogError( "Mail server not setup for sending mail: " + AMailLabel );
					}
				}
				catch (Exception ex)
				{
					CProgram.sLogException( "StartSendMail", ex );
				}
			}
			return bOk;
		}

        public static bool sbProcessMailCue(out UInt32 ArNrMailsCued, out UInt32 ArNrMailsSending, out UInt32 ArNrMailsRemoved)
        {
            bool bOk = false;
            UInt32 nrMailsCued = 0, nrMailsSending = 0, nrMailsRemoved = 0;

            if (_sSendMailList != null)
            {
                try
                {
                    DateTime utc = DateTime.UtcNow;
                    CSendMail mail, firstNewMail = null;
                    int n = _sSendMailList.Count;
                    int i = 0;
                    double t;

                    // processing mails in cue;
                    while (i < n)
                    {
                        mail = _sSendMailList[i++];

                        if (mail != null)
                        {
                            switch (mail._mState)
                            {
                                case DSendMailState.New:
                                    if (firstNewMail == null)
                                    {
                                        firstNewMail = mail;
                                    }
                                    break;
                                case DSendMailState.Cued:
                                    if (firstNewMail == null)
                                    {
                                        firstNewMail = mail;
                                    }
                                    break;
                                case DSendMailState.Sending:
                                    if (mail.mbCheckSendTime(utc))
                                    {
                                        ++nrMailsSending;
                                    }
                                    break;
                                case DSendMailState.Error:
                                    ++_sMailSendErrorCounter;
                                    mail.mbLogResultTread("Processing", true, true);
                                    mail.mDisposeData();
                                    break;
                                case DSendMailState.Succeded:
                                    ++_sMailSendOkCounter;
                                    mail.mbLogResultTread("Processing", true, true);
                                    mail.mDisposeData();
                                    break;
                                case DSendMailState.Disposed:
                                    _sSendMailList.Remove(mail);
                                    mail.mbLogState("Processing", false);
                                    --n;
                                    --i;
                                    ++nrMailsRemoved;
                                    break;
                            }
                        }
                    }
                    nrMailsCued = (UInt32)_sSendMailList.Count;
                    bOk = nrMailsCued > 0;
                    if (nrMailsSending < _sMailSmtpNrThreads && nrMailsCued > 0)
                    {
                        if (firstNewMail != null)
                        {
                            firstNewMail.mbStartThread();
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Process Mail Cue", ex);
                }

            }
            ArNrMailsCued = nrMailsCued;
            ArNrMailsSending = nrMailsSending;
            ArNrMailsRemoved = nrMailsRemoved;
            return bOk;
        }
        public static void sLogMailCue()
        {
            string logText = "Mail cue: ";

            logText += "nTot=" + _sMailSendCounter.ToString();
            logText += ", OK=" + _sMailSendOkCounter.ToString();
            logText += ", Err=" + _sMailSendErrorCounter.ToString();
            UInt32 nrMailsWait = 0, nrMailsSending = 0, nrMailsDone = 0;

            if (_sSendMailList != null)
            {
                try
                {
                    foreach (CSendMail mail in _sSendMailList )
                    { 
                        if (mail != null)
                        {
                            switch (mail._mState)
                            {
                                case DSendMailState.New:    ++nrMailsWait;  break;
                                case DSendMailState.Cued:   ++nrMailsWait; break;
                                case DSendMailState.Sending: ++nrMailsSending; break;
                                case DSendMailState.Error: ++nrMailsDone; break;
                                case DSendMailState.Succeded: ++nrMailsDone; break;
                                case DSendMailState.Disposed: ++nrMailsDone; break;
                            }
                        }
                    }
                    logText += ", wait=" + nrMailsWait.ToString();
                    logText += ", sending=" + nrMailsSending.ToString();
                    logText += ", done=" + nrMailsDone.ToString();
                    logText += ", in cue=" + (nrMailsWait+ nrMailsSending+ nrMailsDone).ToString();
                    CProgram.sLogLine(logText);
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Log Process Mail Cue", ex);
                }
            }
        }
        public static UInt32 sKillMailCue()
		{

			UInt32 nrMailsRemoved = 0;

			if (_sSendMailList != null)
			{
				try
				{
					CSendMail mail;
					int n = _sSendMailList.Count;
					int i = 0;

					// processing mails in cue;
					while (i < n)
					{
						mail = _sSendMailList[i];

						if (mail != null)
						{
							mail.mbStopThread( "Kill" );
							mail.mbLogResultTread( "Killing", true, true );
							mail.mDisposeData();
							++nrMailsRemoved;
						}
					}
				}
				catch (Exception ex)
				{
					CProgram.sLogException( "Kill Mail Cue", ex );
				}
			}
			return nrMailsRemoved;
		}

		public bool _mbMailEventReceivedEnable = false;
		public string _mMailEventReceivedFrom = "";
		public string _mMailEventReceivedTo = "";
		public string _mMailEventReceivedSubject = "";
		public string _mMailEventReceivedBody = "";

		public bool mbMailSetup( string ACenter )
		{
			bool bRun = false;

			try
			{
				_mbMailEventReceivedEnable = false; // get values from settings file

				if (_mbMailEventReceivedEnable)
				{
					// get values from settings file
					_mMailEventReceivedFrom = "";
					_mMailEventReceivedTo = "";
					_mMailEventReceivedSubject = "";
					_mMailEventReceivedBody = "";

					if (_mMailEventReceivedTo == null || _mMailEventReceivedTo.IndexOf( '@' ) < 0 || _mMailEventReceivedFrom == null || _mMailEventReceivedFrom.IndexOf( '@' ) < 0)
					{
						CProgram.sLogLine( "Invalid mail adres" );
					}
					else
					{
						string mailSmtpServer = "127.0.0.1"; // from settings file
						UInt16 mailSmtpPort = 25;
						bool bMailSmtpSsl = true;
						string mailSmtpUser = null;
						string mailSmtpPassword = null;
						UInt32 mailSmtpMinTimeSec = 20;
						UInt16 mailSmtpNrThreads = 1;

						CPasswordString smtpPW = null;
						if (mailSmtpPassword != null && mailSmtpPassword.Length > 0)
						{
							string name = mailSmtpUser.Trim() + "@" + mailSmtpServer.Trim() + ":smtpPW";

							smtpPW = new CPasswordString( name, ACenter );
							smtpPW.mbSetEncrypted( mailSmtpPassword );
						}
						bRun = CSendMail.sbSetSmtpServer( mailSmtpServer, mailSmtpPort, bMailSmtpSsl, mailSmtpUser, smtpPW, mailSmtpMinTimeSec, mailSmtpNrThreads );
						if (false == bRun)
						{
							CProgram.sLogLine( "Failed setup smtp client!" );
						}
					}

				}
			}
			catch (Exception ex)
			{
				CProgram.sLogException( "Setup Mail", ex );
			}
			return bRun;
			// if( mbMailSetup( string ACenter )) start timer for processing mail cue
		}
		public bool mbMailSendOne( string ALabel, string ATitle, string ABody, List<string> AFileList, bool AbStartDirectly )
		{
			bool bOk = false;

			if (false == _mbMailEventReceivedEnable)
			{
				try
				{

					if (CSendMail.sbCheckSmtpClient())
					{
						string mailTitle = ATitle == null || ATitle.Length == 0
							? "Test" + "( " + DateTime.Now.ToString( "HH:mm:ss" ) + ")" : ATitle;
						string mailBody = ABody + "\r\n\r\n<<<< " + CProgram.sGetProgNameVersionNow() + " >>>>>>>>";

						MailMessage message = new MailMessage( new MailAddress( _mMailEventReceivedFrom, "" ), new MailAddress( _mMailEventReceivedTo, "" ) );

						message.Sender = new MailAddress( _mMailEventReceivedFrom, "" );
						message.Subject = mailTitle;
						message.Body = mailBody;
						message.IsBodyHtml = false;

						if (AFileList != null)
						{
							foreach (string fileName in AFileList)
							{
								Attachment attachment = new Attachment( fileName );
								message.Attachments.Add( attachment );
							}
						}
						if (false == CSendMail.sbStartSendMail( ALabel, message, AbStartDirectly, 0, false ))
						{
							CProgram.sLogLine( "Failed send test mail!" + ALabel );
						}

					}
				}
				catch (Exception ex)
				{
					CProgram.sLogException( "Send Mail: " + ALabel, ex );
				}
				
			}
			return bOk;
		}
		// 
	}

#if jfgh

		private void buttonMSendMail_Click( object sender, EventArgs e )
		{
			string smtpServer = textBoxSmtpHost.Text;
			UInt16 smtpPort = 0;
			bool bUseSsl = checkBoxSmtpSsl.Checked;
			string smtpUser = textBoxSmtpUser.Text;
			string smtpPassword = textBoxSmtpPW.Text;
			UInt32 smtpMinTimeSec = 20;
			string center;
			string mailTo = textBoxMailTo.Text;
			string mailFrom = textBoxMailFrom.Text;
			string mailTitle = textBoxMailTitle.Text;
			string mailBody = textBoxMailBody.Text;

			try
			{
				CSendMail._sbMailLogAll = true;
				if (false == CLicKeyDev.sbFormatOrgLabel( out center, comboBoxCenters.Text ))
				{
					CProgram.sLogLine( "Select Center first!" );
				}
				else if (UInt16.TryParse( textBoxSmtpPort.Text, out smtpPort ))
				{
					CPasswordString smtpPW = null;
					if (smtpPassword != null && smtpPassword.Length > 0)
					{
						smtpPW = new CPasswordString( "smtp", center );
						smtpPW.mbEncrypt( smtpPassword );
					}
					if (false == CSendMail.sbSetSmtpServer( smtpServer, smtpPort, bUseSsl, smtpUser, smtpPW, smtpMinTimeSec ))
					{
						CProgram.sLogLine( "Failed setup smtp client!" );
					}
					else if( mailTo == null || mailTo.IndexOf('@') < 0 || mailFrom == null || mailFrom.IndexOf('@')< 0)
						{
						CProgram.sLogLine( "Invalid mail adres" );
					}
					else
					{
						string testLabel = "Test"+"( " + DateTime.Now.ToString( "HH:mm:ss" ) + ")";
						mailTitle += " (" + DateTime.Now.ToString( "HH:mm:ss" ) + ")";
						mailBody += "\r\nTest mail send from " + CProgram.sGetProgNameVersionNow();
					
						MailMessage message = new MailMessage( new MailAddress( mailFrom, "" ), new MailAddress( mailTo, "" ));

						message.Sender = new MailAddress( mailFrom, "" );
						message.Subject = mailTitle;
						message.Body = mailBody;
						message.IsBodyHtml = false;
						if( checkBoxMailAttach1.Checked)
						{
							string fileName = textBoxMailFile1.Text;

							Attachment attachment = new Attachment( fileName );
							message.Attachments.Add( attachment );
						}
						if (checkBoxMailAttach2.Checked)
						{
							string fileName = textBoxMailFile2.Text;

							Attachment attachment = new Attachment( fileName );
							message.Attachments.Add( attachment );
						}

						if (false == CSendMail.sbStartSendMail( testLabel, message, false, 0, false ))
						{
							CProgram.sLogLine( "Failed send test mail!" );
						}
						else
						{

							CProgram.sLogLine( "Sending mail, enabled proccessing SMTP cue" );
							timerProcessSmtp.Enabled = true;
						}
						//sbStartSendMail( string AMailLabel, MailMessage AMailMessage, bool AbStartNow, UInt32 AMaxTimeSec, bool AbRunDirect = false );
					}
				}

				else
				{
					CProgram.sLogLine( "Missing smtp port!" );
				}
			}
			catch (Exception ex)
			{
				CProgram.sLogException( "SendMail test", ex );
			}

		}




			private void buttonKillCue_Click( object sender, EventArgs e )
		{
			UInt32 n = CSendMail.sKillMailCue();

			mDoProcessSmtpCue();
		}

			    private void mDoProcessSmtpCue()
		{
			UInt32 cueN = 0, sendN = 0, clearN = 0;

			if (CSendMail.sbProcessMailCue( out cueN, out sendN, out clearN ))
			{
				labelSmtpCueN.Text = cueN.ToString();
				labelSmtpSendN.Text = sendN.ToString();
				labelSmtpClearN.Text = clearN.ToString();
			}
			else
			{
				labelSmtpCueN.Text = cueN == 0 ? "-" : cueN.ToString();
				labelSmtpSendN.Text = sendN == 0 ? "-" : sendN.ToString();
				labelSmtpClearN.Text = clearN == 0 ? "-" : clearN.ToString();
			}
		}

#endif
}
