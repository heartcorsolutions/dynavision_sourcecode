﻿// CLicKey
// by Simon Vlaar 
// can be used by Techmedic Development B.V
// created 24 August 2016
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management;
using System.Security.Cryptography;
using System.Security;
using System.Collections;
using System.IO;

namespace Program_Base
{

    // licence key request
    // CC1234NNNN-DDDDDDDDD-$CPUID0$-$BIOSID$-$BaSEID$-$DISK00$-PC-NAME-CHECKSUM
    // X = unknown      $hex 32 bit values checksum1 = 32 bit hex value
    // license key responce
    // CC1234NN-DDDDDD-$valid$-YYMMDD-$servip$-$password$-

        // old idea
    // login procedure:(1= now, 2 later, 3 splitting servers)
    // 1:load the device license file from progdata dir (lic file and log files)
    //                                      progdata dir from program settings
    // 1:determin hardware ID
    // 1:check checksums  [if not acquire new license key]
    // 2:(check ip range+time on server)
    // 1:set device ID and Organisation label
    // 2:load file.lks server info for file / FTP server settings (if not use program settings)
    // 2:load organisation.lko from <filedir>\RO\  (contains org label, hash, hash table, check org)
    // 2:ask for user ID and name + PIN-pw
    // 2:load <org>-<userid>-name.lku (PIN + UserGroup)
    // 2:check PIN-pw
    // 2:set userGroup  [ for now use name from settings file
    // 1:load \FileGroup\<org>-userGroup-file.lks for new file server and reconnect
    // 2:check file server time
    // 1:load \UserGroupD\<org>-userGroup-dbase.lks server info   
    // 1:check dbase server time
    // 2:load file server configs for intricon and TZ import when needed [for now use program settings]

    public enum DLicensePC
    {
        SinglePC,
        MultiPcPrefered,
        MultiPCAllowed
    };


    class CLicFileStream        // not used jet, use settings file
    {
        private Stream _mStream = null;
        private UInt32 _mHash;
        private UInt32 _mIndex;
        private UInt16 _mXorMask;
        private UInt16 _mAddMask;
        private bool _mbOk = false;
        private UInt32 _mFileSize;
        private bool _mbEncrypt;    // encrypt

        CLicFileStream( Stream AStream, bool AbUseOrganisation, UInt32 ASecondHash )
        {
            _mStream = AStream;
            _mHash = AbUseOrganisation ? CLicKeyOrg.sGetOrganisationHash() : CProgram.sGetProgramHash();

            _mIndex = _mFileSize = 0;
            _mXorMask = (UInt16)(_mHash >> 16);
            _mAddMask = (UInt16)(_mHash & 0x0FFFF);
            _mbOk = AStream != null;
            _mbEncrypt = false;     // at least the first 4 header bytes are not encrypted
        }
        public byte mReadByte()
        {
            uint b8 = (byte)0x0FF;
            try
            {
                int i = _mStream.ReadByte();
                if (i < 0)
                {
                    _mbOk = false;
                }
                else
                {
                    if( _mIndex == 0)
                    {
                        _mFileSize = (UInt32)_mStream.Length;
                    }

                    int j = (int)(_mIndex & 0x07);

                    b8 &= 0x0FF;

                    _mXorMask ^= (UInt16)(b8 << (8 - j));
                    _mAddMask ^= (UInt16)(b8 << (8 - j));

                    if (_mbEncrypt)
                    {
                        j = (int)(_mIndex % 24);
                        if ((j & 1) != 0) j = 24 - j;
                        UInt32 m = _mHash >> j;
                        b8 ^= m;
                    }
                    ++_mIndex;
                }

            }
            catch
            {
                _mbOk = false;
            }

            return (byte)(b8 & 0x0FF);
        }
        public UInt16 mReadUInt16()
        {
            UInt16 b0 = mReadByte();
            UInt16 b1 = mReadByte();

            b0 &= 0x0FF;
            b1 <<= 8;   b1 &= 0x0FF00;

            return (UInt16)( b0 | b1 );
        }
        public UInt32 mReadUInt32()
        {
            UInt32 b0 = mReadByte();
            UInt32 b1 = mReadByte();
            UInt32 b2 = mReadByte();
            UInt32 b3 = mReadByte();

            b0 &= 0x0FF;
            b1 <<= 8; b1 &= 0x0FF00;
            b2 <<= 16; b2 &= 0x0FF0000;
            b3 <<= 24; b3 &= 0x0FF000000;

            return (UInt16)(b0 | b1 | b2 | b3);
        }
        public string mReadString()
        {
            string s = "";
            byte b, n = mReadByte();

            for( int i = 0; i < n; ++i)
            {
                b = mReadByte();
                if( b > 0)
                {
                    s += (char)b;
                }
            }
            return s;
        }
        public bool mbReadCheckSum()
        {
            if( _mIndex != _mFileSize - 8)
            {
                _mbOk = false;
            }
            else
            {
                UInt32 size = mReadUInt32();
                UInt16 xor = _mXorMask;
                UInt16 add = _mAddMask;
                UInt16 xorFile = mReadUInt16();
                UInt16 addFile = mReadUInt16();

                _mbOk &= size != _mFileSize && xor == xorFile && add == addFile; 
            }
            return _mbOk;
        }

        public bool mbWriteByte( byte AByte)
        {
            try
            {
                uint b8 = AByte;
                int j;

                if (_mbEncrypt)
                {
                    j = (int)(_mIndex % 24);
                    if ((j & 1) != 0) j = 24 - j;
                    UInt32 m = _mHash >> j;
                    b8 ^= m;
                }
                j = (int)(_mIndex & 0x07);
                b8 &= 0x0FF;

                _mXorMask ^= (UInt16)(b8 << (8 - j));
                _mAddMask ^= (UInt16)(b8 << (8 - j));

                _mStream.WriteByte((byte)b8);
            }
            catch
            {
                _mbOk = false;
            }
            return _mbOk;
        }
        public bool mbWriteUInt16( UInt16 AInt )
        {
            UInt32 i = AInt;
            bool b = mbWriteByte((byte)(i & 0x0FF));

            i >>= 8; b &= mbWriteByte((byte)(i & 0x0FF));
            return b;
         }
        public bool mbWriteUInt32(UInt32 AInt)
        {
            UInt32 i = AInt;
            bool b = mbWriteByte((byte)(i & 0x0FF));

            i >>= 8; b &= mbWriteByte((byte)(i & 0x0FF));
            i >>= 8; b &= mbWriteByte((byte)(i & 0x0FF));
            i >>= 8; b &= mbWriteByte((byte)(i & 0x0FF));
            return b;
        }
        public bool mbWriteString( string AString )
        {
            int n = AString == null ? 0 : AString.Length;

            mbWriteByte((byte)n);
            for( int i = 0; i < n; ++i )
            {
                mbWriteByte((byte)AString[i]);
            }
            return _mbOk;
        }
        public bool mbWriteChecksum()
        {
            if( _mbOk)
            {
               if( mbWriteUInt32( _mIndex + 8 ))
               {
                    UInt16 xor = _mXorMask;
                    UInt16 add = _mAddMask;

                    mbWriteUInt16(xor);
                    mbWriteUInt16(add); 
                }
            }
            return _mbOk;
        }
        public bool mbIsOk()
        {
            return _mbOk;
        }
    }

       public class CLicKeyOrg
    {
        private static string _sOrganisationLabel = "X";
        private static UInt32 _sOrganisationHash = 0;
//        private static UInt16 _sOrganisationTableSize = 0;
//        private static byte[] _sOrganisationHashTable = null;
//        private static UInt16 _sOrganisationValidYMD = 0;
//        private static UInt32 _sChecksum = 0;
        public static UInt32 sGetOrganisationHash()
        {
            UInt32 hash = _sOrganisationHash + CProgram.sGetProgramHash();
            int n = _sOrganisationLabel.Length;

            for (int i = 0; i < n; ++i)
            {
                char c = _sOrganisationLabel[i];

                hash += (UInt32)((c & 0x0FF) << (i % 24));
            }
            return hash;
        }
    }

    public class CLicKeyDev         // license key for device (from this the organisation key is derived
    {

        // Usage: see Cprogram example
        private static string _sOrganisationLabel = "";// CC1234NNNN
        private static UInt16 _sOrganisationNr = 0;
        private static UInt32 _sDeviceID = 0;
        private static string _sHardwareStr = "";   // $CPUID0$-$BIOSID$-$BaSEID$-$DISK00$-PC-NAME
        private static Int32 _sDeviceValidYMD = 0;
        private static UInt16 _sUsedWarnDays = 0;
        private static DLicensePC _sUsedLicensePc = DLicensePC.SinglePC;
        private static string _sUsedLicensePath = null;

        //        private static string _sHwChecksum = "";
        public const UInt32 _cNrDevPerOrg = 10000;
        public const UInt16 _cDevIdWeb = 9999;
        public const UInt16 _cDevIdMultiPC = 9988;
        public const UInt16 _cDevIdMultiBase = 1000;
        public const UInt16 _cNrMultiPC = 8000;

        private static string _sPcHwID = null;  // this pc hardware id

        public static bool sbSaveLicKey(string AFullFileName, CEncryptedString ArHardwareKey, CEncryptedString ArLicenseKey)
        {
            bool bOk = false;

            try
            {
                using (StreamWriter sw = new StreamWriter(AFullFileName))
                {

                    if (sw != null)
                    {
                        sw.WriteLine("hwID=" + ArHardwareKey.mGetEncrypted());
                        sw.WriteLine("lic=" + ArLicenseKey.mGetEncrypted());
                        sw.Close();
                        bOk = true;
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed write license key", ex);
            }

            return bOk;
        }

        public static bool sbLoadLicKey(CEncryptedString ArHardwareKey, CEncryptedString ArLicenseKey, string APath)
        {
            bool bOk = false;
            string hwStr = "", licStr = "";
            string path = APath;
            string licFile = APath;

            if (ArHardwareKey != null && ArLicenseKey != null)
            {
                if (path == null || path.Length < 1)
                {
                    path = CProgram.sGetProgDir();
                }

                try
                {
                    licFile = Path.Combine(path, CProgram.sGetProgName() + ".lkd");
                    bool bFound = File.Exists(licFile); // On FTP the name is case sensitive. MultiPC must be exact progName

                    if (false == bFound)
                    {
                        /*                        string[] files = Directory.GetFiles(CProgram.sGetProgDir(), "*" + CProgram.sGetProgName() + ".lkd");

                                                if (files != null && files.Length > 0)
                                                {
                                                    licFile = files[0];
                                                    bFound = true;
                                                }
                        */
                        CProgram.sLogLine("LicKey not present: " + licFile);
                    }
                    if (bFound)
                    {
                        using (StreamReader sr = new StreamReader(licFile))
                        {
                            if (sr != null)
                            {
                                char c;
                                int n;

                                string line1 = sr.ReadLine();
                                if (line1.StartsWith("hwID="))
                                {
                                    n = line1.Length;
                                    for (int i = 5; i < n; ++i)
                                    {
                                        c = line1[i];
                                        if (c > ' ') hwStr += c;
                                    }

                                    string line2 = sr.ReadLine();
                                    if (line2.StartsWith("lic="))
                                    {
                                        n = line2.Length;
                                        for (int i = 4; i < n; ++i)
                                        {
                                            c = line2[i];
                                            if (c > ' ') licStr += c;
                                        }
                                        ArHardwareKey.mbSetEncrypted(hwStr);
                                        ArLicenseKey.mbSetEncrypted(licStr);
                                        bOk = true;
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("LicKey load failed: " + licFile, ex);

                    bOk = false;
                }
            }
            return bOk;
        }
        public static bool sbReopenLicenseRequest()
        {
            bool bOk = _sDeviceID > 0;
            bool bReq = true;

            if (sbIsMultiPC(_sDeviceID))
            {
                bReq = CProgram.sbAskOkCancel("Open License Window", "MultiPC license present, Continue?");
            }
            if (bReq)
            {
                bOk = sbLicenseRequest(_sUsedWarnDays, true, _sUsedLicensePc, _sUsedLicensePath);
            }
            return bOk;
        }

        public static bool sbLicenseRequest(UInt16 AWarnDays, bool AbForceShow, DLicensePC ALicensePc, string ALicensePath = null)
        // checks lisense key, if not valid prompts for license key
        {
            bool bOk = false;

            CEncryptedString hardwareKey = new CEncryptedString("LicHwKey", DEncryptLevel.L1_Program, 0);
            CEncryptedString licenseKey = new CEncryptedString("LicKey", DEncryptLevel.L1_Program, 0);

            if (hardwareKey != null && licenseKey != null)
            {
                bool bChanged;
                bool bLoaded = false;

                switch(ALicensePc)
                {
                    case DLicensePC.SinglePC:
                        bLoaded = sbLoadLicKey(hardwareKey, licenseKey, CProgram.sGetProgDir());
                        break;
                    case DLicensePC.MultiPcPrefered:
                        if( ALicensePath != null)
                        {
                            bLoaded = sbLoadLicKey(hardwareKey, licenseKey, ALicensePath);
                        }
                        if( false == bLoaded)
                        {
                            bLoaded = sbLoadLicKey(hardwareKey, licenseKey, CProgram.sGetProgDir());
                        }
                        break;
                    case DLicensePC.MultiPCAllowed:
                        bLoaded = sbLoadLicKey(hardwareKey, licenseKey, CProgram.sGetProgDir());

                        if (false == bLoaded)
                        {
                            if (ALicensePath != null)
                            {
                                bLoaded = sbLoadLicKey(hardwareKey, licenseKey, ALicensePath);
                            }
                        }
                        break;
                }

                bOk = CLicKeyDev.sbLicenseRequest(out bChanged, ref hardwareKey, ref licenseKey, AWarnDays, AbForceShow, ALicensePc, ALicensePath);

                if( bOk )
                {
                    _sUsedWarnDays = AWarnDays;     // store for sbReopenLicenseRequest()
                    _sUsedLicensePc = ALicensePc;
                    _sUsedLicensePath = ALicensePath;
                }
                if (bOk && bChanged)
                {
                    if (false == sbIsMultiPC(_sDeviceID)
                        || CProgram.sbAskOkCancel("Save License key", "Save MultiPC license localy?"))
                    {
                        string licFile = Path.Combine(CProgram.sGetProgDir(), CProgram.sGetProgName() + ".lkd");
                        sbSaveLicKey(licFile, hardwareKey, licenseKey);
                    }
                }
                CProgram.sSetDeviceIX(_sDeviceID);
            }
            return bOk;
        }

        public static bool sbIsMultiPC(UInt32 ADeviceID)
        {
            uint devID = _sDeviceID % _cNrDevPerOrg;
            uint orgNr = _sDeviceID / _cNrDevPerOrg;

            return orgNr > 0  && (devID == _cDevIdMultiPC || (devID >= _cDevIdMultiBase && devID < _cDevIdMultiBase + _cNrMultiPC));

        }
        public static UInt16 sCalcMultiPcNr()
        {
            // generate a multi pc id from PC name that is hopefully unique
            string pcName = sStripName(Environment.MachineName);
            Int32 sum = 0;
            int n = pcName.Length;
            if (n > 2)
            {
                sum = 4275;
                for (int i = 0; i < n; ++i)
                {
                    sum += (1 + (1 << (i & 10))) * pcName[i];
                }
                while (sum >= _cNrMultiPC)
                {
                    sum = (sum % _cNrMultiPC) + (sum / _cNrMultiPC);
                }
                sum += _cDevIdMultiBase;
            }
            return (UInt16)sum;
        }


        public static bool sbCheckMultiPC()
        {
            uint devID = _sDeviceID % _cNrDevPerOrg;
            uint orgNr = _sDeviceID / _cNrDevPerOrg;

            if (devID == _cDevIdMultiPC)
            {
                // generate a multi pc id from PC name that is hopefully unique
                _sDeviceID = orgNr * _cNrDevPerOrg + (UInt32)(sCalcMultiPcNr());
            }
            return orgNr > 0 && (devID >= _cDevIdMultiBase && devID < _cDevIdMultiBase + _cNrMultiPC);
        }

        public static bool sbLicenseRequest( out bool AbChanged, ref CEncryptedString ArFullHardwareKey, 
                                ref CEncryptedString ArFullLicenseKey, UInt16 AWarnDays, bool AbForceShow, DLicensePC ALicensePc = DLicensePC.SinglePC, string ALicensePath = null)
            // checks lisense key, if not valid prompts for license key
        {
            bool bOk = false;
            bool bAsk = true;
            bool bChanged = false;
            string reason = "Request License key";
            int nrDays = 0;

            if (CProgram.sGetProgVersionLevel() >= DProgramVersionLevel.Released // always prompt in alpha and demo mode
                || CProgram.sGetProgVersionLevel() == DProgramVersionLevel.Beta )
            {
                //bool bMultiLicense = 
                // try licensekey to see if we need to prompt license key
                if (sbSetCheckHardwareKey(ArFullHardwareKey.mDecrypt()))
                {
                    if (sbSetCheckLicKey(ArFullLicenseKey.mDecrypt()))
                    {
                        bOk = sbIsMultiPC(_sDeviceID) ? sbCheckMultiPC() : sbCheckAgainstPc();

                        if( bOk )
                        {
                            bOk = CProgram.sbSetProgramOrganisation(_sOrganisationLabel, _sDeviceID);
                        }
                    }
                }
                if (bOk)
                {
                    nrDays = sGetDaysLeft();

                    if (nrDays <= AWarnDays)
                    {
                        bAsk = true;
                        reason = "Request new license key, " + nrDays.ToString() + " days left.";
                    }
                    else
                    {
                        bAsk = AbForceShow;
                    }
                }
            }
            if( bAsk )
            {
                CLicKeyReqForm form = new CLicKeyReqForm(sCreateHardwareKey(), 
                                "License key", ArFullLicenseKey.mDecrypt(), ALicensePc, ALicensePath);
                
                if( form != null)
                {
                    form.ShowDialog();

                    if( false == form.mbAccept)
                    {
                        CProgram.sLogError("LicKey screen not accepted");
                    }

                    if( form.mbAccept)
                    {
                        if (sbSetCheckLicKey(form.mLicenseKey))
                        {
                            bOk = sbIsMultiPC(_sDeviceID) ? sbCheckMultiPC() : sbCheckAgainstPc();

                            if (bOk)
                            {
                                nrDays = sGetDaysLeft();
                                bOk = nrDays >= 0;
                                bChanged = true;
                                if( bOk )
                                {
                                    ArFullHardwareKey.mbEncrypt(sCombineHardwareKey());

                                    ArFullLicenseKey.mbEncrypt(form.mLicenseKey);
                                }
                                CProgram.sbSetProgramOrganisation(_sOrganisationLabel, _sDeviceID);
                            }
                        }
                        if( bOk == false)
                        {
                            CProgram.sPromptError(true, "", "License invalid!");
                        }
                    }
                } 
            }
            string mpc = sbIsMultiPC(_sDeviceID) ? " MutiPC" : "";

            if (bOk)
            {
                CProgram.sLogLine(_sOrganisationLabel + "(" + _sDeviceID.ToString() + mpc + ") License checked ok, " + nrDays.ToString() + " days left.");
            }
            else
            {
                CProgram.sLogError(_sOrganisationLabel + "(" + _sDeviceID.ToString() + mpc + ") License checked failed!");
            }

            AbChanged = bChanged;
            return bOk;
        }
 
        public static void sClear()
        {
            _sOrganisationLabel = "";
            _sOrganisationNr = 0;
            _sDeviceID = 0;
            _sHardwareStr = "";
            _sDeviceValidYMD = 0;
//            _sHwChecksum = "";
        }

        public static string sGetOrgLabel()
        {
            return _sOrganisationLabel;
        }

/*        public static string sFormatOrgLabel(string AOrgLabel)
        {
            string s = "";
            int nOrg = AOrgLabel == null ? 0 : AOrgLabel.Length;
            int n = 0;
            char c;

            for (int i = 0; i < nOrg; ++i)
            {
                c = AOrgLabel[i];

                if (char.IsLetterOrDigit(c))
                {
                    s += n < 2 ? char.ToUpper(c) : c;
                    ++n;
                }
            }
            return s;
        }
*/        public static bool sbFormatOrgLabel(out string ArNewLabel, string AOrgLabel)
        {
            bool bOk = false;
            string s = "";
            char c;
            int nOrg = AOrgLabel == null ? 0 : AOrgLabel.Length;
            if( nOrg > 4 )
            {
                if (Char.IsLetter(AOrgLabel[0]) && Char.IsLetter(AOrgLabel[1]))
                {
                    s += Char.ToUpper(AOrgLabel[0]);
                    s += Char.ToUpper(AOrgLabel[1]);
                    int i = 2;
                    int nrCenter = 0;
                    while (i < nOrg)
                    {
                        c = AOrgLabel[i];

                        if (Char.IsDigit(c))
                        {
                            nrCenter = nrCenter * 10 + (int)(c - '0');
                            ++i;
                        }
                        else
                        {
                            break;
                        }
                    }
                    if (nrCenter > 0 && nrCenter < 10000)
                    {
                        s += nrCenter.ToString("D4");

                        c = AOrgLabel[i];
                        if (Char.IsLetter(c))
                        {
                            s += c;
                            while (++i < nOrg)
                            {
                                c = AOrgLabel[i];
                                if (Char.IsLetterOrDigit(c) || c == '_')
                                {
                                    s += c;
                                }
                            }
                            bOk = true;
                        }
                    }
                }
            }
            ArNewLabel = s;
            return bOk;
        }
        public static bool sbSetOrgLabel( string AOrgLabel )
        {
            bool bOk = false;

            if (AOrgLabel == null || AOrgLabel.Length < 6)
            {
                _sOrganisationLabel = "X";
            }
            else
            {
                bOk = sbFormatOrgLabel( out _sOrganisationLabel, AOrgLabel); 
            }
            return bOk;
        }
        public static UInt32 sGetDeviceID()
        {
            return _sDeviceID;
        }
        public static bool sbDeviceIsProgrammer()
        {
            UInt32 id = _sDeviceID % 10000;
            UInt32 center = _sDeviceID / 10000;

            return id != 0 && center < 30 && id < 10;
        }
        public static bool sbDeviceIsTestOrganisation()
        {
            UInt32 id = _sDeviceID % 10000;
            UInt32 center = _sDeviceID / 10000;

            return id != 0 && center < 30;
        }
        public static bool sbDeviceIsMultiPC()
        {
            return sbIsMultiPC(_sDeviceID);
        }

        public static string sGetDiviceString()
        {
            return _sDeviceID.ToString( "X8" );
        }
        public static UInt16 sGetOrgNr()
        {
            return _sOrganisationNr;
        }
        public static string sGetHardwareStr()
        {
            return _sHardwareStr;
        }
        public static Int32 sGetDeviceValidYMD()
        {
            return _sDeviceValidYMD;
        }
        public static string sGetDateValidString()
        {
            DateTime dt;

            if (_sDeviceValidYMD > 0)
            {
                int day = _sDeviceValidYMD % 10000;
                int i = _sDeviceValidYMD / 100;
                int year = i / 100;
                int month = i % 100;
                dt = new DateTime(year, month, day);
            }
            else
            {
                dt = DateTime.MinValue;
            }
            return CProgram.sDateTimeToString(dt);
        }
        public static int sGetDaysLeft()
        {
            int i = -1;

            if( _sDeviceValidYMD > 0 )
            {
                try
                {
                    int day = _sDeviceValidYMD % 100;
                    i = _sDeviceValidYMD / 100;
                    int year = i / 100;
                    int month = i % 100;
                    DateTime dt = new DateTime(year, month, day);
                    i = (int)((dt - DateTime.Now).TotalDays + 0.999);
                }
                catch ( Exception)
                {
                    i = -1;
                }
            }
            return i;
        }
        public static string sGetLogLines()
        {
            string s = "LicDev: " + _sDeviceID.ToString();
            if( sbIsMultiPC(_sDeviceID))
            {
                s += " MultiPC";
            }
            s += "\r\nLicValid: " + _sDeviceValidYMD.ToString() + " ";

            int daysLeft = sGetDaysLeft();
            if( daysLeft < 0)
            {
                s += "expired!";
            }
            else
            {
                s += daysLeft.ToString() + " days left";
            }
            return s;
        }
        public static bool sbIsOrgDevValid()
        {
            return _sOrganisationLabel.Length >= 6 && _sDeviceID != 0;
        }
        public static string sCombineHardwareKey()
        {
            string s = "";

            if( sbIsMultiPC( _sDeviceID))
            {
                string pcName = sStripName(Environment.MachineName);
                // CC1234NNNN-$DDDDDDD$-$CPUID0$-$BIOSID$-$BaSEID$-$DISK00$-PC-NAME-$chksum$
                s = _sOrganisationLabel.Length >= 6 ? _sOrganisationLabel : "X";

                s += "-" + sGetDiviceString() + "-0000-0000-0000-0000-" + pcName;
                UInt32 cs = sCalcChecksum1(s);
                s += "-" + cs.ToString("X8");

            }
            else if( _sHardwareStr.Length > 8 )
            {
                // CC1234NNNN-$DDDDDDD$-$CPUID0$-$BIOSID$-$BaSEID$-$DISK00$-PC-NAME-$chksum$
                s = _sOrganisationLabel.Length >= 6 ? _sOrganisationLabel : "X";
 
                s += "-" + sGetDiviceString() + "-" + _sHardwareStr;
                UInt32 cs = sCalcChecksum1(s);
                s += "-" + cs.ToString("X8");
            }
            return s;
        }
        public static string sCreateHardwareKey()
        {
            _sHardwareStr = sCalcHardwareID();
            return sCombineHardwareKey();
        } 

        public static bool sbSetCheckHardwareKey( string AOrgLabel, UInt32 ADeviceID, string AHardwareString, string AChecksum)
        {
            sClear();
            bool bOk = sbSetOrgLabel(AOrgLabel);

            _sDeviceID = ADeviceID;
            _sHardwareStr = AHardwareString;

            if( bOk )
            {
                string s = sCombineHardwareKey();
                UInt32 cs = sCalcChecksum1(s);
                bOk = AChecksum == cs.ToString("X8");
            }
            return bOk;
        }
        public static UInt16 sExtractNr( string AString )
        {
            int i = -1;
            string s = AString;
            int n = s != null ? s.Length : 0;
            string number = "";

            while (n >= 1 && char.IsLetter(s[0]))
            {
                s = s.Remove(0, 1); // remove  leading letters
                --n;
            }
            for( i = 0; i < n; ++i )
            {
                if( char.IsDigit(s[i]))
                {
                    number += s[i];
                }
                else
                {
                    break;
                }
            }
            if( false == int.TryParse(number, out i) || i < 0 )
            {
                i = 0;
            }
            return (UInt16)i;
        }

        public static bool sbCheckHardwareStrValid( string AHwStr)
        {
            bool bOk = false;

            // CPUID0$-$BIOSID$-$BaSEID$-$DISK00$-PC-NAME
            if( AHwStr != null && AHwStr.Length >= 30)
            {
                int j, hex;
                string hexStr;
                bOk = true;
                for( int i = 0; i < 4; ++i )
                {
                    j = i * 9;
                    if (AHwStr[j] != '-' ) bOk = false;
                    hexStr = AHwStr.Substring(j, 8);
                    hex = Convert.ToInt32(hexStr, 16);
                    if (hexStr != hex.ToString("X8")) bOk = false;
                }
            }
            return bOk;
        }
        public static bool sbSetCheckHardwareKey( string AFullHwKey )
        {
            bool bOk = false;

            string hwKey = AFullHwKey; // CC1234NNNN-$DDDDDDD$-$CPUID0$-$BIOSID$-$BaSEID$-$DISK00$-PC-NAME-$chksum$
            int orgNr = 0;
            int idNr = 0;
            int n = hwKey == null ? 0 : hwKey.Length;
            string orgLabel = "";
            string hwString = "";

            sClear();
            if (n > 8)
            {
                int pos = hwKey.LastIndexOf('-');
                if (pos > 0)
                {
                    string left = hwKey.Substring(0, pos);
                    string right = hwKey.Substring(pos + 1, n - pos - 1);  // checksum

                    uint cs = CLicKeyDev.sCalcChecksum1(left);
                    string csHex = cs.ToString("X8");
                    bool bChkOk = right == csHex;

                    if( false == bChkOk)
                    {
                        CProgram.sLogError("Checksum not ok for " + left);
                        bChkOk = CProgram.sbAskOkCancel("LickKey", "Checksum not ok, continue?");
                    }
                    if (bChkOk)
                    {
                        pos = hwKey.IndexOf('-');
                        if (pos >= 1)
                        {
                            orgLabel = hwKey.Substring(0, pos);
                            if (orgLabel.Length >= 8)// CC1234NNNN
                            {
                                orgNr = sExtractNr(orgLabel);
                            }
                            int pos2 = hwKey.IndexOf('-', pos + 1);
                            if (pos2 > pos)
                            {
                                string idStr = hwKey.Substring(pos + 1, pos2 - pos - 1);
                                hwString = left.Substring(pos2 + 1, left.Length - pos2 - 1);

                                idNr = Convert.ToInt32(idStr, 16);

                                bOk = true; // returns true if key string could be read
                            }
                        }
                    }
                }
            }
            _sOrganisationNr = (UInt16)( sbSetOrgLabel(orgLabel) ? orgNr : 0);
            _sDeviceID = (UInt32)idNr;
            _sHardwareStr = hwString;
            return bOk;
        }
        public static string sGetPcName()
        {
            string pcName = "!Unknown PC name!";

            if(_sHardwareStr != null && _sHardwareStr.Length > 0 )
            {
                int pos = _sHardwareStr.LastIndexOf('-');
                   
                if( pos > 0)
                {
                    pcName = _sHardwareStr.Substring(pos + 1);
                }
            }

            return pcName;
        }

        public static bool sbCheckAgainstPc()
        {
            string hwPc =  sCalcHardwareID();

            bool bOk = _sHardwareStr == hwPc;
            return bOk;
        }

        private static bool sbLicNotOkError( string AStatus, [System.Runtime.CompilerServices.CallerLineNumber]int _AutoLN = 0)
        {
            string s = "LicKey not ok [" + _AutoLN.ToString() + "]";

            if (CProgram.sGetProgVersionLevel() == DProgramVersionLevel.Alpha || sbDeviceIsProgrammer() ) s += ": " + AStatus;

            CProgram.sLogError(s );

            return false;
        }
        public static bool sbSetCheckLicKey( string ALicKey )
        {
            string licKey = ALicKey;
            int orgNr = 0;
            UInt32 idNr = 0;
            UInt16 progFirst = 0xFFFF;
            UInt16 progLast = progFirst;
            int date = 0;
            int n = licKey == null ? 0 : licKey.Length;
            string orgLabel = "";
            UInt32 csHw = 0;

            _sOrganisationNr = 0;
            _sDeviceID = 0;
            _sDeviceValidYMD = 0;
            //CC1234NN-$DDDDDDD$-$licKey$-YYYYMMDD-$pr$-$chksum$
            if (ALicKey == null || ALicKey.Length < 20)
            {
                return false; // empty string?
            }
            if (CProgram.sGetProgVersionLevel() == DProgramVersionLevel.Alpha) CProgram.sLogError("Test alpha LicKey=" + ALicKey);

            int pos = licKey.LastIndexOf('-');
            if (pos <= 0)
            {
                return sbLicNotOkError("-@0");
            }
            string left = licKey.Substring(0, pos);
            string right = licKey.Substring(pos + 1, n - pos - 1);  // checksum

            uint cs = CLicKeyDev.sCalcChecksum1(left);

            if (right != cs.ToString("X8"))
            {
                return sbLicNotOkError(right + "!=" + cs.ToString("X8"));
            }
            pos = licKey.IndexOf('-');
            if (pos <= 0)
            {
                return sbLicNotOkError( "-@" + pos.ToString());
            }
            orgLabel = licKey.Substring(0, pos);

            if (orgLabel.Length < 6)// CC1234NN
            {
                return sbLicNotOkError("orgLabel bad " + orgLabel);
            }
            orgNr = sExtractNr(orgLabel);
            if( orgNr == 0 )
            {
                return sbLicNotOkError("orgNr = 0 " + orgLabel);
            }

            int pos2 = licKey.IndexOf('-', pos + 1);
            if (pos2 <= pos)
            {
                return sbLicNotOkError("-2@" + pos2.ToString() + " <= " + pos.ToString());
            }
            string idStr = licKey.Substring(pos + 1, pos2 - pos - 1);

            idNr = Convert.ToUInt32(idStr, 16);
            //CC1234NNNN-$DDDDDDD$-$licKey$-YYYYMMDD-$chksum$
            uint id = (uint)(idNr & 0x7FFFFFFFF);

            if ((idNr & 0x7FFFFFFFF) == 0)
            {
                return sbLicNotOkError("ifNr = 0 " + idStr);
            }
            int pos3 = licKey.IndexOf('-', pos2 + 1);
            if (pos3 <= pos2)
            {
                return sbLicNotOkError("-3@" + pos3.ToString() + " <= " + pos2.ToString());
            }
            string csStr = licKey.Substring(pos2 + 1, pos3 - pos2 - 1);

            csHw = Convert.ToUInt32(csStr, 16);

            n = left.Length;
            string dateStr = left.Substring(pos3 + 1, n - pos3 - 1 - 5);

            if( false == int.TryParse(dateStr, out date))
            {
                return sbLicNotOkError("date error " + dateStr);
            }
            string progNrStr = left.Substring(n - 4, 4);

            progFirst = Convert.ToUInt16(progNrStr, 16);

            progLast = (UInt16)((progFirst >> 8) & 0x0FFF);
            progFirst &= 0x0FF;

            uint progNr = CProgram.sGetProgNr();
            if( false == ( progNr >= progFirst && progNr <= progLast ))
            {
                return sbLicNotOkError("progNr " + progNr.ToString() + " < " + progFirst.ToString() + " or > " + progLast.ToString());
            }

            DateTime dt = new DateTime(date / 10000, (date % 10000) / 100, date % 100);
            int nrDays = (int)((dt - DateTime.Now).TotalDays + 0.999);

            if (nrDays < 0)
            {
                return sbLicNotOkError("days left= " + nrDays.ToString());
            }
            if (false == sbSetOrgLabel(orgLabel))
            {
                return sbLicNotOkError("Set orgLabel failed " + orgLabel);
            }
            UInt32 devNr = id % CLicKeyDev._cNrDevPerOrg;
            if (devNr == CLicKeyDev._cDevIdMultiPC)
            {
               
                idNr = (UInt32)(orgNr * CLicKeyDev._cNrDevPerOrg + CLicKeyDev.sCalcMultiPcNr());
            }
            else
            {
                string hwKey = orgLabel + "-" + idNr.ToString("X8") + "-" + sCalcHardwareID(); // _sHardwareStr;
                UInt32 cs2 = sCalcChecksum2(hwKey);

                if (cs2 != csHw)
                {
                    return sbLicNotOkError("hwKey" + hwKey + "= " + cs2.ToString("X8") + " != " + csHw.ToString("X8"));
                }
            }
            _sOrganisationNr = (UInt16)orgNr;
                    _sDeviceID = idNr;
                    _sDeviceValidYMD = date;
            if( CProgram.sGetProgVersionLevel() == DProgramVersionLevel.Alpha) CProgram.sLogError("LicKey ok=" + ALicKey);
            return true;
        }

        public static bool sbSetCheckLicKeyOld(string ALicKey)
        {
            bool bOk = false;

            _sOrganisationNr = 0;
            _sDeviceID = 0;
            _sDeviceValidYMD = 0;
            //CC1234NN-$DDDDDDD$-$licKey$-YYYYMMDD-$pr$-$chksum$
            if (ALicKey == null || ALicKey.Length < 20)
            {
                //CProgram.sLogError("LicKey not ok");
            }
            else
            {
                string licKey = ALicKey;
                int orgNr = 0;
                UInt32 idNr = 0;
                UInt16 progFirst = 0xFFFF;
                UInt16 progLast = progFirst;
                int date = 0;
                int n = licKey == null ? 0 : licKey.Length;
                string orgLabel = "";
                UInt32 csHw = 0;

                if (false == bOk && CProgram.sGetProgVersionLevel() == DProgramVersionLevel.Alpha) CProgram.sLogError("Test LicKey=" + ALicKey);

                int pos = licKey.LastIndexOf('-');
                if (pos <= 0)
                {
                    CProgram.sLogError("LicKey not ok");
                }
                else
                {
                    string left = licKey.Substring(0, pos);
                    string right = licKey.Substring(pos + 1, n - pos - 1);  // checksum

                    uint cs = CLicKeyDev.sCalcChecksum1(left);

                    if (right != cs.ToString("X8"))
                    {
                        CProgram.sLogError("LicKey not ok");
                        if (CProgram.sGetProgVersionLevel() == DProgramVersionLevel.Alpha) CProgram.sLogError(right + "!=" + cs.ToString("X8"));
                    }
                    else
                    {
                        pos = licKey.IndexOf('-');
                        if (pos <= 0)
                        {
                            CProgram.sLogError("LicKey not ok");
                            if (CProgram.sGetProgVersionLevel() == DProgramVersionLevel.Alpha) CProgram.sLogError("-@" + pos.ToString());
                        }
                        else
                        {
                            orgLabel = licKey.Substring(0, pos);
                            if (orgLabel.Length >= 6)// CC1234NN
                            {
                                orgNr = sExtractNr(orgLabel);
                            }
                            else
                            {
                                CProgram.sLogError("LicKey not ok");
                                if (CProgram.sGetProgVersionLevel() == DProgramVersionLevel.Alpha) CProgram.sLogError("orgLabel bad " + orgLabel);
                            }

                            int pos2 = licKey.IndexOf('-', pos + 1);
                            if (pos2 <= pos)
                            {
                                if (CProgram.sGetProgVersionLevel() == DProgramVersionLevel.Alpha) CProgram.sLogError("-2@" + pos2.ToString() + " <= " + pos.ToString());
                                CProgram.sLogError("LicKey not ok");
                            }
                            else
                            {
                                string idStr = licKey.Substring(pos + 1, pos2 - pos - 1);

                                idNr = Convert.ToUInt32(idStr, 16);
                                //CC1234NNNN-$DDDDDDD$-$licKey$-YYYYMMDD-$chksum$

                                int pos3 = licKey.IndexOf('-', pos2 + 1);
                                if (pos3 <= pos2)
                                {
                                    CProgram.sLogError("LicKey not ok");
                                    if (CProgram.sGetProgVersionLevel() == DProgramVersionLevel.Alpha) CProgram.sLogError("-3@" + pos3.ToString() + " <= " + pos2.ToString());
                                }
                                else
                                {
                                    string csStr = licKey.Substring(pos2 + 1, pos3 - pos2 - 1);

                                    csHw = Convert.ToUInt32(csStr, 16);

                                    n = left.Length;
                                    string dateStr = left.Substring(pos3 + 1, n - pos3 - 1 - 5);

                                    bOk = int.TryParse(dateStr, out date);

                                    string progNrStr = left.Substring(n - 4, 4);

                                    progFirst = Convert.ToUInt16(progNrStr, 16);

                                    if (bOk)
                                    {
                                        progLast = (UInt16)((progFirst >> 8) & 0x0FFF);
                                        progFirst &= 0x0FF;

                                        uint progNr = CProgram.sGetProgNr();
                                        bOk = progNr >= progFirst && progNr <= progLast;

                                        if (false == bOk)
                                        {
                                            CProgram.sLogError("LicKey not ok");
                                            if (CProgram.sGetProgVersionLevel() == DProgramVersionLevel.Alpha) CProgram.sLogError("progNr" + progNr.ToString()
                                                                    + " < " + progFirst.ToString() + " or > " + progLast.ToString());
                                        }
                                    }
                                    else
                                    {
                                        CProgram.sLogError("LicKey not ok");
                                        if (CProgram.sGetProgVersionLevel() == DProgramVersionLevel.Alpha) CProgram.sLogError("date error " + dateStr);
                                    }
                                }
                            }
                        }
                    }
                }
                if (false == bOk && CProgram.sGetProgVersionLevel() == DProgramVersionLevel.Alpha) CProgram.sLogError("Test LicKey=failed");
                if (bOk)
                {
                    bOk = false;
                    uint id = (uint)(idNr & 0x7FFFFFFFF);

                    if (orgNr > 0 && (idNr & 0x7FFFFFFFF) > 0)
                    {
                        DateTime dt = new DateTime(date / 10000, (date % 10000) / 100, date % 100);
                        int nrDays = (int)((dt - DateTime.Now).TotalDays + 0.999);

                        if (nrDays >= 0)
                        {
                            string hwKey = orgLabel + "-" + idNr.ToString("X8") + "-" + sCalcHardwareID(); // _sHardwareStr;
                            UInt32 cs2 = sCalcChecksum2(hwKey);

                            bOk = cs2 == csHw;  // check harware
                            if (false == bOk)
                            {
                                CProgram.sLogError("LicKey not ok");
                                if (CProgram.sGetProgVersionLevel() == DProgramVersionLevel.Alpha) CProgram.sLogError("hwKey" + hwKey + " " + cs2.ToString("X8")
                                        + " != " + csHw.ToString("X8"));
                            }
                        }
                        else
                        {
                            CProgram.sLogError("LicKey not ok");
                            if (CProgram.sGetProgVersionLevel() == DProgramVersionLevel.Alpha) CProgram.sLogError("days left= " + nrDays.ToString());
                        }
                    }
                    else
                    {
                        CProgram.sLogError("LicKey not ok");
                        if (CProgram.sGetProgVersionLevel() == DProgramVersionLevel.Alpha) CProgram.sLogError("OrgNr= " + orgNr.ToString() + ", id=" + id.ToString());
                    }
                }
                if (false == bOk && CProgram.sGetProgVersionLevel() == DProgramVersionLevel.Alpha) CProgram.sLogError("Test LicKey=failed2");
                if (bOk)
                {
                    bOk = sbSetOrgLabel(orgLabel);
                    if (bOk)
                    {
                        _sOrganisationNr = (UInt16)orgNr;
                    }
                    else
                    {
                        CProgram.sLogError("LicKey not ok");
                        if (CProgram.sGetProgVersionLevel() == DProgramVersionLevel.Alpha) CProgram.sLogError("failed ser Org label ");
                    }
                    _sDeviceID = idNr;
                    _sDeviceValidYMD = date;
                }
            }
            if (false == bOk && CProgram.sGetProgVersionLevel() == DProgramVersionLevel.Alpha) CProgram.sLogError("Failed LicKey=" + ALicKey);
            return bOk;
        }

        public static bool sbSetProgramOrgDevice()
        {
            bool bOk = false;

            if( _sOrganisationNr > 0 && _sDeviceID > 0 && sGetDaysLeft() >= 0 )
            {
                bOk = CProgram.sbSetProgramOrganisation(_sOrganisationLabel, _sDeviceID);
            }
            return bOk;
        }

        public static bool sbLoadFromSettings()
        {
            //            _sOrganisationLabel = LicKey.
            return false;

        } 
        public static bool sbLoadOrganisation()
        {
            bool bOk = false;
            string filePath = Path.Combine(CProgram.sGetProgDir(), "organisation.lk");

            return bOk;
        }

        public static string sStripText(string AText)
        {
            string s = "";
            int n = AText == null ? 0 : AText.Length;

            for (int i = 0; i < n; ++i)
            {
                char c = AText[i];

                if (c > ' ')
                {
                    if (c < 127 && char.IsLetterOrDigit(c)) s += c;
                    else if (c == '-' || c == '_') s += c; 
                }
            }
            return s;
        }

        public static string sStripName( string AName )
        {
            string s = "";
            int n = AName == null ? 0 : AName.Length;

            for( int i = 0; i < n; ++i )
            {
                char c = AName[i];

                if( c > ' ' )
                {
                    if (c < 127 && char.IsLetterOrDigit(c)) s += c;
                    else s += "_";
                }
            }
            return s;
        }

        public static string sCalcHex32Str( string AString )
        {
            UInt32 result = 0;
            int n = AString == null ? 0 : AString.Length;
            int j;

            if( n > 0 )
            {
                bool bOk = false;
                for (int i = 0; i < n; ++i)
                {
                    char c = AString[i];
                    if (c != '_')
                    {
                        bOk = true;
                        break;
                    }                        
                }
                if( bOk == false )
                {
                    n = 0;  // string contains only '_';
                }
            }
            for ( int i = 0; i < n; ++i )
            {
                char c = AString[i];
                j = i < 4 ? i * 8 : (i < 8 ? i * 2 : (i % 24));
                result += (UInt32)(c << j);
            }
            return result.ToString("X8");
        }

        public static UInt32 sCalcChecksum1(string AString)
        {
            UInt32 ph = CProgram.sGetProgramHash();
            UInt16 xorMask = 0;
            UInt16 addMask = 0;
            int n = AString == null ? 0 : AString.Length;

            for (int i = 0; i < n; ++i)
            {
                char c = AString[i];
                int j = i & 0x07;

                xorMask ^= (UInt16)((c & 0x0FF) << j);
                addMask += (UInt16)((c & 0x0FF) << (8 - j));
            }
            ph ^= (UInt32)xorMask & 0x0FFFF;
            ph ^= (UInt32)addMask << 16;
            return ph;
        }
        public static UInt32 sCalcChecksum2(string AString)
        {
            UInt32 ph = CProgram.sGetProgramHash();// sGetOrganisationHash();
            UInt16 xorMask = 0;
            UInt16 subMask = 0;
            int n = AString == null ? 0 : AString.Length;

            UInt32 ph2 = ph << 8;
            ph >>= 8;
            ph = (ph & 0x00FF00FF) | (ph2 & 0xFF00FF00);    // swap bytes
            for (int i = 0; i < n; ++i)
            {
                char c = AString[i];
                int j = i & 0x07;

                xorMask ^= (UInt16)((c & 0x0FF) << j);
                subMask -= (UInt16)((c & 0x0FF) << (8 - j));
            }
            ph ^= (UInt32)subMask & 0x0FFFF;
            ph ^= (UInt32)xorMask << 16;
            return ph;
        }
        public static string sCalcHardwareID()
        { // $CPUID0$-$BIOSID$-$BaSEID$-$DISK00$-PC-NAME
            if( _sPcHwID != null && _sPcHwID.Length > 8)
            {
                return _sPcHwID;    // only determin the PC hardware once (saves time)
            }

            string cpuID = sCpuId();
            string biosID = sBiosId();
            string baseID = sBaseId();
            string diskID = sDiskId();
            string pcName = sStripName( Environment.MachineName );

            string s = sCalcHex32Str(cpuID);
            s += "-" + sCalcHex32Str(biosID);
            s += "-" + sCalcHex32Str(baseID);
            s += "-" + sCalcHex32Str(diskID);
            s += "-" + pcName;

            _sPcHwID = s;
            return s;
        }
        //Return a hardware identifier
        private static string sIdentifier2(string AWmiClass, string AWmiProperty)
        {
            string result = "";
            System.Management.ManagementClass mc = null;

                try
                {
                mc = new System.Management.ManagementClass(AWmiClass);

                if (mc != null)
                {
                    System.Management.ManagementObjectCollection moc = null;

                    try
                    {
                        moc = mc.GetInstances();
                    }
                    catch( Exception)
                    {
                        moc = null;
                    }

                    if( moc != null )
                    {
                        foreach (System.Management.ManagementObject mo in moc)
                        {
                            //Only get the first one
                            if (result == "")
                            {
                                try
                                {
                                    if (mo[AWmiProperty] != null)
                                    {
                                        result = mo[AWmiProperty].ToString();
                                        break;
                                    }
                                }
                                catch
                                {
                                }
                            }
                        }
                    }
                }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("HwId2 collect failed for " + AWmiClass + "(" + AWmiProperty + ")", ex);
                }
            return result;
        }
        public static string sCpuId()
        {
            //Uses first CPU identifier available in order of preference
            //Don't get all identifiers, as it is very time consuming
            string retVal = sIdentifier2("Win32_Processor", "UniqueId");
            if (retVal == "") //If no UniqueID, use ProcessorID
            {
                retVal = sIdentifier2("Win32_Processor", "ProcessorId");
                if (retVal == "") //If no ProcessorId, use Name
                {
                    retVal = sIdentifier2("Win32_Processor", "Name");
                    if (retVal == "") //If no Name, use Manufacturer
                    {
                        retVal = sIdentifier2("Win32_Processor", "Manufacturer");
                    }
                    //Add clock speed for extra security
                    retVal += sIdentifier2("Win32_Processor", "MaxClockSpeed");
                }
            }
            return retVal;
        }
        //BIOS Identifier
        public static string sBiosId()
        {
         /*   string result = "";

            result += sIdentifier2("Win32_BIOS", "Manufacturer");
            if (result != null && result.Length > 0) result += "_";
            result += sIdentifier2("Win32_BIOS", "SMBIOSBIOSVersion");
            if (result != null && result.Length > 0) result += "_";
            result += sIdentifier2("Win32_BIOS", "IdentificationCode");
            if (result != null && result.Length > 0) result += "_";
            result += sIdentifier2("Win32_BIOS", "SerialNumber");
            if (result != null && result.Length > 0) result += "_";
            result += sIdentifier2("Win32_BIOS", "ReleaseDate");
            if (result != null && result.Length > 0) result += "_";
            result += sIdentifier2("Win32_BIOS", "Version");

            return result;
            */
            return sIdentifier2("Win32_BIOS", "Manufacturer") + "_"
            + sIdentifier2("Win32_BIOS", "SMBIOSBIOSVersion") + "_"
            + sIdentifier2("Win32_BIOS", "IdentificationCode") + "_"
            + sIdentifier2("Win32_BIOS", "SerialNumber") + "_"
            + sIdentifier2("Win32_BIOS", "ReleaseDate") + "_"
            + sIdentifier2("Win32_BIOS", "Version");
        }
        //Main physical hard drive ID
        public static string sDiskId()
        {
/*            string result = "";

            result += sIdentifier2("Win32_DiskDrive", "Model");
            if (result != null && result.Length > 0) result += "_";
            result += sIdentifier2("Win32_DiskDrive", "Manufacturer");
            if (result != null && result.Length > 0) result += "_";
            result += sIdentifier2("Win32_DiskDrive", "Signature");
            if (result != null && result.Length > 0) result += "_";
            result += sIdentifier2("Win32_DiskDrive", "TotalHeads");

            return result;
*/            return sIdentifier2("Win32_DiskDrive", "Model") + "_"
            + sIdentifier2("Win32_DiskDrive", "Manufacturer") + "_"
            + sIdentifier2("Win32_DiskDrive", "Signature") + "_"
            + sIdentifier2("Win32_DiskDrive", "TotalHeads");
        }
        //Motherboard ID
        public static string sBaseId()
        {
/*            string result = "";
            result = sIdentifier2("Win32_BaseBoard", "Model");

            if (result != null && result.Length > 0) result += "_";

            result += sIdentifier2("Win32_BaseBoard", "Manufacturer");
            if (result != null && result.Length > 0) result += "_";

            result += sIdentifier2("Win32_BaseBoard", "Name");
            if (result != null && result.Length > 0) result += "_";

            result += sIdentifier2("Win32_BaseBoard", "SerialNumber");
            return result;
 */           return sIdentifier2("Win32_BaseBoard", "Model") + "_"
            + sIdentifier2("Win32_BaseBoard", "Manufacturer") + "_"
            + sIdentifier2("Win32_BaseBoard", "Name") + "_"
            + sIdentifier2("Win32_BaseBoard", "SerialNumber");
        }

    }
    /// <summary>
    /// Generates a 16 byte Unique Identification code of a computer
    /// Example: 4876-8DB5-EE85-69D3-FE52-8CF7-395D-2EA9
    /// </summary>
    public class HwScan
    {
        private static string _sFingerPrint = string.Empty;
        public static string Value()
        {
            if (string.IsNullOrEmpty(_sFingerPrint))
            {
             _sFingerPrint = sGetHash("CPU >> " + cpuId() + "\nBIOS >> " +
            biosId() + "\nBASE >> " + baseId()
            +"\nDISK >> "+ diskId() + "\nVIDEO >> " 
            + videoId() + "\nMAC >> " + macId()
                                     );
            }
            return _sFingerPrint;
        }
        private static string sGetHash(string s)
        {
            MD5 sec = new MD5CryptoServiceProvider();
            ASCIIEncoding enc = new ASCIIEncoding();
            byte[] bt = enc.GetBytes(s);
            return sGetHexString(sec.ComputeHash(bt));
        }
        private static string sGetHexString(byte[] ABytes)
        {
            string s = string.Empty;
            for (int i = 0; i < ABytes.Length; i++)
            {
                byte b = ABytes[i];
                int n, n1, n2;
                n = (int)b;
                n1 = n & 15;
                n2 = (n >> 4) & 15;
                if (n2 > 9)
                    s += ((char)(n2 - 10 + (int)'A')).ToString();
                else
                    s += n2.ToString();
                if (n1 > 9)
                    s += ((char)(n1 - 10 + (int)'A')).ToString();
                else
                    s += n1.ToString();
                if ((i + 1) != ABytes.Length && (i + 1) % 2 == 0) s += "-";
            }
            return s;
        }
        #region Original Device ID Getting Code
        //Return a hardware identifier
/*        private static string identifier (string wmiClass, string wmiProperty, string wmiMustBeTrue)
        {
            string result = "";
            System.Management.ManagementClass mc = new System.Management.ManagementClass(wmiClass);
            System.Management.ManagementObjectCollection moc = mc.GetInstances();
            foreach (System.Management.ManagementObject mo in moc)
            {
                if (mo[wmiMustBeTrue].ToString() == "True")
                {
                    //Only get the first one
                    if (result == "")
                    {
                        try
                        {
                            result = mo[wmiProperty].ToString();
                            break;
                        }
                        catch
                        {
                        }
                    }
                }
            }
            return result;
        }
*/        private static string sIdentifier3(string AWmiClass, string AWmiProperty, string AWmiMustBeTrue)
        {
            string result = "";
            System.Management.ManagementClass mc = null;

            try
            {
                mc = new System.Management.ManagementClass(AWmiClass);

                if (mc != null)
                {
                    System.Management.ManagementObjectCollection moc = null;

                    try
                    {
                        moc = mc.GetInstances();
                    }
                    catch (Exception)
                    {
                        moc = null;
                    }

                    if (moc != null)
                    {
                        foreach (System.Management.ManagementObject mo in moc)
                        {
                            if (mo[AWmiMustBeTrue].ToString() == "True")
                            {
                                //Only get the first one
                                if (result == "")
                                {
                                    try
                                    {
                                        if (mo[AWmiProperty] != null)
                                        {
                                            result = mo[AWmiProperty].ToString();
                                            break;
                                        }
                                    }
                                    catch
                                    {
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("HwId collect failed for " + AWmiClass + "(" + AWmiProperty + ")", ex);
            }
            return result;
        }

        //Return a hardware identifier
        /*        private static string sIdentifier2(string wmiClass, string wmiProperty)
                {
                    string result = "";
                    System.Management.ManagementClass mc = new System.Management.ManagementClass(wmiClass);

                    System.Management.ManagementObjectCollection moc = mc.GetInstances();
                    foreach (System.Management.ManagementObject mo in moc)
                    {
                        //Only get the first one
                        if (result == "")
                        {
                            try
                            {
                                result = mo[wmiProperty].ToString();
                                break;
                            }
                            catch
                            {
                            }
                        }
                    }
                    return result;
                }
        */
        private static string sIdentifier2(string AWmiClass, string AWmiProperty)
        {
            string result = "";
            System.Management.ManagementClass mc = null;

            try
            {
                mc = new System.Management.ManagementClass(AWmiClass);

                if (mc != null)
                {
                    System.Management.ManagementObjectCollection moc = null;

                    try
                    {
                        moc = mc.GetInstances();
                    }
                    catch (Exception)
                    {
                        moc = null;
                    }

                    if (moc != null)
                    {
                        foreach (System.Management.ManagementObject mo in moc)
                        {
                            //Only get the first one
                            if (result == "")
                            {
                                try
                                {
                                    if (mo[AWmiProperty] != null)
                                    {
                                        result = mo[AWmiProperty].ToString();
                                        break;
                                    }
                                }
                                catch
                                {
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("HwId2 collect failed for " + AWmiClass + "(" + AWmiProperty + ")", ex);
            }
            return result;
        }

        public static string cpuId()
        {
            //Uses first CPU identifier available in order of preference
            //Don't get all identifiers, as it is very time consuming
            string retVal = sIdentifier2("Win32_Processor", "UniqueId");
            if (retVal == "") //If no UniqueID, use ProcessorID
            {
                retVal = sIdentifier2("Win32_Processor", "ProcessorId");
                if (retVal == "") //If no ProcessorId, use Name
                {
                    retVal = sIdentifier2("Win32_Processor", "Name");
                    if (retVal == "") //If no Name, use Manufacturer
                    {
                        retVal = sIdentifier2("Win32_Processor", "Manufacturer");
                    }
                    //Add clock speed for extra security
                    retVal += sIdentifier2("Win32_Processor", "MaxClockSpeed");
                }
            }
            return retVal;
        }
        //BIOS Identifier
        public static string biosId()
        {
            string result = "";

            result += sIdentifier2("Win32_BIOS", "Manufacturer");
            if (result != null && result.Length > 0) result += "_";
            result += sIdentifier2("Win32_BIOS", "SMBIOSBIOSVersion");
            if (result != null && result.Length > 0) result += "_";
            result += sIdentifier2("Win32_BIOS", "IdentificationCode");
            if (result != null && result.Length > 0) result += "_";
            result += sIdentifier2("Win32_BIOS", "SerialNumber");
            if (result != null && result.Length > 0) result += "_";
            result += sIdentifier2("Win32_BIOS", "ReleaseDate");
            if (result != null && result.Length > 0) result += "_";
            result += sIdentifier2("Win32_BIOS", "Version");

            return result;
        }
        //Main physical hard drive ID
        public static string diskId()
        {
            string result = "";

            result += sIdentifier2("Win32_DiskDrive", "Model");
            if (result != null && result.Length > 0) result += "_";
            result += sIdentifier2("Win32_DiskDrive", "Manufacturer");
            if (result != null && result.Length > 0) result += "_";
            result += sIdentifier2("Win32_DiskDrive", "Signature");
            if (result != null && result.Length > 0) result += "_";
            result += sIdentifier2("Win32_DiskDrive", "TotalHeads");

            return result;
        }
        //Motherboard ID
        public static string baseId()
        {
            string result = "";
            result = sIdentifier2("Win32_BaseBoard", "Model");

            if( result != null && result.Length > 0 ) result += "_";

            result += sIdentifier2("Win32_BaseBoard", "Manufacturer");
            if (result != null && result.Length > 0) result += "_";

            result += sIdentifier2("Win32_BaseBoard", "Name");
            if (result != null && result.Length > 0) result += "_";

            result += sIdentifier2("Win32_BaseBoard", "SerialNumber");
            return result;
        }
        //Primary video controller ID
        public static string videoId()
        {
            string result = "";

            result += sIdentifier2("Win32_VideoController", "DriverVersion");
            if (result != null && result.Length > 0) result += "_";
            result += sIdentifier2("Win32_VideoController", "Name");

            return result;
        }
        //First enabled network card ID
        public static string macId()
        {
            string result = "";

            result += sIdentifier3("Win32_NetworkAdapterConfiguration", "MACAddress", "IPEnabled");
            return result;
        }
        #endregion
    }
}
