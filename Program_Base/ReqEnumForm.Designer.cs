﻿namespace Program_Base
{
    partial class ReqEnumForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReqEnumForm));
			this.panel1 = new System.Windows.Forms.Panel();
			this.panel4 = new System.Windows.Forms.Panel();
			this.buttonUndo = new System.Windows.Forms.Button();
			this.panel8 = new System.Windows.Forms.Panel();
			this.buttonCancel = new System.Windows.Forms.Button();
			this.panel7 = new System.Windows.Forms.Panel();
			this.buttonOk = new System.Windows.Forms.Button();
			this.panel6 = new System.Windows.Forms.Panel();
			this.panel5 = new System.Windows.Forms.Panel();
			this.toolTip = new System.Windows.Forms.ToolTip(this.components);
			this.panel10 = new System.Windows.Forms.Panel();
			this.panel9 = new System.Windows.Forms.Panel();
			this.panel11 = new System.Windows.Forms.Panel();
			this.labelName = new System.Windows.Forms.Label();
			this.panel12 = new System.Windows.Forms.Panel();
			this.listBoxValue = new System.Windows.Forms.ListBox();
			this.panel13 = new System.Windows.Forms.Panel();
			this.panel14 = new System.Windows.Forms.Panel();
			this.labelUnit = new System.Windows.Forms.Label();
			this.panel2 = new System.Windows.Forms.Panel();
			this.panel3 = new System.Windows.Forms.Panel();
			this.buttonClear = new System.Windows.Forms.Button();
			this.buttonClipboard = new System.Windows.Forms.Button();
			this.panel15 = new System.Windows.Forms.Panel();
			this.panel4.SuspendLayout();
			this.panel11.SuspendLayout();
			this.panel12.SuspendLayout();
			this.panel14.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Margin = new System.Windows.Forms.Padding(2);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(697, 18);
			this.panel1.TabIndex = 0;
			// 
			// panel4
			// 
			this.panel4.BackColor = System.Drawing.Color.DarkSlateBlue;
			this.panel4.Controls.Add(this.buttonClipboard);
			this.panel4.Controls.Add(this.panel15);
			this.panel4.Controls.Add(this.buttonClear);
			this.panel4.Controls.Add(this.panel3);
			this.panel4.Controls.Add(this.panel2);
			this.panel4.Controls.Add(this.buttonUndo);
			this.panel4.Controls.Add(this.panel8);
			this.panel4.Controls.Add(this.buttonCancel);
			this.panel4.Controls.Add(this.panel7);
			this.panel4.Controls.Add(this.buttonOk);
			this.panel4.Controls.Add(this.panel6);
			this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel4.Location = new System.Drawing.Point(0, 288);
			this.panel4.Margin = new System.Windows.Forms.Padding(2);
			this.panel4.Name = "panel4";
			this.panel4.Padding = new System.Windows.Forms.Padding(2);
			this.panel4.Size = new System.Drawing.Size(697, 41);
			this.panel4.TabIndex = 2;
			// 
			// buttonUndo
			// 
			this.buttonUndo.BackColor = System.Drawing.Color.DarkSlateBlue;
			this.buttonUndo.Dock = System.Windows.Forms.DockStyle.Right;
			this.buttonUndo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonUndo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.buttonUndo.ForeColor = System.Drawing.SystemColors.ControlLightLight;
			this.buttonUndo.Image = ((System.Drawing.Image)(resources.GetObject("buttonUndo.Image")));
			this.buttonUndo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.buttonUndo.Location = new System.Drawing.Point(426, 2);
			this.buttonUndo.Margin = new System.Windows.Forms.Padding(2);
			this.buttonUndo.Name = "buttonUndo";
			this.buttonUndo.Size = new System.Drawing.Size(108, 37);
			this.buttonUndo.TabIndex = 5;
			this.buttonUndo.Text = "Undo";
			this.buttonUndo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.buttonUndo.UseVisualStyleBackColor = false;
			this.buttonUndo.Click += new System.EventHandler(this.buttonUndo_Click);
			// 
			// panel8
			// 
			this.panel8.Dock = System.Windows.Forms.DockStyle.Right;
			this.panel8.Location = new System.Drawing.Point(534, 2);
			this.panel8.Margin = new System.Windows.Forms.Padding(2);
			this.panel8.Name = "panel8";
			this.panel8.Size = new System.Drawing.Size(28, 37);
			this.panel8.TabIndex = 4;
			// 
			// buttonCancel
			// 
			this.buttonCancel.BackColor = System.Drawing.Color.DarkSlateBlue;
			this.buttonCancel.Dock = System.Windows.Forms.DockStyle.Right;
			this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.buttonCancel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
			this.buttonCancel.Image = ((System.Drawing.Image)(resources.GetObject("buttonCancel.Image")));
			this.buttonCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.buttonCancel.Location = new System.Drawing.Point(562, 2);
			this.buttonCancel.Margin = new System.Windows.Forms.Padding(2);
			this.buttonCancel.Name = "buttonCancel";
			this.buttonCancel.Size = new System.Drawing.Size(113, 37);
			this.buttonCancel.TabIndex = 3;
			this.buttonCancel.Text = "  Cancel";
			this.buttonCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.buttonCancel.UseVisualStyleBackColor = false;
			this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
			// 
			// panel7
			// 
			this.panel7.Dock = System.Windows.Forms.DockStyle.Right;
			this.panel7.Location = new System.Drawing.Point(675, 2);
			this.panel7.Margin = new System.Windows.Forms.Padding(2);
			this.panel7.Name = "panel7";
			this.panel7.Size = new System.Drawing.Size(20, 37);
			this.panel7.TabIndex = 2;
			// 
			// buttonOk
			// 
			this.buttonOk.BackColor = System.Drawing.Color.DarkSlateBlue;
			this.buttonOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.buttonOk.Dock = System.Windows.Forms.DockStyle.Left;
			this.buttonOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonOk.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.buttonOk.ForeColor = System.Drawing.SystemColors.ControlLightLight;
			this.buttonOk.Image = ((System.Drawing.Image)(resources.GetObject("buttonOk.Image")));
			this.buttonOk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.buttonOk.Location = new System.Drawing.Point(22, 2);
			this.buttonOk.Margin = new System.Windows.Forms.Padding(2);
			this.buttonOk.Name = "buttonOk";
			this.buttonOk.Size = new System.Drawing.Size(91, 37);
			this.buttonOk.TabIndex = 0;
			this.buttonOk.Text = "Ok";
			this.buttonOk.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.buttonOk.UseVisualStyleBackColor = false;
			this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
			// 
			// panel6
			// 
			this.panel6.Dock = System.Windows.Forms.DockStyle.Left;
			this.panel6.Location = new System.Drawing.Point(2, 2);
			this.panel6.Margin = new System.Windows.Forms.Padding(2);
			this.panel6.Name = "panel6";
			this.panel6.Size = new System.Drawing.Size(20, 37);
			this.panel6.TabIndex = 1;
			// 
			// panel5
			// 
			this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel5.Location = new System.Drawing.Point(0, 329);
			this.panel5.Margin = new System.Windows.Forms.Padding(2);
			this.panel5.Name = "panel5";
			this.panel5.Size = new System.Drawing.Size(697, 8);
			this.panel5.TabIndex = 3;
			// 
			// panel10
			// 
			this.panel10.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel10.Location = new System.Drawing.Point(0, 266);
			this.panel10.Margin = new System.Windows.Forms.Padding(2);
			this.panel10.Name = "panel10";
			this.panel10.Size = new System.Drawing.Size(697, 22);
			this.panel10.TabIndex = 4;
			// 
			// panel9
			// 
			this.panel9.Dock = System.Windows.Forms.DockStyle.Left;
			this.panel9.Location = new System.Drawing.Point(0, 18);
			this.panel9.Margin = new System.Windows.Forms.Padding(2);
			this.panel9.Name = "panel9";
			this.panel9.Size = new System.Drawing.Size(22, 248);
			this.panel9.TabIndex = 5;
			// 
			// panel11
			// 
			this.panel11.Controls.Add(this.labelName);
			this.panel11.Dock = System.Windows.Forms.DockStyle.Left;
			this.panel11.Location = new System.Drawing.Point(22, 18);
			this.panel11.Margin = new System.Windows.Forms.Padding(2);
			this.panel11.Name = "panel11";
			this.panel11.Size = new System.Drawing.Size(160, 248);
			this.panel11.TabIndex = 6;
			// 
			// labelName
			// 
			this.labelName.Dock = System.Windows.Forms.DockStyle.Top;
			this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelName.Location = new System.Drawing.Point(0, 0);
			this.labelName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.labelName.Name = "labelName";
			this.labelName.Size = new System.Drawing.Size(160, 38);
			this.labelName.TabIndex = 2;
			this.labelName.Text = "var = ";
			this.labelName.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// panel12
			// 
			this.panel12.Controls.Add(this.listBoxValue);
			this.panel12.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel12.Location = new System.Drawing.Point(182, 18);
			this.panel12.Margin = new System.Windows.Forms.Padding(2);
			this.panel12.Name = "panel12";
			this.panel12.Size = new System.Drawing.Size(401, 248);
			this.panel12.TabIndex = 7;
			// 
			// listBoxValue
			// 
			this.listBoxValue.Dock = System.Windows.Forms.DockStyle.Fill;
			this.listBoxValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.listBoxValue.FormattingEnabled = true;
			this.listBoxValue.ItemHeight = 20;
			this.listBoxValue.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6"});
			this.listBoxValue.Location = new System.Drawing.Point(0, 0);
			this.listBoxValue.Name = "listBoxValue";
			this.listBoxValue.Size = new System.Drawing.Size(401, 248);
			this.listBoxValue.TabIndex = 0;
			// 
			// panel13
			// 
			this.panel13.Dock = System.Windows.Forms.DockStyle.Right;
			this.panel13.Location = new System.Drawing.Point(675, 18);
			this.panel13.Margin = new System.Windows.Forms.Padding(2);
			this.panel13.Name = "panel13";
			this.panel13.Size = new System.Drawing.Size(22, 248);
			this.panel13.TabIndex = 8;
			// 
			// panel14
			// 
			this.panel14.Controls.Add(this.labelUnit);
			this.panel14.Dock = System.Windows.Forms.DockStyle.Right;
			this.panel14.Location = new System.Drawing.Point(583, 18);
			this.panel14.Margin = new System.Windows.Forms.Padding(2);
			this.panel14.Name = "panel14";
			this.panel14.Size = new System.Drawing.Size(92, 248);
			this.panel14.TabIndex = 9;
			// 
			// labelUnit
			// 
			this.labelUnit.Dock = System.Windows.Forms.DockStyle.Top;
			this.labelUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelUnit.Location = new System.Drawing.Point(0, 0);
			this.labelUnit.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.labelUnit.Name = "labelUnit";
			this.labelUnit.Size = new System.Drawing.Size(92, 49);
			this.labelUnit.TabIndex = 4;
			this.labelUnit.Text = "Unit";
			// 
			// panel2
			// 
			this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
			this.panel2.Location = new System.Drawing.Point(113, 2);
			this.panel2.Margin = new System.Windows.Forms.Padding(2);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(21, 37);
			this.panel2.TabIndex = 6;
			// 
			// panel3
			// 
			this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
			this.panel3.Location = new System.Drawing.Point(398, 2);
			this.panel3.Margin = new System.Windows.Forms.Padding(2);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(28, 37);
			this.panel3.TabIndex = 7;
			// 
			// buttonClear
			// 
			this.buttonClear.BackColor = System.Drawing.Color.DarkSlateBlue;
			this.buttonClear.Dock = System.Windows.Forms.DockStyle.Right;
			this.buttonClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.buttonClear.ForeColor = System.Drawing.SystemColors.ControlLightLight;
			this.buttonClear.Image = ((System.Drawing.Image)(resources.GetObject("buttonClear.Image")));
			this.buttonClear.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.buttonClear.Location = new System.Drawing.Point(290, 2);
			this.buttonClear.Margin = new System.Windows.Forms.Padding(2);
			this.buttonClear.Name = "buttonClear";
			this.buttonClear.Size = new System.Drawing.Size(108, 37);
			this.buttonClear.TabIndex = 8;
			this.buttonClear.Text = "Clear";
			this.buttonClear.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.buttonClear.UseVisualStyleBackColor = false;
			this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
			// 
			// buttonClipboard
			// 
			this.buttonClipboard.BackColor = System.Drawing.Color.DarkSlateBlue;
			this.buttonClipboard.Dock = System.Windows.Forms.DockStyle.Right;
			this.buttonClipboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonClipboard.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.buttonClipboard.ForeColor = System.Drawing.SystemColors.ControlLightLight;
			this.buttonClipboard.Image = ((System.Drawing.Image)(resources.GetObject("buttonClipboard.Image")));
			this.buttonClipboard.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.buttonClipboard.Location = new System.Drawing.Point(138, 2);
			this.buttonClipboard.Margin = new System.Windows.Forms.Padding(2);
			this.buttonClipboard.Name = "buttonClipboard";
			this.buttonClipboard.Size = new System.Drawing.Size(124, 37);
			this.buttonClipboard.TabIndex = 10;
			this.buttonClipboard.Text = "Clipboard";
			this.buttonClipboard.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.buttonClipboard.UseVisualStyleBackColor = false;
			this.buttonClipboard.Click += new System.EventHandler(this.buttonClipboard_Click);
			// 
			// panel15
			// 
			this.panel15.Dock = System.Windows.Forms.DockStyle.Right;
			this.panel15.Location = new System.Drawing.Point(262, 2);
			this.panel15.Margin = new System.Windows.Forms.Padding(2);
			this.panel15.Name = "panel15";
			this.panel15.Size = new System.Drawing.Size(28, 37);
			this.panel15.TabIndex = 9;
			// 
			// ReqEnumForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.ClientSize = new System.Drawing.Size(697, 337);
			this.Controls.Add(this.panel12);
			this.Controls.Add(this.panel14);
			this.Controls.Add(this.panel13);
			this.Controls.Add(this.panel11);
			this.Controls.Add(this.panel9);
			this.Controls.Add(this.panel10);
			this.Controls.Add(this.panel4);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.panel5);
			this.Margin = new System.Windows.Forms.Padding(2);
			this.Name = "ReqEnumForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "ReqEnumForm";
			this.Shown += new System.EventHandler(this.ReqEnumForm_Shown);
			this.panel4.ResumeLayout(false);
			this.panel11.ResumeLayout(false);
			this.panel12.ResumeLayout(false);
			this.panel14.ResumeLayout(false);
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button buttonUndo;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.ToolTip toolTip;
		private System.Windows.Forms.Panel panel10;
		private System.Windows.Forms.Panel panel9;
		private System.Windows.Forms.Panel panel11;
		private System.Windows.Forms.Label labelName;
		private System.Windows.Forms.Panel panel12;
		private System.Windows.Forms.ListBox listBoxValue;
		private System.Windows.Forms.Panel panel13;
		private System.Windows.Forms.Panel panel14;
		private System.Windows.Forms.Label labelUnit;
		private System.Windows.Forms.Button buttonClipboard;
		private System.Windows.Forms.Panel panel15;
		private System.Windows.Forms.Button buttonClear;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Panel panel2;
	}
}