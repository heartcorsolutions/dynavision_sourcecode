﻿// CSqlEnumRow 
// by Simon Vlaar 
// can be used by Techmedic Development B.V
// created 1 November 2016
//
// Basic functions for a enum list of {<code>,<label>} pair
// read fom a SqlTable or from a file
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Program_Base;
using System.Windows.Forms;
using System.IO;

namespace Program_Base
{
    public enum DSqlEnumVars
    {
        // primary keys
        // variables
        Group = 0,
        Sort,
        Code,
        FloatValue,
        Label,
        NrVars  // keep as last
    }

    public class CSqlEnumRow:CSqlDataTableRow
    {
        public Int32 _mGroup;
        public Int32 _mSort;
        public string _mCode;
        public float _mFloatValue;
        public string _mLabel;
        
        public CSqlEnumRow(CSqlDBaseConnection ASqlConnection, string ATableName):base(ASqlConnection, ATableName)
        {
            UInt64 maskPrimary = CSqlDataTableRow.sGetMaskRange((UInt16)DSqlEnumVars.Code, (UInt16)DSqlEnumVars.Code);
            mInitTableVarRange((UInt16)DSqlEnumVars.NrVars, maskPrimary);
            mClear();
        }

        public override CSqlDataTableRow mCreateNewRow()
        {
            return new CSqlEnumRow(mGetSqlConnection(), mGetTableNameOnly());
        }
        public override bool mbCopyTo(CSqlDataTableRow ATo) // derived  public override void mbCopyTo() must call base.mbCopyTo(ATo)
        {
            bool bOk = false;

            if (ATo != null)
            {
                CSqlEnumRow to = ATo as CSqlEnumRow;

                if (to != null)
                {
                    base.mbCopyTo(ATo);
                    to._mGroup = _mGroup;
                    to._mSort = _mSort;
                    to._mCode = _mCode;
                    to._mFloatValue = _mFloatValue;
                    to._mLabel = _mLabel;
                }
            }
            return bOk;
        }
        public override void mClear()
        {
            base.mClear();
            _mGroup = 0;
            _mSort = 0;
            _mCode = "";
            _mFloatValue = 0.0F;
            _mLabel = "";
        }
        public int mGetIntValue()
        {
            return (int)(_mFloatValue + 0.5F);
        }
        public override DSqlDataType mVarGetSet(DSqlDataCmd ACmd, UInt16 AVarIndex, ref string AVarSqlName, ref string AStringValue, out bool ArbValid)
        {
            // todo omzetten naar DSqlDataType mVarGetSet(DSqlDataCmd ACmd, UInt16 AVarIndex, string AParameter, ref string AStringValue)
            switch ((DSqlEnumVars)AVarIndex)
            {
                // primary keys
                // variables
                case DSqlEnumVars.Group:
                    return mSqlGetSetInt32(ref _mGroup, "GroupValue", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DSqlEnumVars.Sort:
                    return mSqlGetSetInt32(ref _mSort, "SortValue", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DSqlEnumVars.Code:
                    return mSqlGetSetString(ref _mCode, "Code", ACmd, ref AVarSqlName, ref AStringValue, 16, out ArbValid);
                case DSqlEnumVars.FloatValue:
                    return mSqlGetSetFloat(ref _mFloatValue, "Floatvalue", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DSqlEnumVars.Label:
                    return mSqlGetSetString(ref _mLabel, "Label", ACmd, ref AVarSqlName, ref AStringValue, 250, out ArbValid);
            }
            return base.mVarGetSet(ACmd, AVarIndex, ref AVarSqlName, ref AStringValue, out ArbValid);
        }
        public string mCombineCodeLabel( string AGlue)
        {
            return _mCode + AGlue + _mLabel;
        }

    }

    public class CSqlEnumList
    {
        private List<CSqlEnumRow> _mList;
        private CSqlDBaseConnection _mSqlConnection;
        private string _mEnumTableName;

        public CSqlEnumList(string AEnumTableName)
        {
            _mList = new List<CSqlEnumRow>();

            _mEnumTableName = "e_" + AEnumTableName;    // all enum tables start with "e_"
        }

        public void mClear()
        {
            if( _mList != null )
            {
                _mList.Clear();
            }
        }
        public UInt16 mCount()
        {
            UInt16 n = 0;

            if (_mList != null)
            {
               n =  (UInt16)_mList.Count;
            }
            return n;
        }


        public string mGetEnumTableName()
        {
            return _mEnumTableName;
        }

        public List<CSqlEnumRow>mGetEnumList()
        {
            return _mList;
        }
        public CSqlEnumRow mGetEnumNode( UInt16 AIndex )
        {
            CSqlEnumRow node = null;

            if( _mList != null && AIndex < _mList.Count)
            {
                node = _mList[AIndex];
            }
            return node;

        }

        public void mLogEnumList()
        {
            
            if (_mList != null)
            {
                int n = _mList.Count;
                CProgram.sLogLine("List " + _mEnumTableName + " has " + n.ToString() + " enums.");

                CProgram.sLogLine("Act.\tSort\tGroup\tValue\tCode\tLabel" + _mEnumTableName + " has " + n.ToString() + " enums.");

                foreach ( CSqlEnumRow row in _mList)
                {
                    string s = row.mbActive.ToString() + "\t" + row._mSort + "\t" + row._mGroup + "\t" + row._mFloatValue.ToString( "G6" ) 
                        + "\t" + row._mCode + "\t" + row._mLabel;

                    CProgram.sLogLine(s);
                }
            }
        }

        public void mSaveEnumList(CSqlDBaseConnection ASqlConnection)
        {

            if (_mList != null && ASqlConnection != null)
            {
                int n = _mList.Count;
                string fileName = _mEnumTableName + ".csv";

                try
                {
                    string s = _mEnumTableName+"_Key,Active,CreatedUTC,CreatedBy_IX,CreatedDevice_IX,CreatedProgID,ChangedUTC,ChangedBy_IX,ChangedDevice_IX,ChangedProgID,SortValue,GroupValue,FloatValue,Code,Label ";
                    UInt32 i, index= 0;

                    CProgram.sLogLine("List " + _mEnumTableName + " with " + n.ToString() + " enums saving to " + fileName);



                    using (StreamWriter file = new StreamWriter(fileName))
                    {
                        file.WriteLine(s);

                        foreach (CSqlEnumRow row in _mList)
                        {
                            try
                            {
                                i = row.mIndex_KEY;
                                if (i == 0) i = ++index;
                                    
                                // e_studytype_Key,Active,CreatedUTC,CreatedBy_IX,CreatedDevice_IX,CreatedProgID,ChangedUTC,ChangedBy_IX,ChangedDevice_IX,ChangedProgID,SortValue,GroupValue,FloatValue,Code,Label 
                                s = i.ToString() + "," + (row.mbActive ? "1" : "0") + ",";
                                s += "2016-11-18 15:15:58" /*ASqlConnection.mSqlDateTimeSqlString(row.mCreatedUTC)*/ + "," + row.mCreatedBy_IX.ToString() + "," + row.mCreatedDevice_IX.ToString() + "," + row.mCreatedProgID.ToString() + ",";
                                s += "2016-11-18 15:15:58" /*ASqlConnection.mSqlDateTimeSqlString(row.mChangedUTC)*/ + "," + row.mChangedBy_IX.ToString() + "," + row.mChangedDevice_IX.ToString() + "," + row.mChangedProgID.ToString() + ",";
                                s += ASqlConnection.mSqlFloatSqlString(row._mSort) + "," + row._mGroup.ToString() + ",";
                                s += ASqlConnection.mSqlFloatSqlString(row._mFloatValue) + ",\"" + row._mCode.ToString() + "\",\"" + row._mLabel + "\"";

                                file.WriteLine(s);
                            }
                            catch (Exception ex)
                            {
                                CProgram.sLogException("Write line of enum table " + _mEnumTableName, ex);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Write error to file  " + fileName, ex);
                }
            }
        }


/*        public void mStoreEnumListInDB(CSqlDBaseConnection ASqlConnection)
        {

            try
            {
                _mSqlConnection = ASqlConnection;
                CSqlEnumRow rec = new CSqlEnumRow(_mSqlConnection, _mEnumTableName);

                if (rec == null || _mList == null || _mSqlConnection == null)
                {
                    CProgram.sLogLine("Failed to init rec");
                }
                else
                {
                    if (_mList != null)
                    {
                        int n = _mList.Count;

                        foreach (CSqlEnumRow row in _mList)
                        {
                            if( row. mbDoSqlSelect( row.mIndex_KEY))
                            {
                                rec.mbCopyFrom(row);

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed store table in sql dBase", ex);
            }
            finally
            {
            }

        }
*/
        

        public bool mbLoadFromDb(CSqlDBaseConnection ASqlConnection, bool AbLogOk )
        {
            bool bOk = false;
            bool bError = true;
            CSqlCmd cmd = null;
            int n = 0;
            CSqlEnumRow rec;

            try
            {
                _mSqlConnection = ASqlConnection;
                rec = new CSqlEnumRow(_mSqlConnection, _mEnumTableName);

                if (rec == null || _mList == null || _mSqlConnection == null)
                {
                    CProgram.sLogLine("Failed to init rec");
                }
                else
                {
                    cmd = rec.mCreateSqlCmd();

                    _mList.Clear();

                    if (cmd == null)
                    {
                        CProgram.sLogLine("Failed to init cmd");

                    }
                    else
                    {
                        UInt64 testFlags = CSqlDataTableRow.sGetMask((UInt16)DSqlDataTableColum.Active);
                        rec.mbActive = true;

                        cmd.mbSetOrderBy((UInt16)DSqlEnumVars.Sort, DSqlSort.Ascending);

                        if (cmd.mbDoSelectCmd(testFlags, true, rec.mMaskValid))
                        {
                            bOk = true;

                            while (rec != null && cmd.mbReadIntoRow(out bError, rec))
                            {
                                _mList.Add(rec);               // add row to list
                                ++n;
                                rec = new CSqlEnumRow(_mSqlConnection, _mEnumTableName);        // create new row for next record
                            }
                        }

                        cmd.mCloseCmd();
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed read sql table", ex);
            }
            finally
            {
                if( cmd != null )
                {
                    cmd.mCloseCmd();
                }
            }
            if( bOk )
            {
                if (AbLogOk)
                {
                    CProgram.sLogLine("Enum Table " + _mEnumTableName + " contains " + n.ToString() + " labels." + (bError ? " !read error!" : ""));
                }
            }
            else 
            {
                CProgram.sLogLine("Failed reading Enum Table " + _mEnumTableName + "!");
            }
            return bOk;
        }

        public UInt16 mSplitLine( string ALine, UInt16 ANrColumns, ref string[] ArStringArray )
        {
            UInt16 nrValues = 0;
            int lineLen;

            if( ALine != null && (lineLen = ALine.Length) > 0)
            {
                ArStringArray = ANrColumns >= 1 ? new string[ANrColumns] : null;

                if (ArStringArray != null)
                {
                    if (ANrColumns == 1)
                    {
                        CProgram.sTrimString(ALine);
                        if (ALine.Length > 0) ++nrValues;
                    }
                    else
                    {
                        int iStart = 0, iEnd, iColumn = 0;
                        char c;

                        while (lineLen > 0 && ALine[lineLen - 1] <= ' ') --lineLen; // remove trailing spaces etc.

                        while( iColumn < ANrColumns)
                        {
                            while (iStart < lineLen && ALine[iStart] <= ' ') ++iStart; // remove spaces etc. in front

                            if (iColumn + 1 == ANrColumns)
                            {
                                // copy rest of the line into the last substring

                                if (iStart < lineLen)
                                {
                                    ArStringArray[iColumn] = ALine.Substring(iStart, lineLen - iStart);
                                    ++nrValues;
                                }
                                else
                                {
                                    ArStringArray[iColumn] = "";
                                }
                            }
                            else
                            {
                                iEnd = iStart;
                                while (iEnd < lineLen)
                                {
                                    c = ALine[iEnd];

                                    if (Char.IsLetterOrDigit(c) || c == '_' || c == '-' || c == '.' || c == ',' || c == '$')
                                    {
                                        ++iEnd; //walk past label
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }

                                if( iEnd > iStart )
                                {
                                    ArStringArray[iColumn] = ALine.Substring(iStart, iEnd - iStart);
                                    ++nrValues;

                                }
                                else
                                {
                                    ArStringArray[iColumn] = "";
                                }
                                iStart = iEnd;
                                while (iStart < lineLen)
                                {
                                    c = ALine[iStart];
                                    if (c <= ' ' || c == ';' || c == '/' || c == '\\' || c == '=')
                                    {
                                        ++iStart; // walk by seperator char ';'
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                            }
                            ++iColumn;
                        }
                    }
                }

            }
            return nrValues;
        }


        public bool mbLoadFromFile( string AEnumPath, UInt16 ANrColumns)
        {
            bool bOk = false;
            int n = 0;

            try
            {
                string enumPath;

                if( AEnumPath != null && AEnumPath.Length > 0)
                {
                    enumPath = AEnumPath;
                }
                else
                {
                    enumPath = Path.Combine(CProgram.sGetProgDir(), "enums");

                }
                string filePath = Path.Combine(enumPath, _mEnumTableName + ".txt");
  

                if (_mList == null)
                {
                    CProgram.sLogLine("No list");
                }
                else
                {
                    _mList.Clear();
                    bOk = true;

                    using (StreamReader fs = new StreamReader(filePath))
                    {
                        if (fs == null)
                        {
                            CProgram.sLogLine("Failed to open file for enum " + _mEnumTableName);
                        }
                        else
                        {
                            string line;
                            string[] columns = null;
                            int active, sort, group, lastGroup = 0;
                            float value;
                            string code, label;
                            CSqlEnumRow row;
                            UInt16 nrVars;
                            bool bLine;

                            while (null != (line = fs.ReadLine()))
                            {
                                line = CProgram.sTrimString(line);
                                if (line != null && line.Length > 1 )
                                {
                                    
                                    if( line[0] == '\'' || line[0] == '/' || line[0] == '\\' || line[0]=='#' || line[0] == ';' )
                                    {
                                        // skip remark line
                                    }
                                    else if( (nrVars = mSplitLine( line, ANrColumns, ref columns )) >= 2  && columns != null )
                                    {
                                        bLine = false;
                                        active = sort = group = 0;
                                        value = 0.0F;
                                        code = "";
                                        label = "";
                                        if( nrVars == 2 )
                                        {
                                            code = columns[0];
                                            label = columns[1];
                                            if( code != null && code.Length > 0 )
                                            {
                                                active = 1;
                                                group = lastGroup;
                                                if ( code[0] == '$')
                                                {
                                                    value = ++lastGroup;
                                                    group = 0;
                                                }
                                            }
                                            bLine = true;

                                        } else if( nrVars == 6)
                                        {
                                            active = 1;
                                            int.TryParse(columns[0], out active);
                                            int.TryParse(columns[1], out sort);
                                            int.TryParse(columns[2], out group);
                                            CProgram.sbParseFloat(columns[3], ref value);
                                            code = columns[4];
                                            label = columns[5];
                                            bLine = true;
                                        }
                                        else
                                        {
                                            bOk = false;
                                            CProgram.sLogLine( "Invalid enum line:" + line );
                                            active = 0;
                                        }

                                        if (bLine)
                                        {
                                            row = new CSqlEnumRow(_mSqlConnection, _mEnumTableName);        // create new row for next record
                                            if (row != null)
                                            {
                                                row.mbActive = active != 0;
                                                row._mSort = sort;
                                                row._mGroup = group;
                                                row._mFloatValue = value;
                                                row._mCode = code;
                                                row._mLabel = label;
                                                _mList.Add(row);               // add row to list
                                                ++n;
                                            }

                                        }

                                    }
                                }    
                            }
                        }
                        fs.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed read sql table", ex);
            }
            finally
            {
            }
            if (bOk)
            {
                CProgram.sLogLine("Enum File Table " + _mEnumTableName + " contains " + n.ToString() + " labels." );
            }
            else
            {
                CProgram.sLogLine("Failed reading Enum File Table " + _mEnumTableName + "!");
            }
            return bOk;
        }

        public bool mbLoadFromFileCvs(string AEnumPath )
        {
            bool bOk = false;
            bool bError = true;

            int n = 0;


            try
            {
                string enumPath;

                if (AEnumPath != null && AEnumPath.Length > 0)
                {
                    enumPath = AEnumPath;
                }
                else
                {
                    enumPath = Path.Combine(CProgram.sGetProgDir(), "enums");

                }
                string filePath = Path.Combine(enumPath, _mEnumTableName + ".txt");


                if (_mList == null)
                {
                    CProgram.sLogLine("No list");
                }
                else
                {
                    _mList.Clear();

                    using (StreamReader fs = new StreamReader(filePath))
                    {
                        if (fs == null)
                        {
                            CProgram.sLogLine("Failed to open file for enum " + _mEnumTableName);
                        }
                        else
                        {
                            string line;
                            int active, sort, group;
                            string code, label;
                            CSqlEnumRow row;

                            bError = false;
                            while (null != (line = fs.ReadLine()))
                            {
                                line = CProgram.sTrimString(line);
                                if (line != null && line.Length > 1 && Char.IsDigit(line[0]))
                                {
                                    // skip empty or header line (first value = [0|1])

                                    string[] values = line.Split('\t');

                                    if (values != null)
                                    {
                                        active = sort = group = 0;
                                        code = "";
                                        label = "";

                                        if (values.Length == 2)
                                        {
                                            active = 1;
                                            code = values[0];
                                            label = values[1];

                                        }
                                        else if (values.Length == 5)
                                        {
                                            active = 1;
                                            int.TryParse(values[0], out active);
                                            int.TryParse(values[1], out sort);
                                            int.TryParse(values[2], out group);
                                            code = values[3];
                                            label = values[4];
                                        }
                                        if (code.Length > 0 && label.Length > 0)
                                        {
                                            row = new CSqlEnumRow(_mSqlConnection, _mEnumTableName);        // create new row for next record
                                            if (row != null)
                                            {
                                                _mList.Add(row);               // add row to list
                                                ++n;
                                            }

                                        }
                                        else
                                        {
                                            bError = true;
                                        }
                                    }
                                }
                            }
                        }
                        fs.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed read sql table", ex);
            }
            finally
            {
            }
            if (bOk)
            {
                CProgram.sLogLine("Enum File Table " + _mEnumTableName + " contains " + n.ToString() + " labels." + (bError ? " !read error!" : ""));
            }
            else
            {
                CProgram.sLogLine("Failed reading Enum File Table " + _mEnumTableName + "!");
            }
            return bOk;
        }

        public CSqlEnumRow mGetRow( string ACode )
        {
            if( _mList != null && ACode != null && ACode.Length > 0)
            {
                foreach( CSqlEnumRow row in _mList)
                {
                    if( row._mCode == ACode )
                    {
                        return row;
                    }
                }
            }
            return null;
        }

        public string mGetLabel(string ACode)
        {
            string label = null;

            if (_mList != null && ACode != null && ACode.Length > 0)
            {
                foreach (CSqlEnumRow row in _mList)
                {
                    if (row._mCode == ACode)
                    {
                        label = row._mLabel;
                        break;
                    }
                }
            }

            return label;
        }

        public string mGetGroupName(Int32 AGroupNr)
        {
            string groupName = "?" + AGroupNr.ToString() + "?";

            if (_mList != null && AGroupNr > 0)
            {
                foreach (CSqlEnumRow row in _mList)
                {
                    if (row._mGroup == 0 && AGroupNr == row.mGetIntValue())
                    {
                        groupName = row._mLabel;
                        break;
                    }
                }
            }
            return groupName;
        }



        public string mGetCode(string ALabel)
        {
            string code = null;

            if (_mList != null && ALabel != null && ALabel.Length > 0)
            {
                foreach (CSqlEnumRow row in _mList)
                {
                    if (row._mLabel == ALabel)
                    {
                        code = row._mCode;
                        break;
                    }
                }
            }

            return code;
        }

        public float mGetFloatValue(string ACode)
        {
            float value = 0;

            if (_mList != null && ACode != null && ACode.Length > 0)
            {
                foreach (CSqlEnumRow row in _mList)
                {
                    if (row._mCode == ACode)
                    {
                        value = row._mFloatValue;
                        break;
                    }
                }
            }

            return value;
        }
        public int mGetIntValue(string ACode)
        {
            int value = 0;

            if (_mList != null && ACode != null && ACode.Length > 0)
            {
                foreach (CSqlEnumRow row in _mList)
                {
                    if (row._mCode == ACode)
                    {
                        value = (int)( row._mFloatValue + 0.49999F );
                        break;
                    }
                }
            }

            return value;
        }

        public string mGetCodeFromCombine(string ACodeLabel, string AGlue)
        {
            string code = null;

            if (_mList != null && ACodeLabel != null && ACodeLabel.Length > 0)
            {
                foreach (CSqlEnumRow row in _mList)
                {
                    if (row.mCombineCodeLabel( AGlue ) == ACodeLabel)
                    {
                        code = row._mCode;
                        break;
                    }
                }
            }

            return code;
        }
        public string mFillString(CStringSet ASet, bool AbIncludeCode, string AGlue, bool AbIncludeLabel, string ANextSeperator)
        {
            string result = "";


            if (ASet != null && ASet.mbIsNotEmpty() && _mList != null)
            {
                string code;
                UInt16 n = ASet.mCount();

                for (UInt16 i = 0; i < n; ++i)
                {
                    code = ASet.mGetValue(i);

                    if (i > 0) result += ANextSeperator;
                    if (AbIncludeCode)
                    {
                        result += code + AGlue;
                    }
                    if( AbIncludeLabel )
                    {
                        result += mGetLabel(code);
                    }
                }
            }
            return result;
        }
        public bool mFillComboBox( ComboBox AComboBox, bool AbGroupOnly, Int32 AGroupValue, bool AbIncludeCode, string AGlue, string APreSelectCode, string AFirstLine )
        {
            bool bOk = false;

            if( AComboBox != null)
            {
                string s, selected = "";

                bool bFirst = AFirstLine != null && AFirstLine.Length > 0;
                bool bPre = APreSelectCode != null && APreSelectCode.Length > 0;
                bool bPreFound = false;

                AComboBox.BeginUpdate();
                AComboBox.Items.Clear();
                if( bFirst)
                {
                    AComboBox.Items.Add(AFirstLine);
                    if( bPre == false )
                    {
                        selected = AFirstLine;
                    }
                } 

                foreach (CSqlEnumRow row in _mList)
                {
                    if( false == AbGroupOnly || AGroupValue == row._mGroup)
                    {
                        s = AbIncludeCode ? row.mCombineCodeLabel( AGlue ) : row._mLabel;

                        if (s == null || s.Length == 0)
                        {
                            CProgram.sLogError("Empty label in enum" + mGetEnumTableName() );
                        }
                        else
                        {
                            AComboBox.Items.Add(s);
                        }
                        if ( bPre && row._mCode == APreSelectCode )
                        {
                            selected = s;
                            bPreFound = true;
                        }
                    }
                }
                if( bPreFound || bFirst )
                {
                    AComboBox.Text = selected;
                }
                AComboBox.EndUpdate();
                bOk = false == bPre || bPreFound;
            }

            return bOk;
        }
        public bool mFillStringList(List<String> AStringList, bool AbGroupOnly, Int32 AGroupValue, bool AbIncludeCode, string AGlue, string APreSelectCode)
        {
            bool bOk = false;

            if (AStringList != null)
            {
                string s, selected = "";

                bool bPre = APreSelectCode != null && APreSelectCode.Length > 0;
                bool bPreFound = false;

 
                foreach (CSqlEnumRow row in _mList)
                {
                    if (false == AbGroupOnly || AGroupValue == row._mGroup)
                    {
                        s = AbIncludeCode ? row.mCombineCodeLabel(AGlue) : row._mLabel;

                        if (s == null || s.Length == 0)
                        {
                            CProgram.sLogError("Empty label in enum" + mGetEnumTableName());
                        }
                        else
                        {
                            AStringList.Add(s);
                        }
                        if (bPre && row._mCode == APreSelectCode)
                        {
                            selected = s;
                            bPreFound = true;
                        }
                    }
                }
                bOk = false == bPre || bPreFound;
            }

            return bOk;
        }


        public string mReadComboBoxCode(ComboBox AComboBox)
        {
            string code = null;

            if (AComboBox != null)
            {
                string text = AComboBox.Text;

                if (text != null && text.Length > 0)
                {
                    code = mGetCode(text);
                }
            }
            return code;
        }

        public string mReadComboBoxCode(ComboBox AComboBox, bool AbIncludeCode, string AGlue)
        {
            string code = null;
            
            if( AComboBox != null )
            {
                string text = AComboBox.Text;

                if( text != null && text.Length > 0 )
                {
                    code = AbIncludeCode ? mGetCodeFromCombine(text, AGlue) : mGetCode(text);
                }
            }
            return code;
        }

        public bool mFillCheckedListBox(CheckedListBox ACheckedListBox, bool AbGroupOnly, Int32 AGroupValue, bool AbIncludeCode, string AGlue, CStringSet APreSelectSet, bool AbSetOnly)
        {
            bool bOk = false;

            if (ACheckedListBox != null)
            {
                string s;
                int nList = 0;
                int nFound = 0;

                bool bPre = APreSelectSet != null && false == APreSelectSet.mbIsEmpty();
                int nPre = bPre ? APreSelectSet.mCount() : 0;
                bool bInSet;

                ACheckedListBox.BeginUpdate();
                ACheckedListBox.Items.Clear();

                foreach (CSqlEnumRow row in _mList)
                {
                    if (false == AbGroupOnly || AGroupValue == row._mGroup)
                    {
                        bInSet = bPre && APreSelectSet.mbContainsValue(row._mCode);

                        if (AbSetOnly == false || bInSet)
                        {
                            s = AbIncludeCode ? row.mCombineCodeLabel(AGlue) : row._mLabel;

                            if (s == null || s.Length == 0)
                            {
                                CProgram.sLogError("Empty label in enum" + mGetEnumTableName());
                            }
                            else
                            {
                                int index = ACheckedListBox.Items.Add(s);

                                if (bPre && APreSelectSet.mbContainsValue(row._mCode) && index >= 0)
                                {
                                    ACheckedListBox.SetItemChecked(index, true);
                                    ++nFound;
                                }
                                ++nList;
                            }
                        }
                    }
                }
                ACheckedListBox.EndUpdate();
/*                nList = 0;
                foreach (CSqlEnumRow row in _mList)
                {
                    if (false == AbGroupOnly || AGroupValue == row._mGroup)
                    {
                        bInSet = bPre && APreSelectSet.mbContainsValue(row._mCode);

                        if (AbSetOnly == false || bInSet)
                        {
                            s = AbIncludeCode ? row.mCombineCodeLabel(AGlue) : row._mLabel;

                            //                                ACheckedListBox.Items.Add(s);

                            if (bPre && APreSelectSet.mbContainsValue(row._mCode))
                            {
                                ACheckedListBox.SetSelected(nList, true);
                                ++nFound;
                            }
                            ++nList;
                        }
                    }
                }
*/                bOk = false == bPre || nFound == nPre;
            }
            return bOk;
        }

        public int mReadCheckedListBoxSet(CheckedListBox ACheckedListBox, bool AbIncludeCode, string AGlue, CStringSet ArStringSet)
        {
            int n = -1;

            if (ACheckedListBox != null && ACheckedListBox.Items != null && ArStringSet != null )
            {
                int nList = ACheckedListBox.Items.Count;
                n = 0;
                ArStringSet.mClear();

                for( int i = 0; i < nList; ++ i )
                {
                    if(ACheckedListBox.GetItemChecked( i ))
                    {
                        if( ACheckedListBox.Items[i] != null )
                        {
                            string text = ACheckedListBox.Items[i].ToString();

                            if (text != null && text.Length > 0)
                            {
                                string code = AbIncludeCode ? mGetCodeFromCombine(text, AGlue) : mGetCode(text);

                                ArStringSet.mAddValue(code);
                                ++n;
                            }
                        }
                    }
                }
           }
           return n;
        }
        public bool mFillListView(ListView AListView, bool AbGroupOnly, Int32 AGroupValue,bool AbShowCode,  CStringSet APreSelectSet, bool AbSetOnly )
        {
            bool bOk = false;

            if (AListView != null)
            {
                  int nList = 0;
                int nFound = 0;

                bool bPre = APreSelectSet != null && false == APreSelectSet.mbIsEmpty();
                int nPre = bPre ? APreSelectSet.mCount() : 0;
                bool bInSet;

                string allCodesList = "";
                CStringSet shownSet = new CStringSet();

                AListView.BeginUpdate();
                AListView.Items.Clear();

                foreach (CSqlEnumRow row in _mList)
                {
                    if (false == AbGroupOnly || AGroupValue == row._mGroup)
                    {
                        if (shownSet != null && shownSet.mbContainsValue(row._mCode))
                        {
                            allCodesList += "!" + row._mCode + "!,";    // double in full list => do not show double 
                        }
                        else
                        {
                            allCodesList += row._mCode+ ",";
                            bInSet = bPre && APreSelectSet.mbContainsValue(row._mCode);

                            if (AbSetOnly == false || bInSet)
                            {
                                string name = AbShowCode ? row._mCode : row._mLabel;
                                if (name == null || name.Length == 0)
                                {
                                    CProgram.sLogError("Empty line in enum" + mGetEnumTableName());
                                }
                                else
                                {
                                    ListViewItem item = new ListViewItem(name);

                                    if (item != null)
                                    {
                                        if (AbShowCode)
                                        {
                                            if (row._mLabel == null || row._mLabel.Length == 0)
                                            {
                                                CProgram.sLogError("Empty label in enum" + mGetEnumTableName());
                                            }
                                            else
                                            {
                                                item.SubItems.Add(row._mLabel);
                                            }
                                        }
                                        if (bInSet)
                                        {
                                            item.Checked = true;
                                            ++nFound;
                                        }
                                        AListView.Items.Add(item);
                                        ++nList;
                                        if (shownSet != null )
                                        {
                                            shownSet.mAddValue(row._mCode); // show code once
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                AListView.EndUpdate();
                bOk = false == bPre || nFound == nPre;
            }
            return bOk;
        }

        public int mReadListViewSet(ListView AListView, bool AbShowedCode, CStringSet ArStringSet)
        {
            int n = -1;

            if (AListView != null && AListView.Items != null && ArStringSet != null)
            {
                int nList = AListView.Items.Count;
                n = 0;
                ArStringSet.mClear();

                for (int i = 0; i < nList; ++i)
                {
                    if( AListView.Items[i].Checked)
                    {
                        string code = AListView.Items[i].Text;

                        if( AbShowedCode == false )
                        {
                            string label = code;

                            code = mGetCode(label);
                        }
                        ArStringSet.mAddValue(code);
                        ++n;
                    }
                }
            }
            return n;
        }

        /*      
               Properties.Settings.Default.SqlServer
         *      
         *        public bool mbAddSqlDbRow(out int ArIndex, CRecordMit ARec)
                {
                    bool bOk = false;
                    int index = 0;

                    if (ARec != null)
                    {
                        try
                        {
                            if (mbCreateDBaseConnection())
                            {
                                CSqlCmd cmd = ARec.mGetSqlCmd(mDBaseServer);

                                if (cmd == null)
                                {
                                    CProgram.sLogLine("Failed to init cmd");

                                }
                                else if (false == cmd.mbPrepareInsertCmd(ARec.mMaskValid, true, true))
                                {
                                    CProgram.sLogLine("Failed to prepare SQL row in table " + ARec.mGetDbTableName());
                                }
                                else if (cmd.mbExecuteInsertCmd())
                                {
                                    index = (int)ARec.mIndex_KEY;   // return new key
                                    bOk = true;
                                    CProgram.sLogLine("Successfull insert in " + ARec.mGetDbTableName() + "[" + ARec.mIndex_KEY + "]");
                                }
                                else
                                {
                                    CProgram.sLogLine("Failed to add SQL row in table " + ARec.mGetDbTableName());
                                }
                                cmd.mCloseCmd();
                            }
                        }
                        catch (Exception ex)
                        {
                            CProgram.sLogException("Failed add Row", ex);
                        }
                    }
                    ArIndex = index;
                    mTriageUpdateReset();
                    return bOk;
                }
                        public bool mbTriageReadSqlDbTable()
                {
                    bool bOk = false;
                    bool bCursor = false;
                    try
                    {
                        int nrHours = 48;
                        bool bDays = false;
                        DateTime dt = DateTime.Now;

                        mShowBusy(true);
                        //int unit = listBoxViewUnit.SelectedIndex; werkt niet
                        // string unitStr = listBoxViewUnit.SelectedItem != null ? listBoxViewUnit.SelectedItem.ToString() : "";
                        int unit = listBoxViewUnit.TopIndex;

                        if (int.TryParse(textBoxViewTime.Text, out nrHours) && nrHours > 0)
                        {
                            if (unit == 1)
                            {
                                nrHours *= 24;  // days
                            }

                        }
                        else
                        {
                            nrHours = unit == 1 ? 7 * 24 : 48;
                        }

                        if (mbCreateDBaseConnection())
                        {
                            CRecordMit rec = new CRecordMit();

                            if (rec == null)
                            {
                                CProgram.sLogLine("Failed to init cmd");
                            }
                            else
                            {
                                CSqlCmd cmd = rec.mGetSqlCmd(mDBaseServer);

                                if (cmd == null)
                                {
                                    CProgram.sLogLine("Failed to init cmd");

                                }
                                else if (false == cmd.mbPrepareSelectCmd(true, rec.mMaskValid))
                                {
                                    CProgram.sLogLine("Failed to prepare SQL row in table " + rec.mGetDbTableName());
                                }
                                else
                                {
                                    DateTime utc = CProgram.sGetUtcNow();
                                    string name = rec.mVarGetName((UInt16)DSqlRecordVars.ReceivedUTC);
                                    string cmp = rec.mSqlDateTimeCompareSqlString(name, utc, DSqlCompareDateTime.Hour, nrHours);
                                    UInt64 mask = cmd.mGetCmdVarFlags();
                                    string sqlWhere = mMakeTriageSearchString(rec);
                                    int n = 0;
                                    double loadTime = mImageCach != null ? mImageCach.mGetMaxUpdateSec() : 4.0;
                                    int nLoadDirect = 1;
                                    int nDirect = 0;
                                    bool bLoadDirect;
                                    double dbTime;

                                    if (mImageCach != null) mImageCach.mSetDataGrid(null, 0, 0);
                                    dataGridTriage.Rows.Clear();
                                    mbUpdatingGrid = true;

                                    cmd.mWhereAddString(cmp);

                                    if (sqlWhere != null && sqlWhere.Length > 0)
                                    {
                                        cmd.mWhereAddString(sqlWhere);
                                    }
                                    if(radioButtonCurrentPatient.Checked )
                                    {
                                        if( mCurrentPatientID != null && mCurrentPatientID.Length > 0 )
                                        {
                                            rec.mPatientID.mbSetEncrypted(mCurrentPatientID);

                                            string varName = rec.mVarGetName((UInt16)DSqlRecordVars.PatientID_ES);
                                            string s = varName + "='" + rec.mPatientID.mGetEncrypted() + "'";

                                            cmd.mWhereAddString(s );
                                        }
                                    }

                                    if (cmd.mbExecuteSelectCmd())
                                    {
                                        bool bError;
                                        bOk = true;

                                        bCursor = true;
                                        dataGridTriage.UseWaitCursor = true;


                                        //                                string header, line;
                                        //                                rec.mbGetVarNames(out header, mask, ", ");
                                        //                                CProgram.sLogLine("#, " + header);                        

                                        while (cmd.mbReadOneRow(out bError))
                                        {
                                            bLoadDirect = n < nLoadDirect || (DateTime.Now - dt).TotalSeconds < loadTime;
                                            if (bLoadDirect) ++nDirect;
                                            //                                   rec.mbGetVarValues(out line, mask, ", ");
                                            mbTriageStoreGrid(bLoadDirect, rec, false);
                                            ++n;
                                            //                                   CProgram.sLogLine( cmd.mGetNrRowsRead().ToString() +", " + line);
                                            rec.mClear(); // clear for next read
                                        }
                                        bOk = false == bError;
                                        dbTime = (DateTime.Now - dt).TotalSeconds;

                                        ushort signalIndex = 0;
                                        string strIndex = comboBoxTriageChannel.Text;
                                        if (strIndex != null && strIndex.Length > 1) strIndex = strIndex.Substring(0, 1);

                                        if (false == ushort.TryParse(strIndex, out signalIndex))
                                        {
                                            signalIndex = 1;
                                        }
                                        if (signalIndex > 0) --signalIndex;     // program starts at 0, user at 1

                                        // Data grid has been made, enable thread update enable 
                                        if (mImageCach != null) mImageCach.mSetDataGrid(dataGridTriage, (Int32)DTriageGridField.Index, (Int32)DTriageGridField.EventStrip);

                                        nDirect = 0;
                                        for (uint i = 0; i < n; ++i)
                                        {
                                            bLoadDirect = i < nLoadDirect || (DateTime.Now - dt).TotalSeconds < loadTime;
                                            if (bLoadDirect) ++nDirect;

                                            mUpadteTriageRowStrips(i, bLoadDirect, i == 0, signalIndex, true);

                                            if (i == 0)
                                            {
                                                UInt16 nrThreads;
                                                mImageCach.mTryLoadImages(out nrThreads);
                                            }
                                        }

                                        TimeSpan ts = DateTime.Now - dt;
                                        CProgram.sLogLine((bOk ? "" : "!Incomplete ") + "Updated " + n.ToString() + " table rows in " + ts.TotalSeconds.ToString("0.000")
                                                        + " sec. (nDirect= " + nDirect.ToString() + ", db= " + dbTime.ToString("0.000") + " sec)");
                                    }
                                    else
                                    {
                                        CProgram.sLogLine("Failed to search SQL row in table " + rec.mGetDbTableName() + " {?=" + cmd.mGetWhereString() + "}");
                                    }
                                    labelTriageRowCount.Text = n.ToString();
                                }
                                cmd.mCloseCmd();
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException("Failed read sql table", ex);
                    }
                    finally
                    {
                        if (bCursor)
                        {
                            dataGridTriage.UseWaitCursor = false;
                        }
                        mbUpdatingGrid = false;
                        mUpdateTriageStrips();
                        mShowBusy(false);
                    }
                    return bOk;

                }

        */
    }
}
