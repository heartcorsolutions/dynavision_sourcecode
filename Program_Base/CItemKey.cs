﻿// CFileIO
// by Simon Vlaar 
// can be used by Techmedic Development B.V
// created 20 Febrary 2018
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Program_Base
{
	// item file <collection>.ItemKey: 
	// $center=<centername
	// $collection=<collection>
	// $writen: yyyy mmm dd hh mm ss
	// $day0: <end date yyyymmmdd>
	// - <old>
	// +<Name>==<value1>[, value][, value][, value][, value]
	// +<Name>=<end date [0|yyyymmdd],<group>,<var1>,<var2>,<var3>,...
	// +<Name>=<end date [0|yyyymmdd],<group>,<var1>,<var2>,<var3>,...
	// $lines=<nr lines>
	// #<checksum XXXXXXXXXXXXXXXX>
	public class CItemKey
	{
		private const UInt16 _sCheckSumStartShift = 55;
		public const char _cCharParam = '$';
		public const char _cCharEnd = '&';
		public const char _cCharValid = '+';
		public const char _cCharOld = '-';

        public const char _cCharSplit = ';';    // multible parameters spliyt char (do not use ',' because this could be a decimal point
        public const string _cStrSplit = ";";


        public const string _cParamCenter = "center";
		public const string _cParamCollection = "collection";
		public const string _cParamWritten = "written";
		public const string _cParamDay0 = "day0";
		private const string _cParamNrLines = "nrLines";

		public static string _sLineCenter = _cCharParam + _cParamCenter + "=";
		public static string _sLineCollection = _cCharParam + _cParamCollection + "=";
		public static string _sLineWritten = _cCharParam + _cParamWritten + "=";
		public static string _sLineDay0 = _cCharParam + _cParamDay0 + "=";
		public static string _sLineNrLines = _cCharParam + _cParamNrLines + "=";

		private string _mCollection;
		private List<string> _mLinesList = null;

		public CItemKey( string AGroupName )
		{
			_mCollection = AGroupName;

			_mLinesList = new List<string>();
		}

		public static string sMakeItemKeyFileName( string ACollection )
		{
			return ACollection + ".ItemKey";
		}
		public static string sMakeItemListFileName( string ACollection )
		{
			return ACollection + ".ItemList";
		}

		public List<string> mGetLinesList()
		{
			return _mLinesList;
		}
		public bool mbInitCheckSum( ref UInt64 ArCheckSum, ref UInt32 ArLineNr, ref UInt16 ArIndex, UInt64 ASeed, string ACenterName )
		{
			bool bOk = ACenterName != null && ACenterName.Length > 0 && _mCollection != null && _mCollection.Length > 0;

			ArCheckSum = 0;
			ArLineNr = 0;
			ArIndex = 0;

			if (bOk)
			{
				ArCheckSum = ASeed;
				ArIndex = (UInt16)(ASeed % _sCheckSumStartShift);

				mCalcCheckSum( ref ArCheckSum, ref ArLineNr, ref ArIndex, "--" + _mCollection + "@@" + ACenterName + "<<" );
			}

			return bOk;
		}


		public void mCalcCheckSum( ref UInt64 ArCheckSum, ref UInt32 ArLineNr, ref UInt16 ArIndex, string ALine )
		{
			//			bool bNext = true;

			if (ALine != null)
			{
				int len = ALine.Length;

				if (len > 0)
				{
					for (int j = 0; j < len; ++j)
					{
						char c = ALine[j];
						UInt64 m = ((UInt64)(UInt16)c) << ArIndex;

						ArCheckSum ^= m;

						ArIndex = ArIndex == 0 ? _sCheckSumStartShift : (UInt16)(ArIndex - 1);

					}
					++ArLineNr;
					ArCheckSum += ArLineNr;

				}
			}
		}

        public bool mbExtractParamValue(out string ArParamName, out string ArValue, string ALine)
        {
            bool bParam = false;
            string param = "";
            string value = "";

            if (ALine != null && ALine.Length > 0 && ALine[0] == _cCharParam)
            {
                int i = ALine.IndexOf('=');

                if (i > 0)
                {
                    param = ALine.Substring(1, i - 1);
                    value = ALine.Substring(i + 1);
                    bParam = true;
                }
            }
            ArParamName = param;
            ArValue = value;
            return bParam;
        }
        public bool mbExtractItemValue(out string ArParamName, out string ArValue, string ALine)
        {
            bool bParam = false;
            string param = "";
            string value = "";

            if (ALine != null && ALine.Length > 0 && ( ALine[0] == _cCharValid || Char.IsLetterOrDigit( ALine[0])))
            {
                int i = ALine.IndexOf('=');

                if (i > 0)
                {
                    param = ALine.Substring(1, i - 1);
                    value = ALine.Substring(i + 1);
                    bParam = true;
                }
            }
            ArParamName = param;
            ArValue = value;
            return bParam;
        }

        public bool mbGetParamString( out string ArValue, string AParamName )
		{
			bool bFound = false;
			string s = "";

			if (_mLinesList != null && AParamName != null)
			{
				string search = _cCharParam + AParamName + "=";

				foreach (string line in _mLinesList)
				{
					if (line != null && line.StartsWith( search ))
					{
						s = line.Substring( search.Length );
						bFound = true;
						break;
					}
				}
			}

			ArValue = s;
			return bFound;
		}
        public bool mbGetParamUInt32(out UInt32 ArValue, string AParamName)
        {
            string s = "";
            UInt32 value = 0;
            bool bFound = mbGetParamString(out s, AParamName);

            if (bFound)
            {
                bFound = UInt32.TryParse(s, out value);
            }
            ArValue = value;
            return bFound;
        }

        public bool mbGetItemString( ref string ArValue, string AItemName )
		{
			bool bFound = false;
			string s = "";

            if (_mLinesList != null && AItemName != null)
            {
                string search = _cCharValid + AItemName + "=";

                foreach (string line in _mLinesList)
                {
                    if (line != null && line.StartsWith(search))
                    {
                        s = line.Substring(search.Length);
                        bFound = true;
                        break;
                    }
                }
                if (bFound)
                {
                    ArValue = s;
                }
			}
			
			return bFound;
		}

		public string[] mGetItemValues( string AItemName )
		{
			string[] values = null;
			string s = "";

			if (mbGetItemString( ref s, AItemName ))
			{
				if (s != null && s.Length > 0)
				{
					values = s.Split(_cCharSplit);
				}
			}
			return values;
		}

        public bool mbAddItemString(string AItemName, string AValue)
        {
            bool bOk = AItemName != null && AItemName.Length > 0 && Char.IsLetterOrDigit(AItemName[0]);

            if (bOk && _mLinesList != null)
            {
                string value = AValue == null ? "" : AValue.Trim();
                _mLinesList.Add(_cCharValid + AItemName.Trim() + "=" + value);
            }
            return bOk;
        }
        public bool mbAddItemString2(string AItemName, string AValue1, string AValue2)
        {
            bool bOk = AItemName != null && AItemName.Length > 0 && Char.IsLetterOrDigit(AItemName[0]);

            if (bOk && _mLinesList != null)
            {
                string value1 = AValue1 == null ? "" : AValue1.Trim();
                string value2 = AValue2 == null ? "" : AValue2.Trim();
                _mLinesList.Add(_cCharValid + AItemName.Trim() + "=" + value1 +_cStrSplit+value2);
            }
            return bOk;
        }
        public bool mbAddItemString3(string AItemName, string AValue1, string AValue2, string AValue3)
        {
            bool bOk = AItemName != null && AItemName.Length > 0 && Char.IsLetterOrDigit(AItemName[0]);

            if (bOk && _mLinesList != null)
            {
                string value1 = AValue1 == null ? "" : AValue1.Trim();
                string value2 = AValue2 == null ? "" : AValue2.Trim();
                string value3 = AValue3 == null ? "" : AValue3.Trim();
                _mLinesList.Add(_cCharValid + AItemName.Trim() + "=" + value1 + _cStrSplit + value2 + _cStrSplit + value3);
            }
            return bOk;
        }
        public bool mbAddItemString4(string AItemName, string AValue1, string AValue2, string AValue3, string AValue4)
        {
            bool bOk = AItemName != null && AItemName.Length > 0 && Char.IsLetterOrDigit(AItemName[0]);

            if (bOk && _mLinesList != null)
            {
                string value1 = AValue1 == null ? "" : AValue1.Trim();
                string value2 = AValue2 == null ? "" : AValue2.Trim();
                string value3 = AValue3 == null ? "" : AValue3.Trim();
                string value4 = AValue4 == null ? "" : AValue4.Trim();
                _mLinesList.Add(_cCharValid + AItemName.Trim() + "=" + value1 + _cStrSplit + value2 + _cStrSplit + value3 + _cStrSplit + value4);
            }
            return bOk;
        }

        public bool mbAddItemUInt32(string AItemName, UInt32 AValue)
        {
            bool bOk = AItemName != null && AItemName.Length > 0 && Char.IsLetterOrDigit(AItemName[0]);

            if (bOk && _mLinesList != null)
            {
                _mLinesList.Add(_cCharValid + AItemName.Trim() + "=" + AValue.ToString());
            }
            return bOk;
        }
        public bool mbAddItemInt32(string AItemName, Int32 AValue)
        {
            bool bOk = AItemName != null && AItemName.Length > 0 && Char.IsLetterOrDigit(AItemName[0]);

            if (bOk && _mLinesList != null)
            {
                _mLinesList.Add(_cCharValid + AItemName.Trim() + "=" + AValue.ToString());
            }
            return bOk;
        }
        public bool mbAddItemInt16(string AItemName, Int16 AValue)
        {
            bool bOk = AItemName != null && AItemName.Length > 0 && Char.IsLetterOrDigit(AItemName[0]);

            if (bOk && _mLinesList != null)
            {
                _mLinesList.Add(_cCharValid + AItemName.Trim() + "=" + AValue.ToString());
            }
            return bOk;
        }
        public bool mbAddItemUInt16(string AItemName, UInt16 AValue)
        {
            bool bOk = AItemName != null && AItemName.Length > 0 && Char.IsLetterOrDigit(AItemName[0]);

            if (bOk && _mLinesList != null)
            {
                _mLinesList.Add(_cCharValid + AItemName.Trim() + "=" + AValue.ToString());
            }
            return bOk;
        }
        public bool mbAddItemFloat(string AItemName, float AValue)
        {
            bool bOk = AItemName != null && AItemName.Length > 0 && Char.IsLetterOrDigit(AItemName[0]);

            if (bOk && _mLinesList != null)
            {
                _mLinesList.Add(_cCharValid + AItemName.Trim() + "=" + AValue.ToString());
            }
            return bOk;
        }
        public bool mbAddItemDouble(string AItemName, double AValue)
        {
            bool bOk = AItemName != null && AItemName.Length > 0 && Char.IsLetterOrDigit(AItemName[0]);

            if (bOk && _mLinesList != null)
            {
                _mLinesList.Add(_cCharValid + AItemName.Trim() + "=" + AValue.ToString());
            }
            return bOk;
        }


        public static string sMakeDateString(DateTime ADateTime)
        {
            // YYYYMMDD
            string s = "0";

            if (ADateTime != DateTime.MinValue)
            {
                int ymd = ADateTime.Day + ADateTime.Month * 100 + ADateTime.Year * 10000;
                s = ymd.ToString();
            }
            return s;
        }
        public static string sMakeDateTimeString(DateTime ADateTime)
        {
            // YYYYMMDDTHHmmss[.fff]
            string s = "0";

            if (ADateTime != DateTime.MinValue)
            {
                int ymd = ADateTime.Day + ADateTime.Month * 100 + ADateTime.Year * 10000;
                int hms = ADateTime.Second + ADateTime.Minute * 100 + ADateTime.Hour * 10000;
                s = ymd.ToString() + "U" + hms.ToString("000000");
                if (ADateTime.Millisecond > 0)
                {
                    s += "." + ADateTime.Millisecond.ToString("000");
                }
            }
            return s;
        }
        public static string sMakeDateTimeZoneString(DateTime ADateTime, Int16 ATimeZoneMin)
        {
            // YYYYMMDDTHHmmss[.fff]{+|-}hhmm
            string s = "0";

            if (ADateTime != DateTime.MinValue)
            {
                int ymd = ADateTime.Day + ADateTime.Month * 100 + ADateTime.Year * 10000;
                int hms = ADateTime.Second + ADateTime.Minute * 100 + ADateTime.Hour * 10000;
                s = ymd.ToString() + "U" + hms.ToString("000000");
                if (ADateTime.Millisecond > 0)
                {
                    s += "." + ADateTime.Millisecond.ToString("000");
                }
                if( ATimeZoneMin >= 0)
                {
                    int h = ATimeZoneMin / 60;
                    int m = ATimeZoneMin % 60;
                    int i = h * 100 + m;
                    s += "+" + i.ToString();
                }
                else
                {
                    int h = -ATimeZoneMin / 60;
                    int m = -ATimeZoneMin % 60;
                    int i = h * 100 + m;
                    s += "-" + i.ToString();
                }
            }
            return s;
        }
        public static bool sbParseDateTimeZoneString(out DateTime ArDateTime, out Int16 ArTimeZoneMin, string AValue)
        {
            // YYYYMMDDTHHmmss[.fff]{+|-}hhmm
            // 0
            bool bOk = false;
            DateTime dt = DateTime.MinValue;
            Int16 zone = 0;

            try
            {
                if (AValue != null)
                {
                    string s = AValue.Trim();
                    int len;

                    if (s != null && (len = s.Length) > 0)

                    {
                        if (s == "0")
                        {
                            bOk = true; // no value -> MinValue
                        }
                        else
                        {
                            int ymd = 0;
                            int hms = 0;
                            int mil = 0;
                            int z = 0;
                            bool bNeg = false;

                            int i = 0;
                            char c = '\0';

                            // read YYYYMMDD
                            while (i < len)
                            {
                                c = s[i];
                                if (false == char.IsDigit(c))
                                {
                                    break;
                                }
                                ymd = ymd * 10 + (c - '0');
                                ++i;
                            }
                            if (i < len && (c == 'U' || c == 'T'))
                            {
                                ++i;
                                // read HHmmss
                                while (i < len)
                                {
                                    c = s[i];
                                    if (false == char.IsDigit(c))
                                    {
                                        break;
                                    }
                                    hms = hms * 10 + (c - '0');
                                    ++i;
                                }
                                if (i < len && c == '.')
                                {
                                    ++i;
                                    // read .fff
                                    while (i < len)
                                    {
                                        c = s[i];
                                        if (false == char.IsDigit(c))
                                        {
                                            break;
                                        }
                                        mil = mil * 10 + (c - '0');
                                        ++i;
                                    }
                                }
                                if (i < len && (c == '+' || c == '-'))
                                {
                                    bNeg = c == '-';
                                    ++i;
                                    // read zone 
                                    while (i < len)
                                    {
                                        c = s[i];
                                        if (false == char.IsDigit(c))
                                        {
                                            break;
                                        }
                                        z = z * 10 + (c - '0');
                                        ++i;
                                    }
                                }
                            }
                            int day = ymd % 100;
                            ymd /= 100;
                            int month = ymd % 100;
                            int year = ymd / 100;
                            int sec = hms % 100;
                            hms /= 100;
                            int min = hms % 100;
                            int hour = hms / 100;

                            dt = new DateTime(year, month, day, hour, min, sec, mil);

                            hour = z / 100;
                            min = z % 100;
                            zone = (Int16)(z * 60 + min);
                            if (bNeg)
                            {
                                zone = (Int16)(-zone);
                            }
                            bOk = true;
                        }
                    }
                }
            }
            catch ( Exception ex )
            {
                CProgram.sLogException("Bad date time zone " + AValue, ex);
                bOk = false;
            }
            ArDateTime = dt;
            ArTimeZoneMin = zone;
            return bOk;
        }
        public static bool sbParseDateTimeString(out DateTime ArDateTime, string AValue)
        {
            // YYYYMMDDTHHmmss[.fff]
            // 0
            Int16 zone = 0;

            bool bOk = sbParseDateTimeZoneString(out ArDateTime, out zone, AValue);

            return bOk;
        }

        public static string sMakeBoolDigitString(bool AbValue)
        {
            return AbValue ? "1" : "0";
        }
        public static string sMakeBoolTextString(bool AbValue)
        {
            return AbValue ? "true" : "false";
        }

        public static bool sbParseBoolString(out bool ArbValue, string AValue)
        {
            // { [0|f|false]|[1|t|true]}
            // 0
            bool bOk = false;
            bool value = false;

            if (AValue != null)
            {
                string s = AValue.ToLower().Trim();
                int len;

                if (s != null && (len = s.Length) > 0)
                {
                    if (s == "0" || s == "f" || s == "false")
                    {
                        bOk = true;
                    }
                    else if (s == "1" || s == "t" || s == "true")
                    {
                        value = true;
                        bOk = true;
                    }
                }
            }
            ArbValue = value;
            return bOk;
        }


        public bool mbAddItemDate(string AItemName, DateTime ADateTime)
        {
            bool bOk = AItemName != null && AItemName.Length > 0 && Char.IsLetterOrDigit(AItemName[0]);

            if (bOk && _mLinesList != null)
            {
                _mLinesList.Add(_cCharValid + AItemName.Trim() + "=" + sMakeDateString(ADateTime));
            }
            return bOk;
        }
        public bool mbAddItemDateTime(string AItemName, DateTime ADateTime)
        {
            bool bOk = AItemName != null && AItemName.Length > 0 && Char.IsLetterOrDigit(AItemName[0]);

            if (bOk && _mLinesList != null)
            {
                _mLinesList.Add(_cCharValid + AItemName.Trim() + "=" + sMakeDateTimeString(ADateTime));
            }
            return bOk;
        }
        public bool mbAddItemDateTimeZone(string AItemName, DateTime ADateTime, Int16 ATimeZoneMin)
        {
            bool bOk = AItemName != null && AItemName.Length > 0 && Char.IsLetterOrDigit(AItemName[0]);

            if (bOk && _mLinesList != null)
            {
                _mLinesList.Add(_cCharValid + AItemName.Trim() + "=" + sMakeDateTimeZoneString(ADateTime, ATimeZoneMin));
            }
            return bOk;
        }
        public bool mbAddItemBoolDigit(string AItemName, bool AbValue)
        {
            bool bOk = AItemName != null && AItemName.Length > 0 && Char.IsLetterOrDigit(AItemName[0]);

            if (bOk && _mLinesList != null)
            {
                _mLinesList.Add(_cCharValid + AItemName.Trim() + "=" + sMakeBoolDigitString(AbValue));
            }
            return bOk;
        }
        public bool mbAddItemBoolText(string AItemName, bool AbValue)
        {
            bool bOk = AItemName != null && AItemName.Length > 0 && Char.IsLetterOrDigit(AItemName[0]);

            if (bOk && _mLinesList != null)
            {
                _mLinesList.Add(_cCharValid + AItemName.Trim() + "=" + sMakeBoolTextString(AbValue));
            }
            return bOk;
        }

        public bool mbGetItemBool(ref bool ArbValue, string AItemName)
        {
            string s = "";
            bool bFound = false;
            bool value = false;

            if (mbGetItemString(ref s, AItemName))
            {
                bFound = sbParseBoolString(out value, s);
                if( bFound )
                {
                    ArbValue = value;
                }
            }
            return bFound;
        }
        public bool mbGetItemUInt32(ref UInt32 ArValue, string AItemName)
        {
            string s = "";
            bool bFound = false;
            UInt32 value = 0;

            if (mbGetItemString(ref s, AItemName))
            {
                bFound = UInt32.TryParse(s, out value);
                if (bFound)
                {
                    ArValue = value;
                }
            }
            return bFound;
        }
        public bool mbGetItemInt32(ref Int32 ArValue, string AItemName)
        {
            string s = "";
            bool bFound = false;
            Int32 value = 0;

            if (mbGetItemString(ref s, AItemName))
            {
                bFound = Int32.TryParse(s, out value);
                if (bFound)
                {
                    ArValue = value;
                }
            }
            return bFound;
        }
        public bool mbGetItemUInt16(ref UInt16 ArValue, string AItemName)
        {
            string s = "";
            bool bFound = false;
            UInt16 value = 0;

            if (mbGetItemString(ref s, AItemName))
            {
                bFound = UInt16.TryParse(s, out value);
                if (bFound)
                {
                    ArValue = value;
                }
            }
            return bFound;
        }
        public bool mbGetItemInt16(ref Int16 ArValue, string AItemName)
        {
            string s = "";
            bool bFound = false;
            Int16 value = 0;

            if (mbGetItemString(ref s, AItemName))
            {
                bFound = Int16.TryParse(s, out value);
                if (bFound)
                {
                    ArValue = value;
                }
            }
            return bFound;
        }
        public bool mbGetItemFloat(ref float ArValue, string AItemName)
        {
            string s = "";
            bool bFound = false;
            double value = 0;

            if (mbGetItemString(ref s, AItemName))
            {
                bFound = CProgram.sbParseDouble(s, ref value);
                if (bFound)
                {
                    ArValue = (float)value;
                }
            }
            return bFound;
        }
        public bool mbGetItemDouble(ref double ArValue, string AItemName)
        {
            string s = "";
            bool bFound = false;
            double value = 0;

            if (mbGetItemString(ref s, AItemName))
            {
                bFound = CProgram.sbParseDouble(s, ref value);
                if (bFound)
                {
                    ArValue = value;
                }
            }
            return bFound;
        }
        public bool mbGetItemDateTimeZone(ref DateTime ArValue, ref Int16 ArTimeZoneMin, string AItemName)
        {
            string s = "";
            bool bFound = false;
            DateTime value = DateTime.MinValue;
            Int16 zone = 0;


            if (mbGetItemString(ref s, AItemName))
            {
                bFound = sbParseDateTimeZoneString(out value, out zone, s);
                if (bFound)
                {
                    ArValue = value;
                    ArTimeZoneMin = zone;
                }
            }
            return bFound;
        }
        public bool mbGetItemDateTime(ref DateTime ArValue, string AItemName)
        {
            string s = "";
            bool bFound = false;
            DateTime value = DateTime.MinValue;
            Int16 zone = 0;


            if (mbGetItemString(ref s, AItemName))
            {
                bFound = sbParseDateTimeZoneString(out value, out zone, s);
                if (bFound)
                {
                    ArValue = value;
                }
            }
            return bFound;
        }


        public List<string> mGetValidItems( out UInt32 ArNrItemsAtDay, DateTime ATestDate, UInt16 ANrOfDays, UInt32 ADay0 )
		{
			List<string> list = new List<string>();
			UInt32 n = 0;
			UInt32 nAtDay = 0;
			DateTime dt = ATestDate;

			int curDay = dt.Year * 10000 + dt.Month * 100 + dt.Day;
			dt = dt.AddDays( ANrOfDays );
			int dayAtN = dt.Year * 10000 + dt.Month * 100 + dt.Day;

			if (list != null && _mLinesList != null)
			{
				foreach (string line in _mLinesList)
				{
					if (line != null && line.Length > 0 && line[0] == _cCharValid)
					{
						int i = line.IndexOf( '=' );

						if (i > 0)
						{
							string name = line.Substring( 1, i - 1 );
							string values = line.Substring( i + 1 );
							int day = 0;

							if (values != null && values.Length >= 1)
							{
								i = values.IndexOf( ',' );
								if (i > 0)
								{
									values = values.Substring( 0, i );
								}
								int.TryParse( values, out day );

							}
							if (day == 0)
							{
								day = (int)ADay0;
							}
							if (day >= curDay)
							{
								list.Add( name );
								++n;
								if (day >= dayAtN)
								{
									++nAtDay;
								}
							}
						}
					}
				}
			}
			ArNrItemsAtDay = nAtDay;
			return n > 0 ? list : null;
		}

		public static string sMakeParamLine( string AName, string AValue )
		{
			string s = "";

			if (AName != null && AName.Length > 0)
			{
				s = _cCharParam + AName.Trim() + "=" + (AValue == null ? "" : AValue.Trim());

			}
			return s;
		}

		public static string sMakeValidItemLine( bool AbValid, string AName, string AValue1 )
		{
			string s = "";

			if (AName != null && AName.Length > 0)
			{
				s = (AbValid ? _cCharValid : _cCharOld) + AName.Trim() + "=" + (AValue1 == null ? "" : AValue1.Trim());

			}
			return s;
		}
		public static string sMakeValidItemLine2( bool AbValid, string AName, string AValue1, string AValue2 )
		{
			string s = "";

			if (AName != null && AName.Length > 0)
			{
				s = (AbValid ? _cCharParam : _cCharOld) + AName.Trim() + "=" + (AValue1 == null ? "" : AValue1.Trim());
				s += _cStrSplit + AValue2;
			}
			return s;
		}
		public static string sMakeValidItemLine3( bool AbValid, string AName, string AValue1, string AValue2, string AValue3 )
		{
			string s = "";

			if (AName != null && AName.Length > 0)
			{
				s = (AbValid ? _cCharParam : _cCharOld) + AName.Trim() + "=" + (AValue1 == null ? "" : AValue1.Trim());
				s += _cStrSplit + AValue2;
				s += _cStrSplit + AValue3;
			}
			return s;
		}
		public static string sMakeItemLine4( bool AbValid, string AName, string AValue1, string AValue2, string AValue3, string AValue4 )
		{
			string s = "";

			if (AName != null && AName.Length > 0)
			{
				s = _cCharValid + AName.Trim() + "=" + (AValue1 == null ? "" : AValue1.Trim());
				s += _cStrSplit + AValue2;
				s += _cStrSplit + AValue3;
				s += _cStrSplit + AValue4;

			}
			return s;
		}
		public static string sMakeChecksumLine( UInt64 ACheckSum )
		{
			return _cCharEnd + ACheckSum.ToString( "X16" );
		}

		public bool mbReadFromFile( out bool ArbChecksumOk, string AFilePath, UInt64 ASeed, string ACenterName, bool AbCheckStrict )
		{
			bool bOk = false;
			bool bChecksumOk = false;

			try
			{
                if (_mLinesList == null)
                {
                    _mLinesList = new List<string>();
                }
                else
                { 
                    _mLinesList.Clear();
                }

				string[] lines = File.ReadAllLines( AFilePath );
				UInt32 nItems = 0;
				UInt32 nLines = 0;

				UInt16 index = 0;

				UInt64 checkSum = 0;
				UInt64 fileCheckSum = 0;
				bool bEndLine = false;

				if (lines != null && mbInitCheckSum( ref checkSum, ref nLines, ref index, ASeed, ACenterName ))
				{
					for (int i = 0; i < lines.Length; ++i)
					{
						string line = lines[i];

						if (line != null && line.Length > 0)
						{
							line = line.Trim();
							char c = line[0];

							if (c == _cCharEnd)
							{
								// last line and checksum

								fileCheckSum = Convert.ToUInt64( line.Substring( 1 ), 16 );
								bEndLine = true;
							}
							else
							{
								mCalcCheckSum( ref checkSum, ref nLines, ref index, line );
								_mLinesList.Add( line );
							}
						}
					}
					if (bEndLine)
					{
						string center, collection, nrLines;
						UInt32 iLines = 0;

						if (mbGetParamString( out center, _cParamCenter )
							&& mbGetParamString( out collection, _cParamCollection )
							&& mbGetParamString( out nrLines, _cParamNrLines ))
						{
							if (UInt32.TryParse( nrLines, out iLines ))
							{
								++iLines;
								if (center == ACenterName && collection == _mCollection && iLines == nLines)
								{
									bOk = true;
									bChecksumOk = fileCheckSum == checkSum;

									if (false == bChecksumOk && AbCheckStrict)
									{
										bOk = false;
									}
								}
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				CProgram.sLogException( "Failed read " + _mCollection + " Items " + AFilePath, ex );
			}
			ArbChecksumOk = bChecksumOk;

			if (AbCheckStrict && bOk == false)
			{
				_mLinesList = null;
			}
			return bOk;
		}

		public List<string> mReadValidItemsWithEndDay( string AFilePath, UInt64 ASeed, string ACenterName,
						out UInt32 ArNrItemsAtDay, DateTime ATestDate, UInt16 ANrOfDays, UInt32 ADay0 )

		{
			UInt32 nAtDay = 0;
			List<string> strList = null;

			try
			{
				UInt32 fileDay0 = 0;

				bool bCheckSumOk = false;
				bool bCont = mbReadFromFile( out bCheckSumOk, AFilePath, ASeed, ACenterName, true );

				if( bCont && false == bCheckSumOk)
				{
					bCont = false;
				}
				if( bCont && false == mbGetParamUInt32( out fileDay0, CItemKey._cParamDay0 ))
				{
					bCont = false;
				}
				if (false == bCont)
				{
					CProgram.sLogError( "failed read "+ _mCollection + " list: " + AFilePath );
					_mLinesList.Clear();
				}
				else 
				{
					strList = mGetValidItems( out nAtDay, ATestDate, ANrOfDays, fileDay0 );
				}
			}
			catch (Exception ex)
			{
				CProgram.sLogException( "Failed read " + _mCollection + " list: " + AFilePath, ex );
				strList = null;
			}
			ArNrItemsAtDay = nAtDay;
			return strList;
		}

		public bool mbIsNotReservedLine( string ALine )
		{
			return ALine != null && ALine.Length > 0
				&& false == ALine.StartsWith( _sLineCenter )
				&& false == ALine.StartsWith( _sLineCollection )
				&& false == ALine.StartsWith( _sLineWritten )
				&& false == ALine.StartsWith( _sLineDay0 )
				&& false == ALine.StartsWith( _sLineNrLines )
				&& _cCharEnd != ALine[0];
		}
		public bool mbWriteToFile( string AFilePath, UInt64 ASeed, string ACenterName, bool AbWriteInvalid, UInt32 ADay0 )
		{
			bool bOk = false;

			try
			{
				if( ACenterName != null && ACenterName.Length > 0 && ASeed != 0 && AFilePath != null && AFilePath.Length > 0 )
				{
					StreamWriter fs = new StreamWriter( AFilePath );

					if( fs != null )
					{
						string line;
						string lineCenter = _cCharParam + _cParamCenter + "=";
						string lineCollection = _cCharParam + _cParamCollection + "=";
						string lineCDay0 = _cCharParam + _cParamDay0 + "=";
						string lineNrLines = _cCharParam + _cParamNrLines + "=";

						UInt16 index = 0;
						UInt32 nLines = 0;

						UInt64 checkSum = 0;
	
						if ( mbInitCheckSum( ref checkSum, ref nLines, ref index, ASeed, ACenterName ))
						{
							line = sMakeParamLine( _cParamCenter, ACenterName );
							mCalcCheckSum( ref checkSum, ref nLines, ref index, line );
							fs.WriteLine( line );

							line = sMakeParamLine( _cParamCollection, _mCollection );
							mCalcCheckSum( ref checkSum, ref nLines, ref index, line );
							fs.WriteLine( line );

							line = sMakeParamLine( _cParamWritten, CProgram.sDateTimeToYMDHMS(DateTime.UtcNow ));
							mCalcCheckSum( ref checkSum, ref nLines, ref index, line );
							fs.WriteLine( line );

							if( ADay0 > 0)
							{
								line = sMakeParamLine( _cParamDay0, ADay0.ToString() );
								mCalcCheckSum( ref checkSum, ref nLines, ref index, line );
								fs.WriteLine( line );
							}

							foreach( string str in _mLinesList )
							{
								if(mbIsNotReservedLine(str))
								{
									if(AbWriteInvalid || str[0] == _cCharValid || _cCharParam == str[0])
									{
										mCalcCheckSum( ref checkSum, ref nLines, ref index, str );
										fs.WriteLine( str );
									}
								}
							}
							line = sMakeParamLine( _cParamNrLines, nLines.ToString() );
							mCalcCheckSum( ref checkSum, ref nLines, ref index, line );
							fs.WriteLine( line );

							line = sMakeChecksumLine( checkSum );
							fs.WriteLine( line );
							fs.Close();
							bOk = true;
						}
					}
				}
			}
			catch (Exception ex)
			{
				CProgram.sLogException( "Failed write " + _mCollection + " Items " + AFilePath, ex );
			}
			return bOk;
		}

		public bool mbSplitItem( string ALine, out char ArCodeChar, out string ArName, out string ArValue )
		{
			bool bOk = false;
			char codeChar = '\0';
			string name = "";
			string value = "";

			if( ALine != null )
			{
				string line = ALine.Trim();

				if( line != null && line.Length>0)
				{
					int i = line.IndexOf( '=' );

					if( i > 0 )
					{
						value = line.Substring( i + 1 ).Trim();	// split value and name
						line = line.Substring( 0, i ).Trim();
					}
					if (line != null && line.Length > 0)
					{
						char c = line[0];

						if (Char.IsLetterOrDigit( c ))
						{
							// line starts with a name
							name = line;
							bOk = true;
						}
						else if (_cCharOld == c || _cCharParam == c || _cCharValid == c)
						{
							codeChar = c;
							name = line.Substring( 1 );
							bOk = true;
						}
						else
						{
							codeChar = c;
						}
					}
				}
			}
			ArCodeChar = codeChar;
			ArName = name;
			ArValue = value;

			return bOk;
		}
	}
}
