﻿// ReqLabelFormy
// by Simon Vlaar 
// can be used by Techmedic Development B.V
// created 10 September 2016
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Program_Base
{
    public enum DReqVarType
    { Text, Int, Double
    }

    public partial class ReqLabelForm : Form
    {
        public Int32 mIntValue, mIntMin, mIntMax, mIntOrg;
        public double mDoubleValue, mDoubleMin, mDoubleMax, mDoubleOrg;
        public string mFormat;
        public string mStringValue, mStringOrg;
        public DReqVarType mType = DReqVarType.Text;
        public bool mbResult;

        public ReqLabelForm()
        {
            InitializeComponent();

            textBoxValue.Focus();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            mbResult = false;
            Close();
        }

        private void buttonUndo_Click(object sender, EventArgs e)
        {
            textBoxValue.Text = mStringOrg;
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            switch( mType)
            {
                case DReqVarType.Text:
                    mStringValue = textBoxValue.Text;
                    mbResult = true;
                    break;
                case DReqVarType.Int:
                    if( Int32.TryParse(textBoxValue.Text, out mIntValue))
                    {
                        if (mIntValue < mIntMin) mIntValue = mIntMin;
                        else if( mIntValue > mIntMax ) mIntValue = mIntMax;
                        else mbResult = true;
                    }
                    if( mbResult == false )
                    {
                        textBoxValue.Text = mIntValue.ToString();
                    }
                    break;
                case DReqVarType.Double:
                    if (CProgram.sbParseDouble(textBoxValue.Text, ref mDoubleValue))
                    {
                        if (mDoubleValue < mDoubleMin) mDoubleValue = mDoubleMin;
                        else if (mDoubleValue > mDoubleMax) mDoubleValue = mDoubleMax;
                        else mbResult = true;
                        if (mbResult == false)
                        {
                            textBoxValue.Text = mDoubleValue.ToString(mFormat);
                        }
                    }
                    break;
            }
            if( mbResult)
            {
                Close();
            }
        }

        private void textBoxValue_KeyPress(object sender, KeyPressEventArgs e)
        {
            bool b = (e.KeyChar >= '0' && e.KeyChar <= '9') || e.KeyChar == '-' || e.KeyChar == '+';
            switch (mType)
            {
                case DReqVarType.Int:
                     break;
                case DReqVarType.Double:
                    if( false == b )
                    {
                        b = (e.KeyChar == 'e' || e.KeyChar == 'E' || e.KeyChar == '.' || e.KeyChar == ',');
                    }
                    break;
                case DReqVarType.Text:
                    b = e.KeyChar >= ' ';
                    break;
            }
            if( false == b )
            {
                if(e.KeyChar == (char)Keys.Back )
                {

                }
                else
                {
                    if (e.KeyChar == (char)Keys.Return)
                    {
                        if (textBoxValue.Text.Length > 0)
                        {
                            buttonOk_Click(sender, e);
                        }
                    }
                    e.KeyChar = (char)0;
                    e.Handled = true;
                }
            }
        }

        private void ReqLabelForm_Shown(object sender, EventArgs e)
        {
            textBoxValue.Focus();
            textBoxValue.SelectAll();
        }

		private void buttonClear_Click( object sender, EventArgs e )
		{
			textBoxValue.Text = "";
		}

		private void buttonClipboard_Click( object sender, EventArgs e )
		{
			try
			{
				textBoxValue.Text = Clipboard.GetText();
			}
			catch (Exception)
			{

			}
		}

        private void buttonToClipboard_Click(object sender, EventArgs e)
        {
            try
            {
                Clipboard.SetText(textBoxValue.Text);
            }
            catch (Exception)
            {

            }

        }

        private void panel11_Paint(object sender, PaintEventArgs e)
        {

        }

        public void mInitText( string AVarName, string AText, string AUnit, bool AbPassword )
        {
            mType = DReqVarType.Text;
            mStringOrg = AText;
            mStringValue = AText;
            mbResult = false;

            textBoxValue.PasswordChar = AbPassword ? '*': '\0';
            textBoxValue.UseSystemPasswordChar = false;
            buttonToClipboard.Visible = false == AbPassword;

            labelName.Text = AVarName + " = ";
            labelUnit.Text = AUnit;
            textBoxValue.Text = AText;
            toolTip.SetToolTip(textBoxValue, "");
            textBoxValue.Focus();

        }
        public bool mbGetTextResult( ref string ArText)
        {
            if( mbResult )
            {
                ArText = mStringValue;
            }
            return mbResult;
        }
        public void mInitInt(string AVarName, Int32 AInt, string AUnit, Int32 AMin, Int32 AMax)
        {
            mType = DReqVarType.Int;
            mIntOrg = AInt;
            mIntValue = AInt;
            mIntMin = AMin;
            mIntMax = AMax;
            mbResult = false;

            textBoxValue.PasswordChar = '\0';
            buttonToClipboard.Visible = true;

            if (mIntValue < mIntMin) mIntValue = mIntMin;
            if (mIntValue > mIntMax) mIntValue = mIntMax;

            mStringValue = mIntValue.ToString();
            mStringOrg = mStringValue;

            labelName.Text = AVarName + " = ";
            labelUnit.Text = AUnit;
            textBoxValue.Text = mStringValue;
            toolTip.SetToolTip(textBoxValue, mIntMin.ToString() + " <= " + AVarName + " <= " + mIntMax.ToString());
            textBoxValue.Focus();

        }
        public bool mbGetIntResult(ref Int32 ArInt)
        {
            if (mbResult)
            {
                ArInt = mIntValue;
            }
            return mbResult;
        }
        public void mInitDouble(string AVarName, double ADouble, string AFormat, string AUnit, Double AMin, Double AMax)
        {
            mType = DReqVarType.Double;
            mDoubleOrg = ADouble;
            mDoubleValue = ADouble;
            mDoubleMin = AMin;
            mDoubleMax = AMax;
            mFormat = AFormat;
            mbResult = false;

            textBoxValue.PasswordChar = '\0';
            buttonToClipboard.Visible = true;

            if (mDoubleValue < mDoubleMin) mDoubleValue = mDoubleMin;
            if (mDoubleValue > mDoubleMax) mDoubleValue = mDoubleMax;

            mStringValue = mDoubleValue.ToString(mFormat);
            mStringOrg = mStringValue;

            labelName.Text = AVarName + " = ";
            labelUnit.Text = AUnit;
            textBoxValue.Text = mStringValue;
            toolTip.SetToolTip(textBoxValue, mDoubleMin.ToString(mFormat) + " <= " + AVarName 
                            + " <= " + mDoubleMax.ToString(mFormat));
            textBoxValue.Focus();

        }
        public bool mbGetDoubleResult(ref double ArDouble)
        { 
            if (mbResult)
            {
                ArDouble = mDoubleValue;
            }
            return mbResult;
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
