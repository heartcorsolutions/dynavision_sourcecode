﻿// ReqEnumForm
// by Simon Vlaar 
// can be used by Techmedic Development B.V
// created 19 Maart 2018
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Program_Base
{
	public partial class ReqEnumForm : Form
	{
		private static ReqEnumForm _sReqEnumForm = null;

		public UInt16 mEnumValue, mEnumOrg, mNrEnums;
		public bool mbResult;

		public ReqEnumForm()
		{
			InitializeComponent();
			listBoxValue.Focus();
		}
		public static bool sbReqEnumFromArray( string ATitle, string AVarName, string[] AEnumValues, ref UInt16 ArIndex, string AUnit )
		{
			bool bOk = false;

			if (_sReqEnumForm == null)
			{
				_sReqEnumForm = new ReqEnumForm();
			}
			if (_sReqEnumForm != null)
			{
				string title = CProgram.sMakeProgTitle( ATitle, false, true );

				_sReqEnumForm.Text = title;
				_sReqEnumForm.mInitEnum( AVarName, AEnumValues, ArIndex, AUnit );

				_sReqEnumForm.ShowDialog();

				bOk = _sReqEnumForm.mbGetEnumResult( ref ArIndex );
			}

			return bOk;
		}
        public static bool sbReqEnumFromSqlList(string ATitle, string AVarName, List<string> AEnumValues, ref UInt16 ArIndex, string AUnit)
        {
            bool bOk = false;

            if (_sReqEnumForm == null)
            {
                _sReqEnumForm = new ReqEnumForm();
            }
            if (_sReqEnumForm != null)
            {
                string title = CProgram.sMakeProgTitle(ATitle, false, true);

                _sReqEnumForm.Text = title;
                _sReqEnumForm.mInitEnum(AVarName, AEnumValues, ArIndex, AUnit);

                _sReqEnumForm.ShowDialog();

                bOk = _sReqEnumForm.mbGetEnumResult(ref ArIndex);
            }
            return bOk;
        }

        private void buttonCancel_Click( object sender, EventArgs e )
		{
			mbResult = false;
			Close();
		}

		private void buttonUndo_Click( object sender, EventArgs e )
		{
			listBoxValue.SelectedIndex = mEnumOrg;
			listBoxValue.Focus();
		}

		private void buttonOk_Click( object sender, EventArgs e )
		{
			mEnumValue = (UInt16)listBoxValue.SelectedIndex;
			mbResult = true;
			if (mbResult)
			{
				Close();
			}
		}

		private void textBoxValue_KeyPress( object sender, KeyPressEventArgs e )
		{
		}

		private void ReqEnumForm_Shown( object sender, EventArgs e )
		{
			listBoxValue.Focus();
		}

		public void mInitEnum( string AVarName, string[] AEnumValues, UInt16 AIndex, string AUnit )
		{
			mbResult = false;
			mNrEnums = (UInt16)(AEnumValues == null ? 0 : AEnumValues.Length);

			labelName.Text = AVarName + " = ";
			labelUnit.Text = AUnit;
			listBoxValue.Items.Clear();
			if (mNrEnums > 0)
			{
				mEnumOrg = AIndex >= mNrEnums ? (UInt16)(mNrEnums) : AIndex;
				mEnumValue = mEnumOrg;

				for (UInt16 i = 0; i < mNrEnums; ++i)
				{
					listBoxValue.Items.Add( AEnumValues[i] );

				}
				listBoxValue.SelectedIndex = mEnumValue;
			}
			listBoxValue.Focus();
		}
		public void mInitEnum( string AVarName, List<string> AEnumValues, UInt16 AIndex, string AUnit )
		{
			mbResult = false;
			mNrEnums = (UInt16)(AEnumValues == null ? 0 : AEnumValues.Count);

			labelName.Text = AVarName + " = ";
			labelUnit.Text = AUnit;
			listBoxValue.Items.Clear();
			if (mNrEnums > 0)
			{
				mEnumOrg = AIndex >= mNrEnums ? (UInt16)(mNrEnums) : AIndex;
				mEnumValue = mEnumOrg;

				for (UInt16 i = 0; i < mNrEnums; ++i)
				{
					listBoxValue.Items.Add( AEnumValues[i] );

				}
				listBoxValue.SelectedIndex = mEnumValue;
			}
			listBoxValue.Focus();
		}

		private void buttonClear_Click( object sender, EventArgs e )
		{
			listBoxValue.SelectedIndex = 0;
		}

		private void buttonClipboard_Click( object sender, EventArgs e )
		{
			try
			{
				if (mNrEnums > 0)
				{
					string clipText = Clipboard.GetText();

					if (clipText != null && clipText.Length > 0)
					{
						clipText = clipText.ToLower();

						for (UInt16 i = 0; i < mNrEnums; ++i)
						{
							string s = listBoxValue.Items[i].ToString();

							if( clipText == s.ToLower() )
							{
								listBoxValue.SelectedIndex = i;
								break;
							}
						}					
					}
				}
			}
			catch ( Exception)
			{

			}
		}

		public bool mbGetEnumResult( ref UInt16 ArIndex )
		{
			if (mbResult && mNrEnums > 0)
			{
				ArIndex = mEnumValue;
			}
			return mbResult && mNrEnums > 0;
		}

		private void textBox1_TextChanged( object sender, EventArgs e )
		{

		}
	}
}
