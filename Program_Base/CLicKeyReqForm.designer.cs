﻿namespace Program_Base
{
    partial class CLicKeyReqForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CLicKeyReqForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBoxHwKey = new System.Windows.Forms.TextBox();
            this.buttonClipboard = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.labelCopyClipboard = new System.Windows.Forms.Label();
            this.labelSandBox = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.textBoxLicKey = new System.Windows.Forms.TextBox();
            this.buttonLoadLic = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel16 = new System.Windows.Forms.Panel();
            this.labelDir = new System.Windows.Forms.Label();
            this.labelDevice = new System.Windows.Forms.Label();
            this.labelCenterName = new System.Windows.Forms.Label();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.panel15 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.labelDaysLeft = new System.Windows.Forms.Label();
            this.labelTest2 = new System.Windows.Forms.Label();
            this.labelVersionLevel = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.buttonAccept = new System.Windows.Forms.Button();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.richTextInstruct = new System.Windows.Forms.RichTextBox();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panelImage = new System.Windows.Forms.Panel();
            this.richTextBoxLicense = new System.Windows.Forms.RichTextBox();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel9.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.textBoxHwKey);
            this.panel1.Controls.Add(this.buttonClipboard);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 212);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1085, 50);
            this.panel1.TabIndex = 0;
            // 
            // textBoxHwKey
            // 
            this.textBoxHwKey.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxHwKey.Location = new System.Drawing.Point(139, 0);
            this.textBoxHwKey.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxHwKey.Multiline = true;
            this.textBoxHwKey.Name = "textBoxHwKey";
            this.textBoxHwKey.ReadOnly = true;
            this.textBoxHwKey.Size = new System.Drawing.Size(843, 50);
            this.textBoxHwKey.TabIndex = 2;
            this.textBoxHwKey.DoubleClick += new System.EventHandler(this.textBoxHwKey_DoubleClick);
            // 
            // buttonClipboard
            // 
            this.buttonClipboard.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonClipboard.Image = ((System.Drawing.Image)(resources.GetObject("buttonClipboard.Image")));
            this.buttonClipboard.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonClipboard.Location = new System.Drawing.Point(982, 0);
            this.buttonClipboard.Margin = new System.Windows.Forms.Padding(4);
            this.buttonClipboard.Name = "buttonClipboard";
            this.buttonClipboard.Size = new System.Drawing.Size(103, 50);
            this.buttonClipboard.TabIndex = 1;
            this.buttonClipboard.Text = "To \r\nClipboard";
            this.buttonClipboard.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonClipboard.UseVisualStyleBackColor = true;
            this.buttonClipboard.Click += new System.EventHandler(this.buttonClipboard_Click);
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 50);
            this.label1.TabIndex = 0;
            this.label1.Text = "Hardware Key:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.labelCopyClipboard);
            this.panel4.Controls.Add(this.labelSandBox);
            this.panel4.Controls.Add(this.panel2);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 262);
            this.panel4.Margin = new System.Windows.Forms.Padding(4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1085, 20);
            this.panel4.TabIndex = 2;
            // 
            // labelCopyClipboard
            // 
            this.labelCopyClipboard.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelCopyClipboard.Location = new System.Drawing.Point(886, 0);
            this.labelCopyClipboard.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelCopyClipboard.Name = "labelCopyClipboard";
            this.labelCopyClipboard.Size = new System.Drawing.Size(199, 20);
            this.labelCopyClipboard.TabIndex = 7;
            this.labelCopyClipboard.Text = "Hardware key on Clipboard";
            this.labelCopyClipboard.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelCopyClipboard.Visible = false;
            // 
            // labelSandBox
            // 
            this.labelSandBox.AutoSize = true;
            this.labelSandBox.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSandBox.ForeColor = System.Drawing.Color.Red;
            this.labelSandBox.Location = new System.Drawing.Point(139, 0);
            this.labelSandBox.Name = "labelSandBox";
            this.labelSandBox.Size = new System.Drawing.Size(33, 17);
            this.labelSandBox.TabIndex = 5;
            this.labelSandBox.Text = "KjK";
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(139, 20);
            this.panel2.TabIndex = 6;
            // 
            // panel5
            // 
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 198);
            this.panel5.Margin = new System.Windows.Forms.Padding(4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1085, 14);
            this.panel5.TabIndex = 3;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.textBoxLicKey);
            this.panel6.Controls.Add(this.buttonLoadLic);
            this.panel6.Controls.Add(this.label2);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 282);
            this.panel6.Margin = new System.Windows.Forms.Padding(4);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1085, 50);
            this.panel6.TabIndex = 6;
            // 
            // textBoxLicKey
            // 
            this.textBoxLicKey.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxLicKey.Location = new System.Drawing.Point(139, 0);
            this.textBoxLicKey.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxLicKey.Multiline = true;
            this.textBoxLicKey.Name = "textBoxLicKey";
            this.textBoxLicKey.Size = new System.Drawing.Size(843, 50);
            this.textBoxLicKey.TabIndex = 2;
            // 
            // buttonLoadLic
            // 
            this.buttonLoadLic.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonLoadLic.Image = ((System.Drawing.Image)(resources.GetObject("buttonLoadLic.Image")));
            this.buttonLoadLic.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonLoadLic.Location = new System.Drawing.Point(982, 0);
            this.buttonLoadLic.Margin = new System.Windows.Forms.Padding(4);
            this.buttonLoadLic.Name = "buttonLoadLic";
            this.buttonLoadLic.Size = new System.Drawing.Size(103, 50);
            this.buttonLoadLic.TabIndex = 1;
            this.buttonLoadLic.Text = "From Clipboard";
            this.buttonLoadLic.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonLoadLic.UseVisualStyleBackColor = true;
            this.buttonLoadLic.Click += new System.EventHandler(this.buttonLoadLic_Click);
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Left;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(139, 50);
            this.label2.TabIndex = 0;
            this.label2.Text = "License Key=";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // panel7
            // 
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 332);
            this.panel7.Margin = new System.Windows.Forms.Padding(4);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1085, 19);
            this.panel7.TabIndex = 7;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.panel11);
            this.panel8.Controls.Add(this.panel10);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel8.Location = new System.Drawing.Point(0, 584);
            this.panel8.Margin = new System.Windows.Forms.Padding(4);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(1085, 88);
            this.panel8.TabIndex = 8;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.panel16);
            this.panel11.Controls.Add(this.buttonCancel);
            this.panel11.Controls.Add(this.panel15);
            this.panel11.Controls.Add(this.panel14);
            this.panel11.Controls.Add(this.panel13);
            this.panel11.Controls.Add(this.buttonAccept);
            this.panel11.Controls.Add(this.panel12);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel11.Location = new System.Drawing.Point(0, 14);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(1085, 64);
            this.panel11.TabIndex = 3;
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.labelDir);
            this.panel16.Controls.Add(this.labelDevice);
            this.panel16.Controls.Add(this.labelCenterName);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel16.Location = new System.Drawing.Point(588, 0);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(320, 64);
            this.panel16.TabIndex = 7;
            // 
            // labelDir
            // 
            this.labelDir.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDir.Location = new System.Drawing.Point(0, 40);
            this.labelDir.Name = "labelDir";
            this.labelDir.Size = new System.Drawing.Size(320, 20);
            this.labelDir.TabIndex = 4;
            this.labelDir.Text = "Dir = ?C:\\DVTMS\\Data\\Recording\\";
            // 
            // labelDevice
            // 
            this.labelDevice.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDevice.Location = new System.Drawing.Point(0, 20);
            this.labelDevice.Name = "labelDevice";
            this.labelDevice.Size = new System.Drawing.Size(320, 20);
            this.labelDevice.TabIndex = 3;
            this.labelDevice.Text = "PC ID= ^10001001 MultiPC";
            // 
            // labelCenterName
            // 
            this.labelCenterName.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelCenterName.Location = new System.Drawing.Point(0, 0);
            this.labelCenterName.Name = "labelCenterName";
            this.labelCenterName.Size = new System.Drawing.Size(320, 20);
            this.labelCenterName.TabIndex = 2;
            this.labelCenterName.Text = "Center= XX0000YYYY";
            // 
            // buttonCancel
            // 
            this.buttonCancel.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonCancel.Location = new System.Drawing.Point(908, 0);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(144, 64);
            this.buttonCancel.TabIndex = 6;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // panel15
            // 
            this.panel15.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel15.Location = new System.Drawing.Point(1052, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(33, 64);
            this.panel15.TabIndex = 4;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.labelDaysLeft);
            this.panel14.Controls.Add(this.labelTest2);
            this.panel14.Controls.Add(this.labelVersionLevel);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel14.Location = new System.Drawing.Point(374, 0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(214, 64);
            this.panel14.TabIndex = 3;
            // 
            // labelDaysLeft
            // 
            this.labelDaysLeft.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDaysLeft.Location = new System.Drawing.Point(0, 40);
            this.labelDaysLeft.Name = "labelDaysLeft";
            this.labelDaysLeft.Size = new System.Drawing.Size(214, 20);
            this.labelDaysLeft.TabIndex = 2;
            this.labelDaysLeft.Text = "Days left on licence = 9999";
            // 
            // labelTest2
            // 
            this.labelTest2.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelTest2.Location = new System.Drawing.Point(0, 20);
            this.labelTest2.Name = "labelTest2";
            this.labelTest2.Size = new System.Drawing.Size(214, 20);
            this.labelTest2.TabIndex = 3;
            this.labelTest2.Text = "internal use only";
            // 
            // labelVersionLevel
            // 
            this.labelVersionLevel.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelVersionLevel.Location = new System.Drawing.Point(0, 0);
            this.labelVersionLevel.Name = "labelVersionLevel";
            this.labelVersionLevel.Size = new System.Drawing.Size(214, 20);
            this.labelVersionLevel.TabIndex = 0;
            this.labelVersionLevel.Text = "ALPHA TEST";
            // 
            // panel13
            // 
            this.panel13.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel13.Location = new System.Drawing.Point(354, 0);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(20, 64);
            this.panel13.TabIndex = 2;
            // 
            // buttonAccept
            // 
            this.buttonAccept.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonAccept.Image = ((System.Drawing.Image)(resources.GetObject("buttonAccept.Image")));
            this.buttonAccept.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAccept.Location = new System.Drawing.Point(35, 0);
            this.buttonAccept.Margin = new System.Windows.Forms.Padding(4);
            this.buttonAccept.Name = "buttonAccept";
            this.buttonAccept.Size = new System.Drawing.Size(319, 64);
            this.buttonAccept.TabIndex = 1;
            this.buttonAccept.Text = "I accept the license agreement";
            this.buttonAccept.UseVisualStyleBackColor = true;
            this.buttonAccept.Click += new System.EventHandler(this.buttonAccept_Click);
            // 
            // panel12
            // 
            this.panel12.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel12.Location = new System.Drawing.Point(0, 0);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(35, 64);
            this.panel12.TabIndex = 0;
            // 
            // panel10
            // 
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(1085, 14);
            this.panel10.TabIndex = 2;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.richTextInstruct);
            this.panel9.Controls.Add(this.panel18);
            this.panel9.Controls.Add(this.panelImage);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 14);
            this.panel9.Margin = new System.Windows.Forms.Padding(4);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(1085, 184);
            this.panel9.TabIndex = 10;
            // 
            // richTextInstruct
            // 
            this.richTextInstruct.BackColor = System.Drawing.Color.White;
            this.richTextInstruct.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextInstruct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextInstruct.Location = new System.Drawing.Point(82, 0);
            this.richTextInstruct.Name = "richTextInstruct";
            this.richTextInstruct.ReadOnly = true;
            this.richTextInstruct.Size = new System.Drawing.Size(1003, 184);
            this.richTextInstruct.TabIndex = 3;
            this.richTextInstruct.Text = "";
            this.richTextInstruct.LinkClicked += new System.Windows.Forms.LinkClickedEventHandler(this.richTextInstruct_LinkClicked);
            // 
            // panel18
            // 
            this.panel18.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel18.Location = new System.Drawing.Point(60, 0);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(22, 184);
            this.panel18.TabIndex = 1;
            // 
            // panelImage
            // 
            this.panelImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panelImage.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelImage.Location = new System.Drawing.Point(0, 0);
            this.panelImage.MinimumSize = new System.Drawing.Size(60, 0);
            this.panelImage.Name = "panelImage";
            this.panelImage.Size = new System.Drawing.Size(60, 184);
            this.panelImage.TabIndex = 0;
            // 
            // richTextBoxLicense
            // 
            this.richTextBoxLicense.BackColor = System.Drawing.Color.White;
            this.richTextBoxLicense.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBoxLicense.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBoxLicense.Location = new System.Drawing.Point(0, 351);
            this.richTextBoxLicense.Margin = new System.Windows.Forms.Padding(4);
            this.richTextBoxLicense.Name = "richTextBoxLicense";
            this.richTextBoxLicense.ReadOnly = true;
            this.richTextBoxLicense.Size = new System.Drawing.Size(1085, 233);
            this.richTextBoxLicense.TabIndex = 11;
            this.richTextBoxLicense.Text = "License Agreement";
            this.richTextBoxLicense.LinkClicked += new System.Windows.Forms.LinkClickedEventHandler(this.richTextBoxLicense_LinkClicked);
            // 
            // panel17
            // 
            this.panel17.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel17.Location = new System.Drawing.Point(0, 0);
            this.panel17.Margin = new System.Windows.Forms.Padding(4);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(1085, 14);
            this.panel17.TabIndex = 12;
            // 
            // CLicKeyReqForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1085, 672);
            this.Controls.Add(this.richTextBoxLicense);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel17);
            this.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "CLicKeyReqForm";
            this.Text = "CLicKeyReqFormcs";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBoxHwKey;
        private System.Windows.Forms.Button buttonClipboard;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TextBox textBoxLicKey;
        private System.Windows.Forms.Button buttonLoadLic;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Label labelDaysLeft;
        private System.Windows.Forms.Label labelVersionLevel;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Button buttonAccept;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.RichTextBox richTextBoxLicense;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Label labelDevice;
        private System.Windows.Forms.Label labelCenterName;
        private System.Windows.Forms.Label labelTest2;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Panel panelImage;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.RichTextBox richTextInstruct;
        private System.Windows.Forms.Label labelSandBox;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label labelCopyClipboard;
        private System.Windows.Forms.Label labelDir;
    }
}