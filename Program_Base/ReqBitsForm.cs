﻿// ReqBitsForm
// by Simon Vlaar 
// can be used by Techmedic Development B.V
// created 19 Maart 2018

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Program_Base
{
 
    public partial class ReqBitsForm : Form
    {
		private static ReqBitsForm _sReqBitsForm = null;

        public CBitSet64 _mVar, _mVarOriginal;
        CSqlEnumList _mEnumListBitLabels;
        UInt16 _mGroupOnly;

        public bool _mbResult;

        public ReqBitsForm()
        {
            _mVar = new CBitSet64();

            InitializeComponent();

            dataGridView.Focus();
        }

		public static bool sbReqBits( string ATitle, string AVarName, ref CBitSet64 ArVar, CSqlEnumList AEnumListBitLabels, UInt16 AGroupOnly = 0 )
		{
			bool bOk = false;

            if( ArVar == null)
            {
                ArVar = new CBitSet64();
            }
			if (_sReqBitsForm == null)
			{
				_sReqBitsForm = new ReqBitsForm();
			}
			if (_sReqBitsForm != null && ArVar != null && AEnumListBitLabels != null)
			{
				string title = CProgram.sMakeProgTitle( ATitle, false, true );

				_sReqBitsForm.Text = title;
                _sReqBitsForm._mGroupOnly = AGroupOnly;
                _sReqBitsForm._mVarOriginal = ArVar;
                _sReqBitsForm._mEnumListBitLabels = AEnumListBitLabels;

                string name = AVarName;

                if( AGroupOnly != 0)
                { 
                    name += "[" + _sReqBitsForm.mGetGroupName(AGroupOnly) + "]";
                }
                _sReqBitsForm.labelName.Text = name + " = ";

                _sReqBitsForm.mInitBits( ArVar);

				_sReqBitsForm.ShowDialog();

				bOk = _sReqBitsForm.mbGetBitsResult( ref ArVar );
			}
			return bOk;
		}

        public string mGetGroupName( UInt16 AGroupNr )
        {
            string groupName = "?"+AGroupNr.ToString()+"?";

            if (_mEnumListBitLabels != null)
            {
                foreach (CSqlEnumRow node in _mEnumListBitLabels.mGetEnumList())
                {
                    if (node._mGroup == 0)   // group =0: list of groups
                    {
                        int value = node.mGetIntValue();

                        if (value == AGroupNr)
                        {
                            groupName = node._mLabel;
                            break;
                        }
                    }
                }
            }
            return groupName;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            _mbResult = false;
            Close();
        }

        private void buttonUndo_Click(object sender, EventArgs e)
        {
            mInitBits(_mVarOriginal);
        }

        private bool mbReadValues()
        {
            bool bOk = false;

            // get all the bits into mVar
            if (_mEnumListBitLabels != null && _mVar != null)
            {
                _mVarOriginal.mCopyTo(ref _mVar);   // reset all data and change active group bits only

                if ((dataGridView != null && dataGridView.Rows != null))
                {
                    foreach (DataGridViewRow row in dataGridView.Rows)
                    {
                        string strEnabled =  CProgram.sGetObjectString( row.Cells[0].Value);
                        string strBitNr = CProgram.sGetObjectString( row.Cells[3].Value);

                        bool bEnabled = false;
                        UInt16 bitNr = 999;

                        if (CProgram.sbParseBool(strEnabled, ref bEnabled)
                            && UInt16.TryParse(strBitNr, out bitNr))
                        {
                            if (bEnabled)
                            {
                                _mVar.mSetOneBit(bitNr);
                            }
                            else
                            {
                                _mVar.mResetOneBit(bitNr);
                            }
                        }
                    }
                }
                bOk = true;
            }

            return bOk;
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            _mbResult = mbReadValues();
    		if ( _mbResult)
            {
                Close();
            }
        }

        private void textBoxValue_KeyPress(object sender, KeyPressEventArgs e)
        {
        }

        private void ReqBitsForm_Shown(object sender, EventArgs e)
        {
            dataGridView.Focus();
        }

        public void mInitBits( CBitSet64 ABitSet )
        {
            if( ABitSet != null )
            {
                ABitSet.mCopyTo(ref _mVar);
            }
            else
            {
                _mVar.mClear();
            }

            if(_mEnumListBitLabels != null)
            {
                dataGridView.Rows.Clear();

                foreach (CSqlEnumRow node in _mEnumListBitLabels.mGetEnumList())
                {
                    if (node._mGroup != 0 && (_mGroupOnly == 0 || _mGroupOnly == node._mGroup))   // group =0: list of groups
                    {
                        int value = node.mGetIntValue();

                        bool bEnabled = ABitSet.mbIsBitSet((UInt16)value);
                        string groupName = mGetGroupName((UInt16)node._mGroup);

                        dataGridView.Rows.Add(bEnabled, groupName, node._mLabel, value, node._mCode);
                        
                    }
                }

            }
            _mbResult = false;

            dataGridView.Focus();

        }
        public bool mbGetBitsResult( ref CBitSet64 ArBitsResult)
        {
            if( _mbResult )
            {
                _mVar.mCopyTo(ref ArBitsResult);
            }
            return _mbResult;
        }

		private void buttonClear_Click( object sender, EventArgs e )
		{
            CBitSet64 empty = new CBitSet64();

            mInitBits(empty);
		}

		private void panel15_Paint( object sender, PaintEventArgs e )
		{
		
		}

	    private void panel10_Paint( object sender, PaintEventArgs e )
		{
		}

        private void buttonSetTo_Click(object sender, EventArgs e)
        {
            if (mbReadValues())
            {
                string strValue = _mVar._mBitSet.ToString();

                if (CProgram.sbReqLabel("Edit BitSet decimal value", labelName.Text, ref strValue, "", false))
                {
                    UInt64 value = 0;

                    if (UInt64.TryParse(strValue.Trim(), out value))
                    {
                        CBitSet64 newValue = new CBitSet64();

                        if (newValue != null)
                        {
                            newValue._mBitSet = value;
                            mInitBits(newValue);
                        }
                    }
                }
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
        }
    }
}
