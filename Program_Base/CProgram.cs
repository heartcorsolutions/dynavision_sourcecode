﻿// CProgram 
// by Simon Vlaar 
// can be used by Techmedic Development B.V
// created 16 August 2016
//
// Basic functions for running program
// used for logging, errormessage, output formats for date, time and float 
// do not use: DateTime{ .UtcNow, .ToLocal, toUniversal, .ToString } .Now can be used
// do not use: float/double .ToString, TryParse         

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Globalization;
using System.Threading;
using System.Diagnostics;
using System.Net;

//using System.Runtime.CompilerServices;

/* only available from .net 4.5?
namespace System.Runtime.CompilerServices
{
    sealed class CallerMemberNameAttribute : Attribute { }
    sealed class CallerLineNumber : Attribute { };
    sealed class CallerFilePath : Attribute { };
}
*/
namespace Program_Base
{
    public enum DProgramVersionLevel
    {
        Alpha,      // test version software development, use internaly only
        Beta,       // test version for internal use and beta testers
        Demo,       // Demo version
        show,       // version for customer but it will always show the licence screen 
        Released
    }
    [Flags]
    public enum DDebugFlags     // flags for logging and prompting to console, screen, Logfile 
    {
        AllOff = 0,
        Enabled = 0x0001,       // function enabled
        Time = 0x0002,          // log time             HH:mm:ss.fff: <line>    <size>| Class.Function
        Class = 0x0004,         // log Class
        Reversed = 0x0008,      // log reversed on screen
                                //        UseBuffer = 0x0010,
        Log = 0x0100,           // log when log line (program info)
        Info = 0x0200,          // log when info message (user info)
        Warning = 0x0400,       // log when warning message
        Choice = 0x0800,        // log choise made
        Error = 0x1000,         // log when error message
        Exception = 0x2000,     // log when exceptionmessage
        Debug = 0x4000,         // log when debug message

        StandardScreen = 0x3201,
        StandardLog = 0x3F1F,
        StandardPrompt = 0x1E00,
        FullDebug = 0x7F1F
    }
    public enum DFloatDecimal
    {
        Dot,
        Comma
    }

    public enum DShowOrganisation
    {
        No,
        First,
        Before,
        End
    }
    public enum DTimeZoneShown
    {
        None,   //""
        Short,  // "-12
        Long,   //"-12:00
        Name    // "GMT" not done yet
    }
    // usage:
    class ExampleMainForm : Form
    {
        public void runMainProg(string[] AArgs)
        {
            // add next 2 lines in the Form creation after InitializeComponent();
            //CProgram.sSetProgramMainForm(this);                   // put program name in title
            //CProgram.sSetProgLogScreen(TextBoxLog, 10000, 0);     // assign log screen

            CProgram.sSetProgLogConsole(DDebugFlags.FullDebug); // AllOff);
            CProgram.sSetProgLogFile(DDebugFlags.FullDebug); // StandardLog);  // enables file logging
            CProgram.sSetProgLogScreen(DDebugFlags.FullDebug); // StandardScreen);
            CProgram.sSetPromptFlags(DDebugFlags.FullDebug); // StandardPrompt);

            if (CProgram.sbStartProgram("TestProg", 1, 1, "Developer", "SVNL", 0x12345678, 0))
            {
                try
                {
                    Application.EnableVisualStyles();                       // these two lines must come first
                    Application.SetCompatibleTextRenderingDefault(false);

                    CProgram.sSetProgTitleFormat("SV", true, DProgramVersionLevel.Released, DShowOrganisation.First, ""); // set version info in title α= alpha, β= beta, v= version
                    CProgram.sSetProgramMainForm(this, "Example program");

                    CProgram.sLogLine("running");
                    CProgram.sProgLogArguments(AArgs);

                    bool bLicOk = CLicKeyDev.sbLicenseRequest(14, false, DLicensePC.SinglePC);

                    if (bLicOk)
                    {
                        string tail = "";
                        int daysLeft = CLicKeyDev.sGetDaysLeft();
                        //                        int daysLeft = 100;
                        if (daysLeft < 7)
                        {
                            tail = daysLeft.ToString() + " days left!";
                        }
                        CProgram.sSetProgTitleFormat("SV", true, CProgram.sGetProgVersionLevel(), DShowOrganisation.First, tail); // set version info in title α= alpha, β= beta, v= version
                        CProgram.sSetProgramTitle("Example licensed program", true);

                        bool bYes;
                        do
                        {
                            if (CProgram.sbPromptYesNoCancel(false, out bYes, "Test", "Are you ok?"))
                            {
                                if (bYes)
                                {
                                    CProgram.sLogInfo("your ok.");
                                }
                                else
                                {
                                    CProgram.sLogInfo("your not ok.");
                                }
                            }
                            else
                            {
                                CProgram.sPromptWarning(true, "test ended", "You pressed Cancel!");
                            }
                        }
                        while (bYes);
                    }
                    CProgram.sLogLine("stopped running");
                }
                catch (Exception Ex)
                {
                    CProgram.sPromptException(false, "MainForm", "Uncaught Exception", Ex);
                }
                CProgram.sEndProgram();
            }
        }
    }

    // error trace location by automaticly added compile data ( do not fill in _AutoLN, _AutoFN, _AutoCN )  
    //       _AutoLN = source Line Number       [System.Runtime.CompilerServices.CallerLineNumber]
    //       _AutoMN = function method name     [System.Runtime.CompilerServices.CallerMemberName]
    //       _AutoCN = path to Class             [System.Runtime.CompilerServices.CallerFilePath]
    //  end parameter list logXXX() and promptXXX with 
    /*
                    [System.Runtime.CompilerServices.CallerLineNumber]int _AutoLN = 0, 
                    [System.Runtime.CompilerServices.CallerMemberName]string _AutoFN = "", 
                    [System.Runtime.CompilerServices.CallerFilePath] string _AutoCN = "" )
    */
    //    [System.Runtime.CompilerServices.CallerLineNumber]int _AutoLN = 0, [CallerMemberName]string _AutoFN = "", [CallerFilePath] string _AutoCN = "" )
    //              [CallerLineNumber]int _AutoLN, [CallerMemberName]string _AutoFN, [CallerFilePath]_AutoCN )
    class CClassFuncLine                // combine _AutoLN, _AutoMN, _AutoCN into a string
                                        // used only internaly in this file
    {                                   // made to distinguise strings as arguments and to combine once
        private string mClassPath;
        private string mFuncName;
        private int mLineNumber;
        private string mCFL;            // combination <Class>.<Func>:<line>

        public CClassFuncLine(int ASourceLineNumber, string AFuncName, string AClassPath)
        {
            mClassPath = AClassPath;
            mFuncName = AFuncName;
            mLineNumber = ASourceLineNumber;
        }
        public string mGetString()
        {
            if (String.IsNullOrEmpty(mCFL))
            {
                if (mClassPath != null)
                {
                    mCFL = Path.GetFileNameWithoutExtension(mClassPath);
                }
                if (mFuncName != null) mCFL += "." + mFuncName;
                if (mLineNumber > 0) mCFL += "@" + mLineNumber.ToString();
            }
            return mCFL;     // combination <Class>.<Func>:<line>
        }
    }

    /*    public class CDateTime      create own DateTime class to force use of the correct functions?
        {
            private DateTime mDateTime;
            CDateTime()
            {
                mDateTime = DateTime.SpecifyKind( DateTime.MinValue, DateTimeKind.Utc );
            }

            public static CDateTime mGetLocalNow()
            {
                return CProgram.sGetLocalNow();
            }
            public override string ToString()
            {
                mDateTime.

                    mGetShowString();

            }
        }
    */

    class C_ProgLog                     // used only in this file 
    {                                   // name C_ so that class is not in list when finding CProgram

        public bool mbReady = false;    // function is ready to be executed (screen cq file pressent
        public DDebugFlags mFlags = 0;  // Debug control flags
        public int mLineSize = 80;      // print message minum line width to allign | <class>.<funtion>
        public int mEntered = 0;        // reentry protection main function

        public int mBufferEntered = 0;  // reentry protection for buffer
        public string mBuffer = null;
        public int mBufferMaxSize = 1000; // maximum buffer size (chars)

        public void clearAll()
        {
            mEntered = mBufferEntered = 0;
            mBuffer = null;
        }
        public void mAddLineToBuffer(string ALineCR)
        {
            if (mBufferEntered > 0)
            {
                System.Threading.Thread.Sleep(40);  // entry is locked just wait until other thread leaves
            }
            if (++mBufferEntered == 1)   // enter only when first
            {
                try
                {
                    if (mBuffer == null || mBuffer.Length < mBufferMaxSize)
                    {
                        if ((mFlags & DDebugFlags.Reversed) != 0)     // add line to buffer;
                        {
                            mBuffer = ALineCR + mBuffer;
                        }
                        else
                        {
                            mBuffer += ALineCR;
                        }
                    }
                }
                catch (Exception /*Ex*/ )
                {
                    // just disregard memory errors
                }
            }
            //else no access, just disregard line
            --mBufferEntered;
        }

        public delegate bool FnDoLog(string AString);      // returns if line could be send to log device
        public void mbDoLog(FnDoLog AFnDoLog, DDebugFlags AMessageType, char AMarkerChar,
                    string ALine, CClassFuncLine AClassFuncLine, DateTime ANow)
        {
            if ((mFlags & DDebugFlags.Enabled) != 0     // if enabled
                && (mFlags & AMessageType) != 0)
            {
                string line = mbCreateLine(AMarkerChar, ALine, AClassFuncLine, ANow) + "\r\n";
                bool bDone = false;

                if (mEntered > 0)
                {
                    System.Threading.Thread.Sleep(20);  // entry is locked just wait until other thread leaves
                }
                if (mbReady && mEntered == 0)
                {
                    if (++mEntered == 1)
                    {
                        if (String.IsNullOrEmpty(mBuffer) == false)
                        {
                            if (++mBufferEntered == 1)
                            {                                   // try to log with buffer
                                #region log with buffer -> bDone set when succeded
                                try
                                {
                                    string text;
                                    if ((mFlags & DDebugFlags.Reversed) != 0)     // add line to buffer;
                                    {
                                        text = line + mBuffer;
                                    }
                                    else
                                    {
                                        text = mBuffer + line;
                                    }
                                    bDone = AFnDoLog(text);
                                }
                                catch (Exception /*Ex*/)
                                {
                                    // just ignore
                                }
                                finally
                                {
                                    if (bDone)
                                    {
                                        mBuffer = null;
                                    }
                                }
                                #endregion
                            }
                            --mBufferEntered;
                        }
                        if (bDone == false)
                        {
                            try
                            {   // just log current line
                                bDone = AFnDoLog(line);
                            }
                            catch (Exception /*Ex*/)
                            {
                                // just ignore
                            }
                        }
                    }
                    // else failed to lock
                    --mEntered;
                }
                if (!bDone)
                {
                    mAddLineToBuffer(line); // could not log then add to buffer
                }
            }
        }
        public bool mbIsEnabled(DDebugFlags AMessageType)
        {
            return (mFlags & DDebugFlags.Enabled) != 0
                && (mFlags & AMessageType) != 0;
        }

        private string mbCreateLine(char AMarkerChar, string ALine, CClassFuncLine AClassFunc, DateTime ANow)
        {
            string line = "";

            if (ALine != null)
            {
                try
                {
                    if ((mFlags & DDebugFlags.Time) != 0)
                    {
                        line = ANow.ToString("HHmmss.fff" + AMarkerChar + " ");
                    }
                    line += ALine;

                    if ((mFlags & DDebugFlags.Class) != 0 && AClassFunc != null)
                    {
                        string cf = AClassFunc.mGetString();

                        if (string.IsNullOrEmpty(cf) == false)
                        {
                            int l = ALine.Length;

                            if (l < mLineSize)
                            {
                                line += new string(' ', (int)mLineSize - l);
                            }
                            line += " " + AMarkerChar + cf;
                        }
                    }
                }
                catch (Exception /*Ex*/)
                {
                    // if something happened during this code just ignore, can't report it 
                }
            }
            return line;
        }
    }

    public delegate string FnGetProgNameByNr(UInt16 AProgNr);

    public class CProgram   // all functions and data are static so that the functions can be called without a CProgram entity is needed
    {
        static float _cKgToLbs = 2.20462262F; // 1 kilogram = 2.20462262 pounds
        static float _cMeterToFeet = 3.2808399F; // 1 meter = 3.2808399 feet(3 feet 3⅜ inches)
        static float _cFeetToInc = 12.0F; // 1 foot = 12 inches

        // still needs to collect PC references PC name, GUID, ip etc

        // debug log to Console
        private static C_ProgLog _sLogConsole = new C_ProgLog();

        // log screen
        private static C_ProgLog _sLogScreen = new C_ProgLog();
        private static TextBox _sLogScreenTextBox = null;
        private static uint _sLogScreenMaxChars = 10000;  // Maximum number of chars on screen

        // log file
        private static C_ProgLog _sLogFile = new C_ProgLog();
        private static StreamWriter _sLogFileStream;
        private static bool _sbLogFileCreated = false;
        private static bool _sbLogFileDoCreate = false;
        private static string _sLogFileDir = null;
        private static string _sLogFilePath = null;
        private static int _sLogFileYear = 0;               // last logged date in file
        private static int _sLogFileMonth = 0;
        private static int _sLogFileDay = 0;

        private static bool _sbLogDoFlush = true;

        // prompt to user
        private static DDebugFlags sPromptFlags =                // flags indicating what to prompt to the user
                    DDebugFlags.Error | DDebugFlags.Exception;

        // floating point
        private static DFloatDecimal _sProgDecimal = DFloatDecimal.Dot;   // this windows uses dot in float
        private static double _sFloatComparePrecision = 0.0001;
        private static double _sFloatCompareValid = 1e6;
        private static double _sFloatInValid = 1e8;
        // time
        private static TimeSpan _sLocalTimeCorrection; // use this if windows does not correctly conver between local and UTC

        // program 
        public const UInt16 _cMaxProgRunNr = 7;    // otherwise the high bit is set and the dBase gives an error

        private static uint _sProgID = 0;             // program ID:
        private static UInt32 _sProgHash = 0;         // program encryption hash
        private static byte _sProgRunNr = 0;          // B31-B28: running number 0-_cMaxProgRunNr
        private static byte _sProgNr = 0;             // B27-B22: program number 1-63
        private static byte _sProgMajor = 0;          // B21-B18: major number 0-15
        private static byte _sProgMinor = 0;          // B17-B12: minor number 0-63
        private static UInt16 _sProgBuild = 0;        // B11-B00: Build number = 0 - 4095 = Days since 2016 ->  2027
                                                      //        if needed date can be shifted according to major  number
        private static string _sProgName = null;
        private static string _sProgRevision = null;
        private static DateTime _sProgBuildDT;          // build time program
        private static string _sProgDir = null;

        private static DateTime _sProgStartDT;          // program started at
        private static int _sRunningProgs = 0;          // flags indicating running programs progName[i]

        private static string _PcName = null;
        private static string _PcUserName = null;

        private static Form _sProgMainForm = null;      // used for prompt DialogBox
        private static FnGetProgNameByNr mFnGetProgNameByNr = null;

        private static string _sDeveloper = "simon.vlaar@.nl";
        private static UInt32 _sDeveloperHash = 0;
        private static string _sProgHeader = "SVNL";
        private static DShowOrganisation _sbOrganisationPosition = DShowOrganisation.Before;
        private static string _sOrganisationLabel = "";  // organisation label, set by CLicKey
        private static UInt32 _sDeviceID = 0;

        private static string _sProgTitleHeader = "";
        private static string _sProgTitleTrailer = "";
        public static bool _sProgTitleIncludesVersion = true;
        private static DProgramVersionLevel _sProgVersionLevel = DProgramVersionLevel.Alpha;

        private static string _sProgTitleLong = "";
        private static string _sProgTitleShort = "";

        // device PC settings
        private static UInt32 _ProgDeviceIX = 0;
        // user settings
        private static DFloatDecimal _sShowDecimal = DFloatDecimal.Dot;

        private static string _sCultureName = "";
        // https://msdn.microsoft.com/en-us/library/8kb3ddd4(v=vs.110).aspx
        private static string _sShowDateFormat = "yyyy\\/MM\\/dd"; //"yyyy\\\\MM\\\\dd";        // '\' is escape so double "\\" 
        private static string _sShowTimeFormat = "HH:mm:ss";        // hh:mm:ss tt
        private static string _sShowTimeFraction = "HH:mm:ss.fff";
        private static string _sShowDateTimeFormat = "yyyy\\/MM\\/dd HH:mm:ss"; //"yyyy\\\\MM\\\\dd HH:mm:ss"
        private static string _sShowDateTimeFraction = "yyyy\\/MM\\/dd HH:mm:ss.fff"; // "yyyy\\\\MM\\\\dd HH:mm:ss.fff";

        private static string _sShowTrueText = "True";
        private static string _sShowFalseText = "False";
        private static string _sShowTrueLetter = "T";
        private static string _sShowFalseLetter = "F";

        private static bool _mbShowImperial = false;

        private static UInt32 _sProgUserIX = 0;      // user index
        private static string _sProgUserInitials = null;
        private static int _sProgUserTimeOutMin = 60;
        private static string _sProgUserScreenName = null;
        private static string _sProgUserPin = null;
        private static bool _sbProgUserActive = false;
        private static DateTime _sProgUserLastActive = DateTime.MinValue;
        //        private static UInt16 _sProgUserInactiveMin = 60;   // inactive time

        private static ReqLabelForm mReqLabelForm = null;
        private static ReqBoolForm mReqBoolForm = null;

        // memeory
        private static UInt32 _sWarnUsedMemSizeMB = 1000;
        private static UInt32 _sWarnFreeMemSizeMB = 200;
        private static UInt32 _sWarnAvailMemSizeMB = 100;
        private static UInt32 _sMaxWinProgMemSizeMB = 3000;
        private static UInt32 _sMinWinFreeMemSizeMB = 100;

        private static bool _sbLastMemCheckOk = true;
        private static UInt32 _sLastUsedMemSizeMB = 0;
        private static UInt32 _sLastFreeMemSizeMB = 0;
        private static UInt32 _sLastAvailMemSizeMB = 0;

        private static DateTime _sLastWarnUsedMemDT = DateTime.MinValue;
        private static UInt32 _sLastWarnUsedMemMB = 0;
        private static DateTime _sLastWarnFreeMemDT = DateTime.MinValue;
        private static UInt32 _sLastWarnFreeMemMB = 0;
        private static DateTime _sLastWarnAvailDT = DateTime.MinValue;
        private static UInt32 _sLastWarnAvailMemMB = 0;



        // program functions
        public delegate void FnEnumWindows(int hwnd, int lParam);
        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        private static extern int GetWindowText(IntPtr hWnd, StringBuilder strText, int maxCount);
        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        private static extern int GetWindowTextLength(IntPtr hWnd);
        [DllImport("user32")]
        public static extern int EnumWindows(FnEnumWindows x, int y);

        public static void sFnEnumWindows(int hwnd, int lParam)
        {
            int size = GetWindowTextLength((IntPtr)hwnd);

            _sRunningProgs |= 0x01; // indicate at least one window has been iterated in bit 0

            if (size > 0)
            {
                var builder = new StringBuilder(size + 1 + 255);
                GetWindowText((IntPtr)hwnd, builder, builder.Capacity);

                string s = builder.ToString();
                if (s.Contains/*StartsWith*/(_sProgName))
                {
                    int l = _sProgName.Length;
                    int progNr = 1;

                    s.Remove(0, l);
                    l = s.Length;
                    if (l > 0 && s[0] == '[')
                    {
                        int i = s.IndexOf(']');

                        progNr = 0;
                        if (i > 1)
                        {
                            s = s.Substring(0, i - 1);
                            if (int.TryParse(s, out l))
                            {
                                progNr = l;
                            }
                        }
                        if (progNr > 0 && progNr <= 31)
                        {
                            _sRunningProgs |= 0x01 << progNr;
                        }
                    }
                }
            }
        }
        public static bool mbFindFreeProgramRunNumber()
        {
            _sRunningProgs = 0;
            _sProgRunNr = 0;

            EnumWindows(sFnEnumWindows, 0);

            if (_sRunningProgs == 0)
            {
                // function did not run
            }
            else
            {
                // only program nrs 1 .. 15 are valid (progID has 4 bits for progRunNr)
                for (int i = 1; i <= 15; ++i)
                {
                    if ((_sRunningProgs & (0x01 << i)) == 0)
                    {
                        _sProgRunNr = (byte)i;    // first free
                        break;
                    }
                }
            }
            return _sProgRunNr > 0 && _sProgRunNr <= _cMaxProgRunNr;
        }
        public static bool sbSetRunNumber(UInt16 ARunNumber)
        {
            bool bOk = _sProgRunNr <= 1 && ARunNumber > 0 && ARunNumber < _cMaxProgRunNr;

            // only program nrs 1 .. 15 are valid (progID has 4 bits for progRunNr)
            if (bOk)
            {
                _sProgRunNr = (Byte)ARunNumber;
            }
            return bOk;
        }

        public static string sStripName(string AName)
        {
            string s = "";
            int n = AName == null ? 0 : AName.Length;

            for (int i = 0; i < n; ++i)
            {
                char c = AName[i];

                if (c >= ' ')
                {
                    if (c < 127 && char.IsLetterOrDigit(c)) s += c;
                    //                    else s += "-";
                }
            }
            return s;
        }

        public static void sSetProgramLogFlush(bool AbLogDoFlush)
        {
            _sbLogDoFlush = AbLogDoFlush;
        }
        public static bool sbGetProgramLogFlush()
        {
            return _sbLogDoFlush;
        }


        public static bool sbStartProgram(string AProgName, byte AProgNr, byte AMaxProgRun,
            string ADeveloper, String AProgHeader, UInt32 AProgramHash, byte AForceRunNr)
        {
            if (_sLogScreen == null) _sLogScreen = new C_ProgLog();
            if (_sLogConsole == null) _sLogConsole = new C_ProgLog();
            if (_sLogFile == null) _sLogFile = new C_ProgLog();
            if (_sProgName != null || AProgName == null || AProgName.Length <= 1)
            {
                // invalid call or 
                return false;   // second start of program! should not be happening
            }
            _sProgName = AProgName;
            _sProgStartDT = DateTime.Now;
            _sLogFileYear = _sProgStartDT.Year;
            _sLogFileMonth = _sProgStartDT.Month;
            _sLogFileDay = _sProgStartDT.Day;

            _sProgHash = AProgramHash;
            _sDeveloperHash = sCalcStringAddHash(ADeveloper);

            // still to add check executable name to ProgName
            string progFilePath = Application.ExecutablePath;
            _sProgDir = Path.GetDirectoryName(progFilePath);
            string appName = Path.GetFileNameWithoutExtension(progFilePath);
            if (appName.StartsWith(AProgName) == false)
            {
                sLogLine("program executable does not have the right name");
                return false;
            }

            if (AProgNr == 0 || AProgNr > 63) // limit due to 5 bits in progID
            {
                return false;   // invalid program number
            }
            _sProgNr = AProgNr;

            if ((AProgramHash & 0x0FF) == 0 || (AProgramHash & 0x0FF00) == 0
                || (AProgramHash & 0x0FF0000) == 0 || (AProgramHash & 0x0FF) == 0)
            {
                // invalid hash
                return false;
            }

            _sProgRunNr = 0;
            if (!mbFindFreeProgramRunNumber())
            {
                sLogError("Can't determin run number, running = " + _sRunningProgs.ToString("X"));
            }
            else if (AForceRunNr > 0)
            {
                if (AForceRunNr <= _cMaxProgRunNr)
                {
                    _sProgRunNr = AForceRunNr;
                    sLogLine("Force program run number " + AForceRunNr);
                }
                else
                {
                    sLogLine("Bad force program run number " + AForceRunNr);
                }
            }

            // put the version info on automatic in AssemblyInfo.cs
            // [assembly: AssemblyVersion("1.0.*")]
            var version = Assembly.GetEntryAssembly().GetName().Version;
            if (version != null)
            {
                _sProgBuildDT = new DateTime(2000, 1, 1).Add(new TimeSpan(
                    TimeSpan.TicksPerDay * version.Build + // days since 1 January 2000
                    TimeSpan.TicksPerSecond * 2 * version.Revision)); // seconds since midnight
                TimeSpan ts = _sProgBuildDT - new DateTime(2016, 1, 1);

                _sProgMajor = (byte)version.Major;
                _sProgMinor = (byte)version.Minor;
                _sProgBuild = (UInt16)ts.TotalDays;
                if (_sProgBuild < 0) _sProgBuild = 0;

                _sProgID = ((uint)_sProgRunNr & 0x000F) << 28;    // B31-B28: running number 0-7 _cMaxProgRunNr //dbase error at high bit
                _sProgID |= ((uint)_sProgNr & 0x003F) << 22;      // B27-B22: program number 1-63
                _sProgID |= ((uint)_sProgMajor & 0x000F) << 18;   // B21-B18: major number 0-15
                _sProgID |= ((uint)_sProgMinor & 0x005F) << 12;   // B17-B12: minor number 0-63
                _sProgID |= ((uint)_sProgBuild & 0x0FFF);         // B11-B00: Build number = 0 - 4095 = Days since 2016 ->  2027

                _sProgRevision = version.ToString();
            }
            // initialize log functions
            sProgCreateLogFile();

            // check pc local format settings 
            string fltDec = sDeterminProgDecimal(); // check decimal '.' or ','
            DateTimeFormatInfo dtInfo = DateTimeFormatInfo.CurrentInfo;
            string am = dtInfo.AMDesignator;   // should now be AM + PM
            string pm = dtInfo.PMDesignator;

            _PcName = Environment.MachineName;
            _PcUserName = Environment.UserName;

            sLogLine("Started " + sGetProgRunName() + " v=" + sGetProgVersion() + " " + sGetProgBuildDT() + " r=" + sGetProgRevision());
            sLogLine(am + "/ " + pm + " decimal = " + fltDec);
            sLogLine("Started at " + sDateTimeToString(_sProgStartDT) + " user: " + Environment.UserName + " on pc " + Environment.MachineName);
            sbLogMemSize("start program");
            string resultDrive;
            CProgram.sbCheckFreeDriveSpace(out resultDrive, 10, "", 10, false);
            CProgram.sLogLine("progDir=" + sGetProgDir() + " " + resultDrive);
            sLogLine("");

            if (_sLocalTimeCorrection != null && Math.Abs(_sLocalTimeCorrection.TotalSeconds) > 0)
            {
                sLogLine("Local Time correction = " + _sLocalTimeCorrection.ToString());
            }

            if (_sProgMainForm != null)
            {
                string title = _sProgMainForm.Text; // update main form title so that next prog can find window
                sSetProgramTitle(title, true);
            }
            _sDeveloper = ADeveloper;
            _sProgHeader = AProgHeader + "____";
            _sProgHeader = _sProgHeader.Substring(0, 4);  // make sure it is 4 chars
            sSetProgTitleFormat("{", false, DProgramVersionLevel.Alpha, _sbOrganisationPosition, "}");   // should be set by program
            return _sProgRunNr > 0 && _sProgRunNr <= _cMaxProgRunNr;
        }

        public static string sDecriptProgID( UInt32 AProgID )
        {
            string s = "prog";

            UInt32 runNr = ( AProgID >> 28 ) & 0x000F;
            UInt32 progNr = (AProgID >> 22) & 0x003F;
            UInt32 major = (AProgID >> 18) & 0x000F;
            UInt32 minor = (AProgID >> 12) & 0x002F;
            UInt32 build = AProgID & 0x0FFF;
            DateTime dt = new DateTime(2016, 1, 1).AddDays(build);

            if (progNr == 21) s = "eventboard";
            if (progNr == 5) s = "eventboard";  // bug in creating ProgID
            if (progNr == 11) s = "ireader";

            s += "(" + progNr.ToString() + ")[" + runNr.ToString() + "] v" + major.ToString() + "." + minor.ToString() + "." + build.ToString()
                + " " + sDateToYMD(dt);

            /*            _sProgID = ((uint)_sProgRunNr & 0x000F) << 28;    // B31-B28: running number 0-7 _cMaxProgRunNr //dbase error at high bit
                        _sProgID |= ((uint)_sProgNr & 0x002F) << 22;      // B27-B22: program number 1-63
                        _sProgID |= ((uint)_sProgMajor & 0x000F) << 18;   // B21-B18: major number 0-15
                        _sProgID |= ((uint)_sProgMinor & 0x002F) << 12;   // B17-B12: minor number 0-63
                        _sProgID |= ((uint)_sProgBuild & 0x0FFF);         // B11-B00: Build number = 0 - 4095 = Days since 2016 ->  2027
            */

            return s;
        }

        public static void sCreateNewLogFile(string AReason)
        {
            string fltDec = sDeterminProgDecimal(); // check decimal '.' or ','
            DateTimeFormatInfo dtInfo = DateTimeFormatInfo.CurrentInfo;
            string am = dtInfo.AMDesignator;   // should now be AM + PM
            string pm = dtInfo.PMDesignator;

            sLogLine("Closing log file: " + AReason);
            sProgCloseLogFile();
            sProgCreateLogFile();

            sLogLine("New log file: " + AReason);
            sLogLine("Continue " + sGetProgRunName() + " v=" + sGetProgVersion() + " " + sGetProgBuildDT() + " r=" + sGetProgRevision());
            sLogLine(am + "/ " + pm + " decimal = " + fltDec);
            sLogLine("Started at " + sDateTimeToString(_sProgStartDT) + " user: " + Environment.UserName + " on pc " + Environment.MachineName);
            sLogLine("");

        }

        public static DProgramVersionLevel sGetProgVersionLevel()
        {
            return _sProgVersionLevel;
        }
        public static string sGetProgVersionLevelString()
        {
            string s = "?";

            switch (_sProgVersionLevel)
            {
                case DProgramVersionLevel.Alpha: s = "α"; break;
                case DProgramVersionLevel.Beta: s = "β"; break;
                case DProgramVersionLevel.Demo: s = "d"; break;
                case DProgramVersionLevel.show: s = "s"; break;
                case DProgramVersionLevel.Released: s = "v"; break;
            }
            return s;
        }
        public static string sGetProgVersionLevelLetter()
        {
            string s = "?";

            switch (_sProgVersionLevel)
            {
                case DProgramVersionLevel.Alpha: s = "a"; break;
                case DProgramVersionLevel.Beta: s = "b"; break;
                case DProgramVersionLevel.Demo: s = "d"; break;
                case DProgramVersionLevel.show: s = "s"; break;
                case DProgramVersionLevel.Released: s = "v"; break;
            }
            return s;
        }
        public static string sMakeProgTitle(string ATitle, bool AbUseTrailer, bool AbShort)
        {
            string s;

            if (AbShort)
            {
                s = _sProgTitleShort + ATitle; ;
            }
            else
            {
                s = _sProgTitleLong + ATitle;

                if (AbUseTrailer)
                {
                    switch (_sProgVersionLevel)
                    {
                        case DProgramVersionLevel.Alpha: s += " - α TEST α - "; break;
                        case DProgramVersionLevel.Beta: s += " - β TEST β - "; break;
                        case DProgramVersionLevel.Demo: s += " - DEMO - "; break;
                    }
                    s += _sProgTitleTrailer;
                }
                else
                {
                    switch (_sProgVersionLevel)
                    {
                        case DProgramVersionLevel.Alpha: s += " - α"; break;
                        case DProgramVersionLevel.Beta: s += " - β"; break;
                        case DProgramVersionLevel.Demo: s += " - DEMO"; break;
                    }
                }
            }
            return s;
        }
        public static void sSetProgTitleFormat(string AHeader, bool AbIncudeVersion,
                        DProgramVersionLevel AVersionLevel, DShowOrganisation AOrganisationPosition, string ATailer) //  versionType αβv
        {
            _sProgTitleHeader = AHeader;
            _sProgTitleIncludesVersion = AbIncudeVersion;
            _sProgTitleTrailer = "";
            _sbOrganisationPosition = AOrganisationPosition;

            if (false == string.IsNullOrEmpty(ATailer))
            {
                _sProgTitleTrailer = " " + ATailer;
            }
            _sProgVersionLevel = AVersionLevel;

            _sProgTitleLong = "";
            _sProgTitleShort = "";
            if (_sOrganisationLabel.Length > 0 && AOrganisationPosition == DShowOrganisation.First)
            {
                _sProgTitleLong += _sOrganisationLabel + " ";
                _sProgTitleShort += _sOrganisationLabel + " ";
            }

            if (AHeader != null && AHeader.Length > 0)
            {
                _sProgTitleLong += AHeader + " ";
            }
            if (_sOrganisationLabel.Length > 0 && AOrganisationPosition == DShowOrganisation.Before)
            {
                _sProgTitleLong += _sOrganisationLabel + " ";
                _sProgTitleShort += _sOrganisationLabel + " ";
            }
            _sProgTitleLong += sGetProgRunName();
            _sProgTitleShort += sGetProgRunName();

            if (AbIncudeVersion)
            {
                string imperial = sbGetImperial() ? "i" : "";

                if ((_sDeviceID / 10000) < 30)
                {
                    imperial += (_sDeviceID % 10000) < 10 ? "p" : "t";
                }
                _sProgTitleLong += " " + sGetProgVersionLevelString() + _sProgMajor.ToString() + "."
                   + _sProgMinor.ToString() + "." + _sProgBuild.ToString() + imperial + "_" + sGetProgBuildYMD();
                _sProgTitleShort += " " + sGetProgVersionLevelString() + _sProgMajor.ToString() + "."
                    + _sProgMinor.ToString() + "." + _sProgBuild.ToString() + imperial;
            }
            if (_sOrganisationLabel.Length > 0 && AOrganisationPosition == DShowOrganisation.End)
            {
                _sProgTitleLong += " @" + _sOrganisationLabel;
                _sProgTitleShort += " @" + _sOrganisationLabel;
            }
            _sProgTitleLong += ": ";
            _sProgTitleShort += ": ";
        }


        /*        public static string sGetProgTitle()
                {
                    return _sProgTitle;
                }
        */
        public static string sGetProgDeveloper()
        {
            return _sDeveloper;
        }
        public static string sGetProgDir()
        {
            return _sProgDir;
        }
        public static UInt32 sCalcStringXorHash(string AString)
        {
            UInt32 hash = 0;
            UInt32 m;
            UInt16 j = 0;
            char c;
            int n = AString != null ? AString.Length : 0;

            for (UInt16 i = 0; i < n; ++i)
            {
                c = AString[i];
                m = (UInt32)(c & 0x00FFFF);
                m <<= j;
                hash ^= m;
                if (++j >= 24) j = 0;
            }
            return hash;
        }

        public static UInt32 sCalcStringAddHash(string AString)
        {
            UInt32 hash = 0;
            UInt32 m;
            UInt16 j = 0;
            char c;
            int n = AString != null ? AString.Length : 0;

            for (UInt16 i = 0; i < n; ++i)
            {
                c = AString[i];
                m = (UInt32)(c & 0x00FFFF);
                m <<= j;
                hash += m;
                if (++j >= 24) j = 0;
            }
            return hash;
        }
        public static UInt32 sCalcStringSubHash(string AString)
        {
            UInt32 hash = 0;
            UInt32 m;
            UInt16 j = 0;
            char c;
            int n = AString != null ? AString.Length : 0;

            for (UInt16 i = 0; i < n; ++i)
            {
                c = AString[i];
                m = (UInt32)(c & 0x00FFFF);
                m <<= j;
                hash -= m;
                if (++j >= 24) j = 0;
            }
            return hash;
        }

        public static UInt32 sCombineHash(UInt32 APrimairy, UInt32 ASecondary)
        {
            UInt32 hash = APrimairy ^ ASecondary;

            if ((hash & 0x00FF) == 0) hash |= (APrimairy & 0x00FF); // prevent 0 in byte
            if ((hash & 0x00FF00) == 0) hash |= (APrimairy & 0x00FF00);
            if ((hash & 0x00FF0000) == 0) hash |= (APrimairy & 0x00FF0000);
            if ((hash & 0xFF000000) == 0) hash |= (APrimairy & 0xFF000000);

            return hash;
        }

        public static UInt32 sShuffleHash(UInt32 AHash, UInt16 AShuffle)
        {
            UInt32 hash = AHash;

            if (AShuffle > 0)
            {
                UInt32 v;
                UInt16 m;
                UInt16 i;
                uint shuffle = AShuffle;
                UInt16 shift = (UInt16)(shuffle % 4);
                shuffle /= 4;
                UInt16 used = (UInt16)(1 << shift);
                hash = (hash & 0x0FF) << (shift << 3);  // first byte shift

                shift = (UInt16)(shuffle % 3);
                shuffle /= 3;

                v = (AHash >> 8) & 0x00FF;          // second byte shift
                m = 1;
                for (i = 0; i <= shift; ++i)
                {
                    while ((m & used) != 0)    // find first unused position
                    {
                        v <<= 8;
                        m <<= 1;
                    }
                }
                hash |= v;

                shift = (UInt16)(shuffle % 2);
                shuffle /= 2;
                v = (AHash >> 16) & 0x00FF;          // third byte shift
                m = 1;
                for (i = 0; i <= shift; ++i)
                {
                    while ((m & used) != 0)    // find first unused position
                    {
                        v <<= 8;
                        m <<= 1;
                    }
                }
                hash |= v;

                v = (AHash >> 24) & 0x00FF;          // fourth byte shift
                m = 1;
                while ((m & used) != 0)    // find last unused position
                {
                    v <<= 8;
                    m <<= 1;
                }
                hash |= v;
            }
            return hash;
        }
        public static UInt32 sGetProgramHash()
        {
            UInt32 hash = _sProgHash;

            int n = _sDeveloper.Length;

            for (int i = 0; i < n; ++i)
            {
                char c = _sDeveloper[i];

                hash += (UInt32)((c & 0x0FF) << (23 - (i % 24)));
            }
            return hash;
        }
        public static void sProgLogArguments(string[] AArgs)
        {
            string line = "Arguments = ";
            int i = 0;

            foreach (string s in AArgs)
            {
                if (++i > 1) line += ", ";
                line += "'" + s + "'";
            }
            sLogLine(line);
        }
        public static bool sbSetProgramOrganisation(string AOrganisation, UInt32 ADeviceID)
        {
            _sOrganisationLabel = AOrganisation;
            _sDeviceID = ADeviceID;
            if ((_sDeviceID & 0x80000000) != 0)
            {
                // demo flag
                if (_sProgVersionLevel >= DProgramVersionLevel.Demo)
                {
                    _sProgVersionLevel = DProgramVersionLevel.Demo;
                }
            }

            sSetProgTitleFormat(_sProgTitleHeader, _sProgTitleIncludesVersion, _sProgVersionLevel, _sbOrganisationPosition, _sProgTitleTrailer);
            return _sProgID != 0 && (_sDeviceID & 0x7FFFFFFF) != 0;
        }
        public static string sGetOrganisationLabel()
        {
            return _sOrganisationLabel;
        }

        public static string sGetPcName()
        {
            return _PcName;
        }
        public static string sGetPcUserName()
        {
            return _PcUserName;
        }

        public static UInt32 sGetDeviceID()
        {
            return _sDeviceID;
        }
        public static UInt16 sGetProgNr()
        {
            return _sProgNr;
        }
        public static void sSetProgramMainForm(Form AMainForm, string ATitle)
        {
            _sProgMainForm = AMainForm;
            if (_sProgRunNr > 0)
            {
                sSetProgramTitle(ATitle, true);
            }
        }
        public static void sSetProgramTitle(string ATitle, bool AbUseTailer)
        {
            if (_sProgMainForm != null)
            {
                string s = sMakeProgTitle(ATitle, AbUseTailer, false);

                _sProgMainForm.Text = s;
            }
        }
        public static void sEndProgram()
        {
            sLogLine(" End program at " + sDateTimeToString(DateTime.Now) + ".");
            sLogLine("===============================================================");
            _sLogScreenTextBox = null;      // no longer log to screen
            sProgCloseLogFile();            // close log file
        }
        public static string sGetProgName()
        {
            return _sProgName;
        }
        public static byte sGetProgRunNr()
        {
            return _sProgRunNr;
        }
        public static string sGetProgRunName()
        {
            string s = _sProgName;

            if (_sProgRunNr > 1)
            {
                s = "[" + _sProgRunNr.ToString() + "]" + _sProgName;
            }
            return s;
        }
        public static UInt32 sGetProgID()
        {
            return _sProgID;
        }

        public static string sMakeUniqueProgName()
        {
            string s = _sOrganisationLabel + "_" + _sDeviceID.ToString("X8") + "_"
                + _sProgName + "_" + _sProgRunNr.ToString("00");
            return s;
        }

        public static string sGetUserAtPcString()
        {
            string s = _sProgUserInitials;
            if (s != null && s.Length > 0)
            {
                s += "@";
            }
            return s + _PcUserName + "@" + _PcName;
        }

        public static bool sWriteLastActiveFile(string APath, bool AbLogOk, string AExtraLines)
        {
            bool bOk = false;

            string fileName = sMakeUniqueProgName() + "_" + sStripName(_PcName) + "_" + sStripName(_PcUserName) + ".lastActive";

            try
            {
                using (StreamWriter fs = new StreamWriter(Path.Combine(APath, fileName)))
                {
                    if (fs != null)
                    {
                        string s = "Time: " + DateTimeOffset.Now.ToString()
                            + "\r\ncomputer: " + sGetPcUserName() + "\t" + sGetPcName()
                            + "\r\nProg: " + _sProgName + " " + sGetProgVersionLevelString() + _sProgMajor.ToString() + "."
                                    + _sProgMinor.ToString() + "." + _sProgBuild.ToString()
                            + "\r\nStarted: " + sDateTimeToFormatString(_sProgStartDT, "yyyy/MM/dd HH:mm:ss")
                            + "\r\nuser: " + _sProgUserIX + "\t" + _PcUserName;

                        fs.WriteLine(s);
                        if( AExtraLines != null && AExtraLines.Length > 0)
                        {
                            fs.WriteLine(AExtraLines );
                        }
                        fs.Close();
                        bOk = true;
                    }
                }
            }
            catch (Exception)
            {
                bOk = false;
            }
            if (bOk == false)
            {
                CProgram.sLogError("Failed writing activity " + fileName);
            }
            else if (AbLogOk)
            {
                CProgram.sLogLine("Writen activity " + fileName);
            }

            return bOk;
        }
        public static bool sbLockFilePath(string APath, string ALockName, ref StreamWriter ArLockStream, string AExtraLine = null)
        {
            bool bOk = false;

            if (APath != null && APath.Length > 4)
            {
                if (ArLockStream != null)
                {
                    bOk = true;
                }
                else
                {
                    string filePath = Path.Combine(APath, ALockName + ".Lock");

                    try
                    {
                        FileInfo fi = new FileInfo(filePath);

                        if (fi != null && fi.Exists)
                        {
                            try
                            {
                                fi.Delete();
                                fi = null;  // lock file existed but could be deleted => Not Locked
                            }
                            catch (Exception ex)
                            {
                                // file existed but could not be deleted => Is Locked
                            }
                        }
                        else
                        {
                            fi = null; // file does not exist => Not Locked

                        }
                        if ( fi == null)
                        {
                       
                        StreamWriter fs = new StreamWriter(filePath);

                            if (fs != null)
                            {
                                string s = "Start Time: " + DateTimeOffset.Now.ToString() + " Locking " + ALockName
                                    + "\r\nLockFile: " + filePath
                                    + "\r\ncomputer: " + sGetPcUserName() + "\t" + sGetPcName()
                                    + "\r\nProg: " + sGetProgRunName() + " " + sGetProgVersionLevelString() + sGetProgShortVersion()
                                    + "\r\nStarted: " + sDateTimeToFormatString(sGetProgStartDT(), "yyyy/MM/dd HH:mm:ss")
                                    + "\r\nuser: " + sGetProgUserIX() + "\t" + sGetProgUserScreenName();
                                if (AExtraLine != null)
                                {
                                    s += AExtraLine += "\r\n";
                                }

                                fs.WriteLine(s);
                                fs.Flush();
                                fs.Close();

                                StreamWriter fs2 = new StreamWriter(filePath, true);
                                if (fs2 == null)
                                {
                                    CProgram.sLogLine("Failed Append Locking " + ALockName + " file: " + filePath);
                                    fs2 = fs;
                                }
                                else
                                {
                                    fs2.WriteLine("Append lock");
                                    fs2.Flush();
                                    fs.Dispose();
                                }

                                ArLockStream = fs2;
                                CProgram.sLogLine("Locking " + ALockName + " file: " + filePath);

                                bOk = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        bOk = false;
                        ArLockStream = null;
                        CProgram.sLogLine("Failed Locking " + ALockName + " file: " + filePath);
                    }
                }
            }
            return bOk;
        }
        public static bool sbCheckWriteLockFilePath(string APath, string ALockName, ref StreamWriter ArLockStream, string AExtraLine = null)
        {
            bool bOk = false;
            if (APath != null && APath.Length > 4)

            {
                if (ArLockStream == null)
                {
                    bOk = true;
                }
                else
                {
                    string filePath = Path.Combine(APath, ALockName + ".Lock");

                    try
                    {
                        string s = "Check Time: " + DateTimeOffset.Now.ToString() + "\n";
                        if (AExtraLine != null)
                        {
                            s += AExtraLine += "\r\n";
                        }

                        ArLockStream.WriteLine(s);
                        bOk = true;

                        CProgram.sLogLine("Lock test" + ALockName + " file: " + filePath);
                    }
                    catch (Exception ex)
                    {
                        bOk = false;
                        CProgram.sLogLine("Failed Check write Lock " + ALockName + " file: " + filePath);
                    }
                }
            }
            return bOk;
        }

        public static bool sbUnLockFilePath(string APath, string ALockName, ref StreamWriter ArLockStream, string AExtraLine = null)
        {
            bool bOk = false;

            if (APath != null && APath.Length > 4)

            {
                if (ArLockStream == null)
                {
                    bOk = true;
                }
                else
                {
                    string filePath = Path.Combine(APath, ALockName + ".Lock");

                    try
                    {
                        string s = "End Time: " + DateTimeOffset.Now.ToString() + " releasing Lock\n";
                        if (AExtraLine != null)
                        {
                            s += AExtraLine += "\r\n";
                        }

                        ArLockStream.WriteLine(s);

                        ArLockStream.Close();
                        CProgram.sLogLine("UnLocked " + ALockName + " file: " + filePath);
                        ArLockStream = null;
                        bOk = true;

                        string newName = filePath + "_old";

                        if (File.Exists(newName))
                        {
                            File.Delete(newName);
                        }
                        File.Move(filePath, newName);
                    }
                    catch (Exception ex)
                    {
                        bOk = false;
                        ArLockStream = null;
                        CProgram.sLogLine("Failed UnLocking " + ALockName + " file: " + filePath);
                    }
                }
            }
            return bOk;
        }
        public static bool sbCheckLockFilePath(string APath, string ALockName)
        {
            bool bLocked = false;

            if (APath != null && APath.Length > 4)
            {
                string filePath = Path.Combine(APath, ALockName + ".Lock");

                try
                {
                    bLocked = File.Exists(filePath);
                }
                catch (Exception ex)
                {
                    bLocked = false;
                    CProgram.sLogLine("Failed check Lock " + ALockName + " file: " + filePath);
                }
            }
            return bLocked;
        }

        public static string sGetProgStartString()
        {
            return sDateTimeToFormatString(_sProgStartDT, "yyyy/MM/dd HH:mm:ss");
        }
        public static DateTime sGetProgStartDT()
        {
            return _sProgStartDT;
        }
        public static string sGetProgRevision()
        {
            return _sProgRevision;
        }
        public static string sGetProgVersion()
        {
            return sGetProgVersionLevelString() + _sProgMajor.ToString() + "." + _sProgMinor.ToString() + "." + _sProgBuild.ToString();
        }
        public static string sGetProgShortVersion()
        {
            return " v" + _sProgBuild.ToString();
        }

        public static string sGetProgBuildDT()
        {
            return sDateTimeToString(_sProgBuildDT);    // is in programmers time
        }

        public static string sGetProgBuildYMD()
        {
            string s = "";

            if (_sProgBuildDT != null && _sProgBuildDT != DateTime.MinValue)
            {
                s = _sProgBuildDT.ToString("yyyyMMdd");
            }
            return s;
        }

        public static UInt32 sGetDeviceIX()
        {
            return _ProgDeviceIX;
        }

        public static void sSetDeviceIX(UInt32 ADeviceIX)
        {
            _ProgDeviceIX = ADeviceIX;
        }
        public static UInt32 sGetProgUserIX()
        {
            return _sProgUserIX;
        }
        public static void sSetUserIX(UInt32 AUserIX)
        {
            _sProgUserIX = AUserIX;
        }

        public static void sSetProgNameListFn(FnGetProgNameByNr AFn)
        {
            mFnGetProgNameByNr = AFn;
        }

        public static string sGetProgNameVersionNow()
        {
            string s = CProgram.sGetProgName() + " " + CProgram.sGetProgVersion() + " @ " 
                + CProgram.sGetPcName()
                + " @ " + CProgram.sGetOrganisationLabel()
                + " @ " + CProgram.sDateTimeToFormatString(DateTime.Now, "yyyy/MM/dd HH:mm:ss")
                + CProgram.sTimeZoneOffsetStringLong(CProgram.sGetLocalTimeZoneOffsetMin());
            return s;
        }

        public static string sGetProgNameByNr(UInt16 AProgNr)
        {
            if (mFnGetProgNameByNr != null)
            {
                return mFnGetProgNameByNr(AProgNr);
            }
            if (AProgNr == _sProgNr)
            {
                return _sProgName;
            }
            return null;
        }
        // float function
        public static void sProgSetFloatPrecision(double AComparePrecision, double ACompareValid = 1e6)
        {
            _sFloatComparePrecision = AComparePrecision;
            _sFloatCompareValid = ACompareValid;
            _sFloatInValid = 10 * ACompareValid;
        }
        public static double sGetFloatPrecision()
        {
            return _sFloatComparePrecision;
        }
        public static double sGetFloatInvalid()
        {
            return _sFloatInValid;
        }
        public static bool sbFloatCompareEqual(double AFloat1, double AFloat2)
        {
            return Math.Abs(AFloat1 - AFloat2) <= _sFloatComparePrecision;
        }
        public static bool sbFloatIsValid(double AFloat)
        {
            return Math.Abs(AFloat) < _sFloatCompareValid;
        }
        public static bool sbFloatIsZero(double AFloat)
        {
            return Math.Abs(AFloat) < _sFloatComparePrecision;
        }
        public static bool sbFloatIsNotZero(double AFloat)
        {
            double d = Math.Abs(AFloat);
            return d < _sFloatCompareValid && d >= _sFloatComparePrecision;
        }


        // Date and Time çonversion' functions:  
        // Use these so that program is indipendend of running platform
        // All date time must be stored as UTC ( a copy can be stored as local time for offset calculation)
        // Name variables with unit extention UTC (or DT for local)
        // Assumes that windows time is correct and can correctly convert between local to UTC time
        // If not use TimeSpan _sLocalTimeCorrection to correctly convert to UTC
        public static int sGetPcUtcMinutes()
        {
            double sec = (int)((DateTime.Now - DateTime.UtcNow).TotalSeconds);
            if (sec < 0) sec -= 5;
            else
            {
                sec += 5;
            }
            int min = (int)(sec) / 60;
            return min;
        }
        public static string sMakeUtcOffsetString(int AOffsetMinutes)
        {
            string s = "+";
            int min = AOffsetMinutes;

            if (min < 0)
            {
                s = "-";
                min = -min;
            }
            int hour = min / 60;
            min -= hour * 60;
            s += hour.ToString("D2") + ":" + min.ToString("D2");

            return s;
        }
        public static Int16 sGetLocalTimeZoneOffsetMin()
        {
            TimeSpan ts = (DateTime.Now - DateTime.UtcNow);

            if (_sLocalTimeCorrection != null) ts += _sLocalTimeCorrection;

            double minutes = ts.TotalMinutes;
            minutes += minutes >= 0 ? 0.4 : -0.4;

            return (Int16)minutes;
        }
        public static string sTimeZoneOffsetStringLong(Int16 ATimeOffsetMin)
        {
            string s = "+";
            int minutes = ATimeOffsetMin;
            if (minutes < 0)
            {
                s = "-";
                minutes = -minutes;
            }
            s += (minutes / 60).ToString("D02") + ":" + (minutes % 60).ToString("D02");
            return s;
        }
        public static string sTimeZoneOffsetStringShort(Int16 ATimeOffsetMin)
        {
            string s = "+";
            int minutes = ATimeOffsetMin;
            if (minutes < 0)
            {
                s = "-";
                minutes = -minutes;
            }
            s += (minutes / 60).ToString("D02");
            return s;
        }
        public static string sTimeZoneOffsetString(Int16 ATimeOffsetMin, DTimeZoneShown AShowType)
        {
            string s = "+";
            int minutes = ATimeOffsetMin;
            if (minutes < 0)
            {
                s = "-";
                minutes = -minutes;
            }
            switch (AShowType)
            {
                case DTimeZoneShown.None: s = ""; break;
                case DTimeZoneShown.Short: s += (minutes / 60).ToString("D02"); break;
                case DTimeZoneShown.Long: s += (minutes / 60).ToString("D02") + ":" + (minutes % 60).ToString("D02"); break;
                case DTimeZoneShown.Name: s += (minutes / 60).ToString("D02"); break;   // not done yet
            }
            return s;
        }
        public static bool sbParseTimeZoneOffsetString(string AText, ref Int16 ArTimeOffsetMin)
        {
            bool bOk = false;

            if (AText != null)
            {
                bool bSign = false;
                bool bMin = false;
                string strHour = "";
                string strMin = "";

                bOk = true;
                for (int i = 0; i < AText.Length; ++i)
                {
                    char c = AText[i];

                    if (c != ' ' && c != '\t')
                    {
                        if (c == '+' || c == '-')
                        {
                            bOk = false == bSign && false == bMin;
                            strHour += c;
                            bSign = true;
                        }
                        else if (c == ':')
                        {
                            bOk = false == bMin;
                            bMin = true;
                            bSign = true;
                        }
                        else if (Char.IsNumber(c))
                        {
                            if (bMin)
                            {
                                strMin += c;
                            }
                            else
                            {
                                strHour += c;
                            }
                        }
                        else
                        {
                            bOk = false; // illigal char
                        }
                    }
                }

                if (bOk)
                {
                    int timeZone = 0;
                    int hour = 0;
                    int min = 0;

                    int.TryParse(strHour, out hour);
                    int.TryParse(strMin, out min);

                    timeZone = hour * 60;
                    timeZone += hour < 0 ? -min : min;
                    ArTimeOffsetMin = (Int16)timeZone;
                }
            }
            return bOk;
        }
        public static TimeSpan sGetLocalTimeCorrection()
        {
            return _sLocalTimeCorrection; // not used or tested
        }
        public static void sSetLocalTimeOffset(TimeSpan ATimeSpan)
        {
            _sLocalTimeCorrection = ATimeSpan;
        }
        public static DateTime sGetUtcNow()
        {
            DateTime dt = DateTime.UtcNow;

            if (_sLocalTimeCorrection != null) dt -= _sLocalTimeCorrection;
            return dt;
        }
        public static DateTime sGetUtcDateNow()
        {
            DateTime dt = DateTime.UtcNow;

            if (_sLocalTimeCorrection != null) dt -= _sLocalTimeCorrection;
            return dt.Date;
        }
        public static DateTime sGetLocalNow()
        {
            DateTime dt = DateTime.Now;     // system local time is ok

            return dt;
        }
        public static DateTime sGetLocalDateNow()
        {
            DateTime dt = DateTime.Now;     // system local time is ok

            return dt.Date;
        }
        public static DateTime sUseAsLocal(DateTime ADateTime)
        {
            DateTime dt = DateTime.SpecifyKind(ADateTime, DateTimeKind.Local);

            return dt;
        }
        public static DateTime sUseAsUTC(DateTime ADateTime)
        {
            DateTime dt = DateTime.SpecifyKind(ADateTime, DateTimeKind.Utc);

            return dt;
        }
        public static DateTime sLocalToUTC(DateTime ADateTime, TimeSpan AOffsetSubtracted)
        {
            DateTime dt = DateTime.SpecifyKind(ADateTime, DateTimeKind.Local);

            if (ADateTime.Kind == DateTimeKind.Unspecified)
            {
                CProgram.sPromptError(false, "Program", "Unspecified time used");
            }
            else if (ADateTime.Kind == DateTimeKind.Utc)
            {
                CProgram.sPromptError(false, "Program", "Utc time passed as Local time");
            }
            if (AOffsetSubtracted != null)
            {
                dt -= AOffsetSubtracted;
            }
            if (_sLocalTimeCorrection != null) dt -= _sLocalTimeCorrection;
            DateTime utc = DateTime.SpecifyKind(dt.ToUniversalTime(), DateTimeKind.Utc);

            return utc;
        }
        public static DateTime sLocalToUTC(DateTime ADateTime)
        {
            DateTime utc;

            if (ADateTime == DateTime.MinValue)
            {
                utc = DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Utc);
            }
            else
            {
                DateTime dt = DateTime.SpecifyKind(ADateTime, DateTimeKind.Local);
                if (ADateTime.Kind == DateTimeKind.Unspecified)
                {
                    CProgram.sPromptError(false, "Program", "Unspecified time used");
                }
                else if (ADateTime.Kind == DateTimeKind.Utc)
                {
                    CProgram.sPromptError(false, "Program", "Utc time passed as Local time");
                }

                if (_sLocalTimeCorrection != null) dt -= _sLocalTimeCorrection;
                utc = DateTime.SpecifyKind(dt.ToUniversalTime(), DateTimeKind.Utc);
            }

            return utc;
        }
        public static DateTime sDateTimeToUTC(DateTime ADateTime) // convert if needed
        {
            DateTime utc;

            if (ADateTime == DateTime.MinValue)
            {
                utc = DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Utc);
            }
            else
            {
                if (ADateTime.Kind == DateTimeKind.Unspecified)
                {
                    CProgram.sPromptError(false, "Program", "Unspecified time used");
                }
                else if (ADateTime.Kind == DateTimeKind.Utc)
                {
                    CProgram.sPromptError(false, "Program", "Utc time passed as Local time");
                }
                if (ADateTime.Kind != DateTimeKind.Local)
                {
                    return ADateTime;
                }
                DateTime dt = DateTime.SpecifyKind(ADateTime, DateTimeKind.Local);

                if (_sLocalTimeCorrection != null) dt -= _sLocalTimeCorrection;
                utc = DateTime.SpecifyKind(dt.ToUniversalTime(), DateTimeKind.Utc);
            }

            return utc;
        }
        public static DateTime sUtcToLocal(DateTime ADateTime)
        {
            DateTime dt;

            if (ADateTime == DateTime.MinValue)
            {
                dt = DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Local);
            }
            else
            {
                DateTime utc = DateTime.SpecifyKind(ADateTime, DateTimeKind.Utc);

                if (ADateTime.Kind == DateTimeKind.Unspecified)
                {
                    CProgram.sPromptError(false, "Program", "Unspecified time used");
                }
                else if (ADateTime.Kind == DateTimeKind.Local)
                {
                    CProgram.sPromptError(false, "Program", "Local time passed as Utc time");
                }

                if (_sLocalTimeCorrection != null) utc += _sLocalTimeCorrection;

                dt = DateTime.SpecifyKind(utc.ToLocalTime(), DateTimeKind.Local);
            }
            return dt;
        }
        public static DateTime sUtcToLocal(DateTime ADateTime, TimeSpan AOffsetAdded)
        {
            DateTime dt;

            if (ADateTime == DateTime.MinValue)
            {
                dt = DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Local);
            }
            else
            {
                DateTime utc = DateTime.SpecifyKind(ADateTime, DateTimeKind.Utc);

                if (ADateTime.Kind == DateTimeKind.Unspecified)
                {
                    CProgram.sPromptError(false, "Program", "Unspecified time used");
                }
                else if (ADateTime.Kind == DateTimeKind.Local)
                {
                    CProgram.sPromptError(false, "Program", "Local time passed as Utc time");
                }
                if (AOffsetAdded != null)
                {
                    utc += AOffsetAdded;
                }
                if (_sLocalTimeCorrection != null) utc += _sLocalTimeCorrection;

                dt = DateTime.SpecifyKind(utc.ToLocalTime(), DateTimeKind.Local);
            }
            return dt;
        }
        public static DateTime sDateTimeToLocal(DateTime ADateTime) // convert to local if not
        {
            if (ADateTime.Kind == DateTimeKind.Local)
            {
                return ADateTime;
            }
            if (ADateTime == DateTime.MinValue)
            {
                return DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Local);
            }
            DateTime utc = DateTime.SpecifyKind(ADateTime, DateTimeKind.Utc);

            if (_sLocalTimeCorrection != null) utc += _sLocalTimeCorrection;
            DateTime dt = DateTime.SpecifyKind(utc.ToLocalTime(), DateTimeKind.Local);
            return dt;
        }

        // Date and time print functions
        public static string sGetShowDateFormat()
        {
            return _sShowDateFormat;
        }
        public static string sGetShowDateTimeFormat()
        {
            return _sShowDateTimeFormat;
        }
        public static string sGetShowTimeFormat()
        {
            return _sShowTimeFormat;
        }

        public static bool sbProgChangeCulture(string ACultureLang_Country)
        {
            bool bOk = false;
            if (ACultureLang_Country != null && ACultureLang_Country.Length > 0)
            {
                try
                {
                    CultureInfo newCulture;

                    if (ACultureLang_Country == "0" || "x" == ACultureLang_Country.ToLower())
                    {
                        newCulture = CultureInfo.InvariantCulture;
                    }
                    else
                    {
                        newCulture = new CultureInfo(ACultureLang_Country);
                    }
                    if (newCulture != null)
                    {
                        Thread.CurrentThread.CurrentCulture = newCulture;

                        sLogLine("Program Culture set to " + ACultureLang_Country);
                        _sCultureName = ACultureLang_Country;
                        bOk = true;
                    }
                    string fltDec = sDeterminProgDecimal(); // check decimal '.' or ','

                    DateTimeFormatInfo dtInfo = DateTimeFormatInfo.CurrentInfo;
                    string am = dtInfo.AMDesignator;   // should now be AM + PM
                    string pm = dtInfo.PMDesignator;

                    sLogLine("culture info: " + am + "/" + pm + " decimal=" + fltDec);
                }
                catch (Exception ex)
                {
                    sLogException("Set program culture failed: " + ACultureLang_Country, ex);
                }
            }
            return bOk;
        }
        public static void sSetShowDateTimeImperialFormat(String ACultureName, bool AbImperial, string ADateFormat, string ATimeFormat)
        {
            sbProgChangeCulture(ACultureName);
            sSetImperial(AbImperial);

            if (ADateFormat != null && ADateFormat.Length > 0)
            {
                _sShowDateFormat = ADateFormat;
            }
            else
            {
                _sShowDateFormat = AbImperial ? "MM/dd/yyyy" : "dd-MM-yyyy";
            }
            if (ATimeFormat != null && ATimeFormat.Length > 0)
            {
                _sShowTimeFormat = ATimeFormat;
            }
            else
            {
                _sShowTimeFormat = AbImperial ? "hh:mm:ss tt" : "HH:mm:ss";
            }


            if (_sShowTimeFormat.IndexOf('t') >= 0)
            {
                // contains the t time AM/PM request
                DateTimeFormatInfo dtInfo = DateTimeFormatInfo.CurrentInfo;
                if (dtInfo != null)
                {
                    string am = dtInfo.AMDesignator;
                    string pm = dtInfo.PMDesignator;

                    if (am == null || am.Length == 0 || pm == null || pm.Length == 0)
                    {
                        string s = "Missing required AM/PM";
                        CProgram.sLogError(s);
                        if (CProgram.sbAskYesNo(s, "Replace current culture info by invariant culture?"))
                        {
                            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;

                            dtInfo = DateTimeFormatInfo.CurrentInfo;
                            am = dtInfo.AMDesignator;   // should now be AM + PM
                            pm = dtInfo.PMDesignator;

                            string fltDec = sDeterminProgDecimal(); // check decimal '.' or ','

                            s = "Replaced culture info by invariant: " + am + "/" + pm + " decimal=" + fltDec;
                            CProgram.sLogLine(s);
                        }
                    }
                }
            }
            _sShowTimeFraction = _sShowTimeFormat + ".fff";
            _sShowDateTimeFormat = _sShowDateFormat + " " + _sShowTimeFormat;
            _sShowDateTimeFraction = _sShowDateTimeFormat + ".fff";
        }

        public static string sDateTimeToFormatString(DateTime ADateTime, string ADateTimeFormat)
        {
            if (ADateTime == DateTime.MinValue)
            {
                return "";
            }
            return ADateTime.ToString(ADateTimeFormat, CultureInfo.InvariantCulture);
        }
        public static string sDateTimeToString(DateTime ADateTime)    // direct string no Local/UTC check
        {
            if (ADateTime == DateTime.MinValue)
            {
                return "";
            }
            return ADateTime.ToString(_sShowDateTimeFormat, CultureInfo.InvariantCulture);
        }
        public static string sUtcTimeZoneToString(DateTime ADateTimeUTC, Int16 ATimeZoneMin, bool AbShowTimeZone)    // direct string no Local/UTC check
        {
            string s = "";

            if (ADateTimeUTC != DateTime.MinValue)
            {
                DateTime dt = ADateTimeUTC.AddMinutes(ATimeZoneMin);

                s += dt.ToString(_sShowDateTimeFormat, CultureInfo.InvariantCulture);

                if(AbShowTimeZone)
                {
                    s += CProgram.sTimeZoneOffsetStringLong(ATimeZoneMin);
                }
            }
            return s;
        }


        public static string sUtcToLocalString(DateTime ADateTime)    // direct string no Local/UTC check
        {
            if (ADateTime == DateTime.MinValue)
            {
                return "";
            }
            DateTime dt = sUtcToLocal(ADateTime);

            return dt.ToString(_sShowDateTimeFormat, CultureInfo.InvariantCulture);
        }
        public static string sDateToString(DateTime ADateTime)
        {
            if (ADateTime == DateTime.MinValue)
            {
                return "";
            }
            return ADateTime.ToString(_sShowDateFormat, CultureInfo.InvariantCulture);
        }
        public static string sTimeToString(DateTime ADateTime)
        {
            if (ADateTime == DateTime.MinValue)
            {
                return "";
            }
            string f = "h:mm:ss tt x";
            string s = ADateTime.ToString(f, CultureInfo.InvariantCulture);
            return ADateTime.ToString(_sShowTimeFormat, CultureInfo.InvariantCulture);
        }

        public static string sDateTimeToYMDHMS(DateTime ADateTime)
        {
            if (ADateTime == DateTime.MinValue)
            {
                return "00000000000000";
            }
            string s = ADateTime.Year.ToString("D04") + ADateTime.Month.ToString("D02") + ADateTime.Day.ToString("D02")
                 + ADateTime.Hour.ToString("D02") + ADateTime.Minute.ToString("D02") + ADateTime.Second.ToString("D02");
            return s;
        }
        public static string sSecToDHMS(UInt32 ASeconds)
        {
            UInt32 sec = ASeconds % 60;
            UInt32 rest = ASeconds / 60;
            UInt32 min = rest % 60;
            rest = rest / 60;
            UInt32 hour = rest % 24;
            UInt32 day = rest / 24;

            string s = day.ToString("00") + "d" + hour.ToString("00") + "h" + min.ToString("00") + sec.ToString("00");

            return s;
        }
        public static string sDateToYMD(DateTime ADateTime)
        {
            if (ADateTime == DateTime.MinValue)
            {
                return "00000000";
            }
            string s = ADateTime.Year.ToString("D04") + ADateTime.Month.ToString("D02") + ADateTime.Day.ToString("D02");
            return s;
        }
        public static string sTimeToStringDTF(DateTime ADateTime)
        {
            if (ADateTime == DateTime.MinValue)
            {
                return "";
            }
            return ADateTime.ToString(_sShowTimeFraction, CultureInfo.InvariantCulture);
        }
        public static string sDateTimeToStringDTF(DateTime ADateTime)
        {
            if (ADateTime == DateTime.MinValue)
            {
                return "";
            }
            return ADateTime.ToString(_sShowDateTimeFraction, CultureInfo.InvariantCulture);
        }

        public static string sDateTimeStringToUTC(DateTime ADateTime)   // get UTC time converted string
        {
            if (ADateTime == DateTime.MinValue)
            {
                return "";
            }
            DateTime dt = sDateTimeToUTC(ADateTime);
            return dt.ToString(_sShowDateTimeFormat, CultureInfo.InvariantCulture);
        }
        public static string sDateStringToUTC(DateTime ADateTime)
        {
            if (ADateTime == DateTime.MinValue)
            {
                return "";
            }
            DateTime dt = sDateTimeToUTC(ADateTime);
            return dt.ToString(_sShowDateFormat, CultureInfo.InvariantCulture);
        }
        public static string sTimeStringToUTC(DateTime ADateTime)
        {
            if (ADateTime == DateTime.MinValue)
            {
                return "";
            }
            DateTime dt = sDateTimeToUTC(ADateTime);
            return dt.ToString(_sShowTimeFormat, CultureInfo.InvariantCulture);
        }
        public static string sTimeStringToUTCF(DateTime ADateTime)
        {
            if (ADateTime == DateTime.MinValue)
            {
                return "";
            }
            DateTime dt = sDateTimeToUTC(ADateTime);
            return dt.ToString(_sShowTimeFraction, CultureInfo.InvariantCulture);
        }
        public static string sDateTimeStringToUTCF(DateTime ADateTime)
        {
            if (ADateTime == DateTime.MinValue)
            {
                return "";
            }
            DateTime dt = sDateTimeToUTC(ADateTime);
            return dt.ToString(_sShowDateTimeFraction, CultureInfo.InvariantCulture);
        }
        public static string sDateTimeStringToLocal(DateTime ADateTime)   // get Local time converted string
        {
            if (ADateTime == DateTime.MinValue)
            {
                return "";
            }
            DateTime dt = sDateTimeToLocal(ADateTime);
            return dt.ToString(_sShowDateTimeFormat, CultureInfo.InvariantCulture);
        }
        public static string sDateStringToLocal(DateTime ADateTime)
        {
            if (ADateTime == DateTime.MinValue)
            {
                return "";
            }
            DateTime dt = sDateTimeToLocal(ADateTime);
            return dt.ToString(_sShowDateFormat, CultureInfo.InvariantCulture);
        }
        public static string sTimeStringToLocal(DateTime ADateTime)
        {
            if (ADateTime == DateTime.MinValue)
            {
                return "";
            }
            DateTime dt = sDateTimeToLocal(ADateTime);
            return dt.ToString(_sShowTimeFormat, CultureInfo.InvariantCulture);
        }
        public static string sTimeStringToLocalF(DateTime ADateTime)
        {
            if (ADateTime == DateTime.MinValue)
            {
                return "";
            }
            DateTime dt = sDateTimeToLocal(ADateTime);
            return dt.ToString(_sShowTimeFraction, CultureInfo.InvariantCulture);
        }
        public static string sDateTimeStringToLocalF(DateTime ADateTime)
        {
            if (ADateTime == DateTime.MinValue)
            {
                return "";
            }
            DateTime dt = sDateTimeToLocal(ADateTime);
            return dt.ToString(_sShowDateTimeFraction, CultureInfo.InvariantCulture);
        }
        public static string sPrintTimeSpan_dhmSec(double ASeconds, string ASeperator, UInt16 ANrParts = 99)
        {
            string s = "-";

            if (ASeconds > 0.5)
            {
                UInt32 totalSec = (UInt32)(ASeconds + 0.5);
                UInt32 days, hours, min;
                UInt16 c = 0;

                s = "";
                days = totalSec / (24 * 3600);
                totalSec -= days * (24 * 3600);
                hours = totalSec / 3600;
                totalSec -= hours * 3600;
                min = totalSec / 60;
                totalSec -= min * 60;

                if (days > 0)
                {
                    ++c;
                    s += days.ToString() + "d" + ASeperator;
                    if (hours < 10) s += "0";
                }
                if (c > 0 || hours > 0)
                {
                    ++c;
                    s += hours.ToString() + "h" + ASeperator;
                }
                if (c < ANrParts)
                {
                    s += min.ToString("D2");
                    c++;
                    if (c < ANrParts)
                    {
                        s += ":" + totalSec.ToString("D2");
                    }
                    else
                    {
                        s += "m";
                    }
                }
            }
            else if( ASeconds < -0.5 )
            {
                s = "<0";
            }
            return s;
        }

        public static string sPrintTimeSpan_dhMin(double AMinutes, char ASeperatorChar, UInt16 ANrParts = 99)
        {
            string s = "-";

            if (AMinutes > 0.5)
            {
                UInt32 totalMin = (UInt32)(AMinutes + 0.5);
                UInt32 days, hours;
                UInt16 c = 0;

                s = "";
                days = totalMin / (24 * 60);
                totalMin -= days * (24 * 60);
                hours = totalMin / 60;
                totalMin -= hours * 60;

                if (days > 0)
                {
                    ++c;
                    s += days.ToString() + "d";
                    if (ASeperatorChar > 0) s += ASeperatorChar;
                    if (hours < 10) s += "0";
                }
                if (c > 0 || hours > 0)
                {
                    ++c;
                    s += hours.ToString() + "h";
                    if (ASeperatorChar > 0) s += ASeperatorChar;
                }
                if (c < ANrParts)
                {
                    s += totalMin.ToString("D2") + "m";
                }
            }
            else if (AMinutes < -0.5)
            {
                s = "<0";
            }

            return s;
        }

        public static bool sCalcTimeZoneOffset(out Int32 ArTimeZoneOffsetMin, DateTime AUtc, DateTime ALocalDT)
        {
            bool bOk = false;
            ArTimeZoneOffsetMin = 0;

            if (AUtc != null && ALocalDT != null)
            {
                if (AUtc.Kind != DateTimeKind.Utc)
                {
                    CProgram.sLogError("DateTime not UTC");
                }
                else if (ALocalDT.Kind == DateTimeKind.Utc)
                {
                    CProgram.sLogError("DateTime not Local");
                }
                else if (AUtc == DateTime.MinValue)
                {
                    CProgram.sLogError("UTC DateTime is not valid");
                }
                else if (ALocalDT == DateTime.MinValue)
                {
                    CProgram.sLogError("Local DateTime is not valid");
                }
                else
                {
                    TimeSpan ts = ALocalDT - AUtc;
                    double d = ts.TotalMinutes;
                    TimeSpan sec = new TimeSpan(0, 0, 1);

                    if (d < 0) ts -= sec; else ts += sec;

                    ArTimeZoneOffsetMin = (Int32)(ts.TotalMinutes);
                    bOk = true;
                }
            }
            return bOk;
        }
        public static DateTime sCalcLocalDT(DateTime AUtc, Int32 ATimeZoneOffsetMin)
        {
            if (AUtc != null && AUtc != DateTime.MinValue)
            {
                DateTime dt;

                dt = DateTime.SpecifyKind(AUtc, DateTimeKind.Local);
                dt = dt.AddMinutes(ATimeZoneOffsetMin);
                return dt;
            }
            return DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Local); ;
        }
        private static void sProgSetTrueFalse(string ATrueText, string AFalseText)
        {
            if (String.IsNullOrEmpty(ATrueText) == false)
            {
                _sShowTrueText = ATrueText;
                _sShowTrueLetter = _sShowTrueText.Substring(0, 1);

            }
            if (String.IsNullOrEmpty(AFalseText) == false)
            {
                _sShowFalseText = AFalseText;
                _sShowFalseLetter = _sShowFalseText.Substring(0, 1);
            }
        }

        public static string sPrintDateTimeVersion(DateTime ADateTime)
        {
            string s = "";

            if (ADateTime != null && ADateTime != DateTime.MinValue)
            {
                s = CProgram.sDateTimeToString(DateTime.Now);
            }
            s += CProgram.sGetProgShortVersion();

            if (sbGetImperial())
            {
                s += 'i';
            }
            return s;
        }

        // string functions
        public static string sTrimString(string AString)
        {
            string s = "";
            int len = AString == null ? 0 : AString.Length;

            if (len > 0)
            {
                int start = 0, end = len - 1;

                while (start <= end && AString[start] <= ' ') ++start;
                while (start <= end && AString[end] <= ' ') --end;

                if (start <= end)
                {
                    s = AString.Substring(start, end - start + 1);
                }

            }
            return s;
        }
        public static string sTrimHead(string AString)
        {
            string s = "";
            int len = AString == null ? 0 : AString.Length;

            if (len > 0)
            {
                int start = 0, end = len - 1;

                while (start <= end && AString[start] <= ' ') ++start;
                // while (start <= end && AString[end] <= ' ') --end;

                if (start <= end)
                {
                    s = AString.Substring(start, end - start + 1);
                }

            }
            return s;
        }
        public static string sTrimTail(string AString)
        {
            string s = "";
            int len = AString == null ? 0 : AString.Length;

            if (len > 0)
            {
                int start = 0, end = len - 1;

                // while (start <= end && AString[start] <= ' ') ++start;
                while (start <= end && AString[end] <= ' ') --end;

                if (start <= end)
                {
                    s = AString.Substring(start, end - start + 1);
                }

            }
            return s;
        }
        public static string sGetFirstWord(string AString)
        {
            string s = "";
            int len = AString == null ? 0 : AString.Length;

            if (len > 0)
            {
                int start = 0, end = len - 1;

                while (start <= end && AString[start] <= ' ') ++start;
                end = start;
                while (end < len && AString[end] > ' ') ++end;

                if (start <= end)
                {
                    s = AString.Substring(start, end - start);
                }
            }
            return s;
        }
        public static string sGetFirstLine(string AString)
        {
            string s = "";
            int len = AString == null ? 0 : AString.Length;

            if (len > 0)
            {
                int start = 0, end = len - 1;

                while (start <= end && AString[start] <= ' ') ++start;
                end = start;
                while (end < len)
                {
                    char c = AString[end];

                    if (c == '\r' || c == '\n')
                    {
                        break;
                    }
                    ++end;
                }
                if (start <= end)
                {
                    s = AString.Substring(start, end - start);
                }
            }
            return s;
        }

        public static bool sbSplitNameValue(string AString, out string ArName, out string ArValue)
        {
            bool bRead = false;
            int pos;
            string name = "", value = "";

            if (AString != null && AString.Length > 0 && (pos = AString.IndexOf('=')) > 0)
            {
                name = CProgram.sTrimString(AString.Substring(0, pos));
                value = CProgram.sTrimString(AString.Substring(pos + 1));
                bRead = true;
            }
            ArName = name;
            ArValue = value;
            return bRead;
        }


        //
        public static void sSetImperial(bool AbImperial)
        {
            _mbShowImperial = AbImperial;
        }
        public static bool sbGetImperial()
        {
            return _mbShowImperial;
        }

        public static float sShowUserWeightValue(float AWeightKg)
        {
            return _mbShowImperial ? AWeightKg * _cKgToLbs : AWeightKg;

            //_cKgToLbs = 2.20462262F; // 1 kilogram = 2.20462262 pounds
            //_cMeterToFeet = 3.2808399F; // 1 meter = 3.2808399 feet(3 feet 3⅜ inches)
            //_cFeetToInc = 12.0F; // 1 foot = 12 inches
        }
        public static string sShowUserWeightUnit()
        {
            return _mbShowImperial ? "lbs" : "kg";
        }

        public static float sConvertUserWeight(float AUserValue)
        {
            return _mbShowImperial ? AUserValue / _cKgToLbs : AUserValue;
        }

        public static float mShowUserLength(float ALengthM)
        {
            return _mbShowImperial ? ALengthM * _cMeterToFeet : ALengthM;
        }
        public static float mConvertUserLength(float AUserLength)
        {
            return _mbShowImperial ? AUserLength / _cMeterToFeet : AUserLength;
        }
        public static string mShowUserUnit()
        {
            return _mbShowImperial ? "feet" : "m";
        }
        public static string mShowUserMinorUnit()
        {
            return _mbShowImperial ? "inch" : "cm";
        }
        public static void mShowUserLength2(float ALengthM, out float ArLengthMajor, out float ArLengthMinor)
        {
            if (_mbShowImperial)
            {
                float feet = ALengthM * _cMeterToFeet;
                int i = (int)(feet);
                ArLengthMajor = i;
                ArLengthMinor = (feet - i) * _cFeetToInc;
            }
            else
            {
                int i = (int)(ALengthM);
                ArLengthMajor = i;
                ArLengthMinor = (ALengthM - i) * 100.0F;

            }
        }
        public static float mConvertUserLength(float AUserLengthMajor, float AUserLengthMinor)
        {
            float lengthM;

            if (_mbShowImperial)
            {
                lengthM = AUserLengthMajor + AUserLengthMinor / _cFeetToInc;
                lengthM /= _cMeterToFeet;
            }
            else
            {
                lengthM = AUserLengthMajor + AUserLengthMinor * 0.01F;

            }
            return lengthM;
        }


        // basic log function
        public static string sGetLogTypeName(DDebugFlags AMessageType)
        {
            switch (AMessageType)
            {
                case DDebugFlags.Enabled: return "Enabled";
                case DDebugFlags.Time: return "Time";
                case DDebugFlags.Class: return "Class";
                case DDebugFlags.Reversed: return "Reversed";
                //                case DDebugFlags.UseBuffer: return "UseBuffer";
                case DDebugFlags.Log: return "Log";
                case DDebugFlags.Info: return "Info";
                case DDebugFlags.Warning: return "Warning";
                case DDebugFlags.Choice: return "Choise";
                case DDebugFlags.Error: return "Error";
                case DDebugFlags.Exception: return "Exception";
                case DDebugFlags.Debug: return "Debug";
            }
            return "?" + ((int)AMessageType).ToString() + "?";
        }

        public static char sGetLogMarker(DDebugFlags AMessageType)
        {
            char c = '|';

            switch (AMessageType)
            {
                //                case DDebugFlags.Enabled: return '';
                //                case DDebugFlags.Time: return '';
                //                case DDebugFlags.Class: return '';
                //                case DDebugFlags.Reversed: return '';
                //                case DDebugFlags.UseBuffer: return '';
                case DDebugFlags.Log: return '|';
                case DDebugFlags.Info: return '#';
                case DDebugFlags.Warning: return '!';
                case DDebugFlags.Choice: return '?';
                case DDebugFlags.Error: return '$';
                case DDebugFlags.Exception: return '&';
                case DDebugFlags.Debug: return '[';
            }
            return c;
        }
        private static void sProgLogLine(DDebugFlags AMessageType,
                   CClassFuncLine AClassFunc, string ALine)
        {
            DateTime dt = DateTime.Now;

            char c = sGetLogMarker(AMessageType);
            if (_sLogConsole != null) _sLogConsole.mbDoLog(sFnLogConsoleLine, AMessageType, c, ALine, AClassFunc, dt);
            if (_sLogFile != null) _sLogFile.mbDoLog(sFnLogFileLine, AMessageType, c, ALine, AClassFunc, dt);
            if (_sLogScreen != null) _sLogScreen.mbDoLog(sFnLogScreenLine, AMessageType, c, ALine, AClassFunc, dt);
        }

        private static void sProgLogConsoleLine(DDebugFlags AMessageType,
                    string ALine, CClassFuncLine AClassFunc)
        {
            DateTime dt = DateTime.Now;

            char c = sGetLogMarker(AMessageType);

            if (_sLogConsole != null) _sLogConsole.mbDoLog(sFnLogConsoleLine, AMessageType, c, ALine, AClassFunc, dt);
        }
        private static void sProgLogFileLine(DDebugFlags AMessageType,
                   string ALine, CClassFuncLine AClassFunc)
        {
            DateTime dt = DateTime.Now;

            char c = sGetLogMarker(AMessageType);
            if (_sLogFile != null) _sLogFile.mbDoLog(sFnLogFileLine, AMessageType, c, ALine, AClassFunc, dt);
        }
        private static void sProgLogScreenLine(DDebugFlags AMessageType,
                   string ALine, CClassFuncLine AClassFunc)
        {
            DateTime dt = DateTime.Now;

            char c = sGetLogMarker(AMessageType);
            if (_sLogScreen != null) _sLogScreen.mbDoLog(sFnLogScreenLine, AMessageType, c, ALine, AClassFunc, dt);
        }
        // Debug log to Console
        public static void sSetProgLogConsole(DDebugFlags ADebugFlags)
        {
            if (_sLogConsole == null) _sLogConsole = new C_ProgLog();
            if (_sLogConsole != null)
            {
                _sLogConsole.mFlags = ADebugFlags;
                _sLogConsole.mbReady = true;
            }
        }
        private static bool sFnLogConsoleLine(string ALine) // log line to console
        {
            if (ALine != null)
            {
                Console.Write(ALine);
            }
            return true;    // writing to console always possible
        }
        // log to screen functions
        public static void sSetProgLogScreen(DDebugFlags ADebugFlags)
        {
            if (_sLogScreen == null) _sLogScreen = new C_ProgLog();
            if (_sLogScreen != null)
            {
                _sLogScreen.mFlags = ADebugFlags;
            }
        }
        public static void sSetProgLogScreen(TextBox ATextBox, uint AMaxChars, int ALineSize)
        {
            if (_sLogScreen == null) _sLogScreen = new C_ProgLog();
            if (_sLogScreen != null)
            {
                _sLogScreenTextBox = ATextBox;
                _sLogScreen.mLineSize = ALineSize;
                _sLogScreenMaxChars = AMaxChars;
                _sLogScreen.mbReady = ATextBox != null;
            }
        }
        private static bool sFnLogScreenLine(string ALine) // log line to screen
        {
            bool bDone = false;

            if (_sLogScreenTextBox != null && ALine != null)
            {
                try
                {
                    bool bReversed = (_sLogScreen.mFlags & DDebugFlags.Reversed) != 0;
                    string text = _sLogScreenTextBox.Text;
                    int l = text.Length;

                    if (l > _sLogScreenMaxChars)
                    {
                        int n = ((int)_sLogScreenMaxChars * 9) / 10; // reduce text to 90%
                        if (bReversed)
                        {
                            text = text.Substring(0, n);    // remove from tail
                            int i = text.LastIndexOf('\n');
                            if (i > 0 && ++i < n)
                            {
                                text = text.Substring(0, i);    // break after last return
                            }
                        }
                        else
                        {
                            int i = l - n;
                            text = text.Substring(i, n);    // remove from head
                            i = text.IndexOf('\n');
                            if (i > 0)
                            {
                                text = text.Remove(i);  // start after first return
                            }
                        }
                    }
                    if (bReversed)
                    {
                        text = ALine + text;
                    }
                    else
                    {
                        text += ALine;
                    }
                    _sLogScreenTextBox.Text = text;        // put on screen
                    bDone = true;
                }
                catch (Exception /*Ex*/)
                {
                    // just disregard
                }
                // put on screen
            }
            return bDone;       // if not done it will get in the buffer
        }

        // log to file functions
        public static void sSetProgLogFile(DDebugFlags ADebugFlags)
        {
            if (_sLogFile == null) _sLogFile = new C_ProgLog();
            if (_sLogFile != null)
            {
                _sLogFile.mFlags = ADebugFlags;
                _sbLogFileDoCreate = ADebugFlags != 0;
            }
        }
        public static void sSetProgLogFile(StreamWriter AFileStream, int ALineSize, DDebugFlags ADebugFlags)
        {
            if (_sLogFile == null) _sLogFile = new C_ProgLog();
            if (_sLogFile != null && ADebugFlags != 0)
            {
                _sbLogFileDoCreate = false;
                _sLogFileStream = AFileStream;
                _sbLogFileCreated = false;
                _sLogFile.mFlags = ADebugFlags;
                _sLogFile.mLineSize = ALineSize;
                _sLogFile.mbReady = false;
            }
        }
        public static void sOpenProgLogFile(string AProgLogDir, int ALineSize, DDebugFlags ADebugFlags)
        {
            if (_sLogFile == null) _sLogFile = new C_ProgLog();
            if (_sLogFile != null)
            {
                _sLogFileDir = AProgLogDir;
                _sbLogFileDoCreate = true;// create log file on first logging when prog name is known
                _sLogFileStream = null;
                _sbLogFileCreated = false;
                _sLogFile.mFlags = ADebugFlags;
                _sLogFile.mLineSize = ALineSize;
                _sLogFile.mbReady = true;   // create file at next log line
            }
        }
        private static bool sFnLogFileLine(string ALine) // log line to screen
        {
            bool bDone = false;

            if (_sbLogFileDoCreate)
            {
                sProgCreateLogFile();
            }
            if (_sLogFileStream != null)
            {
                try
                {
                    DateTime dt = DateTime.Now;

                    if (dt.Year != _sLogFileYear || dt.Month != _sLogFileMonth || dt.Day != _sLogFileDay)
                    {
                        _sLogFileYear = dt.Year;        // new day in log file, write date
                        _sLogFileMonth = dt.Month;
                        _sLogFileDay = dt.Day;
                        _sLogFileStream.WriteLine("------ " + sDateToString(dt) + " --------------------------------------------------------");
                    }
                }
                catch (Exception /*Ex*/)
                {
                    // just disregard
                }
                // put on screen
            }
            if (_sLogFileStream != null && ALine != null)
            {
                try
                {
                    _sLogFileStream.Write(ALine);   // ALine includes 
                    if (_sbLogDoFlush)
                    {
                        _sLogFileStream.Flush();
                    }
                    bDone = true;
                }
                catch (Exception /*Ex*/)
                {
                    // just disregard
                }
                // put on screen
            }
            return bDone;       // if not done it will get in the buffer
        }

        public static void sProgCloseLogFile()
        {
            _sbLogFileDoCreate = false;
            if (_sLogFileStream != null)
            {
                if (_sbLogFileCreated)
                {
                    _sLogFileStream.Close();
                }
                _sLogFileStream = null;
                _sbLogFileDoCreate = true;
            }
            _sbLogFileCreated = false;
            _sLogFile.mbReady = false;
        }
        private static void sProgCreateLogFile()    // create log file when needed
        {
            if (_sLogFile == null) _sLogFile = new C_ProgLog();
            if (_sLogFile != null)
            {
                if (_sbLogFileDoCreate)
                {
                    if (_sProgName != null && _sProgName.Length > 1)   // need name to be set
                    {
                        _sbLogFileDoCreate = false;    // try one time to create otherwise no logging 
                        try
                        {
                            string path = _sLogFileDir;

                            if (path == null || path.Length < 2)
                            {
                                // path not set then use executable name


                                string url = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                                Uri pathUri = new Uri(url);
                                path = Uri.UnescapeDataString(pathUri.AbsolutePath);
                                if (Directory.Exists(path) == false)
                                {
                                    // executable file directory should exist
                                    throw (new Exception("Exe file path must exist."));
                                }
                            }
                            string runName = _sProgName + "_" + _sProgRunNr.ToString("00");
                            string fileName = runName + "_" + CProgram.sDateTimeToYMDHMS( DateTime.Now ) + ".log";

                            _sLogFilePath = path;
                            _sLogFilePath = Path.Combine(_sLogFilePath, runName);
                            if (Directory.Exists(_sLogFilePath) == false)
                            {
                                Directory.CreateDirectory(_sLogFilePath);
                            }
                            _sLogFilePath = Path.Combine(_sLogFilePath, fileName);

                            _sLogFileStream = new StreamWriter(_sLogFilePath, true);   // append log to existing day log

                            _sbLogFileCreated = _sLogFileStream != null;
                            _sLogFile.mbReady = _sbLogFileCreated;
                        }
                        catch (Exception Ex)
                        {
                            _sLogFile.mbReady = false;
                            sLogError("Failed to create log file " + _sLogFilePath);
                            sPromptException(false, null, "Failed to open log file", Ex);
                        }
                    }
                }
            }
        }
        // log to console and screen and file
        public static void sLogLine(string ALine,
                    [System.Runtime.CompilerServices.CallerLineNumber]int _AutoLN = 0,
                    [System.Runtime.CompilerServices.CallerMemberName]string _AutoFN = "",
                    [System.Runtime.CompilerServices.CallerFilePath] string _AutoCN = "")
        {
            CClassFuncLine cfl = new CClassFuncLine(_AutoLN, _AutoFN, _AutoCN);
            sProgLogLine(DDebugFlags.Log, cfl, ALine);
        }
        public static void sLogInfo(string ALine,
                    [System.Runtime.CompilerServices.CallerLineNumber]int _AutoLN = 0,
                    [System.Runtime.CompilerServices.CallerMemberName]string _AutoFN = "",
                    [System.Runtime.CompilerServices.CallerFilePath] string _AutoCN = "")
        {
            CClassFuncLine cfl = new CClassFuncLine(_AutoLN, _AutoFN, _AutoCN);
            sProgLogLine(DDebugFlags.Info, cfl, ALine);
        }
        public static void sLogWarning(string ALine,
                    [System.Runtime.CompilerServices.CallerLineNumber]int _AutoLN = 0,
                    [System.Runtime.CompilerServices.CallerMemberName]string _AutoFN = "",
                    [System.Runtime.CompilerServices.CallerFilePath] string _AutoCN = "")
        {
            CClassFuncLine cfl = new CClassFuncLine(_AutoLN, _AutoFN, _AutoCN);
            sProgLogLine(DDebugFlags.Warning, cfl, ALine);
        }
        public static void sLogChoise(string ALine,
                   [System.Runtime.CompilerServices.CallerLineNumber]    int _AutoLN = 0,
                    [System.Runtime.CompilerServices.CallerMemberName]string _AutoFN = "",
                    [System.Runtime.CompilerServices.CallerFilePath] string _AutoCN = "")
        {
            CClassFuncLine cfl = new CClassFuncLine(_AutoLN, _AutoFN, _AutoCN);
            sProgLogLine(DDebugFlags.Choice, cfl, ALine);
        }
        public static void sLogError(string ALine,
                   [System.Runtime.CompilerServices.CallerLineNumber]int _AutoLN = 0,
                    [System.Runtime.CompilerServices.CallerMemberName]string _AutoFN = "",
                    [System.Runtime.CompilerServices.CallerFilePath] string _AutoCN = "")
        {
            CClassFuncLine cfl = new CClassFuncLine(_AutoLN, _AutoFN, _AutoCN);
            sProgLogLine(DDebugFlags.Error, cfl, ALine);
        }
        public static void sLogException(string ALine, Exception Ex,
                    [System.Runtime.CompilerServices.CallerLineNumber]int _AutoLN = 0,
                    [System.Runtime.CompilerServices.CallerMemberName]string _AutoFN = "",
                    [System.Runtime.CompilerServices.CallerFilePath] string _AutoCN = "")

        {
            CClassFuncLine cfl = new CClassFuncLine(_AutoLN, _AutoFN, _AutoCN);
            sProgLogLine(DDebugFlags.Exception, cfl, "Exception: " + ALine);
            if (Ex != null)
            {
                sProgLogLine(DDebugFlags.Exception, null, "Exception = " + Ex.ToString());
            }
        }
        public static void sLogDebug(string ALine,
                    [System.Runtime.CompilerServices.CallerLineNumber]int _AutoLN = 0,
                    [System.Runtime.CompilerServices.CallerMemberName]string _AutoFN = "",
                    [System.Runtime.CompilerServices.CallerFilePath] string _AutoCN = "")

        {
            CClassFuncLine cfl = new CClassFuncLine(_AutoLN, _AutoFN, _AutoCN);
            sProgLogLine(DDebugFlags.Debug, cfl, ALine);
        }

        public static string sDeterminProgDecimal()
        {
            // check pc local format settings 
            float f = 3.14F;
            string s = f.ToString();

            _sProgDecimal = sGetDecimal(s);   // check '.' or ','

            return _sProgDecimal == DFloatDecimal.Dot ? "." : ",";
        }

        // user dependend functions
        public static void sSetUserDecimal(DFloatDecimal ADecimal)
        {
            _sShowDecimal = ADecimal;
        }
        public static DFloatDecimal sGetDecimal(string AString)
        {
            if (AString.Contains('.'))
            {
                return DFloatDecimal.Dot;
            }
            return DFloatDecimal.Comma;
        }
        public static string sSwitchDotComma(string AString) // change 12.234,456 -> 12,234.456 or 12,234.456 -> 12.234,456
        {
            string s = AString.Replace('.', '$');
            s = s.Replace(',', '.');

            return s.Replace('$', ',');
        }
        public static string sSwitch2ProgDot(string AString) // change float string to use program readable '.' or ','
        {
            string s = _sProgDecimal == DFloatDecimal.Dot ? AString.Replace(',', '.')
                : AString.Replace('.', ',');

            return s;
        }
        public static char sGetProgDecimalChar()
        {
            return _sProgDecimal == DFloatDecimal.Dot ? '.' : ',';
        }
        public static char sGetDecimalChar(DFloatDecimal ADecimal)
        {
            return ADecimal == DFloatDecimal.Dot ? '.' : ',';
        }
        public static string sCorrectFloat(DFloatDecimal AToDecimal, DFloatDecimal AFromDecimal, string AString)
        {
            if (AString != null && AToDecimal != AFromDecimal)
            {
                return sSwitchDotComma(AString);
            }
            return AString;
        }
        public static string sCorrectFloatFromProg(DFloatDecimal AToDecimal, string AString)
        {
            if (AString != null && AToDecimal != _sProgDecimal)
            {
                return sSwitchDotComma(AString);
            }
            return AString;
        }
        public static string sCorrectToDecimal(DFloatDecimal AToDecimal, string AString)
        {
            if (AString != null)
            {
                if (AToDecimal == DFloatDecimal.Dot)
                {
                    return AString.Replace(',', '.');
                }
                else
                {
                    return AString.Replace('.', ',');
                }
            }
            return AString;
        }
        public static string sCorrectToDotDecimal(string AString)
        {
            if (AString != null)
            {
                return AString.Replace(',', '.');
            }
            return AString;
        }
        public static string sCorrectToProgDecimal(string AString)
        {
            if (AString != null)
            {
                if (_sProgDecimal == DFloatDecimal.Dot)
                {
                    return AString.Replace(',', '.');
                }
                else
                {
                    return AString.Replace('.', ',');
                }
            }
            return AString;
        }

        public static string sCorrectFloatFromUser(DFloatDecimal AToDecimal, string AString)
        {
            if (AString != null && AToDecimal != _sShowDecimal)
            {
                return sSwitchDotComma(AString);
            }
            return AString;
        }
        public static string sCorrectFloatToUser(DFloatDecimal AFromDecimal, string AString)
        {
            if (AString != null && AFromDecimal != _sShowDecimal)
            {
                return sSwitchDotComma(AString);
            }
            return AString;
        }
        public static string sCorrectFloatToProg(DFloatDecimal AFromDecimal, string AString)
        {
            if (AString != null && AFromDecimal != _sProgDecimal)
            {
                return sSwitchDotComma(AString);
            }
            return AString;
        }
        public static string sCorrectFloatProgToUser(string AString)
        {
            if (AString != null && _sProgDecimal != _sShowDecimal)
            {
                return sSwitchDotComma(AString);
            }
            return AString;
        }
        public static string sCorrectFloatUserToProg(string AString)
        {
            if (AString != null && _sProgDecimal != _sShowDecimal)
            {
                return sSwitchDotComma(AString);
            }
            return AString;
        }
        public static string sShowFloat(float AFloat, UInt16 AFloatSize)
        {
            string format = "G";
            if (AFloatSize > 0)
            {
                format += AFloatSize.ToString();

            }
            string s = AFloat.ToString(format);
            return sCorrectFloatProgToUser(s);
        }
        public static string sShowDouble(double ADouble, UInt16 AFloatSize)
        {
            string format = "G";
            if (AFloatSize > 0)
            {
                format += AFloatSize.ToString();

            }
            string s = ADouble.ToString(format);
            return sCorrectFloatProgToUser(s);
        }

        public static string sBoolToProgString(bool AbBool)
        {
            return AbBool ? "True" : "False";
        }
        public static string sBoolToUserString(bool AbBool)
        {
            return AbBool ? _sShowTrueText : _sShowFalseText;
        }

        // parse functions 

        public static bool sbParseBool(string AString, ref bool ArBool)
        {
            bool bOk = false;

            if (AString != null)
            {
                if (0 == AString.CompareTo(_sShowTrueText) || 0 == AString.CompareTo(_sShowTrueLetter))
                {
                    ArBool = true;
                    bOk = true;
                }
                else if (0 == AString.CompareTo(_sShowFalseText) || 0 == AString.CompareTo(_sShowFalseLetter) || 0 == AString.CompareTo("0"))
                {
                    ArBool = false;
                    bOk = true;
                }
                else if (0 == AString.CompareTo("True") || 0 == AString.CompareTo("T") || 0 == AString.CompareTo("1"))
                {
                    ArBool = true;
                    bOk = true;
                }
                else if (0 == AString.CompareTo("False") || 0 == AString.CompareTo("F") || 0 == AString.CompareTo("0"))
                {
                    ArBool = false;
                    bOk = true;
                }
            }
            return bOk;
        }

        public static bool sbParseInt16(string AString, ref Int16 ArValue)
        {
            bool bOk = false;

            if (AString != null)
            {
                bOk = Int16.TryParse(AString, out ArValue);
            }
            return bOk;
        }
        public static bool sbParseUInt16(string AString, ref UInt16 ArValue)
        {
            bool bOk = false;

            if (AString != null)
            {
                bOk = UInt16.TryParse(AString, out ArValue);
            }
            return bOk;
        }
        public static bool sbParseInt32(string AString, ref Int32 ArValue)
        {
            bool bOk = false;

            if (AString != null)
            {
                bOk = Int32.TryParse(AString, out ArValue);
            }
            return bOk;
        }
        public static bool sbParseUInt32(string AString, ref UInt32 ArValue)
        {
            bool bOk = false;

            if (AString != null)
            {
                bOk = UInt32.TryParse(AString, out ArValue);
            }
            return bOk;
        }
        public static bool sbParseFloat(DFloatDecimal AFromDecimal, string AString, ref float ArFloat)
        {
            bool bOk = false;

            if (AString != null)
            {
                string s = sCorrectFloatToProg(AFromDecimal, AString);

                bOk = float.TryParse(s, out ArFloat);
            }
            return bOk;
        }
        public static bool sbParseDouble(DFloatDecimal AFromDecimal, string AString, ref double ArDouble)
        {
            bool bOk = false;

            if (AString != null)
            {
                string s = sCorrectFloatToProg(AFromDecimal, AString);

                bOk = double.TryParse(s, out ArDouble);
            }
            return bOk;
        }

        public static bool sbParseFloat(string AString, ref float ArFloat)
        {
            bool bOk = false;

            if (AString != null)
            {
                string s = sSwitch2ProgDot(AString);

                bOk = float.TryParse(s, out ArFloat);
            }
            return bOk;
        }
        public static bool sbParseDouble(string AString, ref double ArDouble)
        {
            bool bOk = false;

            if (AString != null)
            {
                string s = sSwitch2ProgDot(AString);

                bOk = double.TryParse(s, out ArDouble);
            }
            return bOk;
        }
        public static bool sbParseFloatFromUser(string AString, ref float ArFloat)
        {
            bool bOk = false;

            if (AString != null)
            {
                string s = sCorrectFloatToProg(_sShowDecimal, AString);

                bOk = float.TryParse(s, out ArFloat);
            }
            return bOk;
        }
        public static bool sbParseDoubleFromUser(string AString, ref double ArDouble)
        {
            bool bOk = false;

            if (AString != null)
            {
                string s = sCorrectFloatToProg(_sShowDecimal, AString);

                bOk = double.TryParse(s, out ArDouble);
            }
            return bOk;
        }
        public static bool sbParseDateLocal(string AStringLocal, ref DateTime ArDate) // pase string of local date to DateTime (always local)
        {
            bool bOk = false;

            if (AStringLocal != null)
            {
                DateTime dt = DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Local);
                if (DateTime.TryParse(AStringLocal, out dt))   // returns undefined
                {
                    ArDate = DateTime.SpecifyKind(dt, DateTimeKind.Local);
                    bOk = true;
                }
            }
            return bOk;
        }
        public static bool sbParseDateUser(string AStringLocal, ref DateTime ArDate) // pase string of local date to DateTime (always local)
        {
            bool bOk = false;

            if (AStringLocal != null)
            {
                DateTime dt = DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Local);
                if( DateTime.TryParseExact(AStringLocal, _sShowDateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AllowWhiteSpaces, out dt))
                {
                    ArDate = DateTime.SpecifyKind(dt, DateTimeKind.Local);
                    bOk = true;
                }
            }
            return bOk;
        }

        public static bool sbParseDtUtc(string AString, out DateTime ArUtc, bool AbLogFail = false)
        {
            bool bOk = false;
            DateTime utc = DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Utc);

            try
            {
                if (AString != null)
                {
                    string s = AString;
                    int n = s.Length;
                    DateTimeOffset dto;
                    bool bOffset = false;

                    if (n > 5)
                    {
                        char c = s[n - 5];
                        bOffset = c == '-' || c == '+';
                    }
                    if (bOffset == false)
                    {
                        // does not contain an offset -> add one to get UTC
                        s += "+00:00";
                    }
                    bOk = DateTimeOffset.TryParse(s, out dto);
                    if (AbLogFail && false == bOk)
                    {
                        CProgram.sLogError("Parse Date Time UTC failed for " + AString);
                    }

                    utc = DateTime.SpecifyKind(dto.UtcDateTime, DateTimeKind.Utc);
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Parse Date Time UTC failed for " + AString, ex);
            }

            ArUtc = utc;
            return bOk;
        }
        public static bool sbParseDtLocal(string AString, out DateTime ArDateTime, bool AbLogFail = false)
        {
            bool bOk = false;
            DateTime dt = DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Local);

            try
            {
                if (AString != null)
                {
                    string s = AString;
                    int n = s.Length;
                    DateTimeOffset dto;
                    bool bOffset = false;
                    if (n > 5)
                    {
                        char c = s[n - 5];

                        bOffset = c == '-' || c == '+';
                    }
                    if (bOffset)
                    {
                        bOk = DateTimeOffset.TryParse(s, out dto);
                        if (AbLogFail && false == bOk)
                        {
                            CProgram.sLogError("Parse Date Time Offset Local failed for " + AString);
                        }
                        dt = DateTime.SpecifyKind(dto.DateTime, DateTimeKind.Local);
                    }
                    else
                    {
                        bOk = DateTime.TryParse(s, out dt);
                        if (AbLogFail && false == bOk)
                        {
                            CProgram.sLogError("Parse Date Time Local failed for " + AString);
                        }
                        dt = DateTime.SpecifyKind(dt, DateTimeKind.Local);
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Parse Date Time Local failed for " + AString, ex);
            }
            ArDateTime = dt;
            return bOk;
        }


        public static bool sbParseDtUser(string AString, out DateTime ArDateTime, bool AbLogFail = false)
        {
            bool bOk = false;
            DateTime dt = DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Local);

            try
            {
                if (AString != null)
                {
                    string s = AString;
                    int n = s.Length;
                    DateTimeOffset dto;
                    bool bOffset = false;
                    if (n > 5)
                    {
                        char c = s[n - 5];

                        bOffset = c == '-' || c == '+';
                    }
                    if (bOffset)
                    {
                        bOk = DateTimeOffset.TryParseExact(s, _sShowDateTimeFormat, CultureInfo.InvariantCulture, DateTimeStyles.AllowWhiteSpaces, out dto);
                        if (AbLogFail && false == bOk)
                        {
                            CProgram.sLogError("Parse Date Time Offset User failed for " + AString);
                        }
                        dt = DateTime.SpecifyKind(dto.DateTime, DateTimeKind.Local);
                    }
                    else
                    {
                        bOk = DateTime.TryParseExact(s, _sShowDateTimeFormat, CultureInfo.InvariantCulture, DateTimeStyles.AllowWhiteSpaces, out dt);
                        if (AbLogFail && false == bOk)
                        {
                            CProgram.sLogError("Parse Date Time user failed for " + AString);
                        }
                        dt = DateTime.SpecifyKind(dt, DateTimeKind.Local);
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Parse Date Time Local failed for " + AString, ex);
            }
            ArDateTime = dt;
            return bOk;
        }

        public static bool sbParseTimeSpan(string AStringLocal, ref TimeSpan ArTimeSpan)
        {
            bool bOk = false;

            try
            {
                if (AStringLocal != null)
                {
                    bOk = TimeSpan.TryParse(AStringLocal, out ArTimeSpan);
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Parse Date Time Offset failed for " + AStringLocal, ex);
            }
            return bOk;
        }

        public static bool sbParseYMDHMS(string AString, out DateTime ArDateTime, bool AbLogFail = false)
        {
            bool bOk = false;
            DateTime dt = DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Local);

            try
            {
                if (AString != null)
                {
                    string s = AString;
                    int n = AString == null ? 0 : s.Length;

                    if (n >= 14)
                    {
                        // yyyyMMddhhmmss

                        int year, month, day, hour, min, sec;

                        if (int.TryParse(s.Substring(0, 4), out year)
                                                && int.TryParse(s.Substring(4, 2), out month)
                                                && int.TryParse(s.Substring(6, 2), out day)
                                                && int.TryParse(s.Substring(8, 2), out hour)
                                                && int.TryParse(s.Substring(10, 2), out min)
                                                && int.TryParse(s.Substring(12, 2), out sec))
                        {
                            dt = new DateTime(year, month, day, hour, min, sec, DateTimeKind.Local);
                            bOk = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //                CProgram.sLogException("Parse Date Time Local failed for " + AString, ex);
                bOk = false;
            }
            if (AbLogFail && false == bOk)
            {
                CProgram.sLogError("Parse Date Time Local failed for " + AString);
            }
            ArDateTime = dt;
            return bOk;
        }
        public static bool sbParseYMD(string AString, out DateTime ArDateTime, bool AbLogFail = false)
        {
            bool bOk = false;
            DateTime dt = DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Local);

            try
            {
                if (AString != null)
                {
                    string s = AString;
                    int n = AString == null ? 0 : s.Length;

                    if (n >= 14)
                    {
                        // yyyyMMdd

                        int year, month, day, hour, min, sec;

                        if (int.TryParse(s.Substring(0, 4), out year)
                            && int.TryParse(s.Substring(4, 2), out month)
                            && int.TryParse(s.Substring(6, 2), out day))

                        {
                            dt = new DateTime(year, month, day, 0, 0, 0, DateTimeKind.Local);
                            bOk = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Parse Date Time Local failed for " + AString, ex);
            }
            if (AbLogFail && false == bOk)
            {
                CProgram.sLogError("Parse Date Time Local failed for " + AString);
            }
            ArDateTime = dt;
            return bOk;
        }

        // prompt only functions getting progname in title

        public static void sAskOk(string ATitle, string AText)
        {
            string caption = sMakeProgTitle(ATitle, false, true);

            DialogResult result = MessageBox.Show(_sProgMainForm, AText, caption, MessageBoxButtons.OK,
                    MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, 0);
        }
        public static void sAskOk(Form AParentForm, string ATitle, string AText)
        {
            string caption = sMakeProgTitle(ATitle, false, true);

            DialogResult result = MessageBox.Show(AParentForm, AText, caption, MessageBoxButtons.OK,
                    MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, 0);
        }
        public static void sAskWarning(string ATitle, string AText)
        {
            string caption = sMakeProgTitle(ATitle, false, true);

            DialogResult result = MessageBox.Show(_sProgMainForm, AText, caption, MessageBoxButtons.OK,
                MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, 0);
        }
        public static void sAskWarning(Form AParentForm, string ATitle, string AText)
        {
            string caption = sMakeProgTitle(ATitle, false, true);

            DialogResult result = MessageBox.Show(AParentForm, AText, caption, MessageBoxButtons.OK,
                MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, 0);
        }
        public static void sShowError(string ATitle, string AText)
        {
            string caption = sMakeProgTitle(ATitle, false, true);

            DialogResult result = MessageBox.Show(_sProgMainForm, AText, caption, MessageBoxButtons.OK,
                MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, 0);
        }
        public static void sShowError(Form AParentForm, string ATitle, string AText)
        {
            string caption = sMakeProgTitle(ATitle, false, true);

            DialogResult result = MessageBox.Show(AParentForm, AText, caption, MessageBoxButtons.OK,
                MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, 0);
        }
         public static bool sbAskOkCancel(string ATitle, string AText)
        {
            string caption = sMakeProgTitle(ATitle, false, true);

            DialogResult result = MessageBox.Show(_sProgMainForm, AText, caption, MessageBoxButtons.OKCancel,
                MessageBoxIcon.Question, MessageBoxDefaultButton.Button1, 0);
            return result == DialogResult.OK;
        }
        public static bool sbAskOkCancel(Form AParentForm, string ATitle, string AText)
        {
            string caption = sMakeProgTitle(ATitle, false, true);

            DialogResult result = MessageBox.Show(AParentForm, AText, caption, MessageBoxButtons.OKCancel,
                MessageBoxIcon.Question, MessageBoxDefaultButton.Button1, 0);
            return result == DialogResult.OK;
        }
        public static bool sbAskYesNo(string ATitle, string AText)
        {
            string caption = sMakeProgTitle(ATitle, false, true);

            DialogResult result = MessageBox.Show(_sProgMainForm, AText, caption, MessageBoxButtons.YesNo,
                MessageBoxIcon.Question, MessageBoxDefaultButton.Button1, 0);
            return result == DialogResult.Yes;
        }
        public static bool sbAskYesNo(Form AParentForm, string ATitle, string AText)
        {
            string caption = sMakeProgTitle(ATitle, false, true);

            DialogResult result = MessageBox.Show(AParentForm, AText, caption, MessageBoxButtons.YesNo,
                MessageBoxIcon.Question, MessageBoxDefaultButton.Button1, 0);
            return result == DialogResult.Yes;
        }
        public static bool sbAskYesNoCancel(out bool ArbYes, string ATitle, string AText)
        {
            return sbAskYesNoCancel(_sProgMainForm, out ArbYes, ATitle, AText);
        }

        public static bool sbAskYesNoCancel(Form AParentForm, out bool ArbYes, string ATitle, string AText)
            {
                string caption = sMakeProgTitle(ATitle, false, true);

            ArbYes = false;
            DialogResult result = MessageBox.Show(AParentForm, AText, caption, MessageBoxButtons.YesNoCancel,
                MessageBoxIcon.Question, MessageBoxDefaultButton.Button1, 0);
            if (result == DialogResult.Yes)
            {
                ArbYes = true;
                return true;
            }
            if (result == DialogResult.No)
            {
                return true;
            }
            return false;
        }
        public static bool sbAskAbortRetryIgnore(out bool ArbIgnore, string ATitle, string AText)
        {
            return sbAskAbortRetryIgnore(_sProgMainForm, out ArbIgnore, ATitle, AText);
        }
        public static bool sbAskAbortRetryIgnore(Form AParentForm, out bool ArbIgnore, string ATitle, string AText)
            {
            
            string caption = sMakeProgTitle(ATitle, false, true);

            ArbIgnore = false;
            DialogResult result = MessageBox.Show(AParentForm, AText, caption, MessageBoxButtons.AbortRetryIgnore,
                MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1, 0);
            if (result == DialogResult.Abort)
            {
                return false;
            }
            else if (result == DialogResult.Ignore)
            {
                ArbIgnore = true;
                return true;
            }// retry
            return true;
        }

        // prompt functions get progRunName in title and logs 
        public static void sSetPromptFlags(DDebugFlags AFlags)
        {
            sPromptFlags = AFlags;
        }
        public static DDebugFlags sGetPromptFlags()
        {
            return sPromptFlags;
        }
        public static void sPrompOk(bool AbForced, string ATitle, string AText,
                    [System.Runtime.CompilerServices.CallerLineNumber]int _AutoLN = 0,
                    [System.Runtime.CompilerServices.CallerMemberName]string _AutoFN = "",
                    [System.Runtime.CompilerServices.CallerFilePath] string _AutoCN = "")

        {
            bool bPrompt = AbForced || (sPromptFlags & DDebugFlags.Info) != 0;
            string s = "";
            if (AbForced) s = "Forced"; else if (bPrompt) s = "Prompt";

            sLogLine("PromptOk(" + ATitle + ", " + AText + ") " + s, _AutoLN, _AutoFN, _AutoCN);
            if (bPrompt)
            {
                sAskOk(ATitle, AText);
            }
        }
        public static void sPromptWarning(bool AbForced, string ATitle, string AText,
                    [System.Runtime.CompilerServices.CallerLineNumber]int _AutoLN = 0,
                    [System.Runtime.CompilerServices.CallerMemberName]string _AutoFN = "",
                    [System.Runtime.CompilerServices.CallerFilePath] string _AutoCN = "")
        {
            bool bPrompt = AbForced || (sPromptFlags & DDebugFlags.Warning) != 0;
            string s = "";
            if (AbForced) s = "Forced"; else if (bPrompt) s = "Prompt";

            sLogLine("PromptWarning(" + ATitle + ", " + AText + ") " + s, _AutoLN, _AutoFN, _AutoCN);
            if (bPrompt)
            {
                sAskWarning("Warning " + ATitle, AText);
            }
        }
        public static void sPromptError(bool AbForced, string ATitle, string AText,
                               [System.Runtime.CompilerServices.CallerLineNumber]int _AutoLN = 0,
                    [System.Runtime.CompilerServices.CallerMemberName]string _AutoFN = "",
                    [System.Runtime.CompilerServices.CallerFilePath] string _AutoCN = "")

        {
            bool bPrompt = AbForced || (sPromptFlags & DDebugFlags.Error) != 0;
            string s = "";
            if (AbForced) s = "Forced"; else if (bPrompt) s = "Prompt";

            sLogError("PromptError(" + ATitle + ", " + AText + ") " + s, _AutoLN, _AutoFN, _AutoCN);
            if (bPrompt)
            {
                sShowError("Error " + ATitle, AText);
            }
        }
        public static void sPromptException(bool AbForced, string ATitle, string AText, Exception Ex,
                    [System.Runtime.CompilerServices.CallerLineNumber]int _AutoLN = 0,
                    [System.Runtime.CompilerServices.CallerMemberName]string _AutoFN = "",
                    [System.Runtime.CompilerServices.CallerFilePath] string _AutoCN = "")

        {
            bool bPrompt = AbForced || (sPromptFlags & DDebugFlags.Exception) != 0;
            string s = "";
            if (AbForced) s = "Forced"; else if (bPrompt) s = "Prompt";

            sLogException("PromptError(" + ATitle + ", " + AText + ") " + s, Ex, _AutoLN, _AutoFN, _AutoCN);
            if (bPrompt)
            {
                sShowError("Exception " + ATitle, AText);
            }
        }
        public static bool sbPromptOkCancel(bool AbForced, string ATitle, string AText,
                    [System.Runtime.CompilerServices.CallerLineNumber]int _AutoLN = 0,
                    [System.Runtime.CompilerServices.CallerMemberName]string _AutoFN = "",
                    [System.Runtime.CompilerServices.CallerFilePath] string _AutoCN = "")

        {
            bool bPrompt = AbForced || (sPromptFlags & DDebugFlags.Choice) != 0;
            bool bOk = false;
            string s = "";
            if (AbForced) s = "Forced"; else if (bPrompt) s = "Prompt";

            sLogChoise("PromptOkCancel(" + ATitle + ", " + AText + ") " + s, _AutoLN, _AutoFN, _AutoCN);
            if (bPrompt)
            {
                bOk = sbAskOkCancel(ATitle, AText);
                if (bOk)
                {
                    sLogChoise("PromptOkCancel = OK", _AutoLN, _AutoFN, _AutoCN);
                }
                else
                {
                    sLogChoise("PromptOkCancel = Cancel", _AutoLN, _AutoFN, _AutoCN);
                }
            }
            return bOk;
        }
        public static bool sbPromptYesNo(bool AbForced, string ATitle, string AText,
                               [System.Runtime.CompilerServices.CallerLineNumber]int _AutoLN = 0,
                    [System.Runtime.CompilerServices.CallerMemberName]string _AutoFN = "",
                    [System.Runtime.CompilerServices.CallerFilePath] string _AutoCN = "")

        {
            bool bPrompt = AbForced || (sPromptFlags & DDebugFlags.Choice) != 0;
            bool bOk = false;
            string s = "";
            if (AbForced) s = "Forced"; else if (bPrompt) s = "Prompt";

            sLogChoise("PromptYesNo(" + ATitle + ", " + AText + ")" + s, _AutoLN, _AutoFN, _AutoCN);
            if (bPrompt)
            {
                bOk = sbAskYesNo(ATitle, AText);
                if (bOk)
                {
                    sLogChoise("PromptYesNo = Yes", _AutoLN, _AutoFN, _AutoCN);
                }
                else
                {
                    sLogChoise("PromptYesNo = No", _AutoLN, _AutoFN, _AutoCN);
                }
            }
            return bOk;
        }
        public static bool sbPromptYesNoCancel(bool AbForced, out bool ArbYes, string ATitle, string AText,
                               [System.Runtime.CompilerServices.CallerLineNumber]int _AutoLN = 0,
                    [System.Runtime.CompilerServices.CallerMemberName]string _AutoFN = "",
                    [System.Runtime.CompilerServices.CallerFilePath] string _AutoCN = "")

        {
            bool bPrompt = AbForced || (sPromptFlags & DDebugFlags.Choice) != 0;
            bool bOk = false;
            string s = "";
            if (AbForced) s = "Forced"; else if (bPrompt) s = "Prompt";

            ArbYes = false;
            sLogChoise("PromptYesNoCancel(" + ATitle + ", " + AText + ") " + s,
                        _AutoLN, _AutoFN, _AutoCN);
            if (bPrompt)
            {
                bOk = sbAskYesNoCancel(out ArbYes, ATitle, AText);

                if (bOk)
                {
                    if (ArbYes)
                    {
                        sLogChoise("PromptYesNoCancel = Yes", _AutoLN, _AutoFN, _AutoCN);
                    }
                    else
                    {
                        sLogChoise("PromptYesNoCancel = No", _AutoLN, _AutoFN, _AutoCN);
                    }
                }
                else
                {
                    sLogChoise("PromptYesNoCancel = Cancel", _AutoLN, _AutoFN, _AutoCN);
                }
            }
            return bOk;
        }
        public static bool sbPromptAbortRetryIgnore(bool AbForced, out bool ArbIgnore, string ATitle, string AText,
                               [System.Runtime.CompilerServices.CallerLineNumber]int _AutoLN = 0,
                    [System.Runtime.CompilerServices.CallerMemberName]string _AutoFN = "",
                    [System.Runtime.CompilerServices.CallerFilePath] string _AutoCN = "")

        {
            bool bPrompt = AbForced || (sPromptFlags & DDebugFlags.Choice) != 0;
            bool bOk = false;
            string s = "";
            if (AbForced) s = "Forced"; else if (bPrompt) s = "Prompt";

            ArbIgnore = false;
            sLogChoise("PromptAbortRetryIgnore(" + ATitle + ", " + AText + ") " + s,
                            _AutoLN, _AutoFN, _AutoCN);
            if (bPrompt)
            {
                bOk = sbAskAbortRetryIgnore(out ArbIgnore, ATitle, AText);

                if (bOk)
                {
                    if (ArbIgnore)
                    {
                        sLogChoise("PromptAbortRetryIgnore = Ignore", _AutoLN, _AutoFN, _AutoCN);
                    }
                    else
                    {
                        sLogChoise("PromptAbortRetryIgnore = Retry", _AutoLN, _AutoFN, _AutoCN);
                    }
                }
                else
                {
                    sLogChoise("PromptAbortRetryIgnore = Abort", _AutoLN, _AutoFN, _AutoCN);
                }
            }
            return bOk;
        }

        public static bool sbReqBool(string ATitle, string AVarName, ref bool ArbVar)
        {
            bool bOk = false;

            if (mReqBoolForm == null)
            {
                mReqBoolForm = new ReqBoolForm();
            }
            if (mReqBoolForm != null)
            {
                string title = CProgram.sMakeProgTitle(ATitle, false, true);
                mReqBoolForm.Text = title;
                mReqBoolForm.mInitBool(ArbVar, AVarName);

                mReqBoolForm.ShowDialog();

                bOk = mReqBoolForm.mbGetBoolResult(ref ArbVar);
            }

            return bOk;
        }
        public static bool sbReqInt32(string ATitle, string AVarName, ref Int32 ArVar, string AUnit, Int32 AMinValue, Int32 AMaxValue)
        {
            bool bOk = false;

            if (mReqLabelForm == null)
            {
                mReqLabelForm = new ReqLabelForm();
            }
            if (mReqLabelForm != null)
            {
                string title = CProgram.sMakeProgTitle(ATitle, false, true);
                mReqLabelForm.Text = title;
                mReqLabelForm.mInitInt(AVarName, ArVar, AUnit, AMinValue, AMaxValue);

                mReqLabelForm.ShowDialog();

                bOk = mReqLabelForm.mbGetIntResult(ref ArVar);
            }

            return bOk;
        }
        public static bool sbReqUInt32(string ATitle, string AVarName, ref UInt32 ArVar, string AUnit, UInt32 AMinValue, UInt32 AMaxValue)
        {
            Int32 var = (Int32)ArVar;
            Int32 max = AMaxValue > (UInt32)(Int32.MaxValue) ? Int32.MaxValue : (Int32)AMaxValue;

            bool bOk = sbReqInt32(ATitle, AVarName, ref var, AUnit, (Int32)AMinValue, max);

            if (bOk)
            {
                ArVar = (UInt32)var;
            }
            return bOk;
        }
        public static bool sbReqUInt16(string ATitle, string AVarName, ref UInt16 ArVar, string AUnit, UInt16 AMinValue, UInt16 AMaxValue)
        {
            UInt32 var = (UInt32)ArVar;

            bool bOk = sbReqUInt32(ATitle, AVarName, ref var, AUnit, (UInt32)AMinValue, (UInt32)AMaxValue);

            if (bOk)
            {
                ArVar = (UInt16)var;
            }
            return bOk;
        }
        public static bool sbReqInt16(string ATitle, string AVarName, ref Int16 ArVar, string AUnit, Int16 AMinValue, Int16 AMaxValue)
        {
            Int32 var = (Int32)ArVar;

            bool bOk = sbReqInt32(ATitle, AVarName, ref var, AUnit, (Int32)AMinValue, (Int32)AMaxValue);

            if (bOk)
            {
                ArVar = (Int16)var;
            }
            return bOk;
        }
        public static bool sbReqDouble(string ATitle, string AVarName, ref double ArVar, string AFormat, string AUnit, double AMinValue, double AMaxValue)
        {
            bool bOk = false;

            if (mReqLabelForm == null)
            {
                mReqLabelForm = new ReqLabelForm();
            }
            if (mReqLabelForm != null)
            {
                string title = CProgram.sMakeProgTitle(ATitle, false, true);

                mReqLabelForm.Text = title;
                mReqLabelForm.mInitDouble(AVarName, ArVar, AFormat, AUnit, AMinValue, AMaxValue);

                mReqLabelForm.ShowDialog();

                bOk = mReqLabelForm.mbGetDoubleResult(ref ArVar);
            }

            return bOk;
        }
        public static bool sbReqFloat(string ATitle, string AVarName, ref float ArVar, string AFormat, string AUnit, float AMinValue, float AMaxValue)
        {
            double var = ArVar;
            bool bOk = sbReqDouble(ATitle, AVarName, ref var, AFormat, AUnit, AMinValue, AMaxValue);

            if (bOk)
            {
                ArVar = (float)var;
            }

            return bOk;
        }
        public static bool sbReqLabel(string ATitle, string AVarName, ref string ArVar, string AUnit, bool AbPassword)
        {
            bool bOk = false;

            if (mReqLabelForm == null)
            {
                mReqLabelForm = new ReqLabelForm();
            }
            if (mReqLabelForm != null)
            {
                string title = CProgram.sMakeProgTitle(ATitle, false, true);

                mReqLabelForm.Text = title;
                mReqLabelForm.mInitText(AVarName, ArVar, AUnit, AbPassword);

                mReqLabelForm.ShowDialog();

                bOk = mReqLabelForm.mbGetTextResult(ref ArVar);
            }
            return bOk;
        }
        public static void sSetBusy(bool AbBusy)
        {
            if (_sProgMainForm != null)
            {
                //_sProgMainForm.UseWaitCursor = AbBusy;
                _sProgMainForm.Cursor = AbBusy ? Cursors.WaitCursor : Cursors.Default;
            }
            else
            {
                Application.UseWaitCursor = AbBusy;
            }
        }
        public static UInt16 sCalcAgeYears(UInt16 ABirthYear, UInt16 ABirthMonth, UInt16 ABirthDay, UInt16 ACmpYear, UInt16 ACmpMonth, UInt16 ACmpDay)
        {
            UInt16 ageYears = 0;

            if (ACmpYear >= ABirthYear)
            {
                ageYears = (UInt16)(ACmpYear - ABirthYear);

                if (ageYears > 0 && ACmpMonth <= ABirthMonth)
                {
                    if (ACmpMonth < ABirthMonth)
                    {
                        --ageYears;
                    }
                    else
                    {
                        // equal month
                        if (ACmpDay < ABirthDay)
                        {
                            --ageYears;
                        }
                    }
                }
            }
            return ageYears;
        }
        public static UInt16 sCalcAgeYears(UInt16 ABirthYear, UInt16 ABirthMonth, UInt16 ABirthDay, DateTime ADate)
        {
            UInt16 ageYears = 0;

            if (ADate != null && ADate != DateTime.MinValue)
            {
                int year = ADate.Year;
                int month = ADate.Month;
                int day = ADate.Day;

                ageYears = sCalcAgeYears(ABirthYear, ABirthMonth, ABirthDay, (UInt16)year, (UInt16)month, (UInt16)day);
            }
            return ageYears;
        }

        public static bool sbSplitYMD(UInt32 ADateYYYYMMDD, out UInt16 ArYear, out UInt16 ArMonth, out UInt16 ArDay)
        {
            ArYear = (UInt16)(ADateYYYYMMDD / 10000);
            ArMonth = (UInt16)((ADateYYYYMMDD % 10000) / 100);
            ArDay = (UInt16)(ADateYYYYMMDD % 100);

            return ADateYYYYMMDD > 0;
        }
        public static bool sbSplitYMD(UInt32 ADateYYYYMMDD, out DateTime ArDate)
        {
            bool bOk = ADateYYYYMMDD > 0;
            DateTime dt = DateTime.MinValue;
            try
            {
                if (bOk)
                {
                    dt = new DateTime((int)(ADateYYYYMMDD / 10000), (int)((ADateYYYYMMDD % 10000) / 100), (int)(ADateYYYYMMDD % 100),
                        0, 0, 0, DateTimeKind.Local);
                }
            }
            catch (Exception)
            {
                CProgram.sLogError("Invalid date " + ADateYYYYMMDD.ToString());
                bOk = false;
            }
            ArDate = dt;
            return bOk;
        }

        public static UInt32 sCalcYMD(UInt16 AYear, UInt16 AMonth, UInt16 ADay)
        {
            uint ymd = (UInt32)(AYear * 10000 + AMonth * 100 + ADay);

            if (ymd > 0)
            {
                if (AYear > 9999 || AMonth == 0 || AMonth > 12 || ADay == 0 || ADay > 31)
                {
                    CProgram.sLogError("calcYMD with invalid " + AYear.ToString() + "/" + AMonth.ToString() + "/" + ADay.ToString());
                    ymd = 0;
                }
            }
            return ymd;
        }

        public static UInt32 sCalcYMD(DateTime ADate)
        {
            return ADate == null ? 0 : sCalcYMD((UInt16)ADate.Year, (UInt16)ADate.Month, (UInt16)ADate.Day);
            //            return ADate == null ? 0 : (UInt32)(ADate.Year * 10000 + ADate.Month * 100 + ADate.Day);

        }
        public static DateTime sMakeBeginDay(DateTime ADateTime)
        {
            return ADateTime == DateTime.MinValue ? DateTime.MinValue : new DateTime(ADateTime.Year, ADateTime.Month, ADateTime.Day);
        }
        public static DateTime sMakeEndDay(DateTime ADateTime)
        {
            return ADateTime == DateTime.MinValue || ADateTime == DateTime.MaxValue ? DateTime.MaxValue : new DateTime(ADateTime.Year, ADateTime.Month, ADateTime.Day, 23, 59, 59);
        }


        public static UInt16 sCalcAgeYears(UInt32 ABirthYYYYMMDD, DateTime ADate)
        {
            UInt16 ageYears = 0;
            UInt16 year, month, day;

            if (sbSplitYMD(ABirthYYYYMMDD, out year, out month, out day))
            {
                ageYears = sCalcAgeYears(year, month, day, ADate);
            }
            return ageYears;
        }
        public static UInt16 sGetYear(UInt32 ABirthYYYYMMDD)
        {
            return (UInt16)(ABirthYYYYMMDD / 10000);
        }
        public static bool sbCmpSearchString(string ATest, string AIncludes)
        {
            bool bIncludes = true;

            if (AIncludes != null && AIncludes.Length > 0)
            {
                bIncludes = ATest != null && ATest.Length > 0;

                if (bIncludes)
                {
                    bIncludes = ATest.ToUpper().Contains(AIncludes.ToUpper());
                    //bIncludes = CultureInfo.CompareInfo.IndexOf(ATest, AIncludes, CompareOptions.IgnoreCase) >= 0;
                    //bIncludes = ATest.IndexOf(AIncludes) >= 0;
                }
            }
            return bIncludes;
        }
        public static bool sbParseYear(string AText, out UInt16 AYear, UInt16 AAddBelow100)
        {
            bool bOk = false;
            UInt16 value = 0;

            try
            {
                if (AText == null || AText.Length == 0)
                {
                    bOk = true;
                }
                else
                {
                    string s = AText.Trim();

                    if (s == null || s.Length == 0)
                    {
                        bOk = true;
                    }
                    else if (UInt16.TryParse(s, out value))
                    {
                        if (value < 100)
                        {
                            value += AAddBelow100;
                        }
                        bOk = value < 9999;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            AYear = value;

            return bOk;
        }
        public static bool sbParseMonth(string AText, out UInt16 AMonth)
        {
            bool bOk = false;
            UInt16 value = 0;

            try
            {
                if (AText == null || AText.Length == 0)
                {
                    bOk = true;
                }
                else
                {
                    string s = AText.Trim();

                    if (s == null || s.Length == 0)
                    {
                        bOk = true;
                    }
                    else if (UInt16.TryParse(s, out value))
                    {
                        bOk = value > 0 && value <= 12;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            AMonth = value;

            return bOk;
        }
        public static bool sbParseDay(string AText, out UInt16 ADay)
        {
            bool bOk = false;
            UInt16 value = 0;

            try
            {
                if (AText == null || AText.Length == 0)
                {
                    bOk = true;
                }
                else
                {
                    string s = AText.Trim();

                    if (s == null || s.Length == 0)
                    {
                        bOk = true;
                    }
                    else if (UInt16.TryParse(s, out value))
                    {
                        bOk = value > 0 && value <= 31;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            ADay = value;

            return bOk;
        }
        public static bool sbParseBirthDate(out UInt32 ArYMD, string AYearText, UInt16 AAddYearBelow100, string AMonthText, string ADayText)
        {
            bool bOk = false;
            UInt32 ymd = 0;

            try
            {
                // a lot of test to protect entry of invalid date time

                UInt16 day = 0, month = 0, year = 0;

                if (CProgram.sbParseYear(AYearText, out year, AAddYearBelow100)
                    && CProgram.sbParseDay(ADayText, out day))
                {
                    if (day == 0 || year == 0)
                    {
                        if (day == 0 && year == 0)
                        {
                            ymd = 0;
                            bOk = true;
                        }
                    }
                    else
                    {
                        bOk = UInt16.TryParse(AMonthText, out month);

                        if (bOk)
                        {
                            bOk = month > 0 && month <= 12;

                            if (bOk)
                            {
                                DateTime dt = new DateTime(year, month, day, 0, 0, 0, DateTimeKind.Local);

                                // if not a valid date time the exception will return false
                                bOk = dt != null;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                bOk = false;
            }
            ArYMD = ymd;

            return bOk;
        }


        public static void sSetProgUserInactive()
        {
            _sbProgUserActive = false;

        }
        public static bool sSetProgUser(UInt32 AUserIndex, string AUserInitials, string AUserScreenName, string APin)
        {
            bool bOk = false;

            _sProgUserIX = 0;      // user index
            _sProgUserInitials = null;
            _sProgUserScreenName = null;
            _sProgUserPin = null;
            _sbProgUserActive = false;
            _sProgUserLastActive = DateTime.MinValue;

            if (/*AUserIndex > 0 && still is 0*/ AUserInitials != null && AUserInitials.Length > 0 && AUserScreenName != null && AUserScreenName.Length > 0 && APin != null/* && APin.Length > 0*/)
            {
                _sProgUserIX = AUserIndex;      // user index
                _sProgUserInitials = AUserInitials;
                _sProgUserScreenName = AUserScreenName;
                _sProgUserPin = APin;
                _sbProgUserActive = true;
                _sProgUserLastActive = DateTime.Now;
                bOk = true;

            }
            return bOk;
        }

        public static void sProgUserInit(int AUserTimeOutMin)
        {
            _sProgUserTimeOutMin = AUserTimeOutMin;
            if (_sProgUserTimeOutMin <= 0)
            {
                _sbProgUserActive = _sProgUserInitials != null && _sProgUserInitials.Length > 0;
            }
        }
        public static bool sbCheckProgUserActive()
        {
            if (_sbProgUserActive && _sProgUserTimeOutMin > 0)
            {
                double min = (DateTime.Now - _sProgUserLastActive).TotalMinutes;

                _sbProgUserActive = min < _sProgUserTimeOutMin;

            }
            return _sbProgUserActive;
        }

        public static bool sbCheckProgkUserPin(bool AbAskPin)
        {
            if (_sbProgUserActive || AbAskPin)
            {
                if (_sProgUserTimeOutMin > 0)
                {
                    double min = (DateTime.Now - _sProgUserLastActive).TotalMinutes;

                    _sbProgUserActive = min < _sProgUserTimeOutMin;
                }
                else
                {
                    _sbProgUserActive = _sProgUserInitials != null && _sProgUserInitials.Length > 0;
                }
                if (_sbProgUserActive || AbAskPin)
                {
                    string pin = "";

                    if (_sProgUserPin == null || _sProgUserPin == "")
                    {
                        _sbProgUserActive = true;
                    }
                    else if (CProgram.sbReqLabel("Enter pin for " + _sProgUserInitials, "Pin ", ref pin, "", true))
                    {
                        _sbProgUserActive = _sProgUserPin.Equals(pin);
                    }

                }
            }
            return _sbProgUserActive;
        }

        public static bool sbEnterProgUserArea(bool AbAskPin)
        {
            if (false == sbCheckProgUserActive())
            {
                if (_sProgUserInitials == null || _sProgUserInitials.Length == 0)
                {
                    CProgram.sPromptWarning(false, "User not active", "Select user first.");
                }
                else
                if (/* stil 0 _sProgUserIX > 0 &&*/ AbAskPin)
                {
                    sbCheckProgkUserPin(true);
                }
            }
            if (_sbProgUserActive)
            {
                _sProgUserLastActive = DateTime.Now;
            }
            return _sbProgUserActive;

        }

        /*        public static void sSetProgUserTimeoutMin(UInt16 ATimeoutMin)
                {
                    if (ATimeoutMin > 0)
                    {
                        _sProgUserInactiveMin = ATimeoutMin;
                    }
                }
                */
        public static UInt16 sGetProgUserMinLeft()
        {
            UInt16 minLeft = 0;

            if (_sbProgUserActive)
            {
                if (_sProgUserTimeOutMin > 0)
                {
                    double min = (DateTime.Now - _sProgUserLastActive).TotalMinutes;

                    _sbProgUserActive = min < _sProgUserTimeOutMin;
                    if (_sbProgUserActive)
                    {
                        minLeft = (UInt16)min;
                    }
                }
                else
                {
                    _sbProgUserActive = _sProgUserInitials != null && _sProgUserInitials.Length > 0;

                    if (_sbProgUserActive)
                    {
                        minLeft = (UInt16)999;
                    }
                }

            }
            return minLeft;

        }
        public static string sGetProgUserInitials()
        {
            return _sProgUserInitials;
        }
        public static string sGetProgUserScreenName()
        {
            return _sProgUserScreenName;
        }
        public static void sSetDateTimePicker(DateTimePicker APicker, DateTime ADateTime)
        {
            if (APicker != null)
            {
                if (ADateTime <= APicker.MinDate)
                {
                    APicker.Value = APicker.MinDate;
                }
                else if (ADateTime >= APicker.MaxDate)
                {
                    APicker.Value = APicker.MaxDate;
                }
                else
                {
                    APicker.Value = ADateTime;
                }

            }
        }
        public static bool sbGetDateTimePicker(DateTimePicker APicker, ref DateTime ArDateTime)
        {
            bool bOk = false;
            if (APicker != null)
            {
                if (APicker.Value <= APicker.MinDate)
                {
                    ArDateTime = DateTime.MinValue;
                }
                else if (APicker.Value >= APicker.MaxDate)
                {
                    ArDateTime = DateTime.MaxValue;
                }
                else
                {
                    ArDateTime = APicker.Value;
                    bOk = true;
                }
            }
            return bOk;
        }

        public static void sSetProgMemLimits(UInt32 AWarnUsedMemSizeMB, UInt32 AWarnFreeMemSizeMB, UInt32 AWarnAvailMemSizeMB,
            UInt32 AMaxWinProgMemSizeMB, UInt32 AMinWinFreeMemSizeMB)
        {
            if (AWarnUsedMemSizeMB > 0) _sWarnUsedMemSizeMB = AWarnUsedMemSizeMB;

            if (AWarnFreeMemSizeMB > 0) _sWarnFreeMemSizeMB = AWarnFreeMemSizeMB;

            if (AWarnAvailMemSizeMB > 0) _sWarnAvailMemSizeMB = AWarnAvailMemSizeMB;

            if (AMaxWinProgMemSizeMB > 0) _sMaxWinProgMemSizeMB = AMaxWinProgMemSizeMB;

            if (AMinWinFreeMemSizeMB > 10) _sMinWinFreeMemSizeMB = AMinWinFreeMemSizeMB;

        }

        public static UInt32 sGetProgUsedMemoryMB()
        {
            //size = (UInt32)(GC.GetTotalMemory(false) / 1000000);
            _sLastUsedMemSizeMB = (UInt32)(Environment.WorkingSet / 1000000);
            return _sLastUsedMemSizeMB;
        }

        public static UInt32 sGetFreeMemoryMB()
        {
            UInt32 memFree = 0;

            try
            {
                System.Management.ManagementObjectSearcher searcher = new System.Management.ManagementObjectSearcher(@"\\localhost\root\cimv2", "select FreePhysicalMemory FROM Win32_OperatingSystem");

                if (searcher != null)
                {
                    System.Management.ManagementObjectCollection coll = searcher.Get();

                    foreach (System.Management.ManagementBaseObject obj in coll)
                    {
                        if (obj != null)
                        {
                            object memObj = obj["FreePhysicalMemory"];
                            // obj["TotalPhysicalMemory"] 

                            if (memObj != null)
                            {
                                string freeMemStr = memObj.ToString();
                                long memLong = 0;

                                if (long.TryParse(freeMemStr, out memLong))
                                {
                                    memFree = (UInt32)(memLong / 1024);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            _sLastFreeMemSizeMB = memFree;

            return _sLastFreeMemSizeMB;
        }

        public static bool sbCheckChangedMemSize(out string ArShowText, bool AbLog = false)
        {
            // UInt32 size = (UInt32)(GC.GetTotalMemory(false) / 1000000);
            UInt32 usedMem = sGetProgUsedMemoryMB();
            UInt32 freeMem = sGetFreeMemoryMB();
            UInt32 availMem = sCalcAvailMemSize(usedMem, freeMem, _sMaxWinProgMemSizeMB, _sMinWinFreeMemSizeMB);

            bool bWarnUsed = usedMem > _sWarnUsedMemSizeMB;
            bool bLog = AbLog;

            if (bWarnUsed)
            {
                if (_sLastWarnUsedMemDT == DateTime.MinValue || (DateTime.Now - _sLastWarnUsedMemDT).TotalSeconds >= 10 || usedMem >= (_sLastWarnUsedMemMB + 100))
                {
                    bLog = true;

                    _sLastWarnUsedMemDT = DateTime.Now;
                    _sLastWarnUsedMemMB = usedMem;
                }
            }
            bool bWarnFree = freeMem != 0 && freeMem < _sWarnFreeMemSizeMB;
            if (bWarnFree)
            {
                if (_sLastWarnFreeMemDT == DateTime.MinValue || (DateTime.Now - _sLastWarnFreeMemDT).TotalSeconds >= 10 || (freeMem + 50) <= _sLastWarnFreeMemMB)
                {
                    bLog = true;

                    _sLastWarnFreeMemDT = DateTime.Now;
                    _sLastWarnFreeMemMB = freeMem;
                }
            }
            bool bWarnAvail = availMem < _sWarnAvailMemSizeMB;
            if (bWarnFree)
            {
                if (_sLastWarnAvailDT == DateTime.MinValue || (DateTime.Now - _sLastWarnAvailDT).TotalSeconds >= 10 || (availMem + 50) <= _sLastAvailMemSizeMB)
                {
                    bLog = true;

                    _sLastWarnAvailDT = DateTime.Now;
                    _sLastAvailMemSizeMB = availMem;
                }
            }

            if (bLog)
            {
                string s = "Memory: " + usedMem.ToString() + "MB in use, " + availMem.ToString() + " MB available";

                if (freeMem > 0)
                {
                    s += ", " + freeMem.ToString() + " MB free";
                }
                CProgram.sLogLine(s);
            }
            _sbLastMemCheckOk = sbShowMemSize(out ArShowText, usedMem, freeMem, availMem,
                _sWarnUsedMemSizeMB, _sWarnFreeMemSizeMB, _sWarnAvailMemSizeMB);

            return _sbLastMemCheckOk;
        }


        private static bool sbShowMemSize(out string ArShowText, UInt32 AProgUsedMB, UInt32 AFreeMemMB, UInt32 AAvailMem,
            UInt32 ACheckMemUsedMB, UInt32 ACheckMemFreeMB, UInt32 ACheckAvailFreeMB)
        {
            //          UInt32 avail = sCalcAvailMemSize()
            bool bWarnUsed = AProgUsedMB >= ACheckMemUsedMB;
            bool bWarnFree = AFreeMemMB != 0 && AFreeMemMB <= ACheckMemFreeMB;
            bool bWarnAvail = AAvailMem < ACheckAvailFreeMB;


            ArShowText = (bWarnUsed ? "app: " : "App: ") + AProgUsedMB.ToString()+ (bWarnUsed ? ">" + ACheckMemUsedMB.ToString() : "") + "MB";
            if (AFreeMemMB > 0)
            {
                if (false == bWarnFree && bWarnAvail)
                {
                    ArShowText += " avail " + AAvailMem.ToString()+"<"+ ACheckAvailFreeMB.ToString()+"MB";
                }
                else
                {
                    ArShowText += (bWarnFree ? " pc: " : " PC: ") + AFreeMemMB.ToString() + (bWarnFree ? "<" + ACheckMemFreeMB.ToString(): "") + "MB";
                }
            }
            /* old
                        ArShowText = (bWarnUsed ? "p-" : "P ") + AProgUsedMB.ToString() + "MB";
                        if (AFreeMemMB > 0)
                        {
                            if (false == bWarnFree && bWarnAvail)
                            {
                                ArShowText += " a-" + AAvailMem;
                            }
                            else
                            {
                                ArShowText += (bWarnFree ? " f-" : " F ") + AFreeMemMB.ToString() + "MB";
                            }
                        }
            */
            return false == bWarnUsed && false == bWarnFree && false == bWarnAvail;
        }

        public static UInt32 sCalcAvailMemSize(UInt32 AProgUsedMB, UInt32 AFreeMemMB,
            UInt32 AMaxWinProgMB, UInt32 AMinWinFreeMB)
        {
            UInt32 free = AFreeMemMB > AMinWinFreeMB ? AFreeMemMB - AMinWinFreeMB : 0;
            UInt32 max = AMaxWinProgMB > AProgUsedMB ? AMaxWinProgMB - AProgUsedMB : 0;

            UInt32 avail = free < max ? free : max;

            return avail;
        }

        public static bool sbCheckMemSize(out string ArShowText, string AAtText, UInt32 ACheckMemUsedMB, UInt32 ACheckMemFreeMB, UInt32 ACheckMemAvailMB,
            bool AbLogWarn, bool AbLogAlways = false)
        {
            //UInt32 size = (UInt32)(GC.GetTotalMemory(false) / 1000000);
            UInt32 progMem = sGetProgUsedMemoryMB();
            UInt32 freeMem = sGetFreeMemoryMB();
            UInt32 availMem = sCalcAvailMemSize(progMem, freeMem, _sMaxWinProgMemSizeMB, _sMinWinFreeMemSizeMB);


            bool bWarnUsed = progMem > ACheckMemUsedMB;
            bool bWarnFree = freeMem != 0 && freeMem < ACheckMemFreeMB;

            if (AbLogAlways || (AbLogWarn && (bWarnUsed || bWarnFree)))
            {
                string s = AAtText + ": Memory= " + progMem.ToString() + (bWarnUsed ? ">=" : "<") + ACheckMemUsedMB.ToString() + "MB in use";

                if (freeMem > 0)
                {
                    s += ", " + freeMem.ToString() + (bWarnFree ? "<=" : ">") + ACheckMemFreeMB.ToString() + " MB free";
                }
                CProgram.sLogLine(s);
            }

            _sbLastMemCheckOk = sbShowMemSize(out ArShowText, progMem, freeMem, availMem,
                                                ACheckMemUsedMB, ACheckMemFreeMB, ACheckMemAvailMB);

            return _sbLastMemCheckOk;
        }

        public static bool sbCheckMemSize(out string ArShowText, string AAtText, bool AbLogWarn, bool AbLogAlways = false)
        {
            return sbCheckMemSize(out ArShowText, AAtText, _sWarnUsedMemSizeMB, _sWarnFreeMemSizeMB, _sWarnAvailMemSizeMB,
                           AbLogWarn, AbLogAlways);

        }
        public static bool sbLogMemSize(string AAtText, bool AbLogAlways = true)
        {
            string showText;
            return sbCheckMemSize(out showText, AAtText, _sWarnUsedMemSizeMB, _sWarnFreeMemSizeMB, _sWarnAvailMemSizeMB,
                           true, AbLogAlways);

        }

        public static void sMemoryCleanup(bool AbDeep, bool AbLog)
        {
            Application.DoEvents();
            UInt32 sizeBefore = sGetProgUsedMemoryMB();
            UInt32 before = sGetProgUsedMemoryMB();
            if (AbDeep)
            {
                GC.Collect();
            }
            else
            {
                GC.Collect(AbDeep ? 2 : 1);
            }
            Application.DoEvents();
            UInt32 after = (UInt32)(GC.GetTotalMemory(AbDeep) / 1000000);
            UInt32 diff = after < before ? before - after : 0;
            bool bLog = AbLog;

            if (false == bLog && after > _sWarnUsedMemSizeMB)
            {
                if (_sLastWarnUsedMemDT == DateTime.MinValue || (DateTime.Now - _sLastWarnUsedMemDT).TotalSeconds >= 60 || after >= (_sLastWarnUsedMemMB + 100))
                {
                    bLog = true;
                }
            }
            bLog = false;
            if (bLog)
            {
                UInt32 size = sGetProgUsedMemoryMB();
                UInt32 freeMem = sGetFreeMemoryMB();
                string s = (AbDeep ? "Deep " : "") + "Memory Cleanup: " + after.ToString() + "MB in use, cleaned " + diff.ToString() + "MB";

                if (freeMem > 0)
                {
                    s += ", free memory  " + freeMem.ToString() + " MB, workSet = " + size.ToString() + "MB" + (size - sizeBefore).ToString();
                }

                if (after > _sWarnUsedMemSizeMB)
                {
                    _sLastWarnUsedMemDT = DateTime.Now;
                    _sWarnUsedMemSizeMB = after;
                    CProgram.sLogError(s);
                }
                else
                {
                    CProgram.sLogLine(s);
                }
            }
        }

        public static bool sbHandleFormClosing(bool AbAskUser, FormClosingEventArgs e)
        {
            string reason = "?";
            bool bAsk = false;
            bool bIsClosing = true; 

            switch (e.CloseReason)
            {
                case CloseReason.ApplicationExitCall:
                    break;
                case CloseReason.FormOwnerClosing:
                    reason = "FormOwnerClosing";
                    break;
                case CloseReason.MdiFormClosing:
                    reason = "MdiFormClosing";
                    break;
                case CloseReason.None:
                    reason = "None";
                    break;
                case CloseReason.TaskManagerClosing:
                    reason = "TaskManagerClosing";
                    break;
                case CloseReason.UserClosing:
                    reason = "UserClosing";
                    if (AbAskUser)
                    {
                        reason = "UserClosing, asking user...";
                        bAsk = true;
                    }
                    break;
                case CloseReason.WindowsShutDown:
                    reason = "WindowsShutDown";
                    break;
                default:
                    reason = "Unknown " + ((int)e.CloseReason).ToString();
                    break;
            }
            CProgram.sLogLine("Form Close request: " + reason);

            if (bAsk)
            {
                string note = "";
                bool bCloseOk = CProgram.sbReqLabel("Program Closing: enter user + reason", "user + reason", ref note, "", false);

                if (bCloseOk)
                {
                    CProgram.sLogLine("User Form Close: " + note);
                }
                else
                {
                    bIsClosing = false;
                    e.Cancel = true;
                    CProgram.sLogLine("Canceled User Close program");
                }
            }
            return bIsClosing;
        }

        private static void sbAddResultText(ref bool ArbNext, ref string ArResult, string AText)
        {
            if (ArbNext)
            {
                ArResult += ", ";
            }
            ArResult += AText;

            ArbNext = true;
        }
        public static bool sbTestFilesWrite(out string ArResult, string ABaseDir, string ATestDir, UInt32 AMaxNrFiles, UInt32 AMaxNrBytes)
        {
            bool bOk = false;
            UInt32 n = 1;
            UInt32 size = 1000;
            string result = "";
            bool bNext = false;
            DateTime startDT = DateTime.Now;

            try
            {
                if (ABaseDir == null || ABaseDir.Length < 5)
                {
                    sbAddResultText(ref bNext, ref result, "Bad Base dir");

                }
                else if (false == Directory.Exists(ABaseDir))
                {
                    sbAddResultText(ref bNext, ref result, "Base dir missing");
                }
                else if (ATestDir == null || ATestDir.Length < 1)
                {
                    sbAddResultText(ref bNext, ref result, "Bad Test dir");
                }
                else
                {
                    startDT = DateTime.Now;
                    string toPath = Path.Combine(ABaseDir, ATestDir);

                    if (false == Directory.Exists(toPath))
                    {
                        try
                        {
                            Directory.CreateDirectory(toPath);
                            sbAddResultText(ref bNext, ref result, "created " + ATestDir);
                        }
                        catch (Exception ex)
                        {
                            CProgram.sLogException("TestWriteFiles", ex);
                            sbAddResultText(ref bNext, ref result, "Failed create test dir " + ATestDir);
                        }
                    }
                    if (false == Directory.Exists(toPath))
                    {
                        sbAddResultText(ref bNext, ref result, "Test dir missing");
                    }
                    else if (false == sbReqUInt32("TestWriteFiles", "create nr files", ref n, "", 1, AMaxNrFiles)
                        || false == sbReqUInt32("TestWriteFiles", "create file size", ref size, "bytes", 1, AMaxNrBytes))
                    {
                        sbAddResultText(ref bNext, ref result, "canceled");
                    }
                    else
                    {
                        string fileName = "?";
                        string toFile = "?";

                        bOk = true;
                        try
                        {
                            for (int i = 0; i < n; ++i)
                            {
                                fileName = "Test" + i.ToString("000000") + ".testFile";
                                toFile = Path.Combine(toPath, fileName);
                                using (FileStream fs = new FileStream(toFile, FileMode.Create))
                                {
                                    int data = i;
                                    int sum = 0;
                                    for (int j = 0; j < size; ++j)
                                    {
                                        byte b8 = (byte)data;

                                        fs.WriteByte(b8);
                                        sum += b8;

                                        ++data;
                                    }
                                    fs.Flush();
                                    fs.Close();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            CProgram.sLogException("TestFilesWrite", ex);
                            sbAddResultText(ref bNext, ref result, "Failed create test file " + toFile);
                            bOk = false;
                        }
                        if (bOk)
                        {
                            sbAddResultText(ref bNext, ref result, "writing " + n.ToString() + " files succeded");
                            bOk = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("TestWriteFiles", ex);
                if (bNext) result += ", ";
                result += "exception error";
                bOk = false;
            }
            if (false == bOk)
            {
                result += "!";
            }
            double sec = (DateTime.Now - startDT).TotalSeconds;
            result += " " + sec.ToString("0.000") + " sec";
            CProgram.sLogLine("TestFilesWrite( " + ABaseDir + " \\ " + ATestDir + " " + n.ToString() + "x" + size.ToString() + " bytes: " + result);
            ArResult = result;
            return bOk;
        }

        private static List<FileInfo> sTestFilesList(ref string ArResult, out UInt32 ArNrFiles, out UInt32 ArAvrgSize, out DateTime ArOldestUtc, string ABaseDir, string ATestDir)
        {
            List<FileInfo> fileList = null;
            UInt32 nrFiles = 0;
            Int64 sumSize = 0;
            DateTime oldestUtc = DateTime.MaxValue;
            string toPath = "?";
            bool bNext = false;

            try
            {

                if (ABaseDir == null || ABaseDir.Length < 5)
                {
                    sbAddResultText(ref bNext, ref ArResult, "Bad Base dir");

                }
                else if (false == Directory.Exists(ABaseDir))
                {
                    sbAddResultText(ref bNext, ref ArResult, "Base dir missing");
                }
                else if (ATestDir == null || ATestDir.Length < 1)
                {
                    sbAddResultText(ref bNext, ref ArResult, "Bad Test dir");
                }
                else
                {
                    toPath = Path.Combine(ABaseDir, ATestDir);

                    if (false == Directory.Exists(toPath))
                    {
                        sbAddResultText(ref bNext, ref ArResult, "Test dir missing");
                    }
                    DirectoryInfo di = new DirectoryInfo(toPath);

                    if (di == null)
                    {
                        sbAddResultText(ref bNext, ref ArResult, "Bad info Test dir");
                    }
                    else
                    {
                        FileInfo[] fiList = di.GetFiles();

                        if (fiList == null)
                        {
                            sbAddResultText(ref bNext, ref ArResult, "no files");
                        }
                        else
                        {
                            fileList = new List<FileInfo>();

                            if (fileList != null)
                            {
                                foreach (FileInfo fi in fiList)
                                {
                                    if (fi.Name.EndsWith(".testFile"))
                                    {
                                        fileList.Add(fi);

                                        if (fi.LastWriteTimeUtc < oldestUtc)
                                        {
                                            oldestUtc = fi.LastWriteTimeUtc;
                                        }
                                        sumSize += fi.Length;

                                        ++nrFiles;
                                    }
                                }
                                if (nrFiles == 0)
                                {
                                    fileList = null;
                                    sbAddResultText(ref bNext, ref ArResult, "no test files");

                                }
                                else
                                {
                                    sumSize /= nrFiles;
                                    sbAddResultText(ref bNext, ref ArResult, nrFiles.ToString() + " files ~" + sumSize.ToString() + " " + CProgram.sDateTimeToString(oldestUtc) + " UTC");
                                }

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("TestFilesList", ex);
                sbAddResultText(ref bNext, ref ArResult, "Failed file list " + toPath);
            }
            ArNrFiles = nrFiles;
            ArAvrgSize = (UInt32)sumSize;
            ArOldestUtc = oldestUtc;
            return fileList;
        }
        public static bool sbTestFilesCount(out string ArResult, string ABaseDir, string ATestDir)
        {
            bool bOk = false;
            UInt32 n = 0;
            UInt32 size = 0;
            string result = "";
            bool bNext = false;
            DateTime oldestUTC;
            DateTime startDT = DateTime.Now;

            try
            {
                List<FileInfo> fileList = sTestFilesList(ref result, out n, out size, out oldestUTC, ABaseDir, ATestDir);

            }
            catch (Exception ex)
            {
                CProgram.sLogException("TestWriteFiles", ex);
                if (bNext) result += ", ";
                result += "exception error";
                bOk = false;
            }
            if (false == bOk)
            {
                result += "!";
            }
            double sec = (DateTime.Now - startDT).TotalSeconds;
            result += " " + sec.ToString("0.000") + " sec";
            CProgram.sLogLine("TestFilesCount( " + ABaseDir + " \\ " + ATestDir + " " + n.ToString() + "x" + size.ToString() + " bytes: " + result);
            ArResult = result;
            return bOk;
        }

        public static bool sbTestFilesDelete(out string ArResult, string ABaseDir, string ATestDir)
        {
            bool bOk = false;
            UInt32 n = 0;
            UInt32 size = 0;
            string result = "";
            bool bNext = false;
            DateTime oldestUTC;
            DateTime startDT = DateTime.Now;

            try
            {
                List<FileInfo> fileList = sTestFilesList(ref result, out n, out size, out oldestUTC, ABaseDir, ATestDir);

                if (fileList == null || n == 0)
                {
                    sbAddResultText(ref bNext, ref result, "Nothing to delete");
                }
                else if (sbReqUInt32("Test Files at " + ATestDir + " " + n.ToString() + "x" + size.ToString() + " bytes UTC=" + CProgram.sDateTimeToString(oldestUTC),
                    "Delete nr test files", ref n, "", 1, n))
                {
                    startDT = DateTime.Now;
                    int i = 0;
                    bNext = true;

                    foreach (FileInfo fi in fileList)
                    {
                        if (++i <= n)
                        {
                            try
                            {
                                fi.Delete();
                                bOk = true;
                            }
                            catch (Exception ex)
                            {
                                CProgram.sLogException("TestFilesDelete", ex);
                                sbAddResultText(ref bNext, ref result, "Failed delete " + fi.Name);
                                bOk = false;
                                break;
                            }
                        }
                    }
                    if (bOk)
                    {
                        sbAddResultText(ref bNext, ref result, "deleted " + n.ToString() + " files succeded");
                    }
                }
                else
                {
                    sbAddResultText(ref bNext, ref result, "canceled");
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("TestFilesDelete", ex);
                if (bNext) result += ", ";
                result += "exception error";
                bOk = false;
            }
            if (false == bOk)
            {
                result += "!";
            }
            double sec = (DateTime.Now - startDT).TotalSeconds;
            result += " " + sec.ToString("0.000") + " sec";
            CProgram.sLogLine("TestFilesDelete( " + ABaseDir + " \\ " + ATestDir + " " + n.ToString() + ": " + result);
            ArResult = result;
            return bOk;
        }

        public static bool sbTestFilesRead(out string ArResult, string ABaseDir, string ATestDir)
        {
            bool bOk = false;
            UInt32 n = 0;
            UInt32 size = 0;
            string result = "";
            bool bNext = false;
            DateTime oldestUTC;
            DateTime startDT = DateTime.Now;

            try
            {
                List<FileInfo> fileList = sTestFilesList(ref result, out n, out size, out oldestUTC, ABaseDir, ATestDir);

                if (fileList == null || n == 0)
                {
                    sbAddResultText(ref bNext, ref result, "Nothing to read");
                }
                else if (sbReqUInt32("Test Files at " + ATestDir + " " + n.ToString() + "x" + size.ToString() + " bytes", "Read nr test files", ref n, "", 1, n))
                {
                    startDT = DateTime.Now;
                    bNext = true;
                    bOk = true;
                    foreach (FileInfo fi in fileList)
                    {
                        try
                        {
                            byte b8, cmp = 0;
                            bool bFirst = true;
                            int value;

                            using (FileStream fs = new FileStream(fi.FullName, FileMode.Open))
                            {
                                while ((value = fs.ReadByte()) >= 0)
                                {
                                    b8 = (byte)value;

                                    if (bFirst)
                                    {
                                        bFirst = false;
                                    }
                                    else if (b8 != cmp)
                                    {
                                        bOk = false;
                                        sbAddResultText(ref bNext, ref result, "Failed read test file " + fi.FullName);
                                        break;
                                    }
                                    cmp = ++b8;
                                }
                                fs.Close();
                            }
                        }
                        catch (Exception ex)
                        {
                            CProgram.sLogException("TestFilesWrite", ex);
                            sbAddResultText(ref bNext, ref result, "Exception reading test file " + fi.FullName);
                            bOk = false;
                        }
                    }
                    if (bOk)
                    {
                        sbAddResultText(ref bNext, ref result, "reading " + n.ToString() + " files succeded");
                    }
                }
                else
                {
                    sbAddResultText(ref bNext, ref result, "canceled");
                }

            }
            catch (Exception ex)
            {
                CProgram.sLogException("TestFilesRead", ex);
                if (bNext) result += ", ";
                result += "exception error";
                bOk = false;
            }
            if (false == bOk)
            {
                result += "!";
            }
            double sec = (DateTime.Now - startDT).TotalSeconds;
            result += " " + sec.ToString("0.000") + " sec";
            CProgram.sLogLine("TestFilesRead( " + ABaseDir + " \\ " + ATestDir + " " + n.ToString() + "x" + size.ToString() + " bytes: " + result);
            ArResult = result;
            return bOk;
        }

        public static void sDoOpenLogFolder()
        {
            try
            {
                if (_sLogFilePath != null && _sLogFilePath.Length > 6)
                {
                    string logFile = Path.GetFullPath(_sLogFilePath);
                    string path = Path.GetDirectoryName(logFile);
                    string name = Path.GetFileName(logFile);
                    ProcessStartInfo startInfo = new ProcessStartInfo();
                    startInfo.FileName = @"explorer";

                    startInfo.Arguments = path; // +  " " + name;
                    Process.Start(startInfo);
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Open Log Folder", ex);
            }
        }
        public static void sDoOpenFolder(string AFolder)
        {
            try
            {
                if (AFolder != null && AFolder.Length > 2)
                {
                    string path = Path.GetFullPath(AFolder);
                    if (Directory.Exists(path))
                    {
                        ProcessStartInfo startInfo = new ProcessStartInfo();
                        startInfo.FileName = @"explorer";

                        startInfo.Arguments = path; // +  " " + name;
                        Process.Start(startInfo);
                    }
                    else
                    {
                        CProgram.sLogError("Open unknown folder " + AFolder);
                        CProgram.sAskOk("Open folder " + AFolder, "Directory does not exist");
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Open Folder " + AFolder, ex);
            }
        }
        public static void sDoOpenLogFile()
        {
            try
            {
                if (_sLogFilePath != null && _sLogFilePath.Length > 6)
                {
                    string logFile = Path.GetFullPath(_sLogFilePath);
                    string path = Path.GetDirectoryName(logFile);
                    string name = Path.GetFileName(logFile);
                    ProcessStartInfo startInfo = new ProcessStartInfo();
                    startInfo.FileName = @"explorer";

                    startInfo.Arguments = logFile;
                    Process.Start(startInfo);
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Open Log Folder", ex);
            }
        }

        public static bool sbCheckFreeDriveSpace(out string ArResult, UInt16 ACheckDiskLocalGB, string ADataPath, UInt16 ACheckDiskDataGB, bool AbLogOK)
        {
            string result = "";
            string logStr = "Drive free space: ";
            bool bOk = true;

            if (ACheckDiskLocalGB > 0 || ACheckDiskDataGB > 0)
            {
                try
                {
                    string driveProg = Path.GetPathRoot(sGetProgDir()).ToUpper();
                    bool bCheckDrive2 = ADataPath != null && ADataPath.Length > 2 && ACheckDiskDataGB > 0;
                    string driveData = "";
                    UInt32 freeDriveLocal = ACheckDiskLocalGB;
                    DriveInfo[] allDrives = DriveInfo.GetDrives();

                    if (bCheckDrive2)
                    {
                        driveData = Path.GetPathRoot(ADataPath).ToUpper();

                        bCheckDrive2 = driveProg != driveData;
                    }

                    if (false == bCheckDrive2 && ACheckDiskDataGB > freeDriveLocal)
                    {
                        freeDriveLocal = ACheckDiskDataGB;
                    }
                    if (AbLogOK)
                    {
                        //CProgram.sLogLine("Check drive1 = " + driveProg + (bCheckDrive2 ? " drive2 = " : " skip drive 2 = ") + driveData);
                    }
                    foreach (DriveInfo d in allDrives)
                    {
                        if (d.IsReady == true)
                        {
                            string driveName = d.Name.ToUpper();
                            if (driveName.StartsWith(driveProg))
                            {
                                logStr += d.Name + "=" + d.AvailableFreeSpace.ToString();
                                int freeGB = (int)(d.AvailableFreeSpace / 1000000000);
                                bool bFreeOk = freeGB >= freeDriveLocal;

                                logStr += (bFreeOk ? ">=" : "<") + freeGB.ToString() + (bFreeOk ? ">=" : "<") + freeDriveLocal.ToString() + "GB ";
                                result += d.Name + " " + freeGB +(bFreeOk ? "" : "<"+freeDriveLocal.ToString()) + "GB ";
                                if (false == bFreeOk)
                                {
                                    bOk = false;
                                }
                                //                                CProgram.sLogLine("Check progDrive: " + logStr);
                            }

                            if (bCheckDrive2 && driveName.StartsWith(driveData))
                            {
                                logStr += d.Name + "=" + d.AvailableFreeSpace.ToString();
                                int freeGB = (int)(d.AvailableFreeSpace / 1000000000);
                                bool bFreeOk = freeGB >= ACheckDiskDataGB;

                                logStr += (bFreeOk ? ">" : "<=") + freeGB.ToString() + (bFreeOk ? ">=" : "<") + ACheckDiskDataGB.ToString() + "GB ";
                                result += d.Name + " " + freeGB + (bFreeOk ? "" : "<" + ACheckDiskDataGB.ToString()) + "GB ";
                                if (false == bFreeOk)
                                {
                                    bOk = false;
                                }
                                //                                CProgram.sLogLine("Check dataDrive: " + logStr);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Free drive space", ex);
                }
            }
            if (false == bOk || AbLogOK)
            {
                CProgram.sLogLine(logStr);
            }
            ArResult = result;
            return bOk;
        }
        public static bool sbTestHttpConnectFromUrl(out string ArContent, string AUrl)
        {
            bool bOk = false;
            string content = "";
            try
            {
                if (AUrl != null && AUrl.Length > 4)
                {
                    // todo FtpWebRequest

                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(AUrl);
                    if (request != null)
                    {

                        request.Method = "GET";
                        request.ContentType = "text/xml; encoding='utf-8'";
                        request.ContinueTimeout = 1000;
                        request.Timeout = 2000;

                        HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            using (Stream receiveStream = response.GetResponseStream())
                            {
                                StreamReader readStream = null;

                                if (response.CharacterSet == null)
                                {
                                    readStream = new StreamReader(receiveStream);
                                }
                                else
                                {
                                    readStream = new StreamReader(receiveStream, Encoding.GetEncoding(response.CharacterSet));
                                }

                                content = readStream.ReadToEnd();
                                bOk = content != null && content.Length > 1;
                                response.Close();
                                readStream.Close();
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                CProgram.sLogException("Read from url failed: " + AUrl, ex);
            }
            ArContent = content;
            return bOk;
        }

        public static bool sbTestFtpConnectFromUrl(out string ArContent, string AUrl)
        {
            bool bOk = false;
            string content = "";
            try
            {
                if (AUrl != null && AUrl.Length > 4)
                {
                    // todo FtpWebRequest
                    string url = AUrl;

                    bool bSsl = url.ToLower().StartsWith("ftps:");

                    if (bSsl)
                    {
                        url = "ftp:" + url.Substring(5);
                    }
                    if (false == url.EndsWith("/"))
                    {
                        url = url + "/";
                    }
                    FtpWebRequest request = (FtpWebRequest)WebRequest.Create(url);
                    if (request != null)
                    {
                        request.EnableSsl = true;
                        request.UsePassive = true;
                        request.Method = WebRequestMethods.Ftp.ListDirectory;
                        // request.ContentType = "text/xml; encoding='utf-8'";
                        request.Timeout = 5000;

                        FtpWebResponse response = (FtpWebResponse)request.GetResponse();

                        if (response.StatusCode == FtpStatusCode.CommandOK)
                        {
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                CProgram.sLogException("Read from FTP failed: " + AUrl, ex);
            }
            ArContent = content;
            return bOk;
        }
        public static bool sbWriteLaunchVersion(bool AbEdit, string AVersionLabel, string AVersionHeader, string[] AVersionText)
        {
            bool bOk = true;

            string versionLabel = AVersionLabel;
            string versionHeader = AVersionHeader;
            string[] versionText = AVersionText;

            string version = _sProgMajor.ToString() + "." + _sProgMinor.ToString() + "." + _sProgBuild.ToString();
            DProgramVersionLevel level = sGetProgVersionLevel();
            string levelString = level == DProgramVersionLevel.Released ? " " : sGetProgVersionLevelLetter();
            string versionStart = version + levelString + " ";
            string buildDate = sGetProgBuildYMD();
            string progName = CProgram.sGetProgName();

            if ( AbEdit)
            {
                string edit = "";
                if( versionText != null )
                {
                        foreach (string line in versionText)
                        {
                            edit += line + "\r\n";
                        }
                }
                bOk = CProgram.sbReqLabel("Make Launch version " + versionStart, "Label", ref versionLabel, "", false)
                    && CProgram.sbReqLabel("Make Launch version " + versionStart, "Header", ref versionHeader, "", false)
                    && ReqTextForm.sbReqText("Make Launch version " + versionStart, "Info", ref edit);
                if( bOk )
                {
                    edit.TrimEnd();
                    versionText = edit.Split('\n');                   
                }
            }
            if( bOk )
            {
                string fileName =  "version.json";
                string versionName = versionStart + buildDate + " " + versionLabel.Trim();
                string versionDir = "";
                int n = versionName.Length;

                for( int i = 0; i < n; ++i)
                {
                    char c = versionName[i];
                    versionDir += char.IsLetterOrDigit(c) || c == '.' ? c : '_';
                }


                try
                {
                    using (StreamWriter fs = new StreamWriter(Path.Combine(sGetProgDir(), fileName)))
                    {
                        if (fs != null)
                        {
                            /*  version.json=
                               {
                               "versionName": "1.7 Beta update",
                               "isBeta": "True",
                               "version": "1.7.2",
                               "ChangeLog": "<h1>Version 1.7 released</h1><ul><li>Fixed Ecg Grid</li></ul>",
                               }
                            */
                            bool bIsBeta = level <= DProgramVersionLevel.Beta;
                            string html = "<h1>" + progName + (bIsBeta ? " Beta " : " Version ") + version + " " + versionHeader + "</h1><ul>";

                            if( versionText != null )
                            {
                                int nLines = versionText.Length;
                                int i = 0;

                                foreach( string line in versionText)
                                {
                                    string li = line.TrimEnd();
                                    if (++i < nLines || li.Length > 0)
                                    {
                                        html += "<li>" + li + "</li>";
                                    }
                                }
                            }
                            html += "</ul>";

                            fs.WriteLine("{");
                            fs.WriteLine("\"remCreated\" : \"" + sGetProgName() + " @ " + CProgram.sDateTimeToFormatString(DateTime.Now, "yyyy/MM/dd HH:mm:ss")
                                + CProgram.sTimeZoneOffsetStringLong(CProgram.sGetLocalTimeZoneOffsetMin()) + "\",");
                            fs.WriteLine("\"versionName\" : \"" + versionName + "\",");
                            fs.WriteLine("\"isBeta\" : \"" + ( bIsBeta ? "True" : "False") + "\",");
                            fs.WriteLine("\"version\" : \"" + version + "\",");
                            fs.WriteLine("\r\n\"ChangeLog\" : \"" + html + "\",");
                            fs.WriteLine("}");

                            fs.Close();

                            bOk = true;
                        }
                    }
                    if( bOk )
                    {
                        CProgram.sLogLine("Writen Launch version " + versionStart + buildDate + " " + versionLabel);
                        Clipboard.SetText(versionDir);

                        bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;
                        bool bShift = (Control.ModifierKeys & Keys.Shift) == Keys.Shift;

                        bool bDoCopy = false;
                        string copyBat = "";
                        string destination = "";

                        if (bShift)
                        {
                            destination = "upload";
                            copyBat = Path.Combine(CProgram.sGetProgDir(), "versionUpload.bat");
                            bDoCopy = File.Exists(copyBat);
                        }
                        if (bCtrl)
                        {
                            destination = "local";
                            copyBat = Path.Combine(CProgram.sGetProgDir(), "versionLocal.bat");
                            bDoCopy = File.Exists(copyBat);
                        }

                        if (bDoCopy)
                        {
                            if( CProgram.sbAskYesNo("Launch Version " + versionStart + buildDate, "Run batch to copy version to "+ destination + " folder?"))
                            {
                                try
                                {
                                        ProcessStartInfo startInfo = new ProcessStartInfo();
                                        startInfo.FileName = copyBat;

                                        startInfo.Arguments = versionDir; // +  " " + name;
                                        Process.Start(startInfo);
                                }
                                catch (Exception ex)
                                {
                                    CProgram.sLogException("Failed run copy version " + destination + " batch", ex);
                                }
                            }
                        }
                        else
                        {
                            CProgram.sAskOk("Launch Version name on clipboard", versionDir);
                        }
                    }
                }
                catch (Exception)
                {
                    bOk = false;
                }
            }

            return bOk;
        }
        public static bool sbIsEmptyConfig( string AConfigString )
        {
            bool bEmpty = true;

            if( AConfigString != null && AConfigString.Length > 0)
            {
                bEmpty = AConfigString[0] == '?';
            }
            return bEmpty;
        }
        public static string sGetObjectString( Object AObject)
        {
            string s = "";

            if( AObject != null)
            {
                try
                {
                    s = AObject.ToString();
                }
                catch( Exception)
                {

                }
            }
            return s;
        }
        public static void sAddLine( ref string ArLines, string AAddLine)
        {
            if(AAddLine != null && AAddLine.Length > 0 )
            {
                if( ArLines != null && ArLines.Length > 0)
                {
                    ArLines = ArLines + "\r\n" + AAddLine;
                }
                else
                {
                    ArLines = AAddLine;
                }
            }
        }
        public static void sAddText(ref string ArText, string AAddText, string ASeperator)
        {
            if (AAddText != null && AAddText.Length > 0)
            {
                if (ArText != null && ArText.Length > 0)
                {
                    ArText = ArText + ASeperator + AAddText;
                }
                else
                {
                    ArText = AAddText;
                }
            }
        }
        public static string sExtractValueLabel(string AText, int AStartIndex)
        {
            String label = "";
            int len = AText.Length;
            int i = AStartIndex;

            while (i < len)
            {
                char c = AText[i];
                if (c == '=')
                {
                    i = label.Length;

                    while (--i > 0)
                    {
                        c = label[i];
                        if (c == ' ')
                        {
                            break;
                        }
                    }
                    if (i == 0)
                    {
                        label = "";

                    }
                    else
                    {
                        label = label.Substring(0, i);
                    }
                    break;
                }
                else if (c == '\t')
                {
                    label += ' ';
                }
                else if (c < ' ')
                {
                    break;
                }
                else
                {
                    label += c;
                }
                ++i;
            }
            return label.Trim();
        }
        public static string sExtractValueLabel(string AText, String AValueName) // X=<label> Y=<label2>\r\n
        {
            String label = "";
            int nameLen = AValueName == null ? 0 : AValueName.Length;

            if ( AText != null && nameLen > 0)
            {
                int pos = AText.IndexOf(AValueName + "=");

                if( pos >= 0)
                {
                    pos += nameLen + 1;
                    label = sExtractValueLabel(AText, pos);
                }
            }
            return label;
        }

        public static bool sbCtrlKeyIsDown()
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            return bCtrl;
        }
        public static bool sbAltKeyIsDown()
        {
            bool bAlt = (Control.ModifierKeys & Keys.Alt) == Keys.Alt;

            return bAlt;
        }
        public static bool sbShiftKeyIsDown()
        {
            bool bShift = (Control.ModifierKeys & Keys.Shift) == Keys.Shift;

            return bShift;
        }
    }
    /*
    public class CTestLockFile
    {
        public string mMakeTestLockFile(string AName)
        {
            string s = "TestLock_" + CProgram.sGetPcName() + "_" + CProgram.sGetUserName() + "_" + AName;
            return s;
        }

        public bool mbStartTestLock(string APath, string AName,
            ref StreamWriter ArLockStream, ref string ArStoreLockPath, ref string ARefExtraLine = null)

        public CTestLockFile( String )
    }
    */
}

