﻿using Program_Base;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventBoard.Program_Base
{
    public class CCsvColumn
    {
        public string _mCodeName = "";
        public bool _mbActive = false;
        public string _mHeader = "";
        public string _mFormat = "";
        private string _mValueText = "";
        private bool _mbValueSet = false;
        private bool _mbValueIsString = false;

        private const string _cBoolFormat01 = "01";
        private const string _cBoolFormatLowerCase = "t;f;true,false;l;L";
        private const string _cBoolFormatUpperCase = "T;F;True,False;l;L";

        public CCsvColumn(string ACodeName, bool AbActive, string AHeader, string AFormat = "")
        {
            _mCodeName = ACodeName;
            _mbActive = AbActive;
            _mHeader = AHeader;
            _mFormat = AFormat;
            _mValueText = "";
            _mbValueSet = false;
            _mbValueIsString = true;
        }

        public string mGetTextValue()
        {
            return _mValueText;
        }
        public bool mbValueIsString()
        {
            return _mbValueIsString;
        }

        public void mSetColumn( bool AbActive, string AHeader, string AFormat = "")
        {
            _mbActive = AbActive;
            _mHeader = AHeader;
            _mFormat = AFormat;
            _mValueText = "";
            _mbValueSet = false;
            _mbValueIsString = true;
        }

        public void mClearValue()
        {
            _mValueText = "";
            _mbValueSet = false;
            _mbValueIsString = true;
        }
        private bool mbSetTextValue( string AValueString, bool AbIsString)
        {
            bool bSet = false;

            if (_mbActive)
            {
                bSet = false == _mbValueSet;
                _mValueText = AValueString;
                _mbValueSet = true;
                _mbValueIsString = AbIsString;

            }
            return bSet;
        }
        public bool mbIsSameCode( string ACodeName)
        {
            bool bSame = false;

            if(ACodeName != null)
            {
                bSame = ACodeName.ToLower() == _mCodeName.ToLower();
            }
            return bSame;
        }

        public bool mbSetValueString(string AValue)
        {
            return mbSetTextValue(AValue, true  );
        }

        public bool mbSetValueUInt32(UInt32 AValue)
        {
            string s = AValue.ToString(_mFormat);

            return mbSetTextValue(s, false);
        }
        public bool mbSetValueInt32( Int32 AValue)
        {
            string s = AValue.ToString(_mFormat);

            return mbSetTextValue(s, false);
        }
        public bool mbSetValueDouble( double AValue)
        {
            string s = AValue.ToString(_mFormat);

            return mbSetTextValue(s, false);
        }
        public bool mbSetValueBool(bool AbValue)
        {
            string s = AbValue ? "1" : "0";

            if (_mFormat == null || _mFormat == "")
            {
                // default format
            }
            else if (0 <= _cBoolFormatLowerCase.IndexOf(_mFormat))
            {
                s = AbValue ? "1" : "0";
            }
            else if (0 <= _cBoolFormatUpperCase.IndexOf(_mFormat))
            {
                s = AbValue ? "1" : "0";
            }
            else // if (0 <= _cBoolFormat01.IndexOf(_mFormat)
            {
                s = AbValue ? "1" : "0";
            }
            return mbSetTextValue(s, false);
        }
    }
    public class CCsvFileWriter
    {
        private string _mLineEnd = "\r\n";
        private string _mSeperator = "\t"; //", "; //"\t"
        private bool _mbAddQuotesText = false;
        private bool _mbAddQuotesAll = false;
        //private bool _mbUseDot = false;
        //private bool _mbChangeDot = false;

        private string _mTotalLines = "";
        private string _mHeaders = "";
        private string _mLastRow = "";
        private List<CCsvColumn> _mColumnList = null;
        private UInt32 _mNrColumns = 0;
        private UInt32 _mNrRows = 0;
        private UInt32 _mNrValues = 0;
        private char[] _mQuoteChars = null;


        public CCsvFileWriter()
        {
            _mColumnList = new List<CCsvColumn>();
            _mNrColumns = 0;
            _mNrValues = 0;

            _mQuoteChars = new char[32];
            for( int i = 1; i < 32; ++i )
            {
                _mQuoteChars[i] = (char)i;
            }
            _mQuoteChars[31] = ',';
        }

        public CCsvColumn mAddColumn(string ACodeName, bool AbActive, string AHeader, string AFormat = "")
        {
            CCsvColumn foundColumn = null;

            if (_mColumnList != null && ACodeName != null && ACodeName.Length > 0)
            {

                foreach (CCsvColumn column in _mColumnList)
                {
                    if (column.mbIsSameCode( ACodeName))
                    {
                        foundColumn = column;
                        break;
                    }
                }
                if (null == foundColumn)
                {
                    foundColumn = new CCsvColumn(ACodeName, AbActive, AHeader, AFormat);

                    _mColumnList.Add(foundColumn);
                    ++_mNrColumns;
                }
                else
                {
                    foundColumn.mSetColumn(AbActive, AHeader, AFormat);
                }
            }
            return foundColumn;
        }

        public void mAddColumn(CCsvColumn AColumn)
        {
            if (_mColumnList != null && AColumn._mCodeName != null && AColumn._mCodeName.Length > 0)
            {
                CCsvColumn foundColumn = null;

                foreach (CCsvColumn column in _mColumnList)
                {
                    if (column.mbIsSameCode( AColumn._mCodeName))
                    {
                        foundColumn = column;
                        break;
                    }
                }
                if (null != foundColumn)
                {
                    _mColumnList.Remove(foundColumn);
                    _mColumnList.Add(AColumn);
                    ++_mNrColumns;
                }
                else
                {
                    _mColumnList.Add(AColumn);
                    ++_mNrColumns;
                }
            }
        }
        public void mDisableAllColumns()
        {
            if (_mColumnList != null)
            {
                foreach (CCsvColumn column in _mColumnList)
                {
                    column._mbActive = false;
                }
            }
        }
        public UInt16 mGetNrActiveColumns()
        {
            UInt16 n = 0;

            if (_mColumnList != null)
            {
                foreach (CCsvColumn column in _mColumnList)
                {
                    if (column._mbActive)
                    {
                        ++n;
                    }
                }
            }
            return n;
        }
        public bool mbEnableColumn(string ACodeName, bool AbActive = true, string AHeader = null)
        {
            bool bFound = false;

            if (_mColumnList != null && ACodeName != null && ACodeName.Length > 0)
            {
                CCsvColumn foundColumn = null;
                int insertPos = 0;
                int i = 0;

                foreach (CCsvColumn column in _mColumnList)
                {
                    if (column.mbIsSameCode(ACodeName))
                    {
                        foundColumn = column;
                    }
                    ++i;
                    if ( column._mbActive)
                    {
                        insertPos = i;  // after last active
                    }
                }
                if (null != foundColumn)
                {
                    bFound = true;
                    foundColumn._mbActive = AbActive;
                    if( AHeader != null && AHeader.Length > 0)
                    {
                        foundColumn._mHeader = AHeader;
                    }
                    _mColumnList.Remove(foundColumn);
                    _mColumnList.Insert(insertPos, foundColumn);    // move column after last active 
                }
            }
            return bFound;
        }
        public void mAddMissingColumn(string ACodeName, bool AbActive, string AHeader, string AFormat = "")
        {
            if (_mColumnList != null && ACodeName != null && ACodeName.Length > 0)
            {
                CCsvColumn foundColumn = null;

                foreach (CCsvColumn column in _mColumnList)
                {
                    if (column.mbIsSameCode( ACodeName))
                    {
                        foundColumn = column;
                        break;
                    }
                }
                if (null == foundColumn)
                {
                    foundColumn = new CCsvColumn(ACodeName, AbActive, AHeader, AFormat);

                    _mColumnList.Add(foundColumn);
                    ++_mNrColumns;
                }
                else
                {
//   only add missing                 foundColumn.mSetColumn(AbActive, AHeader, AFormat);
                }
            }
        }

        public void mClearRow()
        {
            if (_mColumnList != null)
            {
                foreach (CCsvColumn column in _mColumnList)
                {
                    column.mClearValue();
                }
            }
            _mNrValues = 0;
        }

        private string mMakeCvsText(string AValueText, bool AbIsString)
        {
            string s = AValueText == null ? "" : AValueText;
            bool bHasQuotes = s.Length > 0 && s[0] == '\"';

            if (false == bHasQuotes)
            {
                if (_mbAddQuotesAll)
                {
                    s = "\"" + s + "\"";
                }
                else if (AbIsString)
                {
                    if (_mbAddQuotesText || s.IndexOfAny(_mQuoteChars) >= 0)
                    {
                        s = "\"" + s + "\"";
                    }
                }
            }
            return s;
        }

        private string mMakeCvsText(CCsvColumn AColumn)
        {
            string s = "";

            if( AColumn != null)
            {
                s = mMakeCvsText(AColumn.mGetTextValue(), AColumn.mbValueIsString());
            }
            else if (_mbAddQuotesAll)
            {
                s = "\"\"";
            }
            return s;
        }

        private CCsvColumn mFindColumn(string ACodeName)
        {
            CCsvColumn foundColumn = null;

            if (_mColumnList != null)
            {
                foreach (CCsvColumn column in _mColumnList)
                {
                    if (column.mbIsSameCode( ACodeName))
                    {
                        foundColumn = column;
                        break;
                    }
                }
            }
            return foundColumn;
        }

        public bool mbAddValueString(string ACodeName, string AValue)
        {
            bool bAdded = false;
            CCsvColumn foundColumn = mFindColumn(ACodeName);

            if (null != foundColumn )
            {
                bAdded = foundColumn.mbSetValueString(AValue);
            }
            return bAdded;
        }

        public bool mbAddValueUInt32(string ACodeName, UInt32 AValue)
        {
            bool bAdded = false;
            CCsvColumn foundColumn = mFindColumn(ACodeName);

            if (null != foundColumn && foundColumn._mbActive)
            {
                bAdded = foundColumn.mbSetValueUInt32(AValue);
            }
            return bAdded;
        }
        public bool mbAddValueInt32(string ACodeName, Int32 AValue)
        {
            bool bAdded = false;
            CCsvColumn foundColumn = mFindColumn(ACodeName);

            if (null != foundColumn && foundColumn._mbActive)
            {
                bAdded = foundColumn.mbSetValueInt32(AValue);
            }
            return bAdded;
        }
        public bool mbAddValueDouble(string ACodeName, double AValue)
        {
            bool bAdded = false;
            CCsvColumn foundColumn = mFindColumn(ACodeName);

            if (null != foundColumn && foundColumn._mbActive)
            {
                bAdded = foundColumn.mbSetValueDouble(AValue);
            }
            return bAdded;
        }
        public bool mbAddValueBool(string ACodeName, bool AbValue)
        {
            bool bAdded = false;
            CCsvColumn foundColumn = mFindColumn(ACodeName);

            if (null != foundColumn && foundColumn._mbActive)
            {
                bAdded = foundColumn.mbSetValueBool(AbValue);
            }
            return bAdded;
        }

        public bool mbWriteStart()
        {
            bool bOk = false;
            uint n = 0;

            _mTotalLines = "";
            _mHeaders = "";
            _mLastRow = "";
            _mNrRows = 0;
            mClearRow();

            if (_mColumnList != null)
            {
                foreach (CCsvColumn column in _mColumnList)
                {
                    if (column._mbActive)
                    {
                        mAddCsvValue(ref _mHeaders, column._mHeader);
                        ++n;
                    }
                }
                bOk = n > 0;
                if (bOk)
                {
                    _mTotalLines = _mHeaders + _mLineEnd;
                }
            }
            return bOk;
        }

        public bool mbWriteRow()
        {
            bool bOk = false;
            uint n = 0;

            _mLastRow = "";

            if (_mColumnList != null)
            {
                foreach (CCsvColumn column in _mColumnList)
                {
                    if (column._mbActive)
                    {
                        mAddCsvValue(ref _mLastRow, mMakeCvsText(column));
                        ++n;
                    }
                }
                bOk = n > 0;
                if (bOk)
                {
                    _mTotalLines += _mLastRow + _mLineEnd;
                }
                ++_mNrRows;
                mClearRow();
            }
            return bOk;
        }

        public void mAddCsvValue( ref string ArLine, string AText)
        {
            string s = ArLine;

            if( ArLine == null || ArLine.Length == 0)
            {
                s = "";
            }
            else
            {
                s += _mSeperator;
            }
            s += AText;
            ArLine = s;
        }

        public string mConvConrolToText(string AControl)
        {
            string s = "";

            if (AControl != null)
            {
                int n = AControl.Length;
                int i = 0;

                while (i < n)
                {
                    char c = AControl[i];

                    if (c == '\\')
                    {
                        if (++i < n)
                        {
                            c = AControl[i];

                            if (c == 't') s += '\t';
                            else if (c == 's') s += ' ';
                            else if (c == 'r') s += '\r';
                            else if (c == 'n') s += '\n';
                            else s += c;
                        }
                        else
                        {
                            s += c;
                        }
                    }
                    else
                    {
                        s += c;
                    }
                    ++i;
                }
            }
            return s;
        }
        public string mConvTextToConrol(string AControl)
        {
            string s = "";

            if (AControl != null)
            {
                int n = AControl.Length;
                int i = 0;

                while (i < n)
                {
                    char c = AControl[i];

                    if (c <= ' ')
                    {
                         if (c == '\t') s += "\\t";
                         else if (c == '\r') s += "\\r";
                         else if (c == '\n') s += "\\n";
                         else if (c == ' ') s += "\\s";
//                         else s += c;
                    }
                    else
                    {
                        s += c;
                    }
                    ++i;
                }
            }
            return s;
        }

        public bool mbMakeCsvConfig( out string ArConfigText, bool AbAll )
        {
            bool bOk = false;
            string configLines = "";

            if (_mColumnList != null && _mColumnList.Count > 0)
            {
                string lineEnd = "\r\n";

                configLines += "$writen= " + CProgram.sDateTimeToYMDHMS(DateTime.Now) + "@" + CProgram.sGetPcName() + lineEnd;
                configLines += "$LineEnd= " + mConvTextToConrol(_mLineEnd) + lineEnd;
                configLines += "$Seperator= " + mConvTextToConrol(_mSeperator) + lineEnd;
                configLines += "$TextQuotes= " + ( _mbAddQuotesText ? "true" : "false") + lineEnd;
                configLines += "$AllQuotes= " + ( _mbAddQuotesAll ? "true" : "false") + lineEnd;

                foreach (CCsvColumn column in _mColumnList)
                {
                    if (AbAll || column._mbActive)
                    {
                        configLines += column._mCodeName + "= " + column._mHeader + lineEnd;
                    }
                }

                bOk = true;
            }
            ArConfigText = configLines;
            return bOk;
        }
        public bool mbSaveCsvConfig(string AFullFileName, bool AbAll)
        {
            bool bOk = false;

            try
            {
                string s = "";

                if (mbMakeCsvConfig(out s, AbAll))
                {
                    File.WriteAllText(AFullFileName, s);
                    bOk = true;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed write Csv config file: " + AFullFileName, ex);
            }
            return bOk;
        }
        public bool mbSaveCsvLines(string AFullFileName)
        {
            bool bOk = false;

            try
            {
                if( _mTotalLines.Length > 0)
                { 
                    File.WriteAllText(AFullFileName, _mTotalLines);
                    bOk = true;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed write Csv file: " + AFullFileName, ex);
            }
            return bOk;
        }

        public bool mbParseCsvConfigLine( string ALowerParmName, string AParmValue)
        {
            bool bOk = false;

            bOk = mbEnableColumn(ALowerParmName, true, AParmValue);

            return bOk;
        }

        public bool mbReadCsvConfig( string AFullFileName)
        {
            bool bOk = false;

            try
            {
                string[] lines = File.ReadAllLines(AFullFileName);
                int nLines = lines == null ? 0 : lines.Length;

                if( nLines > 0)
                {
                    mDisableAllColumns();

                    for( int i = 0; i < nLines; ++i )
                    {
                        string line = lines[i];
                        int nChar = line == null ? 0 : line.Length;
                        int pos = line == null ? -1 : line.IndexOf('=');

                        if( pos > 0 )
                        {
                            string parmName = line.Substring(0, pos).Trim().ToLower();
                            string parmValue = pos < nChar-1 ? line.Substring(pos+1).Trim() : "";

                            if( parmName.Length > 0)
                            {
                                char firstChar = parmName[0];

                                if( firstChar == '$')
                                {
                                    // $parameter = value
                                    string lowerValue = parmValue.ToLower();

                                    if (lowerValue.Length > 0)
                                    {
                                        if (parmName == "$lineend")
                                        {
                                            _mLineEnd = mConvConrolToText(parmValue);
                                        }
                                        else if (parmName == "$seperator")
                                        {
                                            _mSeperator = mConvConrolToText(parmValue);
                                        }
                                        else if (parmName == "$textquotes")
                                        {
                                            _mbAddQuotesText = lowerValue == "true" || lowerValue == "t" || lowerValue == "1";
                                        }
                                        else if (parmName == "$allquotes")
                                        {
                                            _mbAddQuotesAll = lowerValue == "true" || lowerValue == "t" || lowerValue == "1";
                                        }
                                    }
                                }
                                else if( Char.IsLetter(firstChar))
                                {
                                    mbParseCsvConfigLine(parmName, parmValue);
                                }
                                else
                                {
                                    // ';' or other non letter means skip line
                                }
                            }
                        }
                    }
                    bOk = mGetNrActiveColumns() > 0;
                }
                if (_mTotalLines.Length > 0)
                {
                    File.WriteAllText(AFullFileName, _mTotalLines);
                    bOk = true;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed read Csv file: " + AFullFileName, ex);
            }

            return bOk;
        }

        public string mGetLineEnd() { return _mLineEnd; }
        public string mGetSeperator() { return _mSeperator; }
        public bool mbGetAddQuotesText() { return _mbAddQuotesText; }
        public bool mbGetAddQuotesAll() { return _mbAddQuotesAll; }
        //private bool _mbUseDot = false;
        //private bool _mbChangeDot = false;

        public string mGetTotalLines() { return _mTotalLines; }
        public string mGetHeaders() { return _mHeaders; }
        public string mGetLastRow() { return _mLastRow; }
        public UInt32 mGetNrColums() { return _mNrColumns; }
        public UInt32 mGetNrRows() { return _mNrRows; }
        public UInt32 mGetNrValues() { return _mNrValues; }

        public void mSetLineEnd( string ALineEnd) { _mLineEnd = ALineEnd; }
        public void mSetSeperator( string ASeperator ) { _mSeperator = ASeperator; }
        public void mSetAddQuotes(bool AbAddQuatesAll, bool AbAddQuatesText)
        {
            _mbAddQuotesAll = AbAddQuatesAll;
            _mbAddQuotesText = AbAddQuatesText;
        }
    }
}
