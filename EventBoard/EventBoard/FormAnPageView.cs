﻿using Program_Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventBoard
{
    public partial class FormAnPageView : Form
    {
        string _mTitle;
        DateTime _mStartDT;
        Int16 _mTimeZoneMin;
        float _mStripLengthSec;
        UInt16 _mNrChannels;
        UInt16 _mNrShowChannels = 0;
        UInt16 _mStripPartSec = 0;
        UInt16 _mFirstChannelNr = 9999;
        FormAnalyze _mFormAnalyze;

        public bool _mbIsOpen = false;

        public FormAnPageView(FormAnalyze AFormAnalyze, string ATitle, DateTime AStartDT, Int16 ATimeZoneMin, float AStripLengthSec, UInt16 ANrChannels)
        {
            InitializeComponent();

            _mFormAnalyze = AFormAnalyze; 
            _mTitle = ATitle;
            _mStartDT = AStartDT;
            _mTimeZoneMin = ATimeZoneMin;
            _mStripLengthSec = AStripLengthSec;
            _mNrChannels = ANrChannels;

            mUpdateTitle("Initializing...");

            string s = CProgram.sDateTimeToString(_mStartDT) + " " + CProgram.sTimeZoneOffsetStringLong(_mTimeZoneMin)
                + "  " + _mNrChannels + " channels"
                + "  " + CProgram.sPrintTimeSpan_dhmSec(_mStripLengthSec, "");

                labelFullStripInfo.Text = s;
            labelShowChannels.Text = "";
            labelEndTime.Text = CProgram.sDateTimeToString(_mStartDT.AddSeconds(_mStripLengthSec));


            _mbIsOpen = true;
        }

        public void mUpdateTitle(string AText)
        {
            string s = _mTitle;

            if (AText != null && AText.Length > 0)
            {
                s += ": " + AText;
            }
            this.Text = s;
        }
        public void mClearRows(UInt16 ANrShowChannels, UInt16 AStripPartSec)
        {
            try
            {
                if (dgScanPage != null && dgScanPage.Rows != null)
                {
                    dgScanPage.Rows.Clear();
                    _mNrShowChannels = ANrShowChannels;
                    _mStripPartSec = AStripPartSec;
                    _mFirstChannelNr = 9999;

                    if (ANrShowChannels > 0 && AStripPartSec > 0)   // only when active, could be called by close.
                    {
                        string s = "   Shown " + _mNrShowChannels + " channels a " + _mStripPartSec + " sec";

                        labelShowChannels.Text = s;
                    }
                }
                else
                {
                    labelShowChannels.Text = "!!!";
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("", ex);
            }
        }
        public void mAddRow( string AInfo, Image AImage, UInt16 AChannel, float AStartTimeSec)
        {
            if(dgScanPage != null && dgScanPage.Rows != null )
            {
                dgScanPage.Rows.Add(AInfo, AImage, AChannel, AStartTimeSec);
                if(AChannel < _mFirstChannelNr )
                {
                    _mFirstChannelNr = AChannel;
                }
            }
        }
        public int mGetViewImageWidth()
        {
            int width = 2000;

            if (dgScanPage != null && dgScanPage.Columns != null && dgScanPage.Columns.Count >= 2)
            {
                width = dgScanPage.Columns[1].Width;
            }
            return width;
        }
        public int mGetViewImageHeight()
        {
            int height = 100;

            if (dgScanPage != null && dgScanPage.Columns != null && dgScanPage.Columns.Count >= 2)
            {
                height = dgScanPage.RowTemplate.Height;
            }
            return height;
        }

        public void mSelectRow( UInt16 AChannelNr, float ASelectTimeSec)
        {
            if (dgScanPage != null && dgScanPage.Rows != null)
            {
                int selectedRow = -1;
                int nRows = dgScanPage.Rows.Count;

                try
                {
                    for (int iRow = 0; iRow < nRows; ++iRow)
                    {
                        DataGridViewRow row = dgScanPage.Rows[iRow];
                        if ( row != null && row.Cells != null && row.Cells.Count >= 4 && row.Cells[2].Value != null && row.Cells[3].Value != null)
                        {
                            int channel = 0;
                            int startSec = 0;

                            string s2 = row.Cells[2].Value.ToString();
                            string s3 = row.Cells[3].Value.ToString();

                            if (int.TryParse(s2, out channel) && int.TryParse(s3, out startSec))
                            {

                                if (ASelectTimeSec >= startSec && ASelectTimeSec < startSec + _mStripPartSec)
                                {
                                    if (_mNrShowChannels <= 1 || AChannelNr == channel)
                                    {
                                        selectedRow = iRow;
                                        dgScanPage.CurrentCell = row.Cells[0];
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                catch( Exception ex)
                {
                    CProgram.sLogException("mSelectRow(" + AChannelNr + ", " + ASelectTimeSec + ")", ex);
                }
            }
        }

            private void FormAnPageView_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (dgScanPage != null && dgScanPage.Rows != null)
                {
                    dgScanPage.Rows.Clear();
                }
                if (_mFormAnalyze != null)
                {
                    _mFormAnalyze.mClosePageScan();
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("", ex);
            }
        }

        private void dgScanPage_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgScanPage_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void dgScanPage_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void dgScanPage_MouseUp(object sender, MouseEventArgs e)
        {
            int x = e.X;
            int y = e.Y;

            try
            {
                DataGridView.HitTestInfo hitInfo = dgScanPage.HitTest(x, y);

                if (hitInfo != null && e.Button == MouseButtons.Right && CProgram.sbEnterProgUserArea(true))
                {
                    int nClicks = e.Clicks;
                    int c = hitInfo.ColumnIndex;
                    int r = hitInfo.RowIndex;
                    if (r >= 0 && c == 1 && r <= dgScanPage.Rows.Count)
                    {
                        DataGridViewRow row = dgScanPage.Rows[r];
                        if (row != null && row.Cells != null && row.Cells.Count >= 4 && row.Cells[2].Value != null && row.Cells[3].Value != null)
                        {
                            int channel = 0;
                            int startSec = 0;

                            string s2 = row.Cells[2].Value.ToString();
                            string s3 = row.Cells[3].Value.ToString();

                            if (int.TryParse(s2, out channel) && int.TryParse(s3, out startSec))
                            {

                                int w = row.Cells[1].DataGridView.Width;
                                int w2 = mGetViewImageWidth();

                                if (w > 0)
                                {
                                    int cX = x - hitInfo.ColumnX;
                                    int rY = y - hitInfo.RowY;

                                    if (_mFormAnalyze != null)
                                    {
                                        float centerTimeSec = startSec + _mStripPartSec * (float)(cX) / (float)w2;

                                        _mFormAnalyze.mDoMoveSelected((UInt16)channel, centerTimeSec);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("", ex);
            }

        }
        public int mGetHrImageWidth()
        {
            int width = 2000;

            if( pictureBoxHR != null )
            { 
                width = pictureBoxHR.Width;
            }
            return width;
        }
        public int mGetHrImageHeight()
        {
            int height = 100;

            if (pictureBoxHR != null)
            {
                height = pictureBoxHR.Height;
            }
            return height;
        }
        public void mSetHrImage( Image AImage)
        {
            if (pictureBoxHR != null)
            {
                pictureBoxHR.Image = AImage;
            }
        }

        public string mBurden2String(float ABurden)
        {
            string s = "-";

            if (ABurden >= 0)
            {
                int i = (int)(ABurden + 0.5F);

                s = i.ToString() + "%";
            }
            return s;

        }
        public string mHr2String(float AHr)
        {
            string s = "-";

            if (AHr >= 0)
            {
                int i = (int)(AHr + 0.5F);

                s = i.ToString();
            }
            return s;

        }
        public void mSetHrValues(float AHrMean, float ATotalBurden, int AHrLowBpm, float AHrLowBurden, int AHrHighBpm, float AHrHighBurden, float AMinHrBpm, float AMaxHrBpm)
        {
            string strMeanBpm = "";
            string strMeanBurden = "";
            string strLowBpm = "";
            string strLowBurden = "";
            string strHighBpm = "";
            string strHighBurden = "";
            string strMinHrBpm = "";
            string strAMaxHrBpm = "";

            if(AMinHrBpm <= AMaxHrBpm)
            {
                strMeanBpm = mHr2String(AHrMean) + " bpm";
                strMeanBurden = mBurden2String(ATotalBurden);
                strLowBpm = "< " + AHrLowBpm + " bpm";
                strLowBurden = mBurden2String( AHrLowBurden );
                strHighBpm = "> "+ AHrHighBpm + " bpm";
                strHighBurden = mBurden2String( AHrHighBurden );
                strMinHrBpm = mHr2String(AMinHrBpm ) + " min bpm";
                strAMaxHrBpm = mHr2String(AMaxHrBpm) + " max bpm";
            }
            labelMeanHR.Text = strMeanBpm;
            labelMeanBurden.Text = strMeanBurden;
            labelLowHr.Text = strLowBpm;
            labelLowBurden.Text = strLowBurden; ;
            labelHighHR.Text = strHighBpm;
            labelHighBurden.Text = strHighBurden;
            labelHrMin.Text = strMinHrBpm;
            labelHrMax.Text = strAMaxHrBpm;

        }

        private void pictureBoxHR_DoubleClick(object sender, EventArgs e)
        {
        }

        private void pictureBoxHR_MouseUp(object sender, MouseEventArgs e)
        {
            int x = e.X;
            int w = pictureBoxHR.Width;
            float t = (float)x / (float)w * _mStripLengthSec;
            
            mSelectRow(_mFirstChannelNr, t);
        }
    }
}
