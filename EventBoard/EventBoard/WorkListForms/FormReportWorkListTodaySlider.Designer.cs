﻿namespace EventBoard
{
    partial class FormReportWorkListTodaySlider
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel72 = new System.Windows.Forms.Panel();
            this.buttonViewPdf = new System.Windows.Forms.Button();
            this.buttonDeviceList = new System.Windows.Forms.Button();
            this.buttonAddUpdate = new System.Windows.Forms.Button();
            this.buttonPrintPreviousFindings = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBox6 = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.panel72.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel7.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel72
            // 
            this.panel72.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel72.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.panel72.Controls.Add(this.buttonViewPdf);
            this.panel72.Controls.Add(this.buttonDeviceList);
            this.panel72.Controls.Add(this.buttonAddUpdate);
            this.panel72.Controls.Add(this.buttonPrintPreviousFindings);
            this.panel72.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel72.Location = new System.Drawing.Point(0, 708);
            this.panel72.Name = "panel72";
            this.panel72.Size = new System.Drawing.Size(1331, 47);
            this.panel72.TabIndex = 9;
            // 
            // buttonViewPdf
            // 
            this.buttonViewPdf.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonViewPdf.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonViewPdf.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonViewPdf.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonViewPdf.FlatAppearance.BorderSize = 0;
            this.buttonViewPdf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonViewPdf.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonViewPdf.ForeColor = System.Drawing.Color.White;
            this.buttonViewPdf.Location = new System.Drawing.Point(167, 0);
            this.buttonViewPdf.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonViewPdf.Name = "buttonViewPdf";
            this.buttonViewPdf.Size = new System.Drawing.Size(155, 47);
            this.buttonViewPdf.TabIndex = 25;
            this.buttonViewPdf.Text = "Next day >>";
            this.buttonViewPdf.UseVisualStyleBackColor = false;
            // 
            // buttonDeviceList
            // 
            this.buttonDeviceList.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonDeviceList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonDeviceList.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonDeviceList.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonDeviceList.FlatAppearance.BorderSize = 0;
            this.buttonDeviceList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDeviceList.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonDeviceList.ForeColor = System.Drawing.Color.White;
            this.buttonDeviceList.Location = new System.Drawing.Point(0, 0);
            this.buttonDeviceList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonDeviceList.Name = "buttonDeviceList";
            this.buttonDeviceList.Size = new System.Drawing.Size(167, 47);
            this.buttonDeviceList.TabIndex = 17;
            this.buttonDeviceList.Text = "<< Previous day";
            this.buttonDeviceList.UseVisualStyleBackColor = false;
            // 
            // buttonAddUpdate
            // 
            this.buttonAddUpdate.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonAddUpdate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonAddUpdate.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonAddUpdate.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonAddUpdate.FlatAppearance.BorderSize = 0;
            this.buttonAddUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddUpdate.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonAddUpdate.ForeColor = System.Drawing.Color.White;
            this.buttonAddUpdate.Location = new System.Drawing.Point(1142, 0);
            this.buttonAddUpdate.Margin = new System.Windows.Forms.Padding(2);
            this.buttonAddUpdate.Name = "buttonAddUpdate";
            this.buttonAddUpdate.Size = new System.Drawing.Size(115, 47);
            this.buttonAddUpdate.TabIndex = 1;
            this.buttonAddUpdate.Text = "Print list";
            this.buttonAddUpdate.UseVisualStyleBackColor = false;
            // 
            // buttonPrintPreviousFindings
            // 
            this.buttonPrintPreviousFindings.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonPrintPreviousFindings.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonPrintPreviousFindings.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonPrintPreviousFindings.FlatAppearance.BorderColor = System.Drawing.Color.SlateBlue;
            this.buttonPrintPreviousFindings.FlatAppearance.BorderSize = 0;
            this.buttonPrintPreviousFindings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPrintPreviousFindings.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonPrintPreviousFindings.ForeColor = System.Drawing.Color.White;
            this.buttonPrintPreviousFindings.Location = new System.Drawing.Point(1257, 0);
            this.buttonPrintPreviousFindings.Margin = new System.Windows.Forms.Padding(0);
            this.buttonPrintPreviousFindings.Name = "buttonPrintPreviousFindings";
            this.buttonPrintPreviousFindings.Size = new System.Drawing.Size(74, 47);
            this.buttonPrintPreviousFindings.TabIndex = 1;
            this.buttonPrintPreviousFindings.Text = "Close";
            this.buttonPrintPreviousFindings.UseVisualStyleBackColor = false;
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitContainer1.Location = new System.Drawing.Point(0, 40);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel18);
            this.splitContainer1.Panel1.Controls.Add(this.panel17);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel2);
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.splitContainer1.Size = new System.Drawing.Size(1331, 656);
            this.splitContainer1.SplitterDistance = 376;
            this.splitContainer1.TabIndex = 10;
            // 
            // panel18
            // 
            this.panel18.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel18.Location = new System.Drawing.Point(0, 34);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(374, 10);
            this.panel18.TabIndex = 3;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.label8);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel17.Location = new System.Drawing.Point(0, 0);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(374, 34);
            this.panel17.TabIndex = 2;
            // 
            // label8
            // 
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(0, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(374, 34);
            this.label8.TabIndex = 0;
            this.label8.Text = "Daily and Weekly Reports";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 34);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(949, 10);
            this.panel2.TabIndex = 4;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(949, 34);
            this.panel1.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(949, 34);
            this.label1.TabIndex = 0;
            this.label1.Text = "End of Study Report";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.button4);
            this.panel7.Controls.Add(this.button2);
            this.panel7.Controls.Add(this.button3);
            this.panel7.Controls.Add(this.label7);
            this.panel7.Controls.Add(this.comboBox6);
            this.panel7.Controls.Add(this.label6);
            this.panel7.Controls.Add(this.comboBox5);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1331, 40);
            this.panel7.TabIndex = 11;
            // 
            // button3
            // 
            this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button3.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.button3.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.button3.Location = new System.Drawing.Point(412, 6);
            this.button3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(124, 26);
            this.button3.TabIndex = 55;
            this.button3.Text = "Filter is: OFF";
            this.button3.UseVisualStyleBackColor = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(197, 11);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 16);
            this.label7.TabIndex = 53;
            this.label7.Text = "Trial:";
            // 
            // comboBox6
            // 
            this.comboBox6.FormattingEnabled = true;
            this.comboBox6.Location = new System.Drawing.Point(256, 9);
            this.comboBox6.Name = "comboBox6";
            this.comboBox6.Size = new System.Drawing.Size(134, 21);
            this.comboBox6.TabIndex = 52;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(3, 11);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 16);
            this.label6.TabIndex = 51;
            this.label6.Text = "Client:";
            // 
            // comboBox5
            // 
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Location = new System.Drawing.Point(61, 9);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(130, 21);
            this.comboBox5.TabIndex = 50;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Transparent;
            this.button4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button4.Dock = System.Windows.Forms.DockStyle.Right;
            this.button4.FlatAppearance.BorderColor = System.Drawing.Color.SlateBlue;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.Black;
            this.button4.Location = new System.Drawing.Point(1139, 0);
            this.button4.Margin = new System.Windows.Forms.Padding(0);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(117, 38);
            this.button4.TabIndex = 64;
            this.button4.Text = "Refresh after:";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button4.UseVisualStyleBackColor = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Transparent;
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button2.Dock = System.Windows.Forms.DockStyle.Right;
            this.button2.FlatAppearance.BorderColor = System.Drawing.Color.SlateBlue;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.button2.ForeColor = System.Drawing.Color.Black;
            this.button2.Location = new System.Drawing.Point(1256, 0);
            this.button2.Margin = new System.Windows.Forms.Padding(0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(73, 38);
            this.button2.TabIndex = 63;
            this.button2.Text = "00:59";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // FormReportWorkListTodaySlider
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1331, 755);
            this.Controls.Add(this.panel72);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel7);
            this.Name = "FormReportWorkListTodaySlider";
            this.Text = "FormReportWorkListTodaySlider";
            this.panel72.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel72;
        private System.Windows.Forms.Button buttonViewPdf;
        private System.Windows.Forms.Button buttonDeviceList;
        private System.Windows.Forms.Button buttonAddUpdate;
        private System.Windows.Forms.Button buttonPrintPreviousFindings;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBox6;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button2;
    }
}