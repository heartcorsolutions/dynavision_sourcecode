﻿namespace EventBoard
{
    partial class CPrintMctForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CPrintMctForm));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint1 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 25D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint2 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 25D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint3 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 25D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint4 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 25D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint5 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(5D, 25D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint6 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(6D, 25D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint7 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(7D, 25D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint8 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(8D, 25D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint9 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(9D, 25D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint10 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(10D, 25D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint11 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(11D, 7D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint12 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(12D, 7D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint13 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(13D, 25D);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint14 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint15 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint16 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint17 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 0D);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint18 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint19 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint20 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint21 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 0D);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint22 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint23 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint24 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint25 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 0D);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint26 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint27 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint28 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint29 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 0D);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint30 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint31 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint32 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint33 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 0D);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea7 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series7 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint34 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint35 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint36 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint37 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 0D);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea8 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series8 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint38 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint39 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint40 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint41 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 0D);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea9 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series9 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint42 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint43 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint44 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint45 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 0D);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea10 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series10 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint46 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint47 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint48 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint49 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 0D);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea11 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series11 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint50 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint51 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint52 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint53 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 0D);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea12 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series12 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint54 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint55 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint56 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint57 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 0D);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea13 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series13 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint58 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint59 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint60 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint61 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 0D);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea14 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series14 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint62 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint63 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint64 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint65 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 0D);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea15 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.CustomLabel customLabel1 = new System.Windows.Forms.DataVisualization.Charting.CustomLabel();
            System.Windows.Forms.DataVisualization.Charting.Series series15 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series16 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series17 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint66 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(125D, 175D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint67 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(150D, 180D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint68 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(175D, 225D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint69 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(190D, 140D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint70 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(210D, 150D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint71 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(225D, 135D);
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripGenPages = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonPrint = new System.Windows.Forms.ToolStripButton();
            this.toolStripClipboard1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripClipboard2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripEnterData = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel51 = new System.Windows.Forms.Panel();
            this.label44 = new System.Windows.Forms.Label();
            this.labelPrintDate1Bottom = new System.Windows.Forms.Label();
            this.labelStudyNr = new System.Windows.Forms.Label();
            this.labelPage = new System.Windows.Forms.Label();
            this.labelPage1of = new System.Windows.Forms.Label();
            this.labelPageOf = new System.Windows.Forms.Label();
            this.labelPage1xOfX = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel126 = new System.Windows.Forms.Panel();
            this.labelNote1 = new System.Windows.Forms.Label();
            this.labelNote2 = new System.Windows.Forms.Label();
            this.panelSignatures = new System.Windows.Forms.Panel();
            this.panel110 = new System.Windows.Forms.Panel();
            this.labelReportSignDate = new System.Windows.Forms.Label();
            this.panel109 = new System.Windows.Forms.Panel();
            this.labelDateReportSign = new System.Windows.Forms.Label();
            this.panel108 = new System.Windows.Forms.Panel();
            this.labelPhysSignLine = new System.Windows.Forms.Label();
            this.panel107 = new System.Windows.Forms.Panel();
            this.labelPhysSign = new System.Windows.Forms.Label();
            this.panel106 = new System.Windows.Forms.Panel();
            this.labelPhysPrintName = new System.Windows.Forms.Label();
            this.panel105 = new System.Windows.Forms.Panel();
            this.labelPhysNameReportPrint = new System.Windows.Forms.Label();
            this.panel104 = new System.Windows.Forms.Panel();
            this.panelPrintArea1 = new System.Windows.Forms.Panel();
            this.panelEventsStat = new System.Windows.Forms.Panel();
            this.panelt1 = new System.Windows.Forms.Panel();
            this.panelStatInfo = new System.Windows.Forms.Panel();
            this.panelStatInfoGraph = new System.Windows.Forms.Panel();
            this.chartSummaryBar = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel88 = new System.Windows.Forms.Panel();
            this.panelPer = new System.Windows.Forms.Panel();
            this.labelPerMan = new System.Windows.Forms.Label();
            this.labelPerTotal = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.labelPerAbNormal = new System.Windows.Forms.Label();
            this.labelPerNormal = new System.Windows.Forms.Label();
            this.labelPerOther = new System.Windows.Forms.Label();
            this.labelPerVtach = new System.Windows.Forms.Label();
            this.labelPerPace = new System.Windows.Forms.Label();
            this.labelPerPVC = new System.Windows.Forms.Label();
            this.labelPerPAC = new System.Windows.Forms.Label();
            this.labelPerAflut = new System.Windows.Forms.Label();
            this.labelPerAfib = new System.Windows.Forms.Label();
            this.labelPerPause = new System.Windows.Forms.Label();
            this.labelPerBrady = new System.Windows.Forms.Label();
            this.labelPerTachy = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.panelDuration = new System.Windows.Forms.Panel();
            this.labelDurMan = new System.Windows.Forms.Label();
            this.labelDurTotal = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.labelDurAbNormal = new System.Windows.Forms.Label();
            this.labelDurNormal = new System.Windows.Forms.Label();
            this.labelDurOther = new System.Windows.Forms.Label();
            this.labelDurVtach = new System.Windows.Forms.Label();
            this.labelDurPace = new System.Windows.Forms.Label();
            this.labelDurPVC = new System.Windows.Forms.Label();
            this.labelDurPAC = new System.Windows.Forms.Label();
            this.labelDurAflut = new System.Windows.Forms.Label();
            this.labelDurAfib = new System.Windows.Forms.Label();
            this.labelDurPause = new System.Windows.Forms.Label();
            this.labelDurBrady = new System.Windows.Forms.Label();
            this.labelDurTachy = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.panel91 = new System.Windows.Forms.Panel();
            this.labelEpMan = new System.Windows.Forms.Label();
            this.labelEpTotal = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.labelEpAbNormal = new System.Windows.Forms.Label();
            this.labelEpNormal = new System.Windows.Forms.Label();
            this.labelEpOther = new System.Windows.Forms.Label();
            this.labelEpVtach = new System.Windows.Forms.Label();
            this.labelEpPace = new System.Windows.Forms.Label();
            this.labelEpPVC = new System.Windows.Forms.Label();
            this.labelEpPAC = new System.Windows.Forms.Label();
            this.labelEpAflut = new System.Windows.Forms.Label();
            this.labelEpAfib = new System.Windows.Forms.Label();
            this.labelEpPause = new System.Windows.Forms.Label();
            this.labelEpBrady = new System.Windows.Forms.Label();
            this.labelEpTachy = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.panel92 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.labelPace = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.panelStatHeader = new System.Windows.Forms.Panel();
            this.labelTechText = new System.Windows.Forms.Label();
            this.labelUser = new System.Windows.Forms.Label();
            this.panel85 = new System.Windows.Forms.Panel();
            this.label28 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.panel83 = new System.Windows.Forms.Panel();
            this.panel68 = new System.Windows.Forms.Panel();
            this.panel28 = new System.Windows.Forms.Panel();
            this.chartAll = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel33 = new System.Windows.Forms.Panel();
            this.labelGraphAll = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.panel72 = new System.Windows.Forms.Panel();
            this.chartManual = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel73 = new System.Windows.Forms.Panel();
            this.labelGraphManual = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.panel23 = new System.Windows.Forms.Panel();
            this.chartOther = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel26 = new System.Windows.Forms.Panel();
            this.labelGraphOther = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.panel37 = new System.Windows.Forms.Panel();
            this.chartAbN = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel38 = new System.Windows.Forms.Panel();
            this.labelGraphAbN = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.panel20 = new System.Windows.Forms.Panel();
            this.chartVtach = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel21 = new System.Windows.Forms.Panel();
            this.labelGraphVtach = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.panel69 = new System.Windows.Forms.Panel();
            this.chartPace = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel70 = new System.Windows.Forms.Panel();
            this.labelGraphPace = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.chartPVC = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel14 = new System.Windows.Forms.Panel();
            this.labelGraphPVC = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.chartPAC = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel9 = new System.Windows.Forms.Panel();
            this.labelGraphPAC = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel17 = new System.Windows.Forms.Panel();
            this.chartAflut = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel19 = new System.Windows.Forms.Panel();
            this.labelGraphAflut = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.panel74 = new System.Windows.Forms.Panel();
            this.chartAfib = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel75 = new System.Windows.Forms.Panel();
            this.labelGraphAfib = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.panel76 = new System.Windows.Forms.Panel();
            this.chartPause = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel77 = new System.Windows.Forms.Panel();
            this.labelGraphPause = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.panel78 = new System.Windows.Forms.Panel();
            this.chartBrady = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel79 = new System.Windows.Forms.Panel();
            this.labelGraphBrady = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.panel80 = new System.Windows.Forms.Panel();
            this.chartTachy = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel81 = new System.Windows.Forms.Panel();
            this.labelGraphTachy = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.panel82 = new System.Windows.Forms.Panel();
            this.panel64 = new System.Windows.Forms.Panel();
            this.panel62 = new System.Windows.Forms.Panel();
            this.chartHR = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel63 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.panel40 = new System.Windows.Forms.Panel();
            this.labelTotalBeats = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.labelDaysHours = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.labelTrendTo = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.labelFirstDate = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.panel101 = new System.Windows.Forms.Panel();
            this.panelDiagnosis = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.labelDiagnosis = new System.Windows.Forms.Label();
            this.panel16 = new System.Windows.Forms.Panel();
            this.label63 = new System.Windows.Forms.Label();
            this.panel158 = new System.Windows.Forms.Panel();
            this.panel67 = new System.Windows.Forms.Panel();
            this.panel258 = new System.Windows.Forms.Panel();
            this.labelClientName = new System.Windows.Forms.Label();
            this.labelClientTel = new System.Windows.Forms.Label();
            this.panel256 = new System.Windows.Forms.Panel();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.labelClientHeader = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel31 = new System.Windows.Forms.Panel();
            this.panelVert9 = new System.Windows.Forms.Panel();
            this.panel252 = new System.Windows.Forms.Panel();
            this.labelRefPhysicianName = new System.Windows.Forms.Label();
            this.labelRefPhysicianTel = new System.Windows.Forms.Label();
            this.panel250 = new System.Windows.Forms.Panel();
            this.label47 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.labelRefPhysHeader = new System.Windows.Forms.Label();
            this.panelVert8 = new System.Windows.Forms.Panel();
            this.panel244 = new System.Windows.Forms.Panel();
            this.labelPhysicianName = new System.Windows.Forms.Label();
            this.labelPhysicianTel = new System.Windows.Forms.Label();
            this.panel242 = new System.Windows.Forms.Panel();
            this.label51 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.labelPhysHeader = new System.Windows.Forms.Label();
            this.panelVert6 = new System.Windows.Forms.Panel();
            this.panel32 = new System.Windows.Forms.Panel();
            this.panel36 = new System.Windows.Forms.Panel();
            this.panel41 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.labelEndStudyDate = new System.Windows.Forms.Label();
            this.labelPatID = new System.Windows.Forms.Label();
            this.labelSerialNr = new System.Windows.Forms.Label();
            this.labelStartDate = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.labelEndStudyText = new System.Windows.Forms.Label();
            this.labelPatIDHeader = new System.Windows.Forms.Label();
            this.labelSerialNrHeader = new System.Windows.Forms.Label();
            this.labelStartDateHeader = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel24 = new System.Windows.Forms.Panel();
            this.panel34 = new System.Windows.Forms.Panel();
            this.labelPhone2 = new System.Windows.Forms.Label();
            this.labelPhone1 = new System.Windows.Forms.Label();
            this.labelCountry = new System.Windows.Forms.Label();
            this.labelZipCity = new System.Windows.Forms.Label();
            this.labelAddress = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.labelAddressHeader = new System.Windows.Forms.Label();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel27 = new System.Windows.Forms.Panel();
            this.labelPatientLastName = new System.Windows.Forms.Label();
            this.labelPatientFirstName = new System.Windows.Forms.Label();
            this.labelDateOfBirth = new System.Windows.Forms.Label();
            this.labelAgeGender = new System.Windows.Forms.Label();
            this.panel35 = new System.Windows.Forms.Panel();
            this.panel22 = new System.Windows.Forms.Panel();
            this.label41 = new System.Windows.Forms.Label();
            this.labelDOBheader = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.panel25 = new System.Windows.Forms.Panel();
            this.panel29 = new System.Windows.Forms.Panel();
            this.panel30 = new System.Windows.Forms.Panel();
            this.panelDateTimeofEventStrip = new System.Windows.Forms.Panel();
            this.labelSingleEventReportDate = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.panelCardiacEVentReportNumber = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.labelCardiacEventReportNr = new System.Windows.Forms.Label();
            this.labelReportNr = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panelPrintHeader = new System.Windows.Forms.Panel();
            this.panel56 = new System.Windows.Forms.Panel();
            this.panel57 = new System.Windows.Forms.Panel();
            this.panel58 = new System.Windows.Forms.Panel();
            this.labelCenter2 = new System.Windows.Forms.Label();
            this.panel59 = new System.Windows.Forms.Panel();
            this.labelCenter1 = new System.Windows.Forms.Label();
            this.panel60 = new System.Windows.Forms.Panel();
            this.labelPage1 = new System.Windows.Forms.Label();
            this.pictureBoxCenter = new System.Windows.Forms.PictureBox();
            this.label56 = new System.Windows.Forms.Label();
            this.toolStrip1.SuspendLayout();
            this.panel51.SuspendLayout();
            this.panel126.SuspendLayout();
            this.panelSignatures.SuspendLayout();
            this.panel110.SuspendLayout();
            this.panel109.SuspendLayout();
            this.panel108.SuspendLayout();
            this.panel107.SuspendLayout();
            this.panel106.SuspendLayout();
            this.panel105.SuspendLayout();
            this.panelPrintArea1.SuspendLayout();
            this.panelEventsStat.SuspendLayout();
            this.panelt1.SuspendLayout();
            this.panelStatInfo.SuspendLayout();
            this.panelStatInfoGraph.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartSummaryBar)).BeginInit();
            this.panel88.SuspendLayout();
            this.panelPer.SuspendLayout();
            this.panelDuration.SuspendLayout();
            this.panel91.SuspendLayout();
            this.panel92.SuspendLayout();
            this.panelStatHeader.SuspendLayout();
            this.panel85.SuspendLayout();
            this.panel68.SuspendLayout();
            this.panel28.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartAll)).BeginInit();
            this.panel33.SuspendLayout();
            this.panel72.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartManual)).BeginInit();
            this.panel73.SuspendLayout();
            this.panel23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartOther)).BeginInit();
            this.panel26.SuspendLayout();
            this.panel37.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartAbN)).BeginInit();
            this.panel38.SuspendLayout();
            this.panel20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartVtach)).BeginInit();
            this.panel21.SuspendLayout();
            this.panel69.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartPace)).BeginInit();
            this.panel70.SuspendLayout();
            this.panel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartPVC)).BeginInit();
            this.panel14.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartPAC)).BeginInit();
            this.panel9.SuspendLayout();
            this.panel17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartAflut)).BeginInit();
            this.panel19.SuspendLayout();
            this.panel74.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartAfib)).BeginInit();
            this.panel75.SuspendLayout();
            this.panel76.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartPause)).BeginInit();
            this.panel77.SuspendLayout();
            this.panel78.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartBrady)).BeginInit();
            this.panel79.SuspendLayout();
            this.panel80.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartTachy)).BeginInit();
            this.panel81.SuspendLayout();
            this.panel62.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartHR)).BeginInit();
            this.panel63.SuspendLayout();
            this.panel40.SuspendLayout();
            this.panelDiagnosis.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel67.SuspendLayout();
            this.panel258.SuspendLayout();
            this.panel256.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel252.SuspendLayout();
            this.panel250.SuspendLayout();
            this.panel244.SuspendLayout();
            this.panel242.SuspendLayout();
            this.panel32.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel34.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panelDateTimeofEventStrip.SuspendLayout();
            this.panelCardiacEVentReportNumber.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panelPrintHeader.SuspendLayout();
            this.panel56.SuspendLayout();
            this.panel58.SuspendLayout();
            this.panel59.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCenter)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripGenPages,
            this.toolStripButtonPrint,
            this.toolStripClipboard1,
            this.toolStripClipboard2,
            this.toolStripButton1,
            this.toolStripEnterData,
            this.toolStripButton2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(2376, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripGenPages
            // 
            this.toolStripGenPages.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripGenPages.Image = ((System.Drawing.Image)(resources.GetObject("toolStripGenPages.Image")));
            this.toolStripGenPages.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripGenPages.Name = "toolStripGenPages";
            this.toolStripGenPages.Size = new System.Drawing.Size(112, 22);
            this.toolStripGenPages.Text = "Generating Pages...";
            // 
            // toolStripButtonPrint
            // 
            this.toolStripButtonPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonPrint.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonPrint.Image")));
            this.toolStripButtonPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonPrint.Name = "toolStripButtonPrint";
            this.toolStripButtonPrint.Size = new System.Drawing.Size(36, 22);
            this.toolStripButtonPrint.Text = "Print";
            this.toolStripButtonPrint.Click += new System.EventHandler(this.toolStripButton1_Click_1);
            // 
            // toolStripClipboard1
            // 
            this.toolStripClipboard1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripClipboard1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripClipboard1.Image")));
            this.toolStripClipboard1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripClipboard1.Name = "toolStripClipboard1";
            this.toolStripClipboard1.Size = new System.Drawing.Size(72, 22);
            this.toolStripClipboard1.Text = "Clipboard 1";
            this.toolStripClipboard1.Click += new System.EventHandler(this.toolStripClipboard_Click);
            // 
            // toolStripClipboard2
            // 
            this.toolStripClipboard2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripClipboard2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripClipboard2.Image")));
            this.toolStripClipboard2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripClipboard2.Name = "toolStripClipboard2";
            this.toolStripClipboard2.Size = new System.Drawing.Size(72, 22);
            this.toolStripClipboard2.Text = "Clipboard 2";
            this.toolStripClipboard2.Click += new System.EventHandler(this.toolStripClipboard2_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(72, 22);
            this.toolStripButton1.Text = "Clipboard 3";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click_2);
            // 
            // toolStripEnterData
            // 
            this.toolStripEnterData.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripEnterData.Image = ((System.Drawing.Image)(resources.GetObject("toolStripEnterData.Image")));
            this.toolStripEnterData.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripEnterData.Name = "toolStripEnterData";
            this.toolStripEnterData.Size = new System.Drawing.Size(106, 22);
            this.toolStripEnterData.Text = "Enter Header Data";
            this.toolStripEnterData.Visible = false;
            this.toolStripEnterData.Click += new System.EventHandler(this.toolStripEnterData_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(25, 22);
            this.toolStripButton2.Text = "C4";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // panel51
            // 
            this.panel51.Controls.Add(this.label44);
            this.panel51.Controls.Add(this.labelPrintDate1Bottom);
            this.panel51.Controls.Add(this.labelStudyNr);
            this.panel51.Controls.Add(this.labelPage);
            this.panel51.Controls.Add(this.labelPage1of);
            this.panel51.Controls.Add(this.labelPageOf);
            this.panel51.Controls.Add(this.labelPage1xOfX);
            this.panel51.Controls.Add(this.label8);
            this.panel51.Controls.Add(this.label7);
            this.panel51.Controls.Add(this.label3);
            this.panel51.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel51.Location = new System.Drawing.Point(0, 3266);
            this.panel51.Name = "panel51";
            this.panel51.Size = new System.Drawing.Size(2338, 37);
            this.panel51.TabIndex = 34;
            // 
            // label44
            // 
            this.label44.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label44.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(945, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(727, 37);
            this.label44.TabIndex = 17;
            this.label44.Text = "© Copyright 2017 Techmedic International B.V.";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPrintDate1Bottom
            // 
            this.labelPrintDate1Bottom.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPrintDate1Bottom.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPrintDate1Bottom.Location = new System.Drawing.Point(225, 0);
            this.labelPrintDate1Bottom.Name = "labelPrintDate1Bottom";
            this.labelPrintDate1Bottom.Size = new System.Drawing.Size(720, 37);
            this.labelPrintDate1Bottom.TabIndex = 7;
            this.labelPrintDate1Bottom.Text = "^9/99/9999 99:99:99 AM+9999 v99999";
            this.labelPrintDate1Bottom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStudyNr
            // 
            this.labelStudyNr.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelStudyNr.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStudyNr.Location = new System.Drawing.Point(1672, 0);
            this.labelStudyNr.Name = "labelStudyNr";
            this.labelStudyNr.Size = new System.Drawing.Size(180, 37);
            this.labelStudyNr.TabIndex = 41;
            this.labelStudyNr.Text = "^9999999";
            this.labelStudyNr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPage
            // 
            this.labelPage.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelPage.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelPage.Location = new System.Drawing.Point(1852, 0);
            this.labelPage.Name = "labelPage";
            this.labelPage.Size = new System.Drawing.Size(104, 37);
            this.labelPage.TabIndex = 12;
            this.labelPage.Text = "Page";
            this.labelPage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPage1of
            // 
            this.labelPage1of.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelPage1of.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPage1of.Location = new System.Drawing.Point(1956, 0);
            this.labelPage1of.Name = "labelPage1of";
            this.labelPage1of.Size = new System.Drawing.Size(100, 37);
            this.labelPage1of.TabIndex = 11;
            this.labelPage1of.Text = "^99";
            this.labelPage1of.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPageOf
            // 
            this.labelPageOf.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelPageOf.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelPageOf.Location = new System.Drawing.Point(2056, 0);
            this.labelPageOf.Name = "labelPageOf";
            this.labelPageOf.Size = new System.Drawing.Size(70, 37);
            this.labelPageOf.TabIndex = 10;
            this.labelPageOf.Text = "of";
            this.labelPageOf.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPage1xOfX
            // 
            this.labelPage1xOfX.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelPage1xOfX.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPage1xOfX.Location = new System.Drawing.Point(2126, 0);
            this.labelPage1xOfX.Name = "labelPage1xOfX";
            this.labelPage1xOfX.Size = new System.Drawing.Size(166, 37);
            this.labelPage1xOfX.TabIndex = 9;
            this.labelPage1xOfX.Text = "^99p99";
            this.labelPage1xOfX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.Dock = System.Windows.Forms.DockStyle.Left;
            this.label8.Font = new System.Drawing.Font("Verdana", 20F);
            this.label8.Location = new System.Drawing.Point(45, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(180, 37);
            this.label8.TabIndex = 6;
            this.label8.Text = "Print date:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label7.Dock = System.Windows.Forms.DockStyle.Right;
            this.label7.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label7.Location = new System.Drawing.Point(2292, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 37);
            this.label7.TabIndex = 1;
            this.label7.Text = "R1";
            this.label7.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label7.Visible = false;
            // 
            // label3
            // 
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Dock = System.Windows.Forms.DockStyle.Left;
            this.label3.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 37);
            this.label3.TabIndex = 0;
            this.label3.Text = "L1";
            this.label3.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label3.Visible = false;
            // 
            // panel126
            // 
            this.panel126.Controls.Add(this.labelNote1);
            this.panel126.Controls.Add(this.labelNote2);
            this.panel126.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel126.Location = new System.Drawing.Point(0, 3231);
            this.panel126.Name = "panel126";
            this.panel126.Size = new System.Drawing.Size(2338, 35);
            this.panel126.TabIndex = 37;
            // 
            // labelNote1
            // 
            this.labelNote1.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelNote1.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNote1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelNote1.Location = new System.Drawing.Point(0, 0);
            this.labelNote1.Name = "labelNote1";
            this.labelNote1.Size = new System.Drawing.Size(1827, 35);
            this.labelNote1.TabIndex = 0;
            this.labelNote1.Text = "Note: the event data in the trend graphs and episode tables are a combination of " +
    "\\\"non validated automatic triggered events\\\" and validated events.";
            this.labelNote1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelNote2
            // 
            this.labelNote2.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelNote2.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNote2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelNote2.Location = new System.Drawing.Point(1844, 0);
            this.labelNote2.Name = "labelNote2";
            this.labelNote2.Size = new System.Drawing.Size(494, 35);
            this.labelNote2.TabIndex = 23;
            this.labelNote2.Text = "Be carefull with diagnostic purposes!";
            this.labelNote2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panelSignatures
            // 
            this.panelSignatures.Controls.Add(this.panel110);
            this.panelSignatures.Controls.Add(this.panel109);
            this.panelSignatures.Controls.Add(this.panel108);
            this.panelSignatures.Controls.Add(this.panel107);
            this.panelSignatures.Controls.Add(this.panel106);
            this.panelSignatures.Controls.Add(this.panel105);
            this.panelSignatures.Controls.Add(this.panel104);
            this.panelSignatures.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelSignatures.Location = new System.Drawing.Point(0, 3111);
            this.panelSignatures.Name = "panelSignatures";
            this.panelSignatures.Size = new System.Drawing.Size(2338, 120);
            this.panelSignatures.TabIndex = 33;
            // 
            // panel110
            // 
            this.panel110.Controls.Add(this.labelReportSignDate);
            this.panel110.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel110.Location = new System.Drawing.Point(1978, 0);
            this.panel110.Name = "panel110";
            this.panel110.Size = new System.Drawing.Size(325, 120);
            this.panel110.TabIndex = 7;
            // 
            // labelReportSignDate
            // 
            this.labelReportSignDate.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelReportSignDate.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelReportSignDate.ImageAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelReportSignDate.Location = new System.Drawing.Point(0, 0);
            this.labelReportSignDate.Name = "labelReportSignDate";
            this.labelReportSignDate.Size = new System.Drawing.Size(341, 120);
            this.labelReportSignDate.TabIndex = 2;
            this.labelReportSignDate.Text = "^99/99/2019";
            this.labelReportSignDate.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // panel109
            // 
            this.panel109.Controls.Add(this.labelDateReportSign);
            this.panel109.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel109.Location = new System.Drawing.Point(1833, 0);
            this.panel109.Name = "panel109";
            this.panel109.Size = new System.Drawing.Size(145, 120);
            this.panel109.TabIndex = 6;
            // 
            // labelDateReportSign
            // 
            this.labelDateReportSign.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelDateReportSign.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDateReportSign.ImageAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelDateReportSign.Location = new System.Drawing.Point(0, 0);
            this.labelDateReportSign.Name = "labelDateReportSign";
            this.labelDateReportSign.Size = new System.Drawing.Size(150, 120);
            this.labelDateReportSign.TabIndex = 1;
            this.labelDateReportSign.Text = "Date:";
            this.labelDateReportSign.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // panel108
            // 
            this.panel108.Controls.Add(this.labelPhysSignLine);
            this.panel108.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel108.Location = new System.Drawing.Point(1373, 0);
            this.panel108.Name = "panel108";
            this.panel108.Size = new System.Drawing.Size(460, 120);
            this.panel108.TabIndex = 5;
            // 
            // labelPhysSignLine
            // 
            this.labelPhysSignLine.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPhysSignLine.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPhysSignLine.ImageAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelPhysSignLine.Location = new System.Drawing.Point(0, 0);
            this.labelPhysSignLine.Name = "labelPhysSignLine";
            this.labelPhysSignLine.Size = new System.Drawing.Size(460, 120);
            this.labelPhysSignLine.TabIndex = 1;
            this.labelPhysSignLine.Text = "^name\r\n--xxx---";
            this.labelPhysSignLine.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.labelPhysSignLine.Click += new System.EventHandler(this.labelPhysSignLine_Click);
            // 
            // panel107
            // 
            this.panel107.Controls.Add(this.labelPhysSign);
            this.panel107.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel107.Location = new System.Drawing.Point(882, 0);
            this.panel107.Name = "panel107";
            this.panel107.Size = new System.Drawing.Size(491, 120);
            this.panel107.TabIndex = 4;
            // 
            // labelPhysSign
            // 
            this.labelPhysSign.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPhysSign.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPhysSign.ImageAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelPhysSign.Location = new System.Drawing.Point(0, 0);
            this.labelPhysSign.Name = "labelPhysSign";
            this.labelPhysSign.Size = new System.Drawing.Size(491, 120);
            this.labelPhysSign.TabIndex = 1;
            this.labelPhysSign.Text = "^QC\r\nPhysician signature:";
            this.labelPhysSign.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // panel106
            // 
            this.panel106.Controls.Add(this.labelPhysPrintName);
            this.panel106.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel106.Location = new System.Drawing.Point(400, 0);
            this.panel106.Name = "panel106";
            this.panel106.Size = new System.Drawing.Size(482, 120);
            this.panel106.TabIndex = 3;
            // 
            // labelPhysPrintName
            // 
            this.labelPhysPrintName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPhysPrintName.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPhysPrintName.ImageAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelPhysPrintName.Location = new System.Drawing.Point(0, 0);
            this.labelPhysPrintName.Name = "labelPhysPrintName";
            this.labelPhysPrintName.Size = new System.Drawing.Size(482, 120);
            this.labelPhysPrintName.TabIndex = 1;
            this.labelPhysPrintName.Text = "^Phys Last Name\r\nfunction";
            this.labelPhysPrintName.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.labelPhysPrintName.Click += new System.EventHandler(this.labelPhysPrintName_Click);
            // 
            // panel105
            // 
            this.panel105.Controls.Add(this.labelPhysNameReportPrint);
            this.panel105.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel105.Location = new System.Drawing.Point(0, 0);
            this.panel105.Name = "panel105";
            this.panel105.Size = new System.Drawing.Size(400, 120);
            this.panel105.TabIndex = 2;
            // 
            // labelPhysNameReportPrint
            // 
            this.labelPhysNameReportPrint.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPhysNameReportPrint.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPhysNameReportPrint.ImageAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelPhysNameReportPrint.Location = new System.Drawing.Point(0, 0);
            this.labelPhysNameReportPrint.Name = "labelPhysNameReportPrint";
            this.labelPhysNameReportPrint.Size = new System.Drawing.Size(400, 120);
            this.labelPhysNameReportPrint.TabIndex = 2;
            this.labelPhysNameReportPrint.Text = "^QC\r\nPhysician name:";
            this.labelPhysNameReportPrint.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // panel104
            // 
            this.panel104.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel104.Location = new System.Drawing.Point(2316, 0);
            this.panel104.Name = "panel104";
            this.panel104.Size = new System.Drawing.Size(22, 120);
            this.panel104.TabIndex = 1;
            // 
            // panelPrintArea1
            // 
            this.panelPrintArea1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panelPrintArea1.Controls.Add(this.panelEventsStat);
            this.panelPrintArea1.Controls.Add(this.panel101);
            this.panelPrintArea1.Controls.Add(this.panelDiagnosis);
            this.panelPrintArea1.Controls.Add(this.panel158);
            this.panelPrintArea1.Controls.Add(this.panel67);
            this.panelPrintArea1.Controls.Add(this.panel7);
            this.panelPrintArea1.Controls.Add(this.panelDateTimeofEventStrip);
            this.panelPrintArea1.Controls.Add(this.panelCardiacEVentReportNumber);
            this.panelPrintArea1.Controls.Add(this.panelPrintHeader);
            this.panelPrintArea1.Controls.Add(this.panelSignatures);
            this.panelPrintArea1.Controls.Add(this.panel126);
            this.panelPrintArea1.Controls.Add(this.panel51);
            this.panelPrintArea1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.panelPrintArea1.Location = new System.Drawing.Point(0, 100);
            this.panelPrintArea1.Name = "panelPrintArea1";
            this.panelPrintArea1.Size = new System.Drawing.Size(2338, 3303);
            this.panelPrintArea1.TabIndex = 0;
            this.panelPrintArea1.Paint += new System.Windows.Forms.PaintEventHandler(this.panelPrintArea1_Paint);
            // 
            // panelEventsStat
            // 
            this.panelEventsStat.Controls.Add(this.panelt1);
            this.panelEventsStat.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelEventsStat.Location = new System.Drawing.Point(0, 910);
            this.panelEventsStat.Name = "panelEventsStat";
            this.panelEventsStat.Size = new System.Drawing.Size(2338, 2222);
            this.panelEventsStat.TabIndex = 13;
            // 
            // panelt1
            // 
            this.panelt1.Controls.Add(this.panelStatInfo);
            this.panelt1.Controls.Add(this.panelStatHeader);
            this.panelt1.Controls.Add(this.panel83);
            this.panelt1.Controls.Add(this.panel68);
            this.panelt1.Controls.Add(this.panel64);
            this.panelt1.Controls.Add(this.panel62);
            this.panelt1.Controls.Add(this.panel40);
            this.panelt1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelt1.Location = new System.Drawing.Point(0, 0);
            this.panelt1.Name = "panelt1";
            this.panelt1.Size = new System.Drawing.Size(2338, 2222);
            this.panelt1.TabIndex = 5;
            // 
            // panelStatInfo
            // 
            this.panelStatInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelStatInfo.Controls.Add(this.panelStatInfoGraph);
            this.panelStatInfo.Controls.Add(this.panel88);
            this.panelStatInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelStatInfo.Location = new System.Drawing.Point(0, 1489);
            this.panelStatInfo.Name = "panelStatInfo";
            this.panelStatInfo.Size = new System.Drawing.Size(2338, 709);
            this.panelStatInfo.TabIndex = 17;
            // 
            // panelStatInfoGraph
            // 
            this.panelStatInfoGraph.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelStatInfoGraph.Controls.Add(this.chartSummaryBar);
            this.panelStatInfoGraph.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelStatInfoGraph.Location = new System.Drawing.Point(791, 0);
            this.panelStatInfoGraph.Name = "panelStatInfoGraph";
            this.panelStatInfoGraph.Size = new System.Drawing.Size(1545, 707);
            this.panelStatInfoGraph.TabIndex = 1;
            // 
            // chartSummaryBar
            // 
            chartArea1.AxisX.IsLabelAutoFit = false;
            chartArea1.AxisX.LabelStyle.Enabled = false;
            chartArea1.AxisX.LabelStyle.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea1.AxisX.LineColor = System.Drawing.Color.DarkGray;
            chartArea1.AxisX.MajorGrid.Enabled = false;
            chartArea1.AxisX2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea1.AxisY.IsLabelAutoFit = false;
            chartArea1.AxisY.LabelStyle.Font = new System.Drawing.Font("Verdana", 27F);
            chartArea1.AxisY.LabelStyle.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea1.AxisY.LineColor = System.Drawing.Color.DarkGray;
            chartArea1.AxisY.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea1.AxisY.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea1.AxisY2.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea1.BorderWidth = 0;
            chartArea1.Name = "ChartArea1";
            this.chartSummaryBar.ChartAreas.Add(chartArea1);
            this.chartSummaryBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartSummaryBar.Location = new System.Drawing.Point(0, 0);
            this.chartSummaryBar.Name = "chartSummaryBar";
            series1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            series1.BorderWidth = 2;
            series1.ChartArea = "ChartArea1";
            series1.Color = System.Drawing.Color.Transparent;
            series1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series1.IsVisibleInLegend = false;
            series1.Name = "Series1";
            dataPoint1.BorderWidth = 3;
            dataPoint1.Color = System.Drawing.Color.WhiteSmoke;
            dataPoint1.CustomProperties = "DrawingStyle=Cylinder";
            dataPoint1.Font = new System.Drawing.Font("Verdana", 22F);
            dataPoint1.IsValueShownAsLabel = false;
            dataPoint1.IsVisibleInLegend = false;
            dataPoint1.Label = "Tachy";
            dataPoint1.LabelAngle = 0;
            dataPoint1.LabelForeColor = System.Drawing.Color.Black;
            dataPoint2.BorderWidth = 3;
            dataPoint2.Color = System.Drawing.Color.Gainsboro;
            dataPoint2.CustomProperties = "DrawingStyle=Cylinder";
            dataPoint2.Font = new System.Drawing.Font("Verdana", 22F);
            dataPoint2.Label = "Brady";
            dataPoint3.BorderWidth = 3;
            dataPoint3.Color = System.Drawing.Color.LightGray;
            dataPoint3.CustomProperties = "DrawingStyle=Cylinder";
            dataPoint3.Font = new System.Drawing.Font("Verdana", 22F);
            dataPoint3.Label = "Pause";
            dataPoint4.BorderWidth = 3;
            dataPoint4.Color = System.Drawing.Color.Silver;
            dataPoint4.CustomProperties = "DrawingStyle=Cylinder";
            dataPoint4.Font = new System.Drawing.Font("Verdana", 22F);
            dataPoint4.Label = "AFib";
            dataPoint5.BorderWidth = 3;
            dataPoint5.Color = System.Drawing.Color.DarkGray;
            dataPoint5.CustomProperties = "DrawingStyle=Cylinder";
            dataPoint5.Font = new System.Drawing.Font("Verdana", 22F);
            dataPoint5.Label = "AFlut";
            dataPoint6.BorderWidth = 3;
            dataPoint6.Color = System.Drawing.Color.Gray;
            dataPoint6.CustomProperties = "DrawingStyle=Cylinder";
            dataPoint6.Font = new System.Drawing.Font("Verdana", 22F);
            dataPoint6.Label = "PAC\\nSVEB";
            dataPoint7.BorderWidth = 3;
            dataPoint7.Color = System.Drawing.Color.DimGray;
            dataPoint7.CustomProperties = "DrawingStyle=Cylinder";
            dataPoint7.Font = new System.Drawing.Font("Verdana", 22F);
            dataPoint7.Label = "PVC\\nVEB";
            dataPoint8.BorderWidth = 3;
            dataPoint8.Color = System.Drawing.Color.LightGray;
            dataPoint8.CustomProperties = "DrawingStyle=Cylinder";
            dataPoint8.Font = new System.Drawing.Font("Verdana", 22F);
            dataPoint8.Label = "Pace";
            dataPoint9.BorderWidth = 3;
            dataPoint9.Color = System.Drawing.Color.Gainsboro;
            dataPoint9.CustomProperties = "DrawingStyle=Cylinder";
            dataPoint9.Font = new System.Drawing.Font("Verdana", 22F);
            dataPoint9.Label = "VTach";
            dataPoint10.BorderWidth = 3;
            dataPoint10.Color = System.Drawing.Color.Gainsboro;
            dataPoint10.CustomProperties = "DrawingStyle=Cylinder";
            dataPoint10.Font = new System.Drawing.Font("Verdana", 22F);
            dataPoint10.Label = "Other";
            dataPoint11.Color = System.Drawing.Color.White;
            dataPoint11.Font = new System.Drawing.Font("Verdana", 22F);
            dataPoint11.Label = "Norm";
            dataPoint12.Color = System.Drawing.Color.LightGray;
            dataPoint12.Font = new System.Drawing.Font("Verdana", 22F);
            dataPoint12.Label = "AbN";
            dataPoint13.BorderWidth = 3;
            dataPoint13.Color = System.Drawing.Color.Black;
            dataPoint13.CustomProperties = "DrawingStyle=Cylinder";
            dataPoint13.Font = new System.Drawing.Font("Verdana", 22F);
            dataPoint13.Label = "Manual";
            series1.Points.Add(dataPoint1);
            series1.Points.Add(dataPoint2);
            series1.Points.Add(dataPoint3);
            series1.Points.Add(dataPoint4);
            series1.Points.Add(dataPoint5);
            series1.Points.Add(dataPoint6);
            series1.Points.Add(dataPoint7);
            series1.Points.Add(dataPoint8);
            series1.Points.Add(dataPoint9);
            series1.Points.Add(dataPoint10);
            series1.Points.Add(dataPoint11);
            series1.Points.Add(dataPoint12);
            series1.Points.Add(dataPoint13);
            series1.SmartLabelStyle.MovingDirection = System.Windows.Forms.DataVisualization.Charting.LabelAlignmentStyles.Top;
            this.chartSummaryBar.Series.Add(series1);
            this.chartSummaryBar.Size = new System.Drawing.Size(1543, 705);
            this.chartSummaryBar.TabIndex = 0;
            this.chartSummaryBar.Text = "EventPie";
            this.chartSummaryBar.Click += new System.EventHandler(this.chartSummaryBar_Click);
            // 
            // panel88
            // 
            this.panel88.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel88.Controls.Add(this.panelPer);
            this.panel88.Controls.Add(this.panelDuration);
            this.panel88.Controls.Add(this.panel91);
            this.panel88.Controls.Add(this.panel92);
            this.panel88.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel88.Location = new System.Drawing.Point(0, 0);
            this.panel88.Name = "panel88";
            this.panel88.Size = new System.Drawing.Size(791, 707);
            this.panel88.TabIndex = 0;
            // 
            // panelPer
            // 
            this.panelPer.Controls.Add(this.labelPerMan);
            this.panelPer.Controls.Add(this.labelPerTotal);
            this.panelPer.Controls.Add(this.label61);
            this.panelPer.Controls.Add(this.labelPerAbNormal);
            this.panelPer.Controls.Add(this.labelPerNormal);
            this.panelPer.Controls.Add(this.labelPerOther);
            this.panelPer.Controls.Add(this.labelPerVtach);
            this.panelPer.Controls.Add(this.labelPerPace);
            this.panelPer.Controls.Add(this.labelPerPVC);
            this.panelPer.Controls.Add(this.labelPerPAC);
            this.panelPer.Controls.Add(this.labelPerAflut);
            this.panelPer.Controls.Add(this.labelPerAfib);
            this.panelPer.Controls.Add(this.labelPerPause);
            this.panelPer.Controls.Add(this.labelPerBrady);
            this.panelPer.Controls.Add(this.labelPerTachy);
            this.panelPer.Controls.Add(this.label27);
            this.panelPer.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelPer.Location = new System.Drawing.Point(647, 0);
            this.panelPer.Name = "panelPer";
            this.panelPer.Size = new System.Drawing.Size(138, 705);
            this.panelPer.TabIndex = 3;
            // 
            // labelPerMan
            // 
            this.labelPerMan.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerMan.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPerMan.Location = new System.Drawing.Point(0, 660);
            this.labelPerMan.Name = "labelPerMan";
            this.labelPerMan.Size = new System.Drawing.Size(138, 46);
            this.labelPerMan.TabIndex = 27;
            this.labelPerMan.Text = "^9.99";
            this.labelPerMan.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPerTotal
            // 
            this.labelPerTotal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerTotal.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPerTotal.Location = new System.Drawing.Point(0, 614);
            this.labelPerTotal.Name = "labelPerTotal";
            this.labelPerTotal.Size = new System.Drawing.Size(138, 46);
            this.labelPerTotal.TabIndex = 28;
            this.labelPerTotal.Text = "^9.99";
            this.labelPerTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label61
            // 
            this.label61.Dock = System.Windows.Forms.DockStyle.Top;
            this.label61.Font = new System.Drawing.Font("Verdana", 27.75F);
            this.label61.Location = new System.Drawing.Point(0, 600);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(138, 14);
            this.label61.TabIndex = 26;
            // 
            // labelPerAbNormal
            // 
            this.labelPerAbNormal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerAbNormal.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPerAbNormal.Location = new System.Drawing.Point(0, 554);
            this.labelPerAbNormal.Name = "labelPerAbNormal";
            this.labelPerAbNormal.Size = new System.Drawing.Size(138, 46);
            this.labelPerAbNormal.TabIndex = 35;
            this.labelPerAbNormal.Text = "^9.99";
            this.labelPerAbNormal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPerNormal
            // 
            this.labelPerNormal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerNormal.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPerNormal.Location = new System.Drawing.Point(0, 508);
            this.labelPerNormal.Name = "labelPerNormal";
            this.labelPerNormal.Size = new System.Drawing.Size(138, 46);
            this.labelPerNormal.TabIndex = 34;
            this.labelPerNormal.Text = "^9.99";
            this.labelPerNormal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPerOther
            // 
            this.labelPerOther.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerOther.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPerOther.Location = new System.Drawing.Point(0, 462);
            this.labelPerOther.Name = "labelPerOther";
            this.labelPerOther.Size = new System.Drawing.Size(138, 46);
            this.labelPerOther.TabIndex = 32;
            this.labelPerOther.Text = "^9.99";
            this.labelPerOther.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPerVtach
            // 
            this.labelPerVtach.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerVtach.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPerVtach.Location = new System.Drawing.Point(0, 416);
            this.labelPerVtach.Name = "labelPerVtach";
            this.labelPerVtach.Size = new System.Drawing.Size(138, 46);
            this.labelPerVtach.TabIndex = 31;
            this.labelPerVtach.Text = "^9.99";
            this.labelPerVtach.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPerPace
            // 
            this.labelPerPace.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerPace.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPerPace.Location = new System.Drawing.Point(0, 370);
            this.labelPerPace.Name = "labelPerPace";
            this.labelPerPace.Size = new System.Drawing.Size(138, 46);
            this.labelPerPace.TabIndex = 33;
            this.labelPerPace.Text = "^9.99";
            this.labelPerPace.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPerPVC
            // 
            this.labelPerPVC.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerPVC.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPerPVC.Location = new System.Drawing.Point(0, 324);
            this.labelPerPVC.Name = "labelPerPVC";
            this.labelPerPVC.Size = new System.Drawing.Size(138, 46);
            this.labelPerPVC.TabIndex = 12;
            this.labelPerPVC.Text = "^9.99";
            this.labelPerPVC.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPerPAC
            // 
            this.labelPerPAC.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerPAC.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPerPAC.Location = new System.Drawing.Point(0, 278);
            this.labelPerPAC.Name = "labelPerPAC";
            this.labelPerPAC.Size = new System.Drawing.Size(138, 46);
            this.labelPerPAC.TabIndex = 11;
            this.labelPerPAC.Text = "^9.99";
            this.labelPerPAC.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPerAflut
            // 
            this.labelPerAflut.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerAflut.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPerAflut.Location = new System.Drawing.Point(0, 232);
            this.labelPerAflut.Name = "labelPerAflut";
            this.labelPerAflut.Size = new System.Drawing.Size(138, 46);
            this.labelPerAflut.TabIndex = 29;
            this.labelPerAflut.Text = "^9.99";
            this.labelPerAflut.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPerAfib
            // 
            this.labelPerAfib.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerAfib.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPerAfib.Location = new System.Drawing.Point(0, 186);
            this.labelPerAfib.Name = "labelPerAfib";
            this.labelPerAfib.Size = new System.Drawing.Size(138, 46);
            this.labelPerAfib.TabIndex = 4;
            this.labelPerAfib.Text = "^9.99";
            this.labelPerAfib.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPerPause
            // 
            this.labelPerPause.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerPause.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPerPause.Location = new System.Drawing.Point(0, 140);
            this.labelPerPause.Name = "labelPerPause";
            this.labelPerPause.Size = new System.Drawing.Size(138, 46);
            this.labelPerPause.TabIndex = 3;
            this.labelPerPause.Text = "^9.99";
            this.labelPerPause.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPerBrady
            // 
            this.labelPerBrady.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerBrady.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPerBrady.Location = new System.Drawing.Point(0, 94);
            this.labelPerBrady.Name = "labelPerBrady";
            this.labelPerBrady.Size = new System.Drawing.Size(138, 46);
            this.labelPerBrady.TabIndex = 2;
            this.labelPerBrady.Text = "^9.99";
            this.labelPerBrady.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPerTachy
            // 
            this.labelPerTachy.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerTachy.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPerTachy.Location = new System.Drawing.Point(0, 48);
            this.labelPerTachy.Name = "labelPerTachy";
            this.labelPerTachy.Size = new System.Drawing.Size(138, 46);
            this.labelPerTachy.TabIndex = 1;
            this.labelPerTachy.Text = "^9.99";
            this.labelPerTachy.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label27
            // 
            this.label27.Dock = System.Windows.Forms.DockStyle.Top;
            this.label27.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(0, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(138, 48);
            this.label27.TabIndex = 0;
            this.label27.Text = "%";
            this.label27.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panelDuration
            // 
            this.panelDuration.Controls.Add(this.labelDurMan);
            this.panelDuration.Controls.Add(this.labelDurTotal);
            this.panelDuration.Controls.Add(this.label60);
            this.panelDuration.Controls.Add(this.labelDurAbNormal);
            this.panelDuration.Controls.Add(this.labelDurNormal);
            this.panelDuration.Controls.Add(this.labelDurOther);
            this.panelDuration.Controls.Add(this.labelDurVtach);
            this.panelDuration.Controls.Add(this.labelDurPace);
            this.panelDuration.Controls.Add(this.labelDurPVC);
            this.panelDuration.Controls.Add(this.labelDurPAC);
            this.panelDuration.Controls.Add(this.labelDurAflut);
            this.panelDuration.Controls.Add(this.labelDurAfib);
            this.panelDuration.Controls.Add(this.labelDurPause);
            this.panelDuration.Controls.Add(this.labelDurBrady);
            this.panelDuration.Controls.Add(this.labelDurTachy);
            this.panelDuration.Controls.Add(this.label22);
            this.panelDuration.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelDuration.Location = new System.Drawing.Point(417, 0);
            this.panelDuration.Name = "panelDuration";
            this.panelDuration.Size = new System.Drawing.Size(230, 705);
            this.panelDuration.TabIndex = 2;
            // 
            // labelDurMan
            // 
            this.labelDurMan.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurMan.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDurMan.Location = new System.Drawing.Point(0, 660);
            this.labelDurMan.Name = "labelDurMan";
            this.labelDurMan.Size = new System.Drawing.Size(230, 46);
            this.labelDurMan.TabIndex = 27;
            this.labelDurMan.Text = "^90:00:00";
            this.labelDurMan.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDurTotal
            // 
            this.labelDurTotal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurTotal.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDurTotal.Location = new System.Drawing.Point(0, 614);
            this.labelDurTotal.Name = "labelDurTotal";
            this.labelDurTotal.Size = new System.Drawing.Size(230, 46);
            this.labelDurTotal.TabIndex = 28;
            this.labelDurTotal.Text = "^90:00:00";
            this.labelDurTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label60
            // 
            this.label60.Dock = System.Windows.Forms.DockStyle.Top;
            this.label60.Font = new System.Drawing.Font("Verdana", 27.75F);
            this.label60.Location = new System.Drawing.Point(0, 600);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(230, 14);
            this.label60.TabIndex = 26;
            // 
            // labelDurAbNormal
            // 
            this.labelDurAbNormal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurAbNormal.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDurAbNormal.Location = new System.Drawing.Point(0, 554);
            this.labelDurAbNormal.Name = "labelDurAbNormal";
            this.labelDurAbNormal.Size = new System.Drawing.Size(230, 46);
            this.labelDurAbNormal.TabIndex = 35;
            this.labelDurAbNormal.Text = "^9:99:99";
            this.labelDurAbNormal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDurNormal
            // 
            this.labelDurNormal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurNormal.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDurNormal.Location = new System.Drawing.Point(0, 508);
            this.labelDurNormal.Name = "labelDurNormal";
            this.labelDurNormal.Size = new System.Drawing.Size(230, 46);
            this.labelDurNormal.TabIndex = 34;
            this.labelDurNormal.Text = "^9:99:99";
            this.labelDurNormal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDurOther
            // 
            this.labelDurOther.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurOther.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDurOther.Location = new System.Drawing.Point(0, 462);
            this.labelDurOther.Name = "labelDurOther";
            this.labelDurOther.Size = new System.Drawing.Size(230, 46);
            this.labelDurOther.TabIndex = 32;
            this.labelDurOther.Text = "^9:99:99";
            this.labelDurOther.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDurVtach
            // 
            this.labelDurVtach.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurVtach.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDurVtach.Location = new System.Drawing.Point(0, 416);
            this.labelDurVtach.Name = "labelDurVtach";
            this.labelDurVtach.Size = new System.Drawing.Size(230, 46);
            this.labelDurVtach.TabIndex = 31;
            this.labelDurVtach.Text = "^9:99:99";
            this.labelDurVtach.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDurPace
            // 
            this.labelDurPace.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurPace.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDurPace.Location = new System.Drawing.Point(0, 370);
            this.labelDurPace.Name = "labelDurPace";
            this.labelDurPace.Size = new System.Drawing.Size(230, 46);
            this.labelDurPace.TabIndex = 33;
            this.labelDurPace.Text = "^9:99:99";
            this.labelDurPace.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDurPVC
            // 
            this.labelDurPVC.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurPVC.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDurPVC.Location = new System.Drawing.Point(0, 324);
            this.labelDurPVC.Name = "labelDurPVC";
            this.labelDurPVC.Size = new System.Drawing.Size(230, 46);
            this.labelDurPVC.TabIndex = 12;
            this.labelDurPVC.Text = "^9:99:99";
            this.labelDurPVC.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDurPAC
            // 
            this.labelDurPAC.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurPAC.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDurPAC.Location = new System.Drawing.Point(0, 278);
            this.labelDurPAC.Name = "labelDurPAC";
            this.labelDurPAC.Size = new System.Drawing.Size(230, 46);
            this.labelDurPAC.TabIndex = 11;
            this.labelDurPAC.Text = "^9:99:99";
            this.labelDurPAC.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDurAflut
            // 
            this.labelDurAflut.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurAflut.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDurAflut.Location = new System.Drawing.Point(0, 232);
            this.labelDurAflut.Name = "labelDurAflut";
            this.labelDurAflut.Size = new System.Drawing.Size(230, 46);
            this.labelDurAflut.TabIndex = 29;
            this.labelDurAflut.Text = "^9:99:99";
            this.labelDurAflut.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDurAfib
            // 
            this.labelDurAfib.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurAfib.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDurAfib.Location = new System.Drawing.Point(0, 186);
            this.labelDurAfib.Name = "labelDurAfib";
            this.labelDurAfib.Size = new System.Drawing.Size(230, 46);
            this.labelDurAfib.TabIndex = 4;
            this.labelDurAfib.Text = "^9:99:99";
            this.labelDurAfib.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDurPause
            // 
            this.labelDurPause.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurPause.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDurPause.Location = new System.Drawing.Point(0, 140);
            this.labelDurPause.Name = "labelDurPause";
            this.labelDurPause.Size = new System.Drawing.Size(230, 46);
            this.labelDurPause.TabIndex = 3;
            this.labelDurPause.Text = "^9:99:99";
            this.labelDurPause.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDurBrady
            // 
            this.labelDurBrady.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurBrady.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDurBrady.Location = new System.Drawing.Point(0, 94);
            this.labelDurBrady.Name = "labelDurBrady";
            this.labelDurBrady.Size = new System.Drawing.Size(230, 46);
            this.labelDurBrady.TabIndex = 2;
            this.labelDurBrady.Text = "^9:99:99";
            this.labelDurBrady.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDurTachy
            // 
            this.labelDurTachy.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurTachy.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDurTachy.Location = new System.Drawing.Point(0, 48);
            this.labelDurTachy.Name = "labelDurTachy";
            this.labelDurTachy.Size = new System.Drawing.Size(230, 46);
            this.labelDurTachy.TabIndex = 1;
            this.labelDurTachy.Text = "^9:99:99";
            this.labelDurTachy.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label22
            // 
            this.label22.Dock = System.Windows.Forms.DockStyle.Top;
            this.label22.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(0, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(230, 48);
            this.label22.TabIndex = 0;
            this.label22.Text = "Duration";
            this.label22.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel91
            // 
            this.panel91.Controls.Add(this.labelEpMan);
            this.panel91.Controls.Add(this.labelEpTotal);
            this.panel91.Controls.Add(this.label24);
            this.panel91.Controls.Add(this.labelEpAbNormal);
            this.panel91.Controls.Add(this.labelEpNormal);
            this.panel91.Controls.Add(this.labelEpOther);
            this.panel91.Controls.Add(this.labelEpVtach);
            this.panel91.Controls.Add(this.labelEpPace);
            this.panel91.Controls.Add(this.labelEpPVC);
            this.panel91.Controls.Add(this.labelEpPAC);
            this.panel91.Controls.Add(this.labelEpAflut);
            this.panel91.Controls.Add(this.labelEpAfib);
            this.panel91.Controls.Add(this.labelEpPause);
            this.panel91.Controls.Add(this.labelEpBrady);
            this.panel91.Controls.Add(this.labelEpTachy);
            this.panel91.Controls.Add(this.label17);
            this.panel91.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel91.Location = new System.Drawing.Point(210, 0);
            this.panel91.Name = "panel91";
            this.panel91.Size = new System.Drawing.Size(207, 705);
            this.panel91.TabIndex = 1;
            // 
            // labelEpMan
            // 
            this.labelEpMan.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpMan.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEpMan.Location = new System.Drawing.Point(0, 660);
            this.labelEpMan.Name = "labelEpMan";
            this.labelEpMan.Size = new System.Drawing.Size(207, 46);
            this.labelEpMan.TabIndex = 21;
            this.labelEpMan.Text = "^999";
            this.labelEpMan.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelEpTotal
            // 
            this.labelEpTotal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpTotal.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEpTotal.Location = new System.Drawing.Point(0, 614);
            this.labelEpTotal.Name = "labelEpTotal";
            this.labelEpTotal.Size = new System.Drawing.Size(207, 46);
            this.labelEpTotal.TabIndex = 23;
            this.labelEpTotal.Text = "^999";
            this.labelEpTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label24
            // 
            this.label24.Dock = System.Windows.Forms.DockStyle.Top;
            this.label24.Font = new System.Drawing.Font("Verdana", 27.75F);
            this.label24.Location = new System.Drawing.Point(0, 600);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(207, 14);
            this.label24.TabIndex = 29;
            // 
            // labelEpAbNormal
            // 
            this.labelEpAbNormal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpAbNormal.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEpAbNormal.Location = new System.Drawing.Point(0, 554);
            this.labelEpAbNormal.Name = "labelEpAbNormal";
            this.labelEpAbNormal.Size = new System.Drawing.Size(207, 46);
            this.labelEpAbNormal.TabIndex = 32;
            this.labelEpAbNormal.Text = "^999";
            this.labelEpAbNormal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelEpNormal
            // 
            this.labelEpNormal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpNormal.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEpNormal.Location = new System.Drawing.Point(0, 508);
            this.labelEpNormal.Name = "labelEpNormal";
            this.labelEpNormal.Size = new System.Drawing.Size(207, 46);
            this.labelEpNormal.TabIndex = 31;
            this.labelEpNormal.Text = "^999";
            this.labelEpNormal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelEpOther
            // 
            this.labelEpOther.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpOther.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEpOther.Location = new System.Drawing.Point(0, 462);
            this.labelEpOther.Name = "labelEpOther";
            this.labelEpOther.Size = new System.Drawing.Size(207, 46);
            this.labelEpOther.TabIndex = 28;
            this.labelEpOther.Text = "^999";
            this.labelEpOther.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelEpVtach
            // 
            this.labelEpVtach.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpVtach.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEpVtach.Location = new System.Drawing.Point(0, 416);
            this.labelEpVtach.Name = "labelEpVtach";
            this.labelEpVtach.Size = new System.Drawing.Size(207, 46);
            this.labelEpVtach.TabIndex = 27;
            this.labelEpVtach.Text = "^999";
            this.labelEpVtach.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelEpPace
            // 
            this.labelEpPace.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpPace.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEpPace.Location = new System.Drawing.Point(0, 370);
            this.labelEpPace.Name = "labelEpPace";
            this.labelEpPace.Size = new System.Drawing.Size(207, 46);
            this.labelEpPace.TabIndex = 30;
            this.labelEpPace.Text = "^999";
            this.labelEpPace.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelEpPVC
            // 
            this.labelEpPVC.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpPVC.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEpPVC.Location = new System.Drawing.Point(0, 324);
            this.labelEpPVC.Name = "labelEpPVC";
            this.labelEpPVC.Size = new System.Drawing.Size(207, 46);
            this.labelEpPVC.TabIndex = 13;
            this.labelEpPVC.Text = "^999";
            this.labelEpPVC.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelEpPAC
            // 
            this.labelEpPAC.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpPAC.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEpPAC.Location = new System.Drawing.Point(0, 278);
            this.labelEpPAC.Name = "labelEpPAC";
            this.labelEpPAC.Size = new System.Drawing.Size(207, 46);
            this.labelEpPAC.TabIndex = 12;
            this.labelEpPAC.Text = "^999";
            this.labelEpPAC.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelEpAflut
            // 
            this.labelEpAflut.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpAflut.Font = new System.Drawing.Font("Verdana", 27.75F);
            this.labelEpAflut.Location = new System.Drawing.Point(0, 232);
            this.labelEpAflut.Name = "labelEpAflut";
            this.labelEpAflut.Size = new System.Drawing.Size(207, 46);
            this.labelEpAflut.TabIndex = 26;
            this.labelEpAflut.Text = "^999";
            this.labelEpAflut.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelEpAfib
            // 
            this.labelEpAfib.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpAfib.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEpAfib.Location = new System.Drawing.Point(0, 186);
            this.labelEpAfib.Name = "labelEpAfib";
            this.labelEpAfib.Size = new System.Drawing.Size(207, 46);
            this.labelEpAfib.TabIndex = 4;
            this.labelEpAfib.Text = "^999";
            this.labelEpAfib.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelEpPause
            // 
            this.labelEpPause.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpPause.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEpPause.Location = new System.Drawing.Point(0, 140);
            this.labelEpPause.Name = "labelEpPause";
            this.labelEpPause.Size = new System.Drawing.Size(207, 46);
            this.labelEpPause.TabIndex = 3;
            this.labelEpPause.Text = "^999";
            this.labelEpPause.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelEpBrady
            // 
            this.labelEpBrady.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpBrady.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEpBrady.Location = new System.Drawing.Point(0, 94);
            this.labelEpBrady.Name = "labelEpBrady";
            this.labelEpBrady.Size = new System.Drawing.Size(207, 46);
            this.labelEpBrady.TabIndex = 2;
            this.labelEpBrady.Text = "^999";
            this.labelEpBrady.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelEpTachy
            // 
            this.labelEpTachy.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpTachy.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEpTachy.Location = new System.Drawing.Point(0, 48);
            this.labelEpTachy.Name = "labelEpTachy";
            this.labelEpTachy.Size = new System.Drawing.Size(207, 46);
            this.labelEpTachy.TabIndex = 1;
            this.labelEpTachy.Text = "^999";
            this.labelEpTachy.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label17
            // 
            this.label17.Dock = System.Windows.Forms.DockStyle.Top;
            this.label17.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(0, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(207, 48);
            this.label17.TabIndex = 0;
            this.label17.Text = "Episodes";
            this.label17.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel92
            // 
            this.panel92.Controls.Add(this.label13);
            this.panel92.Controls.Add(this.label5);
            this.panel92.Controls.Add(this.label58);
            this.panel92.Controls.Add(this.label2);
            this.panel92.Controls.Add(this.label9);
            this.panel92.Controls.Add(this.label39);
            this.panel92.Controls.Add(this.label25);
            this.panel92.Controls.Add(this.labelPace);
            this.panel92.Controls.Add(this.label29);
            this.panel92.Controls.Add(this.label23);
            this.panel92.Controls.Add(this.label6);
            this.panel92.Controls.Add(this.label15);
            this.panel92.Controls.Add(this.label16);
            this.panel92.Controls.Add(this.label18);
            this.panel92.Controls.Add(this.label19);
            this.panel92.Controls.Add(this.label20);
            this.panel92.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel92.Location = new System.Drawing.Point(0, 0);
            this.panel92.Name = "panel92";
            this.panel92.Size = new System.Drawing.Size(210, 705);
            this.panel92.TabIndex = 0;
            // 
            // label13
            // 
            this.label13.Dock = System.Windows.Forms.DockStyle.Top;
            this.label13.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(0, 660);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(210, 46);
            this.label13.TabIndex = 15;
            this.label13.Text = "Manual";
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(0, 614);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(210, 46);
            this.label5.TabIndex = 17;
            this.label5.Text = "Events";
            // 
            // label58
            // 
            this.label58.Dock = System.Windows.Forms.DockStyle.Top;
            this.label58.Font = new System.Drawing.Font("Verdana", 27.75F);
            this.label58.Location = new System.Drawing.Point(0, 600);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(210, 14);
            this.label58.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(0, 554);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(210, 46);
            this.label2.TabIndex = 26;
            this.label2.Text = "AbNormal";
            // 
            // label9
            // 
            this.label9.Dock = System.Windows.Forms.DockStyle.Top;
            this.label9.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(0, 508);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(210, 46);
            this.label9.TabIndex = 25;
            this.label9.Text = "Normal";
            // 
            // label39
            // 
            this.label39.Dock = System.Windows.Forms.DockStyle.Top;
            this.label39.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(0, 462);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(210, 46);
            this.label39.TabIndex = 23;
            this.label39.Text = "Other";
            // 
            // label25
            // 
            this.label25.Dock = System.Windows.Forms.DockStyle.Top;
            this.label25.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(0, 416);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(210, 46);
            this.label25.TabIndex = 22;
            this.label25.Text = "VTach";
            // 
            // labelPace
            // 
            this.labelPace.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPace.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPace.Location = new System.Drawing.Point(0, 370);
            this.labelPace.Name = "labelPace";
            this.labelPace.Size = new System.Drawing.Size(210, 46);
            this.labelPace.TabIndex = 24;
            this.labelPace.Text = "Pace";
            // 
            // label29
            // 
            this.label29.Dock = System.Windows.Forms.DockStyle.Top;
            this.label29.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(0, 324);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(210, 46);
            this.label29.TabIndex = 11;
            this.label29.Text = "PVC-VEB";
            // 
            // label23
            // 
            this.label23.Dock = System.Windows.Forms.DockStyle.Top;
            this.label23.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(0, 278);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(210, 46);
            this.label23.TabIndex = 10;
            this.label23.Text = "PAC-SVEB";
            // 
            // label6
            // 
            this.label6.Dock = System.Windows.Forms.DockStyle.Top;
            this.label6.Font = new System.Drawing.Font("Verdana", 27.75F);
            this.label6.Location = new System.Drawing.Point(0, 232);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(210, 46);
            this.label6.TabIndex = 20;
            this.label6.Text = "AFlut";
            // 
            // label15
            // 
            this.label15.Dock = System.Windows.Forms.DockStyle.Top;
            this.label15.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(0, 186);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(210, 46);
            this.label15.TabIndex = 4;
            this.label15.Text = "AFib";
            // 
            // label16
            // 
            this.label16.Dock = System.Windows.Forms.DockStyle.Top;
            this.label16.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(0, 140);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(210, 46);
            this.label16.TabIndex = 3;
            this.label16.Text = "Pause";
            // 
            // label18
            // 
            this.label18.Dock = System.Windows.Forms.DockStyle.Top;
            this.label18.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(0, 94);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(210, 46);
            this.label18.TabIndex = 2;
            this.label18.Text = "Brady";
            // 
            // label19
            // 
            this.label19.Dock = System.Windows.Forms.DockStyle.Top;
            this.label19.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(0, 48);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(210, 46);
            this.label19.TabIndex = 1;
            this.label19.Text = "Tachy";
            // 
            // label20
            // 
            this.label20.Dock = System.Windows.Forms.DockStyle.Top;
            this.label20.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(0, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(210, 48);
            this.label20.TabIndex = 0;
            this.label20.Text = "Type";
            // 
            // panelStatHeader
            // 
            this.panelStatHeader.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelStatHeader.Controls.Add(this.labelTechText);
            this.panelStatHeader.Controls.Add(this.labelUser);
            this.panelStatHeader.Controls.Add(this.panel85);
            this.panelStatHeader.Controls.Add(this.label12);
            this.panelStatHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelStatHeader.Location = new System.Drawing.Point(0, 1436);
            this.panelStatHeader.Name = "panelStatHeader";
            this.panelStatHeader.Size = new System.Drawing.Size(2338, 53);
            this.panelStatHeader.TabIndex = 16;
            // 
            // labelTechText
            // 
            this.labelTechText.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelTechText.Font = new System.Drawing.Font("Verdana", 27.75F);
            this.labelTechText.Location = new System.Drawing.Point(2125, 0);
            this.labelTechText.Name = "labelTechText";
            this.labelTechText.Size = new System.Drawing.Size(141, 51);
            this.labelTechText.TabIndex = 24;
            this.labelTechText.Text = "Tech:";
            // 
            // labelUser
            // 
            this.labelUser.AutoSize = true;
            this.labelUser.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelUser.Font = new System.Drawing.Font("Verdana", 27.75F);
            this.labelUser.Location = new System.Drawing.Point(2266, 0);
            this.labelUser.Name = "labelUser";
            this.labelUser.Size = new System.Drawing.Size(70, 45);
            this.labelUser.TabIndex = 23;
            this.labelUser.Text = "XX";
            this.labelUser.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel85
            // 
            this.panel85.Controls.Add(this.label28);
            this.panel85.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel85.Location = new System.Drawing.Point(745, 0);
            this.panel85.Name = "panel85";
            this.panel85.Size = new System.Drawing.Size(809, 51);
            this.panel85.TabIndex = 2;
            // 
            // label28
            // 
            this.label28.Dock = System.Windows.Forms.DockStyle.Left;
            this.label28.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(0, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(686, 51);
            this.label28.TabIndex = 2;
            this.label28.Text = "Episodes Summary Graphs";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label12
            // 
            this.label12.Dock = System.Windows.Forms.DockStyle.Left;
            this.label12.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(0, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(745, 51);
            this.label12.TabIndex = 1;
            this.label12.Text = "Episodes Summary Table";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel83
            // 
            this.panel83.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel83.Location = new System.Drawing.Point(0, 1422);
            this.panel83.Name = "panel83";
            this.panel83.Size = new System.Drawing.Size(2338, 14);
            this.panel83.TabIndex = 15;
            // 
            // panel68
            // 
            this.panel68.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel68.Controls.Add(this.panel28);
            this.panel68.Controls.Add(this.panel72);
            this.panel68.Controls.Add(this.panel23);
            this.panel68.Controls.Add(this.panel37);
            this.panel68.Controls.Add(this.panel20);
            this.panel68.Controls.Add(this.panel69);
            this.panel68.Controls.Add(this.panel13);
            this.panel68.Controls.Add(this.panel3);
            this.panel68.Controls.Add(this.panel17);
            this.panel68.Controls.Add(this.panel74);
            this.panel68.Controls.Add(this.panel76);
            this.panel68.Controls.Add(this.panel78);
            this.panel68.Controls.Add(this.panel80);
            this.panel68.Controls.Add(this.panel82);
            this.panel68.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel68.Location = new System.Drawing.Point(0, 368);
            this.panel68.Name = "panel68";
            this.panel68.Size = new System.Drawing.Size(2338, 1054);
            this.panel68.TabIndex = 14;
            // 
            // panel28
            // 
            this.panel28.Controls.Add(this.chartAll);
            this.panel28.Controls.Add(this.panel33);
            this.panel28.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel28.Location = new System.Drawing.Point(0, 970);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(2336, 80);
            this.panel28.TabIndex = 14;
            // 
            // chartAll
            // 
            chartArea2.AxisX.IsLabelAutoFit = false;
            chartArea2.AxisX.LabelStyle.Enabled = false;
            chartArea2.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea2.AxisX.MajorTickMark.Enabled = false;
            chartArea2.AxisX.ScaleBreakStyle.BreakLineStyle = System.Windows.Forms.DataVisualization.Charting.BreakLineStyle.None;
            chartArea2.AxisX.ScaleBreakStyle.LineColor = System.Drawing.Color.White;
            chartArea2.AxisX.ScaleBreakStyle.Spacing = 1D;
            chartArea2.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea2.AxisY.IsMarksNextToAxis = false;
            chartArea2.AxisY.LabelStyle.Enabled = false;
            chartArea2.AxisY.LineColor = System.Drawing.Color.DarkGray;
            chartArea2.AxisY.MajorGrid.Enabled = false;
            chartArea2.AxisY.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea2.AxisY.MajorTickMark.Enabled = false;
            chartArea2.AxisY.Maximum = 1D;
            chartArea2.AxisY.Minimum = 0D;
            chartArea2.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea2.BorderColor = System.Drawing.Color.DarkGray;
            chartArea2.InnerPlotPosition.Auto = false;
            chartArea2.InnerPlotPosition.Height = 80F;
            chartArea2.InnerPlotPosition.Width = 95F;
            chartArea2.InnerPlotPosition.X = 1F;
            chartArea2.InnerPlotPosition.Y = 1F;
            chartArea2.Name = "TachyEvents";
            this.chartAll.ChartAreas.Add(chartArea2);
            this.chartAll.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartAll.Location = new System.Drawing.Point(197, 0);
            this.chartAll.Name = "chartAll";
            series2.BorderWidth = 3;
            series2.ChartArea = "TachyEvents";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series2.Color = System.Drawing.Color.Black;
            series2.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series2.IsVisibleInLegend = false;
            series2.MarkerSize = 3;
            series2.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series2.Name = "Series1";
            series2.Points.Add(dataPoint14);
            series2.Points.Add(dataPoint15);
            series2.Points.Add(dataPoint16);
            series2.Points.Add(dataPoint17);
            series2.SmartLabelStyle.Enabled = false;
            series2.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            this.chartAll.Series.Add(series2);
            this.chartAll.Size = new System.Drawing.Size(2139, 80);
            this.chartAll.TabIndex = 1;
            this.chartAll.Text = "chart1";
            // 
            // panel33
            // 
            this.panel33.Controls.Add(this.labelGraphAll);
            this.panel33.Controls.Add(this.label26);
            this.panel33.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel33.Location = new System.Drawing.Point(0, 0);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(197, 80);
            this.panel33.TabIndex = 0;
            // 
            // labelGraphAll
            // 
            this.labelGraphAll.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelGraphAll.Font = new System.Drawing.Font("Verdana", 16F, System.Drawing.FontStyle.Bold);
            this.labelGraphAll.Location = new System.Drawing.Point(0, 45);
            this.labelGraphAll.Name = "labelGraphAll";
            this.labelGraphAll.Size = new System.Drawing.Size(197, 30);
            this.labelGraphAll.TabIndex = 2;
            this.labelGraphAll.Text = "^99";
            this.labelGraphAll.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label26
            // 
            this.label26.Dock = System.Windows.Forms.DockStyle.Top;
            this.label26.Font = new System.Drawing.Font("Verdana", 28F, System.Drawing.FontStyle.Bold);
            this.label26.Location = new System.Drawing.Point(0, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(197, 45);
            this.label26.TabIndex = 0;
            this.label26.Text = "All";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel72
            // 
            this.panel72.Controls.Add(this.chartManual);
            this.panel72.Controls.Add(this.panel73);
            this.panel72.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel72.Location = new System.Drawing.Point(0, 890);
            this.panel72.Name = "panel72";
            this.panel72.Size = new System.Drawing.Size(2336, 80);
            this.panel72.TabIndex = 8;
            // 
            // chartManual
            // 
            chartArea3.AxisX.IsLabelAutoFit = false;
            chartArea3.AxisX.LabelStyle.Enabled = false;
            chartArea3.AxisX.LineColor = System.Drawing.Color.DarkGray;
            chartArea3.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea3.AxisX.MajorTickMark.Enabled = false;
            chartArea3.AxisX.ScaleBreakStyle.BreakLineStyle = System.Windows.Forms.DataVisualization.Charting.BreakLineStyle.None;
            chartArea3.AxisX.ScaleBreakStyle.LineColor = System.Drawing.Color.White;
            chartArea3.AxisX.ScaleBreakStyle.Spacing = 1D;
            chartArea3.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea3.AxisY.IsMarksNextToAxis = false;
            chartArea3.AxisY.LabelStyle.Enabled = false;
            chartArea3.AxisY.LineColor = System.Drawing.Color.DarkGray;
            chartArea3.AxisY.MajorGrid.Enabled = false;
            chartArea3.AxisY.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea3.AxisY.MajorTickMark.Enabled = false;
            chartArea3.AxisY.Maximum = 1D;
            chartArea3.AxisY.Minimum = 0D;
            chartArea3.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea3.BorderColor = System.Drawing.Color.DarkGray;
            chartArea3.InnerPlotPosition.Auto = false;
            chartArea3.InnerPlotPosition.Height = 80F;
            chartArea3.InnerPlotPosition.Width = 95F;
            chartArea3.InnerPlotPosition.X = 1F;
            chartArea3.InnerPlotPosition.Y = 1F;
            chartArea3.Name = "TachyEvents";
            this.chartManual.ChartAreas.Add(chartArea3);
            this.chartManual.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartManual.Location = new System.Drawing.Point(197, 0);
            this.chartManual.Name = "chartManual";
            series3.BorderWidth = 3;
            series3.ChartArea = "TachyEvents";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series3.Color = System.Drawing.Color.Black;
            series3.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series3.IsVisibleInLegend = false;
            series3.MarkerSize = 3;
            series3.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series3.Name = "Series1";
            series3.Points.Add(dataPoint18);
            series3.Points.Add(dataPoint19);
            series3.Points.Add(dataPoint20);
            series3.Points.Add(dataPoint21);
            series3.SmartLabelStyle.Enabled = false;
            series3.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            this.chartManual.Series.Add(series3);
            this.chartManual.Size = new System.Drawing.Size(2139, 80);
            this.chartManual.TabIndex = 1;
            this.chartManual.Text = "chart1";
            // 
            // panel73
            // 
            this.panel73.Controls.Add(this.labelGraphManual);
            this.panel73.Controls.Add(this.label54);
            this.panel73.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel73.Location = new System.Drawing.Point(0, 0);
            this.panel73.Name = "panel73";
            this.panel73.Size = new System.Drawing.Size(197, 80);
            this.panel73.TabIndex = 0;
            // 
            // labelGraphManual
            // 
            this.labelGraphManual.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelGraphManual.Font = new System.Drawing.Font("Verdana", 16F, System.Drawing.FontStyle.Bold);
            this.labelGraphManual.Location = new System.Drawing.Point(0, 45);
            this.labelGraphManual.Name = "labelGraphManual";
            this.labelGraphManual.Size = new System.Drawing.Size(197, 30);
            this.labelGraphManual.TabIndex = 2;
            this.labelGraphManual.Text = "^99";
            this.labelGraphManual.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label54
            // 
            this.label54.Dock = System.Windows.Forms.DockStyle.Top;
            this.label54.Font = new System.Drawing.Font("Verdana", 28F, System.Drawing.FontStyle.Bold);
            this.label54.Location = new System.Drawing.Point(0, 0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(197, 45);
            this.label54.TabIndex = 0;
            this.label54.Text = "Manual";
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel23
            // 
            this.panel23.Controls.Add(this.chartOther);
            this.panel23.Controls.Add(this.panel26);
            this.panel23.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel23.Location = new System.Drawing.Point(0, 810);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(2336, 80);
            this.panel23.TabIndex = 13;
            // 
            // chartOther
            // 
            chartArea4.AxisX.IsLabelAutoFit = false;
            chartArea4.AxisX.LabelStyle.Enabled = false;
            chartArea4.AxisX.LineColor = System.Drawing.Color.DarkGray;
            chartArea4.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea4.AxisX.MajorTickMark.Enabled = false;
            chartArea4.AxisX.ScaleBreakStyle.BreakLineStyle = System.Windows.Forms.DataVisualization.Charting.BreakLineStyle.None;
            chartArea4.AxisX.ScaleBreakStyle.LineColor = System.Drawing.Color.White;
            chartArea4.AxisX.ScaleBreakStyle.Spacing = 1D;
            chartArea4.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea4.AxisY.IsMarksNextToAxis = false;
            chartArea4.AxisY.LabelStyle.Enabled = false;
            chartArea4.AxisY.LineColor = System.Drawing.Color.DarkGray;
            chartArea4.AxisY.MajorGrid.Enabled = false;
            chartArea4.AxisY.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea4.AxisY.MajorTickMark.Enabled = false;
            chartArea4.AxisY.Maximum = 1D;
            chartArea4.AxisY.Minimum = 0D;
            chartArea4.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea4.BorderColor = System.Drawing.Color.DarkGray;
            chartArea4.InnerPlotPosition.Auto = false;
            chartArea4.InnerPlotPosition.Height = 80F;
            chartArea4.InnerPlotPosition.Width = 95F;
            chartArea4.InnerPlotPosition.X = 1F;
            chartArea4.InnerPlotPosition.Y = 1F;
            chartArea4.Name = "TachyEvents";
            this.chartOther.ChartAreas.Add(chartArea4);
            this.chartOther.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartOther.Location = new System.Drawing.Point(197, 0);
            this.chartOther.Name = "chartOther";
            series4.BorderWidth = 3;
            series4.ChartArea = "TachyEvents";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series4.Color = System.Drawing.Color.Black;
            series4.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series4.IsVisibleInLegend = false;
            series4.MarkerSize = 3;
            series4.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series4.Name = "Series1";
            series4.Points.Add(dataPoint22);
            series4.Points.Add(dataPoint23);
            series4.Points.Add(dataPoint24);
            series4.Points.Add(dataPoint25);
            series4.SmartLabelStyle.Enabled = false;
            series4.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            this.chartOther.Series.Add(series4);
            this.chartOther.Size = new System.Drawing.Size(2139, 80);
            this.chartOther.TabIndex = 1;
            this.chartOther.Text = "chart2";
            // 
            // panel26
            // 
            this.panel26.Controls.Add(this.labelGraphOther);
            this.panel26.Controls.Add(this.label50);
            this.panel26.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel26.Location = new System.Drawing.Point(0, 0);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(197, 80);
            this.panel26.TabIndex = 0;
            // 
            // labelGraphOther
            // 
            this.labelGraphOther.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelGraphOther.Font = new System.Drawing.Font("Verdana", 16F, System.Drawing.FontStyle.Bold);
            this.labelGraphOther.Location = new System.Drawing.Point(0, 45);
            this.labelGraphOther.Name = "labelGraphOther";
            this.labelGraphOther.Size = new System.Drawing.Size(197, 30);
            this.labelGraphOther.TabIndex = 2;
            this.labelGraphOther.Text = "^99";
            this.labelGraphOther.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label50
            // 
            this.label50.Dock = System.Windows.Forms.DockStyle.Top;
            this.label50.Font = new System.Drawing.Font("Verdana", 28F, System.Drawing.FontStyle.Bold);
            this.label50.Location = new System.Drawing.Point(0, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(197, 45);
            this.label50.TabIndex = 0;
            this.label50.Text = "Other";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel37
            // 
            this.panel37.Controls.Add(this.chartAbN);
            this.panel37.Controls.Add(this.panel38);
            this.panel37.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel37.Location = new System.Drawing.Point(0, 730);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(2336, 80);
            this.panel37.TabIndex = 15;
            // 
            // chartAbN
            // 
            chartArea5.AxisX.IsLabelAutoFit = false;
            chartArea5.AxisX.LabelStyle.Enabled = false;
            chartArea5.AxisX.LineWidth = 3;
            chartArea5.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea5.AxisX.MajorTickMark.Enabled = false;
            chartArea5.AxisX.ScaleBreakStyle.BreakLineStyle = System.Windows.Forms.DataVisualization.Charting.BreakLineStyle.None;
            chartArea5.AxisX.ScaleBreakStyle.LineColor = System.Drawing.Color.White;
            chartArea5.AxisX.ScaleBreakStyle.Spacing = 1D;
            chartArea5.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea5.AxisY.IsMarksNextToAxis = false;
            chartArea5.AxisY.LabelStyle.Enabled = false;
            chartArea5.AxisY.LineColor = System.Drawing.Color.DarkGray;
            chartArea5.AxisY.MajorGrid.Enabled = false;
            chartArea5.AxisY.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea5.AxisY.MajorTickMark.Enabled = false;
            chartArea5.AxisY.Maximum = 1D;
            chartArea5.AxisY.Minimum = 0D;
            chartArea5.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea5.BorderColor = System.Drawing.Color.DarkGray;
            chartArea5.InnerPlotPosition.Auto = false;
            chartArea5.InnerPlotPosition.Height = 80F;
            chartArea5.InnerPlotPosition.Width = 95F;
            chartArea5.InnerPlotPosition.X = 1F;
            chartArea5.InnerPlotPosition.Y = 1F;
            chartArea5.Name = "TachyEvents";
            this.chartAbN.ChartAreas.Add(chartArea5);
            this.chartAbN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartAbN.Location = new System.Drawing.Point(197, 0);
            this.chartAbN.Name = "chartAbN";
            series5.BorderWidth = 3;
            series5.ChartArea = "TachyEvents";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series5.Color = System.Drawing.Color.Black;
            series5.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series5.IsVisibleInLegend = false;
            series5.MarkerSize = 3;
            series5.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series5.Name = "Series1";
            series5.Points.Add(dataPoint26);
            series5.Points.Add(dataPoint27);
            series5.Points.Add(dataPoint28);
            series5.Points.Add(dataPoint29);
            series5.SmartLabelStyle.Enabled = false;
            series5.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            this.chartAbN.Series.Add(series5);
            this.chartAbN.Size = new System.Drawing.Size(2139, 80);
            this.chartAbN.TabIndex = 1;
            this.chartAbN.Text = "chart1";
            // 
            // panel38
            // 
            this.panel38.Controls.Add(this.labelGraphAbN);
            this.panel38.Controls.Add(this.label57);
            this.panel38.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel38.Location = new System.Drawing.Point(0, 0);
            this.panel38.Name = "panel38";
            this.panel38.Size = new System.Drawing.Size(197, 80);
            this.panel38.TabIndex = 0;
            // 
            // labelGraphAbN
            // 
            this.labelGraphAbN.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelGraphAbN.Font = new System.Drawing.Font("Verdana", 16F, System.Drawing.FontStyle.Bold);
            this.labelGraphAbN.Location = new System.Drawing.Point(0, 45);
            this.labelGraphAbN.Name = "labelGraphAbN";
            this.labelGraphAbN.Size = new System.Drawing.Size(197, 30);
            this.labelGraphAbN.TabIndex = 2;
            this.labelGraphAbN.Text = "^99";
            this.labelGraphAbN.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label57
            // 
            this.label57.Dock = System.Windows.Forms.DockStyle.Top;
            this.label57.Font = new System.Drawing.Font("Verdana", 28F, System.Drawing.FontStyle.Bold);
            this.label57.Location = new System.Drawing.Point(0, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(197, 45);
            this.label57.TabIndex = 0;
            this.label57.Text = "AbNorm";
            this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.chartVtach);
            this.panel20.Controls.Add(this.panel21);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel20.Location = new System.Drawing.Point(0, 650);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(2336, 80);
            this.panel20.TabIndex = 12;
            // 
            // chartVtach
            // 
            chartArea6.AxisX.IsLabelAutoFit = false;
            chartArea6.AxisX.LabelStyle.Enabled = false;
            chartArea6.AxisX.LineWidth = 3;
            chartArea6.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea6.AxisX.MajorTickMark.Enabled = false;
            chartArea6.AxisX.ScaleBreakStyle.BreakLineStyle = System.Windows.Forms.DataVisualization.Charting.BreakLineStyle.None;
            chartArea6.AxisX.ScaleBreakStyle.LineColor = System.Drawing.Color.White;
            chartArea6.AxisX.ScaleBreakStyle.Spacing = 1D;
            chartArea6.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea6.AxisY.IsMarksNextToAxis = false;
            chartArea6.AxisY.LabelStyle.Enabled = false;
            chartArea6.AxisY.LineColor = System.Drawing.Color.DarkGray;
            chartArea6.AxisY.MajorGrid.Enabled = false;
            chartArea6.AxisY.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea6.AxisY.MajorTickMark.Enabled = false;
            chartArea6.AxisY.Maximum = 1D;
            chartArea6.AxisY.Minimum = 0D;
            chartArea6.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea6.BorderColor = System.Drawing.Color.DarkGray;
            chartArea6.InnerPlotPosition.Auto = false;
            chartArea6.InnerPlotPosition.Height = 80F;
            chartArea6.InnerPlotPosition.Width = 95F;
            chartArea6.InnerPlotPosition.X = 1F;
            chartArea6.InnerPlotPosition.Y = 1F;
            chartArea6.Name = "TachyEvents";
            this.chartVtach.ChartAreas.Add(chartArea6);
            this.chartVtach.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartVtach.Location = new System.Drawing.Point(197, 0);
            this.chartVtach.Name = "chartVtach";
            series6.BorderWidth = 3;
            series6.ChartArea = "TachyEvents";
            series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series6.Color = System.Drawing.Color.Black;
            series6.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series6.IsVisibleInLegend = false;
            series6.MarkerSize = 3;
            series6.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series6.Name = "Series1";
            series6.Points.Add(dataPoint30);
            series6.Points.Add(dataPoint31);
            series6.Points.Add(dataPoint32);
            series6.Points.Add(dataPoint33);
            series6.SmartLabelStyle.Enabled = false;
            series6.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            this.chartVtach.Series.Add(series6);
            this.chartVtach.Size = new System.Drawing.Size(2139, 80);
            this.chartVtach.TabIndex = 1;
            this.chartVtach.Text = "chart1";
            // 
            // panel21
            // 
            this.panel21.Controls.Add(this.labelGraphVtach);
            this.panel21.Controls.Add(this.label49);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel21.Location = new System.Drawing.Point(0, 0);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(197, 80);
            this.panel21.TabIndex = 0;
            // 
            // labelGraphVtach
            // 
            this.labelGraphVtach.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelGraphVtach.Font = new System.Drawing.Font("Verdana", 16F, System.Drawing.FontStyle.Bold);
            this.labelGraphVtach.Location = new System.Drawing.Point(0, 45);
            this.labelGraphVtach.Name = "labelGraphVtach";
            this.labelGraphVtach.Size = new System.Drawing.Size(197, 30);
            this.labelGraphVtach.TabIndex = 2;
            this.labelGraphVtach.Text = "^99";
            this.labelGraphVtach.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelGraphVtach.Click += new System.EventHandler(this.labelGraphVtach_Click);
            // 
            // label49
            // 
            this.label49.Dock = System.Windows.Forms.DockStyle.Top;
            this.label49.Font = new System.Drawing.Font("Verdana", 28F, System.Drawing.FontStyle.Bold);
            this.label49.Location = new System.Drawing.Point(0, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(197, 45);
            this.label49.TabIndex = 0;
            this.label49.Text = "VTach";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel69
            // 
            this.panel69.Controls.Add(this.chartPace);
            this.panel69.Controls.Add(this.panel70);
            this.panel69.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel69.Location = new System.Drawing.Point(0, 570);
            this.panel69.Name = "panel69";
            this.panel69.Size = new System.Drawing.Size(2336, 80);
            this.panel69.TabIndex = 7;
            // 
            // chartPace
            // 
            chartArea7.AxisX.IsLabelAutoFit = false;
            chartArea7.AxisX.LabelStyle.Enabled = false;
            chartArea7.AxisX.LineColor = System.Drawing.Color.DarkGray;
            chartArea7.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea7.AxisX.MinorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea7.AxisX.ScaleBreakStyle.BreakLineStyle = System.Windows.Forms.DataVisualization.Charting.BreakLineStyle.None;
            chartArea7.AxisX.ScaleBreakStyle.LineColor = System.Drawing.Color.White;
            chartArea7.AxisX.ScaleBreakStyle.Spacing = 1D;
            chartArea7.AxisX2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea7.AxisX2.MajorGrid.Enabled = false;
            chartArea7.AxisX2.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea7.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea7.AxisY.IsLabelAutoFit = false;
            chartArea7.AxisY.LabelStyle.Enabled = false;
            chartArea7.AxisY.LineColor = System.Drawing.Color.DarkGray;
            chartArea7.AxisY.MajorGrid.Enabled = false;
            chartArea7.AxisY.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea7.AxisY.MajorTickMark.Enabled = false;
            chartArea7.AxisY.Maximum = 1D;
            chartArea7.AxisY.Minimum = 0D;
            chartArea7.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea7.BorderColor = System.Drawing.Color.DarkGray;
            chartArea7.InnerPlotPosition.Auto = false;
            chartArea7.InnerPlotPosition.Height = 80F;
            chartArea7.InnerPlotPosition.Width = 95F;
            chartArea7.InnerPlotPosition.X = 1F;
            chartArea7.InnerPlotPosition.Y = 1F;
            chartArea7.Name = "TachyEvents";
            this.chartPace.ChartAreas.Add(chartArea7);
            this.chartPace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartPace.Location = new System.Drawing.Point(197, 0);
            this.chartPace.Name = "chartPace";
            series7.BorderWidth = 3;
            series7.ChartArea = "TachyEvents";
            series7.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series7.Color = System.Drawing.Color.Black;
            series7.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series7.IsVisibleInLegend = false;
            series7.MarkerSize = 3;
            series7.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series7.Name = "Series1";
            series7.Points.Add(dataPoint34);
            series7.Points.Add(dataPoint35);
            series7.Points.Add(dataPoint36);
            series7.Points.Add(dataPoint37);
            series7.SmartLabelStyle.Enabled = false;
            series7.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            this.chartPace.Series.Add(series7);
            this.chartPace.Size = new System.Drawing.Size(2139, 80);
            this.chartPace.TabIndex = 1;
            this.chartPace.Text = "chart1";
            // 
            // panel70
            // 
            this.panel70.Controls.Add(this.labelGraphPace);
            this.panel70.Controls.Add(this.label38);
            this.panel70.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel70.Location = new System.Drawing.Point(0, 0);
            this.panel70.Name = "panel70";
            this.panel70.Size = new System.Drawing.Size(197, 80);
            this.panel70.TabIndex = 0;
            // 
            // labelGraphPace
            // 
            this.labelGraphPace.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelGraphPace.Font = new System.Drawing.Font("Verdana", 16F, System.Drawing.FontStyle.Bold);
            this.labelGraphPace.Location = new System.Drawing.Point(0, 45);
            this.labelGraphPace.Name = "labelGraphPace";
            this.labelGraphPace.Size = new System.Drawing.Size(197, 30);
            this.labelGraphPace.TabIndex = 2;
            this.labelGraphPace.Text = "^99";
            this.labelGraphPace.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label38
            // 
            this.label38.Dock = System.Windows.Forms.DockStyle.Top;
            this.label38.Font = new System.Drawing.Font("Verdana", 28F, System.Drawing.FontStyle.Bold);
            this.label38.Location = new System.Drawing.Point(0, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(197, 45);
            this.label38.TabIndex = 0;
            this.label38.Text = "Pace";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.chartPVC);
            this.panel13.Controls.Add(this.panel14);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel13.Location = new System.Drawing.Point(0, 490);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(2336, 80);
            this.panel13.TabIndex = 10;
            // 
            // chartPVC
            // 
            chartArea8.AxisX.IsLabelAutoFit = false;
            chartArea8.AxisX.LabelStyle.Enabled = false;
            chartArea8.AxisX.LineColor = System.Drawing.Color.DarkGray;
            chartArea8.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea8.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DarkGray;
            chartArea8.AxisX.ScaleBreakStyle.BreakLineStyle = System.Windows.Forms.DataVisualization.Charting.BreakLineStyle.None;
            chartArea8.AxisX.ScaleBreakStyle.LineColor = System.Drawing.Color.White;
            chartArea8.AxisX.ScaleBreakStyle.Spacing = 1D;
            chartArea8.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea8.AxisY.IsLabelAutoFit = false;
            chartArea8.AxisY.LabelStyle.Enabled = false;
            chartArea8.AxisY.LineColor = System.Drawing.Color.DarkGray;
            chartArea8.AxisY.MajorGrid.Enabled = false;
            chartArea8.AxisY.MajorTickMark.Enabled = false;
            chartArea8.AxisY.Maximum = 1D;
            chartArea8.AxisY.Minimum = 0D;
            chartArea8.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea8.BorderColor = System.Drawing.Color.DarkGray;
            chartArea8.InnerPlotPosition.Auto = false;
            chartArea8.InnerPlotPosition.Height = 80F;
            chartArea8.InnerPlotPosition.Width = 95F;
            chartArea8.InnerPlotPosition.X = 1F;
            chartArea8.InnerPlotPosition.Y = 1F;
            chartArea8.Name = "TachyEvents";
            this.chartPVC.ChartAreas.Add(chartArea8);
            this.chartPVC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartPVC.Location = new System.Drawing.Point(197, 0);
            this.chartPVC.Name = "chartPVC";
            series8.BorderWidth = 2;
            series8.ChartArea = "TachyEvents";
            series8.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series8.Color = System.Drawing.Color.Black;
            series8.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series8.IsVisibleInLegend = false;
            series8.MarkerSize = 3;
            series8.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series8.Name = "Series1";
            series8.Points.Add(dataPoint38);
            series8.Points.Add(dataPoint39);
            series8.Points.Add(dataPoint40);
            series8.Points.Add(dataPoint41);
            series8.SmartLabelStyle.Enabled = false;
            series8.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            this.chartPVC.Series.Add(series8);
            this.chartPVC.Size = new System.Drawing.Size(2139, 80);
            this.chartPVC.TabIndex = 1;
            this.chartPVC.Text = "chart2";
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.labelGraphPVC);
            this.panel14.Controls.Add(this.label10);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel14.Location = new System.Drawing.Point(0, 0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(197, 80);
            this.panel14.TabIndex = 0;
            // 
            // labelGraphPVC
            // 
            this.labelGraphPVC.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelGraphPVC.Font = new System.Drawing.Font("Verdana", 16F, System.Drawing.FontStyle.Bold);
            this.labelGraphPVC.Location = new System.Drawing.Point(0, 45);
            this.labelGraphPVC.Name = "labelGraphPVC";
            this.labelGraphPVC.Size = new System.Drawing.Size(197, 30);
            this.labelGraphPVC.TabIndex = 2;
            this.labelGraphPVC.Text = "^99";
            this.labelGraphPVC.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.Dock = System.Windows.Forms.DockStyle.Top;
            this.label10.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(0, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(197, 45);
            this.label10.TabIndex = 0;
            this.label10.Text = "PVC-VEB";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.chartPAC);
            this.panel3.Controls.Add(this.panel9);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 410);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(2336, 80);
            this.panel3.TabIndex = 9;
            // 
            // chartPAC
            // 
            chartArea9.AxisX.IsLabelAutoFit = false;
            chartArea9.AxisX.LabelStyle.Enabled = false;
            chartArea9.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea9.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DarkGray;
            chartArea9.AxisX.ScaleBreakStyle.BreakLineStyle = System.Windows.Forms.DataVisualization.Charting.BreakLineStyle.None;
            chartArea9.AxisX.ScaleBreakStyle.LineColor = System.Drawing.Color.White;
            chartArea9.AxisX.ScaleBreakStyle.Spacing = 1D;
            chartArea9.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea9.AxisY.IsLabelAutoFit = false;
            chartArea9.AxisY.LabelStyle.Enabled = false;
            chartArea9.AxisY.LineColor = System.Drawing.Color.DarkGray;
            chartArea9.AxisY.MajorGrid.Enabled = false;
            chartArea9.AxisY.MajorTickMark.Enabled = false;
            chartArea9.AxisY.Maximum = 1D;
            chartArea9.AxisY.Minimum = 0D;
            chartArea9.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea9.BorderColor = System.Drawing.Color.DarkGray;
            chartArea9.InnerPlotPosition.Auto = false;
            chartArea9.InnerPlotPosition.Height = 80F;
            chartArea9.InnerPlotPosition.Width = 95F;
            chartArea9.InnerPlotPosition.X = 1F;
            chartArea9.InnerPlotPosition.Y = 1F;
            chartArea9.Name = "TachyEvents";
            this.chartPAC.ChartAreas.Add(chartArea9);
            this.chartPAC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartPAC.Location = new System.Drawing.Point(197, 0);
            this.chartPAC.Name = "chartPAC";
            series9.BorderWidth = 2;
            series9.ChartArea = "TachyEvents";
            series9.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series9.Color = System.Drawing.Color.Black;
            series9.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series9.IsVisibleInLegend = false;
            series9.MarkerSize = 3;
            series9.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series9.Name = "Series1";
            series9.Points.Add(dataPoint42);
            series9.Points.Add(dataPoint43);
            series9.Points.Add(dataPoint44);
            series9.Points.Add(dataPoint45);
            series9.SmartLabelStyle.Enabled = false;
            series9.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            this.chartPAC.Series.Add(series9);
            this.chartPAC.Size = new System.Drawing.Size(2139, 80);
            this.chartPAC.TabIndex = 1;
            this.chartPAC.Text = "chart1";
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.labelGraphPAC);
            this.panel9.Controls.Add(this.label1);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(197, 80);
            this.panel9.TabIndex = 0;
            // 
            // labelGraphPAC
            // 
            this.labelGraphPAC.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelGraphPAC.Font = new System.Drawing.Font("Verdana", 16F, System.Drawing.FontStyle.Bold);
            this.labelGraphPAC.Location = new System.Drawing.Point(0, 45);
            this.labelGraphPAC.Name = "labelGraphPAC";
            this.labelGraphPAC.Size = new System.Drawing.Size(197, 30);
            this.labelGraphPAC.TabIndex = 2;
            this.labelGraphPAC.Text = "^99";
            this.labelGraphPAC.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(197, 45);
            this.label1.TabIndex = 0;
            this.label1.Text = "PAC-SVEB";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.chartAflut);
            this.panel17.Controls.Add(this.panel19);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel17.Location = new System.Drawing.Point(0, 330);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(2336, 80);
            this.panel17.TabIndex = 11;
            // 
            // chartAflut
            // 
            chartArea10.AxisX.IsLabelAutoFit = false;
            chartArea10.AxisX.LabelStyle.Enabled = false;
            chartArea10.AxisX.LineColor = System.Drawing.Color.DarkGray;
            chartArea10.AxisX.LineWidth = 3;
            chartArea10.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea10.AxisX.MajorTickMark.Enabled = false;
            chartArea10.AxisX.ScaleBreakStyle.BreakLineStyle = System.Windows.Forms.DataVisualization.Charting.BreakLineStyle.None;
            chartArea10.AxisX.ScaleBreakStyle.LineColor = System.Drawing.Color.White;
            chartArea10.AxisX.ScaleBreakStyle.Spacing = 1D;
            chartArea10.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea10.AxisY.IsMarksNextToAxis = false;
            chartArea10.AxisY.LabelStyle.Enabled = false;
            chartArea10.AxisY.LineColor = System.Drawing.Color.DarkGray;
            chartArea10.AxisY.MajorGrid.Enabled = false;
            chartArea10.AxisY.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea10.AxisY.MajorTickMark.Enabled = false;
            chartArea10.AxisY.Maximum = 1D;
            chartArea10.AxisY.Minimum = 0D;
            chartArea10.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea10.BorderColor = System.Drawing.Color.DarkGray;
            chartArea10.InnerPlotPosition.Auto = false;
            chartArea10.InnerPlotPosition.Height = 80F;
            chartArea10.InnerPlotPosition.Width = 95F;
            chartArea10.InnerPlotPosition.X = 1F;
            chartArea10.InnerPlotPosition.Y = 1F;
            chartArea10.Name = "TachyEvents";
            this.chartAflut.ChartAreas.Add(chartArea10);
            this.chartAflut.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartAflut.Location = new System.Drawing.Point(197, 0);
            this.chartAflut.Name = "chartAflut";
            series10.BorderWidth = 3;
            series10.ChartArea = "TachyEvents";
            series10.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StackedArea;
            series10.Color = System.Drawing.Color.Black;
            series10.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series10.IsVisibleInLegend = false;
            series10.MarkerSize = 3;
            series10.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series10.Name = "Series1";
            series10.Points.Add(dataPoint46);
            series10.Points.Add(dataPoint47);
            series10.Points.Add(dataPoint48);
            series10.Points.Add(dataPoint49);
            series10.SmartLabelStyle.Enabled = false;
            series10.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            this.chartAflut.Series.Add(series10);
            this.chartAflut.Size = new System.Drawing.Size(2139, 80);
            this.chartAflut.TabIndex = 1;
            this.chartAflut.Text = "chart1";
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.labelGraphAflut);
            this.panel19.Controls.Add(this.label48);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel19.Location = new System.Drawing.Point(0, 0);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(197, 80);
            this.panel19.TabIndex = 0;
            // 
            // labelGraphAflut
            // 
            this.labelGraphAflut.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelGraphAflut.Font = new System.Drawing.Font("Verdana", 16F, System.Drawing.FontStyle.Bold);
            this.labelGraphAflut.Location = new System.Drawing.Point(0, 45);
            this.labelGraphAflut.Name = "labelGraphAflut";
            this.labelGraphAflut.Size = new System.Drawing.Size(197, 30);
            this.labelGraphAflut.TabIndex = 2;
            this.labelGraphAflut.Text = "^99";
            this.labelGraphAflut.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label48
            // 
            this.label48.Dock = System.Windows.Forms.DockStyle.Top;
            this.label48.Font = new System.Drawing.Font("Verdana", 28F, System.Drawing.FontStyle.Bold);
            this.label48.Location = new System.Drawing.Point(0, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(197, 45);
            this.label48.TabIndex = 0;
            this.label48.Text = "AFlut";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel74
            // 
            this.panel74.Controls.Add(this.chartAfib);
            this.panel74.Controls.Add(this.panel75);
            this.panel74.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel74.Location = new System.Drawing.Point(0, 250);
            this.panel74.Name = "panel74";
            this.panel74.Size = new System.Drawing.Size(2336, 80);
            this.panel74.TabIndex = 6;
            // 
            // chartAfib
            // 
            chartArea11.AxisX.IsLabelAutoFit = false;
            chartArea11.AxisX.LabelStyle.Enabled = false;
            chartArea11.AxisX.LineColor = System.Drawing.Color.DarkGray;
            chartArea11.AxisX.LineWidth = 3;
            chartArea11.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea11.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DarkGray;
            chartArea11.AxisX.ScaleBreakStyle.BreakLineStyle = System.Windows.Forms.DataVisualization.Charting.BreakLineStyle.None;
            chartArea11.AxisX.ScaleBreakStyle.LineColor = System.Drawing.Color.White;
            chartArea11.AxisX.ScaleBreakStyle.Spacing = 1D;
            chartArea11.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea11.AxisY.IsLabelAutoFit = false;
            chartArea11.AxisY.LabelStyle.Enabled = false;
            chartArea11.AxisY.LineColor = System.Drawing.Color.DarkGray;
            chartArea11.AxisY.MajorGrid.Enabled = false;
            chartArea11.AxisY.MajorTickMark.Enabled = false;
            chartArea11.AxisY.Maximum = 1D;
            chartArea11.AxisY.Minimum = 0D;
            chartArea11.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea11.BorderColor = System.Drawing.Color.DarkGray;
            chartArea11.InnerPlotPosition.Auto = false;
            chartArea11.InnerPlotPosition.Height = 80F;
            chartArea11.InnerPlotPosition.Width = 95F;
            chartArea11.InnerPlotPosition.X = 1F;
            chartArea11.InnerPlotPosition.Y = 1F;
            chartArea11.Name = "TachyEvents";
            this.chartAfib.ChartAreas.Add(chartArea11);
            this.chartAfib.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartAfib.Location = new System.Drawing.Point(197, 0);
            this.chartAfib.Name = "chartAfib";
            series11.BorderWidth = 3;
            series11.ChartArea = "TachyEvents";
            series11.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series11.Color = System.Drawing.Color.Black;
            series11.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series11.IsVisibleInLegend = false;
            series11.MarkerSize = 3;
            series11.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series11.Name = "Series1";
            series11.Points.Add(dataPoint50);
            series11.Points.Add(dataPoint51);
            series11.Points.Add(dataPoint52);
            series11.Points.Add(dataPoint53);
            series11.SmartLabelStyle.Enabled = false;
            series11.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            this.chartAfib.Series.Add(series11);
            this.chartAfib.Size = new System.Drawing.Size(2139, 80);
            this.chartAfib.TabIndex = 1;
            this.chartAfib.Text = "chart1";
            // 
            // panel75
            // 
            this.panel75.Controls.Add(this.labelGraphAfib);
            this.panel75.Controls.Add(this.label36);
            this.panel75.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel75.Location = new System.Drawing.Point(0, 0);
            this.panel75.Name = "panel75";
            this.panel75.Size = new System.Drawing.Size(197, 80);
            this.panel75.TabIndex = 0;
            // 
            // labelGraphAfib
            // 
            this.labelGraphAfib.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelGraphAfib.Font = new System.Drawing.Font("Verdana", 16F, System.Drawing.FontStyle.Bold);
            this.labelGraphAfib.Location = new System.Drawing.Point(0, 45);
            this.labelGraphAfib.Name = "labelGraphAfib";
            this.labelGraphAfib.Size = new System.Drawing.Size(197, 30);
            this.labelGraphAfib.TabIndex = 2;
            this.labelGraphAfib.Text = "^99";
            this.labelGraphAfib.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label36
            // 
            this.label36.Dock = System.Windows.Forms.DockStyle.Top;
            this.label36.Font = new System.Drawing.Font("Verdana", 28F, System.Drawing.FontStyle.Bold);
            this.label36.Location = new System.Drawing.Point(0, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(197, 45);
            this.label36.TabIndex = 0;
            this.label36.Text = "AFib";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel76
            // 
            this.panel76.Controls.Add(this.chartPause);
            this.panel76.Controls.Add(this.panel77);
            this.panel76.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel76.Location = new System.Drawing.Point(0, 170);
            this.panel76.Name = "panel76";
            this.panel76.Size = new System.Drawing.Size(2336, 80);
            this.panel76.TabIndex = 5;
            // 
            // chartPause
            // 
            chartArea12.AxisX.IsLabelAutoFit = false;
            chartArea12.AxisX.LabelStyle.Enabled = false;
            chartArea12.AxisX.LineWidth = 3;
            chartArea12.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea12.AxisX.ScaleBreakStyle.BreakLineStyle = System.Windows.Forms.DataVisualization.Charting.BreakLineStyle.None;
            chartArea12.AxisX.ScaleBreakStyle.LineColor = System.Drawing.Color.White;
            chartArea12.AxisX.ScaleBreakStyle.Spacing = 1D;
            chartArea12.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea12.AxisY.IsLabelAutoFit = false;
            chartArea12.AxisY.LabelStyle.Enabled = false;
            chartArea12.AxisY.LineColor = System.Drawing.Color.DarkGray;
            chartArea12.AxisY.MajorGrid.Enabled = false;
            chartArea12.AxisY.MajorTickMark.Enabled = false;
            chartArea12.AxisY.Maximum = 1D;
            chartArea12.AxisY.Minimum = 0D;
            chartArea12.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea12.InnerPlotPosition.Auto = false;
            chartArea12.InnerPlotPosition.Height = 80F;
            chartArea12.InnerPlotPosition.Width = 95F;
            chartArea12.InnerPlotPosition.X = 1F;
            chartArea12.InnerPlotPosition.Y = 1F;
            chartArea12.Name = "TachyEvents";
            this.chartPause.ChartAreas.Add(chartArea12);
            this.chartPause.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartPause.Location = new System.Drawing.Point(197, 0);
            this.chartPause.Name = "chartPause";
            series12.BorderWidth = 3;
            series12.ChartArea = "TachyEvents";
            series12.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series12.Color = System.Drawing.Color.Black;
            series12.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series12.IsVisibleInLegend = false;
            series12.MarkerSize = 3;
            series12.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series12.Name = "Series1";
            series12.Points.Add(dataPoint54);
            series12.Points.Add(dataPoint55);
            series12.Points.Add(dataPoint56);
            series12.Points.Add(dataPoint57);
            series12.SmartLabelStyle.Enabled = false;
            series12.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            this.chartPause.Series.Add(series12);
            this.chartPause.Size = new System.Drawing.Size(2139, 80);
            this.chartPause.TabIndex = 1;
            this.chartPause.Text = "chart1";
            // 
            // panel77
            // 
            this.panel77.Controls.Add(this.labelGraphPause);
            this.panel77.Controls.Add(this.label34);
            this.panel77.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel77.Location = new System.Drawing.Point(0, 0);
            this.panel77.Name = "panel77";
            this.panel77.Size = new System.Drawing.Size(197, 80);
            this.panel77.TabIndex = 0;
            // 
            // labelGraphPause
            // 
            this.labelGraphPause.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelGraphPause.Font = new System.Drawing.Font("Verdana", 16F, System.Drawing.FontStyle.Bold);
            this.labelGraphPause.Location = new System.Drawing.Point(0, 45);
            this.labelGraphPause.Name = "labelGraphPause";
            this.labelGraphPause.Size = new System.Drawing.Size(197, 30);
            this.labelGraphPause.TabIndex = 2;
            this.labelGraphPause.Text = "^99";
            this.labelGraphPause.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label34
            // 
            this.label34.Dock = System.Windows.Forms.DockStyle.Top;
            this.label34.Font = new System.Drawing.Font("Verdana", 28F, System.Drawing.FontStyle.Bold);
            this.label34.Location = new System.Drawing.Point(0, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(197, 45);
            this.label34.TabIndex = 0;
            this.label34.Text = "Pause";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel78
            // 
            this.panel78.Controls.Add(this.chartBrady);
            this.panel78.Controls.Add(this.panel79);
            this.panel78.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel78.Location = new System.Drawing.Point(0, 90);
            this.panel78.Name = "panel78";
            this.panel78.Size = new System.Drawing.Size(2336, 80);
            this.panel78.TabIndex = 4;
            // 
            // chartBrady
            // 
            chartArea13.AxisX.IsLabelAutoFit = false;
            chartArea13.AxisX.LabelStyle.Enabled = false;
            chartArea13.AxisX.LineWidth = 3;
            chartArea13.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea13.AxisX.ScaleBreakStyle.BreakLineStyle = System.Windows.Forms.DataVisualization.Charting.BreakLineStyle.None;
            chartArea13.AxisX.ScaleBreakStyle.LineColor = System.Drawing.Color.White;
            chartArea13.AxisX.ScaleBreakStyle.Spacing = 1D;
            chartArea13.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea13.AxisY.IsLabelAutoFit = false;
            chartArea13.AxisY.LabelStyle.Enabled = false;
            chartArea13.AxisY.LineColor = System.Drawing.Color.DarkGray;
            chartArea13.AxisY.MajorGrid.Enabled = false;
            chartArea13.AxisY.MajorTickMark.Enabled = false;
            chartArea13.AxisY.Maximum = 1D;
            chartArea13.AxisY.Minimum = 0D;
            chartArea13.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea13.BorderColor = System.Drawing.Color.DarkGray;
            chartArea13.InnerPlotPosition.Auto = false;
            chartArea13.InnerPlotPosition.Height = 80F;
            chartArea13.InnerPlotPosition.Width = 95F;
            chartArea13.InnerPlotPosition.X = 1F;
            chartArea13.InnerPlotPosition.Y = 1F;
            chartArea13.Name = "TachyEvents";
            this.chartBrady.ChartAreas.Add(chartArea13);
            this.chartBrady.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartBrady.Location = new System.Drawing.Point(197, 0);
            this.chartBrady.Name = "chartBrady";
            series13.BorderWidth = 3;
            series13.ChartArea = "TachyEvents";
            series13.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series13.Color = System.Drawing.Color.Black;
            series13.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series13.IsVisibleInLegend = false;
            series13.MarkerSize = 3;
            series13.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series13.Name = "Series1";
            series13.Points.Add(dataPoint58);
            series13.Points.Add(dataPoint59);
            series13.Points.Add(dataPoint60);
            series13.Points.Add(dataPoint61);
            series13.SmartLabelStyle.Enabled = false;
            series13.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            this.chartBrady.Series.Add(series13);
            this.chartBrady.Size = new System.Drawing.Size(2139, 80);
            this.chartBrady.TabIndex = 1;
            this.chartBrady.Text = "chart1";
            // 
            // panel79
            // 
            this.panel79.Controls.Add(this.labelGraphBrady);
            this.panel79.Controls.Add(this.label32);
            this.panel79.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel79.Location = new System.Drawing.Point(0, 0);
            this.panel79.Name = "panel79";
            this.panel79.Size = new System.Drawing.Size(197, 80);
            this.panel79.TabIndex = 0;
            // 
            // labelGraphBrady
            // 
            this.labelGraphBrady.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelGraphBrady.Font = new System.Drawing.Font("Verdana", 16F, System.Drawing.FontStyle.Bold);
            this.labelGraphBrady.Location = new System.Drawing.Point(0, 45);
            this.labelGraphBrady.Name = "labelGraphBrady";
            this.labelGraphBrady.Size = new System.Drawing.Size(197, 30);
            this.labelGraphBrady.TabIndex = 2;
            this.labelGraphBrady.Text = "^99";
            this.labelGraphBrady.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label32
            // 
            this.label32.Dock = System.Windows.Forms.DockStyle.Top;
            this.label32.Font = new System.Drawing.Font("Verdana", 28F, System.Drawing.FontStyle.Bold);
            this.label32.Location = new System.Drawing.Point(0, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(197, 45);
            this.label32.TabIndex = 0;
            this.label32.Text = "Brady";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel80
            // 
            this.panel80.Controls.Add(this.chartTachy);
            this.panel80.Controls.Add(this.panel81);
            this.panel80.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel80.Location = new System.Drawing.Point(0, 10);
            this.panel80.Name = "panel80";
            this.panel80.Size = new System.Drawing.Size(2336, 80);
            this.panel80.TabIndex = 3;
            // 
            // chartTachy
            // 
            chartArea14.AxisX.IsLabelAutoFit = false;
            chartArea14.AxisX.LabelStyle.Enabled = false;
            chartArea14.AxisX.LineWidth = 3;
            chartArea14.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea14.AxisX.ScaleBreakStyle.BreakLineStyle = System.Windows.Forms.DataVisualization.Charting.BreakLineStyle.None;
            chartArea14.AxisX.ScaleBreakStyle.LineColor = System.Drawing.Color.White;
            chartArea14.AxisX.ScaleBreakStyle.Spacing = 1D;
            chartArea14.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea14.AxisY.IsLabelAutoFit = false;
            chartArea14.AxisY.LabelStyle.Enabled = false;
            chartArea14.AxisY.LineColor = System.Drawing.Color.DarkGray;
            chartArea14.AxisY.MajorGrid.Enabled = false;
            chartArea14.AxisY.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea14.AxisY.MajorTickMark.Enabled = false;
            chartArea14.AxisY.Maximum = 1D;
            chartArea14.AxisY.Minimum = 0D;
            chartArea14.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea14.BorderColor = System.Drawing.Color.DarkGray;
            chartArea14.InnerPlotPosition.Auto = false;
            chartArea14.InnerPlotPosition.Height = 80F;
            chartArea14.InnerPlotPosition.Width = 95F;
            chartArea14.InnerPlotPosition.X = 1F;
            chartArea14.InnerPlotPosition.Y = 1F;
            chartArea14.Name = "TachyEvents";
            this.chartTachy.ChartAreas.Add(chartArea14);
            this.chartTachy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartTachy.Location = new System.Drawing.Point(197, 0);
            this.chartTachy.Name = "chartTachy";
            series14.BorderWidth = 2;
            series14.ChartArea = "TachyEvents";
            series14.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series14.Color = System.Drawing.Color.Black;
            series14.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series14.IsVisibleInLegend = false;
            series14.MarkerSize = 3;
            series14.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series14.Name = "Series1";
            series14.Points.Add(dataPoint62);
            series14.Points.Add(dataPoint63);
            series14.Points.Add(dataPoint64);
            series14.Points.Add(dataPoint65);
            series14.SmartLabelStyle.Enabled = false;
            series14.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            this.chartTachy.Series.Add(series14);
            this.chartTachy.Size = new System.Drawing.Size(2139, 80);
            this.chartTachy.TabIndex = 1;
            this.chartTachy.Text = "chart2";
            // 
            // panel81
            // 
            this.panel81.Controls.Add(this.labelGraphTachy);
            this.panel81.Controls.Add(this.label53);
            this.panel81.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel81.Location = new System.Drawing.Point(0, 0);
            this.panel81.Name = "panel81";
            this.panel81.Size = new System.Drawing.Size(197, 80);
            this.panel81.TabIndex = 0;
            // 
            // labelGraphTachy
            // 
            this.labelGraphTachy.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelGraphTachy.Font = new System.Drawing.Font("Verdana", 16F, System.Drawing.FontStyle.Bold);
            this.labelGraphTachy.Location = new System.Drawing.Point(0, 45);
            this.labelGraphTachy.Name = "labelGraphTachy";
            this.labelGraphTachy.Size = new System.Drawing.Size(197, 30);
            this.labelGraphTachy.TabIndex = 1;
            this.labelGraphTachy.Text = "^99";
            this.labelGraphTachy.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label53
            // 
            this.label53.Dock = System.Windows.Forms.DockStyle.Top;
            this.label53.Font = new System.Drawing.Font("Verdana", 28F, System.Drawing.FontStyle.Bold);
            this.label53.Location = new System.Drawing.Point(0, 0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(197, 45);
            this.label53.TabIndex = 0;
            this.label53.Text = "Tachy";
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel82
            // 
            this.panel82.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel82.Location = new System.Drawing.Point(0, 0);
            this.panel82.Name = "panel82";
            this.panel82.Size = new System.Drawing.Size(2336, 10);
            this.panel82.TabIndex = 0;
            // 
            // panel64
            // 
            this.panel64.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel64.Location = new System.Drawing.Point(0, 358);
            this.panel64.Name = "panel64";
            this.panel64.Size = new System.Drawing.Size(2338, 10);
            this.panel64.TabIndex = 13;
            // 
            // panel62
            // 
            this.panel62.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel62.Controls.Add(this.chartHR);
            this.panel62.Controls.Add(this.panel5);
            this.panel62.Controls.Add(this.panel63);
            this.panel62.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel62.Location = new System.Drawing.Point(0, 53);
            this.panel62.Name = "panel62";
            this.panel62.Size = new System.Drawing.Size(2338, 305);
            this.panel62.TabIndex = 12;
            // 
            // chartHR
            // 
            this.chartHR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.chartHR.BorderlineColor = System.Drawing.Color.Black;
            this.chartHR.BorderlineWidth = 0;
            chartArea15.AxisX.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.True;
            chartArea15.AxisX.LabelStyle.Font = new System.Drawing.Font("Verdana", 23F);
            chartArea15.AxisX.LineColor = System.Drawing.Color.DarkGray;
            chartArea15.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea15.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DarkGray;
            chartArea15.AxisX.Maximum = 250D;
            chartArea15.AxisX.MaximumAutoSize = 50F;
            chartArea15.AxisX.Minimum = 100D;
            chartArea15.AxisX.MinorTickMark.Enabled = true;
            chartArea15.AxisX2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea15.AxisY.IsLabelAutoFit = false;
            chartArea15.AxisY.LabelStyle.Font = new System.Drawing.Font("Verdana", 23F);
            chartArea15.AxisY.LineColor = System.Drawing.Color.DarkGray;
            chartArea15.AxisY.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea15.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DarkGray;
            customLabel1.Text = "          ";
            chartArea15.AxisY2.CustomLabels.Add(customLabel1);
            chartArea15.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.True;
            chartArea15.AxisY2.IntervalOffsetType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea15.AxisY2.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea15.AxisY2.MajorGrid.Enabled = false;
            chartArea15.AxisY2.MajorTickMark.Enabled = false;
            chartArea15.AxisY2.Maximum = 10D;
            chartArea15.AxisY2.MaximumAutoSize = 50F;
            chartArea15.AxisY2.Minimum = 0D;
            chartArea15.BorderColor = System.Drawing.Color.Transparent;
            chartArea15.BorderWidth = 0;
            chartArea15.Name = "ChartArea1";
            chartArea15.Position.Auto = false;
            chartArea15.Position.Height = 95F;
            chartArea15.Position.Width = 98F;
            chartArea15.Position.Y = 1F;
            this.chartHR.ChartAreas.Add(chartArea15);
            this.chartHR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartHR.Location = new System.Drawing.Point(146, 0);
            this.chartHR.Name = "chartHR";
            series15.BorderWidth = 2;
            series15.ChartArea = "ChartArea1";
            series15.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series15.Color = System.Drawing.Color.Silver;
            series15.Name = "HrMin";
            series15.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series16.BorderWidth = 2;
            series16.ChartArea = "ChartArea1";
            series16.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series16.Color = System.Drawing.Color.Gray;
            series16.Name = "HrMax";
            series16.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series17.BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.DiagonalLeft;
            series17.BackHatchStyle = System.Windows.Forms.DataVisualization.Charting.ChartHatchStyle.Cross;
            series17.BorderWidth = 3;
            series17.ChartArea = "ChartArea1";
            series17.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series17.Color = System.Drawing.Color.Black;
            series17.LegendText = "HR";
            series17.MarkerSize = 3;
            series17.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series17.Name = "Heart Rate";
            series17.Points.Add(dataPoint66);
            series17.Points.Add(dataPoint67);
            series17.Points.Add(dataPoint68);
            series17.Points.Add(dataPoint69);
            series17.Points.Add(dataPoint70);
            series17.Points.Add(dataPoint71);
            series17.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            this.chartHR.Series.Add(series15);
            this.chartHR.Series.Add(series16);
            this.chartHR.Series.Add(series17);
            this.chartHR.Size = new System.Drawing.Size(2127, 303);
            this.chartHR.TabIndex = 0;
            this.chartHR.Text = "chart1";
            // 
            // panel5
            // 
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(2273, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(63, 303);
            this.panel5.TabIndex = 4;
            // 
            // panel63
            // 
            this.panel63.Controls.Add(this.label21);
            this.panel63.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel63.Location = new System.Drawing.Point(0, 0);
            this.panel63.Name = "panel63";
            this.panel63.Size = new System.Drawing.Size(146, 303);
            this.panel63.TabIndex = 1;
            // 
            // label21
            // 
            this.label21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label21.Font = new System.Drawing.Font("Verdana", 28F, System.Drawing.FontStyle.Bold);
            this.label21.Location = new System.Drawing.Point(0, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(146, 303);
            this.label21.TabIndex = 1;
            this.label21.Text = "HR";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label21.Click += new System.EventHandler(this.label21_Click);
            // 
            // panel40
            // 
            this.panel40.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel40.Controls.Add(this.labelTotalBeats);
            this.panel40.Controls.Add(this.label37);
            this.panel40.Controls.Add(this.labelDaysHours);
            this.panel40.Controls.Add(this.label33);
            this.panel40.Controls.Add(this.labelTrendTo);
            this.panel40.Controls.Add(this.label40);
            this.panel40.Controls.Add(this.labelFirstDate);
            this.panel40.Controls.Add(this.label31);
            this.panel40.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel40.Location = new System.Drawing.Point(0, 0);
            this.panel40.Name = "panel40";
            this.panel40.Size = new System.Drawing.Size(2338, 53);
            this.panel40.TabIndex = 10;
            // 
            // labelTotalBeats
            // 
            this.labelTotalBeats.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelTotalBeats.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalBeats.Location = new System.Drawing.Point(2567, 0);
            this.labelTotalBeats.Name = "labelTotalBeats";
            this.labelTotalBeats.Size = new System.Drawing.Size(75, 51);
            this.labelTotalBeats.TabIndex = 8;
            this.labelTotalBeats.Text = "not used 112121";
            this.labelTotalBeats.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelTotalBeats.Visible = false;
            // 
            // label37
            // 
            this.label37.Dock = System.Windows.Forms.DockStyle.Left;
            this.label37.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(2470, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(97, 51);
            this.label37.TabIndex = 7;
            this.label37.Text = "Beats:";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label37.Visible = false;
            // 
            // labelDaysHours
            // 
            this.labelDaysHours.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelDaysHours.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDaysHours.Location = new System.Drawing.Point(1953, 0);
            this.labelDaysHours.Name = "labelDaysHours";
            this.labelDaysHours.Size = new System.Drawing.Size(517, 51);
            this.labelDaysHours.TabIndex = 4;
            this.labelDaysHours.Text = "^99d-99h-99m";
            this.labelDaysHours.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label33
            // 
            this.label33.Dock = System.Windows.Forms.DockStyle.Left;
            this.label33.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(1671, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(282, 51);
            this.label33.TabIndex = 3;
            this.label33.Text = "Trend time:";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelTrendTo
            // 
            this.labelTrendTo.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelTrendTo.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTrendTo.Location = new System.Drawing.Point(1112, 0);
            this.labelTrendTo.Name = "labelTrendTo";
            this.labelTrendTo.Size = new System.Drawing.Size(559, 51);
            this.labelTrendTo.TabIndex = 10;
            this.labelTrendTo.Text = "^9/99/9999 - 99:99:99 AM ";
            this.labelTrendTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Dock = System.Windows.Forms.DockStyle.Left;
            this.label40.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(1027, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(85, 45);
            this.label40.TabIndex = 9;
            this.label40.Text = "To:";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelFirstDate
            // 
            this.labelFirstDate.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelFirstDate.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFirstDate.Location = new System.Drawing.Point(279, 0);
            this.labelFirstDate.Name = "labelFirstDate";
            this.labelFirstDate.Size = new System.Drawing.Size(748, 51);
            this.labelFirstDate.TabIndex = 2;
            this.labelFirstDate.Text = "^9/99/9999 - 99:99:99 AM +9999 ";
            this.labelFirstDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label31
            // 
            this.label31.Dock = System.Windows.Forms.DockStyle.Left;
            this.label31.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(0, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(279, 51);
            this.label31.TabIndex = 1;
            this.label31.Text = "Trend from:";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel101
            // 
            this.panel101.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel101.Location = new System.Drawing.Point(0, 900);
            this.panel101.Name = "panel101";
            this.panel101.Size = new System.Drawing.Size(2338, 10);
            this.panel101.TabIndex = 50;
            // 
            // panelDiagnosis
            // 
            this.panelDiagnosis.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelDiagnosis.Controls.Add(this.panel6);
            this.panelDiagnosis.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelDiagnosis.Location = new System.Drawing.Point(0, 754);
            this.panelDiagnosis.Name = "panelDiagnosis";
            this.panelDiagnosis.Size = new System.Drawing.Size(2338, 146);
            this.panelDiagnosis.TabIndex = 49;
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.panel15);
            this.panel6.Controls.Add(this.panel16);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(2336, 144);
            this.panel6.TabIndex = 2;
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.labelDiagnosis);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel15.Location = new System.Drawing.Point(278, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(850, 142);
            this.panel15.TabIndex = 1;
            // 
            // labelDiagnosis
            // 
            this.labelDiagnosis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDiagnosis.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDiagnosis.Location = new System.Drawing.Point(0, 0);
            this.labelDiagnosis.Name = "labelDiagnosis";
            this.labelDiagnosis.Size = new System.Drawing.Size(850, 142);
            this.labelDiagnosis.TabIndex = 4;
            this.labelDiagnosis.Text = "^<code>   Acute combined systolic (congestive) and diastolic (congestive) heart f" +
    "ailure";
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.label63);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel16.Location = new System.Drawing.Point(0, 0);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(278, 142);
            this.panel16.TabIndex = 0;
            // 
            // label63
            // 
            this.label63.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label63.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.Location = new System.Drawing.Point(0, 0);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(278, 142);
            this.label63.TabIndex = 1;
            this.label63.Text = "Indication:";
            // 
            // panel158
            // 
            this.panel158.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel158.Location = new System.Drawing.Point(0, 744);
            this.panel158.Name = "panel158";
            this.panel158.Size = new System.Drawing.Size(2338, 10);
            this.panel158.TabIndex = 48;
            // 
            // panel67
            // 
            this.panel67.Controls.Add(this.panel258);
            this.panel67.Controls.Add(this.panel256);
            this.panel67.Controls.Add(this.panel1);
            this.panel67.Controls.Add(this.panelVert9);
            this.panel67.Controls.Add(this.panel252);
            this.panel67.Controls.Add(this.panel250);
            this.panel67.Controls.Add(this.panelVert8);
            this.panel67.Controls.Add(this.panel244);
            this.panel67.Controls.Add(this.panel242);
            this.panel67.Controls.Add(this.panelVert6);
            this.panel67.Controls.Add(this.panel32);
            this.panel67.Controls.Add(this.panel41);
            this.panel67.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel67.Location = new System.Drawing.Point(0, 598);
            this.panel67.Name = "panel67";
            this.panel67.Size = new System.Drawing.Size(2338, 146);
            this.panel67.TabIndex = 47;
            // 
            // panel258
            // 
            this.panel258.Controls.Add(this.labelClientName);
            this.panel258.Controls.Add(this.labelClientTel);
            this.panel258.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel258.Location = new System.Drawing.Point(1810, 1);
            this.panel258.Name = "panel258";
            this.panel258.Size = new System.Drawing.Size(494, 145);
            this.panel258.TabIndex = 20;
            // 
            // labelClientName
            // 
            this.labelClientName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelClientName.Font = new System.Drawing.Font("Verdana", 27F, System.Drawing.FontStyle.Bold);
            this.labelClientName.Location = new System.Drawing.Point(0, 0);
            this.labelClientName.Name = "labelClientName";
            this.labelClientName.Size = new System.Drawing.Size(494, 98);
            this.labelClientName.TabIndex = 32;
            this.labelClientName.Text = "^CLIENT NAME";
            // 
            // labelClientTel
            // 
            this.labelClientTel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelClientTel.Font = new System.Drawing.Font("Verdana", 27F, System.Drawing.FontStyle.Bold);
            this.labelClientTel.Location = new System.Drawing.Point(0, 98);
            this.labelClientTel.Name = "labelClientTel";
            this.labelClientTel.Size = new System.Drawing.Size(494, 47);
            this.labelClientTel.TabIndex = 42;
            this.labelClientTel.Text = "^99-999-9999";
            this.labelClientTel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel256
            // 
            this.panel256.Controls.Add(this.label45);
            this.panel256.Controls.Add(this.label46);
            this.panel256.Controls.Add(this.labelClientHeader);
            this.panel256.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel256.Location = new System.Drawing.Point(1698, 1);
            this.panel256.Name = "panel256";
            this.panel256.Size = new System.Drawing.Size(112, 145);
            this.panel256.TabIndex = 18;
            // 
            // label45
            // 
            this.label45.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label45.Font = new System.Drawing.Font("Verdana", 20F);
            this.label45.Location = new System.Drawing.Point(0, 98);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(112, 47);
            this.label45.TabIndex = 42;
            this.label45.Text = "Phone:";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label46
            // 
            this.label46.Dock = System.Windows.Forms.DockStyle.Top;
            this.label46.Font = new System.Drawing.Font("Verdana", 27F);
            this.label46.Location = new System.Drawing.Point(0, 47);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(112, 47);
            this.label46.TabIndex = 40;
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelClientHeader
            // 
            this.labelClientHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelClientHeader.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelClientHeader.Location = new System.Drawing.Point(0, 0);
            this.labelClientHeader.Name = "labelClientHeader";
            this.labelClientHeader.Size = new System.Drawing.Size(112, 47);
            this.labelClientHeader.TabIndex = 20;
            this.labelClientHeader.Text = "Name:";
            this.labelClientHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel1
            // 
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.panel31);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(1578, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(120, 145);
            this.panel1.TabIndex = 26;
            // 
            // panel31
            // 
            this.panel31.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel31.BackgroundImage")));
            this.panel31.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel31.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel31.Location = new System.Drawing.Point(0, 0);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(120, 120);
            this.panel31.TabIndex = 27;
            // 
            // panelVert9
            // 
            this.panelVert9.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelVert9.Location = new System.Drawing.Point(1576, 1);
            this.panelVert9.Name = "panelVert9";
            this.panelVert9.Size = new System.Drawing.Size(2, 145);
            this.panelVert9.TabIndex = 15;
            // 
            // panel252
            // 
            this.panel252.Controls.Add(this.labelRefPhysicianName);
            this.panel252.Controls.Add(this.labelRefPhysicianTel);
            this.panel252.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel252.Location = new System.Drawing.Point(993, 1);
            this.panel252.Name = "panel252";
            this.panel252.Size = new System.Drawing.Size(583, 145);
            this.panel252.TabIndex = 14;
            // 
            // labelRefPhysicianName
            // 
            this.labelRefPhysicianName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelRefPhysicianName.Font = new System.Drawing.Font("Verdana", 27F, System.Drawing.FontStyle.Bold);
            this.labelRefPhysicianName.Location = new System.Drawing.Point(0, 0);
            this.labelRefPhysicianName.Name = "labelRefPhysicianName";
            this.labelRefPhysicianName.Size = new System.Drawing.Size(583, 90);
            this.labelRefPhysicianName.TabIndex = 29;
            this.labelRefPhysicianName.Text = "^REF PHYSICIAN LAST NAME";
            // 
            // labelRefPhysicianTel
            // 
            this.labelRefPhysicianTel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelRefPhysicianTel.Font = new System.Drawing.Font("Verdana", 27F, System.Drawing.FontStyle.Bold);
            this.labelRefPhysicianTel.Location = new System.Drawing.Point(0, 90);
            this.labelRefPhysicianTel.Name = "labelRefPhysicianTel";
            this.labelRefPhysicianTel.Size = new System.Drawing.Size(583, 55);
            this.labelRefPhysicianTel.TabIndex = 41;
            this.labelRefPhysicianTel.Text = "^99-999-9999";
            this.labelRefPhysicianTel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel250
            // 
            this.panel250.Controls.Add(this.label47);
            this.panel250.Controls.Add(this.label52);
            this.panel250.Controls.Add(this.labelRefPhysHeader);
            this.panel250.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel250.Location = new System.Drawing.Point(839, 1);
            this.panel250.Name = "panel250";
            this.panel250.Size = new System.Drawing.Size(154, 145);
            this.panel250.TabIndex = 12;
            // 
            // label47
            // 
            this.label47.Dock = System.Windows.Forms.DockStyle.Top;
            this.label47.Font = new System.Drawing.Font("Verdana", 20F);
            this.label47.Location = new System.Drawing.Point(0, 94);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(154, 47);
            this.label47.TabIndex = 41;
            this.label47.Text = "Phone:";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label52
            // 
            this.label52.Dock = System.Windows.Forms.DockStyle.Top;
            this.label52.Font = new System.Drawing.Font("Verdana", 27F);
            this.label52.Location = new System.Drawing.Point(0, 47);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(154, 47);
            this.label52.TabIndex = 39;
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelRefPhysHeader
            // 
            this.labelRefPhysHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelRefPhysHeader.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelRefPhysHeader.Location = new System.Drawing.Point(0, 0);
            this.labelRefPhysHeader.Name = "labelRefPhysHeader";
            this.labelRefPhysHeader.Size = new System.Drawing.Size(154, 47);
            this.labelRefPhysHeader.TabIndex = 19;
            this.labelRefPhysHeader.Text = "Ref Phys.:";
            this.labelRefPhysHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panelVert8
            // 
            this.panelVert8.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelVert8.Location = new System.Drawing.Point(837, 1);
            this.panelVert8.Name = "panelVert8";
            this.panelVert8.Size = new System.Drawing.Size(2, 145);
            this.panelVert8.TabIndex = 11;
            // 
            // panel244
            // 
            this.panel244.Controls.Add(this.labelPhysicianName);
            this.panel244.Controls.Add(this.labelPhysicianTel);
            this.panel244.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel244.Location = new System.Drawing.Point(239, 1);
            this.panel244.Name = "panel244";
            this.panel244.Size = new System.Drawing.Size(598, 145);
            this.panel244.TabIndex = 10;
            // 
            // labelPhysicianName
            // 
            this.labelPhysicianName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPhysicianName.Font = new System.Drawing.Font("Verdana", 27F, System.Drawing.FontStyle.Bold);
            this.labelPhysicianName.Location = new System.Drawing.Point(0, 0);
            this.labelPhysicianName.Name = "labelPhysicianName";
            this.labelPhysicianName.Size = new System.Drawing.Size(598, 98);
            this.labelPhysicianName.TabIndex = 25;
            this.labelPhysicianName.Text = "^PHYSICIAN LAST NAME";
            // 
            // labelPhysicianTel
            // 
            this.labelPhysicianTel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelPhysicianTel.Font = new System.Drawing.Font("Verdana", 27F, System.Drawing.FontStyle.Bold);
            this.labelPhysicianTel.Location = new System.Drawing.Point(0, 98);
            this.labelPhysicianTel.Name = "labelPhysicianTel";
            this.labelPhysicianTel.Size = new System.Drawing.Size(598, 47);
            this.labelPhysicianTel.TabIndex = 40;
            this.labelPhysicianTel.Text = "^99-999-9999";
            this.labelPhysicianTel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel242
            // 
            this.panel242.Controls.Add(this.label51);
            this.panel242.Controls.Add(this.label55);
            this.panel242.Controls.Add(this.labelPhysHeader);
            this.panel242.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel242.Location = new System.Drawing.Point(122, 1);
            this.panel242.Name = "panel242";
            this.panel242.Size = new System.Drawing.Size(117, 145);
            this.panel242.TabIndex = 8;
            // 
            // label51
            // 
            this.label51.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label51.Font = new System.Drawing.Font("Verdana", 20F);
            this.label51.Location = new System.Drawing.Point(0, 98);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(117, 47);
            this.label51.TabIndex = 40;
            this.label51.Text = "Phone:";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label55
            // 
            this.label55.Dock = System.Windows.Forms.DockStyle.Top;
            this.label55.Font = new System.Drawing.Font("Verdana", 27F);
            this.label55.Location = new System.Drawing.Point(0, 47);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(117, 47);
            this.label55.TabIndex = 38;
            this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPhysHeader
            // 
            this.labelPhysHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPhysHeader.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelPhysHeader.Location = new System.Drawing.Point(0, 0);
            this.labelPhysHeader.Name = "labelPhysHeader";
            this.labelPhysHeader.Size = new System.Drawing.Size(117, 47);
            this.labelPhysHeader.TabIndex = 17;
            this.labelPhysHeader.Text = "Phys.:";
            this.labelPhysHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panelVert6
            // 
            this.panelVert6.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelVert6.Location = new System.Drawing.Point(120, 1);
            this.panelVert6.Name = "panelVert6";
            this.panelVert6.Size = new System.Drawing.Size(2, 145);
            this.panelVert6.TabIndex = 4;
            // 
            // panel32
            // 
            this.panel32.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel32.Controls.Add(this.panel36);
            this.panel32.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel32.Location = new System.Drawing.Point(0, 1);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(120, 145);
            this.panel32.TabIndex = 25;
            // 
            // panel36
            // 
            this.panel36.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel36.BackgroundImage")));
            this.panel36.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel36.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel36.Location = new System.Drawing.Point(0, 0);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(120, 120);
            this.panel36.TabIndex = 26;
            // 
            // panel41
            // 
            this.panel41.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel41.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel41.Location = new System.Drawing.Point(0, 0);
            this.panel41.Name = "panel41";
            this.panel41.Size = new System.Drawing.Size(2338, 1);
            this.panel41.TabIndex = 27;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.panel8);
            this.panel7.Controls.Add(this.panel10);
            this.panel7.Controls.Add(this.panel11);
            this.panel7.Controls.Add(this.panel34);
            this.panel7.Controls.Add(this.panel12);
            this.panel7.Controls.Add(this.panel18);
            this.panel7.Controls.Add(this.panel30);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 343);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(2338, 255);
            this.panel7.TabIndex = 46;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.labelEndStudyDate);
            this.panel8.Controls.Add(this.labelPatID);
            this.panel8.Controls.Add(this.labelSerialNr);
            this.panel8.Controls.Add(this.labelStartDate);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(2002, 2);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(336, 253);
            this.panel8.TabIndex = 6;
            // 
            // labelEndStudyDate
            // 
            this.labelEndStudyDate.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEndStudyDate.Font = new System.Drawing.Font("Verdana", 27F, System.Drawing.FontStyle.Bold);
            this.labelEndStudyDate.Location = new System.Drawing.Point(0, 188);
            this.labelEndStudyDate.Name = "labelEndStudyDate";
            this.labelEndStudyDate.Size = new System.Drawing.Size(336, 47);
            this.labelEndStudyDate.TabIndex = 47;
            this.labelEndStudyDate.Text = "^9/99/9999";
            this.labelEndStudyDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelEndStudyDate.Visible = false;
            // 
            // labelPatID
            // 
            this.labelPatID.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPatID.Font = new System.Drawing.Font("Verdana", 27F, System.Drawing.FontStyle.Bold);
            this.labelPatID.Location = new System.Drawing.Point(0, 94);
            this.labelPatID.Name = "labelPatID";
            this.labelPatID.Size = new System.Drawing.Size(336, 94);
            this.labelPatID.TabIndex = 46;
            this.labelPatID.Text = "^9999999999";
            // 
            // labelSerialNr
            // 
            this.labelSerialNr.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelSerialNr.Font = new System.Drawing.Font("Verdana", 27F, System.Drawing.FontStyle.Bold);
            this.labelSerialNr.Location = new System.Drawing.Point(0, 47);
            this.labelSerialNr.Name = "labelSerialNr";
            this.labelSerialNr.Size = new System.Drawing.Size(336, 47);
            this.labelSerialNr.TabIndex = 41;
            this.labelSerialNr.Text = "^AAAAAAAAAA";
            this.labelSerialNr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStartDate
            // 
            this.labelStartDate.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStartDate.Font = new System.Drawing.Font("Verdana", 27F, System.Drawing.FontStyle.Bold);
            this.labelStartDate.Location = new System.Drawing.Point(0, 0);
            this.labelStartDate.Name = "labelStartDate";
            this.labelStartDate.Size = new System.Drawing.Size(336, 47);
            this.labelStartDate.TabIndex = 40;
            this.labelStartDate.Text = "^9/99/9999";
            this.labelStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.labelEndStudyText);
            this.panel10.Controls.Add(this.labelPatIDHeader);
            this.panel10.Controls.Add(this.labelSerialNrHeader);
            this.panel10.Controls.Add(this.labelStartDateHeader);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel10.Location = new System.Drawing.Point(1853, 2);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(149, 253);
            this.panel10.TabIndex = 5;
            // 
            // labelEndStudyText
            // 
            this.labelEndStudyText.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEndStudyText.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelEndStudyText.Location = new System.Drawing.Point(0, 188);
            this.labelEndStudyText.Name = "labelEndStudyText";
            this.labelEndStudyText.Size = new System.Drawing.Size(149, 47);
            this.labelEndStudyText.TabIndex = 42;
            this.labelEndStudyText.Text = "Ended:";
            this.labelEndStudyText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelEndStudyText.Visible = false;
            // 
            // labelPatIDHeader
            // 
            this.labelPatIDHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPatIDHeader.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelPatIDHeader.Location = new System.Drawing.Point(0, 94);
            this.labelPatIDHeader.Name = "labelPatIDHeader";
            this.labelPatIDHeader.Size = new System.Drawing.Size(149, 94);
            this.labelPatIDHeader.TabIndex = 41;
            this.labelPatIDHeader.Text = "Patient ID:";
            this.labelPatIDHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSerialNrHeader
            // 
            this.labelSerialNrHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelSerialNrHeader.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelSerialNrHeader.Location = new System.Drawing.Point(0, 47);
            this.labelSerialNrHeader.Name = "labelSerialNrHeader";
            this.labelSerialNrHeader.Size = new System.Drawing.Size(149, 47);
            this.labelSerialNrHeader.TabIndex = 36;
            this.labelSerialNrHeader.Text = "Serial nr:";
            this.labelSerialNrHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStartDateHeader
            // 
            this.labelStartDateHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStartDateHeader.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelStartDateHeader.Location = new System.Drawing.Point(0, 0);
            this.labelStartDateHeader.Name = "labelStartDateHeader";
            this.labelStartDateHeader.Size = new System.Drawing.Size(149, 47);
            this.labelStartDateHeader.TabIndex = 35;
            this.labelStartDateHeader.Text = "Enrolled:";
            this.labelStartDateHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel11
            // 
            this.panel11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel11.Controls.Add(this.panel24);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel11.Location = new System.Drawing.Point(1733, 2);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(120, 253);
            this.panel11.TabIndex = 4;
            // 
            // panel24
            // 
            this.panel24.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel24.BackgroundImage")));
            this.panel24.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel24.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel24.Location = new System.Drawing.Point(0, 0);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(120, 120);
            this.panel24.TabIndex = 5;
            // 
            // panel34
            // 
            this.panel34.Controls.Add(this.labelPhone2);
            this.panel34.Controls.Add(this.labelPhone1);
            this.panel34.Controls.Add(this.labelCountry);
            this.panel34.Controls.Add(this.labelZipCity);
            this.panel34.Controls.Add(this.labelAddress);
            this.panel34.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel34.Location = new System.Drawing.Point(993, 2);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(740, 253);
            this.panel34.TabIndex = 3;
            // 
            // labelPhone2
            // 
            this.labelPhone2.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPhone2.Font = new System.Drawing.Font("Verdana", 27F, System.Drawing.FontStyle.Bold);
            this.labelPhone2.Location = new System.Drawing.Point(0, 188);
            this.labelPhone2.Name = "labelPhone2";
            this.labelPhone2.Size = new System.Drawing.Size(740, 47);
            this.labelPhone2.TabIndex = 48;
            this.labelPhone2.Text = "800-217-0520";
            this.labelPhone2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelPhone1
            // 
            this.labelPhone1.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPhone1.Font = new System.Drawing.Font("Verdana", 27F, System.Drawing.FontStyle.Bold);
            this.labelPhone1.Location = new System.Drawing.Point(0, 141);
            this.labelPhone1.Name = "labelPhone1";
            this.labelPhone1.Size = new System.Drawing.Size(740, 47);
            this.labelPhone1.TabIndex = 47;
            this.labelPhone1.Text = "800-217-0520";
            this.labelPhone1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelCountry
            // 
            this.labelCountry.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelCountry.Font = new System.Drawing.Font("Verdana", 27F, System.Drawing.FontStyle.Bold);
            this.labelCountry.Location = new System.Drawing.Point(0, 94);
            this.labelCountry.Name = "labelCountry";
            this.labelCountry.Size = new System.Drawing.Size(740, 47);
            this.labelCountry.TabIndex = 46;
            this.labelCountry.Text = "^Country";
            this.labelCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelZipCity
            // 
            this.labelZipCity.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelZipCity.Font = new System.Drawing.Font("Verdana", 27F, System.Drawing.FontStyle.Bold);
            this.labelZipCity.Location = new System.Drawing.Point(0, 47);
            this.labelZipCity.Name = "labelZipCity";
            this.labelZipCity.Size = new System.Drawing.Size(740, 47);
            this.labelZipCity.TabIndex = 45;
            this.labelZipCity.Text = "^Texas TX 200111";
            this.labelZipCity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelAddress
            // 
            this.labelAddress.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelAddress.Font = new System.Drawing.Font("Verdana", 27F, System.Drawing.FontStyle.Bold);
            this.labelAddress.Location = new System.Drawing.Point(0, 0);
            this.labelAddress.Name = "labelAddress";
            this.labelAddress.Size = new System.Drawing.Size(740, 47);
            this.labelAddress.TabIndex = 44;
            this.labelAddress.Text = "^112 Alpha Street App 1000 abcde";
            this.labelAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.label4);
            this.panel12.Controls.Add(this.label14);
            this.panel12.Controls.Add(this.label35);
            this.panel12.Controls.Add(this.labelAddressHeader);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel12.Location = new System.Drawing.Point(837, 2);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(156, 253);
            this.panel12.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Font = new System.Drawing.Font("Verdana", 20F);
            this.label4.Location = new System.Drawing.Point(0, 141);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(156, 47);
            this.label4.TabIndex = 39;
            this.label4.Text = "Phone:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label14
            // 
            this.label14.Dock = System.Windows.Forms.DockStyle.Top;
            this.label14.Font = new System.Drawing.Font("Verdana", 27F);
            this.label14.Location = new System.Drawing.Point(0, 94);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(156, 47);
            this.label14.TabIndex = 38;
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label35
            // 
            this.label35.Dock = System.Windows.Forms.DockStyle.Top;
            this.label35.Font = new System.Drawing.Font("Verdana", 27F);
            this.label35.Location = new System.Drawing.Point(0, 47);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(156, 47);
            this.label35.TabIndex = 37;
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelAddressHeader
            // 
            this.labelAddressHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelAddressHeader.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelAddressHeader.Location = new System.Drawing.Point(0, 0);
            this.labelAddressHeader.Name = "labelAddressHeader";
            this.labelAddressHeader.Size = new System.Drawing.Size(156, 47);
            this.labelAddressHeader.TabIndex = 36;
            this.labelAddressHeader.Text = "Address:";
            this.labelAddressHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.panel27);
            this.panel18.Controls.Add(this.panel22);
            this.panel18.Controls.Add(this.panel25);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel18.Location = new System.Drawing.Point(0, 2);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(837, 253);
            this.panel18.TabIndex = 1;
            // 
            // panel27
            // 
            this.panel27.Controls.Add(this.labelPatientLastName);
            this.panel27.Controls.Add(this.labelPatientFirstName);
            this.panel27.Controls.Add(this.labelDateOfBirth);
            this.panel27.Controls.Add(this.labelAgeGender);
            this.panel27.Controls.Add(this.panel35);
            this.panel27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel27.Location = new System.Drawing.Point(239, 0);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(598, 253);
            this.panel27.TabIndex = 37;
            // 
            // labelPatientLastName
            // 
            this.labelPatientLastName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPatientLastName.Font = new System.Drawing.Font("Verdana", 27F, System.Drawing.FontStyle.Bold);
            this.labelPatientLastName.Location = new System.Drawing.Point(0, 0);
            this.labelPatientLastName.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.labelPatientLastName.Name = "labelPatientLastName";
            this.labelPatientLastName.Size = new System.Drawing.Size(598, 92);
            this.labelPatientLastName.TabIndex = 41;
            this.labelPatientLastName.Text = "^LAST NAME";
            this.labelPatientLastName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelPatientFirstName
            // 
            this.labelPatientFirstName.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelPatientFirstName.Font = new System.Drawing.Font("Verdana", 27F, System.Drawing.FontStyle.Bold);
            this.labelPatientFirstName.Location = new System.Drawing.Point(0, 92);
            this.labelPatientFirstName.Name = "labelPatientFirstName";
            this.labelPatientFirstName.Size = new System.Drawing.Size(598, 47);
            this.labelPatientFirstName.TabIndex = 42;
            this.labelPatientFirstName.Text = "^FIRST AND MIDDLE NAME";
            this.labelPatientFirstName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelDateOfBirth
            // 
            this.labelDateOfBirth.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelDateOfBirth.Font = new System.Drawing.Font("Verdana", 27F, System.Drawing.FontStyle.Bold);
            this.labelDateOfBirth.Location = new System.Drawing.Point(0, 139);
            this.labelDateOfBirth.Name = "labelDateOfBirth";
            this.labelDateOfBirth.Size = new System.Drawing.Size(598, 47);
            this.labelDateOfBirth.TabIndex = 43;
            this.labelDateOfBirth.Text = "^9/99/9999";
            this.labelDateOfBirth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelAgeGender
            // 
            this.labelAgeGender.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelAgeGender.Font = new System.Drawing.Font("Verdana", 27F, System.Drawing.FontStyle.Bold);
            this.labelAgeGender.Location = new System.Drawing.Point(0, 186);
            this.labelAgeGender.Name = "labelAgeGender";
            this.labelAgeGender.Size = new System.Drawing.Size(598, 47);
            this.labelAgeGender.TabIndex = 44;
            this.labelAgeGender.Text = "^AGE, GENDER";
            this.labelAgeGender.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel35
            // 
            this.panel35.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel35.Location = new System.Drawing.Point(0, 233);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(598, 20);
            this.panel35.TabIndex = 46;
            // 
            // panel22
            // 
            this.panel22.Controls.Add(this.label41);
            this.panel22.Controls.Add(this.labelDOBheader);
            this.panel22.Controls.Add(this.label42);
            this.panel22.Controls.Add(this.label43);
            this.panel22.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel22.Location = new System.Drawing.Point(120, 0);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(119, 253);
            this.panel22.TabIndex = 36;
            // 
            // label41
            // 
            this.label41.Dock = System.Windows.Forms.DockStyle.Top;
            this.label41.Font = new System.Drawing.Font("Verdana", 27F);
            this.label41.Location = new System.Drawing.Point(0, 47);
            this.label41.Margin = new System.Windows.Forms.Padding(0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(119, 47);
            this.label41.TabIndex = 39;
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelDOBheader
            // 
            this.labelDOBheader.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelDOBheader.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelDOBheader.Location = new System.Drawing.Point(0, 141);
            this.labelDOBheader.Margin = new System.Windows.Forms.Padding(0);
            this.labelDOBheader.Name = "labelDOBheader";
            this.labelDOBheader.Size = new System.Drawing.Size(119, 47);
            this.labelDOBheader.TabIndex = 37;
            this.labelDOBheader.Text = "D.o.b.:";
            this.labelDOBheader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label42
            // 
            this.label42.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label42.Font = new System.Drawing.Font("Verdana", 27F);
            this.label42.Location = new System.Drawing.Point(0, 188);
            this.label42.Margin = new System.Windows.Forms.Padding(0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(119, 65);
            this.label42.TabIndex = 38;
            this.label42.Text = " ";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label43
            // 
            this.label43.Dock = System.Windows.Forms.DockStyle.Top;
            this.label43.Font = new System.Drawing.Font("Verdana", 20F);
            this.label43.Location = new System.Drawing.Point(0, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(119, 47);
            this.label43.TabIndex = 36;
            this.label43.Text = "Name:";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel25
            // 
            this.panel25.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel25.Controls.Add(this.panel29);
            this.panel25.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel25.Location = new System.Drawing.Point(0, 0);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(120, 253);
            this.panel25.TabIndex = 38;
            // 
            // panel29
            // 
            this.panel29.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel29.BackgroundImage")));
            this.panel29.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel29.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel29.Location = new System.Drawing.Point(0, 0);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(120, 120);
            this.panel29.TabIndex = 39;
            // 
            // panel30
            // 
            this.panel30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel30.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel30.Location = new System.Drawing.Point(0, 0);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(2338, 2);
            this.panel30.TabIndex = 0;
            // 
            // panelDateTimeofEventStrip
            // 
            this.panelDateTimeofEventStrip.Controls.Add(this.labelSingleEventReportDate);
            this.panelDateTimeofEventStrip.Controls.Add(this.label11);
            this.panelDateTimeofEventStrip.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelDateTimeofEventStrip.Location = new System.Drawing.Point(0, 254);
            this.panelDateTimeofEventStrip.Name = "panelDateTimeofEventStrip";
            this.panelDateTimeofEventStrip.Size = new System.Drawing.Size(2338, 89);
            this.panelDateTimeofEventStrip.TabIndex = 45;
            // 
            // labelSingleEventReportDate
            // 
            this.labelSingleEventReportDate.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSingleEventReportDate.Font = new System.Drawing.Font("Verdana", 38.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSingleEventReportDate.Location = new System.Drawing.Point(945, 0);
            this.labelSingleEventReportDate.Name = "labelSingleEventReportDate";
            this.labelSingleEventReportDate.Size = new System.Drawing.Size(727, 89);
            this.labelSingleEventReportDate.TabIndex = 11;
            this.labelSingleEventReportDate.Text = "^99/99/9999 99:99:99 AM";
            this.labelSingleEventReportDate.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label11
            // 
            this.label11.Dock = System.Windows.Forms.DockStyle.Left;
            this.label11.Font = new System.Drawing.Font("Verdana", 38.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(0, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(945, 89);
            this.label11.TabIndex = 10;
            this.label11.Text = "Printed:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panelCardiacEVentReportNumber
            // 
            this.panelCardiacEVentReportNumber.Controls.Add(this.panel4);
            this.panelCardiacEVentReportNumber.Controls.Add(this.panel2);
            this.panelCardiacEVentReportNumber.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCardiacEVentReportNumber.Location = new System.Drawing.Point(0, 175);
            this.panelCardiacEVentReportNumber.Name = "panelCardiacEVentReportNumber";
            this.panelCardiacEVentReportNumber.Size = new System.Drawing.Size(2338, 79);
            this.panelCardiacEVentReportNumber.TabIndex = 43;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.labelCardiacEventReportNr);
            this.panel4.Controls.Add(this.labelReportNr);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(400, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1938, 100);
            this.panel4.TabIndex = 2;
            // 
            // labelCardiacEventReportNr
            // 
            this.labelCardiacEventReportNr.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelCardiacEventReportNr.Font = new System.Drawing.Font("Verdana", 45.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCardiacEventReportNr.Location = new System.Drawing.Point(0, 0);
            this.labelCardiacEventReportNr.Name = "labelCardiacEventReportNr";
            this.labelCardiacEventReportNr.Size = new System.Drawing.Size(1548, 100);
            this.labelCardiacEventReportNr.TabIndex = 3;
            this.labelCardiacEventReportNr.Text = "Mobile Cardiac Telemetry Report ";
            this.labelCardiacEventReportNr.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelReportNr
            // 
            this.labelReportNr.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelReportNr.Font = new System.Drawing.Font("Verdana", 45.75F, System.Drawing.FontStyle.Bold);
            this.labelReportNr.Location = new System.Drawing.Point(1638, 0);
            this.labelReportNr.Name = "labelReportNr";
            this.labelReportNr.Size = new System.Drawing.Size(300, 100);
            this.labelReportNr.TabIndex = 2;
            this.labelReportNr.Text = "??";
            this.labelReportNr.Visible = false;
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(400, 79);
            this.panel2.TabIndex = 0;
            // 
            // panelPrintHeader
            // 
            this.panelPrintHeader.Controls.Add(this.panel56);
            this.panelPrintHeader.Controls.Add(this.labelPage1);
            this.panelPrintHeader.Controls.Add(this.pictureBoxCenter);
            this.panelPrintHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelPrintHeader.Location = new System.Drawing.Point(0, 0);
            this.panelPrintHeader.Name = "panelPrintHeader";
            this.panelPrintHeader.Size = new System.Drawing.Size(2338, 175);
            this.panelPrintHeader.TabIndex = 42;
            // 
            // panel56
            // 
            this.panel56.Controls.Add(this.panel57);
            this.panel56.Controls.Add(this.panel58);
            this.panel56.Controls.Add(this.panel59);
            this.panel56.Controls.Add(this.panel60);
            this.panel56.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel56.Location = new System.Drawing.Point(1607, 0);
            this.panel56.Name = "panel56";
            this.panel56.Size = new System.Drawing.Size(731, 175);
            this.panel56.TabIndex = 11;
            // 
            // panel57
            // 
            this.panel57.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel57.Location = new System.Drawing.Point(0, 129);
            this.panel57.Name = "panel57";
            this.panel57.Size = new System.Drawing.Size(731, 27);
            this.panel57.TabIndex = 16;
            // 
            // panel58
            // 
            this.panel58.Controls.Add(this.labelCenter2);
            this.panel58.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel58.Location = new System.Drawing.Point(0, 82);
            this.panel58.Name = "panel58";
            this.panel58.Size = new System.Drawing.Size(731, 47);
            this.panel58.TabIndex = 15;
            // 
            // labelCenter2
            // 
            this.labelCenter2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCenter2.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.labelCenter2.Location = new System.Drawing.Point(0, 0);
            this.labelCenter2.Name = "labelCenter2";
            this.labelCenter2.Size = new System.Drawing.Size(731, 47);
            this.labelCenter2.TabIndex = 12;
            this.labelCenter2.Text = "c2";
            this.labelCenter2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel59
            // 
            this.panel59.Controls.Add(this.labelCenter1);
            this.panel59.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel59.Location = new System.Drawing.Point(0, 27);
            this.panel59.Name = "panel59";
            this.panel59.Size = new System.Drawing.Size(731, 55);
            this.panel59.TabIndex = 14;
            // 
            // labelCenter1
            // 
            this.labelCenter1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCenter1.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.labelCenter1.Location = new System.Drawing.Point(0, 0);
            this.labelCenter1.Name = "labelCenter1";
            this.labelCenter1.Size = new System.Drawing.Size(731, 55);
            this.labelCenter1.TabIndex = 11;
            this.labelCenter1.Text = "c1";
            this.labelCenter1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel60
            // 
            this.panel60.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel60.Location = new System.Drawing.Point(0, 0);
            this.panel60.Name = "panel60";
            this.panel60.Size = new System.Drawing.Size(731, 27);
            this.panel60.TabIndex = 13;
            // 
            // labelPage1
            // 
            this.labelPage1.AutoSize = true;
            this.labelPage1.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPage1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPage1.Location = new System.Drawing.Point(656, 0);
            this.labelPage1.Name = "labelPage1";
            this.labelPage1.Size = new System.Drawing.Size(177, 55);
            this.labelPage1.TabIndex = 10;
            this.labelPage1.Text = "Page 1";
            // 
            // pictureBoxCenter
            // 
            this.pictureBoxCenter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBoxCenter.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBoxCenter.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxCenter.Name = "pictureBoxCenter";
            this.pictureBoxCenter.Size = new System.Drawing.Size(656, 175);
            this.pictureBoxCenter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxCenter.TabIndex = 1;
            this.pictureBoxCenter.TabStop = false;
            // 
            // label56
            // 
            this.label56.Dock = System.Windows.Forms.DockStyle.Top;
            this.label56.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(0, 25);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(2376, 46);
            this.label56.TabIndex = 33;
            this.label56.Text = "^9:99:99";
            this.label56.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // CPrintMctForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(2393, 1770);
            this.Controls.Add(this.label56);
            this.Controls.Add(this.panelPrintArea1);
            this.Controls.Add(this.toolStrip1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "CPrintMctForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Print Study Events";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CPrintMctForm_FormClosing);
            this.Load += new System.EventHandler(this.CPrintMctForm_Load);
            this.Shown += new System.EventHandler(this.CPrintMctForm_Shown);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel51.ResumeLayout(false);
            this.panel126.ResumeLayout(false);
            this.panelSignatures.ResumeLayout(false);
            this.panel110.ResumeLayout(false);
            this.panel109.ResumeLayout(false);
            this.panel108.ResumeLayout(false);
            this.panel107.ResumeLayout(false);
            this.panel106.ResumeLayout(false);
            this.panel105.ResumeLayout(false);
            this.panelPrintArea1.ResumeLayout(false);
            this.panelEventsStat.ResumeLayout(false);
            this.panelt1.ResumeLayout(false);
            this.panelStatInfo.ResumeLayout(false);
            this.panelStatInfoGraph.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartSummaryBar)).EndInit();
            this.panel88.ResumeLayout(false);
            this.panelPer.ResumeLayout(false);
            this.panelDuration.ResumeLayout(false);
            this.panel91.ResumeLayout(false);
            this.panel92.ResumeLayout(false);
            this.panelStatHeader.ResumeLayout(false);
            this.panelStatHeader.PerformLayout();
            this.panel85.ResumeLayout(false);
            this.panel68.ResumeLayout(false);
            this.panel28.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartAll)).EndInit();
            this.panel33.ResumeLayout(false);
            this.panel72.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartManual)).EndInit();
            this.panel73.ResumeLayout(false);
            this.panel23.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartOther)).EndInit();
            this.panel26.ResumeLayout(false);
            this.panel37.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartAbN)).EndInit();
            this.panel38.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartVtach)).EndInit();
            this.panel21.ResumeLayout(false);
            this.panel69.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartPace)).EndInit();
            this.panel70.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartPVC)).EndInit();
            this.panel14.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartPAC)).EndInit();
            this.panel9.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartAflut)).EndInit();
            this.panel19.ResumeLayout(false);
            this.panel74.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartAfib)).EndInit();
            this.panel75.ResumeLayout(false);
            this.panel76.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartPause)).EndInit();
            this.panel77.ResumeLayout(false);
            this.panel78.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartBrady)).EndInit();
            this.panel79.ResumeLayout(false);
            this.panel80.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartTachy)).EndInit();
            this.panel81.ResumeLayout(false);
            this.panel62.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartHR)).EndInit();
            this.panel63.ResumeLayout(false);
            this.panel40.ResumeLayout(false);
            this.panel40.PerformLayout();
            this.panelDiagnosis.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel67.ResumeLayout(false);
            this.panel258.ResumeLayout(false);
            this.panel256.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel252.ResumeLayout(false);
            this.panel250.ResumeLayout(false);
            this.panel244.ResumeLayout(false);
            this.panel242.ResumeLayout(false);
            this.panel32.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel34.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel18.ResumeLayout(false);
            this.panel27.ResumeLayout(false);
            this.panel22.ResumeLayout(false);
            this.panel25.ResumeLayout(false);
            this.panelDateTimeofEventStrip.ResumeLayout(false);
            this.panelCardiacEVentReportNumber.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panelPrintHeader.ResumeLayout(false);
            this.panelPrintHeader.PerformLayout();
            this.panel56.ResumeLayout(false);
            this.panel58.ResumeLayout(false);
            this.panel59.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCenter)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButtonPrint;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripButton toolStripClipboard1;
        private System.Windows.Forms.ToolStripButton toolStripEnterData;
        private System.Windows.Forms.ToolStripButton toolStripClipboard2;
        private System.Windows.Forms.ToolStripButton toolStripGenPages;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.Panel panel51;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label labelPrintDate1Bottom;
        private System.Windows.Forms.Label labelStudyNr;
        private System.Windows.Forms.Label labelPage;
        private System.Windows.Forms.Label labelPage1of;
        private System.Windows.Forms.Label labelPageOf;
        private System.Windows.Forms.Label labelPage1xOfX;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel126;
        private System.Windows.Forms.Panel panelSignatures;
        private System.Windows.Forms.Panel panel110;
        private System.Windows.Forms.Label labelReportSignDate;
        private System.Windows.Forms.Panel panel109;
        private System.Windows.Forms.Label labelDateReportSign;
        private System.Windows.Forms.Panel panel108;
        private System.Windows.Forms.Label labelPhysSignLine;
        private System.Windows.Forms.Panel panel107;
        private System.Windows.Forms.Label labelPhysSign;
        private System.Windows.Forms.Label labelPhysPrintName;
        private System.Windows.Forms.Panel panel105;
        private System.Windows.Forms.Label labelPhysNameReportPrint;
        private System.Windows.Forms.Panel panel104;
        private System.Windows.Forms.Label labelNote2;
        private System.Windows.Forms.Label labelNote1;
        private System.Windows.Forms.Panel panelPrintArea1;
        private System.Windows.Forms.Panel panelEventsStat;
        private System.Windows.Forms.Panel panelPrintHeader;
        private System.Windows.Forms.Panel panel56;
        private System.Windows.Forms.Panel panel57;
        private System.Windows.Forms.Panel panel58;
        private System.Windows.Forms.Label labelCenter2;
        private System.Windows.Forms.Panel panel59;
        private System.Windows.Forms.Label labelCenter1;
        private System.Windows.Forms.Panel panel60;
        private System.Windows.Forms.Label labelPage1;
        private System.Windows.Forms.PictureBox pictureBoxCenter;
        private System.Windows.Forms.Panel panelDateTimeofEventStrip;
        private System.Windows.Forms.Label labelSingleEventReportDate;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panelCardiacEVentReportNumber;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label labelCardiacEventReportNr;
        private System.Windows.Forms.Label labelReportNr;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel101;
        private System.Windows.Forms.Panel panelDiagnosis;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Label labelDiagnosis;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Panel panel158;
        private System.Windows.Forms.Panel panel67;
        private System.Windows.Forms.Panel panel258;
        private System.Windows.Forms.Label labelClientName;
        private System.Windows.Forms.Label labelClientTel;
        private System.Windows.Forms.Panel panel256;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label labelClientHeader;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.Panel panelVert9;
        private System.Windows.Forms.Panel panel252;
        private System.Windows.Forms.Label labelRefPhysicianName;
        private System.Windows.Forms.Label labelRefPhysicianTel;
        private System.Windows.Forms.Panel panel250;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label labelRefPhysHeader;
        private System.Windows.Forms.Panel panelVert8;
        private System.Windows.Forms.Panel panel244;
        private System.Windows.Forms.Label labelPhysicianName;
        private System.Windows.Forms.Label labelPhysicianTel;
        private System.Windows.Forms.Panel panel242;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label labelPhysHeader;
        private System.Windows.Forms.Panel panelVert6;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.Panel panel36;
        private System.Windows.Forms.Panel panel41;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label labelEndStudyDate;
        private System.Windows.Forms.Label labelPatID;
        private System.Windows.Forms.Label labelSerialNr;
        private System.Windows.Forms.Label labelStartDate;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label labelEndStudyText;
        private System.Windows.Forms.Label labelPatIDHeader;
        private System.Windows.Forms.Label labelSerialNrHeader;
        private System.Windows.Forms.Label labelStartDateHeader;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Panel panel34;
        private System.Windows.Forms.Label labelPhone2;
        private System.Windows.Forms.Label labelPhone1;
        private System.Windows.Forms.Label labelCountry;
        private System.Windows.Forms.Label labelZipCity;
        private System.Windows.Forms.Label labelAddress;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label labelAddressHeader;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Label labelPatientLastName;
        private System.Windows.Forms.Label labelPatientFirstName;
        private System.Windows.Forms.Label labelDateOfBirth;
        private System.Windows.Forms.Label labelAgeGender;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label labelDOBheader;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Panel panelt1;
        private System.Windows.Forms.Panel panelStatInfo;
        private System.Windows.Forms.Panel panelStatInfoGraph;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartSummaryBar;
        private System.Windows.Forms.Panel panel88;
        private System.Windows.Forms.Panel panelPer;
        private System.Windows.Forms.Label labelPerMan;
        private System.Windows.Forms.Label labelPerTotal;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label labelPerPVC;
        private System.Windows.Forms.Label labelPerPAC;
        private System.Windows.Forms.Label labelPerAfib;
        private System.Windows.Forms.Label labelPerPause;
        private System.Windows.Forms.Label labelPerBrady;
        private System.Windows.Forms.Label labelPerTachy;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Panel panelDuration;
        private System.Windows.Forms.Label labelDurMan;
        private System.Windows.Forms.Label labelDurTotal;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label labelDurPVC;
        private System.Windows.Forms.Label labelDurPAC;
        private System.Windows.Forms.Label labelDurAfib;
        private System.Windows.Forms.Label labelDurPause;
        private System.Windows.Forms.Label labelDurBrady;
        private System.Windows.Forms.Label labelDurTachy;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Panel panel91;
        private System.Windows.Forms.Label labelEpMan;
        private System.Windows.Forms.Label labelEpTotal;
        private System.Windows.Forms.Label labelEpPVC;
        private System.Windows.Forms.Label labelEpPAC;
        private System.Windows.Forms.Label labelEpAfib;
        private System.Windows.Forms.Label labelEpPause;
        private System.Windows.Forms.Label labelEpBrady;
        private System.Windows.Forms.Label labelEpTachy;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel panel92;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Panel panelStatHeader;
        private System.Windows.Forms.Panel panel85;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel83;
        private System.Windows.Forms.Panel panel68;
        private System.Windows.Forms.Panel panel72;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartManual;
        private System.Windows.Forms.Panel panel73;
        private System.Windows.Forms.Label labelGraphManual;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Panel panel69;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartPace;
        private System.Windows.Forms.Panel panel70;
        private System.Windows.Forms.Label labelGraphPace;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartPVC;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Label labelGraphPVC;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartPAC;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label labelGraphPAC;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel74;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartAfib;
        private System.Windows.Forms.Panel panel75;
        private System.Windows.Forms.Label labelGraphAfib;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Panel panel76;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartPause;
        private System.Windows.Forms.Panel panel77;
        private System.Windows.Forms.Label labelGraphPause;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Panel panel78;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartBrady;
        private System.Windows.Forms.Panel panel79;
        private System.Windows.Forms.Label labelGraphBrady;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Panel panel80;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartTachy;
        private System.Windows.Forms.Panel panel81;
        private System.Windows.Forms.Label labelGraphTachy;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Panel panel82;
        private System.Windows.Forms.Panel panel64;
        private System.Windows.Forms.Panel panel62;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartHR;
        private System.Windows.Forms.Panel panel63;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Panel panel40;
        private System.Windows.Forms.Label labelTotalBeats;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label labelDaysHours;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label labelFirstDate;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartOther;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Label labelGraphOther;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartVtach;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Label labelGraphVtach;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartAflut;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Label labelGraphAflut;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label labelPerAflut;
        private System.Windows.Forms.Label labelDurAflut;
        private System.Windows.Forms.Label labelEpAflut;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labelPerOther;
        private System.Windows.Forms.Label labelPerVtach;
        private System.Windows.Forms.Label labelDurOther;
        private System.Windows.Forms.Label labelDurVtach;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label labelEpOther;
        private System.Windows.Forms.Label labelEpVtach;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label25;
        public System.Windows.Forms.Panel panel106;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartAll;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.Label labelGraphAll;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label labelPerPace;
        private System.Windows.Forms.Label labelDurPace;
        private System.Windows.Forms.Label labelEpPace;
        private System.Windows.Forms.Label labelPace;
        private System.Windows.Forms.Label labelTechText;
        private System.Windows.Forms.Label labelUser;
        private System.Windows.Forms.Panel panel35;
        private System.Windows.Forms.Label labelTrendTo;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label labelPerNormal;
        private System.Windows.Forms.Label labelDurNormal;
        private System.Windows.Forms.Label labelEpNormal;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label labelPerAbNormal;
        private System.Windows.Forms.Label labelDurAbNormal;
        private System.Windows.Forms.Label labelEpAbNormal;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel37;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartAbN;
        private System.Windows.Forms.Panel panel38;
        private System.Windows.Forms.Label labelGraphAbN;
        private System.Windows.Forms.Label label57;
    }
}