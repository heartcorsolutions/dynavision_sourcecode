﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Program_Base;


namespace EventBoard
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            CProgram.sSetProgLogConsole(DDebugFlags.FullDebug); // AllOff);
            CProgram.sSetProgLogFile(DDebugFlags.FullDebug); // StandardLog);  // enables file logging
            CProgram.sSetProgLogScreen(DDebugFlags.FullDebug & (~DDebugFlags.Class)); // StandardLog);
            CProgram.sSetPromptFlags(DDebugFlags.FullDebug); // StandardPrompt);

            if (CProgram.sbStartProgram("EventBoard", 21, 1, "Techmedic", "TMI1", 0x36C281A5, 0))
            {
                try
                {
                    string header = "DVTMS";

                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);

                    CProgram.sSetProgTitleFormat(header, true, DProgramVersionLevel.Beta, DShowOrganisation.First, ""); // set version info in title α= alpha, β= beta, v= version

                    DLicensePC licensePc = DLicensePC.MultiPcPrefered;
                    string licensePath = Properties.Settings.Default.RecordingDir;

                    DateTime dt = DateTime.Now;
                    bool bLicOk = CLicKeyDev.sbLicenseRequest(14, false, licensePc, licensePath);

                    if (bLicOk)
                    {
                        string tail = "";
                        int daysLeft = CLicKeyDev.sGetDaysLeft();
                        //                        int daysLeft = 100;
                        if (daysLeft < 7)
                        {
                            tail = daysLeft.ToString() + " days left!";
                        }
                        CProgram.sSetProgTitleFormat(header, true, CProgram.sGetProgVersionLevel(), DShowOrganisation.First, tail); // set version info in title α= alpha, β= beta, v= version
                        CProgram.sSetProgramTitle("Eventboard opening", true);
                        CProgram.sLogLine("Create & Run Eventboard main form");
                        Application.Run(new FormEventBoard());
                    }
                }
                catch (Exception Ex)
                {
                    CProgram.sLogException( "Program Exit, Uncaught Exception", Ex);
                    //CProgram.sPromptException(false, "MainForm", "Uncaught Exception", Ex);
                }
                CProgram.sEndProgram();
            }
        }

        internal static bool sbReqLabel(string v1, string v2, ref string message, string v3)
        {
            throw new NotImplementedException();
        }
    }
}
