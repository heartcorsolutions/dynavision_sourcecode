﻿namespace EventBoard
{
    partial class FormAnalyze
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ToolStripButton toolStripSelectChannel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAnalyze));
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("V(q-r) = 1.2 mV");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("T(r+r) = 800 mS");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("M 12:13:24 (6 Sec)", new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2});
            this.toolStripChn = new System.Windows.Forms.ToolStrip();
            this.toolStripL1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripNrChannels = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSpace = new System.Windows.Forms.ToolStripLabel();
            this.toolStripChannelsStart = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel8 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripChannelTimeIncrease = new System.Windows.Forms.ToolStripButton();
            this.toolStripChannelPeriod = new System.Windows.Forms.ToolStripButton();
            this.toolStripChannelTimeDecrease = new System.Windows.Forms.ToolStripButton();
            this.toolStripChannelTimeLeft = new System.Windows.Forms.ToolStripButton();
            this.toolStripChannelTimeRight = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel16 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripChannelStartTimeDate = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel23 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripChannelDuration = new System.Windows.Forms.ToolStripButton();
            this.toolStripARangeN = new System.Windows.Forms.ToolStripLabel();
            this.toolStripPageScan = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabelHrInfo = new System.Windows.Forms.ToolStripLabel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel43 = new System.Windows.Forms.Panel();
            this.panel45 = new System.Windows.Forms.Panel();
            this.labelStripTime = new System.Windows.Forms.Label();
            this.labeAllChannels = new System.Windows.Forms.Label();
            this.labelWholeStrip = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel46 = new System.Windows.Forms.Panel();
            this.checkBoxBlackWhite = new System.Windows.Forms.CheckBox();
            this.panel33 = new System.Windows.Forms.Panel();
            this.panel34 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.buttonExportData = new System.Windows.Forms.Button();
            this.checkBoxCh2 = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.panel44 = new System.Windows.Forms.Panel();
            this.comboBoxStripSec = new System.Windows.Forms.ComboBox();
            this.checkBoxAllChannels = new System.Windows.Forms.CheckBox();
            this.checkBoxHoleStrip = new System.Windows.Forms.CheckBox();
            this.checkBoxAddDisclosure = new System.Windows.Forms.CheckBox();
            this.checkBoxAnonymize = new System.Windows.Forms.CheckBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel29 = new System.Windows.Forms.Panel();
            this.textBoxEventRemark = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel19 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.labelEventTime = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.labelEventLabel = new System.Windows.Forms.Label();
            this.labelEventPriority = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.textBoxStudyRemark = new System.Windows.Forms.TextBox();
            this.panel20 = new System.Windows.Forms.Panel();
            this.labelStudyNote = new System.Windows.Forms.Label();
            this.labelStudyProcCodes = new System.Windows.Forms.Label();
            this.buttonStudyFindings = new System.Windows.Forms.Button();
            this.labelStudy = new System.Windows.Forms.Label();
            this.labelStudyText = new System.Windows.Forms.Label();
            this.buttonPdfView = new System.Windows.Forms.Button();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel21 = new System.Windows.Forms.Panel();
            this.comboBoxState = new System.Windows.Forms.ComboBox();
            this.labelNextState = new System.Windows.Forms.Label();
            this.panel15 = new System.Windows.Forms.Panel();
            this.buttonSaveOk = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonPrint = new System.Windows.Forms.Button();
            this.buttonHelpDesk = new System.Windows.Forms.Button();
            this.panel14 = new System.Windows.Forms.Panel();
            this.labelUser = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.label20 = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel32 = new System.Windows.Forms.Panel();
            this.labelPatID = new System.Windows.Forms.Label();
            this.labelPatIDHeader = new System.Windows.Forms.Label();
            this.textBoxPatientRemark = new System.Windows.Forms.TextBox();
            this.contextMenuStripN = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.secToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.secToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.secToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.mSecToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
            this.secToolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.secToolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.secToolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.secToolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.secToolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
            this.verticalUnitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mVToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mVToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.mVToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.mVToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.mVToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.mVToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.mVToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.lineTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pointToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.panelZoomStrip = new System.Windows.Forms.Panel();
            this.listViewStats = new System.Windows.Forms.ListView();
            this.cStag = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cSn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cSmin = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cSmax = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cSmean = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cSunit = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.listViewMeasurements = new System.Windows.Forms.ListView();
            this.cMtime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cMchannel = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cMtag = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cMvalue = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cMunit = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.listViewStrips = new System.Windows.Forms.ListView();
            this.cChannel = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cDuration = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panelChannel6 = new System.Windows.Forms.Panel();
            this.panelChannel5 = new System.Windows.Forms.Panel();
            this.panelChannel4 = new System.Windows.Forms.Panel();
            this.panelChannel3 = new System.Windows.Forms.Panel();
            this.panelChannel2 = new System.Windows.Forms.Panel();
            this.panelChannel1 = new System.Windows.Forms.Panel();
            this.panelChannel12 = new System.Windows.Forms.Panel();
            this.panelChannel11 = new System.Windows.Forms.Panel();
            this.panelChannel10 = new System.Windows.Forms.Panel();
            this.panelChannel9 = new System.Windows.Forms.Panel();
            this.panelChannel8 = new System.Windows.Forms.Panel();
            this.panelChannel7 = new System.Windows.Forms.Panel();
            this.toolStripStrips = new System.Windows.Forms.ToolStrip();
            this.toolStripAddStrip = new System.Windows.Forms.ToolStripButton();
            this.toolStripDeleteStrip = new System.Windows.Forms.ToolStripButton();
            this.toolStripSetDupStrip = new System.Windows.Forms.ToolStripButton();
            this.toolStripView = new System.Windows.Forms.ToolStripButton();
            this.toolStripCopyWindow = new System.Windows.Forms.ToolStripButton();
            this.toolStripFirst = new System.Windows.Forms.ToolStripButton();
            this.toolStripBaseLine = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.AutoScaleCheckbox = new System.Windows.Forms.CheckBox();
            this.buttonCalcRR = new System.Windows.Forms.Button();
            this.miniToolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButton17 = new System.Windows.Forms.ToolStripButton();
            this.toolStrip5 = new System.Windows.Forms.ToolStrip();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.panel42 = new System.Windows.Forms.Panel();
            this.buttonAnAbNormal = new System.Windows.Forms.Button();
            this.labelCreateSec = new System.Windows.Forms.Label();
            this.buttonAnPause = new System.Windows.Forms.Button();
            this.buttonAnBrady = new System.Windows.Forms.Button();
            this.buttonAnTachy = new System.Windows.Forms.Button();
            this.buttonAnNormal = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.panel28 = new System.Windows.Forms.Panel();
            this.labelLoadSec = new System.Windows.Forms.Label();
            this.buttonAnPVC = new System.Windows.Forms.Button();
            this.buttonAnPAC = new System.Windows.Forms.Button();
            this.buttonAflut = new System.Windows.Forms.Button();
            this.buttonAnAFib = new System.Windows.Forms.Button();
            this.MaxAmplitude = new System.Windows.Forms.NumericUpDown();
            this.panel16 = new System.Windows.Forms.Panel();
            this.labelShowTotalSec = new System.Windows.Forms.Label();
            this.buttonOther = new System.Windows.Forms.Button();
            this.buttonAnBline = new System.Windows.Forms.Button();
            this.buttonAnVtach = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonAnPace = new System.Windows.Forms.Button();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel25 = new System.Windows.Forms.Panel();
            this.panelZoomAT = new System.Windows.Forms.Panel();
            this.toolStripZoomAT = new System.Windows.Forms.ToolStrip();
            this.toolStripZoomEndTime = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel11 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripZoomSpeed = new System.Windows.Forms.ToolStripButton();
            this.toolStripZoomAmplitude = new System.Windows.Forms.ToolStripButton();
            this.panel30 = new System.Windows.Forms.Panel();
            this.toolStrip12 = new System.Windows.Forms.ToolStrip();
            this.toolStripOneClick = new System.Windows.Forms.ToolStripButton();
            this.toolStripTwoClick = new System.Windows.Forms.ToolStripButton();
            this.toolStripSnap = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.panel31 = new System.Windows.Forms.Panel();
            this.toolStripZoom = new System.Windows.Forms.ToolStrip();
            this.toolStripSelectZoom = new System.Windows.Forms.ToolStripButton();
            this.toolStripZoomChannel = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel18 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripZoomStartTime = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel20 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripFullStrip = new System.Windows.Forms.ToolStripButton();
            this.toolStripZoomTimeIncrease = new System.Windows.Forms.ToolStripButton();
            this.toolStripZoomDuration = new System.Windows.Forms.ToolStripButton();
            this.toolStripZoomTimeOut = new System.Windows.Forms.ToolStripButton();
            this.toolStripWindowLeft = new System.Windows.Forms.ToolStripButton();
            this.toolStripWindowRight = new System.Windows.Forms.ToolStripButton();
            this.toolStripWindowDown = new System.Windows.Forms.ToolStripButton();
            this.toolStripWindowUp = new System.Windows.Forms.ToolStripButton();
            this.toolStripAmpDec = new System.Windows.Forms.ToolStripButton();
            this.toolStripAmpInc = new System.Windows.Forms.ToolStripButton();
            this.toolStripWindowCenter = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonACDC = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonP1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonP2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel21 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripARange1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonInvert = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonLPF = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonHPF = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonBBF = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonAvgCR = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonAddChannel = new System.Windows.Forms.ToolStripButton();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.panelDrawChannels = new System.Windows.Forms.Panel();
            this.splitContainerChannels = new System.Windows.Forms.SplitContainer();
            this.panelAllChannels = new System.Windows.Forms.Panel();
            this.panelChannelScrol = new System.Windows.Forms.Panel();
            this.panelHR = new System.Windows.Forms.Panel();
            this.pictureBoxHR = new System.Windows.Forms.PictureBox();
            this.panel36 = new System.Windows.Forms.Panel();
            this.label1MeanHR = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.labelMeanBurden = new System.Windows.Forms.Label();
            this.panel37 = new System.Windows.Forms.Panel();
            this.labelHR = new System.Windows.Forms.Label();
            this.panelGraphSeperator = new System.Windows.Forms.Panel();
            this.panelGraphMarker = new System.Windows.Forms.Panel();
            this.panel38 = new System.Windows.Forms.Panel();
            this.ScrollBarChannel = new System.Windows.Forms.HScrollBar();
            this.panel39 = new System.Windows.Forms.Panel();
            this.panelGraph = new System.Windows.Forms.Panel();
            this.pictureBoxGraph = new System.Windows.Forms.PictureBox();
            this.panel41 = new System.Windows.Forms.Panel();
            this.labelGraphRight3 = new System.Windows.Forms.Label();
            this.labelGraphRight2 = new System.Windows.Forms.Label();
            this.labelGraphRight1 = new System.Windows.Forms.Label();
            this.panel47 = new System.Windows.Forms.Panel();
            this.labelGraphHeader = new System.Windows.Forms.Label();
            this.panelChannelsInfo = new System.Windows.Forms.Panel();
            this.panelChannelsButtons = new System.Windows.Forms.Panel();
            this.panel52 = new System.Windows.Forms.Panel();
            this.panelStripAT = new System.Windows.Forms.Panel();
            this.toolStrip10 = new System.Windows.Forms.ToolStrip();
            this.toolStripChannelEndTime = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel15 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripChannelSpeed = new System.Windows.Forms.ToolStripButton();
            this.toolStripChannelAmplitute = new System.Windows.Forms.ToolStripButton();
            this.panelSAF = new System.Windows.Forms.Panel();
            this.panel27 = new System.Windows.Forms.Panel();
            this.textBoxFindings = new System.Windows.Forms.TextBox();
            this.panel23 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.buttonSelectFindings = new System.Windows.Forms.Button();
            this.buttonFindingsRhythm = new System.Windows.Forms.Button();
            this.checkBoxQCD = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.textBoxActivities = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxSymptoms = new System.Windows.Forms.TextBox();
            this.panel35 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.labelPrevTech = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.panelAutoMeasurements = new System.Windows.Forms.Panel();
            this.treeViewAutoMeasurements = new System.Windows.Forms.TreeView();
            this.toolStrip9 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel5 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripCopyAuto = new System.Windows.Forms.ToolStripButton();
            this.panel24 = new System.Windows.Forms.Panel();
            this.toolStrip7 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabelStats = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButtonRecalcStats = new System.Windows.Forms.ToolStripButton();
            this.toolStripCopyTable = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabelAnalysis = new System.Windows.Forms.ToolStripLabel();
            this.toolStripAnalysisNr = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButtonMctTrend = new System.Windows.Forms.ToolStripButton();
            this.panel26 = new System.Windows.Forms.Panel();
            this.toolStrip4 = new System.Windows.Forms.ToolStrip();
            this.toolStripMeasurmentName = new System.Windows.Forms.ToolStripLabel();
            this.toolStripHr = new System.Windows.Forms.ToolStripLabel();
            this.toolStripDeleteteMeasurement = new System.Windows.Forms.ToolStripButton();
            this.toolStripEditMeasurement = new System.Windows.Forms.ToolStripButton();
            this.toolStripRecalcMeasurement = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonStripP1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonStripP2 = new System.Windows.Forms.ToolStripButton();
            this.panel22 = new System.Windows.Forms.Panel();
            this.contextMenuAmplitude = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem13 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem14 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem15 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem16 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem17 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem18 = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuFeed = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem19 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem20 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem21 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem22 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem23 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem24 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem25 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem26 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem27 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem28 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem29 = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuSingleClick = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.contextMenuDualClick = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.contextMenuSelChannel = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.contextMenuAddChannel = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.contextMenuExport = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.dVX1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mIT16ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mIT24ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mECGSimulatorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.simExportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pulseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sinusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sinusRangeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.triangleSawToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.spikeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveViewPartMIT16ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            toolStripSelectChannel = new System.Windows.Forms.ToolStripButton();
            this.toolStripChn.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel43.SuspendLayout();
            this.panel45.SuspendLayout();
            this.panel46.SuspendLayout();
            this.panel33.SuspendLayout();
            this.panel34.SuspendLayout();
            this.panel44.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel29.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel32.SuspendLayout();
            this.contextMenuStripN.SuspendLayout();
            this.toolStripStrips.SuspendLayout();
            this.toolStrip5.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.panel42.SuspendLayout();
            this.panel28.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MaxAmplitude)).BeginInit();
            this.panel16.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panelZoomAT.SuspendLayout();
            this.toolStripZoomAT.SuspendLayout();
            this.panel30.SuspendLayout();
            this.toolStrip12.SuspendLayout();
            this.panel31.SuspendLayout();
            this.toolStripZoom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            this.panelDrawChannels.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerChannels)).BeginInit();
            this.splitContainerChannels.Panel1.SuspendLayout();
            this.splitContainerChannels.Panel2.SuspendLayout();
            this.splitContainerChannels.SuspendLayout();
            this.panelChannelScrol.SuspendLayout();
            this.panelHR.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxHR)).BeginInit();
            this.panel36.SuspendLayout();
            this.panel37.SuspendLayout();
            this.panelGraphSeperator.SuspendLayout();
            this.panel38.SuspendLayout();
            this.panel39.SuspendLayout();
            this.panelGraph.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGraph)).BeginInit();
            this.panel41.SuspendLayout();
            this.panel47.SuspendLayout();
            this.panelChannelsInfo.SuspendLayout();
            this.panelChannelsButtons.SuspendLayout();
            this.panel52.SuspendLayout();
            this.panelStripAT.SuspendLayout();
            this.toolStrip10.SuspendLayout();
            this.panelSAF.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel35.SuspendLayout();
            this.panelAutoMeasurements.SuspendLayout();
            this.toolStrip9.SuspendLayout();
            this.panel24.SuspendLayout();
            this.toolStrip7.SuspendLayout();
            this.panel26.SuspendLayout();
            this.toolStrip4.SuspendLayout();
            this.panel22.SuspendLayout();
            this.contextMenuAmplitude.SuspendLayout();
            this.contextMenuFeed.SuspendLayout();
            this.contextMenuExport.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripSelectChannel
            // 
            toolStripSelectChannel.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSelectChannel.Image")));
            toolStripSelectChannel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            toolStripSelectChannel.ImageTransparentColor = System.Drawing.Color.White;
            toolStripSelectChannel.Name = "toolStripSelectChannel";
            toolStripSelectChannel.RightToLeftAutoMirrorImage = true;
            toolStripSelectChannel.Size = new System.Drawing.Size(28, 26);
            toolStripSelectChannel.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            toolStripSelectChannel.ToolTipText = "Select channels";
            // 
            // toolStripChn
            // 
            this.toolStripChn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.toolStripChn.Dock = System.Windows.Forms.DockStyle.Left;
            this.toolStripChn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripChn.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripChn.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolStripChn.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            toolStripSelectChannel,
            this.toolStripL1,
            this.toolStripNrChannels,
            this.toolStripSpace,
            this.toolStripChannelsStart,
            this.toolStripLabel8,
            this.toolStripChannelTimeIncrease,
            this.toolStripChannelPeriod,
            this.toolStripChannelTimeDecrease,
            this.toolStripChannelTimeLeft,
            this.toolStripChannelTimeRight,
            this.toolStripLabel16,
            this.toolStripLabel1,
            this.toolStripChannelStartTimeDate,
            this.toolStripLabel23,
            this.toolStripChannelDuration,
            this.toolStripARangeN,
            this.toolStripPageScan,
            this.toolStripLabelHrInfo});
            this.toolStripChn.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolStripChn.Location = new System.Drawing.Point(0, 0);
            this.toolStripChn.Name = "toolStripChn";
            this.toolStripChn.Size = new System.Drawing.Size(1050, 29);
            this.toolStripChn.TabIndex = 1;
            this.toolStripChn.Text = "toolstrip";
            this.toolStripChn.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStripChn_ItemClicked);
            // 
            // toolStripL1
            // 
            this.toolStripL1.Font = new System.Drawing.Font("Arial", 10F);
            this.toolStripL1.Name = "toolStripL1";
            this.toolStripL1.Size = new System.Drawing.Size(76, 26);
            this.toolStripL1.Text = "Channels: ";
            // 
            // toolStripNrChannels
            // 
            this.toolStripNrChannels.AutoSize = false;
            this.toolStripNrChannels.Font = new System.Drawing.Font("Arial", 10F);
            this.toolStripNrChannels.Name = "toolStripNrChannels";
            this.toolStripNrChannels.Size = new System.Drawing.Size(20, 28);
            this.toolStripNrChannels.Text = "00";
            this.toolStripNrChannels.ToolTipText = "Number of channels";
            // 
            // toolStripSpace
            // 
            this.toolStripSpace.Font = new System.Drawing.Font("Arial", 10F);
            this.toolStripSpace.Name = "toolStripSpace";
            this.toolStripSpace.Size = new System.Drawing.Size(16, 26);
            this.toolStripSpace.Text = "  ";
            // 
            // toolStripChannelsStart
            // 
            this.toolStripChannelsStart.Font = new System.Drawing.Font("Arial", 10F);
            this.toolStripChannelsStart.Name = "toolStripChannelsStart";
            this.toolStripChannelsStart.Size = new System.Drawing.Size(163, 26);
            this.toolStripChannelsStart.Text = "12:20:13 AM 12/12/2016";
            this.toolStripChannelsStart.ToolTipText = "Channels view start time";
            this.toolStripChannelsStart.Click += new System.EventHandler(this.toolStripChannelsStartTime_Click);
            // 
            // toolStripLabel8
            // 
            this.toolStripLabel8.Font = new System.Drawing.Font("Arial", 10F);
            this.toolStripLabel8.Name = "toolStripLabel8";
            this.toolStripLabel8.Size = new System.Drawing.Size(16, 26);
            this.toolStripLabel8.Text = "  ";
            // 
            // toolStripChannelTimeIncrease
            // 
            this.toolStripChannelTimeIncrease.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripChannelTimeIncrease.Image = ((System.Drawing.Image)(resources.GetObject("toolStripChannelTimeIncrease.Image")));
            this.toolStripChannelTimeIncrease.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripChannelTimeIncrease.Name = "toolStripChannelTimeIncrease";
            this.toolStripChannelTimeIncrease.Size = new System.Drawing.Size(28, 26);
            this.toolStripChannelTimeIncrease.Text = "toolStripButton36";
            this.toolStripChannelTimeIncrease.ToolTipText = " Channels time Zoom In";
            this.toolStripChannelTimeIncrease.Click += new System.EventHandler(this.toolStripChannelTimeIncrease_Click);
            // 
            // toolStripChannelPeriod
            // 
            this.toolStripChannelPeriod.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripChannelPeriod.Font = new System.Drawing.Font("Arial", 10F);
            this.toolStripChannelPeriod.Image = ((System.Drawing.Image)(resources.GetObject("toolStripChannelPeriod.Image")));
            this.toolStripChannelPeriod.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripChannelPeriod.Name = "toolStripChannelPeriod";
            this.toolStripChannelPeriod.Size = new System.Drawing.Size(56, 26);
            this.toolStripChannelPeriod.Text = "12 Sec";
            this.toolStripChannelPeriod.ToolTipText = "Channels view duration";
            this.toolStripChannelPeriod.Click += new System.EventHandler(this.toolStripChannelPeriod_ItemClicked);
            // 
            // toolStripChannelTimeDecrease
            // 
            this.toolStripChannelTimeDecrease.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripChannelTimeDecrease.Image = ((System.Drawing.Image)(resources.GetObject("toolStripChannelTimeDecrease.Image")));
            this.toolStripChannelTimeDecrease.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripChannelTimeDecrease.Name = "toolStripChannelTimeDecrease";
            this.toolStripChannelTimeDecrease.Size = new System.Drawing.Size(28, 26);
            this.toolStripChannelTimeDecrease.Text = "toolStripButton36";
            this.toolStripChannelTimeDecrease.ToolTipText = "Channels Time Zoom Out";
            this.toolStripChannelTimeDecrease.Click += new System.EventHandler(this.toolStripChannelTimeDecrease_Click);
            // 
            // toolStripChannelTimeLeft
            // 
            this.toolStripChannelTimeLeft.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripChannelTimeLeft.Image = ((System.Drawing.Image)(resources.GetObject("toolStripChannelTimeLeft.Image")));
            this.toolStripChannelTimeLeft.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripChannelTimeLeft.Name = "toolStripChannelTimeLeft";
            this.toolStripChannelTimeLeft.Size = new System.Drawing.Size(28, 26);
            this.toolStripChannelTimeLeft.Text = "toolStripButton1";
            this.toolStripChannelTimeLeft.ToolTipText = "Move Channels Left";
            this.toolStripChannelTimeLeft.Click += new System.EventHandler(this.toolStripChannelTimeLeft_Click);
            // 
            // toolStripChannelTimeRight
            // 
            this.toolStripChannelTimeRight.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripChannelTimeRight.Image = ((System.Drawing.Image)(resources.GetObject("toolStripChannelTimeRight.Image")));
            this.toolStripChannelTimeRight.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripChannelTimeRight.Name = "toolStripChannelTimeRight";
            this.toolStripChannelTimeRight.Size = new System.Drawing.Size(28, 26);
            this.toolStripChannelTimeRight.Text = "toolStripButton2";
            this.toolStripChannelTimeRight.ToolTipText = "Move Channels Right";
            this.toolStripChannelTimeRight.Click += new System.EventHandler(this.toolStripChannelTimeRight_Click);
            // 
            // toolStripLabel16
            // 
            this.toolStripLabel16.Font = new System.Drawing.Font("Arial", 10F);
            this.toolStripLabel16.Name = "toolStripLabel16";
            this.toolStripLabel16.Size = new System.Drawing.Size(20, 26);
            this.toolStripLabel16.Text = "   ";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Font = new System.Drawing.Font("Arial", 10F);
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(86, 26);
            this.toolStripLabel1.Text = "Total Event: ";
            // 
            // toolStripChannelStartTimeDate
            // 
            this.toolStripChannelStartTimeDate.Font = new System.Drawing.Font("Arial", 10F);
            this.toolStripChannelStartTimeDate.Name = "toolStripChannelStartTimeDate";
            this.toolStripChannelStartTimeDate.Size = new System.Drawing.Size(163, 26);
            this.toolStripChannelStartTimeDate.Text = "12:20:13 AM 12/12/2016";
            this.toolStripChannelStartTimeDate.ToolTipText = "Record time";
            this.toolStripChannelStartTimeDate.Click += new System.EventHandler(this.toolStripChannelStartTimeDate_Click);
            // 
            // toolStripLabel23
            // 
            this.toolStripLabel23.Font = new System.Drawing.Font("Arial", 10F);
            this.toolStripLabel23.Name = "toolStripLabel23";
            this.toolStripLabel23.Size = new System.Drawing.Size(20, 26);
            this.toolStripLabel23.Text = "   ";
            // 
            // toolStripChannelDuration
            // 
            this.toolStripChannelDuration.CheckOnClick = true;
            this.toolStripChannelDuration.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripChannelDuration.Font = new System.Drawing.Font("Arial", 10F);
            this.toolStripChannelDuration.Image = ((System.Drawing.Image)(resources.GetObject("toolStripChannelDuration.Image")));
            this.toolStripChannelDuration.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripChannelDuration.Name = "toolStripChannelDuration";
            this.toolStripChannelDuration.Size = new System.Drawing.Size(162, 26);
            this.toolStripChannelDuration.Text = "12999 Sec @ 1000 Sps";
            this.toolStripChannelDuration.ToolTipText = "Record duration";
            this.toolStripChannelDuration.Click += new System.EventHandler(this.toolStripChannelDuration_Click_1);
            // 
            // toolStripARangeN
            // 
            this.toolStripARangeN.Font = new System.Drawing.Font("Arial", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripARangeN.Name = "toolStripARangeN";
            this.toolStripARangeN.Size = new System.Drawing.Size(12, 26);
            this.toolStripARangeN.Text = "Ξ.";
            this.toolStripARangeN.ToolTipText = "Ampl Range Limit";
            // 
            // toolStripPageScan
            // 
            this.toolStripPageScan.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripPageScan.Image = ((System.Drawing.Image)(resources.GetObject("toolStripPageScan.Image")));
            this.toolStripPageScan.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripPageScan.Name = "toolStripPageScan";
            this.toolStripPageScan.Size = new System.Drawing.Size(76, 26);
            this.toolStripPageScan.Text = "Page-Scan";
            this.toolStripPageScan.Click += new System.EventHandler(this.toolStripPageScanClick);
            // 
            // toolStripLabelHrInfo
            // 
            this.toolStripLabelHrInfo.Font = new System.Drawing.Font("Arial", 8F);
            this.toolStripLabelHrInfo.Name = "toolStripLabelHrInfo";
            this.toolStripLabelHrInfo.Size = new System.Drawing.Size(21, 26);
            this.toolStripLabelHrInfo.Text = "HR";
            // 
            // statusStrip1
            // 
            this.statusStrip1.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 841);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(2, 0, 23, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1533, 23);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "status line: MM23: point1(11:12:07.12, 1.201mV) - Click second point";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripStatusLabel1.ForeColor = System.Drawing.Color.White;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(485, 18);
            this.toolStripStatusLabel1.Text = "MM23: point1(11:12:07.12, 1.201mV) - Click second point";
            this.toolStripStatusLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel13);
            this.panel1.Controls.Add(this.panel12);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1533, 106);
            this.panel1.TabIndex = 3;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel43);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(1073, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(236, 104);
            this.panel2.TabIndex = 6;
            // 
            // panel43
            // 
            this.panel43.Controls.Add(this.panel45);
            this.panel43.Controls.Add(this.panel46);
            this.panel43.Controls.Add(this.panel44);
            this.panel43.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel43.Location = new System.Drawing.Point(18, 0);
            this.panel43.Name = "panel43";
            this.panel43.Size = new System.Drawing.Size(218, 104);
            this.panel43.TabIndex = 2;
            // 
            // panel45
            // 
            this.panel45.Controls.Add(this.labelStripTime);
            this.panel45.Controls.Add(this.labeAllChannels);
            this.panel45.Controls.Add(this.labelWholeStrip);
            this.panel45.Controls.Add(this.label11);
            this.panel45.Controls.Add(this.label7);
            this.panel45.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel45.Location = new System.Drawing.Point(68, 0);
            this.panel45.Name = "panel45";
            this.panel45.Size = new System.Drawing.Size(101, 104);
            this.panel45.TabIndex = 1;
            // 
            // labelStripTime
            // 
            this.labelStripTime.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripTime.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStripTime.Location = new System.Drawing.Point(0, 80);
            this.labelStripTime.Name = "labelStripTime";
            this.labelStripTime.Size = new System.Drawing.Size(101, 20);
            this.labelStripTime.TabIndex = 310;
            this.labelStripTime.Text = "Strip time (sec)";
            // 
            // labeAllChannels
            // 
            this.labeAllChannels.Dock = System.Windows.Forms.DockStyle.Top;
            this.labeAllChannels.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labeAllChannels.Location = new System.Drawing.Point(0, 60);
            this.labeAllChannels.Name = "labeAllChannels";
            this.labeAllChannels.Size = new System.Drawing.Size(101, 20);
            this.labeAllChannels.TabIndex = 309;
            this.labeAllChannels.Text = "All Channels";
            // 
            // labelWholeStrip
            // 
            this.labelWholeStrip.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelWholeStrip.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelWholeStrip.Location = new System.Drawing.Point(0, 40);
            this.labelWholeStrip.Name = "labelWholeStrip";
            this.labelWholeStrip.Size = new System.Drawing.Size(101, 20);
            this.labelWholeStrip.TabIndex = 308;
            this.labelWholeStrip.Text = "Full Event";
            // 
            // label11
            // 
            this.label11.Dock = System.Windows.Forms.DockStyle.Top;
            this.label11.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(0, 20);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(101, 20);
            this.label11.TabIndex = 307;
            this.label11.Text = "Full Disclosure";
            // 
            // label7
            // 
            this.label7.Dock = System.Windows.Forms.DockStyle.Top;
            this.label7.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(0, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(101, 20);
            this.label7.TabIndex = 306;
            this.label7.Text = "Anonymize";
            // 
            // panel46
            // 
            this.panel46.Controls.Add(this.checkBoxBlackWhite);
            this.panel46.Controls.Add(this.panel33);
            this.panel46.Controls.Add(this.checkBoxCh2);
            this.panel46.Controls.Add(this.label5);
            this.panel46.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel46.Location = new System.Drawing.Point(0, 0);
            this.panel46.Name = "panel46";
            this.panel46.Size = new System.Drawing.Size(68, 104);
            this.panel46.TabIndex = 2;
            // 
            // checkBoxBlackWhite
            // 
            this.checkBoxBlackWhite.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkBoxBlackWhite.Location = new System.Drawing.Point(0, 40);
            this.checkBoxBlackWhite.Name = "checkBoxBlackWhite";
            this.checkBoxBlackWhite.Size = new System.Drawing.Size(68, 20);
            this.checkBoxBlackWhite.TabIndex = 306;
            this.checkBoxBlackWhite.Text = "B&&W";
            this.checkBoxBlackWhite.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxBlackWhite.UseVisualStyleBackColor = true;
            // 
            // panel33
            // 
            this.panel33.Controls.Add(this.panel34);
            this.panel33.Controls.Add(this.buttonExportData);
            this.panel33.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel33.Location = new System.Drawing.Point(0, 63);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(68, 41);
            this.panel33.TabIndex = 305;
            // 
            // panel34
            // 
            this.panel34.Controls.Add(this.label6);
            this.panel34.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel34.Location = new System.Drawing.Point(36, 0);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(32, 41);
            this.panel34.TabIndex = 305;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Top;
            this.label6.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 16);
            this.label6.TabIndex = 0;
            this.label6.Text = "perm";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // buttonExportData
            // 
            this.buttonExportData.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonExportData.Image = ((System.Drawing.Image)(resources.GetObject("buttonExportData.Image")));
            this.buttonExportData.Location = new System.Drawing.Point(0, 0);
            this.buttonExportData.Name = "buttonExportData";
            this.buttonExportData.Size = new System.Drawing.Size(36, 41);
            this.buttonExportData.TabIndex = 304;
            this.buttonExportData.UseVisualStyleBackColor = true;
            this.buttonExportData.Click += new System.EventHandler(this.buttonExportData_Click);
            // 
            // checkBoxCh2
            // 
            this.checkBoxCh2.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkBoxCh2.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxCh2.Location = new System.Drawing.Point(0, 20);
            this.checkBoxCh2.Name = "checkBoxCh2";
            this.checkBoxCh2.Size = new System.Drawing.Size(68, 20);
            this.checkBoxCh2.TabIndex = 304;
            this.checkBoxCh2.Text = "Ch 2";
            this.checkBoxCh2.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 20);
            this.label5.TabIndex = 302;
            this.label5.Text = "Print:";
            // 
            // panel44
            // 
            this.panel44.Controls.Add(this.comboBoxStripSec);
            this.panel44.Controls.Add(this.checkBoxAllChannels);
            this.panel44.Controls.Add(this.checkBoxHoleStrip);
            this.panel44.Controls.Add(this.checkBoxAddDisclosure);
            this.panel44.Controls.Add(this.checkBoxAnonymize);
            this.panel44.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel44.Location = new System.Drawing.Point(169, 0);
            this.panel44.Name = "panel44";
            this.panel44.Size = new System.Drawing.Size(49, 104);
            this.panel44.TabIndex = 0;
            // 
            // comboBoxStripSec
            // 
            this.comboBoxStripSec.Dock = System.Windows.Forms.DockStyle.Top;
            this.comboBoxStripSec.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxStripSec.FormattingEnabled = true;
            this.comboBoxStripSec.Items.AddRange(new object[] {
            "6",
            "12",
            "30",
            "60",
            "120",
            "300"});
            this.comboBoxStripSec.Location = new System.Drawing.Point(0, 80);
            this.comboBoxStripSec.Name = "comboBoxStripSec";
            this.comboBoxStripSec.Size = new System.Drawing.Size(49, 28);
            this.comboBoxStripSec.TabIndex = 306;
            this.comboBoxStripSec.Text = "60";
            // 
            // checkBoxAllChannels
            // 
            this.checkBoxAllChannels.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkBoxAllChannels.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxAllChannels.Location = new System.Drawing.Point(0, 60);
            this.checkBoxAllChannels.Name = "checkBoxAllChannels";
            this.checkBoxAllChannels.Size = new System.Drawing.Size(49, 20);
            this.checkBoxAllChannels.TabIndex = 305;
            this.checkBoxAllChannels.Text = "AC";
            this.checkBoxAllChannels.UseVisualStyleBackColor = true;
            // 
            // checkBoxHoleStrip
            // 
            this.checkBoxHoleStrip.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkBoxHoleStrip.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxHoleStrip.Location = new System.Drawing.Point(0, 40);
            this.checkBoxHoleStrip.Name = "checkBoxHoleStrip";
            this.checkBoxHoleStrip.Size = new System.Drawing.Size(49, 20);
            this.checkBoxHoleStrip.TabIndex = 304;
            this.checkBoxHoleStrip.Text = "FE";
            this.checkBoxHoleStrip.UseVisualStyleBackColor = true;
            // 
            // checkBoxAddDisclosure
            // 
            this.checkBoxAddDisclosure.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkBoxAddDisclosure.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxAddDisclosure.Location = new System.Drawing.Point(0, 20);
            this.checkBoxAddDisclosure.Name = "checkBoxAddDisclosure";
            this.checkBoxAddDisclosure.Size = new System.Drawing.Size(49, 20);
            this.checkBoxAddDisclosure.TabIndex = 2;
            this.checkBoxAddDisclosure.Text = "FD";
            this.checkBoxAddDisclosure.UseVisualStyleBackColor = true;
            this.checkBoxAddDisclosure.CheckedChanged += new System.EventHandler(this.checkBoxAddDisclosure_CheckedChanged_1);
            // 
            // checkBoxAnonymize
            // 
            this.checkBoxAnonymize.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkBoxAnonymize.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxAnonymize.Location = new System.Drawing.Point(0, 0);
            this.checkBoxAnonymize.Name = "checkBoxAnonymize";
            this.checkBoxAnonymize.Size = new System.Drawing.Size(49, 20);
            this.checkBoxAnonymize.TabIndex = 1;
            this.checkBoxAnonymize.Text = "An";
            this.checkBoxAnonymize.UseVisualStyleBackColor = true;
            this.checkBoxAnonymize.CheckStateChanged += new System.EventHandler(this.checkBoxAnonymize_CheckStateChanged);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.panel18);
            this.panel4.Controls.Add(this.panel19);
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(726, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(583, 104);
            this.panel4.TabIndex = 5;
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.panel29);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel18.Location = new System.Drawing.Point(0, 46);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(583, 58);
            this.panel18.TabIndex = 1;
            // 
            // panel29
            // 
            this.panel29.Controls.Add(this.textBoxEventRemark);
            this.panel29.Controls.Add(this.label2);
            this.panel29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel29.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel29.Location = new System.Drawing.Point(0, 0);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(583, 58);
            this.panel29.TabIndex = 2;
            this.toolTip1.SetToolTip(this.panel29, "Record event");
            // 
            // textBoxEventRemark
            // 
            this.textBoxEventRemark.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.textBoxEventRemark.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxEventRemark.Cursor = System.Windows.Forms.Cursors.Default;
            this.textBoxEventRemark.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxEventRemark.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxEventRemark.Location = new System.Drawing.Point(52, 0);
            this.textBoxEventRemark.Multiline = true;
            this.textBoxEventRemark.Name = "textBoxEventRemark";
            this.textBoxEventRemark.ReadOnly = true;
            this.textBoxEventRemark.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxEventRemark.Size = new System.Drawing.Size(531, 58);
            this.textBoxEventRemark.TabIndex = 4;
            this.textBoxEventRemark.TabStop = false;
            this.textBoxEventRemark.Text = "<remark>\r\nfdhgagfhja\r\ndhjashjl\r\nghdgahgfagkhfhasgjd";
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Left;
            this.label2.Font = new System.Drawing.Font("Arial", 10F);
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 58);
            this.label2.TabIndex = 3;
            this.label2.Text = "Rem:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.panel17);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel19.Location = new System.Drawing.Point(0, 24);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(583, 22);
            this.panel19.TabIndex = 2;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.labelEventTime);
            this.panel17.Controls.Add(this.label3);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel17.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel17.Location = new System.Drawing.Point(0, 0);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(583, 19);
            this.panel17.TabIndex = 1;
            this.toolTip1.SetToolTip(this.panel17, "Record event");
            // 
            // labelEventTime
            // 
            this.labelEventTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelEventTime.Font = new System.Drawing.Font("Arial", 14F);
            this.labelEventTime.Location = new System.Drawing.Point(12, 0);
            this.labelEventTime.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelEventTime.Name = "labelEventTime";
            this.labelEventTime.Size = new System.Drawing.Size(571, 19);
            this.labelEventTime.TabIndex = 4;
            this.labelEventTime.Text = "^2016/10/10 12:20:15 AM +12:59";
            this.labelEventTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelEventTime.Click += new System.EventHandler(this.labelEventTime_Click);
            this.labelEventTime.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.labelEventTime_MouseDoubleClick);
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Left;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(12, 19);
            this.label3.TabIndex = 3;
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.labelEventLabel);
            this.panel5.Controls.Add(this.labelEventPriority);
            this.panel5.Controls.Add(this.label8);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(583, 24);
            this.panel5.TabIndex = 0;
            this.toolTip1.SetToolTip(this.panel5, "Record event");
            // 
            // labelEventLabel
            // 
            this.labelEventLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelEventLabel.Font = new System.Drawing.Font("Arial", 14F);
            this.labelEventLabel.Location = new System.Drawing.Point(108, 0);
            this.labelEventLabel.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelEventLabel.Name = "labelEventLabel";
            this.labelEventLabel.Size = new System.Drawing.Size(475, 24);
            this.labelEventLabel.TabIndex = 5;
            this.labelEventLabel.Text = "Automatic - jARITHMIA-AF";
            this.labelEventLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelEventLabel.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.labelEventLabel_MouseDoubleClick);
            // 
            // labelEventPriority
            // 
            this.labelEventPriority.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelEventPriority.Font = new System.Drawing.Font("Arial", 14F);
            this.labelEventPriority.Location = new System.Drawing.Point(72, 0);
            this.labelEventPriority.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelEventPriority.Name = "labelEventPriority";
            this.labelEventPriority.Size = new System.Drawing.Size(36, 24);
            this.labelEventPriority.TabIndex = 4;
            this.labelEventPriority.Text = "High";
            this.labelEventPriority.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelEventPriority.Visible = false;
            this.labelEventPriority.Click += new System.EventHandler(this.labelEventPriority_Click);
            this.labelEventPriority.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.labelEventPriority_MouseDoubleClick);
            // 
            // label8
            // 
            this.label8.Dock = System.Windows.Forms.DockStyle.Left;
            this.label8.Font = new System.Drawing.Font("Arial", 14F);
            this.label8.Location = new System.Drawing.Point(0, 0);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 24);
            this.label8.TabIndex = 300;
            this.label8.Text = "Event:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label8.Click += new System.EventHandler(this.label8_Click);
            this.label8.DoubleClick += new System.EventHandler(this.label8_DoubleClick);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.textBoxStudyRemark);
            this.panel3.Controls.Add(this.panel20);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(298, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(428, 104);
            this.panel3.TabIndex = 4;
            this.toolTip1.SetToolTip(this.panel3, "View Procedure Data");
            // 
            // textBoxStudyRemark
            // 
            this.textBoxStudyRemark.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.textBoxStudyRemark.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxStudyRemark.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxStudyRemark.Font = new System.Drawing.Font("Arial", 12F);
            this.textBoxStudyRemark.Location = new System.Drawing.Point(0, 31);
            this.textBoxStudyRemark.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.textBoxStudyRemark.Multiline = true;
            this.textBoxStudyRemark.Name = "textBoxStudyRemark";
            this.textBoxStudyRemark.ReadOnly = true;
            this.textBoxStudyRemark.Size = new System.Drawing.Size(428, 73);
            this.textBoxStudyRemark.TabIndex = 4;
            this.textBoxStudyRemark.Text = "<procedure code>\r\n2\r\n1234567890123456789012345678901234567890\r\n<remark>";
            this.toolTip1.SetToolTip(this.textBoxStudyRemark, "View Procedure Data");
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.labelStudyNote);
            this.panel20.Controls.Add(this.labelStudyProcCodes);
            this.panel20.Controls.Add(this.buttonStudyFindings);
            this.panel20.Controls.Add(this.labelStudy);
            this.panel20.Controls.Add(this.labelStudyText);
            this.panel20.Controls.Add(this.buttonPdfView);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel20.Location = new System.Drawing.Point(0, 0);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(428, 31);
            this.panel20.TabIndex = 4;
            // 
            // labelStudyNote
            // 
            this.labelStudyNote.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelStudyNote.Font = new System.Drawing.Font("Arial", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStudyNote.Location = new System.Drawing.Point(286, 0);
            this.labelStudyNote.Name = "labelStudyNote";
            this.labelStudyNote.Size = new System.Drawing.Size(22, 31);
            this.labelStudyNote.TabIndex = 7;
            this.labelStudyNote.Text = "sl\r\nsn";
            this.labelStudyNote.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStudyProcCodes
            // 
            this.labelStudyProcCodes.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelStudyProcCodes.Font = new System.Drawing.Font("Arial", 12F);
            this.labelStudyProcCodes.Location = new System.Drawing.Point(211, 0);
            this.labelStudyProcCodes.Name = "labelStudyProcCodes";
            this.labelStudyProcCodes.Size = new System.Drawing.Size(75, 31);
            this.labelStudyProcCodes.TabIndex = 4;
            this.labelStudyProcCodes.Text = "<codes>";
            this.labelStudyProcCodes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolTip1.SetToolTip(this.labelStudyProcCodes, "Study number");
            // 
            // buttonStudyFindings
            // 
            this.buttonStudyFindings.BackColor = System.Drawing.SystemColors.Control;
            this.buttonStudyFindings.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonStudyFindings.Font = new System.Drawing.Font("Arial", 12F);
            this.buttonStudyFindings.Image = ((System.Drawing.Image)(resources.GetObject("buttonStudyFindings.Image")));
            this.buttonStudyFindings.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonStudyFindings.Location = new System.Drawing.Point(284, 0);
            this.buttonStudyFindings.Name = "buttonStudyFindings";
            this.buttonStudyFindings.Size = new System.Drawing.Size(109, 31);
            this.buttonStudyFindings.TabIndex = 5;
            this.buttonStudyFindings.Text = "Findings";
            this.buttonStudyFindings.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonStudyFindings.UseVisualStyleBackColor = false;
            this.buttonStudyFindings.Click += new System.EventHandler(this.buttonStudyFindings_Click);
            // 
            // labelStudy
            // 
            this.labelStudy.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelStudy.Location = new System.Drawing.Point(111, 0);
            this.labelStudy.Name = "labelStudy";
            this.labelStudy.Size = new System.Drawing.Size(100, 31);
            this.labelStudy.TabIndex = 3;
            this.labelStudy.Text = "-^654321";
            this.labelStudy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolTip1.SetToolTip(this.labelStudy, "Study number .  Event number");
            // 
            // labelStudyText
            // 
            this.labelStudyText.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelStudyText.Font = new System.Drawing.Font("Arial", 14F);
            this.labelStudyText.Image = ((System.Drawing.Image)(resources.GetObject("labelStudyText.Image")));
            this.labelStudyText.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelStudyText.Location = new System.Drawing.Point(0, 0);
            this.labelStudyText.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelStudyText.Name = "labelStudyText";
            this.labelStudyText.Size = new System.Drawing.Size(111, 31);
            this.labelStudyText.TabIndex = 3;
            this.labelStudyText.Text = "Study:";
            this.labelStudyText.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolTip1.SetToolTip(this.labelStudyText, "View Study Data");
            this.labelStudyText.Click += new System.EventHandler(this.labelStudy_Click);
            // 
            // buttonPdfView
            // 
            this.buttonPdfView.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonPdfView.BackColor = System.Drawing.SystemColors.Control;
            this.buttonPdfView.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonPdfView.Image = ((System.Drawing.Image)(resources.GetObject("buttonPdfView.Image")));
            this.buttonPdfView.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonPdfView.Location = new System.Drawing.Point(393, 0);
            this.buttonPdfView.Name = "buttonPdfView";
            this.buttonPdfView.Size = new System.Drawing.Size(35, 31);
            this.buttonPdfView.TabIndex = 6;
            this.buttonPdfView.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolTip1.SetToolTip(this.buttonPdfView, "View PDF Reports");
            this.buttonPdfView.UseVisualStyleBackColor = false;
            this.buttonPdfView.Click += new System.EventHandler(this.buttonPdfView_Click);
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.panel21);
            this.panel13.Controls.Add(this.panel15);
            this.panel13.Controls.Add(this.panel14);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel13.Location = new System.Drawing.Point(1309, 0);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(222, 104);
            this.panel13.TabIndex = 3;
            // 
            // panel21
            // 
            this.panel21.Controls.Add(this.comboBoxState);
            this.panel21.Controls.Add(this.labelNextState);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel21.Location = new System.Drawing.Point(0, 21);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(222, 34);
            this.panel21.TabIndex = 4;
            // 
            // comboBoxState
            // 
            this.comboBoxState.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxState.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxState.FormattingEnabled = true;
            this.comboBoxState.Location = new System.Drawing.Point(63, 0);
            this.comboBoxState.Name = "comboBoxState";
            this.comboBoxState.Size = new System.Drawing.Size(159, 37);
            this.comboBoxState.TabIndex = 3;
            this.comboBoxState.SelectedIndexChanged += new System.EventHandler(this.comboBoxState_TextChanged);
            // 
            // labelNextState
            // 
            this.labelNextState.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelNextState.Font = new System.Drawing.Font("Arial", 10F);
            this.labelNextState.Location = new System.Drawing.Point(0, 0);
            this.labelNextState.Name = "labelNextState";
            this.labelNextState.Size = new System.Drawing.Size(63, 34);
            this.labelNextState.TabIndex = 4;
            this.labelNextState.Text = "Change state:";
            this.labelNextState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panel15.Controls.Add(this.buttonSaveOk);
            this.panel15.Controls.Add(this.buttonSave);
            this.panel15.Controls.Add(this.buttonPrint);
            this.panel15.Controls.Add(this.buttonHelpDesk);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel15.Location = new System.Drawing.Point(0, 59);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(222, 45);
            this.panel15.TabIndex = 3;
            // 
            // buttonSaveOk
            // 
            this.buttonSaveOk.BackColor = System.Drawing.SystemColors.Control;
            this.buttonSaveOk.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonSaveOk.Image = ((System.Drawing.Image)(resources.GetObject("buttonSaveOk.Image")));
            this.buttonSaveOk.Location = new System.Drawing.Point(168, 0);
            this.buttonSaveOk.Name = "buttonSaveOk";
            this.buttonSaveOk.Size = new System.Drawing.Size(54, 45);
            this.buttonSaveOk.TabIndex = 2;
            this.toolTip1.SetToolTip(this.buttonSaveOk, "Save Analyzed Event");
            this.buttonSaveOk.UseVisualStyleBackColor = false;
            this.buttonSaveOk.Click += new System.EventHandler(this.buttonSaveOk_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.BackColor = System.Drawing.SystemColors.Control;
            this.buttonSave.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonSave.Image = ((System.Drawing.Image)(resources.GetObject("buttonSave.Image")));
            this.buttonSave.Location = new System.Drawing.Point(111, 0);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(56, 45);
            this.buttonSave.TabIndex = 1;
            this.toolTip1.SetToolTip(this.buttonSave, "Save");
            this.buttonSave.UseVisualStyleBackColor = false;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonPrint
            // 
            this.buttonPrint.BackColor = System.Drawing.SystemColors.Control;
            this.buttonPrint.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonPrint.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonPrint.Image = ((System.Drawing.Image)(resources.GetObject("buttonPrint.Image")));
            this.buttonPrint.Location = new System.Drawing.Point(56, 0);
            this.buttonPrint.Name = "buttonPrint";
            this.buttonPrint.Size = new System.Drawing.Size(55, 45);
            this.buttonPrint.TabIndex = 0;
            this.toolTip1.SetToolTip(this.buttonPrint, "Print Analysis report");
            this.buttonPrint.UseVisualStyleBackColor = false;
            this.buttonPrint.Click += new System.EventHandler(this.buttonPrint_Click);
            // 
            // buttonHelpDesk
            // 
            this.buttonHelpDesk.BackColor = System.Drawing.SystemColors.Control;
            this.buttonHelpDesk.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonHelpDesk.Image = ((System.Drawing.Image)(resources.GetObject("buttonHelpDesk.Image")));
            this.buttonHelpDesk.Location = new System.Drawing.Point(0, 0);
            this.buttonHelpDesk.Name = "buttonHelpDesk";
            this.buttonHelpDesk.Size = new System.Drawing.Size(56, 45);
            this.buttonHelpDesk.TabIndex = 4;
            this.toolTip1.SetToolTip(this.buttonHelpDesk, "Helpdesk");
            this.buttonHelpDesk.UseVisualStyleBackColor = false;
            this.buttonHelpDesk.MouseClick += new System.Windows.Forms.MouseEventHandler(this.buttonHelpDesk_MouseClick);
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.labelUser);
            this.panel14.Controls.Add(this.label22);
            this.panel14.Controls.Add(this.checkedListBox1);
            this.panel14.Controls.Add(this.label20);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel14.Location = new System.Drawing.Point(0, 0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(222, 21);
            this.panel14.TabIndex = 2;
            // 
            // labelUser
            // 
            this.labelUser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelUser.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUser.Location = new System.Drawing.Point(63, 0);
            this.labelUser.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelUser.Name = "labelUser";
            this.labelUser.Size = new System.Drawing.Size(141, 21);
            this.labelUser.TabIndex = 3;
            this.labelUser.Text = "Simon Vlaar jj";
            this.labelUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label22
            // 
            this.label22.Dock = System.Windows.Forms.DockStyle.Right;
            this.label22.Location = new System.Drawing.Point(204, 0);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(18, 21);
            this.label22.TabIndex = 2;
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Items.AddRange(new object[] {
            "All",
            "I",
            "II",
            "III",
            "AVL",
            "AVR",
            "AVF",
            "V1",
            "V2",
            "V3",
            "V4",
            "V5",
            "V6"});
            this.checkedListBox1.Location = new System.Drawing.Point(-69, -1);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(59, 26);
            this.checkedListBox1.TabIndex = 302;
            this.checkedListBox1.Visible = false;
            // 
            // label20
            // 
            this.label20.Dock = System.Windows.Forms.DockStyle.Left;
            this.label20.Font = new System.Drawing.Font("Arial", 12F);
            this.label20.Location = new System.Drawing.Point(0, 0);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(63, 21);
            this.label20.TabIndex = 4;
            this.label20.Text = "User:";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.panel32);
            this.panel12.Controls.Add(this.textBoxPatientRemark);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel12.Location = new System.Drawing.Point(0, 0);
            this.panel12.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(298, 104);
            this.panel12.TabIndex = 2;
            this.toolTip1.SetToolTip(this.panel12, "View Patient Data");
            // 
            // panel32
            // 
            this.panel32.Controls.Add(this.labelPatID);
            this.panel32.Controls.Add(this.labelPatIDHeader);
            this.panel32.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel32.Location = new System.Drawing.Point(0, 0);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(298, 31);
            this.panel32.TabIndex = 3;
            // 
            // labelPatID
            // 
            this.labelPatID.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPatID.Font = new System.Drawing.Font("Arial", 12F);
            this.labelPatID.Location = new System.Drawing.Point(107, 0);
            this.labelPatID.Name = "labelPatID";
            this.labelPatID.Size = new System.Drawing.Size(191, 31);
            this.labelPatID.TabIndex = 2;
            this.labelPatID.Text = "-";
            this.labelPatID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolTip1.SetToolTip(this.labelPatID, "Patient label");
            // 
            // labelPatIDHeader
            // 
            this.labelPatIDHeader.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPatIDHeader.Font = new System.Drawing.Font("Arial", 14F);
            this.labelPatIDHeader.Image = ((System.Drawing.Image)(resources.GetObject("labelPatIDHeader.Image")));
            this.labelPatIDHeader.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelPatIDHeader.Location = new System.Drawing.Point(0, 0);
            this.labelPatIDHeader.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelPatIDHeader.Name = "labelPatIDHeader";
            this.labelPatIDHeader.Size = new System.Drawing.Size(107, 31);
            this.labelPatIDHeader.TabIndex = 1;
            this.labelPatIDHeader.Text = "Patient:";
            this.labelPatIDHeader.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolTip1.SetToolTip(this.labelPatIDHeader, "View Patient Data");
            // 
            // textBoxPatientRemark
            // 
            this.textBoxPatientRemark.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.textBoxPatientRemark.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxPatientRemark.Cursor = System.Windows.Forms.Cursors.Default;
            this.textBoxPatientRemark.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textBoxPatientRemark.Font = new System.Drawing.Font("Arial", 12F);
            this.textBoxPatientRemark.Location = new System.Drawing.Point(0, 32);
            this.textBoxPatientRemark.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.textBoxPatientRemark.Multiline = true;
            this.textBoxPatientRemark.Name = "textBoxPatientRemark";
            this.textBoxPatientRemark.ReadOnly = true;
            this.textBoxPatientRemark.Size = new System.Drawing.Size(298, 72);
            this.textBoxPatientRemark.TabIndex = 2;
            this.textBoxPatientRemark.TabStop = false;
            this.textBoxPatientRemark.Text = "<last>, <Midle>, First1233445565 \\n\r\n22 years, male\r\n\\n<remark>";
            this.toolTip1.SetToolTip(this.textBoxPatientRemark, "View Patient Data");
            // 
            // contextMenuStripN
            // 
            this.contextMenuStripN.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem2,
            this.toolStripMenuItem3,
            this.toolStripMenuItem4,
            this.toolStripSeparator8,
            this.toolStripMenuItem5,
            this.toolStripMenuItem6,
            this.toolStripSeparator9,
            this.toolStripMenuItem7,
            this.toolStripMenuItem8,
            this.toolStripSeparator7,
            this.toolStripMenuItem9,
            this.verticalUnitToolStripMenuItem,
            this.lineTypeToolStripMenuItem});
            this.contextMenuStripN.Name = "contextMenuStripStripY";
            this.contextMenuStripN.Size = new System.Drawing.Size(193, 264);
            this.contextMenuStripN.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStripN_Opening);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(192, 22);
            this.toolStripMenuItem1.Text = "Zoom In";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(192, 22);
            this.toolStripMenuItem2.Text = "Zoom Out";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(192, 22);
            this.toolStripMenuItem3.Text = "Zoom Auto";
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(192, 22);
            this.toolStripMenuItem4.Text = "Zoom Top";
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(189, 6);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(192, 22);
            this.toolStripMenuItem5.Text = "Move Up";
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(192, 22);
            this.toolStripMenuItem6.Text = "Move Down";
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(189, 6);
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(192, 22);
            this.toolStripMenuItem7.Text = "Increase N Time Lines";
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(192, 22);
            this.toolStripMenuItem8.Text = "Decrease N Time Lines";
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(189, 6);
            // 
            // toolStripMenuItem9
            // 
            this.toolStripMenuItem9.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.secToolStripMenuItem5,
            this.secToolStripMenuItem6,
            this.secToolStripMenuItem7,
            this.toolStripMenuItem11,
            this.mSecToolStripMenuItem,
            this.toolStripMenuItem12,
            this.secToolStripMenuItem8,
            this.secToolStripMenuItem9,
            this.secToolStripMenuItem10,
            this.secToolStripMenuItem11,
            this.secToolStripMenuItem12});
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            this.toolStripMenuItem9.Size = new System.Drawing.Size(192, 22);
            this.toolStripMenuItem9.Text = "Time unit";
            // 
            // secToolStripMenuItem5
            // 
            this.secToolStripMenuItem5.Name = "secToolStripMenuItem5";
            this.secToolStripMenuItem5.Size = new System.Drawing.Size(193, 22);
            this.secToolStripMenuItem5.Text = "500 mm/Sec (0.01 Sec)";
            this.secToolStripMenuItem5.Click += new System.EventHandler(this.secToolStripMenuItem5_Click);
            // 
            // secToolStripMenuItem6
            // 
            this.secToolStripMenuItem6.Name = "secToolStripMenuItem6";
            this.secToolStripMenuItem6.Size = new System.Drawing.Size(193, 22);
            this.secToolStripMenuItem6.Text = "250 mm/Sec (0.02 Sec";
            // 
            // secToolStripMenuItem7
            // 
            this.secToolStripMenuItem7.Name = "secToolStripMenuItem7";
            this.secToolStripMenuItem7.Size = new System.Drawing.Size(193, 22);
            this.secToolStripMenuItem7.Text = "100 mm/Sec (0.05 Sec)";
            // 
            // toolStripMenuItem11
            // 
            this.toolStripMenuItem11.Name = "toolStripMenuItem11";
            this.toolStripMenuItem11.Size = new System.Drawing.Size(193, 22);
            this.toolStripMenuItem11.Text = "50 mm/Sec (0.10 Sec)";
            // 
            // mSecToolStripMenuItem
            // 
            this.mSecToolStripMenuItem.Name = "mSecToolStripMenuItem";
            this.mSecToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.mSecToolStripMenuItem.Text = "25 mm/Sec (0.20 Sec)";
            // 
            // toolStripMenuItem12
            // 
            this.toolStripMenuItem12.Name = "toolStripMenuItem12";
            this.toolStripMenuItem12.Size = new System.Drawing.Size(193, 22);
            this.toolStripMenuItem12.Text = "10 mm/Sec (0.50 Sec)";
            // 
            // secToolStripMenuItem8
            // 
            this.secToolStripMenuItem8.Name = "secToolStripMenuItem8";
            this.secToolStripMenuItem8.Size = new System.Drawing.Size(193, 22);
            this.secToolStripMenuItem8.Text = "5 mm/Sec (1 Sec)";
            // 
            // secToolStripMenuItem9
            // 
            this.secToolStripMenuItem9.Name = "secToolStripMenuItem9";
            this.secToolStripMenuItem9.Size = new System.Drawing.Size(193, 22);
            this.secToolStripMenuItem9.Text = "2.5 mm/Sec (2 Sec)";
            // 
            // secToolStripMenuItem10
            // 
            this.secToolStripMenuItem10.Name = "secToolStripMenuItem10";
            this.secToolStripMenuItem10.Size = new System.Drawing.Size(193, 22);
            this.secToolStripMenuItem10.Text = "1 mm/Sec (5 Sec)";
            // 
            // secToolStripMenuItem11
            // 
            this.secToolStripMenuItem11.Name = "secToolStripMenuItem11";
            this.secToolStripMenuItem11.Size = new System.Drawing.Size(193, 22);
            this.secToolStripMenuItem11.Text = "0.5 mm/Sec (10 Sec)";
            // 
            // secToolStripMenuItem12
            // 
            this.secToolStripMenuItem12.Name = "secToolStripMenuItem12";
            this.secToolStripMenuItem12.Size = new System.Drawing.Size(193, 22);
            this.secToolStripMenuItem12.Text = "0.25 mm/Sec (20 Sec)";
            // 
            // verticalUnitToolStripMenuItem
            // 
            this.verticalUnitToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mVToolStripMenuItem,
            this.mVToolStripMenuItem1,
            this.mVToolStripMenuItem2,
            this.mVToolStripMenuItem3,
            this.mVToolStripMenuItem4,
            this.mVToolStripMenuItem5,
            this.mVToolStripMenuItem6});
            this.verticalUnitToolStripMenuItem.Name = "verticalUnitToolStripMenuItem";
            this.verticalUnitToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.verticalUnitToolStripMenuItem.Text = "Amplitude Unit";
            // 
            // mVToolStripMenuItem
            // 
            this.mVToolStripMenuItem.Name = "mVToolStripMenuItem";
            this.mVToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.mVToolStripMenuItem.Text = "50 mm/mV (0.1 mV)";
            // 
            // mVToolStripMenuItem1
            // 
            this.mVToolStripMenuItem1.Name = "mVToolStripMenuItem1";
            this.mVToolStripMenuItem1.Size = new System.Drawing.Size(184, 22);
            this.mVToolStripMenuItem1.Text = "25 mm/mV (0.2 mV)";
            // 
            // mVToolStripMenuItem2
            // 
            this.mVToolStripMenuItem2.Name = "mVToolStripMenuItem2";
            this.mVToolStripMenuItem2.Size = new System.Drawing.Size(184, 22);
            this.mVToolStripMenuItem2.Text = "10 mm/mV (0.5 mV";
            // 
            // mVToolStripMenuItem3
            // 
            this.mVToolStripMenuItem3.Name = "mVToolStripMenuItem3";
            this.mVToolStripMenuItem3.Size = new System.Drawing.Size(184, 22);
            this.mVToolStripMenuItem3.Text = "5 mm/mV (1.0 mV";
            // 
            // mVToolStripMenuItem4
            // 
            this.mVToolStripMenuItem4.Name = "mVToolStripMenuItem4";
            this.mVToolStripMenuItem4.Size = new System.Drawing.Size(184, 22);
            this.mVToolStripMenuItem4.Text = "2.5 mm/mV (2.0 mV)";
            // 
            // mVToolStripMenuItem5
            // 
            this.mVToolStripMenuItem5.Name = "mVToolStripMenuItem5";
            this.mVToolStripMenuItem5.Size = new System.Drawing.Size(184, 22);
            this.mVToolStripMenuItem5.Text = "1 mm/mV (5.0 mV)";
            // 
            // mVToolStripMenuItem6
            // 
            this.mVToolStripMenuItem6.Name = "mVToolStripMenuItem6";
            this.mVToolStripMenuItem6.Size = new System.Drawing.Size(184, 22);
            this.mVToolStripMenuItem6.Text = "0.5 mm/mV (10 mV)";
            // 
            // lineTypeToolStripMenuItem
            // 
            this.lineTypeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pointToolStripMenuItem,
            this.lineToolStripMenuItem});
            this.lineTypeToolStripMenuItem.Name = "lineTypeToolStripMenuItem";
            this.lineTypeToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.lineTypeToolStripMenuItem.Text = "Line Type";
            // 
            // pointToolStripMenuItem
            // 
            this.pointToolStripMenuItem.Name = "pointToolStripMenuItem";
            this.pointToolStripMenuItem.Size = new System.Drawing.Size(102, 22);
            this.pointToolStripMenuItem.Text = "Point";
            // 
            // lineToolStripMenuItem
            // 
            this.lineToolStripMenuItem.Name = "lineToolStripMenuItem";
            this.lineToolStripMenuItem.Size = new System.Drawing.Size(102, 22);
            this.lineToolStripMenuItem.Text = "Line";
            // 
            // toolTip1
            // 
            this.toolTip1.AutoPopDelay = 5000;
            this.toolTip1.InitialDelay = 300;
            this.toolTip1.ReshowDelay = 100;
            // 
            // panelZoomStrip
            // 
            this.panelZoomStrip.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelZoomStrip.BackgroundImage")));
            this.panelZoomStrip.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelZoomStrip.Cursor = System.Windows.Forms.Cursors.Cross;
            this.panelZoomStrip.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelZoomStrip.Location = new System.Drawing.Point(0, 0);
            this.panelZoomStrip.Name = "panelZoomStrip";
            this.panelZoomStrip.Size = new System.Drawing.Size(1310, 162);
            this.panelZoomStrip.TabIndex = 2;
            this.toolTip1.SetToolTip(this.panelZoomStrip, "Measure window");
            this.panelZoomStrip.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panelZoomStrip_MouseDown);
            this.panelZoomStrip.MouseLeave += new System.EventHandler(this.panelZoomStrip_MouseLeave);
            this.panelZoomStrip.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panelZoomStrip_MouseMove);
            this.panelZoomStrip.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panelZoomStrip_MouseUp);
            // 
            // listViewStats
            // 
            this.listViewStats.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.cStag,
            this.cSn,
            this.cSmin,
            this.cSmax,
            this.cSmean,
            this.cSunit});
            this.listViewStats.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewStats.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listViewStats.FullRowSelect = true;
            this.listViewStats.GridLines = true;
            this.listViewStats.HideSelection = false;
            this.listViewStats.Location = new System.Drawing.Point(0, 39);
            this.listViewStats.Name = "listViewStats";
            this.listViewStats.Size = new System.Drawing.Size(323, 137);
            this.listViewStats.TabIndex = 5;
            this.toolTip1.SetToolTip(this.listViewStats, "Statistics");
            this.listViewStats.UseCompatibleStateImageBehavior = false;
            this.listViewStats.View = System.Windows.Forms.View.Details;
            this.listViewStats.SelectedIndexChanged += new System.EventHandler(this.listViewStats_SelectedIndexChanged);
            // 
            // cStag
            // 
            this.cStag.Text = "Tag";
            this.cStag.Width = 70;
            // 
            // cSn
            // 
            this.cSn.Text = "N";
            this.cSn.Width = 29;
            // 
            // cSmin
            // 
            this.cSmin.Text = "Min";
            this.cSmin.Width = 55;
            // 
            // cSmax
            // 
            this.cSmax.Text = "Max";
            this.cSmax.Width = 49;
            // 
            // cSmean
            // 
            this.cSmean.Text = "Mean";
            this.cSmean.Width = 54;
            // 
            // cSunit
            // 
            this.cSunit.Text = "Unit";
            this.cSunit.Width = 44;
            // 
            // listViewMeasurements
            // 
            this.listViewMeasurements.CheckBoxes = true;
            this.listViewMeasurements.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.cMtime,
            this.cMchannel,
            this.cMtag,
            this.cMvalue,
            this.cMunit});
            this.listViewMeasurements.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewMeasurements.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listViewMeasurements.FullRowSelect = true;
            this.listViewMeasurements.GridLines = true;
            this.listViewMeasurements.HideSelection = false;
            this.listViewMeasurements.Location = new System.Drawing.Point(0, 39);
            this.listViewMeasurements.MultiSelect = false;
            this.listViewMeasurements.Name = "listViewMeasurements";
            this.listViewMeasurements.Size = new System.Drawing.Size(324, 137);
            this.listViewMeasurements.TabIndex = 4;
            this.toolTip1.SetToolTip(this.listViewMeasurements, "Measurements");
            this.listViewMeasurements.UseCompatibleStateImageBehavior = false;
            this.listViewMeasurements.View = System.Windows.Forms.View.Details;
            this.listViewMeasurements.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.listViewMeasurements_ItemChecked);
            this.listViewMeasurements.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.listViewMeasurements_ItemSelectionChanged);
            // 
            // cMtime
            // 
            this.cMtime.Text = "Time";
            this.cMtime.Width = 100;
            // 
            // cMchannel
            // 
            this.cMchannel.Text = "Ch";
            this.cMchannel.Width = 30;
            // 
            // cMtag
            // 
            this.cMtag.Text = "Tag";
            this.cMtag.Width = 74;
            // 
            // cMvalue
            // 
            this.cMvalue.Text = "Value";
            this.cMvalue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.cMvalue.Width = 54;
            // 
            // cMunit
            // 
            this.cMunit.Text = "Unit";
            this.cMunit.Width = 49;
            // 
            // listViewStrips
            // 
            this.listViewStrips.CheckBoxes = true;
            this.listViewStrips.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.cChannel,
            this.cDate,
            this.cTime,
            this.cDuration});
            this.listViewStrips.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewStrips.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listViewStrips.FullRowSelect = true;
            this.listViewStrips.GridLines = true;
            this.listViewStrips.HideSelection = false;
            this.listViewStrips.Location = new System.Drawing.Point(0, 39);
            this.listViewStrips.MultiSelect = false;
            this.listViewStrips.Name = "listViewStrips";
            this.listViewStrips.Size = new System.Drawing.Size(316, 137);
            this.listViewStrips.TabIndex = 4;
            this.toolTip1.SetToolTip(this.listViewStrips, "Measurement strips");
            this.listViewStrips.UseCompatibleStateImageBehavior = false;
            this.listViewStrips.View = System.Windows.Forms.View.Details;
            this.listViewStrips.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.listViewStrips_ItemChecked);
            this.listViewStrips.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.listViewStrips_ItemSelectionChanged);
            // 
            // cChannel
            // 
            this.cChannel.Text = "Ch";
            this.cChannel.Width = 71;
            // 
            // cDate
            // 
            this.cDate.Text = "Date";
            this.cDate.Width = 80;
            // 
            // cTime
            // 
            this.cTime.Text = "Time";
            this.cTime.Width = 100;
            // 
            // cDuration
            // 
            this.cDuration.Text = "Sec";
            this.cDuration.Width = 47;
            // 
            // panelChannel6
            // 
            this.panelChannel6.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelChannel6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelChannel6.BackgroundImage")));
            this.panelChannel6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelChannel6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelChannel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelChannel6.Location = new System.Drawing.Point(0, 375);
            this.panelChannel6.Name = "panelChannel6";
            this.panelChannel6.Size = new System.Drawing.Size(783, 75);
            this.panelChannel6.TabIndex = 19;
            this.toolTip1.SetToolTip(this.panelChannel6, "Channel 6");
            this.panelChannel6.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panelChannel6_MouseClick);
            // 
            // panelChannel5
            // 
            this.panelChannel5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelChannel5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelChannel5.BackgroundImage")));
            this.panelChannel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelChannel5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelChannel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelChannel5.Location = new System.Drawing.Point(0, 300);
            this.panelChannel5.Name = "panelChannel5";
            this.panelChannel5.Size = new System.Drawing.Size(783, 75);
            this.panelChannel5.TabIndex = 18;
            this.toolTip1.SetToolTip(this.panelChannel5, "Channel 5");
            this.panelChannel5.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panelChannel5_MouseClick);
            // 
            // panelChannel4
            // 
            this.panelChannel4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelChannel4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelChannel4.BackgroundImage")));
            this.panelChannel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelChannel4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelChannel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelChannel4.Location = new System.Drawing.Point(0, 225);
            this.panelChannel4.Name = "panelChannel4";
            this.panelChannel4.Size = new System.Drawing.Size(783, 75);
            this.panelChannel4.TabIndex = 17;
            this.toolTip1.SetToolTip(this.panelChannel4, "Channel 4");
            this.panelChannel4.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panelChannel4_MouseClick);
            // 
            // panelChannel3
            // 
            this.panelChannel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelChannel3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelChannel3.BackgroundImage")));
            this.panelChannel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelChannel3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelChannel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelChannel3.Location = new System.Drawing.Point(0, 150);
            this.panelChannel3.Name = "panelChannel3";
            this.panelChannel3.Size = new System.Drawing.Size(783, 75);
            this.panelChannel3.TabIndex = 16;
            this.toolTip1.SetToolTip(this.panelChannel3, "Channel 3");
            this.panelChannel3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panelChannel3b_MouseClick);
            // 
            // panelChannel2
            // 
            this.panelChannel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelChannel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelChannel2.BackgroundImage")));
            this.panelChannel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelChannel2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelChannel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelChannel2.Location = new System.Drawing.Point(0, 75);
            this.panelChannel2.Name = "panelChannel2";
            this.panelChannel2.Size = new System.Drawing.Size(783, 75);
            this.panelChannel2.TabIndex = 14;
            this.toolTip1.SetToolTip(this.panelChannel2, "Channel 2");
            this.panelChannel2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panelChannel2_MouseClick);
            // 
            // panelChannel1
            // 
            this.panelChannel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelChannel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelChannel1.BackgroundImage")));
            this.panelChannel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelChannel1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelChannel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelChannel1.Location = new System.Drawing.Point(0, 0);
            this.panelChannel1.Name = "panelChannel1";
            this.panelChannel1.Size = new System.Drawing.Size(783, 75);
            this.panelChannel1.TabIndex = 12;
            this.toolTip1.SetToolTip(this.panelChannel1, "Channel 1");
            this.panelChannel1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panelChannel1_MouseClick);
            // 
            // panelChannel12
            // 
            this.panelChannel12.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelChannel12.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelChannel12.BackgroundImage")));
            this.panelChannel12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelChannel12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelChannel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelChannel12.Location = new System.Drawing.Point(0, 375);
            this.panelChannel12.Name = "panelChannel12";
            this.panelChannel12.Size = new System.Drawing.Size(729, 75);
            this.panelChannel12.TabIndex = 25;
            this.toolTip1.SetToolTip(this.panelChannel12, "Channel 12");
            this.panelChannel12.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panelChannel12_MouseClick);
            // 
            // panelChannel11
            // 
            this.panelChannel11.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelChannel11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelChannel11.BackgroundImage")));
            this.panelChannel11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelChannel11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelChannel11.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelChannel11.Location = new System.Drawing.Point(0, 300);
            this.panelChannel11.Name = "panelChannel11";
            this.panelChannel11.Size = new System.Drawing.Size(729, 75);
            this.panelChannel11.TabIndex = 24;
            this.toolTip1.SetToolTip(this.panelChannel11, "Channel 11");
            this.panelChannel11.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panelChannel11_MouseClick);
            // 
            // panelChannel10
            // 
            this.panelChannel10.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelChannel10.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelChannel10.BackgroundImage")));
            this.panelChannel10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelChannel10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelChannel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelChannel10.Location = new System.Drawing.Point(0, 225);
            this.panelChannel10.Name = "panelChannel10";
            this.panelChannel10.Size = new System.Drawing.Size(729, 75);
            this.panelChannel10.TabIndex = 23;
            this.toolTip1.SetToolTip(this.panelChannel10, "Channel 10");
            this.panelChannel10.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panelChannel10_MouseClick);
            // 
            // panelChannel9
            // 
            this.panelChannel9.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelChannel9.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelChannel9.BackgroundImage")));
            this.panelChannel9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelChannel9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelChannel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelChannel9.Location = new System.Drawing.Point(0, 150);
            this.panelChannel9.Name = "panelChannel9";
            this.panelChannel9.Size = new System.Drawing.Size(729, 75);
            this.panelChannel9.TabIndex = 22;
            this.toolTip1.SetToolTip(this.panelChannel9, "Channel 9");
            this.panelChannel9.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panelChannel9_MouseClick);
            // 
            // panelChannel8
            // 
            this.panelChannel8.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelChannel8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelChannel8.BackgroundImage")));
            this.panelChannel8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelChannel8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelChannel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelChannel8.Location = new System.Drawing.Point(0, 75);
            this.panelChannel8.Name = "panelChannel8";
            this.panelChannel8.Size = new System.Drawing.Size(729, 75);
            this.panelChannel8.TabIndex = 21;
            this.toolTip1.SetToolTip(this.panelChannel8, "Channel 8");
            this.panelChannel8.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panelChannel8_MouseClick);
            // 
            // panelChannel7
            // 
            this.panelChannel7.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelChannel7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelChannel7.BackgroundImage")));
            this.panelChannel7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelChannel7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelChannel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelChannel7.Location = new System.Drawing.Point(0, 0);
            this.panelChannel7.Name = "panelChannel7";
            this.panelChannel7.Size = new System.Drawing.Size(729, 75);
            this.panelChannel7.TabIndex = 20;
            this.toolTip1.SetToolTip(this.panelChannel7, "Channel 7");
            this.panelChannel7.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panelChannel7_MouseClick);
            // 
            // toolStripStrips
            // 
            this.toolStripStrips.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStrips.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripStrips.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStripStrips.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripAddStrip,
            this.toolStripDeleteStrip,
            this.toolStripSetDupStrip,
            this.toolStripView,
            this.toolStripCopyWindow,
            this.toolStripFirst,
            this.toolStripBaseLine,
            this.toolStripLabel3});
            this.toolStripStrips.Location = new System.Drawing.Point(0, 0);
            this.toolStripStrips.Name = "toolStripStrips";
            this.toolStripStrips.Size = new System.Drawing.Size(316, 39);
            this.toolStripStrips.TabIndex = 3;
            this.toolStripStrips.Text = "Strips";
            this.toolTip1.SetToolTip(this.toolStripStrips, "ECG Strips");
            this.toolStripStrips.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStripStrips_ItemClicked_1);
            // 
            // toolStripAddStrip
            // 
            this.toolStripAddStrip.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripAddStrip.Image = ((System.Drawing.Image)(resources.GetObject("toolStripAddStrip.Image")));
            this.toolStripAddStrip.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripAddStrip.Name = "toolStripAddStrip";
            this.toolStripAddStrip.Size = new System.Drawing.Size(36, 36);
            this.toolStripAddStrip.Text = "toolStripButton26";
            this.toolStripAddStrip.ToolTipText = "Add New Strip";
            this.toolStripAddStrip.Click += new System.EventHandler(this.toolStripAddStrip_Click);
            // 
            // toolStripDeleteStrip
            // 
            this.toolStripDeleteStrip.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripDeleteStrip.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDeleteStrip.Image")));
            this.toolStripDeleteStrip.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripDeleteStrip.Name = "toolStripDeleteStrip";
            this.toolStripDeleteStrip.Size = new System.Drawing.Size(36, 36);
            this.toolStripDeleteStrip.Text = "toolStripButton27";
            this.toolStripDeleteStrip.ToolTipText = "Remove Strip";
            this.toolStripDeleteStrip.Click += new System.EventHandler(this.toolStripDeleteStrip_Click);
            // 
            // toolStripSetDupStrip
            // 
            this.toolStripSetDupStrip.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripSetDupStrip.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSetDupStrip.Image")));
            this.toolStripSetDupStrip.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripSetDupStrip.Name = "toolStripSetDupStrip";
            this.toolStripSetDupStrip.Size = new System.Drawing.Size(36, 36);
            this.toolStripSetDupStrip.Text = "Duplicate strip";
            this.toolStripSetDupStrip.ToolTipText = "Set Dup Strip";
            this.toolStripSetDupStrip.Click += new System.EventHandler(this.toolStripStripDup_Click);
            // 
            // toolStripView
            // 
            this.toolStripView.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripView.Image = ((System.Drawing.Image)(resources.GetObject("toolStripView.Image")));
            this.toolStripView.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripView.Name = "toolStripView";
            this.toolStripView.Size = new System.Drawing.Size(36, 36);
            this.toolStripView.Text = "toolStripButton28";
            this.toolStripView.ToolTipText = "View Strip";
            this.toolStripView.Click += new System.EventHandler(this.toolStripButtonView_Click);
            // 
            // toolStripCopyWindow
            // 
            this.toolStripCopyWindow.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripCopyWindow.Image = ((System.Drawing.Image)(resources.GetObject("toolStripCopyWindow.Image")));
            this.toolStripCopyWindow.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripCopyWindow.Name = "toolStripCopyWindow";
            this.toolStripCopyWindow.Size = new System.Drawing.Size(36, 36);
            this.toolStripCopyWindow.Text = "toolStripButton34";
            this.toolStripCopyWindow.ToolTipText = "Copy Window to Strip";
            this.toolStripCopyWindow.Click += new System.EventHandler(this.toolStripButtonCopyWindow_Click);
            // 
            // toolStripFirst
            // 
            this.toolStripFirst.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripFirst.Image = ((System.Drawing.Image)(resources.GetObject("toolStripFirst.Image")));
            this.toolStripFirst.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripFirst.Name = "toolStripFirst";
            this.toolStripFirst.Size = new System.Drawing.Size(36, 36);
            this.toolStripFirst.Text = "Move to First";
            this.toolStripFirst.Click += new System.EventHandler(this.toolStripButtonFirst_Click);
            // 
            // toolStripBaseLine
            // 
            this.toolStripBaseLine.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripBaseLine.Image = ((System.Drawing.Image)(resources.GetObject("toolStripBaseLine.Image")));
            this.toolStripBaseLine.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripBaseLine.Name = "toolStripBaseLine";
            this.toolStripBaseLine.Size = new System.Drawing.Size(60, 36);
            this.toolStripBaseLine.Text = "Base Line";
            this.toolStripBaseLine.Click += new System.EventHandler(this.toolStripBaseLine_Click);
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Font = new System.Drawing.Font("Arial", 10F);
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(48, 16);
            this.toolStripLabel3.Text = "Strips:";
            this.toolStripLabel3.Visible = false;
            // 
            // AutoScaleCheckbox
            // 
            this.AutoScaleCheckbox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.AutoScaleCheckbox.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AutoScaleCheckbox.Location = new System.Drawing.Point(0, 138);
            this.AutoScaleCheckbox.Name = "AutoScaleCheckbox";
            this.AutoScaleCheckbox.Size = new System.Drawing.Size(63, 24);
            this.AutoScaleCheckbox.TabIndex = 17;
            this.AutoScaleCheckbox.Text = "Auto A";
            this.toolTip1.SetToolTip(this.AutoScaleCheckbox, "Auto Amplitude Range (off= limit range)");
            this.AutoScaleCheckbox.UseVisualStyleBackColor = true;
            this.AutoScaleCheckbox.CheckStateChanged += new System.EventHandler(this.AutoScaleCheckbox_CheckStateChanged);
            // 
            // buttonCalcRR
            // 
            this.buttonCalcRR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonCalcRR.Font = new System.Drawing.Font("Verdana", 6F);
            this.buttonCalcRR.Location = new System.Drawing.Point(0, 0);
            this.buttonCalcRR.Margin = new System.Windows.Forms.Padding(0);
            this.buttonCalcRR.Name = "buttonCalcRR";
            this.buttonCalcRR.Size = new System.Drawing.Size(20, 16);
            this.buttonCalcRR.TabIndex = 0;
            this.buttonCalcRR.Text = "RR";
            this.buttonCalcRR.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.toolTip1.SetToolTip(this.buttonCalcRR, "Calc RR times");
            this.buttonCalcRR.UseVisualStyleBackColor = true;
            this.buttonCalcRR.Click += new System.EventHandler(this.buttonCalcRR_Click);
            // 
            // miniToolStrip
            // 
            this.miniToolStrip.AutoSize = false;
            this.miniToolStrip.CanOverflow = false;
            this.miniToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.miniToolStrip.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.miniToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.miniToolStrip.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.miniToolStrip.Location = new System.Drawing.Point(90, 10);
            this.miniToolStrip.Name = "miniToolStrip";
            this.miniToolStrip.Size = new System.Drawing.Size(202, 25);
            this.miniToolStrip.TabIndex = 5;
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(45, 22);
            this.toolStripLabel2.Text = "Stats:";
            // 
            // toolStripButton17
            // 
            this.toolStripButton17.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton17.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton17.Image")));
            this.toolStripButton17.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton17.Name = "toolStripButton17";
            this.toolStripButton17.Size = new System.Drawing.Size(36, 22);
            this.toolStripButton17.Text = "toolStripButton35";
            this.toolStripButton17.Visible = false;
            // 
            // toolStrip5
            // 
            this.toolStrip5.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStrip5.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip5.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel2,
            this.toolStripButton17});
            this.toolStrip5.Location = new System.Drawing.Point(3, 19);
            this.toolStrip5.Name = "toolStrip5";
            this.toolStrip5.Size = new System.Drawing.Size(202, 25);
            this.toolStrip5.TabIndex = 5;
            this.toolStrip5.Text = "toolStrip5";
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.panel9);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(0, 106);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(1533, 735);
            this.panel8.TabIndex = 4;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.splitContainer2);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(1533, 735);
            this.panel9.TabIndex = 0;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.BackColor = System.Drawing.Color.Transparent;
            this.splitContainer2.Panel1.CausesValidation = false;
            this.splitContainer2.Panel1.Controls.Add(this.panelZoomStrip);
            this.splitContainer2.Panel1.Controls.Add(this.panel42);
            this.splitContainer2.Panel1.Controls.Add(this.panel28);
            this.splitContainer2.Panel1.Controls.Add(this.panel16);
            this.splitContainer2.Panel1.Controls.Add(this.panel10);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.AutoScroll = true;
            this.splitContainer2.Panel2.Controls.Add(this.splitContainer4);
            this.splitContainer2.Size = new System.Drawing.Size(1533, 735);
            this.splitContainer2.SplitterDistance = 193;
            this.splitContainer2.TabIndex = 1;
            // 
            // panel42
            // 
            this.panel42.Controls.Add(this.buttonAnAbNormal);
            this.panel42.Controls.Add(this.labelCreateSec);
            this.panel42.Controls.Add(this.buttonAnPause);
            this.panel42.Controls.Add(this.buttonAnBrady);
            this.panel42.Controls.Add(this.buttonAnTachy);
            this.panel42.Controls.Add(this.buttonAnNormal);
            this.panel42.Controls.Add(this.label4);
            this.panel42.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel42.Location = new System.Drawing.Point(1310, 0);
            this.panel42.Name = "panel42";
            this.panel42.Size = new System.Drawing.Size(80, 162);
            this.panel42.TabIndex = 6;
            // 
            // buttonAnAbNormal
            // 
            this.buttonAnAbNormal.BackColor = System.Drawing.Color.White;
            this.buttonAnAbNormal.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonAnAbNormal.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAnAbNormal.Location = new System.Drawing.Point(0, 96);
            this.buttonAnAbNormal.Name = "buttonAnAbNormal";
            this.buttonAnAbNormal.Size = new System.Drawing.Size(80, 24);
            this.buttonAnAbNormal.TabIndex = 10;
            this.buttonAnAbNormal.Text = "AbNormal";
            this.buttonAnAbNormal.UseVisualStyleBackColor = false;
            this.buttonAnAbNormal.Click += new System.EventHandler(this.buttonAnAbNormal_Click);
            // 
            // labelCreateSec
            // 
            this.labelCreateSec.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelCreateSec.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCreateSec.Location = new System.Drawing.Point(0, 120);
            this.labelCreateSec.Name = "labelCreateSec";
            this.labelCreateSec.Size = new System.Drawing.Size(80, 18);
            this.labelCreateSec.TabIndex = 8;
            this.labelCreateSec.Text = "c=?";
            // 
            // buttonAnPause
            // 
            this.buttonAnPause.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonAnPause.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonAnPause.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAnPause.Location = new System.Drawing.Point(0, 72);
            this.buttonAnPause.Name = "buttonAnPause";
            this.buttonAnPause.Size = new System.Drawing.Size(80, 24);
            this.buttonAnPause.TabIndex = 7;
            this.buttonAnPause.Text = "Pause";
            this.buttonAnPause.UseVisualStyleBackColor = false;
            this.buttonAnPause.Click += new System.EventHandler(this.buttonAnPause_Click);
            // 
            // buttonAnBrady
            // 
            this.buttonAnBrady.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonAnBrady.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonAnBrady.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAnBrady.Location = new System.Drawing.Point(0, 48);
            this.buttonAnBrady.Name = "buttonAnBrady";
            this.buttonAnBrady.Size = new System.Drawing.Size(80, 24);
            this.buttonAnBrady.TabIndex = 6;
            this.buttonAnBrady.Text = "Brady";
            this.buttonAnBrady.UseVisualStyleBackColor = false;
            this.buttonAnBrady.Click += new System.EventHandler(this.buttonAnBrady_Click);
            // 
            // buttonAnTachy
            // 
            this.buttonAnTachy.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonAnTachy.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonAnTachy.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAnTachy.Location = new System.Drawing.Point(0, 24);
            this.buttonAnTachy.Name = "buttonAnTachy";
            this.buttonAnTachy.Size = new System.Drawing.Size(80, 24);
            this.buttonAnTachy.TabIndex = 5;
            this.buttonAnTachy.Text = "Tachy";
            this.buttonAnTachy.UseVisualStyleBackColor = false;
            this.buttonAnTachy.Click += new System.EventHandler(this.buttonAnTachy_Click);
            // 
            // buttonAnNormal
            // 
            this.buttonAnNormal.BackColor = System.Drawing.Color.White;
            this.buttonAnNormal.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonAnNormal.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAnNormal.Location = new System.Drawing.Point(0, 0);
            this.buttonAnNormal.Name = "buttonAnNormal";
            this.buttonAnNormal.Size = new System.Drawing.Size(80, 24);
            this.buttonAnNormal.TabIndex = 4;
            this.buttonAnNormal.Text = "Normal";
            this.buttonAnNormal.UseVisualStyleBackColor = false;
            this.buttonAnNormal.Click += new System.EventHandler(this.buttonAnNormal_Click);
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label4.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(0, 138);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 24);
            this.label4.TabIndex = 9;
            this.label4.Text = "Max Ampl R:";
            // 
            // panel28
            // 
            this.panel28.Controls.Add(this.labelLoadSec);
            this.panel28.Controls.Add(this.buttonAnPVC);
            this.panel28.Controls.Add(this.buttonAnPAC);
            this.panel28.Controls.Add(this.buttonAflut);
            this.panel28.Controls.Add(this.buttonAnAFib);
            this.panel28.Controls.Add(this.MaxAmplitude);
            this.panel28.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel28.Location = new System.Drawing.Point(1390, 0);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(80, 162);
            this.panel28.TabIndex = 5;
            // 
            // labelLoadSec
            // 
            this.labelLoadSec.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelLoadSec.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLoadSec.Location = new System.Drawing.Point(0, 120);
            this.labelLoadSec.Name = "labelLoadSec";
            this.labelLoadSec.Size = new System.Drawing.Size(80, 18);
            this.labelLoadSec.TabIndex = 11;
            this.labelLoadSec.Text = "l=?";
            // 
            // buttonAnPVC
            // 
            this.buttonAnPVC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonAnPVC.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonAnPVC.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAnPVC.Location = new System.Drawing.Point(0, 72);
            this.buttonAnPVC.Name = "buttonAnPVC";
            this.buttonAnPVC.Size = new System.Drawing.Size(80, 24);
            this.buttonAnPVC.TabIndex = 10;
            this.buttonAnPVC.Text = "PVC";
            this.buttonAnPVC.UseVisualStyleBackColor = false;
            this.buttonAnPVC.Click += new System.EventHandler(this.buttonAnPVC_Click);
            // 
            // buttonAnPAC
            // 
            this.buttonAnPAC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonAnPAC.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonAnPAC.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAnPAC.Location = new System.Drawing.Point(0, 48);
            this.buttonAnPAC.Name = "buttonAnPAC";
            this.buttonAnPAC.Size = new System.Drawing.Size(80, 24);
            this.buttonAnPAC.TabIndex = 9;
            this.buttonAnPAC.Text = "PAC";
            this.buttonAnPAC.UseVisualStyleBackColor = false;
            this.buttonAnPAC.Click += new System.EventHandler(this.buttonAnPAC_Click);
            // 
            // buttonAflut
            // 
            this.buttonAflut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonAflut.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonAflut.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAflut.Location = new System.Drawing.Point(0, 24);
            this.buttonAflut.Name = "buttonAflut";
            this.buttonAflut.Size = new System.Drawing.Size(80, 24);
            this.buttonAflut.TabIndex = 5;
            this.buttonAflut.Text = "Aflut";
            this.buttonAflut.UseVisualStyleBackColor = false;
            this.buttonAflut.Click += new System.EventHandler(this.buttonAflut_Click);
            // 
            // buttonAnAFib
            // 
            this.buttonAnAFib.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonAnAFib.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonAnAFib.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAnAFib.Location = new System.Drawing.Point(0, 0);
            this.buttonAnAFib.Name = "buttonAnAFib";
            this.buttonAnAFib.Size = new System.Drawing.Size(80, 24);
            this.buttonAnAFib.TabIndex = 4;
            this.buttonAnAFib.Text = "Afib";
            this.buttonAnAFib.UseVisualStyleBackColor = false;
            this.buttonAnAFib.Click += new System.EventHandler(this.buttonAnAFib_Click);
            // 
            // MaxAmplitude
            // 
            this.MaxAmplitude.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.MaxAmplitude.Font = new System.Drawing.Font("Verdana", 10F);
            this.MaxAmplitude.Location = new System.Drawing.Point(0, 138);
            this.MaxAmplitude.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.MaxAmplitude.Name = "MaxAmplitude";
            this.MaxAmplitude.ReadOnly = true;
            this.MaxAmplitude.Size = new System.Drawing.Size(80, 24);
            this.MaxAmplitude.TabIndex = 12;
            this.MaxAmplitude.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.MaxAmplitude.ValueChanged += new System.EventHandler(this.MaxAmplitude_ValueChanged);
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.labelShowTotalSec);
            this.panel16.Controls.Add(this.buttonOther);
            this.panel16.Controls.Add(this.buttonAnBline);
            this.panel16.Controls.Add(this.buttonAnVtach);
            this.panel16.Controls.Add(this.button1);
            this.panel16.Controls.Add(this.buttonAnPace);
            this.panel16.Controls.Add(this.AutoScaleCheckbox);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel16.Location = new System.Drawing.Point(1470, 0);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(63, 162);
            this.panel16.TabIndex = 4;
            // 
            // labelShowTotalSec
            // 
            this.labelShowTotalSec.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelShowTotalSec.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelShowTotalSec.Location = new System.Drawing.Point(0, 120);
            this.labelShowTotalSec.Name = "labelShowTotalSec";
            this.labelShowTotalSec.Size = new System.Drawing.Size(63, 18);
            this.labelShowTotalSec.TabIndex = 16;
            this.labelShowTotalSec.Text = "t=?";
            // 
            // buttonOther
            // 
            this.buttonOther.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonOther.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonOther.Font = new System.Drawing.Font("Arial", 11F);
            this.buttonOther.Location = new System.Drawing.Point(0, 72);
            this.buttonOther.Name = "buttonOther";
            this.buttonOther.Size = new System.Drawing.Size(63, 24);
            this.buttonOther.TabIndex = 11;
            this.buttonOther.Text = "Other";
            this.buttonOther.UseVisualStyleBackColor = false;
            this.buttonOther.Click += new System.EventHandler(this.buttonOther_Click);
            // 
            // buttonAnBline
            // 
            this.buttonAnBline.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonAnBline.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonAnBline.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAnBline.Location = new System.Drawing.Point(0, 48);
            this.buttonAnBline.Name = "buttonAnBline";
            this.buttonAnBline.Size = new System.Drawing.Size(63, 24);
            this.buttonAnBline.TabIndex = 15;
            this.buttonAnBline.Text = "Bline";
            this.buttonAnBline.UseVisualStyleBackColor = false;
            this.buttonAnBline.Click += new System.EventHandler(this.buttonAnBline_Click);
            // 
            // buttonAnVtach
            // 
            this.buttonAnVtach.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonAnVtach.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonAnVtach.Font = new System.Drawing.Font("Arial", 11F);
            this.buttonAnVtach.Location = new System.Drawing.Point(0, 24);
            this.buttonAnVtach.Name = "buttonAnVtach";
            this.buttonAnVtach.Size = new System.Drawing.Size(63, 24);
            this.buttonAnVtach.TabIndex = 12;
            this.buttonAnVtach.Text = "Vtach";
            this.buttonAnVtach.UseVisualStyleBackColor = false;
            this.buttonAnVtach.Click += new System.EventHandler(this.buttonAnVtach_Click);
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Top;
            this.button1.Location = new System.Drawing.Point(0, 24);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(63, 0);
            this.button1.TabIndex = 3;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // buttonAnPace
            // 
            this.buttonAnPace.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonAnPace.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonAnPace.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAnPace.Location = new System.Drawing.Point(0, 0);
            this.buttonAnPace.Name = "buttonAnPace";
            this.buttonAnPace.Size = new System.Drawing.Size(63, 24);
            this.buttonAnPace.TabIndex = 14;
            this.buttonAnPace.Text = "Pace";
            this.buttonAnPace.UseVisualStyleBackColor = false;
            this.buttonAnPace.Click += new System.EventHandler(this.buttonAnPace_Click);
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panel10.Controls.Add(this.panel25);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel10.Location = new System.Drawing.Point(0, 162);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(1533, 31);
            this.panel10.TabIndex = 0;
            // 
            // panel25
            // 
            this.panel25.BackColor = System.Drawing.Color.Transparent;
            this.panel25.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel25.Controls.Add(this.panelZoomAT);
            this.panel25.Controls.Add(this.panel30);
            this.panel25.Controls.Add(this.panel31);
            this.panel25.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel25.Location = new System.Drawing.Point(0, 0);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(1533, 28);
            this.panel25.TabIndex = 4;
            // 
            // panelZoomAT
            // 
            this.panelZoomAT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panelZoomAT.Controls.Add(this.toolStripZoomAT);
            this.panelZoomAT.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelZoomAT.Location = new System.Drawing.Point(1047, 0);
            this.panelZoomAT.Name = "panelZoomAT";
            this.panelZoomAT.Size = new System.Drawing.Size(486, 28);
            this.panelZoomAT.TabIndex = 3;
            // 
            // toolStripZoomAT
            // 
            this.toolStripZoomAT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.toolStripZoomAT.Font = new System.Drawing.Font("Arial", 10F);
            this.toolStripZoomAT.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripZoomAT.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolStripZoomAT.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripZoomEndTime,
            this.toolStripLabel11,
            this.toolStripZoomSpeed,
            this.toolStripZoomAmplitude});
            this.toolStripZoomAT.Location = new System.Drawing.Point(0, 0);
            this.toolStripZoomAT.Name = "toolStripZoomAT";
            this.toolStripZoomAT.Size = new System.Drawing.Size(486, 31);
            this.toolStripZoomAT.TabIndex = 2;
            this.toolStripZoomAT.Text = "toolStrip1";
            this.toolStripZoomAT.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStripZoomAT_ItemClicked);
            // 
            // toolStripZoomEndTime
            // 
            this.toolStripZoomEndTime.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripZoomEndTime.Name = "toolStripZoomEndTime";
            this.toolStripZoomEndTime.Size = new System.Drawing.Size(87, 28);
            this.toolStripZoomEndTime.Text = "12:20:13 AM";
            // 
            // toolStripLabel11
            // 
            this.toolStripLabel11.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripLabel11.AutoSize = false;
            this.toolStripLabel11.Name = "toolStripLabel11";
            this.toolStripLabel11.Size = new System.Drawing.Size(20, 28);
            this.toolStripLabel11.Text = "     ";
            // 
            // toolStripZoomSpeed
            // 
            this.toolStripZoomSpeed.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripZoomSpeed.AutoSize = false;
            this.toolStripZoomSpeed.Image = ((System.Drawing.Image)(resources.GetObject("toolStripZoomSpeed.Image")));
            this.toolStripZoomSpeed.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripZoomSpeed.Name = "toolStripZoomSpeed";
            this.toolStripZoomSpeed.Size = new System.Drawing.Size(180, 28);
            this.toolStripZoomSpeed.Text = "25 mm/s (0.20 Sec)";
            this.toolStripZoomSpeed.ToolTipText = "Speed Grid";
            this.toolStripZoomSpeed.Click += new System.EventHandler(this.toolStripZoomSpeed_Click);
            // 
            // toolStripZoomAmplitude
            // 
            this.toolStripZoomAmplitude.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripZoomAmplitude.AutoSize = false;
            this.toolStripZoomAmplitude.Image = ((System.Drawing.Image)(resources.GetObject("toolStripZoomAmplitude.Image")));
            this.toolStripZoomAmplitude.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripZoomAmplitude.Name = "toolStripZoomAmplitude";
            this.toolStripZoomAmplitude.Size = new System.Drawing.Size(180, 28);
            this.toolStripZoomAmplitude.Text = "10 mm/mV (0.50 mV)";
            this.toolStripZoomAmplitude.ToolTipText = "Amplitude Grid";
            this.toolStripZoomAmplitude.Click += new System.EventHandler(this.toolStripZoomAmplitude_Click);
            // 
            // panel30
            // 
            this.panel30.Controls.Add(this.toolStrip12);
            this.panel30.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel30.Location = new System.Drawing.Point(989, 0);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(215, 28);
            this.panel30.TabIndex = 1;
            // 
            // toolStrip12
            // 
            this.toolStrip12.BackColor = System.Drawing.Color.Transparent;
            this.toolStrip12.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStrip12.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip12.ImageScalingSize = new System.Drawing.Size(32, 24);
            this.toolStrip12.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripOneClick,
            this.toolStripTwoClick,
            this.toolStripSnap,
            this.toolStripButton2});
            this.toolStrip12.Location = new System.Drawing.Point(0, 0);
            this.toolStrip12.Name = "toolStrip12";
            this.toolStrip12.Size = new System.Drawing.Size(215, 25);
            this.toolStrip12.TabIndex = 4;
            this.toolStrip12.Text = "toolStrip12";
            // 
            // toolStripOneClick
            // 
            this.toolStripOneClick.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripOneClick.Image = ((System.Drawing.Image)(resources.GetObject("toolStripOneClick.Image")));
            this.toolStripOneClick.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripOneClick.Name = "toolStripOneClick";
            this.toolStripOneClick.Size = new System.Drawing.Size(36, 28);
            this.toolStripOneClick.Text = "toolStripButton15";
            this.toolStripOneClick.ToolTipText = "One Click measurement";
            this.toolStripOneClick.Visible = false;
            this.toolStripOneClick.Click += new System.EventHandler(this.toolStripOneClick_Click);
            // 
            // toolStripTwoClick
            // 
            this.toolStripTwoClick.Checked = true;
            this.toolStripTwoClick.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolStripTwoClick.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripTwoClick.Image = ((System.Drawing.Image)(resources.GetObject("toolStripTwoClick.Image")));
            this.toolStripTwoClick.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripTwoClick.Name = "toolStripTwoClick";
            this.toolStripTwoClick.Size = new System.Drawing.Size(36, 28);
            this.toolStripTwoClick.Text = "toolStripButton23";
            this.toolStripTwoClick.ToolTipText = "2 Click Measurement";
            this.toolStripTwoClick.Visible = false;
            this.toolStripTwoClick.Click += new System.EventHandler(this.toolStripTwoClick_Click);
            // 
            // toolStripSnap
            // 
            this.toolStripSnap.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripSnap.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSnap.Image")));
            this.toolStripSnap.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripSnap.Name = "toolStripSnap";
            this.toolStripSnap.Size = new System.Drawing.Size(36, 28);
            this.toolStripSnap.ToolTipText = "Snap to value";
            this.toolStripSnap.Visible = false;
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton2.Enabled = false;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(49, 22);
            this.toolStripButton2.Text = "Invert";
            this.toolStripButton2.Visible = false;
            // 
            // panel31
            // 
            this.panel31.Controls.Add(this.toolStripZoom);
            this.panel31.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel31.Location = new System.Drawing.Point(0, 0);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(989, 28);
            this.panel31.TabIndex = 0;
            // 
            // toolStripZoom
            // 
            this.toolStripZoom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.toolStripZoom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripZoom.Font = new System.Drawing.Font("Arial", 10F);
            this.toolStripZoom.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripZoom.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolStripZoom.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSelectZoom,
            this.toolStripZoomChannel,
            this.toolStripLabel18,
            this.toolStripZoomStartTime,
            this.toolStripLabel20,
            this.toolStripFullStrip,
            this.toolStripZoomTimeIncrease,
            this.toolStripZoomDuration,
            this.toolStripZoomTimeOut,
            this.toolStripWindowLeft,
            this.toolStripWindowRight,
            this.toolStripWindowDown,
            this.toolStripWindowUp,
            this.toolStripAmpDec,
            this.toolStripAmpInc,
            this.toolStripWindowCenter,
            this.toolStripButtonACDC,
            this.toolStripButtonP1,
            this.toolStripButtonP2,
            this.toolStripLabel21,
            this.toolStripARange1,
            this.toolStripButton1,
            this.toolStripButtonInvert,
            this.toolStripButtonLPF,
            this.toolStripButtonHPF,
            this.toolStripButtonBBF,
            this.toolStripButtonAvgCR,
            this.toolStripButtonAddChannel});
            this.toolStripZoom.Location = new System.Drawing.Point(0, 0);
            this.toolStripZoom.Name = "toolStripZoom";
            this.toolStripZoom.Size = new System.Drawing.Size(989, 28);
            this.toolStripZoom.TabIndex = 1;
            this.toolStripZoom.Text = "toolStrip8";
            // 
            // toolStripSelectZoom
            // 
            this.toolStripSelectZoom.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripSelectZoom.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSelectZoom.Image")));
            this.toolStripSelectZoom.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripSelectZoom.Name = "toolStripSelectZoom";
            this.toolStripSelectZoom.Size = new System.Drawing.Size(28, 25);
            this.toolStripSelectZoom.Text = "toolStripButton6";
            this.toolStripSelectZoom.ToolTipText = "Select channels";
            // 
            // toolStripZoomChannel
            // 
            this.toolStripZoomChannel.AutoSize = false;
            this.toolStripZoomChannel.Name = "toolStripZoomChannel";
            this.toolStripZoomChannel.Size = new System.Drawing.Size(60, 28);
            this.toolStripZoomChannel.Text = "ECG  1";
            this.toolStripZoomChannel.Click += new System.EventHandler(this.toolStripZoomChannel_Click);
            // 
            // toolStripLabel18
            // 
            this.toolStripLabel18.Name = "toolStripLabel18";
            this.toolStripLabel18.Size = new System.Drawing.Size(28, 25);
            this.toolStripLabel18.Text = "     ";
            // 
            // toolStripZoomStartTime
            // 
            this.toolStripZoomStartTime.Name = "toolStripZoomStartTime";
            this.toolStripZoomStartTime.Size = new System.Drawing.Size(163, 25);
            this.toolStripZoomStartTime.Text = "12:20:13 AM 12/12/2016";
            this.toolStripZoomStartTime.ToolTipText = "Start time of measurement window";
            this.toolStripZoomStartTime.Click += new System.EventHandler(this.toolStripZoomStartTime_Click);
            // 
            // toolStripLabel20
            // 
            this.toolStripLabel20.Name = "toolStripLabel20";
            this.toolStripLabel20.Size = new System.Drawing.Size(28, 25);
            this.toolStripLabel20.Text = "     ";
            // 
            // toolStripFullStrip
            // 
            this.toolStripFullStrip.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripFullStrip.Image = ((System.Drawing.Image)(resources.GetObject("toolStripFullStrip.Image")));
            this.toolStripFullStrip.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripFullStrip.Name = "toolStripFullStrip";
            this.toolStripFullStrip.Size = new System.Drawing.Size(28, 25);
            this.toolStripFullStrip.Text = "«T»";
            this.toolStripFullStrip.ToolTipText = "Full strip";
            this.toolStripFullStrip.Click += new System.EventHandler(this.toolStripFullStrip_Click);
            // 
            // toolStripZoomTimeIncrease
            // 
            this.toolStripZoomTimeIncrease.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripZoomTimeIncrease.Image = ((System.Drawing.Image)(resources.GetObject("toolStripZoomTimeIncrease.Image")));
            this.toolStripZoomTimeIncrease.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripZoomTimeIncrease.Name = "toolStripZoomTimeIncrease";
            this.toolStripZoomTimeIncrease.Size = new System.Drawing.Size(28, 25);
            this.toolStripZoomTimeIncrease.Text = "toolStripButton36";
            this.toolStripZoomTimeIncrease.ToolTipText = " Window time Zoom In";
            this.toolStripZoomTimeIncrease.Click += new System.EventHandler(this.toolStripZoomTimeIncrease_Click);
            // 
            // toolStripZoomDuration
            // 
            this.toolStripZoomDuration.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripZoomDuration.Image = ((System.Drawing.Image)(resources.GetObject("toolStripZoomDuration.Image")));
            this.toolStripZoomDuration.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripZoomDuration.Name = "toolStripZoomDuration";
            this.toolStripZoomDuration.Size = new System.Drawing.Size(56, 25);
            this.toolStripZoomDuration.Text = "12 Sec";
            this.toolStripZoomDuration.ToolTipText = "Measure window duration";
            this.toolStripZoomDuration.Click += new System.EventHandler(this.toolStripZoomDuration_Click);
            // 
            // toolStripZoomTimeOut
            // 
            this.toolStripZoomTimeOut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripZoomTimeOut.Image = ((System.Drawing.Image)(resources.GetObject("toolStripZoomTimeOut.Image")));
            this.toolStripZoomTimeOut.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripZoomTimeOut.Name = "toolStripZoomTimeOut";
            this.toolStripZoomTimeOut.Size = new System.Drawing.Size(28, 25);
            this.toolStripZoomTimeOut.Text = "toolStripButton36";
            this.toolStripZoomTimeOut.ToolTipText = "Window Time Zoom Out";
            this.toolStripZoomTimeOut.Click += new System.EventHandler(this.toolStripZoomTimeOut_Click);
            // 
            // toolStripWindowLeft
            // 
            this.toolStripWindowLeft.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripWindowLeft.Image = ((System.Drawing.Image)(resources.GetObject("toolStripWindowLeft.Image")));
            this.toolStripWindowLeft.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripWindowLeft.Name = "toolStripWindowLeft";
            this.toolStripWindowLeft.Size = new System.Drawing.Size(28, 25);
            this.toolStripWindowLeft.Text = "toolStripButton1";
            this.toolStripWindowLeft.ToolTipText = "Move Window Left";
            this.toolStripWindowLeft.Click += new System.EventHandler(this.toolStripWindowLeft_Click);
            // 
            // toolStripWindowRight
            // 
            this.toolStripWindowRight.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripWindowRight.Image = ((System.Drawing.Image)(resources.GetObject("toolStripWindowRight.Image")));
            this.toolStripWindowRight.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripWindowRight.Name = "toolStripWindowRight";
            this.toolStripWindowRight.Size = new System.Drawing.Size(28, 25);
            this.toolStripWindowRight.Text = "toolStripButton2";
            this.toolStripWindowRight.ToolTipText = "Move Window Right";
            this.toolStripWindowRight.Click += new System.EventHandler(this.toolStripWindowRight_Click);
            // 
            // toolStripWindowDown
            // 
            this.toolStripWindowDown.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripWindowDown.Image = ((System.Drawing.Image)(resources.GetObject("toolStripWindowDown.Image")));
            this.toolStripWindowDown.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripWindowDown.Name = "toolStripWindowDown";
            this.toolStripWindowDown.Size = new System.Drawing.Size(28, 25);
            this.toolStripWindowDown.Text = "toolStripButton3";
            this.toolStripWindowDown.ToolTipText = "Move Window Down";
            this.toolStripWindowDown.Click += new System.EventHandler(this.toolStripWindowDown_Click);
            // 
            // toolStripWindowUp
            // 
            this.toolStripWindowUp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripWindowUp.Image = ((System.Drawing.Image)(resources.GetObject("toolStripWindowUp.Image")));
            this.toolStripWindowUp.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripWindowUp.Name = "toolStripWindowUp";
            this.toolStripWindowUp.Size = new System.Drawing.Size(28, 25);
            this.toolStripWindowUp.Text = "toolStripButton5";
            this.toolStripWindowUp.ToolTipText = "Move Window Up";
            this.toolStripWindowUp.Click += new System.EventHandler(this.toolStripWindowUp_Click);
            // 
            // toolStripAmpDec
            // 
            this.toolStripAmpDec.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripAmpDec.Image = ((System.Drawing.Image)(resources.GetObject("toolStripAmpDec.Image")));
            this.toolStripAmpDec.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripAmpDec.Name = "toolStripAmpDec";
            this.toolStripAmpDec.Size = new System.Drawing.Size(28, 25);
            this.toolStripAmpDec.Text = "toolStripButton2";
            this.toolStripAmpDec.ToolTipText = "toolStripZoom Amplitude In";
            this.toolStripAmpDec.Click += new System.EventHandler(this.toolStripAmpDec_Click);
            // 
            // toolStripAmpInc
            // 
            this.toolStripAmpInc.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripAmpInc.Image = ((System.Drawing.Image)(resources.GetObject("toolStripAmpInc.Image")));
            this.toolStripAmpInc.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripAmpInc.Name = "toolStripAmpInc";
            this.toolStripAmpInc.Size = new System.Drawing.Size(28, 25);
            this.toolStripAmpInc.Text = "toolStripButton1";
            this.toolStripAmpInc.ToolTipText = "Zoom Amplitude Out";
            this.toolStripAmpInc.Click += new System.EventHandler(this.toolStripAmpInc_Click);
            // 
            // toolStripWindowCenter
            // 
            this.toolStripWindowCenter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripWindowCenter.Image = ((System.Drawing.Image)(resources.GetObject("toolStripWindowCenter.Image")));
            this.toolStripWindowCenter.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripWindowCenter.Name = "toolStripWindowCenter";
            this.toolStripWindowCenter.Size = new System.Drawing.Size(28, 25);
            this.toolStripWindowCenter.Text = "toolStripButton4";
            this.toolStripWindowCenter.ToolTipText = "Center Window on Signal";
            this.toolStripWindowCenter.Click += new System.EventHandler(this.toolStripWindowCenter_Click);
            // 
            // toolStripButtonACDC
            // 
            this.toolStripButtonACDC.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonACDC.Font = new System.Drawing.Font("Arial", 8F);
            this.toolStripButtonACDC.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonACDC.Image")));
            this.toolStripButtonACDC.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonACDC.Name = "toolStripButtonACDC";
            this.toolStripButtonACDC.Size = new System.Drawing.Size(25, 25);
            this.toolStripButtonACDC.Text = "?A";
            this.toolStripButtonACDC.ToolTipText = "Measure AC DC Voltage";
            this.toolStripButtonACDC.Click += new System.EventHandler(this.toolStripButtonACDC_Click);
            // 
            // toolStripButtonP1
            // 
            this.toolStripButtonP1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonP1.Font = new System.Drawing.Font("Arial", 8F);
            this.toolStripButtonP1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonP1.Image")));
            this.toolStripButtonP1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonP1.Name = "toolStripButtonP1";
            this.toolStripButtonP1.Size = new System.Drawing.Size(23, 25);
            this.toolStripButtonP1.Text = "p1";
            this.toolStripButtonP1.ToolTipText = "Store P1\r\n(sToggle active, ~Edit P1,^swap P1P2)\r\n";
            this.toolStripButtonP1.Click += new System.EventHandler(this.toolStripButtonP1_Click);
            // 
            // toolStripButtonP2
            // 
            this.toolStripButtonP2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonP2.Font = new System.Drawing.Font("Arial", 8F);
            this.toolStripButtonP2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonP2.Image")));
            this.toolStripButtonP2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonP2.Name = "toolStripButtonP2";
            this.toolStripButtonP2.Size = new System.Drawing.Size(23, 25);
            this.toolStripButtonP2.Text = "p2";
            this.toolStripButtonP2.ToolTipText = "Store P2 & Add Measurement\r\n(sToggle active, ~Edit P2, ^Add P2-P1)";
            this.toolStripButtonP2.Click += new System.EventHandler(this.toolStripButtonP2_Click);
            // 
            // toolStripLabel21
            // 
            this.toolStripLabel21.Name = "toolStripLabel21";
            this.toolStripLabel21.Size = new System.Drawing.Size(28, 25);
            this.toolStripLabel21.Text = "     ";
            // 
            // toolStripARange1
            // 
            this.toolStripARange1.Font = new System.Drawing.Font("Arial", 6F);
            this.toolStripARange1.Name = "toolStripARange1";
            this.toolStripARange1.Size = new System.Drawing.Size(12, 25);
            this.toolStripARange1.Text = "Ξ.";
            this.toolStripARange1.ToolTipText = "Ampl Range Limit";
            this.toolStripARange1.Visible = false;
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton1.Font = new System.Drawing.Font("Webdings", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(35, 25);
            this.toolStripButton1.Text = "q";
            this.toolStripButton1.ToolTipText = "Redraw charts";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click_1);
            // 
            // toolStripButtonInvert
            // 
            this.toolStripButtonInvert.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonInvert.Font = new System.Drawing.Font("Arial", 12F);
            this.toolStripButtonInvert.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonInvert.Image")));
            this.toolStripButtonInvert.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonInvert.Name = "toolStripButtonInvert";
            this.toolStripButtonInvert.Size = new System.Drawing.Size(59, 25);
            this.toolStripButtonInvert.Text = "ƱΩ-Inv";
            this.toolStripButtonInvert.ToolTipText = "Invert Channel";
            this.toolStripButtonInvert.Click += new System.EventHandler(this.toolStripButtonInvert_Click);
            // 
            // toolStripButtonLPF
            // 
            this.toolStripButtonLPF.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonLPF.Font = new System.Drawing.Font("Arial", 6F);
            this.toolStripButtonLPF.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonLPF.Image")));
            this.toolStripButtonLPF.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonLPF.Name = "toolStripButtonLPF";
            this.toolStripButtonLPF.Size = new System.Drawing.Size(23, 25);
            this.toolStripButtonLPF.Text = "-lpf-";
            this.toolStripButtonLPF.ToolTipText = "Low Pass Filter";
            this.toolStripButtonLPF.Click += new System.EventHandler(this.toolStripButtonLPF_Click);
            // 
            // toolStripButtonHPF
            // 
            this.toolStripButtonHPF.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonHPF.Font = new System.Drawing.Font("Arial", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripButtonHPF.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonHPF.Image")));
            this.toolStripButtonHPF.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonHPF.Name = "toolStripButtonHPF";
            this.toolStripButtonHPF.Size = new System.Drawing.Size(25, 25);
            this.toolStripButtonHPF.Text = "-hpf-";
            this.toolStripButtonHPF.ToolTipText = "High Pass Filter";
            this.toolStripButtonHPF.Click += new System.EventHandler(this.toolStripButtonHPF_Click);
            // 
            // toolStripButtonBBF
            // 
            this.toolStripButtonBBF.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonBBF.Font = new System.Drawing.Font("Arial", 6F);
            this.toolStripButtonBBF.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonBBF.Image")));
            this.toolStripButtonBBF.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonBBF.Name = "toolStripButtonBBF";
            this.toolStripButtonBBF.Size = new System.Drawing.Size(25, 25);
            this.toolStripButtonBBF.Text = "-bbf-";
            this.toolStripButtonBBF.ToolTipText = "Mains Filter";
            this.toolStripButtonBBF.Click += new System.EventHandler(this.toolStripButtonMF_Click);
            // 
            // toolStripButtonAvgCR
            // 
            this.toolStripButtonAvgCR.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonAvgCR.Font = new System.Drawing.Font("Arial", 6F);
            this.toolStripButtonAvgCR.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAvgCR.Image")));
            this.toolStripButtonAvgCR.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAvgCR.Name = "toolStripButtonAvgCR";
            this.toolStripButtonAvgCR.Size = new System.Drawing.Size(28, 25);
            this.toolStripButtonAvgCR.Text = "-acrf-";
            this.toolStripButtonAvgCR.ToolTipText = "Avg + Clip Filter\r\n(sAddNoise, ^ShiftTime, ~altAmpl)";
            this.toolStripButtonAvgCR.Click += new System.EventHandler(this.toolStripButtonAvgCR_Click);
            // 
            // toolStripButtonAddChannel
            // 
            this.toolStripButtonAddChannel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonAddChannel.Font = new System.Drawing.Font("Arial", 6F);
            this.toolStripButtonAddChannel.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAddChannel.Image")));
            this.toolStripButtonAddChannel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAddChannel.Name = "toolStripButtonAddChannel";
            this.toolStripButtonAddChannel.Size = new System.Drawing.Size(28, 25);
            this.toolStripButtonAddChannel.Text = "-+~^-";
            this.toolStripButtonAddChannel.ToolTipText = "Add Channel III, aVR+aVL+AVF \r\n(sCopy, ~sMove, ^Del, ~Delta)  ";
            this.toolStripButtonAddChannel.Click += new System.EventHandler(this.toolStripButtonAddChannel_Click);
            // 
            // splitContainer4
            // 
            this.splitContainer4.BackColor = System.Drawing.Color.White;
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            this.splitContainer4.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.splitContainer4.Panel1.Controls.Add(this.panelDrawChannels);
            this.splitContainer4.Panel1.Controls.Add(this.panelAllChannels);
            this.splitContainer4.Panel1.Controls.Add(this.panelChannelScrol);
            this.splitContainer4.Panel1.Controls.Add(this.panelChannelsInfo);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.panelSAF);
            this.splitContainer4.Panel2.Controls.Add(this.panelAutoMeasurements);
            this.splitContainer4.Panel2.Controls.Add(this.panel24);
            this.splitContainer4.Panel2.Controls.Add(this.panel26);
            this.splitContainer4.Panel2.Controls.Add(this.panel22);
            this.splitContainer4.Size = new System.Drawing.Size(1533, 538);
            this.splitContainer4.SplitterDistance = 356;
            this.splitContainer4.TabIndex = 0;
            // 
            // panelDrawChannels
            // 
            this.panelDrawChannels.AutoScroll = true;
            this.panelDrawChannels.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelDrawChannels.Controls.Add(this.splitContainerChannels);
            this.panelDrawChannels.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDrawChannels.Location = new System.Drawing.Point(0, 0);
            this.panelDrawChannels.Name = "panelDrawChannels";
            this.panelDrawChannels.Size = new System.Drawing.Size(1533, 165);
            this.panelDrawChannels.TabIndex = 3;
            // 
            // splitContainerChannels
            // 
            this.splitContainerChannels.BackColor = System.Drawing.Color.White;
            this.splitContainerChannels.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitContainerChannels.Location = new System.Drawing.Point(0, 0);
            this.splitContainerChannels.Margin = new System.Windows.Forms.Padding(1);
            this.splitContainerChannels.Name = "splitContainerChannels";
            // 
            // splitContainerChannels.Panel1
            // 
            this.splitContainerChannels.Panel1.Controls.Add(this.panelChannel6);
            this.splitContainerChannels.Panel1.Controls.Add(this.panelChannel5);
            this.splitContainerChannels.Panel1.Controls.Add(this.panelChannel4);
            this.splitContainerChannels.Panel1.Controls.Add(this.panelChannel3);
            this.splitContainerChannels.Panel1.Controls.Add(this.panelChannel2);
            this.splitContainerChannels.Panel1.Controls.Add(this.panelChannel1);
            // 
            // splitContainerChannels.Panel2
            // 
            this.splitContainerChannels.Panel2.Controls.Add(this.panelChannel12);
            this.splitContainerChannels.Panel2.Controls.Add(this.panelChannel11);
            this.splitContainerChannels.Panel2.Controls.Add(this.panelChannel10);
            this.splitContainerChannels.Panel2.Controls.Add(this.panelChannel9);
            this.splitContainerChannels.Panel2.Controls.Add(this.panelChannel8);
            this.splitContainerChannels.Panel2.Controls.Add(this.panelChannel7);
            this.splitContainerChannels.Size = new System.Drawing.Size(1516, 456);
            this.splitContainerChannels.SplitterDistance = 783;
            this.splitContainerChannels.TabIndex = 1;
            // 
            // panelAllChannels
            // 
            this.panelAllChannels.AutoScroll = true;
            this.panelAllChannels.AutoSize = true;
            this.panelAllChannels.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelAllChannels.BackColor = System.Drawing.Color.White;
            this.panelAllChannels.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelAllChannels.Location = new System.Drawing.Point(0, 0);
            this.panelAllChannels.Name = "panelAllChannels";
            this.panelAllChannels.Size = new System.Drawing.Size(1533, 0);
            this.panelAllChannels.TabIndex = 0;
            // 
            // panelChannelScrol
            // 
            this.panelChannelScrol.AutoSize = true;
            this.panelChannelScrol.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelChannelScrol.Controls.Add(this.panelHR);
            this.panelChannelScrol.Controls.Add(this.panelGraphSeperator);
            this.panelChannelScrol.Controls.Add(this.panel38);
            this.panelChannelScrol.Controls.Add(this.panelGraph);
            this.panelChannelScrol.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelChannelScrol.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.panelChannelScrol.Location = new System.Drawing.Point(0, 165);
            this.panelChannelScrol.Name = "panelChannelScrol";
            this.panelChannelScrol.Size = new System.Drawing.Size(1533, 162);
            this.panelChannelScrol.TabIndex = 2;
            // 
            // panelHR
            // 
            this.panelHR.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelHR.Controls.Add(this.pictureBoxHR);
            this.panelHR.Controls.Add(this.panel36);
            this.panelHR.Controls.Add(this.panel37);
            this.panelHR.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelHR.Location = new System.Drawing.Point(0, 16);
            this.panelHR.Name = "panelHR";
            this.panelHR.Size = new System.Drawing.Size(1533, 40);
            this.panelHR.TabIndex = 1;
            // 
            // pictureBoxHR
            // 
            this.pictureBoxHR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxHR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxHR.Location = new System.Drawing.Point(19, 0);
            this.pictureBoxHR.Name = "pictureBoxHR";
            this.pictureBoxHR.Size = new System.Drawing.Size(1479, 40);
            this.pictureBoxHR.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxHR.TabIndex = 3;
            this.pictureBoxHR.TabStop = false;
            this.pictureBoxHR.Click += new System.EventHandler(this.pictureBoxHR_Click);
            this.pictureBoxHR.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxHR_MouseUp);
            // 
            // panel36
            // 
            this.panel36.Controls.Add(this.label1MeanHR);
            this.panel36.Controls.Add(this.label14);
            this.panel36.Controls.Add(this.labelMeanBurden);
            this.panel36.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel36.Location = new System.Drawing.Point(1498, 0);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(35, 40);
            this.panel36.TabIndex = 1;
            // 
            // label1MeanHR
            // 
            this.label1MeanHR.AutoSize = true;
            this.label1MeanHR.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1MeanHR.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1MeanHR.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1MeanHR.Location = new System.Drawing.Point(0, 24);
            this.label1MeanHR.Name = "label1MeanHR";
            this.label1MeanHR.Size = new System.Drawing.Size(23, 12);
            this.label1MeanHR.TabIndex = 2;
            this.label1MeanHR.Text = "xxx";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Dock = System.Windows.Forms.DockStyle.Top;
            this.label14.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label14.Location = new System.Drawing.Point(0, 12);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(20, 12);
            this.label14.TabIndex = 1;
            this.label14.Text = "HR";
            // 
            // labelMeanBurden
            // 
            this.labelMeanBurden.AutoSize = true;
            this.labelMeanBurden.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelMeanBurden.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMeanBurden.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelMeanBurden.Location = new System.Drawing.Point(0, 0);
            this.labelMeanBurden.Name = "labelMeanBurden";
            this.labelMeanBurden.Size = new System.Drawing.Size(37, 12);
            this.labelMeanBurden.TabIndex = 0;
            this.labelMeanBurden.Text = "199%";
            // 
            // panel37
            // 
            this.panel37.Controls.Add(this.labelHR);
            this.panel37.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel37.Location = new System.Drawing.Point(0, 0);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(19, 40);
            this.panel37.TabIndex = 0;
            // 
            // labelHR
            // 
            this.labelHR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelHR.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelHR.Location = new System.Drawing.Point(0, 0);
            this.labelHR.Name = "labelHR";
            this.labelHR.Size = new System.Drawing.Size(19, 40);
            this.labelHR.TabIndex = 0;
            this.labelHR.Text = "HR";
            // 
            // panelGraphSeperator
            // 
            this.panelGraphSeperator.Controls.Add(this.panelGraphMarker);
            this.panelGraphSeperator.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelGraphSeperator.Location = new System.Drawing.Point(0, 56);
            this.panelGraphSeperator.Name = "panelGraphSeperator";
            this.panelGraphSeperator.Size = new System.Drawing.Size(1533, 4);
            this.panelGraphSeperator.TabIndex = 17;
            this.panelGraphSeperator.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panelGraphSeperator_MouseUp);
            // 
            // panelGraphMarker
            // 
            this.panelGraphMarker.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.panelGraphMarker.Location = new System.Drawing.Point(297, 0);
            this.panelGraphMarker.Name = "panelGraphMarker";
            this.panelGraphMarker.Size = new System.Drawing.Size(8, 8);
            this.panelGraphMarker.TabIndex = 0;
            // 
            // panel38
            // 
            this.panel38.Controls.Add(this.ScrollBarChannel);
            this.panel38.Controls.Add(this.panel39);
            this.panel38.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel38.Location = new System.Drawing.Point(0, 0);
            this.panel38.Name = "panel38";
            this.panel38.Size = new System.Drawing.Size(1533, 16);
            this.panel38.TabIndex = 15;
            // 
            // ScrollBarChannel
            // 
            this.ScrollBarChannel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ScrollBarChannel.Location = new System.Drawing.Point(0, 0);
            this.ScrollBarChannel.Name = "ScrollBarChannel";
            this.ScrollBarChannel.Size = new System.Drawing.Size(1513, 16);
            this.ScrollBarChannel.TabIndex = 1;
            this.ScrollBarChannel.ValueChanged += new System.EventHandler(this.ScrollBarChannel_ValueChanged);
            // 
            // panel39
            // 
            this.panel39.Controls.Add(this.buttonCalcRR);
            this.panel39.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel39.Location = new System.Drawing.Point(1513, 0);
            this.panel39.Name = "panel39";
            this.panel39.Size = new System.Drawing.Size(20, 16);
            this.panel39.TabIndex = 0;
            // 
            // panelGraph
            // 
            this.panelGraph.Controls.Add(this.pictureBoxGraph);
            this.panelGraph.Controls.Add(this.panel41);
            this.panelGraph.Controls.Add(this.panel47);
            this.panelGraph.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelGraph.Location = new System.Drawing.Point(0, 60);
            this.panelGraph.Name = "panelGraph";
            this.panelGraph.Size = new System.Drawing.Size(1533, 102);
            this.panelGraph.TabIndex = 16;
            this.panelGraph.Visible = false;
            // 
            // pictureBoxGraph
            // 
            this.pictureBoxGraph.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxGraph.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxGraph.Location = new System.Drawing.Point(19, 0);
            this.pictureBoxGraph.Name = "pictureBoxGraph";
            this.pictureBoxGraph.Size = new System.Drawing.Size(1479, 102);
            this.pictureBoxGraph.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxGraph.TabIndex = 3;
            this.pictureBoxGraph.TabStop = false;
            this.pictureBoxGraph.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxGraph_MouseUp);
            // 
            // panel41
            // 
            this.panel41.Controls.Add(this.labelGraphRight3);
            this.panel41.Controls.Add(this.labelGraphRight2);
            this.panel41.Controls.Add(this.labelGraphRight1);
            this.panel41.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel41.Location = new System.Drawing.Point(1498, 0);
            this.panel41.Name = "panel41";
            this.panel41.Size = new System.Drawing.Size(35, 102);
            this.panel41.TabIndex = 1;
            // 
            // labelGraphRight3
            // 
            this.labelGraphRight3.AutoSize = true;
            this.labelGraphRight3.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelGraphRight3.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGraphRight3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelGraphRight3.Location = new System.Drawing.Point(0, 24);
            this.labelGraphRight3.Name = "labelGraphRight3";
            this.labelGraphRight3.Size = new System.Drawing.Size(23, 12);
            this.labelGraphRight3.TabIndex = 2;
            this.labelGraphRight3.Text = "xxx";
            // 
            // labelGraphRight2
            // 
            this.labelGraphRight2.AutoSize = true;
            this.labelGraphRight2.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelGraphRight2.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGraphRight2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelGraphRight2.Location = new System.Drawing.Point(0, 12);
            this.labelGraphRight2.Name = "labelGraphRight2";
            this.labelGraphRight2.Size = new System.Drawing.Size(13, 12);
            this.labelGraphRight2.TabIndex = 1;
            this.labelGraphRight2.Text = "A";
            // 
            // labelGraphRight1
            // 
            this.labelGraphRight1.AutoSize = true;
            this.labelGraphRight1.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelGraphRight1.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGraphRight1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelGraphRight1.Location = new System.Drawing.Point(0, 0);
            this.labelGraphRight1.Name = "labelGraphRight1";
            this.labelGraphRight1.Size = new System.Drawing.Size(37, 12);
            this.labelGraphRight1.TabIndex = 0;
            this.labelGraphRight1.Text = "199%";
            // 
            // panel47
            // 
            this.panel47.Controls.Add(this.labelGraphHeader);
            this.panel47.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel47.Location = new System.Drawing.Point(0, 0);
            this.panel47.Name = "panel47";
            this.panel47.Size = new System.Drawing.Size(19, 102);
            this.panel47.TabIndex = 0;
            // 
            // labelGraphHeader
            // 
            this.labelGraphHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelGraphHeader.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGraphHeader.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelGraphHeader.Location = new System.Drawing.Point(0, 0);
            this.labelGraphHeader.Name = "labelGraphHeader";
            this.labelGraphHeader.Size = new System.Drawing.Size(19, 102);
            this.labelGraphHeader.TabIndex = 0;
            this.labelGraphHeader.Text = " A";
            // 
            // panelChannelsInfo
            // 
            this.panelChannelsInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panelChannelsInfo.Controls.Add(this.panelChannelsButtons);
            this.panelChannelsInfo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelChannelsInfo.Location = new System.Drawing.Point(0, 327);
            this.panelChannelsInfo.Name = "panelChannelsInfo";
            this.panelChannelsInfo.Size = new System.Drawing.Size(1533, 29);
            this.panelChannelsInfo.TabIndex = 1;
            // 
            // panelChannelsButtons
            // 
            this.panelChannelsButtons.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panelChannelsButtons.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelChannelsButtons.Controls.Add(this.panel52);
            this.panelChannelsButtons.Controls.Add(this.panelStripAT);
            this.panelChannelsButtons.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelChannelsButtons.Location = new System.Drawing.Point(0, 0);
            this.panelChannelsButtons.Name = "panelChannelsButtons";
            this.panelChannelsButtons.Size = new System.Drawing.Size(1533, 29);
            this.panelChannelsButtons.TabIndex = 9;
            // 
            // panel52
            // 
            this.panel52.AutoSize = true;
            this.panel52.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel52.BackColor = System.Drawing.Color.Transparent;
            this.panel52.Controls.Add(this.toolStripChn);
            this.panel52.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel52.Location = new System.Drawing.Point(0, 0);
            this.panel52.Name = "panel52";
            this.panel52.Size = new System.Drawing.Size(1050, 29);
            this.panel52.TabIndex = 0;
            // 
            // panelStripAT
            // 
            this.panelStripAT.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelStripAT.Controls.Add(this.toolStrip10);
            this.panelStripAT.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelStripAT.Location = new System.Drawing.Point(1019, 0);
            this.panelStripAT.Name = "panelStripAT";
            this.panelStripAT.Size = new System.Drawing.Size(514, 29);
            this.panelStripAT.TabIndex = 3;
            // 
            // toolStrip10
            // 
            this.toolStrip10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.toolStrip10.Font = new System.Drawing.Font("Arial", 9F);
            this.toolStrip10.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip10.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolStrip10.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripChannelEndTime,
            this.toolStripLabel15,
            this.toolStripChannelSpeed,
            this.toolStripChannelAmplitute});
            this.toolStrip10.Location = new System.Drawing.Point(0, 0);
            this.toolStrip10.Name = "toolStrip10";
            this.toolStrip10.Size = new System.Drawing.Size(514, 31);
            this.toolStrip10.TabIndex = 2;
            this.toolStrip10.Text = "toolStrip10";
            this.toolStrip10.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStrip10_ItemClicked);
            // 
            // toolStripChannelEndTime
            // 
            this.toolStripChannelEndTime.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripChannelEndTime.Font = new System.Drawing.Font("Arial", 10F);
            this.toolStripChannelEndTime.Name = "toolStripChannelEndTime";
            this.toolStripChannelEndTime.Size = new System.Drawing.Size(87, 28);
            this.toolStripChannelEndTime.Text = "12:20:13 AM";
            // 
            // toolStripLabel15
            // 
            this.toolStripLabel15.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripLabel15.AutoSize = false;
            this.toolStripLabel15.Font = new System.Drawing.Font("Arial", 10F);
            this.toolStripLabel15.Name = "toolStripLabel15";
            this.toolStripLabel15.Size = new System.Drawing.Size(20, 28);
            this.toolStripLabel15.Text = "     ";
            // 
            // toolStripChannelSpeed
            // 
            this.toolStripChannelSpeed.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripChannelSpeed.AutoSize = false;
            this.toolStripChannelSpeed.Font = new System.Drawing.Font("Arial", 10F);
            this.toolStripChannelSpeed.Image = ((System.Drawing.Image)(resources.GetObject("toolStripChannelSpeed.Image")));
            this.toolStripChannelSpeed.ImageTransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.toolStripChannelSpeed.Name = "toolStripChannelSpeed";
            this.toolStripChannelSpeed.Size = new System.Drawing.Size(180, 28);
            this.toolStripChannelSpeed.Text = "25 mm/s (0.20 Sec)";
            this.toolStripChannelSpeed.ToolTipText = "Speed Grid";
            this.toolStripChannelSpeed.Click += new System.EventHandler(this.toolStripChannelSpeed_Click);
            // 
            // toolStripChannelAmplitute
            // 
            this.toolStripChannelAmplitute.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripChannelAmplitute.AutoSize = false;
            this.toolStripChannelAmplitute.Font = new System.Drawing.Font("Arial", 10F);
            this.toolStripChannelAmplitute.Image = ((System.Drawing.Image)(resources.GetObject("toolStripChannelAmplitute.Image")));
            this.toolStripChannelAmplitute.ImageTransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.toolStripChannelAmplitute.Name = "toolStripChannelAmplitute";
            this.toolStripChannelAmplitute.Size = new System.Drawing.Size(180, 28);
            this.toolStripChannelAmplitute.Text = "10 mm/mV (0.5 mV)";
            this.toolStripChannelAmplitute.ToolTipText = "Amplitude Grid";
            this.toolStripChannelAmplitute.Click += new System.EventHandler(this.toolStripChannelAmplitute_Click);
            // 
            // panelSAF
            // 
            this.panelSAF.BackColor = System.Drawing.Color.White;
            this.panelSAF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelSAF.Controls.Add(this.panel27);
            this.panelSAF.Controls.Add(this.panel23);
            this.panelSAF.Controls.Add(this.panel11);
            this.panelSAF.Controls.Add(this.panel7);
            this.panelSAF.Controls.Add(this.panel6);
            this.panelSAF.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelSAF.Location = new System.Drawing.Point(969, 0);
            this.panelSAF.Name = "panelSAF";
            this.panelSAF.Size = new System.Drawing.Size(548, 178);
            this.panelSAF.TabIndex = 7;
            // 
            // panel27
            // 
            this.panel27.Controls.Add(this.textBoxFindings);
            this.panel27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel27.Location = new System.Drawing.Point(0, 91);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(546, 85);
            this.panel27.TabIndex = 4;
            // 
            // textBoxFindings
            // 
            this.textBoxFindings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxFindings.Location = new System.Drawing.Point(0, 0);
            this.textBoxFindings.MaxLength = 500;
            this.textBoxFindings.Multiline = true;
            this.textBoxFindings.Name = "textBoxFindings";
            this.textBoxFindings.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxFindings.Size = new System.Drawing.Size(546, 85);
            this.textBoxFindings.TabIndex = 23;
            this.textBoxFindings.Text = "1\r\n2\r\n3\r\n4\r\n5";
            this.textBoxFindings.TextChanged += new System.EventHandler(this.textBoxFindings_TextChanged_1);
            // 
            // panel23
            // 
            this.panel23.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel23.Location = new System.Drawing.Point(0, 87);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(546, 4);
            this.panel23.TabIndex = 3;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.buttonSelectFindings);
            this.panel11.Controls.Add(this.buttonFindingsRhythm);
            this.panel11.Controls.Add(this.checkBoxQCD);
            this.panel11.Controls.Add(this.label12);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel11.Location = new System.Drawing.Point(0, 60);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(546, 27);
            this.panel11.TabIndex = 2;
            // 
            // buttonSelectFindings
            // 
            this.buttonSelectFindings.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonSelectFindings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSelectFindings.Font = new System.Drawing.Font("Arial", 10F);
            this.buttonSelectFindings.Location = new System.Drawing.Point(137, 0);
            this.buttonSelectFindings.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.buttonSelectFindings.Name = "buttonSelectFindings";
            this.buttonSelectFindings.Size = new System.Drawing.Size(134, 27);
            this.buttonSelectFindings.TabIndex = 20;
            this.buttonSelectFindings.Text = "Select Findings";
            this.buttonSelectFindings.UseVisualStyleBackColor = true;
            this.buttonSelectFindings.Click += new System.EventHandler(this.button5_Click);
            // 
            // buttonFindingsRhythm
            // 
            this.buttonFindingsRhythm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFindingsRhythm.Font = new System.Drawing.Font("Arial", 10F);
            this.buttonFindingsRhythm.Location = new System.Drawing.Point(293, 0);
            this.buttonFindingsRhythm.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.buttonFindingsRhythm.Name = "buttonFindingsRhythm";
            this.buttonFindingsRhythm.Size = new System.Drawing.Size(244, 27);
            this.buttonFindingsRhythm.TabIndex = 18;
            this.buttonFindingsRhythm.Text = "Select Rythm";
            this.buttonFindingsRhythm.UseVisualStyleBackColor = true;
            this.buttonFindingsRhythm.Click += new System.EventHandler(this.buttonFindingsRhythm_Click);
            // 
            // checkBoxQCD
            // 
            this.checkBoxQCD.AutoSize = true;
            this.checkBoxQCD.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxQCD.Location = new System.Drawing.Point(86, 0);
            this.checkBoxQCD.Name = "checkBoxQCD";
            this.checkBoxQCD.Size = new System.Drawing.Size(51, 27);
            this.checkBoxQCD.TabIndex = 21;
            this.checkBoxQCD.Text = "QC";
            this.checkBoxQCD.UseVisualStyleBackColor = true;
            this.checkBoxQCD.CheckedChanged += new System.EventHandler(this.checkBoxQCD_CheckedChanged);
            this.checkBoxQCD.CheckStateChanged += new System.EventHandler(this.checkBoxQCD_CheckStateChanged);
            // 
            // label12
            // 
            this.label12.Dock = System.Windows.Forms.DockStyle.Left;
            this.label12.Font = new System.Drawing.Font("Arial", 12F);
            this.label12.Location = new System.Drawing.Point(0, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(86, 27);
            this.label12.TabIndex = 19;
            this.label12.Text = "For report:";
            // 
            // panel7
            // 
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 56);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(546, 4);
            this.panel7.TabIndex = 1;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.textBoxActivities);
            this.panel6.Controls.Add(this.label1);
            this.panel6.Controls.Add(this.textBoxSymptoms);
            this.panel6.Controls.Add(this.panel35);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(546, 56);
            this.panel6.TabIndex = 0;
            // 
            // textBoxActivities
            // 
            this.textBoxActivities.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxActivities.Dock = System.Windows.Forms.DockStyle.Left;
            this.textBoxActivities.Font = new System.Drawing.Font("Arial", 12F);
            this.textBoxActivities.Location = new System.Drawing.Point(360, 0);
            this.textBoxActivities.MaxLength = 32;
            this.textBoxActivities.Multiline = true;
            this.textBoxActivities.Name = "textBoxActivities";
            this.textBoxActivities.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxActivities.Size = new System.Drawing.Size(185, 56);
            this.textBoxActivities.TabIndex = 12;
            this.textBoxActivities.Text = "1\r\n2";
            this.textBoxActivities.TextChanged += new System.EventHandler(this.textBoxActivities_TextChanged);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Font = new System.Drawing.Font("Arial", 12F);
            this.label1.Location = new System.Drawing.Point(279, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 56);
            this.label1.TabIndex = 11;
            this.label1.Text = "Activities:";
            // 
            // textBoxSymptoms
            // 
            this.textBoxSymptoms.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxSymptoms.Dock = System.Windows.Forms.DockStyle.Left;
            this.textBoxSymptoms.Font = new System.Drawing.Font("Arial", 12F);
            this.textBoxSymptoms.Location = new System.Drawing.Point(94, 0);
            this.textBoxSymptoms.MaxLength = 32;
            this.textBoxSymptoms.Multiline = true;
            this.textBoxSymptoms.Name = "textBoxSymptoms";
            this.textBoxSymptoms.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxSymptoms.Size = new System.Drawing.Size(185, 56);
            this.textBoxSymptoms.TabIndex = 10;
            this.textBoxSymptoms.Text = "1\r\n2";
            this.textBoxSymptoms.TextChanged += new System.EventHandler(this.textBoxSymptoms_TextChanged);
            // 
            // panel35
            // 
            this.panel35.Controls.Add(this.label9);
            this.panel35.Controls.Add(this.labelPrevTech);
            this.panel35.Controls.Add(this.label10);
            this.panel35.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel35.Location = new System.Drawing.Point(0, 0);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(94, 56);
            this.panel35.TabIndex = 13;
            // 
            // label9
            // 
            this.label9.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label9.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(0, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(94, 14);
            this.label9.TabIndex = 12;
            this.label9.Text = "prev tech=";
            // 
            // labelPrevTech
            // 
            this.labelPrevTech.BackColor = System.Drawing.Color.Transparent;
            this.labelPrevTech.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelPrevTech.Font = new System.Drawing.Font("Arial", 12F);
            this.labelPrevTech.Location = new System.Drawing.Point(0, 36);
            this.labelPrevTech.Name = "labelPrevTech";
            this.labelPrevTech.Size = new System.Drawing.Size(94, 20);
            this.labelPrevTech.TabIndex = 11;
            this.labelPrevTech.Text = "^tech inyy";
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Dock = System.Windows.Forms.DockStyle.Top;
            this.label10.Font = new System.Drawing.Font("Arial", 12F);
            this.label10.Location = new System.Drawing.Point(0, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(94, 20);
            this.label10.TabIndex = 10;
            this.label10.Text = "Symptoms:";
            // 
            // panelAutoMeasurements
            // 
            this.panelAutoMeasurements.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelAutoMeasurements.Controls.Add(this.treeViewAutoMeasurements);
            this.panelAutoMeasurements.Controls.Add(this.toolStrip9);
            this.panelAutoMeasurements.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelAutoMeasurements.Location = new System.Drawing.Point(1517, 0);
            this.panelAutoMeasurements.Name = "panelAutoMeasurements";
            this.panelAutoMeasurements.Size = new System.Drawing.Size(16, 178);
            this.panelAutoMeasurements.TabIndex = 6;
            this.panelAutoMeasurements.Visible = false;
            // 
            // treeViewAutoMeasurements
            // 
            this.treeViewAutoMeasurements.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeViewAutoMeasurements.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.treeViewAutoMeasurements.Location = new System.Drawing.Point(0, 39);
            this.treeViewAutoMeasurements.Name = "treeViewAutoMeasurements";
            treeNode1.Name = "Node2";
            treeNode1.Text = "V(q-r) = 1.2 mV";
            treeNode2.Name = "Node1";
            treeNode2.Text = "T(r+r) = 800 mS";
            treeNode2.ToolTipText = "V(qr) = 1.03 mV";
            treeNode3.Name = "M1";
            treeNode3.Tag = "Measurement";
            treeNode3.Text = "M 12:13:24 (6 Sec)";
            this.treeViewAutoMeasurements.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode3});
            this.treeViewAutoMeasurements.Size = new System.Drawing.Size(14, 137);
            this.treeViewAutoMeasurements.TabIndex = 5;
            // 
            // toolStrip9
            // 
            this.toolStrip9.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStrip9.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip9.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip9.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel5,
            this.toolStripCopyAuto});
            this.toolStrip9.Location = new System.Drawing.Point(0, 0);
            this.toolStrip9.Name = "toolStrip9";
            this.toolStrip9.Size = new System.Drawing.Size(14, 39);
            this.toolStrip9.TabIndex = 4;
            this.toolStrip9.Text = "toolStrip9";
            // 
            // toolStripLabel5
            // 
            this.toolStripLabel5.Name = "toolStripLabel5";
            this.toolStripLabel5.Size = new System.Drawing.Size(138, 14);
            this.toolStripLabel5.Text = "Auto Measurements:";
            // 
            // toolStripCopyAuto
            // 
            this.toolStripCopyAuto.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripCopyAuto.Image = ((System.Drawing.Image)(resources.GetObject("toolStripCopyAuto.Image")));
            this.toolStripCopyAuto.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripCopyAuto.Name = "toolStripCopyAuto";
            this.toolStripCopyAuto.Size = new System.Drawing.Size(36, 36);
            this.toolStripCopyAuto.Text = "toolStripButton35";
            this.toolStripCopyAuto.ToolTipText = "Copy Measurement to Strip";
            // 
            // panel24
            // 
            this.panel24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel24.Controls.Add(this.listViewStats);
            this.panel24.Controls.Add(this.toolStrip7);
            this.panel24.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel24.Location = new System.Drawing.Point(644, 0);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(325, 178);
            this.panel24.TabIndex = 2;
            // 
            // toolStrip7
            // 
            this.toolStrip7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.toolStrip7.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStrip7.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip7.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip7.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabelStats,
            this.toolStripButtonRecalcStats,
            this.toolStripCopyTable,
            this.toolStripLabelAnalysis,
            this.toolStripAnalysisNr,
            this.toolStripButtonMctTrend});
            this.toolStrip7.Location = new System.Drawing.Point(0, 0);
            this.toolStrip7.Name = "toolStrip7";
            this.toolStrip7.Size = new System.Drawing.Size(323, 39);
            this.toolStrip7.TabIndex = 4;
            this.toolStrip7.Text = "Statistics";
            this.toolStrip7.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStrip7_ItemClicked_1);
            // 
            // toolStripLabelStats
            // 
            this.toolStripLabelStats.Font = new System.Drawing.Font("Arial", 10F);
            this.toolStripLabelStats.Name = "toolStripLabelStats";
            this.toolStripLabelStats.Size = new System.Drawing.Size(44, 36);
            this.toolStripLabelStats.Text = "Stats:";
            // 
            // toolStripButtonRecalcStats
            // 
            this.toolStripButtonRecalcStats.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonRecalcStats.Font = new System.Drawing.Font("Arial", 10F);
            this.toolStripButtonRecalcStats.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonRecalcStats.Image")));
            this.toolStripButtonRecalcStats.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripButtonRecalcStats.Name = "toolStripButtonRecalcStats";
            this.toolStripButtonRecalcStats.Size = new System.Drawing.Size(36, 36);
            this.toolStripButtonRecalcStats.Text = "Recalc Stats";
            this.toolStripButtonRecalcStats.Click += new System.EventHandler(this.toolStripButtonRecalcStats_Click);
            // 
            // toolStripCopyTable
            // 
            this.toolStripCopyTable.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripCopyTable.Font = new System.Drawing.Font("Arial", 10F);
            this.toolStripCopyTable.Image = ((System.Drawing.Image)(resources.GetObject("toolStripCopyTable.Image")));
            this.toolStripCopyTable.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripCopyTable.Name = "toolStripCopyTable";
            this.toolStripCopyTable.Size = new System.Drawing.Size(36, 36);
            this.toolStripCopyTable.Text = "Copy table";
            this.toolStripCopyTable.ToolTipText = "Copy Statistics to Clipboard";
            this.toolStripCopyTable.Visible = false;
            // 
            // toolStripLabelAnalysis
            // 
            this.toolStripLabelAnalysis.Font = new System.Drawing.Font("Arial", 10F);
            this.toolStripLabelAnalysis.Name = "toolStripLabelAnalysis";
            this.toolStripLabelAnalysis.Size = new System.Drawing.Size(64, 36);
            this.toolStripLabelAnalysis.Text = "Analysis ";
            // 
            // toolStripAnalysisNr
            // 
            this.toolStripAnalysisNr.Font = new System.Drawing.Font("Arial", 10F);
            this.toolStripAnalysisNr.Name = "toolStripAnalysisNr";
            this.toolStripAnalysisNr.Size = new System.Drawing.Size(16, 36);
            this.toolStripAnalysisNr.Text = "0";
            // 
            // toolStripButtonMctTrend
            // 
            this.toolStripButtonMctTrend.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonMctTrend.Font = new System.Drawing.Font("Arial", 10F);
            this.toolStripButtonMctTrend.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonMctTrend.Image")));
            this.toolStripButtonMctTrend.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonMctTrend.Name = "toolStripButtonMctTrend";
            this.toolStripButtonMctTrend.Size = new System.Drawing.Size(83, 36);
            this.toolStripButtonMctTrend.Text = "MCT Trend";
            this.toolStripButtonMctTrend.ToolTipText = "Open MCT Trend";
            this.toolStripButtonMctTrend.Click += new System.EventHandler(this.toolStripButtonMctTrend_Click);
            // 
            // panel26
            // 
            this.panel26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel26.Controls.Add(this.listViewMeasurements);
            this.panel26.Controls.Add(this.toolStrip4);
            this.panel26.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel26.Location = new System.Drawing.Point(318, 0);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(326, 178);
            this.panel26.TabIndex = 3;
            // 
            // toolStrip4
            // 
            this.toolStrip4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.toolStrip4.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStrip4.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip4.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMeasurmentName,
            this.toolStripHr,
            this.toolStripDeleteteMeasurement,
            this.toolStripEditMeasurement,
            this.toolStripRecalcMeasurement,
            this.toolStripButtonStripP1,
            this.toolStripButtonStripP2});
            this.toolStrip4.Location = new System.Drawing.Point(0, 0);
            this.toolStrip4.Name = "toolStrip4";
            this.toolStrip4.Size = new System.Drawing.Size(324, 39);
            this.toolStrip4.TabIndex = 3;
            this.toolStrip4.Text = "toolStrip4";
            this.toolStrip4.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStrip4_ItemClicked);
            // 
            // toolStripMeasurmentName
            // 
            this.toolStripMeasurmentName.Font = new System.Drawing.Font("Arial", 10F);
            this.toolStripMeasurmentName.Name = "toolStripMeasurmentName";
            this.toolStripMeasurmentName.Size = new System.Drawing.Size(51, 36);
            this.toolStripMeasurmentName.Text = "<strip>";
            this.toolStripMeasurmentName.Click += new System.EventHandler(this.toolStripMeasurmentName_Click);
            // 
            // toolStripHr
            // 
            this.toolStripHr.Font = new System.Drawing.Font("Arial", 10F);
            this.toolStripHr.Name = "toolStripHr";
            this.toolStripHr.Size = new System.Drawing.Size(102, 36);
            this.toolStripHr.Text = "<HR 123 bpm>";
            // 
            // toolStripDeleteteMeasurement
            // 
            this.toolStripDeleteteMeasurement.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripDeleteteMeasurement.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDeleteteMeasurement.Image")));
            this.toolStripDeleteteMeasurement.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripDeleteteMeasurement.Name = "toolStripDeleteteMeasurement";
            this.toolStripDeleteteMeasurement.Size = new System.Drawing.Size(36, 36);
            this.toolStripDeleteteMeasurement.Text = "toolStripButton27";
            this.toolStripDeleteteMeasurement.ToolTipText = "Delete Measurement";
            this.toolStripDeleteteMeasurement.Click += new System.EventHandler(this.toolStripDeleteteMeasurement_Click);
            // 
            // toolStripEditMeasurement
            // 
            this.toolStripEditMeasurement.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripEditMeasurement.Image = ((System.Drawing.Image)(resources.GetObject("toolStripEditMeasurement.Image")));
            this.toolStripEditMeasurement.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripEditMeasurement.Name = "toolStripEditMeasurement";
            this.toolStripEditMeasurement.Size = new System.Drawing.Size(36, 36);
            this.toolStripEditMeasurement.Text = "toolStripButton26";
            this.toolStripEditMeasurement.ToolTipText = "Edit Measurement";
            this.toolStripEditMeasurement.Visible = false;
            // 
            // toolStripRecalcMeasurement
            // 
            this.toolStripRecalcMeasurement.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripRecalcMeasurement.Image = ((System.Drawing.Image)(resources.GetObject("toolStripRecalcMeasurement.Image")));
            this.toolStripRecalcMeasurement.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripRecalcMeasurement.Name = "toolStripRecalcMeasurement";
            this.toolStripRecalcMeasurement.Size = new System.Drawing.Size(36, 36);
            this.toolStripRecalcMeasurement.Text = "toolStripButton29";
            this.toolStripRecalcMeasurement.ToolTipText = "Recalc Measurement";
            this.toolStripRecalcMeasurement.Visible = false;
            // 
            // toolStripButtonStripP1
            // 
            this.toolStripButtonStripP1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonStripP1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonStripP1.Image")));
            this.toolStripButtonStripP1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonStripP1.Name = "toolStripButtonStripP1";
            this.toolStripButtonStripP1.Size = new System.Drawing.Size(27, 36);
            this.toolStripButtonStripP1.Text = "P1";
            this.toolStripButtonStripP1.Click += new System.EventHandler(this.toolStripButtonStripP1_Click);
            // 
            // toolStripButtonStripP2
            // 
            this.toolStripButtonStripP2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonStripP2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonStripP2.Image")));
            this.toolStripButtonStripP2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonStripP2.Name = "toolStripButtonStripP2";
            this.toolStripButtonStripP2.Size = new System.Drawing.Size(27, 36);
            this.toolStripButtonStripP2.Text = "P2";
            this.toolStripButtonStripP2.Click += new System.EventHandler(this.toolStripButtonStripP2_Click);
            // 
            // panel22
            // 
            this.panel22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panel22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel22.Controls.Add(this.listViewStrips);
            this.panel22.Controls.Add(this.toolStripStrips);
            this.panel22.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel22.Location = new System.Drawing.Point(0, 0);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(318, 178);
            this.panel22.TabIndex = 0;
            // 
            // contextMenuAmplitude
            // 
            this.contextMenuAmplitude.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem10,
            this.toolStripMenuItem13,
            this.toolStripMenuItem14,
            this.toolStripMenuItem15,
            this.toolStripMenuItem16,
            this.toolStripMenuItem17,
            this.toolStripMenuItem18});
            this.contextMenuAmplitude.Name = "contextMenuAmplitude";
            this.contextMenuAmplitude.Size = new System.Drawing.Size(185, 158);
            this.contextMenuAmplitude.Text = "Set Amplitude";
            this.contextMenuAmplitude.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenuAmplitude_ItemClicked);
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(184, 22);
            this.toolStripMenuItem10.Text = "50 mm/mV (0.1 mV)";
            // 
            // toolStripMenuItem13
            // 
            this.toolStripMenuItem13.Name = "toolStripMenuItem13";
            this.toolStripMenuItem13.Size = new System.Drawing.Size(184, 22);
            this.toolStripMenuItem13.Text = "25 mm/mV (0.2 mV)";
            // 
            // toolStripMenuItem14
            // 
            this.toolStripMenuItem14.Name = "toolStripMenuItem14";
            this.toolStripMenuItem14.Size = new System.Drawing.Size(184, 22);
            this.toolStripMenuItem14.Text = "10 mm/mV (0.5 mV)";
            // 
            // toolStripMenuItem15
            // 
            this.toolStripMenuItem15.Name = "toolStripMenuItem15";
            this.toolStripMenuItem15.Size = new System.Drawing.Size(184, 22);
            this.toolStripMenuItem15.Text = "5.0 mm/mV (1.0 mV)";
            // 
            // toolStripMenuItem16
            // 
            this.toolStripMenuItem16.Name = "toolStripMenuItem16";
            this.toolStripMenuItem16.Size = new System.Drawing.Size(184, 22);
            this.toolStripMenuItem16.Text = "2.5 mm/mV (2.0 mV)";
            // 
            // toolStripMenuItem17
            // 
            this.toolStripMenuItem17.Name = "toolStripMenuItem17";
            this.toolStripMenuItem17.Size = new System.Drawing.Size(184, 22);
            this.toolStripMenuItem17.Text = "1.0 mm/mV (5.0 mV)";
            // 
            // toolStripMenuItem18
            // 
            this.toolStripMenuItem18.Name = "toolStripMenuItem18";
            this.toolStripMenuItem18.Size = new System.Drawing.Size(184, 22);
            this.toolStripMenuItem18.Text = "0.5 mm/mV (10 mV)";
            // 
            // contextMenuFeed
            // 
            this.contextMenuFeed.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem19,
            this.toolStripMenuItem20,
            this.toolStripMenuItem21,
            this.toolStripMenuItem22,
            this.toolStripMenuItem23,
            this.toolStripMenuItem24,
            this.toolStripMenuItem25,
            this.toolStripMenuItem26,
            this.toolStripMenuItem27,
            this.toolStripMenuItem28,
            this.toolStripMenuItem29});
            this.contextMenuFeed.Name = "contextMenuFeed";
            this.contextMenuFeed.Size = new System.Drawing.Size(194, 246);
            this.contextMenuFeed.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenuFeed_ItemClicked);
            // 
            // toolStripMenuItem19
            // 
            this.toolStripMenuItem19.Name = "toolStripMenuItem19";
            this.toolStripMenuItem19.Size = new System.Drawing.Size(193, 22);
            this.toolStripMenuItem19.Text = "500 mm/Sec (0.01 Sec)";
            // 
            // toolStripMenuItem20
            // 
            this.toolStripMenuItem20.Name = "toolStripMenuItem20";
            this.toolStripMenuItem20.Size = new System.Drawing.Size(193, 22);
            this.toolStripMenuItem20.Text = "250 mm/Sec (0.02 Sec)";
            // 
            // toolStripMenuItem21
            // 
            this.toolStripMenuItem21.Name = "toolStripMenuItem21";
            this.toolStripMenuItem21.Size = new System.Drawing.Size(193, 22);
            this.toolStripMenuItem21.Text = "100 mm/Sec (0.05 Sec)";
            // 
            // toolStripMenuItem22
            // 
            this.toolStripMenuItem22.Name = "toolStripMenuItem22";
            this.toolStripMenuItem22.Size = new System.Drawing.Size(193, 22);
            this.toolStripMenuItem22.Text = "50 mm/Sec (0.10 Sec)";
            // 
            // toolStripMenuItem23
            // 
            this.toolStripMenuItem23.Name = "toolStripMenuItem23";
            this.toolStripMenuItem23.Size = new System.Drawing.Size(193, 22);
            this.toolStripMenuItem23.Text = "25 mm/Sec (0.20 Sec)";
            // 
            // toolStripMenuItem24
            // 
            this.toolStripMenuItem24.Name = "toolStripMenuItem24";
            this.toolStripMenuItem24.Size = new System.Drawing.Size(193, 22);
            this.toolStripMenuItem24.Text = "10 mm/Sec (0.50 Sec)";
            // 
            // toolStripMenuItem25
            // 
            this.toolStripMenuItem25.Name = "toolStripMenuItem25";
            this.toolStripMenuItem25.Size = new System.Drawing.Size(193, 22);
            this.toolStripMenuItem25.Text = "5 mm/Sec (1 Sec)";
            // 
            // toolStripMenuItem26
            // 
            this.toolStripMenuItem26.Name = "toolStripMenuItem26";
            this.toolStripMenuItem26.Size = new System.Drawing.Size(193, 22);
            this.toolStripMenuItem26.Text = "2.5 mm/Sec (2 Sec)";
            // 
            // toolStripMenuItem27
            // 
            this.toolStripMenuItem27.Name = "toolStripMenuItem27";
            this.toolStripMenuItem27.Size = new System.Drawing.Size(193, 22);
            this.toolStripMenuItem27.Text = "1 mm/Sec (5 Sec)";
            // 
            // toolStripMenuItem28
            // 
            this.toolStripMenuItem28.Name = "toolStripMenuItem28";
            this.toolStripMenuItem28.Size = new System.Drawing.Size(193, 22);
            this.toolStripMenuItem28.Text = "0.5 mm/Sec (10 Sec)";
            // 
            // toolStripMenuItem29
            // 
            this.toolStripMenuItem29.Name = "toolStripMenuItem29";
            this.toolStripMenuItem29.Size = new System.Drawing.Size(193, 22);
            this.toolStripMenuItem29.Text = "0.25 mm/Sec (20 Sec)";
            // 
            // contextMenuSingleClick
            // 
            this.contextMenuSingleClick.Name = "contextMenuSingleClick";
            this.contextMenuSingleClick.Size = new System.Drawing.Size(61, 4);
            this.contextMenuSingleClick.Closed += new System.Windows.Forms.ToolStripDropDownClosedEventHandler(this.contextMenuSingleClick_Closing);
            this.contextMenuSingleClick.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenuSingleClick_ItemClicked);
            // 
            // contextMenuDualClick
            // 
            this.contextMenuDualClick.Name = "contextMenuDualClick";
            this.contextMenuDualClick.Size = new System.Drawing.Size(61, 4);
            this.contextMenuDualClick.Closed += new System.Windows.Forms.ToolStripDropDownClosedEventHandler(this.contextMenuDualClick_Closed);
            this.contextMenuDualClick.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenuDualClick_ItemClicked);
            // 
            // contextMenuSelChannel
            // 
            this.contextMenuSelChannel.Name = "contextMenuSelChannel";
            this.contextMenuSelChannel.Size = new System.Drawing.Size(61, 4);
            this.contextMenuSelChannel.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuSelChannel_Opening);
            this.contextMenuSelChannel.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenuSelChannel_ItemClicked);
            // 
            // contextMenuAddChannel
            // 
            this.contextMenuAddChannel.Name = "contextMenuSelChannel";
            this.contextMenuAddChannel.Size = new System.Drawing.Size(61, 4);
            this.contextMenuAddChannel.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenuAddChannel_ItemClicked);
            // 
            // contextMenuExport
            // 
            this.contextMenuExport.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dVX1ToolStripMenuItem,
            this.mIT16ToolStripMenuItem,
            this.mIT24ToolStripMenuItem,
            this.mECGSimulatorToolStripMenuItem,
            this.simExportToolStripMenuItem,
            this.saveViewPartMIT16ToolStripMenuItem});
            this.contextMenuExport.Name = "contextMenuExport";
            this.contextMenuExport.Size = new System.Drawing.Size(186, 158);
            this.contextMenuExport.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuExport_Opening);
            // 
            // dVX1ToolStripMenuItem
            // 
            this.dVX1ToolStripMenuItem.Name = "dVX1ToolStripMenuItem";
            this.dVX1ToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.dVX1ToolStripMenuItem.Text = "DVX1";
            this.dVX1ToolStripMenuItem.Click += new System.EventHandler(this.dVX1ToolStripMenuItem_Click);
            // 
            // mIT16ToolStripMenuItem
            // 
            this.mIT16ToolStripMenuItem.Name = "mIT16ToolStripMenuItem";
            this.mIT16ToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.mIT16ToolStripMenuItem.Text = "MIT16";
            this.mIT16ToolStripMenuItem.Click += new System.EventHandler(this.mIT16ToolStripMenuItem_Click);
            // 
            // mIT24ToolStripMenuItem
            // 
            this.mIT24ToolStripMenuItem.Name = "mIT24ToolStripMenuItem";
            this.mIT24ToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.mIT24ToolStripMenuItem.Text = "MIT24";
            this.mIT24ToolStripMenuItem.Click += new System.EventHandler(this.mIT24ToolStripMenuItem_Click);
            // 
            // mECGSimulatorToolStripMenuItem
            // 
            this.mECGSimulatorToolStripMenuItem.Name = "mECGSimulatorToolStripMenuItem";
            this.mECGSimulatorToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.mECGSimulatorToolStripMenuItem.Text = "MECG simulator";
            this.mECGSimulatorToolStripMenuItem.Click += new System.EventHandler(this.mECGSimulatorToolStripMenuItem_Click);
            // 
            // simExportToolStripMenuItem
            // 
            this.simExportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pulseToolStripMenuItem,
            this.sinusToolStripMenuItem,
            this.sinusRangeToolStripMenuItem,
            this.triangleSawToolStripMenuItem,
            this.spikeToolStripMenuItem});
            this.simExportToolStripMenuItem.Name = "simExportToolStripMenuItem";
            this.simExportToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.simExportToolStripMenuItem.Text = "Sim export";
            this.simExportToolStripMenuItem.Click += new System.EventHandler(this.simExportToolStripMenuItem_Click);
            // 
            // pulseToolStripMenuItem
            // 
            this.pulseToolStripMenuItem.Name = "pulseToolStripMenuItem";
            this.pulseToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.pulseToolStripMenuItem.Text = "Pulse";
            this.pulseToolStripMenuItem.Click += new System.EventHandler(this.pulseToolStripMenuItem_Click);
            // 
            // sinusToolStripMenuItem
            // 
            this.sinusToolStripMenuItem.Name = "sinusToolStripMenuItem";
            this.sinusToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.sinusToolStripMenuItem.Text = "Sinus";
            this.sinusToolStripMenuItem.Click += new System.EventHandler(this.sinusToolStripMenuItem_Click);
            // 
            // sinusRangeToolStripMenuItem
            // 
            this.sinusRangeToolStripMenuItem.Name = "sinusRangeToolStripMenuItem";
            this.sinusRangeToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.sinusRangeToolStripMenuItem.Text = "Sinus range";
            this.sinusRangeToolStripMenuItem.Click += new System.EventHandler(this.sinusRangeToolStripMenuItem_Click);
            // 
            // triangleSawToolStripMenuItem
            // 
            this.triangleSawToolStripMenuItem.Name = "triangleSawToolStripMenuItem";
            this.triangleSawToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.triangleSawToolStripMenuItem.Text = "Triangle-Saw";
            this.triangleSawToolStripMenuItem.Click += new System.EventHandler(this.triangleSawToolStripMenuItem_Click);
            // 
            // spikeToolStripMenuItem
            // 
            this.spikeToolStripMenuItem.Name = "spikeToolStripMenuItem";
            this.spikeToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.spikeToolStripMenuItem.Text = "Spike-Peaks";
            this.spikeToolStripMenuItem.Click += new System.EventHandler(this.spikeToolStripMenuItem_Click);
            // 
            // saveViewPartMIT16ToolStripMenuItem
            // 
            this.saveViewPartMIT16ToolStripMenuItem.Name = "saveViewPartMIT16ToolStripMenuItem";
            this.saveViewPartMIT16ToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.saveViewPartMIT16ToolStripMenuItem.Text = "Save View part MIT16";
            this.saveViewPartMIT16ToolStripMenuItem.Click += new System.EventHandler(this.saveViewPartMIT16ToolStripMenuItem_Click);
            // 
            // FormAnalyze
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1533, 864);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusStrip1);
            this.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.Name = "FormAnalyze";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Analyze: John Doe { mo 16 June 2016 11:23:16  AM 60 sec}[Triaged] Analysis #1234";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormAnalyze_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormAnalyze_FormClosed);
            this.Shown += new System.EventHandler(this.FormAnalyze_Shown);
            this.SizeChanged += new System.EventHandler(this.FormAnalyze_SizeChanged);
            this.toolStripChn.ResumeLayout(false);
            this.toolStripChn.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel43.ResumeLayout(false);
            this.panel45.ResumeLayout(false);
            this.panel46.ResumeLayout(false);
            this.panel33.ResumeLayout(false);
            this.panel34.ResumeLayout(false);
            this.panel34.PerformLayout();
            this.panel44.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel18.ResumeLayout(false);
            this.panel29.ResumeLayout(false);
            this.panel29.PerformLayout();
            this.panel19.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel20.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel21.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panel32.ResumeLayout(false);
            this.contextMenuStripN.ResumeLayout(false);
            this.toolStripStrips.ResumeLayout(false);
            this.toolStripStrips.PerformLayout();
            this.toolStrip5.ResumeLayout(false);
            this.toolStrip5.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.panel42.ResumeLayout(false);
            this.panel28.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MaxAmplitude)).EndInit();
            this.panel16.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel25.ResumeLayout(false);
            this.panelZoomAT.ResumeLayout(false);
            this.panelZoomAT.PerformLayout();
            this.toolStripZoomAT.ResumeLayout(false);
            this.toolStripZoomAT.PerformLayout();
            this.panel30.ResumeLayout(false);
            this.panel30.PerformLayout();
            this.toolStrip12.ResumeLayout(false);
            this.toolStrip12.PerformLayout();
            this.panel31.ResumeLayout(false);
            this.panel31.PerformLayout();
            this.toolStripZoom.ResumeLayout(false);
            this.toolStripZoom.PerformLayout();
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel1.PerformLayout();
            this.splitContainer4.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            this.panelDrawChannels.ResumeLayout(false);
            this.splitContainerChannels.Panel1.ResumeLayout(false);
            this.splitContainerChannels.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerChannels)).EndInit();
            this.splitContainerChannels.ResumeLayout(false);
            this.panelChannelScrol.ResumeLayout(false);
            this.panelHR.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxHR)).EndInit();
            this.panel36.ResumeLayout(false);
            this.panel36.PerformLayout();
            this.panel37.ResumeLayout(false);
            this.panelGraphSeperator.ResumeLayout(false);
            this.panel38.ResumeLayout(false);
            this.panel39.ResumeLayout(false);
            this.panelGraph.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGraph)).EndInit();
            this.panel41.ResumeLayout(false);
            this.panel41.PerformLayout();
            this.panel47.ResumeLayout(false);
            this.panelChannelsInfo.ResumeLayout(false);
            this.panelChannelsButtons.ResumeLayout(false);
            this.panelChannelsButtons.PerformLayout();
            this.panel52.ResumeLayout(false);
            this.panel52.PerformLayout();
            this.panelStripAT.ResumeLayout(false);
            this.panelStripAT.PerformLayout();
            this.toolStrip10.ResumeLayout(false);
            this.toolStrip10.PerformLayout();
            this.panelSAF.ResumeLayout(false);
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel35.ResumeLayout(false);
            this.panelAutoMeasurements.ResumeLayout(false);
            this.panelAutoMeasurements.PerformLayout();
            this.toolStrip9.ResumeLayout(false);
            this.toolStrip9.PerformLayout();
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            this.toolStrip7.ResumeLayout(false);
            this.toolStrip7.PerformLayout();
            this.panel26.ResumeLayout(false);
            this.panel26.PerformLayout();
            this.toolStrip4.ResumeLayout(false);
            this.toolStrip4.PerformLayout();
            this.panel22.ResumeLayout(false);
            this.panel22.PerformLayout();
            this.contextMenuAmplitude.ResumeLayout(false);
            this.contextMenuFeed.ResumeLayout(false);
            this.contextMenuExport.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.TextBox textBoxPatientRemark;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label labelEventLabel;
        private System.Windows.Forms.Label labelEventPriority;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox textBoxStudyRemark;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Button buttonSaveOk;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonPrint;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Label labelUser;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripN;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem secToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem secToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem secToolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem verticalUnitToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripMenuItem mSecToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem12;
        private System.Windows.Forms.ToolStripMenuItem secToolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem secToolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem secToolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem secToolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem secToolStripMenuItem12;
        private System.Windows.Forms.ToolStripMenuItem mVToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mVToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem mVToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem mVToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem mVToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem mVToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem mVToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem lineTypeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pointToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lineToolStripMenuItem;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.Label labelPatIDHeader;
        private System.Windows.Forms.ToolStrip miniToolStrip;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripButton toolStripButton17;
        private System.Windows.Forms.ToolStrip toolStrip5;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Panel panelZoomStrip;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Panel panelZoomAT;
        private System.Windows.Forms.ToolStrip toolStripZoomAT;
        private System.Windows.Forms.ToolStripLabel toolStripZoomEndTime;
        private System.Windows.Forms.ToolStripLabel toolStripLabel11;
        private System.Windows.Forms.ToolStripButton toolStripZoomSpeed;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.ToolStrip toolStripZoom;
        private System.Windows.Forms.ToolStripLabel toolStripZoomChannel;
        private System.Windows.Forms.ToolStripLabel toolStripLabel18;
        private System.Windows.Forms.ToolStripLabel toolStripZoomStartTime;
        private System.Windows.Forms.ToolStripLabel toolStripLabel20;
        private System.Windows.Forms.ToolStripButton toolStripZoomTimeOut;
        private System.Windows.Forms.ToolStripButton toolStripZoomDuration;
        private System.Windows.Forms.ToolStripButton toolStripZoomTimeIncrease;
        private System.Windows.Forms.ToolStripLabel toolStripLabel21;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.Panel panelAutoMeasurements;
        private System.Windows.Forms.TreeView treeViewAutoMeasurements;
        private System.Windows.Forms.ToolStrip toolStrip9;
        private System.Windows.Forms.ToolStripLabel toolStripLabel5;
        private System.Windows.Forms.ToolStripButton toolStripCopyAuto;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.ListView listViewStats;
        private System.Windows.Forms.ColumnHeader cStag;
        private System.Windows.Forms.ColumnHeader cSn;
        private System.Windows.Forms.ColumnHeader cSmin;
        private System.Windows.Forms.ColumnHeader cSmean;
        private System.Windows.Forms.ColumnHeader cSmax;
        private System.Windows.Forms.ColumnHeader cSunit;
        private System.Windows.Forms.ToolStrip toolStrip7;
        private System.Windows.Forms.ToolStripLabel toolStripLabelStats;
        private System.Windows.Forms.ToolStripButton toolStripCopyTable;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.ListView listViewMeasurements;
        private System.Windows.Forms.ColumnHeader cMtime;
        private System.Windows.Forms.ColumnHeader cMtag;
        private System.Windows.Forms.ColumnHeader cMvalue;
        private System.Windows.Forms.ColumnHeader cMunit;
        private System.Windows.Forms.ToolStrip toolStrip4;
        private System.Windows.Forms.ToolStripLabel toolStripMeasurmentName;
        private System.Windows.Forms.ToolStripLabel toolStripHr;
        private System.Windows.Forms.ToolStripButton toolStripDeleteteMeasurement;
        private System.Windows.Forms.ToolStripButton toolStripEditMeasurement;
        private System.Windows.Forms.ToolStripButton toolStripRecalcMeasurement;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.ListView listViewStrips;
        private System.Windows.Forms.ColumnHeader cChannel;
        private System.Windows.Forms.ColumnHeader cDuration;
        private System.Windows.Forms.ColumnHeader cDate;
        private System.Windows.Forms.ColumnHeader cTime;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Label labelStudyText;
        private System.Windows.Forms.Panel panelSAF;
        private System.Windows.Forms.ToolStripButton toolStripZoomAmplitude;
        private System.Windows.Forms.Panel panelChannelsInfo;
        private System.Windows.Forms.Panel panelAllChannels;
        private System.Windows.Forms.Label labelPatID;
        private System.Windows.Forms.Label labelStudy;
        private System.Windows.Forms.ToolStripButton toolStripWindowLeft;
        private System.Windows.Forms.ToolStripButton toolStripWindowRight;
        private System.Windows.Forms.ToolStripButton toolStripWindowDown;
        private System.Windows.Forms.ToolStripButton toolStripWindowCenter;
        private System.Windows.Forms.ToolStripButton toolStripWindowUp;
        private System.Windows.Forms.ContextMenuStrip contextMenuAmplitude;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem13;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem14;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem15;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem16;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem17;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem18;
        private System.Windows.Forms.ContextMenuStrip contextMenuFeed;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem19;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem20;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem21;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem22;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem23;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem24;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem25;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem26;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem27;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem28;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem29;
        private System.Windows.Forms.ContextMenuStrip contextMenuSingleClick;
        private System.Windows.Forms.ContextMenuStrip contextMenuDualClick;
        private System.Windows.Forms.ToolStripButton toolStripAmpDec;
        private System.Windows.Forms.ToolStripButton toolStripAmpInc;
        private System.Windows.Forms.ToolStripLabel toolStripLabelAnalysis;
        private System.Windows.Forms.ToolStripLabel toolStripAnalysisNr;
        private System.Windows.Forms.ToolStripButton toolStripSelectZoom;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Label labelEventTime;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.TextBox textBoxEventRemark;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripButton toolStripButtonRecalcStats;
        private System.Windows.Forms.ColumnHeader cMchannel;
        private System.Windows.Forms.Button buttonHelpDesk;
        private System.Windows.Forms.Label labelStudyProcCodes;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.ToolStrip toolStrip12;
        private System.Windows.Forms.ToolStripButton toolStripOneClick;
        private System.Windows.Forms.ToolStripButton toolStripTwoClick;
        private System.Windows.Forms.ToolStripButton toolStripSnap;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.Button buttonStudyFindings;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Label labelNextState;
        private System.Windows.Forms.ComboBox comboBoxState;
        private System.Windows.Forms.Panel panelChannelsButtons;
        private System.Windows.Forms.Panel panelStripAT;
        private System.Windows.Forms.ToolStrip toolStrip10;
        private System.Windows.Forms.ToolStripLabel toolStripChannelEndTime;
        private System.Windows.Forms.ToolStripLabel toolStripLabel15;
        private System.Windows.Forms.ToolStripButton toolStripChannelSpeed;
        private System.Windows.Forms.ToolStripButton toolStripChannelAmplitute;
        private System.Windows.Forms.Panel panel52;
        private System.Windows.Forms.Button buttonFindingsRhythm;
        private System.Windows.Forms.ToolStripButton toolStripButtonMctTrend;
        private System.Windows.Forms.Button buttonPdfView;
        private System.Windows.Forms.Panel panelChannelScrol;
        private System.Windows.Forms.Panel panelDrawChannels;
        private System.Windows.Forms.SplitContainer splitContainerChannels;
        private System.Windows.Forms.Panel panelChannel6;
        private System.Windows.Forms.Panel panelChannel5;
        private System.Windows.Forms.Panel panelChannel4;
        private System.Windows.Forms.Panel panelChannel3;
        private System.Windows.Forms.Panel panelChannel2;
        private System.Windows.Forms.Panel panelChannel1;
        private System.Windows.Forms.Panel panelChannel12;
        private System.Windows.Forms.Panel panelChannel11;
        private System.Windows.Forms.Panel panelChannel10;
        private System.Windows.Forms.Panel panelChannel9;
        private System.Windows.Forms.Panel panelChannel8;
        private System.Windows.Forms.Panel panelChannel7;
        private System.Windows.Forms.ToolStripLabel toolStripL1;
        private System.Windows.Forms.ToolStripLabel toolStripNrChannels;
        private System.Windows.Forms.ToolStripLabel toolStripSpace;
        private System.Windows.Forms.ToolStripLabel toolStripChannelsStart;
        private System.Windows.Forms.ToolStripLabel toolStripLabel8;
        private System.Windows.Forms.ToolStripButton toolStripChannelTimeIncrease;
        private System.Windows.Forms.ToolStripButton toolStripChannelPeriod;
        private System.Windows.Forms.ToolStripButton toolStripChannelTimeDecrease;
        private System.Windows.Forms.ToolStripButton toolStripChannelTimeLeft;
        private System.Windows.Forms.ToolStripButton toolStripChannelTimeRight;
        private System.Windows.Forms.ToolStripLabel toolStripLabel16;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripLabel toolStripChannelStartTimeDate;
        private System.Windows.Forms.ToolStripLabel toolStripLabel23;
        private System.Windows.Forms.ToolStripButton toolStripChannelDuration;
        private System.Windows.Forms.Panel panel42;
        private System.Windows.Forms.Button buttonAnPause;
        private System.Windows.Forms.Button buttonAnBrady;
        private System.Windows.Forms.Button buttonAnTachy;
        private System.Windows.Forms.Button buttonAnNormal;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.Button buttonAflut;
        private System.Windows.Forms.Button buttonAnAFib;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Button buttonOther;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private System.Windows.Forms.ToolStripButton toolStripFullStrip;
        private System.Windows.Forms.ToolStrip toolStripStrips;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripButton toolStripAddStrip;
        private System.Windows.Forms.ToolStripButton toolStripDeleteStrip;
        private System.Windows.Forms.ToolStripButton toolStripView;
        private System.Windows.Forms.ToolStripButton toolStripCopyWindow;
        private System.Windows.Forms.ToolStripButton toolStripFirst;
        private System.Windows.Forms.ToolStripButton toolStripBaseLine;
        private System.Windows.Forms.ContextMenuStrip contextMenuSelChannel;
        private System.Windows.Forms.ToolStripButton toolStripSetDupStrip;
        private System.Windows.Forms.ContextMenuStrip contextMenuAddChannel;
        private System.Windows.Forms.Panel panel43;
        private System.Windows.Forms.Panel panel45;
        private System.Windows.Forms.Label labelStripTime;
        private System.Windows.Forms.Label labeAllChannels;
        private System.Windows.Forms.Label labelWholeStrip;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel46;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel44;
        private System.Windows.Forms.ComboBox comboBoxStripSec;
        private System.Windows.Forms.CheckBox checkBoxAllChannels;
        private System.Windows.Forms.CheckBox checkBoxHoleStrip;
        private System.Windows.Forms.CheckBox checkBoxAddDisclosure;
        private System.Windows.Forms.CheckBox checkBoxAnonymize;
        private System.Windows.Forms.Button buttonAnPVC;
        private System.Windows.Forms.Button buttonAnPAC;
        private System.Windows.Forms.Button buttonAnVtach;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TextBox textBoxActivities;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxSymptoms;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.TextBox textBoxFindings;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Button buttonAnBline;
        private System.Windows.Forms.Button buttonAnPace;
        private System.Windows.Forms.Button buttonSelectFindings;
        private System.Windows.Forms.CheckBox checkBoxCh2;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.Button buttonExportData;
        private System.Windows.Forms.Panel panel34;
        private System.Windows.Forms.ContextMenuStrip contextMenuExport;
        private System.Windows.Forms.ToolStripMenuItem dVX1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mIT16ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mECGSimulatorToolStripMenuItem;
        public System.Windows.Forms.ToolStrip toolStripChn;
        private System.Windows.Forms.CheckBox checkBoxBlackWhite;
        private System.Windows.Forms.ToolStripMenuItem mIT24ToolStripMenuItem;
        private System.Windows.Forms.Label labelCreateSec;
        private System.Windows.Forms.Label labelLoadSec;
        private System.Windows.Forms.Label labelShowTotalSec;
        public System.Windows.Forms.NumericUpDown MaxAmplitude;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox AutoScaleCheckbox;
        private System.Windows.Forms.ToolStripLabel toolStripARange1;
        private System.Windows.Forms.ToolStripLabel toolStripARangeN;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox checkBoxQCD;
        private System.Windows.Forms.Panel panel35;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelPrevTech;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButtonInvert;
        private System.Windows.Forms.Button buttonAnAbNormal;
        private System.Windows.Forms.Label labelStudyNote;
        private System.Windows.Forms.ToolStripMenuItem simExportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pulseToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripPageScan;
        private System.Windows.Forms.Panel panelHR;
        private System.Windows.Forms.Panel panel36;
        private System.Windows.Forms.Panel panel37;
        private System.Windows.Forms.ToolStripLabel toolStripLabelHrInfo;
        private System.Windows.Forms.PictureBox pictureBoxHR;
        private System.Windows.Forms.Label labelHR;
        private System.Windows.Forms.Label label1MeanHR;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label labelMeanBurden;
        private System.Windows.Forms.Panel panel38;
        private System.Windows.Forms.HScrollBar ScrollBarChannel;
        private System.Windows.Forms.Panel panel39;
        private System.Windows.Forms.ToolStripButton toolStripButtonLPF;
        private System.Windows.Forms.ToolStripButton toolStripButtonHPF;
        private System.Windows.Forms.ToolStripButton toolStripButtonBBF;
        private System.Windows.Forms.ToolStripMenuItem sinusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sinusRangeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem triangleSawToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem spikeToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButtonACDC;
        private System.Windows.Forms.ToolStripButton toolStripButtonAvgCR;
        private System.Windows.Forms.ToolStripButton toolStripButtonAddChannel;
        private System.Windows.Forms.Button buttonCalcRR;
        private System.Windows.Forms.ToolStripButton toolStripButtonP2;
        private System.Windows.Forms.ToolStripButton toolStripButtonP1;
        private System.Windows.Forms.ToolStripButton toolStripButtonStripP1;
        private System.Windows.Forms.ToolStripButton toolStripButtonStripP2;
        private System.Windows.Forms.Panel panelGraph;
        private System.Windows.Forms.PictureBox pictureBoxGraph;
        private System.Windows.Forms.Panel panel41;
        private System.Windows.Forms.Label labelGraphRight3;
        private System.Windows.Forms.Label labelGraphRight2;
        private System.Windows.Forms.Label labelGraphRight1;
        private System.Windows.Forms.Panel panel47;
        private System.Windows.Forms.Label labelGraphHeader;
        private System.Windows.Forms.Panel panelGraphSeperator;
        private System.Windows.Forms.Panel panelGraphMarker;
        private System.Windows.Forms.ToolStripMenuItem saveViewPartMIT16ToolStripMenuItem;
    }
}