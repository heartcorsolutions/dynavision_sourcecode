﻿using Event_Base;
using EventBoard.EntryForms;
using EventBoard.Event_Base;
using EventboardEntryForms;
using PdfSharp;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using Program_Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventBoard
{
    public partial class CPrintAnalysisForm : Form
    {
        UInt32 _mRecordIndex;
        //        UInt32 mAnalysisIndex;
        UInt16 _mAnalysisNr;
        CRecordMit _mRecordDb;
        CRecordMit _mRecordFile;
        CRecAnalysis _mRecAnalysis;
        CRecAnalysisFile _mRecAnalysisFile;
        string _mRecordFilePath;

        bool _mbDisclosure, _mbHoleStrip, _mbAllChannels;
        bool _mbAnonymize;
        private bool _mbPlot2Ch = false;
        UInt16 _mStripSec = 60;

        bool _mbFirst = true;
        bool _mbSecondPage = false;
        UInt16 _mDiscloseBasePageNr = 0;
        UInt16 _mDiscloseIndexPageNr = 0;
        Color _mDefaultPanelColor;
        UInt16 _mDisclosureImageWidth = 0;
        UInt16 _mDisclosureImageHeight = 0;

        Int16 _mDisclosureStripIndex = -1;
        Int16 _mDisclosureStripPart = 0;
        UInt16 _mDisclosureChannel = 0;
        UInt16 _mDisclosureNrChannels = 0;

        DateTime _mDisclosureDT = DateTime.MinValue;
        DateTime _mDisclosureEventDT = DateTime.MinValue;
        float _mDisclosureStartSec = -1;
        float _mDisclosureEndSec = -1;
        float _mDisclosureEventSec = -1;

        float _mDisclosureUnitT = 1.0F;
        float _mDisclosureUnitA = 0.5F;

        float _mDisclosureStartA = -2.0F;
        float _mDisclosureEndA = 2.0F;

        bool _mbDisclosureLastPage = false;
        bool _mbDisclosureFinished = false;

        bool _bDisclosureDebug = false;

        List<Bitmap> mPrintBitmapList;
        //Bitmap mPrint2Bitmap;

        UInt16 mNrPages = 0;
        UInt16 mNrSamples = 0;
        UInt16 mCurrentPrintPage = 0;
        const UInt16 _cNrSamplesFirstPage = 2;
        const UInt16 _cNrSamplesPerPage = 4;

        CRecordMit _mBaseLineRecordDb;
        CRecordMit _mBaseLineRecordFile;
        CRecAnalysis _mBaseLineRecAnalysis;
        CRecAnalysisFile _mBaseLineRecAnalysisFile;

        public CDeviceInfo _mDeviceInfo = null;
        public CStudyInfo _mStudyInfo = null;
        public CPatientInfo _mPatientInfo = null;

        public string _mCreadedByInitials;
        public UInt16 _mStudyReportNr = 0;
        public UInt32 _mReportIX = 0;

        public UInt16 _mCurrentActiveIndex = 0;

        private bool _mbHideReportNr = false;
        private bool _mbBlackWhite = false;

        private bool bNotProgrammer = false;
        private bool _mbQCd = false;

        private bool _mbWarnEmptyBmp = true;

        //CMeasureStrip _mBaseLineStrip;

        /*        public static bool sStoreBaseLine(CRecordMit ARecordDb, CRecordMit ARecordFile, CRecAnalysis ARecAnalysis, CRecAnalysisFile ARecAnalysisFile, CMeasureStrip AStrip)
                {
                    bool bOk = false;

                    if(ARecordDb != null && ARecordFile != null && ARecAnalysis != null && ARecAnalysisFile != null && AStrip != null)
                    {
                        _mBaseLineRecordDb = ARecordDb;
                        _mBaseLineRecordFile = ARecordFile;
                        _mBaseLineRecAnalysis = ARecAnalysis;
                        _mBaseLineRecAnalysisFile = ARecAnalysisFile;
                        _mBaseLineStrip = AStrip;
                        bOk = true;
                    }
                    return bOk;
                }
        */
        public CPrintAnalysisForm(FormAnalyze AAnalyzeForm, bool AbSmall, string ACreadedByInitials, bool AbDisclosure, bool AbHoleStrip, bool AbAllChannels,
            UInt16 AStripSec, bool AbPlot2Ch, bool AbAnonymize, bool AbPrintBlackWhite, bool AbQCd)
        {
            try
            {
                bNotProgrammer = true;

                if (CLicKeyDev.sbDeviceIsProgrammer())
                {
                    bNotProgrammer = true;
                }

                _mRecordIndex = AAnalyzeForm._mRecordIndex;
                //            mAnalysisIndex = AAnalyzeForm.mAna;
                _mAnalysisNr = AAnalyzeForm._mAnalysisSeqNr;
                _mRecordDb = AAnalyzeForm._mRecordDb;
                _mRecordFile = AAnalyzeForm._mRecordFile;
                _mRecordFilePath = AAnalyzeForm._mRecordFilePath;
                _mRecAnalysis = AAnalyzeForm._mRecAnalysis;
                _mRecAnalysisFile = AAnalyzeForm._mRecAnalysisFile;
                _mBaseLineRecordDb = AAnalyzeForm._mBaseLineRecordDb;
                _mBaseLineRecordFile = AAnalyzeForm._mBaseLineRecordFile;
                _mBaseLineRecAnalysis = AAnalyzeForm._mBaseLineRecAnalysis;
                _mBaseLineRecAnalysisFile = AAnalyzeForm._mBaseLineRecAnalysisFile;

                _mCreadedByInitials = ACreadedByInitials;
                //            _mBaseLineStrip = AAnalyzeForm.;

                _mDeviceInfo = AAnalyzeForm._mDeviceInfo;
                _mStudyInfo = AAnalyzeForm._mStudyInfo;
                _mPatientInfo = AAnalyzeForm._mPatientInfo;

                _mbDisclosure = AbDisclosure;
                _mbHoleStrip = AbHoleStrip;
                _mbAllChannels = AbAllChannels;
                _mStripSec = AStripSec;
                _mbPlot2Ch = AbPlot2Ch;
                _mbAnonymize = AbAnonymize;
                _mbBlackWhite = AbPrintBlackWhite;
                _mbQCd = AbQCd;

                InitializeComponent();

                _mbHideReportNr = Properties.Settings.Default.HideReportNr;

                mPrintBitmapList = new List<Bitmap>();

                if (AbSmall)
                {
                    Width = 333;
                    Height = 85;
                }

                toolStrip1.Focus();
                //            toolStripButtonPrint.

                string s = "";
                string reportStr = "";

                _mStudyReportNr = 0;

                if (false == _mbAnonymize && _mStudyInfo != null)
                {
                    if (_mStudyInfo.mbGetNextReportNr(out _mStudyReportNr))
                    {
                        if (false == _mbHideReportNr)
                        {
                            s = _mAnalysisNr > 1 ? " (" + _mAnalysisNr + ")" : "";

                            reportStr = _mStudyReportNr.ToString();
                        }
                    }
                }
                CProgram.sbLogMemSize("opening Analysis print R#" + _mRecordDb.mIndex_KEY.ToString() + " S"
    + _mRecordDb.mStudy_IX.ToString() + "." + _mRecordDb.mSeqNrInStudy.ToString() + " report "
+ _mStudyReportNr.ToString());

                if (_mStudyReportNr == 0)
                {
                    labelReportText2.Text = "";
                    labelReportText3.Text = "";
                }
                labelReportNr.Text = reportStr + s;
                labelPage2ReportNr.Text = reportStr;
                labelPage3ReportNr.Text = reportStr;

                _mDefaultPanelColor = labelStripLead1.BackColor;
                _mDisclosureImageWidth = (UInt16)pictureBoxStrip1.Width;
                _mDisclosureImageHeight = (UInt16)pictureBoxStrip1.Height;

                string strFindings = _mbQCd ? "QC Findings:" : "Findings:";

                labelFindingsText1.Text = strFindings;
                labelFindingsText2.Text = strFindings;
                labelFindingsText3.Text = strFindings;
                labelFindingsText4.Text = strFindings;
                labelFindingsText5.Text = strFindings;
                labelFindingsTextBL.Text = strFindings;
                labelQC.Text = _mbQCd ? "QC" : "";

//                labelTextQC1.Text = labelQC.Text;
  //              labelTextQC2.Text = labelQC.Text;

                mSetFormValues();
                mUpdateMemUse();

                BringToFront();

            }
            catch (Exception ex)
            {
                CProgram.sLogException("Init print Analysis", ex);
            }
        }

        public void mSetFormValues()
        {
            try
            {
                Image img = CDvtmsData.sGetPrintCenterImage(_mbBlackWhite);
                pictureBoxCenter.Image = img;
                pictureBoxCenter2.Image = img;
                string s = CDvtmsData.sGetPrintCenterLine1();

                labelCenter1.Text = s;
                labelCenter1p2.Text = s;

                s = CDvtmsData.sGetPrintCenterLine2();
                labelCenter2.Text = s;
                labelCenter2p2.Text = s;

                string userTitle = Properties.Settings.Default.PrintReportEvent;
                if (userTitle == null || userTitle.Length < 1)
                {
                    userTitle = "Cardiac Event Report";
                }
                if (_mbHideReportNr == false)
                {
                    userTitle += "  " + labelReportNr.Text;
                }

                if( _mRecordIndex == 0)
                {
                    userTitle = Path.GetFileNameWithoutExtension(_mRecordFilePath);

                    Font newFont = new Font(labelCardiacEventReportNr.Font.FontFamily, labelCardiacEventReportNr.Font.SizeInPoints / 2, FontStyle.Bold);

                    labelCardiacEventReportNr.Font = newFont;
                }
                labelCardiacEventReportNr.Text = userTitle;
                if (_mbHideReportNr)
                {
                    labelReportText2.Text = "";
                }

                if (_mRecordDb == null || _mRecordFile == null || _mRecAnalysis == null || _mRecAnalysisFile == null)
                {
                    CProgram.sPromptError(false, "PrintAnalysis", "Form not initialized");
                    Close();
                }
                else
                {
                    mNrSamples = _mRecAnalysisFile.mCountActiveStrips();
                    _mbSecondPage = mNrSamples > _cNrSamplesFirstPage;
                    mNrPages = (UInt16)(mNrSamples <= _cNrSamplesFirstPage ? 1 : 2 + (mNrSamples - _cNrSamplesFirstPage - 1) / _cNrSamplesPerPage);
                    mCurrentPrintPage = 1;
                    //test mNrPages = 2;
                    if (mNrPages > 2) mNrPages = 2; // for now maximum strips = 6 => 2 pages
                    _mDiscloseBasePageNr = mNrPages;
                    if (_mbDisclosure)
                    {
                        ++mNrPages;

                        if(_mbHoleStrip )
                        {
                            float stripLen = _mRecordFile.mGetSamplesTotalTime();
                            int nrStrips = (int)(stripLen / (_mStripSec < 1 ? 1 : _mStripSec));

                            if(_mbAllChannels )
                            {
                                nrStrips *= _mRecordFile.mNrSignals;
                            }
                            int nEstimate = mNrPages + nrStrips / 20 + 1;
                            string sEstimate = "Print Analysis ~ " + nEstimate.ToString() + " pages incl full disclosure ";
                            Text = sEstimate;

                            if( nEstimate > 15 )
                            {
                                if( false == CProgram.sbAskOkCancel( "Analysi Print full disclosure", "Print "+ nEstimate.ToString() + " pages, continue?" ))
                                {
                                    Close();
                                    return;
                                }
                            }
                        }
                    }

                    
                    labelPage2OfX.Text = mNrPages.ToString();
                    labelPage1OfX.Text = mNrPages.ToString();
                    labelPage3OfX.Text = mNrPages.ToString();

                    mInitHeader();
                    mInitFooter();
                    mInitBaseLine();
                    mInitPrevious();
                    mInitCurrent();
                    mInitSample1();
                    mInitSample2();
                    mInitSample3();
                    mInitSample4();
                    mInitSample5();

                    mbMoveToFirstDisclosureStrip();
                    mInitDisclosurePage();
                }

                toolStrip1.Focus();
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Set Analysis Print Form  error", ex);
            }
        }

        void mInitHeader()
        {
            DateTime dt;
            //            string s, tel;
            bool bStudy = false == _mbAnonymize && _mStudyInfo != null && _mStudyInfo.mIndex_KEY > 0;
            bool bPatient = false == _mbAnonymize && _mPatientInfo != null && _mPatientInfo.mIndex_KEY > 0;

            /*           s = "";
                       tel = "";
                       if( bStudy && _mStudyInfo._mClient_IX > 0 )
                       {
                           CClientInfo client = new EventboardEntryForms.CClientInfo(_mStudyInfo.mGetSqlConnection());

                           if( client != null)
                           {
                               client.mbDoSqlSelectIndex(_mStudyInfo._mClient_IX, client.mGetValidMask(true));// CSqlDataTableRow.sGetMask((UInt16)DClientInfoVars.Label));
                               s = client._mFullName;// _mLabel;
                               if(s == null || s.Length == 0 ) s = client._mLabel;
                               tel = client._mDepartmentTelNr;
                           }
                       }
                       labelClientName.Text = s; // "New York Hospital";
                       labelClientTel.Text = tel;
                                             //            labelClientHeader.Text = "CLIENT:";
           */
            CClientInfo.sFillPrintClientName(_mbAnonymize, _mStudyInfo, labelClientName, labelClientTel, false);

            /*               s = "";
               tel = "";
               if( bStudy && _mStudyInfo._mRefPhysisian_IX > 0)
               {
                   CPhysicianInfo physician = new CPhysicianInfo(_mStudyInfo.mGetSqlConnection());

                   if( physician != null )
                   {
                       physician.mbDoSqlSelectIndex(_mStudyInfo._mRefPhysisian_IX, physician.mGetValidMask(true));// CSqlDataTableRow.sGetMask((UInt16)DPhysicianInfoVars.Label));
                       s = physician._mLabel;
                       tel = physician._mPhone1.mDecrypt();
                   }
               }
               labelRefPhysicianName.Text = s; // "Dr. John Smithsonian";
               labelRefPhysicianTel.Text = tel;
               //labelRefPhysHeader.Text = "REFERRING PHYSICIAN:";
              */
            CPhysicianInfo.sFillPrintPhysicianName(_mbAnonymize, _mStudyInfo, true, labelRefPhysicianName, labelRefPhysicianTel, false);
            /*
          s = "";
            tel = "";
            if (bStudy && _mStudyInfo._mPhysisian_IX > 0)
            {
                CPhysicianInfo physician = new CPhysicianInfo(_mStudyInfo.mGetSqlConnection());

                if (physician != null)
                {
                    physician.mbDoSqlSelectIndex(_mStudyInfo._mPhysisian_IX, physician.mGetValidMask(true));// CSqlDataTableRow.sGetMask((UInt16)DPhysicianInfoVars.Label));
                    s = physician._mLabel;
                    tel = physician._mPhone1.mDecrypt();
                }
            }
            else
            {
                s = _mRecordDb.mPhysicianName.mDecrypt();
            }
            labelPhysicianName.Text = s; // "Dr. John Smithsonian";
            labelPhysicianTel.Text = tel;
            //labelPhysHeader.Text = "PHYSICIAN:";

            */
            CPhysicianInfo.sFillPrintPhysicianName(_mbAnonymize, _mStudyInfo, false, labelPhysicianName, labelPhysicianTel, false);

            labelSerialNr.Text = _mRecordDb.mDeviceID; // "AA102044403";
                                                       //            labelSerialNrHeader.Text = "RECORDER SERIAL#:";

            labelStartDate.Text = bStudy ? CProgram.sDateToString(_mStudyInfo._mStudyStartDate) : ""; // "10/23/2016"; study start
                                                                                                      //            labelStartDateHeader.Text = "START DATE:";

            string studyStr = CRecordMit.sGetPrintStudyRecord(_mRecordDb);

            labelStudyNr.Text = studyStr;
            labelStudyNumberResPage2.Text = studyStr;
            labelStudyNumberResPage3.Text = studyStr;
            //labelStudyNrHeader.Text = "STUDY#:";

            labelPatIDHeader.Text = bPatient ? _mPatientInfo.mGetPatientSocIDText().ToUpper() + ":" : "";
            labelPatID.Text = _mbAnonymize ? "" : (bPatient ? _mPatientInfo.mGetPatientSocIDValue(false) : _mRecordDb.mPatientID.mDecrypt());
            labelPatientIDResult.Text = labelPatID.Text;
            labelPatientIDResult3.Text = labelPatID.Text;

            string room = "";
            if(bStudy)
            {
                room = _mStudyInfo._mHospitalRoom;
                if( room != null && room.Length > 0 && _mStudyInfo._mHospitalBed != null && _mStudyInfo._mHospitalBed.Length > 0)
                {
                    room += " - "+ _mStudyInfo._mHospitalBed;
                }
            }
            labelRoom.Text = room;

            //            labelPatIDHeader.Text = "PATIENT ID:";
            labelPhone2.Text = bPatient ? _mPatientInfo._mPatientPhone2.mDecrypt() : ""; // "800-217-0520";
            labelPhone1.Text = bPatient ? _mPatientInfo._mPatientPhone1.mDecrypt() : ""; // "800-217-0520";
            //labelPhoneHeader.Text = "PHONE:";

            /*labelZipCity.Text = bPatient ? (CProgram.sbGetImperial() ? _mPatientInfo._mPatientCity + ", " + _mPatientInfo._mPatientZip.mDecrypt()
                                        : _mPatientInfo._mPatientZip.mDecrypt() + "  " + _mPatientInfo._mPatientCity ) : ""; // "Texas TX 200111";
            labelAddress.Text = bPatient ? _mPatientInfo._mPatientAddress.mDecrypt() : ""; // "Alpha Street 112, Houston";
            */
            CPatientInfo.sFillPrintPatientAddress(_mbAnonymize, _mPatientInfo, labelAddress, labelZipCity, labelCountry);

            //labelAddressHeader.Text = "ADDRESS:";
            /*            int ageYears = bPatient ? _mPatientInfo.mGetAgeYears() : _mRecordDb.mGetAgeYears();// + ", "; // + 
                        string ageGender = ageYears >= 0 ? ageYears.ToString() : "?";// + ", "; // + 
                        if( bPatient && _mPatientInfo._mPatientGender_IX > 0 )
                        {
                            ageGender += " " + CPatientInfo.sGetGenderString((DGender)_mPatientInfo._mPatientGender_IX);
                        }
                        labelAgeGender.Text = ageGender;  //"41, male";

                        labelDateOfBirth.Text =  ageYears < 0 ? "" : CProgram.sDateToString(bPatient ? _mPatientInfo._mPatientDateOfBirth.mDecryptToDate() : _mRecordDb.mGetBirthDate()); //  "06/08/1974";
            */
            CPatientInfo.sFillPrintPatientDOB(_mbAnonymize, _mPatientInfo, _mRecordDb, labelDateOfBirth, labelAgeGender);
            labelDateOfBirthResPage2.Text = labelDateOfBirth.Text;
            labelDateOfBirthResPage3.Text = labelDateOfBirth.Text;

            //            labelDOBheader.Text = "DATE OF BIRTH:";
            /*
             int pos = fullName.IndexOf(',');
            string firstName = "", lastName = fullName; ;
            if( pos > 0)
            {
                lastName = fullName.Substring(0, pos);
                firstName = fullName.Substring(pos + 1);
                if( firstName.StartsWith(", "))
                {
                    firstName = firstName.Substring(2);
                }
            }
            labelPatientFirstName.Text = firstName;  //"Rutger Alexander";
            labelPatientLastName.Text = lastName;  //"Brest van Kempen";
            */
            CPatientInfo.sFillPrintPatientName(_mbAnonymize, _mPatientInfo, _mRecordDb, labelPatientLastName, labelPatientFirstName);
            string fullName = _mbAnonymize ? "" : (bPatient ? _mPatientInfo.mGetFullName() : _mRecordDb.mPatientTotalName.mDecrypt());
            labelPatLastName.Text = fullName;
            labelPatLastNam3.Text = fullName;


            //labelPatientNameHeader.Text = "PATIENT NAME:";
            dt = _mRecordDb.mGetDeviceTime(_mRecordDb.mEventUTC);
            labelSingleEventReportTime.Text = CProgram.sTimeToString(dt);// "10:15 AM";
            labelSingleEventReportDate.Text = CProgram.sDateToString(dt);// "11/12/2016";

            //            labelReportNr.Text = mRecordDb.mSeqNrInStudy.ToString() + ( mAnalysisNr > 1 ? "(" + mAnalysisNr + ")" : "" ); // "175";
            //labelCardiacEventReportNr.Text = "Cardiac Event Report #";
            //label1.Text = "800-217-0520";
            //labelCenter1.Text = "www.cardiolabs.com";

        }
        /*       bool mbGetBaseLine()
               {
                   bool bOk = false;

                   if (_mBaseLineRecordDb != null && _mBaseLineRecordFile != null && _mBaseLineRecAnalysis != null && _mBaseLineRecAnalysisFile != null ) //&& _mBaseLineStrip != null
                        //&& String.Compare(_mBaseLineRecordDb.mPatientID.mDecrypt(), mRecordDb.mPatientID.mDecrypt(), true) == 0)
                   {
                       bOk = true; // already have it
                   }
                   else
                   {
                       // lets try and find it
                       CRecordMit baseLineRecordDb = null;
                       CRecordMit baseLineRecordFile = null;
                       CRecAnalysis baseLineRecAnalysis = null;
                       CRecAnalysisFile baseLineRecAnalysisFile = null;
                       CMeasureStrip baseLineStrip = null;



                       if (baseLineRecordDb != null && baseLineRecordFile != null && baseLineRecAnalysis != null && baseLineRecAnalysisFile != null && baseLineStrip != null) ;
       //                     && String.Compare(_mBaseLineRecordDb.mPatientID.mDecrypt(), mRecordDb.mPatientID.mDecrypt(), true) == 0)
                       {

                           _mBaseLineRecordDb = baseLineRecordDb;
                           _mBaseLineRecordFile = baseLineRecordFile;
                           _mBaseLineRecAnalysis = baseLineRecAnalysis;
                           _mBaseLineRecAnalysisFile = baseLineRecAnalysisFile;
                           _mBaseLineStrip = baseLineStrip;
                           bOk = true; // found it
                       }
                   }

                   return bOk;
               }
       */
        public string mGetEventNrString(UInt32 AEventNr, UInt16 AAnalysisNr, UInt32 ARecordIndex)
        {
            string s = AEventNr.ToString();

            if (AAnalysisNr > 1) s += " - " + AAnalysisNr.ToString();

            // s += " R#" + ARecordIndex.ToString(); // "175";
            return s;
        }


        void mInitBaseLine()
        {
            bool bActive = false;

            if (_mBaseLineRecordDb != null && _mBaseLineRecordFile != null && _mBaseLineRecAnalysis != null && _mBaseLineRecAnalysisFile != null) 
            {
                UInt16 blIndex = 0;
                CMeasureStrip ms = _mBaseLineRecAnalysisFile.mFindActiveStrip(blIndex);

                bActive = ms != null;
                
                mInitEcgHeader(_mBaseLineRecordDb, ms, blIndex,
                        null, null, labelEventNrBL, labelBaselineDate, labelBaselineTime);

                    mInitEcgGraphs(_mBaseLineRecordDb, _mBaseLineRecordFile, ms,
                        pictureBoxBaseline, labelSampleBlStart, labelSampleBlMid, labelSampleBlEnd, labelSampleBlAmpl, labelSampleBlSweep,
                        null, null, null, null, null, null
                        );

                    mInitEcgMeasurements(_mBaseLineRecAnalysisFile, ms, 
                          headerMinHrBL, valueMinHrBL, unitMinHrBL,
                          headerMeanHrBL, valueMeanHrBL, unitMeanHrBL,
                          headerMaxHrBL, valueMaxHrBL, unitMaxHrBL,
                          headerPrBL, valuePrBL, unitPrBL,
                          headerQrsBL, valueQrsBL, unitQrsBL,
                          headerQtBL, valueQtBL, unitQtBL
                          );
                    mInitEcgFindings(ms,
                         labelRhythmsBL, labelFindingsBL, labelSymptomsBL, null
                         );

#if baseLineOld
                /*
                                DateTime dtEvent = _mBaseLineRecordDb.mGetDeviceTime(_mBaseLineRecordDb.mEventUTC);

                                bool bAutoSizeA = false;
                                float cursorStart = -1.0F;
                                float cursorDuration = 0.0F;
                                float unitT = 0.2F;
                                float unitA = 0.5F;
                                int width = pictureBoxBaselineReferenceECGStrip.Width;
                                int height = pictureBoxBaselineReferenceECGStrip.Height;

                                CMeasureStrip mStrip = _mBaseLineRecAnalysisFile.mGetStrip(0);


                                UInt16 channel = mStrip == null ? (UInt16)0 : mStrip._mChannel;
                                float startT = mStrip == null ? 0 : mStrip._mStartTimeSec;
                                float durationT = mStrip == null ? _mBaseLineRecordFile.mGetSamplesTotalTime() : mStrip._mDurationSec;
                                float startA = mStrip == null ? -10 : mStrip._mStartA;
                                float endA = mStrip == null ? 10 : mStrip._mEndA;

                                labelLeadID.Text = mStrip == null || bNotProgrammer ? "" : _mBaseLineRecordDb.mGetChannelName(channel); ;

                                DateTime dtStart = _mBaseLineRecordDb.mStartRecUTC == DateTime.MinValue ? DateTime.MinValue : _mBaseLineRecordDb.mGetDeviceTime(_mBaseLineRecordDb.mStartRecUTC.AddSeconds(startT));
                                string stripLen = "     " + ((int)(durationT + 0.499)).ToString() + "  sec";


                                Image img = mMakeBaseChartImage(width, height, channel, bAutoSizeA,
                                    startT, durationT, startA, endA, cursorStart, cursorDuration, unitT, unitA);

                                pictureBoxBaselineReferenceECGStrip.Image = img;

                                labelBaseSweetSpeed.Text = _mBaseLineRecordFile.mGetShowSpeedString(unitT);
                                labelBaseAmplitudeSet.Text = _mBaseLineRecordFile.mGetShowAmplitudeString(unitA);
                                labelEventMiddle.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT * 0.5F)); // "10:15:15";
                                labelEventEnd.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT)); // "10:15:20";
                                labelEventStart.Text = CProgram.sTimeToString(dtStart) + stripLen;//  "10:15:10";

                                //                labelTechnicianMeasurement.Text = "";// "SHJ";
                                //labelTechMeasure.Text = "Technician:";
                                //labelQTmeasureSI.Text = "(s)";

                                labelBaselineTime.Text = CProgram.sTimeToString(dtEvent); //"10:15:10";

                                labelBaselineDate.Text = CProgram.sDateToString(dtEvent); //"11/23/2016";


                                mInitStripHr(_mBaseLineRecAnalysisFile, 0, labelMinRateHeader, valueMinHrBL, labelMinRateSI, labelMeanRateHeader, valueMeanHrBL, labelMeanRateSI,
                                                            labelMaxRateHeader, valueMaxHrBL, labelMaxRateSI);
                                mInitStripTimeTag(_mBaseLineRecAnalysisFile, 0, DMeasure2Tag.PRtime, labelPRHeader, valuePrBL, labelPRSI);
                                mInitStripTimeTag(_mBaseLineRecAnalysisFile, 0, DMeasure2Tag.QRStime, labelQRSHeader, valueQrsBL, labelQRSSI);
                                mInitStripTimeTag(_mBaseLineRecAnalysisFile, 0, DMeasure2Tag.QTtime, labelQTHeader, valueQtBL, labelQTSI);

                                string refBL = ""; // _mBaseLineRecordDb.mSeqNrInStudy.ToString();

                                refBL += _mBaseLineRecAnalysis._mAnalysisRemark;
                                labelFindingsRythmBL_old.Text = refBL;
                                /*
                                CMeasureStat ms;
                                string min, mean, max;
                                                min = mean = max = "";
                                                ms = _mBaseLineRecAnalysisFile.mFindStat(true, (UInt16)DMeasure2Tag.RRtime);
                                                if (ms != null && ms._mCount > 0)
                                                {
                                                    mean = mBpmString(ms._mMean);
                                                    if (ms._mCount > 1)
                                                    {
                                                        min = mBpmString(ms._mMax);
                                                        max = mBpmString(ms._mMin);
                                                    }
                                                }
                                                labelMaxRate.Text = max; // "120";
                                                labelMeanRate.Text = mean; // "110";
                                                labelMinRate.Text = min; // "100";

                                                if( mean == "" )
                                                {
                                                    labelMeanRateSI.Text = "";
                                                    labelMeanRate.Text = ""; // "110";
                                                }
                                                if ( min == "" )
                                                {
                                                    labelMaxRateSI.Text = "";
                                                    labelMaxRate.Text = ""; // "120";
                                                    labelMaxRateHeader.Text = "";
                                                    labelMinRateSI.Text = "";
                                                    labelMinRate.Text = ""; // "100";
                                                    labelMinRateHeader.Text = "";
                                                }

                                                min = mean = max = "";
                                                ms = _mBaseLineRecAnalysisFile.mFindStat(true, (UInt16)DMeasure2Tag.QTtime);
                                                if (ms != null && ms._mCount > 0)
                                                {
                                                    mean = mTimeString(ms._mMean);
                                                    if (ms._mCount > 1)
                                                    {
                                                        min = mTimeString(ms._mMin);
                                                        max = mTimeString(ms._mMax);
                                                    }
                                                }

                                                labelQT.Text = mean;
                                                if (mean == "")
                                                {
                                                    labelQTSI.Text = "";
                                                    labelQTHeader.Text = ""; //  "0,465";
                                                }

                                                min = mean = max = "";
                                                ms = _mBaseLineRecAnalysisFile.mFindStat(true, (UInt16)DMeasure2Tag.QRStime);
                                                if (ms != null && ms._mCount > 0)
                                                {
                                                    mean = mTimeString(ms._mMean);
                                                    if (ms._mCount > 1)
                                                    {
                                                        min = mTimeString(ms._mMin);
                                                        max = mTimeString(ms._mMax);
                                                    }
                                                }
                                                labelQRS.Text = mean;
                                                if (mean == "")
                                                {
                                                    labelQRSSI.Text = "";
                                                    labelQRSHeader.Text = "";
                                                }
                                                min = mean = max = "";
                                                ms = _mBaseLineRecAnalysisFile.mFindStat(true, (UInt16)DMeasure2Tag.PRtime);
                                                if (ms != null && ms._mCount > 0)
                                                {
                                                    mean = mTimeString(ms._mMean);
                                                    if (ms._mCount > 1)
                                                    {
                                                        min = mTimeString(ms._mMin);
                                                        max = mTimeString(ms._mMax);
                                                    }
                                                }
                                                labelPR.Text = mean;
                                                if (mean == "")
                                                {
                                                    labelPRSI.Text = "";
                                                    labelPRHeader.Text = "";
                                                }

                */
#endif

            }
            if( false == bActive)
            {
                panelECGBaselineSTrip.Visible = false;
                panelBaseLineMFS.Visible = false;
                panelBaseLineHeader.Visible = false;
                panelBlSweepAmpIndicator.Visible = false;
            }
        }
        void mInitPrevious()
        {
            // get findings list and log last 3
        }
        public string mFirstWord(string ALine)
        {
            string s = "";
            int n = ALine == null ? 0 : ALine.Length;
            int l = 0;

            for (int i = 0; i < n; ++i)
            {
                char c = ALine[i];
                if (char.IsLetterOrDigit(c))
                {
                    s += c;
                    ++l;
                }
                else if (l > 0)
                {
                    break;
                }
            }
            return s;
        }
        public string mOneLine(string ALine)
        {
            string s = "";
            int n = ALine == null ? 0 : ALine.Length;
            int l = 0;

            for (int i = 0; i < n; ++i)
            {
                char c = ALine[i];
                if (char.IsLetterOrDigit(c))
                {
                    s += c;
                    ++l;
                }
                else if (c == '\r' || c == '\n')
                {
                    break;
                }
                else if (c >= ' ')
                {
                    s += c;
                    ++l;
                }
                else if (l > 0)
                {
                    s += " ";
                }
            }
            return s;
        }


        string mBpmString(float ATimeSec)
        {
            float f = Math.Abs(ATimeSec);
            float bpm = f < 0.001 ? 0.0F : 1 / f;
            int i = (int)(bpm * 60 + 0.5F);
            return i.ToString(); // bpm.ToString("0.0");
        }
        string mTimeString(float ATimeSec)
        {
            return ATimeSec.ToString("0.000");
        }

        void mInitCurrent()
        {
            CMeasureStat ms;
            string min, mean, max;
            bool bMeasure = false;
            bool bMinMax = false;

            labelTechnicianMeasurement.Text = FormEventBoard._sbPrintHideTechName ? "" : _mCreadedByInitials;// "SHJ";
            if (FormEventBoard._sbPrintHideTechName) labelTechText.Text = "";


            min = mean = max = "";
            ms = _mRecAnalysisFile.mFindStat(true, (UInt16)DMeasure2Tag.RRtime);
            if (ms != null && ms._mbActive && ms._mCount > 0)
            {
                bMeasure = true;
                mean = mBpmString(ms._mMean);
                if (ms._mCount > 1)
                {
                    bMinMax = true;
                    min = mBpmString(ms._mMax);
                    max = mBpmString(ms._mMin);
                }
            }
            labelRateAVG.Text = mean;  //"76";
            labelRateMax.Text = max;  //"76";
            labelRateMin.Text = min;  //"76";
            if (mean == "")
            {
                labelRateMeasure.Text = "";
                labelRatemeasureSI.Text = "";
            }

            min = mean = max = "";
            ms = _mRecAnalysisFile.mFindStat(true, (UInt16)DMeasure2Tag.QTtime);
            if (ms != null && ms._mbActive && ms._mCount > 0)
            {
                bMeasure = true;
                mean = mTimeString(ms._mMean);
                if (ms._mCount > 1)
                {
                    bMinMax = true;
                    min = mTimeString(ms._mMin);
                    max = mTimeString(ms._mMax);
                }
            }
            labelQTAvg.Text = mean; // "76";
            labelQTMax.Text = max;  //"76";
            labelQTmin.Text = min;  //"76";
            if (mean == "")
            {
                labelQTmeasureSI.Text = "";
                labelQTMeasure.Text = "";
            }

            min = mean = max = "";
            ms = _mRecAnalysisFile.mFindStat(true, (UInt16)DMeasure2Tag.QRStime);
            if (ms != null && ms._mbActive && ms._mCount > 0)
            {
                bMeasure = true;
                mean = mTimeString(ms._mMean);
                if (ms._mCount > 1)
                {
                    bMinMax = true;
                    min = mTimeString(ms._mMin);
                    max = mTimeString(ms._mMax);
                }
            }
            labelQRSAvg.Text = mean;  //"76";
            labelQRSMax.Text = max;  //"76";
            labelQRSmin.Text = min;  //"76";
            if (mean == "")
            {
                labelQRSmeasureSI.Text = "";
                labelQRSMeasure.Text = "";
            }

            min = mean = max = "";
            ms = _mRecAnalysisFile.mFindStat(true, (UInt16)DMeasure2Tag.PRtime);
            if (ms != null && ms._mbActive && ms._mCount > 0)
            {
                bMeasure = true;
                mean = mTimeString(ms._mMean);
                if (ms._mCount > 1)
                {
                    bMinMax = true;
                    min = mTimeString(ms._mMin);
                    max = mTimeString(ms._mMax);
                }
            }
            labelPRAvg.Text = mean;  //"76";
            labelPRMax.Text = max;  //"76";
            labelPRmin.Text = min;  //"76";
            if (mean == "")
            {
                labelPRmeasureSI.Text = "";
                labelPRMeasure.Text = "";
            }

            if (bMeasure == false)
            {
                labelAVGMeasurement.Text = "";

            }
            if (bMinMax == false)
            {
                labelMaxMeasurement.Text = "";
                labelMinMeasurement.Text = "";
            }
 
            labelClass1.Text = _mRecAnalysis._mAnalysisClassSet.mbIsEmpty() || CDvtmsData._sEnumListFindingsClass == null ? ""
                : CDvtmsData._sEnumListFindingsClass.mGetLabel(_mRecAnalysis._mAnalysisClassSet.mGetValue(0));


            string s = "";

            if (CDvtmsData._sEnumListStudyRhythms != null)
            {
                s = CDvtmsData._sEnumListStudyRhythms.mFillString(_mRecAnalysis._mAnalysisRhythmSet, false, " ", true, ", ");
            }

            labelFindings1.Text = _mRecAnalysis._mAnalysisRemark; // findings maken ? "Atrial Fibrillation, with Occasional PVC\'s";
            //labelFindingsHeader.Text = "Findings:";
            labelActivationType.Text = mFirstWord(_mRecordDb.mEventTypeString);//            "Patient / Recorder";
            //labelActivationTypeHeader.Text = "Type of Activation:";
            labelActivity1.Text = _mRecAnalysis._mActivitiesRemark; // "Walking up the Stairs";
            //labelActivityHeader.Text = "Activity:";
                                                                //labelSymptomsHeader.Text = "Symptoms:";
            s = "";
            if (_mStudyInfo != null)
            {
                if (CDvtmsData._sEnumListStudyProcedures != null)
                {
                    s = CDvtmsData._sEnumListStudyProcedures.mFillString(_mStudyInfo._mStudyProcCodeSet, true, " ", true, "\r\n");
                }
            }

            labelICDcode.Text = s; // "ICD10";
                                   //            labelDiagnosisHeader.Text = "Diagnosis:";/
                                   //            labelMeasurement.Text = "Measurements";
                                   //            labelCurrentEvent.Text = "Current Event";
                                   //            labelPreviousTransmissions.Text = "Previous Three Transmissions";

        }

        Image mMakeChartImage(CRecordMit ARecordFile, int AWidth, int AHeight, UInt16 AChannel, bool AbAutoSizeA,
            float AStartT, float ADurationT, float AStartA, float AEndA,
            float ACursorStart, float ACursorDuration,
            float AUnitT, float AUnitA, float AMaxRange, bool AbDrawBorder = true)
        {
            CStripChart chart = new CStripChart();
            float startA = AStartA;
            float endA = AEndA;

            ARecordFile.mInitChart(AChannel, chart, AStartT, ADurationT, AUnitA, AUnitT, AMaxRange);

            if (false == AbAutoSizeA)
            {
                startA = chart.mGetStartA();
                endA = chart.mGetEndA();
            }

            Image img = ARecordFile.mCreateChartImage2(AChannel, chart, AWidth, AHeight, ACursorStart, ACursorDuration,
                (_mbBlackWhite ? DChartDrawTo.Print_BW : DChartDrawTo.Print_color), AbDrawBorder);

            return img;
        }
/*
        Image mMakeBaseChartImage(CRecordMit ARecordFile, int AWidth, int AHeight, UInt16 AChannel, bool AbAutoSizeA,
            float AStartT, float ADurationT, float AStartA, float AEndA,
            float ACursorStart, float ACursorDuration,
            float AUnitT, float AUnitA)
        {
            CStripChart chart = new CStripChart();
            float startA = AStartA;
            float endA = AEndA;

            _mBaseLineRecordFile.mInitChart(AChannel, chart, AStartT, ADurationT, AUnitA, AUnitT);

            if (false == AbAutoSizeA)
            {
                startA = chart.mGetStartA();
                endA = chart.mGetEndA();
            }

            Image img = _mBaseLineRecordFile.mCreateChartImage2(AChannel, chart, AWidth, AHeight, ACursorStart, ACursorDuration,
                _mbBlackWhite ? DChartDrawTo.Print_BW : DChartDrawTo.Print_color);

            return img;
        }
        */
        void mInitStripHr(CRecAnalysisFile AAnalysisFile, CMeasureStrip AMs, 
                                            Label ALabelMinText, Label ALabelMinValue, Label ALabelMinUnit,
                                            Label ALabelMeanText, Label ALabelMeanValue, Label ALabelMeanUnit,
                                            Label ALabelMaxText, Label ALabelMaxValue, Label ALabelMaxUnit)
        {
            bool bMean = false;
            bool bMinMax = false;
            string min = "", mean = "", max = "";

            try
            {
                CMeasureStat pStat = AAnalysisFile == null  || AMs == null ? null : AAnalysisFile.mCalcStripStat(AMs, DMeasure2Tag.RRtime);

                if (pStat != null && pStat._mCount > 0)
                {
                    mean = mBpmString(pStat._mMean);
                    bMean = true;
                    if (pStat._mCount > 1)
                    {
                        bMinMax = true;
                        min = mBpmString(pStat._mMax);
                        max = mBpmString(pStat._mMin);
                    }
                }
                
            }
            catch (Exception ex)
            {
                CProgram.sLogException("mInitStripHr", ex);
            }
            ALabelMeanValue.Text = mean;
            ALabelMeanValue.Visible = bMean;
            ALabelMeanText.Visible = bMean;
            ALabelMeanUnit.Visible = bMean;
            ALabelMinValue.Text = min;
            ALabelMaxValue.Text = max;
            ALabelMeanText.Text = "HR:";    // no min max
            ALabelMinValue.Visible = bMinMax;
            ALabelMaxValue.Visible = bMinMax;
            ALabelMinText.Visible = bMinMax;
            ALabelMinUnit.Visible = bMinMax;
            ALabelMaxText.Visible = bMinMax;
            ALabelMaxUnit.Visible = bMinMax;
        }

        void mInitStripTimeTag(CRecAnalysisFile AAnalysisFile, CMeasureStrip AMs, DMeasure2Tag ATagID, Label ALabelMeanText, Label ALabelMeanValue, Label ALabelMeanUnit)
        {
            bool bMean = false;
            string mean = "";

            try
            {
                CMeasureStat pStat = AAnalysisFile == null || AMs == null ? null : AAnalysisFile.mCalcStripStat(AMs, ATagID);

                if (pStat != null && pStat._mCount > 0)
                {
                    mean = mTimeString(pStat._mMean);
                    bMean = true;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("mInitStripTimeTag", ex);
            }
            ALabelMeanValue.Text = mean;
            ALabelMeanValue.Visible = bMean;
            ALabelMeanText.Visible = bMean;
            ALabelMeanUnit.Visible = bMean;
        }

        void mInitFooter()
        {
            /*
            labelReportSignDate.Text = "_________"; //CProgram.sDateToString( DateTime.Now );// "08/23/2016";
            //'labelDateReportSign.Text = "Date:";
            //labelPhysSignLine.Text = "_______________";
            //labelPhysSign.Text = "Physician signature:";
            labelPhysPrintName.Text = "____________"; // "J. Smithsonian";
                                                      //labelPhysNameReportPrint.Text = "Physician\'s name:";
                                                      //label7.Text = ">R";
                                                      //label3.Text = "L<";
            */
            string date = CProgram.sPrintDateTimeVersion(DateTime.Now);

            labelPrintDate1Bottom.Text = date;
            labelPrintDate2Bottom.Text = date;
            labelPrintDate3Bottom.Text = date;
            

            CDvtmsData.sFillPrintPhysicianLabels(CProgram.sGetProgUserIX(), CProgram.sGetProgUserInitials(), "Analysis", _mbQCd,
                labelPhysNameReportPrint, labelPhysPrintName,
                labelPhysSign, labelPhysSignLine,
                labelDateReportSign, labelReportSignDate);

            labelPrintDate2Bottom.Text = labelPrintDate1Bottom.Text;
            labelPrintDate3Bottom.Text = labelPrintDate1Bottom.Text;
        }

        private string mMakeStartTime(DateTime AStartDT, float ADurationSec)
        {
            string s = "";
            int durationSec = (int)(ADurationSec + 0.499);

            if (AStartDT != DateTime.MinValue)
            {
                s = CProgram.sTimeToString(AStartDT) + " " + CProgram.sDateToString(AStartDT) + "   ";
            }
            if (durationSec < 100)
            {
                s += "<" + durationSec.ToString() + " sec>";
            }
            else
            {
                s += "<" + CProgram.sPrintTimeSpan_dhmSec(durationSec, "") + ">";
            }
            return s;
        }

        private void mInitEcgHeader(CRecordMit ARecord, CMeasureStrip AMeasureStrip, UInt16 ASampleNr,
             Label ALabelClass, Label ALabelEvent, Label ALabelSample, Label ALabelDate, Label ALabelTime)
        {
            int stripIndex = AMeasureStrip == null ? 0 : _mRecAnalysisFile.mGetStripIndex(AMeasureStrip) + 1;
            DateTime dtEvent = ARecord.mGetDeviceTime(ARecord.mEventUTC);

            string classification = "";
            string eventNr = ARecord.mSeqNrInStudy.ToString(); // + "." + stripIndex.ToString();
            string date = CProgram.sTimeToString(dtEvent);
            string time = CProgram.sDateToString(dtEvent);

            

            if(AMeasureStrip != null && AMeasureStrip._mRecAnalysis != null)
            {
                classification = AMeasureStrip._mRecAnalysis._mAnalysisClassSet.mbIsEmpty() || CDvtmsData._sEnumListFindingsClass == null ? ""
                       : CDvtmsData._sEnumListFindingsClass.mGetLabel(AMeasureStrip._mRecAnalysis._mAnalysisClassSet.mGetValue(0));
            }

            if (ALabelClass != null) ALabelClass.Text = classification;
            if (ALabelEvent != null) ALabelEvent.Text = eventNr;
            if (ALabelSample != null) ALabelSample.Text = (ASampleNr+1).ToString();
            if (ALabelDate != null) ALabelDate.Text = date;
            if (ALabelTime != null) ALabelTime.Text = time;
        }

        private void mInitEcgGraphs(CRecordMit ARecordDB, CRecordMit ARecordFile, CMeasureStrip AMeasureStrip,
            PictureBox APictBox1, Label ALabelStart1, Label ALabelMid1, Label ALabelEnd1, Label ALabelUnitA1, Label ALabelUnitT1,
            PictureBox APictBox2, Label ALabelStart2, Label ALabelMid2, Label ALabelEnd2, Label ALabelUnitA2, Label ALabelUnitT2

            )
        {
            try
            {
                if (ARecordDB == null || ARecordFile == null || AMeasureStrip == null)
                {
                    APictBox1.Image = null;

                    ALabelUnitT1.Text = "";
                    ALabelUnitA1.Text = "";
                    ALabelEnd1.Text = "";
                    ALabelMid1.Text = "";
                    ALabelStart1.Text = "";
                    if (APictBox2 != null)
                    {
                        APictBox2.Image = null;

                        ALabelUnitT2.Text = "";
                        ALabelUnitA2.Text = "";
                        ALabelEnd2.Text = "";
                        ALabelMid2.Text = "";
                        ALabelStart2.Text = "";
                    }

                }
                else
                {
                    bool bAutoSizeA = false;
                    float cursorStart = -1.0F;
                    float cursorDuration = 0.0F;
                    float unitT = 0.2F;
                    float unitA = 0.5F;
                    int width = APictBox1.Width;
                    int height = APictBox1.Height;
                    UInt16 channel = AMeasureStrip._mChannel;
                    float startT = AMeasureStrip._mStartTimeSec;
                    float durationT = AMeasureStrip._mDurationSec;
                    float startA = AMeasureStrip._mStartA;
                    float endA = AMeasureStrip._mEndA;

                    // plot sample top strip

//                    DateTime dtStart = ARecordDB.mStartRecUTC == DateTime.MinValue ? DateTime.MinValue : ARecordDB.mGetDeviceTime(ARecordDB.mStartRecUTC.AddSeconds(startT));
                    DateTime dtStart = ARecordFile.mBaseUTC == DateTime.MinValue ? DateTime.MinValue : ARecordFile.mGetDeviceTime(ARecordFile.mBaseUTC.AddSeconds(startT));

                    Image img = mMakeChartImage(ARecordFile, width, height, channel, bAutoSizeA,
                       startT, durationT, startA, endA, cursorStart, cursorDuration, unitT, unitA, CRecordMit.sGetMaxAmplitudeRange());

                    APictBox1.Image = img;

                    if( dtStart == DateTime.MinValue)
                    {
                        CProgram.sLogLine("analysisPrint R#" + _mRecordDb.mIndex_KEY.ToString() + " startUTC = NULL");
                    }

                    ALabelUnitT1.Text = ARecordDB.mGetShowSpeedString(unitT); ;// "25 mm/sec";
                    ALabelUnitA1.Text = ARecordDB.mGetShowAmplitudeString(unitA); //"1 mm/mV";
                    ALabelEnd1.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT)); //"10:15:20";
                    ALabelMid1.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT * 0.5F)); //"10:15:15";
                    ALabelStart1.Text = mMakeStartTime(dtStart, durationT);
                    //labelSample3Lead.Text = _mRecordDb.mGetChannelName(channel); //"LEAD I";

                    if (APictBox2 != null)
                    {
                        //  plot sample bottom (full) strip
                        bAutoSizeA = true;

                        if (_mbPlot2Ch && _mRecordFile.mNrSignals > 1)
                        {
                            // Plot alternative channel
                            channel = (UInt16)(channel == 0 ? 1 : 0);
                        }
                        else
                        {
                            // Plot full strip with cursor
                            cursorStart = -1.0F;
                            cursorDuration = 0.0F;
                            unitT = 1.0F;
                            unitA = 0.5F;
                            width = APictBox2.Width;
                            height = APictBox2.Height;

                            startT = 0.0F;
                            durationT = ARecordFile.mGetSamplesTotalTime();
                            startA = -10.0F;
                            endA = 10.0F;

                            cursorStart = AMeasureStrip._mStartTimeSec;
                            cursorDuration = AMeasureStrip._mDurationSec;
//                            dtStart = ARecordDB.mGetDeviceTime(ARecordDB.mStartRecUTC.AddSeconds(startT));
                            dtStart = ARecordFile.mGetDeviceTime(ARecordFile.mBaseUTC.AddSeconds(startT));
                        }

                        img = mMakeChartImage(ARecordFile, width, height, channel, bAutoSizeA,
                            startT, durationT, startA, endA, cursorStart, cursorDuration, unitT, unitA, CRecordMit.sGetMaxAmplitudeRange());

                        APictBox2.Image = img;

                        ALabelUnitT2.Text = ARecordFile.mGetShowSpeedString(unitT); ;// "25 mm/sec";
                        ALabelUnitA2.Text = ARecordFile.mGetShowAmplitudeString(unitA); //"1 mm/mV";
                        ALabelEnd2.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT)); //"10:15:20";
                        ALabelMid2.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT * 0.5F)); //"10:15:15";
                        ALabelStart2.Text = mMakeStartTime(dtStart, durationT);
                        //                labelSample3LeadFull.Text = ARecordDB.mGetChannelName(channel); //"LEAD I";
                    }
                }

            }
            catch (Exception ex)
            {
                CProgram.sLogException("mInitEcgGraphs", ex);
            }

        }

        private void mInitEcgMeasurements(CRecAnalysisFile AAnalysisFile, CMeasureStrip AMs,
            Label AHeaderMinHr, Label AValueMinHr, Label AUnitMinHr,
            Label AHeaderMeanHr, Label AValueMeanHr, Label AUnitMeanHr,
            Label AHeaderMaxHr, Label AValueMaxHr, Label AUnitMaxHr,
            Label AHeaderPr, Label AValuePr, Label AUnitPr,
            Label AHeaderQrs, Label AValueQrs, Label AUnitQrs,
            Label AHeaderQt, Label AValueQt, Label AUnitQt
            )
        {
            mInitStripHr(AAnalysisFile, AMs,
                 AHeaderMinHr, AValueMinHr, AUnitMinHr,
                AHeaderMeanHr, AValueMeanHr, AUnitMeanHr,
                AHeaderMaxHr, AValueMaxHr, AUnitMaxHr);

            mInitStripTimeTag(AAnalysisFile, AMs, DMeasure2Tag.PRtime, AHeaderPr, AValuePr, AUnitPr);
            mInitStripTimeTag(AAnalysisFile, AMs, DMeasure2Tag.QRStime, AHeaderQrs, AValueQrs, AUnitQrs);
            mInitStripTimeTag(AAnalysisFile, AMs, DMeasure2Tag.QTtime, AHeaderQt, AValueQt, AUnitQt);
        }

        private void mInitEcgFindings(CMeasureStrip AMeasureStrip,
             Label ARhythms, Label AFindings, Label ASympthoms, Label AActivities
             )
        {
            string rhythms = "";
            string findings = "";
            string sympthoms = "";
            string activities = "";

            try
            {
                if (AMeasureStrip != null && AMeasureStrip._mRecAnalysis != null)
                {
                    UInt16 count = AMeasureStrip._mRecAnalysis._mAnalysisRhythmSet.mCount();

                    if (CDvtmsData._sEnumListStudyRhythms != null)
                    {
                        if (count > 1)
                        {
                            rhythms = CDvtmsData._sEnumListStudyRhythms.mFillString(AMeasureStrip._mRecAnalysis._mAnalysisRhythmSet, true, " ", false, ", ");
                        }
                        else
                        {
                            rhythms = CDvtmsData._sEnumListStudyRhythms.mFillString(AMeasureStrip._mRecAnalysis._mAnalysisRhythmSet, false, " ", true, ", ");
                        }
                    }
                    else
                    {
                        rhythms = _mRecAnalysis._mAnalysisRhythmSet.mGetSet();
                    }

                    findings = AMeasureStrip._mRecAnalysis._mAnalysisRemark;
                    sympthoms = AMeasureStrip._mRecAnalysis._mSymptomsRemark;
                    activities = AMeasureStrip._mRecAnalysis._mActivitiesRemark;
                }
            }
            catch ( Exception ex)
            {
                CProgram.sLogException("InitEcgFindings", ex);
            }
            if (ARhythms != null) ARhythms.Text = rhythms;
            if (AFindings != null) AFindings.Text = findings;
            if (ASympthoms != null) ASympthoms.Text = sympthoms;
            if (AActivities != null) AActivities.Text = activities;
        }

        void mInitSample1()
        {
            CMeasureStrip ms = _mRecAnalysisFile.mFindActiveStrip(_mCurrentActiveIndex);

            if (ms == null)
            {
                panelEventSample1.Visible = false;
            }
            else
            {
                UInt16 stripIndex = (UInt16)(_mRecAnalysisFile.mGetStripIndex(ms) + 1);

                panelEventSample1.Visible = true;

                mInitEcgHeader(_mRecordDb, ms, _mCurrentActiveIndex,
                    labelClass1, labelSample1EventNr, labelSample1Nr, labelSample1Date, labelSample1Time);

                mInitEcgGraphs(_mRecordDb, _mRecordFile, ms,
                    pictureBoxSample1, labelSample1Start, labelSample1Mid, labelSample1End, labelSample1Ampl, labelSample1Sweep,
                    pictureBoxSample1Full, labelSample1StartFull, labelSample1MidFull, labelSample1EndFull, labelSample1AmplFull, labelSample1SweepFull
                    );

                mInitEcgMeasurements(_mRecAnalysisFile, ms,
                      headerMinHr1, valueMinHr1, unitMinHr1,
                      headerMeanHr1, valueMeanHr1, unitMeanHr1,
                      headerMaxHr1, valueMaxHr1, unitMaxHr1,
                      headerPr1, valuePr1, unitPr1,
                      headerQrs1, valueQrs1, unitQrs1,
                      headerQt1, valueQt1, unitQt1
                      );
                mInitEcgFindings(ms,
                     labelRhythms1, labelFindings1, labelSymptoms1, labelActivity1
                     );
            }
            ++_mCurrentActiveIndex;

        }

        void mInitSample2()
        {
            CMeasureStrip ms = _mRecAnalysisFile.mFindActiveStrip(_mCurrentActiveIndex);

            if (ms == null)
            {
                panelEventSample2.Visible = false;
            }
            else
            {
                UInt16 stripIndex = (UInt16)(_mRecAnalysisFile.mGetStripIndex(ms) + 1);

                panelEventSample2.Visible = true;

                mInitEcgHeader(_mRecordDb, ms, _mCurrentActiveIndex,
                    labelClass2, labelSample2EventNr, labelSample2Nr, labelSample2Date, labelSample2Time);

                mInitEcgGraphs(_mRecordDb, _mRecordFile, ms,
                    pictureBoxSample2, labelSample2Start, labelSample2Mid, labelSample2End, labelSample2Ampl, labelSample2Sweep,
                    null, null, null, null, null, null
                    );

                mInitEcgMeasurements(_mRecAnalysisFile, ms,
                      headerMinHr2, valueMinHr2, unitMinHr2,
                      headerMeanHr2, valueMeanHr2, unitMeanHr2,
                      headerMaxHr2, valueMaxHr2, unitMaxHr2,
                      headerPr2, valuePr2, unitPr2,
                      headerQrs2, valueQrs2, unitQrs2,
                      headerQt2, valueQt2, unitQt2
                      );
                mInitEcgFindings(ms,
                     labelRhythms2, labelFindings2, null, null
                     );
            }
            ++_mCurrentActiveIndex;
        }

        void mInitSample3()
        {
            CMeasureStrip ms = _mRecAnalysisFile.mFindActiveStrip(_mCurrentActiveIndex);

            if (ms == null)
            {
                panelEventSample3.Visible = false;
            }
            else
            {
                UInt16 stripIndex = (UInt16)(_mRecAnalysisFile.mGetStripIndex(ms) + 1);

                panelEventSample3.Visible = true;

                mInitEcgHeader(_mRecordDb, ms, _mCurrentActiveIndex,
                    labelClass3, labelSample3EventNr, labelSample3Nr, labelSample3Date, labelSample3Time);

                mInitEcgGraphs(_mRecordDb, _mRecordFile, ms,
                    pictureBoxSample3, labelSample3Start, labelSample3Mid, labelSample3End, labelSample3Ampl, labelSample3Sweep,
                    pictureBoxSample3Full, labelSample3StartFull, labelSample3MidFull, labelSample3EndFull, labelSample2Ampl, labelSample2Sweep

                    );

                mInitEcgMeasurements(_mRecAnalysisFile, ms,
                      headerMinHr3, valueMinHr3, unitMinHr3,
                      headerMeanHr3, valueMeanHr3, unitMeanHr3,
                      headerMaxHr3, valueMaxHr3, unitMaxHr3,
                      headerPr3, valuePr3, unitPr3,
                      headerQrs3, valueQrs3, unitQrs3,
                      headerQt3, valueQt3, unitQt3
                      );
                mInitEcgFindings(ms,
                     labelRhythms3, labelFindings3, null, null
                     );
             }
            ++_mCurrentActiveIndex;
        }

        void mInitSample4()
        {
            CMeasureStrip ms = _mRecAnalysisFile.mFindActiveStrip(_mCurrentActiveIndex);

            if (ms == null)
            {
                panelEventSample4.Visible = false;
            }
            else
            {
                UInt16 stripIndex = (UInt16)(_mRecAnalysisFile.mGetStripIndex(ms) + 1);

                panelEventSample4.Visible = true;

                mInitEcgHeader(_mRecordDb, ms, _mCurrentActiveIndex,
                    labelClass4, labelSample4EventNr, labelSample4Nr, labelSample4Date, labelSample4Time);

                mInitEcgGraphs(_mRecordDb, _mRecordFile, ms,
                    pictureBoxSample4, labelSample4Start, labelSample4Mid, labelSample4End, labelSample4Ampl, labelSample4Sweep,
                    pictureBoxSample4Full, labelSample4StartFull, labelSample4MidFull, labelSample4EndFull, labelSample4Ampl, labelSample4Sweep

                    );

                mInitEcgMeasurements(_mRecAnalysisFile, ms,
                      headerMinHr4, valueMinHr4, unitMinHr4,
                      headerMeanHr4, valueMeanHr4, unitMeanHr4,
                      headerMaxHr4, valueMaxHr4, unitMaxHr4,
                      headerPr4, valuePr4, unitPr4,
                      headerQrs4, valueQrs4, unitQrs4,
                      headerQt4, valueQt4, unitQt4
                      );
                mInitEcgFindings(ms,
                     labelRhythms4, labelFindings4, null, null
                     );
            }
            ++_mCurrentActiveIndex;
        }

        void mInitSample5()
        {
            CMeasureStrip ms = _mRecAnalysisFile.mFindActiveStrip(_mCurrentActiveIndex);

            if (ms == null)
            {
                panelEventSample5.Visible = false;
            }
            else
            {
                UInt16 stripIndex = (UInt16)(_mRecAnalysisFile.mGetStripIndex(ms) + 1);

                panelEventSample5.Visible = true;

                mInitEcgHeader(_mRecordDb, ms, _mCurrentActiveIndex,
                    labelClass5, labelSample5EventNr, labelSample5Nr, labelSample5Date, labelSample5Time);

                mInitEcgGraphs(_mRecordDb, _mRecordFile, ms,
                    pictureBoxSample5, labelSample5Start, labelSample5Mid, labelSample5End, labelSample5Ampl, labelSample5Sweep,
                    pictureBoxSample5Full, labelSample5StartFull, labelSample5MidFull, labelSample5EndFull, labelSample5Ampl, labelSample5Sweep

                    );

                mInitEcgMeasurements(_mRecAnalysisFile, ms,
                      headerMinHr5, valueMinHr5, unitMinHr5,
                      headerMeanHr5, valueMeanHr5, unitMeanHr5,
                      headerMaxHr5, valueMaxHr5, unitMaxHr5,
                      headerPr5, valuePr5, unitPr5,
                      headerQrs5, valueQrs5, unitQrs5,
                      headerQt5, valueQt5, unitQt5
                      );
                mInitEcgFindings(ms,
                     labelRhythms5, labelFindings5, null, null
                     );
            }
            ++_mCurrentActiveIndex;
        }




        [System.Runtime.InteropServices.DllImportAttribute("gdi32.dll")]
        private static extern int BitBlt(
     IntPtr hdcDest,     // handle to destination DC (device context)
     int nXDest,         // x-coord of destination upper-left corner
     int nYDest,         // y-coord of destination upper-left corner
     int nWidth,         // width of destination rectangle
     int nHeight,        // height of destination rectangle
     IntPtr hdcSrc,      // handle to source DC
     int nXSrc,          // x-coordinate of source upper-left corner
     int nYSrc,          // y-coordinate of source upper-left corner
     System.Int32 dwRop  // raster operation code
     );

        [System.Runtime.InteropServices.DllImportAttribute("gdi32.dll")]
        public static extern IntPtr SelectObject(IntPtr hdc, IntPtr obj);

        [System.Runtime.InteropServices.DllImportAttribute("gdi32.dll")]
        public static extern void DeleteObject(IntPtr obj);

        void mPanelToClipBoard1(Control APanel)
        {
            CProgram.sPromptError(true, "Print", "Do not use this function");
            try
            {
                Bitmap bmp = new Bitmap(APanel.Width, APanel.Height);

                using (Graphics G = Graphics.FromImage(bmp))
                {
                    G.Clear(Color.Cyan);
                }

                Graphics g1 = APanel.CreateGraphics();
                Image MyImage = new Bitmap(APanel.ClientRectangle.Width, APanel.ClientRectangle.Height, g1);
                Graphics g2 = Graphics.FromImage(bmp);
                IntPtr dc1 = g1.GetHdc();
                IntPtr dc2 = g2.GetHdc();
                BitBlt(dc2, 0, 0, ClientRectangle.Width, ClientRectangle.Height, dc1, 0, 0, 13369376);
                g1.ReleaseHdc(dc1);
                g2.ReleaseHdc(dc2);


                //                InvertZOrderOfControls(APanel.Controls);
                //                panel1.DrawToBitmap(bmp, new System.Drawing.Rectangle(0, 0, APanel.Width, APanel.Height));
                //                InvertZOrderOfControls(APanel.Controls);

                //                Graphics g = APanel.CreateGraphics();


                //g.FillRectangle(new SolidBrush(Color.FromArgb(0, Color.Black)), p.DisplayRectangle);
                //               Pen pen = new Pen(Color.Red);
                //                g.DrawLine(pen, 0, 0, APanel.Width, APanel.Height);



                Clipboard.SetImage(bmp);

            }
            catch (Exception /*ex*/)
            {
                //CProgram.sLogException("Panel to Clipboard error", ex);

            }
        }

            private bool mbPrintImages()
        {
            return mbPrint2Pdf();
        }


#if ghfdha
        private bool mbPrintImagesToPrinter()
        {
            bool bOk = false;
            try
            {
                PrintDocument pd = new PrintDocument();

                Margins oldMargins = pd.DefaultPageSettings.Margins;
                Margins margins = new Margins(50, 50, 50, 50);
                pd.DefaultPageSettings.Margins = margins;

                pd.PrintPage += mPrintPage;

                PrintDialog printDialog1 = new PrintDialog();

                if (printDialog1 != null)
                {
                    printDialog1.Document = pd;
                    DialogResult result = printDialog1.ShowDialog(this);
                    if (result == DialogResult.OK)
                    {
                        PrintPreviewDialog printPreviewDialog = new PrintPreviewDialog();

                        if (printPreviewDialog != null)
                        {
                            printPreviewDialog.ClientSize = new System.Drawing.Size(600, 900);
                            printPreviewDialog.Location = new System.Drawing.Point(10, 10);
                            printPreviewDialog.Name = "PrintPreviewDialog1";

                            // Associate the event-handling method with the  
                            // document's PrintPage event. 
                            // Set the minimum size the dialog can be resized to. 
                            printPreviewDialog.MinimumSize = new System.Drawing.Size(375, 250);

                            // Set the UseAntiAlias property to true, which will allow the  
                            // operating system to smooth fonts. 
                            printPreviewDialog.UseAntiAlias = true;

                            printPreviewDialog.Document = pd;
                            printPreviewDialog.StartPosition = FormStartPosition.CenterParent;


                            // Call the ShowDialog method. This will trigger the document's
                            //  PrintPage event.
                            printPreviewDialog.ShowDialog( this );
                        }
                        bOk = true;
                    }
               
/*
                else
                {
                    //here to select the printer attached to user PC
//                    PrintDialog printDialog1 = new PrintDialog();

                    if (printDialog1 != null)
                    {
                        printDialog1.Document = pd;
                        DialogResult result = printDialog1.ShowDialog(this);
                        if (result == DialogResult.OK)
                        {
                            pd.Print();//this will trigger the Print Event handeler PrintPage
                            bOk = true;
                        }
                    }
*/
                }

                pd.Dispose();
            }
            catch (Exception)
            {

            }
            return bOk;
        }

        //The Print Event handeler
/*        private void mPrintPage(object o, PrintPageEventArgs e)
        {
            try
            {
              
                Bitmap bitmap = mCurrentPrintPage == 1 ? mPrint1Bitmap : mPrint2Bitmap;

                if (bitmap != null && bitmap.Width > 0 && bitmap.Height > 0)
                {
                    //Adjust the size of the image to the page to print the full image without loosing any part of it
                    Rectangle m = e.MarginBounds;

                    if ((double)bitmap.Width / (double)bitmap.Height > (double)m.Width / (double)m.Height) // image is wider
                    {
                        m.Height = (int)((double)bitmap.Height / (double)bitmap.Width * (double)m.Width);
                    }
                    else
                    {
                        m.Width = (int)((double)bitmap.Width / (double)bitmap.Height * (double)m.Height);
                    }
                    e.Graphics.DrawImage(bitmap, m);
                }
                if( ++mCurrentPrintPage <= mNrPages)
                {
                    e.HasMorePages = true;
                }
                else
                {
                    e.HasMorePages = false;
                    mCurrentPrintPage = 1;  // set ready for the print by print preview
                }

            }
            catch (Exception)
            {

            }
        }
*/        public static void InvertZOrderOfControls(Control.ControlCollection ControlList)
        {
            // do not process empty control list
            if (ControlList.Count == 0)
                return;
            // only re-order if list is writable
            if (!ControlList.IsReadOnly)
            {
                SortedList<int, Control> sortedChildControls = new SortedList<int, Control>();
                // find all none docked controls and sort in to list
                foreach (Control ctrlChild in ControlList)
                {
                    if (ctrlChild.Dock == DockStyle.None)
                        sortedChildControls.Add(ControlList.GetChildIndex(ctrlChild), ctrlChild);
                }
                // re-order the controls in the parent by swapping z-order of first 
                // and last controls in the list and moving towards the center
                for (int i = 0; i < sortedChildControls.Count / 2; i++)
                {
                    Control ctrlChild1 = sortedChildControls.Values[i];
                    Control ctrlChild2 = sortedChildControls.Values[sortedChildControls.Count - 1 - i];
                    int zOrder1 = ControlList.GetChildIndex(ctrlChild1);
                    int zOrder2 = ControlList.GetChildIndex(ctrlChild2);
                    ControlList.SetChildIndex(ctrlChild1, zOrder2);
                    ControlList.SetChildIndex(ctrlChild2, zOrder1);
                }
            }
            // try to invert the z-order of child controls
            foreach (Control ctrlChild in ControlList)
            {
                try { InvertZOrderOfControls(ctrlChild.Controls); }
                catch { }
            }
        }
#endif
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
        }

        private void toolStripButton1_Click_1(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            try
            {
               }
            catch (Exception ex)
            {
                CProgram.sLogException("Print Analysis", ex);
            }
        }

        private void panel64_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel52_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label39_Click(object sender, EventArgs e)
        {

        }

        private void label57_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label104_Click(object sender, EventArgs e)
        {

        }

        private void label102_Click(object sender, EventArgs e)
        {

        }

        private void panel113_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBoxBaselineReferenceECGStrip_Click(object sender, EventArgs e)
        {

        }

        private void panel62_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel51_Paint(object sender, PaintEventArgs e)
        {

        }
        private void CPrintAnalysisForm_Shown(object sender, EventArgs e)
        {
            BringToFront();
            timer1.Enabled = true;
            toolStrip1.Focus();
            //            mPanelToClipBoard1(panelPrintArea);
            Application.DoEvents();
        }

        private void labelRatemeasureSI_Click(object sender, EventArgs e)
        {

        }

        private void CPrintAnalysisForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            timer1.Enabled = false;
        }

        private void toolStripClipboard_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;

            //mPrint1Bitmap = mPanelToImage(panelPrintArea1, false, true);
            if(mPrintBitmapList != null && mPrintBitmapList.Count >= 1)
            {
                Clipboard.SetImage(mPrintBitmapList[0]);
            }
        }

/*        private bool mbEnterLable( Label ArLabel, string AName )
        {
            bool bOk = false;

            if( ArLabel != null )
            {
                string s = ArLabel.Text;
                    
                    if( CProgram.sbReqLabel( "Enter Analysis header", "Patient Last name", ref s, "" ))
                {
                    ArLabel.Text = s;
                    bOk = true;

                }
            }
            return bOk;
        }
  */      private void toolStripEnterData_Click(object sender, EventArgs e)
        {

        }

        private void dataGridViewPrevThreeTX_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void labelPhysSignLine_Click(object sender, EventArgs e)
        {

        }

        private void labelSerialNrHeader_Click(object sender, EventArgs e)
        {

        }

        private void labelPhysPrintName_Click(object sender, EventArgs e)
        {

        }

        private void label53_Click(object sender, EventArgs e)
        {

        }

        private void label93_Click(object sender, EventArgs e)
        {

        }

        private void label78_Click(object sender, EventArgs e)
        {

        }

        private void label19_Click(object sender, EventArgs e)
        {

        }

        private void toolStripClipboard2_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;

//            mPrint2Bitmap = mPanelToImage(panelPrintArea2, false, true);
            if (mPrintBitmapList != null && mPrintBitmapList.Count >= 2)
            {
                Clipboard.SetImage(mPrintBitmapList[1]);
            }

        }

        private void label14_Click(object sender, EventArgs e)
        {
            /*                // Create a new PDF document

                            PdfDocument document = new PdfDocument();

                            document.Info.Title = "Created with PDFsharp";

                            // Create an empty page

                            PdfPage page = document.AddPage();
                            // Get an XGraphics object for drawing
                            page.Size = PdfSharp.PageSize.A4;
                            page.Orientation = PageOrientation.Portrait;

                            XGraphics gfx = XGraphics.FromPdfPage(page);

                            double wPage = page.Width;
                            double hPage = page.Height;
                            double fMargin = 0.05;
                            double fMarginLeft = 0.055;
                            double x = wPage * fMarginLeft;
                            double y = hPage * fMargin;
                            double w = wPage * (1 - fMargin - fMarginLeft);
                            double h = hPage * (1 - 2 * fMargin);

                            // Create a font
                            XFont font = new XFont("Verdana", 20, XFontStyle.BoldItalic);
                            // Draw the text
                            gfx.DrawString("Hello, World!", font, XBrushes.Black,
                            new XRect(0, 0, page.Width, y),
                            XStringFormats.Center);


                            XImage image = XImage.FromGdiPlusImage(mPrint1Bitmap);
                            // Left position in point

                            gfx.DrawImage(image, x, y, w, h);

                            if (mPrint2Bitmap != null && mNrPages >= 2)
                            {
                                page = document.AddPage();
                                // Get an XGraphics object for drawing
                                gfx = XGraphics.FromPdfPage(page);
                                page.Size = PdfSharp.PageSize.A4;
                                page.Orientation = PageOrientation.Portrait;

                                wPage = page.Width;
                                hPage = page.Height;
                                x = wPage * fMarginLeft;
                                y = hPage * fMargin;
                                w = wPage * (1 - fMargin - fMarginLeft);
                                h = hPage * (1 - 2 * fMargin);

                                // Draw the text
                                gfx.DrawString("Hello, World, this is page 2!", font, XBrushes.Black,
                                new XRect(0, 0, page.Width, y),
                                XStringFormats.Center);


                                image = XImage.FromGdiPlusImage(mPrint2Bitmap);
                                // Left position in point

                                gfx.DrawImage(image, x, y, w, h);

                            }

                            // Save the document...
                            const string filename = "HelloWorld.pdf";
                            document.Save(filename);
                            // ...and start a viewer.
                            Process.Start(filename);

              */
        }

        private bool mbPrint2Pdf()
        {
            bool bOk = false;
            string fullName = "";
            bool bTestRemove = false;

            if (mPrintBitmapList != null && mPrintBitmapList.Count >= 1)
            {
                try
                {
                    CProgram.sMemoryCleanup(true, true);
                    toolStripGenPages.Text = "PDF generarating...";
                    CPdfDocRec pdfDocRec = new CPdfDocRec(CDvtmsData.sGetDBaseConnection());
                    UInt32 studyIX = _mStudyInfo == null ? 0 : _mStudyInfo.mIndex_KEY;
                    UInt16 studySubNr = _mRecordDb.mSeqNrInStudy;
                    UInt32 patientIX = _mPatientInfo == null || _mbAnonymize ? 0 : _mPatientInfo.mIndex_KEY;
                    string reportType = CStudyInfo.sGetStudyPdfCode(DStudyPdfCode.Event, _mbQCd);
  /*                  bool bAnnalysis = _mRecAnalysis != null && _mRecAnalysis.mIndex_KEY != 0;
                    UInt32 refID = bAnnalysis ? _mRecAnalysis.mIndex_KEY : _mRecordDb.mIndex_KEY;
                    string refLetter = bAnnalysis ? "A" : "R";
*/                    bool bStudy = studyIX != 0 && studySubNr != 0;
                    UInt32 refID = bStudy ? studySubNr : _mRecordDb.mIndex_KEY;
                    string refLetter = bStudy ? "r" : "R";
                    DateTime dt = _mRecordDb.mGetDeviceTime(_mRecordDb.mEventUTC);
                    PdfDocument pdfDocument;
                    PageOrientation pageOrientation = PageOrientation.Portrait;
                    string extraName = _mbQCd ? "QC" : "";


                    bool bInitials = _mCreadedByInitials != null && _mCreadedByInitials.Length > 0;

                    if (false == bInitials)
                    {
                        if (CProgram.sbReqLabel("Print pdf", "User Initials", ref _mCreadedByInitials, "", false))
                        {
                            bInitials = _mCreadedByInitials != null && _mCreadedByInitials.Length > 0;
                        }
                    }
                    CProgram.sbLogMemSize("generated Analysis print R#" + _mRecordDb.mIndex_KEY.ToString() + " S"
                        + _mRecordDb.mStudy_IX.ToString() +"." + _mRecordDb.mSeqNrInStudy.ToString() + " report "
+ _mStudyReportNr.ToString() + " " + mNrPages.ToString() + " pages ");

                    if (bInitials && pdfDocRec != null
                        && null != (pdfDocument = pdfDocRec.mbNewDocument(_mCreadedByInitials, studyIX, _mStudyReportNr, patientIX, reportType, refLetter, refID, extraName, dt, dt)))
                    {
                        int nPages =  mPrintBitmapList.Count;
                        int i = 0;

                        while( mPrintBitmapList.Count > 0)
 //                       for (int i = 0; i < nPages; ++i)
                        {
                            if (i++ > nPages)
                            {
                                CProgram.sLogError("printing to much pages");
                                break;
                            }
                            Bitmap page = mPrintBitmapList[0];
                            pdfDocRec.mbAddImagePage(pageOrientation, page);
                            mPrintBitmapList.Remove(page);
                            page.Dispose();
                            page = null;

                        }
                        toolStripGenPages.Text = "PDF saving...";
                        CProgram.sMemoryCleanup(true, true);
                        if (studyIX > 0  && false == _mbAnonymize)
                        {
                            _mStudyInfo.mbSetNextReportNr(_mStudyReportNr);
                            bTestRemove = true;
                            bOk = pdfDocRec.mbSaveToStudyPdf(out fullName, true);
                        }
                        else
                        {
                            // no study save to use record
                            bTestRemove = true;
                            bOk = pdfDocRec.mbSaveToDir(Path.GetDirectoryName(_mRecordFilePath), out fullName, false);
                        }

                        if (bOk)
                        {
                            CProgram.sbLogMemSize("saved Analysis print R#" + _mRecordDb.mIndex_KEY.ToString() + " S" 
                                + _mRecordDb.mStudy_IX.ToString() + "." + _mRecordDb.mSeqNrInStudy.ToString() + " report "
    + _mStudyReportNr.ToString() + " " + mNrPages.ToString() + " pages " + Path.GetFileName(fullName));

                            toolStripGenPages.Text = "PDF opening...";
                            _mReportIX = pdfDocRec.mIndex_KEY;
                            Process.Start(fullName);
                            Close();
                        }
                        mUpdateMemUse();
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("PdfPrint stat event failed", ex);
                    bTestRemove = true;
                }
                if (bTestRemove && _mReportIX == 0)
                {
                    try
                    {
                        bTestRemove = fullName.Length == 0 || false == File.Exists(fullName);
                    }
                    catch (Exception ex)
                    {
                        bTestRemove = false;
                    }
                    if (bTestRemove)
                    {
                        _mStudyInfo.mbReverseNextReportNr(_mStudyReportNr); // something failed revert to previousNr Reports
                    }
                }

            }
            return bOk;
        }
        /*        private void mPrint2Pdfold()
                {
                    if (mPrint1Bitmap != null)
                    {
                        // Create a new PDF document

                        PdfDocument document = new PdfDocument();

                        document.Info.Title = "Created with PDFsharp";

                        // Create an empty page

                        PdfPage page = document.AddPage();
                        // Get an XGraphics object for drawing
                        page.Size = PdfSharp.PageSize.A4;
                        page.Orientation = PageOrientation.Portrait;
                        XGraphics gfx = XGraphics.FromPdfPage(page);

                        double wPage = page.Width;
                        double hPage = page.Height;
                        double fMargin = 0.05;
                        double fMarginLeft = 0.055;
                        double x = wPage * fMarginLeft;
                        double y = hPage * fMargin;
                        double w = wPage * (1 - fMargin - fMarginLeft);
                        double h = hPage * (1 - 2 * fMargin);

                        // Create a font
                        XFont font = new XFont("Verdana", 20, XFontStyle.BoldItalic);
                        // Draw the text
                        gfx.DrawString("Hello, World!", font, XBrushes.Black,
                        new XRect(0, 0, page.Width, y ),
                        XStringFormats.Center);


                        XImage image = XImage.FromGdiPlusImage(mPrint1Bitmap);
                        // Left position in point

                        gfx.DrawImage(image, x, y, w, h);

                        if (mPrint2Bitmap != null && mNrPages >= 2 )
                        {
                            page = document.AddPage();
                            // Get an XGraphics object for drawing
                            page.Size = PdfSharp.PageSize.A4;
                            page.Orientation = PageOrientation.Portrait;

                            gfx = XGraphics.FromPdfPage(page);
                            wPage = page.Width;
                            hPage = page.Height;
                            x = wPage * fMarginLeft;
                            y = hPage * fMargin;
                            w = wPage * (1 - fMargin - fMarginLeft);
                            h = hPage * (1 - 2 * fMargin);

                            // Draw the text
                            gfx.DrawString("Hello, World, this is page 2!", font, XBrushes.Black,
                            new XRect(0, 0, page.Width, y),
                            XStringFormats.Center);


                            image = XImage.FromGdiPlusImage(mPrint2Bitmap);
                            // Left position in point

                            gfx.DrawImage(image, x, y, w, h);

                        }

                        // Save the document...
                        const string filename = "HelloWorld.pdf";
                        document.Save(filename);
                        // ...and start a viewer.
                        Process.Start(filename);
                    }
                }
        */
        private void toolStripButton1_Click_2(object sender, EventArgs e)
        {
            mbPrint2Pdf();
        }
        private void mUpdateMemUse()
        {
 /*           UInt32 progMB = CProgram.sGetProgUsedMemoryMB();
            UInt32 freeMB = CProgram.sGetFreeMemoryMB();
            toolStripMemUsage.Text = "p" + progMB.ToString() + " f" + freeMB.ToString() + "MB";
*/            string memInfo = "";
            toolStripMemUsage.BackColor = CProgram.sbCheckChangedMemSize(out memInfo) ? toolStripClipboard1.BackColor : Color.Orange;
            toolStripMemUsage.Text = memInfo;

        }

        private void migraDocPrintDocument1_PrintPage(object sender, PrintPageEventArgs e)
        {
        }

        private void label100_Click(object sender, EventArgs e)
        {

        }

        private void label43_Click(object sender, EventArgs e)
        {

        }

        private void label48_Click(object sender, EventArgs e)
        {

        }

        private void label91_Click(object sender, EventArgs e)
        {

        }

        private void label52_Click(object sender, EventArgs e)
        {

        }

        private void mInitDiscosureStrip(Label ALead, Label ADate, Label ATime, PictureBox APicture)
        {
            try
            {
                Control stripPanel = APicture.Parent.Parent;
                Image image = null;
                string extra = "";
                string pictName = APicture.Name;

                bool bVisible = true;

                if (_mbDisclosureLastPage)
                {
                    extra = "!";
                    bVisible = false;
                    stripPanel.BackColor = _mDefaultPanelColor;
                }
                else
                {
//                    panel.Visible = true;

                    bool bAutoSizeA = false;
                    float cursorStart = -1.0F;
                    float cursorDuration = 0.0F;

                    image = mMakeChartImage(_mRecordFile,_mDisclosureImageWidth, _mDisclosureImageHeight, _mDisclosureChannel, bAutoSizeA,
                        _mDisclosureStartSec, _mStripSec, _mDisclosureStartA, _mDisclosureEndA, cursorStart, cursorDuration, 
                        _mDisclosureUnitT, _mDisclosureUnitA, CRecordMit.sGetMaxAmplitudeRange(), false);

                    if (image == null)
                    {
                        bAutoSizeA = false;
                        string s = APicture.Name;
                    }

                }
                stripPanel.Visible = bVisible;
                Control textPanel = ALead.Parent;
                bool b = _bDisclosureDebug && _mDisclosureChannel == 0 && (_mDisclosureNrChannels > 1);
                if (b)
                {
                    extra += "+";
                }
                textPanel.BackColor = b ? Color.Gainsboro : _mDefaultPanelColor;
                if (_mDisclosureStartSec > _mDisclosureEndSec)
                {
                    extra += ">";
                    textPanel.BackColor = Color.Red;
                }
                else if (image == null)
                {
                    extra += "-";
                    textPanel.BackColor = Color.Blue;
                }

                if (_bDisclosureDebug == false) extra = "";
                ALead.Text = bNotProgrammer ?  "" : ((_bDisclosureDebug ? _mDisclosureStripPart.ToString("D03") + ": " : "" ) + extra + _mRecordDb.mGetChannelName(_mDisclosureChannel));
                //                ALead.BackColor = _mDisclosureChannel == 0 && _mDisclosureNrChannels > 0 ? Color.LightGray : ADate.BackColor;
                ADate.Text = bNotProgrammer ? "" : extra + CProgram.sDateToString(_mDisclosureDT);
                ATime.Text = extra + CProgram.sTimeToString(_mDisclosureDT);
                if (APicture.InitialImage != null) APicture.InitialImage = null;
//                if (APicture.BackgroundImage != null) APicture.BackgroundImage = null;
                APicture.BackgroundImage = image;

                bool bCopyToClipboard = _bDisclosureDebug;
                if(bCopyToClipboard )
                {
                    if( image != null )
                    {
                        Clipboard.SetImage(image);
                    }
                    else
                    {
                        Clipboard.Clear();
                    }
                    string name = APicture.Name;
                    bCopyToClipboard = false;
                   
                }
                if( stripPanel.Visible != bVisible)
                {
                    string name = stripPanel.Name;
                    stripPanel.Visible = bVisible;
                }
                if( stripPanel != textPanel.Parent.Parent)
                {
                    string name1 = stripPanel.Name;
                    string name2 = textPanel.Name;
                }

            }
            catch (Exception ex)
            {
                CProgram.sLogException("Init disclosure strip", ex);
            }
        }


        private void mInitDisclosurePage()
        {
            ++_mDiscloseIndexPageNr;
            string pageStr = _mDiscloseIndexPageNr.ToString();

            if (_mbDisclosureLastPage)
            {
                _mbDisclosureFinished = true;
                pageStr += "!";
                labelStartPageDateTime.Text = "Not used";
            }
            else
            {
                labelStartPageDateTime.Text = CProgram.sDateTimeToString(_mDisclosureDT);

                mInitDiscosureStrip(labelStripLead1, labelStripDate1, labelStripTime1, pictureBoxStrip1);
                mbMoveToNextDisclosureStrip();

                mInitDiscosureStrip(labelStripLead2, labelStripDate2, labelStripTime2, pictureBoxStrip2);
                mbMoveToNextDisclosureStrip();

                mInitDiscosureStrip(labelStripLead3, labelStripDate3, labelStripTime3, pictureBoxStrip3);
                mbMoveToNextDisclosureStrip();

                mInitDiscosureStrip(labelStripLead4, labelStripDate4, labelStripTime4, pictureBoxStrip4);
                mbMoveToNextDisclosureStrip();

                mInitDiscosureStrip(labelStripLead5, labelStripDate5, labelStripTime5, pictureBoxStrip5);
                mbMoveToNextDisclosureStrip();

                mInitDiscosureStrip(labelStripLead6, labelStripDate6, labelStripTime6, pictureBoxStrip6);
                mbMoveToNextDisclosureStrip();

                mInitDiscosureStrip(labelStripLead7, labelStripDate7, labelStripTime7, pictureBoxStrip7);
                mbMoveToNextDisclosureStrip();

                mInitDiscosureStrip(labelStripLead8, labelStripDate8, labelStripTime8, pictureBoxStrip8);
                mbMoveToNextDisclosureStrip();

                mInitDiscosureStrip(labelStripLead9, labelStripDate9, labelStripTime9, pictureBoxStrip9);
                mbMoveToNextDisclosureStrip();

                mInitDiscosureStrip(labelStripLead10, labelStripDate10, labelStripTime10, pictureBoxStrip10);
                mbMoveToNextDisclosureStrip();

                mInitDiscosureStrip(labelStripLead11, labelStripDate11, labelStripTime11, pictureBoxStrip11);
                mbMoveToNextDisclosureStrip();

                mInitDiscosureStrip(labelStripLead12, labelStripDate12, labelStripTime12, pictureBoxStrip12);
                mbMoveToNextDisclosureStrip();

                mInitDiscosureStrip(labelStripLead13, labelStripDate13, labelStripTime13, pictureBoxStrip13);
                mbMoveToNextDisclosureStrip();

                mInitDiscosureStrip(labelStripLead14, labelStripDate14, labelStripTime14, pictureBoxStrip14);
                mbMoveToNextDisclosureStrip();

                mInitDiscosureStrip(labelStripLead15, labelStripDate15, labelStripTime15, pictureBoxStrip15);
                mbMoveToNextDisclosureStrip();

                mInitDiscosureStrip(labelStripLead16, labelStripDate16, labelStripTime16, pictureBoxStrip16);
                mbMoveToNextDisclosureStrip();

                mInitDiscosureStrip(labelStripLead17, labelStripDate17, labelStripTime17, pictureBoxStrip17);
                mbMoveToNextDisclosureStrip();

                mInitDiscosureStrip(labelStripLead18, labelStripDate18, labelStripTime18, pictureBoxStrip18);
                mbMoveToNextDisclosureStrip();

                mInitDiscosureStrip(labelStripLead19, labelStripDate19, labelStripTime19, pictureBoxStrip19);
                mbMoveToNextDisclosureStrip();

                mInitDiscosureStrip(labelStripLead20, labelStripDate20, labelStripTime20, pictureBoxStrip20);
                mbMoveToNextDisclosureStrip();

                //              pictureBoxStrip18.Image = pictureBoxStrip1.Image;

                DateTime dt = _mDisclosureDT.AddSeconds(_mStripSec);
                labelEndPageTime.Text = CProgram.sTimeToString(dt);

                if (_mbDisclosureLastPage)
                {
                    pageStr += ".";
                }
            }
            labelPage3x.Text = _mDiscloseBasePageNr.ToString() + " - " + pageStr;
            toolStripGenPages.Text = "Disclosure " + pageStr;   // show on screen
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;

            try
            {
                mUpdateMemUse();
                if (_mbFirst)
                {
                    mPrintBitmapList.Add(CPdfDocRec.sPanelToImage(panelPrintArea1, false, true));
                    if (_mbSecondPage) mPrintBitmapList.Add(CPdfDocRec.sPanelToImage(panelPrintArea2, false, true));
                    if (_mbDisclosure && false == _mbDisclosureFinished)
                    {
                        mPrintBitmapList.Add(CPdfDocRec.sPanelToImage(panelPrintArea3, false, true));
                        mInitDisclosurePage();
                        timer1.Interval = 2000;
                    }
                    else
                    {
                        _mbDisclosureFinished = true;
                        timer1.Interval = 500;
                    }
                    _mbFirst = false;
                    timer1.Enabled = true;
                }
                else if (_mbDisclosureFinished)
                {
                    toolStripGenPages.Text = "Done.";
                    mbPrintImages();
                   Close();
                }
                else
                {

                    if (_mbDisclosure)
                    {
                        Bitmap bmp = CPdfDocRec.sPanelToImage(panelPrintArea3, false, _mbWarnEmptyBmp);
                        if (bmp != null)
                        {
                            mPrintBitmapList.Add(bmp);
                        }
                        else
                        {
                            _mbWarnEmptyBmp = false;    // disable show errors
                        }
                        mInitDisclosurePage();
                        timer1.Interval = 2000;
                    }
                    timer1.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Print Analysis", ex);
                toolStripGenPages.Text = "Error " + toolStripGenPages.Text;
            }
        }

        private void label53_Click_1(object sender, EventArgs e)
        {
        }

        public bool mbMoveToFirstDisclosureStrip()
        {
            bool bDraw = false;
            if (_mbDisclosure == false || _mRecordDb == null || _mRecordFile == null)
            {
                _mbDisclosureFinished = true;
                _mbDisclosureLastPage = true;
                labelEventDataTime.Text = "Not used";
            }
            else
            {
                _mDisclosureStripPart = 0;
                bDraw = true;
                _mDisclosureEventDT = _mRecordDb.mGetDeviceTime(_mRecordDb.mEventUTC);
                _mDisclosureEventSec = _mRecordDb.mGetEventSec();
                _mDisclosureDT = _mRecordFile.mGetDeviceTime(_mRecordFile.mBaseUTC);
                _mDisclosureStartSec = 0;
                _mDisclosureEndSec = _mRecordFile.mGetSamplesTotalTime();
                _mDisclosureChannel = 0;
                _mDisclosureNrChannels = 0;
                _mDisclosureStripIndex = 0;

                _mDisclosureImageWidth = (UInt16)pictureBoxStrip1.Width;
                _mDisclosureImageHeight = (UInt16)pictureBoxStrip1.Height;

                labelEventDataTime.Text = CProgram.sDateTimeToString(_mDisclosureEventDT);
                labelUnitFeed.Text = _mRecordFile.mGetShowSpeedString(_mDisclosureUnitT);
                labelUnitAmp.Text = _mRecordFile.mGetShowAmplitudeString(_mDisclosureUnitA);

                DateTime endDT = _mDisclosureDT.AddSeconds(_mDisclosureEndSec);
                /*                labelSFdStripInfo.Text = "Date:  " + CProgram.sDateToString(_mDisclosureDT)
                                    + "   Start:  " + CProgram.sTimeToString(_mDisclosureDT)
                //                    + "   Stop:  " + CProgram.sDateTimeToString(endDT)
                                    + "   Length:  " + _mDisclosureEndSec.ToString("0.0")
                                    + " sec. "
                                    + (_mbHoleStrip ? " Drawing full strip" : " Drawing parts of strip")
                                    + (_mbAllChannels ? " all channels" : "")
                                    + ".";*/
                               labelSFdStripInfo.Text = "Start strip " + CProgram.sTimeToString(_mDisclosureDT) + " " + CProgram.sDateToString(_mDisclosureDT)
                //                    + "   Stop:  " + CProgram.sDateTimeToString(endDT)
                                    + ", length " + _mDisclosureEndSec.ToString("0.0") + " sec."
                                    + (_mbHoleStrip ? ", Drawing full strip" : ", Drawing strip parts")
                                    + (_mbAllChannels ? ", all channels" : "")
                                    + ".";


                string strLength = _mStripSec.ToString() + " seconds";
                labelStripLength.Text = strLength;

                // calculate first
//                _mbHoleStrip = true;
//                _mbAllChannels = true;

                if (_mbAllChannels)
                {
                    _mDisclosureChannel = 0;
                    _mDisclosureNrChannels = _mRecordFile.mNrSignals;
                }
                else
                {
                    _mDisclosureStripIndex = 0;
                    _mDisclosureNrChannels = 1;
                    CMeasureStrip ms = _mRecAnalysisFile.mFindActiveStrip((UInt16)_mDisclosureStripIndex);

                    if (ms != null )
                    {
                        _mDisclosureChannel = ms._mChannel;
                        _mDisclosureNrChannels = 1;

                        if (false == _mbHoleStrip)
                        {
                            _mDisclosureStartSec = ms._mStartTimeSec;
                            _mDisclosureDT = _mRecordFile.mGetDeviceTime(_mRecordFile.mBaseUTC.AddSeconds(_mDisclosureStartSec));
                            _mDisclosureEndSec = _mDisclosureStartSec + ms._mDurationSec;
                            _mDisclosureStartA = ms._mStartA;
                            _mDisclosureEndA = ms._mEndA;
                        }
                    }
                    else
                    {
                        _mDisclosureChannel = 0;
                    }
                }
                if (_mbHoleStrip)
                {
                    UInt16 startChannel = _mbAllChannels ? (UInt16)0 : _mDisclosureChannel;
                    UInt16 nrChannels = _mbAllChannels ? _mDisclosureNrChannels : (UInt16)1;
                    _mDisclosureStartA = 1e6F;
                    _mDisclosureEndA = -1e6F;

                    float minA, maxA;

                    for (UInt16 i = 0; i < nrChannels; ++i)
                    {
                        minA = 1e6F;
                        maxA = -1e6F;
                        _mRecordFile.mbGetAmplitudeRange(startChannel, 0.0F, _mDisclosureEndSec, out minA, out maxA, CRecordMit.sGetMaxAmplitudeRange());

                        if (minA < _mDisclosureStartA) _mDisclosureStartA = minA;
                        if (maxA > _mDisclosureEndA) _mDisclosureEndA = maxA;
                        ++startChannel;
                    }

                    _mDisclosureStartSec = 0;
                    _mDisclosureStartA -= _mDisclosureUnitA * 0.05F;
                    _mDisclosureEndA += _mDisclosureUnitA * 0.05F;
                }
                else
                {
                    _mDisclosureStripIndex = 0;
                    // find first active strip
                    bDraw = mbToStartCurrentDisclosureStrip(); // false if no strip
                    _mDisclosureNrChannels = 1;
                }
                if (bDraw)
                {
                    _mbDisclosureFinished = false;
                    _mbDisclosureLastPage = false;
                }
                else
                {
                    _mbDisclosureFinished = true;
                    _mbDisclosureLastPage = true;
                }
            }
            return bDraw;
        }

        private void labelStartDate_Click(object sender, EventArgs e)
        {

        }

        private void labelStripLength3_Click(object sender, EventArgs e)
        {

        }

        private void labelStripDate1_Click(object sender, EventArgs e)
        {

        }

        private void label19_Click_1(object sender, EventArgs e)
        {

        }

        private void panel21_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panelPhysInfo_Paint(object sender, PaintEventArgs e)
        {

        }

        private void labelSample5Sweep_Click(object sender, EventArgs e)
        {

        }

        private void labelSample5Start_Click(object sender, EventArgs e)
        {

        }

        private void labelTimeSampleNr2_Click(object sender, EventArgs e)
        {

        }

        public bool mbToStartCurrentDisclosureStrip()
        {
            bool bPresent = false;
            CMeasureStrip ms = _mRecAnalysisFile.mFindActiveStrip((UInt16)_mDisclosureStripIndex);

            if (ms != null)
            {
                bPresent = true;
                _mDisclosureChannel = ms._mChannel;
                _mDisclosureNrChannels = 1;
                _mDisclosureStartSec = ms._mStartTimeSec;
                _mDisclosureDT = _mRecordFile.mGetDeviceTime(_mRecordFile.mBaseUTC.AddSeconds(_mDisclosureStartSec));
                _mDisclosureEndSec = _mDisclosureStartSec + ms._mDurationSec;
                _mDisclosureStartA = ms._mStartA;
                _mDisclosureEndA = ms._mEndA;

            }

            return bPresent;
        }
        public bool mbMoveToNextDisclosureStrip()
        {

            if (_mbDisclosure == false)
            {
                _mbDisclosureFinished = true;
                _mbDisclosureLastPage = true;
            }
            else if (false == _mbDisclosureLastPage)
            {
                ++_mDisclosureStripPart;
                bool bNextDone = false;
                if (_mDisclosureNrChannels > 1)
                {
                    if (++_mDisclosureChannel < _mDisclosureNrChannels)
                    {
                        bNextDone = true;   // show next channel
                    }
                }
                if (false == bNextDone)
                {
                    _mDisclosureStartSec += _mStripSec; // move to next part of strip
                    _mDisclosureDT = _mDisclosureDT.AddSeconds(_mStripSec);
                    bNextDone = _mDisclosureStartSec + 0.01F < _mDisclosureEndSec;
                    if (bNextDone && _mDisclosureNrChannels > 1)
                    {
                        _mDisclosureChannel = 0;    // all strips start at 0 again
                    }
                }
                if (false == bNextDone)
                {
                    if (_mbHoleStrip)
                    {
                        bNextDone = false; // done
                    }
                    else
                    {
                        ++_mDisclosureStripIndex;
                        if (mbToStartCurrentDisclosureStrip())
                        {
                            bNextDone = true;
                            if (_mDisclosureNrChannels > 1)
                            {
                                _mDisclosureChannel = 0;    // all strips start at 0 again
                            }
                        }
                    }
                }
                _mbDisclosureLastPage = false == bNextDone;
            }
            return _mbDisclosureLastPage;
        }
    }
}
