﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Program_Base;
using Event_Base;
using System.IO;
using System.Drawing.Printing;
using System.Diagnostics;
//using System.Windows.Media;
//using System.Windows.Media.Imaging;
using EventboardEntryForms;
using EventBoard.EntryForms;
using DynaVisionGraph.Filter;
using EventBoard.Event_Base;

namespace EventBoard
{
    public enum DMeasureClickState
    {
        Off,
        Down1,
        Up1,
        Down2,
        Up2
    }

    public partial class FormAnalyze : Form
    {
        public const float _cZoomTimeShiftFactor = 0.5F;
        public const float _cZoomAmplShiftFactor = 0.5F;
        public const float _cChannelsTimeShiftFactor = 0.75F;

        public static bool _sbTriageLockAnRec = true;

        public DateTime _mCreateFormStartDT;
        public DateTime _mCreateFormEndDT;
        public DateTime _mLoadFormStartDT;
        public DateTime _mLoadFormEndDT;
        public double _mCreateFormSec;
        public double _mLoadFormSec;

        public static bool _sbSaveAnStripImage;
        public static bool _sbPlot2Ch = false;
        public static bool _sbBlackWhite = false;

        public static float _sZoomDurationSec = 6;
        public static UInt16 _sZoomChannel = 0;
        public static float _sChannelsDurationSec = 120;
        public static bool _sbPrintFD = false;
        public static bool _sbPrintFE = false;
        public static bool _sbPrintAC = false;
        public static UInt16 _sPrintStripSec = 60;

        public static UInt16 _sAnHrMinBpm = 0;
        public static UInt16 _sAnHrLowBpm = 40;
        public static UInt16 _sAnHrBaseBpm = 80;
        public static UInt16 _sAnHrHighBpm = 120;
        public static UInt16 _sAnHrMaxBpm = 350;


        public UInt32 _mRecordIndex;
        UInt32 _mAnalysisIndex;
        public UInt16 _mAnalysisSeqNr;
        bool _mbNew = false;
        bool _mbReadOnly = true;
        CSqlDBaseConnection _mDbConnection;
        UInt16 _mOldState = 0;
        UInt16 _mNewState = 0;

        FormEventBoard _mEventBoardForm;

        //        string _mCreadedByInitials;

        public CRecordMit _mRecordDb = null;
        public CRecordMit _mRecordFile = null;
        public CRecAnalysis _mRecAnalysis = null;
        public CRecAnalysisFile _mRecAnalysisFile = null;

        public CDeviceInfo _mDeviceInfo = null;
        public CStudyInfo _mStudyInfo = null;
        public CPatientInfo _mPatientInfo = null;

        public CRecordMit _mBaseLineRecordDb = null;
        public CRecordMit _mBaseLineRecordFile = null;
        public CRecAnalysis _mBaseLineRecAnalysis = null;
        public CRecAnalysisFile _mBaseLineRecAnalysisFile = null;

        string _mRecordingDir;
        string _mRecordDir;
        public string _mRecordFilePath;
        UInt16 _mNrSignals = 0;
        CStripChart _mZoomChart;
        CStripChart[] _mStripCharts;
        Image _mZoomImage;
        Image[] _mStripImages;
        Panel[] _mStripPanels;

        UInt16 _mZoomChannel = 0;
        float _mZoomCenterSec = 3;
        float _mZoomDurationSec = 6.0F;

        float _mChannelsCenterSec = 0;
        float _mChannelsDurationSec = 120.0F;

        float _mZoomUnitT = 0.2F;
        float _mZoomUnitA = 0.5F;
        float _mChannelsUnitT = 1.0F; //0.2F; // 25 mm/S
        float _mChannelsUnitA = 0.5F; // 10 mm/mV;

        float _mChannelsAmplitudeRange = 0.0F;
        float _mChannelsAmplitudeMin = 0.0F;
        float _mChannelsAmplitudeMax = 0.0F;


        DateTime _mChannelStartTime;
        Point _mPoint1, _mPoint2;
        float _mPoint1T, _mPoint1A;
        float _mPoint2T, _mPoint2A;
        DMeasureClickState _mClickState = DMeasureClickState.Off;
        bool _mbClickDualPoint = true;
        bool _mbMeasureLineDrawn = false;

        bool _mbAmplitudeZoomWindow = false;
        bool _mSpeedZoomWindow = false;

        int _mSelectedStripIndex = 0;
        int _mSelectedMeasurementIndex = 0;
        int _mSelectNewStripIndex = -1;
        int _mSelectNewMeasurementIndex = -1;
        bool _mbUpdatingStripList = false;
        bool _mbUpdatingMeasurementList = false;
        bool _mbUpdatingStatList = false;

        private const UInt16 _cMaxStripChart = 12;

        Bitmap _mPrintBitmap;

        private const UInt16 _cAnNrButtons = 16;
        private UInt16 _mAnCountActive = 0;
        private Color _mAnColorOff = Color.White;
        private Color _mAnColorOn = Color.DimGray;
        private String _mAnCurrentCode = "";

        private Button[] _mAnButtons = null;
        private string[] _mAnCodes = null;

        private bool mbPrintEntered = false;

        private static bool _sbAnalyzeFindingsLoaded = false;
        private static string[] _sAnalyzeFindingsOptions = null;
        private string _mAnalyzeSubTitle;

        private StreamWriter _mLockStream = null;
        private string _mLockFilePath;
        private string _mLockName;

        private bool _mbChanged = false;
        private bool _mbInitScreenDone = false;
        private string _mPrevTechnician;

        bool bShowIndexValues = false;

        private FormAnPageView _mFormPageView = null;
        private UInt16 _mPageViewNrChannels = 0;
        private UInt16 _mPageViewShowChannel = 0;
        private UInt16 _mPageViewPartSec = 0;

        private string _mTextLPF = "";
        private string _mTextHPF = "";
        private string _mTextBBF = "";
        private string _mTextACRF = "";

        UInt16 mEcgNrAvgPoints = 0;
        UInt16 mPlethNrAvgPoints = 0;

        public FormAnalyze(FormEventBoard AEventBoardForm)
        {
            try
            {
                _mCreateFormStartDT = DateTime.Now;
                _mEventBoardForm = AEventBoardForm;

                _mbChanged = false;
                _mPrevTechnician = "";
                mResetP1P2();

                bool bProgrammer = CLicKeyDev.sbDeviceIsProgrammer();

                InitializeComponent();
                CDvtmsData.sbInitData();
                labelUser.Text = CProgram.sGetProgUserInitials();

                buttonExportData.Visible = true;// need save view part 
                bool bExtra = CLicKeyDev.sGetOrgNr() < 30;

                dVX1ToolStripMenuItem.Visible = bExtra;
                mIT16ToolStripMenuItem.Visible = bExtra;
                mIT24ToolStripMenuItem.Visible = bExtra;
                mECGSimulatorToolStripMenuItem.Visible = bExtra;
                simExportToolStripMenuItem.Visible = bExtra;

                bShowIndexValues = bProgrammer;

                if (FormEventBoard._sbForceAnonymize)
                {
                    checkBoxAnonymize.Checked = true;
                    checkBoxAnonymize.Enabled = false;
                }

                if (CProgram.sbEnterProgUserArea(true))
                {

                    _mZoomDurationSec = _sZoomDurationSec >= 1 ? _sZoomDurationSec : 6;
                    _mZoomChannel = (UInt16)(_sZoomChannel > 0 ? _sZoomChannel - 1 : 0);
                    _mChannelsDurationSec = _sChannelsDurationSec >= 1 ? _sChannelsDurationSec : 120;
                    checkBoxAddDisclosure.Checked = _sbPrintFD;
                    checkBoxHoleStrip.Checked = _sbPrintFE;
                    //                    checkBoxAllChannels.Checked = _sbPrintAC;
                    if (_sPrintStripSec > 0)
                    {
                        uint value, selected = 0;
                        foreach (string s in comboBoxStripSec.Items)
                        {
                            if (UInt32.TryParse(s, out value))
                            {
                                if (_sPrintStripSec >= value)
                                {
                                    selected = value;
                                }
                            }
                        }
                        if (selected > 0)
                        {
                            comboBoxStripSec.Text = selected.ToString();
                        }
                    }
                    //                _mCreadedByInitials = "";

                    toolStripStatusLabel1.Text = "";
                    checkBoxQCD.Visible = FormEventBoard._sbQCdEnabled;

                    for (DMeasure1Tag i = DMeasure1Tag.Unknown; ++i < DMeasure1Tag.NrTags;)
                    {
                        string s = CMeasurePoint.sGetMeasure1ShowString(i);

                        contextMenuSingleClick.Items.Add(s);
                    }
                    for (DMeasure2Tag i = DMeasure2Tag.Unknown; ++i < DMeasure2Tag.NrTags;)
                    {
                        string s = CMeasurePoint.sGetMeasure2ShowString(i);

                        contextMenuDualClick.Items.Add(s);
                    }

                    //            SizeF _mScaleFactor = AutoScaleFactor;
                    panelAutoMeasurements.Enabled = false;
                    //toolStripUpDown.Enabled = false;
                    toolStripSnap.Enabled = false;
                    labelNextState.Text = "State:";
                    mUpdateFullDisclosure();

                    mbAnInitArray(buttonAnNormal.BackColor, buttonAnTachy.BackColor);
                    mbAnAddButton(buttonAnNormal, CDvtmsData._cFindings_Normal);
                    mbAnAddButton(buttonAnTachy, CDvtmsData._cFindings_Tachy);
                    mbAnAddButton(buttonAnBrady, CDvtmsData._cFindings_Brady);
                    mbAnAddButton(buttonAnPause, CDvtmsData._cFindings_Pause);
                    mbAnAddButton(buttonAnAFib, CDvtmsData._cFindings_AF);
                    mbAnAddButton(buttonAflut, CDvtmsData._cFindings_AFL);
                    mbAnAddButton(buttonAnPAC, CDvtmsData._cFindings_PAC);
                    mbAnAddButton(buttonAnPVC, CDvtmsData._cFindings_PVC);
                    mbAnAddButton(buttonAnPace, CDvtmsData._cFindings_Pace);
                    mbAnAddButton(buttonAnVtach, CDvtmsData._cFindings_VT);
                    mbAnAddButton(buttonAnBline, CDvtmsData._cFindings_BL);
                    mbAnAddButton(buttonOther, CDvtmsData._cFindings_Other);
                    mbAnAddButton(buttonAnAbNormal, CDvtmsData._cFindings_AbNormal);

                    checkBoxCh2.Checked = _sbPlot2Ch;
                    checkBoxBlackWhite.Checked = _sbBlackWhite;

                    BringToFront();
                    _mCreateFormEndDT = DateTime.Now;
                    _mCreateFormSec = (_mCreateFormEndDT - _mCreateFormStartDT).TotalSeconds;
                    labelCreateSec.Text = "c=" + _mCreateFormSec.ToString("0.000") + "sec";

                    labelCreateSec.Visible = bProgrammer;
                    labelLoadSec.Visible = bProgrammer;

                    float range = CRecordMit.sLimitAmplitudeRange;
                    if (range < 1) range = 4;
                    MaxAmplitude.Value = (int)(range + 0.5F);
                    AutoScaleCheckbox.Checked = false == CRecordMit.sbLimitAmplitudeRange;

                    toolStripButtonAddChannel.Text = "Add\r\nCh";
                    mUpdateP1P2(false);
                }
                else
                {
                    Close();
                }

            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed init FormAnalyze", ex);
                Close();
            }
        }
        public void mReleaseLock()
        {
            if (_sbTriageLockAnRec)
            {
                CProgram.sbUnLockFilePath(_mLockFilePath, _mLockName, ref _mLockStream);
            }
        }
        public bool mbIsLocked()
        {
            return _mLockStream != null;
        }
        private bool mbAnInitArray(Color AColorOff, Color ACollorOn)
        {
            bool bOk = false;

            _mAnColorOff = AColorOff;
            _mAnColorOn = ACollorOn;

            _mAnCountActive = 0;

            _mAnButtons = new Button[_cAnNrButtons];
            _mAnCodes = new string[_cAnNrButtons];

            if (_mAnButtons != null && _mAnCodes != null)
            {
                for (UInt32 i = 0; i < _cAnNrButtons; ++i)
                {
                    _mAnButtons[i] = null;
                    _mAnCodes[i] = null;
                }
                bOk = true;
            }
            return bOk;
        }

        private bool mbAnAddButton(Button AButton, string ACode)
        {
            bool bOk = AButton != null && ACode != null && ACode.Length > 0 && _mAnCountActive < _cAnNrButtons;

            if (bOk)
            {
                _mAnButtons[_mAnCountActive] = AButton;
                _mAnCodes[_mAnCountActive] = ACode;
                ++_mAnCountActive;

            }
            else
            {
                CProgram.sLogError("Failed add Annotation button: " + ACode);
            }
            return bOk;
        }

        private bool mbAnUpdateButtons()
        {
            bool bFound = false;
            if (_mAnButtons != null && _mAnCodes != null)
            {
                for (UInt32 i = 0; i < _mAnCountActive; ++i)
                {
                    if (_mAnButtons[i] != null)
                    {
                        bool bOn = _mAnCodes[i] == _mAnCurrentCode;
                        Color color = bOn ? _mAnColorOn : _mAnColorOff;

                        if (_mAnButtons[i].BackColor != color)
                        {
                            _mAnButtons[i].BackColor = color;
                        }
                        if (bOn)
                        {
                            bFound = true;
                        }
                    }
                }
            }
            return bFound;
        }

        private bool mbAnDisableButtons()
        {
            bool bFound = false;
            if (_mAnButtons != null && _mAnCodes != null)
            {
                for (UInt32 i = 0; i < _mAnCountActive; ++i)
                {
                    if (_mAnButtons[i] != null)
                    {
                        bool bOn = _mAnCodes[i] == _mAnCurrentCode;

                        _mAnButtons[i].Visible = bOn;
                        if (bOn)
                        {
                            bFound = true;
                        }
                    }
                }
            }
            return bFound;
        }
        private bool mbAnSetActive(string ACode)
        {
            _mAnCurrentCode = ACode;

            bool b = mbAnUpdateButtons();

            if (false == b)
            {
                CProgram.sLogError("Failed set Annotation button: " + ACode);
            }
            return b;
        }
        private bool mbAnClearActive()
        {
            _mAnCurrentCode = "";

            bool b = mbAnUpdateButtons();

            return b;
        }
        private string mAnGetCurrentCode()
        {
            return _mAnCurrentCode;
        }
        private bool mbAnSetActiveSet(CStringSet ACodeSet)
        {
            if (ACodeSet != null)
            {
                _mAnCurrentCode = ACodeSet.mGetValue(0);
            }
            else
            {
                _mAnCurrentCode = "";
            }
            bool b = mbAnUpdateButtons();

            if (false == b && _mAnCurrentCode != null && _mAnCurrentCode.Length > 0)
            {
                CProgram.sLogError("Failed set Annotation button: " + _mAnCurrentCode);
            }
            return b;
        }
        private bool mbAnClearActiveSet()
        {
            _mAnCurrentCode = "";

            bool b = mbAnUpdateButtons();

            return b;
        }


        private void mAnGetCurrentCodeSet(ref CStringSet ArCodeSet)
        {
            if (ArCodeSet != null)
            {
                ArCodeSet.mClear();

                if (_mAnCurrentCode != null && _mAnCurrentCode.Length > 0)
                {
                    //                    if(_mAnCurrentCode != _cFindingNormal )
                    {
                        ArCodeSet.mAddValue(_mAnCurrentCode);
                    }
                }
            }
        }

        public void mStatusLine(string AText)
        {
            toolStripStatusLabel1.Text = AText;
        }

        private void mSetZoomCenter(float ACenterSec)
        {
            if (_mRecordFile != null)
            {

                float h = _mZoomDurationSec * 0.5F;
                float duration = _mRecordFile.mGetSamplesTotalTime();

                _mZoomCenterSec = ACenterSec;

                if (_mZoomCenterSec < h)
                {
                    _mZoomCenterSec = h;
                }
                else if (_mZoomCenterSec > duration - h)
                {
                    _mZoomCenterSec = duration - h;
                }
            }
        }

        private void mSetChannelsCenter(float ACenterSec)
        {
            if (_mRecordFile != null)
            {

                float h = _mChannelsDurationSec * 0.5F;
                float duration = _mRecordFile.mGetSamplesTotalTime();

                _mChannelsCenterSec = ACenterSec;

                if (_mChannelsCenterSec < h)
                {
                    _mChannelsCenterSec = h;
                }
                else if (_mChannelsCenterSec > duration - h)
                {
                    _mChannelsCenterSec = duration - h;
                }

            }
        }

        public string mFirstWord(string ALine)
        {
            string s = "";
            int n = ALine == null ? 0 : ALine.Length;
            int l = 0;

            for (int i = 0; i < n; ++i)
            {
                char c = ALine[i];
                if (char.IsLetterOrDigit(c))
                {
                    s += c;
                    ++l;
                }
                else if (l > 0)
                {
                    break;
                }
            }
            return s;
        }
        public string mOneLine(string ALine)
        {
            string s = "";
            int n = ALine == null ? 0 : ALine.Length;
            int l = 0;

            for (int i = 0; i < n; ++i)
            {
                char c = ALine[i];
                if (c >= ' ') // char.IsLetterOrDigit(c) || c == ':' || c == '=' || c)
                {
                    s += c;
                    ++l;
                }
                else if (l > 0)
                {
                    if (c == '\n')
                    {
                        s += "; ";
                    }
                    else
                    {
                        s += " ";
                    }
                }
            }
            return s;
        }

        private void mUpdateRythmButton()
        {
            mGetActiveAnalysis();
            if (_mRecAnalysis != null)
            {
                buttonFindingsRhythm.Text = _mRecAnalysis._mAnalysisRhythmSet.mbIsEmpty() ? " -- Select Rhythm --"
                                 : _mRecAnalysis._mAnalysisRhythmSet.mGetShowSet(" + ") + " Rhythms";
            }
        }

        private void mInitPatientInfo()
        {
            string s = "--Anonymized--";
            bool bPatient = _mPatientInfo != null && _mPatientInfo.mIndex_KEY > 0;
            int age = bPatient ? _mPatientInfo.mGetAgeYears() : (_mRecordDb == null ? -1 : _mRecordDb.mGetAgeYears());
            string ageGender = "\r\n";

            if (age >= 0) ageGender += age.ToString() + " years";

            if (bPatient && _mPatientInfo._mPatientGender_IX > 0)
            {
                ageGender += ", " + CPatientInfo.sGetGenderString((DGender)_mPatientInfo._mPatientGender_IX);

            }
            //                    labelPatIDHeader.Text = _mPatientInfo.mGetPatientSocIDText().ToUpper() + ":";

            if (checkBoxAnonymize.Checked)
            {
                s += ageGender;
                labelPatID.Text = "--An--";
            }
            else
            {
                s = bPatient ? _mPatientInfo.mGetFullName() : "!" + (_mRecordDb == null ? "" : _mRecordDb.mPatientTotalName.mDecrypt());
                s += ageGender;

                if (bPatient && _mPatientInfo._mRemarkLabel.Length > 0)
                {
                    s += "\r\n" + _mPatientInfo._mRemarkLabel;
                }
                labelPatID.Text = bPatient ? _mPatientInfo.mGetPatientSocIDValue(true)/* _mPatientID.mDecrypt()*/ : (_mRecordDb == null ? "" : _mRecordDb.mPatientID.mDecrypt()); // "12341234";
            }
            textBoxPatientRemark.Text = s;
        }

        public void mInitScreenData()
        {
            try
            {
                for (DRecState i = DRecState.Received; i < DRecState.NrRecStates; ++i)
                {
                    comboBoxState.Items.Add(CRecordMit.sGetRecStateUserString((UInt16)i));
                }

                if (_mRecordDb != null)
                {

                    bool bStudy = _mStudyInfo != null && _mStudyInfo.mIndex_KEY > 0;

                    _mOldState = _mRecordDb.mRecState;
                    comboBoxState.Text = CRecordMit.sGetRecStateUserString(_mOldState);

                    mInitPatientInfo();
                    labelStudy.Text = (bStudy ? _mStudyInfo.mIndex_KEY.ToString() : "") + " . " + _mRecordDb.mSeqNrInStudy.ToString();
                    labelStudyProcCodes.Text = ""; // bStudy ? mStudyInfo._mStudyProcCodeSet.mGetSet() : "";
                    string s = "";
                    _mAnalyzeSubTitle = _mRecordDb.mStudy_IX > 0 ? "S" + _mRecordDb.mStudy_IX.ToString() + "." + _mRecordDb.mSeqNrInStudy.ToString()
                                                : "R#" + _mRecordDb.mIndex_KEY.ToString() + " " + _mRecordDb.mDeviceID;

                    /*                    if( _mRecordDb._mStudyPermissions.mbIsNotEmpty())
                                        {
                                            string permissions = CDvtmsData.sGetStudyPermissionsBitLabels(_mRecordDb._mStudyPermissions, "+");

                                            _mAnalyzeSubTitle += " <" + permissions + "> ";

                                        }
                     */

                    if (bStudy)
                    {
                        if (CDvtmsData._sEnumListStudyProcedures != null)
                        {
                            s = CDvtmsData._sEnumListStudyProcedures.mFillString(_mStudyInfo._mStudyProcCodeSet, true, " ", true, "\r\n");
                        }
                        s += "\r\n" + _mStudyInfo._mStudyRemarkLabel;

                    }
                    textBoxStudyRemark.Text = s;

                    s = "";
                    if (bStudy)
                    {
                        s = _mStudyInfo.mGetLabelNoteMark();

                        if (s.Length > 0)
                        {
                            string t = _mStudyInfo.mGetRemarkAndNote();

                            new ToolTip().SetToolTip(labelStudyNote, t);
                        }

                    }
                    labelStudyNote.Text = s;


                    labelEventPriority.Text = "Normal"; // mRecordDb.mPriority;
                    labelEventLabel.Text = /*mFirstWord(*/_mRecordDb.mEventTypeString/*)*/;

                    string remark = mOneLine(_mRecordDb.mRecRemark.mDecrypt());

                    if (remark.StartsWith(_mRecordDb.mRecLabel))
                    {
                        textBoxEventRemark.Text = remark;
                    }
                    else
                    {
                        textBoxEventRemark.Text = _mRecordDb.mRecLabel + "\r\n" + remark;
                    }
                    /*
                    DateTime dt = _mRecordDb.mGetDeviceTime(_mRecordDb.mEventUTC);
                    string strEvent = CProgram.sDateTimeToString(dt) + " " + CProgram.sTimeZoneOffsetString((Int16)_mRecordDb.mTimeZoneOffsetMin, DTimeZoneShown.Long);
                    if (_mRecordFile != null)
                    {
                        strEvent += _mRecordFile.mGetTimeZoneMovedChar();

                        double stripSec = _mRecordFile.mRecDurationSec;
                        double secRel = (_mRecordDb.mEventUTC - _mRecordFile.mBaseUTC).TotalSeconds;// mBaseUTC is not in RecordDB
                        if (secRel > stripSec + 2 || secRel < -2)
                        {
                            strEvent += "Δ";

                            CProgram.sLogError("R" + _mRecordDb.mIndex_KEY.ToString() + " eUTC" + CProgram.sDateTimeToYMDHMS(_mRecordDb.mEventUTC ) 
                                + " " + CProgram.sTimeZoneOffsetString((Int16)_mRecordDb.mTimeZoneOffsetMin, DTimeZoneShown.Long)
                                +  " file " + CProgram.sDateTimeToYMDHMS(_mRecordFile.mBaseUTC) + " " + stripSec.ToString("0.0") + " Δt=" + secRel.ToString("0.0") + " " + _mRecordDb.mFileName
                                );
                        }
                    }
                    */
                    string strEvent = _mRecordDb.mGetEventTimeCheckedTimeZone(_mRecordFile, "openAnalyze");
                    labelEventTime.Text = strEvent;

                    //                    labelEventTime.Text = CProgram.sTimeToString(dt) + " " + CProgram.sDateToString(dt);

                    //                    labelRecorderNr.Text = mDeviceInfo != null ? mDeviceInfo._mDeviceSerialNr + " " + mDeviceInfo._mRemarkLabel : "!" + mRecordDb.mDeviceID;

                    contextMenuSelChannel.Items.Clear();
                    contextMenuAddChannel.Items.Clear();
                    for (UInt16 i = 0; i < _mNrSignals; ++i)
                    {
                        contextMenuSelChannel.Items.Add(_mRecordDb.mGetChannelName(i));
                        contextMenuAddChannel.Items.Add(_mRecordDb.mGetChannelName(i));
                    }
                }
                mGetActiveAnalysis();

                if (_mRecAnalysis != null)
                {
                    textBoxFindings.Text = _mRecAnalysis._mAnalysisRemark;
                    textBoxSymptoms.Text = _mRecAnalysis._mSymptomsRemark;
                    textBoxActivities.Text = _mRecAnalysis._mActivitiesRemark;
                    toolStripAnalysisNr.Text = _mRecAnalysis.mIndex_KEY.ToString();
                    mUpdateRythmButton();

                    mbAnSetActiveSet(_mRecAnalysis._mAnalysisClassSet);
                    /*                    if (CDvtmsData._sEnumListFindingsClass != null)
                                        {
                                            bool bChecked;
                                            string label;

                                            for (UInt16 iFindings = 0; iFindings < 6; ++iFindings)
                                            {
                                                CSqlEnumRow row = CDvtmsData._sEnumListFindingsClass.mGetEnumNode(iFindings);
                                                if (row == null)
                                                {
                                                    bChecked = false;
                                                    label = "";
                                                }
                                                else
                                                {
                                                    label = row._mCode;
                                                    bChecked = _mRecAnalysis._mAnalysisClassSet.mbContainsValue(row._mCode);
                                                }
                                                switch (iFindings)
                                                {
                                                    // only check if not equal to prevent set state action
                                                    // triggers a read values!!
                                                    case 0: radioButtonF1.Text = label; if (radioButtonF1.Checked != bChecked) radioButtonF1.Checked = bChecked; break; 
                                                    case 1: radioButtonF2.Text = label; if (radioButtonF2.Checked != bChecked) radioButtonF2.Checked = bChecked; break;
                                                    case 2: radioButtonF3.Text = label; if (radioButtonF3.Checked != bChecked) radioButtonF3.Checked = bChecked; break;
                                                    case 3: radioButtonF4.Text = label; if (radioButtonF4.Checked != bChecked) radioButtonF4.Checked = bChecked; break;
                                                    case 4: radioButtonF5.Text = label; if (radioButtonF5.Checked != bChecked) radioButtonF5.Checked = bChecked; break;
                                                    case 5: radioButtonF6.Text = label; if (radioButtonF6.Checked != bChecked) radioButtonF6.Checked = bChecked; break;
                                                }
                                            }
                                        }
                    */

                    _mPrevTechnician = CDvtmsData.sFindUserInitial(_mRecAnalysis._mTechnician_IX);
                    labelPrevTech.Text = _mPrevTechnician;

                }
                else
                {
                    textBoxFindings.Text = "";
                    textBoxSymptoms.Text = "";
                    textBoxActivities.Text = "";
                    toolStripAnalysisNr.Text = "";
                    mbAnSetActive("");

                }
                labelUser.Text = CProgram.sGetProgUserInitials();
                treeViewAutoMeasurements.Nodes.Clear();
                mSetDualClick(true);
                mUpdateStateOk();   //buttonSaveOk.Enabled = mNewState > 0 && mNewState != mOldState;
                toolStripBaseLine.Visible = _mBaseLineRecordDb != null;

                buttonSave.Enabled = false;

                bool bRecIndex = _mRecordIndex > 0;
                if (_mbReadOnly || false == bRecIndex)
                {
                    buttonSaveOk.Enabled = false;
                    buttonSaveOk.Visible = bRecIndex;
                    comboBoxState.Enabled = false;
                    comboBoxState.Visible = bRecIndex;
                    comboBoxState.BackColor = Color.LightBlue;
                    labelNextState.BackColor = Color.LightBlue;
                    labelNextState.Visible = bRecIndex;
                }
                if (_mbReadOnly)
                {
                    textBoxSymptoms.Enabled = false;
                    textBoxActivities.Enabled = false;
                    textBoxFindings.Enabled = false;
                    buttonSelectFindings.Enabled = false;
                    buttonFindingsRhythm.Enabled = false;
                    mbAnDisableButtons();
                }
                mUpdateButtonLPF();
                mUpdateButtonHPF();
                mUpdateButtonBBF();
                mUpdateButtonAvgClipReset();

                _mbInitScreenDone = true;
            }
            catch (Exception ex)
            {
                CProgram.sLogException("InitScreenData Analysis screen failed", ex);
            }
        }

        void mGetNewState()
        {
            string s = comboBoxState.Text;
            UInt16 state = 0;

            if (CRecordMit.sbParseStateUserString(out state, s))
            {
                mSetNewState(state);
            }
        }

        private void mUpdateStateOk()
        {
            bool b = _mbReadOnly == false && _mNewState > 0 && _mNewState != _mOldState || _mOldState == (int)DRecState.Analyzed;

            labelNextState.Text = b ? "Next State:" : "State:";

            if (_mNewState == (int)DRecState.QCd)
            {
                if (FormEventBoard._sbQCdEnabled)
                {
                    if (checkBoxQCD.Checked)
                    {
                        // ok 
                    }
                    else
                    {
                        b = false;
                    }
                }
                else
                {
                    b = false;
                }
            }

            if (_mRecordDb.mStudy_IX > 0 && _mRecAnalysis != null)
            {
                mGetActiveAnalysis();
                if (_mRecAnalysis._mAnalysisClassSet.mbIsEmpty() && _mNewState >= (int)DRecState.Analyzed)
                {
                    b = false;
                }
            }
            buttonSaveOk.Enabled = b;

            comboBoxState.Text = CRecordMit.sGetRecStateUserString(_mNewState);
        }

        void mSetNewState(UInt16 ANewState)
        {
            _mNewState = ANewState;

            mUpdateStateOk();
        }

        bool mbGetAnalysisValues()
        {
            bool bOk = false;

            mGetActiveAnalysis();

            if (_mRecAnalysis != null)
            {
                //bool bChecked
                mAnGetCurrentCodeSet(ref _mRecAnalysis._mAnalysisClassSet);

                /*                if(CDvtmsData._sEnumListFindingsClass != null )
                                {

                                    for (UInt16 iFindings = 0; iFindings < 6; ++iFindings)
                                    {
                                        CSqlEnumRow row = CDvtmsData._sEnumListFindingsClass.mGetEnumNode(iFindings);

                                        if (row != null)
                                        {
                                            bChecked = false;
                                            switch (iFindings)
                                            {
                                                case 0: bChecked = radioButtonF1.Checked; break;
                                                case 1: bChecked = radioButtonF2.Checked; break;
                                                case 2: bChecked = radioButtonF3.Checked; break;
                                                case 3: bChecked = radioButtonF4.Checked; break;
                                                case 4: bChecked = radioButtonF5.Checked; break;
                                                case 5: bChecked = radioButtonF6.Checked; break;

                                            }
                                            if (bChecked)
                                            {
                                                _mRecAnalysis._mAnalysisClassSet.mAddValue(row._mCode);
                                            }
                                        }
                                    }
                                }
                */
                _mRecAnalysis._mAnalysisRemark = textBoxFindings.Text;
                _mRecAnalysis._mSymptomsRemark = textBoxSymptoms.Text;
                _mRecAnalysis._mActivitiesRemark = textBoxActivities.Text;


                //* for now copy the same analysis to the strip
                foreach (CMeasureStrip ms in _mRecAnalysisFile._mStripList)
                {
                    ms._mRecAnalysis = _mRecAnalysis;
                }
                bOk = _mRecAnalysis._mAnalysisClassSet != null && _mRecAnalysis._mAnalysisClassSet.mbIsNotEmpty();
            }
            return bOk;
        }

        public void mUpdateStripChannels()
        {
            try
            {
                if (_mRecordDb != null && _mRecordFile != null && _mStripCharts != null && _mStripImages != null && _mStripPanels != null)
                {
                    //DateTime dt = mRecordDb.mGetDeviceTime(mRecordDb.mStartRecUTC);
                    //toolStripChannelStartTime.Text = CProgram.sTimeToString(dt) + "  " +  CProgram.sDateToString(dt);

                    //toolStripChannelDuration.Text = mRecordDb.mRecDurationSec.ToString("0.0") + " Sec";
                    //dt = dt.AddSeconds(mRecordDb.mRecDurationSec);
                    //toolStripChannelEndTime.Text = CProgram.sTimeToString(dt);

                    toolStripChannelAmplitute.Text = _mRecordFile.mGetShowAmplitudeString(_mChannelsUnitA);
                    toolStripChannelSpeed.Text = _mRecordFile.mGetShowSpeedString(_mChannelsUnitT);

                    //toolStripRecordNumber.Text = mRecordDb.mIndex_KEY.ToString();
                    //toolStripNrChannels.Text = "# " + mNrSignals.ToString();

                    float length = _mRecordFile.mGetSamplesTotalTime();

                    float start = _mChannelsCenterSec - _mChannelsDurationSec * 0.5F;
                    float end = _mChannelsCenterSec + _mChannelsDurationSec * 0.5F;

                    toolStripChannelTimeLeft.Enabled = start > 0.01;
                    toolStripChannelTimeRight.Enabled = length - end > 0.01;

                    _mChannelsAmplitudeRange = 0.0F;
                    _mChannelsAmplitudeMin = 1e6F;
                    _mChannelsAmplitudeMax = -1e6F;
                    float min, max, range;
                    for (UInt16 i = 0; i < _mNrSignals; ++i)
                    {
                        if (i < _cMaxStripChart && _mStripPanels[i] != null && _mStripCharts[i] != null)
                        {
                            if (_mRecordFile.mbGetAmplitudeRange(i, start, length, out min, out max, CRecordMit.sGetMaxAmplitudeRange()))
                            {
                                range = max - min;
                                if (range > _mChannelsAmplitudeRange) _mChannelsAmplitudeRange = range;
                                if (min < _mChannelsAmplitudeMin) _mChannelsAmplitudeMin = min;
                                if (max > _mChannelsAmplitudeMax) _mChannelsAmplitudeMax = max;
                            }
                        }
                    }
                    if (_mChannelsAmplitudeRange < 0.1)
                    {
                        _mChannelsAmplitudeRange = 0.1F;
                    }
                    else
                    {
                        _mChannelsAmplitudeRange *= 1.1f;    // draw litlebit smaller then top to top;
                    }

                    for (UInt16 i = 0; i < _mNrSignals; ++i)
                    {
                        if (i < _cMaxStripChart && _mStripPanels[i] != null && _mStripCharts[i] != null)
                        {
                            //                                            float minA = rec.mGetSamplesMinValue();
                            //                                            float maxA = rec.mGetSamplesMaxValue();
                            int width = _mStripPanels[i].Width;
                            int height = _mStripPanels[i].Height;

                            float zoomStart = -1;
                            float zoomDuration = 0.0F;

                            if (i == _mZoomChannel)
                            {
                                zoomStart = _mZoomCenterSec - _mZoomDurationSec * 0.5F;
                                float zoomEnd = _mZoomCenterSec + _mZoomDurationSec * 0.5F;

                                if (zoomStart <= end && zoomEnd >= start)
                                {
                                    if (zoomStart < start) //&& zoomEnd <= _mChannelsCenterSec + _mChannelsDurationSec * 0.5F)
                                    {
                                        zoomStart = start;
                                    }
                                    if (zoomEnd > end)
                                    {
                                        zoomEnd = end;
                                    }
                                    zoomDuration = zoomEnd - zoomStart;
                                }
                                else
                                {
                                    zoomStart = -1;
                                }
                            }
                            Image img = _mRecordFile.mCreateChartImage3FixedRangeA(i, _mStripCharts[i], width, height, _mChannelsAmplitudeRange,
                                zoomStart, zoomDuration, DChartDrawTo.Display);// mPrePlotFullUnitSec);

                            _mStripImages[i] = img;

                            // draw markers

                            mOverlayMeasurements(ref img, false, false, false, i, _mStripCharts[i], width, height, _mChannelsAmplitudeRange,
                                start, _mChannelsDurationSec, DChartDrawTo.Display);

                            _mStripPanels[i].BackgroundImage = img;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("updateStripChannels analysis screen failed", ex);
            }

        }

        public void mOverlayMeasurements(ref Image ArImage, bool AbDrawAllChannels, bool AbDrawLabel, 
                    bool AbDrawValue, ushort AChannelIndex, CStripChart AChart,
                    int AWidth, int AHeight, float ARangeA,
                    float ACursorStartT, float ACursorWidthSec, DChartDrawTo ADrawTo)
        {
            if (AWidth > 2 && AHeight > 2 && AChart != null && ArImage != null)
            {
                if (_mRecAnalysisFile._mStripList != null && _mRecordDb != null)
                {
                    try
                    {
                        using (Graphics graphics = Graphics.FromImage(ArImage))
                        {
                            float endT = ACursorStartT + ACursorWidthSec;
                            int topY = (int)(AHeight * 0.1);
                            int bottomY = AHeight - 1;
                            float fontSize = 3.0F * AHeight / 80.0F;
                            Font font = new Font("Verdana", fontSize);
                            int textH = (int)(font.GetHeight());
                            int charHalfWidth = (int)(font.SizeInPoints / 2);
                            int textY = bottomY - 6 - textH;
                            int w = ArImage.Width - 1;
                            int h = ArImage.Height - 1;

                            Pen penStripActive1 = new Pen(Color.Silver);
                            Pen penStripActive0 = new Pen(Color.MediumSlateBlue);

                            CMeasurePoint mpCursor = mGetSelectedMeasurePoint();

                            foreach (CMeasureStrip ms in _mRecAnalysisFile._mStripList)
                            {
                                if (ms != null) // draw strip
                                {
                                    if (AbDrawAllChannels || ms._mChannel == AChannelIndex)
                                    {
                                        int x1, x2;

                                        if( AChart.mbGetLineT(out x1, out x2, ms._mStartTimeSec, ms._mStartTimeSec + ms._mDurationSec))
                                        {
                                            int lineY = 1;

                                            Pen pen = ms._mbActive ? penStripActive1 : penStripActive0;

                                            graphics.DrawLine(pen, x1, lineY, x2, lineY);
                                            ++lineY;
                                            graphics.DrawLine(pen, x1, lineY, x2, lineY);
                                        }
/* old                                      
                                        Color color = ms._mbActive ? Color.Silver : Color.MediumSlateBlue;
                                        int lineY = 1;

                                        int x1 = AChart.mGetPixelT(ms._mStartTimeSec);
                                        int x2 = AChart.mGetPixelT(ms._mStartTimeSec + ms._mDurationSec);

                                        Pen pen = new Pen(color);

                                        graphics.DrawLine(pen, x1, lineY, x2, lineY);
                                        ++lineY;
                                        graphics.DrawLine(pen, x1, lineY, x2, lineY);
*/
                                    }
                                }

                                if (ms != null && ms._mbActive) // draw points
                                {
                                    foreach (CMeasurePoint mp in ms._mPointList)
                                    {
                                        if (mp._mbActive)
                                        {
                                            if (AbDrawAllChannels || mp._mChannel == AChannelIndex)
                                            {
                                                bool bDraw = mp._mPoint1T <= endT;
                                                Color color = Color.Black;
                                                string text = CMeasurePoint.sGetMeasure2TagCode((DMeasure2Tag)mp._mTagID);
                                                int value;
                                                int lineY = 2;

                                                if (bDraw)
                                                {
                                                    if (mp._mbDualPoint)
                                                    {
                                                        bDraw = mp._mPoint2T >= ACursorStartT;
                                                    }
                                                    switch ((DMeasure2Tag)mp._mTagID)
                                                    {
                                                        case DMeasure2Tag.RRtime:
                                                            lineY = 4;
                                                            color = Color.Blue;
                                                            if (AbDrawValue)
                                                            {
                                                                value = (int)(1000 * mp.mGetValue() + 0.4999);
                                                                if (value < 0) value = -value;
                                                                text += " " + value.ToString();
                                                            }
                                                            break;
                                                        case DMeasure2Tag.PRtime:
                                                            color = Color.Green;
                                                            break;
                                                        case DMeasure2Tag.QRStime:
                                                            color = Color.Black;
                                                            break;
                                                        case DMeasure2Tag.QTtime:
                                                            color = Color.Purple;
                                                            break;
                                                        case DMeasure2Tag.RRqtc:
                                                            lineY = 6;
                                                            color = Color.DarkBlue;
                                                            if (AbDrawValue)
                                                            {
                                                                value = (int)(1000 * mp.mGetValue() + 0.4999);
                                                                if (value < 0) value = -value;
                                                                text += " " + value.ToString();
                                                            }
                                                            break;
                                                        case DMeasure2Tag.Pamp:
                                                            bDraw = false;
                                                            color = Color.Blue;
                                                            break;
                                                        case DMeasure2Tag.Qamp:
                                                            bDraw = false;
                                                            color = Color.Blue;
                                                            break;
                                                        case DMeasure2Tag.Ramp:
                                                            bDraw = false;
                                                            break;
                                                        case DMeasure2Tag.Samp:
                                                            bDraw = false;
                                                            break;
                                                        case DMeasure2Tag.Tamp:
                                                            bDraw = false;
                                                            break;
                                                    }

                                                    if (bDraw)
                                                    {
                                                        int x1, x2;

                                                        if (AChart.mbGetLineT(out x1, out x2, mp._mPoint1T, mp._mPoint2T))
                                                        {
                                                            /*                                                        

                                                                                                                    int x1 = AChart.mGetPixelT(mp._mPoint1T);
                                                                                                                    int x2 = AChart.mGetPixelT(mp._mPoint2T);
                                                            */
                                                            Pen pen = new Pen(color);

                                                            if (mp == mpCursor)
                                                            {
                                                                text = "●" + text;
                                                            }
                                                            graphics.DrawLine(pen, x1, bottomY - lineY, x2, bottomY - lineY);
                                                            graphics.DrawLine(pen, x1, bottomY - lineY + 1, x2, bottomY - lineY + 1);

                                                            if (AbDrawLabel)
                                                            {
                                                                Brush brush = new SolidBrush(color);
                                                                int x = (x1 + x2) / 2 - text.Length * charHalfWidth;
                                                                if (mp == mpCursor)
                                                                {
                                                                    graphics.DrawLine(pen, x1, topY, x2, topY);
                                                                }
                                                                graphics.DrawLine(pen, x1, topY, x1, bottomY);
                                                                graphics.DrawLine(pen, x2, topY, x2, bottomY);
                                                                graphics.DrawString(text, font, brush, new PointF(x, textY));
                                                            }
                                                            else
                                                            {
                                                                if (mp == mpCursor)
                                                                {
                                                                    int y = textY;
                                                                    graphics.DrawLine(pen, x1, y, x1, bottomY);
                                                                    graphics.DrawLine(pen, x2, y, x2, bottomY);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            graphics.Dispose();
                        }
                    }
                    catch (Exception)
                    {
                    }
                }
            }
        }


        public void mInitStripChannels()
        {
            try
            {
                if (_mRecordDb != null && _mRecordFile != null && _mStripCharts != null && _mStripImages != null && _mStripPanels != null)
                {
                    DateTime dt = _mRecordFile.mBaseUTC == DateTime.MinValue
                                            ? _mRecordDb.mGetDeviceTime(_mRecordDb.mBaseUTC)
                                            : _mRecordFile.mGetDeviceTime(_mRecordFile.mBaseUTC);
                    _mChannelStartTime = dt;
                    toolStripChannelStartTimeDate.Text = CProgram.sTimeToString(dt) + "  " + CProgram.sDateToString(dt);

                    float durationSec = _mRecordFile.mGetSamplesTotalTime();
                    toolStripChannelDuration.Text = (durationSec < 100 ? durationSec.ToString("0.0") + " Sec"
                                    : CProgram.sPrintTimeSpan_dhmSec(durationSec, " ")) + " @ " + _mRecordFile.mSampleFrequency + " Sps";

                    float tStart = _mChannelsCenterSec - _mChannelsDurationSec * 0.5F;
                    if (tStart < 0)
                    {
                        tStart = 0.0F;
                        _mChannelsCenterSec = _mChannelsDurationSec * 0.5F;
                    }

                    string s = _mChannelsDurationSec + 0.1F >= durationSec ? "■" : "";
                    dt = dt.AddSeconds(tStart);
                    toolStripChannelsStart.Text = CProgram.sTimeToString(dt) + "  " + CProgram.sDateToString(dt);
                    toolStripChannelPeriod.Text = s + CProgram.sPrintTimeSpan_dhmSec(_mChannelsDurationSec, " ");// _mChannelsDurationSec.ToString("0.0") + " Sec";

                    dt = dt.AddSeconds(_mChannelsDurationSec);
                    toolStripChannelEndTime.Text = CProgram.sTimeToString(dt);


                    //toolStripChannelAmplitute.Text = mRecordFile.mGetShowAmplitudeString(mChannelsUnitA);
                    //toolStripChannelSpeed.Text = mRecordFile.mGetShowTimeString(mChannelsUnitT);

                    //                    toolStripRecordNumber.Text = mRecordDb.mIndex_KEY.ToString();
                    toolStripNrChannels.Text = /*"# " +*/ _mNrSignals.ToString();

                    float maxRange = CRecordMit.sGetMaxAmplitudeRange();

                    for (UInt16 i = 0; i < _mNrSignals; ++i)
                    {
                        if (i < _cMaxStripChart && _mStripPanels[i] != null && _mStripCharts[i] != null)
                        {
                            float length = _mRecordFile.mGetSamplesTotalTime();
                            //                                            float minA = rec.mGetSamplesMinValue();
                            //                                            float maxA = rec.mGetSamplesMaxValue();
                            int width = _mStripPanels[i].Width;
                            int height = _mStripPanels[i].Height;

                            float zoomStart = -1;
                            if (i == _mZoomChannel)
                            {
                                zoomStart = _mZoomCenterSec - _mZoomDurationSec * 0.5F;
                            }

                            _mRecordFile.mInitChart(i, _mStripCharts[i], tStart, _mChannelsDurationSec, _mChannelsUnitA, _mChannelsUnitT, maxRange);// mPrePlotFullUnitSec);
                        }
                    }
                    toolStripARangeN.Text = maxRange > 1e6 ? "Ξ∞" : "Ξ" + maxRange.ToString("0.0") + "mV";


                }
                mUpdateStripChannels();
                mInitChannelsScrollBar();
            }
            catch (Exception ex)
            {
                CProgram.sLogException("InitStripChannels analysis screen failed", ex);
            }

        }
        public void mInitChannelsScrollBar()
        {
            bool bEnabled = false;
            int pos = -1;

            if (_mRecordDb != null)
            {
                float durationSec = _mRecordFile.mGetSamplesTotalTime();

                if (_mChannelsDurationSec > durationSec)
                {
                    _mChannelsDurationSec = durationSec < 2 ? 2.0F : durationSec;
                }
                if (_mChannelsDurationSec < durationSec)
                {
                    // scrollbar setup

                    float dT = _mChannelsCenterSec - _mChannelsDurationSec * 0.5F;
                    float T = durationSec - _mChannelsDurationSec;

                    pos = 0;
                    bEnabled = true;
                    if (T >= 1.0 && dT >= 0)
                    {
                        pos = (int)(dT * 1000 / T);
                    }
                    mChangedChannelsScrollbar();
                }
            }
            if (bEnabled)
            {

                if (ScrollBarChannel.Enabled == false)
                {
                    ScrollBarChannel.Enabled = true;
                }
                if (ScrollBarChannel.Value != pos) ScrollBarChannel.Value = pos;
            }
            else
            {
                if (ScrollBarChannel.Enabled == true)
                {
                    ScrollBarChannel.Enabled = false;
                }
            }
        }

        public void mChangedChannelsScrollbar()
        {
            int pos = ScrollBarChannel.Value;
            {
                if (_mRecordDb != null)
                {
                    float durationSec = _mRecordFile.mGetSamplesTotalTime();

                    if (_mChannelsDurationSec < durationSec)
                    {
                        // scrollbar setup
                        float T = durationSec - _mChannelsDurationSec;
                        float max = durationSec - _mChannelsDurationSec * 0.5F;
                        float dT = pos * 0.001F * T;
                        float t = dT + _mChannelsDurationSec * 0.5F;

                        if (t > max) t = max;
                        if (t < _mChannelsDurationSec * 0.5F) t = _mChannelsDurationSec * 0.5F;
                        if (Math.Abs(_mChannelsCenterSec - t) > 0.01F)
                        {
                            _mChannelsCenterSec = t;
                            mInitStripChannels();
                        }
                    }
                    mUpdateGraphMarker();
                }
            }
        }

        public void mUpdateGraphMarker()
        {
            if (_mRecordFile != null)
            {
                float durationSec = _mRecordFile.mGetSamplesTotalTime();

                if (durationSec >= 1)
                {
                    float f = _mZoomCenterSec / durationSec;
                    int left = 19;  // 19-4
                    int right = 31; // 35 - 4
                    int w = panelGraphSeperator.Width - left - right;
                    int c = (int)(_mZoomDurationSec / durationSec * w);
                    if (c < 8) c = 8;
                    int x = (int)(left - c / 2 + f * w);
                    if (panelGraphMarker.Left != x) panelGraphMarker.Left = x;
                    if (panelGraphMarker.Width != c) panelGraphMarker.Width = c;
                }
            }
        }
        public void mUpdateZoomChannel()
        {
            if (_mRecordDb != null && _mRecordFile != null && _mZoomChart != null)
            {
                //                                            float minA = rec.mGetSamplesMinValue();
                //                                            float maxA = rec.mGetSamplesMaxValue();
                int width = panelZoomStrip.Width;
                int height = panelZoomStrip.Height;

                float h = _mZoomDurationSec * 0.5F;
                float startT = _mZoomCenterSec - h;
                if (startT < 0.0F) startT = 0.0F;

                float length = _mRecordFile.mGetSamplesTotalTime();

                float endT = startT + _mZoomDurationSec;

                toolStripWindowLeft.Enabled = startT > 0.01;
                toolStripWindowRight.Enabled = length - endT > 0.01;


                mResetMeasurement();
                Image img = _mRecordFile.mCreateChartImage2(_mZoomChannel, _mZoomChart, width, height, -1.0F, 0.0F, DChartDrawTo.Display,
                    true, DPLotAnnotations.All, DPLotAnnotations.All);// mPrePlotFullUnitSec);

                _mZoomImage = img;

                mOverlayMeasurements(ref img, false, true, true, _mZoomChannel, _mZoomChart, width, height, -1, startT, _mZoomDurationSec, DChartDrawTo.Display);

                panelZoomStrip.BackgroundImage = img;

                /* only image belonging to stored time frame must be stored!!
                should be saved!!!                CMeasureStrip ms = mGetSelectedMeasureStrip();

                 if( ms != null && ms._mStripImage )
                {
                    ms._mStripImage = _mZoomImage;
                }
*/

                DateTime dt = _mRecordDb.mGetDeviceTime(_mRecordFile.mBaseUTC).AddSeconds(startT);
                toolStripZoomStartTime.Text = CProgram.sTimeToString(dt) + "  " + CProgram.sDateToString(dt);

                toolStripZoomDuration.Text = _mZoomDurationSec < 100 ? _mZoomDurationSec.ToString("0") + " Sec"
                                                : CProgram.sPrintTimeSpan_dhmSec(_mZoomDurationSec, " ");

                dt = dt.AddSeconds(_mZoomDurationSec);
                toolStripZoomEndTime.Text = CProgram.sTimeToString(dt);

                toolStripZoomAmplitude.Text = _mRecordFile.mGetShowAmplitudeString(_mZoomUnitA);
                toolStripZoomSpeed.Text = _mRecordFile.mGetShowSpeedString(_mZoomUnitT);

                CheckZoomOnChannels();

                //                toolStripZoomChannel.Text = mRecordDb.mGetChannelName(mZoomChannel);
                UInt32 invMask = _mRecAnalysisFile != null ? _mRecAnalysisFile._mInvChannelsMask : (_mRecordDb == null ? 0 : _mRecordDb.mInvertChannelsMask);
                bool bInverted = (invMask & (1 << _mZoomChannel)) != 0;
                toolStripButtonInvert.Text = bInverted ? "-Ʊ Inv" : "+Ω Inv";
                mUpdateGraphMarker();
            }
        }
        public void mInitZoomChannel()
        {
            try
            {
                if (_mRecordDb != null && _mRecordFile != null && _mZoomChart != null)
                {
                    //                                            float minA = rec.mGetSamplesMinValue();
                    //                                            float maxA = rec.mGetSamplesMaxValue();
                    float h = _mZoomDurationSec * 0.5F;
                    float startT = _mZoomCenterSec - h;
                    if (startT < 0.0F) startT = 0.0F;

                    float maxRange = CRecordMit.sGetMaxAmplitudeRange();

                    _mRecordFile.mInitChart(_mZoomChannel, _mZoomChart, startT, _mZoomDurationSec, _mZoomUnitA, _mZoomUnitT, maxRange);

                    toolStripARange1.Text = maxRange > 1e6 ? "Ξ∞" : "Ξ" + maxRange.ToString("0.0") + "mV";



                    //                DateTime dt = mRecordDb.mGetDeviceTime(mRecordDb.mStartRecUTC).AddSeconds(startT);
                    //                toolStripZoomStartTime.Text = CProgram.sTimeToString(dt) + "  " + CProgram.sDateToString(dt);

                    //                toolStripZoomDuration.Text = mZoomDurationSec.ToString("0.0") + " Sec";
                    //                dt = dt.AddSeconds(mZoomDurationSec);
                    //                toolStripZoomEndTime.Text = CProgram.sTimeToString(dt);

                    //toolStripZoomAmplitude.Text = mRecordFile.mGetShowAmplitudeString(mZoomUnitA);
                    //toolStripZoomSpeed.Text = mRecordFile.mGetShowTimeString(mZoomUnitT);

                    toolStripZoomChannel.Text = _mRecordDb.mGetChannelName(_mZoomChannel);
                    toolStripZoomChannel.ToolTipText = "Channel " + (_mZoomChannel + 1).ToString();
                    mUpdateZoomChannel();
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("InitZoomChannel Analysis screen failed", ex);
            }

        }

        public void mInitScreens()
        {
            try
            {
                if (_mRecordDb != null)
                {
                    CProgram.sLogLine("Init Analysis form " + _mAnalysisSeqNr.ToString() + " / " + _mRecordDb.mNrAnalysis.ToString()
                        + " for: R#" + _mRecordDb.mIndex_KEY.ToString() + " Study #" + _mRecordDb.mStudy_IX.ToString()
                        + (_mStudyInfo != null ? (_mStudyInfo.mIndex_KEY != _mRecordDb.mStudy_IX ? "!=S" : "=S") : "?S")
                        + " SeqNr = " + _mRecordDb.mSeqNrInStudy.ToString() + " / " + (_mStudyInfo != null ? _mStudyInfo._mNrRecords.ToString() : "0")
                        + " Patient # " + _mRecordDb.mPatient_IX.ToString() + (_mPatientInfo != null ? "+P" : "?P")
                        + " for deviceID: " + _mRecordDb.mDeviceID + (_mDeviceInfo != null ? "+D" : "?P"));
                }
                //                _mChannelsDurationSec = _mRecordDb != null ? _mRecordDb.mRecDurationSec : 1.0F;
                //               if(_mChannelsDurationSec < 2.0 && _mRecordFile != null)
                {
                    if (_mRecordFile != null)
                    {
                        float totalSec = _mRecordFile.mGetSamplesTotalTime();

                        if (_mChannelsDurationSec > totalSec)
                        {
                            _mChannelsDurationSec = totalSec;
                        }
                        if (_sChannelsDurationSec == 0)
                        {
                            if (totalSec < 240)
                            {
                                _mChannelsDurationSec = totalSec;
                            }
                        }
                    }
                }
                if (_mChannelsDurationSec <= 1.0F) _mChannelsDurationSec = 1.0F;
                //                if (_mChannelsDurationSec > 120.0F) _mChannelsDurationSec = 120.0F;
                _mChannelsCenterSec = _mChannelsDurationSec * 0.5F;
                if (_mRecAnalysisFile != null)
                {
                    CMeasureStrip ms = _mRecAnalysisFile.mFindActiveStrip(0);

                    if (ms != null)
                    {
                        mSetStripIndex(_mRecAnalysisFile.mGetStripIndex(ms));
                        _mZoomChannel = ms._mChannel;
                        _mZoomCenterSec = ms._mStartTimeSec + ms._mDurationSec * 0.5F;
                        _mZoomDurationSec = ms._mDurationSec;
                        if (_mZoomDurationSec < 1) _mZoomDurationSec = 6;
                    }
                }
                //                StripChn.LayoutStyle = ToolStripLayoutStyle.HorizontalStackWithOverflow;
                ScrollBarChannel.Maximum = 1000;
                ScrollBarChannel.Minimum = 0;
                ScrollBarChannel.Value = 0;
                mInitChannelsScrollBar();

                mInitScreenData();
                mInitStripChannels();
                mInitZoomChannel();
                mInitStrips();
                mUpdateStripList(false);
                mInitHrAmplitudeGraph();
                mCheckSympthomsActions();
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Init Analysis screen failed", ex);
            }
        }

        public void mInitStrips()
        {
        }

        public void mUpdateScreenChannels()
        {
            /*            // plot event  strip
                        t0 = ARec.mGetEventSec() - mPrePlotEventBeforeSec;
                        if (t0 < 0.0F) t0 = 0.0F;
                        float t1 = t0 + mPrePlotEventBeforeSec + mPrePlotEventAfterSec;
                        if (t1 > ARec.mGetSamplesTotalTime()) t1 = ARec.mGetSamplesTotalTime();

                        length = t1 - t0;
                        width = mPrePlotEventWidth;
                        height = mPrePlotEventHeight;

                        img = ARec.mCreateChartImage(ASignalIndex, t0, length, width, height, mPrePlotEventUnitSec);
            */
        }

        public bool mbFindBaseLine()
        {
            bool bOk = false;
            CSqlCmd cmd = null;

            try
            {
                if (_mRecordIndex != 0 && _mDbConnection != null && _mRecordDb != null && _mRecordDb.mRecState != (UInt16)DRecState.BaseLine)
                {
                    CRecordMit recordDb = new CRecordMit(_mRecordDb.mGetSqlConnection());

                    mGetActiveAnalysis();
                    if (_mRecordDb != null && recordDb.mStudy_IX > 0 // only find baseline in study!!
                        && _mRecordFile != null && _mRecAnalysis != null && _mRecAnalysisFile != null)
                    {
                        cmd = recordDb.mCreateSqlCmd(_mDbConnection);

                        if (cmd == null)
                        {
                            CProgram.sLogLine("Failed to init cmd");
                        }
                        else
                        {
                            //                            recordDb.mPatientID = _mRecordDb.mPatientID.mCreateCopy();
                            recordDb.mRecState = (UInt16)DRecState.BaseLine;
                            recordDb.mStudy_IX = _mRecordDb.mStudy_IX;
                            UInt64 flags = CSqlDataTableRow.sGetMask((UInt16)DRecordMitVars.RecState_IX);

                            flags |= CSqlDataTableRow.sGetMask((UInt16)DRecordMitVars.Study_IX);
                            //                            flags |= _mRecordDb.mStudy_IX == 0 ? CSqlDataTableRow.sGetMask((UInt16)DRecordMitVars.PatientID_ES)
                            //                                                            : CSqlDataTableRow.sGetMask((UInt16)DRecordMitVars.Study_IX);

                            if (cmd.mbDoSelectCmd(flags, true, _mRecordDb.mMaskAll))
                            {
                                bool bError;

                                if (cmd.mbReadOneRow(out bError))
                                {
                                    _mBaseLineRecordDb = recordDb;

                                    bOk = true;
                                }

                            }
                        }
                        cmd.mCloseCmd();
                    }
                }
            }

            catch (Exception ex)
            {
                CProgram.sLogException("Failed load base line record for record # " + _mRecordIndex, ex);
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.mCloseCmd();
                }
            }
            return bOk;
        }

        public bool mbLoadBaseLine()
        {
            bool bOk = false;

            if (_mBaseLineRecordDb != null)
            {

                _mBaseLineRecAnalysis = new CRecAnalysis(CDvtmsData.sGetDBaseConnection());
                _mBaseLineRecAnalysisFile = new CRecAnalysisFile();

                if (_mBaseLineRecAnalysis != null && _mBaseLineRecAnalysisFile != null)
                {
                    UInt32 recordIndex = _mBaseLineRecordDb.mIndex_KEY;
                    UInt16 analysisNr = 0;
                    string casePath;
                    string filePath;


                    if (_mBaseLineRecordDb.mbGetCasePath(out casePath))
                    {
                        string recordDir = Path.Combine(_mRecordingDir, casePath);
                        filePath = Path.Combine(recordDir, _mBaseLineRecordDb.mFileName);

                        _mBaseLineRecordFile = FormAnalyze.sImportDataFiles(CDvtmsData.sGetDBaseConnection(), filePath, _mBaseLineRecordDb.mInvertChannelsMask,
                            _mBaseLineRecordDb.mReceivedUTC, _mBaseLineRecordDb.mTimeZoneOffsetMin, DMoveTimeZone.Move, (DImportConverter)_mBaseLineRecordDb.mImportConverter,
                            (DImportMethod)_mBaseLineRecordDb.mImportMethod);

                        if (_mBaseLineRecordFile != null)
                        {
                            string fullName = Path.Combine(recordDir, "Analysis-R" + recordIndex.ToString() + "-A" + _mBaseLineRecordDb.mNrAnalysis.ToString() + ".ini");

                            if (false == _mBaseLineRecAnalysisFile.mbLoadMeasureFile(fullName, recordIndex, analysisNr, _mBaseLineRecordDb.mInvertChannelsMask, ref _mBaseLineRecordFile))    // load last analysis of baseline
                            {
                                fullName = Path.Combine(recordDir, "Analysis-R" + recordIndex.ToString() + "-A0" + ".ini"); // old used 0
                                _mBaseLineRecAnalysisFile.mbLoadMeasureFile(fullName, recordIndex, analysisNr, _mBaseLineRecordDb.mInvertChannelsMask, ref _mBaseLineRecordFile);
                            }
                            else
                            {

                            }

                            List<CSqlDataTableRow> analysisList;
                            _mBaseLineRecAnalysis = sLoadAnalysisList(recordIndex, out analysisList);

                            sAssignAnalysisToStrips(ref _mBaseLineRecAnalysisFile, analysisList);

                            bOk = true;
                        }
                    }
                }
            }
            return bOk;
        }

        public void mUpdateTitle(bool AbNew, bool AbAnonimize)
        {
            if (_mRecordDb != null)
            {
                if (_mRecordIndex > 0)
                {
                    //                DateTime dt = CProgram.sUtcToLocal(_mRecordDb.mReceivedUTC);
                    DateTime dt = _mRecordDb.mGetDeviceTime(_mRecordDb.mEventUTC);
                    string aStr = _mRecAnalysis == null ? "-" : _mRecAnalysis._mSeqInRecording.ToString();
                    string rStr = _mDeviceInfo != null ? _mDeviceInfo._mDeviceSerialNr + " " + _mDeviceInfo._mRemarkLabel : "!" + _mRecordDb.mDeviceID;
                    string sStr = _mRecordDb.mStudy_IX == 0 ? "" : " S" + _mRecordDb.mStudy_IX.ToString() + "." + _mRecordDb.mSeqNrInStudy.ToString() + "> ";
                    string permStr = "";
                    string tStr = "";
                    string cStr = "";

                    if (_mRecordDb._mStudyTrial_IX > 0)
                    {
                        tStr = " " + CTrialInfo.sGetTrialLabel(_mRecordDb._mStudyTrial_IX, true, "");
                        // add trial label
                    }
                    if (_mRecordDb._mStudyClient_IX > 0)
                    {
                        cStr = " " + CClientInfo.sGetClientLabel(_mRecordDb._mStudyClient_IX, true, "");
                        // add trial label
                    }
                    if (_mRecordDb._mStudyPermissions.mbIsNotEmpty())
                    {
                        permStr = " <" + CDvtmsData.sGetStudyPermissionsBitLabels(_mRecordDb._mStudyPermissions, "+") + ">";
                    }

                    string title = "event "// + CProgram.sDateTimeToString(dt) + " " + CProgram.sTimeZoneOffsetString((Int16)_mRecordDb.mTimeZoneOffsetMin, DTimeZoneShown.Long)
                        + sStr
                        + (AbAnonimize ? "" : " " + _mRecordDb.mPatientTotalName.mDecrypt())
                        + " { " + rStr + " R#" + _mRecordDb.mIndex_KEY.ToString()
                        + (_mbReadOnly ? "-Read Only-" : "")
                        + permStr
                        + tStr
                        + cStr
                        /*+ ", " + aStr + " / " + mRecordDb.mNrAnalysis.ToString()
                        + " Analysis " + (AbNew ? "*" : "") + mAnalysisIndex*/ + " }";

                    Text = CProgram.sMakeProgTitle(title, true, true);
                }
                else
                {
                    string title = "View file S" + _mRecordDb.mStudy_IX.ToString() + " " + Path.GetFileNameWithoutExtension(_mRecordFilePath)

                        + (AbAnonimize ? "" : " " + _mRecordDb.mPatientTotalName.mDecrypt())
                        + (_mbReadOnly ? "-Read Only-" : "");

                    Text = CProgram.sMakeProgTitle(title, true, true);
                }
            }
        }
        public bool mbInitCharts(bool AbNew)
        {
            bool bOk = false;

            _mNrSignals = _mRecordFile.mNrSignals;
            if (_mNrSignals > 0)
            {
                mUpdateTitle(AbNew, checkBoxAnonymize.Checked);

                _mZoomChart = new CStripChart();
                _mStripCharts = new CStripChart[_mNrSignals];
                _mStripImages = new Image[_mNrSignals];
                _mStripPanels = new Panel[_cMaxStripChart];

                if (_mZoomChart != null && _mStripCharts != null && _mStripImages != null && _mStripPanels != null)
                {
                    // set zoom chart defaults
                    int imgHeight = panelChannel1.Height;
                    int offHeight = 2;

                    mSetZoomCenter(_mRecordDb.mGetEventSec());
                    ///          _mZoomDurationSec = 6.0F;

                    bOk = true;
                    for (UInt16 i = 0; i < _cMaxStripChart; ++i)
                    {
                        switch (i)
                        {
                            case 0: _mStripPanels[i] = panelChannel1; break;
                            case 1: _mStripPanels[i] = panelChannel2; break;
                            case 2: _mStripPanels[i] = panelChannel3; break;
                            case 3: _mStripPanels[i] = panelChannel4; break;
                            case 4: _mStripPanels[i] = panelChannel5; break;
                            case 5: _mStripPanels[i] = panelChannel6; break;
                            case 6: _mStripPanels[i] = panelChannel7; break;
                            case 7: _mStripPanels[i] = panelChannel8; break;
                            case 8: _mStripPanels[i] = panelChannel9; break;
                            case 9: _mStripPanels[i] = panelChannel10; break;
                            case 10: _mStripPanels[i] = panelChannel11; break;
                            case 11: _mStripPanels[i] = panelChannel12; break;
                            default:
                                _mStripPanels[i] = null;
                                break;
                        }
                        if (_mStripPanels[i] != null)
                        {
                            if (i < _mNrSignals)
                            {
                                _mStripCharts[i] = new CStripChart();
                                if (_mStripCharts[i] == null) { bOk = false; break; }

                                if (_mStripPanels[i].Height != imgHeight) _mStripPanels[i].Height = imgHeight;
                                _mStripPanels[i].Enabled = true;
                                // set chart defaults
                                /*                                _mStripPanels[i].Visible = true;
                                                                if (i < 6)
                                                                {
                                                                    splitContainerChannels.Panel1.Controls.SetChildIndex(_mStripPanels[i], 6-i);
                                                                }
                                                                else
                                                                {
                                                                    splitContainerChannels.Panel2.Controls.SetChildIndex(_mStripPanels[i], 12-i);
                                                                }
                                                            */
                            }
                            else
                            {
                                _mStripPanels[i].BackgroundImage = null;    // make panel small and disabled for not visible
                                _mStripPanels[i].Height = offHeight;
                                _mStripPanels[i].Enabled = false;
                                //                                _mStripPanels[i].Visible = false; changes order
                            }
                        }
                    }
                    int hTotalStrips = _mNrSignals;
                    if (_mNrSignals <= 6)
                    {
                        splitContainerChannels.Panel2Collapsed = true;
                    }
                    else
                    {
                        hTotalStrips = 6;
                        splitContainerChannels.Panel2Collapsed = false;
                    }
                    hTotalStrips *= panelChannel1.Height;
                    hTotalStrips += 4;
                    splitContainerChannels.Height = hTotalStrips;
                    //panelTopChannels.Height = hTotalStrips;
                    //panelChnls.Height = hTotalStrips;
                }
            }
            return bOk;
        }

        private void mSetRecordIndex(bool AbReadOnly, UInt32 ARecordIndex, bool AbNew, UInt32 AAnalysisIndex, UInt16 AAnalysisNr, CSqlDBaseConnection ADbConnection, string ARecordingDir)
        {
            _mbReadOnly = AbReadOnly;
            _mRecordIndex = ARecordIndex;
            _mbNew = AbNew;
            _mAnalysisIndex = AAnalysisIndex;
            _mAnalysisSeqNr = AAnalysisNr;
            _mDbConnection = ADbConnection;
            _mRecordingDir = ARecordingDir;
            _mAnalysisIndex = AbNew ?  /*from record */AAnalysisIndex + 1 : AAnalysisIndex;
            toolStripBaseLine.Visible = false;

            if (false == AbReadOnly)
            {
                CProgram.sPromptError(true, "Analyze", "Read only Record expected");
            }
            //mbLockRecordFile( ARecordIndex, )
            mUpdateTitle(AbNew, checkBoxAnonymize.Checked);
        }

        public static CRecordMit sImportScpFiles(CSqlDBaseConnection ADbConnection, String AFilePath, UInt32 ASetInvChannels, DateTime AReceivedUTC, Int16 ADefaultTimeZoneMin, DMoveTimeZone AMoveTimeZone,
            DImportMethod AImportMethod, bool AbApplyTimeZoneCor, bool AbLogResult
            )
        {
            bool bOk = false;
            CRecordMit rec = null;
            string fileName = Path.GetFileName(AFilePath);
            string path = Path.GetDirectoryName(AFilePath);

            CProgram.sLogLine("Importing TZ SCP file " + fileName + "...");

            try
            {
                DateTime startTime = DateTime.Now;
                DateTime sampleUTC = DateTime.MinValue;

                double dTDataRead = 0.0, dTPlot = 0.0;

                //        if( fileName.EndsWith(".tze"))
                //               {
                rec = CScpReader.sReadAllScp(path, ASetInvChannels, ADbConnection, ADefaultTimeZoneMin, ref sampleUTC, null, 0, AbApplyTimeZoneCor, AbLogResult);
                /* always read all scp files                }
                                else
                                {
                                    rec = CScpReader.sReadOneScp(AFilePath, ADbConnection, ADefaultTimeZoneMin, DImportConverter.TZAeraScp);
                                }
                */
                TimeSpan ts = DateTime.Now - startTime;
                dTDataRead = ts.TotalSeconds;

                if (rec != null)
                {
                    rec.mReceivedUTC = AReceivedUTC;           // received time = read time
                    rec.mFileName = fileName;
                    //rec.mStoredAt = StoreName;
                    rec.mImportMethod = (UInt16)AImportMethod;            // Import method enum
                    rec.mImportConverter = (UInt16)DImportConverter.TZAeraScp;

                    rec.mSignalLabels = "";

                    if (rec.mDeviceModel == null || rec.mDeviceModel.Length == 0)
                    {
                        rec.mDeviceModel = "{SCP}";
                    }
                    if (rec.mRecLabel == null || rec.mRecLabel.Length == 0)
                    {
                        if (rec.mRecRemark != null && rec.mRecRemark.mbNotEmpty())
                        {
                            rec.mRecLabel = CProgram.sGetFirstLine(rec.mRecRemark.mDecrypt());
                        }
                    }
                    bOk = true;
                    if (DMoveTimeZone.Move == AMoveTimeZone)
                    {
                        rec.mbMoveTimeZone(ADefaultTimeZoneMin);
                    }
                }
            }
            catch (Exception e)
            {
                CProgram.sLogException("Failed load TZ Scp file", e);
            }
            if (!bOk)
            {
                rec = null;
            }
            return rec;
        }

        public static CRecordMit sImportDV2ScpFile(CSqlDBaseConnection ADbConnection, String AFilePath, UInt32 ASetInvChannels, DateTime AReceivedUTC, Int16 ADefaultTimeZoneMin, DMoveTimeZone AMoveTimeZone,
            DImportMethod AImportMethod, bool AbApplyTimeZoneCor, bool AbLogResult)
        {
            bool bOk = false;
            CRecordMit rec = null;
            string fileName = Path.GetFileName(AFilePath);
            string path = Path.GetDirectoryName(AFilePath);

            CProgram.sLogLine("Importing DV2 SCP file " + fileName + "...");

            try
            {
                DateTime startTime = DateTime.Now;
                double dTDataRead = 0.0, dTPlot = 0.0;


                rec = CScpReader.sReadOneScp(AFilePath, ASetInvChannels, ADbConnection, ADefaultTimeZoneMin, DImportConverter.TZAeraScp, AbApplyTimeZoneCor, AbLogResult);

                TimeSpan ts = DateTime.Now - startTime;
                dTDataRead = ts.TotalSeconds;

                if (rec != null)
                {
                    rec.mReceivedUTC = AReceivedUTC;           // received time = read time
                    rec.mFileName = fileName;
                    //rec.mStoredAt = StoreName;
                    rec.mImportMethod = (UInt16)AImportMethod;            // Import method enum
                    rec.mImportConverter = (UInt16)DImportConverter.DV2Scp;

                    rec.mSignalLabels = "";

                    if (rec.mDeviceModel == null || rec.mDeviceModel.Length == 0)
                    {
                        rec.mDeviceModel = "{DV2 SCP}";
                    }
                    if (rec.mRecLabel == null || rec.mRecLabel.Length == 0)
                    {
                        if (rec.mRecRemark != null && rec.mRecRemark.mbNotEmpty())
                        {
                            //                            rec.mRecLabel = CProgram.sGetFirstLine(rec.mRecRemark.mDecrypt());
                        }
                    }
                    bOk = true;
                    if (DMoveTimeZone.Move == AMoveTimeZone)
                    {
                        rec.mbMoveTimeZone(ADefaultTimeZoneMin);
                    }

                }
            }
            catch (Exception e)
            {
                CProgram.sLogException("Failed load DV2  file", e);

            }
            if (!bOk)
            {
                rec = null;
            }

            return rec;
        }
        /*
                public static CRecordMit sImportScpFilesOld(CSqlDBaseConnection ADbConnection, String AFilePath, DateTime AReceivedUTC, DImportMethod AImportMethod, 
                    Int16 ADefaultTimeZoneMin, DMoveTimeZone AMoveTimeZone, bool AbApplyTimeZoneCor, bool AbLogResult)
                {
                    bool bOk = false;
                    CRecordMit rec = null;
                    string fileName = Path.GetFileName(AFilePath);
                    CProgram.sLogLine("Importing SCP file " + fileName + "...");

                    try
                    {
                        DateTime startTime = DateTime.Now;
                        double dTDataRead = 0.0, dTPlot = 0.0;

                        CScpReader scp = new CScpReader();

                        if (scp != null)
                        {
                            bOk = scp.mbReadFile(AFilePath, false, ADefaultTimeZoneMin, AbApplyTimeZoneCor, AbLogResult);

                            if (bOk)
                            {
                                TimeSpan ts = DateTime.Now - startTime;
                                dTDataRead = ts.TotalSeconds;

                                rec = new CRecordMit(ADbConnection);

                                if (rec != null)
                                {
                                    if (rec.mbCreateSignals(scp.mNrSignals)
                                        && rec.mbCreateAllSignalData(scp.mSignals[0].mNrValues))
                                    {
                                        bOk = true;
                                        for (ushort j = 0; j < scp.mNrSignals; ++j)
                                        {
                                            bOk &= rec.mSignals[j].mbCopyFrom(scp.mSignals[j]);
                                        }
                                        if (bOk == false)
                                        {
                                            rec = null;
                                        }
                                    }
                                    else
                                    {
                                        rec = null;
                                    }
                                }
                                bOk = rec != null;
                                if (bOk)
                                {
                                    DateTime utc = scp.mMeasureUTC;

                                    if (utc == DateTime.MinValue)
                                    {
                                        utc = AReceivedUTC;
                                    }
                                    // convert Csp to recordMIT
                                    rec.mSampleFrequency = (uint)scp.mSampleFrequency;
                                    rec.mSampleUnitT = scp.mTimeUnit;             // 1 / mFrequency
                                    rec.mSampleUnitA = scp.mSignalUnit;            // 1 / mAdcGain
                                    rec.mNrSamples = scp.mNrSamples;                // nr sample values in the file
                                                                                    //private CAnnotation mAnnotationAtr;     // annotation file

                                    // From Header in dBase 
                                    rec.mReceivedUTC = AReceivedUTC;           // received time = read time
                                    rec.mFileName = fileName;
                                    //rec.mStoredAt = StoreName;
                                    rec.mBaseUTC = utc;                // start time of strip (functions are given in sec. from this point
                                    rec.mEventUTC = utc;              // Event time
                                    rec.mEventTimeSec = 0.0F;            // event time relative to base
                                    rec.mImportConverter = (UInt16)DImportConverter.TZAeraScp;         // import converter enum
                                    rec.mImportMethod = (UInt16)AImportMethod;            // Import method enum

                                    rec.mSignalLabels = "";

                                    // JSON file    using public for easy acces without get set functions
                                    rec.mDeviceID = scp.mDeviceID;
                                    rec.mPatientID.mbEncrypt(scp.mPatientID);

                                    string totalName = CRecordMit.sCombineNames(scp.mPatientLastName, "", scp.mPatientFirstName);

                                    rec.mPatientTotalName.mbEncrypt(totalName);
                                    //rec.mPatientBirthDate;
                                    //rec.mPhysicianName;
                                    //rec.mPatientComment;
                                    //rec.mEventTypeString;
                                    rec.mStartRecUTC = utc;
                                    rec.mRecDurationSec = scp.mSignals[0].mNrValues * scp.mTimeUnit;
                                    //rec.mTransmitTimeUTC;
                                    //rec.mTransmitTimeLocal;
                                    // rec.mPatientBirthYear;
                                    rec.mRecState = (UInt16)DRecState.Received;

                                    if (rec.mDeviceModel == null || rec.mDeviceModel.Length == 0)
                                    {
                                        rec.mDeviceModel = "{SCP}";
                                    }
                                    if (rec.mRecLabel == null || rec.mRecLabel.Length == 0)
                                    {
                                        if (rec.mRecRemark != null && rec.mRecRemark.mbNotEmpty())
                                        {
                                            rec.mRecLabel = CProgram.sGetFirstLine(rec.mRecRemark.mDecrypt());
                                        }
                                    }
        //                            rec.mCheckManualName(AManualName);
                                }
                                if (DMoveTimeZone.Move == AMoveTimeZone)
                                {
                                    rec.mbMoveTimeZone(ADefaultTimeZoneMin);
                                }

                            }
                        }
                    }
                    catch (Exception e)
                    {
                        CProgram.sLogException("Failed load Intricon file", e);

                    }
                    if (!bOk)
                    {
                        rec = null;
                    }
                    return rec;
                }
        */
        public static CRecordMit sImportDataFiles(CSqlDBaseConnection ADbConnection, string AFilePath, UInt32 ASetInvChannels,
            DateTime AReceivedUTC, Int32 ADefaultTimeZoneMin, DMoveTimeZone AMoveTimeZone,
            DImportConverter AImportConverter, DImportMethod AImportMethod)
        // use record time zone to load from file if no time zone is present
        {
            CRecordMit rec = null;
            CDvxEvtRec dvxRec = null;
            string result;

            switch (AImportConverter)
            {
                case DImportConverter.IntriconSironaZip:      // Import of zip file: unpack encrypted zip => import file (HEA, DAT, .ANN)

                case DImportConverter.IntriconSironaMit:      // Import from directory selecting .HEA file using (DAT and ANN)
                    rec = new CRecordMit(ADbConnection);

                    if (rec != null)
                    {
                        rec.mbReadFile(AFilePath, ASetInvChannels, AReceivedUTC, ADefaultTimeZoneMin, AMoveTimeZone, false, false, AImportConverter, AImportMethod, "");
                    }
                    break;
                case DImportConverter.TZAeraScp:               // Import from directory selecting scp
                    rec = FormAnalyze.sImportScpFiles(ADbConnection, AFilePath, ASetInvChannels, AReceivedUTC, (Int16)ADefaultTimeZoneMin, AMoveTimeZone, AImportMethod,
                                        false, CLicKeyDev.sbDeviceIsProgrammer()); // TZ has UTC date and time with time zone
                    break;
                case DImportConverter.DV2Scp:
                    rec = FormAnalyze.sImportDV2ScpFile(ADbConnection, AFilePath, ASetInvChannels, AReceivedUTC, (Int16)ADefaultTimeZoneMin, AMoveTimeZone, AImportMethod,
                                        true, CLicKeyDev.sbDeviceIsProgrammer()); // DV2 has local date time and no Timezone
                    break;
                case DImportConverter.SironaHttp:
                    rec = new CRecordMit(ADbConnection);

                    if (rec != null)
                    {
                        rec.mbReadFile(AFilePath, ASetInvChannels, AReceivedUTC, ADefaultTimeZoneMin, AMoveTimeZone, false, false, AImportConverter, AImportMethod, "");
                    }
                    break;
                case DImportConverter.DVX1:

                    if (CDvxEvtRec.sbLoadEcgData(out result, out dvxRec, out rec, AFilePath, (Int16)ADefaultTimeZoneMin, AMoveTimeZone))
                    {
                        if (rec != null)
                        {
                            rec.mModifyInvertMask(ASetInvChannels);
                        }
                    }
                    break;
                case DImportConverter.DvxSpO:

                    if (CDvxPleth.sbLoadPlethData(out result, /*out dvxRec, */out rec, AFilePath, (Int16)ADefaultTimeZoneMin, AMoveTimeZone))
                    {
                        if (rec != null)
                        {
                            rec.mModifyInvertMask(ASetInvChannels);
                        }
                    }
                    break;
            }

            return rec;
        }

        private bool mbLockRecordFile(UInt32 ARecordIndex, string ARecordPath)
        {
            bool bLocked = false;

            if (false == _mbReadOnly && _sbTriageLockAnRec)
            {
                _mLockName = CRecordMit.sGetLockName();
                _mLockFilePath = ARecordPath;

                bLocked = CProgram.sbLockFilePath(_mLockFilePath, _mLockName, ref _mLockStream);
                if (false == bLocked)
                {
                    _mbReadOnly = true;
                    mUpdateTitle(false, checkBoxAnonymize.Checked);
                }
            }
            return bLocked;
        }

        public bool mbLoadRecord2(bool AbReadOnly, UInt32 ARecordIndex, bool AbNew, UInt32 AAnalysisIndex, UInt16 AAnalysisNr, CSqlDBaseConnection ADbConnection, string ARecordingDir)
        {
            bool bOk = false;
            string showError = "";

            _mbReadOnly = AbReadOnly;
            _mRecordIndex = ARecordIndex;
            _mbNew = AbNew;
            _mAnalysisIndex = AAnalysisIndex;
            _mAnalysisSeqNr = AbNew ? (UInt16)(AAnalysisNr + 1) : AAnalysisNr;
            _mDbConnection = ADbConnection;
            _mRecordingDir = ARecordingDir;

            _mLoadFormStartDT = DateTime.Now;
            double dbSec = 0;
            double importSec = 0;
            double lockSec = 0;
            double chartSec = 0;
            double devStPtSec = 0;
            double ldAnSec = 0;
            double dbAnSec = 0;
            double blSec = 0;
            DateTime dt = DateTime.Now;

            try
            {
                if (ARecordIndex != 0 && _mDbConnection != null)
                {
                    _mRecordDb = new CRecordMit(_mDbConnection);

                    _mRecAnalysis = new CRecAnalysis(_mDbConnection);
                    _mRecAnalysisFile = new CRecAnalysisFile();

                    if (_mRecordDb != null && _mRecAnalysis != null && _mRecAnalysisFile != null)
                    {
                        if (false == _mRecordDb.mbDoSqlSelectIndex(ARecordIndex, _mRecordDb.mGetValidMask(true)))
                        {
                            CProgram.sLogLine("Failed to collect Record #" + ARecordIndex.ToString());
                        }
                        else
                        {
                            string casePath;

                            dbSec = (DateTime.Now - dt).TotalSeconds;
                            dt = DateTime.Now;
                            //string filePath;
                            if (_mRecordDb.mbGetCasePath(out casePath))
                            {
                                _mRecordDir = Path.Combine(_mRecordingDir, casePath);
                                _mRecordFilePath = Path.Combine(_mRecordDir, _mRecordDb.mFileName);

                                mbLockRecordFile(ARecordIndex, _mRecordDir);

                                lockSec = (DateTime.Now - dt).TotalSeconds;

                                dt = DateTime.Now;


                                _mRecordFile = FormAnalyze.sImportDataFiles(CDvtmsData.sGetDBaseConnection(), _mRecordFilePath, _mRecordDb.mInvertChannelsMask,
                                        _mRecordDb.mReceivedUTC, _mRecordDb.mTimeZoneOffsetMin, DMoveTimeZone.Move, // use record time zone to load from file
                                        (DImportConverter)_mRecordDb.mImportConverter, (DImportMethod)_mRecordDb.mImportMethod);

                                if (_mRecordFile == null)
                                {
                                    CProgram.sPromptError(false, "Read record " + _mRecordDb.mIndex_KEY.ToString()
                                        + " data File", "Failed " + CRecordMit.sGetConverterTag((DImportConverter)_mRecordDb.mImportConverter)
                                        + " import on file");
                                }
                                else if (_mRecordFile.mGetNrSamples() == 0)
                                {
                                    CProgram.sPromptError(false, "Read record " + _mRecordDb.mIndex_KEY.ToString()
                                        + " data File", "Read empty  " + CRecordMit.sGetConverterTag((DImportConverter)_mRecordDb.mImportConverter)
                                        + " file");

                                }
                                else
                                {
                                    importSec = (DateTime.Now - dt).TotalSeconds;
                                    dt = DateTime.Now;

                                    if (_mRecordFile.mDeviceID == null || _mRecordFile.mDeviceID.Length == 0)
                                    {
                                        _mRecordFile.mDeviceID = _mRecordDb.mDeviceID;
                                    }
                                    if (_mRecordFile.mDeviceModel == null || _mRecordFile.mDeviceModel.Length == 0)
                                    {
                                        _mRecordFile.mDeviceModel = _mRecordDb.mDeviceModel;
                                    }
                                    if (_mRecordFile.mEventTypeString == null || _mRecordFile.mEventTypeString.Length == 0)
                                    {
                                        _mRecordFile.mEventTypeString = _mRecordDb.mEventTypeString;
                                    }
                                    if (_mRecordFile.mEventUTC == DateTime.MinValue)
                                    {
                                        _mRecordFile.mbSetEventUTC(_mRecordDb.mEventUTC);
                                    }
                                    if (_mRecordFile.mBaseUTC != DateTime.MinValue)
                                    {
                                        double difSec = (_mRecordFile.mBaseUTC - _mRecordDb.mBaseUTC).TotalSeconds;
                                        if (difSec < -1 || difSec > 1)
                                        {
                                            // should not happen?? if timeZone is not read from json then timezone should be from record
                                            CProgram.sPromptError(true, "Analyze R#" + _mRecordDb.mIndex_KEY.ToString(), "Base UTC not equal");
                                            _mRecordDb.mTimeZoneOffsetMin = _mRecordFile.mTimeZoneOffsetMin;
                                            _mRecordDb.mBaseUTC = _mRecordFile.mBaseUTC;
                                        }
                                    }
                                    if (_mRecordDb.mTimeZoneOffsetMin != _mRecordFile.mTimeZoneOffsetMin)
                                    {
                                        CProgram.sPromptError(true, "Analyze", "Time zone different!");
                                        _mRecordDb.mTimeZoneOffsetMin = _mRecordFile.mTimeZoneOffsetMin;
                                    }
                                    if (_mRecordFile.mEventUTC == DateTime.MinValue && _mRecordFile.mEventTimeSec > 0.0)
                                    {
                                        _mRecordFile.mbSetEventSec(_mRecordFile.mEventTimeSec);
                                    }
                                    if (_mRecordDb.mEventUTC != DateTime.MinValue && _mRecordFile.mEventUTC != _mRecordDb.mEventUTC)
                                    {
                                        double sec = 999999.99;
                                        if (_mRecordFile.mEventUTC != DateTime.MinValue)
                                        {
                                            TimeSpan ts = _mRecordDb.mEventUTC - _mRecordFile.mEventUTC;
                                            sec = ts.TotalSeconds;
                                            if (sec < 0) sec = -sec;
                                        }
                                        if (sec >= 1)
                                        {
                                            _mRecordFile.mbSetEventUTC(_mRecordDb.mEventUTC);
                                        }
                                    }
                                    bOk = mbInitCharts(AbNew);

                                    chartSec = (DateTime.Now - dt).TotalSeconds;
                                    dt = DateTime.Now;

                                    if (bOk)
                                    {
                                        //mRecAnalysisFile.mbLoad
                                        string fullName = Path.Combine(_mRecordDir, mMakeAnalisisFileName());

                                        _mRecAnalysisFile.mbLoadMeasureFile(fullName, _mRecordIndex, _mAnalysisSeqNr, _mRecordDb.mInvertChannelsMask, ref _mRecordFile);
                                        ldAnSec = (DateTime.Now - dt).TotalSeconds;
                                        dt = DateTime.Now;

                                        List<CSqlDataTableRow> analysisList;
                                        _mRecAnalysis = sLoadAnalysisList(ARecordIndex, out analysisList);

                                        sAssignAnalysisToStrips(ref _mRecAnalysisFile, analysisList);
                                        dbAnSec = (DateTime.Now - dt).TotalSeconds;
                                        dt = DateTime.Now;
                                    }
                                }
                            }
                        }

                        if (bOk)
                        {
                            _mDeviceInfo = new CDeviceInfo(_mDbConnection);

                            if (_mDeviceInfo != null)
                            {
                                //                                _mDeviceInfo.mbLoadCheckFromRecord(false, _mRecordDb, true, ref _mStudyInfo, ref _mPatientInfo, false, false);
                                _mDeviceInfo.mbLoadCheckFromRecordEB(ref showError, _mRecordDb, true, ref _mStudyInfo, ref _mPatientInfo, false);
                            }
                            devStPtSec = (DateTime.Now - dt).TotalSeconds;
                            dt = DateTime.Now;

                            // check and load RecAnalysis

                            /*
                            if (_mAnalysisIndex > 0)
                            {
                                if (false == _mRecAnalysis.mbDoSqlSelectIndex(_mAnalysisIndex, _mRecAnalysis.mMaskValid))
                                {

                                }
                            }
                            else if (_mAnalysisSeqNr > 0)
                            {
                                List<CSqlDataTableRow> analysisList;
                                bool bError;
                                UInt64 searchFlags = CSqlDataTableRow.sGetMask((UInt16)DRecAnalysisVars.Recording_IX)
                                    | CSqlDataTableRow.sGetMask((UInt16)DRecAnalysisVars.SeqInRecording);

                                _mRecAnalysis._mRecording_IX = _mRecordDb.mIndex_KEY;
                                _mRecAnalysis._mSeqInRecording = _mAnalysisSeqNr;

                                if (_mRecAnalysis.mbDoSqlSelectList(out bError, out analysisList, _mRecAnalysis.mMaskValid, searchFlags, true, ""))
                                {
                                    if( analysisList != null && analysisList.Count > 0 )
                                    {
                                        _mRecAnalysis.mbCopyFrom(analysisList[0]);
                                    }
                                }
                            }
                            */
                            bool blFound = mbFindBaseLine();

                            toolStripBaseLine.Visible = blFound;
                            blSec = (DateTime.Now - dt).TotalSeconds;
                            dt = DateTime.Now;
                        }
                    }
                }
                mUpdateTitle(AbNew, checkBoxAnonymize.Checked);

            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed load record for record # " + ARecordIndex, ex);
            }
            finally
            {
            }
            _mLoadFormEndDT = DateTime.Now;
            _mLoadFormSec = (_mLoadFormEndDT - _mLoadFormStartDT).TotalSeconds;
            labelLoadSec.Text = "l=" + _mLoadFormSec.ToString("0.000") + "sec";

            if (_mLoadFormSec >= (CLicKeyDev.sbDeviceIsProgrammer() ? 2 : 4))
            {
                string s = "Load R" + ARecordIndex.ToString() + " in " + _mLoadFormSec.ToString("0.00")
                    + "sec, db=" + dbSec.ToString("0.00")
                    + ", imp=" + importSec.ToString("0.00")
                    + ", lock=" + lockSec.ToString("0.00")
                    + ", chart=" + chartSec.ToString("0.00")
                    + ", devStPt=" + devStPtSec.ToString("0.00")
                    + ", ldAn=" + ldAnSec.ToString("0.00")
                    + ", dbAn=" + dbAnSec.ToString("0.00")
                    + ", bl=" + blSec.ToString("0.00");

                CProgram.sLogLine(s);
            }
            return bOk;
        }

        public string mMakeAnalisisFileName()
        {
            string fileName = _mRecordIndex == 0
                ? "Analysis-" + Path.GetFileNameWithoutExtension(_mRecordFilePath) + ".ini"
                : "Analysis-R" + _mRecordIndex.ToString() + "-A" + _mAnalysisSeqNr.ToString() + ".ini";
            return fileName;
        }

        public bool mbShowRecord(bool AbReadOnly, CRecordMit ARecord, CSqlDBaseConnection ADbConnection, string ARecordDir, string AFileName)
        {
            bool bOk = false;
            string showError = "";

            _mbReadOnly = AbReadOnly;
            _mRecordIndex = ARecord.mIndex_KEY;
            _mbNew = false;
            _mAnalysisIndex = 0;
            _mAnalysisSeqNr = 0;
            _mDbConnection = ADbConnection;
            _mRecordingDir = ARecordDir;

            _mLoadFormStartDT = DateTime.Now;
            double dbSec = 0;
            double importSec = 0;
            double lockSec = 0;
            double chartSec = 0;
            double devStPtSec = 0;
            double ldAnSec = 0;
            double dbAnSec = 0;
            double blSec = 0;
            DateTime dt = DateTime.Now;

            try
            {
                if (ARecord != null && _mDbConnection != null)
                {
                    _mRecordDb = new CRecordMit(ADbConnection);
                    _mRecordFile = ARecord;
                    ARecord.mbCopyTo(_mRecordDb);

                    _mRecAnalysis = new CRecAnalysis(_mDbConnection);
                    _mRecAnalysisFile = new CRecAnalysisFile();

                    if (_mRecordDb != null && _mRecAnalysis != null && _mRecAnalysisFile != null)
                    {
                        _mRecordDir = ARecordDir;
                        _mRecordFilePath = Path.Combine(_mRecordDir, AFileName);

                        //                                mbLockRecordFile(ARecordIndex, _mRecordDir);

                        lockSec = (DateTime.Now - dt).TotalSeconds;

                        dt = DateTime.Now;

                        importSec = (DateTime.Now - dt).TotalSeconds;
                        dt = DateTime.Now;

                        if (_mRecordFile.mDeviceID == null || _mRecordFile.mDeviceID.Length == 0)
                        {
                            _mRecordFile.mDeviceID = _mRecordDb.mDeviceID;
                        }
                        if (_mRecordFile.mDeviceModel == null || _mRecordFile.mDeviceModel.Length == 0)
                        {
                            _mRecordFile.mDeviceModel = _mRecordDb.mDeviceModel;
                        }
                        if (_mRecordFile.mEventTypeString == null || _mRecordFile.mEventTypeString.Length == 0)
                        {
                            _mRecordFile.mEventTypeString = _mRecordDb.mEventTypeString;
                        }
                        if (_mRecordFile.mEventUTC == DateTime.MinValue)
                        {
                            _mRecordFile.mbSetEventUTC(_mRecordDb.mEventUTC);
                        }
                        if (_mRecordFile.mBaseUTC != DateTime.MinValue)
                        {
                            double difSec = (_mRecordFile.mBaseUTC - _mRecordDb.mBaseUTC).TotalSeconds;
                            if (difSec < -1 || difSec > 1)
                            {
                                // should not happen?? if timeZone is not read from json then timezone should be from record
                                CProgram.sPromptError(true, "Analyze R#" + _mRecordDb.mIndex_KEY.ToString(), "Base UTC not equal");
                                _mRecordDb.mTimeZoneOffsetMin = _mRecordFile.mTimeZoneOffsetMin;
                                _mRecordDb.mBaseUTC = _mRecordFile.mBaseUTC;
                            }
                        }
                        if (_mRecordDb.mTimeZoneOffsetMin != _mRecordFile.mTimeZoneOffsetMin)
                        {
                            CProgram.sPromptError(true, "Analyze", "Time zone different!");
                            _mRecordDb.mTimeZoneOffsetMin = _mRecordFile.mTimeZoneOffsetMin;
                        }
                        if (_mRecordFile.mEventUTC == DateTime.MinValue && _mRecordFile.mEventTimeSec > 0.0)
                        {
                            _mRecordFile.mbSetEventSec(_mRecordFile.mEventTimeSec);
                        }
                        if (_mRecordDb.mEventUTC != DateTime.MinValue && _mRecordFile.mEventUTC != _mRecordDb.mEventUTC)
                        {
                            double sec = 999999.99;
                            if (_mRecordFile.mEventUTC != DateTime.MinValue)
                            {
                                TimeSpan ts = _mRecordDb.mEventUTC - _mRecordFile.mEventUTC;
                                sec = ts.TotalSeconds;
                                if (sec < 0) sec = -sec;
                            }
                            if (sec >= 1)
                            {
                                _mRecordFile.mbSetEventUTC(_mRecordDb.mEventUTC);
                            }
                        }
                        bOk = mbInitCharts(false);

                        chartSec = (DateTime.Now - dt).TotalSeconds;
                        dt = DateTime.Now;

                        if (bOk)
                        {
                            //mRecAnalysisFile.mbLoad
                            string fullName = Path.Combine(_mRecordDir, mMakeAnalisisFileName());

                            _mRecAnalysisFile.mbLoadMeasureFile(fullName, _mRecordIndex, _mAnalysisSeqNr, _mRecordDb.mInvertChannelsMask, ref _mRecordFile);
                            ldAnSec = (DateTime.Now - dt).TotalSeconds;
                            dt = DateTime.Now;

                            dbAnSec = (DateTime.Now - dt).TotalSeconds;
                            dt = DateTime.Now;
                        }
                    }

                    if (bOk)
                    {
                        _mDeviceInfo = new CDeviceInfo(_mDbConnection);

                        if (_mDeviceInfo != null)
                        {
                            //                                _mDeviceInfo.mbLoadCheckFromRecord(false, _mRecordDb, true, ref _mStudyInfo, ref _mPatientInfo, false, false);
                            _mDeviceInfo.mbLoadCheckFromRecordEB(ref showError, _mRecordDb, true, ref _mStudyInfo, ref _mPatientInfo, false);
                        }
                        devStPtSec = (DateTime.Now - dt).TotalSeconds;
                        dt = DateTime.Now;

                        toolStripBaseLine.Visible = false;
                        blSec = (DateTime.Now - dt).TotalSeconds;
                        dt = DateTime.Now;
                    }
                }

                mUpdateTitle(false, checkBoxAnonymize.Checked);

            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed show record for " + AFileName, ex);
            }
            finally
            {
            }
            _mLoadFormEndDT = DateTime.Now;
            _mLoadFormSec = (_mLoadFormEndDT - _mLoadFormStartDT).TotalSeconds;
            labelLoadSec.Text = "l=" + _mLoadFormSec.ToString("0.000") + "sec";

            return bOk;
        }

        private void mCheckSympthomsActions()
        {
            string symptoms = textBoxSymptoms.Text;
            string activities = textBoxActivities.Text;

            if ((symptoms == null || symptoms.Length == 0)
                && (activities == null || activities.Length == 0)
                    && (_mOldState == (UInt16)DRecState.Received)
                    && _mRecordDb != null
                    )
            {
                string remark = _mRecordDb.mRecRemark.mDecrypt();
                /*
                                int posS = remark.IndexOf("S=");
                                if (posS >= 0)
                                {
                                    symptoms = mExtractLabel(remark, posS + 2);
                                }
                                int posA = remark.IndexOf("A=");
                                if (posA >= 0)
                                {
                                    activities = mExtractLabel(remark, posA + 2);
                                }
                                textBoxSymptoms.Text = symptoms;
                                textBoxActivities.Text = activities;
                */
                textBoxSymptoms.Text = CProgram.sExtractValueLabel(remark, "S");
                textBoxActivities.Text = CProgram.sExtractValueLabel(remark, "A");

            }
        }


        public static CRecAnalysis sLoadAnalysisList(UInt32 ARecordIndex, out List<CSqlDataTableRow> ArAnalysisList)
        {
            CRecAnalysis firstAnalysis = null;
            List<CSqlDataTableRow> analysisList = null;

            try
            {
                CRecAnalysis analysis = new CRecAnalysis(CDvtmsData.sGetDBaseConnection());

                if (analysis != null)
                {
                    bool bError;
                    UInt64 searchFlags = CSqlDataTableRow.sGetMask((UInt16)DRecAnalysisVars.Recording_IX);

                    //                        | CSqlDataTableRow.sGetMask((UInt16)DRecAnalysisVars.SeqInRecording);

                    analysis._mRecording_IX = ARecordIndex;
                    //                    _mRecAnalysis._mSeqInRecording = _mAnalysisSeqNr;

                    if (analysis.mbDoSqlSelectList(out bError, out analysisList, analysis.mMaskValid, searchFlags, false, ""))
                    {
                        if (analysisList != null && analysisList.Count > 0)
                        {
                            firstAnalysis = analysisList[0] as CRecAnalysis;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed get Analysis list for record # " + ARecordIndex, ex);
            }
            ArAnalysisList = firstAnalysis == null ? null : analysisList;
            return firstAnalysis;
        }

        public static void sAssignAnalysisToStrips(ref CRecAnalysisFile ARecAnalysisFile, List<CSqlDataTableRow> ArAnalysisList)
        {
            int nList = ArAnalysisList != null ? ArAnalysisList.Count : 0;

            if (nList > 0)
            {
                CRecAnalysis firstAnalysis = null;
                int i = 0;
                int firstSeq = int.MaxValue;

                foreach (CRecAnalysis analysis in ArAnalysisList)
                {
                    if (analysis._mSeqInRecording < firstSeq)
                    {
                        firstAnalysis = analysis;    // find first analysis for re strip 0
                    }
                }

                foreach (CMeasureStrip ms in ARecAnalysisFile._mStripList)
                {
                    ms._mRecAnalysis = firstAnalysis;   // setto first in case the analysis is not found

                    foreach (CRecAnalysis analysis in ArAnalysisList)
                    {
                        if (analysis._mSeqInRecording == i)
                        {
                            ms._mRecAnalysis = analysis;    // find correct analysis;
                            break;
                        }
                    }
                    ++i;
                }
            }
        }


        void mSetDualClick(bool AbDual)
        {
            _mClickState = DMeasureClickState.Off;
            _mbClickDualPoint = AbDual;

            if (AbDual)
            {
                if (toolStripOneClick.Checked) toolStripOneClick.Checked = false;
                if (false == toolStripTwoClick.Checked) toolStripTwoClick.Checked = true;
            }
            else
            {
                if (false == toolStripOneClick.Checked) toolStripOneClick.Checked = true;
                if (toolStripTwoClick.Checked) toolStripTwoClick.Checked = false;
            }
            mResetMeasurement();
        }

        void mDrawMeasureLine(bool AbOn)
        {
            if (_mbMeasureLineDrawn != AbOn)
            {
                ControlPaint.DrawReversibleLine(_mPoint1, _mPoint2, Color.White);
                _mbMeasureLineDrawn = AbOn;
            }
        }
        void mResetMeasurement()
        {
            if (_mClickState != DMeasureClickState.Off && _mClickState != DMeasureClickState.Up2)
            {
                mStatusLine("");
            }
            _mClickState = DMeasureClickState.Off;
            mDrawMeasureLine(false);
        }

        private string mGetIndexStr(float ATime, float AAmpl)
        {
            string s = "[";

            if (_mRecordFile != null)
            {
                int t = (int)(ATime * _mRecordFile.mSampleFrequency + 0.5);
                s += t.ToString();

                int a = _mRecordFile.mSampleUnitA < 0.00001 ? 0 : (int)(AAmpl / _mRecordFile.mSampleUnitA);
                s += ", " + a.ToString();
            }
            return s + "]";
        }

        void mMeasureMouseMove(int AX, int AY, bool AbLeftDown)
        {
            bool bLine = false;
            bool bMsg = _mbMeasureLineDrawn;
            string msg = "";

            mDrawMeasureLine(false);
            if (AX > 0 && AX < panelZoomStrip.Width && AY > 0 && AY < panelZoomStrip.Height)
            {
                Point point = panelZoomStrip.PointToScreen(new Point(AX, AY));
                float t = _mZoomChart.mGetValueT(AX);
                float a = _mZoomChart.mGetValueA(AY);
                string channel = mGetChannelInfo();

                DateTime dt = _mChannelStartTime.AddSeconds(t);

                if (AbLeftDown)
                {
                    msg = "_";
                }
                msg += "(" + AX.ToString() + ", " + AY.ToString() + ") ";
                msg += channel + "  ";

                switch (_mClickState)
                {
                    case DMeasureClickState.Off:
                        msg += "0 ";
                        break;
                    case DMeasureClickState.Down1:
                        msg += "1_ ";
                        msg += "Point1= " + CProgram.sTimeToString(dt) + " " + t.ToString("0.0000") + " Sec, " + a.ToString("0.0000") + " mV";
                        if (bShowIndexValues)
                        {
                            msg += " p1" + mGetIndexStr(t, a);
                        }
                        break;
                    case DMeasureClickState.Up1:
                        {
                            double dT = (t - _mPoint1T);
                            double dA = (a - _mPoint1A);
                            double hr = Math.Abs(dT);
                            string hrStr = hr < 0.001 ? "" : " (hr=" + (60 / hr).ToString("0.0") + " bpm)";
                            msg += "1^ ";

                            msg += "Point2: " + CProgram.sTimeToString(dt) + " " + t.ToString("0.0000") + " Sec, " + a.ToString("0.0000") + " mV => dT= "
                                + dT.ToString("0.000") + " Sec" + hrStr + ", dA= " + dA.ToString("0.0000") + " mV";
                            _mPoint2 = point;
                            bLine = true;
                            if (bShowIndexValues)
                            {
                                msg += " p2" + mGetIndexStr(t, a) + " dP" + mGetIndexStr((float)dT, (float)dA);
                            }
                        }
                        break;
                    case DMeasureClickState.Down2:
                        {
                            double dT = (t - _mPoint1T);
                            double dA = (a - _mPoint1A);
                            double hr = Math.Abs(dT);
                            string hrStr = hr < 0.001 ? "" : " (hr=" + (60 / hr).ToString("0.0") + " bpm)";
                            msg += "2_ ";
                            msg += "Point2= " + CProgram.sTimeToString(dt) + " " + t.ToString("0.0000") + " Sec, " + a.ToString("0.0000") + " mV => dT= "
                                + dT.ToString("0.0000") + " Sec" + hrStr + ", dA= " + dA.ToString("0.0000") + " mV";
                            _mPoint2 = point;
                            bLine = true;
                            if (bShowIndexValues)
                            {
                                msg += " p2" + mGetIndexStr(t, a) + " dP" + mGetIndexStr((float)dT, (float)dA);
                            }
                        }
                        break;
                    case DMeasureClickState.Up2:
                        msg = "";
                        //                        msg += "2^ ";
                        break;
                }

            }
            if (bMsg || msg.Length > 0)
            {
                mStatusLine(msg);
            }
            if (bLine)
            {
                mDrawMeasureLine(true);
            }
        }
        private void mDrawVerticalLine(int AX, int AY)
        {
            int w = panelZoomStrip.Width;
            int h = panelZoomStrip.Height;
            Color color = Color.FromArgb(255, 255, 0); //Green;

            /*            ControlPaint.DrawReversibleLine(panelZoomStrip.PointToScreen(new Point(0, AY)), panelZoomStrip.PointToScreen(new Point(w-1, AY)), color);
                        if (AY < h - 1)
                        {
                            ControlPaint.DrawReversibleLine(panelZoomStrip.PointToScreen(new Point(0, AY+1)), panelZoomStrip.PointToScreen(new Point(w - 1, AY+1)), color);
                        }
            */
            ControlPaint.DrawReversibleLine(panelZoomStrip.PointToScreen(new Point(AX, 0)), panelZoomStrip.PointToScreen(new Point(AX, h - 1)), color);
            if (AX < w - 1)
            {
                ControlPaint.DrawReversibleLine(panelZoomStrip.PointToScreen(new Point(AX + 1, 0)), panelZoomStrip.PointToScreen(new Point(AX + 1, h - 1)), color);
            }
            if (AX > 0)
            {
                ControlPaint.DrawReversibleLine(panelZoomStrip.PointToScreen(new Point(AX - 1, 0)), panelZoomStrip.PointToScreen(new Point(AX - 1, h - 1)), color);
            }

            /*            int w = panelZoomStrip.Width;
                        int h = panelZoomStrip.Height;

                        Graphics g = panelZoomStrip.CreateGraphics();
                        Pen pen = new Pen(Color.DarkBlue);

                        g.DrawLine(pen, 0, AY, w - 1, AY);
                        g.DrawLine(pen, AX, 0, AX, h-1);
            */
        }

        private void mDrawCrossLine(int AX, int AY)
        {
            int w = panelZoomStrip.Width;
            int h = panelZoomStrip.Height;
            Color color = Color.Green;

            ControlPaint.DrawReversibleLine(panelZoomStrip.PointToScreen(new Point(0, AY)), panelZoomStrip.PointToScreen(new Point(w - 1, AY)), color);
            if (AY < h - 1)
            {
                ControlPaint.DrawReversibleLine(panelZoomStrip.PointToScreen(new Point(0, AY + 1)), panelZoomStrip.PointToScreen(new Point(w - 1, AY + 1)), color);
            }
            ControlPaint.DrawReversibleLine(panelZoomStrip.PointToScreen(new Point(AX, 0)), panelZoomStrip.PointToScreen(new Point(AX, h - 1)), color);
            if (AX < w - 1)
            {
                ControlPaint.DrawReversibleLine(panelZoomStrip.PointToScreen(new Point(AX + 1, 0)), panelZoomStrip.PointToScreen(new Point(AX + 1, h - 1)), color);
            }

            /*            int w = panelZoomStrip.Width;
                        int h = panelZoomStrip.Height;

                        Graphics g = panelZoomStrip.CreateGraphics();
                        Pen pen = new Pen(Color.DarkBlue);

                        g.DrawLine(pen, 0, AY, w - 1, AY);
                        g.DrawLine(pen, AX, 0, AX, h-1);
            */
        }
        bool mbMeasureMouseClick(int AX, int AY, bool AbUp)
        {
            bool bExecute = false;
            bool bLine = false;
            bool bMsg = _mbMeasureLineDrawn;
            string msg = "";

            mDrawMeasureLine(false);
            if (AX > 0 && AX < panelZoomStrip.Width && AY > 0 && AY < panelZoomStrip.Height)
            {
                Point point = panelZoomStrip.PointToScreen(new Point(AX, AY));
                float t = _mZoomChart.mGetValueT(AX);
                float a = _mZoomChart.mGetValueA(AY);
                string channel = mGetChannelInfo();

                msg = "(" + AX.ToString() + ", " + AY.ToString() + ") ";
                msg += AbUp ? "^ " : "_ ";
                msg += channel + " ";

                switch (_mClickState)
                {
                    case DMeasureClickState.Off:
                        msg += "0 ";
                        if (AbUp == false)
                        {
                            _mClickState = DMeasureClickState.Down1;
                            _mPoint1 = point;
                            _mPoint1T = t;
                            _mPoint1A = a;
                            mDrawVerticalLine(AX, AY); //mDrawCrossLine(AX, AY);
                        }
                        if (bShowIndexValues)
                        {
                            msg += " p" + mGetIndexStr(t, a);
                        }
                        break;
                    case DMeasureClickState.Down1:
                        msg += "1_ ";
                        if (AbUp)
                        {
                            _mClickState = DMeasureClickState.Up1;
                            _mPoint1 = point;
                            _mPoint1T = t;
                            _mPoint1A = a;
                            _mPoint2 = _mPoint1;
                            bExecute = _mbClickDualPoint == false;
                            bLine = bExecute == false;
                        }
                        msg += "Point1= " + t.ToString("0.000") + " Sec, " + a.ToString("0.000") + " mV";
                        if (bShowIndexValues)
                        {
                            msg += " p1" + mGetIndexStr(t, a);
                        }
                        break;
                    case DMeasureClickState.Up1:
                        {
                            double dT = (t - _mPoint1T);
                            double dA = (a - _mPoint1A);
                            double hr = Math.Abs(dT);
                            string hrStr = hr < 0.001 ? "" : " (hr=" + (60 / hr).ToString("0.0") + " bpm)";
                            msg += "1^ ";
                            if (AbUp == false)
                            {
                                _mClickState = DMeasureClickState.Down2;
                                _mPoint2 = point;
                                _mPoint2T = t;
                                _mPoint2A = a;
                            }
                            bLine = true;
                            msg += "Point2: " + t.ToString("0.000") + " Sec, " + a.ToString("0.000") + " mV => dT= "
                                + dT.ToString("0.000") + " Sec" + hrStr + ", dA= " + dA.ToString("0.000") + " mV";
                            _mPoint2 = point;
                            if (bShowIndexValues)
                            {
                                msg += " p2" + mGetIndexStr(t, a) + " dP" + mGetIndexStr((float)dT, (float)dA);
                            }
                        }
                        break;
                    case DMeasureClickState.Down2:
                        {
                            double dT = (t - _mPoint1T);
                            double dA = (a - _mPoint1A);
                            double hr = Math.Abs(dT);
                            string hrStr = hr < 0.001 ? "" : " (hr=" + (60 / hr).ToString("0.0") + " bpm)";
                            msg += "2_ ";
                            if (AbUp)
                            {
                                _mClickState = DMeasureClickState.Up2;
                                _mPoint2 = point;
                                _mPoint2T = t;
                                _mPoint2A = a;
                                bExecute = true;
                            }
                            msg += "Point2= " + t.ToString("0.000") + " Sec, " + a.ToString("0.000") + " mV => dT= "
                                + dT.ToString("0.000") + " Sec" + hrStr + ", dA= " + dA.ToString("0.000") + " mV";
                            _mPoint2 = point;
                            if (bShowIndexValues)
                            {
                                msg += " p2" + mGetIndexStr(t, a) + " dP" + mGetIndexStr((float)dT, (float)dA);
                            }

                        }
                        break;
                    case DMeasureClickState.Up2:
                        //msg += "2^ ";
                        msg = "";
                        break;
                }
            }
            else
            {
                mResetMeasurement();
                bMsg = false;
            }
            if (bMsg || msg.Length > 0)
            {
                mStatusLine(msg);
            }
            if (bLine)
            {
                mDrawMeasureLine(true);
            }
            return bExecute;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void toolStripComboBox1_Click(object sender, EventArgs e)
        {

        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {

        }

        private void secToolStripMenuItem5_Click(object sender, EventArgs e)
        {

        }

        private void contextMenuStripN_Opening(object sender, CancelEventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void toolStripComboBox4_Click(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void mSetZoomCenterByRelPos(UInt16 AChannel, float ARelOnStrip)
        {
            if (AChannel < _mNrSignals && _mRecordFile != null)
            {
                bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;
                bool bAlt = (Control.ModifierKeys & Keys.Alt) == Keys.Alt;
                float posSec = _mChannelsCenterSec + _mChannelsDurationSec * (ARelOnStrip - 0.5F);
                float stripDuration = _mRecordFile.mGetSamplesTotalTime();

                _mZoomChannel = AChannel;

                if (bCtrl)
                {
                    // control => left side => set start of zoom
                    _mZoomCenterSec = posSec + _mZoomDurationSec * 0.5F;
                }
                else if (bAlt)
                {
                    // alt right side => size of zoom
                    float clipMin = _mZoomCenterSec - _mZoomDurationSec * 0.5F;
                    float size = posSec - clipMin;
                    _mZoomDurationSec = Math.Abs(size);
                    _mZoomCenterSec = (clipMin + size * 0.5F);
                }
                else
                {
                    // normal => set center of zoom
                    _mZoomCenterSec = posSec;
                }

                if (_mZoomCenterSec < _mZoomDurationSec * 0.5F)
                {
                    _mZoomCenterSec = _mZoomDurationSec * 0.5F;
                }
                else if (_mZoomCenterSec > stripDuration - _mZoomDurationSec * 0.5F)
                {
                    _mZoomCenterSec = stripDuration - _mZoomDurationSec * 0.5F;
                }
                if (_mZoomDurationSec < 1.0)
                {
                    _mZoomDurationSec = 1.0F;
                }
                mInitZoomChannel();
                mUpdateStripChannels();   // needed for cursor
            }
        }
        private void panelChannel1_Click(object sender, EventArgs e)
        {

        }

        private void panelChannel1_MouseClick(object sender, MouseEventArgs e)
        {
            float f = e.X / (float)panelChannel1.Width;

            mSetZoomCenterByRelPos(0, f);
        }

        private void panelZoomStrip_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void panelZoomStrip_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                mbMeasureMouseClick(e.X, e.Y, false);
            }
            else
            {
                mResetMeasurement();
            }
        }

        private string mGetChannelInfo()
        {
            CMeasureStrip ms = mGetSelectedMeasureStrip();
            string channel = _mZoomChannel.ToString() + "-";

            if (_mRecordDb != null) channel += _mRecordDb.mGetChannelName(_mZoomChannel) + " ";
            if (ms != null && ms._mChannel != _mZoomChannel)
            {
                channel += "!= <" + _mRecordDb.mGetChannelName(ms._mChannel) + "> ";
            }
            return channel;
        }

        private void panelZoomStrip_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {

                if (mbMeasureMouseClick(e.X, e.Y, true))
                {
                    string channel = mGetChannelInfo();

                    // do action
                    if (_mbClickDualPoint)
                    {
                        double dT = (_mPoint2T - _mPoint1T);
                        double hr = Math.Abs(dT);
                        string hrStr = hr < 0.001 ? "" : " (hr=" + (60 / hr).ToString("0.0") + " bpm)";
                        string msg = _mbClickDualPoint ? "exec2 " + channel + dT.ToString("0.0000") + " Sec" + hrStr + ", " + (_mPoint2A - _mPoint1A).ToString("0.0000") + " mV"
                                                         : "exec1 " + channel + _mPoint1T.ToString("0.0000") + " Sec, " + _mPoint1A.ToString("0.0000") + " mV";
                        mStatusLine(msg);
                        contextMenuDualClick.Show(panelZoomStrip.PointToScreen(new Point(e.X + 15, e.Y)));
                    }
                    else
                    {
                        double dT = (_mPoint2T - _mPoint1T);
                        double hr = Math.Abs(dT);
                        string hrStr = hr < 0.001 ? "" : " (hr=" + (60 / hr).ToString("0.0") + " bpm)";
                        string msg = _mbClickDualPoint ? "exec2 " + channel + dT.ToString("0.0000") + " Sec" + hrStr + ", " + (_mPoint2A - _mPoint1A).ToString("0.0000") + " mV"
                                                         : "exec1 " + channel + _mPoint1T.ToString("0.0000") + " Sec, " + _mPoint1A.ToString("0.0000") + " mV";
                        mStatusLine(msg);
                        contextMenuSingleClick.Show(panelZoomStrip.PointToScreen(new Point(e.X + 15, e.Y)));
                    }
                    mDrawVerticalLine(e.X, e.Y);
                }
            }
            else
            {
                mResetMeasurement();
            }
        }

        private void panelZoomStrip_MouseMove(object sender, MouseEventArgs e)
        {
            mMeasureMouseMove(e.X, e.Y, e.Button == MouseButtons.Left);
        }

        private void panelZoomStrip_MouseLeave(object sender, EventArgs e)
        {
            mDrawMeasureLine(false);
        }

        private void toolStripTwoClick_Click(object sender, EventArgs e)
        {
            mSetDualClick(true);
        }

        private void toolStripOneClick_Click(object sender, EventArgs e)
        {
            mSetDualClick(false);
        }

        private void contextMenuDualClick_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (e.ClickedItem != null)
            {
                string s = e.ClickedItem.Text;
                DMeasure2Tag tag = CMeasurePoint.sGetMeasure2TagFromShow(s);

                if (tag != DMeasure2Tag.Unknown)
                {
                    string channel = mGetChannelInfo();
                    bool bUseAmp = CMeasurePoint.sbGetMeasure2UseAmp(tag);
                    string val = bUseAmp ? (_mPoint2A - _mPoint1A).ToString("0.000") : (_mPoint2T - _mPoint1T).ToString("0.000");
                    string unit = CMeasurePoint.sGetMeasure2TagUnit(tag);
                    string name = CMeasurePoint.sGetMeasure2TagString(tag);
                    string cmd = channel + " " + name + " = " + val + " " + unit;

                    mStatusLine(cmd);
                    // do command: add to list

                    mAddMeasurePoint(new CMeasurePoint(tag, _mZoomChannel, _mPoint1T, _mPoint1A, _mPoint2T, _mPoint2A));

                    mResetMeasurement();
                }
            }
        }

        private void contextMenuSingleClick_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (e.ClickedItem != null)
            {
                string s = e.ClickedItem.Text;
                DMeasure1Tag tag = CMeasurePoint.sGetMeasure1TagFromShow(s);

                if (tag != DMeasure1Tag.Unknown)
                {
                    string channel = mGetChannelInfo();
                    bool bUseAmp = CMeasurePoint.sbGetMeasure1UseAmp(tag);
                    string val = bUseAmp ? _mPoint1A.ToString("0.000") : _mPoint1T.ToString("0.000");
                    string unit = CMeasurePoint.sGetMeasure1TagUnit(tag);
                    string name = CMeasurePoint.sGetMeasure1TagString(tag);
                    string cmd = channel + " " + name + " = " + val + " " + unit;

                    mStatusLine(cmd);
                    // do command
                    mAddMeasurePoint(new CMeasurePoint(tag, _mZoomChannel, _mPoint1T, _mPoint1A));

                    mResetMeasurement();
                }
            }
        }

        private void panelChannel2_MouseClick(object sender, MouseEventArgs e)
        {
            float f = e.X / (float)panelChannel2.Width;

            mSetZoomCenterByRelPos(1, f);
        }

        private void panelChannel3b_MouseClick(object sender, MouseEventArgs e)
        {
            float f = e.X / (float)panelChannel3.Width;

            mSetZoomCenterByRelPos(2, f);
        }

        private void FormAnalyze_SizeChanged(object sender, EventArgs e)
        {
            int newWidth = Size.Width - 10;

            //            panelChnls.Width = newWidth;
            panelAllChannels.Width = newWidth;
            //            panelChannel1.Width = panelChnls.Width;
            //            panelChannel2.Width = panelChnls.Width;
            //            panelChannel3.Width = panelChnls.Width;

            //            string s = "Width= " + Size.Width.ToString() + ", P1= " + panelChannel1.Width.ToString() + ", all=" + panelAllChannels.Width.ToString()
            //                + ", P34=" + panel34.Width.ToString() + ", sp=" + splitContainer4.Panel1.Width.ToString();
            //            toolStripStatusLabel1.Text = s;
            splitContainer4.SplitterDistance = splitContainer4.Size.Height - 196;
            mUpdateStripChannels();
            mUpdateZoomChannel();
        }



        public void mUpdateStripList(bool AbChanged)
        {
            if (_mbUpdatingStripList)
            {
                return; // update during update
            }
            _mbUpdatingStripList = true;

            try
            {
                /*            int index = 0;

                                        if (listViewStrips.SelectedItems.Count > 0)
                                        {
                                            index = listViewStrips.Items.IndexOf(listViewStrips.SelectedItems[0]);
                                        }
                            */
                if (AbChanged)
                {
                    mSetChanged();
                }
                listViewStrips.Items.Clear();

                if (_mRecAnalysisFile._mStripList != null && _mRecordDb != null)
                {
                    int nr = 0;

                    if (_mSelectNewStripIndex >= 0)
                    {
                        _mSelectedStripIndex = _mSelectNewStripIndex;
                        _mSelectNewStripIndex = -1;
                    }
                    else if (listViewStrips.SelectedItems.Count > 0)
                    {
                        _mSelectedStripIndex = listViewStrips.Items.IndexOf(listViewStrips.SelectedItems[0]);
                    }
                    if (_mSelectedStripIndex > 0 && _mSelectedStripIndex >= _mRecAnalysisFile._mStripList.Count)
                    {
                        _mSelectedStripIndex = _mRecAnalysisFile._mStripList.Count - 1;
                    }

                    foreach (CMeasureStrip ms in _mRecAnalysisFile._mStripList)
                    {
                        ++nr;
                        ListViewItem item = new ListViewItem(_mRecordDb.mGetChannelName(ms._mChannel));
                        if (item != null)
                        {
                            //                            DateTime dt = _mRecordDb.mGetDeviceTime(_mRecordDb.mStartRecUTC).AddSeconds(ms._mStartTimeSec);
                            DateTime dt = _mRecordFile.mGetDeviceTime(_mRecordFile.mBaseUTC).AddSeconds(ms._mStartTimeSec);
                            bool bSelected = _mSelectedStripIndex == nr - 1;

                            item.Selected = bSelected;
                            item.Checked = ms._mbActive;

                            item.BackColor = bSelected ? Color.LightGreen : listViewStrips.BackColor;

                            //                        item.Checked = ms._mbActive;
                            item.SubItems.Add(CProgram.sDateToString(dt));
                            item.SubItems.Add(CProgram.sTimeToString(dt));
                            item.SubItems.Add(ms._mDurationSec.ToString("0.0"));
                            //N not present                            item.SubItems.Add(ms.mGetCount().ToString());

                            listViewStrips.Items.Add(item);
                        }
                    }
                }
                mUpdateMeasurementList();
            }
            catch (Exception ex)
            {
                CProgram.sPromptException(false, "Analyze", "Exception updating Strip List", ex);
            }

            _mbUpdatingStripList = false;
        }
        public void mUpdateMeasurementList()
        {
            if (_mbUpdatingMeasurementList)
            {
                return; // update during update
            }
            _mbUpdatingMeasurementList = true;

            try
            {
                string stripName = "";
                string stripHr = "";

                /*            int index = 0;
                            if (listViewMeasurements.SelectedItems.Count > 0)
                            {
                                index = listViewMeasurements.Items.IndexOf(listViewMeasurements.SelectedItems[0]);
                            }
                */
                listViewMeasurements.Items.Clear();

                if (_mRecAnalysisFile._mStripList != null && _mRecordDb != null)
                {
                    CMeasureStrip ms = mGetSelectedMeasureStrip();

                    if (_mSelectNewMeasurementIndex >= 0)
                    {
                        _mSelectedMeasurementIndex = _mSelectNewMeasurementIndex;
                        _mSelectNewMeasurementIndex = -1;
                    }
                    /*                   else if (listViewMeasurements.SelectedItems.Count > 0)
                                       {
                                           mSelectedMeasurementIndex = listViewMeasurements.Items.IndexOf(listViewMeasurements.SelectedItems[0]);
                                       }
                   */
                    //                if (mSelectedMeasurementIndex > 0 && mSelectedMeasurementIndex >= mRecAnalysisFile._mStripList.Count)
                    //                {
                    //                    mSelectedMeasurementIndex = mRecAnalysisFile._mStripList.Count - 1;
                    //                }
                    if (ms != null)
                    {
                        int nr = 0;
                        int nRR = 0;
                        float rrSum = 0;
                        //                        DateTime dt = _mRecordDb.mGetDeviceTime(_mRecordDb.mStartRecUTC).AddSeconds(ms._mStartTimeSec);
                        DateTime dt = _mRecordFile.mGetDeviceTime(_mRecordFile.mBaseUTC).AddSeconds(ms._mStartTimeSec);

                        if (_mSelectedMeasurementIndex >= ms.mGetCount())
                        {
                            _mSelectedMeasurementIndex = ms.mGetCount();
                        }
                        stripName = (_mSelectedStripIndex + 1).ToString() + ": " + _mRecordDb.mGetChannelName(ms._mChannel) + "  " + CProgram.sTimeToString(dt);
                        foreach (CMeasurePoint mp in ms._mPointList)
                        {
                            //                            dt = _mRecordDb.mGetDeviceTime(_mRecordDb.mStartRecUTC).AddSeconds(mp._mPoint1T);
                            dt = _mRecordFile.mGetDeviceTime(_mRecordFile.mBaseUTC).AddSeconds(mp._mPoint1T);
                            ++nr;
                            ListViewItem item = new ListViewItem(CProgram.sTimeToString(dt));
                            if (item != null)
                            {
                                bool bSelected = _mSelectedMeasurementIndex == nr - 1;

                                item.Selected = bSelected;
                                item.Checked = mp._mbActive;
                                item.BackColor = bSelected ? Color.LightGreen : listViewStrips.BackColor;
                                //item.Checked = mp._mbActive;
                                item.SubItems.Add((mp._mChannel + 1).ToString());
                                item.SubItems.Add(mp.mGetTagString());
                                item.SubItems.Add(mp.mGetValueString());
                                item.SubItems.Add(mp.mGetUnit());

                                listViewMeasurements.Items.Add(item);
                            }
                            if (mp._mTagID == (UInt16)DMeasure2Tag.RRtime)
                            {
                                rrSum += Math.Abs(mp.mGetValue());
                                ++nRR;
                            }
                            if (nRR > 0 && rrSum > 0.001)
                            {
                                int hr = (int)(60.0F / (rrSum / nRR) + 0.5F);

                                stripHr = " HR " + hr.ToString() + " bpm";
                            }
                        }
                    }
                }
                toolStripMeasurmentName.Text = stripName;
                toolStripHr.Text = stripHr;

                mUpdateStatsList();

            }
            catch (Exception ex)
            {
                CProgram.sPromptException(false, "Analyze", "Exception updating MeasurementList", ex);
            }
            _mbUpdatingMeasurementList = false;
        }

        public void mUpdateStatsList()
        {
            if (_mbUpdatingStatList)
            {
                return;
            }
            _mbUpdatingStatList = true;
            try
            {
                listViewStats.Items.Clear();

                if (_mRecAnalysisFile != null && _mRecordDb != null)
                {
                    _mRecAnalysisFile.mRecalcStatAllStrips();

                    if (_mRecAnalysis != null)
                    {
                        _mRecAnalysis._mMeasuredHrN = 0;
                        _mRecAnalysis._mMeasuredHrMin = 0.0F;
                        _mRecAnalysis._mMeasuredHrMean = 0.0F;
                        _mRecAnalysis._mMeasuredHrMax = 0.0F;
                        _mRecAnalysis._mbManualEvent = _mRecordDb.mbIsManualEventType(); // mEventTypeString.Contains("Manual");
                        _mRecAnalysis._mNrTotalStrips = _mRecAnalysisFile._mStripList == null ? (UInt16)0 : (UInt16)_mRecAnalysisFile._mStripList.Count;
                        _mRecAnalysis._mNrSelectedStrips = _mRecAnalysisFile.mCountActiveStrips();
                    }

                    if (_mRecAnalysisFile._mStatList != null && _mRecordDb != null)
                    {
                        CMeasureStat msRR = _mRecAnalysisFile.mFindStat(true, (UInt16)DMeasure2Tag.RRtime);

                        if (msRR != null && msRR._mCount > 0 && msRR._mMean > 0.001F)
                        {
                            float hrMin, hrMean, hrMax;
                            string min, mean, max;

                            hrMin = hrMean = hrMax = 0.0F;
                            min = mean = max = "";

                            hrMean = 60.0F / msRR._mMean;
                            hrMin = hrMax = hrMean;
                            mean = ((int)(hrMean + 0.4999F)).ToString();
                            if (msRR._mCount > 1)
                            {
                                hrMin = msRR._mMax > 0.0001 ? 60.0F / msRR._mMax : 0.0F;
                                min = msRR._mMax > 0.0001F ? ((int)(hrMin + 0.4999)).ToString() : "-";
                                hrMax = msRR._mMin > 0.0001 ? 60.0F / msRR._mMin : 0.0F;
                                max = msRR._mMin > 0.0001F ? ((int)(hrMax + 0.4999)).ToString() : "-";
                            }
                            ListViewItem itemRR = new ListViewItem("Heart rate");
                            if (itemRR != null)
                            {
                                itemRR.Checked = false;
                                itemRR.SubItems.Add(msRR._mCount.ToString());
                                itemRR.SubItems.Add(min);
                                itemRR.SubItems.Add(max);
                                itemRR.SubItems.Add(mean);
                                itemRR.SubItems.Add("BPM");

                                listViewStats.Items.Add(itemRR);
                            }
                            if (_mRecAnalysis != null)
                            {
                                _mRecAnalysis._mMeasuredHrN = msRR._mCount;
                                _mRecAnalysis._mMeasuredHrMin = hrMin;
                                _mRecAnalysis._mMeasuredHrMean = hrMean;
                                _mRecAnalysis._mMeasuredHrMax = hrMax;
                            }


                            UInt16 nQt;
                            float fRR, fQT, fQTcB, fQTcF;

                            if (_mRecAnalysisFile.mbFindQTc(out nQt, out fRR, out fQT, out fQTcB, out fQTcF))
                            {
                                ListViewItem itemQTc = new ListViewItem("QTc (B,F)");
                                if (itemQTc != null)
                                {
                                    itemQTc.Checked = false;
                                    itemQTc.SubItems.Add(nQt.ToString());
                                    itemQTc.SubItems.Add(fQTcB.ToString("0.000"));
                                    itemQTc.SubItems.Add(fQTcF.ToString("0.000"));
                                    itemQTc.SubItems.Add("");
                                    itemQTc.SubItems.Add("Sec");

                                    listViewStats.Items.Add(itemQTc);
                                }
                            }
                        }

                        foreach (CMeasureStat ms in _mRecAnalysisFile._mStatList)
                        {
                            UInt16 n = ms._mCount;
                            int nr = 0;

                            if (n > 0 || ms._mbShowEmpty)
                            {
                                ++nr;
                                ListViewItem item = new ListViewItem(ms.mGetTagString());
                                if (item != null)
                                {
                                    item.Checked = ms._mbActive;
                                    item.SubItems.Add(n.ToString());
                                    item.SubItems.Add(n <= 1 ? "" : ms.mGetValueString(ms._mMin));
                                    item.SubItems.Add(n <= 1 ? "" : ms.mGetValueString(ms._mMax));
                                    item.SubItems.Add(ms.mGetValueString(ms._mMean));
                                    item.SubItems.Add(ms.mGetUnit());

                                    listViewStats.Items.Add(item);
                                }
                            }
                        }
                    }
                }
                mUpdateWithOverlay();
            }
            catch (Exception ex)
            {
                CProgram.sPromptException(false, "Analyze", "Exception updating  Stats List", ex);
            }

            _mbUpdatingStatList = false;
        }

        public void mAddMeasurePoint(CMeasurePoint APoint)
        {
            CMeasureStrip ms = mGetSelectedMeasureStrip();
            bool bNew = false;

            if (ms == null)
            {
                if (_mRecAnalysisFile._mStripList.Count > 0)
                {
                    ms = _mRecAnalysisFile.mGetStrip(0);
                }
                else
                {
                    ms = _mRecAnalysisFile.mAddStrip(_mZoomChannel, _mZoomChart.mGetStartTimeSec(), _mZoomChart.mGetStripTimeSec(),
                            _mZoomChart.mGetStartA(), _mZoomChart.mGetEndA(), _mZoomImage);
                    bNew = true;
                }
            }
            if (ms != null)
            {
                int i = ms.mGetCount();
                if (ms.mbAddPoint(APoint))
                {
                    int index = _mRecAnalysisFile._mStripList.IndexOf(ms);
                    mStatusLine("Added Point( " + APoint.mGetTagString() + " = " + APoint.mGetValueString() + "  " + APoint.mGetUnit()
                        + " ) to strip " + index.ToString() + (bNew ? " (new)" : ""));

                    mSetMeasurementIndex(i);
                }
                mUpdateStripList(true);
            }
        }
        private void mUpdateWithOverlay()
        {
            if (false == _mbUpdatingStripList && false == _mbUpdatingMeasurementList)
            {
                mUpdateStripChannels();
                mUpdateZoomChannel();
            }
        }

        private void mSetMeasurementIndex(int AIndex)
        {
            _mSelectedMeasurementIndex = _mSelectNewMeasurementIndex = AIndex;
        }
        private void mSetStripIndex(int AIndex)
        {
            _mSelectedStripIndex = _mSelectNewStripIndex = AIndex;

        }

        public CMeasureStrip mGetSelectedMeasureStrip()
        {
            //            int index = 0;

            //            if (listViewStrips.Items.Count > 0)
            if (_mbUpdatingStripList == false && _mRecAnalysisFile._mStripList != null)
            {
                if (_mSelectedStripIndex > 0 && _mSelectedStripIndex >= _mRecAnalysisFile._mStripList.Count)
                {
                    _mSelectedStripIndex = _mRecAnalysisFile._mStripList.Count - 1;
                }
                //                index = listViewStrips.Items.IndexOf(listViewStrips.SelectedItems[0]);
            }
            return _mRecAnalysisFile.mGetStrip((UInt16)_mSelectedStripIndex);
        }

        public CMeasurePoint mGetSelectedMeasurePoint()
        {
            CMeasurePoint mp = null;

            if (_mbUpdatingStripList == false && _mbUpdatingMeasurementList == false)
            {
                CMeasureStrip ms = mGetSelectedMeasureStrip();

                if (ms != null)
                {
                    /*                int index = 0;

                                    if (listViewMeasurements.SelectedItems.Count > 0)
                                    {
                                        index = listViewMeasurements.Items.IndexOf(listViewMeasurements.SelectedItems[0]);
                                    }

                                    mp = ms.mGetPoint((UInt16)index);
                    */
                    if (_mSelectedMeasurementIndex > 0 && _mSelectedMeasurementIndex >= listViewMeasurements.Items.Count)
                    {
                        _mSelectedMeasurementIndex = listViewMeasurements.Items.Count - 1;
                    }
                    mp = ms.mGetPoint((UInt16)_mSelectedMeasurementIndex);
                }
            }
            return mp;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            mUpdateStatsList();
        }

        private void toolStripZoomAmplitude_Click(object sender, EventArgs e)
        {
            _mbAmplitudeZoomWindow = true;
            _mSpeedZoomWindow = true;

            contextMenuAmplitude.Show(panelZoomAT.PointToScreen(new Point(panelZoomAT.Width / 3, 0)));
        }

        /*        public bool mbParseFloat(string AString, ref float ArFloat)
                {
                    string s = "";
                    int n = AString == null ? 0 : AString.Length;
                    int l = 0;
                    for (int i = 0; i < n; ++i)
                    {
                        char c = AString[i];

                        if (char.IsDigit(c))
                        {
                            s += c;
                        }
                        else if ((c == '.' || c == ','))
                        {
                            s += '.';
                        }
                        else if( c == 'e' || c == 'E' || c == '-' || c == '+' )
                        {
                            s += c;
                        }
                        else if( c != ' ' && l > 0)
                        {
                            break;
                        }
                    }
                    return CProgram.sbParseFloat(DFloatDecimal.Dot, s, ref ArFloat);
                }
        */

        private void contextMenuAmplitude_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (e.ClickedItem != null && _mRecordFile != null)
            {
                string text = e.ClickedItem.Text;
                float f = _mRecordFile.mGetAmplitudeFromShowString(text);

                if (_mbAmplitudeZoomWindow)
                {
                    _mZoomUnitA = f;
                    _mZoomChart.mSetUnitA(f);
                    mUpdateZoomChannel();
                }
                else
                {
                    _mChannelsUnitA = f;

                    for (UInt16 i = 0; i < _mNrSignals; ++i)
                    {
                        if (_mStripCharts[i] != null)
                        {
                            _mStripCharts[i].mSetUnitA(f);
                        }
                    }
                    mUpdateStripChannels();
                }
            }
        }

        private void contextMenuDualClick_Closing(object sender, ToolStripDropDownClosingEventArgs e)
        {
            mResetMeasurement();
        }

        private void contextMenuSingleClick_Closing(object sender, ToolStripDropDownClosingEventArgs e)
        {
            mResetMeasurement();
        }

        private void contextMenuSingleClick_Closing(object sender, ToolStripDropDownClosedEventArgs e)
        {
            mResetMeasurement();
        }

        private void contextMenuDualClick_Closed(object sender, ToolStripDropDownClosedEventArgs e)
        {
            mResetMeasurement();
        }

        private void toolStripChannelAmplitute_Click(object sender, EventArgs e)
        {
            _mbAmplitudeZoomWindow = false;
            _mSpeedZoomWindow = false;

            contextMenuAmplitude.Show(panelStripAT.PointToScreen(new Point(panelStripAT.Width / 3, 0)));
        }

        private void toolStripZoomSpeed_Click(object sender, EventArgs e)
        {
            _mbAmplitudeZoomWindow = true;
            _mSpeedZoomWindow = true;

            contextMenuFeed.Show(panelZoomAT.PointToScreen(new Point(panelZoomAT.Width * 2 / 3, 0)));
        }

        private void toolStripChannelSpeed_Click(object sender, EventArgs e)
        {
            _mbAmplitudeZoomWindow = false;
            _mSpeedZoomWindow = false;

            contextMenuFeed.Show(panelStripAT.PointToScreen(new Point(panelStripAT.Width * 2 / 3, 0)));
        }

        private void contextMenuFeed_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (e.ClickedItem != null && _mRecordFile != null)
            {
                string text = e.ClickedItem.Text;
                float f = _mRecordFile.mGetSpeedFromShowString(text);

                if (_mbAmplitudeZoomWindow)
                {
                    _mZoomUnitT = f;
                    _mZoomChart.mSetUnitT(f);
                    mUpdateZoomChannel();
                }
                else
                {
                    _mChannelsUnitT = f;

                    for (UInt16 i = 0; i < _mNrSignals; ++i)
                    {
                        if (_mStripCharts[i] != null)
                        {
                            _mStripCharts[i].mSetUnitT(f);
                        }
                    }
                    mUpdateStripChannels();
                }
            }

        }

        void mZoomTimeMax()
        {
            if (_mZoomChart != null && _mRecordFile != null)
            {
                float stripDuration = _mRecordFile.mGetSamplesTotalTime();

                _mZoomDurationSec = stripDuration;
                _mZoomCenterSec = _mZoomDurationSec * 0.5F;
                if (_mZoomDurationSec < 1.0F) _mZoomDurationSec = 1.0F;

                _mZoomChart.mbSetGraphMaxTA(_mZoomCenterSec - 0.5F * _mZoomDurationSec, _mZoomDurationSec, _mZoomChart.mGetStartA(), _mZoomChart.mGetEndA(), CRecordMit.cMinRangeAmplitude);
                mUpdateStripChannels();
                mUpdateZoomChannel();
            }
        }
        void mZoomTimeFactor(float AFactor)
        {
            if (_mZoomChart != null && _mRecordFile != null)
            {
                float stripDuration = _mRecordFile.mGetSamplesTotalTime();

                _mZoomDurationSec *= AFactor;
                _mZoomDurationSec = (int)(_mZoomDurationSec + 0.50001F);
                if (_mZoomDurationSec > stripDuration)
                {
                    _mZoomDurationSec = stripDuration;
                    _mZoomCenterSec = _mZoomDurationSec * 0.5F;
                }
                if (_mZoomDurationSec < 1.0F) _mZoomDurationSec = 1.0F;
                if (_mZoomCenterSec < _mZoomDurationSec * 0.5F)
                {
                    _mZoomCenterSec = _mZoomDurationSec * 0.5F;
                }
                else if (_mZoomCenterSec > stripDuration - _mZoomDurationSec * 0.5F)
                {
                    _mZoomCenterSec = stripDuration - _mZoomDurationSec * 0.5F;
                }

                _mZoomChart.mbSetGraphMaxTA(_mZoomCenterSec - 0.5F * _mZoomDurationSec, _mZoomDurationSec, _mZoomChart.mGetStartA(), _mZoomChart.mGetEndA(), CRecordMit.cMinRangeAmplitude);
                mUpdateStripChannels();
                mUpdateZoomChannel();
            }
        }
        void mZoomTimeSet(float ADurationSec)
        {
            if (_mZoomChart != null && _mRecordFile != null)
            {
                float stripDuration = _mRecordFile.mGetSamplesTotalTime();

                _mZoomDurationSec = ADurationSec;
                _mZoomDurationSec = (int)(_mZoomDurationSec + 0.50001F);
                if (_mZoomDurationSec > stripDuration)
                {
                    _mZoomDurationSec = stripDuration;
                    _mZoomCenterSec = _mZoomDurationSec * 0.5F;
                }
                if (_mZoomDurationSec < 1.0F) _mZoomDurationSec = 1.0F;
                if (_mZoomCenterSec < _mZoomDurationSec * 0.5F)
                {
                    _mZoomCenterSec = _mZoomDurationSec * 0.5F;
                }
                else if (_mZoomCenterSec > stripDuration - _mZoomDurationSec * 0.5F)
                {
                    _mZoomCenterSec = stripDuration - _mZoomDurationSec * 0.5F;
                }

                _mZoomChart.mbSetGraphMaxTA(_mZoomCenterSec - 0.5F * _mZoomDurationSec, _mZoomDurationSec, _mZoomChart.mGetStartA(), _mZoomChart.mGetEndA(), CRecordMit.cMinRangeAmplitude);
                mUpdateStripChannels();
                mUpdateZoomChannel();
            }
        }
        void mZoomTimeShift(float AShiftFactor)
        {
            if (_mZoomChart != null && _mRecordFile != null)
            {
                float stripDuration = _mRecordFile.mGetSamplesTotalTime();

                _mZoomCenterSec += _mZoomDurationSec * AShiftFactor;
                _mZoomDurationSec = (int)(_mZoomDurationSec + 0.499F);
                if (_mZoomDurationSec > stripDuration)
                {
                    _mZoomDurationSec = stripDuration;
                    _mZoomCenterSec = _mZoomDurationSec * 0.5F;
                }
                if (_mZoomDurationSec < 1.0F) _mZoomDurationSec = 1.0F;
                if (_mZoomCenterSec < _mZoomDurationSec * 0.5F)
                {
                    _mZoomCenterSec = _mZoomDurationSec * 0.5F;
                }
                else if (_mZoomCenterSec > stripDuration - _mZoomDurationSec * 0.5F)
                {
                    _mZoomCenterSec = stripDuration - _mZoomDurationSec * 0.5F;
                }

                _mZoomChart.mbSetGraphMaxTA(_mZoomCenterSec - 0.5F * _mZoomDurationSec, _mZoomDurationSec, _mZoomChart.mGetStartA(), _mZoomChart.mGetEndA(), CRecordMit.cMinRangeAmplitude);
                mUpdateStripChannels();
                mUpdateZoomChannel();
            }
        }
        void mZoomTimeCenter(float ACenterSec)
        {
            if (_mZoomChart != null && _mRecordFile != null)
            {
                float stripDuration = _mRecordFile.mGetSamplesTotalTime();

                _mZoomCenterSec = ACenterSec;
                if (_mZoomCenterSec < _mZoomDurationSec * 0.5F)
                {
                    _mZoomCenterSec = _mZoomDurationSec * 0.5F;
                }
                else if (_mZoomCenterSec > stripDuration - _mZoomDurationSec * 0.5F)
                {
                    _mZoomCenterSec = stripDuration - _mZoomDurationSec * 0.5F;
                }

                _mZoomChart.mbSetGraphMaxTA(_mZoomCenterSec - 0.5F * _mZoomDurationSec, _mZoomDurationSec, _mZoomChart.mGetStartA(), _mZoomChart.mGetEndA(), CRecordMit.cMinRangeAmplitude);
                mUpdateStripChannels();
                mUpdateZoomChannel();
            }
        }
        void mChannelsTimeFactor(float AFactor)
        {
            if (_mZoomChart != null && _mRecordFile != null)
            {
                float stripDuration = _mRecordFile.mGetSamplesTotalTime();

                _mChannelsDurationSec *= AFactor;
                _mChannelsDurationSec = (int)(_mChannelsDurationSec + 0.50001F);
                if (_mChannelsDurationSec > stripDuration)
                {
                    _mChannelsDurationSec = stripDuration;
                    _mChannelsCenterSec = _mChannelsDurationSec * 0.5F;
                }
                if (_mChannelsDurationSec < 1.0F) _mZoomDurationSec = 1.0F;
                if (_mChannelsCenterSec < _mChannelsDurationSec * 0.5F)
                {
                    _mChannelsCenterSec = _mChannelsDurationSec * 0.5F;
                }
                else if (_mChannelsCenterSec > stripDuration - _mChannelsDurationSec * 0.5F)
                {
                    _mChannelsCenterSec = stripDuration - _mChannelsDurationSec * 0.5F;
                }

                //!! mZoomChart.mbSetGraphMaxTA(mZoomCenterSec - 0.5F * mZoomDurationSec, mZoomDurationSec, mZoomChart.mGetStartA(), mZoomChart.mGetEndA(), CRecordMit.cMinRangeAmplitude);
                //mUpdateStripChannels();
                mInitStripChannels();
                //                mUpdateZoomChannel();
            }
        }
        void mChannelsTimeSet(float ADurationSec)
        {
            if (_mZoomChart != null && _mRecordFile != null)
            {
                float stripDuration = _mRecordFile.mGetSamplesTotalTime();

                _mChannelsDurationSec = ADurationSec;
                _mChannelsDurationSec = (int)(_mChannelsDurationSec + 0.50001F);
                if (_mChannelsDurationSec > stripDuration)
                {
                    _mChannelsDurationSec = stripDuration;
                    _mChannelsCenterSec = _mChannelsDurationSec * 0.5F;
                }
                if (_mChannelsDurationSec < 1.0F) _mZoomDurationSec = 1.0F;
                if (_mChannelsCenterSec < _mChannelsDurationSec * 0.5F)
                {
                    _mChannelsCenterSec = _mChannelsDurationSec * 0.5F;
                }
                else if (_mChannelsCenterSec > stripDuration - _mChannelsDurationSec * 0.5F)
                {
                    _mChannelsCenterSec = stripDuration - _mChannelsDurationSec * 0.5F;
                }

                //!! mZoomChart.mbSetGraphMaxTA(mZoomCenterSec - 0.5F * mZoomDurationSec, mZoomDurationSec, mZoomChart.mGetStartA(), mZoomChart.mGetEndA(), CRecordMit.cMinRangeAmplitude);
                //mUpdateStripChannels();
                mInitStripChannels();
                //                mUpdateZoomChannel();
            }
        }
        void mChannelsTimeShift(float AShiftFactor)
        {
            if (_mZoomChart != null && _mRecordFile != null)
            {
                float stripDuration = _mRecordFile.mGetSamplesTotalTime();

                _mChannelsCenterSec += _mChannelsDurationSec * AShiftFactor;
                _mChannelsDurationSec = (int)(_mChannelsDurationSec + 0.499F);
                if (_mChannelsDurationSec > stripDuration)
                {
                    _mChannelsDurationSec = stripDuration;
                    _mChannelsCenterSec = _mChannelsDurationSec * 0.5F;
                }
                if (_mChannelsDurationSec < 1.0F) _mChannelsDurationSec = 1.0F;
                if (_mChannelsCenterSec < _mChannelsDurationSec * 0.5F)
                {
                    _mChannelsCenterSec = _mChannelsDurationSec * 0.5F;
                }
                else if (_mChannelsCenterSec > stripDuration - _mChannelsDurationSec * 0.5F)
                {
                    _mChannelsCenterSec = stripDuration - _mChannelsDurationSec * 0.5F;
                }

                //mZoomChart.mbSetGraphMaxTA(mZoomCenterSec - 0.5F * mZoomDurationSec, mZoomDurationSec, mZoomChart.mGetStartA(), mZoomChart.mGetEndA(), CRecordMit.cMinRangeAmplitude);
                //mUpdateStripChannels();
                mInitStripChannels();
                //mUpdateZoomChannel();
            }
        }
        void mChannelsTimeCenter(float ACenter)
        {
            if (_mZoomChart != null && _mRecordFile != null)
            {
                float stripDuration = _mRecordFile.mGetSamplesTotalTime();

                _mChannelsCenterSec = ACenter;
                if (_mChannelsCenterSec < _mChannelsDurationSec * 0.5F)
                {
                    _mChannelsCenterSec = _mChannelsDurationSec * 0.5F;
                }
                else if (_mChannelsCenterSec > stripDuration - _mChannelsDurationSec * 0.5F)
                {
                    _mChannelsCenterSec = stripDuration - _mChannelsDurationSec * 0.5F;
                }
                mInitStripChannels();
            }
        }
        bool CheckZoomOnChannels()
        {
            bool bOk = true;

            float channelsStart = _mChannelsCenterSec - 0.5F * _mChannelsDurationSec;
            float channelsStop = _mChannelsCenterSec + 0.5F * _mChannelsDurationSec;

            if (_mZoomCenterSec < channelsStart || _mZoomCenterSec > channelsStop)
            {
                mChannelsTimeCenter(_mZoomCenterSec);
                bOk = false;

            }
            mUpdateGraphMarker();
            return bOk;
        }

        private void toolStripZoomTimeIncrease_Click(object sender, EventArgs e)
        {
            mZoomTimeFactor(0.5F);
        }

        private void toolStripZoomTimeOut_Click(object sender, EventArgs e)
        {
            mZoomTimeFactor(2.0F);
        }

        void mZoomAmpFactor(float AFactor)
        {
            if (_mZoomChart != null && _mRecordFile != null)
            {
                float startA = _mZoomChart.mGetStartA();
                float endA = _mZoomChart.mGetEndA();
                float duration = _mZoomChart.mGetStripTimeSec();
                float centerA = 0.5F * (startA + endA);
                float rangeA = endA - startA;
                float minRangeA = CRecordMit.cMinRangeAmplitude;

                rangeA *= AFactor;
                if (rangeA < minRangeA)
                {
                    rangeA = minRangeA;
                }
                startA = centerA - rangeA * 0.5F;
                endA = centerA + rangeA * 0.5F;

                _mZoomChart.mbSetGraphMaxTA(_mZoomCenterSec - 0.5F * _mZoomDurationSec, _mZoomDurationSec, startA, endA, minRangeA);
                mUpdateZoomChannel();
            }
        }
        void mZoomAmpShift(float AShiftFactor)
        {
            if (_mZoomChart != null && _mRecordFile != null)
            {
                float startA = _mZoomChart.mGetStartA();
                float endA = _mZoomChart.mGetEndA();
                float duration = _mZoomChart.mGetStripTimeSec();
                float centerA = 0.5F * (startA + endA);
                float rangeA = endA - startA;
                float minRangeA = CRecordMit.cMinRangeAmplitude;

                centerA += rangeA * AShiftFactor;
                if (rangeA < minRangeA)
                {
                    rangeA = minRangeA;
                }

                startA = centerA - rangeA * 0.5F;
                endA = centerA + rangeA * 0.5F;

                _mZoomChart.mbSetGraphMaxTA(_mZoomCenterSec - 0.5F * _mZoomDurationSec, _mZoomDurationSec, startA, endA, minRangeA);
                mUpdateZoomChannel();
            }
        }
        private void toolStripAmpDec_Click(object sender, EventArgs e)
        {
            mZoomAmpFactor(0.5F);
        }

        private void toolStripAmpInc_Click(object sender, EventArgs e)
        {
            mZoomAmpFactor(2.0F);
        }

        private void toolStripWindowCenter_Click(object sender, EventArgs e)
        {
            mInitZoomChannel();
        }

        private void toolStripWindowLeft_Click(object sender, EventArgs e)
        {
            mZoomTimeShift(-_cZoomTimeShiftFactor);// 0.4F);
        }

        private void toolStripWindowRight_Click(object sender, EventArgs e)
        {
            mZoomTimeShift(_cZoomTimeShiftFactor); // 0.4F);
        }

        private void toolStripWindowDown_Click(object sender, EventArgs e)
        {
            mZoomAmpShift(-_cZoomAmplShiftFactor);// 0.4F);
        }

        private void toolStripWindowUp_Click(object sender, EventArgs e)
        {
            mZoomAmpShift(_cZoomAmplShiftFactor);// 0.4F);
        }

        private void toolStripAddStrip_Click(object sender, EventArgs e)
        {
            if (_mRecAnalysisFile._mStripList != null && _mRecordDb != null)
            {
                mSetStripIndex(_mRecAnalysisFile._mStripList.Count);
                _mRecAnalysisFile.mAddStrip(_mZoomChannel, _mZoomChart.mGetStartTimeSec(), _mZoomChart.mGetStripTimeSec(),
                         _mZoomChart.mGetStartA(), _mZoomChart.mGetEndA(), _mZoomImage);
                mUpdateStripList(true);
            }
        }
        private void toolStripStripDup_Click(object sender, EventArgs e)
        {
            if (_mRecAnalysisFile._mStripList != null && _mRecordDb != null)
            {
                //                mSetStripIndex(_mRecAnalysisFile._mStripList.Count);

                if (_mNrSignals > 1)
                {
                    if (_mNrSignals == 2)
                    {
                        _mZoomChannel = (UInt16)(_mZoomChannel == 0 ? 1 : 0);

                        mSetStripIndex(_mRecAnalysisFile._mStripList.Count);
                        mUpdateZoomChannel();
                        _mRecAnalysisFile.mAddStrip(_mZoomChannel, _mZoomChart.mGetStartTimeSec(), _mZoomChart.mGetStripTimeSec(),
                         _mZoomChart.mGetStartA(), _mZoomChart.mGetEndA(), _mZoomImage);
                        mUpdateStripChannels();
                        mUpdateStripList(true);
                    }
                    else
                    {
                        Point point = panel22.PointToScreen(new Point(30, 0));
                        contextMenuAddChannel.Show(point);

                        //                        _mZoomChannel = (UInt16)index;
                    }
                }
            }
        }
        private void contextMenuAddChannel_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (e.ClickedItem != null && _mRecordFile != null && contextMenuSelChannel.Items != null)
            {
                string text = e.ClickedItem.Text;
                int index = contextMenuAddChannel.Items.IndexOf(e.ClickedItem);

                if (index >= 0)
                {
                    _mZoomChannel = (UInt16)index;

                    mSetStripIndex(_mRecAnalysisFile._mStripList.Count);
                    mUpdateZoomChannel();   // update zoom chart and image
                    _mRecAnalysisFile.mAddStrip(_mZoomChannel, _mZoomChart.mGetStartTimeSec(), _mZoomChart.mGetStripTimeSec(),
                     _mZoomChart.mGetStartA(), _mZoomChart.mGetEndA(), _mZoomImage);
                    mUpdateStripChannels();
                    mUpdateStripList(true);
                }
            }
        }


        private void toolStripDeleteStrip_Click(object sender, EventArgs e)
        {
            CMeasureStrip ms = mGetSelectedMeasureStrip();

            if (ms != null)
            {
                _mRecAnalysisFile._mStripList.Remove(ms);
                mUpdateStripList(true);
            }
        }

        private void toolStripButtonView_Click(object sender, EventArgs e)
        {
            // view
            CMeasureStrip ms = mGetSelectedMeasureStrip();

            if (ms != null)
            {
                _mZoomChannel = ms._mChannel;
                _mZoomCenterSec = ms._mStartTimeSec + ms._mDurationSec * 0.5F;
                _mZoomDurationSec = ms._mDurationSec;
                if (_mZoomDurationSec < 1) _mZoomDurationSec = 6;

                mInitZoomChannel();
                mUpdateStripChannels();   // needed for cursor
            }
        }

        private void toolStripButtonFirst_Click(object sender, EventArgs e)
        {
            // view
            CMeasureStrip ms = mGetSelectedMeasureStrip();

            if (ms != null)
            {
                int index = _mRecAnalysisFile.mGetStripIndex(ms);
                if (index > 0)
                {
                    _mRecAnalysisFile._mStripList.Remove(ms);
                    _mRecAnalysisFile._mStripList.Insert(0, ms);
                    mSetStripIndex(_mRecAnalysisFile.mGetStripIndex(ms));

                    _mZoomChannel = ms._mChannel;
                    _mZoomCenterSec = ms._mStartTimeSec + ms._mDurationSec * 0.5F;
                    _mZoomDurationSec = ms._mDurationSec;
                    if (_mZoomDurationSec < 1) _mZoomDurationSec = 6;

                    mInitZoomChannel();
                    mUpdateStripList(true);
                    mUpdateStripChannels();   // needed for cursor
                }
            }
        }
        private void toolStripButtonCopyWindow_Click(object sender, EventArgs e)
        {
            CMeasureStrip ms = mGetSelectedMeasureStrip();

            if (ms != null)
            {
                ms.mChangeStrip(_mZoomChannel, _mZoomChart.mGetStartTimeSec(), _mZoomChart.mGetStripTimeSec(),
                         _mZoomChart.mGetStartA(), _mZoomChart.mGetEndA(), _mZoomImage);
                mUpdateStripList(true);
            }
        }

        private void toolStripDeleteteMeasurement_Click(object sender, EventArgs e)
        {
            CMeasureStrip ms = mGetSelectedMeasureStrip();

            if (ms != null)
            {
                CMeasurePoint mp = mGetSelectedMeasurePoint();

                if (mp != null && ms._mPointList != null)
                {
                    int index = ms._mPointList.IndexOf(mp);

                    if (index >= 0)
                    {
                        ms._mPointList.Remove(mp);
                        mUpdateStripList(true);
                    }
                }
            }
        }

        private void listViewStrips_ItemActivate_1(object sender, EventArgs e)
        {

        }

        private void mCheckSelectedStripIndex()
        {
            //int oldIndex = mSelectedStripIndex;

            if (listViewStrips.SelectedItems.Count == 0)
            {
                mUpdateStripList(true); // needed ?
            }
            else
            {

                if (_mSelectedStripIndex != listViewStrips.Items.IndexOf(listViewStrips.SelectedItems[0]))
                {
                    mUpdateStripList(true);
                }
            }
        }

        private void mCheckSelectedMeasurementIndex()
        {
            //int oldIndex = mSelectedMeasurementIndex;

            if (listViewMeasurements.SelectedItems.Count == 0)
            {
                mUpdateMeasurementList(); // needed ?
            }
            else
            {
                if (_mSelectedStripIndex != listViewMeasurements.Items.IndexOf(listViewMeasurements.SelectedItems[0]))
                {
                    mUpdateMeasurementList();
                }
            }
        }

        private void listViewStrips_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (e.IsSelected)
            {
                if (_mSelectedStripIndex != e.ItemIndex)
                {
                    mSetStripIndex(e.ItemIndex);
                    mUpdateStripList(false);
                }
                mUpdateWithOverlay();
            }
        }

        private void listViewMeasurements_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (e.IsSelected)
            {
                if (_mSelectedMeasurementIndex != e.ItemIndex)
                {
                    mSetMeasurementIndex(e.ItemIndex);
                    mUpdateStripList(false);
                }
                mUpdateWithOverlay();
            }
        }
        public static void InvertZOrderOfControls(Control.ControlCollection ControlList)
        {
            // do not process empty control list
            if (ControlList.Count == 0)
                return;
            // only re-order if list is writable
            if (!ControlList.IsReadOnly)
            {
                SortedList<int, Control> sortedChildControls = new SortedList<int, Control>();
                // find all none docked controls and sort in to list
                foreach (Control ctrlChild in ControlList)
                {
                    if (ctrlChild.Dock == DockStyle.None)
                        sortedChildControls.Add(ControlList.GetChildIndex(ctrlChild), ctrlChild);
                }
                // re-order the controls in the parent by swapping z-order of first 
                // and last controls in the list and moving towards the center
                for (int i = 0; i < sortedChildControls.Count / 2; i++)
                {
                    Control ctrlChild1 = sortedChildControls.Values[i];
                    Control ctrlChild2 = sortedChildControls.Values[sortedChildControls.Count - 1 - i];
                    int zOrder1 = ControlList.GetChildIndex(ctrlChild1);
                    int zOrder2 = ControlList.GetChildIndex(ctrlChild2);
                    ControlList.SetChildIndex(ctrlChild1, zOrder2);
                    ControlList.SetChildIndex(ctrlChild2, zOrder1);
                }
            }
            // try to invert the z-order of child controls
            foreach (Control ctrlChild in ControlList)
            {
                try { InvertZOrderOfControls(ctrlChild.Controls); }
                catch { }
            }
        }

        [System.Runtime.InteropServices.DllImportAttribute("gdi32.dll")]
        private static extern int BitBlt(
     IntPtr hdcDest,     // handle to destination DC (device context)
     int nXDest,         // x-coord of destination upper-left corner
     int nYDest,         // y-coord of destination upper-left corner
     int nWidth,         // width of destination rectangle
     int nHeight,        // height of destination rectangle
     IntPtr hdcSrc,      // handle to source DC
     int nXSrc,          // x-coordinate of source upper-left corner
     int nYSrc,          // y-coordinate of source upper-left corner
     System.Int32 dwRop  // raster operation code
     );

        [System.Runtime.InteropServices.DllImportAttribute("gdi32.dll")]
        public static extern IntPtr SelectObject(IntPtr hdc, IntPtr obj);

        [System.Runtime.InteropServices.DllImportAttribute("gdi32.dll")]
        public static extern void DeleteObject(IntPtr obj);

        void mPanelToClipBoard(Control APanel)
        {
            try
            {
                _mPrintBitmap = new Bitmap(APanel.Width, APanel.Height);

                using (Graphics G = Graphics.FromImage(_mPrintBitmap))
                {
                    G.Clear(Color.White);           // needed for init
                }

                Graphics g1 = APanel.CreateGraphics();
                Image MyImage = new Bitmap(APanel.ClientRectangle.Width, APanel.ClientRectangle.Height, g1);
                Graphics g2 = Graphics.FromImage(_mPrintBitmap);
                IntPtr dc1 = g1.GetHdc();
                IntPtr dc2 = g2.GetHdc();
                BitBlt(dc2, 0, 0, this.ClientRectangle.Width, this.ClientRectangle.Height, dc1, 0, 0, 13369376);
                g1.ReleaseHdc(dc1);
                g2.ReleaseHdc(dc2);

                Clipboard.SetImage(_mPrintBitmap);

            }
            catch (Exception ex)
            {
                CProgram.sLogException("Image to Clipboard error", ex);
            }
        }
        void mPanelToClipBoard2(Control APanel, bool AbInvert)
        {
            try
            {
                _mPrintBitmap = new Bitmap(APanel.Width, APanel.Height);

                using (Graphics G = Graphics.FromImage(_mPrintBitmap))
                {
                    G.Clear(Color.White);           // needed for init
                }

                if (AbInvert) InvertZOrderOfControls(APanel.Controls);
                APanel.DrawToBitmap(_mPrintBitmap, new System.Drawing.Rectangle(0, 0, APanel.Width, APanel.Height));
                if (AbInvert) InvertZOrderOfControls(APanel.Controls);

                Clipboard.SetImage(_mPrintBitmap);

            }
            catch (Exception ex)
            {
                CProgram.sLogException("Image to Clipboard error", ex);
            }
        }

        private void mPrintImage()
        {
            try
            {
                PrintDocument pd = new PrintDocument();

                pd.PrintPage += mPrintPage;
                //here to select the printer attached to user PC
                PrintDialog printDialog1 = new PrintDialog();

                if (printDialog1 != null)
                {
                    printDialog1.Document = pd;
                    DialogResult result = printDialog1.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        pd.Print();//this will trigger the Print Event handeler PrintPage
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        //The Print Event handeler
        private void mPrintPage(object o, PrintPageEventArgs e)
        {
            try
            {
                if (_mPrintBitmap != null && _mPrintBitmap.Width > 0 && _mPrintBitmap.Height > 0)
                {
                    //Adjust the size of the image to the page to print the full image without loosing any part of it
                    Rectangle m = e.MarginBounds;

                    if ((double)_mPrintBitmap.Width / (double)_mPrintBitmap.Height > (double)m.Width / (double)m.Height) // image is wider
                    {
                        m.Height = (int)((double)_mPrintBitmap.Height / (double)_mPrintBitmap.Width * (double)m.Width);
                    }
                    else
                    {
                        m.Width = (int)((double)_mPrintBitmap.Width / (double)_mPrintBitmap.Height * (double)m.Height);
                    }
                    e.Graphics.DrawImage(_mPrintBitmap, m);
                }
            }
            catch (Exception)
            {

            }
        }

        public Image mPrintPanelToClipboard()
        {
            return null;
        }

        private void buttonPrint_Click(object sender, EventArgs e)
        {
            if (mbSaveStripAnalysis())
            {
                if (mbPrintEntered)
                {
                    CProgram.sLogError("Analyze form detects reentry of print function");
                }
                else
                {
                    mbPrintEntered = true;
                    mPrint();
                    mbPrintEntered = false;
                }
            }
        }
        private void mPrint()
        {
            if (_mRecAnalysisFile._mStripList != null && _mRecordDb != null && _mRecAnalysisFile._mStripList.Count == 0)
            {
                // need at least one strip for findings print
                mSetStripIndex(_mRecAnalysisFile._mStripList.Count);
                _mRecAnalysisFile.mAddStrip(_mZoomChannel, _mZoomChart.mGetStartTimeSec(), _mZoomChart.mGetStripTimeSec(),
                         _mZoomChart.mGetStartA(), _mZoomChart.mGetEndA(), _mZoomImage);
                mUpdateStripList(true);
            }

            mbGetAnalysisValues();
            mbLoadBaseLine();

            /*            bool bInitials = _mCreadedByInitials != null && _mCreadedByInitials.Length > 0;

                        if (false == bInitials)
                        {
                            if (CProgram.sbReqLabel("Print pdf", "User Initials", ref _mCreadedByInitials, ""))
                            {
                                bInitials = _mCreadedByInitials != null && _mCreadedByInitials.Length > 0;
                            }
                        }
                        labelUser.Text = _mCreadedByInitials;
                        if ( bInitials )
            */
            labelUser.Text = CProgram.sGetProgUserInitials();
            string printInitials;
            string strChanged = _mbChanged ? "Saved" : "Current";

            if (CProgram.sbEnterProgUserArea(true)
                && FormEventBoard.sbGetPrintUserInitials(strChanged + " Analysis", out printInitials, _mbChanged, _mPrevTechnician))
            {
                bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;
                bool b12 = _mRecordDb != null && _mRecordDb.mNrSignals >= 12 ? !bCtrl : bCtrl;

                bool bAnonymize = checkBoxAnonymize.Checked;
                bool bDisclosure = checkBoxAddDisclosure.Checked;
                bool bHoleStrip = checkBoxHoleStrip.Checked;
                bool bAllChannels = checkBoxAllChannels.Checked;
                bool bCh2 = checkBoxCh2.Checked;
                bool bBlackWhite = checkBoxBlackWhite.Checked;
                UInt16 stripSec = 60;
                bool bQCd = FormEventBoard._sbQCdEnabled && checkBoxQCD.Checked;

                UInt16.TryParse(comboBoxStripSec.Text, out stripSec);

                if (b12)
                {
                    DTriageTimerMode prevMode = FormEventBoard.sSetTriageTimerEntering(DTriageTimerMode.Print, "Print12 " + strChanged + " Analysis by " + printInitials);

                    using (CPrintAnalys12Form form = new CPrintAnalys12Form(this, true, CProgram.sGetProgUserInitials(), bAnonymize, bBlackWhite, bQCd))
                    {
                        if (form != null)
                        {
                            form.ShowDialog();
                        }
                    }
                    FormEventBoard.sSetTriageTimerLeaving(prevMode, "Print12 Analysis");
                }
                else
                {
                    DTriageTimerMode prevMode = FormEventBoard.sSetTriageTimerEntering(DTriageTimerMode.Print, "Print " + strChanged + " Analysis for " + printInitials);

                    using (CPrintAnalysisForm form = new CPrintAnalysisForm(this, true, printInitials,
                                    bDisclosure, bHoleStrip, bAllChannels, stripSec, bCh2, bAnonymize, bBlackWhite, bQCd))
                    {
                        if (form != null && false == form.IsDisposed)
                        {
                            form.ShowDialog();
                        }
                    }
                    FormEventBoard.sSetTriageTimerLeaving(prevMode, "Print Analysis");
                }
                CProgram.sMemoryCleanup(true, true);
            }
        }

        /*        string mMakeAnalysisFullFileName()
                {
                    string fullName = !!!!!!mMakeAnalysisFullFileName();

                    return fullName;
                }
        */
        private void mSaveRecAnalysis()
        {
            if (_mbChanged)
            {
                if (_mRecAnalysis != null && _mRecordDb != null && _mRecordIndex > 0)
                {
                    if (_mRecAnalysis._mSeqInRecording == 0)
                    {
                        _mRecAnalysis._mSeqInRecording = ++_mRecordDb.mNrAnalysis;
                    }
                    if (_mRecordDb.mNrAnalysis < _mRecAnalysis._mSeqInRecording)
                    {
                        _mRecordDb.mNrAnalysis = _mRecAnalysis._mSeqInRecording;
                    }
                    _mRecAnalysis._mStudy_IX = _mRecordDb.mStudy_IX;
                    if (_mRecAnalysis._mStudy_IX == 0 && _mStudyInfo != null)
                    {
                        _mRecAnalysis._mStudy_IX = _mStudyInfo.mIndex_KEY;
                    }
                    _mRecAnalysis._mMeasuredUTC = DateTime.UtcNow;
                    _mRecAnalysis._mTechnician_IX = CProgram.sGetProgUserIX();
                    _mRecAnalysis._mRecording_IX = _mRecordIndex;

                    if (_mRecAnalysis.mIndex_KEY == 0)
                    {
                        _mRecAnalysis.mbDoSqlInsert();
                    }
                    else
                    {
                        _mRecAnalysis.mbDoSqlUpdate(_mRecAnalysis.mMaskValid, true);
                    }
                }
                if (_sbSaveAnStripImage)
                {
                    mSaveStripImages();
                }
                string fullName = Path.Combine(_mRecordDir, mMakeAnalisisFileName());// A1
                _mRecAnalysisFile.mbSaveMeasureFile(fullName, _mRecordIndex, _mAnalysisSeqNr);
            }
        }
        public void mSaveStripImages()
        {
            int iStrip = 0;
            string fileName, fullName;
            int t, d;

            if (_mRecordIndex > 0)
            {
                foreach (CMeasureStrip ms in _mRecAnalysisFile._mStripList)
                {
                    ++iStrip;

                    if (ms._mStripImage != null)
                    {
                        t = (int)ms._mStartTimeSec;
                        d = (int)ms._mDurationSec;

                        fileName = "Analysis-R" + _mRecordIndex.ToString() + "-A" + _mAnalysisSeqNr.ToString() + "_C" + ms._mChannel.ToString("D2")
                            + "_T" + t.ToString("D5") + "_D" + d.ToString("D5") + ".png";
                        fullName = Path.Combine(_mRecordDir, fileName);
                        try
                        {
                            ms._mStripImage.Save(fullName, System.Drawing.Imaging.ImageFormat.Png);
                            ms._mStripImageName = fileName; // set the name of the saved image
                        }
                        catch (Exception ex)
                        {
                            CProgram.sLogError("Save image failed:" + fullName);
                        }
                    }

                }
            }
        }

        private CRecAnalysis mGetActiveAnalysis()
        {
            CRecAnalysis analysis = _mRecAnalysis;

            if (_mRecAnalysisFile._mStripList != null && _mRecordDb != null)
            {
                if (_mRecAnalysis == null && _mRecAnalysisFile._mStripList.Count > 0)
                {
                    _mRecAnalysis = _mRecAnalysisFile._mStripList[0]._mRecAnalysis;
                }
                if (_mRecAnalysis == null)
                {
                    _mRecAnalysis = new CRecAnalysis(CDvtmsData.sGetDBaseConnection());
                }

                if (_mRecAnalysisFile._mStripList.Count == 0)
                {
                    // need at least one strip for findings print
                    mSetStripIndex(_mRecAnalysisFile._mStripList.Count);
                    _mRecAnalysisFile.mAddStrip(_mZoomChannel, _mZoomChart.mGetStartTimeSec(), _mZoomChart.mGetStripTimeSec(),
                             _mZoomChart.mGetStartA(), _mZoomChart.mGetEndA(), _mZoomImage);
                    if (_mRecAnalysisFile._mStripList.Count > 0)
                    {
                        _mRecAnalysisFile._mStripList[0]._mRecAnalysis = _mRecAnalysis;
                    }
                    analysis = _mRecAnalysis;
                    mUpdateStripList(true);
                }
                else
                {
                    analysis = _mRecAnalysisFile._mStripList[0]._mRecAnalysis;
                    if (analysis == null)
                    {
                        _mRecAnalysisFile._mStripList[0]._mRecAnalysis = _mRecAnalysis;
                        analysis = _mRecAnalysis;
                    }
                }


            } else if (_mRecAnalysis == null)
            {
                _mRecAnalysis = new CRecAnalysis(CDvtmsData.sGetDBaseConnection());
                analysis = _mRecAnalysis;
            }
            return analysis;
        }


        private void mSetChanged()
        {
            if (_mbInitScreenDone && false == _mbChanged && false == _mbReadOnly)
            {
                _mbChanged = true;
                labelPrevTech.BackColor = Color.LightGray;
                labelUser.BackColor = Color.LightGreen;
                buttonSave.Enabled = true;
            }
        }


        private bool mbSaveStripAnalysis()
        {
            bool bSave = true;
            if (false == _mbReadOnly && _mbChanged)
            {
                bSave = true;

                string fullName = Path.Combine(_mRecordDir, mMakeAnalisisFileName());

                if (_mRecAnalysisFile._mStripList != null && _mRecordDb != null && _mRecAnalysisFile._mStripList.Count == 0)
                {
                    // need at least one strip for findings print
                    mSetStripIndex(_mRecAnalysisFile._mStripList.Count);
                    _mRecAnalysisFile.mAddStrip(_mZoomChannel, _mZoomChart.mGetStartTimeSec(), _mZoomChart.mGetStripTimeSec(),
                             _mZoomChart.mGetStartA(), _mZoomChart.mGetEndA(), _mZoomImage);
                    mUpdateStripList(true);
                }

                bool bAn = mbGetAnalysisValues();

                if (_mNewState == (UInt16)DRecState.QCd)
                {
                    if (FormEventBoard._sbQCdEnabled)
                    {
                        if (checkBoxQCD.Checked)
                        {
                            // ok 
                        }
                        else
                        {
                            bSave = false;
                            CProgram.sAskOk(this, "Save", "QCd save only with QCd check marked!");
                        }
                    }
                    else
                    {
                        bSave = false;
                        CProgram.sAskOk(this, "Save", "QCd not allowed!");
                    }
                }
                if (bSave)
                {
                    mSaveRecAnalysis();
                    _mRecAnalysisFile.mbSaveMeasureFile(fullName, _mRecordIndex, _mAnalysisSeqNr);
                }
            }
            return bSave;
        }
        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (false == _mbReadOnly)
            {
                mbSaveStripAnalysis();
                /*
                if (_mRecAnalysisFile._mStripList != null && _mRecordDb != null && _mRecAnalysisFile._mStripList.Count == 0)
                {
                    // need at least one strip for findings print
                    mSetStripIndex(_mRecAnalysisFile._mStripList.Count);
                    _mRecAnalysisFile.mAddStrip(_mZoomChannel, _mZoomChart.mGetStartTimeSec(), _mZoomChart.mGetStripTimeSec(),
                             _mZoomChart.mGetStartA(), _mZoomChart.mGetEndA(), _mZoomImage);
                    mUpdateStripList();
                }

                mGetAnalysisValues();
                mSaveRecAnalysis();
                */
            }
        }

        private void buttonSaveOk_Click(object sender, EventArgs e)
        {
            if (false == _mbReadOnly && mbSaveStripAnalysis())
            {
                /*
                string fullName = Path.Combine(_mRecordDir, "Analysis-R" + _mRecordIndex.ToString() + "-A" + _mAnalysisSeqNr.ToString() + ".ini");

                if (_mRecAnalysisFile._mStripList != null && _mRecordDb != null && _mRecAnalysisFile._mStripList.Count == 0)
                 {
                    // need at least one strip for findings print
                    mSetStripIndex(_mRecAnalysisFile._mStripList.Count);
                    _mRecAnalysisFile.mAddStrip(_mZoomChannel, _mZoomChart.mGetStartTimeSec(), _mZoomChart.mGetStripTimeSec(),
                             _mZoomChart.mGetStartA(), _mZoomChart.mGetEndA(), _mZoomImage);
                    mUpdateStripList();
                }

                mGetAnalysisValues();
                mSaveRecAnalysis();
                _mRecAnalysisFile.mbSaveMeasureFile(fullName, _mRecordIndex, _mAnalysisSeqNr);
    */
                // save record changes 

                if (_mEventBoardForm != null)
                {
                    UInt64 saveMask = CSqlDataTableRow.sGetMask((UInt16)DRecordMitVars.NrAnalysis)
                        | CSqlDataTableRow.sGetMask((UInt16)DRecordMitVars.StudyPermissions);

                    if (_mRecAnalysisFile != null)
                    {
                        _mRecordDb.mInvertChannelsMask = _mRecAnalysisFile._mInvChannelsMask;
                        saveMask |= CSqlDataTableRow.sGetMask((UInt16)DRecordMitVars.InvertChannelsMask);
                    }

                    if (_mNewState > 0) _mRecordDb.mRecState = _mNewState;

                    if (_mEventBoardForm.mbTriageUpdateDbState(_mRecordDb, saveMask))
                    {
                        Close();
                    }
                }
            }
        }

        private void FormAnalyze_FormClosing(object sender, FormClosingEventArgs e)
        {
            mClosePageScan();
            _mbUpdatingStripList = true; // disable updating
            _mbUpdatingMeasurementList = true;
            _mbMeasureLineDrawn = false;
            if (listViewStats != null && listViewStats.Items != null) listViewStats.Items.Clear();
            if (listViewStrips != null && listViewStrips.Items != null) listViewStrips.Items.Clear();
            if (listViewMeasurements != null && listViewMeasurements.Items != null) listViewMeasurements.Items.Clear();

            //                  public static bool sbLockFilePath(string APath, string ALockName, ref StreamWriter ArLockStream)
            if (_sbTriageLockAnRec)
            {
                CProgram.sbUnLockFilePath(_mLockFilePath, _mLockName, ref _mLockStream);
            }

        }

        private void buttonHelpDesk_MouseClick(object sender, MouseEventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            string exeName = bCtrl ? "helpdesk2.exe" : "helpdesk.exe";
            string exePath = Path.Combine("..\\helpdesk\\", exeName);

            if (File.Exists(exePath))
            {
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = exePath;
                //startInfo.Arguments;
                Process.Start(startInfo);
            }
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            //           CPrintAnalysisForm.sStoreBaseLine(mRecordDb, mRecordFile, mRecAnalysis, mRecAnalysisFile, mGetSelectedMeasureStrip());
        }

        private void toolStripBaseLine_Click(object sender, EventArgs e)
        {
            FormAnalyze formAnalyze = new FormAnalyze(_mEventBoardForm);

            if (formAnalyze != null && _mBaseLineRecordDb != null)
            {
                try
                {
                    UInt32 recordIndex = _mBaseLineRecordDb.mIndex_KEY;
                    bool bNew = false;
                    UInt32 analysisIndex = 0;
                    UInt16 analysisNr = 0;

                    if (mbLoadBaseLine())
                    {
                        formAnalyze.mSetRecordIndex(true, recordIndex, bNew, analysisIndex, analysisNr, _mDbConnection, _mRecordingDir);

                        formAnalyze._mRecordDb = _mBaseLineRecordDb;
                        formAnalyze._mRecordFile = _mBaseLineRecordFile;
                        formAnalyze._mRecAnalysis = _mBaseLineRecAnalysis;
                        formAnalyze._mRecAnalysisFile = _mBaseLineRecAnalysisFile;
                        formAnalyze._mAnalysisSeqNr = 0;
                        formAnalyze._mRecordIndex = recordIndex;
                        formAnalyze._mDeviceInfo = _mDeviceInfo;
                        formAnalyze._mStudyInfo = _mStudyInfo;
                        formAnalyze._mPatientInfo = _mPatientInfo;

                        try
                        {
                            formAnalyze.mbInitCharts(false);

                            formAnalyze.mInitScreens();

                            formAnalyze.Show();
                            formAnalyze = null; // opened stand alone
                            //                                        formAnalyze.ShowDialog();
                        }
                        catch (Exception ex)
                        {
                            CProgram.sLogException("Base Analysis Form run", ex);
                        }
                    }

                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Base Analyze form", ex);
                }
            }
            if (formAnalyze != null)
            {
                formAnalyze.mReleaseLock();
            }
        }

        private void comboBoxState_TextChanged(object sender, EventArgs e)
        {
            //mSetChanged();
            mGetNewState();
        }

        private void FormAnalyze_Shown(object sender, EventArgs e)
        {
            BringToFront();

            double totalSec = (DateTime.Now - _mCreateFormStartDT).TotalSeconds;

            labelShowTotalSec.Text = "t=" + totalSec.ToString("0.000") + "sec";
            CProgram.sLogLine("R" + _mRecordIndex.ToString() + " AnalysisForm " + _mCreateFormSec.ToString("0.000") + "sec, load " + _mLoadFormSec.ToString("0.000") + "sec, total  " + totalSec.ToString("0.000") + "sec");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            /*            mGetAnalysisValues();
                        mbLoadBaseLine();

                        CPrintStudyEventsForm form = new CPrintStudyEventsForm(this, true);
                        if (form != null)
                        {
                            form.ShowDialog();
                        }
            */
        }

        private void buttonFindings_Click(object sender, EventArgs e)
        {
            if (_mStudyInfo != null && _mStudyInfo.mIndex_KEY != 0)
            {
                CPreviousFindingsForm form = new CPreviousFindingsForm(_mStudyInfo.mIndex_KEY, false, 0, _mAnalysisIndex);

                if (form != null)
                {
                    form.ShowDialog();
                }
            }

        }

        private void buttonFindingsRhythm_Click(object sender, EventArgs e)
        {
            mGetActiveAnalysis();

            if (_mRecAnalysis != null)
            {
                CRhythmForm form = new CRhythmForm(_mRecAnalysis._mAnalysisRhythmSet);

                if (form != null)
                {
                    int x = this.Left + this.Width - form.Width;
                    int y = this.Top + this.Height - form.Height;

                    if (x < 0) x = 0;
                    if (y < 0) y = 0;

                    form.Location = new Point(x, y);
                    form.ShowDialog();

                    if (form._mSelectedSet != null)
                    {
                        _mRecAnalysis._mAnalysisRhythmSet = form._mSelectedSet;
                        mUpdateRythmButton();
                        mSetChanged();
                    }
                }
            }
        }
        void mAnalyzeUpgradeState(String ACode, DRecState ANewState, bool AbSingleCode)
        {
            if (false == _mbReadOnly)
            {
                mSetChanged();
                if (_mRecAnalysisFile._mStripList != null && _mRecordDb != null && _mRecAnalysisFile._mStripList.Count == 0)
                {
                    // just add at least one strip so that it is visable on the study, normally it should already be measured
                    mSetStripIndex(_mRecAnalysisFile._mStripList.Count);
                    _mRecAnalysisFile.mAddStrip(_mZoomChannel, _mZoomChart.mGetStartTimeSec(), _mZoomChart.mGetStripTimeSec(),
                             _mZoomChart.mGetStartA(), _mZoomChart.mGetEndA(), _mZoomImage);
                    mUpdateStripList(true);
                }
                if (AbSingleCode)
                {
                    mbAnClearActiveSet();
                }
                mbAnSetActive(ACode);
                if (_mRecordDb.mRecState <= (UInt16)DRecState.Analyzed)
                {
                    bool bAn = mbGetAnalysisValues();

                    mSetNewState((UInt16)ANewState);

                    if (FormEventBoard._sbQCdEnabled && checkBoxQCD.Checked)
                    {
                        if (CRecordMit.sbIsStateAnalyzed(_mNewState, false))
                        {
                            mSetNewState((UInt16)DRecState.QCd);
                        }
                    }
                }
            }
        }


        /*       void mAnalyzeUpgradeState(RadioButton ARadioButton )
               {
                   if (_mRecAnalysisFile._mStripList != null && _mRecordDb != null && _mRecAnalysisFile._mStripList.Count == 0 && ARadioButton.Checked)
                   {
                       // just add at least one strip so that it is visable on the study, normally it should already be measured
                       mSetStripIndex(_mRecAnalysisFile._mStripList.Count);
                       _mRecAnalysisFile.mAddStrip(_mZoomChannel, _mZoomChart.mGetStartTimeSec(), _mZoomChart.mGetStripTimeSec(),
                                _mZoomChart.mGetStartA(), _mZoomChart.mGetEndA(), _mZoomImage);
                       mUpdateStripList();
                   }

                   if (_mRecordDb.mRecState <= (UInt16)DRecState.Analyzed
                   && ARadioButton != null && ARadioButton.Checked )
                   {
                       mGetAnalysisValues();
                       bool b = ARadioButton.Text == "BL" || ARadioButton.Text.StartsWith("Base");
                       mSetNewState(b ? (UInt16)DRecState.BaseLine : (UInt16)DRecState.Analyzed);
                   }
               }
       


        private void radioButtonF1_CheckedChanged(object sender, EventArgs e)
        {
            mAnalyzeUpgradeState( radioButtonF1);
        }

        private void radioButtonF2_CheckedChanged(object sender, EventArgs e)
        {
            mAnalyzeUpgradeState(radioButtonF2);
        }

        private void radioButtonF3_CheckedChanged(object sender, EventArgs e)
        {
            mAnalyzeUpgradeState(radioButtonF3);
        }

        private void radioButtonF4_CheckedChanged(object sender, EventArgs e)
        {
            mAnalyzeUpgradeState(radioButtonF4);
        }

        private void radioButtonF5_CheckedChanged(object sender, EventArgs e)
        {
            mAnalyzeUpgradeState(radioButtonF5);
        }

        private void radioButtonF6_CheckedChanged(object sender, EventArgs e)
        {
            mAnalyzeUpgradeState(radioButtonF6);
        }
*/
        private void buttonStudyFindings_Click(object sender, EventArgs e)
        {
            if (_mStudyInfo != null && _mStudyInfo.mIndex_KEY != 0)
            {
                CPreviousFindingsForm form = new CPreviousFindingsForm(_mStudyInfo.mIndex_KEY, false, 0, _mAnalysisIndex);

                if (form != null)
                {
                    form.ShowDialog();
                }
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void textBoxFindings_TextChanged(object sender, EventArgs e)
        {

        }

        private void toolStrip3_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void toolStripStrips_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void toolStrip4_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void mOpenMctTrend()
        {
            if (_mStudyInfo != null && _mStudyInfo.mIndex_KEY > 0)
            {
                bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;
                bool bSelectFiles = (Control.ModifierKeys & Keys.Alt) == Keys.Alt;

                if (bCtrl) bCtrl = CProgram.sbAskYesNo(this, "MCT Full debug", "Do full debug logging?");

                DTriageTimerMode prevMode = FormEventBoard.sSetTriageTimerEntering(DTriageTimerMode.DialogSecond, "MCT");

                CMCTTrendDisplay form = new CMCTTrendDisplay(_mStudyInfo.mIndex_KEY, DDeviceType.Unknown, bCtrl, bSelectFiles);

                if (form != null)//&& form.bStatsLoaded)
                {
                    form.Show();
                }
                FormEventBoard.sSetTriageTimerLeaving(prevMode, "MCT");

            }

        }
        private void toolStrip7_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            mOpenMctTrend();
        }

        private void buttonPdfView_Click(object sender, EventArgs e)
        {
            if (_mStudyInfo != null && _mStudyInfo.mIndex_KEY > 0)
            {
                try
                {
                    CStudyReportListForm form = new CStudyReportListForm(_mStudyInfo.mIndex_KEY);

                    if (form != null)
                    {
                        form.ShowDialog();
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Call pdf study list", ex);
                }
            }
        }

        private void toolStrip11_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {

        }

        private void toolStripButtonRecalcStats_Click(object sender, EventArgs e)
        {
            mUpdateStatsList();
        }

        private void toolStripButtonMctTrend_Click(object sender, EventArgs e)
        {
            mOpenMctTrend();
        }

        private void mTriageOpenEventDir(bool AbAltDvxStudy)
        {
            // open explorer wit file at cursor
            try
            {
                string openDir = "";

                if (AbAltDvxStudy && _mRecordDb != null)
                {
                    string rem = _mRecordDb.mRecRemark.mDecrypt();
                    UInt32 dvxStudyIX;

                    if (CDvxEvtRec.sbGetStudyDeviceFromRemark(out openDir, out dvxStudyIX, rem, _mRecordDb.mDeviceID))
                    {
                    }
                }
                else
                {
                    openDir = _mRecordDir;

                    if (_mRecordDir == null || _mRecordingDir.Length < 2)
                    {
                        string casePath;
                        //string filePath;
                        if (_mRecordDb.mbGetCasePath(out casePath))
                        {
                            openDir = Path.Combine(_mRecordingDir, casePath);
                        }
                    }

                }
                if (openDir.Length > 0)
                {
                    ProcessStartInfo startInfo = new ProcessStartInfo();
                    startInfo.FileName = @"explorer";
                    startInfo.Arguments = openDir;
                    Process.Start(startInfo);
                }
            }
            catch (Exception e2)
            {
                CProgram.sLogException("Failed open explorer", e2);
            }
        }


        private void toolStripRecordNumber_DoubleClick(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl) //&& CLicKeyDev.sbDeviceIsProgrammer())
            {
                bool bShift = (Control.ModifierKeys & Keys.Shift) == Keys.Shift;

                mTriageOpenEventDir(bShift);
            }
        }

        private void toolStrip7_ItemClicked_1(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void label8_DoubleClick(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl) //&& CLicKeyDev.sbDeviceIsProgrammer())
            {
                bool bShift = (Control.ModifierKeys & Keys.Shift) == Keys.Shift;

                mTriageOpenEventDir(bShift);
            }
        }

        private void toolStripChannelDuration_Click(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl) //&& CLicKeyDev.sbDeviceIsProgrammer())
            {
                bool bShift = (Control.ModifierKeys & Keys.Shift) == Keys.Shift;
                mTriageOpenEventDir(bShift);
            }

        }

        private void panelChannel4_MouseClick(object sender, MouseEventArgs e)
        {
            float f = e.X / (float)panelChannel4.Width;

            mSetZoomCenterByRelPos(3, f);

        }

        private void panelChannel5_MouseClick(object sender, MouseEventArgs e)
        {
            float f = e.X / (float)panelChannel5.Width;

            mSetZoomCenterByRelPos(4, f);

        }

        private void panelChannel6_MouseClick(object sender, MouseEventArgs e)
        {
            float f = e.X / (float)panelChannel6.Width;

            mSetZoomCenterByRelPos(5, f);

        }

        private void panelChannel7_MouseClick(object sender, MouseEventArgs e)
        {
            float f = e.X / (float)panelChannel7.Width;

            mSetZoomCenterByRelPos(6, f);

        }

        private void panelChannel8_MouseClick(object sender, MouseEventArgs e)
        {
            float f = e.X / (float)panelChannel8.Width;

            mSetZoomCenterByRelPos(7, f);

        }

        private void panelChannel9_MouseClick(object sender, MouseEventArgs e)
        {
            float f = e.X / (float)panelChannel9.Width;

            mSetZoomCenterByRelPos(8, f);

        }

        private void panelChannel10_MouseClick(object sender, MouseEventArgs e)
        {
            float f = e.X / (float)panelChannel10.Width;

            mSetZoomCenterByRelPos(9, f);

        }

        private void panelChannel11_MouseClick(object sender, MouseEventArgs e)
        {
            float f = e.X / (float)panelChannel11.Width;

            mSetZoomCenterByRelPos(10, f);

        }

        private void panelChannel12_MouseClick(object sender, MouseEventArgs e)
        {
            float f = e.X / (float)panelChannel12.Width;

            mSetZoomCenterByRelPos(11, f);
        }

        private void panelChannel5_MouseClick_1(object sender, MouseEventArgs e)
        {
            float f = e.X / (float)panelChannel5.Width;

            mSetZoomCenterByRelPos(4, f);
        }

        private void toolStrip11_ItemClicked_1(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void toolStrip11_ItemClicked_2(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void toolStrip11_ItemClicked_3(object sender, ToolStripItemClickedEventArgs e)
        {


        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void toolStripChannelTimeIncrease_Click(object sender, EventArgs e)
        {
            mChannelsTimeFactor(0.5F);
        }

        private void toolStripChannelTimeDecrease_Click(object sender, EventArgs e)
        {
            mChannelsTimeFactor(2.0F);
        }

        private void toolStripChannelTimeLeft_Click(object sender, EventArgs e)
        {
            mChannelsTimeShift(-_cChannelsTimeShiftFactor);//0.75F);
        }

        private void toolStripChannelTimeRight_Click(object sender, EventArgs e)
        {
            mChannelsTimeShift(+_cChannelsTimeShiftFactor);//0.75F);
        }

        private void toolStripZoomDuration_Click(object sender, EventArgs e)
        {
            if (_mRecordFile != null)
            {
                UInt32 t = (UInt32)(_mZoomDurationSec + 0.4);
                UInt32 tMax = (UInt32)(_mRecordFile.mGetSamplesTotalTime() + 0.5);

                if (tMax < 2) tMax = 2;

                if (CProgram.sbReqUInt32("Zoom strip time", "Zoom strip duration", ref t, "sec", 1, tMax))
                {
                    toolStripZoomDuration.Text = t.ToString();
                    mZoomTimeSet(t);
                }
            }
        }

        private void toolStripChannelDuration_Click_1(object sender, EventArgs e)
        {
            if (_mZoomChart != null && _mRecordFile != null)
            {
                _mChannelsDurationSec = _mRecordFile.mGetSamplesTotalTime();
                _mChannelsCenterSec = 0;
                mInitStripChannels();
            }
        }

        private void ScrollBarChannel_ValueChanged(object sender, EventArgs e)
        {
            mChangedChannelsScrollbar();
        }

        private void labelEventTime_Click(object sender, EventArgs e)
        {
            /* double click active             if(_mRecordDb != null)
                        {
                            float t = _mRecordDb.mEventTimeSec;

                            if (t < 0) t = 0;
                            else
                            {
                                float duration = _mRecordFile.mGetSamplesTotalTime();
                                if (t > duration)
                                {
                                    t = duration;
                                }
                            }
                            mSetZoomCenter(t);
                            mSetChannelsCenter(t);
                            mInitZoomChannel();
                            mInitStripChannels();

                        }
                        */
        }

        private void listViewStats_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {

        }

        private void button8_Click(object sender, EventArgs e)
        {

        }

        private void button9_Click(object sender, EventArgs e)
        {

        }

        private void toolStripFullStrip_Click(object sender, EventArgs e)
        {
            mZoomTimeMax();
        }

        private void mUpdateFullDisclosure()
        {
            bool b = checkBoxAddDisclosure.Checked;
            checkBoxHoleStrip.Enabled = b;
            //checkBoxAllChannels.Enabled = b;
            labelWholeStrip.Enabled = b;
            //            labeAllChannels.Enabled = b;
            //            labelStripTime.Enabled = b;
            //            comboBoxStripSec.Enabled = b;
        }

        private void checkBoxAddDisclosure_CheckedChanged(object sender, EventArgs e)
        {
            mUpdateFullDisclosure();
        }

        public void mSetFullStripTime()
        {
            if (_mRecordFile != null)
            {
                UInt32 t = (UInt32)(_mChannelsDurationSec + 0.4);
                UInt32 tMax = (UInt32)(_mRecordFile.mGetSamplesTotalTime() + 0.5);

                if (tMax < 2) tMax = 2;

                if (CProgram.sbReqUInt32("Channels strip time", "channels strip duration", ref t, "sec", 1, tMax))
                {
                    toolStripChannelDuration.Text = t.ToString();
                    mChannelsTimeSet(t);
                }
            }

        }
        private void toolStripChannelPeriod_ItemClicked(object sender, EventArgs e)
        {
            mSetFullStripTime();
        }


        private void labelEventTime_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            mEventDoubleClick();
        }
        private void mEventDoubleClick()
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;
            bool bShift = (Control.ModifierKeys & Keys.Shift) == Keys.Shift;

            if (bCtrl) //&& CLicKeyDev.sbDeviceIsProgrammer())
            {

                mTriageOpenEventDir(bShift);
            }
            else
            {
                bool bAlt = (Control.ModifierKeys & Keys.Alt) == Keys.Alt;
                if (bAlt && _mRecordDb != null)
                {
                    string s = "R#" + _mRecordDb.mIndex_KEY.ToString()
                        + " S" + _mRecordDb.mStudy_IX.ToString() + "." + _mRecordDb.mSeqNrInStudy.ToString()
                        + " D=" + _mRecordDb.mDevice_IX.ToString()
                        + "_" + _mRecordDb.mDeviceID
                        + " strip=" + _mRecordDb.mRecDurationSec.ToString("0.0") + "sec "
                        + _mRecordDb.mSampleFrequency.ToString() + " Sps " + _mRecordDb.mNrSignals.ToString() + " signals\r\n"
                        + "db{ baseUTC=" + CProgram.sDateTimeToYMDHMS(_mRecordDb.mBaseUTC)
                        + " TZone="
                        + CProgram.sTimeZoneOffsetStringLong((Int16)_mRecordDb.mTimeZoneOffsetMin)
                        + _mRecordDb.mGetTimeZoneMovedChar() + " event " + _mRecordDb.mGetEventSec().ToString("0.0")
                        + " eventDT= " + CProgram.sDateTimeToYMDHMS(_mRecordDb.mGetDeviceTime(_mRecordDb.mGetEventUTC()));
                    s += "\r\nrem=" + _mRecordDb.mRecRemark.mDecrypt();
                    if (_mRecordFile != null)
                    {
                        s += "\r\nfile " + _mRecordFile.mFileName + " R#" + _mRecordFile.mIndex_KEY.ToString()
                         + " D=" + _mRecordFile.mDevice_IX.ToString()
                        + "_" + _mRecordFile.mDeviceID
                        + " strip=" + _mRecordFile.mGetSamplesTotalTime().ToString("0.0") + "sec "
                        + _mRecordFile.mSampleFrequency.ToString() + " Sps\r\n"
                        + "file{ baseUTC=" + CProgram.sDateTimeToYMDHMS(_mRecordFile.mBaseUTC)
                        + " TZone="
                        + CProgram.sTimeZoneOffsetStringLong((Int16)_mRecordFile.mTimeZoneOffsetMin)
                        + _mRecordFile.mGetTimeZoneMovedChar() + " event " + _mRecordFile.mGetEventSec().ToString("0.0")
                        + " eventDT= " + CProgram.sDateTimeToYMDHMS(_mRecordFile.mGetDeviceTime(_mRecordFile.mGetEventUTC()));

                        double deltaSec = (_mRecordDb.mEventUTC - _mRecordFile.mBaseUTC).TotalSeconds;
                        s += "\r\n delta TZone " + (_mRecordFile.mTimeZoneOffsetMin - _mRecordDb.mTimeZoneOffsetMin).ToString()
                            + " min, dEvent=" + deltaSec.ToString("0.0") + " sec";

                        if (deltaSec < -1 || deltaSec > _mRecordFile.mGetSamplesTotalTime() + 1)
                        {
                            s += " not within strip!";
                        }

                        string fullDataInfo = _mRecordFile.mGetDataInfo("File", -1, -1);
                        s += "\r\n" + fullDataInfo;

                        string stripDataInfo = _mRecordFile.mGetDataInfo("Strip", _mChannelsCenterSec - _mChannelsDurationSec / 2, _mChannelsCenterSec + _mChannelsDurationSec / 2);
                        s += "\r\n" + stripDataInfo;

                        string zoomDataInfo = _mRecordFile.mGetDataInfo("Zoom", _mZoomCenterSec - _mZoomDurationSec / 2, _mZoomCenterSec + _mZoomDurationSec / 2);
                        s += "\r\n" + zoomDataInfo;

                        if (bShift)
                        {
                            float testLevel = 0.0F;
                            float smoothPeriod = 1.0F;
                            float hysteresisLevel = 0.1F;
                            bool bDelta = false;

                            string title = "Count pulses";

                            if (CProgram.sbReqBool(title, "Use derived (delta)", ref bDelta))
                            {
                                if (bDelta)
                                {
                                    title = "Count derived flanks";
                                }
                                if (CProgram.sbReqFloat(title, "test level", ref testLevel, "0.00", "mV", -100, 100)
                                && CProgram.sbReqFloat(title, "hysteresis level", ref hysteresisLevel, "0.00", "mV", -100, 100)
                                && CProgram.sbReqFloat(title, "smooth period ", ref smoothPeriod, "0.00", "sec", 0, 100))
                                {

                                    /*                                string fullCountInfo = _mRecordFile.mGetCountInfo("File", -1, -1,
                                                                        testLevel, hysteresisLevel, smoothPeriod, 999);
                                                                    s += "\r\n" + fullCountInfo;

                                                                    string stripCountInfo = _mRecordFile.mGetCountInfo("Strip", _mChannelsCenterSec - _mChannelsDurationSec / 2, _mChannelsCenterSec + _mChannelsDurationSec / 2,
                                                                        testLevel, hysteresisLevel, smoothPeriod);
                                                                    s += "\r\n" + stripCountInfo;
                                    */
                                    string zoomCountInfo = _mRecordFile.mGetCountInfo("Zoom", _mZoomCenterSec - _mZoomDurationSec / 2, _mZoomCenterSec + _mZoomDurationSec / 2,
                                        bDelta, testLevel, hysteresisLevel, smoothPeriod, _mZoomChannel);
                                    s += "\r\n" + zoomCountInfo;

                                }
                            }
                        }
                        if (CLicKeyDev.sbDeviceIsProgrammer())
                        {
                            string compressInfo = _mRecordFile.mGetCompressInfo(1);

                            s += "\r\n" + compressInfo;

                            compressInfo = _mRecordFile.mGetCompressInfo(4);

                            s += "\r\n" + compressInfo;

                            compressInfo = _mRecordFile.mGetCompressInfo(8);

                            s += "\r\n" + compressInfo;
                            compressInfo = _mRecordFile.mGetCompressInfo(16);

                            s += "\r\n" + compressInfo;

                        }

                    }
                    ReqTextForm.sbShowText("Analysis Record info: R" + _mRecordDb.mIndex_KEY.ToString(), "info", s);
                }
                else
                {
                    // normal double click -> jump to event time
                    if (_mRecordDb != null)
                    {
                        float t = _mRecordDb.mEventTimeSec;

                        if (t < 0) t = 0;
                        else
                        {
                            float duration = _mRecordFile.mGetSamplesTotalTime();
                            if (t > duration)
                            {
                                t = duration;
                            }
                        }
                        mSetZoomCenter(t);
                        mSetChannelsCenter(t);
                        mInitZoomChannel();
                        mInitStripChannels();
                    }
                }
            }
        }

        private void labelEventPriority_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl) //&& CLicKeyDev.sbDeviceIsProgrammer())
            {
                bool bShift = (Control.ModifierKeys & Keys.Shift) == Keys.Shift;

                mTriageOpenEventDir(bShift);
            }
        }

        private void labelEventLabel_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl) //&& CLicKeyDev.sbDeviceIsProgrammer())
            {
                bool bShift = (Control.ModifierKeys & Keys.Shift) == Keys.Shift;

                mTriageOpenEventDir(bShift);
            }
        }

        private void toolStripZoomChannel_Click(object sender, EventArgs e)
        {
            if (_mNrSignals > 1)
            {
                if (_mNrSignals == 2)
                {
                    _mZoomChannel = (UInt16)(_mZoomChannel == 0 ? 1 : 0);
                    mUpdateZoomChannel();
                    mUpdateStripChannels();
                }
                else
                {
                    Point point = panel31.PointToScreen(new Point(30, 0));
                    contextMenuSelChannel.Show(point);
                }
            }
        }

        private void toolStrip10_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void contextMenuSelChannel_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (e.ClickedItem != null && _mRecordFile != null && contextMenuSelChannel.Items != null)
            {
                string text = e.ClickedItem.Text;
                int index = contextMenuSelChannel.Items.IndexOf(e.ClickedItem);

                if (index >= 0)
                {
                    _mZoomChannel = (UInt16)index;
                    mUpdateZoomChannel();
                    mUpdateStripChannels();
                }
            }
        }

        private void contextMenuSelChannel_Opening(object sender, CancelEventArgs e)
        {

        }

        private void toolStripStrips_ItemClicked_1(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void checkBoxAddDisclosure_CheckedChanged_1(object sender, EventArgs e)
        {
            mUpdateFullDisclosure();
        }

        private void buttonSaveMIT16_Click(object sender, EventArgs e)
        {
            //mExportData();
        }
        /*private void mExportData()
        {
            if( _mRecordFile != null )
            {
//                if (checkBoxExportMit16.Checked)
                {
                    string mit16FileName = "R" + _mRecordIndex.ToString("D7") + "_MIT16_" + CProgram.sDateTimeToYMDHMS(DateTime.Now) + ".hea";
                    string mit16Path = Path.Combine(Path.GetDirectoryName(_mRecordFilePath), mit16FileName);

                    _mRecordFile.mbSaveMIT16(mit16Path);
                }
//                if (checkBoxExportEcgSim.Checked)
                {
                    string mecgFileName = "R" + _mRecordIndex.ToString("D7") + "_MECG_" + CProgram.sDateTimeToYMDHMS(DateTime.Now) + ".txt";
                    string mecgPath = Path.Combine(Path.GetDirectoryName(_mRecordFilePath), mecgFileName);
                    _mRecordFile.mbSaveMECG(mecgPath);
                }
//                if (checkBoxExportDvx.Checked)
                {
                    string dvxFileName = "R" + _mRecordIndex.ToString("D7") + "_DvxEcg_" + CProgram.sDateTimeToYMDHMS(DateTime.Now) + ".DvxEcg";
                    string dvxPath = Path.Combine(Path.GetDirectoryName(_mRecordFilePath), dvxFileName);

//                    _mRecordFile.mbSaveDvxEcg(dvxPath, );
                }
            }
        }
        */
        private void buttonAnNormal_Click(object sender, EventArgs e)
        {
            mAnalyzeUpgradeState(CDvtmsData._cFindings_Normal, DRecState.Analyzed, false);
        }

        private void buttonAnTachy_Click(object sender, EventArgs e)
        {
            mAnalyzeUpgradeState(CDvtmsData._cFindings_Tachy, DRecState.Analyzed, false);
        }

        private void buttonAnBrady_Click(object sender, EventArgs e)
        {
            mAnalyzeUpgradeState(CDvtmsData._cFindings_Brady, DRecState.Analyzed, false);
        }

        private void buttonAnPause_Click(object sender, EventArgs e)
        {
            mAnalyzeUpgradeState(CDvtmsData._cFindings_Pause, DRecState.Analyzed, false);
        }

        private void buttonAnAFib_Click(object sender, EventArgs e)
        {
            mAnalyzeUpgradeState(CDvtmsData._cFindings_AF, DRecState.Analyzed, false);
        }

        private void buttonAflut_Click(object sender, EventArgs e)
        {
            mAnalyzeUpgradeState(CDvtmsData._cFindings_AFL, DRecState.Analyzed, false);
        }

        private void buttonAnPAC_Click(object sender, EventArgs e)
        {
            mAnalyzeUpgradeState(CDvtmsData._cFindings_PAC, DRecState.Analyzed, false);
        }

        private void buttonAnPVC_Click(object sender, EventArgs e)
        {
            mAnalyzeUpgradeState(CDvtmsData._cFindings_PVC, DRecState.Analyzed, false);
        }

        private void buttonAnPace_Click(object sender, EventArgs e)
        {
            mAnalyzeUpgradeState(CDvtmsData._cFindings_Pace, DRecState.Analyzed, false);
        }

        private void buttonAnVtach_Click(object sender, EventArgs e)
        {
            mAnalyzeUpgradeState(CDvtmsData._cFindings_VT, DRecState.Analyzed, false);
        }

        private void buttonAnBline_Click(object sender, EventArgs e)
        {
            mAnalyzeUpgradeState(CDvtmsData._cFindings_BL, DRecState.BaseLine, false);
        }

        private void buttonOther_Click(object sender, EventArgs e)
        {
            mAnalyzeUpgradeState(CDvtmsData._cFindings_Other, DRecState.Analyzed, false);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (CFindingsTextSelector.sbEditTextBox(textBoxFindings, "Analyze " + _mAnalyzeSubTitle,
                ref _sbAnalyzeFindingsLoaded, ref _sAnalyzeFindingsOptions, "AnalyzeFindings.txt"))
            {
                mSetChanged();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void dVX1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_mRecordFile != null)
            {
                UInt16 nrBlocks = 0;
                UInt16 nrBits = 24;
                bool bCurTime = true;

                if (CProgram.sbReqUInt16("Save dvx1 file", "Nr blocks (0=one file)", ref nrBlocks, "/min", 0, 60)
                    && CProgram.sbReqUInt16("Save dvx1 file", "Nr bits", ref nrBits, "bits", 0, 32)
                    && CProgram.sbReqBool("Save dvx1 file", "Current time", ref bCurTime))
                {
                    DateTime startTime = DateTime.UtcNow;
                    DateTime eventTime = startTime;

                    if (bCurTime)
                    {
                        eventTime = eventTime.AddSeconds(_mRecordFile.mEventTimeSec);
                    }
                    else
                    {
                        startTime = _mRecordFile.mBaseUTC;
                        eventTime = _mRecordFile.mGetEventUTC();
                    }
                    _mRecordFile.mbSaveDvx1(_mRecordDir, nrBlocks, nrBits, startTime, eventTime);
                }
            }
        }

        private void saveViewPartMIT16ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {

                if (_mRecordFile != null)
                {
                    float startSec = (float)(_mZoomCenterSec - _mZoomDurationSec * 0.5F);
                    float durSec = _mZoomDurationSec;
                    float tMax = _mRecordFile.mGetSamplesTotalTime();

                    if (tMax < 2) tMax = 2;
                    if (startSec < 0) startSec = 0;

                    if (CProgram.sbReqFloat("Save view part MIT", "Start strip", ref startSec, "0.000", "sec", 0, tMax))
                    {
                        if (CProgram.sbReqFloat("Save view part MIT", "Duration strip (0=all)", ref durSec, "0.000", "sec", 0, tMax))
                        {
                            string label = "";
                            if (CProgram.sbReqLabel("Save view part MIT(max 6 char no space)", "Hint(classification)", ref label, "", false))
                            {


                                DateTime dt = _mRecordFile.mGetDeviceTime(_mRecordFile.mBaseUTC.AddSeconds(startSec));
                                string dtStr = CProgram.sDateTimeToYMDHMS(dt);
                                string strRecNr = "R" + _mRecordIndex.ToString("D7");
                                if (_mRecordDb.mStudy_IX > 0)
                                {
                                    _mRecordFile.mStudy_IX = _mRecordDb.mStudy_IX;  // needed for remark in hea file
                                    _mRecordFile.mSeqNrInStudy = _mRecordDb.mSeqNrInStudy;
                                    strRecNr += "S" + _mRecordFile.mStudy_IX + "p"+ _mRecordDb.mSeqNrInStudy;
                                }
                                if (label.Length == 0)
                                {
                                    label = "part";
                                }
                                string mit16FileName = _mRecordDb.mDeviceID + "_" + _mRecordDb.mPatientID.mDecrypt() //strRecNr + label
                                    + "_" + dtStr;
                                string mit16Path = Path.GetDirectoryName(_mRecordFilePath);
                                string mit16FilePath = Path.Combine(mit16Path, mit16FileName + ".hea");
                                string remark = strRecNr + " " + CProgram.sGetProgUserInitials() + ": "+ label;
                                bool bOk = _mRecordFile.mbSaveMIT(mit16FilePath, 16, true, startSec, durSec);
 
                                if (bOk)
                                {
                                    _mRecordDb.mbSaveSironaJson(mit16FilePath, startSec, durSec, label, remark);
     
                                    CProgram.sLogLine("Saved view part " + mit16FileName);
                                    if (CProgram.sbAskOkCancel("Saved view part ", 
                                        "Open event Record folder to view " + mit16FileName + "? "))
                                    {
                                        mTriageOpenEventDir(false);
                                    }
                                    string pathCopyMIT = Path.Combine(CProgram.sGetProgDir(), "copyMIT.bat");

                                    if (File.Exists(pathCopyMIT)
                                        && CProgram.sbAskOkCancel("Saved view part ",
                                        "Start batch to copy MIT files " + mit16FileName +"?"))
                                    {
                                        CProgram.sLogLine("Start copyMIT for " + mit16FileName);
                                        ProcessStartInfo startInfo = new ProcessStartInfo();
                                        startInfo.FileName = pathCopyMIT;
                                        startInfo.Arguments = "\""+ mit16Path + "\" "+ mit16FileName;
                                        Process.Start(startInfo);
                                    }
                   

                                }
                                else
                                {
                                    CProgram.sAskOk("Save part MIT", "Failed save view part to " + mit16FileName);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed save view part", ex);
            }

        }


        private void mIT16ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_mRecordFile != null)
            {
                string snr = "0000000" + _mRecordFile.mDeviceID;
                int strLen = snr == null ? 0 : snr.Length;
                if (strLen > 0)
                {
                    snr = snr.Substring(strLen - 6);   // get last 6 chars as snr
                }
                string mit16FileName = "DVX" + snr + "_R" + _mRecordIndex.ToString("D7") + "_MIT16_" + CProgram.sDateTimeToYMDHMS(DateTime.Now) + ".hea";
                string mit16Path = Path.Combine(Path.GetDirectoryName(_mRecordFilePath), mit16FileName);

                _mRecordFile.mbSaveMIT(mit16Path, 16, true);
            }
        }
        private void mIT24ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_mRecordFile != null)
            {
                string snr = "0000000" + _mRecordFile.mDeviceID;
                int strLen = snr == null ? 0 : snr.Length;
                if (strLen > 0)
                {
                    snr = snr.Substring(strLen - 6);   // get last 6 chars as snr
                }
                string mitFileName = "DVX" + snr + "_R" + _mRecordIndex.ToString("D7") + "_MIT24_" + CProgram.sDateTimeToYMDHMS(DateTime.Now) + ".hea";
                string mitPath = Path.Combine(Path.GetDirectoryName(_mRecordFilePath), mitFileName);

                _mRecordFile.mbSaveMIT(mitPath, 24, true);
            }
        }

        private void mECGSimulatorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_mRecordFile != null)
            {
                string mecgFileName = "R" + _mRecordIndex.ToString("D7") + "_MECG_" + CProgram.sDateTimeToYMDHMS(DateTime.Now) + ".txt";
                string mecgPath = Path.Combine(Path.GetDirectoryName(_mRecordFilePath), mecgFileName);
                _mRecordFile.mbSaveMECG(mecgPath, true);
            }
        }

        private void buttonExportData_Click(object sender, EventArgs e)
        {
            contextMenuExport.Show(buttonExportData.PointToScreen(new Point(0, 0)));
        }

        private void FormAnalyze_FormClosed(object sender, FormClosedEventArgs e)
        {
            mReleaseLock();
            mResetP1P2();
        }

        private void toolStripZoomStartTime_Click(object sender, EventArgs e)
        {
            if (_mRecordFile != null)
            {
                float t = (float)(_mZoomCenterSec - _mZoomDurationSec * 0.5F);
                UInt32 tMax = (UInt32)(_mRecordFile.mGetSamplesTotalTime() - _mZoomDurationSec + 0.5);

                if (tMax < 2) tMax = 2;
                if (t < 0) t = 0;

                if (CProgram.sbReqFloat("Zoom strip time", "Zoom strip start", ref t, "0.000", "sec", 0, tMax))
                {
                    float tCenter = t + _mZoomDurationSec * 0.5F;
                    mZoomTimeCenter(tCenter);
                }
            }
        }
        private void toolStripChannelsStartTime_Click(object sender, EventArgs e)
        {
            if (_mRecordFile != null)
            {
                float t = (float)(_mChannelsCenterSec - _mChannelsDurationSec * 0.5F);
                UInt32 tMax = (UInt32)(_mRecordFile.mGetSamplesTotalTime() - _mChannelsDurationSec + 0.5);

                if (tMax < 2) tMax = 2;
                if (t < 0) t = 0;

                if (CProgram.sbReqFloat("Channels strip time", "Channels strip start", ref t, "0.000", "sec", 0, tMax))
                {
                    float tCenter = t + _mChannelsDurationSec * 0.5F;
                    mChannelsTimeCenter(tCenter);
                }
            }
        }



        private void toolStripChannelStartTimeDate_Click(object sender, EventArgs e)
        {
            if (_mRecordFile != null)
            {
                mChannelsTimeCenter(0.0F);
            }
        }

        private void contextMenuExport_Opening(object sender, CancelEventArgs e)
        {

        }

        private void labelEventPriority_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void toolStripChn_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            //            mOpenPageScan();
        }

        private void toolStripPageScanClick(object sender, EventArgs e)
        {
            mOpenPageScan();
        }

        private string mMakePgeViewInfo(UInt16 AChannel, DateTime ADateTime)
        {
            string s = _mRecordFile.mGetChannelName(AChannel) + "\r\n\r\n" + CProgram.sTimeToString(ADateTime)
                + "\r\n" + CProgram.sDateToString(ADateTime);
            return s;
        }

        private void mCalcPageScanAmplitude(out float ArAmplitudeRange, UInt16 AFirstChannel, UInt16 ANrChannels,
            float AStartSec, float ADurationSec, float AMaxAmplRange)
        {
            float amplitudeRange = 0.0F;
            UInt16 iChannel = AFirstChannel;
            float min, max, range;

            for (UInt16 i = 0; i < ANrChannels; ++i, ++iChannel)
            {
                if (_mRecordFile.mbGetAmplitudeRange(i, AStartSec, ADurationSec, out min, out max, AMaxAmplRange))
                {
                    range = max - min;
                    if (range > amplitudeRange) amplitudeRange = range;
                }
            }
            if (amplitudeRange < 0.1)
            {
                amplitudeRange = 0.1F;
            }
            else
            {
                amplitudeRange *= 1.1f;    // draw litlebit smaller then top to top;
            }
            ArAmplitudeRange = amplitudeRange;
        }

        private Image mMakePageScanImage(UInt16 AChannelNr, float AStartSec, float ADurationSec, float AAmplitudeRange, int AImgWidth, int AImgHeight, float AMaxRange)
        {
            //bool bAutoSizeA = false;
            //            float cursorStart = -1.0F;
            //            float cursorDuration = 0.0F;
            bool bDrawBorder = false;
            DChartDrawTo drawTo = DChartDrawTo.Display;
            float unitT = 1.0F; // 0.2F;
            float unitA = 0.5F;
            bool bBorderLine = false;

            CStripChart chart = new CStripChart();

            _mRecordFile.mInitChart(AChannelNr, chart, AStartSec, ADurationSec, unitA, unitT, AMaxRange);

            Image img = _mRecordFile.mCreateChartImage3FixedRangeA(AChannelNr, chart, AImgWidth, AImgHeight, AAmplitudeRange,
                -1, -1, drawTo, bBorderLine);// mPrePlotFullUnitSec);

            //            Image img = _mRecordFile.mCreateChartImage2(AChannelNr, chart, AImgWidth, AImgHeight, -1, -1, drawTo, bDrawBorder);

            return img;
        }

        private Image mMakePageHrImage(float AStartSec, float ADurationSec, int AImgWidth, int AImgHeight,
                 UInt16 AMinHR, UInt16 ALowHR, UInt16 ABaseHR, UInt16 AHighHR, UInt16 AMaxHR,
                 out float ArHrMean, out float ArTotalBurden,
                 out float ArHrLowBurden, out float ArHrHighBurden, out float ArMinHrBpm, out float ArMaxHrBpm)

        {
            //            float cursorStart = -1.0F;
            //            float cursorDuration = 0.0F;
            bool bDrawBorder = false;
            DChartDrawTo drawTo = DChartDrawTo.Display;
            float unitA = 10;
            float unitT = 1.0F;


            CStripChart chart = new CStripChart();

            _mRecordFile.mInitChart(0, chart, AStartSec, ADurationSec, unitA, unitT, 1000);

            Image img = _mRecordFile.mCreateHrChartImage(_mRecordFile.mAnnotationAtr, chart, AImgWidth, AImgHeight,
                            AMinHR, ALowHR, ABaseHR, AHighHR, AMaxHR, -1, -1, drawTo, bDrawBorder,
                             out ArHrMean, out ArTotalBurden,
                            out ArHrLowBurden, out ArHrHighBurden, out ArMinHrBpm, out ArMaxHrBpm);

            return img;
        }

        private void mFillPageScan()
        {
            try
            {
                if (_mFormPageView != null)
                {
                    DateTime startDT = _mRecordFile.mGetDeviceTime(_mRecordFile.mBaseUTC);
                    float stripLengthSec = _mRecordFile.mGetSamplesTotalTime();
                    UInt16 nrChannels = _mRecordFile.mNrSignals;
                    bool bMultipleChannel = checkBoxAllChannels.Checked && nrChannels > 1;
                    UInt16 nrShowChannels = bMultipleChannel ? _mRecordFile.mNrSignals : (UInt16)1;
                    UInt16 selectedChannel = _mZoomChannel;
                    UInt16 stripPartSec = 0;

                    if (false == UInt16.TryParse(comboBoxStripSec.Text, out stripPartSec)
                        || stripPartSec < 0.001)
                    {
                        stripPartSec = 60;
                    }

                    if (_mPageViewNrChannels != nrShowChannels || _mPageViewPartSec != stripPartSec
                            || (false == bMultipleChannel && _mPageViewShowChannel != selectedChannel))
                    {
                        // fill Page Scan
                        _mFormPageView.mClearRows(nrShowChannels, stripPartSec);

                        UInt32 nrStrips = (UInt32)(stripLengthSec / stripPartSec + 0.99999);

                        //                        for( mInitStripChannels =)

                        string info = mMakePgeViewInfo(0, startDT);
                        Image img = _mZoomImage;
                        UInt32 stripTimeSec = 0;
                        DateTime stripDT;
                        UInt32 n = nrShowChannels * nrStrips;
                        int iCount = 0;

                        int imgWidth = _mFormPageView.mGetViewImageWidth();
                        int imgHeight = _mFormPageView.mGetViewImageHeight();
                        float maxAmplRange = CRecordMit.sGetMaxAmplitudeRange();
                        float amplitudeRange = 0;

                        for (int iStrip = 0; iStrip < nrStrips; ++iStrip)
                        {
                            stripDT = startDT.AddSeconds(stripTimeSec);
                            _mFormPageView.mUpdateTitle("Fill " + iCount + " / " + n);

                            if (bMultipleChannel)
                            {
                                mCalcPageScanAmplitude(out amplitudeRange, 0, nrShowChannels, stripTimeSec, stripPartSec, maxAmplRange);

                                for (UInt16 iChannel = 0; iChannel < nrShowChannels; ++iChannel)
                                {
                                    info = mMakePgeViewInfo(iChannel, stripDT);

                                    img = mMakePageScanImage(iChannel, stripTimeSec, stripPartSec, amplitudeRange, imgWidth, imgHeight, maxAmplRange);

                                    _mFormPageView.mAddRow(info, img, iChannel, stripTimeSec);

                                }
                            }
                            else
                            {
                                info = mMakePgeViewInfo(selectedChannel, stripDT);
                                mCalcPageScanAmplitude(out amplitudeRange, selectedChannel, 1, stripTimeSec, stripPartSec, maxAmplRange);
                                img = mMakePageScanImage(selectedChannel, stripTimeSec, stripPartSec, amplitudeRange, imgWidth, imgHeight, maxAmplRange);

                                _mFormPageView.mAddRow(info, img, selectedChannel, stripTimeSec);
                            }
                            stripTimeSec += stripPartSec;
                        }
                        _mFormPageView.mUpdateTitle("");
                        // dummy

                        _mPageViewNrChannels = nrShowChannels;
                        _mPageViewPartSec = stripPartSec;
                        _mPageViewShowChannel = selectedChannel;
                    }
                    else
                    {
                        _mFormPageView.mSelectRow(_mZoomChannel, _mZoomCenterSec);
                    }
                    _mFormPageView.BringToFront();
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("", ex);
            }
        }

        private void mOpenPageScan()
        {
            try
            {
                if (_mFormPageView != null)
                {
                    UInt16 channelNr = _mZoomChannel;
                    float selectTimeSec = _mZoomCenterSec;
                    // already open
                    _mFormPageView.Show();
                    _mFormPageView.mSelectRow(channelNr, selectTimeSec);
                }
                else
                {
                    string title = this.Text;
                    DateTime startDT = _mRecordFile.mGetDeviceTime(_mRecordFile.mBaseUTC);
                    Int16 timeZoneMin = (Int16)_mRecordFile.mTimeZoneOffsetMin;
                    float stripLengthSec = _mRecordFile.mGetSamplesTotalTime();
                    UInt16 nrChannels = _mRecordFile.mNrSignals;

                    _mFormPageView = new FormAnPageView(this, title, startDT, timeZoneMin, stripLengthSec, nrChannels);
                    if (_mFormPageView != null)
                    {
                        _mFormPageView.Show();

                        int width = _mFormPageView.mGetHrImageWidth();
                        int height = _mFormPageView.mGetHrImageHeight();
                        float hrLowBurden, hrHighBurden, minHrBpm, maxHrBpm, meanHrBpm, totalBurden;

                        Image img = mMakePageHrImage(0, stripLengthSec, width, height,
                                    _sAnHrMinBpm, _sAnHrLowBpm, _sAnHrBaseBpm, _sAnHrHighBpm, _sAnHrMaxBpm,
                                    out meanHrBpm, out totalBurden, out hrLowBurden, out hrHighBurden, out minHrBpm, out maxHrBpm);

                        _mFormPageView.mSetHrImage(img);
                        _mFormPageView.mSetHrValues(meanHrBpm, totalBurden, _sAnHrLowBpm, hrLowBurden, _sAnHrHighBpm, hrHighBurden, minHrBpm, maxHrBpm);
                    }
                }
                mFillPageScan();
            }
            catch (Exception ex)
            {
                CProgram.sLogException("", ex);
            }
        }

        public void mClosePageScan()
        {
            if (_mFormPageView != null)
            {
                FormAnPageView f = _mFormPageView;

                _mFormPageView = null;      // reset 
                _mPageViewNrChannels = 0;
                _mPageViewPartSec = 0;
                _mPageViewShowChannel = 0;

                f.mClearRows(0, 0);         // clear grid (unuse images)
                f.Close();
            }
        }
        public void mDoMoveSelected(UInt16 AChannel, float ACenterSec)
        {
            _mZoomChannel = AChannel;
            mSetZoomCenter(ACenterSec);
            mSetChannelsCenter(ACenterSec);
            mInitZoomChannel();
            mInitStripChannels();

        }
        public void mMoveScanClick(float ACenterTimeSec)
        {

        }


        private void mInitHeartRateGraph()
        {
            try
            {
                bool bVisible = false;
                float stripLengthSec = _mRecordFile.mGetSamplesTotalTime();

                int width = pictureBoxHR.Width;
                int height = pictureBoxHR.Height;

                float hrLowBurden, hrHighBurden, minHrBpm, maxHrBpm, meanHrBpm, totalBurden;

                if (Height < 10)
                {
                    height = 40;
                }
                if (width < 2000)
                {
                    width = 2000;
                }
                Image img = mMakePageHrImage(0, stripLengthSec, width, height,
                            _sAnHrMinBpm, _sAnHrLowBpm, _sAnHrBaseBpm, _sAnHrHighBpm, _sAnHrMaxBpm,
                            out meanHrBpm, out totalBurden, out hrLowBurden, out hrHighBurden, out minHrBpm, out maxHrBpm);

                pictureBoxHR.Image = img;

                string strMeanHR = "";
                string strMeanBurden = "";
                string s = "-HR-";
                if (minHrBpm <= maxHrBpm)
                {
                    int minHr = (int)(minHrBpm + 0.5);
                    int maxHr = (int)(maxHrBpm + 0.5);
                    int lowBurden = (int)(hrLowBurden + 0.5);
                    int highBurden = (int)(hrHighBurden + 0.5);

                    bVisible = true;
                    s = "HR min " + minHr + ", " + lowBurden + "% < " + _sAnHrLowBpm + " bpm"
                        + "\r\n"
                        + "HR max " + maxHr + ", " + highBurden + " % > " + _sAnHrHighBpm + " bpm";

                    int meanHR = (int)(meanHrBpm + 0.5);
                    int total = (int)(totalBurden + 0.5);

                    strMeanHR = meanHR.ToString();
                    strMeanBurden = total.ToString() + "%";
                }
                toolStripLabelHrInfo.Text = s;
                label1MeanHR.Text = strMeanHR;
                labelMeanBurden.Text = strMeanBurden;
                panelHR.Visible = bVisible;

                int h = ScrollBarChannel.Height + 2;

                if (bVisible)
                {
                    h += height + panelGraphSeperator.Height;
                }
                panelChannelScrol.Height = h;
            }
            catch (Exception ex)
            {
                CProgram.sLogException("", ex);
            }
        }
        private void mInitAmplitudeGraph()
        {
            try
            {
                bool bVisible = false;
                float stripLengthSec = _mRecordFile.mGetSamplesTotalTime();
                string id = "R" + _mRecordDb.mIndex_KEY;

                if(_mRecordDb.mStudy_IX > 0)
                {
                    id += "_S" + _mRecordDb.mStudy_IX + "." + _mRecordDb.mSeqNrInStudy;
                }

                int width = pictureBoxGraph.Width;
                int height = pictureBoxGraph.Height;




                string label1 = "";
                string label2 = "";
                string label3 = "";

                UInt32 nBeatsNormal = 0;
                double beatMin, beatMax, beatMean;

                UInt32 nBeatsSyst = 0;
                double systMin, systMax, systMean;

                UInt32 nBeatsDia = 0;
                double diaMin, diaMax, diaMean;

                if (Height < 10)
                {
                    height = 40;
                }
                if (width < 2000)
                {
                    width = 2000;
                }

                if (_mRecordFile.mAnnotationAtr != null)
                {
                    Image img = new Bitmap(width, height);

                    using (Graphics graphics = Graphics.FromImage(img))
                    {
                        graphics.Clear(Color.White);
                    }

                    nBeatsNormal = _mRecordFile.mAnnotationAtr.mCalcMinMaxValue(out beatMin, out beatMax, out beatMean, CAnnotationLine._cNormalBeat);
                    if (nBeatsNormal > 0)
                    {
                        bVisible = true;

                        beatMin *= _mRecordFile.mSampleUnitA;
                        beatMax *= _mRecordFile.mSampleUnitA;
                        beatMean *= _mRecordFile.mSampleUnitA;

                        label1 = beatMax.ToString("0.0");
                        label2 = "mV";
                        label3 = beatMin.ToString("0.0");

                        double delta = 0.03 * (beatMax - beatMin);
                        mPlotAnnotationImage(img, Color.Black, CAnnotationLine._cNormalBeat, 0,
                            0, stripLengthSec, beatMin- delta, beatMax+delta, 0, 0, 0,
                                            mEcgNrAvgPoints );


                        float regionDuration = _mZoomDurationSec;
                        float regionStart = _mZoomCenterSec - regionDuration / 2;
                        string s = _mRecordFile.mInfoAnnotationCsv(id, _mRecordFile.mAnnotationAtr,
                            CAnnotationLine._cNormalBeat, mEcgNrAvgPoints,
                            regionStart, regionDuration, "ECG", "mV");

                        ReqTextForm.sbShowText("ECG beat stats", "stats", s);
                    }

                    nBeatsSyst = _mRecordFile.mAnnotationAtr.mCalcMinMaxValue(out systMin, out systMax, out systMean, CAnnotationLine._cPlethSystole);   // Pleth Systolic marker
                    if (nBeatsSyst > 0)
                    {
                        bVisible = true;
                        systMin *= _mRecordFile.mSampleUnitA;
                        systMax *= _mRecordFile.mSampleUnitA;
                        systMean *= _mRecordFile.mSampleUnitA;

                        double minRange = systMin;
                        double maxRange = systMax;


                        nBeatsDia = _mRecordFile.mAnnotationAtr.mCalcMinMaxValue(out diaMin, out diaMax, out diaMean, CAnnotationLine._cPlethDiastole);   // Pleth Diastolic marker

                        if( nBeatsDia > 0)
                        {
                            diaMin *= _mRecordFile.mSampleUnitA;
                            diaMax *= _mRecordFile.mSampleUnitA;
                            diaMean *= _mRecordFile.mSampleUnitA;

                            minRange = diaMin;
                        }

                        label1 = maxRange.ToString("0.0");
                        label2 = "*hg";
                        label3 = minRange.ToString("0.0");

                        double delta = 0.03 * (maxRange - minRange);
                        mPlotAnnotationImage(img, Color.Red, CAnnotationLine._cPlethSystole, 0,
                            0, stripLengthSec, minRange - delta, maxRange + delta, 0, 0, 0,
                            mPlethNrAvgPoints);

                        if (nBeatsDia > 0)
                        {
                            mPlotAnnotationImage(img, Color.Blue, CAnnotationLine._cPlethDiastole, 0,
                                0, stripLengthSec, minRange - delta, maxRange + delta, 0, 0, 0,
                                mPlethNrAvgPoints);

                            // plot difference
                            mPlotAnnotationImage(img, Color.Green, CAnnotationLine._cPlethSystole, CAnnotationLine._cPlethDiastole, 
                                0, stripLengthSec, minRange - delta, maxRange + delta, 0, 0, 0,
                                mPlethNrAvgPoints);

                            float regionDuration = _mZoomDurationSec;
                            float regionStart = _mZoomCenterSec - regionDuration / 2;
                            string s =  _mRecordFile.mInfoAnnotationCsv(id, _mRecordFile.mAnnotationAtr,
                                CAnnotationLine._cPlethSystole, CAnnotationLine._cPlethDiastole, mPlethNrAvgPoints,
                                regionStart, regionDuration, "Pleth", "*hg");

                            ReqTextForm.sbShowText("Pleth beat stats", "stats", s);
                        }
                    }
                    if (bVisible)
                    {
                        pictureBoxGraph.Image = img;
                    }
                }

                labelGraphRight1.Text = label1;
                labelGraphRight2.Text = label2;
                labelGraphRight3.Text = label3;

                panelGraph.Visible = bVisible;

                int h = ScrollBarChannel.Height + 2;

                if (panelHR.Visible)
                {
                    h += panelHR.Height + panelGraphSeperator.Height;
                }
                if (bVisible)
                {
                    h += height+2;
                }
                panelChannelScrol.Height = h;
            }
            catch (Exception ex)
            {
                CProgram.sLogException("", ex);
            }
        }
        
        private void mInitHrAmplitudeGraph()
        {
            mInitHeartRateGraph();
            mInitAmplitudeGraph();
        }

        private void mPlotAnnotationImage(Image AImage, Color AColor, UInt16 ABeatCodeHigh, UInt16 ABeatCodeLow,
            float AStartSec, float ADurationSec, double AMinValue, double AMaxValue, 
            double ALineHighValue, double ALineLowValue, double ALineMeanValue, UInt16 ANrAvgPoints)

        {
            //            float cursorStart = -1.0F;
            //            float cursorDuration = 0.0F;
            DChartDrawTo drawTo = DChartDrawTo.Display;
            float unitA = 10;
            float unitT = 1.0F;

            CStripChart chart = new CStripChart();

            _mRecordFile.mInitChart(0, chart, AStartSec, ADurationSec, unitA, unitT, 1000);

            _mRecordFile.mPlotAnnotationImage(AImage, _mRecordFile.mAnnotationAtr, AColor, chart,
                ABeatCodeHigh, ABeatCodeLow, AMinValue, AMaxValue,
                ALineHighValue, ALineLowValue, ALineMeanValue, drawTo, ANrAvgPoints);
        }

        private void pictureBoxHR_MouseUp(object sender, MouseEventArgs e)
        {
            if (_mRecordFile != null)
            {
                float stripLengthSec = _mRecordFile.mGetSamplesTotalTime();
                int x = e.X;
                int w = pictureBoxHR.Width;
                float t = (float)x / (float)w * stripLengthSec;

                mDoMoveSelected(_mZoomChannel, t);
            }
        }

        private void checkBoxAnonymize_CheckStateChanged(object sender, EventArgs e)
        {
            mInitPatientInfo();
            mUpdateTitle(_mbNew, checkBoxAnonymize.Checked);
        }

        private void labelStudy_Click(object sender, EventArgs e)
        {

        }

        private void CheckBoxAutoScale_CheckChanged(object sender, EventArgs e)
        {
            CRecordMit.sbLimitAmplitudeRange = AutoScaleCheckbox.Checked;
        }

        private void MaxAmplitude_ValueChanged(object sender, EventArgs e)
        {
            CRecordMit.sLimitAmplitudeRange = (float)MaxAmplitude.Value;

            if (CRecordMit.sbLimitAmplitudeRange)
            {
                mInitZoomChannel();
                mInitStripChannels();
            }
        }

        private void AutoScaleCheckbox_CheckStateChanged(object sender, EventArgs e)
        {
            CRecordMit.sbLimitAmplitudeRange = false == AutoScaleCheckbox.Checked;
            mInitZoomChannel();
            mInitStripChannels();
        }

        private void label6_Click(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl && false == _mbReadOnly)
            {
                CDvtmsData.sbEditStudyPermissionsBits(_mAnalyzeSubTitle + " required permissions", ref _mRecordDb._mStudyPermissions, 0);

                mUpdateTitle(_mbNew, checkBoxAnonymize.Checked);
            }
        }

        private void checkBoxQCD_CheckStateChanged(object sender, EventArgs e)
        {
            if (checkBoxQCD.Checked)
            {
                if (CRecordMit.sbIsStateAnalyzed(_mNewState, false))
                {
                    if (mbGetAnalysisValues())
                    {
                        mSetNewState((UInt16)DRecState.QCd);
                    }
                }
            }
            mUpdateStateOk();
        }

        private void textBoxSymptoms_TextChanged(object sender, EventArgs e)
        {
            mSetChanged();

        }

        private void textBoxActivities_TextChanged(object sender, EventArgs e)
        {
            mSetChanged();
        }

        private void textBoxFindings_TextChanged_1(object sender, EventArgs e)
        {
            mSetChanged();
        }

        private void toolStripButton1_Click_1(object sender, EventArgs e)
        {
            _mbUpdatingStripList = false;
            _mbUpdatingMeasurementList = false;
            mUpdateStripChannels();
            mUpdateZoomChannel();
        }

        private void toolStripButtonInvert_Click(object sender, EventArgs e)
        {
            if (_mRecAnalysisFile != null)
            {
                UInt32 invMask = _mRecAnalysisFile._mInvChannelsMask;
                UInt32 mask = (1U << _mZoomChannel);
                invMask = invMask ^ mask;
                //                _mRecAnalysisFile._mInvChannelsMask = invMask;
                mSetChanged();
                // set ECG with new inver mask
                _mRecAnalysisFile.mModifyInvertMask(invMask, ref _mRecordFile);
                //                _mRecordFile.mModifyInvertMask(invMask);

                mInitStripChannels();
                mInitZoomChannel();
            }
        }

        private void buttonAnAbNormal_Click(object sender, EventArgs e)
        {
            mAnalyzeUpgradeState(CDvtmsData._cFindings_AbNormal, DRecState.Analyzed, false);
        }

        private void checkBoxQCD_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void simExportToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void pulseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mDoMenuSimPulse();
        }

        bool mbCreateSimFolder(out string ArSimFolder, string ABaseName, string AInfoText)
        {
            bool bOk = false;

            string dataPath = CDvtmsData.sGetDataDir();
            string toPath = "";

            try
            {
                toPath = Path.Combine(dataPath, "ExportSim");
                if (false == Directory.Exists(toPath))
                {
                    Directory.CreateDirectory(toPath);
                }
                if (Directory.Exists(toPath))
                {
                    toPath = Path.Combine(toPath, ABaseName);
                    if (false == Directory.Exists(toPath))
                    {
                        Directory.CreateDirectory(toPath);
                    }
                    if (Directory.Exists(toPath))
                    {
                        string fullPath = Path.Combine(toPath, ABaseName + ".info");
                        File.WriteAllText(fullPath, AInfoText);
                        bOk = true;
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("SimFolder create+log " + ABaseName, ex);
                bOk = false;
            }
            ArSimFolder = toPath;

            return bOk;

        }

        bool mbSaveToSimFolder(ref String ArResult, CRecordMit ARecord, string ASimFolder, string ABaseName, string APartName, string AMethod)
        {
            bool bOk = false;
            string title = "Sim " + AMethod + " " + ABaseName;

            if (ARecord != null && ARecord.mNrSamples > 0)
            {
                // Save HEA
                try
                {
                    string fullPath = Path.Combine(ASimFolder, ABaseName + "_HEA16.hea");
                    bOk = ARecord.mbSaveMIT(fullPath, 16, true);
                }
                catch (Exception ex)
                {
                    CProgram.sLogException(title + " write MIT16", ex);
                    bOk = false;
                }

                ArResult += bOk ? ", writen HEA16" : ", failed MIT16";


                if (bOk)
                {
                    // Save MECG
                    try
                    {
                        string fullPath = Path.Combine(ASimFolder, ABaseName + "_MECG.txt");
                        bOk = ARecord.mbSaveMECG(fullPath, true);
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException(title + " write MECG", ex);
                        bOk = false;
                    }
                    ArResult += bOk ? ", writen MECG" : ", failed MECG";
                }
                UInt32 dvxUnitsPermV = CDvxEvtRec._cDefaultAmplGainPermV;
                bool bAddLOff = true;

                if (bOk && CProgram.sbReqUInt32(title, "dvx Unit/mV (0=" + ARecord.mAdcGain.ToString() + ")", ref dvxUnitsPermV, "", 0, 99999)
                    && CProgram.sbReqBool(title, "Add Lead Off Channel", ref bAddLOff))
                {

                    // Save DVX Dat + DvxEvtRec
                    try
                    {
                        // must start with dvx nr and time
                        string dvxName = "0_" + CProgram.sDateTimeToYMDHMS(ARecord.mBaseUTC) + AMethod + APartName + "_X"; // to long "_DVX24";
                        string fullPath = Path.Combine(ASimFolder, dvxName + CDvxEvtRec._cEcgExtension);
                        double scale = 1.0;

                        if (dvxUnitsPermV == 0 || dvxUnitsPermV == ARecord.mAdcGain)
                        {
                            bOk = ARecord.mbSaveDvxRaw(fullPath, bAddLOff, 24, true);
                            dvxUnitsPermV = (UInt32)ARecord.mAdcGain;
                        }
                        else
                        {
                            scale = dvxUnitsPermV / (double)ARecord.mAdcGain;
                            bOk = ARecord.mbSaveDvxRawScaled(fullPath, bAddLOff, 24, scale, true);
                        }

                        CDvxEvtRec dvxEvtRec = new CDvxEvtRec();

                        if (dvxEvtRec != null)
                        {
                            UInt16 nSignals = ARecord.mNrSignals;
                            int signalMask = (1 << ARecord.mNrSignals) - 1;

                            if (bAddLOff)
                            {
                                ++nSignals;
                                signalMask <<= 1; // first value is leadoff and not loaded for viewing
                            }
                            dvxEvtRec._mDeviceSnr = 0;                   // device serial number
                            dvxEvtRec._mStudyNr = 0;                     // study index number

                            dvxEvtRec._mFileName = dvxName;            // origianal event file name (.dat or .dat.evt) and store file name (.DvxEvtRec)
                            dvxEvtRec._mFileStartUTC = ARecord.mBaseUTC;  // Date Time of start Primairy file
                            dvxEvtRec._mEventSampleNr = 0;
                            dvxEvtRec._mEventType = "Sim";                   // primairy event type
                            dvxEvtRec._mEventUTC = ARecord.mBaseUTC;     // Date Time of start Primairy file
                            dvxEvtRec._mTimeZoneOffsetMin = (Int16)ARecord.mTimeZoneOffsetMin;

                            dvxEvtRec._mSamplesPerSec = ARecord.mSampleFrequency;             // sample frequency
                            dvxEvtRec._mAdjustSpsFactor = 0;         // adjust the sample frequency to fit with time stamp factor sps = ((1-f) * SamplesPerSec + (f*SpcCalc)

                            dvxEvtRec._mAmplGainPermV = (int)dvxUnitsPermV;            // gain used for amplitude value (calibration value)
                            dvxEvtRec._mHardwareScale = 6;                 // harware scaling used at ECG chip
                            dvxEvtRec._mHardwareClockHz = 32000;          // sample frequency reference clock

                            dvxEvtRec._mNrChannelsInFile = nSignals;             // number of channels in file
                            dvxEvtRec._mbLeadOffInFile = bAddLOff;           // first channel is leadoff channel in file
                            dvxEvtRec._mbLeadOffSkip = true;             // skip leadoff channel
                            dvxEvtRec._mbRespSkipFirstChannel = false;
                            dvxEvtRec._mLoadChannelsMask = (UInt16)(signalMask);           // channels mask for loading file into a reacord
                                                                                           // B0=no lead off, B1=CH1, B2=CH2

                            dvxEvtRec._mNrBytesPerSample = 3;             // number of bytes per sample (default 3: 24 bits)
                            dvxEvtRec._mbSignExtend = true;               // sign extend value to a signed 32 bit integer
                            dvxEvtRec._mbRevertBytesMSB = true;              // revert bytes (LSB-MSB), default true
                            dvxEvtRec._mbFillGaps = false;                 // fill in the gaps with squeare 0.1mV at sps/4
                                                                           /*not implemented*/
                            dvxEvtRec._mbExactStrip = false;              // strip must be exact length starting at -_mPreEventSec and ending at _mPostEventSec

                            dvxEvtRec._mPreEventSec = 30;                 // requested nr seconds before event
                            dvxEvtRec._mPostEventSec = 30;                // requested nr seconds after event

                            dvxEvtRec.mbAddFileToList(dvxName);                  // files to be included. // selected files or files calculated the first time containing the strip

                            dvxEvtRec._mbLoadFromStudy = false;            // load files from study (otherwise from recording folder)
                            dvxEvtRec._mValuesOrigin = "sim";
                            dvxEvtRec._mbManualImport = true;
                            dvxEvtRec._mbReadCheckSumOK = false;

                            dvxEvtRec._mRecordIX = 0;
                            dvxEvtRec._mSourceFilePath = "";

                            bOk = dvxEvtRec.mbSaveFile(fullPath + CDvxEvtRec._cEvtRecExtension);

                        }
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException(title + " write DVX24", ex);
                        bOk = false;
                    }

                    ArResult += bOk ? ", writen DVX24" : ", failed DVX24";
                }
                if (bOk)
                {
                    CProgram.sLogLine("Sim " + AMethod + " " + ABaseName + ": " + ARecord.mNrSignals + " x " + ARecord.mNrSamples + "samples, " + ARecord.mGetSamplesTotalTime() + ArResult);

                    if (CProgram.sbAskOkCancel(this, "Simulate " + ABaseName, "open folder, success: " + ArResult))
                    {
                        try
                        {
                            ProcessStartInfo startInfo = new ProcessStartInfo();
                            startInfo.FileName = @"explorer";
                            startInfo.Arguments = ASimFolder;
                            Process.Start(startInfo);
                        }
                        catch (Exception e2)
                        {
                            CProgram.sLogException("Failed open explorer", e2);
                        }
                    }
                }
                else
                {
                    CProgram.sAskWarning(this, title, "result: " + ArResult);
                }
            }
            return bOk;
        }

        private void mSimAddPulse(CSignalData ASignal, UInt32 APeriodN, UInt32 ALeadN, UInt32 APulseN, Int32 AValue, Int32 AOffset)
        {
            if (ASignal != null)
            {
                int i = 0;
                Int32 value0 = AOffset;
                Int32 value1 = AOffset + AValue;

                for (int j = (int)ALeadN; --j >= 0;)
                {
                    ASignal.mAddValue(value0);
                    ++i;
                }
                for (int j = (int)APulseN; --j >= 0;)
                {
                    ASignal.mAddValue(value1);
                    ++i;
                }
                while (i < APeriodN)
                {
                    ASignal.mAddValue(value0);
                    ++i;

                }
            }
        }

        public CRecordMit mCreatePulseRecord(DateTime AUtc, float AAmplitudemV, float APeriodSec, float APulseTimeSec, UInt32 ANrPeriods,
            UInt16 ANrChannels, UInt32 ASamplesPerSec, UInt32 AUnitsPermV, bool AbFlipPulse, float AOffsetmV)

        {
            CRecordMit rec = null;

            // create record and signals for Pulse
            try
            {
                UInt32 samplesPerPeriod = (UInt32)(APeriodSec * ASamplesPerSec);
                UInt32 totalSamples = samplesPerPeriod * ANrPeriods;
                Int16 timeZoneMin = CProgram.sGetLocalTimeZoneOffsetMin();
                UInt32 samplesPerPulse = (UInt32)(APulseTimeSec * ASamplesPerSec + 0.5F);

                Int32 amplitudePulse = (Int32)(AAmplitudemV * AUnitsPermV + 0.5);
                Int32 amplitudeOffset = (Int32)(AOffsetmV * AUnitsPermV + 0.5);
                UInt32 samplesSpace = (UInt32)((samplesPerPeriod - samplesPerPulse) / (1 + ANrChannels));


                if (totalSamples > 0 && AUnitsPermV > 0 && ASamplesPerSec > 0)
                {

                    rec = new CRecordMit(CDvtmsData.sGetDBaseConnection());

                    if (rec.mbCreateSignals(ANrChannels) && rec.mbCreateAllSignalData(totalSamples + 1000))
                    {

                        rec.mBaseUTC = AUtc;
                        rec.mTimeZoneOffsetMin = timeZoneMin;
                        rec.mSampleFrequency = ASamplesPerSec;
                        rec.mAdcGain = AUnitsPermV;
                        rec.mbSetEventSec(0);
                        rec.mEventTypeString = "Sim";
                        rec.mSampleUnitA = 1.0F / AUnitsPermV;
                        rec.mSampleUnitT = 1.0F / ASamplesPerSec;
                        rec.mNrSamples = 0;

                        for (int channel = 0; channel < ANrChannels; ++channel)
                        {
                            CSignalData signal = rec.mSignals[channel];
                            UInt32 leadN = (UInt32)(samplesSpace * (channel + 1));

                            for (int cycle = 0; cycle < ANrPeriods; ++cycle)
                            {
                                mSimAddPulse(signal, samplesPerPeriod, leadN, samplesPerPulse,
                                    (AbFlipPulse && 1 == (cycle & 1) ? -amplitudePulse : amplitudePulse), amplitudeOffset);
                                if (signal.mNrValues > rec.mNrSamples)
                                {
                                    rec.mNrSamples = signal.mNrValues;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("sim PULSE create record", ex);
            }
            return rec;
        }
        private void mSimAddTri(CSignalData ASignal, UInt32 APeriodN, float APulseSkewPer, Int32 AValue, Int32 AOffset)
        {
            if (ASignal != null && APeriodN >= 4)
            {
                UInt32 n = (UInt32)(APeriodN * APulseSkewPer / 100);
                int i = 0, v;

                if (n <= 2)
                {
                    n = 2;
                }
                double dA = AValue / (double)n;

                for (int j = 0; j <= n; ++j)
                {
                    v = (Int32)(j * dA + 0.5);    // rising flank from vally to incl top
                    ASignal.mAddValue(v + AOffset);
                    ++i;
                }
                dA = AValue / (double)(APeriodN - n);
                while (i < APeriodN)
                {
                    v = (Int32)((APeriodN - i) * dA + 0.5);    // falling flank excl vally
                    ASignal.mAddValue(v + AOffset);
                    ++i;
                }
            }
        }

        public CRecordMit mCreateTriRecord(DateTime AUtc, float AAmplitudemV, float APeriodStartSec, float APeriodStopSec,
            float APulseSkewPer, UInt32 ANrPeriods,
            UInt16 ANrChannels, UInt32 ASamplesPerSec, UInt32 AUnitsPermV, bool AbFlipPulse, float AOffsetmV)

        {
            CRecordMit rec = null;

            // create record and signals for Pulse
            try
            {
                UInt32 samplesPerPeriodStart = (UInt32)(APeriodStartSec * ASamplesPerSec);
                UInt32 samplesPerPeriodStop = (UInt32)(APeriodStopSec * ASamplesPerSec);
                UInt32 maxPerPeriod = samplesPerPeriodStart > samplesPerPeriodStop ? samplesPerPeriodStart : samplesPerPeriodStop;
                UInt32 totalSamples = maxPerPeriod * ANrPeriods;
                Int16 timeZoneMin = CProgram.sGetLocalTimeZoneOffsetMin();
                UInt32 samplesPerPulse = (UInt32)(maxPerPeriod * ASamplesPerSec + 0.5F);

                Int32 amplitudePulse = (Int32)(AAmplitudemV * AUnitsPermV + 0.5);
                // move offset by half amplitude to get around 0 at 0 offset
                Int32 amplitudeOffset = (Int32)(AOffsetmV * AUnitsPermV + 0.5);

                if (totalSamples > 0 && AUnitsPermV > 0 && ASamplesPerSec > 0)
                {

                    rec = new CRecordMit(CDvtmsData.sGetDBaseConnection());

                    if (rec.mbCreateSignals(ANrChannels) && rec.mbCreateAllSignalData(totalSamples + 1000))
                    {
                        rec.mBaseUTC = AUtc;
                        rec.mTimeZoneOffsetMin = timeZoneMin;
                        rec.mSampleFrequency = ASamplesPerSec;
                        rec.mAdcGain = AUnitsPermV;
                        rec.mbSetEventSec(0);
                        rec.mEventTypeString = "Sim";
                        rec.mSampleUnitA = 1.0F / AUnitsPermV;
                        rec.mSampleUnitT = 1.0F / ASamplesPerSec;
                        rec.mNrSamples = 0;

                        double dCycleSamples = ANrPeriods <= 1 ? 0
                            : ((Int32)samplesPerPeriodStop - (Int32)samplesPerPeriodStart) / (double)(ANrPeriods - 1);

                        for (int channel = 0; channel < ANrChannels; ++channel)
                        {
                            CSignalData signal = rec.mSignals[channel];

                            for (int cycle = 0; cycle < ANrPeriods; ++cycle)
                            {
                                bool bFlipPulse = AbFlipPulse && 1 == (cycle & 1);
                                UInt32 samplesPerPeriod = (UInt32)(samplesPerPeriodStart + cycle * dCycleSamples + 0.5);
                                Int32 amplitude = bFlipPulse ? -amplitudePulse : amplitudePulse;
                                Int32 offset = AbFlipPulse ? amplitudeOffset // flip is aready around 0
                                    : amplitudeOffset - amplitude / 2;      // shift signal to be around 0
                                mSimAddTri(signal, samplesPerPeriod, APulseSkewPer, amplitude, offset);
                                if (signal.mNrValues > rec.mNrSamples)
                                {
                                    rec.mNrSamples = signal.mNrValues;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("sim TRI create record", ex);
            }
            return rec;
        }
        private void mSimAddPeak(CSignalData ASignal, UInt32 APeriodN, UInt32 APulseWidthN,
            bool AbSidePulsePT, bool AbFlipT, Int32 AValue, Int32 AOffset)
        {
            UInt32 samplesR = APulseWidthN <= 3 ? 3 : APulseWidthN;

            if (ASignal != null && APeriodN >= 3 * samplesR)
            {
                UInt32 samplesStart = samplesR;
                UInt32 samplesPart = 0;

                Int32 amplitudeR = AValue;
                Int32 amplitudeP = amplitudeR / 4;
                Int32 amplitudeT = AbFlipT ? -amplitudeR / 2 : amplitudeR / 2;
                double dA;

                if (AbSidePulsePT)
                {
                    //
                    //   P Q  R S   T
                    //       /\
                    //      |  |    __
                    //      |  |   /  \
                    // __/\_|  |__/    \__________
                    // 1 111 1  1  2  3 1 ......

                    UInt32 nRest = APeriodN - samplesStart - samplesR;
                    UInt32 nParts = 10;
                    UInt32 nNeeded = samplesR * nParts;
                    if (nNeeded <= nRest)
                    {
                        samplesPart = samplesR;
                    }
                    else
                    {
                        samplesPart = nRest / nParts;    // pulse is bigger than perios => scale back
                    }
                }
                int i = 0, v;
                // start flat
                for (int j = 0; j < samplesStart; ++j)
                {
                    ASignal.mAddValue(AOffset);
                    ++i;
                }
                if (samplesPart > 0)
                {
                    // rising flank P
                    dA = amplitudeP / (double)samplesPart;
                    for (int j = 0; j < samplesPart; ++j)
                    {
                        v = (Int32)(j * dA + 0.5);    // rising flank base excl top
                        ASignal.mAddValue(v + AOffset);
                        ++i;
                    }
                    // falling flank P
                    dA = amplitudeP / (double)samplesPart;
                    for (int j = 0; j < samplesPart; ++j)
                    {
                        v = amplitudeP - (Int32)(j * dA + 0.5);    // falling flank incl top excl base 
                        ASignal.mAddValue(v + AOffset);
                        ++i;
                    }
                    // flat Q
                    for (int j = 0; j < samplesPart; ++j)
                    {
                        ASignal.mAddValue(AOffset);
                        ++i;
                    }
                }
                UInt32 nR = samplesR / 2;

                // rising flank R
                dA = amplitudeR / (double)nR;
                for (int j = 0; j < nR; ++j)
                {
                    v = (Int32)(j * dA + 0.5);    // rising flank base excl top
                    ASignal.mAddValue(v + AOffset);
                    ++i;
                }
                // falling flank R
                nR = samplesR - nR;
                dA = amplitudeR / (double)nR;
                for (int j = 0; j < nR; ++j)
                {
                    v = amplitudeR - (Int32)(j * dA + 0.5);    // falling flank incl top excl base 
                    ASignal.mAddValue(v + AOffset);
                    ++i;
                }
                if (samplesPart > 0)
                {
                    // flat S
                    for (int j = 0; j < samplesPart; ++j)
                    {
                        ASignal.mAddValue(AOffset);
                        ++i;
                    }

                    // rising flank T
                    UInt32 nT = samplesPart * 2;
                    dA = amplitudeT / (double)nT;
                    for (int j = 0; j < nT; ++j)
                    {
                        v = (Int32)(j * dA + 0.5);    // rising flank base excl top
                        ASignal.mAddValue(v + AOffset);
                        ++i;
                    }
                    // falling flank T
                    nT = samplesPart * 3;
                    dA = amplitudeT / (double)nT;
                    for (int j = 0; j < nT; ++j)
                    {
                        v = amplitudeT - (Int32)(j * dA + 0.5);    // falling flank incl top excl base 
                        ASignal.mAddValue(v + AOffset);
                        ++i;
                    }
                }
                // last part is flat
                while (i < APeriodN)
                {
                    ASignal.mAddValue(AOffset);
                    ++i;
                }

            }
        }


        public CRecordMit mCreatePeakRecord(DateTime AUtc, float AAmplitudemV, float APeriodStartSec, float APeriodStopSec,
            float APulseWithSec, bool AbSidePulsePT, bool AbFlipT, UInt32 ANrPeriods,
            UInt16 ANrChannels, UInt32 ASamplesPerSec, UInt32 AUnitsPermV, bool AbFlipPulse, float AOffsetmV, float ASmoothAvgFilterSec)

        {
            CRecordMit rec = null;

            // create record and signals for Pulse
            try
            {
                UInt32 samplesPerPeriodStart = (UInt32)(APeriodStartSec * ASamplesPerSec);
                UInt32 samplesPerPeriodStop = (UInt32)(APeriodStopSec * ASamplesPerSec);
                UInt32 maxPerPeriod = samplesPerPeriodStart > samplesPerPeriodStop ? samplesPerPeriodStart : samplesPerPeriodStop;
                UInt32 totalSamples = maxPerPeriod * ANrPeriods;
                Int16 timeZoneMin = CProgram.sGetLocalTimeZoneOffsetMin();
                UInt32 samplesPerPulse = (UInt32)(maxPerPeriod * ASamplesPerSec + 0.5F);
                UInt32 samplesWidth = (UInt32)(APulseWithSec * ASamplesPerSec + 0.5F);

                Int32 amplitudePulse = (Int32)(AAmplitudemV * AUnitsPermV + 0.5);
                // move offset by half amplitude to get around 0 at 0 offset
                Int32 amplitudeOffset = (Int32)(AOffsetmV * AUnitsPermV + 0.5);

                if (totalSamples > 0 && AUnitsPermV > 0 && ASamplesPerSec > 0)
                {

                    rec = new CRecordMit(CDvtmsData.sGetDBaseConnection());

                    if (rec.mbCreateSignals(ANrChannels) && rec.mbCreateAllSignalData(totalSamples + 1000))
                    {
                        rec.mBaseUTC = AUtc;
                        rec.mTimeZoneOffsetMin = timeZoneMin;
                        rec.mSampleFrequency = ASamplesPerSec;
                        rec.mAdcGain = AUnitsPermV;
                        rec.mbSetEventSec(0);
                        rec.mEventTypeString = "Sim";
                        rec.mSampleUnitA = 1.0F / AUnitsPermV;
                        rec.mSampleUnitT = 1.0F / ASamplesPerSec;
                        rec.mNrSamples = 0;

                        double dCycleSamples = ANrPeriods <= 1 ? 0
                            : ((Int32)samplesPerPeriodStop - (Int32)samplesPerPeriodStart) / (double)(ANrPeriods - 1);

                        for (int channel = 0; channel < ANrChannels; ++channel)
                        {
                            CSignalData signal = rec.mSignals[channel];
                            Int32 amplitude, offset;

                            for (int cycle = 0; cycle < ANrPeriods; ++cycle)
                            {
                                UInt32 samplesPerPeriod = (UInt32)(samplesPerPeriodStart + cycle * dCycleSamples + 0.5);
                                if( cycle < 2)
                                {
                                    int b = cycle;
                                }
                                if (AbSidePulsePT)
                                {
                                    amplitude = AbFlipPulse ? -amplitudePulse : amplitudePulse;
                                    offset = AbFlipPulse ? amplitudeOffset // flip is aready around 0
                                        : amplitudeOffset - amplitude / 2;      // shift signal to be around 0
                                }
                                else
                                {
                                    bool bFlipPulse = AbFlipPulse && 1 == (cycle & 1);

                                    amplitude = bFlipPulse ? -amplitudePulse : amplitudePulse;
                                    offset = AbFlipPulse ? amplitudeOffset // flip is aready around 0
                                        : amplitudeOffset - amplitude / 2;      // shift signal to be around 0
                                }
                                if (samplesPerPeriod < samplesWidth * 4)
                                {
                                    samplesPerPeriod = samplesWidth * 4;
                                }
                                mSimAddPeak(signal, samplesPerPeriod, samplesWidth, AbSidePulsePT, AbFlipT, amplitude, offset);
                                if (signal.mNrValues > rec.mNrSamples)
                                {
                                    rec.mNrSamples = signal.mNrValues;
                                }
                            }
                            if (ASmoothAvgFilterSec >= 0.001)
                            {
                                rec.mbDoAvgHistoryFilter(true, ASmoothAvgFilterSec, true, 0, 0.0F, 0.0F);
                                rec.mbDoShiftTime(-0.5F * ASmoothAvgFilterSec, true, 0);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("sim Peak create record", ex);
            }
            return rec;
        }

        private string mReplaceDot(string AValue, char AReplacement)
        {
            string s = AValue;

            if (AValue != null)
            {
                s = s.Replace('.', AReplacement);
                s = s.Replace(',', AReplacement);
            }
            return s;
        }
        private string mMakeFloatStr(double AValue)
        {
            string s = "";
            double d = AValue;

            if (d > -0.00001 && d < 0.00001)
            {
                s = "0";
            }
            else
            {
                if (d < 0)
                {
                    d = -d;
                    s = "-";
                }
                if (d < 1.0)
                {
                    s += mReplaceDot(d.ToString("0.00"), '_');
                }
                else if (d < 10.00)
                {
                    s += mReplaceDot(d.ToString("0.0"), '_');
                }
                else
                {
                    int i = (int)(d + 0.5);
                    s += i.ToString();
                }
            }
            return s;
        }
        private void mDoMenuSimPulse()
        {
            string method = "Pulse";
            string name = "sim";
            string baseName;
            UInt32 samplesPerSec = 1000;
            UInt16 nrChannels = 1;
            float periodSec = 1.0F;
            float pulseSec = 0.1F;
            float amplitude = 3.0F;
            UInt32 nrPeriods = 10;
            UInt32 unitsPermV = 4000;
            float noisemV = 0;
            float noiseHz = 50;
            bool bNoiseShift = false;
            string title = "Simulate " + method;
            string result = "";
            Int16 timeZoneMin = CProgram.sGetLocalTimeZoneOffsetMin();
            bool bFlipPulse = false;
            float offsetmV = 0.0F;

            DateTime dt = DateTime.Now;
            DateTime dtUtc = dt.AddMinutes(-timeZoneMin);
            string simFolder;

            if (CProgram.sbReqUInt16(title, "nr channels", ref nrChannels, "", 1, 16)
            && nrChannels > 0
            && CProgram.sbReqUInt32(title, "samples per sec", ref samplesPerSec, "Sps", 0, 10000)
            && CProgram.sbReqFloat(title, "period time", ref periodSec, "0.000", "sec", 0.001F, 10000)
            && CProgram.sbReqFloat(title, "pulse time", ref pulseSec, "0.000", "sec", 0.001F, periodSec)
            && CProgram.sbReqFloat(title, "amplitude", ref amplitude, "0.000", "mV", -4.000F, 4.000F)
            && CProgram.sbReqBool(title, "Flip pulse amplitude", ref bFlipPulse)
            && CProgram.sbReqFloat(title, "offset", ref offsetmV, "0.000", "mV", -4.000F, 4.000F)
            && CProgram.sbReqUInt32(title, "units per mV (0=dvx<signal)", ref unitsPermV, "1/mV", 1, 1000000)
            && CProgram.sbReqUInt32(title, "nr periods", ref nrPeriods, "", 1, 100000)
            && CProgram.sbReqFloat(title, "Add noise", ref noisemV, "0.000", "mV", 0.000F, 99999F)
            && (noisemV < 0.001 || CProgram.sbReqFloat(title, "Add noise frequency", ref noiseHz, "0.000", "Hz", 0.001F, 99999F))
            && (nrChannels == 1 || noisemV < 0.001 || CProgram.sbReqBool(title, "Add Noise shift by channel", ref bNoiseShift))
)
            {
                if (unitsPermV == 0) unitsPermV = CDvxEvtRec._cDefaultAmplGainPermV;

                baseName = method + CProgram.sDateTimeToYMDHMS(dt);
                // PulseYYYYMMddhhmmss<nrChannels>C<pulse with>ms<pulse height>mV<periodTime>s
                name = samplesPerSec + "S"
                    + nrChannels.ToString() + "C"
                    + mMakeFloatStr(pulseSec * 1000) + "ms"
                    + mMakeFloatStr(amplitude) + "mV"
                    + mMakeFloatStr(periodSec) + "s";
                //+ samplesPerSec + "Sps";                  // max total file name 64 char
                if (noisemV >= 0.001)
                {
                    int hz = (int)(noiseHz + 0.5);
                    name += hz + "N" + mMakeFloatStr(noisemV);
                }

                if (CProgram.sbReqLabel("Simulate " + method, "name", ref name, "", false))
                {
                    baseName += "_" + name;

                    string infoText = "";
                    string cr = "\r\n";
                    UInt32 samplesPerPeriod = (UInt32)(periodSec * samplesPerSec + 0.5F);
                    UInt32 totalSamples = samplesPerPeriod * nrPeriods;
                    UInt32 samplesPerPulse = (UInt32)(pulseSec * samplesPerSec + 0.5F);
                    float totalTime = totalSamples / (float)samplesPerSec;

                    Int32 amplitudePulse = (Int32)(amplitude * unitsPermV + 0.5);
                    UInt32 samplesSpace = (UInt32)((samplesPerPeriod - samplesPerPulse) / (1 + nrChannels));


                    infoText += "method=" + method + cr;
                    infoText += "name=" + name + cr;

                    infoText += "DateTime=" + CProgram.sDateTimeToYMDHMS(dt).ToString() + cr;
                    infoText += "fullName=" + baseName.ToString() + cr;
                    infoText += "nrChannels=" + nrChannels.ToString() + cr;
                    infoText += "samplesPerSec=" + samplesPerSec.ToString() + cr;
                    infoText += "periodSec=" + periodSec.ToString("0.000") + cr;
                    infoText += "pulseSec=" + pulseSec.ToString("0.000") + cr;
                    infoText += "NoisemV=" + noisemV.ToString("0.0000") + cr;
                    infoText += "NoiseHz=" + noiseHz.ToString("0.000") + cr;
                    infoText += "NoiseShift=" + bNoiseShift.ToString() + cr;

                    infoText += "amplitudemV=" + amplitude.ToString("0.000") + cr;
                    infoText += "flipPulse=" + bFlipPulse.ToString() + cr;
                    infoText += "offsetmV=" + offsetmV.ToString("0.000") + cr;
                    infoText += "unitsPermV=" + unitsPermV.ToString() + cr;
                    infoText += "nrPeriods=" + nrPeriods.ToString() + cr;
                    infoText += ";-----" + cr;
                    infoText += "samplesPerPeriod=" + samplesPerPeriod.ToString() + cr;
                    infoText += "samplesPerPulse=" + samplesPerPulse.ToString() + cr;
                    infoText += "amplitudePulse=" + amplitudePulse.ToString() + cr;
                    infoText += "totalSamples=" + totalSamples.ToString() + cr;
                    infoText += "totalSec=" + totalTime.ToString() + "\t" + CProgram.sPrintTimeSpan_dhmSec(totalTime, "") + cr;

                    if (mbCreateSimFolder(out simFolder, baseName, infoText))
                    {

                        CRecordMit rec = mCreatePulseRecord(dtUtc, amplitude, periodSec, pulseSec, nrPeriods,
                            nrChannels, samplesPerSec, unitsPermV, bFlipPulse, offsetmV);

                        if (rec != null)
                        {
                            mbAddSinusNoise(rec, noisemV, noiseHz, bNoiseShift);
                            mDoSimSaveRec(ref result, title, rec, simFolder, baseName, name, method);
                        }
                    }
                }
            }
        }
        private void mDoMenuSimTriangle()
        {
            string method = "Tri";
            string name = "sim";
            string baseName;
            UInt32 samplesPerSec = 1000;
            UInt16 nrChannels = 1;
            float periodStartSec = 1.0F;
            float periodStopSec = 1.0F;
            float pulseSkewPer = 50F;
            float amplitude = 3.0F;
            UInt32 nrPeriods = 10;
            UInt32 unitsPermV = 4000;
            float noisemV = 0;
            float noiseHz = 50;
            bool bNoiseShift = false;
            string title = "Simulate Triangle";
            string result = "";
            Int16 timeZoneMin = CProgram.sGetLocalTimeZoneOffsetMin();
            bool bFlipPulse = false;
            float offsetmV = 0.0F;

            DateTime dt = DateTime.Now;
            DateTime dtUtc = dt.AddMinutes(-timeZoneMin);
            string simFolder;

            if (CProgram.sbReqUInt16(title, "nr channels", ref nrChannels, "", 1, 16)
            && nrChannels > 0
            && CProgram.sbReqUInt32(title, "samples per sec", ref samplesPerSec, "Sps", 0, 10000)
            && CProgram.sbReqFloat(title, "period start time", ref periodStartSec, "0.000", "sec", 0.001F, 10000)
            && CProgram.sbReqFloat(title, "period stop time", ref periodStopSec, "0.000", "sec", 0.001F, 10000)
            && CProgram.sbReqFloat(title, "pulse skew", ref pulseSkewPer, "0.000", "%", 0.000F, 100.0F)
            && CProgram.sbReqFloat(title, "amplitude", ref amplitude, "0.000", "mV", -4.000F, 4.000F)
            && CProgram.sbReqBool(title, "Flip pulse amplitude", ref bFlipPulse)
            && CProgram.sbReqFloat(title, "offset", ref offsetmV, "0.000", "mV", -4.000F, 4.000F)
            && CProgram.sbReqUInt32(title, "units per mV (0=dvx<signal)", ref unitsPermV, "1/mV", 1, 1000000)
            && CProgram.sbReqUInt32(title, "nr periods", ref nrPeriods, "", 1, 100000)
            && CProgram.sbReqFloat(title, "Add noise", ref noisemV, "0.000", "mV", 0.000F, 99999F)
            && (noisemV < 0.001 || CProgram.sbReqFloat(title, "Add noise frequency", ref noiseHz, "0.000", "Hz", 0.001F, 99999F))
            && (nrChannels == 1 || noisemV < 0.001 || CProgram.sbReqBool(title, "Add Noise shift by channel", ref bNoiseShift))
)
            {
                int per = (int)(0.5 + pulseSkewPer);
                if (unitsPermV == 0) unitsPermV = CDvxEvtRec._cDefaultAmplGainPermV;

                baseName = method + CProgram.sDateTimeToYMDHMS(dt);
                // PulseYYYYMMddhhmmss<nrChannels>C<pulse with>ms<pulse height>mV<periodTime>s
                name = samplesPerSec + "S"
                    + nrChannels.ToString() + "C"
                    + per.ToString() + "per"
                    + mMakeFloatStr(amplitude) + "mV"
                    + mMakeFloatStr(periodStartSec) + "s";
                //+ samplesPerSec + "Sps";                  // max total file name 64 char
                if (noisemV >= 0.001)
                {
                    int hz = (int)(noiseHz + 0.5);
                    name += hz + "N" + mMakeFloatStr(noisemV);
                }

                if (CProgram.sbReqLabel(title, "name", ref name, "", false))
                {
                    baseName += "_" + name;

                    string infoText = "";
                    string cr = "\r\n";
                    //                    UInt32 samplesPerPeriod = (UInt32)(periodSec * samplesPerSec + 0.5F);
                    //                  UInt32 totalSamples = samplesPerPeriod * nrPeriods;
                    //UInt32 samplesPerPulse = (UInt32)(pulseSec * samplesPerSec + 0.5F);
                    //                    float totalTime = totalSamples / (float)samplesPerSec;
                    float totalTime = (periodStartSec + periodStopSec) / 2 * nrPeriods;
                    Int32 amplitudePulse = (Int32)(amplitude * unitsPermV + 0.5);
                    //                    UInt32 samplesSpace = (UInt32)((samplesPerPeriod - samplesPerPulse) / (1 + nrChannels));


                    infoText += "method=" + method + cr;
                    infoText += "name=" + name + cr;

                    infoText += "DateTime=" + CProgram.sDateTimeToYMDHMS(dt).ToString() + cr;
                    infoText += "fullName=" + baseName.ToString() + cr;
                    infoText += "nrChannels=" + nrChannels.ToString() + cr;
                    infoText += "samplesPerSec=" + samplesPerSec.ToString() + cr;
                    infoText += "periodStartSec=" + periodStartSec.ToString("0.000") + cr;
                    infoText += "periodStopSec=" + periodStopSec.ToString("0.000") + cr;
                    infoText += "pulseSkewPer=" + pulseSkewPer.ToString("0.0") + cr;
                    infoText += "NoisemV=" + noisemV.ToString("0.0000") + cr;
                    infoText += "NoiseHz=" + noiseHz.ToString("0.000") + cr;
                    infoText += "NoiseShift=" + bNoiseShift.ToString() + cr;

                    infoText += "amplitudemV=" + amplitude.ToString("0.000") + cr;
                    infoText += "flipPulse=" + bFlipPulse.ToString() + cr;
                    infoText += "offsetmV=" + offsetmV.ToString("0.000") + cr;
                    infoText += "unitsPermV=" + unitsPermV.ToString() + cr;
                    infoText += "nrPeriods=" + nrPeriods.ToString() + cr;
                    infoText += ";-----" + cr;
                    //                    infoText += "samplesPerPeriod=" + samplesPerPeriod.ToString() + cr;
                    //                   infoText += "samplesPerPulse=" + samplesPerPulse.ToString() + cr;
                    infoText += "amplitudePulse=" + amplitudePulse.ToString() + cr;
                    //                    infoText += "totalSamples=" + totalSamples.ToString() + cr;
                    infoText += "totalSec=" + totalTime.ToString() + "\t" + CProgram.sPrintTimeSpan_dhmSec(totalTime, "") + cr;

                    if (mbCreateSimFolder(out simFolder, baseName, infoText))
                    {

                        CRecordMit rec = mCreateTriRecord(dtUtc, amplitude, periodStartSec, periodStopSec, pulseSkewPer, nrPeriods,
                            nrChannels, samplesPerSec, unitsPermV, bFlipPulse, offsetmV);

                        if (rec != null)
                        {
                            mbAddSinusNoise(rec, noisemV, noiseHz, bNoiseShift);

                            mDoSimSaveRec(ref result, title, rec, simFolder, baseName, name, method);
                        }
                    }
                }
            }
        }
        private void mDoMenuSimPeak()
        {
            string method = "Peak";
            string name = "sim";
            string baseName;
            UInt32 samplesPerSec = 1000;
            UInt16 nrChannels = 1;
            float periodStartSec = 1.0F;
            float periodStopSec = 1.0F;
            float pulseWidthSec = 0.04F;
            bool bSidePeaksPT = true;
            bool bFlipT = false;
            float amplitude = 3.0F;
            UInt32 nrPeriods = 10;
            UInt32 unitsPermV = 4000;
            float noisemV = 0;
            float noiseHz = 50;
            bool bNoiseShift = false;
            string title = "Simulate Spike-Peaks";
            string result = "";
            Int16 timeZoneMin = CProgram.sGetLocalTimeZoneOffsetMin();
            bool bFlipPulse = false;
            float offsetmV = 0.0F;
            float smoothAvgFilterSec = 0.0F;

            DateTime dt = DateTime.Now;
            DateTime dtUtc = dt.AddMinutes(-timeZoneMin);
            string simFolder;


            if (CProgram.sbReqUInt16(title, "nr channels", ref nrChannels, "", 1, 16)
            && nrChannels > 0
            && CProgram.sbReqUInt32(title, "samples per sec", ref samplesPerSec, "Sps", 0, 10000)
            && CProgram.sbReqFloat(title, "period start time", ref periodStartSec, "0.000", "sec", 0.001F, 10000)
            && CProgram.sbReqFloat(title, "period stop time", ref periodStopSec, "0.000", "sec", 0.001F, 10000)
            && CProgram.sbReqFloat(title, "pulse width", ref pulseWidthSec, "0.000", "sec", 0.000F, 100.0F)
            && CProgram.sbReqBool(title, "add side peaks PT", ref bSidePeaksPT)
            && (false == bSidePeaksPT || CProgram.sbReqBool(title, "flip peak T", ref bFlipT))
            && CProgram.sbReqFloat(title, "amplitude", ref amplitude, "0.000", "mV", -4.000F, 4.000F)
            && CProgram.sbReqBool(title, "Flip pulse amplitude", ref bFlipPulse)
            && CProgram.sbReqFloat(title, "smooth Avg", ref smoothAvgFilterSec, "0.000", "sec", 0.000F, 4.000F)
            && CProgram.sbReqFloat(title, "offset", ref offsetmV, "0.000", "mV", -4.000F, 4.000F)
            && CProgram.sbReqUInt32(title, "units per mV (0=dvx<signal)", ref unitsPermV, "1/mV", 1, 1000000)
            && CProgram.sbReqUInt32(title, "nr periods", ref nrPeriods, "", 1, 100000)
            && CProgram.sbReqFloat(title, "Add noise", ref noisemV, "0.000", "mV", 0.000F, 99999F)
            && (noisemV < 0.001 || CProgram.sbReqFloat(title, "Add noise frequency", ref noiseHz, "0.000", "Hz", 0.001F, 99999F))
            && (nrChannels == 1 || noisemV < 0.001 || CProgram.sbReqBool(title, "Add Noise shift by channel", ref bNoiseShift))
)
            {
                if (unitsPermV == 0) unitsPermV = CDvxEvtRec._cDefaultAmplGainPermV;

                baseName = method + CProgram.sDateTimeToYMDHMS(dt);
                // PulseYYYYMMddhhmmss<nrChannels>C<pulse with>ms<pulse height>mV<periodTime>s
                name = samplesPerSec + "S"
                    + nrChannels.ToString() + "C"
                    + mMakeFloatStr(pulseWidthSec) + "s"
                    + mMakeFloatStr(amplitude) + "mV"
                    + mMakeFloatStr(periodStartSec) + "s";
                //+ samplesPerSec + "Sps";                  // max total file name 64 char
                if (noisemV >= 0.001)
                {
                    int hz = (int)(noiseHz + 0.5);
                    name += hz + "N" + mMakeFloatStr(noisemV);
                }

                if (CProgram.sbReqLabel(title, "name", ref name, "", false))
                {
                    baseName += "_" + name;

                    string infoText = "";
                    string cr = "\r\n";
                    //                    UInt32 samplesPerPeriod = (UInt32)(periodSec * samplesPerSec + 0.5F);
                    //                  UInt32 totalSamples = samplesPerPeriod * nrPeriods;
                    //UInt32 samplesPerPulse = (UInt32)(pulseSec * samplesPerSec + 0.5F);
                    //                    float totalTime = totalSamples / (float)samplesPerSec;
                    float totalTime = (periodStartSec + periodStopSec) / 2 * nrPeriods;
                    Int32 amplitudePulse = (Int32)(amplitude * unitsPermV + 0.5);
                    //                    UInt32 samplesSpace = (UInt32)((samplesPerPeriod - samplesPerPulse) / (1 + nrChannels));


                    infoText += "method=" + method + cr;
                    infoText += "name=" + name + cr;

                    infoText += "DateTime=" + CProgram.sDateTimeToYMDHMS(dt).ToString() + cr;
                    infoText += "fullName=" + baseName.ToString() + cr;
                    infoText += "nrChannels=" + nrChannels.ToString() + cr;
                    infoText += "samplesPerSec=" + samplesPerSec.ToString() + cr;
                    infoText += "periodStartSec=" + periodStartSec.ToString("0.000") + cr;
                    infoText += "periodStopSec=" + periodStopSec.ToString("0.000") + cr;
                    infoText += "pulseWidthSec=" + pulseWidthSec.ToString("0.000") + cr;
                    infoText += "sidePeaksPT" + bSidePeaksPT.ToString() + cr;
                    infoText += "flipT" + bFlipT.ToString() + cr;
                    infoText += "NoisemV=" + noisemV.ToString("0.0000") + cr;
                    infoText += "NoiseHz=" + noiseHz.ToString("0.000") + cr;
                    infoText += "NoiseShift=" + bNoiseShift.ToString() + cr;

                    infoText += "amplitudemV=" + amplitude.ToString("0.000") + cr;
                    infoText += "flipPulse=" + bFlipPulse.ToString() + cr;

                    infoText += "smoothAvgFilterSec=" + smoothAvgFilterSec.ToString("0.000") + cr;
                    infoText += "offsetmV=" + offsetmV.ToString("0.000") + cr;
                    infoText += "unitsPermV=" + unitsPermV.ToString() + cr;
                    infoText += "nrPeriods=" + nrPeriods.ToString() + cr;
                    infoText += ";-----" + cr;
                    //                    infoText += "samplesPerPeriod=" + samplesPerPeriod.ToString() + cr;
                    //                   infoText += "samplesPerPulse=" + samplesPerPulse.ToString() + cr;
                    infoText += "amplitudePulse=" + amplitudePulse.ToString() + cr;
                    //                    infoText += "totalSamples=" + totalSamples.ToString() + cr;
                    infoText += "totalSec=" + totalTime.ToString() + "\t" + CProgram.sPrintTimeSpan_dhmSec(totalTime, "") + cr;

                    if (mbCreateSimFolder(out simFolder, baseName, infoText))
                    {

                        CRecordMit rec = mCreatePeakRecord(dtUtc, amplitude, periodStartSec, periodStopSec,
                            pulseWidthSec, bSidePeaksPT, bFlipT, nrPeriods,
                            nrChannels, samplesPerSec, unitsPermV, bFlipPulse, offsetmV, smoothAvgFilterSec);

                        if (rec != null)
                        {
                            mbAddSinusNoise(rec, noisemV, noiseHz, bNoiseShift);

                            mDoSimSaveRec(ref result, title, rec, simFolder, baseName, name, method);
                        }
                    }
                }
            }
        }

        private void mDoSimSaveRec(ref String ArResult, string ATitle, CRecordMit ARecord, string ASimFolder, 
            string ABaseName, string APartName, string AMethod)
        {
            bool bSave = true;

            if (CLicKeyDev.sbDeviceIsProgrammer())
            {
                if (CProgram.sbAskYesNo(this, ATitle, "Show results, replace signal"))
                {
                    string remark = "Simulate " + ABaseName;
                    _mRecordFile = ARecord;
                    _mRecordDb.mIndex_KEY = 0;
                    _mRecordDb.mStudy_IX = 0;
                    _mRecordDb.mRecRemark.mbSetEncrypted(remark);
                    _mbReadOnly = true;

                    textBoxEventRemark.Text = remark;

                    mUpdateTitle(true, true);

                    mInitScreenData();
                    mInitHrAmplitudeGraph();
                    mbInitCharts(false);
                    mInitStripChannels();
                    mInitZoomChannel();
                }
                bSave = CProgram.sbAskYesNo(this, ATitle, "Save signals");
            }
            if (bSave)
            {
                mbSaveToSimFolder(ref ArResult, ARecord, ASimFolder, ABaseName, APartName, AMethod);
            }
            BringToFront();
        }

        private void mSimAddSinus(CSignalData ASignal, UInt32 APeriodN, double AdRad, Int32 AValue)
        {
            if (ASignal != null)
            {
                for (int i = 0; i < APeriodN; ++i)
                {
                    ASignal.mAddValue((Int32)(Math.Sin(i * AdRad) * AValue + 0.5));
                }
            }
        }
        private void mSimAddFlat(CSignalData ASignal, UInt32 AN, Int32 AValue)
        {
            if (ASignal != null)
            {
                int i = 0;

                for (int j = (int)AN; --j >= 0;)
                {
                    ASignal.mAddValue(0);
                    ++i;
                }
            }
        }

        public CRecordMit mCreateSinusRecord(DateTime AUtc, float AAmplitudemV, float APeriodSec, UInt32 ANrPeriods, float APrePostSec,
            UInt16 ANrChannels, bool AbCopyChannels, UInt16 ASimChannel,
            UInt32 ASamplesPerSec, UInt32 AUnitsPermV)

        {
            CRecordMit rec = null;

            // create record and signals for Sinus
            try
            {
                double samplesPerPeriod = (APeriodSec * ASamplesPerSec);
                UInt32 samplesPrePost = (UInt32)(APrePostSec * ASamplesPerSec);
                UInt32 samplesPeriod = (UInt32)(samplesPerPeriod * ANrPeriods);

                double dRad = 2 * Math.PI / samplesPerPeriod;

                UInt32 totalSamples = samplesPrePost + samplesPeriod + samplesPrePost;
                Int16 timeZoneMin = CProgram.sGetLocalTimeZoneOffsetMin();

                Int32 amplitudePulse = (Int32)(AAmplitudemV * AUnitsPermV + 0.5);

                if (totalSamples > 0 && AUnitsPermV > 0 && ASamplesPerSec > 0)
                {

                    rec = new CRecordMit(CDvtmsData.sGetDBaseConnection());

                    if (rec.mbCreateSignals(ANrChannels) && rec.mbCreateAllSignalData(totalSamples + 1000))
                    {

                        rec.mBaseUTC = AUtc;
                        rec.mTimeZoneOffsetMin = timeZoneMin;
                        rec.mSampleFrequency = ASamplesPerSec;
                        rec.mAdcGain = AUnitsPermV;
                        rec.mbSetEventSec(0);
                        rec.mEventTypeString = "Sim";
                        rec.mSampleUnitA = 1.0F / AUnitsPermV;
                        rec.mSampleUnitT = 1.0F / ASamplesPerSec;
                        rec.mNrSamples = 0;

                        for (int channel = 0; channel < ANrChannels; ++channel)
                        {
                            CSignalData signal = rec.mSignals[channel];

                            if (AbCopyChannels || channel == ASimChannel)
                            {
                                mSimAddFlat(signal, samplesPrePost, 0);

                                mSimAddSinus(signal, samplesPeriod, dRad, amplitudePulse);

                                mSimAddFlat(signal, samplesPrePost, 0);
                            }
                            else
                            {
                                mSimAddFlat(signal, totalSamples, 0);

                            }

                            if (signal.mNrValues > rec.mNrSamples)
                            {
                                rec.mNrSamples = signal.mNrValues;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("sim PULSE create record", ex);
            }
            return rec;
        }

        public bool mbAddSinusNoise(CRecordMit ARecord, float ANoisemV, float ANoiseHz, bool AbNoiseShift)
        {
            bool bOk = false;

            if (ARecord != null)
            {
                // create record and signals for Sinus
                try
                {
                    UInt16 nrChannels = ARecord.mNrSignals;
                    UInt32 nrSamples = ARecord.mNrSamples;
                    UInt32 sampleFreq = ARecord.mSampleFrequency;

                    if( nrChannels > 0 && nrSamples > 0 && sampleFreq > 0 && ARecord.mSampleUnitA > 0.0000001 )
                    {
                        Int32 amplitudeNoise = (Int32)(ANoisemV / ARecord.mSampleUnitA + 0.5);

                        bOk = true;

                        if(amplitudeNoise > 0 && ANoiseHz >= 0.001)
                        {
                            double dRad = 2 * Math.PI / (double)sampleFreq * ANoiseHz;


                        for (int channel = 0; channel < nrChannels; ++channel)
                        {
                            CSignalData signal = ARecord.mSignals[channel];

                            if (signal != null && signal.mValues != null)
                            {
                                    double startPhase = 0;

                                    if(AbNoiseShift)
                                    {
                                        startPhase = 2 * Math.PI * channel / (double)nrChannels;
                                    }
                                    for (int i = 0; i < nrSamples; ++i)
                                    {
                                        int noise = (Int32)(Math.Sin(startPhase + i * dRad) * amplitudeNoise + 0.5);

                                        signal.mValues[i] += noise;
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("sim add noise", ex);
                }
            }
            return bOk;
        }

        private void mDoMenuSimSinus()
        {
            string method = "Sin";
            string name = "sim";
            string baseName;
            UInt32 samplesPerSec = 1000;
            UInt16 nrChannels = 1;
            bool bCopyChannels = true;
            UInt16 simChannel = 1;

            float freqHz = 50.0F;
            float simLengthSec = 20;
            int minNrPeriods = 2;
            float prePostSec = 5;
            float amplitude = 3.0F;
            UInt32 unitsPermV = 4000;
            float noisemV = 0;
            float noiseHz = 50;
            bool bNoiseShift = false;
            string title = "Simulate " + method;
            string result = "";
            Int16 timeZoneMin = CProgram.sGetLocalTimeZoneOffsetMin();
            

            DateTime dt = DateTime.Now;
            DateTime dtUtc = dt.AddMinutes(-timeZoneMin);
            string simFolder;

            if (CProgram.sbReqUInt16(title, "nr channels", ref nrChannels, "", 1, 16)
            && nrChannels > 0
            && (nrChannels == 1 || CProgram.sbReqBool(title, "All Channels", ref bCopyChannels))
            && (bCopyChannels || CProgram.sbReqUInt16(title, "signal channel  nr", ref simChannel, "", 1, nrChannels))
            && CProgram.sbReqUInt32(title, "samples per sec", ref samplesPerSec, "Sps", 0, 10000)
            && CProgram.sbReqFloat(title, "frequency", ref freqHz, "0.00", "Hz", 0.001F, 10000))
            {
                float minLength = minNrPeriods / freqHz;

                if (CProgram.sbReqFloat(title, "sim length", ref simLengthSec, "0.00", "sec", minLength, 99999)
            && CProgram.sbReqFloat(title, "pre&post flat time", ref prePostSec, "0.000", "sec", 0.000F, 99999F)
            && CProgram.sbReqFloat(title, "amplitude", ref amplitude, "0.000", "mV", -4.000F, 4.000F)
            && CProgram.sbReqUInt32(title, "units per mV (0=dvx<signal)", ref unitsPermV, "1/mV", 1, 1000000)
            && CProgram.sbReqFloat(title, "Add noise", ref noisemV, "0.000", "mV", 0.000F, 99999F)
            && ( noisemV < 0.001 || CProgram.sbReqFloat(title, "Add noise frequency", ref noiseHz, "0.000", "Hz", 0.001F, 99999F))
            && (nrChannels == 1 || noisemV < 0.001 || CProgram.sbReqBool(title, "Add Noise shift by channel", ref bNoiseShift))

            )
                {
                    if (unitsPermV == 0) unitsPermV = CDvxEvtRec._cDefaultAmplGainPermV;

                    baseName = method + CProgram.sDateTimeToYMDHMS(dt);
                    // SinusYYYYMMddhhmmss<pulse with>ms<pulse height>mV<periodTime>s
                    name = samplesPerSec + "S"
                        + nrChannels.ToString() + "C"
                        + mMakeFloatStr(freqHz) + "Hz"
                        + mMakeFloatStr(amplitude) + "mV";
                    //                        + mMakeFloatStr(simLengthSec) + "s";
                    //+samplesPerSec + "Sps";// max total file name 64 char
                    if (noisemV >= 0.001)
                    {
                        int hz = (int)(noiseHz + 0.5);
                        name += hz + "N" + mMakeFloatStr(noisemV);
                    }
                    if ( false == bCopyChannels)
                    {
                        name += "Ch" + simChannel;
                    }

                    if (CProgram.sbReqLabel("Simulate " + method, "name", ref name, "", false))
                    {
                        baseName += "_" + name;

                        float periodSec = 1.0F / freqHz;
                        UInt32 nrPeriods = (UInt32)(simLengthSec * freqHz + 0.5);
                        simLengthSec = nrPeriods / freqHz;

                        float totalTime = prePostSec + simLengthSec + prePostSec;

                        string infoText = "";
                        string cr = "\r\n";
                        UInt32 samplesPerPeriod = (UInt32)(periodSec * samplesPerSec + 0.5F);
                        UInt32 totalSamples = (UInt32)totalTime * samplesPerSec;
                        UInt32 samplesPerPost = (UInt32)(prePostSec * samplesPerSec + 0.5F);

                        Int32 amplitudePulse = (Int32)(amplitude * unitsPermV + 0.5);


                        infoText += "method=" + method + cr;
                        infoText += "name=" + name + cr;

                        infoText += "DateTime=" + CProgram.sDateTimeToYMDHMS(dt).ToString() + cr;
                        infoText += "fullName=" + baseName.ToString() + cr;
                        infoText += "nrChannels=" + nrChannels.ToString() + cr;
                        infoText += "copyChannels=" + bCopyChannels.ToString() + cr;
                        infoText += "simChannel=" + simChannel.ToString() + cr;

                        infoText += "samplesPerSec=" + samplesPerSec.ToString() + cr;

                        infoText += "freqHz=" + freqHz.ToString("0.000") + cr;
                        infoText += "simLengthSec=" + simLengthSec.ToString("0.000") + cr;
                        infoText += "prePostSec=" + prePostSec.ToString("0.000") + cr;

                        infoText += "NoisemV=" + noisemV.ToString("0.0000") + cr;
                        infoText += "NoiseHz=" + noiseHz.ToString("0.000") + cr;
                        infoText += "NoiseShift=" + bNoiseShift.ToString() + cr;


                        infoText += "amplitudemV=" + amplitude.ToString("0.000") + cr;
                        infoText += "unitsPermV=" + unitsPermV.ToString() + cr;
                        infoText += ";-----" + cr;
                        infoText += "periodSec=" + periodSec.ToString("0.000") + cr;
                        infoText += "nrPeriods=" + nrPeriods.ToString() + cr;
                        infoText += "samplesPerPeriod=" + samplesPerPeriod.ToString() + cr;
                        infoText += "amplitudePulse=" + amplitudePulse.ToString() + cr;
                        infoText += "totalSamples=" + totalSamples.ToString() + cr;
                        infoText += "totalSec=" + totalTime.ToString() + "\t" + CProgram.sPrintTimeSpan_dhmSec(totalTime, "") + cr;

                        if (mbCreateSimFolder(out simFolder, baseName, infoText))
                        {
                            --simChannel;

                            CRecordMit rec = mCreateSinusRecord(dtUtc, amplitude, periodSec, nrPeriods, prePostSec,
                                nrChannels, bCopyChannels, simChannel, samplesPerSec, unitsPermV);


                            if (rec != null)
                            {
                                mbAddSinusNoise(rec, noisemV, noiseHz, bNoiseShift);
                                mDoSimSaveRec(ref result, title, rec, simFolder, baseName, name, method);
                            }
                        }
                    }
                }
            }
        }

        

        private void mDoMenuSimSinusRange()
        {
            string method = "SinX";
            string name = "sim";
            string baseName;
            UInt32 samplesPerSec = 1000;
            UInt16 nrChannels = 1;
            bool bCopyChannels = true;
            UInt16 simChannel = 1;

            float simLengthSec = 10;
            UInt32 minNrPeriods = 2;
            float prePostSec = 1;
            float amplitude = 3.0F;
            UInt32 unitsPermV = 4000;
            float noisemV = 0;
            float noiseHz = 50;
            bool bNoiseShift = false;
            string title = "Simulate range " + method;
            string result = "";
            Int16 timeZoneMin = CProgram.sGetLocalTimeZoneOffsetMin();


            DateTime dt = DateTime.Now;
            DateTime dtUtc = dt.AddMinutes(-timeZoneMin);
            string simFolder;

            string range = "";
            string cr = "\r\n";
            bool bLongRange = false;

            if( false == CProgram.sbReqBool(title, "Long Range List", ref bLongRange))
            {
                return;
            }
            if (bLongRange)
            {
                for (int i = 1; i <= 9; ++i) range += "0.0" + i + cr;
                for (int i = 1; i <= 9; ++i) range += "0." + i + cr;
                for (int i = 1; i <= 9; ++i) range += i + cr;

                for (int i = 10; i <= 40; i += 5) range += i + cr;
                for (int i = 41; i < 70; i += 1) range += i + cr;
                for (int i = 70; i <= 250; i += 5) range += i + cr;
            }
            else
            {
                range = "1" + cr + "5" + cr + "50" + cr + "60" + cr + "100" + cr + "150";
            }

            if (ReqTextForm.sbReqText(title, "Frequency range", ref range )
            && CProgram.sbReqUInt16(title, "nr channels", ref nrChannels, "", 1, 16)
            && nrChannels > 0
            && (nrChannels == 1 || CProgram.sbReqBool(title, "All Channels", ref bCopyChannels))
            && (bCopyChannels || CProgram.sbReqUInt16(title, "signal channel  nr", ref simChannel, "", 1, nrChannels))
            && CProgram.sbReqUInt32(title, "samples per sec", ref samplesPerSec, "Sps", 0, 10000)

            && CProgram.sbReqFloat(title, "sim length", ref simLengthSec, "0.00", "sec", 1, 99999)
            && CProgram.sbReqFloat(title, "pre&post flat time", ref prePostSec, "0.000", "sec", 0.000F, 99999F)
            && CProgram.sbReqFloat(title, "amplitude", ref amplitude, "0.000", "mV", -4.000F, 4.000F)
            && CProgram.sbReqUInt32(title, "units per mV (0=dvx<signal)", ref unitsPermV, "1/mV", 1, 1000000)
            && CProgram.sbReqFloat(title, "Add noise", ref noisemV, "0.000", "mV", 0.000F, 99999F)
            && (noisemV < 0.001 || CProgram.sbReqFloat(title, "Add noise frequency", ref noiseHz, "0.000", "Hz", 0.001F, 99999F))
            && (nrChannels == 1 || noisemV < 0.001 || CProgram.sbReqBool(title, "Add Noise shift by channel", ref bNoiseShift))

            )
            {
                try
                {
                    if (unitsPermV == 0) unitsPermV = CDvxEvtRec._cDefaultAmplGainPermV;

                    baseName = method + CProgram.sDateTimeToYMDHMS(dt);
                    // SinusYYYYMMddhhmmss<pulse with>ms<pulse height>mV<periodTime>s
                    name = samplesPerSec + "S"
                        + nrChannels.ToString() + "C"
                        + "xHz"
                        + mMakeFloatStr(amplitude) + "mV";

                    if(noisemV>= 0.001 )
                    {
                        int hz = (int)(noiseHz + 0.5);
                        name += hz + "N" + mMakeFloatStr(noisemV);
                    }
                    //                        + mMakeFloatStr(simLengthSec) + "s";
                    //+samplesPerSec + "Sps";// max total file name 64 char
                    if (false == bCopyChannels)
                    {
                        name += "Ch" + simChannel;
                    }

                    if (CProgram.sbReqLabel("Simulate " + method, "name", ref name, "", false))
                    {
                        baseName += "_" + name;

                        string[] freqLines = range.Split();
                        int nLines = freqLines == null ? 0 : freqLines.Length;

                        if (nLines > 0)
                        {
                            string freqRange = "";
                            uint nStrips = 0;
                            UInt32 totalNrSamples = 0;
                            CRecordMit sumRec = null;
                            List<CRecordMit> recList = new List<CRecordMit>();

                            --simChannel;

                            for (int iLine = 0; iLine < nLines; ++iLine)
                            {
                                float freqHz = 50.0F;
                                string line = freqLines[iLine];

                                if (float.TryParse(line, out freqHz) && freqHz >= 0.001)
                                {

                                    float periodSec = 1.0F / freqHz;
                                    UInt32 nrPeriods = (UInt32)(simLengthSec * freqHz + 0.5);

                                    if (nrPeriods < minNrPeriods) nrPeriods = minNrPeriods;
                                    float partLengthSec = (float)nrPeriods / freqHz;

                                    CRecordMit rec = mCreateSinusRecord(dtUtc, amplitude, periodSec, nrPeriods, prePostSec,
                                        nrChannels, bCopyChannels, simChannel, samplesPerSec, unitsPermV);

                                    if (rec != null)
                                    {
                                        if (totalNrSamples > 0) freqRange += "+";
                                        freqRange += line.Trim();

                                        totalNrSamples += rec.mNrSamples;
                                        ++nStrips;
                                        recList.Add(rec);
                                    }
                                }
                            }

                            if (totalNrSamples > 0)
                            {
                                // combine int one record

                                sumRec = new CRecordMit(CDvtmsData.sGetDBaseConnection());

                                if (sumRec.mbCreateSignals(nrChannels) && sumRec.mbCreateAllSignalData(totalNrSamples))
                                {
                                    sumRec.mBaseUTC = dtUtc;
                                    sumRec.mTimeZoneOffsetMin = timeZoneMin;
                                    sumRec.mSampleFrequency = samplesPerSec;
                                    sumRec.mAdcGain = unitsPermV;
                                    sumRec.mbSetEventSec(0);
                                    sumRec.mEventTypeString = "SimX";
                                    sumRec.mSampleUnitA = 1.0F / unitsPermV;
                                    sumRec.mSampleUnitT = 1.0F / samplesPerSec;
                                    sumRec.mNrSamples = totalNrSamples;

                                    if (sumRec != null)
                                    {
                                        for (int iStrip = 0; iStrip < nStrips; ++iStrip)
                                        {
                                            CRecordMit rec = recList[iStrip];
                                            UInt32 nrSamples = rec.mNrSamples;

                                            for (int iChannel = 0; iChannel < nrChannels; ++iChannel)
                                            {
                                                CSignalData signal = rec.mSignals[iChannel];
                                                CSignalData sumSignal = sumRec.mSignals[iChannel];

                                                if (signal != null && signal.mValues != null)
                                                {
                                                    for (int i = 0; i < nrSamples; ++i)
                                                    {
                                                        sumSignal.mAddValue(signal.mValues[i]);
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }
                            }
                            if (totalNrSamples > 0 && sumRec != null)
                            {
                                float totalTime = sumRec.mGetSamplesTotalTime();

                                string infoText = "";
                                //                        string cr = "\r\n";
                                UInt32 totalSamples = sumRec.mNrSamples;
                                UInt32 samplesPerPost = (UInt32)(prePostSec * samplesPerSec + 0.5F);

                                Int32 amplitudePulse = (Int32)(amplitude * unitsPermV + 0.5);

                                infoText += "method=" + method + cr;
                                infoText += "name=" + name + cr;

                                infoText += "DateTime=" + CProgram.sDateTimeToYMDHMS(dt).ToString() + cr;
                                infoText += "fullName=" + baseName.ToString() + cr;
                                infoText += "nrChannels=" + nrChannels.ToString() + cr;
                                infoText += "copyChannels=" + bCopyChannels.ToString() + cr;
                                infoText += "simChannel=" + (simChannel + 1).ToString() + cr;

                                infoText += "samplesPerSec=" + samplesPerSec.ToString() + cr;

                                infoText += "freqRangeHz=" + freqRange + cr;
                                infoText += "nrFreq=" + nStrips + cr;
                                infoText += "simLengthSec=" + simLengthSec.ToString("0.000") + cr;
                                infoText += "prePostSec=" + prePostSec.ToString("0.000") + cr;

                                infoText += "NoisemV=" + noisemV.ToString("0.0000") + cr;
                                infoText += "NoiseHz=" + noiseHz.ToString("0.000") + cr;
                                infoText += "NoiseShift=" + bNoiseShift.ToString() + cr;

                                infoText += "amplitudemV=" + amplitude.ToString("0.000") + cr;
                                infoText += "unitsPermV=" + unitsPermV.ToString() + cr;
                                infoText += ";-----" + cr;
                                infoText += "amplitudePulse=" + amplitudePulse.ToString() + cr;
                                infoText += "totalSamples=" + totalSamples.ToString() + cr;
                                infoText += "totalSec=" + totalTime.ToString() + "\t" + CProgram.sPrintTimeSpan_dhmSec(totalTime, "") + cr;

                                if (mbCreateSimFolder(out simFolder, baseName, infoText))
                                {
                                    mbAddSinusNoise(sumRec, noisemV, noiseHz, bNoiseShift);
                                    mDoSimSaveRec(ref result, title, sumRec, simFolder, baseName, name, method);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Simulate Sin Range", ex);
                }
            }
        }

#if jhgflsglj
        private void mDoMenuSimPulseOld()
        {
            string method = "Pulse";
            string name = "sim";
            string fullName;
            UInt32 samplesPerSec = 1000;
            UInt16 nrChannels = 1;
            float periodSec = 1.0F;
            float pulseSec = 0.2F;
            float amplitude = 1.0F;
            UInt32 nrPeriods = 1;
            UInt32 unitsPermV = 4000;
            DateTime dt, dtUtc;
            string title;
            string result = "";
            Int16 timeZoneMin = CProgram.sGetLocalTimeZoneOffsetMin();

            if (CProgram.sbReqLabel("Simulate " + method, "name", ref name, "", false))
            {
                dt = DateTime.Now;
                dtUtc = dt.AddMinutes(-timeZoneMin);


                fullName = method + CProgram.sDateTimeToYMDHMS(dt) + name;
                title = "Simulate " + fullName;

                if (CProgram.sbReqUInt16(title, "nr channels", ref nrChannels, "", 1, 16)
                    && nrChannels > 0
                    && CProgram.sbReqUInt32(title + "(dvx=20913)", "samples per sec", ref samplesPerSec, "Sps", 0, 10000)
                    && CProgram.sbReqFloat(title, "period time", ref periodSec, "0.000", "sec", 0.001F, 10000)
                    && CProgram.sbReqFloat(title, "pulse time", ref pulseSec, "0.000", "sec", 0.001F, periodSec)
                    && CProgram.sbReqFloat(title, "amplitude", ref amplitude, "0.000", "mV", -4.000F, 4.000F)
                    && CProgram.sbReqUInt32(title, "units per mV", ref unitsPermV, "1/mV", 1, 1000000)
                    && CProgram.sbReqUInt32(title, "nr periods", ref nrPeriods, "", 1, 100000)
                    )
                {
                    if (samplesPerSec == 0) samplesPerSec = 20913;

                    string logText = "";
                    string cr = "\r\n";
                    UInt32 samplesPerPeriod = (UInt32)(periodSec * samplesPerSec + 0.5F);
                    UInt32 totalSamples = samplesPerPeriod * nrPeriods;
                    UInt32 samplesPerPulse = (UInt32)(pulseSec * samplesPerSec + 0.5F);
                    float totalTime = totalSamples / (float)samplesPerSec;

                    Int32 amplitudePulse = (Int32)(amplitude * unitsPermV + 0.5);
                    UInt32 samplesSpace = (UInt32)((samplesPerPeriod - samplesPerPulse) / (1 + nrChannels));


                    logText += "method=" + method + cr;
                    logText += "name=" + name + cr;

                    logText += "DateTime=" + CProgram.sDateTimeToYMDHMS(dt).ToString() + cr;
                    logText += "fullName=" + fullName.ToString() + cr;
                    logText += "nrChannels=" + nrChannels.ToString() + cr;
                    logText += "samplesPerSec=" + samplesPerSec.ToString() + cr;
                    logText += "periodSec=" + periodSec.ToString("0.000") + cr;
                    logText += "pulseSec=" + pulseSec.ToString("0.000") + cr;
                    logText += "amplitudemV=" + amplitude.ToString("0.000") + cr;
                    logText += "unitsPermV=" + unitsPermV.ToString() + cr;
                    logText += "nrPeriods=" + nrPeriods.ToString() + cr;
                    logText += ";-----" + cr;
                    logText += "samplesPerPeriod=" + samplesPerPeriod.ToString() + cr;
                    logText += "samplesPerPulse=" + samplesPerPulse.ToString() + cr;
                    logText += "amplitudePulse=" + amplitudePulse.ToString() + cr;
                    logText += "totalSamples=" + totalSamples.ToString() + cr;
                    logText += "totalSec=" + totalTime.ToString() + "\t" + CProgram.sPrintTimeSpan_dhmSec(totalTime, "") + cr;

                    string dataPath = CDvtmsData.sGetDataDir();
                    string toPath = "";
                    CRecordMit rec = null;
                    bool bOk = false;

                    try
                    {
                        toPath = Path.Combine(dataPath, "ExportSim");
                        if (false == Directory.Exists(toPath))
                        {
                            Directory.CreateDirectory(toPath);
                        }
                        if (Directory.Exists(toPath))
                        {
                            toPath = Path.Combine(toPath, fullName);
                            if (false == Directory.Exists(toPath))
                            {
                                Directory.CreateDirectory(toPath);
                            }
                            if (Directory.Exists(toPath))
                            {
                                string fullPath = Path.Combine(toPath, fullName + ".info");
                                File.WriteAllText(fullPath, logText);
                                bOk = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException(title + " create+log", ex);
                        result = "write info";
                        bOk = false;
                    }
                    if (bOk)
                    {
                        // create record and signals
                        try
                        {

                            rec = new CRecordMit(CDvtmsData.sGetDBaseConnection());

                            bOk = rec != null && rec.mbCreateSignals(nrChannels) && rec.mbCreateAllSignalData(totalSamples + 1000);

                            if (bOk)
                            {
                                rec.mBaseUTC = dtUtc;
                                rec.mTimeZoneOffsetMin = timeZoneMin;
                                rec.mSampleFrequency = samplesPerSec;
                                rec.mAdcGain = unitsPermV;
                                rec.mbSetEventSec(0);
                                rec.mEventTypeString = "Sim";
                                rec.mSampleUnitA = 1.0F / unitsPermV;
                                rec.mSampleUnitT = 1.0F / samplesPerSec;
                                rec.mNrSamples = 0;

                                for (int channel = 0; channel < nrChannels; ++channel)
                                {
                                    CSignalData signal = rec.mSignals[channel];
                                    UInt32 leadN = (UInt32)(samplesSpace * (channel + 1));

                                    for (int cycle = 0; cycle < nrPeriods; ++cycle)
                                    {
                                        mSimAddPulse(signal, samplesPerPeriod, leadN, samplesPerPulse, amplitudePulse);
                                        if( signal.mNrValues > rec.mNrSamples)
                                        {
                                            rec.mNrSamples = signal.mNrValues;
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            CProgram.sLogException(title + " create record", ex);
                            bOk = false;
                        }
                        result = bOk ? "created" : "no signal";
                    }
                    if (bOk && rec != null)
                    {
                        // Save HEA
                        try
                        {
                            string fullPath = Path.Combine(toPath, fullName + "_HEA16.hea");
                            bOk = rec.mbSaveMIT(fullPath, 16, true);
                        }
                        catch (Exception ex)
                        {
                            CProgram.sLogException(title + " write MIT16", ex);
                            bOk = false;
                        }

                        result += bOk ? ", writen HEA16" : ", failed MIT16";
                    }

                    if (bOk && rec != null)
                    {
                        // Save MECG
                        try
                        {
                            string fullPath = Path.Combine(toPath, fullName + "_MECG.txt");
                            bOk = rec.mbSaveMECG(fullPath, true);
                        }
                        catch (Exception ex)
                        {
                            CProgram.sLogException(title + " write MECG", ex);
                            bOk = false;
                        }
                        result += bOk ? ", writen MECG" : ", failed MECG";
                    }
                    if (bOk && rec != null)
                    {
                        // Save DVX Dat + DvxEvtRec
                        try
                        {
                            string dvxName = fullName + "_DVX24";
                            string fullPath = Path.Combine(toPath, dvxName + CDvxEvtRec._cEcgExtension);

                            bOk = rec.mbSaveDvxRaw(fullPath, true, 24, true);

                            CDvxEvtRec dvxEvtRec = new CDvxEvtRec();

                            if (dvxEvtRec != null)
                            {
                                dvxEvtRec._mDeviceSnr = 0;                   // device serial number
                                dvxEvtRec._mStudyNr = 0;                     // study index number

                                dvxEvtRec._mFileName = dvxName;            // origianal event file name (.dat or .dat.evt) and store file name (.DvxEvtRec)
                                dvxEvtRec._mFileStartUTC = dtUtc;  // Date Time of start Primairy file
                                dvxEvtRec._mEventSampleNr = 0;
                                dvxEvtRec._mEventType = "Sim";                   // primairy event type
                                dvxEvtRec._mEventUTC = dtUtc;     // Date Time of start Primairy file
                                dvxEvtRec._mTimeZoneOffsetMin = timeZoneMin;

                                dvxEvtRec._mSamplesPerSec = samplesPerSec;             // sample frequency
                                dvxEvtRec._mAdjustSpsFactor = 0;         // adjust the sample frequency to fit with time stamp factor sps = ((1-f) * SamplesPerSec + (f*SpcCalc)

                                dvxEvtRec._mAmplGainPermV = (int)unitsPermV;            // gain used for amplitude value (calibration value)
                                dvxEvtRec._mHardwareScale = 6;                 // harware scaling used at ECG chip
                                dvxEvtRec._mHardwareClockHz = 32000;          // sample frequency reference clock

                                dvxEvtRec._mNrChannelsInFile = nrChannels;             // number of channels in file
                                dvxEvtRec._mbLeadOffInFile = false;           // first channel is leadoff channel in file
                                dvxEvtRec._mbLeadOffSkip = true;             // skip leadoff channel
                                dvxEvtRec._mbRespSkipFirstChannel = false;
                                dvxEvtRec._mLoadChannelsMask = (UInt16)((1 << nrChannels) - 1);           // channels mask for loading file into a reacord
                                                                                                          // B0=no lead off, B1=CH1, B2=CH2

                                dvxEvtRec._mNrBytesPerSample = 3;             // number of bytes per sample (default 3: 24 bits)
                                dvxEvtRec._mbSignExtend = true;               // sign extend value to a signed 32 bit integer
                                dvxEvtRec._mbRevertBytesMSB = true;              // revert bytes (LSB-MSB), default true
                                dvxEvtRec._mbFillGaps = false;                 // fill in the gaps with squeare 0.1mV at sps/4
                                                                               /*not implemented*/
                                dvxEvtRec._mbExactStrip = false;              // strip must be exact length starting at -_mPreEventSec and ending at _mPostEventSec

                                dvxEvtRec._mPreEventSec = 30;                 // requested nr seconds before event
                                dvxEvtRec._mPostEventSec = 30;                // requested nr seconds after event

                                dvxEvtRec.mbAddFileToList(dvxName);                  // files to be included. // selected files or files calculated the first time containing the strip

                                dvxEvtRec._mbLoadFromStudy = false;            // load files from study (otherwise from recording folder)
                                dvxEvtRec._mValuesOrigin = "sim";
                                dvxEvtRec._mbManualImport = true;
                                dvxEvtRec._mbReadCheckSumOK = false;

                                dvxEvtRec._mRecordIX = 0;
                                dvxEvtRec._mSourceFilePath = "";

                                bOk = dvxEvtRec.mbSaveFile(fullPath + CDvxEvtRec._cEvtRecExtension);

                            }
                        }
                        catch (Exception ex)
                        {
                            CProgram.sLogException(title + " write DVX24", ex);
                            bOk = false;
                        }

                        result += bOk ? ", writen DVX24" : ", failed DVX24";
                    }
                    if (bOk)
                    {
                        if (CProgram.sbAskOkCancel("Simulate " + fullName, "open folder, success: " + result))
                        {
                            try
                            {

                                ProcessStartInfo startInfo = new ProcessStartInfo();
                                startInfo.FileName = @"explorer";
                                startInfo.Arguments = toPath;
                                Process.Start(startInfo);
                            }
                            catch (Exception e2)
                            {
                                CProgram.sLogException("Failed open explorer", e2);
                            }

                        }
                    }
                    else
                    {
                        CProgram.sAskWarning("Simulate " + fullName, "result: " + result);

                    }


                }
            }
        }
#endif

            private void mUpdateButtonLPF()
        {
            string s = "lpf\r\n--";

            if( _mTextLPF != null && _mTextLPF.Length > 0)
            {
                s = "LPF\r\n" + _mTextLPF;
            }
            toolStripButtonLPF.Text = s;
        }
        private void mUpdateButtonHPF()
        {
            string s = "hpf\r\n--";

            if (_mTextHPF != null && _mTextHPF.Length > 0)
            {
                s = "HPF\r\n" + _mTextHPF;
            }
            toolStripButtonHPF.Text = s;
        }
        private void mUpdateButtonBBF()
        {
            string s = "bbf\r\n--";

            if (_mTextBBF != null && _mTextBBF.Length > 0)
            {
                s = "BBF\r\n" + _mTextBBF;
            }
            toolStripButtonBBF.Text = s;
        }
        private void mUpdateButtonAvgClipReset()
        {
            string s = "acrf\r\n--";

            if (_mTextACRF != null && _mTextACRF.Length > 0)
            {
                s = "ACRF\r\n" + _mTextACRF;
            }
            toolStripButtonAvgCR.Text = s;

        }

        private void toolStripButtonLPF_Click(object sender, EventArgs e)
        {
            mDoButtonLPF();
        }

        private void mDoButtonLPF()
        {
            float fc = 150F;
            bool bAllChannels = true;
            string strChNr = (_mZoomChannel + 1).ToString();

            bool bB3 = false;

            if (CProgram.sbReqFloat("Low Pass Filter(Test-No Save)", "fc", ref fc, "0.00", "Hz", 0.001F, 999F)
                && CProgram.sbReqBool("Low Pass Filter(Test-No Save) " + fc.ToString("0.00") + " Hz", "Butterworth 3 order (off=1RC)", ref bB3)
                && CProgram.sbReqBool("Low Pass Filter(Test-No Save) " + fc.ToString("0.00") + " Hz", "All Channels (off = zoom channel "+ strChNr + " only", ref bAllChannels))
            {
                if (_mRecordFile != null)
                {
                    bool bOk = bB3 ? _mRecordFile.mbDoButherworth3Filter(DFilterType.LowPass, fc, bAllChannels, _mZoomChannel)
                            : _mRecordFile.mbDoLowPassFilter(fc, bAllChannels, _mZoomChannel);
                    if( bOk )
                    {
                        _mTextLPF = fc.ToString("0.0") + (bB3 ? " B3" : " RC");

                        mbInitCharts(false);
                        mInitStripChannels();
                        mInitZoomChannel();
                        BringToFront();

                    }
                }
            }
            mUpdateButtonLPF();
        }

        private void toolStripButtonHPF_Click(object sender, EventArgs e)
        {
            mDoButtonHPF();
        }
        private void mDoButtonHPF()
        {
            float fc = 0.05F;
            bool bAllChannels = true;
            string strChNr = (_mZoomChannel + 1).ToString();

            bool bB3 = false;

            if (CProgram.sbReqFloat("High Pass Filter(Test-No Save)", "fc", ref fc, "0.00", "Hz", 0.001F, 999F)
               && CProgram.sbReqBool("Low Pass Filter(Test-No Save) " + fc.ToString("0.00") + " Hz", "Butterworth 3 order (off=1RC)", ref bB3)
               && CProgram.sbReqBool("High Pass Filter(Test-No Save) " + fc.ToString("0.00") + " Hz", "All Channels (off = zoom channel " + strChNr + " only", ref bAllChannels))
            {
                if( _mRecordFile != null )
                {
                    bool bOk = bB3 ? _mRecordFile.mbDoButherworth3Filter(DFilterType.HighPass, fc, bAllChannels, _mZoomChannel)
                        : _mRecordFile.mbDoHighPassFilter(fc, bAllChannels, _mZoomChannel);

                    if( bOk )
                    {
                        _mTextHPF = fc.ToString("0.00") + (bB3 ? " B3" : " RC");

                        mbInitCharts(false);
                        mInitStripChannels();
                        mInitZoomChannel();
                        BringToFront();

                    }
                }
            }
            mUpdateButtonHPF();
        }

        private void toolStripButtonMF_Click(object sender, EventArgs e)
        {
            mDoButtonMF();
        }
        private void mDoButtonMF()
        {
            float fc = 50F;
            bool bAllChannels = true;
            string strChNr = (_mZoomChannel + 1).ToString();

            bool bB3 = true;

            if (CProgram.sbReqFloat("Mains Band Block Filter(Test-No Save)", "fc", ref fc, "0.00", "Hz", 0.001F, 999F)
               && CProgram.sbReqBool("Low Pass Filter(Test-No Save) " + fc.ToString("0.00") + " Hz", "Butterworth 3 order (off=AvgHist)", ref bB3)
                && CProgram.sbReqBool("Mains Filter(Test-No Save) " + fc.ToString("0.00") + " Hz", "All Channels (off = zoom channel " + strChNr + " only", ref bAllChannels))
            {
                if (_mRecordFile != null)
                {
                    bool bOk = bB3 ? _mRecordFile.mbDoButherworth3Filter(DFilterType.BandBlock, fc, bAllChannels, _mZoomChannel)
                        : _mRecordFile.mbDoBandBlockAvgHistoryFilter(fc, bAllChannels, _mZoomChannel);

                    if (bOk)
                    {
                        _mTextBBF = fc.ToString("0.00") + (bB3 ? " B3" : " ah");

                        mbInitCharts(false);
                        mInitStripChannels();
                        mInitZoomChannel();
                        BringToFront();

                    }
                }
            }
            mUpdateButtonBBF();
        }
        private void toolStripButtonAvgCR_Click(object sender, EventArgs e)
        {
            if (CProgram.sbCtrlKeyIsDown())
            {
                mDoButtonShiftTime();
            }
            else if( CProgram.sbShiftKeyIsDown())
            {
                mDoButtonAddNoise();
            }
            else if( CProgram.sbAltKeyIsDown())
            {
                mDoButtonAltAmpl();
            }
            else
            {
                mDoButtonAvgClipReset();
            }
        }
        private void mDoButtonAvgClipReset()
        {
            bool bUseStoredHistory = true;
            UInt32 avgTimeMilSec = 0;
            float deltamV = 0;
            float deltaFactor = 0.5F;
            float clipmV = 0;
            float resetmV = 0;
            bool bAllChannels = true;
            string strChNr = (_mZoomChannel + 1).ToString();


            bool bOk = CProgram.sbReqUInt32("Average Filter(Test-No Save)", "Average time (0=off)", ref avgTimeMilSec, "mSec", 0, 99999);
            bool bDoAvg = false;
            bool bDoCR = false;

            if (bOk)
            {
                if (avgTimeMilSec > 0)
                {
                    bOk = CProgram.sbReqBool("Average Filter(Test-No Save)", "Use History buffer", ref bUseStoredHistory)
                        && CProgram.sbReqFloat("Average Filter(Test-No Save)", "Apply deltas bigger(0=off)", ref deltamV, "0.0000", "mV", 0, 99999)
                        && (deltamV < 0.01 || CProgram.sbReqFloat("Average Filter(Test-No Save)", "Apply delta factor", ref deltaFactor, "0.00", "mV", 0, 99999));

                    bDoAvg = bOk;
                }
            }
            if (bOk)
            {
                bOk = CProgram.sbReqFloat("Clip Range(Test-No Save)", "Clip range(0=off)", ref clipmV, "0.00", "Hz", 0.00F, 999F);

                if (bOk && clipmV > 0.01)
                {
                    bOk = CProgram.sbReqFloat("Clip Range(Test-No Save)", "Clip to value(0=mid line)", ref resetmV, "0.00", "mV", -9999F, 9999F);

                    bDoCR = bOk;
                }
                if (bOk && _mRecordFile != null
                && CProgram.sbReqBool("Avg Clip Reset Filter(Test-No Save) " + avgTimeMilSec + "ms", "All Channels (off = zoom channel " + strChNr + " only", ref bAllChannels))
                {
                    string s = "";

                    if (bDoAvg)
                    {
                        bOk = _mRecordFile.mbDoAvgHistoryFilter(bUseStoredHistory, avgTimeMilSec * 0.001F, bAllChannels, _mZoomChannel, deltamV, deltaFactor);
                        if (bOk)
                        {
                            if (deltamV > 0.01)
                            {
                                s += "d";
                            }
                            s += avgTimeMilSec + "ms";
                        }
                    }
                    if (bOk && bDoCR)
                    {
                        bOk = _mRecordFile.mbDoClipResetFilter(clipmV, bAllChannels, _mZoomChannel, resetmV);
                        if (bOk)
                        {
                            s += "+C";
                        }
                    }
                    if (bOk)
                    {
                        _mTextACRF = s;

                        mbInitCharts(false);
                        mInitStripChannels();
                        mInitZoomChannel();
                        BringToFront();


                    }
                }
            }
            mUpdateButtonAvgClipReset();
        }
        private void mDoButtonShiftTime()
        {
            Int32 shiftTimeMilSec = 0;
            bool bAllChannels = true;
            string strChNr = (_mZoomChannel + 1).ToString();


            bool bOk = CProgram.sbReqInt32("Shift Time(Test-No Save)", "Shift time (0=off)", ref shiftTimeMilSec, "mSec", -99999, 99999);

            if (bOk)
            {
                if (bOk && _mRecordFile != null
                && CProgram.sbReqBool("Shift Time(Test-No Save) " + shiftTimeMilSec + "ms", "All Channels (off = zoom channel " + strChNr + " only", ref bAllChannels))
                {
                        bOk = _mRecordFile.mbDoShiftTime(shiftTimeMilSec * 0.001F, bAllChannels, _mZoomChannel);
                    if (bOk)
                    {
                        _mTextACRF = "t"+ shiftTimeMilSec+"ms";

                        mbInitCharts(false);
                        mInitStripChannels();
                        mInitZoomChannel();
                        BringToFront();
                    }
                }
            }
            mUpdateButtonAvgClipReset();
        }
        private void mDoButtonAddNoise()
        {
            if (_mRecordFile != null)
            {
                bool bUpdate = false;
                UInt16 nrChannels = _mRecordFile.mNrSignals;
                string strChNr = (_mZoomChannel + 1).ToString();
                string title = _mAnalyzeSubTitle + " Test-No Save";
                float noisemV = 0.2F;
                float noiseHz = 50.0F;
                float noisePhaseDeg = 0;
                bool bAllChannels = false;

                if (_mNrSignals > 0)
                {
                    if (CProgram.sbReqFloat(title, "Add noise", ref noisemV, "0.000", "mV", 0.000F, 99999F)
                    && CProgram.sbReqFloat(title, "Add noise frequency", ref noiseHz, "0.000", "Hz", 0.001F, 99999F)
                    && CProgram.sbReqFloat(title, "Add noise phase", ref noisePhaseDeg, "0.0", "deg", -720F, 720F)
                    && CProgram.sbReqBool("Add noise(Test-No Save) " + noiseHz.ToString("0.0") + " Hz", "All Channels (off = zoom channel " + strChNr + " only", ref bAllChannels))
                    {
                        bUpdate = _mRecordFile.mbDoAddNoise(bAllChannels, _mZoomChannel, noiseHz, noisemV, noisePhaseDeg);
                    }
                }
                if (bUpdate)
                {
                    mbInitCharts(false);
                    mInitStripChannels();
                    mInitZoomChannel();
                }
                BringToFront();
            }
        }
        private void mDoButtonAltAmpl()
        {
            if (_mRecordFile != null)
            {
                bool bUpdate = false;
                UInt16 nrChannels = _mRecordFile.mNrSignals;
                string strChNr = (_mZoomChannel + 1).ToString();
                string title = _mAnalyzeSubTitle + " Alt Amplitude - Test-No Save";
                float basemV = 0.0F;
                float scale = 1.0F;
                float offset = 0.0F;
                bool bAllChannels = false;

                if (_mNrSignals > 0)
                {
                    if (CProgram.sbReqFloat(title, "Base amplitude", ref basemV, "0.000", "mV", -9999F, 99999F)
                    && CProgram.sbReqFloat(title, "Scale amplitude", ref scale, "0.000", "*", -9999F, 99999F)
                    && CProgram.sbReqFloat(title, "Offset amplitude", ref offset, "0.0", "mV", -999F, 99999F)
                    && CProgram.sbReqBool(title, "All Channels (off = zoom channel " + strChNr + " only", ref bAllChannels))
                    {
                        bUpdate = _mRecordFile.mbDoAltAmpl(bAllChannels, _mZoomChannel, basemV, scale, offset);
                    }
                }
                if (bUpdate)
                {
                    mbInitCharts(false);
                    mInitStripChannels();
                    mInitZoomChannel();
                }
                BringToFront();
            }
        }


        private void sinusToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mDoMenuSimSinus();
        }

        private void toolStripButtonACDC_Click(object sender, EventArgs e)
        {
            mMeasureACDC();
        }

        private void mMeasureACDC()
        {
            if (_mRecordFile != null)
            {
                float periodSec = 0;
                float pulseSec = 0;
                uint nPulses = 0;
                float dcmV = 0;
                float acmVrms = 0;
                float minmV = 0;
                float maxmV = 0;
                string headerLine = "";
                string valuesLine = "";
                UInt16 iChannel = _mZoomChannel;
                float durationSec = _mZoomDurationSec;
                float startSec = _mZoomCenterSec - durationSec / 2;
                float thresholdFactor = 0.1F;

                if (_mRecordFile.mbMeasureACDC(out periodSec, out pulseSec, out nPulses,
                    out dcmV, out acmVrms, out minmV,  out maxmV,
                    out headerLine, out valuesLine,
                    iChannel, startSec, durationSec, thresholdFactor))
                {
                    string s = headerLine + "\r\n" + valuesLine;

                    Clipboard.SetText(valuesLine);

                    ReqTextForm.sbShowText("Measured ACDC (on Clipboard) min 2 and hole periods!", "result AC/DC", s);
                }
            }
        }

        private void sinusRangeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mDoMenuSimSinusRange();
        }

        private void toolStripButtonAddChannel_Click(object sender, EventArgs e)
        {
            mDoButtonAddChannel();
        }
        private void mDoButtonAddChannel()
        {
            if( _mRecordFile != null )
            {
                bool bUpdate = false;
                UInt16 nrChannels = _mRecordFile.mNrSignals;
                string strChNr = (_mZoomChannel + 1).ToString();
                string title = _mAnalyzeSubTitle + " Test-No Save";

                if ( _mNrSignals > 0)
                {
                    if (CProgram.sbCtrlKeyIsDown())
                    {
                        // delete channel

                        if( CProgram.sbAskOkCancel(this, title, "Delete Channel " + strChNr + "?"))
                        {
                            //bUpdate = _mRecordFile.
                            bUpdate = _mRecordFile.mbDoDeleteChannel(_mZoomChannel);
                        }
                    }
                    else if (CProgram.sbShiftKeyIsDown())
                    {
                        if (CProgram.sbAltKeyIsDown())
                        {
                            // Move zoom to ch
                            UInt16 ch = 0;
                            if( CProgram.sbReqUInt16(title, "Move " + strChNr + " to channel(0=first, 99=last)", ref ch, "", 0, 999))
                            {
                                if (ch > 0) --ch;
                                

                                bUpdate = _mRecordFile.mbDoMoveChannel(_mZoomChannel, ch);

                            }
                        }
                        else
                        {
                            // copy zoom channel to tail
                            if (CProgram.sbAskOkCancel(this, title, "Copy Channel " + strChNr + "?"))
                            {
                                bUpdate = _mRecordFile.mbDoCopyChannel(_mZoomChannel);
                            }
                        }
                    }
                    else if (CProgram.sbAltKeyIsDown())
                    {
                        // Add delta( zoom - ch)
                        UInt16 ch = (UInt16)(_mZoomChannel+1);
                        if (CProgram.sbReqUInt16(title, "Add delta channel(" + strChNr + ")  channel", ref ch, "("+ strChNr + "=derive)", 0, 999))
                        {
                            if (ch > 0) --ch;
                            if (_mZoomChannel == ch)
                            {
                                bUpdate = _mRecordFile.mbDoDeriveChannel(_mZoomChannel);

                            }
                            else
                            {
                                bUpdate = _mRecordFile.mbDoDeltaChannel(_mZoomChannel, ch);
                            }
                        }
                    }
                    else
                    {
                        // add lead3(II-I) & aVR+aVL+aVF
                        bool bAddCh3 = nrChannels <= 2;
                        bool bAddChV = nrChannels < 12 && nrChannels >= 2;

                        if (CProgram.sbReqBool(title, "Calc lead III = II-I", ref bAddCh3)
                            && CProgram.sbReqBool(title, "Calc derived leads (aVR,aVL,aVF)", ref bAddChV)
                            )
                        {
                            if (bAddChV && nrChannels == 2)
                            {
                                bAddCh3 = true; // 2 leads need lead III as well
                            }

                            if (bAddCh3)
                            {
                                UInt16 newChNr = nrChannels;

                                bUpdate = _mRecordFile.mbDoDeltaChannel(1, 0);

                                if (bUpdate)
                                {
                                    bUpdate = _mRecordFile.mbDoMoveChannel(newChNr, 2); // move new Lead3 to ch3 
                                    nrChannels = _mRecordFile.mNrSignals;
                                }
                                if( false == bUpdate )
                                {
                                    bAddChV = false;    // fails
                                }
                            }
                            if(bAddChV)
                            {
                                bUpdate = _mRecordFile.mbDoCalcDerivedChannels();
                            }
                        }
                    }
                }
                if( bUpdate )
                {
                    mbInitCharts(false);
                    mInitStripChannels();
                    mInitZoomChannel();
                }
                BringToFront();
            }
        }
        private void mDoButtonCalcRR()
        {
            if (_mRecordFile != null)
            {
                bool bUpdate = false;
                UInt16 nrChannels = _mRecordFile.mNrSignals;
                string strChNr = (_mZoomChannel + 1).ToString();
                string title = _mAnalyzeSubTitle + " Test-No Save";
                UInt16 channelMaskRR = (UInt16)(1 << _mZoomChannel);
                UInt16 channelMaskPri = (UInt16)(1 << _mZoomChannel);
                float preAvgTimeSec = 0.02F;     // pre filter signal to get rid of noise
                float postAvgTimeSec = 0.1F;     // 80..140 ms -> 100ms both 50Hz and 60Hz filtering
                float startPeak = 0.3F;
                float thresholdFactor = 0.5F;
                UInt16 maxBPM = 400;
                bool bAddChannelRR = true;
                bool bAddChannelPri = true;
                float hpfFreqHz = 0.05F;
                bool bQuadraticDelta = false;

                if (_mNrSignals > 0)
                {
                    if (CProgram.sbReqUInt16(title, "Calc RR(" + strChNr + ")  channel mask RR", ref channelMaskRR, "", 0, 999)
                        && CProgram.sbReqUInt16(title, "Calc RR(" + strChNr + ")  channel mask Pri", ref channelMaskPri, "", 0, 999)
                        && CProgram.sbReqFloat(title, "Calc RR(" + strChNr + ")  pre avg time", ref preAvgTimeSec, "0.000", "sec", 0, 2.0F)
                        && CProgram.sbReqFloat(title, "Calc RR(" + strChNr + ")  post avg time", ref postAvgTimeSec, "0.000", "sec", 0, 2.0F)
                        && CProgram.sbReqUInt16(title, "Calc RR(" + strChNr + ")  max BPM", ref maxBPM, "", 1, 999)
                        && CProgram.sbReqFloat(title, "Calc RR(" + strChNr + ")  hp filter Pri", ref hpfFreqHz, "0.000", "Hz", 0, 999.0F)
                        && CProgram.sbReqFloat(title, "Calc RR(" + strChNr + ")  threshold factor", ref thresholdFactor, "0.000", "*", 0, 1000)
                        && CProgram.sbReqBool( title, "Calc RR(" + strChNr + ")  quadratic delta", ref bQuadraticDelta )

                        )
                    {

                        bUpdate = _mRecordFile.mbDoCalcRR(channelMaskRR, bAddChannelRR, channelMaskPri, bAddChannelPri, preAvgTimeSec, postAvgTimeSec, 
                            startPeak, thresholdFactor, maxBPM, hpfFreqHz, bQuadraticDelta);
                    }
                }
                if (bUpdate)
                {
                    mbInitCharts(false);
                    mInitStripChannels();
                    mInitZoomChannel();
                    mInitHrAmplitudeGraph();
                }
                BringToFront();
            }
        }
        private void mDoButtonCalcPlethRR()
        {
            if (_mRecordFile != null)
            {
                bool bUpdate = false;
                UInt16 nrChannels = _mRecordFile.mNrSignals;
                string strChNr = (_mZoomChannel + 1).ToString();
                string title = _mAnalyzeSubTitle + " Test-No Save";
                float preAvgTimeSec = 0.0F;     // pre filter signal to get rid of noise
                float startTimeSec = 8.0F;     // time to find the first range (peak-valley) 2*60/15BPM
                float minPeak = 0.1F;
                float thresholdFactor = 0.5F;
                UInt16 maxBPM = 400;
                bool bAddChannelRR = true;

                if (_mNrSignals > 0)
                {
                    if (CProgram.sbReqFloat(title, "Calc Pleth RR(" + strChNr + ")  pre avg time", ref preAvgTimeSec, "0.000", "sec", 0, 2.0F)
                        && CProgram.sbReqFloat(title, "Calc Pleth RR(" + strChNr + ")  start time", ref startTimeSec, "0.000", "sec", 0, 200.0F)
                        && CProgram.sbReqFloat(title, "Calc Pleth RR(" + strChNr + ")  min peak", ref minPeak, "0.000", "mV", 0, 20.0F)
                        && CProgram.sbReqFloat(title, "Calc Pleth RR(" + strChNr + ")  threshold factor", ref thresholdFactor, "0.000", "*", 0, 1.0F)
                        && CProgram.sbReqUInt16(title, "Calc Pleth RR(" + strChNr + ")  avarege nr points", ref mPlethNrAvgPoints, "", 0, 1000)
                        )
                    {

                        bUpdate = _mRecordFile.mbDoCalcPlethRR(_mZoomChannel, bAddChannelRR, preAvgTimeSec, startTimeSec, minPeak, thresholdFactor, maxBPM);
                    }
                }
                if (bUpdate)
                {
                    mbInitCharts(false);
                    mInitStripChannels();
                    mInitZoomChannel();
                    mInitHrAmplitudeGraph();
                }
                BringToFront();
            }
        }
        private void mDoButtonInfoPlethRR()
        {
            if (_mRecordFile != null)
            {
                string title = _mAnalyzeSubTitle + " Test-No Save";
                float stripLengthSec = _mRecordFile.mGetSamplesTotalTime();
                string id = "R" + _mRecordDb.mIndex_KEY;

                if (_mRecordDb.mStudy_IX > 0)
                {
                    id += "_S" + _mRecordDb.mStudy_IX + "." + _mRecordDb.mSeqNrInStudy;
                }

                if (_mNrSignals > 0)
                {
                    if (CProgram.sbReqUInt16(title, "Info Pleth RR - avarege nr points", ref mPlethNrAvgPoints, "", 0, 1000)
                        )
                    {


                        float regionDuration = _mZoomDurationSec;
                        float regionStart = _mZoomCenterSec - regionDuration / 2;
                        string s = _mRecordFile.mInfoAnnotationCsv(id, _mRecordFile.mAnnotationAtr,
                            CAnnotationLine._cPlethSystole, CAnnotationLine._cPlethDiastole, mPlethNrAvgPoints,
                            regionStart, regionDuration, "Pleth", "*hg");

                        ReqTextForm.sbShowText("Pleth beat stats", "stats", s);
                    }
                }
                BringToFront();
            }
        }
        private void mDoButtonInfoEcgRR()
        {
            if (_mRecordFile != null)
            {
                string title = _mAnalyzeSubTitle + " Test-No Save";
                float stripLengthSec = _mRecordFile.mGetSamplesTotalTime();
                string id = "R" + _mRecordDb.mIndex_KEY;

                if (_mRecordDb.mStudy_IX > 0)
                {
                    id += "_S" + _mRecordDb.mStudy_IX + "." + _mRecordDb.mSeqNrInStudy;
                }

                if (_mNrSignals > 0)
                {
                    if (CProgram.sbReqUInt16(title, "Info ECG RR - avarege nr points", ref mEcgNrAvgPoints, "", 0, 1000)
                        )
                    {
                        float regionDuration = _mZoomDurationSec;
                        float regionStart = _mZoomCenterSec - regionDuration / 2;
                        string s = _mRecordFile.mInfoAnnotationCsv(id, _mRecordFile.mAnnotationAtr,
                            CAnnotationLine._cNormalBeat, mEcgNrAvgPoints,
                            regionStart, regionDuration, "ECG", "mV");

                        ReqTextForm.sbShowText("ECG beat stats", "stats", s);
                    }
                }
                BringToFront();
            }
        }


        private void buttonCalcRR_Click(object sender, EventArgs e)
        {
            if (CProgram.sbCtrlKeyIsDown())
            {
                if (CProgram.sbShiftKeyIsDown())
                {
                    mDoButtonInfoPlethRR();
                }
                else
                {
                    mDoButtonCalcPlethRR();
                }
            }
            else
            {
                if (CProgram.sbShiftKeyIsDown())
                {
                    mDoButtonInfoEcgRR();
                }
                else
                {
                    mDoButtonCalcRR();
                }
            }
        }

        private void triangleSawToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mDoMenuSimTriangle();
        }

        private void spikeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mDoMenuSimPeak();
        }

        private void toolStripZoomAT_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void listViewStrips_ItemActivate(object sender, EventArgs e)
        {
            mUpdateMeasurementList();
        }

        private void listViewStrips_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (e.Item != null)
            {
                {
                    if (false == _mbUpdatingStripList )
                    {
                        //                    
                        int index = e.Item.Index;
                        if( index != _mSelectedStripIndex)
                        {
                            mSetStripIndex(index);
                        }
                        CMeasureStrip ms = _mRecAnalysisFile.mGetStrip((UInt16)index);

                        if( ms != null )
                        {
                            ms._mbActive = e.Item.Checked;
                        }
                        mUpdateStatsList();
                    }
                }
            }
        }

        private void listViewMeasurements_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (e.Item != null)
            {
                {
                    if (false == _mbUpdatingMeasurementList)
                    {
                        int index = e.Item.Index;

                        CMeasurePoint mp = null;
                        CMeasureStrip ms = mGetSelectedMeasureStrip();

                        if (ms != null)
                        {
                            if( index != _mSelectedMeasurementIndex )
                            {
                                mSetMeasurementIndex(index);
                            }
                            mp = ms.mGetPoint((UInt16)index);

                            if( mp != null )
                            {
                                mp._mbActive = e.Item.Checked;
                            }
                        }
                        mUpdateStatsList();                       
                    }
                }
            }
        }
        public void mResetP1P2()
        {
            CRecordP1P2.sDisableP1P2();
        }
        public void mUpdateP1P2(bool AbUpdateCharts)
        {
            toolStripButtonP1.Text = CRecordP1P2.sGetP1Label();
            toolStripButtonP2.Text = CRecordP1P2.sGetP2Label();
            if (AbUpdateCharts)
            {
                // redraw charts to ger P1&P2 markers
//                mbInitCharts(false);
                mInitStripChannels();
                mInitZoomChannel();
            }
        }

        private void toolStripButtonP2_Click(object sender, EventArgs e)
        {
            mDoButtonP2();
        }

        private void toolStripButtonP1_Click(object sender, EventArgs e)
        {
            mDoButtonP1();
        }

        public void mSetP1(float ATimeSec, float AAmplitude)
        {
        }

        private void toolStripButtonStripP1_Click(object sender, EventArgs e)
        {
            mDoButtonStripP1();
        }

        private void mDoButtonStripP1()
        {
            CMeasureStrip ms = mGetSelectedMeasureStrip();

            if (ms != null)
            {
                CMeasurePoint mp = mGetSelectedMeasurePoint();

                if (mp != null && ms._mPointList != null)
                {
                    bool bShift = CProgram.sbShiftKeyIsDown();
                    float t = bShift ? mp._mPoint2T : mp._mPoint1T;
                    float a = bShift ? mp._mPoint2A : mp._mPoint1A;

                    CRecordP1P2.sSetP1(t, a);
                    mUpdateP1P2(true);
                }
            }
        }
        private void mDoButtonStripP2()
        {
            CMeasureStrip ms = mGetSelectedMeasureStrip();

            if (ms != null)
            {
                CMeasurePoint mp = mGetSelectedMeasurePoint();

                if (mp != null && ms._mPointList != null)
                {
                    bool bShift = CProgram.sbShiftKeyIsDown();
                    float t = bShift ? mp._mPoint2T : mp._mPoint1T;
                    float a = bShift ? mp._mPoint2A : mp._mPoint1A;

                    CRecordP1P2.sSetP2(t, a);
                    mUpdateP1P2(true);
                }
            }
        }

        private void toolStripMeasurmentName_Click(object sender, EventArgs e)
        {
            mDoMoveToMeasurement();
        }
        private void mDoMoveToMeasurement()
        {
            CMeasureStrip ms = mGetSelectedMeasureStrip();

            if (ms != null)
            {
                CMeasurePoint mp = mGetSelectedMeasurePoint();

                if (mp != null && ms._mPointList != null)
                {

                    bool bShift = CProgram.sbShiftKeyIsDown();
                    bool bCtrl = CProgram.sbCtrlKeyIsDown();
                    float t = bShift ? mp._mPoint2T : (bCtrl ? mp._mPoint1T : 0.5F * (mp._mPoint1T + mp._mPoint2T ));
                    //                    float a = bShift ? mp._mPoint2A : (bCtrl ? mp._mPoint1A : 0.5F * (mp._mPoint1A + mp._mPoint2A));

                    if (_mZoomChannel != mp._mChannel)
                    {
                        _mZoomCenterSec = t;
                        _mZoomChannel = mp._mChannel;
                        mInitZoomChannel();
                        mInitStripChannels();
                    }
                    else
                    {
                        mSetZoomCenter(t);
                        mInitStripChannels();
                    }

                }
            }

        }

        private void pictureBoxGraph_MouseUp(object sender, MouseEventArgs e)
        {
            if (_mRecordFile != null)
            {
                float stripLengthSec = _mRecordFile.mGetSamplesTotalTime();
                int x = e.X;
                int w = pictureBoxGraph.Width;
                float t = (float)x / (float)w * stripLengthSec;

                mDoMoveSelected(_mZoomChannel, t);
            }

        }

        private void pictureBoxHR_Click(object sender, EventArgs e)
        {

        }

        private void panelGraphSeperator_MouseUp(object sender, MouseEventArgs e)
        {
            if (_mRecordFile != null)
            {
                float stripLengthSec = _mRecordFile.mGetSamplesTotalTime();
                int x = e.X;
                int left = 15;
                int right = 31;
                int w = panelGraphSeperator.Width - left - right;
                float t = (float)(x-left) / (float)w * stripLengthSec;

                mDoMoveSelected(_mZoomChannel, t);
            }

        }

         private void toolStripButtonStripP2_Click(object sender, EventArgs e)
        {
            mDoButtonStripP2();
        }

        private void mDoButtonP1()
        {
            if( CProgram.sbShiftKeyIsDown())
            {
                // shift: toggle P1 on/off
                bool b = CRecordP1P2.sbGetStateP1();
                CRecordP1P2.sSetStateP1(!b);
                mUpdateP1P2(true);
            }
            else if (CProgram.sbCtrlKeyIsDown())
            {
                //Ctrl: swap P1/P2 

                CRecordP1P2.sSwapP1P2();
                mUpdateP1P2(true);
            }
            else if (CProgram.sbAltKeyIsDown())
            {
                float t, a;
                bool b = CRecordP1P2.sbGetP1(out t, out a);

                if( b )
                {
                    if( CProgram.sbReqFloat( "Edit P1", "Time from start", ref t, "0.000", "sec", -999999F, 999999F)
                        && CProgram.sbReqFloat("Edit P1", "Amplitude", ref a, "0.000", "sec", -999999F, 999999F))
                    {
                        CRecordP1P2.sSetP1(t, a);
                        mUpdateP1P2(true);
                    }
                }
            }
            else
            {
                CRecordP1P2.sSetP1(_mPoint1T, _mPoint1A);
                mUpdateP1P2(true);
            }     
        }
        private void mDoButtonP2()
        {
            bool bAddMeasurement = false;

            if (CProgram.sbShiftKeyIsDown())
            {
                // shift: toggle P2 on/off
                bool b = CRecordP1P2.sbGetStateP2();
                CRecordP1P2.sSetStateP2(!b);
                mUpdateP1P2(true);
            }
            else if (CProgram.sbCtrlKeyIsDown())
            {
                //Ctrl: add measurement P2-P1
                bool b1 = CRecordP1P2.sbGetStateP1();
                bool b2 = CRecordP1P2.sbGetStateP2();

                if (false == b1)
                {
                    CProgram.sbAskOkCancel(this, "Ctrl P2: Add Measurement", "P1 not set!");
                }
                else if (false == b2)
                {
                    CProgram.sbAskOkCancel(this, "Ctrl P2: Add Measurement", "P2 not set!");
                }
                else
                {
                    //                    CRecordP1P2.sSwapP1P2();
                    // add measurement to current strip
                    bAddMeasurement = true;
                    mUpdateP1P2(true);
                }               
            }
            else if (CProgram.sbAltKeyIsDown())
            {
                float t, a;
                bool b = CRecordP1P2.sbGetP2(out t, out a);

                if (b)
                {
                    if (CProgram.sbReqFloat("Edit P2", "Time from start", ref t, "0.000", "sec", -999999F, 999999F)
                        && CProgram.sbReqFloat("Edit P2", "Amplitude", ref a, "0.000", "sec", -999999F, 999999F))
                    {
                        CRecordP1P2.sSetP2(t, a);
                        mUpdateP1P2(true);
                    }
                }
            }
            else
            {
                CRecordP1P2.sSetP2(_mPoint1T, _mPoint1A);
                // add measurement 
                bAddMeasurement = true;
                mUpdateP1P2(true);
            }
            if( bAddMeasurement)
            {
                float t1, a1, t2, a2;
                bool b1 = CRecordP1P2.sbGetP1(out t1, out a1);
                bool b2 = CRecordP1P2.sbGetP2(out t2, out a2);
                if( b1 && b2 )
                {
                    DateTime atDT = _mRecordFile.mBaseUTC.AddSeconds(t1);
                    float dt = t2 - t1;
                    float da = a2 - a1;
                    string s = CProgram.sDateTimeToString(atDT) 
                        + " dT=" + dt.ToString("0.0000") + " s, dA=" + da.ToString("0.0000") + " mV";
                    if(CProgram.sbAskOkCancel(this, "Ctrl P2: Add Measurement", s))
                    {
                        // start popup middle of zoom channel

                        _mPoint1T = t1;
                        _mPoint1A = a1;
                        _mPoint2T = t2;
                        _mPoint2A = a2;
                        BringToFront();
                        contextMenuDualClick.Show(panelZoomStrip.PointToScreen(new Point(panelZoomStrip.Width/2, 10)));
                    }
                }

            }
        }
    }
}