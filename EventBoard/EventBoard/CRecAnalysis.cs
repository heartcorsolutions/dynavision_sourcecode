﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Program_Base;
using Event_Base;
using System.IO;
using System.Drawing;

namespace EventBoard
{
    public enum DMeasure1Tag
    {
        Unknown,
        Time,
        Amp,
        NrTags
    }

    public enum DMeasure2Tag
    {
        Unknown,
        RRtime,
        PRtime,
        QRStime,
        QTtime,
        RRqtc,  // RR time measurement for QTcB/QTcF calculation
        Pamp,
        Qamp,
        Ramp,
        Samp,
        Tamp,
        NrTags
    }

    public class CMeasurePoint
    {
        public bool _mbActive;
        public bool _mbDualPoint;
        public UInt16 _mTagID;
        public UInt16 _mChannel;
        public float _mPoint1T, _mPoint1A;
        public float _mPoint2T, _mPoint2A;

        public CMeasurePoint()
        {
            _mbActive = false;
            _mbDualPoint = false;
            _mTagID = 0;
            _mChannel = 0;

            _mPoint1T = _mPoint1A = _mPoint2T = _mPoint2A = 0.0F;
        }
        public CMeasurePoint( DMeasure1Tag ATag, UInt16 AChannel, float APoint1T, float APoint1A )
        {
            _mbActive = true;
            _mbDualPoint = false;
            _mTagID = (UInt16)ATag;
            _mChannel = AChannel;

            _mPoint1T = APoint1T;
            _mPoint1A = APoint1A;
            _mPoint2T = _mPoint2A = 0.0F;
        }

        public CMeasurePoint(DMeasure2Tag ATag, UInt16 AChannel, float APoint1T, float APoint1A, float APoint2T, float APoint2A)
        {
            _mbActive = true;
            _mbDualPoint = true;
            _mTagID = (UInt16)ATag;
            _mChannel = AChannel;

            _mPoint1T = APoint1T;
            _mPoint1A = APoint1A;
            _mPoint2T = APoint2T;
            _mPoint2A = APoint2A;
        }
        public void mInvertAmplitude()
        {
            _mPoint1A = -_mPoint1A;
            _mPoint2A = -_mPoint2A;
        }

        public string mGetTagString()
        {
            string s = "";

            if (_mbDualPoint)
            {
                s = sGetMeasure2TagString((DMeasure2Tag)_mTagID);
            }
            else
            {
                s = sGetMeasure1TagString((DMeasure1Tag)_mTagID);
            }
            return s;
        }
        public string mGetShowString()
        {
            string s = "";

            if (_mbDualPoint)
            {
                s = sGetMeasure2ShowString((DMeasure2Tag)_mTagID);
            }
            else
            {
                s = sGetMeasure1ShowString((DMeasure1Tag)_mTagID);
            }
            return s;
        }
        public float mGetValue()
        {
            float f = 0.0F;

            if (_mbDualPoint)
            {
                f = sbGetMeasure2UseAmp((DMeasure2Tag)_mTagID) ? _mPoint2A - _mPoint1A : _mPoint2T - _mPoint1T;
            }
            else
            {
                f = sbGetMeasure1UseAmp((DMeasure1Tag)_mTagID) ? _mPoint1A : _mPoint1T;
            }
            return f;
        }
        public string mGetValueString()
        {
            string s = "";

            if (_mbDualPoint)
            {
                if( sbGetMeasure2UseAmp((DMeasure2Tag)_mTagID) )
                {
                    float f = _mPoint2A - _mPoint1A;

                    s = f.ToString("0.000");
                }
                else
                {
                    float f = _mPoint2T - _mPoint1T;

                    s = f.ToString("0.000");
                }
            }
            else
            {
                if (sbGetMeasure1UseAmp((DMeasure1Tag)_mTagID))
                {
                    s = _mPoint1A.ToString("0.000");
                }
                else
                {
                    s = _mPoint1T.ToString("0.000");
                }
            }
            return s;
        }
        public string mGetUnit()
        {
            string s = "";

            if (_mbDualPoint)
            {
                s = sGetMeasure2TagUnit((DMeasure2Tag)_mTagID);
            }
            else
            {
                s = sGetMeasure1TagUnit((DMeasure1Tag)_mTagID);
            }
            return s;
        }

        public static string sGetMeasure1TagString(DMeasure1Tag ATagID)
        {
            switch (ATagID)
            {
                case DMeasure1Tag.Time: return "Time";
                case DMeasure1Tag.Amp: return "Amp";
            }
            return "?";
        }
        public static string sGetMeasure1ShowString(DMeasure1Tag ATagID)
        {
            switch (ATagID)
            {
                case DMeasure1Tag.Time: return "Time";
                case DMeasure1Tag.Amp: return "Amplitude";
            }
            return "?";
        }
        public static bool sbGetMeasure1UseAmp(DMeasure1Tag ATagID)
        {
            switch (ATagID)
            {
                case DMeasure1Tag.Time: return false;
                case DMeasure1Tag.Amp: return true;
            }
            return false;
        }
        public static string sGetMeasure1TagUnit(DMeasure1Tag ATagID)
        {
            switch (ATagID)
            {
                case DMeasure1Tag.Time: return "Sec";
                case DMeasure1Tag.Amp: return "mV";
            }
            return "?";
        }

        public static DMeasure1Tag sGetMeasure1TagFromString(string AString)
        {
            for (DMeasure1Tag i = DMeasure1Tag.Unknown; ++i < DMeasure1Tag.NrTags;)
            {
                if (AString == sGetMeasure1TagString(i))
                {
                    return i;
                }
            }
            return DMeasure1Tag.Unknown;
        }
        public static DMeasure1Tag sGetMeasure1TagFromShow(string AString)
        {
            for (DMeasure1Tag i = DMeasure1Tag.Unknown; ++i < DMeasure1Tag.NrTags;)
            {
                if (AString == sGetMeasure1ShowString(i))
                {
                    return i;
                }
            }
            return DMeasure1Tag.Unknown;
        }

        public static string sGetMeasure2TagCode(DMeasure2Tag ATagID)
        {
            switch (ATagID)
            {
                case DMeasure2Tag.RRtime: return "RR";
                case DMeasure2Tag.PRtime: return "PR";
                case DMeasure2Tag.QRStime: return "QRS";
                case DMeasure2Tag.QTtime: return "QT";
                case DMeasure2Tag.RRqtc: return "RRqtc";
                case DMeasure2Tag.Pamp: return "P";
                case DMeasure2Tag.Qamp: return "Q";
                case DMeasure2Tag.Ramp: return "R";
                case DMeasure2Tag.Samp: return "S";
                case DMeasure2Tag.Tamp: return "T";
            }
            return "?";
        }
        public static string sGetMeasure2TagString(DMeasure2Tag ATagID)
        {
            switch (ATagID)
            {
                case DMeasure2Tag.RRtime: return "RR time";
                case DMeasure2Tag.PRtime: return "PR time";
                case DMeasure2Tag.QRStime: return "QRS time";
                case DMeasure2Tag.QTtime: return "QT time";
                case DMeasure2Tag.RRqtc: return "RRqtc time";
                case DMeasure2Tag.Pamp: return "P amp";
                case DMeasure2Tag.Qamp: return "Q amp";
                case DMeasure2Tag.Ramp: return "R amp";
                case DMeasure2Tag.Samp: return "S amp";
                case DMeasure2Tag.Tamp: return "T amp";
            }
            return "?";
        }
        public static string sGetMeasure2ShowString(DMeasure2Tag ATagID)
        {
            switch (ATagID)
            {
                case DMeasure2Tag.RRtime: return "RR time";
                case DMeasure2Tag.PRtime: return "PR time";
                case DMeasure2Tag.QRStime: return "QRS time";
                case DMeasure2Tag.QTtime: return "QT time";
                case DMeasure2Tag.RRqtc: return "RRqtc time";
                case DMeasure2Tag.Pamp: return "P amplitude";
                case DMeasure2Tag.Qamp: return "Q amplitude";
                case DMeasure2Tag.Ramp: return "R amplitude";
                case DMeasure2Tag.Samp: return "S amplitude";
                case DMeasure2Tag.Tamp: return "T amplitude";
            }
            return "?";
        }
        public static bool sbGetMeasure2UseAmp(DMeasure2Tag ATagID)
        {
            switch (ATagID)
            {
                case DMeasure2Tag.RRtime: return false;
                case DMeasure2Tag.PRtime: return false;
                case DMeasure2Tag.QRStime: return false;
                case DMeasure2Tag.QTtime: return false;
                case DMeasure2Tag.RRqtc: return false;
                case DMeasure2Tag.Pamp: return true;
                case DMeasure2Tag.Qamp: return true;
                case DMeasure2Tag.Ramp: return true;
                case DMeasure2Tag.Samp: return true;
                case DMeasure2Tag.Tamp: return true;
            }
            return false;
        }
        public static string sGetMeasure2TagUnit(DMeasure2Tag ATagID)
        {
            switch (ATagID)
            {
                case DMeasure2Tag.RRtime: return "Sec";
                case DMeasure2Tag.PRtime: return "Sec";
                case DMeasure2Tag.QRStime: return "Sec";
                case DMeasure2Tag.QTtime: return "Sec";
                case DMeasure2Tag.RRqtc: return "Sec";
                case DMeasure2Tag.Pamp: return "mV";
                case DMeasure2Tag.Qamp: return "mV";
                case DMeasure2Tag.Ramp: return "mV";
                case DMeasure2Tag.Samp: return "mV";
                case DMeasure2Tag.Tamp: return "mV";
            }
            return "?";
        }
        public static DMeasure2Tag sGetMeasure2TagFromString(string AString)
        {
            for (DMeasure2Tag i = DMeasure2Tag.Unknown; ++i < DMeasure2Tag.NrTags;)
            {
                if (AString == sGetMeasure2TagString(i))
                {
                    return i;
                }
            }
            return DMeasure2Tag.Unknown;
        }
        public static DMeasure2Tag sGetMeasure2TagFromShow(string AString)
        {
            for (DMeasure2Tag i = DMeasure2Tag.Unknown; ++i < DMeasure2Tag.NrTags;)
            {
                if (AString == sGetMeasure2ShowString(i))
                {
                    return i;
                }
            }
            return DMeasure2Tag.Unknown;
        }
        public string mGetActiveString()
        {
            return _mbActive ? "+" : "";
        }

    }
    public class CMeasureStrip
    {
        public bool _mbActive;

        public UInt16 _mChannel;
        public float _mStartTimeSec;
        public float _mDurationSec;
        public float _mStartA;
        public float _mEndA;

        public Image _mStripImage;
        public string _mStripImageName;

        public List<CMeasurePoint> _mPointList;

        public CRecAnalysis _mRecAnalysis;

        public CMeasureStrip()
        {
            _mbActive = false;
            _mChannel = 0;
            _mStartTimeSec = 0.0F;
            _mDurationSec = 0.0F;
            _mStartA = 0.0F;
            _mEndA = 0.0F;
            _mStripImage = null;
            _mStripImageName = null;

        _mPointList = new List<CMeasurePoint>();
        }
        public CMeasureStrip( UInt16 AChannel, float AStartTimeSec, float ADurationSec, float AStartA, float AEndA, Image AImage )
        {
            _mbActive = true;
            _mChannel = AChannel;
            _mStartTimeSec = AStartTimeSec;
            _mDurationSec = ADurationSec;
            _mStartA = AStartA;
            _mEndA = AEndA;
            _mStripImage = AImage;
            _mStripImageName = null;

            _mPointList = new List<CMeasurePoint>();
        }
        public void mChangeStrip(UInt16 AChannel, float AStartTimeSec, float ADurationSec, float AStartA, float AEndA, Image AImage)
        {
            _mbActive = true;
            _mChannel = AChannel;
            _mStartTimeSec = AStartTimeSec;
            _mDurationSec = ADurationSec;
            _mStartA = AStartA;
            _mEndA = AEndA;
            _mStripImage = AImage;
            _mStripImageName = null;
        }
        public void mSetStripImage( Image AImage )
        {
            _mStripImage = AImage;
            _mStripImageName = null; // fill in later
        }

        public bool mbAddPoint(CMeasurePoint APoint)
        {
            bool bOk = false;

            if (_mPointList != null && APoint != null)
            {
                _mPointList.Add(APoint);
                bOk = true;
            }

            return bOk;
        }
        public CMeasurePoint mGetPoint(UInt16 AIndex)
        {
            CMeasurePoint p = null;

            if (_mPointList != null && AIndex < _mPointList.Count)
            {
                p = _mPointList[AIndex];
            }
            return p;
        }
        public string mGetActiveString()
        {
            return _mbActive ? "+" : "";
        }
        public UInt16 mGetCount()
        {
            return (UInt16)( (_mPointList == null) ? 0 : _mPointList.Count );
        }

    }
    public class CMeasureStat
    {
        public bool _mbActive;
        public bool _mbShowEmpty;
        public bool _mbDualPoint;
        public uint _mTagID;

        public UInt16 _mCount;
        public float _mMin;
        public float _mMax;
        public float _mMean;

        public CMeasureStat()
        {
            _mbActive = false;
            _mbShowEmpty = false;
            _mbDualPoint = false;
            _mTagID = 0;
            _mCount = 0;
            _mMin = _mMax = _mMean = 0;
        }
        public CMeasureStat(bool AbDualPoint, UInt16 ATag, bool AbShowEmpty)
        {
            _mbActive = true;
            _mbShowEmpty = AbShowEmpty;
            _mbDualPoint = AbDualPoint;
            _mTagID = ATag;
            _mCount = 0;
            _mMin = _mMax = _mMean = 0;
        }
        public CMeasureStat(DMeasure1Tag ATag, bool AbShowEmpty)
        {
            _mbActive = true;
            _mbShowEmpty = AbShowEmpty;
            _mbDualPoint = false;
            _mTagID = (UInt16)ATag;
            _mCount = 0;
            _mMin = _mMax = _mMean = 0;
        }
        public CMeasureStat(DMeasure2Tag ATag, bool AbShowEmpty)
        {
            _mbActive = true;
            _mbShowEmpty = AbShowEmpty;
            _mbDualPoint = true;
            _mTagID = (UInt16)ATag;
            _mCount = 0;
            _mMin = _mMax = _mMean = 0;
        }
        public string mGetActiveString()
        {
            return _mbActive ? "+" : "";
        }
        public string mGetTagString()
        {
            string s = "";

            if (_mbDualPoint)
            {
                s = CMeasurePoint.sGetMeasure2TagString((DMeasure2Tag)_mTagID);
            }
            else
            {
                s = CMeasurePoint.sGetMeasure1TagString((DMeasure1Tag)_mTagID);
            }
            return s;
        }
        public string mGetShowString()
        {
            string s = "";

            if (_mbDualPoint)
            {
                s = CMeasurePoint.sGetMeasure2ShowString((DMeasure2Tag)_mTagID);
            }
            else
            {
                s = CMeasurePoint.sGetMeasure1ShowString((DMeasure1Tag)_mTagID);
            }
            return s;
        }
        public string mGetValueString( float AValue)
        {
            return AValue.ToString("0.000");
        }
        public string mGetUnit()
        {
            string s = "";

            if (_mbDualPoint)
            {
                s = CMeasurePoint.sGetMeasure2TagUnit((DMeasure2Tag)_mTagID);
            }
            else
            {
                s = CMeasurePoint.sGetMeasure1TagUnit((DMeasure1Tag)_mTagID);
            }
            return s;
        }
        public void mAddPoint( CMeasurePoint APoint)
        {
            if( APoint != null )
            {
                float f = Math.Abs( APoint.mGetValue());

                if ( _mCount == 0)
                {
                    _mMin = _mMean = _mMax = f;
                }
                else
                {
                    if (f < _mMin) _mMin = f;
                    if (f > _mMax) _mMax = f;
                    _mMean += f;
                }
                ++_mCount;
            }
        }

        public bool mbCalcMean()
        {
            bool b = false;

            if( _mCount > 0 )
            {
                _mMean /= _mCount;
                b = true;
            }
            return b;
        }
    }


    public class CRecAnalysisFile
    {
        public List<CMeasureStrip> _mStripList;
        public List<CMeasureStat> _mStatList;
        public UInt32 _mInvChannelsMask;

        public CRecAnalysisFile()
        {
            _mStripList = new List<CMeasureStrip>();
            _mStatList = new List<CMeasureStat>();
            _mInvChannelsMask = 0;
        }

        public void mClearAll()
        {
            if (_mStripList != null)
            {
                _mStripList.Clear();
            }
            if (_mStatList != null)
            {
                _mStatList.Clear();
            }
            _mInvChannelsMask = 0;
        }

        public CMeasureStrip mAddStrip(UInt16 AChannel, float AStartTimeSec, float ADurationSec, float AStartA, float AEndA, Image AImage)
        {
            CMeasureStrip ms = null;

            if (_mStripList != null && ADurationSec > 0.001 )
            {
                ms = new CMeasureStrip(AChannel, AStartTimeSec, ADurationSec, AStartA, AEndA, AImage);

                if (ms != null)
                {
                    _mStripList.Add(ms);
                }
            }
            return ms;
        }
        public bool mbAddStrip(CMeasureStrip AMs)
        {
            bool bOk = false;

            if (_mStripList != null && AMs != null)
            {
                _mStripList.Add(AMs);
                bOk = true;
            }

            return bOk;
        }
        public CMeasureStrip mGetStrip(UInt16 AIndex)
        {
            CMeasureStrip ms = null;

            if (_mStripList != null && AIndex < _mStripList.Count)
            {
                ms = _mStripList[AIndex];
            }
            return ms;
        }
        public bool mbAddStat(CMeasureStat AMs)
        {
            bool bOk = false;

            if (_mStatList != null && AMs != null)
            {
                _mStatList.Add(AMs);
                bOk = true;
            }

            return bOk;
        }
        public CMeasureStat mGetStat(UInt16 AIndex)
        {
            CMeasureStat ms = null;

            if (_mStatList != null && AIndex < _mStatList.Count)
            {
                ms = _mStatList[AIndex];
            }
            return ms;
        }

        public CMeasureStat mFindStat( bool AbDualPoint, UInt16 ATagID)
        {
            CMeasureStat msFound = null;

            foreach( CMeasureStat ms in _mStatList )
            {
                if( ms._mbDualPoint == AbDualPoint && ms._mTagID == ATagID)
                {
                    msFound = ms;
                    break;
                }
            }
            return msFound;
        }

        public bool mbFindQTc( out UInt16 ArN, out float ArRR, out float ArQT, out float ArQTcB, out float ArQTcF)
        {
            bool bFound = false;
            UInt16 n = 0;
            float rr = 0;
            float qt = 0;
            float qtcb = 0;
            float qtcf = 0;

            CMeasureStat ms;


            ms = mFindStat(true, (UInt16)DMeasure2Tag.RRqtc);
            if (ms != null && ms._mbActive && ms._mCount > 0)
            {
                rr = ms._mMean;
                n = ms._mCount;
            }

            ms = mFindStat(true, (UInt16)DMeasure2Tag.QTtime);
            if (ms != null && ms._mbActive && ms._mCount > 0)
            {
                qt = ms._mMean;
            }

            bFound = rr > 0.1 && qt > 0.001;
            if( bFound )
            {
                qtcb = mCalcQtcB(rr, qt);
                qtcf = mCalcQtcF(rr, qt);
            }

            ArN = n;
            ArRR = rr;
            ArQT = qt;
            ArQTcB = qtcb;
            ArQTcF = qtcf;
            return bFound;
        }

        public float mCalcQtcB(float ARR, float AQT)
        {
            float result = 0;

            if (ARR > 0.1 && AQT > 0.001)
            {
                result = (float)(AQT / Math.Pow(ARR, 0.5));
            }
            return result;
        }
        public float mCalcQtcF(float ARR, float AQT)
        {
            float result = 0;

            if (ARR > 0.1 && AQT > 0.001)
            {
                result = (float)(AQT / Math.Pow(ARR, 0.33333333333));
            }
            return result;
        }

        public UInt16 mCountActiveStrips()
        {
            UInt16 n = 0;

            foreach( CMeasureStrip ms in _mStripList)
            {
                if( ms._mbActive )
                {
                    ++n;
                }
            }
            return n;
        }

        public CMeasureStrip mFindActiveStrip(UInt16 AIndex)
        {
            CMeasureStrip msFound = null;
            UInt16 index = 0;

            foreach (CMeasureStrip ms in _mStripList)
            {
                if (ms._mbActive)
                {
                    if (index == AIndex)
                    {
                        msFound = ms;
                        break;
                    }
                    ++index;
                }
            }
            return msFound;
        }

        public int mGetStripIndex( CMeasureStrip AStrip)
        {
            int index = -1;

            if( AStrip != null )
            {
                index = 0;
                foreach (CMeasureStrip ms in _mStripList)
                {
                    if( ms == AStrip )
                    {
                        return index;
                    }
                    ++index;
                }
            }
            return index;
        }

        public void mRecalcStatAllStrips()
        {
            if (_mStatList != null)
            {
                _mStatList.Clear();

                mbAddStat(new CMeasureStat(DMeasure2Tag.RRtime, true));
                mbAddStat(new CMeasureStat(DMeasure2Tag.PRtime, true));
                mbAddStat(new CMeasureStat(DMeasure2Tag.QRStime, true));
                mbAddStat(new CMeasureStat(DMeasure2Tag.QTtime, true));

                foreach (CMeasureStrip ms in _mStripList)
                {
// use active only for drawing, calc stats on all active measurements                     if (ms._mbActive)
                    {
                        foreach (CMeasurePoint mp in ms._mPointList)
                        {
                            if (mp._mbActive)
                            {
                                CMeasureStat pstat = null;

                                foreach (CMeasureStat mstat in _mStatList)
                                {
                                    // find existing stat with same point tag
                                    if (mstat._mbDualPoint == mp._mbDualPoint
                                        && mstat._mTagID == mp._mTagID)
                                    {
                                        pstat = mstat;
                                        break;
                                    }
                                }
                                if (pstat == null)
                                {
                                    pstat = new CMeasureStat(mp._mbDualPoint, mp._mTagID, false);
                                    mbAddStat(pstat);
                                }
                                if (pstat != null)
                                {
                                    pstat.mAddPoint(mp);
                                }
                            }
                        }
                    }
                }

                // calculate mean
                foreach (CMeasureStat mstat in _mStatList)
                {
                    mstat.mbCalcMean();
                }
            }
        }

        public CMeasureStat mCalcStripStat(CMeasureStrip AMs, DMeasure2Tag ATagID)
        {
            CMeasureStat pStat = null;

            if (_mStripList != null && AMs != null)
            {
                pStat = new CMeasureStat(ATagID, false);

                if (pStat != null)
                {
                    foreach (CMeasurePoint mp in AMs._mPointList)
                    {
                        if (mp._mbActive && ATagID == (DMeasure2Tag)mp._mTagID)
                        {
                            pStat.mAddPoint(mp);
                        }
                    }
                    if (false == pStat.mbCalcMean())
                    {
                        pStat = null;
                    }
                }

            }
            return pStat;
        }


        /*        public CMeasureStat mCalcStripStat(UInt16 AStripIndex, DMeasure2Tag ATagID)
                {
                    CMeasureStat pStat = null;

                    if (_mStripList != null && AStripIndex < _mStripList.Count)
                    {
                        CMeasureStrip ms = _mStripList[AStripIndex];

                        if (ms != null)
                        {
                            pStat = new CMeasureStat(ATagID, false);

                            if (pStat != null)
                            {
                                foreach (CMeasurePoint mp in ms._mPointList)
                                {
                                    if (mp._mbActive && ATagID == (DMeasure2Tag)mp._mTagID)
                                    {
                                        pStat.mAddPoint(mp);
                                    }
                                }
                                if (false == pStat.mbCalcMean())
                                {
                                    pStat = null;
                                }
                            }
                        }
                    }
                    return pStat;
                }
        */

        public void mModifyInvertMask(UInt32 ASetInvMask, ref CRecordMit ALoadedRecord)

        {
            // set ECG with new invert mask

            foreach (CMeasureStrip ms in _mStripList)
            {
                UInt32 mask = 1U << ms._mChannel;

                if ((_mInvChannelsMask & mask) != (ASetInvMask & mask))
                {
                    ms._mStartA = -ms._mStartA;
                    ms._mEndA = -ms._mEndA;

                    foreach (CMeasurePoint mp in ms._mPointList)
                    {
                        mp.mInvertAmplitude();
                    }
                }
                foreach (CMeasureStat mstat in _mStatList)
                {
                    if (mstat._mbDualPoint && CMeasurePoint.sbGetMeasure2UseAmp((DMeasure2Tag)mstat._mTagID))
                    {
                        float min = -mstat._mMax;
                        float max = -mstat._mMin;

                        mstat._mMin = min;
                        mstat._mMax = max;
                        mstat._mMean = -mstat._mMean;
                    }
                }
            }
            if (ALoadedRecord != null)
            {
                ALoadedRecord.mModifyInvertMask(ASetInvMask);
            }    
            _mInvChannelsMask = ASetInvMask;
        }

        public bool mbSaveMeasureFile(string AFullName, UInt32 ARecordNr, UInt16 AAnalisysNr)
        {
            bool bOk = false;

            try
            {

                using (StreamWriter sw = new StreamWriter(AFullName))
                {
                    int nStrips = 0, nPoints = 0, nStats = 0, iStrip = 0, iPoint, iStat = 0;
                    DateTimeOffset dto = DateTimeOffset.Now;

                    sw.WriteLine("[Analysis]");
                    sw.WriteLine("Record= " + ARecordNr.ToString());
                    sw.WriteLine("Analysis= " + AAnalisysNr.ToString());
                    sw.WriteLine("writen= " + dto.ToString());


                    if (_mStripList != null)
                    {
                        nStrips = _mStripList.Count;
                    }
                    if (_mStatList != null)
                    {
                        nStats = _mStatList.Count;
                    }
                    sw.WriteLine("nrStrips= " + nStrips.ToString());
                    sw.WriteLine("nrStats= " + nStats.ToString());
                    sw.WriteLine("invChannels= " + _mInvChannelsMask.ToString());

                    foreach (CMeasureStrip ms in _mStripList)
                    {
                        ++iStrip;
                        iPoint = 0;
                        sw.WriteLine("[STRIP" + iStrip.ToString() + "]");

                        sw.WriteLine("active= " + (ms._mbActive ? "1" : "0"));
                        sw.WriteLine("channel= " + ms._mChannel.ToString());
                        sw.WriteLine("startSec= " + ms._mStartTimeSec.ToString("0.000"));
                        sw.WriteLine("durationSec= " + ms._mDurationSec.ToString("0.000"));
                        sw.WriteLine("startAmp= " + ms._mStartA.ToString("0.000"));
                        sw.WriteLine("endAmp= " + ms._mEndA.ToString("0.000"));
                        sw.WriteLine("image= " + ms._mStripImageName);

                        nPoints = ms._mPointList == null ? 0 : ms._mPointList.Count;
                        sw.WriteLine("nrPoints= " + nPoints.ToString());


                        foreach (CMeasurePoint mp in ms._mPointList)
                        {
                            ++iPoint;
                            sw.WriteLine("[POINT" + iPoint.ToString() + "strip" + iStrip.ToString() + "]");

                            /*        public bool _mbActive;
                                    public bool _mbDualPoint;
                                    public UInt16 _mTagID;
                                    public UInt16 _mChannel;
                                    public float _mPoint1T, _mPoint1A;
                                    public float _mPoint2T, _mPoint2A;
                            */
                            sw.WriteLine("active= " + (mp._mbActive ? "1" : "0"));
                            sw.WriteLine("dualPoint= " + (mp._mbDualPoint ? "1" : "0"));
                            sw.WriteLine("tag= " + mp.mGetTagString());
                            sw.WriteLine("value= " + mp.mGetValue().ToString("0.000"));

                            sw.WriteLine("channel= " + mp._mChannel.ToString());
                            sw.WriteLine("point1T= " + mp._mPoint1T.ToString("0.000"));
                            sw.WriteLine("point1A= " + mp._mPoint1A.ToString("0.000"));
                            if (mp._mbDualPoint)
                            {
                                sw.WriteLine("point2T= " + mp._mPoint2T.ToString("0.000"));
                                sw.WriteLine("point2A= " + mp._mPoint2A.ToString("0.000"));
                            }
                        }
                    }
                    if (nStats > 0)
                    {
                        foreach (CMeasureStat mstat in _mStatList)
                        {
                            ++iStat;
                            sw.WriteLine("[STAT" + iStat.ToString() + "]");
                            sw.WriteLine("active= " + (mstat._mbActive ? "1" : "0"));
                            sw.WriteLine("dualPoint= " + (mstat._mbDualPoint ? "1" : "0"));
                            sw.WriteLine("tag= " + mstat.mGetTagString());
                            sw.WriteLine("count= " + mstat._mCount.ToString());

                            sw.WriteLine("min= " + mstat._mMin.ToString("0.000"));
                            sw.WriteLine("max= " + mstat._mMax.ToString("0.000"));
                            sw.WriteLine("mmean= " + mstat._mMean.ToString("0.000"));

                            /*           public bool _mbActive;
                                    public bool _mbShowEmpty;
                                    public bool _mbDualPoint;
                                    public uint _mTagID;

                                    public UInt16 _mCount;
                                    public float _mMin;
                                    public float _mMax;
                                    public float _mMean;
                            */
                        }
                    }

                    bOk = true;
                    sw.Close();
                }

            }
            catch (Exception ex)
            {
                CProgram.sLogException("Error writing analysis file " + AFullName, ex);
            }
            return bOk;
        }

        enum DMF_GROUP { None, Analysis, Strip, Point, Stat };

        public bool mbLoadMeasureFile(string AFullName, UInt32 ARecordNr, UInt16 AAnalysisNr, UInt32 ADefaultInvChannels, ref CRecordMit ArLoadedRecord)
        {
            bool bOk = false;

            try
            {
                if (_mStripList.Count > 0)
                {
                    _mStripList.Clear();
                }
                if (_mStatList.Count > 0)
                {
                    _mStatList.Clear();
                }
                _mInvChannelsMask = ADefaultInvChannels;
                if ( false == File.Exists( AFullName))
                {
                    return false;
                }
                using (StreamReader sr = new StreamReader(AFullName))
                {
                    CMeasureStrip ms = null;
                    CMeasurePoint mp = null;

                    int pos;
                    UInt32 recordNr = 0;
                    UInt16 analysisNr = 0;
                    DMF_GROUP groupNr = DMF_GROUP.None;
                    string line, name, value;
                    char c;

                    while ((line = sr.ReadLine()) != null)
                    {
                        if (line.Length > 0)
                        {
                            c = line[0];
                            if (c == ';' || c == '/' || c == '\\')
                            {
                                // its a remark => do nothing
                            }
                            else if (c == '[')
                            {
                                // new group
                                mp = null;
                                groupNr = DMF_GROUP.None;

                                pos = line.IndexOf(']');
                                name = pos > 0 ? line.Substring(1, pos - 1 ): line.Substring(1);
                                name = name.ToUpper();

                                if (name.StartsWith("STRIP"))   //[STRIP3] number is discarded
                                {
                                    groupNr = DMF_GROUP.Strip;
                                    ms = null;
                                    if (_mStripList != null)
                                    {
                                        ms = new CMeasureStrip();

                                        if (ms != null)
                                        {
                                            _mStripList.Add(ms);
                                        }
                                    }
                                }
                                else if (name.StartsWith("POINT"))  //[POINT7strip3] number and strip is discarded
                                {
                                    groupNr = DMF_GROUP.Point;
                                    if (ms != null && ms._mPointList != null)
                                    {
                                        mp = new CMeasurePoint();

                                        if (mp != null)
                                        {
                                            ms._mPointList.Add(mp);
                                        }
                                    }
                                }
                                else if (name == "ANALYSIS")
                                {
                                    groupNr = DMF_GROUP.Analysis;

                                    ms = null;
                                }
                                else if (name == "STAT")
                                {

                                }

                            }
                            else if ((pos = line.IndexOf('=')) > 0)
                            {
                                name = line.Substring(0, pos);
                                                                    // name= value
                                value = line.Substring(pos + 2);

                                switch (groupNr)
                                {
                                    case DMF_GROUP.None:
                                        break;
                                    case DMF_GROUP.Analysis:
                                        mExtracMfAnalysis(name, value, ref recordNr, ref analysisNr);
                                        break;
                                    case DMF_GROUP.Strip:
                                        mExtracMfStrip(ms, name, value);
                                        break;
                                    case DMF_GROUP.Point:
                                        mExtracMfPoint(mp, name, value);
                                        break;
                                    case DMF_GROUP.Stat:
                                        break;
                                }
                            }

                        }
                    }
                    mRecalcStatAllStrips();
                    bOk = recordNr == ARecordNr && ( AAnalysisNr == 0 || analysisNr == AAnalysisNr );

                    sr.Close();

                    if(ArLoadedRecord != null)
                    {
                        ArLoadedRecord.mModifyInvertMask(_mInvChannelsMask);
                    }
                }

            }
            catch (Exception ex)
            {
                CProgram.sLogException("Error reading analysis file " + AFullName, ex);
            }
            return bOk;
        }

        public void mExtracMfAnalysis(string AName, string AValue, ref UInt32 ARecordNr, ref UInt16 AAnalysisNr)
        {
            if (AName == "Record")
            {
                UInt32.TryParse(AValue, out ARecordNr);
            }
            else if (AName == "Analysis")
            {
                UInt16.TryParse(AValue, out AAnalysisNr);
            }
            else if (AName == "invChannels")
            {
                UInt32.TryParse(AValue, out _mInvChannelsMask);
            }

        }

        bool mbParseFloat( string AString, ref float ArFloat )
        {
            return CProgram.sbParseFloat(AString, ref ArFloat);
            
//            return float.TryParse(AString, out ArFloat);
        }
        public void mExtracMfStrip( CMeasureStrip AMs, string AName, string AValue )
        {
            if (AMs != null)
            {
                if (AName == "active")
                {
                    int i = 0;

                    int.TryParse(AValue, out i);
                    AMs._mbActive = i != 0;
                }
                else if (AName == "channel")
                {
                    UInt16.TryParse(AValue, out AMs._mChannel);
                }
                else if (AName == "startSec")
                {
                    mbParseFloat( AValue, ref AMs._mStartTimeSec);
                }
                else if (AName == "durationSec")
                {
                    mbParseFloat(AValue, ref AMs._mDurationSec);
                }
                else if (AName == "startAmp")
                {
                    mbParseFloat(AValue, ref AMs._mStartA);
                }
                else if (AName == "endAmp")
                {
                    mbParseFloat(AValue, ref AMs._mEndA);
                }
                else if (AName == "image")
                {
                    AMs._mStripImageName = AValue == null ? null : AValue.Trim();
                }
            }
        }

        public void mExtracMfPoint(CMeasurePoint AMp, string AName, string AValue)
        {
            if (AMp != null)
            {
                if (AName == "active")
                {
                    int i = 0;

                    int.TryParse(AValue, out i);
                    AMp._mbActive = i != 0;
                }
                else if (AName == "dualPoint")
                {
                    int i = 0;

                    int.TryParse(AValue, out i);
                    AMp._mbDualPoint = i != 0;
                }
                else if (AName == "tag")
                {
                    if( AMp._mbDualPoint )
                    {
                        AMp._mTagID = (UInt16)CMeasurePoint.sGetMeasure2TagFromString(AValue);
                    }
                    else
                    {
                        AMp._mTagID = (UInt16)CMeasurePoint.sGetMeasure1TagFromString(AValue);
                    }
                }
                else if (AName == "value")
                {
                    float f = 0.0F;
                    mbParseFloat(AValue, ref f);
                }
                else if (AName == "channel")
                {
                    UInt16.TryParse(AValue, out AMp._mChannel);
                }
                else if (AName == "point1T")
                {
                    mbParseFloat(AValue, ref AMp._mPoint1T);
                }
                else if (AName == "point1A")
                {
                    mbParseFloat(AValue, ref AMp._mPoint1A);
                }
                else if (AName == "point2T")
                {
                    mbParseFloat(AValue, ref AMp._mPoint2T);
                }
                else if (AName == "point2A")
                {
                    mbParseFloat(AValue, ref AMp._mPoint2A);
                }
            }
        }
    }
    //=======================================================
    // CRecAnnalysis

    public enum DAnalysisState
    {
        Unknown = 0,
        New,
        Measured,
        EventReported,
        StudyReported,
        NrStates
    }


    public enum DRecAnalysisVars
    {
        // primary keys
        Recording_IX,
        SeqInRecording,
        // variables
        Study_IX,
        AnalysisClassSet,
        AnalysisRhythmSet,
        AnalysisRemark,
        SymptomsCodeSet,
        SymptomsRemark,
        ActivitiesCodeSet,
        ActivitiesRemark,
        State,
        Technician_IX,
        MeasuredUTC,                            // use time zone from record
        Physician_IX,
        ReportedUTC,
        ReportNr,
        StudyReportNr,
        StudyReportUTC,
        StudyReport_IX, // 18
        //---- 20170120
        MeasuredHrN,    // 19
        MeasuredHrMin,
        MeasuredHrMean,
        MeasuredHrMax,
        IncludeReportTable,
        IncludeReportStrip, // 14
        ManualEvent,
        NrSelectedStrips,
        NrTotalStrips,  //27
        NrSqlVars       // keep as last
    };

    public class CRecAnalysis:CSqlDataTableRow
    {
        public UInt32 _mRecording_IX;
        public UInt32 _mStudy_IX;
        public UInt16 _mSeqInRecording;              // analyze strip number
        public CStringSet _mAnalysisClassSet;        // Findings
        public CStringSet _mAnalysisRhythmSet;        // Findings
        public string _mAnalysisRemark;
        public string _mSymptomsRemark;
        public CStringSet _mSymptomsCodeSet;
        public string _mActivitiesRemark;
        public CStringSet _mActivitiesCodeSet;
        public UInt16 _mState;
        public UInt32 _mTechnician_IX;
        public DateTime _mMeasuredUTC;
        public UInt32 _mPhysician_IX;
        public DateTime _mReportedUTC;              // used for strip start time
        public UInt16 _mReportNr;
        public UInt16 _mStudyReportNr;
        public DateTime _mStudyReportUTC;
        public UInt32 _mStudyReport_IX;

        //---- 20170120
        public UInt16 _mMeasuredHrN;
        public float _mMeasuredHrMin;
        public float _mMeasuredHrMean;
        public float _mMeasuredHrMax;
        public UInt32 _mIncludeReportTable;
        public UInt32 _mIncludeReportStrip;
        public bool _mbManualEvent;  // 0 = automatic, 1 = manual
        public UInt16 _mNrSelectedStrips;
        public UInt16 _mNrTotalStrips;

        // temporary not stored in dBase
        public CRecordMit _mrRecordMit;           // temporary to store loaded record (used by MCT)


        public CRecAnalysis(CSqlDBaseConnection ASqlConnection) : base(ASqlConnection, "d_recanalysis")
        {
            try
            {
                UInt64 maskPrimary = CSqlDataTableRow.sGetMaskRange((UInt16)DRecAnalysisVars.Recording_IX, (UInt16)DRecAnalysisVars.SeqInRecording);
                UInt64 maskValid = CSqlDataTableRow.sGetMaskRange((UInt16)0, (UInt16)DRecAnalysisVars.NrSqlVars - 1);// mGetValidMask(false);

                mInitTableMasks(maskPrimary, maskValid);  // set primairyInsertFlags
                                                          // mInitTable("d_StudyInfo", (UInt16)DStudyInfoVars.NrSqlVars, 0);
                _mAnalysisClassSet = new CStringSet();
                _mAnalysisRhythmSet = new CStringSet();
                _mSymptomsCodeSet = new CStringSet();
                _mActivitiesCodeSet = new CStringSet();

                mClear();
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Init ", ex);
            }
        }
        public override CSqlDataTableRow mCreateNewRow()
        {
            return new CRecAnalysis(mGetSqlConnection());
        }
        public override bool mbCopyTo(CSqlDataTableRow ATo) // derived  public override void mbCopyTo() must call base.mbCopyTo(ATo)
        {
            bool bOk = false;

            if (ATo != null)
            {
                CRecAnalysis to = ATo as CRecAnalysis;

                if (to != null)
                {
                    bOk = base.mbCopyTo(ATo);
                    
                    to._mRecording_IX = _mRecording_IX;
                    to._mStudy_IX = _mStudy_IX;
                    to._mSeqInRecording = _mSeqInRecording;
                    to._mAnalysisClassSet.mSetSet(_mAnalysisClassSet);        // Findings
                    to._mAnalysisRhythmSet.mSetSet(_mAnalysisRhythmSet);        // Findings
                    to._mAnalysisRemark = _mAnalysisRemark;
                    to._mSymptomsCodeSet.mSetSet(_mSymptomsCodeSet );
                    to._mSymptomsRemark = _mSymptomsRemark;
                    to._mActivitiesRemark = _mActivitiesRemark;
                    to._mActivitiesCodeSet.mSetSet( _mActivitiesCodeSet );
                    to._mState = _mState;
                    to._mTechnician_IX = _mTechnician_IX;
                    to._mMeasuredUTC = _mMeasuredUTC;
                    to._mPhysician_IX = _mPhysician_IX;
                    to._mReportedUTC = _mReportedUTC;
                    to._mReportNr = _mReportNr;
                    to._mStudyReportNr = _mStudyReportNr;
                    to._mStudyReportUTC = _mStudyReportUTC;
                    to._mStudyReport_IX = _mStudyReport_IX;
                    //---- 20170120
                    to._mMeasuredHrN = _mMeasuredHrN;
                    to._mMeasuredHrMin = _mMeasuredHrMin;
                    to._mMeasuredHrMean = _mMeasuredHrMean;
                    to._mMeasuredHrMax = _mMeasuredHrMax;
                    to._mIncludeReportTable = _mIncludeReportTable;
                    to._mIncludeReportStrip = _mIncludeReportStrip;
                    to._mbManualEvent = _mbManualEvent;
                    to._mNrSelectedStrips = _mNrSelectedStrips;
                    to._mNrTotalStrips = _mNrTotalStrips;
                    to._mrRecordMit = _mrRecordMit;
                }
            }
            return bOk;
        }
        public override void mClear()
        {
            base.mClear();
            _mRecording_IX = 0;
            _mStudy_IX = 0;
            _mSeqInRecording = 0;
            _mAnalysisClassSet.mClear();        // Findings
            _mAnalysisRhythmSet.mClear();        // Findings
            _mAnalysisRemark = "";
            _mSymptomsCodeSet.mClear();
            _mSymptomsRemark = "";
            _mActivitiesRemark = "";
            _mActivitiesCodeSet.mClear();
            _mState = (UInt16)DAnalysisState.Unknown;
            _mTechnician_IX = 0;
            _mMeasuredUTC = DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Utc);
            _mPhysician_IX = 0;
            _mReportedUTC = _mMeasuredUTC;
            _mReportNr = 0;
            _mStudyReportNr = 0;
            _mStudyReportUTC = _mMeasuredUTC;
            _mStudyReport_IX = 0;
            //---- 20170120
            _mMeasuredHrN = 0;
            _mMeasuredHrMin = 0.0F;
            _mMeasuredHrMean = 0.0F;
            _mMeasuredHrMax = 0.0F;
            _mIncludeReportTable = Int32.MaxValue;
            _mIncludeReportStrip = Int32.MaxValue;
            _mbManualEvent = false;  // 0 = automatic, 1 = manual
            _mNrSelectedStrips = 0;
            _mNrTotalStrips = 0;

            _mrRecordMit = null;
        }

        public override DSqlDataType mVarGetSet(DSqlDataCmd ACmd, UInt16 AVarIndex, ref string AVarSqlName, ref string AStringValue, out bool ArbValid)
        {
            switch ((DRecAnalysisVars)AVarIndex)
            {
                case DRecAnalysisVars.Recording_IX:
                    return mSqlGetSetUInt32(ref _mRecording_IX, "Recording_IX", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DRecAnalysisVars.SeqInRecording:
                    return mSqlGetSetUInt16(ref _mSeqInRecording, "SeqInRecording", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DRecAnalysisVars.Study_IX:
                    return mSqlGetSetUInt32(ref _mStudy_IX, "Study_IX", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DRecAnalysisVars.AnalysisClassSet:
                    return mSqlGetSetStringSet(ref _mAnalysisClassSet, "AnalysisClassSet", ACmd, ref AVarSqlName, ref AStringValue, 64, out ArbValid);
                case DRecAnalysisVars.AnalysisRhythmSet:
                    return mSqlGetSetStringSet(ref _mAnalysisRhythmSet, "AnalysisRhythmSet", ACmd, ref AVarSqlName, ref AStringValue, 64, out ArbValid);
                case DRecAnalysisVars.AnalysisRemark:
                    return mSqlGetSetString(ref _mAnalysisRemark, "AnalysisRemark", ACmd, ref AVarSqlName, ref AStringValue, 250, out ArbValid);
                case DRecAnalysisVars.SymptomsCodeSet:
                    return mSqlGetSetStringSet(ref _mSymptomsCodeSet, "SymptomsCodeSet", ACmd, ref AVarSqlName, ref AStringValue, 64, out ArbValid);
                case DRecAnalysisVars.SymptomsRemark:
                    return mSqlGetSetString(ref _mSymptomsRemark, "SymptomsRemark", ACmd, ref AVarSqlName, ref AStringValue, 250, out ArbValid);
                case DRecAnalysisVars.ActivitiesCodeSet:
                    return mSqlGetSetStringSet(ref _mActivitiesCodeSet, "ActivitiesCodeSet", ACmd, ref AVarSqlName, ref AStringValue, 64, out ArbValid);
                case DRecAnalysisVars.ActivitiesRemark:
                    return mSqlGetSetString(ref _mActivitiesRemark, "ActivitiesRemark", ACmd, ref AVarSqlName, ref AStringValue, 250, out ArbValid);
                case DRecAnalysisVars.State:
                    return mSqlGetSetUInt16(ref _mState, "State", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DRecAnalysisVars.Technician_IX:
                    return mSqlGetSetUInt32(ref _mTechnician_IX, "Technician_IX", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DRecAnalysisVars.MeasuredUTC:
                    return mSqlGetSetUTC(ref _mMeasuredUTC, "MeasuredUTC", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DRecAnalysisVars.Physician_IX:
                    return mSqlGetSetUInt32(ref _mPhysician_IX, "Physician_IX", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DRecAnalysisVars.ReportedUTC:
                    return mSqlGetSetUTC(ref _mReportedUTC, "ReportedUTC", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DRecAnalysisVars.ReportNr:
                    return mSqlGetSetUInt16(ref _mReportNr, "ReportNr", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DRecAnalysisVars.StudyReportNr:
                    return mSqlGetSetUInt16(ref _mStudyReportNr, "StudyReportNr", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DRecAnalysisVars.StudyReportUTC:
                    return mSqlGetSetUTC(ref _mStudyReportUTC, "StudyReportUTC", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DRecAnalysisVars.StudyReport_IX:
                    return mSqlGetSetUInt32(ref _mStudyReport_IX, "StudyReport_IX", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                //20170120
                case DRecAnalysisVars.MeasuredHrN:
                    return mSqlGetSetUInt16(ref _mMeasuredHrN, "MeasuredHrN", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DRecAnalysisVars.MeasuredHrMin:
                    return mSqlGetSetFloat(ref _mMeasuredHrMin, "MeasuredHrMin", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DRecAnalysisVars.MeasuredHrMean:
                    return mSqlGetSetFloat(ref _mMeasuredHrMean, "MeasuredHrMean", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DRecAnalysisVars.MeasuredHrMax:
                    return mSqlGetSetFloat(ref _mMeasuredHrMax, "MeasuredHrMax", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DRecAnalysisVars.IncludeReportTable:
                    return mSqlGetSetUInt32(ref _mIncludeReportTable, "IncludeReportTable", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DRecAnalysisVars.IncludeReportStrip:
                    return mSqlGetSetUInt32(ref _mIncludeReportStrip, "IncludeReportStrip", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DRecAnalysisVars.ManualEvent:
                    return mSqlGetSetBool(ref _mbManualEvent, "ManualEvent", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DRecAnalysisVars.NrSelectedStrips:
                    return mSqlGetSetUInt16(ref _mNrSelectedStrips, "NrSelectedStrips", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DRecAnalysisVars.NrTotalStrips:
                    return mSqlGetSetUInt16(ref _mNrTotalStrips, "NrTotalStrips", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
             }
            return base.mVarGetSet(ACmd, AVarIndex, ref AVarSqlName, ref AStringValue, out ArbValid);
        }

        public string mGetAnalysisClassString()
        {
            return _mAnalysisClassSet.mGetSet();
        }
        public string mGetAnalysisFullClasssificationString()
        {
            string s = "";

            if(_mAnalysisClassSet.mbIsNotEmpty())
            {
                //if( CDvtmsData._s)
                s = _mAnalysisClassSet.mGetSet();
            }
            if( _mAnalysisRemark != null && _mAnalysisRemark.Length > 0)

            {
                s += "\n" + _mAnalysisRemark;
            }
            return s;
        }
    }
}
