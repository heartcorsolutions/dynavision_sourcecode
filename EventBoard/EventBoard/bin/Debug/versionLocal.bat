@echo off
echo Copy Eventboard Version to Local: %1
set toPath=\\192.168.0.36\SoftwareReleases\suEventSystem\suEventboard\
dir %toPath%
if "%1"=="" GOTO noParam
echo Creating Local sub folder %1
@pause

mkDir %toPath%\%1
copy version.json %toPath%\%1
copy eventboard.exe %toPath%\%1
mkDir %toPath%\%1\Content
mkDir "%toPath%\%1\Content\0. Release notes"
mkDir "%toPath%\%1\Content\9. Extra"
mkDir "%toPath%\%1\Content\9. Extra\Eventboard_Config"

copy c:\Projects\EventSystem\EventboardReader\EventBoard\ChangeHistoryEventboard.rtf "%toPath%\%1\Content\0. Release notes\"
copy c:\Projects\EventSystem\EventboardReader\EventBoard\eventboard_config.rtf "%toPath%\%1\Content\9. Extra\Eventboard_Config\"
copy c:\Projects\EventSystem\EventboardReader\EventBoard\EventBoard\App.config "%toPath%\%1\Content\9. Extra\Eventboard_Config\Eventboard.exe.All_config" 

dir %toPath%\%1
echo Done copy Eventboard to Local version %1
pause
rem start %toPath%\%1\
set toFile="%toPath%\%1\Content\0. Release notes\ChangeHistoryEventboard.rtf"
echo toFile=%toFile%
dir %toFile%
%toFile%
echo Done show Eventboard Local version %1
pause
exit
:noParam
echo no parameter given
pause
