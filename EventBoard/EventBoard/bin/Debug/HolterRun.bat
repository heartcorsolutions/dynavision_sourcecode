@ren HolterRun.bat %1 %2 %3 %4 %5 %6 %7 %8 %9 
@rem HolterProgParams= %p %f %j %d %t %x %c %s
@rem optional %n %e (max 9 parameters to batch)
@echo 1:%1
@echo 2:%2
@echo 3:%3
@echo 4:%4
@echo 5:%5
@echo 6:%6
@echo 7:%7
@echo 8:%8
@echo 9:%9
@dir %1\%2 
@pause
@d:
@cd "d:\ProgramFiles\DVTMSX\DVX holter\" 
@dir *.exe
@set parmsIn=%3
@set parmsOut=%parmsIn:~1,-1%
@echo params=%parmsOut%
DVXholter.exe --params=%parmsOut%
pause
