@echo off
echo Copy Eventboard Version to Upload: %1

echo use total commander to copy from development to release!
pause
exit

set toPath="q:\DEVELOPMENT\EVENTBOARD\SOFTWARE RELEASES" 
dir %toPath%
if "%1"=="" GOTO noParam
echo Creating Eventboard Upload sub folder %1
@pause

mkDir %toPath%\%1
copy version.json %toPath%\%1
copy eventboard.exe %toPath%\%1
mkDir %toPath%\%1\Content
mkDir "%toPath%\%1\Content\0. Release notes"
copy c:\Projects\EventSystem\EventboardReader\EventBoard\ChangeHistoryEventboard.rtf "%toPath%\%1\Content\0. Release notes\"

dir %toPath%\%1
echo Done copy Eventboard to Upload version %1
pause
start %toPath%\%1\
set toFile="%toPath%\%1\Content\0. Release notes\ChangeHistoryEventboard.rtf"
echo toFile=%toFile%
dir %toFile%
%toFile%

exit
:noParam
echo no parameter given
pause
