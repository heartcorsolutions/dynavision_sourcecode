$SR	Sinus Rhythm
SNR	Normal Sinus Rhythm
SBl	Sinoatrial Block
SP	Sinus Pause
SA	Sinus Arrhythmia
SB	Sinus Bradycardia
ST	Sinus Tachycardia

$AR	Atrial Rhythm
AFib	Atrial Fibrillation (afib)
AFlut	Atrial Flutter
AMT	Multifocal Atrial Tachycardia
APC	Premature Atrial Complex
AST	Supraventricular Tachycardia
AWP	Wandering Atrial Pacemaker
AWPWS	Wolff-Parkinson-White Syndrome

$JR	Junctional Rhythm
JAR	Accelerated Junctional Rhythm
JER	Junctional Escape Rhythm
JT	Junctional Tachycardia
JPC	Premature Junctional Complex

$VR	Ventricular Rhythm
VAIR	Accelerated Idioventricular Rhythm
VA	Asystole
VIR	Idioventricular Rhythm
VPC	Premature Ventricular Complex
VPCB	Premature Ventricular Complex - Bigeminy
VPCT	Premature Ventricular Complex - Trigeminy
VPCQ	Premature Ventricular Complex - Quadrigeminy
VF	Ventricular Fibrillation
VT	Ventricular Tachycardia
VTM	Ventricular Tachycardia Monomorphic
VTP	Ventricular Tachycardia Polymorphic
VTdP	Torsade de Pointes

$AVR AtrioVentricular Rhythm
BBB	Bundle Branch Block
FDHB	First Degree Heart Block
SDHB1	Second Degree Heart Block Type I
SDHB2	Second Degree Heart Block Type II
TDHB	Third Degree Heart Block

$PR	Pacemaker Rhythm
PSC	Normal Single Chamber Pacemaker
PDC	Normal Dual Chamber Pacemaker
PFtC	Failure to Capture
PFtP	Failure to Pace
PFtS	Failure to Sense
