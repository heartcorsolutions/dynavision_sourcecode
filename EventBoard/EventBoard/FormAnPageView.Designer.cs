﻿namespace EventBoard
{
    partial class FormAnPageView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panelMainHR = new System.Windows.Forms.Panel();
            this.pictureBoxHR = new System.Windows.Forms.PictureBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.labelLowHr = new System.Windows.Forms.Label();
            this.labelLowBurden = new System.Windows.Forms.Label();
            this.labelHighHR = new System.Windows.Forms.Label();
            this.labelHighBurden = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.labelHrMin = new System.Windows.Forms.Label();
            this.labelHrMax = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.labelUnitFeed = new System.Windows.Forms.Label();
            this.labelUnitAmp = new System.Windows.Forms.Label();
            this.labelShowChannels = new System.Windows.Forms.Label();
            this.labelFullStripInfo = new System.Windows.Forms.Label();
            this.labelEndTime = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.dgScanPage = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewImageColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel5 = new System.Windows.Forms.Panel();
            this.labelMeanBurden = new System.Windows.Forms.Label();
            this.labelMeanHR = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panelMainHR.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxHR)).BeginInit();
            this.panel6.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgScanPage)).BeginInit();
            this.SuspendLayout();
            // 
            // panelMainHR
            // 
            this.panelMainHR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelMainHR.Controls.Add(this.pictureBoxHR);
            this.panelMainHR.Controls.Add(this.panel6);
            this.panelMainHR.Controls.Add(this.panel1);
            this.panelMainHR.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelMainHR.Location = new System.Drawing.Point(0, 0);
            this.panelMainHR.Name = "panelMainHR";
            this.panelMainHR.Size = new System.Drawing.Size(1447, 89);
            this.panelMainHR.TabIndex = 0;
            // 
            // pictureBoxHR
            // 
            this.pictureBoxHR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxHR.Location = new System.Drawing.Point(57, 0);
            this.pictureBoxHR.Name = "pictureBoxHR";
            this.pictureBoxHR.Size = new System.Drawing.Size(1331, 87);
            this.pictureBoxHR.TabIndex = 2;
            this.pictureBoxHR.TabStop = false;
            this.pictureBoxHR.DoubleClick += new System.EventHandler(this.pictureBoxHR_DoubleClick);
            this.pictureBoxHR.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxHR_MouseUp);
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.labelMeanHR);
            this.panel6.Controls.Add(this.labelMeanBurden);
            this.panel6.Controls.Add(this.panel7);
            this.panel6.Controls.Add(this.labelLowHr);
            this.panel6.Controls.Add(this.labelLowBurden);
            this.panel6.Controls.Add(this.labelHighHR);
            this.panel6.Controls.Add(this.labelHighBurden);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel6.Location = new System.Drawing.Point(1388, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(57, 87);
            this.panel6.TabIndex = 1;
            // 
            // labelLowHr
            // 
            this.labelLowHr.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelLowHr.Location = new System.Drawing.Point(0, 61);
            this.labelLowHr.Name = "labelLowHr";
            this.labelLowHr.Size = new System.Drawing.Size(57, 13);
            this.labelLowHr.TabIndex = 1;
            this.labelLowHr.Text = "LowHr";
            this.labelLowHr.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelLowBurden
            // 
            this.labelLowBurden.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelLowBurden.Location = new System.Drawing.Point(0, 74);
            this.labelLowBurden.Name = "labelLowBurden";
            this.labelLowBurden.Size = new System.Drawing.Size(57, 13);
            this.labelLowBurden.TabIndex = 5;
            this.labelLowBurden.Text = "LowHr%";
            this.labelLowBurden.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelHighHR
            // 
            this.labelHighHR.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelHighHR.Location = new System.Drawing.Point(0, 13);
            this.labelHighHR.Name = "labelHighHR";
            this.labelHighHR.Size = new System.Drawing.Size(57, 13);
            this.labelHighHR.TabIndex = 3;
            this.labelHighHR.Text = "highHR";
            this.labelHighHR.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelHighBurden
            // 
            this.labelHighBurden.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelHighBurden.Location = new System.Drawing.Point(0, 0);
            this.labelHighBurden.Name = "labelHighBurden";
            this.labelHighBurden.Size = new System.Drawing.Size(57, 13);
            this.labelHighBurden.TabIndex = 0;
            this.labelHighBurden.Text = "highHR%";
            this.labelHighBurden.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.labelHrMin);
            this.panel1.Controls.Add(this.labelHrMax);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(57, 87);
            this.panel1.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Location = new System.Drawing.Point(0, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "(BPM)";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "HR";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 13);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(57, 13);
            this.panel2.TabIndex = 2;
            // 
            // labelHrMin
            // 
            this.labelHrMin.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelHrMin.Location = new System.Drawing.Point(0, 74);
            this.labelHrMin.Name = "labelHrMin";
            this.labelHrMin.Size = new System.Drawing.Size(57, 13);
            this.labelHrMin.TabIndex = 1;
            this.labelHrMin.Text = "HRmin";
            this.labelHrMin.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelHrMax
            // 
            this.labelHrMax.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelHrMax.Location = new System.Drawing.Point(0, 0);
            this.labelHrMax.Name = "labelHrMax";
            this.labelHrMax.Size = new System.Drawing.Size(57, 13);
            this.labelHrMax.TabIndex = 0;
            this.labelHrMax.Text = "HRmax";
            this.labelHrMax.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.labelUnitFeed);
            this.panel3.Controls.Add(this.labelUnitAmp);
            this.panel3.Controls.Add(this.labelShowChannels);
            this.panel3.Controls.Add(this.labelFullStripInfo);
            this.panel3.Controls.Add(this.labelEndTime);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 93);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1447, 16);
            this.panel3.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Right;
            this.label3.Location = new System.Drawing.Point(737, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(197, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Right click moves Zoom window to strip.";
            // 
            // labelUnitFeed
            // 
            this.labelUnitFeed.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelUnitFeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUnitFeed.Location = new System.Drawing.Point(934, 0);
            this.labelUnitFeed.Name = "labelUnitFeed";
            this.labelUnitFeed.Size = new System.Drawing.Size(167, 16);
            this.labelUnitFeed.TabIndex = 11;
            this.labelUnitFeed.Text = "5 mm/sec (1.0 Sec)";
            this.labelUnitFeed.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelUnitAmp
            // 
            this.labelUnitAmp.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelUnitAmp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUnitAmp.Location = new System.Drawing.Point(1101, 0);
            this.labelUnitAmp.Name = "labelUnitAmp";
            this.labelUnitAmp.Size = new System.Drawing.Size(123, 16);
            this.labelUnitAmp.TabIndex = 10;
            this.labelUnitAmp.Text = "10 mm/mV (0.50 mV)";
            this.labelUnitAmp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelShowChannels
            // 
            this.labelShowChannels.AutoSize = true;
            this.labelShowChannels.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelShowChannels.Location = new System.Drawing.Point(35, 0);
            this.labelShowChannels.Name = "labelShowChannels";
            this.labelShowChannels.Size = new System.Drawing.Size(35, 13);
            this.labelShowChannels.TabIndex = 1;
            this.labelShowChannels.Text = "label3";
            this.labelShowChannels.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelFullStripInfo
            // 
            this.labelFullStripInfo.AutoSize = true;
            this.labelFullStripInfo.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelFullStripInfo.Location = new System.Drawing.Point(0, 0);
            this.labelFullStripInfo.Name = "labelFullStripInfo";
            this.labelFullStripInfo.Size = new System.Drawing.Size(35, 13);
            this.labelFullStripInfo.TabIndex = 0;
            this.labelFullStripInfo.Text = "label3";
            this.labelFullStripInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelEndTime
            // 
            this.labelEndTime.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelEndTime.Location = new System.Drawing.Point(1224, 0);
            this.labelEndTime.Name = "labelEndTime";
            this.labelEndTime.Size = new System.Drawing.Size(223, 16);
            this.labelEndTime.TabIndex = 12;
            this.labelEndTime.Text = "ghdhfasghfghsakfgsa HH:mm:ss AM";
            this.labelEndTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.dgScanPage);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 109);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1447, 969);
            this.panel4.TabIndex = 2;
            // 
            // dgScanPage
            // 
            this.dgScanPage.AllowUserToAddRows = false;
            this.dgScanPage.AllowUserToDeleteRows = false;
            this.dgScanPage.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgScanPage.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4});
            this.dgScanPage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgScanPage.GridColor = System.Drawing.SystemColors.Window;
            this.dgScanPage.Location = new System.Drawing.Point(0, 0);
            this.dgScanPage.MultiSelect = false;
            this.dgScanPage.Name = "dgScanPage";
            this.dgScanPage.ReadOnly = true;
            this.dgScanPage.RowHeadersVisible = false;
            this.dgScanPage.RowTemplate.Height = 100;
            this.dgScanPage.RowTemplate.ReadOnly = true;
            this.dgScanPage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgScanPage.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgScanPage.ShowEditingIcon = false;
            this.dgScanPage.Size = new System.Drawing.Size(1447, 969);
            this.dgScanPage.TabIndex = 0;
            this.dgScanPage.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgScanPage_CellContentClick);
            this.dgScanPage.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgScanPage_CellContentDoubleClick);
            this.dgScanPage.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgScanPage_CellDoubleClick);
            this.dgScanPage.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dgScanPage_MouseUp);
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Column1.DefaultCellStyle = dataGridViewCellStyle2;
            this.Column1.HeaderText = "Info";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 50;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column2.HeaderText = "Strip";
            this.Column2.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Channel";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Visible = false;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "StartSec";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Visible = false;
            // 
            // panel5
            // 
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 89);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1447, 4);
            this.panel5.TabIndex = 3;
            // 
            // labelMeanBurden
            // 
            this.labelMeanBurden.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelMeanBurden.Location = new System.Drawing.Point(0, 30);
            this.labelMeanBurden.Name = "labelMeanBurden";
            this.labelMeanBurden.Size = new System.Drawing.Size(57, 13);
            this.labelMeanBurden.TabIndex = 6;
            this.labelMeanBurden.Text = "meanHR%";
            // 
            // labelMeanHR
            // 
            this.labelMeanHR.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelMeanHR.Location = new System.Drawing.Point(0, 43);
            this.labelMeanHR.Name = "labelMeanHR";
            this.labelMeanHR.Size = new System.Drawing.Size(57, 13);
            this.labelMeanHR.TabIndex = 7;
            this.labelMeanHR.Text = "meanHR";
            // 
            // panel7
            // 
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 26);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(57, 4);
            this.panel7.TabIndex = 8;
            // 
            // FormAnPageView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1447, 1078);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panelMainHR);
            this.Name = "FormAnPageView";
            this.Text = "FormAnPageView";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormAnPageView_FormClosing);
            this.panelMainHR.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxHR)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgScanPage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelMainHR;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label labelHrMin;
        private System.Windows.Forms.Label labelHrMax;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DataGridView dgScanPage;
        private System.Windows.Forms.Label labelFullStripInfo;
        private System.Windows.Forms.Label labelShowChannels;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewImageColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelUnitFeed;
        private System.Windows.Forms.Label labelUnitAmp;
        private System.Windows.Forms.PictureBox pictureBoxHR;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label labelLowHr;
        private System.Windows.Forms.Label labelLowBurden;
        private System.Windows.Forms.Label labelHighHR;
        private System.Windows.Forms.Label labelHighBurden;
        private System.Windows.Forms.Label labelEndTime;
        private System.Windows.Forms.Label labelMeanHR;
        private System.Windows.Forms.Label labelMeanBurden;
        private System.Windows.Forms.Panel panel7;
    }
}