﻿using DynaVisionGraph.Filter;
using Event_Base;
using EventBoard.EntryForms;
using EventBoard.Event_Base;
using EventboardEntryForms;
using EventBoardEntryForms;
using Program_Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using TechmedicEcgViewer.Filters;

namespace EventBoard
{
    enum DTriageGridField
    {
        PSA, Patient, Event, Recorded, Remark, EventStrip, Triage, Index, State, FilePath, FileName,
        NrSignals, Study, Active, ModelNr, RecDir, NrAnalysis, ReadUTC, NrGridFields
    }

    enum DTriageViewMode
    {
        Triage, Analyze, Report, Seperator, ListAll, ListReceived, ListExclude, ListNoise, ListLeadOff,
        ListBaseLine, ListTriaged, ListAnalyzed, ListReported, ListQCd,
        NrViewModes
    }

    public enum DTriageTimerMode
    {
        New,
        Stop,
        Halt,
        Print,
        DialogSecond,
        DialogOpen,
        Show,
        Run,
    }
    // DTriageTimerMode prevMode = FormEventBoard.sSetTriageTimerEntering(DTriageTimerMode.Print, "Print ");
    // FormEventBoard.sSetTriageTimerLeaving(prevMode, "Print ");

    public partial class FormEventBoard : Form
    {
        const Int32 _cLastActivityWriteSec = 900;
        //beta version in program.cs + version (major.minor) in AssemblyInfo.cs 
        // Launch version.json defaults:
        public string versionLabel = "Save part ECG Fix Device Model dB2019March25";
        public string versionHeader = "Save part ECG and Fix Device Model !req. dB2019March25!";
        public string[] versionText = { "This version requires data base and ireader update dB2019March25!",
            "Save view part ECG for import as new event",
            "Model selected in device stored in Record",
         };

        private static FormEventBoard _sEventBoardForm = null;

        //TODO: Refactor this
        public static bool InvertChannel;

        private static DTriageTimerMode _sTriageTimermode = DTriageTimerMode.New;
        private static bool _sbTriageTimerOn = false;
        private static bool _sbTriageDoMemInfo = false;
        private static bool _sbTriageDoTest = false;
        private static bool _sbTriageDoImageCash = false;
        private static bool _sbTriageDoUpdate = false;
        private static string _sTriageLastCall = null;
        private static bool _sbTriageLogCall = true;
        private static bool _sbTriageLogOld = false;

        public String _mRecordingDir;
        private String _mDVXStudyDir = "";

        private string _mHeaViewExe;
        private string _mScpViewExe;
        public CSqlDBaseConnection _mDBaseServer;

        private CEncryptedString _mSqlServer = null;
        private CEncryptedInt _mSqlPort = null;
        private DSqlSslMode _mSqlSslMode = DSqlSslMode.Default;
        private CEncryptedString _mSqlDbName = null;
        private CEncryptedString _mSqlUser = null;
        //        private CEncryptedString mSqlPassword = null;
        private CPasswordString _mSqlPassword = null;
        Font _mTriageFont = null;
        private bool _mbShowTriageFields = false;

        private DateTime _mLastTriageUpdateDT;
        private DateTime _mLastActivityWriteDT;
        private DateTime _mLastReaderScanCheckDT;
        private int _mTriageUpdSec = 0;
        private DateTime _mLastTriageActivityDT;
        private float _mTriageCountDays = 28;
        private bool _mbTriageLockAnRec = true;

        private string _mCurrentDeviceSNR;
        private string _mCurrentPatID;
        private UInt32 _mCurrentStudyIX;

        CImageCach _mImageCach = null;
        bool _mbShowTriageImage = true;

        Image _mTriageImgOther = null;
        Image _mTriageImgRead = null;
        Image _mTriageImgTriaged = null;
        Image _mTriageImgNoise = null;
        Image _mTriageImgExclude = null;
        Image _mTriageImgLeadOff = null;

        int _mWidthTriageStripNormal = 0;
        int _mWidthScreenNormal = 0;

        DTriageViewMode _mCurrentTriageView = DTriageViewMode.NrViewModes;   // ivalid => forces update

        private UInt32 _mTriageRowsLimit = 0;
        private UInt32 _mTriageDrawLimit = 0;

        private bool _mbAutoMove2First = false;
        private bool _mbAutoMove2Last = false;

        bool _mbUpdatingGrid = false;
        bool _mbUpdatingTriage = false;
        bool _mbUpdatingStrip = false;
        bool _mbUpdatingTriageTimer = false;

        private string _mQuickTachyText, _mQuickBradyText, _mQuickAFibText, _mQuickPauseText, _mQuickOtherText, _mQuickNormText, _mQuickAbnText;
        private string _mQuickPacText, _mQuickPvcText, _mQuickPaceText, _mQuickVtText, _mQuickVfText, _mQuickAFlText, _mQuickASysText;

        private Color _defaultFooterColor = Color.Gray;

        private UInt16 _mNrFullPreviewStrips = 2;

        private UInt16 _mCheckSystemMin = 15;
        //        private UInt16 _mCheckMemUsedMB = 800;
        //        private UInt16 _mCheckMemFreeMB = 200;
        private UInt16 _mCheckDiskLocalGB = 10;
        private UInt16 _mCheckDiskDataGB = 10;
        private string _mCheckServerTzUrl = "";
        private string _mCheckServerSironaUrl = "";

        DateTime _mTimerUpdDT = DateTime.Now;

        public static bool _sbTriageOnly = false;
        public static bool _sbForceAnonymize = false;
        public static bool _sbForceHideS0 = false;
        public static bool _sbStudyRecordsAllowUpdate= false;

        public static bool sbTzQuickFilter = false;

        //20190227 
        public static CBitSet64 _sStudyPermissionsActive = null;    // cripled to only contain site / location 
        public static CBitSet64 _sStudyPermissionsFixed = null;
        public static string _sStudyPermissionsText = "Permissions";
        public static bool _sbStudyPermissionsLocked = false;
        public static UInt32 _sSelectedTrial = 0;
        public static bool _sbSelectTrialLocked = false;
        public static UInt32 _sSelectedClient = 0;

        public static bool _sbQCdEnabled = false;
        public static bool _sbStudyUseExcluded = false;
        public static bool _sbMctUseExcluded = false;

        public static bool _sbPrintAskTechName = false;
        public static bool _sbPrintForceTechName = true;
        public static bool _sbPrintHideTechName = true;
        public Color _mIndexBackColor;

        Font _mTriageImageFont;
        Brush _mTriageImageBrush;
        StringFormat _mTriageImageFormat;
       

        private static DTriageTimerMode sSetTriageTimerMode(bool AbCheckLevel, DTriageTimerMode ANexTimerMode, string ANextLabel,
                               [System.Runtime.CompilerServices.CallerLineNumber]int _AutoLN = 0,
                    [System.Runtime.CompilerServices.CallerMemberName]string _AutoFN = "",
                    [System.Runtime.CompilerServices.CallerFilePath] string _AutoCN = "")
        {
            DTriageTimerMode oldMode = _sTriageTimermode;
            DTriageTimerMode newMode = ANexTimerMode;

            Color color = Color.Red;
            Color colorNormal = Color.LightSlateGray;
            string location = "[" + _AutoCN + "." + _AutoFN + ":" + _AutoLN + "]";
            string buttonLabel = null;
            string newCall = "req " + newMode.ToString() + ANextLabel + " " + location;

            if (AbCheckLevel && oldMode != DTriageTimerMode.New)
            {
                if (newMode > oldMode)
                {
                    newMode = oldMode;
                }
            }

            if (_sbTriageLogOld)
            {
                CProgram.sLogLine("TTM old: " + oldMode.ToString() + " " + _sTriageLastCall);
            }
            switch (ANexTimerMode)
            {
                case DTriageTimerMode.New:
                    color = Color.White;
                    buttonLabel = "-";
                    _sbTriageTimerOn = false;
                    _sbTriageDoMemInfo = false;
                    _sbTriageDoTest = false;
                    _sbTriageDoImageCash = false;
                    _sbTriageDoUpdate = false;
                    break;
                case DTriageTimerMode.Stop:
                    color = Color.White;
                    buttonLabel = "Stop";
                    _sbTriageTimerOn = false;
                    _sbTriageDoMemInfo = false;
                    _sbTriageDoTest = false;
                    _sbTriageDoImageCash = false;
                    _sbTriageDoUpdate = false;
                    break;

                case DTriageTimerMode.Halt:
                    color = Color.Yellow;
                    buttonLabel = "Halt";
                    _sbTriageTimerOn = false;
                    _sbTriageDoMemInfo = false;
                    _sbTriageDoTest = false;
                    _sbTriageDoImageCash = false;
                    _sbTriageDoUpdate = false;
                    break;

                case DTriageTimerMode.Print:
                    color = Color.Blue;
                    buttonLabel = "-";
                    _sbTriageTimerOn = false;
                    _sbTriageDoMemInfo = false;
                    _sbTriageDoTest = false;
                    _sbTriageDoImageCash = false;
                    _sbTriageDoUpdate = false;

                    break;

                case DTriageTimerMode.DialogSecond:
                    color = colorNormal;
                    buttonLabel = "Dlg2";
                    _sbTriageTimerOn = true;
                    _sbTriageDoMemInfo = true;
                    _sbTriageDoTest = false;
                    _sbTriageDoImageCash = false;
                    _sbTriageDoUpdate = false;

                    break;

                case DTriageTimerMode.DialogOpen:
                    color = colorNormal;
                    buttonLabel = "Dlg1";
                    _sbTriageTimerOn = true;
                    _sbTriageDoMemInfo = true;
                    _sbTriageDoTest = true;
                    _sbTriageDoImageCash = true;
                    _sbTriageDoUpdate = true;
                    break;

                case DTriageTimerMode.Show:
                    color = colorNormal;
                    buttonLabel = "Dlg1";
                    _sbTriageTimerOn = true;
                    _sbTriageDoMemInfo = true;
                    _sbTriageDoTest = true;
                    _sbTriageDoImageCash = true;
                    _sbTriageDoUpdate = true;
                    break;

                case DTriageTimerMode.Run:
                    color = colorNormal;
                    buttonLabel = "Run";
                    _sbTriageTimerOn = true;
                    _sbTriageDoMemInfo = true;
                    _sbTriageDoTest = true;
                    _sbTriageDoImageCash = true;
                    _sbTriageDoUpdate = true;
                    break;

            }

            _sTriageTimermode = newMode;
            _sTriageLastCall = "TriageInterupt= " + (_sbTriageTimerOn ? "ON " : "off ") + newMode.ToString() + ", " + newCall;

            if (_sEventBoardForm != null)
            {
                ToolStripLabel tsl = _sEventBoardForm.toolTriageUpdTime2;
                if (tsl != null)
                {
                    tsl.BackColor = color;
                    tsl.Text = buttonLabel;
                }
                Timer timer = _sEventBoardForm.timerTriage;
                if (timer != null)
                {
                    if (timer.Enabled != _sbTriageTimerOn)
                    {
                        timer.Enabled = _sbTriageTimerOn;
                    }
                }
            }
            if (_sbTriageLogOld || _sbTriageLogCall)
            {
                CProgram.sLogLine("TTM new: " + newMode.ToString() + " " + _sTriageLastCall);
            }
            return oldMode;
        }

        public static DTriageTimerMode sSetTriageTimerEntering(DTriageTimerMode ANexTimerMode, string ANextLabel,
                        [System.Runtime.CompilerServices.CallerLineNumber]int _AutoLN = 0,
             [System.Runtime.CompilerServices.CallerMemberName]string _AutoFN = "",
             [System.Runtime.CompilerServices.CallerFilePath] string _AutoCN = "")
        {
            return sSetTriageTimerMode(true, ANexTimerMode, "Entering " + ANextLabel, _AutoLN, _AutoFN, _AutoCN);
        }

        public static DTriageTimerMode sSetTriageTimerLeaving(DTriageTimerMode AOldTimerMode, string ANextLabel,
                         [System.Runtime.CompilerServices.CallerLineNumber]int _AutoLN = 0,
              [System.Runtime.CompilerServices.CallerMemberName]string _AutoFN = "",
              [System.Runtime.CompilerServices.CallerFilePath] string _AutoCN = "")
        {
            return sSetTriageTimerMode(false, AOldTimerMode, "Leaving " + ANextLabel, _AutoLN, _AutoFN, _AutoCN);
        }

        private void UserControl1_Paint(object sender, PaintEventArgs e)
        {
            Panel pn = sender as Panel;

            if (pn != null)
            {
                Rectangle rectangle = pn.RectangleToScreen(pn.ClientRectangle);

                ControlPaint.DrawBorder(e.Graphics, rectangle, Color.White, ButtonBorderStyle.Solid);
            }
        }

        public static FormEventBoard sGetMainForm()     // need this form in a couple of child forms, passing not practical because they are not always called from main form
        {
            return _sEventBoardForm;
        }

        public void mUpdateUserButtons()
        {
            string userInitials = CProgram.sGetProgUserInitials();
            bool bInitials = userInitials != null && userInitials.Length > 0;
            bool bActive = CProgram.sbCheckProgUserActive();

            if (userInitials != toolStripUserLabel.Text) toolStripUserLabel.Text = userInitials;
            if (toolStripUserLock.Enabled != bActive) toolStripUserLock.Enabled = bActive;
            if (toolStripUserOk.Enabled == !bActive) toolStripUserOk.Enabled = !bActive;
            //if (toolStripUserCancel.Enabled )!= bInitials) toolStripUserCancel.Enabled = !bInitials;

            toolStripUserLabel.ForeColor = bActive ? Color.DarkGreen : Color.Red;

            toolStripSelectUser.Text = userInitials;
            if (toolStripSelectUser.Enabled != !bInitials) toolStripSelectUser.Enabled = !bInitials;
            //            toolStripUserLabel.Font.Bold = bActive;
        }

        public FormEventBoard()
        {
            try
            {
                bool bProgrammer = CLicKeyDev.sbDeviceIsProgrammer();
                _sEventBoardForm = this;

                _sStudyPermissionsActive = new CBitSet64();
                _sStudyPermissionsFixed = new CBitSet64();

                InitializeComponent();

                _defaultFooterColor = panelStripInfo.BackColor;
                _sbTriageOnly = Properties.Settings.Default.TriageOnly;
                _sbForceAnonymize = Properties.Settings.Default.ForceAnonymize;
                _sbForceHideS0 = Properties.Settings.Default.ForceHideS0;

                if(_sbForceHideS0)
                {
                    checkBoxHideS0.Checked = true;
                    checkBoxHideS0.Enabled = false;
                }

                _sbStudyRecordsAllowUpdate = Properties.Settings.Default.StudyRecordsAllowUpdate;
                _sbQCdEnabled = ( Properties.Settings.Default.QCdEnabled && false == _sbTriageOnly )
                    || bProgrammer;

                _sbStudyUseExcluded = Properties.Settings.Default.StudyUseExcluded;
                _sbMctUseExcluded = Properties.Settings.Default.MctUseExcluded;

                buttonTriageFields.Visible = bProgrammer;
                _mLastActivityWriteDT = DateTime.Now.AddMinutes(1 -_cLastActivityWriteSec);
                _mLastTriageActivityDT = DateTime.Now;
                _mLastReaderScanCheckDT = _mLastActivityWriteDT.AddMinutes(-100);
                panelStatsUser.Visible = false; // bProgrammer;
                //labelUser.Text = CProgram.sGetProgUserInitials();

                CSironaStateSettings.sSetHolterReqLength(Properties.Settings.Default.SirMaxReqLengthMin, Properties.Settings.Default.SirHolterReqLengthMin);
                //               UInt16 userTimeout = 60;
                //                CProgram.sSetProgUserTimeoutMin(userTimeout);
                //panel11.Paint += new PaintEventHandler(UserControl1_Paint);
                CProgram.sSetProgramMainForm(this, "");

                UInt32 sqlGlobalRowLimit = Properties.Settings.Default.SqlGlobalRowLimit;

                string sqlDbName = Properties.Settings.Default.SqlDbName;

                DateTime startDT = DateTime.Now;
                string startTime = CProgram.sDateTimeToYMDHMS(startDT);
                string startZone = CProgram.sMakeUtcOffsetString(CProgram.sGetLocalTimeZoneOffsetMin());

                string cultureName = Properties.Settings.Default.CultureFormat;
                string dateFormat = Properties.Settings.Default.DateFormat;
                string timeFormat = Properties.Settings.Default.TimeFormat;
                bool bFloatWithDot = Properties.Settings.Default.FloatWithDot;
                bool bImperial = Properties.Settings.Default.ImperialUnits;

                CProgram.sSetShowDateTimeImperialFormat(cultureName, bImperial, dateFormat, timeFormat);
                CProgram.sSetUserDecimal(bFloatWithDot ? DFloatDecimal.Dot : DFloatDecimal.Comma);


                CDvtmsData._sbPrintPhysicianAskName = false;
                CDvtmsData._sPrintPhysicianNameMode = Properties.Settings.Default.PrintPhysicianNameMode; // 0 = off, 1 = label, 2 = text, 3 = img, 4 = both, 5 = line
                CDvtmsData._sPrintPhysicianSignMode = Properties.Settings.Default.PrintPhysicianSignMode; // 0 = off, 1 = label, 2 = text, 3 = img, 4 = both 5 = line
                CDvtmsData._sPrintPhysicianDateMode = Properties.Settings.Default.PrintPhysicianDateMode; // 0 = off, 1 = label, 2 = today, 3 = ask
                CDvtmsData._sPrintPhysicianLabel = Properties.Settings.Default.PrintPhysicianLabel;


                CDvtmsData._sbUserListEnfored = Properties.Settings.Default.UserListEnfored;
                CDvtmsData._sUserPinLength = Properties.Settings.Default.UserPinLength;

                CHolterFilesForm._sHolterProgRun = Properties.Settings.Default.HolterProgRun;
                CHolterFilesForm._sHolterProgParams = Properties.Settings.Default.HolterProgParms;
                CHolterFilesForm._sbHolterProgAddStudy = Properties.Settings.Default.HolterProgAddStudy;
                CHolterFilesForm._sHolterProgValidNrChannels = Properties.Settings.Default.HolterProgValidNrChannels;
                CHolterFilesForm._sHolterProgChannelFormat = Properties.Settings.Default.HolterProgChannelFormat;
                CHolterFilesForm._sHolterProgSnrFormat = Properties.Settings.Default.HolterProgSnrFormat;

                String t;
                t = "Program start at " + startDT.ToString() + startZone;
                CProgram.sLogLine(t);

                _mRecordingDir = Properties.Settings.Default.RecordingDir;
                _mDVXStudyDir = Properties.Settings.Default.DvxStudyFolder;


                string dbName = Properties.Settings.Default.SqlDbName;
                int dbPort = Properties.Settings.Default.SqlPort;
                string dbServer = Properties.Settings.Default.SqlServer;
                int dbSslMode = Properties.Settings.Default.SqlSslMode;
                string dbUser = Properties.Settings.Default.SqlUser;
                string dbPW = Properties.Settings.Default.SqlPassword;

                if (CProgram.sbIsEmptyConfig( dbName ) || CProgram.sbIsEmptyConfig(_mRecordingDir ) || CProgram.sbIsEmptyConfig( dbUser ) )
                {
                    if( bProgrammer)
                    {
                        bool bDemo = CProgram.sbAskYesNo("Select Center", "Center = NL0022Demo (cancel = US0001Soft)?");

                        if (bDemo)
                        {
                            // 32199E6E-$4C8DE91A-15BC5A91-6996FE00-0E54403C-36F3B124-50EF40F1-55A3F556-6B547F3C
                            uint nr = 22;
                            _mRecordingDir = "m:\\";
                            dbName = "NL" + nr.ToString("0000"); nr = 0x32199E6E; dbPW = nr.ToString("X8") + "-$"; nr = 192; dbServer = nr.ToString() + ".";
                            dbName += "Demo"; nr = 0x4C8DE91A; dbPW += nr.ToString("X8"); nr = 168; dbServer += nr.ToString() + ".";
                            nr = 0x15BC5A91; dbPW += nr.ToString("X8"); nr = 0; dbServer += nr.ToString() + ".";
                            dbPort = 3306; nr = 0x6996FE00; dbPW += nr.ToString("X8"); nr = 36; dbServer += nr.ToString();
                            dbUser = dbName + "01dbUser"; nr = 0x0E54403C; dbPW += nr.ToString("X8");
                            nr = 0x36F3B124; dbPW += nr.ToString("X8"); nr = 0x50EF40F1; dbPW += nr.ToString("X8");
                            nr = 0x55A3F556; dbPW += nr.ToString("X8"); nr = 0x6B547F3C; dbPW += nr.ToString("X8");

                            _mDVXStudyDir = "m:\\DvxDataSimon\\";


                            CProgram.sbSetProgramOrganisation(dbName, 1);
                        }
                        else
                        {
                            uint nr = 1;
                            _mRecordingDir = "d:\\Data\\";
                            dbName = "NL" + nr.ToString("0000"); nr = 0x321CAE66; dbPW = nr.ToString("X8") + "-$"; nr = 192; dbServer = nr.ToString() + ".";
                            dbName += "Soft"; nr = 0xD82D02B5; dbPW += nr.ToString("X8"); nr = 168; dbServer += nr.ToString() + ".";
                            _mRecordingDir += dbName; nr = 0xDE9A2E85; dbPW += nr.ToString("X8"); nr = 0; dbServer += nr.ToString() + ".";
                            dbPort = 3306; nr = 0x73B799EE; dbPW += nr.ToString("X8"); nr = 36; dbServer += nr.ToString();
                            dbUser = CProgram.sGetPcUserName(); nr = 0xB1C2FD57; dbPW += nr.ToString("X8");

                            _mDVXStudyDir = _mRecordingDir;
                        }
                        _mRecordingDir += "\\Recording\\";
                                 
                        dbSslMode = (int)DSqlSslMode.REQUIRED;
                        CProgram.sLogLine("Using Programmer defaults for recording and dBase " + dbName);

                        UInt32 holterMin = CSironaStateSettings.sGetHolterReqLengthSec() / 60;
                        if (holterMin > 10)
                        {
                            CSironaStateSettings.sSetHolterReqLength(15, 15);
                            CSirAutoCollect._sReqAgeMin = 10;     // Only request for files that are at least x minutes old
                            CSirAutoCollect._sScanTimeMin = 5;      // wait to do a scan after last scan
                            CSirAutoCollect._sMaxActionsInQueue = 1;
                        }
                        bool bUseBatch = false;
                        if( bUseBatch)
                        {
                            CHolterFilesForm._sHolterProgRun = "HolterRun.bat";
                            CHolterFilesForm._sHolterProgParams = "%p %f \"%j\" %d %t %x %c %s %i";
                            CHolterFilesForm._sbHolterProgAddStudy = true;
                            CHolterFilesForm._sHolterProgValidNrChannels = "3;12;5";
                            CHolterFilesForm._sHolterProgChannelFormat = "I, II, II-I";
                            CHolterFilesForm._sHolterProgSnrFormat = "DVX900000";
                        }
                        else
                        {
                            CHolterFilesForm._sHolterProgRun = "d:\\ProgramFiles\\DVTMSX\\DVX holter\\DVXholter.exe";
                            CHolterFilesForm._sHolterProgParams = "--params=%j";
                            CHolterFilesForm._sbHolterProgAddStudy = true;
                            CHolterFilesForm._sHolterProgValidNrChannels = "3;12;5";
                            CHolterFilesForm._sHolterProgChannelFormat = "I, II, II-I";
                            CHolterFilesForm._sHolterProgSnrFormat = "DVX900000";
                        }
                        CDvtmsData._sbPrintPhysicianAskName = false;
                        CDvtmsData._sPrintPhysicianNameMode = 4; // 0 = off, 1 = label, 2 = text, 3 = img, 4 = both, 5 = line
                        CDvtmsData._sPrintPhysicianSignMode = 4; // 0 = off, 1 = label, 2 = text, 3 = img, 4 = both 5 = line
                        CDvtmsData._sPrintPhysicianDateMode = 2; // 0 = off, 1 = label, 2 = today, 3 = line, 99 = ask

                    }
                    else
                    {
                        CProgram.sPromptError(true, "Missing configuration file!!", "Setup Configuration File!!");
                        Close();
                        return;
                    }
                }
                CDvxEvtRec.sbSetDvxStudyFolder(_mDVXStudyDir, false);

                if (sqlGlobalRowLimit > 1)
                {
                    CSqlDBaseConnection.sSetGlobalSqlRowLimit(sqlGlobalRowLimit);
                }
                CProgram.sSetProgTitleFormat("DVTMS", true, CProgram.sGetProgVersionLevel(), DShowOrganisation.First, "");
                CProgram.sSetProgramTitle("   <start " + startTime + startZone + ">", true);

                _mRecordingDir = Path.GetFullPath(_mRecordingDir);

                CProgram.sLogLine("ProgramDir=" + CProgram.sGetProgDir());
                CProgram.sLogLine("RecordingDir=" + _mRecordingDir);

                _mSqlDbName = new CEncryptedString("SqlDbName", DEncryptLevel.L1_Program, 32);
                if (_mSqlDbName != null && _mSqlDbName.mbSetEncrypted(dbName))
                {
                    Properties.Settings.Default.SqlDbName = _mSqlDbName.mGetEncrypted(); // store after encription has changed
                }

                string centerName = _mSqlDbName.mDecrypt();
                string centerFileName = centerName + ".CenterName";
                string centerFile = Path.Combine(_mRecordingDir, centerFileName);
                // check if directory points to the correct center
                if (false == File.Exists(centerFile))
                {
                    if (false == File.Exists(Path.Combine(_mRecordingDir, "..\\" + centerFileName)))
                    {
                        string fullPath = Path.GetFullPath(_mRecordingDir);
                        string drive = fullPath.Substring(0, 2).ToUpper();
                        CProgram.sLogError("Center=" + centerName + ", recording dir = " + _mRecordingDir);
                        //                        CProgram.sPromptError(true, "Config error!", "Center name not set correctly in recording dir, check configuration!! ");
                        CProgram.sPromptError(true, "Check drive " + drive + " and configuration!!", "Center file " + centerFile + " not found!");
                        _mSqlDbName.mbEncrypt("----");
                        Close();
                        return;
                    }
                }
                labelDrive.Text = _mRecordingDir.Substring(0, 2);
                string orgLabel = CProgram.sGetOrganisationLabel();
                if (centerName.ToUpper() != orgLabel.ToUpper())
                {
                    CProgram.sLogError("Center= " + centerName + ", Org = " + orgLabel);
                    CProgram.sPromptError(true, "Config error! Check configuration!!", "Center name not equal to database name!");
                    _mSqlDbName.mbEncrypt("----");
                    Close();
                    return;
                }
                string progDir = CProgram.sGetProgDir();
                // check center name in program dir
                /*               if (false == File.Exists(Path.Combine(progDir, "..\\..\\" + centerFileName)))
                               {
                                   if (false == File.Exists(Path.Combine(progDir, "..\\" + centerFileName)))
                                   {
                                       CProgram.sLogError("Center=" + centerName + ", recording dir = " + mRecordingDir);
                                       CProgram.sPromptError(true, "Config error!", "Center name not set correctly in program dir, check configuration!! ");
                                       mSqlDbName.mbEncrypt("----");
                                       Close();
                                       return;
                                   }
                               }
               */                //                Text = CProgram.sGetProgTitle() + " " + centerName; // set title

                //_mHeaViewExe = Properties.Settings.Default.HeaViewExe;
                //_mScpViewExe = Properties.Settings.Default.ScpViewExe;

                _mSqlServer = new CEncryptedString("SqlServer", DEncryptLevel.L1_Program, 64);
                if (_mSqlServer != null && _mSqlServer.mbSetEncrypted(dbServer))
                {
                    Properties.Settings.Default.SqlServer = _mSqlServer.mGetEncrypted(); // store after incription has changed
                }
                _mSqlPort = new CEncryptedInt("SqlPort", DEncryptLevel.L1_Program);
                if (_mSqlPort != null && _mSqlPort.mbSetEncrypted(dbPort))
                {
                    Properties.Settings.Default.SqlPort = _mSqlPort.mGetEncryptedInt(); // store after incription has changed
                }
                _mSqlSslMode = dbSslMode <= 0 ? DSqlSslMode.Default : (dbSslMode > (int)DSqlSslMode.VERIFY_IDENTITY ? DSqlSslMode.VERIFY_IDENTITY : (DSqlSslMode)dbSslMode);

                _mSqlUser = new CEncryptedString("SqlUser", DEncryptLevel.L1_Program, 32);
                if (_mSqlUser != null && _mSqlUser.mbSetEncrypted(dbUser))
                {
                    Properties.Settings.Default.SqlUser = _mSqlUser.mGetEncrypted(); // store after incription has changed
                }
                /*                mSqlPassword = new CEncryptedString("SqlPassword", DEncryptLevel.L1_Program, 32);
                                if (mSqlPassword != null && mSqlPassword.mbSetEncrypted(Properties.Settings.Default.SqlPassword))
                                {
                                    Properties.Settings.Default.SqlPassword = mSqlPassword.mGetEncrypted(); // store after incription has changed
                                }
                */
                _mSqlPassword = new CPasswordString(_mSqlUser.mDecrypt() + "@" + _mSqlServer.mDecrypt() + ":sqlPW", orgLabel);

                if (_mSqlPassword != null)
                {
                    _mSqlPassword.mbSetEncrypted(dbPW);

                    if (false == _mSqlPassword.mbIsEncrypted())
                    {
                        if (CProgram.sbAskYesNo("SqlPassword is not Encrypted!", "Change to encrypted password! Continue anyway?"))
                        {
                            _mSqlPassword.mbEncrypt(_mSqlPassword.mGetEncrypted());
                        }
                        else
                        {
                            CProgram.sLogError("Password for dBase is not encrypted, close program");
                            Close();
                            return;
                        }
                    }
                }

                int triageNrHours = Properties.Settings.Default.TriageNrHours;
                if (triageNrHours < 1) triageNrHours = 1;

                if (triageNrHours % 24 == 0) { triageNrHours /= 24; listBoxViewUnit.TopIndex = 1; }
                else { listBoxViewUnit.TopIndex = 0; }
                textBoxViewTime.Text = triageNrHours.ToString();

                string triageMode = Properties.Settings.Default.TriageMode;

                DTriageViewMode mode = mTriageViewParseLabel(triageMode);
                if (mode == DTriageViewMode.NrViewModes) mode = DTriageViewMode.Triage;
                //int modeIndex = listBoxEventMode.Items.IndexOf(triageMode);     // make a  DTriageViewMode from string
                //if (modeIndex < 0) modeIndex = 0;
                //if ( DTriageViewMode)
                //listBoxEventMode.SelectedIndex = modeIndex;     // D

                _mTriageUpdSec = Properties.Settings.Default.TriageUpdSec;
                _mTriageCountDays = Properties.Settings.Default.TriageCountDays;
                _mbTriageLockAnRec = Properties.Settings.Default.TriageLockAnRec;
                FormAnalyze._sbTriageLockAnRec = _mbTriageLockAnRec;

                UInt32 timerMilSec = Properties.Settings.Default.TriageTimerMilSec;

                if (timerMilSec > 0)
                {
                    if (timerMilSec < 50)
                    {
                        timerMilSec = 50;
                    }
                    timerTriage.Interval = (int)timerMilSec;
                }
                else if (bProgrammer)
                {
                    timerMilSec = (UInt32)timerTriage.Interval / 2;
                    if (timerMilSec < 20)
                    {
                        timerMilSec = 20;
                    }
                    timerTriage.Interval = (int)timerMilSec;
                }

                string studyLabel = Properties.Settings.Default.PrintStudyLabel;
                string studyCode = Properties.Settings.Default.PrintStudyCode;
                CDvtmsData.sSetPrintStudyLabel(studyLabel, studyCode);

                _mQuickTachyText = Properties.Settings.Default.QuickTachyText;
                _mQuickBradyText = Properties.Settings.Default.QuickBradyText;
                _mQuickAFibText = Properties.Settings.Default.QuickAfibText;
                _mQuickPauseText = Properties.Settings.Default.QuickPauseText;
                _mQuickOtherText = Properties.Settings.Default.QuickOtherText;
                _mQuickPacText = Properties.Settings.Default.QuickPace;
                _mQuickPvcText = Properties.Settings.Default.QuickPVC;
                _mQuickPaceText = Properties.Settings.Default.QuickPace;
                _mQuickVtText = Properties.Settings.Default.QuickVT;
                _mQuickNormText = Properties.Settings.Default.QuickNormal;
                _mQuickAbnText = Properties.Settings.Default.QuickAbNormal;
                _mQuickVfText = Properties.Settings.Default.QuickVF;
                _mQuickAFlText = Properties.Settings.Default.QuickAFl;
                _mQuickASysText = Properties.Settings.Default.QuickASys;

                CDvtmsData._sPrintGridColor = Properties.Settings.Default.PrintGridColor;
                CDvtmsData._sPrintSignalColor = Properties.Settings.Default.PrintSignalColor;
                CDvtmsData._sPrintSignalWidth = Properties.Settings.Default.PrintSignalWidth;

                CStudyInfo._sbStudyCountRecStates = Properties.Settings.Default.StudyCountRecStates;

                _mCheckSystemMin = Properties.Settings.Default.CheckSystemMin;

                UInt16 checkMemUsedMB = Properties.Settings.Default.CheckMemUsedMB;
                UInt16 checkMemFreeMB = Properties.Settings.Default.CheckMemFreeMB;
                UInt16 checkMemAvailMB = Properties.Settings.Default.CheckMemAvailMB;
                UInt16 maxWinProgMB = Properties.Settings.Default.MaxWinProgMB;
                UInt16 minWinFreeMB = Properties.Settings.Default.MinWinFreeMB;
                _mCheckDiskLocalGB = Properties.Settings.Default.CheckDiskDataGB;
                _mCheckDiskDataGB = Properties.Settings.Default.CheckDiskDataGB;
                _mCheckServerTzUrl = Properties.Settings.Default.CheckServerTzUrl;
                _mCheckServerSironaUrl = Properties.Settings.Default.CheckServerSironaUrl;

                CProgram.sSetProgMemLimits(checkMemUsedMB, checkMemFreeMB, checkMemAvailMB, maxWinProgMB, minWinFreeMB);

                CPatientInfo.sSetPatientSocID(Properties.Settings.Default.UsePatientSocID, Properties.Settings.Default.PatientIDText, Properties.Settings.Default.SocSecText);

                labelPatIdText.Text = CPatientInfo.sGetPatientSocIDText() + ":";

                toolStripReaderCheck.Text = "";
                toolStripButtonWarn.Text = "_";
                int daysLeft = CLicKeyDev.sGetDaysLeft();
                if (daysLeft <= 14)
                {
                    toolStripReaderCheck.Text = "dl= " + daysLeft.ToString();
                }
                panelViewMode.Visible = bProgrammer;

                labelUpdateTS.Text = ">";
                labelUpdateTC.Text = ">";
                //                listBoxViewUnit.SelectedIndex = 0;
                //                listBoxEventMode.SelectedIndex = 0;
                _mTriageFont = new Font("Courier New", 10.2F, FontStyle.Regular);

                UInt16 maxTryLoad = (UInt16)Properties.Settings.Default.ImgCachRetryCount;
                double updSec = Properties.Settings.Default.ImgCachUpdSec;
                UInt16 nrThreads = (UInt16)Properties.Settings.Default.ImgCachNrThreads;
                float maxHours = Properties.Settings.Default.ImgMaxCachHours;
                UInt16 simLoadDelay = (UInt16)Properties.Settings.Default.ImgCachDelayTck;

                int eventShowChannel = Properties.Settings.Default.EventShowChannel;

                if (eventShowChannel < 1) eventShowChannel = 1;
                comboBoxEventShowCh.Text = eventShowChannel.ToString();
#if DEBUG
                simLoadDelay += 0;
#endif
                if (simLoadDelay > 0)
                {
                    CProgram.sLogLine("ImageCach simulating delay TCK = " + simLoadDelay.ToString());
                }

                comboBoxTriageChannel.Items.Clear();

                buttonShowAll.FlatAppearance.BorderSize = 1;    // force 1

                string actChannel = "";
                for (int iChannel = 0; iChannel < 12; ++iChannel)
                {
                    string strChannel = (iChannel + 1).ToString() + ". " + CRecordMit.sGetDefaultChannelName((UInt16)iChannel, 0);
                    if (eventShowChannel == iChannel + 1)
                    {
                        actChannel = strChannel;
                    }
                    comboBoxTriageChannel.Items.Add(strChannel);
                }
                comboBoxTriageChannel.Text = actChannel;


                mLoadTriageButtonImages();
                _mbShowTriageImage = maxHours >= 0.01;
                _mImageCach = new CImageCach(updSec, maxTryLoad, nrThreads, maxHours, simLoadDelay);
                if (_mImageCach != null)
                {
                    Image img = mLoadSingleImageFile(Path.Combine(CProgram.sGetProgDir(), "Icons\\TriageStripEmpty.png"));
                    CImageCach.sSetEmptyImage(img);
                    img = mLoadSingleImageFile(Path.Combine(CProgram.sGetProgDir(), "Icons\\TriageStripNoData.png"));
                    CImageCach.sSetNoDataImage(img);
                }
                labelImageNk.Text = "";
                labelLoadImages.Text = "";
                labelMemUsage.Text = "";

                UInt16 priority = (UInt16)DPriority.High;
                buttonHigh.Text = CRecordMit.sGetPriorityValue(priority).ToString() + " " + CRecordMit.sGetPriorityText(priority);
                priority = (UInt16)DPriority.Normal;
                buttonNormal2.Text = CRecordMit.sGetPriorityValue(priority).ToString() + " " + CRecordMit.sGetPriorityText(priority);
                priority = (UInt16)DPriority.Low;
                buttonLow.Text = CRecordMit.sGetPriorityValue(priority).ToString() + " " + CRecordMit.sGetPriorityText(priority);

                _mWidthTriageStripNormal = dataGridTriage.Columns[(int)DTriageGridField.EventStrip].Width;
                _mWidthScreenNormal = Size.Width;
                mDisableUnactive();

                UInt32 dbLimit = 10000;

                _mTriageRowsLimit = Properties.Settings.Default.TriageRowsLimit;
                _mTriageDrawLimit = Properties.Settings.Default.TriageDrawLimit;

                CStudyListForm._sStudyRowsLimit = Properties.Settings.Default.StudyRowsLimit;

                if (false == mbCreateDBaseConnection())
                {
                    CProgram.sPromptError(true, "Startup failed!", "Failed to connect to dBase, please check connection and settings!");
                    Close();
                }
                else
                {
                    string enumPath = Path.Combine(CProgram.sGetProgDir(), "enums");
                    CDvtmsData.sSetDBaseServer(_mRecordingDir, _mDBaseServer, enumPath);

                    dbLimit = _mDBaseServer.mGetSqlRowLimit();
                    if (bProgrammer)
                    {
                        bool bWriteTable = false;

                        if (bWriteTable && mbCreateDBaseConnection())
                        {
                            CDvtmsData.mWriteTablesToCvs();
                        }
                    }

                    checkBoxUseLimit.Checked = _mTriageRowsLimit > 0 && _mTriageRowsLimit < dbLimit
                        || _mTriageDrawLimit > 0 && _mTriageDrawLimit < dbLimit;

                    string userInitials = Properties.Settings.Default.UserInitials;
                    string userPin = Properties.Settings.Default.UserPin;
                    int userTimeOutMin = Properties.Settings.Default.UserTimeOutMin;

                    if (false == CDvtmsData.sbLoadUserInitialIdNrList(toolStripSelectUser.Items, true, true))
                    {
                        toolStripSelectUser.Visible = false;
                        if (CDvtmsData.sbUserUseNoList())
                        {
                            if (bProgrammer)
                            {
                                CProgram.sSetProgUser(1, "sv", "Simon Vlaar", "4321");
                            }
                            else
                            {
                                CProgram.sSetProgUser(0, userInitials, userInitials, userPin);
                            }
                        }
                    }
                    else
                    {
                        toolStripSelectUser.Visible = true;
                        toolStripSelectUser.Enabled = true;
                        if (bProgrammer)
                        {
                            if (false == CDvtmsData.sbSetUserInitials("sv", "4321"))
                            {
                                CProgram.sSetProgUser(1, "sv", "Simon Vlaar", "4321");
                            }
                        }
                        else
                        {
                            CDvtmsData.sbSetUserInitials(userInitials, userPin);
                        }
                    }

                    makeLauncherVersionToolStripMenuItem.Visible = CLicKeyDev.sGetOrgNr() < 30;

                    CProgram.sProgUserInit(userTimeOutMin);
                    mUpdateUserButtons();

                    _mNrFullPreviewStrips = (UInt16)Properties.Settings.Default.NrFullPreviewStrips;
                    mEnableFullStrips();    // setenable the number of vfull preview strips

                    mTriageViewInitAllButtons();
                    mTriageViewCursorButton(DTriageViewMode.ListAll, false);

                    if (_sbTriageOnly)
                    {
                        mEnforceTriageOnly(true);
                    }

                    mSetTriageView(mode, false);

                    CDvtmsData.sbInitData();
                    mUpdateTriageStrips();
                    mUpdateTriageTime(DateTime.Now, false);

                    panelLoadStats.Visible = bProgrammer;
                    //                labelUpdTime.Visible = bProgrammer;
                    labelDrive.Visible = bProgrammer;
                    panelRight61.Visible = bProgrammer;

                    CTzDeviceSettings._sbUseQueue = Properties.Settings.Default.TzUseQueue;
                    CSironaStateSettings._sbUseQueue = Properties.Settings.Default.SirUseQueue;

                    CDeviceInfo.sbModelFromDeviceInfo = Properties.Settings.Default.ModelFromDevice;

                    if (bProgrammer)
                    {
                        bool b = true; // CTzDeviceSettings._sbUseQueue;
                        if (b)
                        {
                            b = true;
                        }
                        CTzDeviceSettings._sbUseQueue = b;
                        CSironaStateSettings._sbUseQueue = b;
                        CSirAutoCollect._sScanTimeMin = 5;
                    }
                    CPreviousFindingsForm._sbPlot2Ch = Properties.Settings.Default.StudyPrint2Ch;
                    FormAnalyze._sbPlot2Ch = Properties.Settings.Default.AnalyzePrint2Ch;

                    FormAnalyze._sZoomDurationSec = Properties.Settings.Default.AnalyzeZoomSec;
                    FormAnalyze._sZoomChannel = Properties.Settings.Default.AnalyzeZoomChannel;
                    FormAnalyze._sChannelsDurationSec = Properties.Settings.Default.AnalyzeChannelsSec;
                    FormAnalyze._sbPrintFD = Properties.Settings.Default.AnalyzePrintFD;
                    FormAnalyze._sbPrintFE = Properties.Settings.Default.AnalyzePrintFE;
                    FormAnalyze._sbPrintAC = Properties.Settings.Default.AnalyzePrintAC;
                    FormAnalyze._sPrintStripSec = Properties.Settings.Default.AnalyzePrintStripSec;

                    FormAnalyze._sbSaveAnStripImage = Properties.Settings.Default.SaveAnStripImage;

                    FormAnalyze._sAnHrMinBpm = Properties.Settings.Default.AnHrMinBpm;
                    FormAnalyze._sAnHrLowBpm = Properties.Settings.Default.AnHrLowBpm;
                    FormAnalyze._sAnHrBaseBpm = Properties.Settings.Default.AnHrBaseBpm;
                    FormAnalyze._sAnHrHighBpm = Properties.Settings.Default.AnHrHighBpm;
                    FormAnalyze._sAnHrMaxBpm = Properties.Settings.Default.AnHrMaxBpm;

                    bool bBlackWhite = Properties.Settings.Default.PrintBlackWhite;
                    CPreviousFindingsForm._sbBlackWhite = bBlackWhite;
                    FormAnalyze._sbBlackWhite = bBlackWhite;
                    CMCTTrendDisplay._sbBlackWhite = bBlackWhite;
                    CMCTTrendDisplay._sMctMinBlockMin = Properties.Settings.Default.MctMinBlockMin;
                    CMCTTrendDisplay._sMctMaxBlockMin = Properties.Settings.Default.MctMaxBlockMin;

                    mUpdateTriageCountDays();
                    mEnableSelectView(true);    // view All
                    sSetTriageTimerMode(false, DTriageTimerMode.New, "Form start");

                    CTzAutoCollect._sMinFileSize = Properties.Settings.Default.TcAcMinFileSize;        // only add files that are a minimum size
                    CTzAutoCollect._sReqAgeMin = Properties.Settings.Default.TcAcReqAgeMinMin;   // Only request for files that are at least x minutes old
                    CTzAutoCollect._sScanTimeMin = Properties.Settings.Default.TzAcScanMin;      // wait to do a scan after last scan
                    CTzAutoCollect._sMaxTryRequest = Properties.Settings.Default.TzAcMaxTry;
                    if (CTzAutoCollect._sMaxTryRequest < 1) CTzAutoCollect._sMaxTryRequest = 1;       // maximum number of request for a file
                    CTzAutoCollect._sWaitTryRequestMin = Properties.Settings.Default.TzAcWaitTryMin;
                    if (CTzAutoCollect._sWaitTryRequestMin < 1) CTzAutoCollect._sWaitTryRequestMin = 1; // wait time before requesting the same file again

                    CTzAutoCollect._sActionMaxNrReq = Properties.Settings.Default.TzAcActionMaxReq;
                    if (CTzAutoCollect._sActionMaxNrReq < 1) CTzAutoCollect._sActionMaxNrReq = 1;                  // max nr File request in a action
                    CTzAutoCollect._sActionMaxNrInBlock = Properties.Settings.Default.TzAcActionMaxInBlock;
                    if (CTzAutoCollect._sActionMaxNrInBlock < 1) CTzAutoCollect._sActionMaxNrInBlock = 1;
                    else if (CTzAutoCollect._sActionMaxNrInBlock > 250) CTzAutoCollect._sActionMaxNrInBlock = 250;// must be <= 250 _ 

                    CTzAutoCollect._sActionMaxNrOfBlocks = Properties.Settings.Default.TzAcActionMaxBlocks;  // must be <= 250 _ 
                    if (CTzAutoCollect._sActionMaxNrOfBlocks < 1) CTzAutoCollect._sActionMaxNrOfBlocks = 1;
                    else if (CTzAutoCollect._sActionMaxNrOfBlocks > 250) CTzAutoCollect._sActionMaxNrOfBlocks = 250;// must be <= 250 _ 
                    CTzAutoCollect._sMaxActionsInQueue = Properties.Settings.Default.TzAcActionMaxQueue;
                    if (CTzAutoCollect._sMaxActionsInQueue < 1) CTzAutoCollect._sMaxActionsInQueue = 1;   // do not add action if queue is full occupied 

                    float limitAmplRange = Properties.Settings.Default.LimitAmplRange;
                    CRecordMit.sbLimitAmplitudeRange = limitAmplRange >= 1.0F;
                    CRecordMit.sLimitAmplitudeRange = limitAmplRange < 1.0F ? 6.0F : limitAmplRange;

                    sbTzQuickFilter = Properties.Settings.Default.TzQuickFilter;
                    mFillTzFilterList();

//                    labelStudyPermissions.Visible = bProgrammer;

                    textBoxViewTime.Focus();

                    _sStudyPermissionsActive._mBitSet = Properties.Settings.Default.StudyPermActive;
                    _sStudyPermissionsFixed._mBitSet = Properties.Settings.Default.StudyPermFixed;
                    string strPermissions = Properties.Settings.Default.StudyPermText;
                    _sStudyPermissionsText = strPermissions == null || strPermissions.Length == 0 ? "Permissions" : strPermissions;
                    _sbStudyPermissionsLocked = Properties.Settings.Default.StudyPermLocked;
                    _sSelectedTrial = Properties.Settings.Default.SelectedTrial;
                    _sbSelectTrialLocked = Properties.Settings.Default.SelectedTrialLocked;
                    _sSelectedClient = Properties.Settings.Default.SelectedClient;
                    _mIndexBackColor = labelTriageRow.BackColor;

                    CStudyListForm._sStudyActiveRecDays = Properties.Settings.Default.StudyActiveRecDays;

                    _sStudyPermissionsActive.mOrSet(_sStudyPermissionsFixed );

                    checkBoxStudyPermissions.Text = CDvtmsData._sStudyPermSiteText + "=";

                    labelSelectPermisions.Visible = bProgrammer;

                    if(_sbForceAnonymize)
                    {
                        checkBoxAnonymize.Checked = true;
                        checkBoxAnonymize.Enabled = false;
                        toolStripWorkNew.Visible = false;
                    }

                    _sbPrintAskTechName = Properties.Settings.Default.PrintAskTechName;
                    _sbPrintForceTechName = Properties.Settings.Default.PrintForceTechName;
                    _sbPrintHideTechName = Properties.Settings.Default.PrintHideTechName;

                    int nList = CDvtmsData.sFillStudyPermissionsComboBox(comboBoxLocation, "---", _sStudyPermissionsActive, CDvtmsData._sStudyPermSiteGroup);
                    bool bVisible = bProgrammer || nList > 0;
                    comboBoxLocation.Visible = bVisible;
                    checkBoxStudyPermissions.Visible = bVisible;
                    checkBoxStudyPermissions.Checked = _sStudyPermissionsActive.mbIsNotEmpty();
                    if (_sbStudyPermissionsLocked)
                    {
                        checkBoxStudyPermissions.Enabled = false;
                    }
                    checkBoxTrial.Checked = _sSelectedTrial > 0;
                    if (_sbSelectTrialLocked)
                    {
                        checkBoxTrial.Enabled = false;
                    }
                    mUpdateClient();
                    mUpdateTrial();
                    mUpdateStudyPermissions();

                    _mTriageImageFont = new Font("Courier New", 10.2F, FontStyle.Regular);
                    _mTriageImageBrush = new SolidBrush(Color.DarkBlue);
                    _mTriageImageFormat = new StringFormat();
                    _mTriageImageFormat.Alignment = StringAlignment.Far;
                    _mTriageImageFormat.LineAlignment = StringAlignment.Near;




    }
}
            catch (Exception ex)
            {
                CProgram.sPromptException(true, "MainForm", "Exception during Main Form constructor", ex);
            }
        }

        ~FormEventBoard()
        {
            mDisposeDBaseConnection();
        }

        private void mUpdateClient()
        {
            labelClient.Text = CClientInfo.sGetClientLabel(_sSelectedClient, false, "---");
        }

        private void mUpdateTrial()
        {
            labelTrial.Text = CTrialInfo.sGetTrialLabel(_sSelectedTrial, false, "---" );
        }

        private void mUpdateStudyPermissions()
        {
            string s = "<>";

            _sStudyPermissionsActive.mOrSet(_sStudyPermissionsFixed);   // always make sure the fixed bits are set

            if (_sStudyPermissionsActive  != null && _sStudyPermissionsActive.mbIsNotEmpty())
            {
                s = " <" + CDvtmsData.sGetStudyPermissionsBitLabels(_sStudyPermissionsActive, "+") + ">";
            }

            labelSelectPermisions.Text = s;

            CDvtmsData.sbSetStudyPermissionsComboBox(comboBoxLocation, _sStudyPermissionsActive, CDvtmsData._sStudyPermSiteGroup);
        }

        private void mEnforceTriageOnly(bool AbSetAll)
{
            if( AbSetAll || false == checkBoxAnonymize.Checked )
            {
                checkBoxAnonymize.Checked = true;
                checkBoxAnonymize.Enabled = false;
                checkBoxHideS0.Checked = true;

                toolStripWorkNew.Visible = false;
                toolStripWorld.Visible = false;
                toolStripPatientList.Visible = false;
                toolStripDepartments.Visible = false;
                toolStripPhysician.Visible = false;
                toolStripButton1.Visible = false;
                toolStripDevices.Visible = false;
                toolStripAddDevice.Visible = false;
                toolStripUsers.Visible = false;
                toolStripCustomers.Visible = false;
                toolStripButtonTrial.Visible = false;
                buttonPdfView.Visible = false;
                buttonFindings.Visible = false;
                labelStudyReports.Visible = false;

                Anayze.Visible = false;
            }
        }

        private void mUpdateTriageCountDays()
        {
            int days = (int)(_mTriageCountDays);
            int hours = (int)((_mTriageCountDays - days) * 24 + 0.49);
            string s = days.ToString() + "d\r\n" + hours.ToString() + "h";
            labelTriageCountDays.Text = days == 0 && hours == 0 ? "-d\r\n-h" : s;

            labelCountedAll.Text = "-";
            labelCountedAnalyzed.Text = "-";
            labelCountedReceived.Text = "-";
            labelCountedTriaged.Text = "-";
        }
        private void mTriageUpdateReset()
        {
            if (_mTriageUpdSec > 0)
            {
                _mLastTriageActivityDT = DateTime.Now;
            }
        }

        private void mDisableUnactive()
        {
            //toolStripTop.Enabled = false;
            buttonRefresh.Enabled = false;
            labelDate.Text = "-";
            labelTime.Text = "-";
            //panelView.Enabled = false;
            panelPriority.Enabled = false;
            buttonAnalyzeAdd.Enabled = false;
        }

        Image mLoadSingleImageFile(string AFullName)
        {
            Image img = null;
            try
            {
                img = Image.FromFile(AFullName);
            }
            catch (Exception /*ex*/)
            {
                CProgram.sLogError("Failed loading image file " + AFullName);
                img = null;
            }
            return img;
        }
        public void mLoadTriageButtonImages()
        {
            string imgPath = Path.Combine(CProgram.sGetProgDir(), "Icons\\Triage\\");

            _mTriageImgOther = mLoadSingleImageFile(Path.Combine(imgPath, "TriageButtonInactiveShade.png"));
            _mTriageImgRead = mLoadSingleImageFile(Path.Combine(imgPath, "TriageButtonIdleShade.png"));
            _mTriageImgTriaged = mLoadSingleImageFile(Path.Combine(imgPath, "TriageButtonAnalyzeShade.png"));
            _mTriageImgNoise = mLoadSingleImageFile(Path.Combine(imgPath, "TriageButtonNoiseShade.png"));
            _mTriageImgExclude = mLoadSingleImageFile(Path.Combine(imgPath, "TriageButtonExcludeShade.png"));
            _mTriageImgLeadOff = mLoadSingleImageFile(Path.Combine(imgPath, "TriageButtonLeadoffShade.png"));
        }

        public void mShowBusy(bool AbBusy)
        {
            this.Cursor = AbBusy ? Cursors.WaitCursor : Cursors.Default;
        }
        public string mPathCombine(string APath, string AFileName)
        {
            string s = APath;
            int n = APath == null ? 0 : s.Length;
            if (n > 0 && s[n - 1] != '\\')
            {
                s += "\\";
            }
            return s + AFileName;
        }
        public string mPathCombine(string APath, string ADir1, string ADir2, string AFileName)
        {
            string s = APath;
            int n = APath == null ? 0 : s.Length;
            if (n > 0 && s[n - 1] != '\\')
            {
                s += "\\";
            }
            return s + ADir1 + "\\" + ADir2 + "\\" + AFileName;
        }

        private bool mbCreateDirectory(String ADirName)
        {
            bool bOk = Directory.Exists(ADirName);

            if (bOk == false)
            {
                Directory.CreateDirectory(ADirName);

                bOk = Directory.Exists(ADirName);
            }
            return bOk;
        }

        public bool mbCreateDBaseConnection()
        {
            bool bOk = _mDBaseServer != null;

            if (bOk)
            {
                try
                {
                    //mDBaseServer.mCloseSqlCmd(false);
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Close command db connection to server:" + _mSqlServer, ex);
                }

            }
            else
            {
                if (_mSqlServer != null && _mSqlPort != null && _mSqlUser != null && _mSqlPassword != null)
                {
                    try
                    {
                        _mDBaseServer = new CSqlDBaseConnection();

                        if (_mDBaseServer != null)
                        {
                            string server = _mSqlServer.mDecrypt();
                            int port = _mSqlPort.mDecrypt();
                            string user = _mSqlUser.mDecrypt();
                            string password = _mSqlPassword.mDecrypt();
                            string dbName = _mSqlDbName.mDecrypt();

                            string enumPath = Path.Combine(CProgram.sGetProgDir(), "enums");
                            CDvtmsData.sSetDBaseServer(_mRecordingDir, _mDBaseServer, enumPath);  // needed outside FormEventBoard


                            bOk = _mDBaseServer.mbSqlSetupServer(server, port, _mSqlSslMode, dbName, user, password);
                            if (false == bOk)
                            {
                                CProgram.sLogError("Invalid settings for server " + server);
                            }
                            else
                            {
                                bOk = _mDBaseServer.mbSqlCreateConnection();
                                if (false == bOk)
                                {
                                    CProgram.sLogError("Failed connection to server " + server);
                                }
                                else
                                {
                                    CProgram.sLogLine("Connected to dBase " + _mDBaseServer.mSqlGetServerName() + "." + _mDBaseServer.mSqlGetDBaseName());
                                }
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException("create db connection to server:" + _mSqlServer, ex);
                    }
                }
                if (false == bOk)
                {
                    _mDBaseServer.mSqlDisposeConnection();
                    _mDBaseServer = null;
                }
            }
            return bOk;
        }

        public bool mbTestDBaseConnection(out string ArState)
        {
            string state = "?";
            bool bOk = _mDBaseServer != null;

            if (bOk)
            {
                try
                {
                    _mDBaseServer.mCloseSqlCmd(true);
                    state = "OK";
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Close command db connection to server:" + _mSqlServer, ex);
                    state = "Cmd error ";
                }
                bOk = CDvtmsData.sbTestDBaseServer();  // testc by downloading a known enum list;
                if (false == bOk)
                {
                    state += "Failed";
                    CProgram.sLogError("test DBase connection: " + state + " =>disconnecting DB");
                    mDisposeDBaseConnection();
                }
            }
            else
            {
                state = "Not connected";
            }
            ArState = state;
            return bOk;
        }

        public void mDisposeDBaseConnection()
        {
            if (_mDBaseServer != null)
            {
                _mDBaseServer.mSqlDisposeConnection();
                CProgram.sLogLine("DisConnected from dBase " + _mDBaseServer.mSqlGetServerName() + "." + _mDBaseServer.mSqlGetDBaseName());
                _mDBaseServer = null;
            }
        }

        public bool mbAddSqlDbRow(out int ArIndex, CRecordMit ARec)
        {
            bool bOk = false;
            int index = 0;
            CSqlCmd cmd = null;

            if (ARec != null)
            {
                try
                {
                    if (mbCreateDBaseConnection())
                    {
                        cmd = ARec.mCreateSqlCmd(_mDBaseServer);

                        if (cmd == null)
                        {
                            CProgram.sLogLine("Failed to init cmd");

                        }
                        else if (false == cmd.mbPrepareInsertCmd(ARec.mMaskValid, true, true))
                        {
                            CProgram.sLogLine("Failed to prepare SQL row in table " + ARec.mGetDbTableName());
                        }
                        else if (cmd.mbExecuteInsertCmd())
                        {
                            index = (int)ARec.mIndex_KEY;   // return new key
                            bOk = true;
                            CProgram.sLogLine("Successfull insert in " + ARec.mGetDbTableName() + "[" + ARec.mIndex_KEY + "]");
                        }
                        else
                        {
                            CProgram.sLogLine("Failed to add SQL row in table " + ARec.mGetDbTableName());
                        }
                        cmd.mCloseCmd();
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Failed add Row", ex);
                }
                finally
                {
                    if (cmd != null)
                    {
                        cmd.mCloseCmd();
                    }
                }
            }
            ArIndex = index;
            mTriageUpdateReset();
            return bOk;
        }

        public string mGetStateStringOld(UInt32 ARecIndex, UInt16 APriority, UInt16 ARecState, UInt16 ATableRowState, UInt32 AStudyIX, UInt16 AEventNr, UInt16 ANrEvents, UInt16 AToDo, UInt16 ADone,
            UInt16 ANrChannels, float ADurationSec)
        {
            //            string s = APriority.ToString() + " " + CRecordMit.sGetPriorityLetters(APriority) + "\n" + CRecordMit.sGetRecStateChar(ARecState) + "\n"
            //                    + CSqlDataTableRow.sGetActiveChar(ATableRowState) + "\n" + AEventNr.ToString() + "/" + ANrEvents.ToString();
            bool b = ANrEvents == 0;
            string s = "S" + AStudyIX.ToString();

            if (AStudyIX == 0)
            {
                s += "\nR" + ARecIndex.ToString();
            }
            else
            {
                s += "\n# " + AEventNr.ToString();
            }

            s += "\nch " + ANrChannels.ToString();

            s += "\n" + CProgram.sPrintTimeSpan_dhmSec(ADurationSec, "", 2);
            /*
            int sec = (int)ADurationSec;
            int min = sec / 60;
            int hour = min / 60;
            min = min - hour * 60;
            sec = sec % 60;
            if (hour > 0)
            {
                s += "\n" + hour.ToString() + "h" + min.ToString("D02");
            }
            else
            {
                s += "\n" + min.ToString("D02") + ":" + sec.ToString("D02");
            }
            */
            /*string s = CRecordMit.sGetPriorityValue(APriority).ToString() + " " + CRecordMit.sGetPriorityLetters(APriority) + " " //+ CRecordMit.sGetRecStateChar(ARecState) + "  "
                                                                                                                                  //+ CSqlDataTableRow.sGetActiveChar(ATableRowState)
                    + "\n# " + AEventNr.ToString() + " / " + (b ? "-" : ANrEvents.ToString())
                    + "\nToDo: " + (b ? "-" : AToDo.ToString())
                    + "\nDone: " + (b ? "-" : ADone.ToString())
                    ;
*/
            return s;
        }

        public string mGetStateString( CRecordMit ARecord )
        {
            //            string s = APriority.ToString() + " " + CRecordMit.sGetPriorityLetters(APriority) + "\n" + CRecordMit.sGetRecStateChar(ARecState) + "\n"
            //                    + CSqlDataTableRow.sGetActiveChar(ATableRowState) + "\n" + AEventNr.ToString() + "/" + ANrEvents.ToString();

            string s = "?0?";

            if (ARecord != null && ARecord.mIndex_KEY > 0)
            {
                s = "S" + ARecord.mStudy_IX.ToString();

                if (ARecord.mStudy_IX == 0)
                {
                    s += "\nR" + ARecord.mIndex_KEY.ToString();
                }
                else
                {
                    s += "\n# " + ARecord.mSeqNrInStudy.ToString();
                }

                s += "\nch " + ARecord.mNrSignals.ToString() + " ";
                if( ARecord._mStudyTrial_IX > 0 )
                {
                    s += "T";
                }
                if( ARecord._mStudyPermissions != null && ARecord._mStudyPermissions.mbIsNotEmpty())
                {
                    s += "@";
                }
                s += "\n" + CProgram.sPrintTimeSpan_dhmSec(ARecord.mRecDurationSec, "", 2);
            }
            return s;
        }

        public string mGetTriageButtonTextOld(DRecState AState)
        {
            string noise = "Noise ";
            string exclude = "Exclude";
            string s1 = "──────";
            string leadOff = "Lead off";
            string analyze = "Analyze ";
            string s2 = "───────";

            switch ((DRecState)AState)
            {
                //                case DRecState.Unknown:     
                //                case DRecState.Recording: 
                //                case DRecState.Recorded: 
                //                case DRecState.Read: 
                //                case DRecState.Processed: 
                case DRecState.Noise: noise = noise.ToUpper(); break;
                case DRecState.LeadOff: leadOff = leadOff.ToUpper(); break;
                case DRecState.Excluded: exclude = exclude.ToUpper(); break;
                //case DRecState.BaseLine: 
                case DRecState.Triaged: analyze = analyze.ToUpper(); break;
                    //case DRecState.Analyzed: 
                    //                case DRecState.Reported: 
            }
            string s = noise + " │ " + leadOff + " \n" + s1 + "─┼─" + s2 + "\n" + exclude + " │ " + analyze;

            return s;
        }

        public Image mGetTriageButtonImage(DRecState AState)
        {
            switch ((DRecState)AState)
            {
                //                case DRecState.Unknown:     
                //                case DRecState.Recording: 
                //                case DRecState.Recorded: 
                case DRecState.Received:
                    return _mTriageImgRead;
                case DRecState.Processed:
                    return _mTriageImgRead;
                case DRecState.Noise:
                    return _mTriageImgNoise;
                case DRecState.LeadOff:
                    return _mTriageImgLeadOff;
                case DRecState.Excluded:
                    return _mTriageImgExclude;
                //case DRecState.BaseLine: 
                case DRecState.Triaged:
                    return _mTriageImgTriaged;
                    //case DRecState.Analyzed: 
                    //                case DRecState.Reported: 
            }
            return _mTriageImgOther;
        }

        public void mUpdateGridRowState(DataGridViewRow AGridRow)
        {
            if (AGridRow != null)
            {
                string stateStr = AGridRow.Cells[(int)DTriageGridField.State].Value.ToString();
                if (stateStr.Length > 2) stateStr = stateStr.Substring(0, 2);
                UInt16 state = 0;
                UInt16.TryParse(stateStr, out state);

                string priorityStr = AGridRow.Cells[(int)DTriageGridField.PSA].Value.ToString();
                UInt16 priority = 0;
                priorityStr = priorityStr.Substring(0, 1);
                UInt16.TryParse(priorityStr, out priority);

                string activeStr = AGridRow.Cells[(int)DTriageGridField.Active].Value.ToString();
                UInt16 active = 0;
                UInt16.TryParse(activeStr, out active);

                //                UInt16 countIndex = 1;
                //               UInt16 countTotal = 0; // future info from study or record list
                //                UInt16 countToDo = 0;
                //                UInt16 countDone = 0;
                //                string s = mGetStateString(priority, state, active, countIndex, countTotal, countToDo, countDone);

                //                AGridRow.Cells[(int)DTriageGridField.PSA].Value = s;

                //s = mGetTriageButtonText((DRecState)state);
                Image img = mGetTriageButtonImage((DRecState)state);
                AGridRow.Cells[(int)DTriageGridField.Triage].Value = img;

                AGridRow.DefaultCellStyle.BackColor = CRecordMit.sGetRecStateBgColor(state);
            }
            mTriageUpdateReset();
        }

        public string mFirstWord(string ALine)
        {
            string s = "";
            int n = ALine == null ? 0 : ALine.Length;
            int l = 0;

            for (int i = 0; i < n; ++i)
            {
                char c = ALine[i];
                if (char.IsLetterOrDigit(c))
                {
                    s += c;
                    ++l;
                }
                else if (l > 0)
                {
                    break;
                }
            }
            return s;
        }

        public string mMakeRowPatient(CRecordMit ARec, bool AbAnonimize)
        {
            /*           if( ARec.mStudy_IX == 20)
                       {
                           int bd = ARec.mPatientBirthDate.mDecrypt();
                           int a = ARec.mSeqNrInStudy;

                       }
                       */
            bool bAnonimize = AbAnonimize || _sbForceAnonymize;

            int age = ARec.mGetAgeYears();// mGetAgeYears(ARec.mPatientBirthDate.mDecrypt());

            string rs = "";
            if (ARec.mStudy_IX == 0 || ARec.mDevice_IX == 0)
            {
                rs = "[";
                if (ARec.mDevice_IX == 0) rs += "?D";
                if (ARec.mStudy_IX == 0) rs += "!S";
                rs += "] ";
            }
            string patient = rs;
            patient += bAnonimize ? "--An--" : ARec.mPatientID.mDecrypt();
            if (age > 0) patient += "\n" + age.ToString() + " years";
            if (ARec.mPatientGender > (int)DGender.Unknown) patient += ", " + CPatientInfo.sGetGenderString((DGender)ARec.mPatientGender);
            patient += "\n" + (bAnonimize ? "--Anonymized--" : ARec.mPatientTotalName.mDecrypt());

            return patient;
        }

        public string mMakeRowRemark(CRecordMit ARec)
        {
            string remark = "";
            string recRemark = ARec.mRecRemark.mDecrypt();

            if (ARec.mDevice_IX == 0) remark += "?#" + ARec.mDeviceID + "\r\n";
            if (ARec.mRecLabel != null && ARec.mRecLabel.Length > 0 && false == recRemark.StartsWith(ARec.mRecLabel)) remark = ARec.mRecLabel + "\r\n";

            remark += recRemark;

            return remark;
        }


        bool mbTriageStoreGrid(bool AbDirectLoad, CRecordMit ARec, bool AbLoadImage, string ACursorIndexKey, UInt16 AQuickViewCh)
        {
            bool bOk = true;
            bool bHidePatient = checkBoxAnonymize.Checked;

            UInt32 studyIX = ARec.mStudy_IX;
            UInt16 seqInStudy = ARec.mSeqNrInStudy;
            UInt16 countTotal = 0; // future info from study or record list
            UInt16 countToDo = 0;
            UInt16 countDone = 0;
            string indexKey = ARec.mIndex_KEY.ToString();
            bool bCursor = ACursorIndexKey != null && ACursorIndexKey.Length > 0 && ACursorIndexKey == indexKey;

            UInt16 quickViewChannel = AQuickViewCh < ARec.mNrSignals ? AQuickViewCh : (UInt16)0;

            int n = dataGridTriage.Rows.Count;

            string eventTimeString = "", patientDateString = "";
            DateTime dt = ARec.mGetEventUTC();
            //            string patientName = ARec.mPatientTotalName.mDecrypt();
            string state = ARec.mRecState.ToString("D2") + "\n" + CRecordMit.sGetStateUserString(ARec.mRecState);
            string converter = CRecordMit.sGetConverterUserString(ARec.mImportConverter);
            string method = CRecordMit.sGetMethodUserString(ARec.mImportMethod);

            if (dt > DateTime.MinValue) eventTimeString = ARec.mGetDeviceTimeString(dt, DTimeZoneShown.Short);
            if (ARec.mPatientBirthDate.mbNotZero()) patientDateString = CProgram.sDateToString(ARec.mPatientBirthDate.mDecryptToDate());

            dt = CProgram.sDateTimeToLocal(ARec.mReceivedUTC);
            string read = "";
            read += CProgram.sDateTimeToString(dt) + "\r\n";// sDateTimeToFormatString(dt, "yyyy/MM/dd HH:mm:ss");

            if (ARec.mTransmitUTC > DateTime.MinValue)
            {
                int min = (int)((ARec.mReceivedUTC - ARec.mTransmitUTC).TotalMinutes + 0.499);

                if (min >= 6000)
                {
                    read += min >= 60000 ? "" : "T+?";
                }
                else if (min >= 30)
                {
                    read += "T" + (min / 60).ToString() + ":" + (min % 60).ToString("00");
                    //                    read = "T+" + (min / 60).ToString() + ":" + (min % 60).ToString("00");
                }
            }
            //read += "\r\n" + CProgram.sDateTimeToString(dt);// sDateTimeToFormatString(dt, "yyyy/MM/dd HH:mm:ss");

            if (ARec.mEventUTC > DateTime.MinValue)
            {
                double durationSec = ARec.mRecDurationSec < 1 ? ARec.mGetSamplesTotalTime() : ARec.mRecDurationSec;
                DateTime endRecUtc = ARec.mBaseUTC.AddSeconds(ARec.mRecDurationSec);

                if (ARec.mEventUTC > endRecUtc)
                {
                    endRecUtc = ARec.mEventUTC;
                }
                // compare end of event strip because event can be 1 hour
                int min = (int)((ARec.mReceivedUTC - endRecUtc).TotalMinutes + 0.499);

                if (min >= 6000)
                {
                    read += min > 60000 ? "" : "E+?";
                    //                    read += min > 600000 ? "" : "\nE+?";
                }
                else if (min >= 30)
                {
                    read += "E" + (min / 60).ToString() + ":" + (min % 60).ToString("00");
                    //read += "\nE+" + (min / 60).ToString() + ":" + (min % 60).ToString("00");
                }
            }
            read += "\n" + ARec.mDeviceID;
            string readUTC = "-?-";
            string rdDir = "";
            string filePath = "";
            if (ARec.mReceivedUTC > DateTime.MinValue)
            {
                string ym, d;

                ym = "YM" + ARec.mReceivedUTC.Year.ToString() + ARec.mReceivedUTC.Month.ToString("00");
                d = "D" + ARec.mReceivedUTC.Day.ToString("00");

                filePath = mPathCombine /*Path.Combine*/(_mRecordingDir, ym, d, ARec.mStoredAt);
                rdDir = ym + "\\" + d + "\n" + (ARec.mStoredAt.Length > 0 ? ARec.mStoredAt : "-?-");

                readUTC = ARec.mReceivedUTC.Hour.ToString("00") + ":" + ARec.mReceivedUTC.Minute.ToString("00") + ":" + ARec.mReceivedUTC.Second.ToString("00")
                    + "\r\n" + ARec.mReceivedUTC.Year.ToString() + ARec.mReceivedUTC.Month.ToString("00") + ARec.mReceivedUTC.Day.ToString("00");
            }
            string eventStr = /*mFirstWord(*/ARec.mEventTypeString/*)*/ + "\n" + eventTimeString;
            string remark = mMakeRowRemark(ARec);

            string psa = mGetStateString(ARec);
//            string psa = mGetStateString(ARec.mIndex_KEY, 2, ARec.mRecState, (UInt16)(ARec.mbActive ? 1 : 0), studyIX, seqInStudy,
  //              countTotal, countToDo, countDone, ARec.mNrSignals, ARec.mRecDurationSec);
            //string triage = mGetTriageButtonText((DRecState)ARec.mRecState);
            Image triageImg = mGetTriageButtonImage((DRecState)ARec.mRecState);
            Image eventStrip = AbLoadImage ? mLoadImage(AbDirectLoad, ARec.mIndex_KEY, filePath, quickViewChannel, ARec.mFileName, false, n) : null;

            string study = ARec.mStudy_IX.ToString();
            string active = ARec.mbActive ? "1" : "0";
            string patient = mMakeRowPatient(ARec, bHidePatient);

            int offset = ARec.mTimeZoneOffsetMin;
            string utcOffset = "UTC";
            if (offset >= 0) utcOffset += "+"; else { utcOffset += "-"; offset = -offset; }
            utcOffset += (offset / 60).ToString("00") + ":" + (offset % 60).ToString("00");


            string devModel = ARec.mDeviceModel == null ? "" : ARec.mDeviceModel;
            if (devModel.Length > 10)
            {
                devModel = devModel.Substring(0, 10);
            }
            string modelNr = "Model: " + devModel + "\n#: " + ARec.mDeviceID //+ "\n" //+ CRecordMit.sGetConverterUserString(ARec.mImportConverter) 
                                 + "\n" + utcOffset
                    + "\nimport=" + CRecordMit.sGetMethodUserString(ARec.mImportMethod)
                    + "\n   " + CRecordMit.sGetConverterUserString(ARec.mImportConverter);

            string recStudyRef = "A" + ARec.mNrAnalysis.ToString() + " ";
            if (ARec.mStudy_IX > 0) recStudyRef += "S" + ARec.mStudy_IX.ToString();
            if( ARec.mSeqNrInStudy > 0 ) recStudyRef += "." + ARec.mSeqNrInStudy.ToString();
            if (ARec.mPatient_IX > 0) recStudyRef += " P" + ARec.mPatient_IX.ToString();
            if (ARec._mStudyTrial_IX > 0) recStudyRef += " T" + ARec._mStudyTrial_IX.ToString();
            if (ARec._mStudyClient_IX > 0) recStudyRef += " C" + ARec._mStudyClient_IX.ToString();
            if (ARec._mStudyPermissions != null && ARec._mStudyPermissions.mbIsNotEmpty()) recStudyRef += " &&" + ARec._mStudyPermissions._mBitSet.ToString();

            // calculate abs path dir
            //DTriageGridFields: Index, State, Count, Patient, Recorded, Event, 
            //           Remark, EventStrip, Triage, FilePath, FileName, NrSignals, Study, Active
            dataGridTriage.Rows.Add(psa, patient, eventStr, read, remark, eventStrip, triageImg,
                indexKey, state, filePath, ARec.mFileName, ARec.mNrSignals.ToString(), study, active, modelNr, rdDir, recStudyRef, readUTC);

            if (dataGridTriage.Rows.Count > n)
            {
                DataGridViewRow gridRow = dataGridTriage.Rows[n];

                if (gridRow != null)
                {
                    gridRow.DefaultCellStyle.BackColor = CRecordMit.sGetRecStateBgColor(ARec.mRecState);
                    gridRow.Cells[(int)DTriageGridField.Triage].Style.Font = _mTriageFont;
                    if (bCursor)
                    {
                        dataGridTriage.CurrentCell = gridRow.Cells[0];  // set cursor back to current
                    }
                }
            }
            mTriageUpdateReset();
            return bOk;
        }
        public string mGetImageFullName(ushort ASignalIndex, string AFileName, bool AbOld)
        {
            string s = "Full";

            if (AbOld == false)
            {
                s += (ASignalIndex + 1).ToString("00");   // program uses 0, user starts at 1
            }
            return s + "_" + AFileName + ".png";
        }
        public string mGetImageEventName(ushort ASignalIndex, string AFileName, bool AbOld)
        {
            string s = "Event";

            if (AbOld == false)
            {
                s += (ASignalIndex + 1).ToString("00");
            }
            return s + "_" + AFileName + ".png";
        }

        public Image mLoadImage(bool AbDirectLoad, UInt32 AIndex, string AFilePath, ushort ASignalIndex, string AFileName, bool AbFull, Int32 ARow)
        {
            Image img = null;
            string fileName = "";

            try
            {
                fileName = AbFull ? "Full" : "Event";

                fileName += (ASignalIndex + 1).ToString("00");   // program uses 0, user starts at 1

                fileName += "_" + Path.GetFileNameWithoutExtension(AFileName) + ".png";

                if (_mImageCach == null)
                {
                    string filePath = mPathCombine /*Path.Combine*/(AFilePath, fileName);

                    img = Image.FromFile(filePath);
                }
                else
                {
                    img = _mImageCach.mGetImage(AbDirectLoad, AIndex, AFilePath, fileName, ARow);
                }
            }
            catch (Exception /*ex*/)
            {
                CProgram.sLogError("Failed " + (AbDirectLoad ? "direct " : "") + "loading " + AIndex.ToString()
                    + ": " + AFilePath + fileName);
                img = null;
            }

            return img;
        }

        public void mUpdateTriageFullStrip(Panel APanel, bool AbDirectLoad, UInt32 AIndex, string AFilePath, string AFileName, ushort ASignalIndex)
        {
            Image img = null;
            string fileName = "";

            try
            {
                if (AFilePath != null && AFilePath.Length > 0 && AFileName != null && AFileName.Length > 0)
                {
                    fileName = mGetImageFullName(ASignalIndex, AFileName, false);

                    if (_mImageCach == null)
                    {
                        string filePath = mPathCombine /*Path.Combine*/(AFilePath, fileName);

                        img = Image.FromFile(filePath);
                    }
                    else
                    {
                        img = _mImageCach.mGetImage(AbDirectLoad, AIndex, AFilePath, fileName, -1);
                    }
                }
            }
            catch (Exception /*ex*/)
            {
                CProgram.sLogError("Failed " + (AbDirectLoad ? "direct " : "") + "loading " + AIndex.ToString()
                    + ": " + AFilePath + fileName);
                img = null;
            }
            APanel.BackgroundImage = img;
        }
        string mGetFullStripChannelName(UInt16 AChannel)
        {
            string s = "?";

            if (AChannel < comboBoxTriageChannel.Items.Count)
            {
                s = comboBoxTriageChannel.Items[AChannel].ToString();
            }
            return s;

        }
        public void mUpdateTriageFullStripImages(bool AbDirectLoad, UInt32 AIndex, string AFilePath, string AFileName, ushort ASignalIndex, ushort ANrSignals)
        {
            uint nrChannels = (UInt16)(ANrSignals <= 1 ? 1 : ANrSignals);
            UInt16 channel = (UInt16)(ASignalIndex % nrChannels);

            if (_mNrFullPreviewStrips >= 1)
            {
                mUpdateTriageFullStrip(panelTriageStrip0, AbDirectLoad, AIndex, AFilePath, AFileName, channel);
                labelFullStrip0.Text = mGetFullStripChannelName(channel);
            }
            else
            {
                panelTriageStrip0.BackgroundImage = null;
            }
            if (_mNrFullPreviewStrips >= 2 && nrChannels >= 2)
            {
                channel = (UInt16)(++channel % nrChannels);
                mUpdateTriageFullStrip(panelTriageStrip1, AbDirectLoad, AIndex, AFilePath, AFileName, channel);
                labelFullStrip1.Text = mGetFullStripChannelName(channel);
            }
            else
            {
                panelTriageStrip1.BackgroundImage = null;
                labelFullStrip1.Text = "-";
            }
            if (_mNrFullPreviewStrips >= 3 && nrChannels >= 3)
            {
                channel = (UInt16)(++channel % nrChannels);
                mUpdateTriageFullStrip(panelTriageStrip2, AbDirectLoad, AIndex, AFilePath, AFileName, channel);
                labelFullStrip2.Text = mGetFullStripChannelName(channel);
            }
            else
            {
                panelTriageStrip2.BackgroundImage = null;
                labelFullStrip2.Text = "-";
            }
            mTriageUpdateReset();
        }
        private void mEnableFullStrips()
        {
            bool bVisible = _mNrFullPreviewStrips >= 1;

            if (panelTriageStrip0.Visible != bVisible)
            {
                panelTriageStrip0.BackgroundImage = null;
                panelTriageStrip0.Visible = bVisible;
                labelFullStrip0.Text = "=";
            }
            bVisible = _mNrFullPreviewStrips >= 2;

            if (panelTriageStrip1.Visible != bVisible)
            {
                panelTriageStrip1.BackgroundImage = null;
                panelTriageStrip1.Visible = bVisible;
                labelFullStrip1.Text = "=";
            }
            bVisible = _mNrFullPreviewStrips >= 3;

            if (panelTriageStrip2.Visible != bVisible)
            {
                panelTriageStrip2.BackgroundImage = null;
                panelTriageStrip2.Visible = bVisible;
                labelFullStrip2.Text = "=";
            }
        }
        private void mClearFullStrips()
        {
            panelTriageStrip0.BackgroundImage = null;
            panelTriageStrip1.BackgroundImage = null;
            panelTriageStrip2.BackgroundImage = null;
            labelFullStrip0.Text = ".";
            labelFullStrip1.Text = ".";
            labelFullStrip2.Text = ".";
        }

        public void mUpdateTriageStrips(bool AbShow = true)
        {
            string rowIndexText = "- / -";
            mTriageUpdateReset();
            if (_mbUpdatingStrip)
            {
                return;
            }
            if (_mbUpdatingGrid)
            {
                return;
            }
            // show pictures
            try
            {
                bool bAnonimize = checkBoxAnonymize.Checked || _sbForceAnonymize;
                _mbUpdatingStrip = true;
                DateTime dt = DateTime.Now;
                ushort signalIndex = 0, nrSignals = 0;
                string strIndex = comboBoxTriageChannel.Text;
                UInt32 nrEvents = 0;
                labelUpdateTC.Text = ">UpdTC";
                labelUpdTimeStrip.Text = "uS?";

                if (strIndex != null && strIndex.Length > 1) strIndex = strIndex.Substring(0, 1);

                if (false == ushort.TryParse(strIndex, out signalIndex))
                {
                    signalIndex = 1;
                }
                if (signalIndex > 0) --signalIndex;     // program starts at 0, user at 1

                Color recordBgColor = _defaultFooterColor;

                DataGridViewRow currentRow = dataGridTriage.CurrentRow;
                int rowIndex = 0;
                int nrRows = dataGridTriage.Rows.Count;

                dataGridTriage.UseWaitCursor = true;
                mShowBusy(true);

                if (AbShow && currentRow != null && Index.Index >= 0 && currentRow.Cells.Count >= (int)DTriageGridField.NrGridFields)
                {
                    rowIndex = currentRow.Index + 1;

                    string filePath = currentRow.Cells[(int)DTriageGridField.FilePath].Value.ToString();
                    string fileName = currentRow.Cells[(int)DTriageGridField.FileName].Value.ToString();
                    string patName = currentRow.Cells[(int)DTriageGridField.Patient].Value.ToString();
                    string indexKey = currentRow.Cells[(int)DTriageGridField.Index].Value.ToString();
                    string studyIndex = currentRow.Cells[(int)DTriageGridField.Study].Value.ToString();
                    string channels = currentRow.Cells[(int)DTriageGridField.NrSignals].Value.ToString();
                    string readUTC = currentRow.Cells[(int)DTriageGridField.ReadUTC].Value.ToString();
                    //string studyStr = index.Cells[(int)DTriageGridField.Study].Value.ToString();
                    string studyRem = "";
                    string modelNr = currentRow.Cells[(int)DTriageGridField.ModelNr].Value.ToString();
                    string state = currentRow.Cells[(int)DTriageGridField.State].Value.ToString();
                    string recDir = currentRow.Cells[(int)DTriageGridField.RecDir].Value.ToString();
                    string nrAnalysis = currentRow.Cells[(int)DTriageGridField.NrAnalysis].Value.ToString();

                    CStudyInfo studyInfo = null;
                    CPatientInfo patientInfo = null;

                    UInt32 indexKeyNr = 0, studyKeyNr = 0; ;

                    UInt16.TryParse(channels, out nrSignals);

                    bool bLocked = false;

                    if (_mbTriageLockAnRec)
                    {
                        bLocked = CProgram.sbCheckLockFilePath(filePath, CRecordMit.sGetLockName());
                    }

                    if (signalIndex > 0 && signalIndex >= nrSignals)
                    {
                        signalIndex = 0;
                        comboBoxTriageChannel.SelectedIndex = 0;
                    }

                    if (UInt32.TryParse(indexKey, out indexKeyNr) && indexKeyNr != 0)
                    {
                        mUpdateTriageFullStripImages(true, indexKeyNr, filePath, Path.GetFileNameWithoutExtension(fileName), signalIndex, nrSignals);

                        if (currentRow.Cells[(int)DTriageGridField.EventStrip].Value == null)
                        {
                            bool bLoad = false; // = true;  direct load
                            Image img = mLoadImage(bLoad, indexKeyNr, filePath, 0, fileName, false, currentRow.Index);

                            if (img != null)
                            {
                                currentRow.Cells[(int)DTriageGridField.EventStrip].Value = img;
                            }
                        }
                    }
                    else
                    {
                        mClearFullStrips();
                    }
                    if (bLocked)
                    {
                        recordBgColor = Color.Orange;
                        indexKey += " ®";
                    }

                    modelNr += "\nR.UTC= " + CProgram.sGetFirstLine(readUTC);

                    int pos = state.IndexOf('\n');
                    if (pos >= 0) state = state.Substring(pos + 1);
                    modelNr += "\nstate=" + state;
                    string patID = bAnonimize ? "--An--" : "";
                    string studyNrStr = "-";
                    // string studySubNrStr = "-";  is not in table
                    //string studyText = "";
                    string eventsStr = "";
                    string clientStr = "";
                    string trialStr = "";
                    // string eventDur = "";
                    string freeNote = "";
                    string studyPerm = "Press button to create study";

                    pos = patName.IndexOf('\n');
                    if (pos >= 0)
                    {
                        patID = patName.Substring(0, pos);
                        patName = patName.Substring(pos + 1);
                    }
                    else
                    {
                        patName = bAnonimize ? "--Anonymized--" : "";
                    }

                    if (UInt32.TryParse(studyIndex, out studyKeyNr) && studyKeyNr > 0)
                    {
                        studyInfo = new CStudyInfo(CDvtmsData.sGetDBaseConnection());

                        studyPerm = "";
                        if (studyInfo != null && studyInfo.mbDoSqlSelectIndex(studyKeyNr, studyInfo.mMaskValid))
                        {
                            studyNrStr = studyInfo.mIndex_KEY.ToString();

                            if (studyInfo._mNrRecords > 0)
                            {
                                eventsStr = "Events: " + studyInfo._mNrRecords.ToString();
                                nrEvents = studyInfo._mNrRecords;
                            }
                            if (studyInfo._mNrReports > 0)
                            {
                                eventsStr += "\r\nReports: " + studyInfo._mNrReports.ToString();
                            }

                            if (CDvtmsData._sEnumListStudyProcedures != null)
                            {
                                studyRem = CDvtmsData._sEnumListStudyProcedures.mFillString(studyInfo._mStudyProcCodeSet, true, " ", true, "\r\n");
                            }
                            studyRem += "\r\n" + studyInfo._mStudyRemarkLabel;

                            if (studyInfo._mPatient_IX > 0)
                            {
                                patientInfo = new CPatientInfo(CDvtmsData.sGetDBaseConnection());

                                if (patientInfo != null && patientInfo.mbDoSqlSelectIndex(studyInfo._mPatient_IX, patientInfo.mMaskValid))
                                {
                                    patID = bAnonimize ? "--An--" + studyInfo._mPatient_IX.ToString()
                                        : patientInfo.mGetPatientSocIDValue(true);// _mPatientID.mDecrypt();
                                    patName = bAnonimize ? "--Anonymized--" : patientInfo.mGetFullName();

                                    int age = patientInfo.mGetAgeYears();
                                    if (age >= 0) patName += "\r\n" + age.ToString() + " years";

                                    if (patientInfo._mPatientGender_IX > 0)
                                    {
                                        patName += ", " + CPatientInfo.sGetGenderString((DGender)patientInfo._mPatientGender_IX);

                                    }
                                    if (patientInfo._mRemarkLabel.Length > 0)
                                    {
                                        patName += "\r\n" + patientInfo._mRemarkLabel;
                                    }
                                    //                                    if( false == bAnonimize)
                                    {
                                        string studyNote = studyInfo._mStudyPhysicianInstruction;
                                        if (studyNote != null && studyNote.Length > 0)
                                        {
                                            freeNote = "Study Note:\r\n" + studyNote.Trim();
                                        }

                                    }
                                }
                            }
                            if (studyInfo._mTrial_IX > 0)
                            {
                                CTrialInfo trial = new CTrialInfo(CDvtmsData.sGetDBaseConnection());

                                if (trial != null && trial.mbDoSqlSelectIndex(studyInfo._mTrial_IX, CSqlDataTableRow.sGetMask((UInt16)DTrialInfoVars.Label)))
                                {
                                    trialStr += "Trial= " + trial._mLabel;
                                }
                                else
                                {
                                    trialStr += "Trial= ?" + studyInfo._mTrial_IX.ToString();
                                }
                            }
                            if (studyInfo._mClient_IX > 0)
                            {
                                CClientInfo client = new CClientInfo(CDvtmsData.sGetDBaseConnection());

                                if (client != null && client.mbDoSqlSelectIndex(studyInfo._mClient_IX, CSqlDataTableRow.sGetMask((UInt16)DClientInfoVars.Label)))
                                {
                                    clientStr += "Client= " + client._mLabel;
                                }
                                else
                                {
                                    clientStr += "Client= ?" + studyInfo._mClient_IX.ToString();
                                }
                            }

                            if (studyInfo._mStudyPermissions.mbIsNotEmpty())
                            {
                                studyPerm = " <" + CDvtmsData.sGetStudyPermissionsBitLabels(studyInfo._mStudyPermissions, " +")+" >";
                            }
                        }
                    }
                    labelPatientID.Text = patID;
                    labelTriagePatient.Text = patName;
                    labelRecNr.Text = indexKey;
                    labelEventStudyInfo.Text = nrAnalysis;
                    labelRecIndex.Text = indexKey;
                    labelStudyIndex.Text = studyIndex;
                    labelNrChannels.Text = nrSignals.ToString();
                    labelStudyNrEvents.Text = eventsStr; 
                    labelTriageStudy.Text = studyNrStr;
                    textStudyFreeNote.Text = freeNote;

                    labelModelNr.Text = modelNr;
                    textBoxStudyRemark.Text = studyRem;
                    textBoxRecordDir.Text = recDir;
                    labelStudyPermissions.Text = studyPerm;
                    labelStudyClient.Text = clientStr;
                    labelStudyTrial.Text = trialStr;
                }
                else
                {
                    rowIndex = 0;
                    labelTriagePatient.Text = "";
                    labelRecNr.Text = "";
                    labelEventStudyInfo.Text = "";
                    labelRecIndex.Text = "-";
                    labelStudyIndex.Text = "";
                    labelNrChannels.Text = "-";
                    labelPatientID.Text = "-";
                    labelTriageStudy.Text = "-";
                    textStudyFreeNote.Text = "";

                    labelStudyNrEvents.Text = "-";
                    labelModelNr.Text = "";
                    mClearFullStrips();
                    textBoxStudyRemark.Text = "";
                    textBoxRecordDir.Text = "";
                    labelStudyPermissions.Text = "";
                    labelStudyClient.Text = "";
                    labelStudyTrial.Text = "";
                }
                //               labelRecNr.BackColor = recordBgColor;
                //labelEventText.BackColor = recordBgColor;
                //textBoxRecordDir.Visible = false;



                CSqlDBaseConnection db = CDvtmsData.sGetDBaseConnection();

                UInt32 dbLimit = db == null ? _mTriageRowsLimit : db.mGetSqlRowLimit();

                UInt32 nRowLimit = _mTriageRowsLimit == 0 ? dbLimit : _mTriageRowsLimit;
                UInt32 nDrawLimit = _mTriageDrawLimit == 0 ? nRowLimit : _mTriageDrawLimit;
                bool bLimitActive = checkBoxUseLimit.Checked;

                if (false == bLimitActive)
                {
                    nRowLimit = nDrawLimit = dbLimit;
                }
                string limitStr = nDrawLimit.ToString();
                Color indexColor = _mIndexBackColor;
                Color nrEventsColor = _mIndexBackColor; 

                if (nDrawLimit >= nRowLimit)
                {
                    nDrawLimit = nRowLimit;
                    limitStr += nRowLimit < dbLimit ? "<" : "/";
                }
                else
                {
                    limitStr += nRowLimit < dbLimit ? "«" : "|";
                }
                limitStr += nRowLimit.ToString();
                labelRowLimit.Text = limitStr;

                if ( AbShow )
                {
                    rowIndexText = rowIndex.ToString();

                    if (bLimitActive)
                    {
                        rowIndexText += rowIndex >= nDrawLimit ? " « " : " < ";
                    }
                    else
                    {
                        rowIndexText += rowIndex >= nDrawLimit ? " = " : " / ";
                    }
                    rowIndexText += AbShow ? nrRows.ToString() : "";
                    if (nrRows >= nRowLimit)
                    {
                        rowIndexText += nRowLimit >= dbLimit ? "!" : "~";
                        indexColor = Color.Orange;
                    }
                }
                else
                {
                    rowIndexText = ">Locked<";
                    indexColor = Color.LightBlue;

                }
                if (labelTriageRow.BackColor != indexColor) labelTriageRow.BackColor = indexColor;


                if( nrEvents >= dbLimit )
                {
                    nrEventsColor = Color.Orange;
                }
                if(labelStudyNrEvents.BackColor != nrEventsColor  ) labelStudyNrEvents.BackColor = nrEventsColor;

                double sec = (DateTime.Now - dt).TotalSeconds;
                labelUpdateTC.Text = "uStrip= " + sec.ToString("0.000") + "sec.";
                labelUpdTimeStrip.Text = "uS=" + sec.ToString("0.0") + "s";

                mUpdateStripInfoColor(recordBgColor);
                //                currentRow.Cells[DTriageGridField.PSA].
            }
            catch (Exception e2)
            {
                CProgram.sLogException("Failed  update strip", e2);
            }
            finally
            {
                dataGridTriage.UseWaitCursor = false;
                _mbUpdatingStrip = false;
                mShowBusy(false);
            }
            labelTriageRow.Text = rowIndexText;
        }

        private void mUpdateStripInfoColor(Color AColor)
        {
            if (panelStripInfo.BackColor != AColor) panelStripInfo.BackColor = AColor;
            if (textBoxStudyRemark.BackColor != AColor) textBoxStudyRemark.BackColor = AColor;
            if (textBoxRecordDir.BackColor != AColor) textBoxRecordDir.BackColor = AColor;

        }
        public void mUpdateTriageRowStrips(uint ARow, bool AbDirectLoad, bool AbLoadFull, UInt16 ASignalIndex, bool AbDirectLoadFull)
        {
            mTriageUpdateReset();
            // show pictures
            try
            {
                if (ARow < dataGridTriage.Rows.Count)
                {
                    DataGridViewRow index = dataGridTriage.Rows[(int)ARow];

                    if (index != null && Index.Index >= 0 && index.Cells.Count >= 12)
                    {
                        UInt32 indexKeyNr = 0;

                        string filePath = index.Cells[(int)DTriageGridField.FilePath].Value.ToString();
                        string fileName = index.Cells[(int)DTriageGridField.FileName].Value.ToString();
                        string indexKey = index.Cells[(int)DTriageGridField.Index].Value.ToString();
                        string nrSignals = index.Cells[(int)DTriageGridField.NrSignals].Value.ToString();

                        UInt16 nSignals = 1;

                        if (false == UInt16.TryParse(nrSignals, out nSignals))
                        {
                            nSignals = 1;
                        }
                        UInt16 iSignal = (UInt16)(ASignalIndex < nSignals ? ASignalIndex : (nSignals <= 1 ? 0 : nSignals - 1));

                        if (UInt32.TryParse(indexKey, out indexKeyNr) && indexKeyNr != 0)
                        {
                            if (index.Cells[(int)DTriageGridField.EventStrip].Value == null)
                            {
                                Image img = mLoadImage(AbDirectLoad, indexKeyNr, filePath, iSignal, fileName, false, (Int32)ARow);

                                if (img != null)
                                {
                                    index.Cells[(int)DTriageGridField.EventStrip].Value = img;
                                }
                            }
                            if (AbLoadFull)
                            {
                                //                               mUpdateTriageFullStripImages(AbDirectLoadFull, indexKeyNr, filePath, Path.GetFileNameWithoutExtension(fileName), AFullIndex, ANrChannels);
                            }
                        }
                    }
                }
            }
            catch (Exception e2)
            {
                CProgram.sLogException("Failed  update strip", e2);
            }
        }

        /*private void mTriageOpenEventViewer()
        {
            mTriageUpdateReset();
            try
            {
                string toFile = "";

                if (mbGetTriageCursorFilePath(out toFile))
                {
                    string ext = Path.GetExtension(toFile);
                    string exe = _mHeaViewExe;

                    if (ext == ".scp")
                    {
                        exe = _mScpViewExe;
                    }
                    else
                    {
                        toFile = Path.ChangeExtension(toFile, "dat");
                    }
                    if (File.Exists(exe))
                    {
                        ProcessStartInfo startInfo = new ProcessStartInfo();
                        startInfo.FileName = exe;
                        startInfo.Arguments = toFile;
                        Process.Start(startInfo);
                    }
                }
            }
            catch (Exception e2)
            {
                CProgram.sLogException("Failed toolstripopen", e2);
            }
        }
        */
        private void mTriageOpenEventDir( bool AbDvxStudyDir)
        {
            mTriageUpdateReset();
            // open explorer wit file at cursor
            try
            {
                string toFile = "";
                string toPath = "";

                if(AbDvxStudyDir)
                {
                    try
                    {
                        DataGridViewRow index = dataGridTriage.CurrentRow;

                        if (index != null && index.Cells.Count >= 12)
                        {
                            string remark = index.Cells[(int)DTriageGridField.Remark].Value.ToString();

                            UInt32 dvxStudyIX;

                            if (CDvxEvtRec.sbGetStudyDeviceFromRemark(out toPath, out dvxStudyIX, remark, ""))
                            {
                            }
                        }
                    }
                    catch (Exception e2)
                    {
                        CProgram.sLogException("Failed get cursor path", e2);
                    }

                }
                else if (mbGetTriageCursorFilePath(out toFile))
                {
                    toPath = Path.GetDirectoryName(toFile);
                }
                if ( toPath.Length > 0 )
                    { 
                    ProcessStartInfo startInfo = new ProcessStartInfo();
                    startInfo.FileName = @"explorer";
                    startInfo.Arguments = toPath;
                    Process.Start(startInfo);
                    //                    mbGetCursorUpdateRead("+");
                }
            }
            catch (Exception e2)
            {
                CProgram.sLogException("Failed open explorer", e2);
            }
        }

        bool mbGetCursorIndexState(out int ArIndexKey, out string ArState)
        {
            bool bOk = false;
            int indexKey = 0;
            string state = "";

            try
            {
                DataGridViewRow index = dataGridTriage.CurrentRow;

                if (index != null && index.Cells.Count >= 10)
                {
                    string indexString = index.Cells[(int)DTriageGridField.Index].Value.ToString();

                    state = index.Cells[(int)DTriageGridField.State].Value.ToString();

                    if (state.Length > 3) state = state.Remove(0, 3);

                    bOk = int.TryParse(indexString, out indexKey);

                    if (bOk)
                    {
                        if (indexKey == 0)
                        {
                            CProgram.sLogLine("Cursor has an invalid index key (0)");
                            bOk = false;
                        }
                    }
                }

            }
            catch (Exception e2)
            {
                bOk = false;
                CProgram.sLogException("Failed get cursor for index", e2);
            }
            ArIndexKey = indexKey;
            ArState = state;

            return bOk;
        }
        bool mbGetTriageCursorFilePath(out string AFilePath)
        {
            bool bOk = false;

            AFilePath = "";
            try
            {
                DataGridViewRow index = dataGridTriage.CurrentRow;

                if (index != null && index.Cells.Count >= 12)
                {
                    string filePath = index.Cells[(int)DTriageGridField.FilePath].Value.ToString();
                    string fileName = index.Cells[(int)DTriageGridField.FileName].Value.ToString();

                    if (filePath != null && filePath.Length > 0)
                    {
                        if (Directory.Exists(filePath))
                        {
                            AFilePath = mPathCombine /*Path.Combine*/(filePath, fileName);

                            bOk = true;
                        }
                        else
                        {
                            CProgram.sPromptError(true, "Recording folder missing", "" + filePath + " does not exist!");
                        }
                    }
                }

            }
            catch (Exception e2)
            {
                bOk = false;
                CProgram.sLogException("Failed get cursor path", e2);
            }
            return bOk;
        }

        bool mbGetTriageCursorIndexState(out int ArIndexKey, out string ArState)
        {
            bool bOk = false;
            int indexKey = 0;
            string state = "";

            try
            {
                DataGridViewRow index = dataGridTriage.CurrentRow;

                if (index != null && index.Cells.Count >= 10)
                {
                    string indexString = index.Cells[(int)DTriageGridField.Index].Value.ToString();
                    state = index.Cells[(int)DTriageGridField.State].Value.ToString();

                    if (state.Length > 3) state = state.Remove(0, 3);


                    bOk = int.TryParse(indexString, out indexKey);

                    if (bOk)
                    {
                        if (indexKey == 0)
                        {
                            CProgram.sLogLine("Cursor has an invalid index key (0)");
                            bOk = false;
                        }
                    }
                }

            }
            catch (Exception e2)
            {
                bOk = false;
                CProgram.sLogException("Failed get cursor for index", e2);
            }
            ArIndexKey = indexKey;
            ArState = state;

            return bOk;
        }

        void mUpdateTriageTime(DateTime ANow, bool AbAllowFirstLast)
        {
            int sec = 0;
            int min = 0;
            string s = _sTriageTimermode.ToString();
            //"--:--";

            if (_mLastTriageUpdateDT != null && _mLastTriageUpdateDT > DateTime.MinValue)
            {
                sec = (int)(ANow - _mLastTriageUpdateDT).TotalSeconds;
                min = sec / 60;
                sec %= 60;

                s = min.ToString() + ":" + sec.ToString("00");
            }
            if( timerTriage == null || timerTriage.Enabled == false )
            {
                s += "!";
            }
            if (_mTriageUpdSec >= 15)
            {
                if (_mLastTriageUpdateDT > _mLastTriageActivityDT)
                {
                    _mLastTriageActivityDT = _mLastTriageUpdateDT;
                }
                sec = (int)(ANow - _mLastTriageActivityDT).TotalSeconds;
                sec = _mTriageUpdSec - sec;
                if (sec < 15)
                {

                    if (sec <= 0)
                    {
                        s += "\nT- 0";
                        _mLastTriageActivityDT = ANow;
                        toolTriageUpdTime2.Text = "+";
                        mUpdateTriageScreen(false, AbAllowFirstLast);
                    }
                    else
                    {
                        s += "\nT-" + sec.ToString();
                    }
                }
            }
            toolTriageUpdTime2.Text = s;
        }
        public void mUpdateTriageScreen(bool AbForceDo, bool AbAllowFirstLast)
        {
            if (_mbUpdatingTriage)
            {
                return;
            }
            if (false == AbForceDo && false == _sbTriageDoUpdate)
            {
                if (dataGridTriage.Visible)
                {
                    dataGridTriage.Visible = false;
                    dataGridTriage.Rows.Clear();
                    labelUpdateTS.Text = ">L<";
                    mUpdateTriageStrips(false);
                }
                return;
            }
            try
            {
                bool bShow = CProgram.sbCheckProgUserActive(); // sbEnterProgUserArea(false);
                _mbUpdatingTriage = true;

                if (_sbTriageOnly)
                {
                    mEnforceTriageOnly(false);
                }

                labelUpdateTS.Text = ">UpdTS";
                labelUpdTimeTable.Text = "uT?";

                DateTime dt = DateTime.Now;
                string s = "";
                string indexKey = "";

                mCheckTriageViewMode(false);

                string offset = "";
                double sec = (int)((dt - DateTime.UtcNow).TotalSeconds);
                if (sec < 0) sec -= 5;
                else
                {
                    sec += 5; offset = "+";
                }
                int min = (int)(sec) / 60;
                int hour = min / 60;
                min %= 60;
                offset += hour.ToString("D2") + ":" + min.ToString("D2");

                if (bShow)
                {
                    if (false == dataGridTriage.Visible ) dataGridTriage.Visible = true;

                    // get current cursor index
                    DataGridViewRow row = dataGridTriage.CurrentRow;
                    if (row != null && row.Cells.Count > (int)DTriageGridField.Index)
                    {
                        indexKey = row.Cells[(int)DTriageGridField.Index].Value.ToString();
                    }
                    if (false == mbTriageReadSqlDbTable(indexKey, AbAllowFirstLast))
                    {
                        s += "!t!";
                    }

                    s += "uTable= ";
                    mUpdateTriageTime(dt, AbAllowFirstLast);
                    sec = (DateTime.Now - dt).TotalSeconds;
                    s += sec.ToString("0.000") + "sec.";
                    labelUpdateTS.Text = s;
                    if (AbForceDo)
                    {
//                        sSetTriageTimerMode(false, DTriageTimerMode.Run, "Run update");
                        _sbTriageTimerOn = true; // sv20191114
                        _sbTriageDoImageCash = true;
                    }
                }
                else
                {

                    dataGridTriage.Visible = false;
                    dataGridTriage.Rows.Clear();
                    labelUpdateTS.Text =  ">L<";
                    sec = (DateTime.Now - dt).TotalSeconds;
                }
                _mLastTriageUpdateDT = DateTime.Now;
                mTriageUpdateReset();
                labelUpdTimeTable.Text = "uT=" + sec.ToString("0.0") + "s";

                mUpdateTriageStrips(bShow);

                timerTriage.Enabled = _sbTriageTimerOn;

            }
            catch (Exception ex)
            {
                CProgram.sLogException("Triage update screen error", ex);
            }
            finally
            {
                _mbUpdatingTriage = false;
            }
        }
        public void mCheckTriageViewMode(bool AbUpdate)
        {
            DTriageViewMode mode = mTriageViewParseLabel(listBoxEventMode.Text);

            if (_mCurrentTriageView != mode)
            {
                mSetTriageView(mode, AbUpdate);
            }
        }

        public string mMakeTriageSearchString(CRecordMit ANewRec)
        {
            string whereStr = "";
            bool bTriage = true;

            mCheckTriageViewMode(true);


            if (_mCurrentTriageView < DTriageViewMode.NrViewModes && ANewRec != null)
            {
                string name = ANewRec.mVarGetName((UInt16)DRecordMitVars.RecState_IX);

                if (name != null && name.Length > 1)
                {

                    switch (_mCurrentTriageView)
                    {
                        case DTriageViewMode.Triage:
                            whereStr = name + "=" + ((int)(DRecState.Received)).ToString();
                            break;
                        case DTriageViewMode.Analyze:
                            whereStr = name + "=" + ((int)(DRecState.Triaged)).ToString();
                            break;
                        case DTriageViewMode.Report:
                            whereStr = name + "=" + ((int)(DRecState.Analyzed)).ToString();
                            bTriage = false;
                            break;
                        case DTriageViewMode.Seperator:
                            whereStr = name + "=" + ((int)(DRecState.Received)).ToString();
                            break;
                        case DTriageViewMode.ListAll:
                            whereStr = ""; // name + "=" + ((int)(DRecState.)).ToString();
                            bTriage = false;
                            break;
                        case DTriageViewMode.ListReceived:
                            whereStr = name + "=" + ((int)(DRecState.Received)).ToString();
                            break;
                        case DTriageViewMode.ListExclude:
                            whereStr = name + "=" + ((int)(DRecState.Excluded)).ToString();
                            break;
                        case DTriageViewMode.ListNoise:
                            whereStr = name + "=" + ((int)(DRecState.Noise)).ToString();
                            break;
                        case DTriageViewMode.ListLeadOff:
                            whereStr = name + "=" + ((int)(DRecState.LeadOff)).ToString();
                            break;
                        case DTriageViewMode.ListBaseLine:
                            whereStr = name + "=" + ((int)(DRecState.BaseLine)).ToString();
                            break;
                        case DTriageViewMode.ListTriaged:
                            whereStr = name + "=" + ((int)(DRecState.Triaged)).ToString();
                            break;
                        case DTriageViewMode.ListAnalyzed:
                            whereStr = name + "=" + ((int)(DRecState.Analyzed)).ToString();
                            bTriage = false;
                            break;
                        case DTriageViewMode.ListReported:
                            whereStr = name + "=" + ((int)(DRecState.Reported)).ToString();
                            bTriage = false;
                            break;
                        case DTriageViewMode.ListQCd:
                            whereStr = name + "=" + ((int)(DRecState.QCd)).ToString();
                            bTriage = false;
                            break;
                    }
                }
                dataGridTriage.Columns[(int)DTriageGridField.Triage].Visible = bTriage;
            }

            return whereStr;
        }


        public bool mbTriageReadSqlDbTable(string ACursorIndexKey, bool AbAllowFirstLast)
        {
            bool bOk = false;
            bool bCursor = false;
            CSqlCmd cmd = null;
            int nRows = -1;
            string cursorIndexKey = ACursorIndexKey;

            if (_mbUpdatingGrid == true)
            {
                return false;
            }
            try
            {
                int nrHours = 48;
                DateTime dt = DateTime.Now;

                _mbUpdatingGrid = true;
                mShowBusy(true);
                //int unit = listBoxViewUnit.SelectedIndex; werkt niet
                // string unitStr = listBoxViewUnit.SelectedItem != null ? listBoxViewUnit.SelectedItem.ToString() : "";
                int unit = listBoxViewUnit.TopIndex;

                if (int.TryParse(textBoxViewTime.Text, out nrHours) && nrHours > 0)
                {
                    if (unit == 1)
                    {
                        nrHours *= 24;  // days
                    }
                }
                else
                {
                    nrHours = unit == 1 ? 7 * 24 : 48;
                }
                if (mbCreateDBaseConnection())
                {
                    CRecordMit rec = new CRecordMit(_mDBaseServer);

                    CSqlDBaseConnection db = CDvtmsData.sGetDBaseConnection();

                    UInt32 dbLimit = db == null ? _mTriageRowsLimit : db.mGetSqlRowLimit();

                    UInt32 nRowLimit = _mTriageRowsLimit == 0 ? dbLimit : _mTriageRowsLimit;
                    UInt32 nDrawLimit = _mTriageDrawLimit == 0 ? nRowLimit : _mTriageDrawLimit;

                    if (false == checkBoxUseLimit.Checked)
                    {
                        nRowLimit = nDrawLimit = dbLimit;
                    }
                    string limitStr = nDrawLimit.ToString();

                    if (nDrawLimit >= nRowLimit)
                    {
                        nDrawLimit = nRowLimit;
                        limitStr += nRowLimit < dbLimit ? "<" : "/";
                    }
                    else
                    {
                        limitStr += nRowLimit < dbLimit ? "«" : "|";
                    }
                    limitStr += nRowLimit;
                    labelRowLimit.Text = limitStr;

                    if (_mTriageCountDays >= 0.01)
                    {
                        UInt32 nCountTotal = 0;
                        UInt32 nCountReceived = 0;
                        UInt32 nCountTriaged = 0;
                        UInt32 nCountAnalyzed = 0;
                        string info = mCountRows(out nCountTotal, out nCountReceived, out nCountTriaged, out nCountAnalyzed, _mTriageCountDays * 24);

                        labelCountedAll.Text = nCountTotal.ToString();
                        labelCountedReceived.Text = nCountReceived.ToString();
                        labelCountedTriaged.Text = nCountTriaged.ToString();
                        labelCountedAnalyzed.Text = nCountAnalyzed.ToString();
                    }
                    else
                    {
                        labelCountedAll.Text = "-";
                        labelCountedReceived.Text = "-";
                        labelCountedTriaged.Text = "-";
                        labelCountedAnalyzed.Text = "-";
                    }

                    if (rec == null)
                    {
                        CProgram.sLogLine("Failed to init cmd");
                    }
                    else
                    {   // using
                        cmd = rec.mCreateSqlCmd(_mDBaseServer);

                        if (cmd == null)
                        {
                            CProgram.sLogLine("Failed to init cmd");

                        }
                        else if (false == cmd.mbPrepareSelectCmd(true, rec.mMaskValid))
                        {
                            CProgram.sLogLine("Failed to prepare SQL row in table " + rec.mGetDbTableName());
                        }
                        else
                        {
                            DateTime utc = CProgram.sGetUtcNow();
                            string name = rec.mVarGetName((UInt16)DRecordMitVars.ReceivedUTC);
                            string cmp = rec.mSqlDateTimeCompareSqlString(name, utc, DSqlCompareDateTime.Hour, nrHours);
                            UInt64 mask = cmd.mGetCmdVarFlags();
                            string sqlWhere = mMakeTriageSearchString(rec);
                            double loadTime = _mImageCach != null ? _mImageCach.mGetMaxUpdateSec() : 4.0;
                            int nLoadDirect = 1;
                            int nDirect = 0;
                            bool bLoadDirect;
                            double dbTime, dbExec;
                            bool bError;
                            UInt32 studySearchIX = 0;
//                            UInt32 trialSearchIX = _sSelectedTrial > 0 && checkBoxTrial.Checked ? _sSelectedTrial : 0;
                            UInt32 clientSearchIX = _sSelectedClient > 0 && checkBoxClient.Checked ? _sSelectedClient : 0;
                            bool bTestTime = true;

                            List<CSqlDataTableRow> list = null;

                            int eventshowChannel = 1;

                            nRows = 0;

                            if (false == int.TryParse(comboBoxEventShowCh.Text, out eventshowChannel))
                            {
                                eventshowChannel = 1;
                            }
                            eventshowChannel = eventshowChannel < 1 ? 0 : eventshowChannel - 1;

                            UInt32.TryParse(textBoxSelectStudy.Text, out studySearchIX);

                            if (_mImageCach != null) _mImageCach.mSetDataGrid(null, 0, 0);

                            dataGridTriage.Rows.Clear();

                            if (checkBoxAllStudy.Checked)
                            {
                                if (studySearchIX > 0)
                                {
                                    bTestTime = false;
                                }
                                if (_sSelectedTrial > 0)
                                {
                                    bTestTime = false;
                                }
                            }
                            if (bTestTime)
                            {
                                cmd.mWhereAddString(cmp);
                            }

                            if (sqlWhere != null && sqlWhere.Length > 0)
                            {
                                cmd.mWhereAddString(sqlWhere);
                            }
                            if (checkBoxTrial.Checked)
                            {
                                string varName = rec.mVarGetName((UInt16)DRecordMitVars.Trial_IX);
                                string s = varName + "=" + _sSelectedTrial.ToString();
                                cmd.mWhereAddString(s);
                            }
                            if (clientSearchIX > 0)
                            {
                                string varName = rec.mVarGetName((UInt16)DRecordMitVars.Client_IX);
                                string s = varName + "=" + clientSearchIX.ToString();
                                cmd.mWhereAddString(s);
                            }
                            if ( checkBoxStudyPermissions.Checked && false == checkBoxAllStudy.Checked)
                            {
                                string s = "";
                                rec._mStudyPermissions = _sStudyPermissionsActive;
                                rec.mSqlWhereAndCmpVar(ref s, DSqlDataCmd.WhereEqual, (UInt16)DRecordMitVars.StudyPermissions, false);
                                cmd.mWhereAddString(s);
                            }
                            if (radioButtonNoStudy.Checked)
                            {
                                string varName = rec.mVarGetName((UInt16)DRecordMitVars.Study_IX);
                                string s = varName + "=0";
                                cmd.mWhereAddString(s);
                            }
                            else if (radioButtonCurrentDevice.Checked)
                            {
                                /* if (mCurrentStudyIX > 0)
                                 {
                                     string varName = rec.mVarGetName((UInt16)DRecordMitVars.Study_IX);
                                     string s = varName + "=" + mCurrentStudyIX.ToString();
                                     cmd.mWhereAddString(s);
                                 }

                                 else*/
                                if (_mCurrentDeviceSNR != null && _mCurrentDeviceSNR.Length > 0)
                                {
                                    string varName = rec.mVarGetName((UInt16)DRecordMitVars.DeviceID);
                                    string s = varName + "='" + _mCurrentDeviceSNR + "'";

                                    cmd.mWhereAddString(s);
                                }
                            }
                            else if (radioButtonCurrentPatID.Checked)
                            {
                                if (_mCurrentStudyIX > 0)
                                {
                                    string varName = rec.mVarGetName((UInt16)DRecordMitVars.Study_IX);
                                    string s = varName + "=" + _mCurrentStudyIX.ToString();
                                    cmd.mWhereAddString(s);
                                }

                                else if (_mCurrentPatID != null && _mCurrentPatID.Length > 0)
                                {
                                    string varName = rec.mVarGetName((UInt16)DRecordMitVars.PatientID_ES);
                                    if (rec.mPatientID != null)
                                    {
                                        rec.mPatientID.mbEncrypt(CProgram.sTrimString(_mCurrentPatID));
                                        string s = varName + "='" + rec.mPatientID.mGetEncrypted() + "'";

                                        cmd.mWhereAddString(s);
                                    }
                                }
                            }
                            else
                            {
                                // all 
                                string text;
                                if (studySearchIX > 0)
                                {
                                    string varName = rec.mVarGetName((UInt16)DRecordMitVars.Study_IX);
                                    string s = varName + "=" + studySearchIX.ToString();
                                    cmd.mWhereAddString(s);
                                }
                                else if (checkBoxHideS0.Checked)
                                {
                                    string varName = rec.mVarGetName((UInt16)DRecordMitVars.Study_IX);
                                    string s = varName + ">0";
                                    cmd.mWhereAddString(s);
                                }
                                text = textBoxSelectDevice.Text;
                                if (text != null)
                                {
                                    text = text.Trim();
                                    if (text != null && text.Length > 0)
                                    {
                                        string varName = rec.mVarGetName((UInt16)DRecordMitVars.DeviceID);
                                        string sqlText = _mDBaseServer.mSqlConvertToSqlString(text);
                                        string s = _mDBaseServer.mSqlStringContainsSqlString(varName, sqlText);
                                        cmd.mWhereAddString(s);
                                    }
                                }
                                text = textBoxSelectPatID.Text;
                                if (text != null)
                                {
                                    text = text.Trim();
                                    if (text != null && text.Length > 0)
                                    {
                                        string varName = rec.mVarGetName((UInt16)DRecordMitVars.PatientID_ES);
                                        string sqlText = _mDBaseServer.mSqlConvertToSqlString(text);
                                        string s = _mDBaseServer.mSqlStringContainsSqlString(varName, sqlText);
                                        cmd.mWhereAddString(s);
                                    }
                                }
                                text = textBoxSelectName.Text;
                                if (text != null)
                                {
                                    text = text.Trim();
                                    if (text != null && text.Length > 0)
                                    {
                                        string varName = rec.mVarGetName((UInt16)DRecordMitVars.PatientTotalName_ES);
                                        string sqlText = _mDBaseServer.mSqlConvertToSqlString(text);
                                        string s = _mDBaseServer.mSqlStringContainsSqlString(varName, sqlText);
                                        cmd.mWhereAddString(s);
                                    }
                                }

                                text = textBoxSelectType.Text;
                                if (text != null)
                                {
                                    text = text.Trim();
                                    if (text != null && text.Length > 0)
                                    {
                                        string varName = rec.mVarGetName((UInt16)DRecordMitVars.EventTypeString);
                                        string sqlText = _mDBaseServer.mSqlConvertToSqlString(text);
                                        string s = _mDBaseServer.mSqlStringContainsSqlString(varName, sqlText);
                                        cmd.mWhereAddString(s);
                                    }
                                }

                                text = textBoxSelectRem.Text;
                                if (text != null)
                                {
                                    text = text.Trim();
                                    if (text != null && text.Length > 0)
                                    {
                                        string varName = rec.mVarGetName((UInt16)DRecordMitVars.RecRemark_ES);
                                        string sqlText = _mDBaseServer.mSqlConvertToSqlString(text);
                                        string s = _mDBaseServer.mSqlStringContainsSqlString(varName, sqlText);
                                        cmd.mWhereAddString(s);
                                    }
                                }
                            }

                            string sortLabel = comboBoxTriageSort.Text;
                            if (sortLabel == "Record") cmd.mbSetOrderBy((UInt16)DSqlDataTableColum.Index_KEY, DSqlSort.Ascending);
                            else if (sortLabel == "Device snr") cmd.mbSetOrderBy((UInt16)DRecordMitVars.DeviceID, DSqlSort.Ascending);
                            else if (sortLabel == "Received Time") cmd.mbSetOrderBy((UInt16)DRecordMitVars.ReceivedUTC, DSqlSort.Ascending);
                            else if (sortLabel == "Event Time") cmd.mbSetOrderBy((UInt16)DRecordMitVars.EventUTC, DSqlSort.Ascending);
                            else if (sortLabel == "Event Type") cmd.mbSetOrderBy((UInt16)DRecordMitVars.EventTypeString, DSqlSort.Ascending);
                            else if (sortLabel == "Patient ID") cmd.mbSetOrderBy((UInt16)DRecordMitVars.PatientID_ES, DSqlSort.Ascending);
                            else if (sortLabel == "Patient Name") cmd.mbSetOrderBy((UInt16)DRecordMitVars.PatientTotalName_ES, DSqlSort.Ascending);

                            cmd.mSetLimit(nRowLimit, 0);

                            try
                            {
                                list = cmd.mExecuteSelectListCmd(out bError);
                            }
                            catch (Exception ex)
                            {
                                CProgram.sLogException("Failed collect Record list", ex);
                                list = null;
                                bError = true;
                                bOk = false;
                                nRows = 0;
                            }

                            cmd.mCloseCmd();

                            if (list != null)
                            {
                                int nList = list.Count;
                                int nLast = nList - 1;


                                bOk = true;
                                bCursor = true;
                                dataGridTriage.UseWaitCursor = true;

                                dbExec = (DateTime.Now - dt).TotalSeconds;
                                nRows = 0;
                                foreach (CRecordMit node in list)
                                {
                                    bLoadDirect = false; // n < nLoadDirect || (DateTime.Now - dt).TotalSeconds < loadTime;
                                    if (bLoadDirect) ++nDirect;
                                    //                                    rec.mbGetVarValues(out line, mask, ", ");
                                    if (AbAllowFirstLast)
                                    {
                                        if (_mbAutoMove2First && nRows == 0)
                                        {
                                            cursorIndexKey = node.mIndex_KEY.ToString();
                                        }
                                        else if( false == _mbAutoMove2First && _mbAutoMove2Last && nRows == nLast)
                                        {
                                            cursorIndexKey = node.mIndex_KEY.ToString();
                                        }
                                    }

                                    mbTriageStoreGrid(bLoadDirect, node, false, cursorIndexKey, (UInt16)eventshowChannel); // do not load images during filling of grid
                                    ++nRows;
                                    //CProgram.sLogLine(cmd.mGetNrRowsRead().ToString() + ", " + line);

                                }

                                bOk = false == bError;
                                dbTime = (DateTime.Now - dt).TotalSeconds;

                                ushort signalIndex = 0;
                                string strIndex = comboBoxTriageChannel.Text;
                                if (strIndex != null && strIndex.Length > 1) strIndex = strIndex.Substring(0, 1);

                                if (false == ushort.TryParse(strIndex, out signalIndex))
                                {
                                    signalIndex = 1;
                                }
                                if (signalIndex > 0) --signalIndex;     // program starts at 0, user at 1

                                //Data grid has been made, enable thread update enable
                                if (_mImageCach != null) _mImageCach.mSetDataGrid(dataGridTriage, (Int32)DTriageGridField.Index, (Int32)DTriageGridField.EventStrip);

                                signalIndex = (UInt16)eventshowChannel;
                                nDirect = 0;
                                if (_mbShowTriageImage)
                                {
                                    for (uint i = 0; i < nRows; ++i)
                                    {
                                        bLoadDirect = i < nLoadDirect || (DateTime.Now - dt).TotalSeconds < loadTime;
                                        if (bLoadDirect) ++nDirect;

                                        if (i < nDrawLimit)
                                        {
                                            mUpdateTriageRowStrips(i, bLoadDirect, i == 0, signalIndex, true);  // update grid with images
                                        }

                                        if (i == 0)
                                        {
                                            UInt16 nrThreads;
                                            if (_mImageCach != null) _mImageCach.mTryLoadImages(out nrThreads);
                                        }
                                    }
                                }
                                if (bOk == false)
                                {
                                    TimeSpan ts = DateTime.Now - dt;
                                    CProgram.sLogLine((bOk ? "" : "!Incomplete ") + "Updated " + nRows.ToString() + " table rows in " + ts.TotalSeconds.ToString("0.000")
                                                    + " sec. (nDirect= " + nDirect.ToString() + ", exec= " + dbExec.ToString("0.000") + ", db= " + dbTime.ToString("0.000") + " sec)");

                                }
                            }
                            else
                            {
                                CProgram.sLogLine("Failed to search SQL row in table " + rec.mGetDbTableName() + " {?=" + cmd.mGetWhereString() + "}");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed read sql table " + nRows.ToString(), ex);
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.mCloseCmd();
                }
                if (bCursor)
                {
                    dataGridTriage.UseWaitCursor = false;
                }
                _mbUpdatingGrid = false;
                mUpdateTriageStrips();
                mShowBusy(false);
            }
            return bOk;
        }

        public bool mbTriageUpdateRow(CRecordMit ARecordDb)
        {
            bool bOk = false;
            bool bHidePatient = checkBoxAnonymize.Checked;

            if (ARecordDb != null && ARecordDb.mIndex_KEY > 0 && ARecordDb.mRecState > 0)
            {
                try
                {
                    DataGridViewRow row;
                    string newState = ((DRecState)ARecordDb.mRecState).ToString();
                    string indexStr = ARecordDb.mIndex_KEY.ToString();

                    int n = dataGridTriage.Rows.Count;

                    for (int i = 0; i < n; ++i)
                    {
                        row = dataGridTriage.Rows[i];
                        if (row != null && row.Cells.Count > (int)DTriageGridField.Index)
                        {
                            if (row.Cells[(int)DTriageGridField.Index] != null)
                            {
                                string s = row.Cells[(int)DTriageGridField.Index].Value.ToString();

                                if (indexStr.CompareTo(s) == 0)
                                {
                                    UInt32 studyIX = ARecordDb.mStudy_IX;
                                    UInt16 countIndex = ARecordDb.mSeqNrInStudy;
                                    UInt16 countTotal = 0; // future info from study or record list
                                    UInt16 countToDo = 0;
                                    UInt16 countDone = 0;

                                    string psa = mGetStateString(ARecordDb);
//                                    string psa = mGetStateString(ARecordDb.mIndex_KEY, 2, ARecordDb.mRecState, (UInt16)(ARecordDb.mbActive ? 1 : 0), studyIX, 
  //                                      countIndex, countTotal, countToDo, countDone, ARecordDb.mNrSignals, ARecordDb.mRecDurationSec);
                                    string remark = mMakeRowRemark(ARecordDb);
                                    string patient = mMakeRowPatient(ARecordDb, bHidePatient);

                                    row.Cells[(int)DTriageGridField.PSA].Value = psa;

                                    row.Cells[(int)DTriageGridField.State].Value = ARecordDb.mRecState.ToString("D2") + "\n" + newState;
                                    row.Cells[(int)DTriageGridField.Study].Value = ARecordDb.mStudy_IX.ToString();

                                    row.Cells[(int)DTriageGridField.Patient].Value = patient;
                                    row.Cells[(int)DTriageGridField.Remark].Value = remark;
                                    mUpdateGridRowState(row);
                                    break;
                                }
                            }
                        }

                    }
                    bOk = true;
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Failed update Row", ex);
                }
            }
            mTriageUpdateReset();
            return bOk;
        }

        public bool mbTriageUpdateDbState(CRecordMit ARecordDb, UInt64 ASaveFlags)
        {
            bool bOk = false;
            CSqlCmd cmd = null;
            bool bHidePatient = checkBoxAnonymize.Checked;

            if (ARecordDb != null && ARecordDb.mIndex_KEY > 0 && ARecordDb.mRecState > 0)
            {
                try
                {
                    if (mbCreateDBaseConnection())
                    {
                        cmd = ARecordDb.mCreateSqlCmd(_mDBaseServer);
                        UInt64 mask = ASaveFlags | CRecordMit.sGetMask((UInt16)DRecordMitVars.RecState_IX);

                        if (cmd == null)
                        {
                            CProgram.sLogLine("Failed to init cmd");
                        }
                        else if (false == cmd.mbPrepareUpdateCmd(true, mask, true, true, 0))
                        {
                            CProgram.sLogLine("Failed to prepare update SQL row in table " + ARecordDb.mGetDbTableName());
                        }
                        else if (cmd.mbExecuteUpdateCmd())
                        {
                            mbTriageUpdateRow(ARecordDb);
                            bOk = true;
                            CProgram.sLogLine("Successfull updated  " + ARecordDb.mIndex_KEY.ToString() + " to " + ARecordDb.mRecState.ToString());
                        }
                        else
                        {
                            CProgram.sLogLine("Failed to update SQL row[" + ARecordDb.mIndex_KEY.ToString() + "] in table " + ARecordDb.mGetDbTableName());
                        }
                        cmd.mCloseCmd();
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Failed update Row", ex);
                }
                finally
                {
                    if (cmd != null)
                    {
                        cmd.mCloseCmd();
                    }
                }
            }
            mTriageUpdateReset();
            return bOk;
        }
        public void mTriageSetNewState(DRecState AState)
        {
            int index = 0;
            string oldState = "";
            CSqlCmd cmd = null;

            if (mbGetCursorIndexState(out index, out oldState))
            {
                CRecordMit rec = new CRecordMit(CDvtmsData.sGetDBaseConnection());
                string newState = CRecordMit.sGetStateUserString((UInt16)AState);

                if (rec != null)
                {
                    rec.mIndex_KEY = (UInt32)index;
                    rec.mRecState = (UInt16)AState;

                    try
                    {
                        if (mbCreateDBaseConnection())
                        {
                            cmd = rec.mCreateSqlCmd(_mDBaseServer);
                            UInt64 mask = CRecordMit.sGetMask((UInt16)DRecordMitVars.RecState_IX);

                            if (cmd == null)
                            {
                                CProgram.sLogLine("Failed to init cmd");
                            }
                            else if (false == cmd.mbPrepareUpdateCmd(true, mask, true, true, 0))
                            {
                                CProgram.sLogLine("Failed to prepare update SQL row in table " + rec.mGetDbTableName());
                            }
                            else if (cmd.mbExecuteUpdateCmd())
                            {
                                DataGridViewRow row = dataGridTriage.CurrentRow;

                                cmd.mCloseCmd();
                                if (row != null)
                                {
                                    int i = (int)(AState);
                                    row.Cells[(int)DTriageGridField.State].Value = i.ToString("D2") + "\n" + newState;
                                    mUpdateGridRowState(row);
                                }
                                CProgram.sLogLine("Successfull updated  " + index.ToString() + "  from " + oldState + " to " + newState);
                            }
                            else
                            {
                                CProgram.sLogLine("Failed to update SQL row[" + index.ToString() + "] in table " + rec.mGetDbTableName());
                            }
                            cmd.mCloseCmd();
                        }
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException("Failed update Row", ex);
                    }
                    finally
                    {
                        if (cmd != null)
                        {
                            cmd.mCloseCmd();
                        }
                    }
                }
            }
            else
            {
                CProgram.sLogLine("Select row in table first.");
            }
            mTriageUpdateReset();
        }


        private void toolStripDropDownButton1_Click(object sender, EventArgs e)
        {

        }

        private void contextMenuSettings_Opening(object sender, CancelEventArgs e)
        {

        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void toolStripUserOk_Click(object sender, EventArgs e)
        {
            string s = CProgram.sGetProgUserInitials();

            if (s == null || s.Length == 0)
            {
                // new user
                string initials = "";
                string pin = "";

                if (CProgram.sbReqLabel("Enter user initials ", "Initials", ref initials, "", false))
                {
                    if (initials != null && initials.Length > 0)
                    {
                        if (CProgram.sbReqLabel("Enter pin for " + initials, "Pin", ref pin, "", true)
                            && CDvtmsData.sbUserCheckPin( ref pin, "Select User " + initials ))
                        {
                            if (false == CDvtmsData.sbSetUserInitials(initials, pin))
                            {
                                CProgram.sAskOk("Select user", "Invalid user: " + initials);
                            }
                        }
                    }
                }
            }
            else
            {
                CProgram.sbEnterProgUserArea(true); // ask for pin if needed
            }
            mUpdateUserButtons();
            mUpdateTriageScreen(true, false);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Anayze_Click(object sender, EventArgs e)
        {
        }

        private void label1ProcedureDone24_Click(object sender, EventArgs e)
        {

        }

        private void comboBoxChannel_SelectedIndexChanged(object sender, EventArgs e)
        {
            mUpdateTriageStrips();
        }

        private void dataGridTriage_SelectionChanged(object sender, EventArgs e)
        {
            mUpdateTriageStrips();
        }

        private void buttonNoise_Click(object sender, EventArgs e)
        {
            mTriageSetNewState(DRecState.Noise);
        }

        private void buttonNormal_Click(object sender, EventArgs e)
        {
            mTriageSetNewState(DRecState.Excluded);
        }

        private void buttonLeadOff_Click(object sender, EventArgs e)
        {
            mTriageSetNewState(DRecState.LeadOff);
        }

        private void buttonAnalyze_Click(object sender, EventArgs e)
        {
            mTriageSetNewState(DRecState.Triaged);
        }

        private void buttonRead_Click(object sender, EventArgs e)
        {
            mTriageSetNewState(DRecState.Received);
        }

        private void buttonAnalyzed_Click(object sender, EventArgs e)
        {
            mTriageSetNewState(DRecState.Analyzed);
        }

        private void panel48_Click(object sender, EventArgs e)
        {
            //mUpdateTriageScreen();
        }

        private void mUpdateMemUse()
        {
            string memInfo = "?";
            bool bMemOk = CProgram.sbCheckMemSize(out memInfo, "UpdateMemUse", false);
            labelMemUsage.BackColor = bMemOk ? labelLoadImages.BackColor : Color.Orange;
            labelMemUsage.Text = memInfo;
            /*
                        UInt32 progMB = CProgram.sGetProgUsedMemoryMB();
                        UInt32 freeMB = CProgram.sGetFreeMemoryMB();
                        bool bProgMemError = progMB >= _mCheckMemUsedMB;
                        bool bFreeMemError = freeMB <= _mCheckMemFreeMB;

                        labelMemUsage.BackColor = bProgMemError || bFreeMemError ? Color.Orange : labelLoadImages.BackColor;
                        labelMemUsage.Text = (bProgMemError ? "p-": "P ") + progMB.ToString() 
                        + (bFreeMemError ? " f-" : " F ") + freeMB.ToString() 
                        + "MB";
                        */
        }
        private void timerTriage_Tick(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;

            if (_mbUpdatingTriageTimer)
            {
                if ((dt - _mLastActivityWriteDT).TotalSeconds > _cLastActivityWriteSec + 60)
                {
                    // triage timer hang up?
                    _mbUpdatingTriageTimer = false;
                }
            }
            else
            {
                try
                {
                    _mbUpdatingTriageTimer = true;

                    mUpdateTriageTime(dt, true);

                    if (_sbTriageDoMemInfo && (DateTime.Now - _mTimerUpdDT).TotalSeconds >= 5)
                    {
                        _mTimerUpdDT = DateTime.Now;
                        mUpdateUserButtons();
                        string txt = "";

                        if (_mImageCach != null)
                        {
                            UInt32 nSearch, nFailed;

                            UInt32 nMoved = _mImageCach.mCheckSearchImagesDone(dt, out nSearch, out nFailed);
                            UInt32 nLoad = _mImageCach.mGetNrOfLoadedImages();

                            //                            txt += nLoad == 0 ? "" : ((nLoad + 999) / 1000).ToString() + "kIm";
                            txt += nLoad == 0 ? "" : nLoad.ToString() + " Images";
                            txt += _mbShowTriageImage ? "● " : "◦ ";

                            /*                           if (nSearch > 0)
                                                       {
                                                           txt += " ? " + nSearch.ToString();
                                                       }
                                                       if( nFailed > 0)
                                                       {
                                                           txt += " !" + nFailed.ToString();
                                                       }
                           */
                        }
                        labelImageNk.Text = txt;
                        mUpdateMemUse();
                    }
                    if ((dt - _mLastActivityWriteDT).TotalSeconds > _cLastActivityWriteSec) // log activety file every 15 minutes
                    {
                        _mLastActivityWriteDT = dt;
                        CProgram.sWriteLastActiveFile(_mRecordingDir, true, CLicKeyDev.sGetLogLines());
                    }
                    if (_sbTriageDoTest)
                    {
                        if (_mCheckSystemMin > 0 && (dt - _mLastReaderScanCheckDT).TotalMinutes > _mCheckSystemMin) // log reader activety file every 10 minutes
                        {
                            _mLastReaderScanCheckDT = dt;
                            mCheckReaderScan(false, true, true, false);
                        }
                    }
                    string s = "ics=off";
                    if (_sbTriageDoImageCash && _mImageCach != null)
                    {
                        UInt16 nrThreads;
                        UInt32 n = _mImageCach.mTryLoadImages(out nrThreads);

                        s = /*n == 0 ? "" : */"ics= " + n.ToString();
                        //                        if (nrThreads > 0)
                        {
                            s += ", nt= " + nrThreads.ToString();
                        }
                    }
                    if (labelLoadImages.Text != s)
                    {
                        labelLoadImages.Text = s;
                    }
                }
                catch (Exception ex)
                {
                    // timer exception just continue
                }
                _mbUpdatingTriageTimer = false;
            }
        }
        string mGetLastUse()
        {
            Int32 nt = dataGridTriage == null || dataGridTriage.Rows == null ? 0 : dataGridTriage.Rows.Count;
            UInt32 nI = _mImageCach.mGetNrOfLoadedImages(); 
            return "nt=" + nt.ToString() + ", nI=" + nI.ToString();
        }
        bool mLogMemUse( string AAtText)
        {
            return CProgram.sbLogMemSize(AAtText + "(" + mGetLastUse() + ")", true);
        }
        private void mCheckReaderScan(bool AbShowOk, bool AbLogOk, bool AbCheckSystem, bool AbPromptUser)
        {
            string s = "";
            string tested = "DB";
            DateTime nowDT = DateTime.Now;
            bool bWarn = false;

            try
            {
                FileInfo fi = new FileInfo(Path.Combine(_mRecordingDir, "Sir.lastScan"));
                if (fi != null && fi.Exists)
                {
                    int min = (int)(DateTime.UtcNow - fi.LastWriteTimeUtc).TotalMinutes;

                    if (min > 30)
                    {
                        s += "ZSir>" + min.ToString() + "min ";
                        bWarn = true;
                    }
                    tested += ", ZSir";
                }
                fi = new FileInfo(Path.Combine(_mRecordingDir, "Tz.lastScan"));
                if (fi != null && fi.Exists)
                {
                    int min = (int)(DateTime.UtcNow - fi.LastWriteTimeUtc).TotalMinutes;

                    if (min > 30)
                    {
                        s += "Tz>" + min.ToString() + "min ";
                        bWarn = true;
                    }
                    tested += ", TZ";
                }
                fi = new FileInfo(Path.Combine(_mRecordingDir, "DV2.lastScan"));
                if (fi != null && fi.Exists)
                {
                    int min = (int)(DateTime.UtcNow - fi.LastWriteTimeUtc).TotalMinutes;

                    if (min > 30)
                    {
                        s += "DV2>" + min.ToString() + "min ";
                        bWarn = true;
                    }
                    tested += ", DV2";
                }
                fi = new FileInfo(Path.Combine(_mRecordingDir, "HSir.lastScan"));
                if (fi != null && fi.Exists)
                {
                    int min = (int)(DateTime.UtcNow - fi.LastWriteTimeUtc).TotalMinutes;

                    if (min > 30)
                    {
                        s += "HSir>" + min.ToString() + "min ";
                        bWarn = true;
                    }
                    tested += ", HSir";
                }
                fi = new FileInfo(Path.Combine(_mRecordingDir, "TzServer.lastScan"));
                if (fi != null && fi.Exists)
                {
                    int min = (int)(DateTime.UtcNow - fi.LastWriteTimeUtc).TotalMinutes;

                    if (min > 30)
                    {
                        s += "TzSvr>" + min.ToString() + "min ";
                        bWarn = true;
                    }
                    tested += ", TZsvr";
                }
                fi = new FileInfo(Path.Combine(_mRecordingDir, "SirServer.lastScan"));
                if (fi != null && fi.Exists)
                {
                    int min = (int)(DateTime.UtcNow - fi.LastWriteTimeUtc).TotalMinutes;

                    if (min > 30)
                    {
                        s += "SirSvr>" + min.ToString() + "min ";
                        bWarn = true;
                    }
                    tested += ", Sirsvr";
                }
            }
            catch (Exception)
            {
                s += "!ex!";
            }
            string stateDB = "!"; ;
            bool bFailedDB = false == mbTestDBaseConnection(out stateDB);

            bool bLog = s != null && s.Length > 0;
            string text = "";
            double sec = (DateTime.Now - nowDT).TotalSeconds;
            string strTime = sec.ToString("0.000") + "sec, ";
            bool bLogTime = sec > 3.0;

            if (bFailedDB)
            {
                CProgram.sLogWarning(strTime + "Check DB: " + stateDB);
                text += "DB: " + stateDB + " ";
                bWarn = true;

            }
            if (AbCheckSystem)
            {
                string resultDrive;

                if (false == CProgram.sbCheckFreeDriveSpace(out resultDrive, _mCheckDiskLocalGB, _mRecordingDir, _mCheckDiskDataGB, AbLogOk))
                {
                    text += " " + resultDrive;
                    bWarn = true;
                }
                else if (AbShowOk)
                {
                    text += " " + resultDrive;
                }
                string resultMem;
                string lastUse = mGetLastUse();

                if (false == CProgram.sbCheckMemSize(out resultMem, "cr("+ lastUse + ")", true, AbLogOk))
                {
                    text += " " + resultMem;
                    bWarn = true;

                }
                else if (AbShowOk)
                {
                    text += " " + resultMem;
                }
                string content;
                string url = _mCheckServerSironaUrl;
                bool bTestUrl = url != null && url.ToLower().StartsWith("http");

                if (false == bTestUrl && CLicKeyDev.sbDeviceIsProgrammer())
                {
                    url = "https://nl0022demo.dvtms.com:63180/hello";    // Sirona
                    bTestUrl = true;
                }
                if (bTestUrl)
                {
                    if (false == CProgram.sbTestHttpConnectFromUrl(out content, url))
                    {
                        text += " svrSir!";
                        bWarn = true;

                    }
                    else if (AbShowOk)
                    {
                        text += " +svrSir";
                    }
                    tested += ", svrSir";
                }
                url = _mCheckServerTzUrl;
                bTestUrl = url != null && url.ToLower().StartsWith("http");

                if (false == bTestUrl && CLicKeyDev.sbDeviceIsProgrammer())
                {
                    url = "http://nl0022demo.dvtms.com:63129/hello";    // tz
                    bTestUrl = true;
                }
                if (bTestUrl)
                {
                    if (false == CProgram.sbTestHttpConnectFromUrl(out content, url))
                    {
                        text += " svrTZ!";
                        bWarn = true;

                    }
                    else if (AbShowOk)
                    {
                        text += " +svrTZ";
                    }
                    tested += ", svrTZ";
                }
            }

            string dl = "";
            int daysLeft = CLicKeyDev.sGetDaysLeft();
            if (daysLeft <= 14)
            {
                dl = "dl= " + daysLeft.ToString() + ", ";
                //not effective                bWarn = true;
            }
            UInt32 nChachedImages = 0;

            if (_mImageCach != null)
            {
                nChachedImages = _mImageCach.mGetNrOfLoadedImages();
            }

            CProgram.sMemoryCleanup(false, true);
            double tTest = (DateTime.Now - nowDT).TotalSeconds;
            string timeTest = "";
            if (AbShowOk || tTest >= 4)
            {
                timeTest = " =" + tTest.ToString("0.0") + "s";
            }
            if (bLog)
            {
                CProgram.sLogWarning(strTime + "Check Reader: " + s + "  @" + _mCheckSystemMin.ToString() + "min, IC=" + nChachedImages.ToString());
                text += " Reader: " + s;
            }
            if (false == bFailedDB && false == bLog)
            {
                if (AbShowOk)
                {
                    text += " Tested ok: " + tested;
                }
                if (AbLogOk)
                {
                    CProgram.sLogLine("Check Reader & DB OK:" + tested + " in " + tTest.ToString("0.000")
                        + " sec @" + _mCheckSystemMin.ToString() + "min, IC=" + nChachedImages.ToString());
                }
            }

            string showText = (bLogTime ? dl + strTime + text : dl + text) + timeTest;
            if (AbPromptUser)
            {
                CProgram.sAskOk("Check Reader and system @" + _mCheckSystemMin.ToString() + "min, IC=" + nChachedImages.ToString(), showText);
            }
            toolStripButtonWarn.Text = bWarn ? "!" : ".";
            toolStripButtonWarn.ToolTipText = showText;
            toolStripReaderCheck.Text = bWarn || AbShowOk ? showText : "";
        }

        private void mCheckDbCollect()
        {
            string tested = "DB?";
            DateTime nowDT = DateTime.Now;

            try
            {
                string stateDB = "!"; ;

                if (mbTestDBaseConnection(out stateDB))
                {
                    tested = "DB " + stateDB;

                    CSqlDBaseConnection db = CDvtmsData.sGetDBaseConnection();
                    uint nLimit = db != null ? db.mGetSqlRowLimit() : 0;
                    CRecordMit searchRec = new CRecordMit(db);
                    List<CSqlDataTableRow> recList = null;

                    if (searchRec != null)
                    {
                        bool bError;
                        UInt64 searchMask = 0;
                        string receivedName = searchRec.mVarGetName((UInt16)DRecordMitVars.ReceivedUTC);
                        DateTime dt = DateTime.Now;
                        string whereStr = receivedName + "<" + searchRec.mSqlDateTimeSqlString(dt);

                        if (searchRec.mbDoSqlSelectList(out bError, out recList, searchRec.mMaskValid, searchMask, false, whereStr))
                        {
                            int n = recList != null ? n = recList.Count : 0;

                            tested += " collected " + n.ToString() + " records (limit " + nLimit.ToString() + ")";
                        }
                        else
                        {
                            tested += " search failed(limit " + nLimit.ToString() + ")";
                        }
                    }
                }
                else
                {
                    tested = "!DB! " + stateDB;
                }

                double sec = (DateTime.Now - nowDT).TotalSeconds;

                tested += " in " + sec.ToString("0.000") + "sec, ";

            }
            catch (Exception)
            {

            }
            toolStripReaderCheck.Text = tested;
            CProgram.sLogLine(tested);
        }

        private void panel48_Paint(object sender, PaintEventArgs e)
        {

        }

/*        private void buttonEventView_Click(object sender, EventArgs e)
        {
            mTriageOpenEventViewer();
        }
*/
        private void buttonEventFolder_Click(object sender, EventArgs e)
        {
            bool bShift = (Control.ModifierKeys & Keys.Shift) == Keys.Shift;

            mTriageOpenEventDir(bShift);
        }

        private void labelRecIndex_Click(object sender, EventArgs e)
        {

        }

        private void dataGridTriage_CellClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridTriage_MouseUp(object sender, MouseEventArgs e)
        {
            int x = e.X;
            int y = e.Y;

            DataGridView.HitTestInfo hitInfo = dataGridTriage.HitTest(x, y);

            if (hitInfo != null && CProgram.sbEnterProgUserArea(true))
            {
                int c = hitInfo.ColumnIndex;
                int r = hitInfo.RowIndex;
                if (r >= 0 && c == (int)DTriageGridField.Triage)
                {
                    int centerX = 82; // 79; // text = 63;
                    int centerY = 34; // 37; // text = 30;
                    int border = 2;
                    //int hight = 70;
                    int cX = x - hitInfo.ColumnX;
                    int rY = y - hitInfo.RowY;

                    int iX = 0, iY = 0;

                    if (cX > border)
                    {
                        if (cX < centerX - border) iX = 1;
                        else if (cX > centerX + border) iX = 2;
                    }
                    if (rY > border)
                    {
                        if (rY < centerY - border) iY = 1;
                        else if (rY > centerY + border && rY < Height - border) iY = 2;
                    }
                    if (iX > 0 && iY > 0)
                    {
                        DataGridViewRow row = dataGridTriage.Rows[r];

                        //CProgram.sLogLine("Triage[" + r.ToString() + "] hit at (" + cX.ToString() + ", " + rY.ToString() + ")");

                        if (dataGridTriage.CurrentRow != row)
                        {
                            dataGridTriage.CurrentCell = row.Cells[c];
                        }
                        if (iY == 1)
                        {
                            if (iX == 1) mTriageSetNewState(DRecState.Excluded); else mTriageSetNewState(DRecState.LeadOff);
                            //text                             if (iX == 1) mTriageSetNewState(DRecState.Noise); else mTriageSetNewState(DRecState.LeadOff);
                        }
                        else
                        {
                            if (iX == 1) mTriageSetNewState(DRecState.Noise);
                            else
                            {
                                if (e.Button == MouseButtons.Left)
                                {
                                    mTriageSetNewState(DRecState.Triaged);
                                }
                                else
                                if (e.Button == MouseButtons.Right)
                                {
                                    contextMenuQuickStat.Show(dataGridTriage, x, y);
                                }
                            }
                            //text                            if (iX == 1) mTriageSetNewState(DRecState.Normal); else mTriageSetNewState(DRecState.Triaged);
                        }
                        if (dataGridTriage.CurrentRow != row)
                        {
                            CProgram.sPromptError(true, "", "Current row is not correct");
                        }
                    }
                }
            }
        }

        private void buttonAnalyzed_Click_1(object sender, EventArgs e)
        {
            mTriageSetNewState(DRecState.Analyzed);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            mTriageSetNewState(DRecState.BaseLine);
        }

        private void panel22_Paint(object sender, PaintEventArgs e)
        {

        }

        private void buttonTriageFields_Click(object sender, EventArgs e)
        {
            _mbShowTriageFields = !_mbShowTriageFields;

            for (DTriageGridField i = DTriageGridField.Triage; ++i < DTriageGridField.NrGridFields;)
            {
                dataGridTriage.Columns[(int)i].Visible = _mbShowTriageFields;
            }
        }

        private void buttonViewFirst_Click(object sender, EventArgs e)
        {
            int n = dataGridTriage.Rows.Count;

            if (n > 0)
            {
                bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

                if( bCtrl)
                {
                    _mbAutoMove2First = !_mbAutoMove2First;
                    buttonSelectFirst.Text = _mbAutoMove2First ? "ë9" : "9";
                }

                dataGridTriage.CurrentCell = dataGridTriage.Rows[0].Cells[0];
                mUpdateTriageStrips();
            }
        }

        private void buttonViewLast_Click(object sender, EventArgs e)
        {
            int n = dataGridTriage.Rows.Count;

            if (n > 0)
            {
                bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

                if (bCtrl)
                {
                    _mbAutoMove2Last = !_mbAutoMove2Last;
                    buttonSelectLast.Text = _mbAutoMove2Last ? "ë:" : ":";
                }
                dataGridTriage.CurrentCell = dataGridTriage.Rows[n - 1].Cells[0];
                mUpdateTriageStrips();
            }
        }

        private DRecState mTriageViewGetRecState(DTriageViewMode ATriageView)
        {
            DRecState state = DRecState.Unknown;

            switch ((DTriageViewMode)ATriageView)
            {
                case DTriageViewMode.Triage:
                    state = DRecState.Received;
                    break;
                case DTriageViewMode.Analyze:
                    state = DRecState.Triaged;
                    break;
                case DTriageViewMode.Report:
                    state = DRecState.Analyzed;
                    break;
                case DTriageViewMode.Seperator:
                    state = DRecState.Unknown;
                    break;
                case DTriageViewMode.ListAll:
                    state = DRecState.Unknown;
                    break;
                case DTriageViewMode.ListReceived:
                    state = DRecState.Received;
                    break;
                case DTriageViewMode.ListExclude:
                    state = DRecState.Excluded;
                    break;
                case DTriageViewMode.ListNoise:
                    state = DRecState.Noise;
                    break;
                case DTriageViewMode.ListLeadOff:
                    state = DRecState.LeadOff;
                    break;
                case DTriageViewMode.ListBaseLine:
                    state = DRecState.BaseLine;
                    break;
                case DTriageViewMode.ListTriaged:
                    state = DRecState.Triaged;
                    break;
                case DTriageViewMode.ListAnalyzed:
                    state = DRecState.Analyzed;
                    break;
                case DTriageViewMode.ListReported:
                    state = DRecState.Reported;
                    break;
                case DTriageViewMode.ListQCd:
                    state = DRecState.QCd;
                    break;
            }
            return state;
        }
        private string mTriageViewGetLabel(DTriageViewMode ATriageView)
        {
            string label = "TVM?";

            switch ((DTriageViewMode)ATriageView)
            {
                case DTriageViewMode.Triage:
                    label = "Triage";
                    break;
                case DTriageViewMode.Analyze:
                    label = "Analyze";
                    break;
                case DTriageViewMode.Report:
                    label = "Report";
                    break;
                case DTriageViewMode.Seperator:
                    label = "------------";
                    break;
                case DTriageViewMode.ListAll:
                    label = "List All";
                    break;
                case DTriageViewMode.ListReceived:
                    label = "List Received";
                    break;
                case DTriageViewMode.ListExclude:
                    label = "List Exclude";
                    break;
                case DTriageViewMode.ListNoise:
                    label = "List Noise";
                    break;
                case DTriageViewMode.ListLeadOff:
                    label = "List LeadOff";
                    break;
                case DTriageViewMode.ListBaseLine:
                    label = "List BaseLine";
                    break;
                case DTriageViewMode.ListTriaged:
                    label = "List Triaged";
                    break;
                case DTriageViewMode.ListAnalyzed:
                    label = "List Analyzed";
                    break;
                case DTriageViewMode.ListReported:
                    label = "List Reported";
                    break;
                case DTriageViewMode.ListQCd:
                    label = "List QC'd";
                    break;
            }
            return label;
        }

        private DTriageViewMode mTriageViewParseLabel(string ALabel)
        {
            DTriageViewMode state = DTriageViewMode.NrViewModes;
            string label;

            for (DTriageViewMode i = DTriageViewMode.Triage; i < DTriageViewMode.NrViewModes; ++i)
            {
                label = mTriageViewGetLabel(i);

                if (label != null && label == ALabel)
                {
                    state = i;
                    break;
                }
            }
            return state;
        }

        private Color mTriageViewGetColor(DTriageViewMode ATriageView)
        {
            DRecState state = mTriageViewGetRecState(ATriageView);
            Color color = state == DRecState.Unknown ? Color.White : CRecordMit.sGetRecStateBgColor((UInt16)state);

            return color;
        }

        private Button mTriageViewGetButton(DTriageViewMode ATriageView)
        {
            Button button = null;

            switch (ATriageView)
            {
                case DTriageViewMode.Triage:
                    button = buttonShowTriage;
                    break;
                case DTriageViewMode.Analyze:
                    button = buttonShowAnalyze;
                    break;
                case DTriageViewMode.Report:
                    button = buttonShowReport;
                    break;
                case DTriageViewMode.Seperator:
                    //			button = buttonShow;
                    break;
                case DTriageViewMode.ListAll:
                    button = buttonShowAll;
                    break;
                case DTriageViewMode.ListReceived:
                    button = buttonShowReceived;
                    break;
                case DTriageViewMode.ListExclude:
                    button = buttonShowExclude;
                    break;
                case DTriageViewMode.ListNoise:
                    button = buttonShowNoise;
                    break;
                case DTriageViewMode.ListLeadOff:
                    button = buttonShowLeadOff;
                    break;
                case DTriageViewMode.ListBaseLine:
                    button = buttonShowBaseLine;
                    break;
                case DTriageViewMode.ListTriaged:
                    button = buttonShowTriaged;
                    break;
                case DTriageViewMode.ListAnalyzed:
                    button = buttonShowAnalyzed;
                    break;
                case DTriageViewMode.ListReported:
                    button = buttonShowReported;
                    break;
                case DTriageViewMode.ListQCd:
                    button = buttonShowQCd;
                    break;
            }
            return button;
        }

        private void mTriageViewCursorButton(DTriageViewMode ATriageView, bool AbActive)
        {
            Button button = mTriageViewGetButton(ATriageView);

            if (button != null)
            {
                //                button.Font.Bold = AbActive;
                button.FlatAppearance.BorderSize = AbActive ? 3 : 1;

                if (AbActive)
                {
                    button.Focus();
                }
            }

        }

        private void mTriageViewInitButton(DTriageViewMode ATriageView, bool AbActive)
        {
            Button button = mTriageViewGetButton(ATriageView);

            if (button != null)
            {
                Color color = mTriageViewGetColor(ATriageView);

                if (button == buttonShowAll)
                {
                    color = Color.Yellow;
                }
                button.BackColor = color;
                //                button.Font.Bold = AbActive;
                button.FlatAppearance.BorderSize = AbActive ? 3 : 1;
            }
        }

        private void mTriageViewInitAllButtons()
        {
            string label;

            listBoxEventMode.Items.Clear();

            for (DTriageViewMode i = DTriageViewMode.Triage; i < DTriageViewMode.NrViewModes; ++i)
            {
                mTriageViewInitButton(i, false);
                label = mTriageViewGetLabel(i);
                if (label != null && label.Length > 0)
                {
                    listBoxEventMode.Items.Add(label);
                }
            }
            labelCountedAll.BackColor = buttonShowAll.BackColor;
            labelCountedReceived.BackColor = buttonShowReceived.BackColor;
            labelCountedTriaged.BackColor = buttonShowTriaged.BackColor;
            labelCountedTriaged.BackColor = buttonShowTriaged.BackColor;
        }

        private void mSetTriageView(DTriageViewMode ATriageView, bool AbUpdate)
        {
            if (_mCurrentTriageView != ATriageView && ATriageView < DTriageViewMode.NrViewModes)
            {
                mTriageViewCursorButton(_mCurrentTriageView, false);

                Color color = mTriageViewGetColor(ATriageView);
                string label = mTriageViewGetLabel(ATriageView);
                listBoxEventMode.Text = label;
                //listBoxEventMode.SelectedIndex = (int)ATriageView;
                panelViewColorStrip.BackColor = color;
                _mCurrentTriageView = ATriageView;

                mTriageViewCursorButton(_mCurrentTriageView, true);

                string viewModeStr1 = "Event";
                string viewModeStr2 = mTriageViewGetLabel(_mCurrentTriageView);

                labelViewMode1.Text = viewModeStr1 + ": " + viewModeStr2;
            }
            if (AbUpdate)
            {
                mUpdateTriageScreen(true, true);
            }
        }


        private void button10_Click(object sender, EventArgs e)
        {
            listBoxEventMode.SelectedIndex = (int)DTriageViewMode.Triage;
            mUpdateTriageScreen(true, true);
        }

        private void button11_Click(object sender, EventArgs e)
        {
            listBoxEventMode.SelectedIndex = (int)DTriageViewMode.Analyze;
            mUpdateTriageScreen(true, true);

        }

        private void button12_Click(object sender, EventArgs e)
        {
            listBoxEventMode.SelectedIndex = (int)DTriageViewMode.Report;
            mUpdateTriageScreen(true, true);
        }

        private void buttonRead_Click_1(object sender, EventArgs e)
        {
            mTriageSetNewState(DRecState.Reported);
        }

        private void panel63_Paint(object sender, PaintEventArgs e)
        {
        }

        private void panelViewInc_Click(object sender, EventArgs e)
        {
            int nrHours = 48;
            int unit = listBoxViewUnit.TopIndex;

            if (int.TryParse(textBoxViewTime.Text, out nrHours) && nrHours > 0)
            {
                if (unit == 1)
                {
                    nrHours += 1;  // days
                }
                else
                {
                    nrHours += 4;
                }
                textBoxViewTime.Text = nrHours.ToString();
            }
        }

        private void panelViewDec_Click(object sender, EventArgs e)
        {
            int nrHours = 48;
            int unit = listBoxViewUnit.TopIndex;

            if (int.TryParse(textBoxViewTime.Text, out nrHours) && nrHours > 0)
            {
                if (unit == 1)
                {
                    nrHours -= 1;  // days
                }
                else
                {
                    nrHours -= 4;
                }
                if (nrHours < 1)
                {
                    nrHours = 1;
                }
                textBoxViewTime.Text = nrHours.ToString();
            }

        }

        private void buttonTriageReported_Click(object sender, EventArgs e)
        {
            mTriageSetNewState(DRecState.Reported);
        }

        private void buttonTestLoadImage_Click(object sender, EventArgs e)
        {
            if (_mImageCach != null)
            {
                UInt16 nrThreads;
                _mImageCach.mTryLoadImages(out nrThreads);
            }
        }

        private void mWindowSizeChanged()
        {
            int newWidth = dataGridTriage.Size.Width - 40;

            if (dataGridTriage.Columns.Count >= (int)DTriageGridField.Triage)
            {
                for (int i = 0; i <= (int)DTriageGridField.Triage; ++i)
                {
                    if (i != (int)DTriageGridField.EventStrip)
                    {
                        if (dataGridTriage.Columns[i].Visible)
                        {
                            newWidth -= dataGridTriage.Columns[i].Width;
                        }
                    }
                }
                if (newWidth < 100) newWidth = 100;

                dataGridTriage.Columns[(int)DTriageGridField.EventStrip].Width = newWidth;
            }
        }
        private void FormEventBoard_SizeChanged(object sender, EventArgs e)
        {
            mWindowSizeChanged();

            /*                int deltaWidth = Size.Width - mWidthScreenNormal;
                        int width = dataGridTriage.Columns[(int)DTriageGridField.EventStrip].Width;
                        int newWidth = width + deltaWidth;

                        if (newWidth > 100)
                        {
                            dataGridTriage.Columns[(int)DTriageGridField.EventStrip].Width = newWidth;
                            mWidthScreenNormal = Size.Width;

                        }
            */
        }

        private void dataGridTriage_CellMouseMove(object sender, DataGridViewCellMouseEventArgs e)
        {
            /*           int x = e.X;
                       int y = e.Y;


                       DataGridView.HitTestInfo hitInfo = dataGridTriage.HitTest(x, y);
           */
            //if (hitInfo != null)
            {
                Cursor oldCursor = dataGridTriage.Cursor;
                Cursor newCursor = Cursors.Default;
                int c = e.ColumnIndex; // hitInfo.ColumnIndex;
                int r = e.RowIndex; // hitInfo.RowIndex;
                if (r >= 0)
                {
                    if (c == (int)DTriageGridField.Triage)
                    {
                        newCursor = Cursors.Hand;
                    }
                }
                if (newCursor != oldCursor)
                {
                    dataGridTriage.Cursor = newCursor;
                }
            }
        }

        private void toolStripEvents_Click(object sender, EventArgs e)
        {

        }

        private void mCreateStudy(ref CRecordMit ArRecordDb, ref CDeviceInfo ArDevice, ref CStudyInfo ArStudy, ref CPatientInfo ArPatient)
        {
            if (ArRecordDb != null && ArDevice != null && ArDevice.mIndex_KEY > 0 && ArStudy == null || ArStudy.mIndex_KEY == 0)
            {
                try
                {

                    if (ArStudy == null) ArStudy = new CStudyInfo(CDvtmsData.sGetDBaseConnection());
                    if (ArPatient == null) ArPatient = new CPatientInfo(CDvtmsData.sGetDBaseConnection());

                    if (ArStudy != null && ArPatient != null)
                    {
                        ArStudy._mStudyPhysicianInstruction = "recorder ID= " + ArRecordDb.mDeviceID
          + "\r\nfull name= " + ArRecordDb.mPatientTotalName.mDecrypt()
          + "\r\nPh name= " + ArRecordDb.mPhysicianName.mDecrypt()
           + "\r\n" + ArDevice.mGetAddStudyModeChar()+"Ref= " + ArRecordDb.mPatientID.mDecrypt()
          + "\r\nstrip= " + CProgram.sUtcToLocalString(ArRecordDb.mBaseUTC)
          + "\r\nevent= " + CProgram.sUtcToLocalString(ArRecordDb.mEventUTC)
      + "\r\nAfter create study go to STAT screen to attach study!";

                        DateTime dtEvent = ArRecordDb.mGetEventUTC();
                        DateTime dtStart = ArRecordDb.mBaseUTC;
                        if (dtStart == DateTime.MinValue) dtStart = dtEvent;

                        ArStudy._mStudyStartDate = CProgram.sUtcToLocal(dtStart.AddHours(dtStart.Hour >= 2 ? -1 : 0));

                        if (ArDevice != null && ArDevice.mIndex_KEY > 0)
                        {
                            ArStudy._mStudyRecorder_IX = ArDevice.mIndex_KEY;
                        }

                        ArPatient.mbSetPatientIDValue(ArRecordDb.mPatientID);
                        ArPatient._mSocSecNr.mbCopyFrom(ArRecordDb.mPatientID);

                        if (ArRecordDb.mPatientTotalName.mbNotEmpty())
                       {
                            string firstName, middleName, lastName;

                            ArRecordDb.mGetPatientName(out firstName, out middleName, out lastName);

                            ArPatient._mPatientLastName.mbEncrypt(lastName);
                            ArPatient._mPatientMiddleName.mbEncrypt(middleName);
                            ArPatient._mPatientFirstName.mbEncrypt(firstName);
                        }
                        ArPatient._mPatientDateOfBirth.mbCopyFrom(ArRecordDb.mPatientBirthDate);

                        ArPatient._mPatientGender_IX = (UInt16)ArRecordDb.mPatientGender;


                        CStudyPatientForm form = new CStudyPatientForm(ArStudy, ArPatient, false, true, true, true);

                        if (form != null && form.mbInitFinished)    // veryfied initialyzed before using loaded lists)
                        {
                            // form creates client, physician, device list

                            if (form._mClientList != null && ArDevice != null )
                            {
                                if( ArDevice._mClient_IX > 0 )
                                {
                                    CClientInfo client = form._mClient.mGetNodeFromList(form._mClientList, ArDevice._mClient_IX) as CClientInfo;

                                    if (client != null)
                                    {
                                        ArStudy._mClient_IX = client.mIndex_KEY;
                                        form._mClient.mbCopyFrom(client);
                                        form.mSetClient(client._mLabel);
                                    }
                                }
                                else if ( ArDevice._mRemarkLabel != null && ArDevice._mRemarkLabel.Length > 0)
                                    {
                                    CClientInfo client = form._mClient.mGetNodeFromList(form._mClientList, (UInt16)DClientInfoVars.Label, ArDevice._mRemarkLabel) as CClientInfo;

                                    if (client != null)
                                    {
                                        ArStudy._mClient_IX = client.mIndex_KEY;
                                        form._mClient.mbCopyFrom(client);
                                        form.mSetClient(client._mLabel);
                                    }
                                }
                            }

                            if (form._mPhysicianList != null && ArRecordDb.mPhysicianName.mbNotEmpty())
                            {
                                string name = ArRecordDb.mPhysicianName.mDecrypt();
                                CPhysicianInfo physician = form._mPhysician.mGetNodeFromList(form._mPhysicianList, (UInt16)DPhysicianInfoVars.Label, name) as CPhysicianInfo;

                                if (physician != null)
                                {
                                    ArStudy._mPhysisian_IX = physician.mIndex_KEY;
                                    form.mSetClient(name);
                                }
                            }

                            form.mSetRefIdOther(ArPatient.mGetPatientSocIDValue(false));//_mPatientID.mDecrypt());

                            if (DialogResult.OK == form.ShowDialog())
                            {
                                if (form._mStudyInfo != null && form._mStudyInfo.mIndex_KEY > 0)
                                {
                                    ArStudy = form._mStudyInfo;
                                    ArPatient = form._mPatientInfo;
                                    ArDevice = form._mActiveDevice;

                                    CStudyInfo study2;
                                    CPatientInfo patient2;

                                    mbAddRecordToStudyDo(ArRecordDb, ArDevice, "create", ArDevice._mDeviceSerialNr, ArStudy.mIndex_KEY, out study2, out patient2);
                           // !        ArDevice.mbLoadCheckFromRecord(true, ArRecordDb, false, ref ArStudy, ref ArPatient, true, false); // this should update the record

                                    string error = "";

                                    if (ArRecordDb.mStudy_IX == 0) error += "Study not Set, ";
                                    if (ArRecordDb.mSeqNrInStudy == 0) error += "Not added to Study, ";
                                    if (ArRecordDb.mPatient_IX == 0) error += "Patient not Set, ";
                                    if (error.Length > 0) CProgram.sPromptError(false, "Create Study for event #" + ArRecordDb.mIndex_KEY.ToString(),
                                                                   "Failed create, " + error);
                                }
                                // needs more code to set record and device

                            }
                        }

                    }

                }
                catch (Exception ex)
                {
                    CProgram.sLogException("failed create study for R" + ArRecordDb.mIndex_KEY.ToString(), ex);
                }
            }
        }

        private bool mbCheckCreateDeviceStudy(ref CRecordMit ArRecordDb, ref CDeviceInfo ArDevice, ref CStudyInfo ArStudy, ref CPatientInfo ArPatient)
        {

            bool bContinue = ArRecordDb != null;
            bool bCreate = true;

            if (bContinue)
            {
                if (ArDevice == null || ArDevice.mIndex_KEY == 0)
                {
                    string deviceStr = ArRecordDb.mDeviceID;

                    if (deviceStr == null || deviceStr.Length == 0)
                    {
                        bCreate = false;
                        bContinue = CProgram.sbAskOkCancel("Record R"+ ArRecordDb.mIndex_KEY, "No device, Continue?");
                    }
                    else
                    {
                        bContinue = CProgram.sbReqBool("Unknown Devive " + deviceStr, "Create device " + deviceStr, ref bCreate);
                    }
                    if (bContinue && bCreate)
                    {
                        CDeviceForm form = new CDeviceForm(deviceStr, true);

                        if (form != null)
                        {
                            form.ShowDialog();
                            mbTriageUpdateDbState(ArRecordDb, 0);
                        }
                        bContinue = false;
                    }
                }
                else if (ArStudy == null || ArStudy.mIndex_KEY == 0)
                {
                    string deviceStr = ArRecordDb.mDeviceID;

                    if (ArDevice._mState_IX != (UInt16)DDeviceState.Available)
                    {

                        string s = "Manage device " + deviceStr + ", check { state=" + CDeviceInfo.sGetStateString(ArDevice._mState_IX);

                        if (ArDevice._mActiveStudy_IX == 0)
                        {
                            s += ", No study set";
                        }
                        else
                        {
                            if (ArDevice._mActiveRefID.mbNotEmpty())
                            {
                                bool bSame = ArDevice.mbSameRefID(ArRecordDb.mPatientID);

                                s += bSame ? ", Event RefID ok" : ", Event RefID not equal";
                            }
                            bool bInRange = ArDevice.mbActiveTime(ArRecordDb.mEventUTC);
                            s += bInRange ? ", Event In Time Window" : ", Event Outside time window";
                        }

                        s += " }";

                        bContinue = CProgram.sbReqBool("Devive " + deviceStr + " is not available", s, ref bCreate);

                        if (bContinue && bCreate)
                        {
                            CDeviceForm form = new CDeviceForm(deviceStr, true);

                            if (form != null)
                            {
                                form.ShowDialog();
                                //                                mbTriageUpdateDbState(ArRecordDb, 0);
                            }
                            bContinue = false;
                        }
                    }
                    else
                    {
                        string title = "Unknown Study for device" + deviceStr;
                        string question = "Create study first from Event record";

                        if( ArDevice._mAddStudyMode != (UInt16)DAddStudyModeEnum.ActiveStudy)
                        {
                            title += " (add method = " + ArDevice.mGetAddStudyModeLabel(false) + ArDevice.mGetAddStudyModeMctLabel() + ")";
                            if(ArDevice._mAddStudyMode == (UInt16)DAddStudyModeEnum.Manual)
                            {
                                question += " (use cancel and add manual!)";
                                bCreate = false;
                            }
                        }

                        bContinue = CProgram.sbReqBool(title, question, ref bCreate);

                        if (bContinue && bCreate)
                        {
                            mCreateStudy(ref ArRecordDb, ref ArDevice, ref ArStudy, ref ArPatient);

                            // needs update record in triage list.
                            mUpdateCurrentGridRow(ArRecordDb, "Created Study " + ArRecordDb.mStudy_IX.ToString());

                            // mbTriageUpdateDbState(ArRecordDb, 0);
                            bContinue = false;
                        }
                    }
                }
            }
            return bContinue;
        }
        public void mStartAnalyze()
        {
            bool bReadOnly = _sbTriageOnly;

            if (false == mbCheckDaysLeft())
            {
                return;
            }
            /*if (_sbTriageOnly)
            {
                return;
            }
*/
            DateTime dt = DateTime.Now;
            DateTime startDT = dt;
            Double totalSec = 0.0;
            Double dbSec = 0.0;
            Double createSec = 0.0;
            Double stateSec = 0.0;
            Double loadSec = 0.0;
            Double devStdPtSec = 0.0;
            Double screenSec = 0.0;
            Double showSec = 0.0;
            Double stripSec = 0.0;

            UInt32 recordIndex = 0;
            mLogMemUse("Open Analyze");
            if (mbCreateDBaseConnection())
            {
                dbSec = (DateTime.Now - dt).TotalSeconds;
                dt = DateTime.Now;

                FormAnalyze formAnalyze = new FormAnalyze(this);

                if (formAnalyze != null)
                {
                    createSec = (DateTime.Now - dt).TotalSeconds;
                    dt = DateTime.Now;

                    mSetTzFilters();    // enable the TZ quick filters at load

                    try
                    {
                        bool bNew = true;
                        UInt32 analysisIndex = 0;
                        UInt16 analysisNr = 0;

                        int index = 0;
                        string oldState = "";

                        if (mbGetCursorIndexState(out index, out oldState))
                        {
                            recordIndex = (uint)index;
                            mUpdateStripInfoColor(Color.White);
                            stateSec = (DateTime.Now - dt).TotalSeconds;
                            dt = DateTime.Now;

                            if (formAnalyze.mbLoadRecord2(bReadOnly, recordIndex, bNew, analysisIndex, analysisNr, _mDBaseServer, _mRecordingDir))
                            {
                                loadSec = (DateTime.Now - dt).TotalSeconds;
                                dt = DateTime.Now;
                                bool bUpdateCurrent = formAnalyze._mRecordDb.mStudy_IX == 0 && mbTestCurrent();

                                bool b = mbCheckCreateDeviceStudy(ref formAnalyze._mRecordDb, ref formAnalyze._mDeviceInfo, ref formAnalyze._mStudyInfo, ref formAnalyze._mPatientInfo);

                                devStdPtSec = (DateTime.Now - dt).TotalSeconds;
                                dt = DateTime.Now;
                                if (b)
                                {
                                    _mCurrentStudyIX = formAnalyze._mRecordDb.mStudy_IX;
                                    formAnalyze.mInitScreens();

                                    screenSec = (DateTime.Now - dt).TotalSeconds;
                                    dt = DateTime.Now;
                                    if (formAnalyze != null)
                                    {
                                        try
                                        {
                                            formAnalyze.Show();
                                            // Show will return directly do not update or turn  cursor off
                                            UInt32 nSamples = 0;
                                            float stripLengthSec = 0.0F;
                                            if( formAnalyze._mRecordFile != null )
                                            {
                                                nSamples = formAnalyze._mRecordFile.mGetNrSamples();
                                                stripLengthSec = formAnalyze._mRecordFile.mGetSamplesTotalTime();
                                            }
                                            CProgram.sLogLine( "Show Analyze R#" + formAnalyze._mRecordDb.mIndex_KEY.ToString() + " S"+ formAnalyze._mRecordDb.mStudy_IX.ToString()
                                                + "." + formAnalyze._mRecordDb.mSeqNrInStudy.ToString() + "("+ nSamples.ToString() + ", " + stripLengthSec.ToString("0.0" ) + " sec)");
                                            showSec = (DateTime.Now - dt).TotalSeconds;
                                            dt = DateTime.Now;
                                            mbTriageUpdateRow(formAnalyze._mRecordDb);
                                            mUpdateTriageStrips();
                                            if (_mbTriageLockAnRec)
                                            {
                                                if (formAnalyze.mbIsLocked())
                                                {
                                                    mUpdateStripInfoColor(Color.Yellow);
                                                }
                                                else
                                                {
                                                    mUpdateStripInfoColor(Color.Orange);
                                                }
                                            }
                                            else
                                            {
                                                mUpdateStripInfoColor(Color.LightBlue);
                                            }
                                            stripSec = (DateTime.Now - dt).TotalSeconds;
                                            dt = DateTime.Now;
                                            formAnalyze = null; // shown and in use;
                                            //                                        formAnalyze.ShowDialog();
                                        }
                                        catch (Exception ex)
                                        {
                                            CProgram.sLogException("Analysis Form run", ex);
                                        }
                                    }
                                }

                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException("Analyze form", ex);
                    }
                    if (formAnalyze != null)
                    {
                        formAnalyze.mReleaseLock();
                    }
                }

                mTriageUpdateReset();

            }
            totalSec = (DateTime.Now - startDT).TotalSeconds;

            if (totalSec >= (CLicKeyDev.sbDeviceIsProgrammer() ? 2 : 4))
            {
                string s = "Stat R" + recordIndex.ToString() + " in " + totalSec.ToString("0.00")
                     + "sec, db=" + dbSec.ToString("0.00")
                     + ", imp=" + createSec.ToString("0.00")
                     + ", state=" + stateSec.ToString("0.00")
                     + ", load=" + loadSec.ToString("0.00")
                     + ", devStPt=" + devStdPtSec.ToString("0.00")
                     + ", scrn=" + screenSec.ToString("0.00")
                     + ", show=" + showSec.ToString("0.00")
                     + ", strip=" + stripSec.ToString("0.00");

               CProgram.sLogLine(s);
            }
        }
        private void Anayze_Click_1(object sender, EventArgs e)
        {
            mStartAnalyze();
        }

        private void FormEventBoard_Shown(object sender, EventArgs e)
        {
            mWindowSizeChanged();

            int triageNrHours = Properties.Settings.Default.TriageNrHours;
            if (triageNrHours < 1) triageNrHours = 1;

            if (triageNrHours % 24 == 0) { triageNrHours /= 24; listBoxViewUnit.TopIndex = 1; }
            else { listBoxViewUnit.TopIndex = 0; }

            textBoxViewTime.Focus();
        }

        private void listBoxViewUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i = listBoxViewUnit.TopIndex;

            /*             if( listBoxViewUnit.TopIndex == 0 )
                        {
                            listBoxViewUnit.TopIndex = 1;
                        }
                        else
                        {
                            listBoxViewUnit.TopIndex = 0;
                        }
            */
        }

        private void toolStripUsers_Click(object sender, EventArgs e)
        {
            if (CProgram.sbEnterProgUserArea(true))
            {
                CUserForm form = new CUserForm();

                if (form != null)
                {
                    form.ShowDialog();
                }
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if (CProgram.sbEnterProgUserArea(true))
            {
                CDeviceModelForm form = new CDeviceModelForm(null);

                if (form != null)
                {
                    form.ShowDialog();
                }
            }
        }

        private void toolStripDepartments_Click(object sender, EventArgs e)
        {
            if (CProgram.sbEnterProgUserArea(true))
            {
                CClientInfoForm form = new CClientInfoForm(null, 0, false);

                if (form != null)
                {
                    form.ShowDialog();
                }
            }
        }

        private void toolStripCustomers_Click(object sender, EventArgs e)
        {
            if (CProgram.sbEnterProgUserArea(true))
            {
                CHospitalListForm form = new CHospitalListForm();

                if (form != null)
                {
                    form.ShowDialog();
                }
            }
        }

        private void toolStripButtonCic_Click(object sender, EventArgs e)
        {

        }

        private void toolStripWorkNew_Click(object sender, EventArgs e)
        {
            if (CProgram.sbEnterProgUserArea(true))
            {
                CStudyPatientForm form = new CStudyPatientForm(null, null, false, true, true, true);

                if (form != null)
                {
                    form.ShowDialog();
                }
            }

        }

        private void toolStripStudyList(object sender, EventArgs e)
        {
            CStudyListForm form = new CStudyListForm(null, 0, false, false, "", "", 0, false, 0);

            if (form != null)
            {
                form.ShowDialog();
            }

        }

        private void toolStripDevices_Click(object sender, EventArgs e)
        {
            if (CProgram.sbEnterProgUserArea(true))
            {
                CDeviceListForm form = new CDeviceListForm();

                if (form != null)
                {
                    form.ShowDialog();
                }
            }
        }

        private void listBoxViewUnit_MouseClick(object sender, MouseEventArgs e)
        {

            if (listBoxViewUnit.TopIndex == 0)
            {
                listBoxViewUnit.TopIndex = 1;
            }
            else
            {
                listBoxViewUnit.TopIndex = 0;
            }
        }

        private void toolStripExit_Click(object sender, EventArgs e)
        {
            Close();
        }


        private void mOpenHelpDesk(bool AbTV)
        {
            string exeName = AbTV ? "helpdesk2.exe" : "helpdesk.exe";
            string exePath = Path.Combine("helpdesk\\", exeName);

            if (File.Exists(exePath))
            {
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = exePath;
                //startInfo.Arguments;
                Process.Start(startInfo);
            }
            else
            {
                CProgram.sPromptWarning(true, "Open helpdesk", "failed to find helpdesk file");
            }

        }
        private void toolStripHelpdesk_Click(object sender, EventArgs e)
        {
            //            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            //            if (Control.MouseButtons == MouseButtons.Right)
            //            {
            contextMenuHelpDesk.Show(Control.MousePosition);//.PointToScreen(new Point(0, 0)));
                                                            //            }
                                                            //            else
            {
                //mOpenHelpDesk(bCtrl);
            }

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void buttonFindings_Click(object sender, EventArgs e)
        {
            if (_sbTriageOnly)
            {
                return;
            }
            try
            {
                DataGridViewRow index = dataGridTriage.CurrentRow;

                if (index != null && index.Cells.Count >= 10)
                {
                    UInt32 studyNr = 0;
                    string studyStr = index.Cells[(int)DTriageGridField.Study].Value.ToString();

                    if (UInt32.TryParse(studyStr, out studyNr) && studyNr > 0)
                    {
                        CPreviousFindingsForm form = new CPreviousFindingsForm(studyNr, true, 0, 0);

                        if (form != null)
                        {
                            form.ShowDialog();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("failed findings call", ex);
            }
        }

        private void buttonSetState_Click(object sender, EventArgs e)
        {
            contextMenuState.Show(buttonSetState.PointToScreen(new Point(0, 0)));

        }

        private void contextMenuState_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (e.ClickedItem != null)
            {
                string text = e.ClickedItem.Text;
                DRecState state = DRecState.Unknown;
                DRecState i = state;

                while (++i < DRecState.NrRecStates)
                {
                    if (text == i.ToString())
                    {
                        state = i;
                        break;
                    }
                }
                if (state != DRecState.Unknown)
                {
                    if (_sbTriageOnly)
                    {
                        if (state <= DRecState.Triaged)
                        {
                            mTriageSetNewState(state);
                        }
                    }
                    else if( state == DRecState.QCd)
                    {
                        if(_sbQCdEnabled)
                        {
                            mTriageSetNewState(state);
                        }
                    }
                    else
                    {
                        mTriageSetNewState(state);
                    }
                }
            }
        }

        private void contextMenuState_Opening(object sender, CancelEventArgs e)
        {

        }

        private void toolStripPhysician_Click(object sender, EventArgs e)
        {
            if (CProgram.sbEnterProgUserArea(true))
            {
                CPhysiciansForm form = new CPhysiciansForm(null, false);
                //            CUserListForm form = new CUserListForm();

                if (form != null)
                {
                    form.ShowDialog();
                }
            }
        }

        private void toolStripButtonUpdate2_Click(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;
            if (bCtrl)
            {
                _mbUpdatingGrid = false;
                _mbUpdatingStrip = false;
                _mbUpdatingTriage = false;
                mShowBusy(false);
            }
            sSetTriageTimerMode(false, DTriageTimerMode.Run, "Run update");
            if (CProgram.sbEnterProgUserArea(true))
            {
                mUpdateTriageScreen(true, true);
            }
        }

        private void toolStripWorld_Click(object sender, EventArgs e)
        {
            if (CProgram.sbEnterProgUserArea(true))
            {
                CStudyListForm form = new CStudyListForm(null, 0, false, false, "", "", 0, false, 0);

                if (form != null)
                {
                    form.ShowDialog();
                }
            }
        }

        private void toolStripPatientList_Click(object sender, EventArgs e)
        {
            if (CProgram.sbEnterProgUserArea(true))
            {
                CPatientListForm form = new CPatientListForm("", null, 0, false,
                                    "", false, "", "", "", DateTime.MinValue,
                    true, true, true);

                if (form != null)
                {
                    form.ShowDialog();
                }
            }
        }

        private void toolStripAddDevice_Click(object sender, EventArgs e)
        {
            if (CProgram.sbEnterProgUserArea(true))
            {
                UInt32 studyIX = 0;
                string deviceSNR = mGetRowDeviceSnr(out studyIX);

                CDeviceForm form = new CDeviceForm(deviceSNR, true);

                if (form != null)
                {
                    form.ShowDialog();
                }
            }
        }

        private void mEnableSelectView(bool AbEnabled)
        {
            panelSelectView.Enabled = AbEnabled;
            textBoxSelectStudy.Text = "";
            textBoxSelectDevice.Text = "";
            textBoxSelectPatID.Text = "";
            textBoxSelectName.Text = "";
            textBoxSelectType.Text = "";
            textBoxSelectRem.Text = "";
        }

        private void radioButtonViewAll_CheckedChanged(object sender, EventArgs e)
        {
            _mCurrentDeviceSNR = "";
            _mCurrentPatID = "";
            _mCurrentStudyIX = 0;
            mEnableSelectView(true);
        }

        public bool mbTestCurrent()
        {
            return _mCurrentStudyIX > 0
            || _mCurrentDeviceSNR != null && _mCurrentDeviceSNR.Length > 0
            || _mCurrentPatID != null && _mCurrentPatID.Length > 0;
        }

        public string mOneLine(string ALine)
        {
            string s = "";
            int n = ALine == null ? 0 : ALine.Length;
            int l = 0;

            for (int i = 0; i < n; ++i)
            {
                char c = ALine[i];
                if (char.IsLetterOrDigit(c))
                {
                    s += c;
                    ++l;
                }
                else if (c == '\r' || c == '\n')
                {
                    break;
                }
                else if (c >= ' ')
                {
                    s += c;
                    ++l;
                }
                else if (l > 0)
                {
                    s += " ";
                }
            }
            return s;
        }

        string mGetRowDeviceSnr(out UInt32 ArStudyNr)
        {
            string deviceSNR = "";
            UInt32 studyIX = 0;

            try
            {
                DataGridViewRow index = dataGridTriage.CurrentRow;

                if (index != null && index.Cells.Count >= 10)
                {
                    string mdlSnr = index.Cells[(int)DTriageGridField.ModelNr].Value.ToString();
                    int pos = mdlSnr.IndexOf('#');
                    if (pos > 0)
                    {
                        pos += 3; // "#: "
                        if (mdlSnr.Length > pos)
                        {
                            deviceSNR = mOneLine(mdlSnr.Substring(pos));
                        }
                    }
                    string studyNr = index.Cells[(int)DTriageGridField.Study].Value.ToString();
                    if (false == UInt32.TryParse(studyNr, out studyIX))
                    {
                        studyIX = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("select device", ex);
            }
            ArStudyNr = studyIX;
            return deviceSNR;
        }
        string mGetRowPatID(out UInt32 ArStudyNr)
        {
            string patID = "";
            UInt32 studyIX = 0;

            try
            {
                DataGridViewRow index = dataGridTriage.CurrentRow;

                if (index != null && index.Cells.Count >= 10)
                {
                    string patStr = index.Cells[(int)DTriageGridField.Patient].Value.ToString();
                    patID = CProgram.sGetFirstLine(patStr);
                    int pos = patID.IndexOf(']');
                    if (pos >= 0)
                    {
                        patID = CProgram.sTrimString(patID.Substring(pos + 1));
                    }

                    string studyNr = index.Cells[(int)DTriageGridField.Study].Value.ToString();
                    if (false == UInt32.TryParse(studyNr, out studyIX))
                    {
                        studyIX = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("select device", ex);
            }
            ArStudyNr = studyIX;
            return patID;
        }

        private void radioButtonCurrentDevice_CheckedChanged(object sender, EventArgs e)
        {
            _mCurrentDeviceSNR = "";
            _mCurrentPatID = "";
            _mCurrentStudyIX = 0;

            mEnableSelectView(false);
            try
            {
                _mCurrentDeviceSNR = mGetRowDeviceSnr(out _mCurrentStudyIX);
            }
            catch (Exception ex)
            {
                CProgram.sLogException("select device", ex);
            }
        }

        private void labelModelNr_Click(object sender, EventArgs e)
        {

        }

        private void dataGridTriage_DoubleClick(object sender, EventArgs e)
        {
            mStartAnalyze();
        }

        private void buttonSetState_Click_1(object sender, EventArgs e)
        {
            if (_sbTriageOnly)
            {
                return;
            }
            contextMenuState.Show(buttonSetState.PointToScreen(new Point(0, 0)));
        }

        private void buttonShowTriage_Click(object sender, EventArgs e)
        {
            mSetTriageView(DTriageViewMode.Triage, true);
        }

        private void buttonShowAnalyze_Click(object sender, EventArgs e)
        {
            mSetTriageView(DTriageViewMode.Analyze, true);
        }

        private void buttonShowReport_Click(object sender, EventArgs e)
        {
            mSetTriageView(DTriageViewMode.Report, true);
        }

        private void buttonShowAll_Click(object sender, EventArgs e)
        {
            mSetTriageView(DTriageViewMode.ListAll, true);
        }

        private void buttonShowReceived_Click(object sender, EventArgs e)
        {
            mSetTriageView(DTriageViewMode.ListReceived, true);
        }

        private void buttonShowTriaged_Click(object sender, EventArgs e)
        {
            mSetTriageView(DTriageViewMode.ListTriaged, true);
        }

        private void buttonShowAnalyzed_Click(object sender, EventArgs e)
        {
            mSetTriageView(DTriageViewMode.ListAnalyzed, true);
        }

        private void buttonShowReported_Click(object sender, EventArgs e)
        {
            mSetTriageView(DTriageViewMode.ListReported, true);
        }

        private void buttonShowNoise_Click(object sender, EventArgs e)
        {
            mSetTriageView(DTriageViewMode.ListNoise, true);
        }

        private void buttonShowNormal_Click(object sender, EventArgs e)
        {
            mSetTriageView(DTriageViewMode.ListExclude, true);
        }

        private void buttonShowBaseLine_Click(object sender, EventArgs e)
        {
            mSetTriageView(DTriageViewMode.ListBaseLine, true);
        }

        private void panel48_Paint_1(object sender, PaintEventArgs e)
        {
            //            mUpdateTriageScreen();
        }

        private void buttonShowLeadOff_Click(object sender, EventArgs e)
        {
            mSetTriageView(DTriageViewMode.ListLeadOff, true);
        }

        private void radioButtonViewPriLow_CheckedChanged(object sender, EventArgs e)
        {
            _mCurrentDeviceSNR = "";
            mEnableSelectView(false);

        }

        private void radioButtonViewPriMid_CheckedChanged(object sender, EventArgs e)
        {
            _mCurrentDeviceSNR = "";
            mEnableSelectView(false);

        }

        private void radioButtonViewPriHigh_CheckedChanged(object sender, EventArgs e)
        {
            _mCurrentDeviceSNR = "";
            mEnableSelectView(false);

        }

        private void mOpenDoc(string ADocName)
        {
            string docName = "local" + ADocName;
            string docPath = Path.Combine(CProgram.sGetProgDir(), docName);
            bool bOpen = false;

            try
            {
                if (File.Exists(docPath))
                {
                    bOpen = true;
                }
                else
                {
                    docName = ADocName;
                    docPath = Path.Combine(CProgram.sGetProgDir(), docName);
                    bOpen = File.Exists(docPath);
                }

                if (bOpen)
                {
                    ProcessStartInfo startInfo = new ProcessStartInfo();
                    startInfo.FileName = docPath;
                    //startInfo.Arguments;
                    Process.Start(startInfo);
                }
                else
                {
                    CProgram.sAskWarning("Eventboard", "File " + ADocName + " not found!");
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed open doc " + ADocName, ex);
            }
        }

        private void mOpenManual()
        {
            mOpenDoc("Content\\1. Manuals\\EventboardManual.pdf");
        }
        private void mOpenHystory()
        {
            mOpenDoc("Content\\0. Release notes\\ChangeHistoryEventboard.rtf");
        }
        private void mOpenDownloadLinks()
        {
            mOpenDoc("DownloadEventboard.rtf");
        }

        private void toolStripHelp_Click(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;
            bool bAlt = (Control.ModifierKeys & Keys.Alt) == Keys.Alt;

            /*if( Control.MouseButtons==MouseButtons.Right)
            {

            }
            else 
            */
            if (bCtrl == false && bAlt)
            {
                CLicKeyDev.sbReopenLicenseRequest();
            }
            else if (bCtrl)
            {
                mOpenHystory();
            }
            else
            {
                contextMenuHelp.Show(Control.MousePosition);//.PointToScreen(new Point(0, 0)));
                                                            //                mOpenManual();
            }
            /*            else
                        {
                            string docName = bCtrl ? "ChangeHistoryEventboard.rtf" : "EventboardManual.pdf";
                            string docPath = Path.Combine(CProgram.sGetProgDir(), docName);

                            if (File.Exists(docPath))
                            {
                                ProcessStartInfo startInfo = new ProcessStartInfo();
                                startInfo.FileName = docPath;
                                //startInfo.Arguments;
                                Process.Start(startInfo);
                            }
                        }
            */
        }

        private void textBoxViewTime_Validated(object sender, EventArgs e)
        {
        }

        private void textBoxViewTime_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                mUpdateTriageScreen(true, true);
            }
        }

        private void buttonSetState_Click_2(object sender, EventArgs e)
        {

        }
        bool mbCheckDaysLeft()
        {
            int daysLeft = CLicKeyDev.sGetDaysLeft();
            bool bOk = daysLeft >= 0;

            if (!bOk)
            {
                CProgram.sPromptError(true, "DVTMS: i-Reader", "License past end time!, contact dealer!");
                Close();

            }
            return bOk;
        }

        private void mCheckUpdateOtherRecords(CRecordMit ArRecord, ref CDeviceInfo ArDevice, ref CStudyInfo ArStudy, ref CPatientInfo ArPatient)
        {
            if (ArRecord.mStudy_IX > 0 && ArStudy.mIndex_KEY > 0)
            {
                // now find all records that have the same device and still not have the

                CRecordMit searchRec = new CRecordMit(CDvtmsData.sGetDBaseConnection());
                List<CSqlDataTableRow> recList = null;

                if (searchRec != null)
                {
                    string showError = "";
                    bool bError;
                    UInt64 searchMask = CSqlDataTableRow.sGetMask((UInt16)DRecordMitVars.RecState_IX)
                        | CSqlDataTableRow.sGetMask((UInt16)DRecordMitVars.Study_IX)
                        | CSqlDataTableRow.sGetMask((UInt16)DRecordMitVars.DeviceID);
                    string eventName = searchRec.mVarGetName((UInt16)DRecordMitVars.EventUTC);
                    DateTime dt = ArStudy._mStudyStartDate;
                    dt = dt.AddHours(-12);
                    string whereStr = eventName + ">" + searchRec.mSqlDateTimeSqlString(dt);

                    searchRec.mStudy_IX = 0;
                    searchRec.mDeviceID = ArRecord.mDeviceID;
                    searchRec.mRecState = (UInt16)DRecState.Received;

                    if (searchRec.mbDoSqlSelectList(out bError, out recList, searchRec.mMaskValid, searchMask, false, whereStr))
                    {
                        if (recList != null && recList.Count > 0
                            && CProgram.sbAskOkCancel("Created Study", "Check " + recList.Count.ToString() + " received events with same serial number " + ArRecord.mDeviceID + "?")
                        )
                        {
                            string studyStr = ArStudy.mIndex_KEY.ToString() + " pat=" + ArStudy._mPatient_IX.ToString() + " dev=" + ArStudy._mStudyRecorder_IX.ToString()
                                + " " + CProgram.sDateTimeToString(ArStudy._mRecorderStartUTC) + " < " + CProgram.sDateTimeToString(ArStudy._mRecorderEndUTC);
                            string devStr = ArDevice.mIndex_KEY.ToString() + " " + ArDevice._mDeviceSerialNr + " study = " + ArDevice._mActiveStudy_IX.ToString() + " refID = " + ArDevice._mActiveRefID.mDecrypt()
                                + " " + CProgram.sDateTimeToString(ArDevice._mRecorderStartUTC) + " < " + CProgram.sDateTimeToString(ArDevice._mRecorderEndUTC);
                            int nList = recList.Count;
                            int nDone = 0;
                            foreach (CRecordMit rec in recList)
                            {
                                string recNr = "R#" + rec.mIndex_KEY.ToString() + " dev=" + rec.mDeviceID + ", pat=" + rec.mPatientID.mDecrypt() + " event " + rec.mGetEventUTC().ToString();

                                bool bLoad = ArDevice.mbLoadCheckFromRecordEB( ref showError, rec, false, ref ArStudy, ref ArPatient, false);
//                                bool bLoad = ArDevice.mbLoadCheckFromRecord(false, rec, false, ref ArStudy, ref ArPatient, false, false);
                                if (rec.mStudy_IX > 0)
                                {
                                    ++nDone;
                                }
                            }
                            CProgram.sLogLine("Updated " + nDone.ToString() + " / " + nList.ToString() + " event records to use study " + ArStudy.mIndex_KEY.ToString());
                        }
                    }
                }
                mUpdateTriageScreen(true, false);
            }

        }

        
        private void labelStudyIdText_Click(object sender, EventArgs e)
        {
            mOpenTriageStudy();
        }

        private void mCreateNewStudyFromRecord(CRecordMit ARecord, CDeviceInfo ADevice)
        {
            if (_sbTriageOnly)
            {
                return;
            }
            try
            {
                if (ARecord == null || ADevice == null || ARecord.mIndex_KEY == 0 || ADevice.mIndex_KEY == 0)
                {
                    CProgram.sLogError("Record or Device not ok");
                }
                else
                {
                    string showError = "";
                    CStudyInfo study = new CStudyInfo(CDvtmsData.sGetDBaseConnection());
                    CPatientInfo patient = new CPatientInfo(CDvtmsData.sGetDBaseConnection());

                    if (study != null && patient != null)
                    {
                        // first try to load and update record, study, patient

                        bool bLoad = ADevice.mbLoadCheckFromRecordEB(ref showError, ARecord, true, ref study, ref patient, false);
//                        bool bLoad = ADevice.mbLoadCheckFromRecord(false, ARecord, true, ref study, ref patient, false, false);

                        bool bStudyPresent = study.mIndex_KEY > 0;
                        bool bDoOther = false;

                        if (bStudyPresent)
                        {
                            CStudyPatientForm form = new CStudyPatientForm(study, patient, false, true, true, true);

                            if (form != null)
                            {
                                bDoOther = DialogResult.OK == form.ShowDialog();
                            }
                        }
                        else
                        {
                            bool bUpdateCurrent = ARecord.mStudy_IX == 0 && mbTestCurrent();
                            bool bCheck = mbCheckCreateDeviceStudy(ref ARecord, ref ADevice, ref study, ref patient);

                            if (study.mIndex_KEY > 0)
                            {
                                if (bUpdateCurrent) _mCurrentStudyIX = ARecord.mStudy_IX;

                                bLoad = ADevice.mbLoadCheckFromRecordEB(ref showError, ARecord, true, ref study, ref patient, false);
                                //bLoad = ADevice.mbLoadCheckFromRecord(false, ARecord, true, ref study, ref patient, false, false);
                                bDoOther = ARecord.mStudy_IX > 0;
                            }
                            mbTriageUpdateDbState(ARecord, 0);   // update this record
                        }
                        if (bDoOther)
                        {
                            mCheckUpdateOtherRecords(ARecord, ref ADevice, ref study, ref patient);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("create study from record", ex);
            }

        }
        private void mCreateNewStudyFromRecord1old(CRecordMit ARecord)
        {
            string showError = "";
            if (_sbTriageOnly)
            {
                return;
            }
            try
            {
                // create new study from active record
                DataGridViewRow index = dataGridTriage.CurrentRow;

                if (index != null && index.Cells.Count >= 10)
                {
                    string recordStr = index.Cells[(int)DTriageGridField.Index].Value.ToString();
                    UInt32 recordIndex = 0;

                    if (UInt32.TryParse(recordStr, out recordIndex) && recordIndex > 0)
                    {
                        CRecordMit record = new CRecordMit(CDvtmsData.sGetDBaseConnection());
                        if (record != null && record.mbDoSqlSelectIndex(recordIndex, record.mMaskValid))
                        {
                            CDeviceInfo device = new CDeviceInfo(CDvtmsData.sGetDBaseConnection());
                            CStudyInfo study = new CStudyInfo(CDvtmsData.sGetDBaseConnection());
                            CPatientInfo patient = new CPatientInfo(CDvtmsData.sGetDBaseConnection());

                            if (device != null && study != null && patient != null)
                            {
                                // first try to load and update record, study, patient

                                bool bLoad = device.mbLoadCheckFromRecordEB(ref showError, record, true, ref study, ref patient, false);
                                //bool bLoad = device.mbLoadCheckFromRecord(false, record, true, ref study, ref patient, false, false);

                                bool bDevicePresent = device.mMaskIndexKey > 0;
                                bool bStudyPresent = study.mIndex_KEY > 0;
                                bool bDoOther = false;

                                if (bStudyPresent)
                                {
                                    CStudyPatientForm form = new CStudyPatientForm(study, patient, false, true, true, true);

                                    if (form != null)
                                    {
                                        bDoOther = DialogResult.OK == form.ShowDialog();
                                    }
                                }
                                else
                                {
                                    bool bUpdateCurrent = record.mStudy_IX == 0 && mbTestCurrent();


                                    bool bCheck = mbCheckCreateDeviceStudy(ref record, ref device, ref study, ref patient);

                                    if (study.mIndex_KEY > 0)
                                    {
                                        if (bUpdateCurrent) _mCurrentStudyIX = record.mStudy_IX;

                                        bLoad = device.mbLoadCheckFromRecordEB(ref showError, record, true, ref study, ref patient, false);
//                                        bLoad = device.mbLoadCheckFromRecord(false, record, true, ref study, ref patient, false, false);
                                        bDoOther = record.mStudy_IX > 0;
                                    }
                                    mbTriageUpdateDbState(record, 0);   // update this record
                                }
                                if (bDoOther)
                                {
                                    mCheckUpdateOtherRecords(record, ref device, ref study, ref patient);
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                CProgram.sLogException("create study from record", ex);
            }

        }

        private void mOpenTriageStudyByNumber(UInt32 AStudyNr)

        {
            if (_sbTriageOnly)
            {
                return;
            }
            try
            {
                if ( AStudyNr > 0)
                {
                    CStudyInfo study = new CStudyInfo(CDvtmsData.sGetDBaseConnection());

                    if (study != null)
                    {
                        if (study.mbDoSqlSelectIndex(AStudyNr, study.mGetValidMask(true)))
                        {
                            CStudyPatientForm form = new CStudyPatientForm(study, null, false, true, true, true);

                            if (form != null)
                            {
                                form.Show();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Open Study form for S" + AStudyNr.ToString(), ex);
            }
        }

        private bool mbAddRecordToStudyAskOk(CRecordMit ARecord, CDeviceInfo ADevice, string AAddRecRemark, string ADeviceName, CStudyInfo ArStudy, CPatientInfo ArPatient)
        {
            bool bOk = false;

            if ( ARecord != null && ADevice != null)
            {
                UInt32 studyIX = 0;
                UInt32 recordIX = ARecord.mIndex_KEY;
                string s = "Recording " + recordIX.ToString()
                       + "\r\n Device " + ADeviceName
                       + "\r\n mode remark " + AAddRecRemark;

                s += "\r\n event " + ARecord.mGetDeviceTimeString(ARecord.mGetEventUTC(), DTimeZoneShown.Long) + " " + ARecord.mEventTypeString
                  + "\r\n patient: " + ARecord.mPatientID.mDecrypt() + " " + CProgram.sDateToString(ARecord.mPatientBirthDate.mDecryptToDate())
                    + " " + CPatientInfo.sGetGenderString((DGender)ARecord.mPatientGender)
                    + " " + ARecord.mPatientTotalName.mDecrypt()
                    + "\r\n";
                if (ArStudy == null || ArStudy.mIndex_KEY == 0)
                {
                    s += "\r\n No Study!";
                }
                else
                {
                    studyIX = ArStudy.mIndex_KEY;
                    s += "\r\n Study " + ArStudy.mIndex_KEY.ToString() + "  " + ArStudy._mStudyRemarkLabel;
                }
                if (ArPatient == null || ArPatient.mIndex_KEY == 0)
                {
                    s += "\r\n No Patient!";
                }
                else
                {
                    s += "\r\n Patient " + ArPatient.mIndex_KEY.ToString() + "  " + ArPatient._mSocSecNr.mDecrypt() 
                        + " " + CProgram.sDateToString(ArPatient._mPatientDateOfBirth.mDecryptToDate())
                        + " "+ CPatientInfo.sGetGenderString((DGender)ArPatient._mPatientGender_IX) + " " + ArPatient.mGetFullName();
                }
                s += "\r\n\r\nAdd Record " + recordIX.ToString() + " to Study " + studyIX.ToString() + "?";

                bOk = ReqTextForm.sbReqText("Add Record " + recordIX.ToString() + " to Study " + studyIX.ToString() + " ? ", "Info", ref s);

            }

            return bOk;
        }
        private bool mbAddRecordToStudyDo(CRecordMit ARecord, CDeviceInfo ADevice, string AAddRecRemark, string ADeviceName, UInt32 AStudyIX, out CStudyInfo ArStudy, out CPatientInfo ArPatient)
        {
            bool bOk = false;
            CStudyInfo study = null;
            CPatientInfo patient = null;
            string showError = "";

            bOk = ADevice.mbAddRecordToStudy(ref showError, ARecord, AAddRecRemark, ADeviceName, AStudyIX, out study, out patient, true, true);

            if (bOk)
            {
                mbTriageUpdateRow(ARecord);
                mUpdateTriageStrips();
            }
            ArStudy = study;
            ArPatient = patient;
            return bOk;
        }

        private bool mbLoadStudyPatient( UInt32 ARecordIX, out CStudyInfo ArStudy, out CPatientInfo ArPatient, UInt32 AStudyIX )
        {
            bool bOk = false;
            CStudyInfo study = new CStudyInfo(CDvtmsData.sGetDBaseConnection()); 
            CPatientInfo patient = new CPatientInfo(CDvtmsData.sGetDBaseConnection());

            if (study != null && patient != null && AStudyIX > 0)
            {
                bOk = study.mbDoSqlSelectIndex(AStudyIX, study.mMaskValid);

                if (false == bOk)
                {
                    CProgram.sLogError(" record R#" + ARecordIX.ToString() + " failed load study " + AStudyIX.ToString());
                }
                else
                {
                    UInt32 patientIX = study._mPatient_IX;

                    bOk = patientIX > 0 && patient.mbDoSqlSelectIndex(patientIX, patient.mMaskValid);
                
                    if (false == bOk)
                    {
                        CProgram.sLogError(" record R#" + ARecordIX.ToString() + " S" + AStudyIX.ToString() + " failed load patient " + patientIX.ToString());
                    }
                }
            }
            ArStudy = study;
            ArPatient = patient;
            return bOk;
        }

        private bool mbAddRecordBySelectStudy(CRecordMit ARecord, CDeviceInfo ADevice, string AAddRecRemark, string ADeviceName, out CStudyInfo ArStudy, out CPatientInfo ArPatient)
        {
            bool bOk = false;
            CStudyInfo study = null;
            CPatientInfo patient = null;
            UInt32 studyIX = 0;

            if (CProgram.sbEnterProgUserArea(true) && ARecord != null && ADevice != null)
            {
                string title = "Record " + ARecord.mIndex_KEY.ToString() + " (" + ADeviceName + ")";
                string info = "R#" + ARecord.mIndex_KEY.ToString()
                    + " event " + ARecord.mGetDeviceTimeString(ARecord.mGetEventUTC(), DTimeZoneShown.Long)
                    + " " + ARecord.mDeviceID
                    + " patient: " + ARecord.mPatientID.mDecrypt() + " " + CProgram.sDateToString(ARecord.mPatientBirthDate.mDecryptToDate())
                    + " " + CPatientInfo.sGetGenderString((DGender)ARecord.mPatientGender)
                    + " " + ARecord.mPatientTotalName.mDecrypt();
                CStudyListForm form = new CStudyListForm(null, 0, true, true, title, info, ADevice.mIndex_KEY, true, 0);

                if (form != null)
                {
                    // set search criteria: deviceIX

                    form.ShowDialog();
                    if (form._mbSelected && form._mSelectedIndex > 0)
                    {
                        studyIX = form._mSelectedIndex;
                        if (mbLoadStudyPatient(ARecord.mIndex_KEY, out study, out patient, studyIX))
                        {
                            if (mbAddRecordToStudyAskOk(ARecord, ADevice, AAddRecRemark, ADeviceName, study, patient))
                            {
                                bOk = mbAddRecordToStudyDo(ARecord, ADevice, AAddRecRemark, ADeviceName, studyIX, out study, out patient);
                            }
                        }
                    }
                }
            }
            ArStudy = study;
            ArPatient = patient;

            return bOk;
        }

        private bool mbAddRecordBySelectPatient(CRecordMit ARecord, CDeviceInfo ADevice, string AAddRecRemark, string ADeviceName, 
            bool AbUniqueID, out CStudyInfo ArStudy, out CPatientInfo ArPatient)
        {
            bool bOk = false;
            CStudyInfo study = null;
            CPatientInfo patient = null;
            UInt32 studyIX = 0;

            if (CProgram.sbEnterProgUserArea(true) && ARecord != null && ADevice != null)
            {
                string fullName = ARecord.mPatientTotalName.mDecrypt();
                string title = "Record " + ARecord.mIndex_KEY.ToString() + " (" + ADeviceName + ")";
                string info = "R#" + ARecord.mIndex_KEY.ToString()
                    + " event " + ARecord.mGetDeviceTimeString(ARecord.mGetEventUTC(), DTimeZoneShown.Long)
                    + " " + ARecord.mDeviceID
                    + " patient: " + ARecord.mPatientID.mDecrypt() + " " + CProgram.sDateToString(ARecord.mPatientBirthDate.mDecryptToDate())
                    + " " + CPatientInfo.sGetGenderString((DGender)ARecord.mPatientGender)
                    + " " + fullName;
                UInt32 patientIX = 0;
                string firstName = "", middleName="", lastName="";
                DateTime dob = DateTime.MinValue; // ARecord.mPatientBirthDate.mDecryptToDate();
                string refID = ARecord.mPatientID.mDecrypt();

                // only auto search on patient Unique ID
                //    ARecord.mGetPatientName(out firstName, out middleName, out lastName);
                    
                    CPatientListForm formPatient = new CPatientListForm(title, null, 0, true,
                        info, AbUniqueID, refID, firstName, lastName, dob, 
                    true, true, true);

                if( formPatient != null )
                {
                    formPatient.ShowDialog();

                    if( formPatient._mbSelected && formPatient._mSelectedIndex > 0 )
                    {
                        patientIX = formPatient._mSelectedIndex;

                        CStudyListForm formStudy = new CStudyListForm(null, 0, true, true, title, info, ADevice.mIndex_KEY, true, patientIX);

                        if (formStudy != null)
                        {
                            // set search criteria: deviceIX

                            formStudy.ShowDialog();
                            if (formStudy._mbSelected && formStudy._mSelectedIndex > 0)
                            {
                                studyIX = formStudy._mSelectedIndex;
                                if (mbLoadStudyPatient(ARecord.mIndex_KEY, out study, out patient, studyIX))
                                {
                                    if (mbAddRecordToStudyAskOk(ARecord, ADevice, AAddRecRemark, ADeviceName, study, patient))
                                    {
                                        bOk = mbAddRecordToStudyDo(ARecord, ADevice, AAddRecRemark, ADeviceName, studyIX, out study, out patient);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            ArStudy = study;
            ArPatient = patient;

            return bOk;
        }

        private void mOpenTriageStudy()
        {
            UInt32 recordIndex = 0;
            StreamWriter recordLockStream = null;
            string recordLockFilePath = null;
            string recordLockName = null;
            bool bRecordAccess = true;

            if (_sbTriageOnly)
            {
                return;
            }
            try
            {
                string studyText = labelTriageStudy.Text;
                UInt32 studyNr = 0;

                if (UInt32.TryParse(studyText, out studyNr) && studyNr > 0)
                {
                    mOpenTriageStudyByNumber(studyNr);
                }
                else
                {
                    // find active record
                    DataGridViewRow gridRow = dataGridTriage.CurrentRow;

                    if (gridRow != null && gridRow.Cells.Count >= 10)
                    {
                        string recordStr = gridRow.Cells[(int)DTriageGridField.Index].Value.ToString();

                        if (UInt32.TryParse(recordStr, out recordIndex) && recordIndex > 0)
                        {
                            CRecordMit record = new CRecordMit(CDvtmsData.sGetDBaseConnection());
                            CDeviceInfo device = new CDeviceInfo(CDvtmsData.sGetDBaseConnection());
                            CStudyInfo study = null;
                            CPatientInfo patient = null;

                            if (record != null && device != null
                                && record.mbDoSqlSelectIndex(recordIndex, record.mMaskValid))
                            {
                                // lock record 
                                if (FormAnalyze._sbTriageLockAnRec)
                                {
                                    string casePath;

                                    if (record.mbGetCasePath(out casePath))
                                    {
                                        recordLockFilePath = Path.Combine(_mRecordingDir, casePath);
                                        recordLockName = CRecordMit.sGetLockName();
                                        bRecordAccess = CProgram.sbLockFilePath(recordLockFilePath, recordLockName, ref recordLockStream);

                                        if (false == bRecordAccess)
                                        {
                                            CProgram.sAskWarning("R#" + record.mIndex_KEY.ToString() + " add study", "Failed to lock record!");
                                        }
                                    }
                                }
                                //                                device.find
                                string deviceName = "";

                                if (false == device.mbLoadDeviceFromRecord(ref deviceName, false, record, false, true, true))
                                {
                                    CProgram.sAskWarning("R#" + record.mIndex_KEY.ToString() + " add to study", "Device not found: " + record.mDeviceID);
                                }
                                else if( bRecordAccess )
                                {
                                    bool bShift = (Control.ModifierKeys & Keys.Shift) == Keys.Shift;
                                    bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;
                                    bool bAlt = (Control.ModifierKeys & Keys.Alt) == Keys.Alt;

                                    if (record.mStudy_IX != 0)
                                    {
                                        mOpenTriageStudyByNumber(record.mStudy_IX);

                                    }
                                    else if (bShift)
                                    {
                                        mbAddRecordBySelectPatient(record, device, "Manual patient (Unique ID)", deviceName, true, out study, out patient);
                                    }
                                    else if (bCtrl)
                                    {
                                        mbAddRecordBySelectStudy(record, device, "Manual study", deviceName, out study, out patient);
                                    }
                                    else if (bAlt)
                                    {
                                        mbAddRecordBySelectPatient(record, device, "Manual patient (Patient ID)", deviceName, false, out study, out patient);
                                    }
                                    else
                                    {
                                        mCreateNewStudyFromRecord(record, device);
                                    }
                                }
                            }
                        }
                    }
                }
                mUpdateTriageStrips();
            }
            catch (Exception ex)
            {
                CProgram.sLogException("open triage study for record " + recordIndex.ToString(), ex);
            }
            if (bRecordAccess && FormAnalyze._sbTriageLockAnRec)
            {
                CProgram.sbUnLockFilePath(recordLockFilePath, recordLockName, ref recordLockStream);
            }
        }


        private void labelModelNr_Click_1(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl)
            {
                //textBoxRecordDir.Visible = !textBoxRecordDir.Visible;
                panelLoadStats.Visible = !panelLoadStats.Visible;
                //                labelUpdTime.Visible = !labelUpdTime.Visible;
                labelDrive.Visible = !labelDrive.Visible;
            }
        }

        private void textBoxRecordDir_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxRecordDir_DoubleClick(object sender, EventArgs e)
        {
            if (_sbTriageOnly)
            {
                return;
            }
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl) //&& CLicKeyDev.sbDeviceIsProgrammer())
            {
                bool bShift = (Control.ModifierKeys & Keys.Shift) == Keys.Shift;

                mTriageOpenEventDir(bShift);
            }
        }

        private void textBoxRecordDir_MouseDoubleClick(object sender, MouseEventArgs e)
        {
        }

        private void labelModelNr_DoubleClick(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl) //&& CLicKeyDev.sbDeviceIsProgrammer())
            {
                bool bShift = (Control.ModifierKeys & Keys.Shift) == Keys.Shift;

                mTriageOpenEventDir(bShift);
            }

        }

        private void labelStudyClient_Click(object sender, EventArgs e)
        {
            if (_sbTriageOnly)
            {
                return;
            }
        }

        private void toolStripListReports_Click(object sender, EventArgs e)
        {

        }

        private void labelStudyReports_Click(object sender, EventArgs e)
        {
            if (_sbTriageOnly)
            {
                return;
            }
            try
            {
                DataGridViewRow index = dataGridTriage.CurrentRow;

                if (index != null && index.Cells.Count >= 10)
                {
                    UInt32 studyNr = 0;
                    string studyStr = index.Cells[(int)DTriageGridField.Study].Value.ToString();

                    if (UInt32.TryParse(studyStr, out studyNr))
                    {
                        CPreviousFindingsForm form = new CPreviousFindingsForm(studyNr, true, 0, 0);

                        if (form != null)
                        {
                            form.ShowDialog();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("failed findings call", ex);
            }
            /*            try
                        {
                            DataGridViewRow index = dataGridTriage.CurrentRow;

                            if (index != null && index.Cells.Count >= 10)
                            {
                                UInt32 studyNr = 0;
                                string studyStr = index.Cells[(int)DTriageGridField.Study].Value.ToString();

                                if (UInt32.TryParse(studyStr, out studyNr))
                                {
                                    CStudyReportListForm form = new CStudyReportListForm(studyNr, true, 0, 0);

                                    if (form != null)
                                    {
                                        form.ShowDialog();
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            CProgram.sLogException("failed  studyReprtList call", ex);
                        }
            */
        }

        private void radioButtonCurrentPatID_CheckedChanged(object sender, EventArgs e)
        {
            _mCurrentDeviceSNR = "";
            _mCurrentPatID = "";
            _mCurrentStudyIX = 0;

            mEnableSelectView(false);

            try
            {
                _mCurrentPatID = mGetRowPatID(out _mCurrentStudyIX);
            }
            catch (Exception ex)
            {
                CProgram.sLogException("select device", ex);
            }
        }

        private void buttonPdfView_Click(object sender, EventArgs e)
        {
            if (_sbTriageOnly)
            {
                return;
            }
            try
            {
                DataGridViewRow index = dataGridTriage.CurrentRow;

                if (index != null && index.Cells.Count >= 10)
                {
                    UInt32 studyNr = 0;
                    string studyStr = index.Cells[(int)DTriageGridField.Study].Value.ToString();

                    if (UInt32.TryParse(studyStr, out studyNr) && studyNr > 0)
                    {
                        CStudyReportListForm form = new CStudyReportListForm(studyNr);

                        if (form != null)
                        {
                            form.ShowDialog();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Call pdf study list", ex);
            }
        }

        private void toolStripUserLock_Click(object sender, EventArgs e)
        {
            CProgram.sSetProgUserInactive();
            mUpdateUserButtons();
            mUpdateTriageScreen(true, false);
        }

        private void toolStripUserCancel_Click(object sender, EventArgs e)
        {
            CProgram.sSetProgUser(0, "", "", "");
            mUpdateUserButtons();
            mUpdateTriageScreen(true, false);
        }

        private void labelRecNr_DoubleClick(object sender, EventArgs e)
        {
            if (_sbTriageOnly)
            {
                return;
            }
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl) //&& CLicKeyDev.sbDeviceIsProgrammer())
            {
                bool bShift = (Control.ModifierKeys & Keys.Shift) == Keys.Shift;

                mTriageOpenEventDir(bShift);
            }
        }

        private void mTriageOpenStudyDir()
        {
            mTriageUpdateReset();
            // open explorer wit file at cursor
            try
            {
                string studyText = labelTriageStudy.Text;
                UInt32 studyNr = 0;

                if (UInt32.TryParse(studyText, out studyNr) && studyNr > 0)
                {
                    string studyPath;

                    if (CDvtmsData.sbGetStudyCaseDir(out studyPath, studyNr, true))
                    {
                        // open explorer wit file at cursor
                        try
                        {
                            ProcessStartInfo startInfo = new ProcessStartInfo();
                            startInfo.FileName = @"explorer";
                            startInfo.Arguments = studyPath;
                            Process.Start(startInfo);
                        }
                        catch (Exception e2)
                        {
                            CProgram.sLogException("Failed open explorer", e2);
                        }

                    }
                    else
                    {
                        CProgram.sPromptError(true, "Study " + studyNr.ToString() + " folder missing", "" + studyPath + " does not exist!");
                    }
                }
            }
            catch (Exception e2)
            {
                CProgram.sLogException("Failed open explorer", e2);
            }
        }

        private void labelTriageStudy_DoubleClick(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl) //&& CLicKeyDev.sbDeviceIsProgrammer())
            {
                mTriageOpenStudyDir();
            }
        }

        private void contextMenuQuickStat_Opening(object sender, CancelEventArgs e)
        {

        }

        private void mQuickStat(CStringSet AAnalyseSet, string AAnalysisText)
        {
            if (false == mbCheckDaysLeft())
            {
                return;
            }

            if (mbCreateDBaseConnection())
            {
                {
                    try
                    {
                        int index = 0;
                        string oldState = "";

                        if (mbGetCursorIndexState(out index, out oldState))
                        {
                            if (oldState != "Received" && oldState != "Triaged")
                            {
                                CProgram.sPrompOk(true, "R" + index.ToString() + " Quick stat: " + AAnalysisText, "Quick stat not allowed for state " + oldState);
                            }
                            else
                            {
                                CRecordMit rec = new CRecordMit(CDvtmsData.sGetDBaseConnection());
                                CRecAnalysis recAnalysis = new CRecAnalysis(CDvtmsData.sGetDBaseConnection());
                                bool bError = false;
                                List<CSqlDataTableRow> list;
                                UInt64 mask = CSqlDataTableRow.sGetMask((UInt16)DSqlDataTableColum.Index_KEY) | CSqlDataTableRow.sGetMask((UInt16)DRecAnalysisVars.Recording_IX);

                                //string newState = CRecordMit.sGetStateUserString((UInt16)AState);

                                if (rec != null && recAnalysis != null)
                                {
                                    rec.mIndex_KEY = (UInt32)index;

                                    recAnalysis._mRecording_IX = rec.mIndex_KEY;

                                    if (rec.mbDoSqlSelectIndex((UInt32)index, rec.mGetValidMask(true)))
                                    {
                                        bool b = true;

                                        if (rec.mStudy_IX == 0)
                                        {
                                            CDeviceInfo deviceInfo = null;
                                            CStudyInfo studyInfo = null;
                                            CPatientInfo patientInfo = null;

                                            b = mbCheckCreateDeviceStudy(ref rec, ref deviceInfo, ref studyInfo, ref patientInfo);
                                        }
                                        if (b)
                                        {
                                            if (rec.mStudy_IX == 0)
                                            {
                                                CProgram.sPrompOk(true, "R" + index.ToString() + " Quick stat: " + AAnalysisText, "Quick stat not allowed for unknown study");
                                            }
                                            else if (recAnalysis.mbDoSqlSelectList(out bError, out list, mask, CSqlDataTableRow.sGetMask((UInt16)DRecAnalysisVars.Recording_IX), false, "")
                                                && list.Count > 0)
                                            {
                                                CProgram.sPrompOk(true, "R" + index.ToString() + " Quick stat: " + AAnalysisText, "Quick stat not allowed for existing analysis");
                                            }
                                            else if (bError)
                                            {
                                                CProgram.sPrompOk(true, "R" + index.ToString() + " Quick stat: " + AAnalysisText, "Quick stat error finding current analysis");
                                            }
                                            else
                                            {
                                                recAnalysis._mAnalysisClassSet = AAnalyseSet;
                                                recAnalysis._mAnalysisRemark = AAnalysisText;
                                                recAnalysis._mSeqInRecording = ++rec.mNrAnalysis;
                                                recAnalysis._mStudy_IX = rec.mStudy_IX;
                                                recAnalysis._mMeasuredUTC = DateTime.UtcNow;
                                                recAnalysis._mTechnician_IX = CProgram.sGetProgUserIX();
                                                recAnalysis._mRecording_IX = rec.mIndex_KEY;
                                                recAnalysis._mbManualEvent = rec.mbIsManualEventType(); 

                                                if (recAnalysis.mbDoSqlInsert())
                                                {
                                                    rec.mRecState = (UInt16)DRecState.Analyzed;
                                                    UInt64 saveMask = CSqlDataTableRow.sGetMask((UInt16)DRecordMitVars.NrAnalysis) | CSqlDataTableRow.sGetMask((UInt16)DRecordMitVars.RecState_IX);
                                                    mbTriageUpdateDbState(rec, saveMask);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException("Quick Stat Analyze form", ex);
                    }
                }
            }
            mTriageUpdateReset();
        }

        private void quickStatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // quickStat call stat
            mStartAnalyze();
        }

        private void quickPauseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CStringSet set = new CStringSet();

            if (set != null)
            {
                set.mAddValue(CDvtmsData._cFindings_Pause);

                string t = _mQuickPauseText != null && _mQuickPauseText.Length > 0 ? _mQuickPauseText : "Quick Pause";
                mQuickStat(set, t);
            }
        }

        private void dataGridTriage_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void openManualToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mOpenManual();
        }

        private void quickPACToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CStringSet set = new CStringSet();

            if (set != null)
            {
                set.mAddValue(CDvtmsData._cFindings_PAC);

                string t = _mQuickPacText != null && _mQuickPacText.Length > 0 ? _mQuickPacText : "Quick PAC-SVEB";
                mQuickStat(set, t);
            }
        }

        private void quickPVCToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CStringSet set = new CStringSet();

            if (set != null)
            {
                set.mAddValue(CDvtmsData._cFindings_PVC);

                string t = _mQuickPvcText != null && _mQuickPvcText.Length > 0 ? _mQuickPvcText : "Quick PVC-VEB";
                mQuickStat(set, t);
            }

        }

        private void quickPaceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CStringSet set = new CStringSet();

            if (set != null)
            {
                set.mAddValue(CDvtmsData._cFindings_Pace);

                string t = _mQuickPaceText != null && _mQuickPaceText.Length > 0 ? _mQuickPaceText : "Quick Pace";
                mQuickStat(set, t);
            }

        }

        private void quickVFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CStringSet set = new CStringSet();

            if (set != null)
            {
                set.mAddValue(CDvtmsData._cFindings_VF);

                string t = _mQuickVfText != null && _mQuickVfText.Length > 0 ? _mQuickVfText : "Quick VF";
                mQuickStat(set, t);
            }

        }

        private void quickAFlToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CStringSet set = new CStringSet();

            if (set != null)
            {
                set.mAddValue(CDvtmsData._cFindings_AFL);

                string t = _mQuickAFlText != null && _mQuickAFlText.Length > 0 ? _mQuickAFlText : "Quick AFlutter";
                mQuickStat(set, t);
            }
        }

        private void quickASysToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CStringSet set = new CStringSet();

            if (set != null)
            {
                set.mAddValue(CDvtmsData._cFindings_ASys);

                string t = _mQuickASysText != null && _mQuickASysText.Length > 0 ? _mQuickASysText : "Quick ASystole";
                mQuickStat(set, t);
            }
        }

        private void radioButtonNoStudy_CheckedChanged(object sender, EventArgs e)
        {
            _mCurrentDeviceSNR = "";
            _mCurrentPatID = "";
            _mCurrentStudyIX = 0;
            mEnableSelectView(false);
        }

        private void labelSelectStudy_Click(object sender, EventArgs e)
        {
            string s = "";
            if (CProgram.sbEnterProgUserArea(true))
            {
                CStudyListForm form = new CStudyListForm(null, 0, false, true, "Triage fileter", "", 0, true, 0);

                if (form != null)
                {
                    form.ShowDialog();
                    if (form._mbSelected && form._mSelectedIndex > 0)
                    {
                        s = form._mSelectedIndex.ToString();
                    }
                }
            }
            textBoxSelectStudy.Text = s;
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            mEnableSelectView(true);
        }


        private void label15_Click(object sender, EventArgs e)
        {
            mSetFullPreviewChannels();
        }
        private void mSetFullPreviewChannels()
        {
            UInt16 n = _mNrFullPreviewStrips;

            if (CProgram.sbReqUInt16("Eventboard Triage", "Nr of preview full strips", ref n, "", 0, 3))
            {
                _mNrFullPreviewStrips = n;
                mEnableFullStrips();
            }
        }

        private void toolTriageUpdTime2_Click(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;
            if (bCtrl)
            {
                _mbUpdatingGrid = false;
                _mbUpdatingStrip = false;
                _mbUpdatingTriage = false;
                mShowBusy(false);
                sSetTriageTimerLeaving(DTriageTimerMode.Stop, "Stop update");
                mUpdateMemUse();
            }
            mTriageUpdateReset();
        }

        private void heckDBReaderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mCheckReaderScan(true, true, true, true);
        }

        public void mUpdateCurrentGridRow(CRecordMit ARecord, string AText)
        {
            DataGridViewRow row = dataGridTriage.CurrentRow;

            if (row != null && row.Cells.Count >= (int)DTriageGridField.NrGridFields
                && row.Cells[(int)DTriageGridField.Index].Value.ToString() == ARecord.mIndex_KEY.ToString())
            {
                row.Cells[(int)DTriageGridField.PSA].Value = mGetStateString(ARecord); 
                row.Cells[(int)DTriageGridField.Patient].Value = mMakeRowPatient(ARecord, checkBoxAnonymize.Checked);
                row.Cells[(int)DTriageGridField.Remark].Value = ARecord.mRecRemark.mDecrypt();
                row.Cells[(int)DTriageGridField.State].Value = ARecord.mRecState.ToString("D2") + "\n" + DRecState.Received.ToString();
                row.Cells[(int)DTriageGridField.Study].Value = ARecord.mStudy_IX.ToString();
                mUpdateGridRowState(row);
                mUpdateTriageStrips();
            }
            else
            {
                CProgram.sLogLine("Failed to find row of " + AText + " " + ARecord.mIndex_KEY.ToString());

            }
        }

            private void mRevertRecordS0()
        {

            int index = 0;
            string oldState = "";

            if (mbGetCursorIndexState(out index, out oldState))
            {
                if (mbCreateDBaseConnection())
                {
                    if (CProgram.sbEnterProgUserArea(true))
                    {
                        try
                        {
                            string user = CProgram.sGetProgUserInitials();
                            CRecordMit rec = new CRecordMit(CDvtmsData.sGetDBaseConnection());

                            if (rec != null && rec.mbDoSqlSelectIndex((UInt32)index, rec.mGetValidMask(true)))
                            {
                                if (rec.mStudy_IX > 0
                                    && CProgram.sbAskOkCancel("Revert Event Record " + index.ToString() + " S" + rec.mStudy_IX.ToString() + " by " + user,
                                    "Revert to Study 0 and state Received?"))
                                {
                                    string addRem = "Revert " + rec.mStudy_IX.ToString() + "." + rec.mSeqNrInStudy.ToString() + " by " + user;
                                    string clearText = "Reverted by " + user;
                                    string oldRem = rec.mRecRemark.mDecrypt();
                                    rec.mStudy_IX = 0;
                                    rec.mSeqNrInStudy = 0;
                                    rec.mPatient_IX = 0;
                                    //rec.mPatientID.mbSetEncrypted(clearText);
                                    rec.mPatientTotalName.mbSetEncrypted(clearText);
                                    rec.mRecRemark.mbSetEncrypted(oldRem + "\r\n" + addRem);
                                    rec.mRecState = (UInt16)DRecState.Received;
                                    rec.mPatientBirthDate.mbSetEncrypted(0);
                                    rec._mStudyPermissions = new CBitSet64();
                                    rec._mStudyTrial_IX = 0;
                                    rec._mStudyClient_IX = 0;

                                    CProgram.sLogLine("R#" + index.ToString() + ":" + addRem);

                                    UInt64 saveMask = rec.mGetValidMask(false);
                                    if (rec.mbDoSqlUpdate(saveMask, true))
                                    {
                                        DataGridViewRow row = dataGridTriage.CurrentRow;

                                        if (row != null && row.Cells.Count >= (int)DTriageGridField.NrGridFields
                                            && row.Cells[(int)DTriageGridField.Index].Value.ToString() == index.ToString())
                                        {
                                            row.Cells[(int)DTriageGridField.PSA].Value = "S0!!\r\nR-"+rec.mIndex_KEY.ToString();
                                            row.Cells[(int)DTriageGridField.Patient].Value = clearText;
                                            row.Cells[(int)DTriageGridField.Remark].Value += "\r\n" + addRem;
                                            row.Cells[(int)DTriageGridField.State].Value = rec.mRecState.ToString("D2") + "\n" + DRecState.Received.ToString();
                                            row.Cells[(int)DTriageGridField.Study].Value = "0";
                                            mUpdateGridRowState(row);
                                        }
                                        else
                                        {
                                            CProgram.sLogLine("Failed to find row of revert Record " + index.ToString());
                                        }
                                    }
                                    else
                                    {
                                        CProgram.sLogLine("Failed update Record " + index.ToString());
                                    }
                                }
                                else
                                {
                                    CProgram.sLogLine("Failed revert Record " + index.ToString());
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            CProgram.sLogException("Failed revert Record " + index.ToString(), ex);
                        }
                        finally
                        {
                        }
                        mUpdateTriageStrips();
                    }
                }
                else
                {
                    CProgram.sLogLine("Select row in table first.");
                }
                mTriageUpdateReset();
            }

        }
        private void labelRevertS0_DoubleClick(object sender, EventArgs e)
        {
            if (_sbTriageOnly)
            {
                return;
            }
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl) //&& CLicKeyDev.sbDeviceIsProgrammer())
            {
                mRevertRecordS0();
            }

        }

        private void contextMenuHelpDesk_Opening(object sender, CancelEventArgs e)
        {

        }

        private void labelNrChannels_Click(object sender, EventArgs e)
        {
            mSetFullPreviewChannels();
        }

        private void checkBoxAnonymize_CheckedChanged(object sender, EventArgs e)
        {
            mUpdateTriageScreen(true, false);
        }

        private void toolStripSelectUser_TextChanged(object sender, EventArgs e)
        {
            string s = CProgram.sGetProgUserInitials();

            if (s == null || s.Length == 0)
            {
                // new user
                string initials = toolStripSelectUser.Text;
                if (initials != null && initials.Length > 0)
                {
                    string pin = "";

                    if (CProgram.sbReqLabel("Enter pin for " + initials, "Pin", ref pin, "", true)
                       && pin != null && pin.Length >= 1)
                    {
                        if (false == CDvtmsData.sbSetUserInitials(initials, pin))
                        {
                            CProgram.sAskOk("Select user", "Invalid user: " + initials);
                        }
                    }
                }
            }
            else
            {
                CProgram.sbEnterProgUserArea(true); // ask for pin if needed
            }
            mUpdateUserButtons();
            mUpdateTriageScreen(true, false);
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {

        }

        private void contextMenuHelp_Opening(object sender, CancelEventArgs e)
        {

        }

        private void checkDBCollectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mCheckDbCollect();
        }

        private void labelRowLimit_Click(object sender, EventArgs e)
        {
            CSqlDBaseConnection db = CDvtmsData.sGetDBaseConnection();
            UInt32 dbLimit = db == null ? _mTriageRowsLimit : db.mGetSqlRowLimit();
            UInt32 nRowLimit = _mTriageRowsLimit;
            UInt32 nDrawLimit = _mTriageDrawLimit;

            if (CProgram.sbReqUInt32("Limit the number of rows displayed (0=dbLimit)", "Number rows displayed", ref nRowLimit, "rows", 0, dbLimit)
                && CProgram.sbReqUInt32("Limit number of rows with ECG strips(0=Rows Limit)", "Number with ECG strips", ref nDrawLimit, "rows", 0, dbLimit)
                )
            {
                _mTriageRowsLimit = nRowLimit;
                _mTriageDrawLimit = nDrawLimit <= nRowLimit ? nDrawLimit : nRowLimit;
                checkBoxUseLimit.Checked = true;
                labelRowLimit.Text = nDrawLimit.ToString() + "_" + nRowLimit.ToString();
            }
        }

        private void FormEventBoard_FormClosing(object sender, FormClosingEventArgs e)
        {
            CProgram.sbHandleFormClosing(false, e);
        }

        private void FormEventBoard_FormClosed(object sender, FormClosedEventArgs e)
        {
            CProgram.sLogLine("Main form closed");
        }

        private void addLogLineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string s = "";

            if (ReqTextForm.sbReqText("Add Log Line", "Log text", ref s))
            {
                CProgram.sLogLine(s);
            }

        }

        private void newLogFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string reason = "";

            if (CProgram.sbReqLabel("Create New Log File", "Reason", ref reason, "", false))
            {
                CProgram.sCreateNewLogFile(reason);
            }

        }

        private void createTestFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string baseDir = CProgram.sGetProgDir();
            string testDir = "TestFiles";
            string result;

            CProgram.sbTestFilesWrite(out result, baseDir, testDir, 10000, 1000000);
            CProgram.sAskOk("Local Test Files - Create", result);

        }

        private void countTestFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string baseDir = CProgram.sGetProgDir();
            string testDir = "TestFiles";
            string result;

            CProgram.sbTestFilesCount(out result, baseDir, testDir);
            CProgram.sAskOk("Local Test Files - Count", result);

        }

        private void readTestFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string baseDir = CProgram.sGetProgDir();
            string testDir = "TestFiles";
            string result;

            CProgram.sbTestFilesRead(out result, baseDir, testDir);
            CProgram.sAskOk("Local Test Files - Read", result);

        }

        private void deleteTestFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string baseDir = CProgram.sGetProgDir();
            string testDir = "TestFiles";
            string result;

            CProgram.sbTestFilesDelete(out result, baseDir, testDir);

            CProgram.sAskOk("Local Test Files - Delete", result);
        }

        private void recordingCreateTestFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string baseDir = _mRecordingDir;
            string testDir = "TestFiles";
            string result;

            CProgram.sbTestFilesWrite(out result, baseDir, testDir, 10000, 1000000);

            CProgram.sAskOk("Recording Test Files - Create", result);

        }

        private void recordingCountTestFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string baseDir = _mRecordingDir;
            string testDir = "TestFiles";
            string result;

            CProgram.sbTestFilesCount(out result, baseDir, testDir);

            CProgram.sAskOk("Recording Test Files - Count", result);

        }

        private void recordingTestFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string baseDir = _mRecordingDir;
            string testDir = "TestFiles";
            string result;

            CProgram.sbTestFilesRead(out result, baseDir, testDir);
            CProgram.sAskOk("Recording Test Files - Read", result);
        }

        private void folderCreateTestFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == folderBrowserDialogTest.ShowDialog())
            {
                string baseDir = folderBrowserDialogTest.SelectedPath;
                string testDir = "TestFiles";
                string result;

                CProgram.sbTestFilesWrite(out result, baseDir, testDir, 10000, 1000000);
                CProgram.sAskOk("Folder Test Files - Create", result);

            }

        }

        private void folderCountTestFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == folderBrowserDialogTest.ShowDialog())
            {
                string baseDir = folderBrowserDialogTest.SelectedPath;
                string testDir = "TestFiles";
                string result;

                CProgram.sbTestFilesCount(out result, baseDir, testDir);
                CProgram.sAskOk("Folder Test Files - Count", result);

            }

        }

        private void folderReadTestFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == folderBrowserDialogTest.ShowDialog())
            {
                string baseDir = folderBrowserDialogTest.SelectedPath;
                string testDir = "TestFiles";
                string result;

                CProgram.sbTestFilesRead(out result, baseDir, testDir);
                CProgram.sAskOk("Folder Test Files - Read", result);
            }

        }

        private void folderDeleteTestFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == folderBrowserDialogTest.ShowDialog())
            {
                string baseDir = folderBrowserDialogTest.SelectedPath;
                string testDir = "TestFiles";
                string result;

                CProgram.sbTestFilesDelete(out result, baseDir, testDir);
                CProgram.sAskOk("Folder Test Files - Delete", result);
            }
        }


        private void programInfoToClipboardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string s = Text + " _ " + CProgram.sGetUserAtPcString();

                if (_mRecordingDir != null && _mRecordingDir.Length > 2)
                {
                    s += " _ " + _mRecordingDir;
                }
                Clipboard.SetText(s);
            }
            catch (Exception)
            {

            }
        }

        private void recordingDeleteTestFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string baseDir = _mRecordingDir;
            string testDir = "TestFiles";
            string result;

            CProgram.sbTestFilesDelete(out result, baseDir, testDir);
            CProgram.sAskOk("Recording Test Files - Delete", result);

        }

        private void openLogFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CProgram.sDoOpenLogFolder();
        }

        private void openLogFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CProgram.sDoOpenLogFile();
        }

        private void openRecorderFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CProgram.sDoOpenFolder(_mRecordingDir);
        }

        private void quickNormalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CStringSet set = new CStringSet();

            if (set != null)
            {
                set.mAddValue(CDvtmsData._cFindings_Normal);

                string t = _mQuickNormText != null && _mQuickNormText.Length > 0 ? _mQuickNormText : "Quick Normal";
                mQuickStat(set, t);
            }

        }

        private void testHttpTzToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string content;


            string url = "http://nl0022demo.dvtms.com:63129/hello";

            if (CLicKeyDev.sbDeviceIsProgrammer())
            {
                if (CProgram.sbReqLabel("Test url", "url", ref url, "", false))
                {
                    bool bOk = CProgram.sbTestHttpConnectFromUrl(out content, url);
                    CProgram.sLogLine("HttpTZ " + url + (bOk ? " ok" : " failed!"));
                    CProgram.sAskOk("Test HttpTz", bOk ? "Test HttpTz succeded" : "Test HttpTz failed");
                }
            }
            else
            {
                CProgram.sAskOk("Test HttpTz", "Test HttpTz not implemented yet");

            }
        }

        private void toolStripButtonWarn_Click(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;
            if (bCtrl)
            {
                toolStripButtonWarn.Text = "c";
                toolStripReaderCheck.Text = "";
                string showText = toolStripButtonWarn.ToolTipText + " @c" + CProgram.sTimeToString(DateTime.Now);
                toolStripButtonWarn.ToolTipText = showText;
                CProgram.sLogLine(showText);
            }
            else
            {
                mCheckReaderScan(true, true, true, true);
            }
            CProgram.sMemoryCleanup(true, true);
            mUpdateMemUse();
        }

        private void labelImageNk_Click(object sender, EventArgs e)
        {
            if (_mImageCach != null)
            {
                labelImageNk.Text = "clr";
                _mImageCach.mRemoveOldImages(DateTime.Now, 0.01);
                CProgram.sMemoryCleanup(true, true);
                mUpdateMemUse();
            }
        }

        private void labelImageNk_DoubleClick(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl)
            {
                if (_mImageCach != null)
                {
                    float hours = _mImageCach.mMaxCachHour;
                    if (CProgram.sbReqFloat("Clear & Set image buffer duration (0=off)", "age", ref hours, "0.00", "hours", 0.0F, 9999))
                    {
                        labelImageNk.Text = "clr" + hours.ToString("0.00");
                        _mImageCach.mMaxCachHour = hours;
                        _mbShowTriageImage = hours >= 0.0099;
                        _mImageCach.mRemoveOldImages(DateTime.Now, hours);
                        CProgram.sMemoryCleanup(true, true);
                        mUpdateMemUse();
                    }
                }
            }
        }

        private void labelMemUsage_Click(object sender, EventArgs e)
        {
            mUpdateMemUse();
        }

        public List<CSqlCountItem> mbCountRows(out UInt32 ArTotalCount, float AAgeHours)
        {
            List<CSqlCountItem> list = null;
            UInt32 nTotal = 0;

            bool bError;
            CSqlDBaseConnection db = CDvtmsData.sGetDBaseConnection();
            CRecordMit rec = new CRecordMit(db);

            if (rec != null && db != null)
            {
                DateTime utcSearch = DateTime.UtcNow.AddHours(-AAgeHours);
                UInt64 flags = 0; // CSqlDataTableRow.sGetMask();
                UInt16 varNr = (UInt16)DRecordMitVars.RecState_IX;
                string varName = rec.mVarGetName((UInt16)DRecordMitVars.ReceivedUTC);
                string where = varName + ">=" + db.mSqlDateTimeSqlString(utcSearch);

                if (false == rec.mbDoSqlCountList(out bError, out nTotal, out list, varNr, flags, true, where))
                {
                    list = null;
                    nTotal = 0;
                }
            }

            ArTotalCount = nTotal;
            return list;
        }

        public string mCountRows(out UInt32 ArTotalCount, out UInt32 ArReceivedCount, out UInt32 ArTriagedCount, out UInt32 ArAnalyzedCount, float AAgeHours)
        {
            string info = "";
            UInt32 nTotal = 0;
            List<CSqlCountItem> list = mbCountRows(out nTotal, AAgeHours);

            UInt32 nReceived = 0;
            UInt32 nTriaged = 0;
            UInt32 nAnalyzed = 0;

            int days = (int)(AAgeHours / 24);
            float hours = AAgeHours - days * 24;


            info = "Event record search (" + days.ToString() + " days " + hours.ToString("0.0") + " hours: ";
            if (list == null)
            {
                info += " returns emty";
            }
            else
            {
                int nList = list.Count;

                info += nTotal.ToString() + " total\r\n";

                for (int i = 0; i < nList; ++i)
                {
                    CSqlCountItem item = list[i];
                    UInt16 var = 0;

                    if (item.mbGetVarUInt16(out var) && var <= (UInt16)DRecState.NrRecStates)
                    {
                        DRecState state = (DRecState)var;
                        info += state.ToString();
                        if (/*state <= DRecState.Processed ||*/ state == DRecState.Received)
                        {
                            nReceived = item._mCount;
                        }
                        else if (state == DRecState.Triaged)
                        {
                            nTriaged = item._mCount;
                        }
                        else if (state == DRecState.Analyzed)
                        {
                            nAnalyzed = item._mCount;
                        }
                    }
                    else
                    {
                        info += "?" + item._mVarValue;
                    }
                    info += "= " + item._mCount.ToString() + "\r\n";
                }
            }

            ArReceivedCount = nReceived;
            ArTriagedCount = nTriaged;
            ArAnalyzedCount = nAnalyzed;
            ArTotalCount = nTotal;
            return info;
        }

        private void mCountShowRows()
        {
            float days = _mTriageCountDays;

            if (CProgram.sbReqFloat("Select how many days all the events are counted", "Last", ref days, "0.00", "days", 0.0F, 9999999F))
            {
                DateTime startUTC = DateTime.UtcNow;

                UInt32 nTotal = 0;
                UInt32 nReceived = 0;
                UInt32 nTriaged = 0;
                UInt32 nAnalyzed = 0;
                string info = mCountRows(out nTotal, out nReceived, out nTriaged, out nAnalyzed, days * 24);
                double sec = (DateTime.UtcNow - startUTC).TotalSeconds;

                info += sec.ToString("0.00") + " sec";
                ReqTextForm.sbShowText("Count Event records", "state counts", info);

            }

        }
        private void labelCountRows_Click(object sender, EventArgs e)
        {
            mCountShowRows();
        }

        private void labelTriageCountDays_Click(object sender, EventArgs e)
        {
            float days = _mTriageCountDays;

            if (CProgram.sbReqFloat("Select how many days the events are counted", "Last", ref days, "0.00", "days", 0.0F, 999999F))
            {
                _mTriageCountDays = days;
            }
            mUpdateTriageCountDays();
        }

        private void textBoxRecordDir_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void labelRecNr_Click(object sender, EventArgs e)
        {

        }

        private void FilterList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void quickVTToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CStringSet set = new CStringSet();

            if (set != null)
            {
                set.mAddValue(CDvtmsData._cFindings_VT);

                string t = _mQuickVtText != null && _mQuickVtText.Length > 0 ? _mQuickVtText : "Quick VT";
                mQuickStat(set, t);
            }

        }

        private void testServerFtpToolStripMenuItem_Click(object sender, EventArgs e)
        {

            string url = "ftps://dvtms.com:63199";

            if (CLicKeyDev.sbDeviceIsProgrammer())
            {
                if (CProgram.sbReqLabel("Test FTP url", "url", ref url, "", false))
                {
                    string content;

                    bool bOk = CProgram.sbTestFtpConnectFromUrl(out content, url);
                    CProgram.sLogLine("serverFTP " + url + (bOk ? " ok" : " failed!"));
                    CProgram.sAskOk("Test server Tz", bOk ? "Test serverFTP succeded" : "Test serverFTP failed");
                }
            }
            else
            {
                CProgram.sAskOk("Test HttpTz", "Test HttpTz not implemented yet");

            }
        }

        private void makeLauncherVersionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CProgram.sbWriteLaunchVersion(true, versionLabel, versionHeader, versionText);
        }

        private void programInfoToClipboardToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            try
            {
                string s = Text + " _ " + CProgram.sGetUserAtPcString();

                if (_mRecordingDir != null && _mRecordingDir.Length > 2)
                {
                    s += " _ " + _mRecordingDir;
                }
                Clipboard.SetText(s);
            }
            catch (Exception)
            {

            }
        }

        private void toolStripMenuItem10_Click(object sender, EventArgs e)
        {
            CProgram.sDoOpenLogFolder();
        }

        private void openContentFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string dir = Path.Combine(CProgram.sGetProgDir(), "Content");
            CProgram.sDoOpenFolder(dir);
        }

        private void labelStudyPermisions_Click(object sender, EventArgs e)
        {
            if(false == _sbStudyPermissionsLocked)
            {
                bool bOk = CDvtmsData.sbEditStudyPermissionsBits( "Triage select permisions", ref _sStudyPermissionsActive);

                _sStudyPermissionsActive.mOrSet(_sStudyPermissionsFixed);
                if ( bOk )
                {
                    checkBoxStudyPermissions.Checked = _sStudyPermissionsActive.mbIsNotEmpty();
                }
                mUpdateStudyPermissions();
            }
        }

        private void labelTrial_Click(object sender, EventArgs e)
        {
            if( false == _sbSelectTrialLocked)
            {
                bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

                CTrialInfoForm trialForm = new CTrialInfoForm(null, _sSelectedTrial, true);

                if (trialForm != null && false == bCtrl)
                {
                    trialForm.ShowDialog();
                    if (trialForm._mbSelected)
                    {
                        _sSelectedTrial = trialForm._mSelectedIndex;
                        checkBoxTrial.Checked = _sSelectedTrial > 0;
                    }
                }
                else
                {
                    if (CProgram.sbReqUInt32("View Triage Trial", "Trial number", ref _sSelectedTrial, "", 0, UInt32.MaxValue))
                    {
                        checkBoxTrial.Checked = _sSelectedTrial > 0;
                    }
                }
                mUpdateTrial();
            }
        }

        private void labelRevertS0_Click(object sender, EventArgs e)
        {

        }

        private void labelClient_Click(object sender, EventArgs e)
        {
            if (false == _sbSelectTrialLocked)
            {
                bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

                CClientInfoForm clientForm = new CClientInfoForm(null, _sSelectedClient, true);

                if (clientForm != null && false == bCtrl)
                {
                    clientForm.ShowDialog();
                    if (clientForm._mbSelected)
                    {
                        _sSelectedClient = clientForm._mSelectedIndex;
                        checkBoxClient.Checked = _sSelectedClient > 0;
                    }
                }
                else
                {
                    if (CProgram.sbReqUInt32("View Triage Client", "Client number", ref _sSelectedClient, "", 0, UInt32.MaxValue))
                    {
                        checkBoxClient.Checked = _sSelectedClient > 0;
                    }
                }
                mUpdateClient();
            }
        }

        private void labelEventStudyInfo_Click(object sender, EventArgs e)
        {
            string s = labelEventStudyInfo.Text;
            int pos = s == null ? -1 : s.IndexOf('&');

            if( pos > 0)
            {
                s = s.Substring(pos + 1);
                Clipboard.SetText(s);
            }
        }

        private void toolStripButtonTrial_Click(object sender, EventArgs e)
        {
            if (CProgram.sbEnterProgUserArea(true))
            {
                CTrialInfoForm form = new CTrialInfoForm(null, 0, false);

                if (form != null)
                {
                    form.ShowDialog();
                }
            }

        }

        private void comboBoxLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
           _sStudyPermissionsActive = CDvtmsData.sGetStudyPermissionsComboBox(comboBoxLocation, CDvtmsData._sStudyPermSiteGroup);
            checkBoxStudyPermissions.Checked = _sStudyPermissionsActive != null && _sStudyPermissionsActive.mbIsNotEmpty();
            mUpdateStudyPermissions();
        }

        private void buttonShowQCd_Click(object sender, EventArgs e)
        {
            mSetTriageView(DTriageViewMode.ListQCd, true);

        }

        private void checkBoxStudyPermissions_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void openChangeHystoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mOpenHystory();
        }

        private void textBoxSelectStudy_DoubleClick(object sender, EventArgs e)
        {
            if( textBoxSelectStudy.Text == null || textBoxSelectStudy.Text.Length == 0)
            {
                textBoxSelectStudy.Text = labelTriageStudy.Text;

            }
        }

        private void changeUserPinToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CDvtmsData.sbChangeUserPW();
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void updWindowSizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mWindowSizeChanged();
        }

        private void labelEventText_Click(object sender, EventArgs e)
        {
            mClickEventLabel();
        }

        private void mChangeDvxRecordLength(UInt32 ARecordIX )
        {
            try
            {
                if (mbCreateDBaseConnection())
                {
                    string user = CProgram.sGetProgUserInitials();
                    CRecordMit rec = new CRecordMit(CDvtmsData.sGetDBaseConnection());

                    if (rec != null && rec.mbDoSqlSelectIndex((UInt32)ARecordIX, rec.mGetValidMask(true)))
                    {
                        string recordPath = CDvtmsData.sGetRecordingCaseDir(rec);

                        if( recordPath != null && recordPath.Length > 3)
                        {
                            string fileName = rec.mFileName;
                            string ext = Path.GetExtension(fileName);
                            string filePath = "";

                            if( ext == CDvxEvtRec._cEcgExtension)
                            {
                                filePath = Path.Combine(recordPath, Path.ChangeExtension(fileName, CDvxEvtRec._cEvtRecExtension));
                            }
                            else if(ext == CDvxEvtRec._cEventExtension)
                            {
                                filePath = Path.Combine(recordPath, Path.ChangeExtension(fileName, CDvxEvtRec._cEvtRecExtension));
                            }
                            else if (ext == CDvxEvtRec._cEvtRecExtension)
                            {
                                filePath = Path.Combine(recordPath, fileName);
                            }
                            else
                            {
                                CProgram.sAskWarning("Change DVX Record", "Record R" + ARecordIX + " is Not a DVX Event Record");
                            }
                            if(filePath.Length > 3)
                                {
                                if( false == File.Exists( filePath ))
                                {
                                    CProgram.sAskWarning("Change DVX Record", "Record R" + ARecordIX + " has no DVX Event Record");
                                }
                                else
                                {
                                    bool bCheckSum;
                                    CDvxEvtRec dvxRec = CDvxEvtRec.sLoadDvxEvtRecFile(out bCheckSum, filePath, false);

                                    if ( dvxRec == null )
                                    {
                                        CProgram.sAskWarning("Change DVX Record", "Record R" + ARecordIX + " has no or bad DVX Event Record file");
                                    }
                                    else
                                    {
                                        string title = "change DVX R" + ARecordIX;
                                        UInt32 preSec = dvxRec._mPreEventSec;
                                        UInt32 postSec = dvxRec._mPostEventSec;

                                        if (dvxRec._mbLoadFromStudy)
                                        {
                                            title += " dvxS" + dvxRec._mStudyNr;
                                        }
                                        title += " {" + preSec + ", " + postSec + " sec} " + dvxRec.mGetNrDatFiles() + " files";

                                        if( CProgram.sbReqUInt32( title, "New strip pre event time", ref preSec, "sec", 0, 999999) 
                                            && CProgram.sbReqUInt32(title, "New strip post event time", ref postSec, "sec", 0, 999999))
                                        {
                                            string[] files = null;
                                            string source = dvxRec._mbLoadFromStudy ? dvxRec._mSourceFilePath : filePath;
                                            DateTime lastUTC;
                                            bool bFoundPrimairy;


                                            dvxRec._mPreEventSec = preSec;
                                            dvxRec._mPostEventSec = postSec;
                                            dvxRec.mClearFileList();

                                            dvxRec.mbDeterminFileRange(ref files, source, out lastUTC, out bFoundPrimairy);

                                            UInt32 n = dvxRec.mGetNrDatFiles();
                                            bool bStoreList = n > 0;

                                            title = "change DVX R" + ARecordIX;
                                            if (dvxRec._mbLoadFromStudy)
                                            {
                                                title += " dvxS" + dvxRec._mStudyNr;
                                            }
                                            title += " new {" + preSec + ", " + postSec + " sec} " 
                                                + (bFoundPrimairy ? "" : "-evt ")
                                                + dvxRec.mGetNrDatFiles() + " files, last=" +CProgram.sDateTimeToYMDHMS( lastUTC );
                                            if ( CProgram.sbReqBool( title, "Use found "+ n + " files (off scan later)", ref bStoreList ))
                                            {
                                                if( false == bStoreList)
                                                {
                                                    dvxRec.mClearFileList();
                                                }
                                                File.Move(filePath, FilePath + CProgram.sDateTimeToYMDHMS(DateTime.Now));

                                                dvxRec.mbSaveFile(filePath);
                                            }
                                        }
                                    }
                                }
                            }

                        }

                        /*                            if (recordPath)



                                                        if (rec.mStudy_IX > 0
                                                            && CProgram.sbAskOkCancel("Revert Event Record " + index.ToString() + " S" + rec.mStudy_IX.ToString() + " by " + user,
                                                            "Revert to Study 0 and state Received?"))
                                                        {
                                                            string addRem = "Revert " + rec.mStudy_IX.ToString() + "." + rec.mSeqNrInStudy.ToString() + " by " + user;
                                                            string clearText = "Reverted by " + user;
                                                            string oldRem = rec.mRecRemark.mDecrypt();
                                                            rec.mStudy_IX = 0;
                                                            rec.mSeqNrInStudy = 0;
                                                            rec.mPatient_IX = 0;
                                                            //rec.mPatientID.mbSetEncrypted(clearText);
                                                            rec.mPatientTotalName.mbSetEncrypted(clearText);
                                                            rec.mRecRemark.mbSetEncrypted(oldRem + "\r\n" + addRem);
                                                            rec.mRecState = (UInt16)DRecState.Received;
                                                            rec.mPatientBirthDate.mbSetEncrypted(0);
                                                            rec._mStudyPermissions = new CBitSet64();
                                                            rec._mStudyTrial_IX = 0;
                                                            rec._mStudyClient_IX = 0;

                                                            CProgram.sLogLine("R#" + index.ToString() + ":" + addRem);

                                                            UInt64 saveMask = rec.mGetValidMask(false);
                                                            if (rec.mbDoSqlUpdate(saveMask, true))
                                                            {
                                                                DataGridViewRow row = dataGridTriage.CurrentRow;

                                                                if (row != null && row.Cells.Count >= (int)DTriageGridField.NrGridFields
                                                                    && row.Cells[(int)DTriageGridField.Index].Value.ToString() == index.ToString())
                                                                {
                                                                    row.Cells[(int)DTriageGridField.PSA].Value = "S0!!\r\nR-" + rec.mIndex_KEY.ToString();
                                                                    row.Cells[(int)DTriageGridField.Patient].Value = clearText;
                                                                    row.Cells[(int)DTriageGridField.Remark].Value += "\r\n" + addRem;
                                                                    row.Cells[(int)DTriageGridField.State].Value = rec.mRecState.ToString("D2") + "\n" + DRecState.Received.ToString();
                                                                    row.Cells[(int)DTriageGridField.Study].Value = "0";
                                                                    mUpdateGridRowState(row);
                                                                }
                                                                else
                                                                {
                                                                    CProgram.sLogLine("Failed to find row of revert Record " + index.ToString());
                                                                }
                                                            }
                                                            else
                                                            {
                                                                CProgram.sLogLine("Failed update Record " + index.ToString());
                                                            }
                                                        }
                                                        else
                                                        {
                                                            CProgram.sLogLine("Failed revert Record " + index.ToString());
                                                        }
                                                }
                                            }
                                            mUpdateTriageStrips();
                                        }
                                    }
                                            else
                                            {
                                        CProgram.sLogLine("Select row in table first.");
                                    }
                                    mTriageUpdateReset();
                                }
                        */
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("ChangeDvxRecordLength", ex);
                }

        }

        private void mClickEventLabel()
        {
            if (_sbTriageOnly)
            {
                return;
            }
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;
            bool bShift = (Control.ModifierKeys & Keys.Shift) == Keys.Shift;

            if (bCtrl && bShift) //&& CLicKeyDev.sbDeviceIsProgrammer())
            {
                int index = 0;
                string oldState = "";

                if (mbGetCursorIndexState(out index, out oldState))
                {
                    if (CProgram.sbEnterProgUserArea(true))
                    {
                        mChangeDvxRecordLength((UInt32)index);
                    }
                }
            }
        }

        private void labelEventText_DoubleClick(object sender, EventArgs e)
        {
            mClickEventLabel();
        }

        private void dataGridTriage_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if( e != null )
            {
                int rowIndex = e.RowIndex;
                bool bView = _mCurrentTriageView == DTriageViewMode.Triage || _mCurrentTriageView == DTriageViewMode.Analyze 
                    || _mCurrentTriageView == DTriageViewMode.ListReceived;

                if (rowIndex >= 0 && bView)
                {
                    if (rowIndex >= 0 && e.ColumnIndex == (int)DTriageGridField.EventStrip)
                    {
                        try
                        {
                            e.Paint(e.ClipBounds, e.PaintParts);

                            Object remObject = dataGridTriage.Rows[rowIndex].Cells[(int)DTriageGridField.Remark].Value;

                            if (remObject != null)
                            {
                                string remText = remObject.ToString();
                                string xStr = CProgram.sExtractValueLabel(remText, "X");
                                string xInfo = "";

                                if (xStr.Length > 0 && CCardioLogsAPI.sbMakeTriageText( out xInfo, xStr ))
                                {
                                    int x = e.CellBounds.X; if (_mTriageImageFormat.Alignment == StringAlignment.Far) x += e.CellBounds.Width-2;
                                    else if (_mTriageImageFormat.Alignment == StringAlignment.Center) x += e.CellBounds.Width / 2;

                                    int y = e.CellBounds.Y; if (_mTriageImageFormat.LineAlignment == StringAlignment.Far) y += e.CellBounds.Height;
                                    else if (_mTriageImageFormat.LineAlignment == StringAlignment.Center) y += e.CellBounds.Height / 2;

                                    e.Graphics.DrawString(xInfo, _mTriageImageFont, _mTriageImageBrush, x, y, _mTriageImageFormat);
                                }
                            }
                            e.Handled = true;                           
                        }
                        catch (Exception ex)
                        {
                            CProgram.sLogException("PaintGrid[" + rowIndex + ", " + e.ColumnIndex + "]", ex);
                        }
                    }
                }
            }
        }

        private void quickAbNormalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CStringSet set = new CStringSet();

            if (set != null)
            {
                set.mAddValue(CDvtmsData._cFindings_AbNormal);

                string t = _mQuickAbnText != null && _mQuickAbnText.Length > 0 ? _mQuickAbnText : "Quick AbNormal";
                mQuickStat(set, t);
            }
        }

        private void openDownloadLinksToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void openLicenseInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CLicKeyDev.sbReopenLicenseRequest();
        }

        private void startTMIHelpdeskToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mOpenHelpDesk(false);
        }

        private void startStandardTeamViewerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mOpenHelpDesk(true);
        }

        private void quickTachyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CStringSet set = new CStringSet();

            if (set != null)
            {
                set.mAddValue(CDvtmsData._cFindings_Tachy);

                string t = _mQuickTachyText != null && _mQuickTachyText.Length > 0 ? _mQuickTachyText : "Quick Tachy";
                mQuickStat(set, t);
            }
        }

        private void quickBradyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CStringSet set = new CStringSet();

            if (set != null)
            {
                set.mAddValue(CDvtmsData._cFindings_Brady);

                string t = _mQuickBradyText != null && _mQuickBradyText.Length > 0 ? _mQuickBradyText : "Quick Brady";
                mQuickStat(set, t);
            }
        }

        private void quickAFIBToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CStringSet set = new CStringSet();

            if (set != null)
            {
                set.mAddValue(CDvtmsData._cFindings_AF);

                string t = _mQuickAFibText != null && _mQuickAFibText.Length > 0 ? _mQuickAFibText : "Quick AFib";
                mQuickStat(set, t);
            }
        }

        private void quickOtherToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CStringSet set = new CStringSet();

            if (set != null)
            {
                set.mAddValue(CDvtmsData._cFindings_Other);

                string t = _mQuickOtherText != null && _mQuickOtherText.Length > 0 ? _mQuickOtherText : "Quick Other";
                mQuickStat(set, t);
            }
        }

        /// <summary>
        /// Creates the list of checkboxes for the filters
        /// </summary>
        private void mFillTzFilterList()
        {
            CheckedListBox.ObjectCollection filterList = FilterList.Items;
            filterList.Add(new InvertFilter(), false);
            filterList.Add(new HighPassFilter(), sbTzQuickFilter);
        }

        /// <summary>
        /// Sets a public list of filters to be used when reading in data
        /// </summary>
        private void mSetTzFilters()
        {
            CTzQuickFilter.sTzQuickFilters = new List<Filter>();
            for (int i = 0; i < FilterList.Items.Count; i++)
            {
                Filter filter = (Filter)FilterList.Items[i];
                filter.Enabled = FilterList.GetItemChecked(i);
                CTzQuickFilter.sTzQuickFilters.Add(filter);
            }
        }

        public static bool sbGetPrintUserInitials( string ATitle, out string ArPrintInitials, bool AbUseCurrent, string ADocInitials)
        {
            bool bOk = true;
            string initials = "";

// only on print            if (false == _sbPrintHideTechName)
            {
                initials = CProgram.sGetProgUserInitials();

                if (initials == null || initials.Length == 0)
                {
                    initials = ADocInitials;
                }
                if (false == AbUseCurrent)
                {
                    if( ADocInitials != null && ADocInitials.Length > 0 )
                    {
                        initials = ADocInitials;
                    }
                    if (_sbPrintForceTechName)
                    {
                        initials = CProgram.sGetProgUserInitials();
                    }
                    if (_sbPrintAskTechName)
                    {
                        bOk = CProgram.sbReqLabel("Print pdf " + ATitle + ": set Technichian initials", "Technichian initials", ref initials, "", false);
                    }
                }
            }
            ArPrintInitials = initials;
            return bOk;
        }
    }
}
