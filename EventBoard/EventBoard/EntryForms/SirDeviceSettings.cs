﻿using Event_Base;
using EventBoard.EntryForms;
using EventBoard.Event_Base;
using Program_Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SironaDevice
{
    public partial class SironaDevSet : Form
    {
        public CSironaStateSettings _mSettings = null;
        public CSironaStateSettings _mStatus = null;

        public string _mSerialNr;
        public string _mSironaDeviceRoot;
        public string _mUnitDevicePath;
        private CDeviceInfo _mDeviceInfo = null;

        public static Image _sImageEqual = null;
        public static Image _sImageNotEqual = null;

        private CSirAutoCollect _mCollect = null;
        private bool _mbHolterActive = false;

        private DateTime _mLastTestRunUTC = DateTime.MinValue;

        DateTime _mStatusFileDT = DateTime.MinValue;

        /*        public const string _cSettingNumberExt = "settingNumber";
                public const string _cActionNumberExt = "actionNumber";
                public const string _cSettingFileExt = "xxs";
                public const string _cActionFileExt = "xxe";
                public const string _cSettingSdcardName = "config";
                public const string _csToServerName = "ToServer";
        */
        public SironaDevSet(string ADeviceSerial, CDeviceInfo ADeviceInfo)
        {
            try
            {
                _mSerialNr = ADeviceSerial;
                _mDeviceInfo = ADeviceInfo;

                if (_mSerialNr != null && _mSerialNr.Length > 0 && CSironaStateSettings.sbGetSironaDevicePath(out _mSironaDeviceRoot, null))
                {
                    if (CSironaStateSettings.sbGetSironaDevicePath(out _mUnitDevicePath, _mSerialNr))
                    {
                        _mSettings = new CSironaStateSettings();
                        _mStatus = new CSironaStateSettings();

                        if (_mSettings != null && _mStatus != null)
                        {
                            InitializeComponent();

                            _mSettings.mClear(_mSerialNr, "");

                            mSetResultText(true, "Defaults set");

                            bool bDevice = false;
                            bool bOk = mbLoadDeviceSettings(out bDevice, false);

                            mSetResultText(bOk, bDevice ? "loading last settings for " + _mSerialNr : "loading default settings");

                            if (_sImageEqual == null) _sImageEqual = pictureBoxEqual.InitialImage;
                            if (_sImageNotEqual == null) _sImageNotEqual = pictureBoxNotEqual.InitialImage;

                            mUpdateFileTime();
                            mFillEditForm();
                            //mUpdateFileTime();
                            labelDeviceSnr.Text = ADeviceSerial;

                            Text = CProgram.sMakeProgTitle("Sirona Setup " + ADeviceSerial, false, true);

                            if (CSironaStateSettings._sbUseQueue)
                            {
                                labelSetting.Text = "Command queue active.";
                                buttonClearQueue.Visible = true;
                            }
                            _mbHolterActive = CSironaStateSettings.sbIsHolterActive();
                            if (_mbHolterActive)
                            {
                                mbLoadCollect();

                                bool bAutoCollect = _mCollect != null && _mCollect._mbEnabled;

                                checkBoxAutoCollect.Checked = bAutoCollect;
                                panelAutoCollect.Visible = bAutoCollect;
                                mbFillCollect();
                            }
                            else
                            {
                                checkBoxAutoCollect.Visible = false;
                                panelAutoCollect.Visible = false;

                            }
                            mbFillStudyActive();
                        }
                    }
                    else
                    {
                        CProgram.sPromptError(true, "Sirona Device Form", "No Sirona device directory for " + _mSerialNr);
                        Close();
                    }
                }
                else
                {
                    Close();
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Init SirDevice Form", ex);
            }
        }
        public void mSetResultText(bool AbOk, string AText)
        {
            string s = "";
            if (AText != null && AText != "")
            {
                s = CProgram.sTimeToString(DateTime.Now);
                s += AbOk ? " Ok: " : " Failed: ";
            }
            labelResult.Text = s + AText;
            labelResult.BackColor = AbOk ? Color.Green : Color.Red;
        }
        public void mSetResultInfoText(string AText)
        {
            labelResult.Text = CProgram.sTimeToString(DateTime.Now) + ": "  + AText;
            labelResult.BackColor = Color.DarkOrange;
        }

        private string mMakeDHMS( UInt32 ANrSec)
        {
            string s = "0";

            if( ANrSec > 0 )
            {
                UInt32 days, hours, min, sec;

                min = ANrSec / 60;
                sec = ANrSec % 60;

                hours = min / 60;
                min = min % 60;

                days = hours / 24;
                hours = hours % 24;

                s = days.ToString() + "d " + hours.ToString() + ":" + min.ToString("D2") + ":" + sec.ToString("D2");
            }
            return s;
        }

        private void mFillStatusForm()
        {
            textBoxDevice.Text = _mStatus._mDeviceSerial;
            textBoxModel.Text = _mStatus._mModelNumber;
            textBoxFirmware.Text = _mStatus._mFirmwareVersion;
            textBoxAppVersion.Text = _mStatus._mAppVersion;
            textBoxServerUrl.Text = _mStatus._mServerBaseUrl;
            textBoxBatLevel.Text = _mStatus._mBatteryLevel.ToString();
            comboBoxSource.Text = _mStatus._mUpdateSource;
            textBoxCable.Text = _mStatus._mCableType;

            labelResultError.Text = _mStatus._mStateResult + " " +_mStatus._mStateError;

            string s = "";
            UInt32 value = _mStatus._mLeadOffBits;
            UInt32 mask = 1;
            for( int i = 0; i < 32; ++i )
            {
                if( 0 != (value & mask))
                {
                    s += i.ToString() + " ";
                }
                mask <<= 1;
            }

            textBoxLeadOff.Text = s;
            textBoxStartTime.Text = CProgram.sDateTimeToString( _mStatus._mProcedureStartTimeDTO.LocalDateTime ) + CProgram.sTimeZoneOffsetStringLong((Int16)_mStatus._mProcedureStartTimeDTO.Offset.TotalMinutes);
            textBoxProcLength.Text = mMakeDHMS(_mStatus._mProcedureLengthSec);
            labelProcEnd.Text = _mStatus._mProcedureStartTimeDTO.AddSeconds(_mStatus._mProcedureLengthSec).ToString("yyyy-MM-dd HH:mm:ss");
            textBoxProcType.Text = _mStatus._mProcedureType;
            textBoxProcState.Text = _mStatus._mProcedureState;
            textBoxCommunicationDelay.Text = _mStatus._mCommunicationDelay.ToString();
            textBoxAddress.Text = _mStatus._mDevice_address;
        }

        private void mFillEditForm()
        {
             textBoxPatientID.Text = _mSettings._mPatientId;
            textBoxFirst.Text = _mSettings._mPatientFirstName;
            textBoxMiddle.Text = _mSettings._mPatientMiddleName;
            textBoxLast.Text = _mSettings._mPatientLastName;
            textBoxDOB.Text = _mSettings._mPatientDOB == DateTime.MinValue ? "" : CProgram.sDateToString(_mSettings._mPatientDOB );
            textBoxPhysisian.Text = _mSettings._mPhysicianName;
            textBoxComment.Text = _mSettings._mPatientComment;
            textBoxBradyOnset.Text = _mSettings._mBradyThresholdBpm.ToString();
            textBoxTachyOnset.Text = _mSettings._mTachyThresholdBpm.ToString();
            textBoxPauseOnset.Text = CProgram.sCorrectFloatProgToUser( _mSettings._mPauseThresholdSec.ToString("0.0"));
            checkBoxAfib.Checked = _mSettings._mbAfibOn;
            comboBoxPreTrigger.Text = _mSettings._mPreTriggerSec.ToString();
            comboBoxPostTrigger.Text = _mSettings._mPostTriggerSec.ToString();
            textBoxRecordLength.Text = _mSettings._mRecordLengthDays.ToString();
            comboBoxSampleRate.Text = _mSettings._mSampleRate.ToString();
            checkBoxTTX.Checked = _mSettings._mbTtm3x;
            checkBoxUiLock.Checked = _mSettings._mbUiLock;
            textBoxAutoLimit.Text = _mSettings._mAutoEventLimit.ToString();
            textBoxManualLimit.Text = _mSettings._mManualEventLimit.ToString();
            comboBoxCommand.Text = _mSettings._mOperationCommand;
            checkBoxAnnotation.Checked = _mSettings._mbReqAnotations;
            checkBoxECG.Checked = _mSettings._mbReqEcg;
            comboBoxStartType.Text = _mSettings._mOperationType;

                textBoxStartLength.Text = _mSettings._mReqLengthSec.ToString();

            try
            {

                dateTimePickerStart.MinDate = _mStatus._mProcedureStartTimeDTO.DateTime;
                Int32 startSec = (Int32)_mSettings._mProcedureLengthSec - (Int32)_mSettings._mReqLengthSec - 60;
                DateTime dt = _mStatus._mProcedureStartTimeDTO.DateTime == DateTime.MinValue ? DateTime.Now.AddSeconds(_mSettings._mReqLengthSec - 60 ) 
                        : _mStatus._mProcedureStartTimeDTO.DateTime.AddSeconds(startSec <= 0 ? 0 : startSec);
                dateTimePickerStart.Value = dt;

                labelEndQuiry.Text = dt.AddSeconds(_mSettings._mReqLengthSec).ToString("yyyy-MM-dd HH:mm:ss");

                string txt = "No start";

                if ((_mStatus._mProcedureStartTimeDTO - DateTime.Now).TotalDays >= -3650)
                {
                    double nrSec = (dt - _mStatus._mProcedureStartTimeDTO).TotalSeconds;
                    txt = mMakeDHMS((uint)(nrSec+0.5));
                }
                labelStartAtTime.Text = txt;
            }
            catch (Exception)
            {

            }
            
        }

        private void mUpdateOneIcon( PictureBox ArPictureBox, bool AbState)
        {
            Image img = AbState ?_sImageEqual : _sImageNotEqual;

            if( ArPictureBox.Image != img )
            {
                ArPictureBox.Image = img;
            }          
        }
        private void mUpdateCompareIcons()
        {
            // status lines
            mUpdateOneIcon(pictureBoxDevice, _mSettings._mDeviceSerial  != null && _mSettings._mDeviceSerial .Length > 0
                && _mStatus._mDeviceSerial != null && _mStatus._mDeviceSerial.Length > 0
                && _mSettings._mDeviceSerial == _mSerialNr
                && _mSettings._mDeviceSerial == _mStatus._mDeviceSerial);
            mUpdateOneIcon(pictureBoxModel, _mSettings._mModelNumber == _mStatus._mModelNumber);
            mUpdateOneIcon(pictureBoxFirmware, _mSettings._mFirmwareVersion == _mStatus._mFirmwareVersion);
            mUpdateOneIcon(pictureBoxAppVersion, _mSettings._mAppVersion == _mStatus._mAppVersion);
            mUpdateOneIcon(pictureBoxServerUrl, _mSettings._mServerBaseUrl == _mStatus._mServerBaseUrl);
            mUpdateOneIcon(pictureBoxBatLevel, _mStatus._mBatteryLevel >= 20); // _mSettings._mBatteryLevel == _mStatus._mBatteryLevel);
            mUpdateOneIcon(pictureBoxSource, _mSettings._mUpdateSource == _mStatus._mUpdateSource);
            mUpdateOneIcon(pictureBoxCable, _mSettings._mCableType == _mStatus._mCableType);
            mUpdateOneIcon(pictureBoxLeadOff, _mSettings._mLeadOffBits == _mStatus._mLeadOffBits);
            mUpdateOneIcon(pictureBoxPatientID, _mSettings._mPatientId == _mStatus._mPatientId);
            mUpdateOneIcon(pictureBoxFirst, _mSettings._mPatientFirstName == _mStatus._mPatientFirstName);
            mUpdateOneIcon(pictureBoxMiddle, _mSettings._mPatientMiddleName == _mStatus._mPatientMiddleName);
            mUpdateOneIcon(pictureBoxLast, _mSettings._mPatientLastName == _mStatus._mPatientLastName);
            mUpdateOneIcon(pictureBoxDOB, _mSettings._mPatientDOB == _mStatus._mPatientDOB);
            mUpdateOneIcon(pictureBoxPhysisian, _mSettings._mPhysicianName == _mStatus._mPhysicianName);
            mUpdateOneIcon(pictureBoxComment, _mSettings._mPatientComment == _mStatus._mPatientComment);
            mUpdateOneIcon(pictureBoxStartTime, _mSettings._mProcedureStartTimeDTO == _mStatus._mProcedureStartTimeDTO);
            mUpdateOneIcon(pictureBoxProcLength, _mSettings._mProcedureLengthSec <= _mStatus._mProcedureLengthSec);
            mUpdateOneIcon(pictureBoxProcType, _mSettings._mProcedureType == _mStatus._mProcedureType);
            mUpdateOneIcon(pictureBoxProcState, _mSettings._mProcedureState == _mStatus._mProcedureState);
            mUpdateOneIcon(pictureBoxCommunicationDelay, _mSettings._mCommunicationDelay == _mStatus._mCommunicationDelay);
            mUpdateOneIcon(pictureBoxAddress, _mSettings._mDevice_address == _mStatus._mDevice_address); 
            mUpdateOneIcon(pictureBoxBradyOnset, _mSettings._mBradyThresholdBpm == _mStatus._mBradyThresholdBpm);
            mUpdateOneIcon(pictureBoxTachyOnset, _mSettings._mTachyThresholdBpm == _mStatus._mTachyThresholdBpm);
            mUpdateOneIcon(pictureBoxAfib, _mSettings._mbAfibOn == _mStatus._mbAfibOn);
            mUpdateOneIcon(pictureBoxPauseOnset, _mSettings._mPauseThresholdSec == _mStatus._mPauseThresholdSec);
            mUpdateOneIcon(pictureBoxPreTrigger, _mSettings._mPreTriggerSec == _mStatus._mPreTriggerSec);
            mUpdateOneIcon(pictureBoxPostTrigger, _mSettings._mPostTriggerSec == _mStatus._mPostTriggerSec);
            mUpdateOneIcon(pictureBoxRecordLength, _mSettings._mRecordLengthDays == _mStatus._mRecordLengthDays);
            mUpdateOneIcon(pictureBoxAudio, _mSettings._mbAudioMute == _mStatus._mbAudioMute);
            mUpdateOneIcon(pictureBoxSampleRate, _mSettings._mSampleRate == _mStatus._mSampleRate);
            mUpdateOneIcon(pictureBoxTTX, _mSettings._mbTtm3x == _mStatus._mbTtm3x);
            mUpdateOneIcon(pictureBoxUiLock, _mSettings._mbUiLock == _mStatus._mbUiLock);
            mUpdateOneIcon(pictureBoxAutoLimit, _mSettings._mAutoEventLimit == _mStatus._mAutoEventLimit);
            mUpdateOneIcon(pictureBoxManualLimit, _mSettings._mManualEventLimit == _mStatus._mManualEventLimit);

            string error = "";
            mbParseReqTime(ref error, false);
        }

        private void mParseCheckCommand( string AOperation, ref string ArErr)
        {
            DSironaOperation operation = CSironaStateSettings.sParseOperation(AOperation);

            if( operation == DSironaOperation.Unknown)
            {
                operation = DSironaOperation.None;
            }
            else if (operation == DSironaOperation.DataRequest)
            {
                if( _mSettings._mReqLengthSec > CSironaStateSettings.sGetMaxReqLengthSec())
                {
                    ArErr += "Request Length, ";
                }
                // test if block is beond current recording
            }

            _mSettings._mOperationMode = operation;
            _mSettings._mOperationCommand = CSironaStateSettings.sGetOperationLabel(operation);
        }

        private bool mbReadForm()
        {
            bool bOk = false;
            string err = "";

            try
            {
                string s;
                DateTime dob = DateTime.MinValue;
                UInt16 i = 0;
                float f = 0;

                _mSettings._mDeviceSerial = _mSerialNr;
                _mSettings._mPatientId = textBoxPatientID.Text.Trim();
                _mSettings._mPatientFirstName = textBoxFirst.Text.Trim();
                _mSettings._mPatientMiddleName = textBoxMiddle.Text.Trim();
                _mSettings._mPatientLastName = textBoxLast.Text.Trim();

                s = textBoxDOB.Text.Trim();
                if (s == null || s.Length == 0)
                {
                    _mSettings._mPatientDOB = DateTime.MinValue;
                }
                else if (CProgram.sbParseDateUser(s, ref dob))
                {
                    _mSettings._mPatientDOB = dob;
                }
                else if (CProgram.sbParseDateLocal(s, ref dob))
                {
                    _mSettings._mPatientDOB = dob;
                }
                /*                else if (CProgram.sbParseDtLocal(s, out dob))
                                                {
                                                    _mSettings._mPatientDOB = dob;
                                                }
                                */
                else
                {
                    err += "DOB, ";
                }

                _mSettings._mPhysicianName = textBoxPhysisian.Text.Trim();
                _mSettings._mPatientComment = textBoxComment.Text;

                if (UInt16.TryParse(textBoxBradyOnset.Text, out i)) { _mSettings._mBradyThresholdBpm = i; } else { err += "Brad, "; }

                if (UInt16.TryParse(textBoxTachyOnset.Text, out i)) { _mSettings._mTachyThresholdBpm = i; } else { err += "Tachy, "; }
                _mSettings._mbAfibOn = checkBoxAfib.Checked;
                if (CProgram.sbParseFloat(textBoxPauseOnset.Text, ref f)) { _mSettings._mPauseThresholdSec = f; } else { err += "Pause, "; }
                if (UInt16.TryParse(comboBoxPreTrigger.Text, out i)) { _mSettings._mPreTriggerSec = i; } else { err += "Pre Trigger, "; }
                if (UInt16.TryParse(comboBoxPostTrigger.Text, out i)) { _mSettings._mPostTriggerSec = i; } else { err += "Post Triger, "; }
                if (UInt16.TryParse(textBoxRecordLength.Text, out i)) { _mSettings._mRecordLengthDays = i; } else { err += "Record Length, "; }
                _mSettings._mbAudioMute = checkBoxAudio.Checked;

                if (UInt16.TryParse(comboBoxSampleRate.Text, out i)) { _mSettings._mSampleRate = i; } else { err += "Sample Rate, "; }
                _mSettings._mbTtm3x = checkBoxTTX.Checked;
                _mSettings._mbUiLock = checkBoxUiLock.Checked;
                if (UInt16.TryParse(textBoxAutoLimit.Text, out i)) { _mSettings._mAutoEventLimit = i; } else { err += "Auto limit, "; }
                if (UInt16.TryParse(textBoxManualLimit.Text, out i)) { _mSettings._mManualEventLimit = i; } else { err += "Manual limit, "; }

                _mSettings._mbReqAnotations = checkBoxAnnotation.Checked;
                _mSettings._mbReqEcg = checkBoxECG.Checked;

                _mSettings._mOperationType = comboBoxStartType.Text.Trim();

                mParseCheckCommand(comboBoxCommand.Text, ref err);


                if (_mSettings._mTachyThresholdBpm != 0)
                {
                    _mSettings._mTachyThresholdBpm -= (UInt16)(_mSettings._mTachyThresholdBpm % 5);
                    if (_mSettings._mTachyThresholdBpm < 120) _mSettings._mTachyThresholdBpm = 120;
                    if (_mSettings._mTachyThresholdBpm > 240) _mSettings._mTachyThresholdBpm = 240;
                }
                if (_mSettings._mBradyThresholdBpm != 0)
                {
                    _mSettings._mBradyThresholdBpm -= (UInt16)(_mSettings._mBradyThresholdBpm % 5);
                    if (_mSettings._mBradyThresholdBpm < 15) _mSettings._mBradyThresholdBpm = 15;
                    if (_mSettings._mBradyThresholdBpm > 60) _mSettings._mBradyThresholdBpm = 60;
                }
                if (_mSettings._mPauseThresholdSec != 0)
                {
                    _mSettings._mPauseThresholdSec = (float)((UInt16)(_mSettings._mPauseThresholdSec * 2)) * 0.5F;
                    if (_mSettings._mPauseThresholdSec < 1.5) _mSettings._mPauseThresholdSec = 1.5F;
                    if (_mSettings._mPauseThresholdSec > 100) _mSettings._mPauseThresholdSec = 10;
                }
                i = (UInt16)(_mSettings._mPreTriggerSec / 15);

                _mSettings._mPreTriggerSec = (UInt16)(i * 15);
                if (_mSettings._mPreTriggerSec == 75) _mSettings._mPreTriggerSec = 90;
                if (_mSettings._mPreTriggerSec == 105) _mSettings._mPreTriggerSec = 120;
                if (_mSettings._mPreTriggerSec > 120 && _mSettings._mPreTriggerSec < 180) _mSettings._mPreTriggerSec = 180;
                if (_mSettings._mPreTriggerSec > 180) _mSettings._mPreTriggerSec = 300;

                i = (UInt16)(_mSettings._mPostTriggerSec / 15);
                _mSettings._mPostTriggerSec = (UInt16)(i * 15);
                if(_mSettings._mPostTriggerSec < 15 ) _mSettings._mPostTriggerSec = 15;
                if (_mSettings._mPostTriggerSec == 75) _mSettings._mPostTriggerSec = 90;
                if (_mSettings._mPostTriggerSec == 105) _mSettings._mPostTriggerSec = 120;
                if (_mSettings._mPostTriggerSec > 120 && _mSettings._mPostTriggerSec < 180) _mSettings._mPreTriggerSec = 180;
                if (_mSettings._mPostTriggerSec > 180) _mSettings._mPostTriggerSec = 300;

                if (_mSettings._mRecordLengthDays < 1) _mSettings._mRecordLengthDays = 1;
                if (_mSettings._mRecordLengthDays > 30) _mSettings._mRecordLengthDays = 30;

                if (_mSettings._mSampleRate < 128) _mSettings._mSampleRate = 128;
                if (_mSettings._mSampleRate > 128) _mSettings._mSampleRate = 256;

                if (_mSettings._mAutoEventLimit < 1) _mSettings._mAutoEventLimit = 1;
                if (_mSettings._mAutoEventLimit > 20)
                {
                    if (_mSettings._mAutoEventLimit <= 50) _mSettings._mAutoEventLimit = 50;
                    else if (_mSettings._mAutoEventLimit <= 100) _mSettings._mAutoEventLimit = 100;
                    else _mSettings._mAutoEventLimit = 1000;
                }

                if (_mSettings._mManualEventLimit < 1) _mSettings._mManualEventLimit = 1;
                if (_mSettings._mManualEventLimit > 20)
                {
                    if (_mSettings._mManualEventLimit <= 50) _mSettings._mManualEventLimit = 50;
                    else if (_mSettings._mManualEventLimit <= 100) _mSettings._mManualEventLimit = 100;
                    else _mSettings._mManualEventLimit = 1000;
                }

                mbParseReqTime(ref err, _mSettings._mOperationMode == DSironaOperation.DataRequest );

                bOk = err.Length == 0;
                if (false == bOk)
                {
                    err = err.Substring(0, err.Length - 2); // remove last ', '
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Read Sir Settings form failed", ex);
            }
            mSetResultText(bOk, err);
            return bOk;
        }

        bool mbParseReqTime( ref string ArError, bool AbAddError)
        {
            bool bOk = true;

            try
            {
                bool bIsHolter = false;
                UInt32 maxLengthSec = 0;

                UInt16 i;
                if (UInt16.TryParse(textBoxStartLength.Text, out i))
                {
                    bIsHolter = CSironaStateSettings.sbIsHolter(i);
                    maxLengthSec = bIsHolter ? CSironaStateSettings.sGetHolterReqLengthSec() : CSironaStateSettings.sGetMaxReqLengthSec();
                    _mSettings._mReqLengthSec = (UInt16)(i > maxLengthSec ? maxLengthSec : i );
                }
                else
                {
                    bOk = false;
                    ArError += "ReqLength, ";
                }

                DateTime dt = dateTimePickerStart.Value;

                string txtStart = "No start";
                string txtEnd = "!";

                if ((_mStatus._mProcedureStartTimeDTO.DateTime - DateTime.Now).TotalDays >= -3650)
                {
                    int startSec = (int)( (dt - _mStatus._mProcedureStartTimeDTO.DateTime).TotalSeconds + 0.5);

                    if(startSec < 0 )
                    {
                        dateTimePickerStart.Value = _mStatus._mProcedureStartTimeDTO.DateTime;
                        startSec = 0;
                    }
                    _mSettings._mReqStartAtSec = (UInt32)startSec;
                    if (startSec + _mSettings._mReqLengthSec > _mStatus._mProcedureLengthSec)
                    {
                        bOk = false;
                        if(AbAddError) ArError += "ReqPastEnd, ";
                        txtStart = "Past End";
                        txtEnd = "!"+ dt.AddSeconds(_mSettings._mReqLengthSec).ToString("yyyy-MM-dd HH:mm:ss");
                    }
                    else
                    {
                        txtStart = mMakeDHMS((uint)(_mSettings._mReqStartAtSec + 0.5));
                        txtEnd = dt.AddSeconds(_mSettings._mReqLengthSec).ToString("yyyy-MM-dd HH:mm:ss");
                    }
                }
                else
                {
                    if (AbAddError) ArError += "NoProcStart, ";

                    bOk = false;
                }
                labelStartAtTime.Text = txtStart;
                labelEndQuiry.Text =(  bIsHolter ? "H " : "" ) +txtEnd;
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Parse Request Time", ex);
            }

            return AbAddError ? bOk : true;
        }

        public bool mbCheckSettingsFile(out string ArFileInfo)
        {
            bool bOk = false;

            if( CSironaStateSettings._sbUseQueue)
            {
                bOk = mbCheckSettingsFileQueue(out ArFileInfo);

            }
            else
            {
                bOk = mbCheckSettingsFileNoQueue(out ArFileInfo);
            }

            return bOk;
        }
            public bool mbCheckSettingsFileNoQueue(out string ArFileInfo)
        {
            bool bOk = false;
            string s = "";
            string name;

            if (_mUnitDevicePath != null && _mSerialNr != null && _mSerialNr.Length > 0 && CSironaStateSettings.sbMakeSettingsFileName(out name, _mSerialNr))
            {
                string fileName = Path.Combine(_mUnitDevicePath, name);
                string toDir = Path.Combine(_mUnitDevicePath, CSironaStateSettings._csToServerName);
                string toFileName = Path.Combine(toDir, name);

                try
                {
                    if (File.Exists(fileName))
                    {
                        DateTime dt = File.GetLastWriteTime(fileName);

                        if (dt.Year > 2000)
                        {
                            s += CProgram.sDateTimeToString(dt);
                            bOk = true;
                        }

                        if (File.Exists(toFileName))
                        {
                            s += " uploading";
                        }
                    }
                    else
                    {
                        s += "not present";
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Failed collect info for " + fileName, ex);
                }

            }
            ArFileInfo = s;

            return bOk;
        }
        public bool mbCheckSettingsFileQueue(out string ArFileInfo )
        {
            bool bOk = false;
            string s = "";
            UInt32 nrInQueue = 0;
            DateTime oldDT = DateTime.MaxValue;

            bOk = CSironaStateSettings.sbCheckSettingsFileQueue(out s, out nrInQueue, out oldDT, _mSerialNr);

            ArFileInfo = s;

            return bOk;
        }

        private void mbClearQueue()
        {
            bool bOk = false;
            string s;
            UInt32 nrInQueue = 0;
            string devicePath;

            bOk = CSironaStateSettings.sbClearQueue(out s, _mSerialNr);

            mSetResultText(bOk, s);
            mUpdateFileTime();
        }

        public bool mbCheckLoadStatusFile(out string ArFileInfo)
        {
            bool bOk = false;
            string s = "";
            string name;

            if (_mUnitDevicePath != null && _mSerialNr != null && _mSerialNr.Length > 0 && CSironaStateSettings.sbMakeStatusFileName(out name, _mSerialNr))
            {

                //string fileName = Path.Combine(_mDevicePath, name);
                string fromDir = Path.Combine(_mUnitDevicePath, CSironaStateSettings._csFromServerName);
                string fromFileName = Path.Combine(fromDir, name);

                try
                {
                    if (File.Exists(fromFileName))
                    {
                        DateTime dt = File.GetLastWriteTime(fromFileName);

                        if (dt.Year > 2000)
                        {
                            s += CProgram.sDateTimeToString(dt);

                            UInt32 sec = (UInt32)(DateTime.Now - dt).TotalSeconds;

                            if( sec >= 60 )
                            {
                                s += " << " + CProgram.sPrintTimeSpan_dhmSec(sec, "");
                            }
                            bOk = true;
                        }

                        _mStatus.mClear("?", "?");
                        if (false == _mStatus.mbReadFile(fromFileName))
                        {
                            s += ", failed load!";
                        }
                        else
                        {
                            _mStatusFileDT = dt;
                        }
                    }
                    else
                    {
                        s += "Not present";
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Failed collect info for " + fromFileName, ex);
                }

            }
            ArFileInfo = s;

            return bOk;
        }

        private void mUpdateFileTime()
        {
            string settingStr = "";
            string statusStr = "";

            mbCheckSettingsFile(out settingStr);
            mbCheckLoadStatusFile(out statusStr);
            mFillStatusForm();
            mUpdateCompareIcons();

            labelSettingFileInfo.Text = settingStr;
            labelActionFileInfo.Text = statusStr;
        }
        public bool mbLoadDeviceSettings(out bool AbDeviceLoaded, bool AbLoadDefaults)
        {
            bool bOk = false;

            AbDeviceLoaded = true;
            string fileName = "";

            try
            {
                if (CSironaStateSettings.sbMakeSettingsFileName(out fileName, _mSerialNr))
                {
                    fileName = Path.Combine(_mUnitDevicePath, fileName);

                    if (AbLoadDefaults || false == File.Exists(fileName))
                    {
                        AbDeviceLoaded = false;
                        CSironaStateSettings.sbMakeSettingsFileName(out fileName, CSironaStateSettings._cDefaultsName);

                        fileName = Path.Combine(_mSironaDeviceRoot, fileName);
                    }
                    _mSettings.mClear(_mSerialNr, "");
                    bOk = _mSettings.mbReadFile(fileName);
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Read failed from " + fileName, ex);
                bOk = false;
            }
            _mSettings._mDeviceSerial = _mSerialNr;

            return bOk;
        }
        private void label31_Click(object sender, EventArgs e)
        {

        }

        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label42_Click(object sender, EventArgs e)
        {

        }

        private void panel11_Paint(object sender, PaintEventArgs e)
        {

        }

        private void labelServerSettings_Click(object sender, EventArgs e)
        {
            /*            string pw = "";

                        if( CProgram.sbReqLabel("Sir Config Server settings: "+_mSerialNr, "Password", ref pw, "", true))
                        {
                            if( pw == "Sir4"+CProgram.sGetOrganisationLabel())
                            {
                                SirserverSettings form = new SirserverSettings(_mSettings, _mSerialNr);

                                if (form != null)
                                {
                                    form.ShowDialog();
                                    if (form.bChanged)
                                    {
                                        mSetResultText(true, "Server settings changed");
                                    }
                                }
                            }
                        }
            */
        }

        private void checkBoxNewSetting_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void labelLoadCurrent_Click(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl && CLicKeyDev.sbDeviceIsProgrammer())
            {
                _mSettings.mClear(_mSerialNr, "");
                mSetResultText(true, "Program defaults set");
            }
            else
            {
                bool bDevice = false;
                bool bOk = mbLoadDeviceSettings(out bDevice, false);

                mSetResultText(bOk, bDevice ? "last settings for " + _mSerialNr + " loaded" : "default settings loaded");
            }
            _mSettings._mDeviceSerial = _mSerialNr;
            mFillEditForm();
            mUpdateFileTime();
        }

        private void labelLoadSettings_Click(object sender, EventArgs e)
        {
            mLoadSettingsFrom();
        }

        private void mLoadSettingsFrom()
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;
            string fileName = "";

            openFileDialogSirS.InitialDirectory = _mSironaDeviceRoot;

            if (DialogResult.OK == openFileDialogSirS.ShowDialog())
            {
                fileName = openFileDialogSirS.FileName;

                mSetResultText(_mSettings.mbReadFile(fileName), "Load setting " + Path.GetFileName(fileName));
            }

            _mSettings._mDeviceSerial = _mSerialNr;
            mFillEditForm();
            mUpdateFileTime();
        }


        private void labelSaveAs_Click(object sender, EventArgs e)
        {
            if (mbReadForm())
            {

                string fileName = "";

                try
                {
                    if (CSironaStateSettings.sbMakeSettingsFileName(out fileName, _mSerialNr))
                    {
                        //                        fileName = Path.Combine(_mSirPath, fileName);

                        saveFileDialog.InitialDirectory = _mUnitDevicePath;
                        saveFileDialog.FileName = fileName;

                        if (DialogResult.OK == saveFileDialog.ShowDialog())
                        {
                            fileName = saveFileDialog.FileName;
                            mCopyStateToSettings();

                            mSetResultText(_mSettings.mbWriteFile(fileName, true), "Write setting " + Path.GetFileName(fileName));
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("SaveAs failed to " + fileName, ex);
                }
            }
            mUpdateFileTime();
        }


        private void labelSaveToServer_Click(object sender, EventArgs e)
        {
            if (mbReadForm())
            {
                string name = "";

                try
                {
                    bool bDo = true;
                    bool bAsk = true;
                    string askTxt = "Unknown command";
                    string cmdInfo = "";
                    DateTime dt;
                    timerRefresh.Stop();

                    switch ( _mSettings._mOperationMode)
                    {
                        case DSironaOperation.None:
                            bAsk = false;
                            
                            break;
                        case DSironaOperation.Start:
                            cmdInfo = "Start" + _mSettings._mOperationType;
                            if (_mStatus.mbStateIsIdle())
                            {
                                bAsk = false;
                            }
                            else
                            {
                                askTxt = "State not at Idle";
                            }
                            break;
                        case DSironaOperation.Stop:
                            cmdInfo = "Stop";
                            if (_mStatus.mbStateIsActive())
                            {
                                bAsk = false;
                            }
                            else
                            {
                                askTxt = "State not at Active";
                            }
                            break;
                        case DSironaOperation.Erase:
                            cmdInfo = "Erase";
                            if (_mStatus.mbStateIsFinished())
                            {
                                bAsk = false;
                            }
                            else
                            {
                                askTxt = "State not at Finished";
                            }
                            break;
                        case DSironaOperation.DataRequest:

                            dt = _mStatus._mProcedureStartTimeDTO.DateTime.AddSeconds(_mSettings._mReqStartAtSec);

                            cmdInfo = "Q" + CProgram.sDateTimeToYMDHMS(dt)
                                + "R" + _mSettings._mReqLengthSec.ToString("0000")
                                + (_mSettings._mbReqAnotations ? "A" : "")
                                + (_mSettings._mbReqEcg ? "E" : "");

                            if (_mStatus.mbStateIsActive()) // && ( _mStatus.mbTypeIsHolter() || _mStatus.mbTypeIsMct() ))
                            {
                                bAsk = false;
                            }
                            else
                            {
                                askTxt = "State not at Active or not at Holter/MCT";
                            }
                            break;
                        case DSironaOperation.ParamChange:
                            cmdInfo = "Param";
                            if (_mStatus.mbStateIsActive())
                            {
                                bAsk = false;
                            }
                            else
                            {
                                askTxt = "State not at Active";
                            }
                            break;
                    }
                    if ( bAsk)
                    {
                        askTxt += ", Continue?";

                        bDo = CProgram.sbAskOkCancel("Sirona send Command to " + _mSerialNr, askTxt);
                    }

                    if (bDo && CSironaStateSettings.sbMakeSettingsFileName(out name, _mSerialNr))
                    {
                        string fileName = Path.Combine(_mUnitDevicePath, name);
                        string result = "";
                        bool bOk = true;

                        if (bOk)
                        {
                            bOk = _mSettings.mbWriteFile(fileName, true);
                            result = "Write setting " + Path.GetFileName(fileName);

                            if (bOk)
                            {
                                string toDir = Path.Combine(_mUnitDevicePath, CSironaStateSettings._csToServerName);
                                string toFileName = Path.Combine(toDir, CSironaStateSettings.sMake2FileName(name, DateTime.UtcNow, 0, cmdInfo));

                                if (_mSettings.mbWriteFile(toFileName, false))
                                {
                                    result += ", copied to server.";
                                }
                                else
                                {
                                    result += ", failed copy to server!";
                                }
                            }
                            mSetResultText(bOk, result);
                            CProgram.sLogLine("Sirona Save to server: " + result);
                        }
                    }
                    timerRefresh.Start();


                }
                catch (Exception ex)
                {
                    CProgram.sLogException("SaveToServer failed for " + name, ex);
                }
            }
            mUpdateFileTime();
        }


        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            timerRefresh.Stop();

            mUpdateFileTime();
            timerRefresh.Start();

        }

        private void labelClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void label18_Click(object sender, EventArgs e)
        {

        }

        private void textBoxStudyLength_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void mShowInfoLine(string ALabel, string ASetting, string AStatus)
        {
            mSetResultInfoText(ALabel + " = " + ASetting + ", device status = " + AStatus);
        }
        private void mShowInfoLine(string ALabel, UInt32 ASetting, UInt32 AStatus)
        {
            mSetResultInfoText(ALabel + " = " + ASetting + ", device status = " + AStatus);
        }
        private void mShowCopyLine(string ALabel, ref string ArSetting, string AStatus)
        {
            mbReadForm();
            mSetResultInfoText(ALabel + " set to " + AStatus + " (old = " + ArSetting + ")");
            ArSetting = AStatus;
            mFillEditForm();
            mFillStatusForm();
            mUpdateCompareIcons();
        }
        private void mShowCopyLine(string ALabel, ref UInt16 ArSetting, UInt16 AStatus)
        {
            mbReadForm();
            mSetResultInfoText(ALabel + " set to " + AStatus.ToString() + " (old = " + ArSetting.ToString() + ")");
            ArSetting = AStatus;
            mFillEditForm();
            mFillStatusForm();
            mUpdateCompareIcons();
        }
        private void mShowCopyLine(string ALabel, ref UInt32 ArSetting, UInt32 AStatus)
        {
            mbReadForm();
            mSetResultInfoText(ALabel + " set to " + AStatus.ToString() + " (old = " + ArSetting.ToString() + ")");
            ArSetting = AStatus;
            mFillEditForm();
            mFillStatusForm();
            mUpdateCompareIcons();
        }
        private void mShowCopyLine(string ALabel, ref DateTime ArSetting, DateTime AStatus)
        {
            mbReadForm();
            mSetResultInfoText(ALabel + " set to " + AStatus.ToString() + " (old = " + ArSetting.ToString() + ")");
            ArSetting = AStatus;
            mFillEditForm();
            mFillStatusForm();
            mUpdateCompareIcons();
        }
        private void mShowCopyLine(string ALabel, ref DateTimeOffset ArSetting, DateTimeOffset AStatus)
        {
            mbReadForm();
            mSetResultInfoText(ALabel + " set to " + AStatus.ToString() + " (old = " + ArSetting.ToString() + ")");
            ArSetting = AStatus;
            mFillEditForm();
            mFillStatusForm();
            mUpdateCompareIcons();
        }
        private void mShowCopyLine(string ALabel, ref bool ArSetting, bool AStatus)
        {
            mbReadForm();
            mSetResultInfoText(ALabel + " set to " + AStatus.ToString() + " (old = " + ArSetting.ToString() + ")");
            ArSetting = AStatus;
            mFillEditForm();
            mFillStatusForm();
            mUpdateCompareIcons();
        }
        private void mShowCopyLine(string ALabel, ref float ArSetting, float AStatus)
        {
            mbReadForm();
            mSetResultInfoText(ALabel + " set to " + AStatus.ToString("0.0") + " (old = " + ArSetting.ToString("0.0") + ")");
            ArSetting = AStatus;
            mFillEditForm();
            mFillStatusForm();
            mUpdateCompareIcons();
        }


        private void pictureBoxDevice_Click(object sender, EventArgs e)
        {
            mShowInfoLine("Device snr", _mSettings._mDeviceSerial, _mStatus._mDeviceSerial);

            /*
                mShowInfoLine("", _mSettings._m, _mStatus._m);
                mShowCopyLine("", ref _mSettings._m, _mStatus._m);

             * */
        }

        private void pictureBoxModel_Click(object sender, EventArgs e)
        {
            mShowCopyLine("Model", ref _mSettings._mModelNumber, _mStatus._mModelNumber);
        }

        private void pictureBoxFirmware_Click(object sender, EventArgs e)
        {
            mShowCopyLine("Firmware", ref _mSettings._mFirmwareVersion, _mStatus._mFirmwareVersion);
        }

        private void pictureBoxAppVersion_Click(object sender, EventArgs e)
        {
            mShowCopyLine("App", ref _mSettings._mAppVersion, _mStatus._mAppVersion);
        }

        private void pictureBoxBatLevel_Click(object sender, EventArgs e)
        {
            mShowCopyLine("Battery", ref _mSettings._mBatteryLevel, _mStatus._mBatteryLevel);
        }

        private void pictureBoxSource_Click(object sender, EventArgs e)
        {
            mShowCopyLine("Source", ref _mSettings._mUpdateSource, _mStatus._mUpdateSource);
        }

        private void pictureBoxCable_Click(object sender, EventArgs e)
        {
            mShowCopyLine("Cable", ref _mSettings._mCableType, _mStatus._mCableType);
        }

        private void pictureBoxLeadOff_Click(object sender, EventArgs e)
        {
            mShowCopyLine("LeadOff", ref _mSettings._mLeadOffBits, _mStatus._mLeadOffBits);
        }

        private void pictureBoxStartTime_Click(object sender, EventArgs e)
        {
            mShowCopyLine("Prc. Start", ref _mSettings._mProcedureStartTimeDTO, _mStatus._mProcedureStartTimeDTO);
        }

        private void pictureBoxProcLength_Click(object sender, EventArgs e)
        {
            mShowCopyLine("Proc Length", ref _mSettings._mProcedureLengthSec, _mStatus._mProcedureLengthSec);
        }

        private void pictureBoxProcType_Click(object sender, EventArgs e)
        {
            mShowCopyLine("Proc Type", ref _mSettings._mProcedureType, _mStatus._mProcedureType);
        }

        private void pictureBoxProcState_Click(object sender, EventArgs e)
        {
            mShowCopyLine("Proc State", ref _mSettings._mProcedureState, _mStatus._mProcedureState);
        }

        private void pictureBoxPatientID_Click(object sender, EventArgs e)
        {
            mShowCopyLine("Patient ID", ref _mSettings._mPatientId, _mStatus._mPatientId);
        }

        private void pictureBoxFirst_Click(object sender, EventArgs e)
        {
            mShowCopyLine("First Name", ref _mSettings._mPatientFirstName, _mStatus._mPatientFirstName);
        }

        private void pictureBoxMiddle_Click(object sender, EventArgs e)
        {
            mShowCopyLine("Middle Name", ref _mSettings._mPatientMiddleName, _mStatus._mPatientMiddleName);
        }

        private void pictureBoxLast_Click(object sender, EventArgs e)
        {
            mShowCopyLine("Last Name", ref _mSettings._mPatientLastName, _mStatus._mPatientLastName);
        }

        private void pictureBoxDOB_Click(object sender, EventArgs e)
        {
            mShowCopyLine("DOB", ref _mSettings._mPatientDOB, _mStatus._mPatientDOB);
        }

        private void pictureBoxPhysisian_Click(object sender, EventArgs e)
        {
            mShowCopyLine("Physician", ref _mSettings._mPhysicianName, _mStatus._mPhysicianName);
        }

        private void pictureBoxComment_Click(object sender, EventArgs e)
        {
            mShowCopyLine("Comment", ref _mSettings._mPatientComment, _mStatus._mPatientComment);
        }

        private void pictureBoxBradyOnset_Click(object sender, EventArgs e)
        {
            mShowCopyLine("Brady", ref _mSettings._mBradyThresholdBpm, _mStatus._mBradyThresholdBpm);
        }

        private void pictureBoxTachyOnset_Click(object sender, EventArgs e)
        {
            mShowCopyLine("Tachy", ref _mSettings._mTachyThresholdBpm, _mStatus._mTachyThresholdBpm);
        }

        private void pictureBoxAfib_Click(object sender, EventArgs e)
        {
            mShowCopyLine("AFib", ref _mSettings._mbAfibOn, _mStatus._mbAfibOn);
        }

        private void pictureBoxPauseOnset_Click(object sender, EventArgs e)
        {
            mShowCopyLine("Pause", ref _mSettings._mPauseThresholdSec, _mStatus._mPauseThresholdSec);
        }

        private void pictureBoxPreTrigger_Click(object sender, EventArgs e)
        {
            mShowCopyLine("PreTrigger", ref _mSettings._mPreTriggerSec, _mStatus._mPreTriggerSec);
        }

        private void pictureBoxPostTrigger_Click(object sender, EventArgs e)
        {
            mShowCopyLine("PostTriger", ref _mSettings._mPostTriggerSec, _mStatus._mPostTriggerSec);
        }

        private void pictureBoxRecordLength_Click(object sender, EventArgs e)
        {
            mShowCopyLine("Rec Length", ref _mSettings._mRecordLengthDays, _mStatus._mRecordLengthDays);
        }

        private void pictureBoxAudio_Click(object sender, EventArgs e)
        {
            mShowCopyLine("Audio", ref _mSettings._mbAudioMute, _mStatus._mbAudioMute);
        }

        private void pictureBoxSampleRate_Click(object sender, EventArgs e)
        {
            mShowCopyLine("Sample rate", ref _mSettings._mSampleRate, _mStatus._mSampleRate);
        }

        private void pictureBoxTTX_Click(object sender, EventArgs e)
        {
            mShowCopyLine("TTX", ref _mSettings._mbTtm3x, _mStatus._mbTtm3x);
        }

        private void pictureBoxUiLock_Click(object sender, EventArgs e)
        {
            mShowCopyLine("UI Lock", ref _mSettings._mbUiLock, _mStatus._mbUiLock);
        }

        private void pictureBoxAutoLimit_Click(object sender, EventArgs e)
        {
            mShowCopyLine("Auto limit", ref _mSettings._mAutoEventLimit, _mStatus._mAutoEventLimit);
        }

        private void pictureBoxManualLimit_Click(object sender, EventArgs e)
        {
            mShowCopyLine("Manual limit", ref _mSettings._mManualEventLimit, _mStatus._mManualEventLimit);
        }

        private void mCopyStateToSettings()
        {
            _mSettings._mDeviceSerial = _mStatus._mDeviceSerial;
            _mSettings._mModelNumber = _mStatus._mModelNumber;
            _mSettings._mFirmwareVersion = _mStatus._mFirmwareVersion;
            _mSettings._mAppVersion = _mStatus._mAppVersion;
            _mSettings._mServerBaseUrl = _mStatus._mServerBaseUrl;
            _mSettings._mBatteryLevel = _mStatus._mBatteryLevel;
            _mSettings._mStateResult = _mStatus._mStateResult;
            _mSettings._mUpdateSource = _mStatus._mUpdateSource;
            _mSettings._mCableType = _mStatus._mCableType;
            _mSettings._mLeadOffBits = _mStatus._mLeadOffBits;
            _mSettings._mProcedureStartTimeDTO = _mStatus._mProcedureStartTimeDTO;
            _mSettings._mProcedureLengthSec = _mStatus._mProcedureLengthSec;
            _mSettings._mProcedureState = _mStatus._mProcedureState;
            _mSettings._mProcedureType = _mStatus._mProcedureType;
            _mSettings._mCommunicationDelay = _mStatus._mCommunicationDelay;
            _mSettings._mDevice_address = _mStatus._mDevice_address;
        }

        private void labelMarkState_Click(object sender, EventArgs e)
        {
            mbReadForm();
            mSetResultInfoText("Mark state done");
            mCopyStateToSettings();
            mFillEditForm();
            mFillStatusForm();
            mUpdateCompareIcons();

        }

        private void labelUseDevPat_Click(object sender, EventArgs e)
        {
            mbReadForm();
            mSetResultInfoText("Use Patient info from device");

            _mSettings._mPatientId = _mStatus._mPatientId;
            _mSettings._mPatientFirstName = _mStatus._mPatientFirstName;
            _mSettings._mPatientMiddleName = _mStatus._mPatientMiddleName;
            _mSettings._mPatientLastName = _mStatus._mPatientLastName;
            _mSettings._mPhysicianName = _mStatus._mPhysicianName;
            _mSettings._mPatientComment = _mStatus._mPatientComment;
            _mSettings._mPatientDOB = _mStatus._mPatientDOB;

            mFillEditForm();
            mFillStatusForm();
            mUpdateCompareIcons();
        }

        private void labelUseDevSet_Click(object sender, EventArgs e)
        {
            mbReadForm();
            mSetResultInfoText("Use settings from device");

            _mSettings._mBradyThresholdBpm = _mStatus._mBradyThresholdBpm;
            _mSettings._mTachyThresholdBpm = _mStatus._mTachyThresholdBpm;
            _mSettings._mbAfibOn = _mStatus._mbAfibOn;
            _mSettings._mPauseThresholdSec = _mStatus._mPauseThresholdSec;
            _mSettings._mPreTriggerSec = _mStatus._mPreTriggerSec;
            _mSettings._mPostTriggerSec = _mStatus._mPostTriggerSec;
            _mSettings._mRecordLengthDays = _mStatus._mRecordLengthDays;
            _mSettings._mbAudioMute = _mStatus._mbAudioMute;
            _mSettings._mSampleRate = _mStatus._mSampleRate;
            _mSettings._mbTtm3x = _mStatus._mbTtm3x;
            _mSettings._mbUiLock = _mStatus._mbUiLock;
            _mSettings._mAutoEventLimit = _mStatus._mAutoEventLimit;
            _mSettings._mManualEventLimit = _mStatus._mManualEventLimit;

            mFillEditForm();
            mFillStatusForm();
            mUpdateCompareIcons();
        }

        private void labelClearPatient_Click(object sender, EventArgs e)
        {
            mbReadForm();
            mSetResultInfoText("Clear Patient info");

            _mSettings._mPatientId = "";
            _mSettings._mPatientFirstName = "";
            _mSettings._mPatientMiddleName = "";
            _mSettings._mPatientLastName = "";
            _mSettings._mPhysicianName = "";
            _mSettings._mPatientComment = "";
            _mSettings._mPatientDOB = DateTime.MinValue;

            mFillEditForm();
            mFillStatusForm();
            mUpdateCompareIcons();

        }

        private void dateTimePickerStart_ValueChanged(object sender, EventArgs e)
        {
            string error = "";

            mbParseReqTime(ref error, false);
/*
            DateTime dt = _mStatus._mProcedureStartTimeDTO.DateTime;
            TimeSpan ts = dateTimePickerStart.Value - dt;

            int i = (int)(ts.TotalSeconds - ts.Seconds);    // start at hole minute

            _mSettings._mReqStartAtSec = (UInt32)(i < 0 || i > 100000000 ? 0 : i);
            textBoxStartAt.Text = _mSettings._mReqStartAtSec.ToString();
*/        }

        private void pictureBoxServerUrl_Click(object sender, EventArgs e)
        {
            mShowCopyLine("Url", ref _mSettings._mServerBaseUrl, _mStatus._mServerBaseUrl);

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            mShowCopyLine("ComDelay", ref _mSettings._mCommunicationDelay, _mStatus._mCommunicationDelay);
        }

        private void labelLoadDefaults_Click(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl && CLicKeyDev.sbDeviceIsProgrammer())
            {
                _mSettings.mClear(_mSerialNr, "");
                mSetResultText(true, "Program defaults set");
            }
            else
            {
                bool bDevice = false;
                bool bOk = mbLoadDeviceSettings(out bDevice, true);

                mSetResultText(bOk, bDevice ? "last settings for " + _mSerialNr + " loaded" : "default settings loaded");
            }
            _mSettings._mDeviceSerial = _mSerialNr;
            mFillEditForm();
            mUpdateFileTime();

        }

        private void labelSaveDefaults_Click(object sender, EventArgs e)
        {
            if (mbReadForm())
            {
                string result = "";
                string fileName = "";
                bool bOk = false;

                try
                {
                    if (CSironaStateSettings.sbMakeSettingsFileName(out fileName, _mSerialNr))
                    {
                        fileName = Path.Combine(_mUnitDevicePath, fileName);
                        bOk = _mSettings.mbWriteFile(fileName, true); 

                        if (bOk)
                        {
                            bOk = _mSettings.mbWriteFile(fileName, true);
                            result = "Writen settings " + Path.GetFileName(fileName);
                            //                        fileName = Path.Combine(_mSirPath, fileName);

                            CSironaStateSettings.sbMakeSettingsFileName(out fileName, "defaults");

                            fileName = Path.Combine(_mSironaDeviceRoot, fileName);
                            bOk = _mSettings.mbWriteFile(fileName, false);
                            result += bOk ? ", Writen defaults" : ", Failed write defaults!";
                        }
                        else
                        {
                            result = "Failed write settings";
                        }
                    }
                    mSetResultText(bOk, result);
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("SaveDefaults failed from " + fileName, ex);
                }
            }
            mUpdateFileTime();

        }

        private void textBoxStartLength_TextChanged(object sender, EventArgs e)
        {
            string error = "";
            mbParseReqTime(ref error, false);
        }

        private void labelMarkAll_Click(object sender, EventArgs e)
        {
            labelMarkState_Click(sender, e);
            labelUseDevPat_Click(sender, e);
            labelUseDevSet_Click(sender, e);
        }

        private void label24_Click(object sender, EventArgs e)
        {
            string result = "";

            bool bOk = mbParseReqTime(ref result, true);

            mSetResultText(bOk, "Update @ "+ result);
            CProgram.sLogLine("Sirona Update @: " + result);
        }

        private void label19_Click(object sender, EventArgs e)
        {
            if (mbReadForm())
            {
                string name = "";

                mCopyStateToSettings();
                mFillStatusForm();

                try
                {
                    if (CSironaStateSettings.sbMakeSettingsFileName(out name, _mSerialNr))
                    {
                        string fileName = Path.Combine(_mUnitDevicePath, name);
                        mSetResultText(_mSettings.mbWriteFile(fileName, true), "Write state " + Path.GetFileName(fileName));
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Save state failed " + name, ex);
                }
            }
            mUpdateFileTime();

        }

        private void timerRefresh_Tick(object sender, EventArgs e)
        {
            mUpdateFileTime();
            if( checkBoxAutoRunTest.Checked)
            {
                double min = (DateTime.UtcNow - _mLastTestRunUTC).TotalMinutes;
                int waitMin = CLicKeyDev.sbDeviceIsProgrammer() ? 1 : 5;

                if (min >= waitMin)
                {
                    mTestCollect(true);
                    _mLastTestRunUTC = DateTime.UtcNow;
                }

            }
            else if(checkBoxAutoCollect.Checked && checkBoxCollectEnabled.Checked)
            {
                double min = (DateTime.UtcNow - _mLastTestRunUTC).TotalMinutes;
                int waitMin = CLicKeyDev.sbDeviceIsProgrammer() ? 1 : 5;

                if (min >= waitMin)
                {
                    mbUpdateInfoCollect();
                    _mLastTestRunUTC = DateTime.UtcNow;
                }
            }
        }

        private void buttonClearQueue_Click(object sender, EventArgs e)
        {
            timerRefresh.Stop();
            mbClearQueue();
            timerRefresh.Start();
        }

        private void labelDeviceSnr_DoubleClick(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if( bCtrl )
            {
                try
                {
                    if (Directory.Exists(_mUnitDevicePath))
                    {
                        ProcessStartInfo startInfo = new ProcessStartInfo();
                        startInfo.FileName = @"explorer";
                        startInfo.Arguments = _mUnitDevicePath;
                        Process.Start(startInfo);
                    }
                    else
                    {
                        CProgram.sAskOk("Sirona Device " + _mSerialNr, " Device path does not exist");
                    }
                }
                catch (Exception e2)
                {
                    CProgram.sLogException("Failed open explorer", e2);
                }

            }
        }
        private bool mbLoadCollect()
        {
            bool bOk = false;

            if (_mbHolterActive)
            {
                _mCollect = new CSirAutoCollect("ECG", true);
                if (_mCollect != null)
                {
                    bOk = mbUpdateInfoCollect();
                }
            }
            return bOk;
        }
        private bool mbFillCollect()
        {
            bool bOk = false;

            if (_mCollect != null)
            {
                bool bActive = mbCheckStudyActive();

                checkBoxCollectEnabled.Checked = _mCollect._mbEnabled;
                checkBoxAcEvent.Checked = _mCollect._mbGenEvent;

                bOk = true;
            }
            else
            {
                checkBoxCollectEnabled.Checked = false;
                checkBoxCollectEnabled.Checked = false;
            }
            mbCheckButtonCollect();
            return bOk;
        }
        private bool mbReadCollect()
        {
            bool bOk = false;

            try
            {
                if (_mCollect != null)
                {
                    bool bActive = mbCheckStudyActive();

                    _mCollect._mbEnabled = checkBoxCollectEnabled.Checked;
                    _mCollect._mbGenEvent = checkBoxAcEvent.Checked;

                    bOk = true;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Read Collect Sirona", ex);
            }
            mbUpdateInfoCollect();
            return bOk;
        }
        private bool mbUpdateInfoCollect()
        {
            bool bOk = false;
            string parmInfo = "-";
            string scanInfo = "-";

            if (_mCollect != null && _mDeviceInfo != null)
            {
                // parameters
                parmInfo = _mCollect.mParamFileInfoLoadNew(_mUnitDevicePath, _mSerialNr, true);
                mbFillCollect();

                scanInfo = _mCollect.mScanInfoLoadMissing(_mUnitDevicePath, _mSerialNr, _mDeviceInfo._mActiveStudy_IX, true, _mStatusFileDT);
                // 
            }
            labelAcSavedInfo.Text = parmInfo;
            labelAcLastScan.Text = scanInfo;
            mbCheckButtonCollect();
            return bOk;
        }
        private bool mbCheckButtonCollect()
        {
            bool bOk = false;

            if (_mCollect != null)
            {
                bOk = mbCheckStudyActive();

                buttonAcTest.Enabled = bOk;
            }
            return bOk;
        }
        private bool mbCheckStudyActive()
        {
            bool bActive = false;

            if (_mDeviceInfo != null && _mDeviceInfo._mAddStudyMode == (UInt16)DAddStudyModeEnum.ActiveStudy )
            {
                if (_mDeviceInfo._mActiveStudy_IX != 0 && _mDeviceInfo._mRecorderEndUTC != DateTime.MinValue)
                {
                    bActive = DateTime.UtcNow < _mDeviceInfo._mRecorderEndUTC;
                }
            }
            return bActive;
        }
        private bool mbFillStudyActive()
        {
            string s = "No Study";
            bool bActive = false;

            if (_mDeviceInfo != null)
            {
                if (_mDeviceInfo._mActiveStudy_IX != 0)
                {
                    if (_mDeviceInfo._mAddStudyMode != (UInt16)DAddStudyModeEnum.ActiveStudy)
                    {
                        s = _mDeviceInfo.mGetAddStudyModeLabel(false) + " ! Study " + _mDeviceInfo._mActiveStudy_IX.ToString();
                    }
                    else
                    {
                        s = "Study " + _mDeviceInfo._mActiveStudy_IX.ToString();

                        if (_mDeviceInfo._mRecorderEndUTC != DateTime.MinValue)
                        {
                            bActive = DateTime.UtcNow < _mDeviceInfo._mRecorderEndUTC;
                            if (bActive)
                            {
                                s += " Active, started " + CProgram.sDateTimeToString(_mDeviceInfo._mRecorderStartUTC) + " UTC";
                            }
                            else
                            {
                                s += " Ended " + CProgram.sDateTimeToString(_mDeviceInfo._mRecorderEndUTC) + " UTC";
                            }
                        }
                    }
                }
            }
            labelStudyActive.Text = s;

            return bActive;
        }
        private void mTestCollect( bool AbRunAutomatic)
        {
            if (mbReadCollect())
            {
                string result = "";
                bool bRunDone = false;
                bool bEnabled = false;
                bool bRunAutomatic = AbRunAutomatic;

                bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

                if (bCtrl && false == bRunAutomatic )
                {
                    if (false == CProgram.sbAskYesNo("Sirona collect ECG for " + _mDeviceInfo._mActiveStudy_IX.ToString(), "Run automatic scan for missing ECG"))
                    {
                        return;
                    }
                    bRunAutomatic = true;
                }

                bool bOk = _mCollect.mbRunCycle(out result, out bEnabled, out bRunDone, _mUnitDevicePath, _mSerialNr, _mDeviceInfo._mActiveStudy_IX,
                    _mDeviceInfo._mRecorderStartUTC, _mDeviceInfo._mRecorderEndUTC, bRunAutomatic);

                mSetResultText(bOk, result);

                if( bOk && bRunDone && _mCollect != null)
                {
                    try
                    {
                        DateTime dt = _mCollect._mNextRequestStartUTC.AddMinutes(_mCollect._mDeviceTZoneMin);
                        dateTimePickerStart.Value = dt;
                        textBoxStartLength.Text = _mCollect._mNextRequestLengthSec.ToString();
                    }
                    catch(Exception)
                    {

                    }
                }

                mbUpdateInfoCollect();
                mUpdateFileTime();
            }
        }
        private void mSaveCollect()
        {
            if (mbReadCollect())
            {
                mSetResultText(_mCollect.mbSaveParamFile(_mUnitDevicePath, _mSerialNr), "save collect ecg ");

                mbUpdateInfoCollect();
            }
        }

        private void checkBoxAutoCollect_CheckedChanged(object sender, EventArgs e)
        {
            panelAutoCollect.Visible = checkBoxAutoCollect.Checked;

        }

        private void buttonModeSave_Click(object sender, EventArgs e)
        {
            mSaveCollect();
        }

        private void buttonAcTest_Click(object sender, EventArgs e)
        {
            mTestCollect( false);
        }

        private void buttonAcRefreshInfo_Click(object sender, EventArgs e)
        {
            mbUpdateInfoCollect();
        }

        private void labelSetting_DoubleClick(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl && CLicKeyDev.sbDeviceIsProgrammer())
            {
                CSironaStateSettings._sbUseQueue = !CSironaStateSettings._sbUseQueue;
                labelSetting.Text = CSironaStateSettings._sbUseQueue ? "Queue ON" : "Queue off";
            }
        }

        private void labelStudyActive_DoubleClick(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl && _mDeviceInfo != null && _mDeviceInfo._mActiveStudy_IX > 0)
            {
                string studyPath;

                if (CDvtmsData.sbGetStudyRecorderDir(out studyPath, _mDeviceInfo._mActiveStudy_IX, true))
                {
                    ProcessStartInfo startInfo = new ProcessStartInfo();
                    startInfo.FileName = @"explorer";
                    startInfo.Arguments = studyPath;
                    Process.Start(startInfo);
                }
            }

        }

        private void pictureBoxAddress_Click(object sender, EventArgs e)
        {
            mShowCopyLine("Address", ref _mSettings._mDevice_address, _mStatus._mDevice_address);
        }

        private void mSetStart( Int32 AOffsetSec)
        {
            if( _mStatus._mProcedureStartTimeDTO.DateTime.Year > 2000 )
            {
                DateTime dt = _mStatus._mProcedureStartTimeDTO.DateTime;
                Int32 length = 0;
                int start = 0;

                Int32.TryParse(textBoxStartLength.Text, out length);

                if( AOffsetSec < 0 )
                {
                    start = (Int32)_mStatus._mProcedureLengthSec - length;
                }
                else
                {
                    start = length;
                }
                if(start > _mStatus._mProcedureLengthSec)
                {
                    start = (Int32)_mStatus._mProcedureLengthSec - length;
                }
                if (start < 0) start = 0;
                dt = dt.AddSeconds(start);
                dateTimePickerStart.Value = dt;
            }

        }

        private void textBoxStartTime_DoubleClick(object sender, EventArgs e)
        {
            mSetStart(0);
        }

        private void labelProcEnd_DoubleClick(object sender, EventArgs e)
        {
            mSetStart(-1);
        }

        private void labelEndQuiry_Click(object sender, EventArgs e)
        {
            mSetStart(-1);
        }
    }
}
