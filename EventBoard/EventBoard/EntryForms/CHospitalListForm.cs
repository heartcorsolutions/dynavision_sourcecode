﻿using Program_Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventBoardEntryForms
{
    public partial class CHospitalListForm : Form
    {
        public CHospitalListForm()
        {
            InitializeComponent();


            Width = 1600;   // Nicer size then minimum 1240

            Text = CProgram.sMakeProgTitle("Hospital List", false, true);

        }
    }
}
