﻿using Event_Base;
using EventBoard;
using EventBoard.EntryForms;
using EventBoard.Event_Base;
using Program_Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventboardEntryForms
{
    public enum DStudyReportListVars
    {
        PdfNr, ReportDate, ReportType, SubNr, From, Until, Techician, CreatedAt, Diagnosis, AnalysisDate, QcMD, Tachy, Brady, Pause, Afib, Other, SentDate, SentTo //....., 
    }
    public partial class CStudyReportListForm : Form
    {
        public CPdfDocRec _mPdfDocRec = null;
        public List<CSqlDataTableRow> _mPdfDocList = null;

        public CStudyInfo _mStudyInfo = null;
        public CPatientInfo _mPatientInfo = null;
        UInt32 _mStudyIndex;
        bool _mbAnonymize;


        public CStudyReportListForm(UInt32 AStudyIndex )
        {
            bool bOk = false;
            bool bError = false;

            try
            {
                CDvtmsData.sbInitData();

                _mStudyIndex = AStudyIndex;
                _mStudyInfo = new CStudyInfo(CDvtmsData.sGetDBaseConnection());
                _mPatientInfo = new CPatientInfo(CDvtmsData.sGetDBaseConnection());
                _mPdfDocRec = new CPdfDocRec(CDvtmsData.sGetDBaseConnection());

                if (_mPdfDocRec != null && _mStudyInfo != null && _mPatientInfo != null )
                {
                    if( _mStudyInfo.mbDoSqlSelectIndex( AStudyIndex, _mStudyInfo.mGetValidMask(true) ))
                    {
                        if( _mPatientInfo.mbDoSqlSelectIndex(_mStudyInfo._mPatient_IX, _mPatientInfo.mGetValidMask(true)))
                        {
                            UInt64 loadMask = _mPdfDocRec.mMaskValid;
                            UInt64 whereMask = CSqlDataTableRow.sGetMask((UInt16)DPdfDocRecVars.Study_IX);

                            _mPdfDocRec._mStudy_IX = _mStudyInfo.mIndex_KEY;

                            bOk = _mPdfDocRec.mbDoSqlSelectList(out bError, out _mPdfDocList, loadMask, whereMask, true, "");   // just load for current study
                        }
                    }
                }
                if (bOk)
                {
                    InitializeComponent();

                    _mbAnonymize = FormEventBoard._sbForceAnonymize;

                    bool AbEditReport = false;
                    panelStudyReport1.Visible = AbEditReport;
                    panelStudyReport1.Enabled = AbEditReport;
                    panelStudyButtons.Visible = AbEditReport;
                    panelSelect.Visible = true;

                    mSetStudy();
                    mSetList();
                    //                    mSetReport();/
                    Text = CProgram.sMakeProgTitle("List reports for Study " + _mStudyIndex.ToString(), false, true);

                }
                else
                {
                    Close();
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("PreviousFindingsForm() failed to init", ex);
            }
        }
        private void mSetStudy()
        {
            if (_mStudyInfo != null)
            {
                string s = _mStudyInfo.mGetLabelNoteMark();

                labelStudyNote.Text = s;

                if (s.Length > 0)
                {
                    s = _mStudyInfo.mGetRemarkAndNote();

                    new ToolTip().SetToolTip(labelStudyNote, s);
                }

                int age = _mPatientInfo.mGetAgeYears();
                labelStudyIndex.Text = _mStudyInfo.mIndex_KEY.ToString();
                
                labelFullName.Text = _mPatientInfo != null ? ( _mbAnonymize ? "--":  _mPatientInfo.mGetFullName()) : "";
                labelDoB.Text = _mPatientInfo != null && age >= 0 && false == _mbAnonymize ? CProgram.sDateToString(_mPatientInfo._mPatientDateOfBirth.mDecryptToDate()) : "";

                labelAge.Text = _mPatientInfo != null && age >= 0 ? age.ToString(): "";
                labelGender.Text = _mPatientInfo != null ? CPatientInfo.sGetGenderString( (DGender)_mPatientInfo._mPatientGender_IX ) : "";
            }
        }

        private bool mbIsDay( DateTime ADateTime )
        {
            bool bIsDay = false;

            if( ADateTime.Hour >= 6 ) // >= 6:00 && <= 23:59
            {
                if (ADateTime.Hour <= 23 )
                {
                    bIsDay = true;
                }
                else if(ADateTime.Hour == 23)
                {
                    bIsDay = ADateTime.Minute <= 59;
                }
            }
            return bIsDay;
        }

        public string mMaCodeString( bool AbManual )
        {
            return AbManual ? "M" : "A";
        }

        private void mSetList()
        {
            dataGridView.Rows.Clear();
            string missingReports = "";

            CRecordMit rec = new CRecordMit(CDvtmsData.sGetDBaseConnection());

            if (rec != null && _mPdfDocList != null && _mStudyInfo != null )
            {
                CRecAnalysis selectedRow = null;
                UInt16 lastReportNr = 0;
                int count = 0;
                UInt64 patMask = _mPatientInfo != null ? _mPatientInfo.mGetValidMask(true) : 0;
                UInt64 recMask = rec.mMaskValid;

                foreach (CPdfDocRec row in _mPdfDocList)
                {
                    UInt32 reportNr = row.mIndex_KEY;
                    string reportDate = CProgram.sDateTimeToString(CProgram.sDateTimeToLocal(row._mFileUTC)); 
                    string reportType = row._mReportType;
                    UInt16 subNr = row._mStudySubNr;
                    TimeSpan ts = row._mEndPeriodDT - row._mStartPeriodDT;
                    double seconds = ts.TotalSeconds;

                    string fromTime = CProgram.sDateTimeToString(row._mStartPeriodDT);
                    string untilTime = seconds < 1 ? "" : CProgram.sDateTimeToString(row._mEndPeriodDT);
                    string techician = row._mCreadedByInitials;
                    string createdAt = row._mCreatedAt;
                    string diagnosis = "";
                    string analysisDate = "";
                    string qcMD = "";
                    UInt16 tachy = 0;
                    UInt16 brady = 0;
                    UInt16 pause = 0;
                    UInt16 afib = 0;
                    UInt16 other = 0;
                    string sentDate = "";
                    string sentTo = "";
                    string fileName = row._mPdfFileName;
                    string refID = row._mRefID != 0 && fileName.Contains("_r0" )  ? row._mRefID.ToString() : "";

                    /*                    bool bRec = rec.mbDoSqlSelectIndex(row._mRecording_IX, recMask);
                                        DateTime eventDT = rec.mGetDeviceTime( rec.mGetEventUTC());

                                        string analysisStr = row.mIndex_KEY.ToString();
                                        bool bAddReport = _mStudyReport._mReportTableSet.mbContainsValue(analysisStr);
                                        bool bAddTable = _mStudyReport._mReportStripSet.mbContainsValue(analysisStr);
                                        string reportDate = row._mReportedUTC > DateTime.MinValue ? CProgram.sDateToString(CProgram.sDateTimeToLocal(row._mReportedUTC)) : "";
                                        string reportNr = row._mReportNr == 0 ? "" : row._mReportNr.ToString();
                                        string eventNr = bRec ? rec.mSeqNrInStudy.ToString() :  "R#" + row._mRecording_IX.ToString();
                                        string maCode = mMaCodeString(row._mbManualEvent);

                                        string eventDate = bRec ? CProgram.sDateTimeToString(rec.mGetDeviceTime( eventDT)) : "-";
                                        string analysisNr = row._mSeqInRecording.ToString();
                                        string analysisDate = row._mMeasuredUTC > DateTime.MinValue ? CProgram.sDateToString(CProgram.sDateTimeToLocal(row._mMeasuredUTC)) : "";
                                        string technician = "";
                                        string classification = CDvtmsData._sEnumListFindingsClass != null && row._mAnalysisClassSet.mbIsNotEmpty() ? CDvtmsData._sEnumListFindingsClass.mGetLabel(row._mAnalysisClassSet.mGetValue(0)) : "";
                                        string rhythm = row._mAnalysisRhythmSet == null || row._mAnalysisRhythmSet.mbIsEmpty() ? "" : row._mAnalysisRhythmSet.mGetShowSet("+");
                                        string symptoms = row._mSymptomsRemark;
                                        string hrMin = "";
                                        string hrMax = "";
                                        string hrMean = "";
                                        bool bIsDay = mbIsDay(eventDT);

                                        if (row._mMeasuredHrN > 0)
                                        {
                                            hrMean = row._mMeasuredHrMean.ToString("0");
                                            if (row._mMeasuredHrN > 0)
                                            {
                                                hrMin = row._mMeasuredHrMin.ToString("0");
                                                hrMax = row._mMeasuredHrMax.ToString("0");
                                            }
                                        }
                    */
                    dataGridView.Rows.Add(reportNr, subNr, reportDate, reportType, refID, fromTime, untilTime, techician, createdAt, fileName,
                        diagnosis, analysisDate, qcMD, tachy, brady, pause, afib, other, sentDate, sentTo);

                    /*                    if (_mAnalysisIndex > 0 && row.mIndex_KEY == _mAnalysisIndex)
                                        {
                                            selectedRow = row;
                                            DataGridViewRow gridRow = dataGridView.Rows[n];

                                            if (gridRow != null)
                                            {
                                                dataGridView.CurrentCell = gridRow.Cells[0];  // set cursor back to current
                                            }
                                    }
                    */
                    if(subNr > lastReportNr )
                    {
                        lastReportNr = subNr;
                    }
                    ++count;
                }
                labelNrReports.Text = count.ToString();

                UInt16 oldNrReports = _mStudyInfo._mNrReports;

                if (oldNrReports == lastReportNr)
                {
                    if (count == lastReportNr)
                    {
                        missingReports = "√";
                    }
                    else if (count > lastReportNr)
                    {
                        missingReports = "last report " + lastReportNr.ToString() + ", " + (count - lastReportNr).ToString() + " extra reports";
                    }
                    else
                    {
                        missingReports = "last report " + lastReportNr.ToString() + ", " + (lastReportNr - count).ToString() + " missing reports";
                    }
                }
                else
                {
                    if (count == lastReportNr)
                    {
                        missingReports = "Nr Reports " + oldNrReports.ToString() + " != " + lastReportNr.ToString();
                    }
                    else if (count > lastReportNr)
                    {
                        missingReports = "Nr Reports " + oldNrReports.ToString() + " != " + lastReportNr.ToString()
                            + (count - lastReportNr).ToString() + " extra reports";
                    }
                    else
                    {
                        missingReports = "Nr Reports " + oldNrReports.ToString() + " != " + lastReportNr.ToString()
                            + (lastReportNr - count).ToString() + " missing reports";
                    }
                }
            
            //                labelListStrips.Text = n.ToString();
            //                if (selectedRow == null)
            //                {
            //                  _mSelectedIndex = 0;
            //                }
            }
            labelMissingReports.Text = missingReports;
        }
        private void mSetReport()
        {
/*            if( _mStudyReport != null)
            {
                textBoxSummary.Text = _mStudyReport._mSummary.mDecrypt();
                textBoxConclusion.Text = _mStudyReport._mConclusion.mDecrypt();
            }
*/        }

/*        public bool mbSaveReport()
        {
            return false;
            /*            bool bOk = false;
                        string resultText = "";

                        mUpdateStats();    // calculates sets

                        if( _mStudyReport != null )
                        {
                            _mStudyReport._mStudyIX = _mStudyInfo.mIndex_KEY;

                            _mStudyReport._mReportTableSet.mSetSet(_mSelectedTableSet);    // show in table: analysis index numbers [- sample flags]
                            _mStudyReport._mReportStripSet.mSetSet(_mSelectedStripSet);    // show in strips: analysis index numbers [- sample flags]

                            _mStudyReport._mReportSectionMask = 0;
                            if (radioButtonDaily.Checked) { _mStudyReport._mReportSectionMask |= 1 << (UInt16)DReportSection.Daily; }
                            if (radioButtonWeekly.Checked) { _mStudyReport._mReportSectionMask |= 1 << (UInt16)DReportSection.Weekly; }
                            if (radioButtonEOS.Checked) { _mStudyReport._mReportSectionMask |= 1 << (UInt16)DReportSection.EndOfStudy; }

                            _mStudyReport._mSummary.mbEncrypt(textBoxSummary.Text);
                            _mStudyReport._mConclusion.mbEncrypt(textBoxConclusion.Text);

                            _mStudyReport._mReportUTC = DateTime.UtcNow;
                            _mStudyReport._mReportedByIX = 0;

                            if( _mStudyReport._mReportNr == 0 )
                            {
                                _mStudyReport._mReportNr = ++_mStudyInfo._mNrReports;

                                if( _mStudyReport.mbDoSqlInsert())
                                {                       
                                    if( _mStudyInfo.mbDoSqlUpdate(CSqlDataTableRow.sGetMask((UInt16)DStudyInfoVars.NrReports), true))
                                    {
                                        resultText = "Inserted.";
                                        bOk = true;
                                    }
                                    else
                                    {
                                        resultText = "Inserted, failed update study.";
                                    }
                                }
                                else
                                {
                                    resultText = "Failed insert!";
                                }
                            }
                            else
                            {
                                if (_mStudyReport.mbDoSqlUpdate(_mStudyReport.mGetValidMask(true), true))
                                {
                                    resultText = "Updated.";
                                    bOk = true;
                                }
                                else
                                {
                                    resultText = "Failed update!";
                                }
                            }
                            resultText = "Study Report: #" + _mStudyReport.mIndex_KEY.ToString() + " / " + _mStudyInfo._mNrReports.ToString() + ", " + resultText;
                        }
                        labelReportResult.Text = resultText;
                        return bOk;
             * /
        }
*/
        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
//            mbSaveReport();
        }

        private void button1_Click(object sender, EventArgs e)
        {
//            mbSaveReport();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            textBoxConclusion.Text = "";
        }

        private void button8_Click(object sender, EventArgs e)
        {
            textBoxSummary.Text = "";
        }

        private void label13_Click_1(object sender, EventArgs e)
        {

        }

        private void textBoxSummary_TextChanged(object sender, EventArgs e)
        {

        }

        private void panelStudyReport1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void dataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if( e.RowIndex >= 0 && e.RowIndex < dataGridView.Rows.Count)
            {
                DataGridViewRow row = dataGridView.Rows[e.RowIndex];

                if( row != null )
                {



/*                    if (e.ColumnIndex == 0)
                    {
                        if( row.Cells[0].Value != null )
                        {
                            bool b = (bool)row.Cells[0].Value;
                            b = !b;
                            
                            row.Cells[0].Value = b;
                            if( b == false )
                            {
                                row.Cells[1].Value = false; // turn off strip when turning off table

                            }
                            mUpdateStats();
                        }

                    }
                    else if (e.ColumnIndex == 1)
                    {
                        if (row.Cells[1].Value != null)
                        {
                            bool b = (bool)row.Cells[1].Value;

                            b = !b;
                            row.Cells[1].Value = b;
                            if (b == true)
                            {
                                row.Cells[0].Value = true; // turn on table when turning on strip 
                            }

                            mUpdateStats();
                        }
                    }
                    */
                }
            }
        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void panel40_Paint(object sender, PaintEventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void buttonPrintPreviousFindings_Click(object sender, EventArgs e)
        {
 /*           if( mbSaveReport()) // collects data
            {
                CPrintStudyEventsForm form = new CPrintStudyEventsForm(this, dataGridView, true);

                if (form != null)
                {
                    form.ShowDialog();
                }
            }
*/
        }

        private void dataGridView_DoubleClick(object sender, EventArgs e)
        {
            try
            {
               
                if (dataGridView.CurrentRow != null )
                {
                    mOpenPdf(dataGridView.CurrentRow);
/*

                    CRecAnalysis selectedRow = null;

                    if (dataGridView.CurrentRow.Cells.Count > (int)DStudyReportListVars.AnalysisIndex
                        && dataGridView.CurrentRow.Cells[(int)DStudyReportListVars.AnalysisIndex].Value != null )
                    {
                        string analysisStr = dataGridView.CurrentRow.Cells[(int)DStudyReportListVars.AnalysisIndex].Value.ToString();

                        if (_mList != null && _mStudyInfo != null && analysisStr != null && analysisStr.Length > 0)
                        {
                            foreach (CRecAnalysis row in _mList)
                            {
                                if (analysisStr == row.mIndex_KEY.ToString())
                                {
                                    selectedRow = row;
                                    break;
                                }
                            }
                        }
                    }

                    if (selectedRow != null)
                    {
                        FormEventBoard eventBoardForm = FormEventBoard.sGetMainForm();
                        FormAnalyze formAnalyze = new FormAnalyze(eventBoardForm);

                        if (eventBoardForm != null && formAnalyze != null)
                        {
                            UInt32 recordIndex = selectedRow._mRecording_IX;
                            UInt32 analysisIndex = selectedRow.mIndex_KEY;
                            UInt16 analysisNr = selectedRow._mSeqInRecording;

                            if (formAnalyze.mbLoadRecord2(false, recordIndex, false, analysisIndex, analysisNr, eventBoardForm.mDBaseServer, eventBoardForm.mRecordingDir))
                            {
                                formAnalyze.mInitScreens();

                                if (formAnalyze != null)
                                {
                                    try
                                    {
                                        formAnalyze.Show();
                                        //                                        formAnalyze.ShowDialog();
                                    }
                                    catch (Exception ex)
                                    {
                                        CProgram.sLogException("Analysis Form run", ex);
                                    }
                                }
                            }
                        }
                    }
                    */
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed to run Analysis Form", ex);
            }
        }

        private CPdfDocRec mGetActivePdfDocRec(DataGridViewRow ARow)
        {
            CPdfDocRec pdfDocRec = null;

            if( ARow != null)
            {
                if( ARow.Cells != null && ARow.Cells.Count >= 1)
                {
                    string nrString = ARow.Cells[(UInt16)DStudyReportListVars.PdfNr].Value.ToString();
                    UInt32 pdfNr = 0;

                    if( UInt32.TryParse( nrString, out pdfNr ))
                    {
                        if( pdfNr > 0 )
                        {

                            foreach( CPdfDocRec rec in _mPdfDocList)
                            {
                                if( rec.mIndex_KEY == pdfNr )
                                {
                                    pdfDocRec = rec;
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            return pdfDocRec;
        }

        private void mOpenPdf(DataGridViewRow ARow )
        {
            CPdfDocRec pdfDoc = mGetActivePdfDocRec(ARow);

            if( pdfDoc != null)
            {
                string pdfDir;
                
                if( CDvtmsData.sbGetStudyPdfDir(out pdfDir, _mStudyIndex, true))
                {
                    string pdfFile = Path.Combine(pdfDir, pdfDoc._mPdfFileName);

                    if( File.Exists(pdfFile))
                    {
                        Process.Start(pdfFile);
                    }
                    else
                    {
                        CProgram.sPromptError(true, "View study(" + _mStudyIndex.ToString() + ") dicument", "Could not find file " + pdfDoc._mPdfFileName);
                    }
                }
            }
        }

        private void button5_Click_1(object sender, EventArgs e)
        {
            if(dataGridView.Rows.Count > 0 && dataGridView.CurrentRow != null )
            {
                DataGridViewRow row = dataGridView.CurrentRow;

                if (row != null)
                {
                    mOpenPdf(row);
                }
            }

        }

        private void dataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView.Rows.Count > 0 && dataGridView.CurrentRow != null)
            {
                DataGridViewRow row = dataGridView.CurrentRow;

                if (row != null)
                {
                    mOpenPdf(row);
                }
            }
        }
        private void buttonCheckNrReports_Click(object sender, EventArgs e)
        {
            if( _mStudyInfo != null )
            {
                UInt16 nrReports;

                bool bOk = CPdfDocRec.sbDeterminNrReports( out nrReports, _mStudyInfo.mIndex_KEY);

                if( bOk )
                {
                    _mStudyInfo.mbSetNrReports(nrReports);
                }

                mSetList();
            }
        }

        private void labelStudyIndex_DoubleClick(object sender, EventArgs e)
        {

            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl) //&& CLicKeyDev.sbDeviceIsProgrammer())
            {
                mOpenStudyDir();
            }

        }
        private void mOpenStudyDir()
        {
            if (_mStudyInfo != null && _mStudyInfo.mIndex_KEY > 0)
            {
                string studyPath;

                if (CDvtmsData.sbGetStudyCaseDir(out studyPath, _mStudyInfo.mIndex_KEY, true))
                {
                    // open explorer wit file at cursor
                    try
                    {
                        ProcessStartInfo startInfo = new ProcessStartInfo();
                        startInfo.FileName = @"explorer";
                        startInfo.Arguments = studyPath;
                        Process.Start(startInfo);
                    }
                    catch (Exception e2)
                    {
                        CProgram.sLogException("Failed open explorer", e2);
                    }
                }
            }
        }

    }
}
