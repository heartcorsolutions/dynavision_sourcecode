﻿namespace SironaDevice
{
    partial class SironaDevSet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SironaDevSet));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.labelResult = new System.Windows.Forms.TextBox();
            this.panel13 = new System.Windows.Forms.Panel();
            this.labelSetting = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.checkBoxAutoCollect = new System.Windows.Forms.CheckBox();
            this.buttonClearQueue = new System.Windows.Forms.Button();
            this.label43 = new System.Windows.Forms.Label();
            this.buttonRefresh = new System.Windows.Forms.Button();
            this.label33 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.labelResultError = new System.Windows.Forms.Label();
            this.labelSettingFileInfo = new System.Windows.Forms.Label();
            this.labelActionFileInfo = new System.Windows.Forms.Label();
            this.pictureBoxEqual = new System.Windows.Forms.PictureBox();
            this.pictureBoxNotEqual = new System.Windows.Forms.PictureBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.labelEndQuiry = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.dateTimePickerStart = new System.Windows.Forms.DateTimePicker();
            this.label32 = new System.Windows.Forms.Label();
            this.labelStartAtTime = new System.Windows.Forms.Label();
            this.checkBoxAudio = new System.Windows.Forms.CheckBox();
            this.textBoxStartLength = new System.Windows.Forms.TextBox();
            this.checkBoxECG = new System.Windows.Forms.CheckBox();
            this.checkBoxAnnotation = new System.Windows.Forms.CheckBox();
            this.pictureBoxAudio = new System.Windows.Forms.PictureBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.comboBoxStartType = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBoxCommand = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.checkBoxAfib = new System.Windows.Forms.CheckBox();
            this.pictureBoxRecordLength = new System.Windows.Forms.PictureBox();
            this.label16 = new System.Windows.Forms.Label();
            this.pictureBoxPostTrigger = new System.Windows.Forms.PictureBox();
            this.pictureBoxPreTrigger = new System.Windows.Forms.PictureBox();
            this.pictureBoxPauseOnset = new System.Windows.Forms.PictureBox();
            this.comboBoxPostTrigger = new System.Windows.Forms.ComboBox();
            this.pictureBoxAfib = new System.Windows.Forms.PictureBox();
            this.comboBoxPreTrigger = new System.Windows.Forms.ComboBox();
            this.pictureBoxTachyOnset = new System.Windows.Forms.PictureBox();
            this.pictureBoxBradyOnset = new System.Windows.Forms.PictureBox();
            this.label59 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxBradyOnset = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.textBoxRecordLength = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.textBoxTachyOnset = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.textBoxPauseOnset = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label45 = new System.Windows.Forms.Label();
            this.labelUseDevSet = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.labelProcEnd = new System.Windows.Forms.Label();
            this.pictureBoxServerUrl = new System.Windows.Forms.PictureBox();
            this.textBoxServerUrl = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.textBoxProcState = new System.Windows.Forms.TextBox();
            this.textBoxProcType = new System.Windows.Forms.TextBox();
            this.pictureBoxProcState = new System.Windows.Forms.PictureBox();
            this.pictureBoxProcType = new System.Windows.Forms.PictureBox();
            this.pictureBoxProcLength = new System.Windows.Forms.PictureBox();
            this.pictureBoxStartTime = new System.Windows.Forms.PictureBox();
            this.pictureBoxComment = new System.Windows.Forms.PictureBox();
            this.pictureBoxPhysisian = new System.Windows.Forms.PictureBox();
            this.pictureBoxDOB = new System.Windows.Forms.PictureBox();
            this.pictureBoxLast = new System.Windows.Forms.PictureBox();
            this.pictureBoxMiddle = new System.Windows.Forms.PictureBox();
            this.textBoxComment = new System.Windows.Forms.TextBox();
            this.pictureBoxFirst = new System.Windows.Forms.PictureBox();
            this.label49 = new System.Windows.Forms.Label();
            this.pictureBoxPatientID = new System.Windows.Forms.PictureBox();
            this.textBoxDOB = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.textBoxPhysisian = new System.Windows.Forms.TextBox();
            this.textBoxLast = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.textBoxMiddle = new System.Windows.Forms.TextBox();
            this.textBoxProcLength = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.textBoxFirst = new System.Windows.Forms.TextBox();
            this.textBoxStartTime = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.textBoxPatientID = new System.Windows.Forms.TextBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.labelClearPatient = new System.Windows.Forms.Label();
            this.labelUseDevPat = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.labelMarkState = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.pictureBoxAddress = new System.Windows.Forms.PictureBox();
            this.textBoxAddress = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBoxCommunicationDelay = new System.Windows.Forms.PictureBox();
            this.label41 = new System.Windows.Forms.Label();
            this.textBoxCommunicationDelay = new System.Windows.Forms.TextBox();
            this.labelCommunicationDelay = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.comboBoxSource = new System.Windows.Forms.TextBox();
            this.textBoxCable = new System.Windows.Forms.TextBox();
            this.textBoxLeadOff = new System.Windows.Forms.TextBox();
            this.pictureBoxLeadOff = new System.Windows.Forms.PictureBox();
            this.pictureBoxCable = new System.Windows.Forms.PictureBox();
            this.pictureBoxSource = new System.Windows.Forms.PictureBox();
            this.pictureBoxBatLevel = new System.Windows.Forms.PictureBox();
            this.pictureBoxAppVersion = new System.Windows.Forms.PictureBox();
            this.pictureBoxFirmware = new System.Windows.Forms.PictureBox();
            this.pictureBoxModel = new System.Windows.Forms.PictureBox();
            this.pictureBoxDevice = new System.Windows.Forms.PictureBox();
            this.textBoxFirmware = new System.Windows.Forms.TextBox();
            this.checkBoxTTX = new System.Windows.Forms.CheckBox();
            this.pictureBoxManualLimit = new System.Windows.Forms.PictureBox();
            this.textBoxModel = new System.Windows.Forms.TextBox();
            this.pictureBoxAutoLimit = new System.Windows.Forms.PictureBox();
            this.textBoxDevice = new System.Windows.Forms.TextBox();
            this.checkBoxUiLock = new System.Windows.Forms.CheckBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.pictureBoxUiLock = new System.Windows.Forms.PictureBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.pictureBoxTTX = new System.Windows.Forms.PictureBox();
            this.label25 = new System.Windows.Forms.Label();
            this.pictureBoxSampleRate = new System.Windows.Forms.PictureBox();
            this.label61 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxBatLevel = new System.Windows.Forms.TextBox();
            this.textBoxManualLimit = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.textBoxAutoLimit = new System.Windows.Forms.TextBox();
            this.textBoxAppVersion = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.comboBoxSampleRate = new System.Windows.Forms.ComboBox();
            this.label62 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label28 = new System.Windows.Forms.Label();
            this.labelDeviceSnr = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelMarkAll = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.labelSaveDefaults = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.labelLoadDefaults = new System.Windows.Forms.Label();
            this.labelLoadSettings = new System.Windows.Forms.Label();
            this.labelSaveAs = new System.Windows.Forms.Label();
            this.labelSaveToServer = new System.Windows.Forms.Label();
            this.labelClose = new System.Windows.Forms.Label();
            this.labelLoadCurrent = new System.Windows.Forms.Label();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialogSirS = new System.Windows.Forms.OpenFileDialog();
            this.openFileDialogTxt = new System.Windows.Forms.OpenFileDialog();
            this.timerRefresh = new System.Windows.Forms.Timer(this.components);
            this.panelAutoCollect = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.labelAcLastScan = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.panel19 = new System.Windows.Forms.Panel();
            this.labelAcSavedInfo = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.buttonAcRefreshInfo = new System.Windows.Forms.Button();
            this.panel17 = new System.Windows.Forms.Panel();
            this.buttonAcTest = new System.Windows.Forms.Button();
            this.checkBoxAutoRunTest = new System.Windows.Forms.CheckBox();
            this.checkBoxAcEvent = new System.Windows.Forms.CheckBox();
            this.checkBoxCollectEnabled = new System.Windows.Forms.CheckBox();
            this.label79 = new System.Windows.Forms.Label();
            this.buttonModeSave = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.labelStudyActive = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxEqual)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNotEqual)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAudio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecordLength)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPostTrigger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPreTrigger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPauseOnset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAfib)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxTachyOnset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBradyOnset)).BeginInit();
            this.panel12.SuspendLayout();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxServerUrl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxProcState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxProcType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxProcLength)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStartTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPhysisian)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDOB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMiddle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFirst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPatientID)).BeginInit();
            this.panel10.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCommunicationDelay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLeadOff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBatLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAppVersion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFirmware)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxModel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDevice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxManualLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAutoLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxUiLock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxTTX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSampleRate)).BeginInit();
            this.panel8.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panelAutoCollect.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1003, 2);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel12);
            this.panel2.Controls.Add(this.panel11);
            this.panel2.Controls.Add(this.panel10);
            this.panel2.Controls.Add(this.panel9);
            this.panel2.Controls.Add(this.panel8);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1003, 587);
            this.panel2.TabIndex = 1;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.labelResult);
            this.panel5.Controls.Add(this.panel13);
            this.panel5.Controls.Add(this.checkBoxAutoCollect);
            this.panel5.Controls.Add(this.buttonClearQueue);
            this.panel5.Controls.Add(this.label43);
            this.panel5.Controls.Add(this.buttonRefresh);
            this.panel5.Controls.Add(this.label33);
            this.panel5.Controls.Add(this.label36);
            this.panel5.Controls.Add(this.labelResultError);
            this.panel5.Controls.Add(this.labelSettingFileInfo);
            this.panel5.Controls.Add(this.labelActionFileInfo);
            this.panel5.Controls.Add(this.pictureBoxEqual);
            this.panel5.Controls.Add(this.pictureBoxNotEqual);
            this.panel5.Controls.Add(this.label34);
            this.panel5.Controls.Add(this.label38);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 473);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1003, 114);
            this.panel5.TabIndex = 182;
            // 
            // labelResult
            // 
            this.labelResult.BackColor = System.Drawing.Color.Green;
            this.labelResult.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.labelResult.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelResult.ForeColor = System.Drawing.Color.White;
            this.labelResult.Location = new System.Drawing.Point(4, 47);
            this.labelResult.Multiline = true;
            this.labelResult.Name = "labelResult";
            this.labelResult.Size = new System.Drawing.Size(733, 39);
            this.labelResult.TabIndex = 254;
            this.labelResult.Text = "Result of action\r\nmultiple lines";
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.labelSetting);
            this.panel13.Controls.Add(this.label71);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel13.Location = new System.Drawing.Point(0, 90);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(1003, 24);
            this.panel13.TabIndex = 253;
            // 
            // labelSetting
            // 
            this.labelSetting.AutoSize = true;
            this.labelSetting.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSetting.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelSetting.ForeColor = System.Drawing.Color.Black;
            this.labelSetting.Location = new System.Drawing.Point(746, 0);
            this.labelSetting.Name = "labelSetting";
            this.labelSetting.Size = new System.Drawing.Size(257, 16);
            this.labelSetting.TabIndex = 184;
            this.labelSetting.Text = " Only one Setting + Action is stored!";
            this.labelSetting.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelSetting.DoubleClick += new System.EventHandler(this.labelSetting_DoubleClick);
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Dock = System.Windows.Forms.DockStyle.Left;
            this.label71.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label71.ForeColor = System.Drawing.Color.Black;
            this.label71.Location = new System.Drawing.Point(0, 0);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(663, 16);
            this.label71.TabIndex = 182;
            this.label71.Text = "Note: settings are stored on the server from where the device collects it - delay" +
    "s may occur ! ";
            this.label71.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // checkBoxAutoCollect
            // 
            this.checkBoxAutoCollect.AutoSize = true;
            this.checkBoxAutoCollect.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxAutoCollect.Location = new System.Drawing.Point(749, 50);
            this.checkBoxAutoCollect.Name = "checkBoxAutoCollect";
            this.checkBoxAutoCollect.Size = new System.Drawing.Size(135, 20);
            this.checkBoxAutoCollect.TabIndex = 252;
            this.checkBoxAutoCollect.Text = "View Auto Collect";
            this.checkBoxAutoCollect.UseVisualStyleBackColor = true;
            this.checkBoxAutoCollect.CheckedChanged += new System.EventHandler(this.checkBoxAutoCollect_CheckedChanged);
            // 
            // buttonClearQueue
            // 
            this.buttonClearQueue.BackColor = System.Drawing.Color.MediumPurple;
            this.buttonClearQueue.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonClearQueue.ForeColor = System.Drawing.Color.White;
            this.buttonClearQueue.Location = new System.Drawing.Point(553, 21);
            this.buttonClearQueue.Name = "buttonClearQueue";
            this.buttonClearQueue.Size = new System.Drawing.Size(118, 28);
            this.buttonClearQueue.TabIndex = 251;
            this.buttonClearQueue.Text = "Clear Queue";
            this.buttonClearQueue.UseVisualStyleBackColor = false;
            this.buttonClearQueue.Visible = false;
            this.buttonClearQueue.Click += new System.EventHandler(this.buttonClearQueue_Click);
            // 
            // label43
            // 
            this.label43.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label43.ForeColor = System.Drawing.Color.Black;
            this.label43.Location = new System.Drawing.Point(668, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(66, 30);
            this.label43.TabIndex = 182;
            this.label43.Text = "Legend:";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.BackColor = System.Drawing.Color.MediumPurple;
            this.buttonRefresh.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonRefresh.ForeColor = System.Drawing.Color.White;
            this.buttonRefresh.Location = new System.Drawing.Point(3, 6);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(104, 39);
            this.buttonRefresh.TabIndex = 214;
            this.buttonRefresh.Text = "Refresh:";
            this.buttonRefresh.UseVisualStyleBackColor = false;
            this.buttonRefresh.Click += new System.EventHandler(this.buttonRefresh_Click);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Arial", 10F);
            this.label33.Location = new System.Drawing.Point(122, 6);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(109, 16);
            this.label33.TabIndex = 208;
            this.label33.Text = "Last Setting file:";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Arial", 10F);
            this.label36.Location = new System.Drawing.Point(122, 29);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(105, 16);
            this.label36.TabIndex = 209;
            this.label36.Text = "Last Status file:";
            // 
            // labelResultError
            // 
            this.labelResultError.AutoSize = true;
            this.labelResultError.Font = new System.Drawing.Font("Arial", 10F);
            this.labelResultError.Location = new System.Drawing.Point(746, 69);
            this.labelResultError.Name = "labelResultError";
            this.labelResultError.Size = new System.Drawing.Size(95, 16);
            this.labelResultError.TabIndex = 250;
            this.labelResultError.Text = "failure error jjj";
            this.labelResultError.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSettingFileInfo
            // 
            this.labelSettingFileInfo.AutoSize = true;
            this.labelSettingFileInfo.Font = new System.Drawing.Font("Arial", 10F);
            this.labelSettingFileInfo.Location = new System.Drawing.Point(241, 6);
            this.labelSettingFileInfo.Name = "labelSettingFileInfo";
            this.labelSettingFileInfo.Size = new System.Drawing.Size(374, 16);
            this.labelSettingFileInfo.TabIndex = 212;
            this.labelSettingFileInfo.Text = "^2017/12/22 12:15:24 AM , 10 in Queue 20199999222222";
            // 
            // labelActionFileInfo
            // 
            this.labelActionFileInfo.AutoSize = true;
            this.labelActionFileInfo.Font = new System.Drawing.Font("Arial", 10F);
            this.labelActionFileInfo.Location = new System.Drawing.Point(241, 29);
            this.labelActionFileInfo.Name = "labelActionFileInfo";
            this.labelActionFileInfo.Size = new System.Drawing.Size(147, 16);
            this.labelActionFileInfo.TabIndex = 213;
            this.labelActionFileInfo.Text = "20117/12/22 12:15:24";
            // 
            // pictureBoxEqual
            // 
            this.pictureBoxEqual.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxEqual.BackgroundImage")));
            this.pictureBoxEqual.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxEqual.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxEqual.InitialImage")));
            this.pictureBoxEqual.Location = new System.Drawing.Point(743, 1);
            this.pictureBoxEqual.Name = "pictureBoxEqual";
            this.pictureBoxEqual.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxEqual.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBoxEqual.TabIndex = 244;
            this.pictureBoxEqual.TabStop = false;
            // 
            // pictureBoxNotEqual
            // 
            this.pictureBoxNotEqual.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxNotEqual.BackgroundImage")));
            this.pictureBoxNotEqual.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxNotEqual.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxNotEqual.InitialImage")));
            this.pictureBoxNotEqual.Location = new System.Drawing.Point(743, 26);
            this.pictureBoxNotEqual.Name = "pictureBoxNotEqual";
            this.pictureBoxNotEqual.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxNotEqual.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBoxNotEqual.TabIndex = 243;
            this.pictureBoxNotEqual.TabStop = false;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Arial", 10F);
            this.label34.Location = new System.Drawing.Point(773, 6);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(194, 16);
            this.label34.TabIndex = 246;
            this.label34.Text = "Settings and status are Equal";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Arial", 10F);
            this.label38.Location = new System.Drawing.Point(773, 29);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(227, 16);
            this.label38.TabIndex = 248;
            this.label38.Text = "Settings and status are NOT Equal";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.labelEndQuiry);
            this.panel4.Controls.Add(this.label24);
            this.panel4.Controls.Add(this.dateTimePickerStart);
            this.panel4.Controls.Add(this.label32);
            this.panel4.Controls.Add(this.labelStartAtTime);
            this.panel4.Controls.Add(this.checkBoxAudio);
            this.panel4.Controls.Add(this.textBoxStartLength);
            this.panel4.Controls.Add(this.checkBoxECG);
            this.panel4.Controls.Add(this.checkBoxAnnotation);
            this.panel4.Controls.Add(this.pictureBoxAudio);
            this.panel4.Controls.Add(this.label27);
            this.panel4.Controls.Add(this.label20);
            this.panel4.Controls.Add(this.label18);
            this.panel4.Controls.Add(this.comboBoxStartType);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.comboBoxCommand);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.checkBoxAfib);
            this.panel4.Controls.Add(this.pictureBoxRecordLength);
            this.panel4.Controls.Add(this.label16);
            this.panel4.Controls.Add(this.pictureBoxPostTrigger);
            this.panel4.Controls.Add(this.pictureBoxPreTrigger);
            this.panel4.Controls.Add(this.pictureBoxPauseOnset);
            this.panel4.Controls.Add(this.comboBoxPostTrigger);
            this.panel4.Controls.Add(this.pictureBoxAfib);
            this.panel4.Controls.Add(this.comboBoxPreTrigger);
            this.panel4.Controls.Add(this.pictureBoxTachyOnset);
            this.panel4.Controls.Add(this.pictureBoxBradyOnset);
            this.panel4.Controls.Add(this.label59);
            this.panel4.Controls.Add(this.label58);
            this.panel4.Controls.Add(this.label57);
            this.panel4.Controls.Add(this.label56);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.textBoxBradyOnset);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.label14);
            this.panel4.Controls.Add(this.textBoxRecordLength);
            this.panel4.Controls.Add(this.label15);
            this.panel4.Controls.Add(this.textBoxTachyOnset);
            this.panel4.Controls.Add(this.label23);
            this.panel4.Controls.Add(this.textBoxPauseOnset);
            this.panel4.Controls.Add(this.label22);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 361);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1003, 112);
            this.panel4.TabIndex = 73;
            // 
            // labelEndQuiry
            // 
            this.labelEndQuiry.AutoSize = true;
            this.labelEndQuiry.Font = new System.Drawing.Font("Arial", 10F);
            this.labelEndQuiry.Location = new System.Drawing.Point(821, 90);
            this.labelEndQuiry.Name = "labelEndQuiry";
            this.labelEndQuiry.Size = new System.Drawing.Size(151, 16);
            this.labelEndQuiry.TabIndex = 281;
            this.labelEndQuiry.Text = "^2017-06-10  16:19:53";
            this.labelEndQuiry.Click += new System.EventHandler(this.labelEndQuiry_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Arial", 10F);
            this.label24.Location = new System.Drawing.Point(876, 65);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(22, 16);
            this.label24.TabIndex = 280;
            this.label24.Text = "@";
            this.label24.Click += new System.EventHandler(this.label24_Click);
            // 
            // dateTimePickerStart
            // 
            this.dateTimePickerStart.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            this.dateTimePickerStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerStart.Location = new System.Drawing.Point(725, 63);
            this.dateTimePickerStart.Name = "dateTimePickerStart";
            this.dateTimePickerStart.Size = new System.Drawing.Size(150, 20);
            this.dateTimePickerStart.TabIndex = 279;
            this.dateTimePickerStart.ValueChanged += new System.EventHandler(this.dateTimePickerStart_ValueChanged);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Arial", 10F);
            this.label32.Location = new System.Drawing.Point(784, 89);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(30, 16);
            this.label32.TabIndex = 278;
            this.label32.Text = "sec";
            // 
            // labelStartAtTime
            // 
            this.labelStartAtTime.AutoSize = true;
            this.labelStartAtTime.Font = new System.Drawing.Font("Arial", 10F);
            this.labelStartAtTime.Location = new System.Drawing.Point(897, 66);
            this.labelStartAtTime.Name = "labelStartAtTime";
            this.labelStartAtTime.Size = new System.Drawing.Size(92, 16);
            this.labelStartAtTime.TabIndex = 277;
            this.labelStartAtTime.Text = "^ 3d 12:23:11";
            // 
            // checkBoxAudio
            // 
            this.checkBoxAudio.AutoSize = true;
            this.checkBoxAudio.Location = new System.Drawing.Point(338, 91);
            this.checkBoxAudio.Name = "checkBoxAudio";
            this.checkBoxAudio.Size = new System.Drawing.Size(15, 14);
            this.checkBoxAudio.TabIndex = 240;
            this.checkBoxAudio.UseVisualStyleBackColor = true;
            // 
            // textBoxStartLength
            // 
            this.textBoxStartLength.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxStartLength.Location = new System.Drawing.Point(725, 86);
            this.textBoxStartLength.Name = "textBoxStartLength";
            this.textBoxStartLength.Size = new System.Drawing.Size(50, 23);
            this.textBoxStartLength.TabIndex = 276;
            this.textBoxStartLength.Text = "^3600";
            this.textBoxStartLength.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxStartLength.TextChanged += new System.EventHandler(this.textBoxStartLength_TextChanged);
            // 
            // checkBoxECG
            // 
            this.checkBoxECG.AutoSize = true;
            this.checkBoxECG.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxECG.Location = new System.Drawing.Point(576, 89);
            this.checkBoxECG.Name = "checkBoxECG";
            this.checkBoxECG.Size = new System.Drawing.Size(57, 20);
            this.checkBoxECG.TabIndex = 274;
            this.checkBoxECG.Text = "ECG";
            this.checkBoxECG.UseVisualStyleBackColor = true;
            // 
            // checkBoxAnnotation
            // 
            this.checkBoxAnnotation.AutoSize = true;
            this.checkBoxAnnotation.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxAnnotation.Location = new System.Drawing.Point(576, 63);
            this.checkBoxAnnotation.Name = "checkBoxAnnotation";
            this.checkBoxAnnotation.Size = new System.Drawing.Size(95, 20);
            this.checkBoxAnnotation.TabIndex = 273;
            this.checkBoxAnnotation.Text = "Annotation";
            this.checkBoxAnnotation.UseVisualStyleBackColor = true;
            // 
            // pictureBoxAudio
            // 
            this.pictureBoxAudio.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxAudio.BackgroundImage")));
            this.pictureBoxAudio.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxAudio.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxAudio.InitialImage")));
            this.pictureBoxAudio.Location = new System.Drawing.Point(430, 86);
            this.pictureBoxAudio.Name = "pictureBoxAudio";
            this.pictureBoxAudio.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxAudio.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxAudio.TabIndex = 235;
            this.pictureBoxAudio.TabStop = false;
            this.pictureBoxAudio.Click += new System.EventHandler(this.pictureBoxAudio_Click);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Arial", 10F);
            this.label27.Location = new System.Drawing.Point(668, 89);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(56, 16);
            this.label27.TabIndex = 272;
            this.label27.Text = "Length:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial", 10F);
            this.label20.Location = new System.Drawing.Point(668, 65);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(58, 16);
            this.label20.TabIndex = 271;
            this.label20.Text = "Start at:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 10F);
            this.label18.Location = new System.Drawing.Point(478, 65);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(99, 16);
            this.label18.TabIndex = 269;
            this.label18.Text = "Data Request:";
            // 
            // comboBoxStartType
            // 
            this.comboBoxStartType.Font = new System.Drawing.Font("Arial", 10F);
            this.comboBoxStartType.FormattingEnabled = true;
            this.comboBoxStartType.Items.AddRange(new object[] {
            "Holter",
            "Event",
            "Post",
            "MCT"});
            this.comboBoxStartType.Location = new System.Drawing.Point(647, 36);
            this.comboBoxStartType.Name = "comboBoxStartType";
            this.comboBoxStartType.Size = new System.Drawing.Size(78, 24);
            this.comboBoxStartType.TabIndex = 268;
            this.comboBoxStartType.Text = "Holter";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 10F);
            this.label5.Location = new System.Drawing.Point(569, 36);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 16);
            this.label5.TabIndex = 267;
            this.label5.Text = "Start type:";
            // 
            // comboBoxCommand
            // 
            this.comboBoxCommand.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxCommand.FormattingEnabled = true;
            this.comboBoxCommand.Items.AddRange(new object[] {
            "NONE",
            "START",
            "STOP",
            "ERASE",
            "PARAM_CHANGE",
            "DATA_REQUEST"});
            this.comboBoxCommand.Location = new System.Drawing.Point(647, 5);
            this.comboBoxCommand.Name = "comboBoxCommand";
            this.comboBoxCommand.Size = new System.Drawing.Size(223, 27);
            this.comboBoxCommand.TabIndex = 266;
            this.comboBoxCommand.Text = "PARAM_CHANGE";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(478, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(174, 19);
            this.label3.TabIndex = 265;
            this.label3.Text = "Operation command: ";
            // 
            // checkBoxAfib
            // 
            this.checkBoxAfib.AutoSize = true;
            this.checkBoxAfib.Location = new System.Drawing.Point(104, 91);
            this.checkBoxAfib.Name = "checkBoxAfib";
            this.checkBoxAfib.Size = new System.Drawing.Size(15, 14);
            this.checkBoxAfib.TabIndex = 263;
            this.checkBoxAfib.UseVisualStyleBackColor = true;
            // 
            // pictureBoxRecordLength
            // 
            this.pictureBoxRecordLength.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxRecordLength.BackgroundImage")));
            this.pictureBoxRecordLength.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxRecordLength.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxRecordLength.InitialImage")));
            this.pictureBoxRecordLength.Location = new System.Drawing.Point(430, 59);
            this.pictureBoxRecordLength.Name = "pictureBoxRecordLength";
            this.pictureBoxRecordLength.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxRecordLength.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxRecordLength.TabIndex = 257;
            this.pictureBoxRecordLength.TabStop = false;
            this.pictureBoxRecordLength.Click += new System.EventHandler(this.pictureBoxRecordLength_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 10F);
            this.label16.Location = new System.Drawing.Point(233, 90);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(79, 16);
            this.label16.TabIndex = 185;
            this.label16.Text = "Audio Mute";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBoxPostTrigger
            // 
            this.pictureBoxPostTrigger.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxPostTrigger.BackgroundImage")));
            this.pictureBoxPostTrigger.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxPostTrigger.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxPostTrigger.InitialImage")));
            this.pictureBoxPostTrigger.Location = new System.Drawing.Point(430, 32);
            this.pictureBoxPostTrigger.Name = "pictureBoxPostTrigger";
            this.pictureBoxPostTrigger.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxPostTrigger.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxPostTrigger.TabIndex = 256;
            this.pictureBoxPostTrigger.TabStop = false;
            this.pictureBoxPostTrigger.Click += new System.EventHandler(this.pictureBoxPostTrigger_Click);
            // 
            // pictureBoxPreTrigger
            // 
            this.pictureBoxPreTrigger.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxPreTrigger.BackgroundImage")));
            this.pictureBoxPreTrigger.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxPreTrigger.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxPreTrigger.InitialImage")));
            this.pictureBoxPreTrigger.Location = new System.Drawing.Point(430, 4);
            this.pictureBoxPreTrigger.Name = "pictureBoxPreTrigger";
            this.pictureBoxPreTrigger.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxPreTrigger.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxPreTrigger.TabIndex = 255;
            this.pictureBoxPreTrigger.TabStop = false;
            this.pictureBoxPreTrigger.Click += new System.EventHandler(this.pictureBoxPreTrigger_Click);
            // 
            // pictureBoxPauseOnset
            // 
            this.pictureBoxPauseOnset.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxPauseOnset.BackgroundImage")));
            this.pictureBoxPauseOnset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxPauseOnset.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxPauseOnset.InitialImage")));
            this.pictureBoxPauseOnset.Location = new System.Drawing.Point(193, 59);
            this.pictureBoxPauseOnset.Name = "pictureBoxPauseOnset";
            this.pictureBoxPauseOnset.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxPauseOnset.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxPauseOnset.TabIndex = 254;
            this.pictureBoxPauseOnset.TabStop = false;
            this.pictureBoxPauseOnset.Click += new System.EventHandler(this.pictureBoxPauseOnset_Click);
            // 
            // comboBoxPostTrigger
            // 
            this.comboBoxPostTrigger.Font = new System.Drawing.Font("Arial", 10F);
            this.comboBoxPostTrigger.FormattingEnabled = true;
            this.comboBoxPostTrigger.Items.AddRange(new object[] {
            "15",
            "30",
            "45",
            "60",
            "90",
            "120",
            "180",
            "300"});
            this.comboBoxPostTrigger.Location = new System.Drawing.Point(338, 31);
            this.comboBoxPostTrigger.Name = "comboBoxPostTrigger";
            this.comboBoxPostTrigger.Size = new System.Drawing.Size(50, 24);
            this.comboBoxPostTrigger.TabIndex = 242;
            // 
            // pictureBoxAfib
            // 
            this.pictureBoxAfib.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxAfib.BackgroundImage")));
            this.pictureBoxAfib.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxAfib.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxAfib.InitialImage")));
            this.pictureBoxAfib.Location = new System.Drawing.Point(193, 86);
            this.pictureBoxAfib.Name = "pictureBoxAfib";
            this.pictureBoxAfib.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxAfib.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxAfib.TabIndex = 253;
            this.pictureBoxAfib.TabStop = false;
            this.pictureBoxAfib.Click += new System.EventHandler(this.pictureBoxAfib_Click);
            // 
            // comboBoxPreTrigger
            // 
            this.comboBoxPreTrigger.Font = new System.Drawing.Font("Arial", 10F);
            this.comboBoxPreTrigger.FormattingEnabled = true;
            this.comboBoxPreTrigger.Items.AddRange(new object[] {
            "15",
            "30",
            "45",
            "60",
            "90",
            "120",
            "180",
            "300"});
            this.comboBoxPreTrigger.Location = new System.Drawing.Point(338, 3);
            this.comboBoxPreTrigger.Name = "comboBoxPreTrigger";
            this.comboBoxPreTrigger.Size = new System.Drawing.Size(50, 24);
            this.comboBoxPreTrigger.TabIndex = 241;
            this.comboBoxPreTrigger.Text = "^99";
            // 
            // pictureBoxTachyOnset
            // 
            this.pictureBoxTachyOnset.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxTachyOnset.BackgroundImage")));
            this.pictureBoxTachyOnset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxTachyOnset.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxTachyOnset.InitialImage")));
            this.pictureBoxTachyOnset.Location = new System.Drawing.Point(193, 32);
            this.pictureBoxTachyOnset.Name = "pictureBoxTachyOnset";
            this.pictureBoxTachyOnset.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxTachyOnset.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxTachyOnset.TabIndex = 252;
            this.pictureBoxTachyOnset.TabStop = false;
            this.pictureBoxTachyOnset.Click += new System.EventHandler(this.pictureBoxTachyOnset_Click);
            // 
            // pictureBoxBradyOnset
            // 
            this.pictureBoxBradyOnset.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxBradyOnset.BackgroundImage")));
            this.pictureBoxBradyOnset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxBradyOnset.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxBradyOnset.InitialImage")));
            this.pictureBoxBradyOnset.Location = new System.Drawing.Point(193, 3);
            this.pictureBoxBradyOnset.Name = "pictureBoxBradyOnset";
            this.pictureBoxBradyOnset.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxBradyOnset.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxBradyOnset.TabIndex = 251;
            this.pictureBoxBradyOnset.TabStop = false;
            this.pictureBoxBradyOnset.Click += new System.EventHandler(this.pictureBoxBradyOnset_Click);
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Arial", 10F);
            this.label59.Location = new System.Drawing.Point(233, 63);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(101, 16);
            this.label59.TabIndex = 234;
            this.label59.Text = "Record length:";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Arial", 10F);
            this.label58.Location = new System.Drawing.Point(233, 36);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(85, 16);
            this.label58.TabIndex = 233;
            this.label58.Text = "Post trigger:";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Arial", 10F);
            this.label57.Location = new System.Drawing.Point(233, 7);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(79, 16);
            this.label57.TabIndex = 232;
            this.label57.Text = "Pre trigger:";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Arial", 10F);
            this.label56.Location = new System.Drawing.Point(6, 90);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(73, 16);
            this.label56.TabIndex = 230;
            this.label56.Text = "AF detect:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 10F);
            this.label4.Location = new System.Drawing.Point(6, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 16);
            this.label4.TabIndex = 61;
            this.label4.Text = "Brady:";
            // 
            // textBoxBradyOnset
            // 
            this.textBoxBradyOnset.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxBradyOnset.Location = new System.Drawing.Point(104, 4);
            this.textBoxBradyOnset.Name = "textBoxBradyOnset";
            this.textBoxBradyOnset.Size = new System.Drawing.Size(40, 23);
            this.textBoxBradyOnset.TabIndex = 64;
            this.textBoxBradyOnset.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 10F);
            this.label7.Location = new System.Drawing.Point(150, 7);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 16);
            this.label7.TabIndex = 67;
            this.label7.Text = "bpm";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 10F);
            this.label9.Location = new System.Drawing.Point(387, 7);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(30, 16);
            this.label9.TabIndex = 69;
            this.label9.Text = "sec";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 10F);
            this.label6.Location = new System.Drawing.Point(387, 36);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 16);
            this.label6.TabIndex = 76;
            this.label6.Text = "sec";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 10F);
            this.label14.Location = new System.Drawing.Point(6, 36);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(49, 16);
            this.label14.TabIndex = 70;
            this.label14.Text = "Tachy:";
            // 
            // textBoxRecordLength
            // 
            this.textBoxRecordLength.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxRecordLength.Location = new System.Drawing.Point(338, 59);
            this.textBoxRecordLength.Name = "textBoxRecordLength";
            this.textBoxRecordLength.Size = new System.Drawing.Size(50, 23);
            this.textBoxRecordLength.TabIndex = 80;
            this.textBoxRecordLength.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 10F);
            this.label15.Location = new System.Drawing.Point(387, 63);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(38, 16);
            this.label15.TabIndex = 83;
            this.label15.Text = "days";
            // 
            // textBoxTachyOnset
            // 
            this.textBoxTachyOnset.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxTachyOnset.Location = new System.Drawing.Point(104, 33);
            this.textBoxTachyOnset.Name = "textBoxTachyOnset";
            this.textBoxTachyOnset.Size = new System.Drawing.Size(40, 23);
            this.textBoxTachyOnset.TabIndex = 71;
            this.textBoxTachyOnset.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Arial", 10F);
            this.label23.Location = new System.Drawing.Point(6, 63);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(52, 16);
            this.label23.TabIndex = 84;
            this.label23.Text = "Pause:";
            // 
            // textBoxPauseOnset
            // 
            this.textBoxPauseOnset.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxPauseOnset.Location = new System.Drawing.Point(104, 60);
            this.textBoxPauseOnset.Name = "textBoxPauseOnset";
            this.textBoxPauseOnset.Size = new System.Drawing.Size(40, 23);
            this.textBoxPauseOnset.TabIndex = 85;
            this.textBoxPauseOnset.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Arial", 10F);
            this.label22.Location = new System.Drawing.Point(150, 63);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(30, 16);
            this.label22.TabIndex = 88;
            this.label22.Text = "sec";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 10F);
            this.label12.Location = new System.Drawing.Point(150, 36);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(35, 16);
            this.label12.TabIndex = 74;
            this.label12.Text = "bpm";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.MediumPurple;
            this.panel12.Controls.Add(this.label45);
            this.panel12.Controls.Add(this.labelUseDevSet);
            this.panel12.Controls.Add(this.label13);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel12.Location = new System.Drawing.Point(0, 340);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(1003, 21);
            this.panel12.TabIndex = 72;
            // 
            // label45
            // 
            this.label45.Dock = System.Windows.Forms.DockStyle.Right;
            this.label45.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label45.ForeColor = System.Drawing.Color.White;
            this.label45.Location = new System.Drawing.Point(481, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(337, 21);
            this.label45.TabIndex = 192;
            this.label45.Text = "Action";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelUseDevSet
            // 
            this.labelUseDevSet.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelUseDevSet.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelUseDevSet.ForeColor = System.Drawing.Color.White;
            this.labelUseDevSet.Location = new System.Drawing.Point(818, 0);
            this.labelUseDevSet.Name = "labelUseDevSet";
            this.labelUseDevSet.Size = new System.Drawing.Size(185, 21);
            this.labelUseDevSet.TabIndex = 191;
            this.labelUseDevSet.Text = "Use device settings";
            this.labelUseDevSet.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelUseDevSet.Click += new System.EventHandler(this.labelUseDevSet_Click);
            // 
            // label13
            // 
            this.label13.Dock = System.Windows.Forms.DockStyle.Left;
            this.label13.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(0, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(141, 21);
            this.label13.TabIndex = 180;
            this.label13.Text = "Triggers";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.labelProcEnd);
            this.panel11.Controls.Add(this.pictureBoxServerUrl);
            this.panel11.Controls.Add(this.textBoxServerUrl);
            this.panel11.Controls.Add(this.label37);
            this.panel11.Controls.Add(this.textBoxProcState);
            this.panel11.Controls.Add(this.textBoxProcType);
            this.panel11.Controls.Add(this.pictureBoxProcState);
            this.panel11.Controls.Add(this.pictureBoxProcType);
            this.panel11.Controls.Add(this.pictureBoxProcLength);
            this.panel11.Controls.Add(this.pictureBoxStartTime);
            this.panel11.Controls.Add(this.pictureBoxComment);
            this.panel11.Controls.Add(this.pictureBoxPhysisian);
            this.panel11.Controls.Add(this.pictureBoxDOB);
            this.panel11.Controls.Add(this.pictureBoxLast);
            this.panel11.Controls.Add(this.pictureBoxMiddle);
            this.panel11.Controls.Add(this.textBoxComment);
            this.panel11.Controls.Add(this.pictureBoxFirst);
            this.panel11.Controls.Add(this.label49);
            this.panel11.Controls.Add(this.pictureBoxPatientID);
            this.panel11.Controls.Add(this.textBoxDOB);
            this.panel11.Controls.Add(this.label48);
            this.panel11.Controls.Add(this.textBoxPhysisian);
            this.panel11.Controls.Add(this.textBoxLast);
            this.panel11.Controls.Add(this.label55);
            this.panel11.Controls.Add(this.label47);
            this.panel11.Controls.Add(this.label54);
            this.panel11.Controls.Add(this.textBoxMiddle);
            this.panel11.Controls.Add(this.textBoxProcLength);
            this.panel11.Controls.Add(this.label46);
            this.panel11.Controls.Add(this.label52);
            this.panel11.Controls.Add(this.textBoxFirst);
            this.panel11.Controls.Add(this.textBoxStartTime);
            this.panel11.Controls.Add(this.label50);
            this.panel11.Controls.Add(this.label31);
            this.panel11.Controls.Add(this.label29);
            this.panel11.Controls.Add(this.label40);
            this.panel11.Controls.Add(this.textBoxPatientID);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel11.Location = new System.Drawing.Point(0, 192);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(1003, 148);
            this.panel11.TabIndex = 71;
            this.panel11.Paint += new System.Windows.Forms.PaintEventHandler(this.panel11_Paint);
            // 
            // labelProcEnd
            // 
            this.labelProcEnd.AutoSize = true;
            this.labelProcEnd.Font = new System.Drawing.Font("Arial", 10F);
            this.labelProcEnd.Location = new System.Drawing.Point(842, 37);
            this.labelProcEnd.Name = "labelProcEnd";
            this.labelProcEnd.Size = new System.Drawing.Size(151, 16);
            this.labelProcEnd.TabIndex = 256;
            this.labelProcEnd.Text = "^2017-06-10  16:19:53";
            this.labelProcEnd.DoubleClick += new System.EventHandler(this.labelProcEnd_DoubleClick);
            // 
            // pictureBoxServerUrl
            // 
            this.pictureBoxServerUrl.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxServerUrl.BackgroundImage")));
            this.pictureBoxServerUrl.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxServerUrl.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxServerUrl.InitialImage")));
            this.pictureBoxServerUrl.Location = new System.Drawing.Point(941, 118);
            this.pictureBoxServerUrl.Name = "pictureBoxServerUrl";
            this.pictureBoxServerUrl.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxServerUrl.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxServerUrl.TabIndex = 255;
            this.pictureBoxServerUrl.TabStop = false;
            this.pictureBoxServerUrl.Click += new System.EventHandler(this.pictureBoxServerUrl_Click);
            // 
            // textBoxServerUrl
            // 
            this.textBoxServerUrl.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxServerUrl.Location = new System.Drawing.Point(677, 119);
            this.textBoxServerUrl.Name = "textBoxServerUrl";
            this.textBoxServerUrl.ReadOnly = true;
            this.textBoxServerUrl.Size = new System.Drawing.Size(259, 23);
            this.textBoxServerUrl.TabIndex = 254;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Arial", 10F);
            this.label37.Location = new System.Drawing.Point(554, 122);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(74, 16);
            this.label37.TabIndex = 253;
            this.label37.Text = "Server url:";
            // 
            // textBoxProcState
            // 
            this.textBoxProcState.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxProcState.Location = new System.Drawing.Point(679, 91);
            this.textBoxProcState.Name = "textBoxProcState";
            this.textBoxProcState.ReadOnly = true;
            this.textBoxProcState.Size = new System.Drawing.Size(113, 23);
            this.textBoxProcState.TabIndex = 252;
            // 
            // textBoxProcType
            // 
            this.textBoxProcType.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxProcType.Location = new System.Drawing.Point(679, 63);
            this.textBoxProcType.Name = "textBoxProcType";
            this.textBoxProcType.ReadOnly = true;
            this.textBoxProcType.Size = new System.Drawing.Size(113, 23);
            this.textBoxProcType.TabIndex = 251;
            // 
            // pictureBoxProcState
            // 
            this.pictureBoxProcState.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxProcState.BackgroundImage")));
            this.pictureBoxProcState.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxProcState.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxProcState.InitialImage")));
            this.pictureBoxProcState.Location = new System.Drawing.Point(798, 90);
            this.pictureBoxProcState.Name = "pictureBoxProcState";
            this.pictureBoxProcState.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxProcState.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxProcState.TabIndex = 250;
            this.pictureBoxProcState.TabStop = false;
            this.pictureBoxProcState.Click += new System.EventHandler(this.pictureBoxProcState_Click);
            // 
            // pictureBoxProcType
            // 
            this.pictureBoxProcType.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxProcType.BackgroundImage")));
            this.pictureBoxProcType.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxProcType.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxProcType.InitialImage")));
            this.pictureBoxProcType.Location = new System.Drawing.Point(798, 62);
            this.pictureBoxProcType.Name = "pictureBoxProcType";
            this.pictureBoxProcType.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxProcType.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxProcType.TabIndex = 249;
            this.pictureBoxProcType.TabStop = false;
            this.pictureBoxProcType.Click += new System.EventHandler(this.pictureBoxProcType_Click);
            // 
            // pictureBoxProcLength
            // 
            this.pictureBoxProcLength.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxProcLength.BackgroundImage")));
            this.pictureBoxProcLength.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxProcLength.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxProcLength.InitialImage")));
            this.pictureBoxProcLength.Location = new System.Drawing.Point(798, 33);
            this.pictureBoxProcLength.Name = "pictureBoxProcLength";
            this.pictureBoxProcLength.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxProcLength.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxProcLength.TabIndex = 248;
            this.pictureBoxProcLength.TabStop = false;
            this.pictureBoxProcLength.Click += new System.EventHandler(this.pictureBoxProcLength_Click);
            // 
            // pictureBoxStartTime
            // 
            this.pictureBoxStartTime.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxStartTime.BackgroundImage")));
            this.pictureBoxStartTime.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxStartTime.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxStartTime.InitialImage")));
            this.pictureBoxStartTime.Location = new System.Drawing.Point(941, 5);
            this.pictureBoxStartTime.Name = "pictureBoxStartTime";
            this.pictureBoxStartTime.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxStartTime.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxStartTime.TabIndex = 247;
            this.pictureBoxStartTime.TabStop = false;
            this.pictureBoxStartTime.Click += new System.EventHandler(this.pictureBoxStartTime_Click);
            // 
            // pictureBoxComment
            // 
            this.pictureBoxComment.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxComment.BackgroundImage")));
            this.pictureBoxComment.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxComment.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxComment.InitialImage")));
            this.pictureBoxComment.Location = new System.Drawing.Point(473, 64);
            this.pictureBoxComment.Name = "pictureBoxComment";
            this.pictureBoxComment.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxComment.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxComment.TabIndex = 246;
            this.pictureBoxComment.TabStop = false;
            this.pictureBoxComment.Click += new System.EventHandler(this.pictureBoxComment_Click);
            // 
            // pictureBoxPhysisian
            // 
            this.pictureBoxPhysisian.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxPhysisian.BackgroundImage")));
            this.pictureBoxPhysisian.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxPhysisian.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxPhysisian.InitialImage")));
            this.pictureBoxPhysisian.Location = new System.Drawing.Point(473, 5);
            this.pictureBoxPhysisian.Name = "pictureBoxPhysisian";
            this.pictureBoxPhysisian.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxPhysisian.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxPhysisian.TabIndex = 245;
            this.pictureBoxPhysisian.TabStop = false;
            this.pictureBoxPhysisian.Click += new System.EventHandler(this.pictureBoxPhysisian_Click);
            // 
            // pictureBoxDOB
            // 
            this.pictureBoxDOB.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxDOB.BackgroundImage")));
            this.pictureBoxDOB.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxDOB.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxDOB.InitialImage")));
            this.pictureBoxDOB.Location = new System.Drawing.Point(233, 118);
            this.pictureBoxDOB.Name = "pictureBoxDOB";
            this.pictureBoxDOB.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxDOB.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxDOB.TabIndex = 244;
            this.pictureBoxDOB.TabStop = false;
            this.pictureBoxDOB.Click += new System.EventHandler(this.pictureBoxDOB_Click);
            // 
            // pictureBoxLast
            // 
            this.pictureBoxLast.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxLast.BackgroundImage")));
            this.pictureBoxLast.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxLast.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxLast.InitialImage")));
            this.pictureBoxLast.Location = new System.Drawing.Point(233, 90);
            this.pictureBoxLast.Name = "pictureBoxLast";
            this.pictureBoxLast.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxLast.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxLast.TabIndex = 243;
            this.pictureBoxLast.TabStop = false;
            this.pictureBoxLast.Click += new System.EventHandler(this.pictureBoxLast_Click);
            // 
            // pictureBoxMiddle
            // 
            this.pictureBoxMiddle.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxMiddle.BackgroundImage")));
            this.pictureBoxMiddle.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxMiddle.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxMiddle.InitialImage")));
            this.pictureBoxMiddle.Location = new System.Drawing.Point(233, 62);
            this.pictureBoxMiddle.Name = "pictureBoxMiddle";
            this.pictureBoxMiddle.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxMiddle.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxMiddle.TabIndex = 242;
            this.pictureBoxMiddle.TabStop = false;
            this.pictureBoxMiddle.Click += new System.EventHandler(this.pictureBoxMiddle_Click);
            // 
            // textBoxComment
            // 
            this.textBoxComment.Location = new System.Drawing.Point(276, 64);
            this.textBoxComment.MaxLength = 100;
            this.textBoxComment.Multiline = true;
            this.textBoxComment.Name = "textBoxComment";
            this.textBoxComment.Size = new System.Drawing.Size(189, 77);
            this.textBoxComment.TabIndex = 177;
            // 
            // pictureBoxFirst
            // 
            this.pictureBoxFirst.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxFirst.BackgroundImage")));
            this.pictureBoxFirst.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxFirst.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxFirst.InitialImage")));
            this.pictureBoxFirst.Location = new System.Drawing.Point(233, 33);
            this.pictureBoxFirst.Name = "pictureBoxFirst";
            this.pictureBoxFirst.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxFirst.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxFirst.TabIndex = 241;
            this.pictureBoxFirst.TabStop = false;
            this.pictureBoxFirst.Click += new System.EventHandler(this.pictureBoxFirst_Click);
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Arial", 10F);
            this.label49.Location = new System.Drawing.Point(273, 37);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(72, 16);
            this.label49.TabIndex = 178;
            this.label49.Text = "Comment:";
            // 
            // pictureBoxPatientID
            // 
            this.pictureBoxPatientID.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxPatientID.BackgroundImage")));
            this.pictureBoxPatientID.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxPatientID.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxPatientID.InitialImage")));
            this.pictureBoxPatientID.Location = new System.Drawing.Point(233, 5);
            this.pictureBoxPatientID.Name = "pictureBoxPatientID";
            this.pictureBoxPatientID.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxPatientID.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxPatientID.TabIndex = 240;
            this.pictureBoxPatientID.TabStop = false;
            this.pictureBoxPatientID.Click += new System.EventHandler(this.pictureBoxPatientID_Click);
            // 
            // textBoxDOB
            // 
            this.textBoxDOB.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxDOB.Location = new System.Drawing.Point(104, 119);
            this.textBoxDOB.Name = "textBoxDOB";
            this.textBoxDOB.Size = new System.Drawing.Size(123, 23);
            this.textBoxDOB.TabIndex = 176;
            this.textBoxDOB.Text = "YYYY/MM/DD";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Arial", 10F);
            this.label48.Location = new System.Drawing.Point(6, 122);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(71, 16);
            this.label48.TabIndex = 171;
            this.label48.Text = "Pat. DOB:";
            // 
            // textBoxPhysisian
            // 
            this.textBoxPhysisian.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxPhysisian.Location = new System.Drawing.Point(368, 6);
            this.textBoxPhysisian.MaxLength = 16;
            this.textBoxPhysisian.Name = "textBoxPhysisian";
            this.textBoxPhysisian.Size = new System.Drawing.Size(97, 23);
            this.textBoxPhysisian.TabIndex = 175;
            // 
            // textBoxLast
            // 
            this.textBoxLast.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxLast.Location = new System.Drawing.Point(104, 91);
            this.textBoxLast.MaxLength = 16;
            this.textBoxLast.Name = "textBoxLast";
            this.textBoxLast.Size = new System.Drawing.Size(123, 23);
            this.textBoxLast.TabIndex = 174;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Arial", 10F);
            this.label55.Location = new System.Drawing.Point(554, 94);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(80, 16);
            this.label55.TabIndex = 226;
            this.label55.Text = "Proc. state:";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Arial", 10F);
            this.label47.Location = new System.Drawing.Point(273, 9);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(86, 16);
            this.label47.TabIndex = 170;
            this.label47.Text = "Phys. name:";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Arial", 10F);
            this.label54.Location = new System.Drawing.Point(554, 66);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(76, 16);
            this.label54.TabIndex = 224;
            this.label54.Text = "Proc. type:";
            // 
            // textBoxMiddle
            // 
            this.textBoxMiddle.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxMiddle.Location = new System.Drawing.Point(104, 63);
            this.textBoxMiddle.MaxLength = 1;
            this.textBoxMiddle.Name = "textBoxMiddle";
            this.textBoxMiddle.Size = new System.Drawing.Size(123, 23);
            this.textBoxMiddle.TabIndex = 173;
            // 
            // textBoxProcLength
            // 
            this.textBoxProcLength.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxProcLength.Location = new System.Drawing.Point(679, 34);
            this.textBoxProcLength.Name = "textBoxProcLength";
            this.textBoxProcLength.ReadOnly = true;
            this.textBoxProcLength.Size = new System.Drawing.Size(113, 23);
            this.textBoxProcLength.TabIndex = 223;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Arial", 10F);
            this.label46.Location = new System.Drawing.Point(6, 94);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(63, 16);
            this.label46.TabIndex = 169;
            this.label46.Text = "Pat: last:";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Arial", 10F);
            this.label52.Location = new System.Drawing.Point(554, 37);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(88, 16);
            this.label52.TabIndex = 222;
            this.label52.Text = "Proc. length:";
            // 
            // textBoxFirst
            // 
            this.textBoxFirst.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxFirst.Location = new System.Drawing.Point(104, 34);
            this.textBoxFirst.MaxLength = 16;
            this.textBoxFirst.Name = "textBoxFirst";
            this.textBoxFirst.Size = new System.Drawing.Size(123, 23);
            this.textBoxFirst.TabIndex = 172;
            // 
            // textBoxStartTime
            // 
            this.textBoxStartTime.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxStartTime.Location = new System.Drawing.Point(679, 6);
            this.textBoxStartTime.Name = "textBoxStartTime";
            this.textBoxStartTime.ReadOnly = true;
            this.textBoxStartTime.Size = new System.Drawing.Size(259, 23);
            this.textBoxStartTime.TabIndex = 195;
            this.textBoxStartTime.Text = "2017-06-10  16:19:53  -05:00";
            this.textBoxStartTime.DoubleClick += new System.EventHandler(this.textBoxStartTime_DoubleClick);
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Arial", 10F);
            this.label50.Location = new System.Drawing.Point(554, 9);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(77, 16);
            this.label50.TabIndex = 194;
            this.label50.Text = "Proc. start:";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Arial", 10F);
            this.label31.Location = new System.Drawing.Point(6, 66);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(82, 16);
            this.label31.TabIndex = 168;
            this.label31.Text = "Pat. middle:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Arial", 10F);
            this.label29.Location = new System.Drawing.Point(6, 37);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(64, 16);
            this.label29.TabIndex = 167;
            this.label29.Text = "Pat. first:";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Arial", 10F);
            this.label40.Location = new System.Drawing.Point(6, 9);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(54, 16);
            this.label40.TabIndex = 153;
            this.label40.Text = "Pat. ID:";
            // 
            // textBoxPatientID
            // 
            this.textBoxPatientID.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxPatientID.Location = new System.Drawing.Point(104, 6);
            this.textBoxPatientID.MaxLength = 16;
            this.textBoxPatientID.Name = "textBoxPatientID";
            this.textBoxPatientID.Size = new System.Drawing.Size(123, 23);
            this.textBoxPatientID.TabIndex = 154;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.MediumPurple;
            this.panel10.Controls.Add(this.labelClearPatient);
            this.panel10.Controls.Add(this.labelUseDevPat);
            this.panel10.Controls.Add(this.label70);
            this.panel10.Controls.Add(this.label10);
            this.panel10.Controls.Add(this.labelMarkState);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(0, 173);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(1003, 19);
            this.panel10.TabIndex = 70;
            // 
            // labelClearPatient
            // 
            this.labelClearPatient.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelClearPatient.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelClearPatient.ForeColor = System.Drawing.Color.White;
            this.labelClearPatient.Location = new System.Drawing.Point(305, 0);
            this.labelClearPatient.Name = "labelClearPatient";
            this.labelClearPatient.Size = new System.Drawing.Size(160, 19);
            this.labelClearPatient.TabIndex = 187;
            this.labelClearPatient.Text = "Clear Patient Info ";
            this.labelClearPatient.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelClearPatient.Click += new System.EventHandler(this.labelClearPatient_Click);
            // 
            // labelUseDevPat
            // 
            this.labelUseDevPat.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelUseDevPat.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelUseDevPat.ForeColor = System.Drawing.Color.White;
            this.labelUseDevPat.Location = new System.Drawing.Point(94, 0);
            this.labelUseDevPat.Name = "labelUseDevPat";
            this.labelUseDevPat.Size = new System.Drawing.Size(211, 19);
            this.labelUseDevPat.TabIndex = 186;
            this.labelUseDevPat.Text = "Use Patient from device ";
            this.labelUseDevPat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelUseDevPat.Click += new System.EventHandler(this.labelUseDevPat_Click);
            // 
            // label70
            // 
            this.label70.Dock = System.Windows.Forms.DockStyle.Right;
            this.label70.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label70.ForeColor = System.Drawing.Color.White;
            this.label70.Location = new System.Drawing.Point(557, 0);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(313, 19);
            this.label70.TabIndex = 182;
            this.label70.Text = "Procedure state";
            this.label70.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label10
            // 
            this.label10.Dock = System.Windows.Forms.DockStyle.Left;
            this.label10.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(0, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(94, 19);
            this.label10.TabIndex = 180;
            this.label10.Text = "Patient";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelMarkState
            // 
            this.labelMarkState.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelMarkState.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelMarkState.ForeColor = System.Drawing.Color.White;
            this.labelMarkState.Location = new System.Drawing.Point(870, 0);
            this.labelMarkState.Name = "labelMarkState";
            this.labelMarkState.Size = new System.Drawing.Size(133, 19);
            this.labelMarkState.TabIndex = 184;
            this.labelMarkState.Text = "Use State";
            this.labelMarkState.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelMarkState.Click += new System.EventHandler(this.labelMarkState_Click);
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.pictureBoxAddress);
            this.panel9.Controls.Add(this.textBoxAddress);
            this.panel9.Controls.Add(this.label39);
            this.panel9.Controls.Add(this.pictureBox1);
            this.panel9.Controls.Add(this.pictureBoxCommunicationDelay);
            this.panel9.Controls.Add(this.label41);
            this.panel9.Controls.Add(this.textBoxCommunicationDelay);
            this.panel9.Controls.Add(this.labelCommunicationDelay);
            this.panel9.Controls.Add(this.label35);
            this.panel9.Controls.Add(this.comboBoxSource);
            this.panel9.Controls.Add(this.textBoxCable);
            this.panel9.Controls.Add(this.textBoxLeadOff);
            this.panel9.Controls.Add(this.pictureBoxLeadOff);
            this.panel9.Controls.Add(this.pictureBoxCable);
            this.panel9.Controls.Add(this.pictureBoxSource);
            this.panel9.Controls.Add(this.pictureBoxBatLevel);
            this.panel9.Controls.Add(this.pictureBoxAppVersion);
            this.panel9.Controls.Add(this.pictureBoxFirmware);
            this.panel9.Controls.Add(this.pictureBoxModel);
            this.panel9.Controls.Add(this.pictureBoxDevice);
            this.panel9.Controls.Add(this.textBoxFirmware);
            this.panel9.Controls.Add(this.checkBoxTTX);
            this.panel9.Controls.Add(this.pictureBoxManualLimit);
            this.panel9.Controls.Add(this.textBoxModel);
            this.panel9.Controls.Add(this.pictureBoxAutoLimit);
            this.panel9.Controls.Add(this.textBoxDevice);
            this.panel9.Controls.Add(this.checkBoxUiLock);
            this.panel9.Controls.Add(this.label21);
            this.panel9.Controls.Add(this.label44);
            this.panel9.Controls.Add(this.pictureBoxUiLock);
            this.panel9.Controls.Add(this.label42);
            this.panel9.Controls.Add(this.label51);
            this.panel9.Controls.Add(this.pictureBoxTTX);
            this.panel9.Controls.Add(this.label25);
            this.panel9.Controls.Add(this.pictureBoxSampleRate);
            this.panel9.Controls.Add(this.label61);
            this.panel9.Controls.Add(this.label17);
            this.panel9.Controls.Add(this.label11);
            this.panel9.Controls.Add(this.textBoxBatLevel);
            this.panel9.Controls.Add(this.textBoxManualLimit);
            this.panel9.Controls.Add(this.label8);
            this.panel9.Controls.Add(this.label60);
            this.panel9.Controls.Add(this.textBoxAutoLimit);
            this.panel9.Controls.Add(this.textBoxAppVersion);
            this.panel9.Controls.Add(this.label2);
            this.panel9.Controls.Add(this.label30);
            this.panel9.Controls.Add(this.comboBoxSampleRate);
            this.panel9.Controls.Add(this.label62);
            this.panel9.Controls.Add(this.label26);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 19);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(1003, 154);
            this.panel9.TabIndex = 69;
            // 
            // pictureBoxAddress
            // 
            this.pictureBoxAddress.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxAddress.BackgroundImage")));
            this.pictureBoxAddress.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxAddress.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxAddress.InitialImage")));
            this.pictureBoxAddress.Location = new System.Drawing.Point(473, 92);
            this.pictureBoxAddress.Name = "pictureBoxAddress";
            this.pictureBoxAddress.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxAddress.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxAddress.TabIndex = 268;
            this.pictureBoxAddress.TabStop = false;
            this.pictureBoxAddress.Click += new System.EventHandler(this.pictureBoxAddress_Click);
            // 
            // textBoxAddress
            // 
            this.textBoxAddress.Cursor = System.Windows.Forms.Cursors.Default;
            this.textBoxAddress.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxAddress.Location = new System.Drawing.Point(338, 92);
            this.textBoxAddress.Name = "textBoxAddress";
            this.textBoxAddress.ReadOnly = true;
            this.textBoxAddress.Size = new System.Drawing.Size(129, 23);
            this.textBoxAddress.TabIndex = 267;
            this.textBoxAddress.Text = "^EC:FE:7E:19:A3:75";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Arial", 10F);
            this.label39.Location = new System.Drawing.Point(273, 95);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(64, 16);
            this.label39.TabIndex = 266;
            this.label39.Text = "Address:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(863, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(144, 151);
            this.pictureBox1.TabIndex = 265;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBoxCommunicationDelay
            // 
            this.pictureBoxCommunicationDelay.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxCommunicationDelay.BackgroundImage")));
            this.pictureBoxCommunicationDelay.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxCommunicationDelay.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxCommunicationDelay.InitialImage")));
            this.pictureBoxCommunicationDelay.Location = new System.Drawing.Point(473, 33);
            this.pictureBoxCommunicationDelay.Name = "pictureBoxCommunicationDelay";
            this.pictureBoxCommunicationDelay.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxCommunicationDelay.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxCommunicationDelay.TabIndex = 254;
            this.pictureBoxCommunicationDelay.TabStop = false;
            this.pictureBoxCommunicationDelay.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Arial", 10F);
            this.label41.Location = new System.Drawing.Point(418, 37);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(12, 16);
            this.label41.TabIndex = 253;
            this.label41.Text = ".";
            // 
            // textBoxCommunicationDelay
            // 
            this.textBoxCommunicationDelay.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxCommunicationDelay.Location = new System.Drawing.Point(368, 34);
            this.textBoxCommunicationDelay.Name = "textBoxCommunicationDelay";
            this.textBoxCommunicationDelay.ReadOnly = true;
            this.textBoxCommunicationDelay.Size = new System.Drawing.Size(42, 23);
            this.textBoxCommunicationDelay.TabIndex = 251;
            this.textBoxCommunicationDelay.Text = "100";
            // 
            // labelCommunicationDelay
            // 
            this.labelCommunicationDelay.AutoSize = true;
            this.labelCommunicationDelay.Font = new System.Drawing.Font("Arial", 10F);
            this.labelCommunicationDelay.Location = new System.Drawing.Point(273, 37);
            this.labelCommunicationDelay.Name = "labelCommunicationDelay";
            this.labelCommunicationDelay.Size = new System.Drawing.Size(94, 16);
            this.labelCommunicationDelay.TabIndex = 252;
            this.labelCommunicationDelay.Text = "Comm. delay:";
            this.labelCommunicationDelay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Arial", 10F);
            this.label35.Location = new System.Drawing.Point(6, 125);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(84, 16);
            this.label35.TabIndex = 247;
            this.label35.Text = "Cable Type:";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comboBoxSource
            // 
            this.comboBoxSource.Cursor = System.Windows.Forms.Cursors.Default;
            this.comboBoxSource.Font = new System.Drawing.Font("Arial", 10F);
            this.comboBoxSource.Location = new System.Drawing.Point(368, 63);
            this.comboBoxSource.Name = "comboBoxSource";
            this.comboBoxSource.ReadOnly = true;
            this.comboBoxSource.Size = new System.Drawing.Size(99, 23);
            this.comboBoxSource.TabIndex = 245;
            this.comboBoxSource.Text = "^WinAndr";
            // 
            // textBoxCable
            // 
            this.textBoxCable.Cursor = System.Windows.Forms.Cursors.Default;
            this.textBoxCable.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxCable.Location = new System.Drawing.Point(103, 122);
            this.textBoxCable.Name = "textBoxCable";
            this.textBoxCable.ReadOnly = true;
            this.textBoxCable.Size = new System.Drawing.Size(124, 23);
            this.textBoxCable.TabIndex = 242;
            this.textBoxCable.Text = "8";
            // 
            // textBoxLeadOff
            // 
            this.textBoxLeadOff.Cursor = System.Windows.Forms.Cursors.Default;
            this.textBoxLeadOff.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxLeadOff.Location = new System.Drawing.Point(368, 122);
            this.textBoxLeadOff.Name = "textBoxLeadOff";
            this.textBoxLeadOff.ReadOnly = true;
            this.textBoxLeadOff.Size = new System.Drawing.Size(99, 23);
            this.textBoxLeadOff.TabIndex = 241;
            this.textBoxLeadOff.Text = "1 2 3 4 5 6 7 8 9 0";
            // 
            // pictureBoxLeadOff
            // 
            this.pictureBoxLeadOff.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxLeadOff.BackgroundImage")));
            this.pictureBoxLeadOff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxLeadOff.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxLeadOff.InitialImage")));
            this.pictureBoxLeadOff.Location = new System.Drawing.Point(473, 121);
            this.pictureBoxLeadOff.Name = "pictureBoxLeadOff";
            this.pictureBoxLeadOff.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxLeadOff.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxLeadOff.TabIndex = 239;
            this.pictureBoxLeadOff.TabStop = false;
            this.pictureBoxLeadOff.Click += new System.EventHandler(this.pictureBoxLeadOff_Click);
            // 
            // pictureBoxCable
            // 
            this.pictureBoxCable.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxCable.BackgroundImage")));
            this.pictureBoxCable.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxCable.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxCable.InitialImage")));
            this.pictureBoxCable.Location = new System.Drawing.Point(233, 121);
            this.pictureBoxCable.Name = "pictureBoxCable";
            this.pictureBoxCable.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxCable.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxCable.TabIndex = 237;
            this.pictureBoxCable.TabStop = false;
            this.pictureBoxCable.Click += new System.EventHandler(this.pictureBoxCable_Click);
            // 
            // pictureBoxSource
            // 
            this.pictureBoxSource.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxSource.BackgroundImage")));
            this.pictureBoxSource.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxSource.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxSource.InitialImage")));
            this.pictureBoxSource.Location = new System.Drawing.Point(473, 62);
            this.pictureBoxSource.Name = "pictureBoxSource";
            this.pictureBoxSource.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxSource.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxSource.TabIndex = 236;
            this.pictureBoxSource.TabStop = false;
            this.pictureBoxSource.Click += new System.EventHandler(this.pictureBoxSource_Click);
            // 
            // pictureBoxBatLevel
            // 
            this.pictureBoxBatLevel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxBatLevel.BackgroundImage")));
            this.pictureBoxBatLevel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxBatLevel.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxBatLevel.InitialImage")));
            this.pictureBoxBatLevel.Location = new System.Drawing.Point(473, 4);
            this.pictureBoxBatLevel.Name = "pictureBoxBatLevel";
            this.pictureBoxBatLevel.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxBatLevel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxBatLevel.TabIndex = 234;
            this.pictureBoxBatLevel.TabStop = false;
            this.pictureBoxBatLevel.Click += new System.EventHandler(this.pictureBoxBatLevel_Click);
            // 
            // pictureBoxAppVersion
            // 
            this.pictureBoxAppVersion.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxAppVersion.BackgroundImage")));
            this.pictureBoxAppVersion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxAppVersion.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxAppVersion.InitialImage")));
            this.pictureBoxAppVersion.Location = new System.Drawing.Point(233, 91);
            this.pictureBoxAppVersion.Name = "pictureBoxAppVersion";
            this.pictureBoxAppVersion.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxAppVersion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxAppVersion.TabIndex = 233;
            this.pictureBoxAppVersion.TabStop = false;
            this.pictureBoxAppVersion.Click += new System.EventHandler(this.pictureBoxAppVersion_Click);
            // 
            // pictureBoxFirmware
            // 
            this.pictureBoxFirmware.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxFirmware.BackgroundImage")));
            this.pictureBoxFirmware.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxFirmware.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxFirmware.InitialImage")));
            this.pictureBoxFirmware.Location = new System.Drawing.Point(233, 62);
            this.pictureBoxFirmware.Name = "pictureBoxFirmware";
            this.pictureBoxFirmware.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxFirmware.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxFirmware.TabIndex = 232;
            this.pictureBoxFirmware.TabStop = false;
            this.pictureBoxFirmware.Click += new System.EventHandler(this.pictureBoxFirmware_Click);
            // 
            // pictureBoxModel
            // 
            this.pictureBoxModel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxModel.BackgroundImage")));
            this.pictureBoxModel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxModel.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxModel.InitialImage")));
            this.pictureBoxModel.Location = new System.Drawing.Point(233, 33);
            this.pictureBoxModel.Name = "pictureBoxModel";
            this.pictureBoxModel.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxModel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxModel.TabIndex = 231;
            this.pictureBoxModel.TabStop = false;
            this.pictureBoxModel.Click += new System.EventHandler(this.pictureBoxModel_Click);
            // 
            // pictureBoxDevice
            // 
            this.pictureBoxDevice.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxDevice.BackgroundImage")));
            this.pictureBoxDevice.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxDevice.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxDevice.InitialImage")));
            this.pictureBoxDevice.Location = new System.Drawing.Point(233, 4);
            this.pictureBoxDevice.Name = "pictureBoxDevice";
            this.pictureBoxDevice.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxDevice.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxDevice.TabIndex = 230;
            this.pictureBoxDevice.TabStop = false;
            this.pictureBoxDevice.Click += new System.EventHandler(this.pictureBoxDevice_Click);
            // 
            // textBoxFirmware
            // 
            this.textBoxFirmware.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxFirmware.Location = new System.Drawing.Point(104, 63);
            this.textBoxFirmware.Name = "textBoxFirmware";
            this.textBoxFirmware.ReadOnly = true;
            this.textBoxFirmware.Size = new System.Drawing.Size(123, 23);
            this.textBoxFirmware.TabIndex = 199;
            this.textBoxFirmware.Text = "1.0.abcdefg";
            // 
            // checkBoxTTX
            // 
            this.checkBoxTTX.AutoSize = true;
            this.checkBoxTTX.Location = new System.Drawing.Point(679, 38);
            this.checkBoxTTX.Name = "checkBoxTTX";
            this.checkBoxTTX.Size = new System.Drawing.Size(15, 14);
            this.checkBoxTTX.TabIndex = 264;
            this.checkBoxTTX.UseVisualStyleBackColor = true;
            // 
            // pictureBoxManualLimit
            // 
            this.pictureBoxManualLimit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxManualLimit.BackgroundImage")));
            this.pictureBoxManualLimit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxManualLimit.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxManualLimit.InitialImage")));
            this.pictureBoxManualLimit.Location = new System.Drawing.Point(744, 121);
            this.pictureBoxManualLimit.Name = "pictureBoxManualLimit";
            this.pictureBoxManualLimit.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxManualLimit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxManualLimit.TabIndex = 261;
            this.pictureBoxManualLimit.TabStop = false;
            this.pictureBoxManualLimit.Click += new System.EventHandler(this.pictureBoxManualLimit_Click);
            // 
            // textBoxModel
            // 
            this.textBoxModel.Cursor = System.Windows.Forms.Cursors.Default;
            this.textBoxModel.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxModel.Location = new System.Drawing.Point(104, 34);
            this.textBoxModel.Name = "textBoxModel";
            this.textBoxModel.ReadOnly = true;
            this.textBoxModel.Size = new System.Drawing.Size(123, 23);
            this.textBoxModel.TabIndex = 198;
            this.textBoxModel.Text = "SIR-RX93030-000";
            // 
            // pictureBoxAutoLimit
            // 
            this.pictureBoxAutoLimit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxAutoLimit.BackgroundImage")));
            this.pictureBoxAutoLimit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxAutoLimit.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxAutoLimit.InitialImage")));
            this.pictureBoxAutoLimit.Location = new System.Drawing.Point(744, 91);
            this.pictureBoxAutoLimit.Name = "pictureBoxAutoLimit";
            this.pictureBoxAutoLimit.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxAutoLimit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxAutoLimit.TabIndex = 260;
            this.pictureBoxAutoLimit.TabStop = false;
            this.pictureBoxAutoLimit.Click += new System.EventHandler(this.pictureBoxAutoLimit_Click);
            // 
            // textBoxDevice
            // 
            this.textBoxDevice.Cursor = System.Windows.Forms.Cursors.Default;
            this.textBoxDevice.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxDevice.Location = new System.Drawing.Point(104, 5);
            this.textBoxDevice.Name = "textBoxDevice";
            this.textBoxDevice.ReadOnly = true;
            this.textBoxDevice.Size = new System.Drawing.Size(123, 23);
            this.textBoxDevice.TabIndex = 197;
            this.textBoxDevice.Text = "123323131";
            // 
            // checkBoxUiLock
            // 
            this.checkBoxUiLock.AutoSize = true;
            this.checkBoxUiLock.Location = new System.Drawing.Point(679, 67);
            this.checkBoxUiLock.Name = "checkBoxUiLock";
            this.checkBoxUiLock.Size = new System.Drawing.Size(15, 14);
            this.checkBoxUiLock.TabIndex = 262;
            this.checkBoxUiLock.UseVisualStyleBackColor = true;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Arial", 10F);
            this.label21.Location = new System.Drawing.Point(272, 125);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(90, 16);
            this.label21.TabIndex = 193;
            this.label21.Text = "Lead off bits:";
            // 
            // label44
            // 
            this.label44.Font = new System.Drawing.Font("Arial", 10F);
            this.label44.ForeColor = System.Drawing.Color.Black;
            this.label44.Location = new System.Drawing.Point(6, 62);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(75, 25);
            this.label44.TabIndex = 191;
            this.label44.Text = "Firmware:";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBoxUiLock
            // 
            this.pictureBoxUiLock.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxUiLock.BackgroundImage")));
            this.pictureBoxUiLock.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxUiLock.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxUiLock.InitialImage")));
            this.pictureBoxUiLock.Location = new System.Drawing.Point(744, 62);
            this.pictureBoxUiLock.Name = "pictureBoxUiLock";
            this.pictureBoxUiLock.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxUiLock.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxUiLock.TabIndex = 238;
            this.pictureBoxUiLock.TabStop = false;
            this.pictureBoxUiLock.Click += new System.EventHandler(this.pictureBoxUiLock_Click);
            // 
            // label42
            // 
            this.label42.Font = new System.Drawing.Font("Arial", 10F);
            this.label42.ForeColor = System.Drawing.Color.Black;
            this.label42.Location = new System.Drawing.Point(6, 36);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(63, 19);
            this.label42.TabIndex = 189;
            this.label42.Text = "Model:";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label51
            // 
            this.label51.Font = new System.Drawing.Font("Arial", 10F);
            this.label51.ForeColor = System.Drawing.Color.Black;
            this.label51.Location = new System.Drawing.Point(6, 4);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(66, 25);
            this.label51.TabIndex = 185;
            this.label51.Text = "Device:";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBoxTTX
            // 
            this.pictureBoxTTX.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxTTX.BackgroundImage")));
            this.pictureBoxTTX.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxTTX.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxTTX.InitialImage")));
            this.pictureBoxTTX.Location = new System.Drawing.Point(744, 33);
            this.pictureBoxTTX.Name = "pictureBoxTTX";
            this.pictureBoxTTX.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxTTX.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxTTX.TabIndex = 259;
            this.pictureBoxTTX.TabStop = false;
            this.pictureBoxTTX.Click += new System.EventHandler(this.pictureBoxTTX_Click);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Arial", 10F);
            this.label25.Location = new System.Drawing.Point(273, 66);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(82, 16);
            this.label25.TabIndex = 193;
            this.label25.Text = "Update Src:";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBoxSampleRate
            // 
            this.pictureBoxSampleRate.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxSampleRate.BackgroundImage")));
            this.pictureBoxSampleRate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxSampleRate.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxSampleRate.InitialImage")));
            this.pictureBoxSampleRate.Location = new System.Drawing.Point(744, 4);
            this.pictureBoxSampleRate.Name = "pictureBoxSampleRate";
            this.pictureBoxSampleRate.Size = new System.Drawing.Size(24, 24);
            this.pictureBoxSampleRate.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxSampleRate.TabIndex = 258;
            this.pictureBoxSampleRate.TabStop = false;
            this.pictureBoxSampleRate.Click += new System.EventHandler(this.pictureBoxSampleRate_Click);
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Arial", 10F);
            this.label61.Location = new System.Drawing.Point(554, 125);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(125, 16);
            this.label61.TabIndex = 237;
            this.label61.Text = "Manual event limit:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 10F);
            this.label17.Location = new System.Drawing.Point(6, 125);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(45, 16);
            this.label17.TabIndex = 187;
            this.label17.Text = "Cable";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 10F);
            this.label11.Location = new System.Drawing.Point(415, 8);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(20, 16);
            this.label11.TabIndex = 183;
            this.label11.Text = "%";
            // 
            // textBoxBatLevel
            // 
            this.textBoxBatLevel.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxBatLevel.Location = new System.Drawing.Point(368, 5);
            this.textBoxBatLevel.Name = "textBoxBatLevel";
            this.textBoxBatLevel.ReadOnly = true;
            this.textBoxBatLevel.Size = new System.Drawing.Size(42, 23);
            this.textBoxBatLevel.TabIndex = 181;
            this.textBoxBatLevel.Text = "100";
            // 
            // textBoxManualLimit
            // 
            this.textBoxManualLimit.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxManualLimit.Location = new System.Drawing.Point(679, 122);
            this.textBoxManualLimit.Name = "textBoxManualLimit";
            this.textBoxManualLimit.Size = new System.Drawing.Size(58, 23);
            this.textBoxManualLimit.TabIndex = 238;
            this.textBoxManualLimit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 10F);
            this.label8.Location = new System.Drawing.Point(273, 8);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(57, 16);
            this.label8.TabIndex = 182;
            this.label8.Text = "Battery:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Arial", 10F);
            this.label60.Location = new System.Drawing.Point(554, 95);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(108, 16);
            this.label60.TabIndex = 235;
            this.label60.Text = "Auto event limit:";
            // 
            // textBoxAutoLimit
            // 
            this.textBoxAutoLimit.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxAutoLimit.Location = new System.Drawing.Point(679, 92);
            this.textBoxAutoLimit.Name = "textBoxAutoLimit";
            this.textBoxAutoLimit.Size = new System.Drawing.Size(58, 23);
            this.textBoxAutoLimit.TabIndex = 236;
            this.textBoxAutoLimit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxAppVersion
            // 
            this.textBoxAppVersion.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxAppVersion.Location = new System.Drawing.Point(104, 92);
            this.textBoxAppVersion.Name = "textBoxAppVersion";
            this.textBoxAppVersion.ReadOnly = true;
            this.textBoxAppVersion.Size = new System.Drawing.Size(123, 23);
            this.textBoxAppVersion.TabIndex = 180;
            this.textBoxAppVersion.Text = "1.0.abcdefg";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 10F);
            this.label2.Location = new System.Drawing.Point(6, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 16);
            this.label2.TabIndex = 180;
            this.label2.Text = "App Version:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Arial", 10F);
            this.label30.Location = new System.Drawing.Point(554, 8);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(88, 16);
            this.label30.TabIndex = 125;
            this.label30.Text = "Sample rate:";
            // 
            // comboBoxSampleRate
            // 
            this.comboBoxSampleRate.Font = new System.Drawing.Font("Arial", 10F);
            this.comboBoxSampleRate.FormattingEnabled = true;
            this.comboBoxSampleRate.Items.AddRange(new object[] {
            "128",
            "256"});
            this.comboBoxSampleRate.Location = new System.Drawing.Point(679, 4);
            this.comboBoxSampleRate.Name = "comboBoxSampleRate";
            this.comboBoxSampleRate.Size = new System.Drawing.Size(58, 24);
            this.comboBoxSampleRate.TabIndex = 126;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Arial", 10F);
            this.label62.Location = new System.Drawing.Point(554, 37);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(58, 16);
            this.label62.TabIndex = 239;
            this.label62.Text = "TTX on:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Arial", 10F);
            this.label26.Location = new System.Drawing.Point(554, 66);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(73, 16);
            this.label26.TabIndex = 195;
            this.label26.Text = "UI lock on:";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.MediumPurple;
            this.panel8.Controls.Add(this.label28);
            this.panel8.Controls.Add(this.labelDeviceSnr);
            this.panel8.Controls.Add(this.label1);
            this.panel8.Controls.Add(this.labelMarkAll);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(1003, 19);
            this.panel8.TabIndex = 68;
            // 
            // label28
            // 
            this.label28.Dock = System.Windows.Forms.DockStyle.Right;
            this.label28.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label28.ForeColor = System.Drawing.Color.White;
            this.label28.Location = new System.Drawing.Point(557, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(313, 19);
            this.label28.TabIndex = 190;
            this.label28.Text = "Technical settings";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelDeviceSnr
            // 
            this.labelDeviceSnr.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelDeviceSnr.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelDeviceSnr.ForeColor = System.Drawing.Color.White;
            this.labelDeviceSnr.Location = new System.Drawing.Point(100, 0);
            this.labelDeviceSnr.Name = "labelDeviceSnr";
            this.labelDeviceSnr.Size = new System.Drawing.Size(141, 19);
            this.labelDeviceSnr.TabIndex = 188;
            this.labelDeviceSnr.Text = "1234567890";
            this.labelDeviceSnr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelDeviceSnr.DoubleClick += new System.EventHandler(this.labelDeviceSnr_DoubleClick);
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 19);
            this.label1.TabIndex = 180;
            this.label1.Text = "Device";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelMarkAll
            // 
            this.labelMarkAll.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelMarkAll.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelMarkAll.ForeColor = System.Drawing.Color.White;
            this.labelMarkAll.Location = new System.Drawing.Point(870, 0);
            this.labelMarkAll.Name = "labelMarkAll";
            this.labelMarkAll.Size = new System.Drawing.Size(133, 19);
            this.labelMarkAll.TabIndex = 191;
            this.labelMarkAll.Text = "Use All";
            this.labelMarkAll.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelMarkAll.Click += new System.EventHandler(this.labelMarkAll_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.SlateBlue;
            this.panel3.Controls.Add(this.labelSaveDefaults);
            this.panel3.Controls.Add(this.label19);
            this.panel3.Controls.Add(this.labelLoadDefaults);
            this.panel3.Controls.Add(this.labelLoadSettings);
            this.panel3.Controls.Add(this.labelSaveAs);
            this.panel3.Controls.Add(this.labelSaveToServer);
            this.panel3.Controls.Add(this.labelClose);
            this.panel3.Controls.Add(this.labelLoadCurrent);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 589);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1003, 43);
            this.panel3.TabIndex = 2;
            // 
            // labelSaveDefaults
            // 
            this.labelSaveDefaults.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSaveDefaults.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelSaveDefaults.ForeColor = System.Drawing.Color.White;
            this.labelSaveDefaults.Location = new System.Drawing.Point(389, 0);
            this.labelSaveDefaults.Name = "labelSaveDefaults";
            this.labelSaveDefaults.Size = new System.Drawing.Size(130, 43);
            this.labelSaveDefaults.TabIndex = 192;
            this.labelSaveDefaults.Text = "Save \r\nDefaults";
            this.labelSaveDefaults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelSaveDefaults.Click += new System.EventHandler(this.labelSaveDefaults_Click);
            // 
            // label19
            // 
            this.label19.Dock = System.Windows.Forms.DockStyle.Right;
            this.label19.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label19.ForeColor = System.Drawing.Color.White;
            this.label19.Location = new System.Drawing.Point(519, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(128, 43);
            this.label19.TabIndex = 193;
            this.label19.Text = "Save\r\nState";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label19.Click += new System.EventHandler(this.label19_Click);
            // 
            // labelLoadDefaults
            // 
            this.labelLoadDefaults.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelLoadDefaults.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelLoadDefaults.ForeColor = System.Drawing.Color.White;
            this.labelLoadDefaults.Location = new System.Drawing.Point(231, 0);
            this.labelLoadDefaults.Name = "labelLoadDefaults";
            this.labelLoadDefaults.Size = new System.Drawing.Size(114, 43);
            this.labelLoadDefaults.TabIndex = 191;
            this.labelLoadDefaults.Text = "Load \r\nDefaults";
            this.labelLoadDefaults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelLoadDefaults.Click += new System.EventHandler(this.labelLoadDefaults_Click);
            // 
            // labelLoadSettings
            // 
            this.labelLoadSettings.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelLoadSettings.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelLoadSettings.ForeColor = System.Drawing.Color.White;
            this.labelLoadSettings.Location = new System.Drawing.Point(119, 0);
            this.labelLoadSettings.Name = "labelLoadSettings";
            this.labelLoadSettings.Size = new System.Drawing.Size(112, 43);
            this.labelLoadSettings.TabIndex = 186;
            this.labelLoadSettings.Text = "Load \r\nsettings";
            this.labelLoadSettings.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelLoadSettings.Click += new System.EventHandler(this.labelLoadSettings_Click);
            // 
            // labelSaveAs
            // 
            this.labelSaveAs.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSaveAs.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelSaveAs.ForeColor = System.Drawing.Color.White;
            this.labelSaveAs.Location = new System.Drawing.Point(647, 0);
            this.labelSaveAs.Name = "labelSaveAs";
            this.labelSaveAs.Size = new System.Drawing.Size(128, 43);
            this.labelSaveAs.TabIndex = 184;
            this.labelSaveAs.Text = "Save to \r\nFile";
            this.labelSaveAs.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelSaveAs.Click += new System.EventHandler(this.labelSaveAs_Click);
            // 
            // labelSaveToServer
            // 
            this.labelSaveToServer.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSaveToServer.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelSaveToServer.ForeColor = System.Drawing.Color.White;
            this.labelSaveToServer.Location = new System.Drawing.Point(775, 0);
            this.labelSaveToServer.Name = "labelSaveToServer";
            this.labelSaveToServer.Size = new System.Drawing.Size(161, 43);
            this.labelSaveToServer.TabIndex = 2;
            this.labelSaveToServer.Text = "Save to \r\nServer";
            this.labelSaveToServer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelSaveToServer.Click += new System.EventHandler(this.labelSaveToServer_Click);
            // 
            // labelClose
            // 
            this.labelClose.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelClose.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelClose.ForeColor = System.Drawing.Color.White;
            this.labelClose.Location = new System.Drawing.Point(936, 0);
            this.labelClose.Name = "labelClose";
            this.labelClose.Size = new System.Drawing.Size(67, 43);
            this.labelClose.TabIndex = 183;
            this.labelClose.Text = "Close";
            this.labelClose.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelClose.Click += new System.EventHandler(this.labelClose_Click);
            // 
            // labelLoadCurrent
            // 
            this.labelLoadCurrent.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelLoadCurrent.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelLoadCurrent.ForeColor = System.Drawing.Color.White;
            this.labelLoadCurrent.Location = new System.Drawing.Point(0, 0);
            this.labelLoadCurrent.Name = "labelLoadCurrent";
            this.labelLoadCurrent.Size = new System.Drawing.Size(119, 43);
            this.labelLoadCurrent.TabIndex = 188;
            this.labelLoadCurrent.Text = "Reload \r\nsettings";
            this.labelLoadCurrent.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelLoadCurrent.Click += new System.EventHandler(this.labelLoadCurrent_Click);
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.Filter = "Sirona Settings|*.sirSet";
            this.saveFileDialog.Title = "Save Sirona Settings  file";
            // 
            // openFileDialogSirS
            // 
            this.openFileDialogSirS.FileName = "default.tzs";
            this.openFileDialogSirS.Filter = "Sirona Settings|*.sirSet";
            this.openFileDialogSirS.Title = "Select Sirona Settings  file";
            // 
            // openFileDialogTxt
            // 
            this.openFileDialogTxt.FileName = "input.txt";
            this.openFileDialogTxt.Filter = "Sirona Settings|*.sirSet";
            this.openFileDialogTxt.Title = "Select Sirona Settings  file";
            // 
            // timerRefresh
            // 
            this.timerRefresh.Enabled = true;
            this.timerRefresh.Interval = 15000;
            this.timerRefresh.Tick += new System.EventHandler(this.timerRefresh_Tick);
            // 
            // panelAutoCollect
            // 
            this.panelAutoCollect.Controls.Add(this.panel7);
            this.panelAutoCollect.Controls.Add(this.panel6);
            this.panelAutoCollect.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelAutoCollect.Location = new System.Drawing.Point(0, 631);
            this.panelAutoCollect.Name = "panelAutoCollect";
            this.panelAutoCollect.Size = new System.Drawing.Size(1003, 81);
            this.panelAutoCollect.TabIndex = 3;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.panel18);
            this.panel7.Controls.Add(this.buttonAcRefreshInfo);
            this.panel7.Controls.Add(this.panel17);
            this.panel7.Controls.Add(this.buttonAcTest);
            this.panel7.Controls.Add(this.checkBoxAutoRunTest);
            this.panel7.Controls.Add(this.checkBoxAcEvent);
            this.panel7.Controls.Add(this.checkBoxCollectEnabled);
            this.panel7.Controls.Add(this.label79);
            this.panel7.Controls.Add(this.buttonModeSave);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 30);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1003, 46);
            this.panel7.TabIndex = 2;
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.panel20);
            this.panel18.Controls.Add(this.panel19);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel18.Location = new System.Drawing.Point(569, 0);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(434, 46);
            this.panel18.TabIndex = 25;
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.labelAcLastScan);
            this.panel20.Controls.Add(this.label91);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel20.Location = new System.Drawing.Point(0, 19);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(434, 27);
            this.panel20.TabIndex = 24;
            // 
            // labelAcLastScan
            // 
            this.labelAcLastScan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelAcLastScan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelAcLastScan.Location = new System.Drawing.Point(58, 0);
            this.labelAcLastScan.Name = "labelAcLastScan";
            this.labelAcLastScan.Size = new System.Drawing.Size(376, 27);
            this.labelAcLastScan.TabIndex = 10;
            this.labelAcLastScan.Text = "^YYYYMMDDHHmmss xx missing scp\r\nhdfgf";
            this.labelAcLastScan.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label91
            // 
            this.label91.Dock = System.Windows.Forms.DockStyle.Left;
            this.label91.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label91.Location = new System.Drawing.Point(0, 0);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(58, 27);
            this.label91.TabIndex = 11;
            this.label91.Text = "scan:";
            this.label91.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.labelAcSavedInfo);
            this.panel19.Controls.Add(this.label87);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel19.Location = new System.Drawing.Point(0, 0);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(434, 19);
            this.panel19.TabIndex = 23;
            // 
            // labelAcSavedInfo
            // 
            this.labelAcSavedInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelAcSavedInfo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelAcSavedInfo.Location = new System.Drawing.Point(58, 0);
            this.labelAcSavedInfo.Name = "labelAcSavedInfo";
            this.labelAcSavedInfo.Size = new System.Drawing.Size(376, 19);
            this.labelAcSavedInfo.TabIndex = 10;
            this.labelAcSavedInfo.Text = "^YYYYMMDDHHmmss";
            this.labelAcSavedInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label87
            // 
            this.label87.Dock = System.Windows.Forms.DockStyle.Left;
            this.label87.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label87.Location = new System.Drawing.Point(0, 0);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(58, 19);
            this.label87.TabIndex = 11;
            this.label87.Text = "Saved";
            this.label87.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // buttonAcRefreshInfo
            // 
            this.buttonAcRefreshInfo.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonAcRefreshInfo.Font = new System.Drawing.Font("Webdings", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.buttonAcRefreshInfo.Location = new System.Drawing.Point(531, 0);
            this.buttonAcRefreshInfo.Name = "buttonAcRefreshInfo";
            this.buttonAcRefreshInfo.Size = new System.Drawing.Size(38, 46);
            this.buttonAcRefreshInfo.TabIndex = 28;
            this.buttonAcRefreshInfo.Text = "q";
            this.buttonAcRefreshInfo.UseVisualStyleBackColor = true;
            this.buttonAcRefreshInfo.Click += new System.EventHandler(this.buttonAcRefreshInfo_Click);
            // 
            // panel17
            // 
            this.panel17.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel17.Location = new System.Drawing.Point(520, 0);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(11, 46);
            this.panel17.TabIndex = 27;
            // 
            // buttonAcTest
            // 
            this.buttonAcTest.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonAcTest.Location = new System.Drawing.Point(484, 0);
            this.buttonAcTest.Name = "buttonAcTest";
            this.buttonAcTest.Size = new System.Drawing.Size(36, 46);
            this.buttonAcTest.TabIndex = 26;
            this.buttonAcTest.Text = "Test";
            this.buttonAcTest.UseVisualStyleBackColor = true;
            this.buttonAcTest.Click += new System.EventHandler(this.buttonAcTest_Click);
            // 
            // checkBoxAutoRunTest
            // 
            this.checkBoxAutoRunTest.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxAutoRunTest.Location = new System.Drawing.Point(417, 0);
            this.checkBoxAutoRunTest.Name = "checkBoxAutoRunTest";
            this.checkBoxAutoRunTest.Size = new System.Drawing.Size(67, 46);
            this.checkBoxAutoRunTest.TabIndex = 30;
            this.checkBoxAutoRunTest.Text = "Auto Run Test";
            this.checkBoxAutoRunTest.UseVisualStyleBackColor = true;
            // 
            // checkBoxAcEvent
            // 
            this.checkBoxAcEvent.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxAcEvent.Location = new System.Drawing.Point(319, 0);
            this.checkBoxAcEvent.Name = "checkBoxAcEvent";
            this.checkBoxAcEvent.Size = new System.Drawing.Size(98, 46);
            this.checkBoxAcEvent.TabIndex = 29;
            this.checkBoxAcEvent.Text = "View Event";
            this.checkBoxAcEvent.UseVisualStyleBackColor = true;
            // 
            // checkBoxCollectEnabled
            // 
            this.checkBoxCollectEnabled.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxCollectEnabled.Location = new System.Drawing.Point(215, 0);
            this.checkBoxCollectEnabled.Name = "checkBoxCollectEnabled";
            this.checkBoxCollectEnabled.Size = new System.Drawing.Size(104, 46);
            this.checkBoxCollectEnabled.TabIndex = 21;
            this.checkBoxCollectEnabled.Text = "Auto Collect \r\nMissing \r\nECG files";
            this.checkBoxCollectEnabled.UseVisualStyleBackColor = true;
            // 
            // label79
            // 
            this.label79.Dock = System.Windows.Forms.DockStyle.Left;
            this.label79.Location = new System.Drawing.Point(200, 0);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(15, 46);
            this.label79.TabIndex = 20;
            this.label79.Text = " :";
            this.label79.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // buttonModeSave
            // 
            this.buttonModeSave.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonModeSave.Location = new System.Drawing.Point(0, 0);
            this.buttonModeSave.Name = "buttonModeSave";
            this.buttonModeSave.Size = new System.Drawing.Size(200, 46);
            this.buttonModeSave.TabIndex = 19;
            this.buttonModeSave.Text = "Save Auto collect mode";
            this.buttonModeSave.UseVisualStyleBackColor = true;
            this.buttonModeSave.Click += new System.EventHandler(this.buttonModeSave_Click);
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.labelStudyActive);
            this.panel6.Controls.Add(this.label81);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1003, 30);
            this.panel6.TabIndex = 1;
            // 
            // labelStudyActive
            // 
            this.labelStudyActive.AutoSize = true;
            this.labelStudyActive.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelStudyActive.Font = new System.Drawing.Font("Arial", 10F);
            this.labelStudyActive.Location = new System.Drawing.Point(703, 0);
            this.labelStudyActive.Name = "labelStudyActive";
            this.labelStudyActive.Size = new System.Drawing.Size(300, 16);
            this.labelStudyActive.TabIndex = 196;
            this.labelStudyActive.Text = "^Study 999999 Active / Ended  yyyymmmdd hh";
            this.labelStudyActive.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelStudyActive.DoubleClick += new System.EventHandler(this.labelStudyActive_DoubleClick);
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Dock = System.Windows.Forms.DockStyle.Left;
            this.label81.Font = new System.Drawing.Font("Arial", 8F);
            this.label81.Location = new System.Drawing.Point(0, 0);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(604, 28);
            this.label81.TabIndex = 195;
            this.label81.Text = "Auto collect works when a active study is present. \r\n(Simple version: one request" +
    " is send  after proc length exceeds next block. No check on missing or out of se" +
    "quence blocks)";
            this.label81.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // SironaDevSet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1003, 712);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panelAutoCollect);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "SironaDevSet";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sirona device configuration settings";
            this.panel2.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxEqual)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNotEqual)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAudio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecordLength)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPostTrigger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPreTrigger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPauseOnset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAfib)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxTachyOnset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBradyOnset)).EndInit();
            this.panel12.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxServerUrl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxProcState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxProcType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxProcLength)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStartTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPhysisian)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDOB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMiddle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFirst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPatientID)).EndInit();
            this.panel10.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCommunicationDelay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLeadOff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBatLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAppVersion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFirmware)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxModel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDevice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxManualLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAutoLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxUiLock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxTTX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSampleRate)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panelAutoCollect.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel18.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.panel19.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label labelSaveToServer;
        private System.Windows.Forms.TextBox textBoxPatientID;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label labelClose;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label labelSaveAs;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialogSirS;
        private System.Windows.Forms.OpenFileDialog openFileDialogTxt;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox textBoxComment;
        private System.Windows.Forms.TextBox textBoxDOB;
        private System.Windows.Forms.TextBox textBoxPhysisian;
        private System.Windows.Forms.TextBox textBoxLast;
        private System.Windows.Forms.TextBox textBoxMiddle;
        private System.Windows.Forms.TextBox textBoxFirst;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxBradyOnset;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBoxTachyOnset;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox textBoxPauseOnset;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxBatLevel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxAppVersion;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox textBoxProcLength;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.TextBox textBoxManualLimit;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.TextBox textBoxAutoLimit;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox textBoxFirmware;
        private System.Windows.Forms.TextBox textBoxModel;
        private System.Windows.Forms.TextBox textBoxDevice;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Button buttonRefresh;
        private System.Windows.Forms.Label labelActionFileInfo;
        private System.Windows.Forms.Label labelSettingFileInfo;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxRecordLength;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.ComboBox comboBoxSampleRate;
        private System.Windows.Forms.ComboBox comboBoxPostTrigger;
        private System.Windows.Forms.ComboBox comboBoxPreTrigger;
        private System.Windows.Forms.PictureBox pictureBoxProcState;
        private System.Windows.Forms.PictureBox pictureBoxProcType;
        private System.Windows.Forms.PictureBox pictureBoxProcLength;
        private System.Windows.Forms.PictureBox pictureBoxStartTime;
        private System.Windows.Forms.PictureBox pictureBoxComment;
        private System.Windows.Forms.PictureBox pictureBoxPhysisian;
        private System.Windows.Forms.PictureBox pictureBoxDOB;
        private System.Windows.Forms.PictureBox pictureBoxLast;
        private System.Windows.Forms.PictureBox pictureBoxMiddle;
        private System.Windows.Forms.PictureBox pictureBoxFirst;
        private System.Windows.Forms.PictureBox pictureBoxPatientID;
        private System.Windows.Forms.PictureBox pictureBoxCable;
        private System.Windows.Forms.PictureBox pictureBoxSource;
        private System.Windows.Forms.PictureBox pictureBoxAudio;
        private System.Windows.Forms.PictureBox pictureBoxBatLevel;
        private System.Windows.Forms.PictureBox pictureBoxAppVersion;
        private System.Windows.Forms.PictureBox pictureBoxFirmware;
        private System.Windows.Forms.PictureBox pictureBoxModel;
        private System.Windows.Forms.PictureBox pictureBoxDevice;
        private System.Windows.Forms.PictureBox pictureBoxManualLimit;
        private System.Windows.Forms.PictureBox pictureBoxAutoLimit;
        private System.Windows.Forms.PictureBox pictureBoxTTX;
        private System.Windows.Forms.PictureBox pictureBoxSampleRate;
        private System.Windows.Forms.PictureBox pictureBoxRecordLength;
        private System.Windows.Forms.PictureBox pictureBoxPostTrigger;
        private System.Windows.Forms.PictureBox pictureBoxPreTrigger;
        private System.Windows.Forms.PictureBox pictureBoxPauseOnset;
        private System.Windows.Forms.PictureBox pictureBoxAfib;
        private System.Windows.Forms.PictureBox pictureBoxTachyOnset;
        private System.Windows.Forms.PictureBox pictureBoxBradyOnset;
        private System.Windows.Forms.PictureBox pictureBoxLeadOff;
        private System.Windows.Forms.CheckBox checkBoxUiLock;
        private System.Windows.Forms.PictureBox pictureBoxUiLock;
        private System.Windows.Forms.TextBox textBoxCable;
        private System.Windows.Forms.TextBox textBoxLeadOff;
        private System.Windows.Forms.CheckBox checkBoxAudio;
        private System.Windows.Forms.CheckBox checkBoxAfib;
        private System.Windows.Forms.TextBox textBoxProcState;
        private System.Windows.Forms.TextBox textBoxProcType;
        private System.Windows.Forms.CheckBox checkBoxTTX;
        private System.Windows.Forms.ComboBox comboBoxStartType;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBoxCommand;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label labelStartAtTime;
        private System.Windows.Forms.TextBox textBoxStartLength;
        private System.Windows.Forms.CheckBox checkBoxECG;
        private System.Windows.Forms.CheckBox checkBoxAnnotation;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBoxStartTime;
        private System.Windows.Forms.PictureBox pictureBoxEqual;
        private System.Windows.Forms.PictureBox pictureBoxNotEqual;
        private System.Windows.Forms.TextBox comboBoxSource;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label labelResultError;
        private System.Windows.Forms.Label labelDeviceSnr;
        private System.Windows.Forms.Label labelUseDevPat;
        private System.Windows.Forms.Label labelMarkState;
        private System.Windows.Forms.Label labelClearPatient;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.DateTimePicker dateTimePickerStart;
        private System.Windows.Forms.PictureBox pictureBoxServerUrl;
        private System.Windows.Forms.TextBox textBoxServerUrl;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.PictureBox pictureBoxCommunicationDelay;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox textBoxCommunicationDelay;
        private System.Windows.Forms.Label labelCommunicationDelay;
        private System.Windows.Forms.Label labelLoadCurrent;
        private System.Windows.Forms.Label labelLoadSettings;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label labelUseDevSet;
        private System.Windows.Forms.Label labelLoadDefaults;
        private System.Windows.Forms.Label labelSaveDefaults;
        private System.Windows.Forms.Label labelEndQuiry;
        private System.Windows.Forms.Label labelProcEnd;
        private System.Windows.Forms.Label labelMarkAll;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Timer timerRefresh;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button buttonClearQueue;
        private System.Windows.Forms.CheckBox checkBoxAutoCollect;
        private System.Windows.Forms.Panel panelAutoCollect;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label labelStudyActive;
        private System.Windows.Forms.Button buttonModeSave;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.CheckBox checkBoxCollectEnabled;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Label labelAcLastScan;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Label labelAcSavedInfo;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label labelSetting;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Button buttonAcRefreshInfo;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Button buttonAcTest;
        private System.Windows.Forms.CheckBox checkBoxAcEvent;
        private System.Windows.Forms.CheckBox checkBoxAutoRunTest;
        private System.Windows.Forms.TextBox labelResult;
        private System.Windows.Forms.PictureBox pictureBoxAddress;
        private System.Windows.Forms.TextBox textBoxAddress;
        private System.Windows.Forms.Label label39;
    }
}