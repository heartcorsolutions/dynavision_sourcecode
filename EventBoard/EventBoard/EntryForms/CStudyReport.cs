﻿using EventboardEntryForms;
using Program_Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventBoard.EntryForms
{
    public enum DReportSection
    {
        Daily = 1,
        Weekly,
        EndOfStudy,
        MCT,
        Hour,
        Period,
        NrEnumValues
    }
    public enum DStudyReportVars
    {
        // primary keys
        StudyIX,
        ReportNr,
        // variables
        ReportTableSet,
        ReportStripSet,
        ReportSection,
        Summary,
        Conclusion,
        ReportUTC,
        ReportedByIX,
        NrSqlVars       // keep as last
    };

    public class CStudyReport: CSqlDataTableRow
    {
        public UInt32 _mStudyIX;
        public UInt16 _mReportNr;

        public CStringSet _mReportTableSet;    // show in table: analysis index numbers [- sample flags]
        public CStringSet _mReportStripSet;    // show in strips: analysis index numbers [- sample flags]

        public UInt16 _mReportSectionMask;
        public CEncryptedString _mSummary;
        public CEncryptedString _mConclusion;

        public DateTime _mReportUTC;
        public UInt32 _mReportedByIX;

        public CStudyReport(CSqlDBaseConnection ASqlConnection) : base(ASqlConnection, "d_studyReport")
        {
            try
            {
                UInt64 maskPrimary = CSqlDataTableRow.sGetMaskRange((UInt16)DStudyReportVars.StudyIX, (UInt16)DStudyReportVars.ReportNr);
                UInt64 maskValid = CSqlDataTableRow.sGetMaskRange((UInt16)0, (UInt16)DStudyReportVars.NrSqlVars - 1);// mGetValidMask(false);

                mInitTableMasks(maskPrimary, maskValid);  // set primairyInsertFlags
                                                         
                _mReportTableSet = new CStringSet();
                _mReportStripSet = new CStringSet();
                _mSummary = new CEncryptedString( "ReportSummar", DEncryptLevel.L2_HashCode );
                _mConclusion = new CEncryptedString("ReportConclusion", DEncryptLevel.L2_HashCode); 

                mClear();
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Init ", ex);
            }

        }

        public override CSqlDataTableRow mCreateNewRow()
        {
            return new CStudyReport(mGetSqlConnection());
        }

        public override bool mbCopyTo(CSqlDataTableRow ATo) // derived  public override void mbCopyTo() must call base.mbCopyTo(ATo)
        {
            bool bOk = false;

            if (ATo != null)
            {
                CStudyReport to = ATo as CStudyReport;

                if (to != null)
                {
                    bOk = base.mbCopyTo(ATo);

                    to._mStudyIX = _mStudyIX;
                    to._mReportNr = _mReportNr;

                    to._mReportTableSet.mSetSet( _mReportTableSet);    // show in table: analysis index numbers [- sample flags]
                    to._mReportStripSet.mSetSet( _mReportStripSet);    // show in strips: analysis index numbers [- sample flags]

                    to._mReportSectionMask = 0;
                    if (_mSummary != null) to._mSummary = _mSummary.mCreateCopy();
                    if (_mConclusion != null) to._mConclusion = _mConclusion.mCreateCopy();

                    to._mReportUTC = _mReportUTC;
                    to._mReportedByIX = _mReportedByIX;

                  }
            }
            return bOk;
        }
        public override void mClear()
        {
            base.mClear();
            _mStudyIX = 0;
            _mReportNr = 0;

            if(_mReportTableSet != null ) _mReportTableSet.mClear();    // show in table: analysis index numbers [- sample flags]
            if( _mReportStripSet != null ) _mReportStripSet.mClear();    // show in strips: analysis index numbers [- sample flags]

            _mReportSectionMask = 0;
            if(_mSummary != null ) _mSummary.mClear();
            if( _mConclusion != null ) _mConclusion.mClear();

            _mReportUTC = DateTime.MinValue;
            _mReportedByIX = 0;
        }

        public override DSqlDataType mVarGetSet(DSqlDataCmd ACmd, UInt16 AVarIndex, ref string AVarSqlName, ref string AStringValue, out bool ArbValid)
        {
            switch ((DStudyReportVars)AVarIndex)
            {
                case DStudyReportVars.StudyIX:
                    return mSqlGetSetUInt32(ref _mStudyIX, "Study_IX", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DStudyReportVars.ReportNr:
                    return mSqlGetSetUInt16(ref _mReportNr, "ReportNr", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DStudyReportVars.ReportTableSet:
                    return mSqlGetSetStringSet(ref _mReportTableSet, "ReportTableSet", ACmd, ref AVarSqlName, ref AStringValue, 64, out ArbValid);
                case DStudyReportVars.ReportStripSet:
                    return mSqlGetSetStringSet(ref _mReportStripSet, "ReportStripSet", ACmd, ref AVarSqlName, ref AStringValue, 64, out ArbValid);
                case DStudyReportVars.ReportSection:
                    return mSqlGetSetUInt16(ref _mReportSectionMask, "ReportSectionMask", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DStudyReportVars.Summary:
                    return mSqlGetSetEncryptedString(ref _mSummary, "Summary_ES", ACmd, ref AVarSqlName, ref AStringValue, 64, out ArbValid);
                case DStudyReportVars.Conclusion:
                    return mSqlGetSetEncryptedString(ref _mConclusion, "Conclusion_ES", ACmd, ref AVarSqlName, ref AStringValue, 64, out ArbValid);
                case DStudyReportVars.ReportUTC:
                    return mSqlGetSetUTC(ref _mReportUTC, "ReportUTC", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DStudyReportVars.ReportedByIX:
                    return mSqlGetSetUInt32(ref _mReportedByIX, "ReportedBy_IX", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
            }
            return base.mVarGetSet(ACmd, AVarIndex, ref AVarSqlName, ref AStringValue, out ArbValid);
        }

        public string mGetReportLabel(UInt16 AValue)
        {
            if (AValue < (UInt16)DReportSection.NrEnumValues)
            {
                switch ((DReportSection)AValue)
                {
                    case DReportSection.Daily: return "Daily";
                    case DReportSection.Weekly: return "Weekly";
                    case DReportSection.EndOfStudy: return "End of Study";
                    case DReportSection.MCT: return "MCT";
                    case DReportSection.Hour: return "Hourly";
                    case DReportSection.Period: return "Period";
                }

            }
            return "?ReportSection?";
        }

        public string mGetReportCode(UInt16 AValue)
        {
            if (AValue < (UInt16)DReportSection.NrEnumValues)
            {
                bool bQC = false;

                switch ((DReportSection)AValue)
                {
                    case DReportSection.Daily: return CStudyInfo.sGetStudyPdfCode(DStudyPdfCode.Day, bQC); // "DSR";
                    case DReportSection.Weekly: return CStudyInfo.sGetStudyPdfCode(DStudyPdfCode.Week, bQC); // "WSR";
                    case DReportSection.EndOfStudy: return CStudyInfo.sGetStudyPdfCode(DStudyPdfCode.EOS, bQC); //"ESR";
                    case DReportSection.MCT: return CStudyInfo.sGetStudyPdfCode(DStudyPdfCode.MCT, bQC); //"MCT";
                    case DReportSection.Hour: return CStudyInfo.sGetStudyPdfCode(DStudyPdfCode.Hour, bQC); //"MCT";
                    case DReportSection.Period: return CStudyInfo.sGetStudyPdfCode(DStudyPdfCode.Period, bQC); //"MCT";

                }

            }
            return "?ReportSection?";
        }

        public string mGetMaskReportLabel(UInt16 AMask)
        {
            string label = "";

            for( UInt16 i = 0; i < (UInt16)DReportSection.NrEnumValues; ++ i)
            {
                if( (AMask & (1 << i )) != 0 )
                {
                    if( label.Length > 0 )
                    {
                        label += "+";
                    }
                    label += mGetReportLabel(i);
                }
            }
            return label;
        }
        public string mGetMaskReportCode(UInt16 AMask)
        {
            string label = "";

            for (UInt16 i = 0; i < (UInt16)DReportSection.NrEnumValues; ++i)
            {
                if ((AMask & (1 << i)) != 0)
                {
                    if (label.Length > 0)
                    {
                        label += "+";
                    }
                    label += mGetReportCode(i);
                }
            }
            return label;
        }
    }
}
