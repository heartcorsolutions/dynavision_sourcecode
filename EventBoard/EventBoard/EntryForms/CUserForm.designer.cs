﻿namespace EventboardEntryForms
{
    partial class CUserForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CUserForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBoxUserLastName = new System.Windows.Forms.TextBox();
            this.labelUserLastName = new System.Windows.Forms.Label();
            this.textBoxUserMiddleName = new System.Windows.Forms.TextBox();
            this.labelUserMiddleName = new System.Windows.Forms.Label();
            this.textBoxUserFirstName = new System.Windows.Forms.TextBox();
            this.labelUserFirstName = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.textBoxUserDateOfBirthYear = new System.Windows.Forms.TextBox();
            this.comboBoxUserDateOfBirthMonthList = new System.Windows.Forms.ComboBox();
            this.textBoxUserDateOfBirthDay = new System.Windows.Forms.TextBox();
            this.labelUserDateOfBirth = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.labelAgeResult = new System.Windows.Forms.Label();
            this.labelAge = new System.Windows.Forms.Label();
            this.comboBoxAcademicTitleList = new System.Windows.Forms.ComboBox();
            this.labelUserTitle = new System.Windows.Forms.Label();
            this.comboBoxGenderTitleList = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.checkBoxAdministrator = new System.Windows.Forms.CheckBox();
            this.checkBoxRefPhysician = new System.Windows.Forms.CheckBox();
            this.checkQualityControl = new System.Windows.Forms.CheckBox();
            this.checkAdministration = new System.Windows.Forms.CheckBox();
            this.checkBoxReadPatDetails = new System.Windows.Forms.CheckBox();
            this.checkBoxReadTriage = new System.Windows.Forms.CheckBox();
            this.checkBoxTechnician = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.checkBoxRightsPatDetails = new System.Windows.Forms.CheckBox();
            this.checkBoxPrintReports = new System.Windows.Forms.CheckBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.checkBoxPhysician = new System.Windows.Forms.CheckBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.checkBoxWriteReports = new System.Windows.Forms.CheckBox();
            this.checkBoxWriteAnalysis = new System.Windows.Forms.CheckBox();
            this.checkBoxWritePatDetails = new System.Windows.Forms.CheckBox();
            this.checkBoxWriteTriage = new System.Windows.Forms.CheckBox();
            this.checkBoxReadReports = new System.Windows.Forms.CheckBox();
            this.checkBoxReadAnalyse = new System.Windows.Forms.CheckBox();
            this.labelWriteRights = new System.Windows.Forms.Label();
            this.labelReadRights = new System.Windows.Forms.Label();
            this.labelGeneralRights = new System.Windows.Forms.Label();
            this.checkBoxInventoryManagement = new System.Windows.Forms.CheckBox();
            this.checkBoxRightsSettings = new System.Windows.Forms.CheckBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.comboBoxUserCountyList = new System.Windows.Forms.ComboBox();
            this.textBoxUserSkype = new System.Windows.Forms.TextBox();
            this.textBoxUserEmail = new System.Windows.Forms.TextBox();
            this.labelUserEmail = new System.Windows.Forms.Label();
            this.labelUserSkype = new System.Windows.Forms.Label();
            this.textBoxUserPhone2 = new System.Windows.Forms.TextBox();
            this.labelUserPhone2 = new System.Windows.Forms.Label();
            this.textBoxUserHouseNumber = new System.Windows.Forms.TextBox();
            this.labelUserHouseNumber = new System.Windows.Forms.Label();
            this.textBoxUserCell = new System.Windows.Forms.TextBox();
            this.labelUserCell = new System.Windows.Forms.Label();
            this.textBoxUserAddress = new System.Windows.Forms.TextBox();
            this.labelUserAddress = new System.Windows.Forms.Label();
            this.comboBoxUserStateList = new System.Windows.Forms.ComboBox();
            this.textBoxUserPhone1 = new System.Windows.Forms.TextBox();
            this.labelUserPhone1 = new System.Windows.Forms.Label();
            this.labelUserCountry = new System.Windows.Forms.Label();
            this.textBoxUserCity = new System.Windows.Forms.TextBox();
            this.labelUserCity = new System.Windows.Forms.Label();
            this.labelUserState = new System.Windows.Forms.Label();
            this.textBoxUserZip = new System.Windows.Forms.TextBox();
            this.labelUserZip = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel32 = new System.Windows.Forms.Panel();
            this.panel31 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.panel30 = new System.Windows.Forms.Panel();
            this.panel29 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel19 = new System.Windows.Forms.Panel();
            this.panel23 = new System.Windows.Forms.Panel();
            this.panel22 = new System.Windows.Forms.Panel();
            this.panel21 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.panel24 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel25 = new System.Windows.Forms.Panel();
            this.panel26 = new System.Windows.Forms.Panel();
            this.panel27 = new System.Windows.Forms.Panel();
            this.panel28 = new System.Windows.Forms.Panel();
            this.panel33 = new System.Windows.Forms.Panel();
            this.panel34 = new System.Windows.Forms.Panel();
            this.button4 = new System.Windows.Forms.Button();
            this.panel35 = new System.Windows.Forms.Panel();
            this.panel36 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.panel37 = new System.Windows.Forms.Panel();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel31.SuspendLayout();
            this.panel29.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel33.SuspendLayout();
            this.panel34.SuspendLayout();
            this.panel36.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 579);
            this.panel1.Margin = new System.Windows.Forms.Padding(13, 10, 13, 10);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1172, 25);
            this.panel1.TabIndex = 2;
            // 
            // textBoxUserLastName
            // 
            this.textBoxUserLastName.Location = new System.Drawing.Point(117, 126);
            this.textBoxUserLastName.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxUserLastName.Name = "textBoxUserLastName";
            this.textBoxUserLastName.Size = new System.Drawing.Size(249, 23);
            this.textBoxUserLastName.TabIndex = 9;
            // 
            // labelUserLastName
            // 
            this.labelUserLastName.AutoSize = true;
            this.labelUserLastName.Location = new System.Drawing.Point(15, 123);
            this.labelUserLastName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelUserLastName.Name = "labelUserLastName";
            this.labelUserLastName.Size = new System.Drawing.Size(78, 17);
            this.labelUserLastName.TabIndex = 8;
            this.labelUserLastName.Text = "Last name:";
            // 
            // textBoxUserMiddleName
            // 
            this.textBoxUserMiddleName.Location = new System.Drawing.Point(117, 89);
            this.textBoxUserMiddleName.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxUserMiddleName.Name = "textBoxUserMiddleName";
            this.textBoxUserMiddleName.Size = new System.Drawing.Size(249, 23);
            this.textBoxUserMiddleName.TabIndex = 7;
            // 
            // labelUserMiddleName
            // 
            this.labelUserMiddleName.AutoSize = true;
            this.labelUserMiddleName.Location = new System.Drawing.Point(15, 89);
            this.labelUserMiddleName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelUserMiddleName.Name = "labelUserMiddleName";
            this.labelUserMiddleName.Size = new System.Drawing.Size(92, 17);
            this.labelUserMiddleName.TabIndex = 6;
            this.labelUserMiddleName.Text = "Middle name:";
            // 
            // textBoxUserFirstName
            // 
            this.textBoxUserFirstName.Location = new System.Drawing.Point(117, 52);
            this.textBoxUserFirstName.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxUserFirstName.Name = "textBoxUserFirstName";
            this.textBoxUserFirstName.Size = new System.Drawing.Size(249, 23);
            this.textBoxUserFirstName.TabIndex = 5;
            this.textBoxUserFirstName.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // labelUserFirstName
            // 
            this.labelUserFirstName.AutoSize = true;
            this.labelUserFirstName.Location = new System.Drawing.Point(15, 54);
            this.labelUserFirstName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelUserFirstName.Name = "labelUserFirstName";
            this.labelUserFirstName.Size = new System.Drawing.Size(78, 17);
            this.labelUserFirstName.TabIndex = 4;
            this.labelUserFirstName.Text = "First name:";
            // 
            // label13
            // 
            this.label13.Dock = System.Windows.Forms.DockStyle.Left;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(0, 0);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(152, 30);
            this.label13.TabIndex = 69;
            this.label13.Text = "Personal details";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // textBoxUserDateOfBirthYear
            // 
            this.textBoxUserDateOfBirthYear.Location = new System.Drawing.Point(304, 163);
            this.textBoxUserDateOfBirthYear.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxUserDateOfBirthYear.Name = "textBoxUserDateOfBirthYear";
            this.textBoxUserDateOfBirthYear.Size = new System.Drawing.Size(62, 23);
            this.textBoxUserDateOfBirthYear.TabIndex = 13;
            // 
            // comboBoxUserDateOfBirthMonthList
            // 
            this.comboBoxUserDateOfBirthMonthList.DropDownWidth = 200;
            this.comboBoxUserDateOfBirthMonthList.FormattingEnabled = true;
            this.comboBoxUserDateOfBirthMonthList.Items.AddRange(new object[] {
            "January",
            "February",
            "March",
            "April",
            "May ",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"});
            this.comboBoxUserDateOfBirthMonthList.Location = new System.Drawing.Point(163, 162);
            this.comboBoxUserDateOfBirthMonthList.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxUserDateOfBirthMonthList.Name = "comboBoxUserDateOfBirthMonthList";
            this.comboBoxUserDateOfBirthMonthList.Size = new System.Drawing.Size(136, 24);
            this.comboBoxUserDateOfBirthMonthList.TabIndex = 12;
            // 
            // textBoxUserDateOfBirthDay
            // 
            this.textBoxUserDateOfBirthDay.Location = new System.Drawing.Point(117, 163);
            this.textBoxUserDateOfBirthDay.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxUserDateOfBirthDay.Name = "textBoxUserDateOfBirthDay";
            this.textBoxUserDateOfBirthDay.Size = new System.Drawing.Size(38, 23);
            this.textBoxUserDateOfBirthDay.TabIndex = 11;
            // 
            // labelUserDateOfBirth
            // 
            this.labelUserDateOfBirth.AutoSize = true;
            this.labelUserDateOfBirth.Location = new System.Drawing.Point(15, 166);
            this.labelUserDateOfBirth.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelUserDateOfBirth.Name = "labelUserDateOfBirth";
            this.labelUserDateOfBirth.Size = new System.Drawing.Size(90, 17);
            this.labelUserDateOfBirth.TabIndex = 10;
            this.labelUserDateOfBirth.Text = "Date of birth:";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownWidth = 200;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.comboBox1.Location = new System.Drawing.Point(227, 198);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(4);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(139, 24);
            this.comboBox1.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(184, 202);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 17);
            this.label6.TabIndex = 14;
            this.label6.Text = "Sex:";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel2.Controls.Add(this.labelAgeResult);
            this.panel2.Controls.Add(this.labelAge);
            this.panel2.Controls.Add(this.comboBoxAcademicTitleList);
            this.panel2.Controls.Add(this.labelUserTitle);
            this.panel2.Controls.Add(this.comboBoxGenderTitleList);
            this.panel2.Controls.Add(this.textBoxUserDateOfBirthYear);
            this.panel2.Controls.Add(this.comboBoxUserDateOfBirthMonthList);
            this.panel2.Controls.Add(this.textBoxUserDateOfBirthDay);
            this.panel2.Controls.Add(this.labelUserDateOfBirth);
            this.panel2.Controls.Add(this.comboBox1);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.textBoxUserLastName);
            this.panel2.Controls.Add(this.labelUserLastName);
            this.panel2.Controls.Add(this.textBoxUserMiddleName);
            this.panel2.Controls.Add(this.labelUserMiddleName);
            this.panel2.Controls.Add(this.textBoxUserFirstName);
            this.panel2.Controls.Add(this.labelUserFirstName);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(466, 285);
            this.panel2.TabIndex = 98;
            // 
            // labelAgeResult
            // 
            this.labelAgeResult.AutoSize = true;
            this.labelAgeResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAgeResult.Location = new System.Drawing.Point(117, 202);
            this.labelAgeResult.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelAgeResult.Name = "labelAgeResult";
            this.labelAgeResult.Size = new System.Drawing.Size(35, 17);
            this.labelAgeResult.TabIndex = 101;
            this.labelAgeResult.Text = "age";
            // 
            // labelAge
            // 
            this.labelAge.AutoSize = true;
            this.labelAge.Location = new System.Drawing.Point(15, 202);
            this.labelAge.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelAge.Name = "labelAge";
            this.labelAge.Size = new System.Drawing.Size(37, 17);
            this.labelAge.TabIndex = 100;
            this.labelAge.Text = "Age:";
            // 
            // comboBoxAcademicTitleList
            // 
            this.comboBoxAcademicTitleList.DropDownWidth = 200;
            this.comboBoxAcademicTitleList.FormattingEnabled = true;
            this.comboBoxAcademicTitleList.Items.AddRange(new object[] {
            "Prof. Dr.",
            "Prof.",
            "Dr.",
            "Msc.",
            "Bsc.",
            "MBBS",
            "MBA"});
            this.comboBoxAcademicTitleList.Location = new System.Drawing.Point(220, 14);
            this.comboBoxAcademicTitleList.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxAcademicTitleList.Name = "comboBoxAcademicTitleList";
            this.comboBoxAcademicTitleList.Size = new System.Drawing.Size(146, 24);
            this.comboBoxAcademicTitleList.TabIndex = 3;
            // 
            // labelUserTitle
            // 
            this.labelUserTitle.AutoSize = true;
            this.labelUserTitle.Location = new System.Drawing.Point(15, 18);
            this.labelUserTitle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelUserTitle.Name = "labelUserTitle";
            this.labelUserTitle.Size = new System.Drawing.Size(39, 17);
            this.labelUserTitle.TabIndex = 1;
            this.labelUserTitle.Text = "Title:";
            this.labelUserTitle.Click += new System.EventHandler(this.label10_Click_1);
            // 
            // comboBoxGenderTitleList
            // 
            this.comboBoxGenderTitleList.DropDownWidth = 200;
            this.comboBoxGenderTitleList.FormattingEnabled = true;
            this.comboBoxGenderTitleList.Items.AddRange(new object[] {
            "Mr.",
            "Mrs.",
            "Ms."});
            this.comboBoxGenderTitleList.Location = new System.Drawing.Point(117, 14);
            this.comboBoxGenderTitleList.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxGenderTitleList.Name = "comboBoxGenderTitleList";
            this.comboBoxGenderTitleList.Size = new System.Drawing.Size(96, 24);
            this.comboBoxGenderTitleList.TabIndex = 2;
            this.comboBoxGenderTitleList.SelectedIndexChanged += new System.EventHandler(this.comboBox3_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.Dock = System.Windows.Forms.DockStyle.Left;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(0, 0);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(144, 31);
            this.label8.TabIndex = 101;
            this.label8.Text = "User type";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // checkBoxAdministrator
            // 
            this.checkBoxAdministrator.AutoSize = true;
            this.checkBoxAdministrator.Location = new System.Drawing.Point(13, 25);
            this.checkBoxAdministrator.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxAdministrator.Name = "checkBoxAdministrator";
            this.checkBoxAdministrator.Size = new System.Drawing.Size(110, 21);
            this.checkBoxAdministrator.TabIndex = 38;
            this.checkBoxAdministrator.Text = "Administrator";
            this.checkBoxAdministrator.UseVisualStyleBackColor = true;
            // 
            // checkBoxRefPhysician
            // 
            this.checkBoxRefPhysician.AutoSize = true;
            this.checkBoxRefPhysician.Location = new System.Drawing.Point(13, 50);
            this.checkBoxRefPhysician.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxRefPhysician.Name = "checkBoxRefPhysician";
            this.checkBoxRefPhysician.Size = new System.Drawing.Size(150, 21);
            this.checkBoxRefPhysician.TabIndex = 39;
            this.checkBoxRefPhysician.Text = "Referring Physician";
            this.checkBoxRefPhysician.UseVisualStyleBackColor = true;
            // 
            // checkQualityControl
            // 
            this.checkQualityControl.AutoSize = true;
            this.checkQualityControl.Location = new System.Drawing.Point(184, 25);
            this.checkQualityControl.Margin = new System.Windows.Forms.Padding(4);
            this.checkQualityControl.Name = "checkQualityControl";
            this.checkQualityControl.Size = new System.Drawing.Size(118, 21);
            this.checkQualityControl.TabIndex = 42;
            this.checkQualityControl.Text = "Quality control";
            this.checkQualityControl.UseVisualStyleBackColor = true;
            this.checkQualityControl.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            // 
            // checkAdministration
            // 
            this.checkAdministration.AutoSize = true;
            this.checkAdministration.Location = new System.Drawing.Point(184, 50);
            this.checkAdministration.Margin = new System.Windows.Forms.Padding(4);
            this.checkAdministration.Name = "checkAdministration";
            this.checkAdministration.Size = new System.Drawing.Size(115, 21);
            this.checkAdministration.TabIndex = 43;
            this.checkAdministration.Text = "Administrative";
            this.checkAdministration.UseVisualStyleBackColor = true;
            // 
            // checkBoxReadPatDetails
            // 
            this.checkBoxReadPatDetails.AutoSize = true;
            this.checkBoxReadPatDetails.Location = new System.Drawing.Point(297, 49);
            this.checkBoxReadPatDetails.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxReadPatDetails.Name = "checkBoxReadPatDetails";
            this.checkBoxReadPatDetails.Size = new System.Drawing.Size(116, 21);
            this.checkBoxReadPatDetails.TabIndex = 48;
            this.checkBoxReadPatDetails.Text = "Patient details";
            this.checkBoxReadPatDetails.UseVisualStyleBackColor = true;
            this.checkBoxReadPatDetails.CheckedChanged += new System.EventHandler(this.checkBox5_CheckedChanged);
            // 
            // checkBoxReadTriage
            // 
            this.checkBoxReadTriage.AutoSize = true;
            this.checkBoxReadTriage.Location = new System.Drawing.Point(297, 72);
            this.checkBoxReadTriage.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxReadTriage.Name = "checkBoxReadTriage";
            this.checkBoxReadTriage.Size = new System.Drawing.Size(72, 21);
            this.checkBoxReadTriage.TabIndex = 49;
            this.checkBoxReadTriage.Text = "Triage ";
            this.checkBoxReadTriage.UseVisualStyleBackColor = true;
            // 
            // checkBoxTechnician
            // 
            this.checkBoxTechnician.AutoSize = true;
            this.checkBoxTechnician.Location = new System.Drawing.Point(13, 100);
            this.checkBoxTechnician.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxTechnician.Name = "checkBoxTechnician";
            this.checkBoxTechnician.Size = new System.Drawing.Size(96, 21);
            this.checkBoxTechnician.TabIndex = 41;
            this.checkBoxTechnician.Text = "Technician";
            this.checkBoxTechnician.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.Dock = System.Windows.Forms.DockStyle.Left;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(0, 0);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(98, 31);
            this.label9.TabIndex = 109;
            this.label9.Text = "Rights";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // checkBoxRightsPatDetails
            // 
            this.checkBoxRightsPatDetails.AutoSize = true;
            this.checkBoxRightsPatDetails.Location = new System.Drawing.Point(32, 49);
            this.checkBoxRightsPatDetails.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxRightsPatDetails.Name = "checkBoxRightsPatDetails";
            this.checkBoxRightsPatDetails.Size = new System.Drawing.Size(159, 21);
            this.checkBoxRightsPatDetails.TabIndex = 44;
            this.checkBoxRightsPatDetails.Text = "Patient details visible";
            this.checkBoxRightsPatDetails.UseVisualStyleBackColor = true;
            // 
            // checkBoxPrintReports
            // 
            this.checkBoxPrintReports.AutoSize = true;
            this.checkBoxPrintReports.Location = new System.Drawing.Point(32, 95);
            this.checkBoxPrintReports.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxPrintReports.Name = "checkBoxPrintReports";
            this.checkBoxPrintReports.Size = new System.Drawing.Size(105, 21);
            this.checkBoxPrintReports.TabIndex = 46;
            this.checkBoxPrintReports.Text = "Print reports";
            this.checkBoxPrintReports.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel4.Controls.Add(this.checkBoxPhysician);
            this.panel4.Controls.Add(this.checkBoxTechnician);
            this.panel4.Controls.Add(this.checkAdministration);
            this.panel4.Controls.Add(this.checkQualityControl);
            this.panel4.Controls.Add(this.checkBoxRefPhysician);
            this.panel4.Controls.Add(this.checkBoxAdministrator);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Margin = new System.Windows.Forms.Padding(4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(466, 161);
            this.panel4.TabIndex = 112;
            // 
            // checkBoxPhysician
            // 
            this.checkBoxPhysician.AutoSize = true;
            this.checkBoxPhysician.Location = new System.Drawing.Point(13, 75);
            this.checkBoxPhysician.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxPhysician.Name = "checkBoxPhysician";
            this.checkBoxPhysician.Size = new System.Drawing.Size(87, 21);
            this.checkBoxPhysician.TabIndex = 40;
            this.checkBoxPhysician.Text = "Physician";
            this.checkBoxPhysician.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel6.Controls.Add(this.checkBoxWriteReports);
            this.panel6.Controls.Add(this.checkBoxWriteAnalysis);
            this.panel6.Controls.Add(this.checkBoxWritePatDetails);
            this.panel6.Controls.Add(this.checkBoxWriteTriage);
            this.panel6.Controls.Add(this.checkBoxReadReports);
            this.panel6.Controls.Add(this.checkBoxReadAnalyse);
            this.panel6.Controls.Add(this.labelWriteRights);
            this.panel6.Controls.Add(this.labelReadRights);
            this.panel6.Controls.Add(this.labelGeneralRights);
            this.panel6.Controls.Add(this.checkBoxInventoryManagement);
            this.panel6.Controls.Add(this.checkBoxRightsSettings);
            this.panel6.Controls.Add(this.checkBoxPrintReports);
            this.panel6.Controls.Add(this.checkBoxRightsPatDetails);
            this.panel6.Controls.Add(this.checkBoxReadPatDetails);
            this.panel6.Controls.Add(this.checkBoxReadTriage);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Margin = new System.Windows.Forms.Padding(4);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(645, 161);
            this.panel6.TabIndex = 113;
            this.panel6.Paint += new System.Windows.Forms.PaintEventHandler(this.panel6_Paint);
            // 
            // checkBoxWriteReports
            // 
            this.checkBoxWriteReports.AutoSize = true;
            this.checkBoxWriteReports.Location = new System.Drawing.Point(504, 118);
            this.checkBoxWriteReports.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxWriteReports.Name = "checkBoxWriteReports";
            this.checkBoxWriteReports.Size = new System.Drawing.Size(81, 21);
            this.checkBoxWriteReports.TabIndex = 55;
            this.checkBoxWriteReports.Text = "Reports ";
            this.checkBoxWriteReports.UseVisualStyleBackColor = true;
            // 
            // checkBoxWriteAnalysis
            // 
            this.checkBoxWriteAnalysis.AutoSize = true;
            this.checkBoxWriteAnalysis.Location = new System.Drawing.Point(504, 95);
            this.checkBoxWriteAnalysis.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxWriteAnalysis.Name = "checkBoxWriteAnalysis";
            this.checkBoxWriteAnalysis.Size = new System.Drawing.Size(81, 21);
            this.checkBoxWriteAnalysis.TabIndex = 54;
            this.checkBoxWriteAnalysis.Text = "Analyse ";
            this.checkBoxWriteAnalysis.UseVisualStyleBackColor = true;
            // 
            // checkBoxWritePatDetails
            // 
            this.checkBoxWritePatDetails.AutoSize = true;
            this.checkBoxWritePatDetails.Location = new System.Drawing.Point(504, 49);
            this.checkBoxWritePatDetails.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxWritePatDetails.Name = "checkBoxWritePatDetails";
            this.checkBoxWritePatDetails.Size = new System.Drawing.Size(116, 21);
            this.checkBoxWritePatDetails.TabIndex = 52;
            this.checkBoxWritePatDetails.Text = "Patient details";
            this.checkBoxWritePatDetails.UseVisualStyleBackColor = true;
            // 
            // checkBoxWriteTriage
            // 
            this.checkBoxWriteTriage.AutoSize = true;
            this.checkBoxWriteTriage.Location = new System.Drawing.Point(504, 72);
            this.checkBoxWriteTriage.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxWriteTriage.Name = "checkBoxWriteTriage";
            this.checkBoxWriteTriage.Size = new System.Drawing.Size(72, 21);
            this.checkBoxWriteTriage.TabIndex = 53;
            this.checkBoxWriteTriage.Text = "Triage ";
            this.checkBoxWriteTriage.UseVisualStyleBackColor = true;
            // 
            // checkBoxReadReports
            // 
            this.checkBoxReadReports.AutoSize = true;
            this.checkBoxReadReports.Location = new System.Drawing.Point(297, 118);
            this.checkBoxReadReports.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxReadReports.Name = "checkBoxReadReports";
            this.checkBoxReadReports.Size = new System.Drawing.Size(81, 21);
            this.checkBoxReadReports.TabIndex = 51;
            this.checkBoxReadReports.Text = "Reports ";
            this.checkBoxReadReports.UseVisualStyleBackColor = true;
            // 
            // checkBoxReadAnalyse
            // 
            this.checkBoxReadAnalyse.AutoSize = true;
            this.checkBoxReadAnalyse.Location = new System.Drawing.Point(297, 95);
            this.checkBoxReadAnalyse.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxReadAnalyse.Name = "checkBoxReadAnalyse";
            this.checkBoxReadAnalyse.Size = new System.Drawing.Size(81, 21);
            this.checkBoxReadAnalyse.TabIndex = 50;
            this.checkBoxReadAnalyse.Text = "Analyse ";
            this.checkBoxReadAnalyse.UseVisualStyleBackColor = true;
            // 
            // labelWriteRights
            // 
            this.labelWriteRights.AutoSize = true;
            this.labelWriteRights.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelWriteRights.Location = new System.Drawing.Point(501, 17);
            this.labelWriteRights.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelWriteRights.Name = "labelWriteRights";
            this.labelWriteRights.Size = new System.Drawing.Size(46, 17);
            this.labelWriteRights.TabIndex = 117;
            this.labelWriteRights.Text = "Write";
            // 
            // labelReadRights
            // 
            this.labelReadRights.AutoSize = true;
            this.labelReadRights.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelReadRights.Location = new System.Drawing.Point(294, 17);
            this.labelReadRights.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelReadRights.Name = "labelReadRights";
            this.labelReadRights.Size = new System.Drawing.Size(46, 17);
            this.labelReadRights.TabIndex = 116;
            this.labelReadRights.Text = "Read";
            // 
            // labelGeneralRights
            // 
            this.labelGeneralRights.AutoSize = true;
            this.labelGeneralRights.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGeneralRights.Location = new System.Drawing.Point(31, 17);
            this.labelGeneralRights.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelGeneralRights.Name = "labelGeneralRights";
            this.labelGeneralRights.Size = new System.Drawing.Size(66, 17);
            this.labelGeneralRights.TabIndex = 115;
            this.labelGeneralRights.Text = "General";
            // 
            // checkBoxInventoryManagement
            // 
            this.checkBoxInventoryManagement.AutoSize = true;
            this.checkBoxInventoryManagement.Location = new System.Drawing.Point(32, 118);
            this.checkBoxInventoryManagement.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxInventoryManagement.Name = "checkBoxInventoryManagement";
            this.checkBoxInventoryManagement.Size = new System.Drawing.Size(171, 21);
            this.checkBoxInventoryManagement.TabIndex = 47;
            this.checkBoxInventoryManagement.Text = "Inventory management";
            this.checkBoxInventoryManagement.UseVisualStyleBackColor = true;
            // 
            // checkBoxRightsSettings
            // 
            this.checkBoxRightsSettings.AutoSize = true;
            this.checkBoxRightsSettings.Location = new System.Drawing.Point(32, 72);
            this.checkBoxRightsSettings.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxRightsSettings.Name = "checkBoxRightsSettings";
            this.checkBoxRightsSettings.Size = new System.Drawing.Size(186, 21);
            this.checkBoxRightsSettings.TabIndex = 45;
            this.checkBoxRightsSettings.Text = "Change program settings";
            this.checkBoxRightsSettings.UseVisualStyleBackColor = true;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.comboBoxUserCountyList);
            this.panel7.Controls.Add(this.textBoxUserSkype);
            this.panel7.Controls.Add(this.textBoxUserEmail);
            this.panel7.Controls.Add(this.labelUserEmail);
            this.panel7.Controls.Add(this.labelUserSkype);
            this.panel7.Controls.Add(this.textBoxUserPhone2);
            this.panel7.Controls.Add(this.labelUserPhone2);
            this.panel7.Controls.Add(this.textBoxUserHouseNumber);
            this.panel7.Controls.Add(this.labelUserHouseNumber);
            this.panel7.Controls.Add(this.textBoxUserCell);
            this.panel7.Controls.Add(this.labelUserCell);
            this.panel7.Controls.Add(this.textBoxUserAddress);
            this.panel7.Controls.Add(this.labelUserAddress);
            this.panel7.Controls.Add(this.comboBoxUserStateList);
            this.panel7.Controls.Add(this.textBoxUserPhone1);
            this.panel7.Controls.Add(this.labelUserPhone1);
            this.panel7.Controls.Add(this.labelUserCountry);
            this.panel7.Controls.Add(this.textBoxUserCity);
            this.panel7.Controls.Add(this.labelUserCity);
            this.panel7.Controls.Add(this.labelUserState);
            this.panel7.Controls.Add(this.textBoxUserZip);
            this.panel7.Controls.Add(this.labelUserZip);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Margin = new System.Windows.Forms.Padding(4);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(935, 285);
            this.panel7.TabIndex = 124;
            this.panel7.Paint += new System.Windows.Forms.PaintEventHandler(this.panel7_Paint);
            // 
            // comboBoxUserCountyList
            // 
            this.comboBoxUserCountyList.DropDownWidth = 200;
            this.comboBoxUserCountyList.FormattingEnabled = true;
            this.comboBoxUserCountyList.Items.AddRange(new object[] {
            "USA",
            "Mexico"});
            this.comboBoxUserCountyList.Location = new System.Drawing.Point(414, 121);
            this.comboBoxUserCountyList.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxUserCountyList.Name = "comboBoxUserCountyList";
            this.comboBoxUserCountyList.Size = new System.Drawing.Size(188, 24);
            this.comboBoxUserCountyList.TabIndex = 27;
            // 
            // textBoxUserSkype
            // 
            this.textBoxUserSkype.Location = new System.Drawing.Point(414, 198);
            this.textBoxUserSkype.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxUserSkype.Name = "textBoxUserSkype";
            this.textBoxUserSkype.Size = new System.Drawing.Size(188, 23);
            this.textBoxUserSkype.TabIndex = 35;
            // 
            // textBoxUserEmail
            // 
            this.textBoxUserEmail.Location = new System.Drawing.Point(107, 234);
            this.textBoxUserEmail.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxUserEmail.Name = "textBoxUserEmail";
            this.textBoxUserEmail.Size = new System.Drawing.Size(193, 23);
            this.textBoxUserEmail.TabIndex = 37;
            // 
            // labelUserEmail
            // 
            this.labelUserEmail.AutoSize = true;
            this.labelUserEmail.Location = new System.Drawing.Point(32, 240);
            this.labelUserEmail.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelUserEmail.Name = "labelUserEmail";
            this.labelUserEmail.Size = new System.Drawing.Size(46, 17);
            this.labelUserEmail.TabIndex = 36;
            this.labelUserEmail.Text = "Email:";
            // 
            // labelUserSkype
            // 
            this.labelUserSkype.AutoSize = true;
            this.labelUserSkype.Location = new System.Drawing.Point(335, 201);
            this.labelUserSkype.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelUserSkype.Name = "labelUserSkype";
            this.labelUserSkype.Size = new System.Drawing.Size(51, 17);
            this.labelUserSkype.TabIndex = 34;
            this.labelUserSkype.Text = "Skype:";
            // 
            // textBoxUserPhone2
            // 
            this.textBoxUserPhone2.Location = new System.Drawing.Point(414, 160);
            this.textBoxUserPhone2.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxUserPhone2.Name = "textBoxUserPhone2";
            this.textBoxUserPhone2.Size = new System.Drawing.Size(188, 23);
            this.textBoxUserPhone2.TabIndex = 31;
            // 
            // labelUserPhone2
            // 
            this.labelUserPhone2.AutoSize = true;
            this.labelUserPhone2.Location = new System.Drawing.Point(335, 163);
            this.labelUserPhone2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelUserPhone2.Name = "labelUserPhone2";
            this.labelUserPhone2.Size = new System.Drawing.Size(65, 17);
            this.labelUserPhone2.TabIndex = 30;
            this.labelUserPhone2.Text = "Phone 2:";
            // 
            // textBoxUserHouseNumber
            // 
            this.textBoxUserHouseNumber.Location = new System.Drawing.Point(231, 85);
            this.textBoxUserHouseNumber.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxUserHouseNumber.Name = "textBoxUserHouseNumber";
            this.textBoxUserHouseNumber.Size = new System.Drawing.Size(69, 23);
            this.textBoxUserHouseNumber.TabIndex = 21;
            // 
            // labelUserHouseNumber
            // 
            this.labelUserHouseNumber.AutoSize = true;
            this.labelUserHouseNumber.Location = new System.Drawing.Point(194, 88);
            this.labelUserHouseNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelUserHouseNumber.Name = "labelUserHouseNumber";
            this.labelUserHouseNumber.Size = new System.Drawing.Size(27, 17);
            this.labelUserHouseNumber.TabIndex = 20;
            this.labelUserHouseNumber.Text = "Nr:";
            // 
            // textBoxUserCell
            // 
            this.textBoxUserCell.Location = new System.Drawing.Point(107, 198);
            this.textBoxUserCell.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxUserCell.Name = "textBoxUserCell";
            this.textBoxUserCell.Size = new System.Drawing.Size(193, 23);
            this.textBoxUserCell.TabIndex = 33;
            // 
            // labelUserCell
            // 
            this.labelUserCell.AutoSize = true;
            this.labelUserCell.Location = new System.Drawing.Point(32, 201);
            this.labelUserCell.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelUserCell.Name = "labelUserCell";
            this.labelUserCell.Size = new System.Drawing.Size(35, 17);
            this.labelUserCell.TabIndex = 32;
            this.labelUserCell.Text = "Cell:";
            // 
            // textBoxUserAddress
            // 
            this.textBoxUserAddress.Location = new System.Drawing.Point(107, 8);
            this.textBoxUserAddress.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxUserAddress.Multiline = true;
            this.textBoxUserAddress.Name = "textBoxUserAddress";
            this.textBoxUserAddress.Size = new System.Drawing.Size(495, 66);
            this.textBoxUserAddress.TabIndex = 17;
            // 
            // labelUserAddress
            // 
            this.labelUserAddress.AutoSize = true;
            this.labelUserAddress.Location = new System.Drawing.Point(32, 12);
            this.labelUserAddress.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelUserAddress.Name = "labelUserAddress";
            this.labelUserAddress.Size = new System.Drawing.Size(64, 17);
            this.labelUserAddress.TabIndex = 16;
            this.labelUserAddress.Text = "Address:";
            // 
            // comboBoxUserStateList
            // 
            this.comboBoxUserStateList.DropDownWidth = 200;
            this.comboBoxUserStateList.FormattingEnabled = true;
            this.comboBoxUserStateList.Items.AddRange(new object[] {
            "NA",
            "Alabama",
            "Alaska",
            "Arizona",
            "Arkansas",
            "California",
            "Colorado",
            "Connecticut",
            "Delaware",
            "District of Columbia",
            "Florida",
            "Georgia",
            "Hawaii",
            "Idaho",
            "Illinois",
            "Indiana",
            "Iowa",
            "Kansas",
            "Kentucky",
            "Louisiana",
            "Maine",
            "Maryland",
            "Massachusetts",
            "Michigan",
            "Minnesota",
            "Mississippi",
            "Missouri",
            "Montana",
            "Nebraska",
            "Nevada",
            "New Hampshire",
            "New Jersey",
            "New Mexico",
            "New York",
            "North Carolina",
            "North Dakota",
            "Ohio",
            "Oklahoma",
            "Oregon",
            "Pennsylvania",
            "Rhode Island",
            "South Carolina",
            "South Dakota",
            "Tennessee",
            "Texas",
            "Utah",
            "Vermont",
            "Virginia",
            "Washington",
            "West Virginia",
            "Wisconsin"});
            this.comboBoxUserStateList.Location = new System.Drawing.Point(414, 84);
            this.comboBoxUserStateList.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxUserStateList.Name = "comboBoxUserStateList";
            this.comboBoxUserStateList.Size = new System.Drawing.Size(188, 24);
            this.comboBoxUserStateList.TabIndex = 23;
            // 
            // textBoxUserPhone1
            // 
            this.textBoxUserPhone1.Location = new System.Drawing.Point(107, 160);
            this.textBoxUserPhone1.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxUserPhone1.Name = "textBoxUserPhone1";
            this.textBoxUserPhone1.Size = new System.Drawing.Size(193, 23);
            this.textBoxUserPhone1.TabIndex = 29;
            // 
            // labelUserPhone1
            // 
            this.labelUserPhone1.AutoSize = true;
            this.labelUserPhone1.Location = new System.Drawing.Point(32, 163);
            this.labelUserPhone1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelUserPhone1.Name = "labelUserPhone1";
            this.labelUserPhone1.Size = new System.Drawing.Size(65, 17);
            this.labelUserPhone1.TabIndex = 28;
            this.labelUserPhone1.Text = "Phone 1:";
            // 
            // labelUserCountry
            // 
            this.labelUserCountry.AutoSize = true;
            this.labelUserCountry.Location = new System.Drawing.Point(335, 125);
            this.labelUserCountry.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelUserCountry.Name = "labelUserCountry";
            this.labelUserCountry.Size = new System.Drawing.Size(61, 17);
            this.labelUserCountry.TabIndex = 26;
            this.labelUserCountry.Text = "Country:";
            // 
            // textBoxUserCity
            // 
            this.textBoxUserCity.Location = new System.Drawing.Point(107, 122);
            this.textBoxUserCity.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxUserCity.Name = "textBoxUserCity";
            this.textBoxUserCity.Size = new System.Drawing.Size(193, 23);
            this.textBoxUserCity.TabIndex = 25;
            // 
            // labelUserCity
            // 
            this.labelUserCity.AutoSize = true;
            this.labelUserCity.Location = new System.Drawing.Point(32, 125);
            this.labelUserCity.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelUserCity.Name = "labelUserCity";
            this.labelUserCity.Size = new System.Drawing.Size(35, 17);
            this.labelUserCity.TabIndex = 24;
            this.labelUserCity.Text = "City:";
            // 
            // labelUserState
            // 
            this.labelUserState.AutoSize = true;
            this.labelUserState.Location = new System.Drawing.Point(335, 88);
            this.labelUserState.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelUserState.Name = "labelUserState";
            this.labelUserState.Size = new System.Drawing.Size(45, 17);
            this.labelUserState.TabIndex = 22;
            this.labelUserState.Text = "State:";
            // 
            // textBoxUserZip
            // 
            this.textBoxUserZip.Location = new System.Drawing.Point(107, 85);
            this.textBoxUserZip.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxUserZip.Name = "textBoxUserZip";
            this.textBoxUserZip.Size = new System.Drawing.Size(77, 23);
            this.textBoxUserZip.TabIndex = 19;
            // 
            // labelUserZip
            // 
            this.labelUserZip.AutoSize = true;
            this.labelUserZip.Location = new System.Drawing.Point(32, 88);
            this.labelUserZip.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelUserZip.Name = "labelUserZip";
            this.labelUserZip.Size = new System.Drawing.Size(32, 17);
            this.labelUserZip.TabIndex = 18;
            this.labelUserZip.Text = "Zip:";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Controls.Add(this.panel32);
            this.panel3.Controls.Add(this.panel31);
            this.panel3.Controls.Add(this.panel30);
            this.panel3.Controls.Add(this.panel29);
            this.panel3.Controls.Add(this.panel10);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1172, 30);
            this.panel3.TabIndex = 125;
            // 
            // panel5
            // 
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(982, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(190, 30);
            this.panel5.TabIndex = 5;
            // 
            // panel32
            // 
            this.panel32.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel32.Location = new System.Drawing.Point(644, 0);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(58, 30);
            this.panel32.TabIndex = 4;
            // 
            // panel31
            // 
            this.panel31.Controls.Add(this.label5);
            this.panel31.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel31.Location = new System.Drawing.Point(507, 0);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(137, 30);
            this.panel31.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Left;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(133, 30);
            this.label5.TabIndex = 99;
            this.label5.Text = "Contact details";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel30
            // 
            this.panel30.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel30.Location = new System.Drawing.Point(189, 0);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(318, 30);
            this.panel30.TabIndex = 2;
            // 
            // panel29
            // 
            this.panel29.Controls.Add(this.label13);
            this.panel29.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel29.Location = new System.Drawing.Point(22, 0);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(167, 30);
            this.panel29.TabIndex = 1;
            // 
            // panel10
            // 
            this.panel10.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(22, 30);
            this.panel10.TabIndex = 0;
            // 
            // panel12
            // 
            this.panel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel12.Location = new System.Drawing.Point(0, 30);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(1172, 11);
            this.panel12.TabIndex = 127;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.panel18);
            this.panel13.Controls.Add(this.panel16);
            this.panel13.Controls.Add(this.panel15);
            this.panel13.Controls.Add(this.panel14);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel13.Location = new System.Drawing.Point(0, 41);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(1172, 285);
            this.panel13.TabIndex = 128;
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.panel9);
            this.panel18.Controls.Add(this.panel7);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel18.Location = new System.Drawing.Point(507, 0);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(935, 285);
            this.panel18.TabIndex = 3;
            // 
            // panel9
            // 
            this.panel9.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel9.Location = new System.Drawing.Point(645, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(290, 285);
            this.panel9.TabIndex = 125;
            // 
            // panel16
            // 
            this.panel16.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel16.Location = new System.Drawing.Point(488, 0);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(19, 285);
            this.panel16.TabIndex = 2;
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.panel2);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel15.Location = new System.Drawing.Point(22, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(466, 285);
            this.panel15.TabIndex = 1;
            // 
            // panel14
            // 
            this.panel14.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel14.Location = new System.Drawing.Point(0, 0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(22, 285);
            this.panel14.TabIndex = 0;
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.panel23);
            this.panel19.Controls.Add(this.panel22);
            this.panel19.Controls.Add(this.panel21);
            this.panel19.Controls.Add(this.panel20);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel19.Location = new System.Drawing.Point(0, 326);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(1172, 31);
            this.panel19.TabIndex = 129;
            // 
            // panel23
            // 
            this.panel23.Controls.Add(this.label9);
            this.panel23.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel23.Location = new System.Drawing.Point(507, 0);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(935, 31);
            this.panel23.TabIndex = 4;
            // 
            // panel22
            // 
            this.panel22.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel22.Location = new System.Drawing.Point(488, 0);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(19, 31);
            this.panel22.TabIndex = 3;
            // 
            // panel21
            // 
            this.panel21.Controls.Add(this.label8);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel21.Location = new System.Drawing.Point(22, 0);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(466, 31);
            this.panel21.TabIndex = 2;
            // 
            // panel20
            // 
            this.panel20.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel20.Location = new System.Drawing.Point(0, 0);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(22, 31);
            this.panel20.TabIndex = 1;
            // 
            // panel24
            // 
            this.panel24.Controls.Add(this.panel11);
            this.panel24.Controls.Add(this.panel25);
            this.panel24.Controls.Add(this.panel26);
            this.panel24.Controls.Add(this.panel27);
            this.panel24.Controls.Add(this.panel28);
            this.panel24.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel24.Location = new System.Drawing.Point(0, 357);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(1172, 161);
            this.panel24.TabIndex = 130;
            // 
            // panel11
            // 
            this.panel11.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel11.Location = new System.Drawing.Point(1152, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(20, 161);
            this.panel11.TabIndex = 126;
            // 
            // panel25
            // 
            this.panel25.Controls.Add(this.panel6);
            this.panel25.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel25.Location = new System.Drawing.Point(507, 0);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(645, 161);
            this.panel25.TabIndex = 4;
            // 
            // panel26
            // 
            this.panel26.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel26.Location = new System.Drawing.Point(488, 0);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(19, 161);
            this.panel26.TabIndex = 3;
            // 
            // panel27
            // 
            this.panel27.Controls.Add(this.panel4);
            this.panel27.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel27.Location = new System.Drawing.Point(22, 0);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(466, 161);
            this.panel27.TabIndex = 2;
            // 
            // panel28
            // 
            this.panel28.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel28.Location = new System.Drawing.Point(0, 0);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(22, 161);
            this.panel28.TabIndex = 1;
            // 
            // panel33
            // 
            this.panel33.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel33.Controls.Add(this.panel34);
            this.panel33.Controls.Add(this.panel35);
            this.panel33.Controls.Add(this.panel36);
            this.panel33.Controls.Add(this.panel37);
            this.panel33.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel33.Location = new System.Drawing.Point(0, 534);
            this.panel33.Margin = new System.Windows.Forms.Padding(13, 10, 13, 10);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(1172, 45);
            this.panel33.TabIndex = 131;
            // 
            // panel34
            // 
            this.panel34.Controls.Add(this.button4);
            this.panel34.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel34.Location = new System.Drawing.Point(932, 0);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(100, 45);
            this.panel34.TabIndex = 3;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button4.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.button4.FlatAppearance.BorderSize = 2;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button4.Location = new System.Drawing.Point(0, 0);
            this.button4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(100, 45);
            this.button4.TabIndex = 6;
            this.button4.Text = "Add";
            this.button4.UseVisualStyleBackColor = false;
            // 
            // panel35
            // 
            this.panel35.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel35.Location = new System.Drawing.Point(1032, 0);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(20, 45);
            this.panel35.TabIndex = 2;
            // 
            // panel36
            // 
            this.panel36.Controls.Add(this.button1);
            this.panel36.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel36.Location = new System.Drawing.Point(1052, 0);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(100, 45);
            this.panel36.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.button1.FlatAppearance.BorderSize = 2;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button1.Location = new System.Drawing.Point(0, 0);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 45);
            this.button1.TabIndex = 6;
            this.button1.Text = "Cancel";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // panel37
            // 
            this.panel37.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel37.Location = new System.Drawing.Point(1152, 0);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(20, 45);
            this.panel37.TabIndex = 0;
            // 
            // CUserForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(1172, 604);
            this.Controls.Add(this.panel33);
            this.Controls.Add(this.panel24);
            this.Controls.Add(this.panel19);
            this.Controls.Add(this.panel13);
            this.Controls.Add(this.panel12);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.Name = "CUserForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "User";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel31.ResumeLayout(false);
            this.panel29.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel18.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel19.ResumeLayout(false);
            this.panel23.ResumeLayout(false);
            this.panel21.ResumeLayout(false);
            this.panel24.ResumeLayout(false);
            this.panel25.ResumeLayout(false);
            this.panel27.ResumeLayout(false);
            this.panel33.ResumeLayout(false);
            this.panel34.ResumeLayout(false);
            this.panel36.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBoxUserLastName;
        private System.Windows.Forms.Label labelUserLastName;
        private System.Windows.Forms.TextBox textBoxUserMiddleName;
        private System.Windows.Forms.Label labelUserMiddleName;
        private System.Windows.Forms.TextBox textBoxUserFirstName;
        private System.Windows.Forms.Label labelUserFirstName;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBoxUserDateOfBirthYear;
        private System.Windows.Forms.ComboBox comboBoxUserDateOfBirthMonthList;
        private System.Windows.Forms.TextBox textBoxUserDateOfBirthDay;
        private System.Windows.Forms.Label labelUserDateOfBirth;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox checkBoxAdministrator;
        private System.Windows.Forms.CheckBox checkBoxRefPhysician;
        private System.Windows.Forms.CheckBox checkQualityControl;
        private System.Windows.Forms.CheckBox checkAdministration;
        private System.Windows.Forms.CheckBox checkBoxReadPatDetails;
        private System.Windows.Forms.CheckBox checkBoxReadTriage;
        private System.Windows.Forms.CheckBox checkBoxTechnician;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox checkBoxRightsPatDetails;
        private System.Windows.Forms.CheckBox checkBoxPrintReports;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.ComboBox comboBoxGenderTitleList;
        private System.Windows.Forms.Label labelUserTitle;
        private System.Windows.Forms.ComboBox comboBoxAcademicTitleList;
        private System.Windows.Forms.Label labelWriteRights;
        private System.Windows.Forms.Label labelReadRights;
        private System.Windows.Forms.Label labelGeneralRights;
        private System.Windows.Forms.CheckBox checkBoxInventoryManagement;
        private System.Windows.Forms.CheckBox checkBoxRightsSettings;
        private System.Windows.Forms.CheckBox checkBoxWriteReports;
        private System.Windows.Forms.CheckBox checkBoxWriteAnalysis;
        private System.Windows.Forms.CheckBox checkBoxWritePatDetails;
        private System.Windows.Forms.CheckBox checkBoxWriteTriage;
        private System.Windows.Forms.CheckBox checkBoxReadReports;
        private System.Windows.Forms.CheckBox checkBoxReadAnalyse;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.ComboBox comboBoxUserCountyList;
        private System.Windows.Forms.TextBox textBoxUserSkype;
        private System.Windows.Forms.Label labelUserSkype;
        private System.Windows.Forms.TextBox textBoxUserEmail;
        private System.Windows.Forms.Label labelUserEmail;
        private System.Windows.Forms.TextBox textBoxUserPhone2;
        private System.Windows.Forms.Label labelUserPhone2;
        private System.Windows.Forms.TextBox textBoxUserHouseNumber;
        private System.Windows.Forms.Label labelUserHouseNumber;
        private System.Windows.Forms.TextBox textBoxUserCell;
        private System.Windows.Forms.Label labelUserCell;
        private System.Windows.Forms.TextBox textBoxUserAddress;
        private System.Windows.Forms.Label labelUserAddress;
        private System.Windows.Forms.ComboBox comboBoxUserStateList;
        private System.Windows.Forms.TextBox textBoxUserPhone1;
        private System.Windows.Forms.Label labelUserPhone1;
        private System.Windows.Forms.Label labelUserCountry;
        private System.Windows.Forms.TextBox textBoxUserCity;
        private System.Windows.Forms.Label labelUserCity;
        private System.Windows.Forms.Label labelUserState;
        private System.Windows.Forms.TextBox textBoxUserZip;
        private System.Windows.Forms.Label labelUserZip;
        private System.Windows.Forms.CheckBox checkBoxPhysician;
        private System.Windows.Forms.Label labelAgeResult;
        private System.Windows.Forms.Label labelAge;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.Panel panel35;
        private System.Windows.Forms.Panel panel36;
        private System.Windows.Forms.Panel panel37;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel34;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button1;
    }
}