﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventboardEntryForms
{
    public class CUserInfo
    {

        public string _mGenderTitleList;
        public string _mAcademicTitleList;
        public string _mUserFirstName;
        public string _mUserMiddleName;
        public string _mUserLastName;
        public string _mDateOfBirthDay;
        //public string _mDateOfBirthMonthList;
        public string _mDateOfBirthYear;
        // public string _mAge   Dit is een gecalculeerde waarde op basis van de vorige invoer
        public string _mUserAddress;
        public string _mUserZip;
        public string _mUserHouseNumber;
        // public string _mUserStateList;
        public string _mUserCity;
        // public string _mUserCountryList;
        public string _mUserPhone1;
        public string _mUserPhone2;
        public string _mUserCell;
        public string _mUserSkype;
        public string _mUserEmail;        
    }
}
