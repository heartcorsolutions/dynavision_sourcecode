﻿using Event_Base;
using EventBoard;
using EventBoard.Properties;
using EventBoard.EntryForms;
using Program_Base;
using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventboardEntryForms
{
    public enum DFindingsTableVars
    {
        IncludeTable, IncludeStrip, ReportDate, ReportNr, EventNr, RecState, EventDateTime,
        EventMA, AnalysisNr, AnalysisDate, Technician,
        Classification, Rhythm, Findings, Symptoms, HrMin, HrMean, HrMax, N,
        AnalysisIndex, IsDay, FindingsClass, RecordIX,
        RecordET, NR_FindingsTableVars
    }
    public partial class CPreviousFindingsForm : Form
    {
        public static bool _sbPlot2Ch = false;
        public static bool _sbBlackWhite = false;

        public CStudyReport _mStudyReport = null;
        public CStudyInfo _mStudyInfo = null;
        public CPatientInfo _mPatientInfo = null;
        private CRecAnalysis _mAnalysisInfo = null;
        public List<CSqlDataTableRow> _mAnalysisList = null;
        public List<CSqlDataTableRow> _mRecordList = null;  // cash all records collected for stat to use to cal start and end time
        private bool _mbEditReport;
        UInt32 _mStudyIndex;
        UInt32 _mStudyReportIndex;
        UInt32 _mAnalysisIndex;

        //public string _mCreadedByInitials;

        public UInt16 _mSelectedTableN, _mTotalN;
        public UInt16 _mSelectedStripN;
        public UInt16 _mSelectedManN, _mTotalManN;
        public UInt16 _mSelectedQcN, _mTotalQCN;
        public UInt16 _mSelectedTachy, _mTotalTachy;
        public UInt16 _mSelectedBrady, _mTotalBrady;
        public UInt16 _mSelectedPause, _mTotalPause;
        public UInt16 _mSelectedAfib, _mTotalAfib;
        public UInt16 _mSelectedAFl, _mTotalAFl;
        public UInt16 _mSelectedPAC, _mTotalPAC;
        public UInt16 _mSelectedPVC, _mTotalPVC;
        public UInt16 _mSelectedPace, _mTotalPace;
        public UInt16 _mSelectedVT, _mTotalVT;
        public UInt16 _mSelectedBL, _mTotalBL;
        public UInt16 _mSelectedOther, _mTotalOther;
        public UInt16 _mSelectedNorm, _mTotalNorm;
        public UInt16 _mSelectedAbN, _mTotalAbN;
        public UInt16 _mTotalExtra;

        public float _mSelectedHrMin, _mTotalHrMin;
        public float _mSelectedHrMax, _mTotalHrMax;
        public float _mSelectedHrMean, _mTotalHrMean;
        public UInt16 _mSelectedHrCount, _mTotalHrCount;

        public UInt16 _mSelectedHrMinIX, _mSelectedHrMaxIX;
        public float _mTachyHrMin, _mTachyHrMax;
        public float _mBradyHrMin, _mBradyHrMax;
        public float _mPauseHrMin, _mPauseHrMax;
        public float _mAfibHrMin, _mAfibHrMax;
        public float _mAfHrMin, _mAfHrMax;
        public float _mAflHrMin, _mAflHrMax;
        public float _mPacHrMin, _mPacHrMax;
        public float _mPvcHrMin, _mPvcHrMax;
        public float _mPaceHrMin, _mPaceHrMax;
        public float _mVtHrMin, _mVtHrMax;
        public float _mBlHrMin, _mBlHrMax;
        public float _mOtherHrMin, _mOtherHrMax;
        public float _mNormHrMin, _mNormHrMax;
        public float _mAbNHrMin, _mAbNHrMax;

        public UInt16 _mAutoActivation, _mManualActivation;
        public UInt16 _mDayActivation, _mNightActivation;


        public CStringSet _mSelectedTableSet;
        public CStringSet _mSelectedStripSet;
        //const int cAnalysisColumn = 15;

        DateTime _mStartPeriodDT;   // needed for print
        DateTime _mEndPeriodDT;
        string _mRecordsInfo;
        bool _mbUseExcluded = false;

        public CPreviousFindingsForm(UInt32 AStudyIndex, bool AbEditReport, UInt32 AStudyReportIndex, UInt32 AAnalysisIndex)
        {
            bool bOk = false;
            bool bError = false;
            bool bLimitReached = false;

            try
            {
                Width = 1600;   // Nicer size then minimum 1240
                CDvtmsData.sbInitData();

                _mStudyIndex = AStudyIndex;
                _mStudyReportIndex = AStudyReportIndex;
                _mAnalysisIndex = AAnalysisIndex;
                _mbEditReport = AbEditReport;
                _mbUseExcluded = FormEventBoard._sbStudyUseExcluded;

                _mStudyReport = new CStudyReport(CDvtmsData.sGetDBaseConnection());
                _mStudyInfo = new CStudyInfo(CDvtmsData.sGetDBaseConnection());
                _mPatientInfo = new CPatientInfo(CDvtmsData.sGetDBaseConnection());
                _mAnalysisInfo = new CRecAnalysis(CDvtmsData.sGetDBaseConnection());
                _mRecordList = new List<CSqlDataTableRow>();

                _mSelectedTableSet = new CStringSet();
                _mSelectedStripSet = new CStringSet();

                if (_mStudyReport != null && _mStudyInfo != null && _mPatientInfo != null && _mAnalysisInfo != null 
                    && _mSelectedTableSet != null && _mSelectedStripSet != null && _mRecordList != null)
                {
                    if( _mStudyInfo.mbDoSqlSelectIndex( AStudyIndex, _mStudyInfo.mGetValidMask(true) ))
                    {
                        if( _mPatientInfo.mbDoSqlSelectIndex(_mStudyInfo._mPatient_IX, _mPatientInfo.mGetValidMask(true)))
                        {
                            UInt64 loadMask = _mAnalysisInfo.mGetValidMask(true);
                            UInt64 whereMask = CSqlDataTableRow.sGetMask((UInt16)DRecAnalysisVars.Study_IX);

                            _mAnalysisInfo._mStudy_IX = _mStudyInfo.mIndex_KEY;

                            bOk = _mAnalysisInfo.mbDoSqlSelectList(out bError, out _mAnalysisList, loadMask, whereMask, true, "");   // just load all

                            if( bOk )
                            {
                                bOk = mbLoadRecords(out bLimitReached);
                            }
                        }
                    }
                }

                if (bOk && CProgram.sbEnterProgUserArea(true))
                {
                    InitializeComponent();

                    if (FormEventBoard._sbForceAnonymize)
                    {
                        checkBoxAnonymize.Checked = true;
                        checkBoxAnonymize.Enabled = false;
                    }

                    Width = 1400;   // Nicer size then minimum 1240

                    string strTitle = "";
                    if( AbEditReport == false)
                    {
  //                      panelStudyPeriod.Height = 310;
                        dataGridView.Columns[0].Visible = false;
                        dataGridView.Columns[1].Visible = false;
                        strTitle = "Previous Event Findings In Study " + _mStudyIndex.ToString();
                    }
                    else
                    {
                        strTitle = "Create Report of Analyzed Events for Study " + _mStudyIndex.ToString();
                    }
                    Text = CProgram.sMakeProgTitle(strTitle, false, true);

                    splitContainerFindingsConclusion.Panel1.Enabled = AbEditReport;
                    panelStudyPeriod.Visible = true;
                    //nelStudyReport1.Visible = AbEditReport;
                    //panelStudyReport1.Enabled = AbEditReport;
                    //panelStudyButtons.Visible = AbEditReport;
                    panelSelect.Visible = true; // false; // AbEditReport;

                    checkBoxReducePdf.Visible = CLicKeyDev.sbDeviceIsProgrammer();
                    checkBoxQCD.Enabled = FormEventBoard._sbQCdEnabled;
                    labelUseExluded.Text = _mbUseExcluded ? "+Excluded" : "";

                    if( bLimitReached )
                    {
                        labelRecordInfo.BackColor = Color.Orange;
                    }
                    mSetStudy();
                    mSetList();
                    mSetReport();
                    mUpdateStats();
                    labelRecordInfo.Text = _mRecordsInfo;
                    try
                    {
                        dataGridView.Sort(dataGridView.Columns[(int)DFindingsTableVars.RecordET], ListSortDirection.Ascending);
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException("Sort table", ex);
                    }
                    labelPdfNr.Text = "";

                    checkBoxPlot2ch.Checked = _sbPlot2Ch;
                    checkBoxBlackWhite.Checked = _sbBlackWhite;
                }
                else
                {
                    Close();
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("PreviousFindingsForm() failed to init", ex);
            }

        }
        private void buttonReloadList_Click(object sender, EventArgs e)
        {
            try
            {
               
                UInt64 loadMask = _mAnalysisInfo.mGetValidMask(true);
                UInt64 whereMask = CSqlDataTableRow.sGetMask((UInt16)DRecAnalysisVars.Study_IX);
                bool bError;
                bool bLimitReached;

                _mAnalysisInfo._mStudy_IX = _mStudyInfo.mIndex_KEY;

                _mAnalysisInfo.mbDoSqlSelectList(out bError, out _mAnalysisList, loadMask, whereMask, true, "");   // just load all

                mbLoadRecords(out bLimitReached);
                if (bLimitReached)
                {
                    labelRecordInfo.BackColor = Color.Orange;
                }

                labelRecordInfo.Text = _mRecordsInfo;

                mSetList();
                mUpdateStats();


                dataGridView.Sort(dataGridView.Columns[(int)DFindingsTableVars.RecordET], ListSortDirection.Ascending);
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Sort table", ex);
            }
            labelPdfNr.Text = "";

        }

        private void mShowAmplRange()
        {
            float range = CRecordMit.sGetMaxAmplitudeRange();

            string s = "";

            if( CRecordMit.sbLimitAmplitudeRange && range < 1e9)
            {
                s = range.ToString("0.0") + " mV";
            }
            else
            {
                s = "Auto";
            }
            buttonAmplRange.Text = s + "\nAmplR";
        }

        private void mSetAmplRange()
        {
            float range = CRecordMit.sGetMaxAmplitudeRange();
            if (CProgram.sbReqFloat("Print ECG Aplitude Range", "Max Amplitude Range (0=off)", ref range, "0.0", "mV", 0, 1e9F))
            {
                if (range < 0.5F || range >= 1e8F)
                {
                    CRecordMit.sbLimitAmplitudeRange = false;
                }
                else
                {
                    CRecordMit.sbLimitAmplitudeRange = true;
                    CRecordMit.sLimitAmplitudeRange = range;
                }
            }
            mShowAmplRange();
        }

        private void mCheckMinMax(UInt16 AN, float AMin, float AMax, ref float ArMin, ref float ArMax)
        {
            if (AN > 0 && AMin <= AMax)
            {
                if (ArMin > AMin) ArMin = AMin;
                if (ArMax < AMax) ArMax = AMax;
            }
        }
  /*      private void mCheckMinMax(CRecAnalysis AAnalysis, ref float ArMin, ref float ArMax)
        {
            if (AAnalysis != null && AAnalysis._mMeasuredHrN > 0)
            {
                if (ArMin > AAnalysis._mMeasuredHrMin) ArMin = AAnalysis._mMeasuredHrMin;
                if (ArMax < AAnalysis._mMeasuredHrMax) ArMax = AAnalysis._mMeasuredHrMax;
            }
        }
*/
        public CRecAnalysis mGetAnalysis( UInt32 AAnalysisIX)
        {
            CRecAnalysis rec = null;

            if( _mAnalysisList != null && _mAnalysisInfo != null )
            {
                rec = _mAnalysisInfo.mGetNodeFromList(_mAnalysisList, AAnalysisIX) as CRecAnalysis;
            }

            return rec;
        }

        public string mMakeEventNrString(UInt16 AEventNr)
        {
            string s = "";

            if( AEventNr > 0 )
            {
                s = AEventNr.ToString();

                int len = s.Length;

                if( len < 5 )
                {
                    string t = new string(' ', 5 - len);
                    s = t + s;
                }
            }
            return s;
        }

        private void mSetList()
        {
            dataGridView.Rows.Clear();

            CRecordMit rec = new CRecordMit(CDvtmsData.sGetDBaseConnection());

            if (rec != null && _mAnalysisList != null && _mStudyInfo != null && _mStudyReport != null && CDvtmsData._sEnumListFindingsClass != null)
            {
                CRecAnalysis selectedRow = null;
                int n = 0;
                UInt64 patMask = _mPatientInfo != null ? _mPatientInfo.mGetValidMask(true) : 0;
                UInt64 recMask = rec.mMaskValid;
                CSqlDBaseConnection dBase = CDvtmsData.sGetDBaseConnection();

                bool bLog = true;
                bool bFirstList = false; // _mStudyReport._mReportTableSet.mbIsEmpty() && _mStudyReportIndex == 0;

                if (bLog)
                {
                    string strLog = "IncludeTable, IncludeStrip, ReportDate, ReportNr, EventNr, EventDateTime, EventMA, AnalysisNr, AnalysisDate, Technician,"  +
        " Classification, Rhythm, Findings, Symptoms, HrMin, HrMean, HrMax, N, AnalysisIndex, IsDay, FindingsClass, RecordIX, RecordET, typeET";

                    CProgram.sLogLine(strLog);

                }

                dataGridView.Sort(dataGridView.Columns[(int)DFindingsTableVars.RecordET], ListSortDirection.Ascending); // default sort by event time

                foreach (CRecAnalysis row in _mAnalysisList)
                {
                    rec = mFindRecord(row._mRecording_IX);
                        
                    bool bRec = rec != null && rec.mIndex_KEY > 0;

                    if( bRec && false == CRecordMit.sbIsStateAnalyzed( rec.mRecState, _mbUseExcluded))
                    {
                        bRec = false;
                        int subNr = rec.mSeqNrInStudy;

                    }
                    if (bRec)
                    {
                        DateTime eventDT = bRec ? rec.mGetDeviceTime(rec.mGetEventUTC()) : DateTime.MinValue;

                        string analysisStr = row.mIndex_KEY.ToString();
                        bool bAddReport = bFirstList || _mStudyReport._mReportTableSet.mbContainsValue(analysisStr);
                        bool bAddStrip = bFirstList || _mStudyReport._mReportStripSet.mbContainsValue(analysisStr);
                        string reportDate = row._mReportedUTC > DateTime.MinValue ? CProgram.sDateToString(CProgram.sDateTimeToLocal(row._mReportedUTC)) : "";
                        string reportNr = row._mReportNr == 0 ? "" : row._mReportNr.ToString();
                        //string eventNr = bRec ? rec.mSeqNrInStudy.ToString() : "R#" + row._mRecording_IX.ToString();
                        string eventNr = mMakeEventNrString(bRec ? rec.mSeqNrInStudy : (UInt16)0);

                        string maCode = mMaCodeString(row._mbManualEvent);

                        string eventDate = bRec ? CProgram.sDateTimeToString(eventDT) : "-";
                        string analysisNr = row._mSeqInRecording.ToString();
                        string analysisDate = row._mMeasuredUTC > DateTime.MinValue ? CProgram.sDateToString(CProgram.sDateTimeToLocal(row._mMeasuredUTC)) : "";
                        string technician = CDvtmsData.sFindUserInitial(row._mTechnician_IX);

                        string classification = "";
                        string rhythm = row._mAnalysisRhythmSet == null || row._mAnalysisRhythmSet.mbIsEmpty() ? "" : row._mAnalysisRhythmSet.mGetShowSet("+");
                        string findings = row._mAnalysisRemark == null ? "" : row._mAnalysisRemark;
                        string symptoms = row._mSymptomsRemark;
                        string hrMin = "";
                        string hrMax = "";
                        string hrMean = "";
                        string nStr = "";
                        string recordIX = "";
                        string recState = bRec ? CRecordMit.sGetRecStateUserAbreviation(rec.mRecState) : "";
                        double dblET = bRec ? eventDT.ToOADate() : 0.0;
                        string eventType = bRec ? rec.mEventTypeString : "?";
                        //                    UInt16 nHr = 0;

                        bool bIsDay = mbIsDay(eventDT);
                        string findingsClass = "";

                        if (CDvtmsData._sEnumListFindingsClass != null && row._mAnalysisClassSet.mbIsNotEmpty())
                        {
                            findingsClass = row._mAnalysisClassSet.mGetValue(0);
                            if (findingsClass != null && findingsClass.Length > 0)
                            {
                                classification = CDvtmsData._sEnumListFindingsClass.mGetLabel(findingsClass);
                            }

                        }

                        if (row._mMeasuredHrN > 0)
                        {
                            nStr = row._mMeasuredHrN.ToString();
                            hrMean = row._mMeasuredHrMean.ToString("0");
                            if (row._mMeasuredHrN > 1)
                            {
                                hrMin = row._mMeasuredHrMin.ToString("0");
                                hrMax = row._mMeasuredHrMax.ToString("0");
                            }
                        }

                        recordIX = row._mRecording_IX.ToString();
                        if (bLog)
                        {
                            string strLog = "AR" + n.ToString() + ": "
                                + bAddReport + ", " + bAddStrip + ", " + reportDate + ", " + reportNr + ", " + eventNr + ", " + eventDate
                                + ", " + maCode + ", " + analysisNr + ", " + analysisDate + ", " + technician + ", " + classification
                                + ", " + rhythm + ", " + findings + ", " + symptoms + ", " + hrMin + ", " + hrMean + ", " + hrMax + ", " +
                                nStr + ", " + analysisStr + ", " + bIsDay + ", " + findingsClass + ", " + recordIX + ", " + dblET + ", " + eventType;
                            CProgram.sLogLine(strLog);

                        }

                        dataGridView.Rows.Add(bAddReport, bAddStrip, reportDate, reportNr, eventNr, recState, eventDate, maCode, analysisNr, analysisDate,
                            technician, classification, rhythm, findings, symptoms, hrMin, hrMean, hrMax, nStr, analysisStr, bIsDay,
                            findingsClass, recordIX, dblET, eventType);

                        if (_mAnalysisIndex > 0 && row.mIndex_KEY == _mAnalysisIndex)
                        {
                            selectedRow = row;
                            DataGridViewRow gridRow = dataGridView.Rows[n];

                            if (gridRow != null)
                            {
                                dataGridView.CurrentCell = gridRow.Cells[0];  // set cursor back to current
                            }
                        }
                        ++n;
                    }
                }
                //                labelListStrips.Text = n.ToString();
                if (selectedRow == null)
                {
                    //                  _mSelectedIndex = 0;
                }
            }
        }

        private void mUpdateStats()
        {
            try
            {
                string recTableSet = "";

                _mSelectedTableN = _mTotalN = _mSelectedStripN = _mSelectedTachy = _mTotalTachy = 0;
                _mSelectedManN = _mTotalManN = _mSelectedNorm = _mTotalNorm = _mSelectedAbN = _mTotalAbN =_mTotalExtra = _mSelectedQcN = _mTotalQCN = 0;
                _mSelectedBrady = _mTotalBrady = _mSelectedPause = _mTotalPause = _mSelectedAfib = _mTotalAfib = _mSelectedOther = _mTotalOther = 0;
                _mSelectedAFl = _mSelectedPAC = _mSelectedPVC = _mSelectedPace = _mSelectedVT = _mSelectedBL = 0;
                _mTotalAFl = _mTotalPAC = _mTotalPVC = _mTotalPace = _mTotalVT = _mTotalBL = 0;
                _mSelectedHrMin = _mTotalHrMin = 1e6F;
                _mSelectedHrMax = _mTotalHrMax = -1e6F;
                _mSelectedHrMean = _mTotalHrMean = 0.0F;
                _mSelectedHrCount = _mTotalHrCount = 0;

                _mSelectedHrMinIX = _mSelectedHrMaxIX = 0;
                _mTachyHrMin = _mBradyHrMin = _mPauseHrMin = _mAfibHrMin = _mOtherHrMin = _mNormHrMin = _mAbNHrMin = 1e6F;
                _mAflHrMin = _mPacHrMin = _mPvcHrMin = _mPaceHrMin = _mVtHrMin = _mBlHrMin = 1e6F;
                _mTachyHrMax = _mBradyHrMax = _mPauseHrMax = _mAfibHrMax = _mOtherHrMax = _mNormHrMax = _mAbNHrMax = -1e6F;
                _mAflHrMax = _mPacHrMax = _mPvcHrMax = _mPaceHrMax = _mVtHrMax = _mBlHrMax = -1e6F;

                _mAutoActivation = _mManualActivation = _mDayActivation = _mNightActivation = 0;

                _mSelectedTableSet.mClear();
                _mSelectedStripSet.mClear();

                _mStartPeriodDT = DateTime.SpecifyKind(DateTime.MaxValue, DateTimeKind.Local);

                _mEndPeriodDT = DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Local);

                if (_mAnalysisList != null && _mAnalysisList.Count > 0 && dataGridView.Rows != null && dataGridView.Rows.Count > 0 && CDvtmsData._sEnumListFindingsClass != null)
                {
                    bool bTable, bStrip, bMan, bQC;
                    int nRow = dataGridView.Rows.Count;
                    //string tachy = "Tachy";
                    //string brady = "Brady";
                    //string pause = "Pause";
                    //string afib1 = "AF";
                    string afib2 = "Afib";
                    string classification;
                    string analysisStr;
                    string qcd = CRecordMit.sGetRecStateUserAbreviation((UInt16)DRecState.QCd);

                    UInt32 analysisIX;
                    UInt32 recordIX;
                    bool bIsDay;
                    //CRecAnalysis selectedAnalysis;
                    DataGridViewRow gridRow;
                    UInt16 nHr;
                    UInt16 hrMin, hrMax, hrMean;


                    for (int i = 0; i < nRow; ++i)
                    {
                        //selectedAnalysis = null;
                        analysisStr = "";
                        analysisIX = 0;
                        nHr = 0;
                        hrMin = 10000;
                        hrMax = 0;
                        hrMean = 0;

                        gridRow = dataGridView.Rows[i];
                        if (gridRow != null && gridRow.Cells.Count > (int)DFindingsTableVars.AnalysisIndex)
                        {
                            analysisStr = gridRow.Cells[(int)DFindingsTableVars.AnalysisIndex].Value.ToString();

                            if (analysisStr.Length > 0)
                            {
                                UInt32.TryParse(analysisStr, out analysisIX);
                            }

                            if (gridRow.Cells[(int)DFindingsTableVars.N].Value != null)
                            {
                                string s = gridRow.Cells[(int)DFindingsTableVars.N].Value.ToString();
                                if (s.Length > 0)
                                {
                                    UInt16.TryParse(s, out nHr);
                                }
                            }

                         }
                        if (analysisIX > 0)
                        {
                            ++_mTotalN;
                            bTable = gridRow.Cells[0].Value != null && (bool)gridRow.Cells[0].Value;
                            bStrip = gridRow.Cells[1].Value != null && (bool)gridRow.Cells[1].Value;
                            bIsDay = gridRow.Cells[(int)DFindingsTableVars.IsDay].Value != null && (bool)gridRow.Cells[(int)DFindingsTableVars.IsDay].Value;
                            bMan = (string)gridRow.Cells[(int)DFindingsTableVars.EventMA].Value == "M";
                            bQC = (string)gridRow.Cells[(int)DFindingsTableVars.RecState].Value == qcd;


                            if (bStrip) bTable = true;

                            if (bStrip) { ++_mSelectedStripN; _mSelectedStripSet.mAddValue(analysisStr); }

                            if (bMan)
                            {
                                ++_mTotalManN;
                                if (bTable) ++_mSelectedManN;
                            }
                            if (bQC)
                            {
                                ++_mTotalQCN;
                                if (bTable) ++_mSelectedQcN;
                            }

                            if (nHr > 0)
                            //                                if (selectedAnalysis._mMeasuredHrN > 0)
                            {
                                if (gridRow.Cells[(int)DFindingsTableVars.HrMean].Value != null)
                                {
                                    string s = gridRow.Cells[(int)DFindingsTableVars.HrMean].Value.ToString();
                                    if (s.Length > 0)
                                    {
                                        UInt16.TryParse(s, out hrMean);
                                    }
                                    hrMin = hrMax = hrMean;
                                }
                                if (nHr > 1)
                                {
                                    if (gridRow.Cells[(int)DFindingsTableVars.HrMin].Value != null)
                                    {
                                        string s = gridRow.Cells[(int)DFindingsTableVars.HrMin].Value.ToString();
                                        if (s.Length > 0)
                                        {
                                            UInt16.TryParse(s, out hrMin);
                                        }
                                    }
                                    if (gridRow.Cells[(int)DFindingsTableVars.HrMax].Value != null)
                                    {
                                        string s = gridRow.Cells[(int)DFindingsTableVars.HrMax].Value.ToString();
                                        if (s.Length > 0)
                                        {
                                            UInt16.TryParse(s, out hrMax);
                                        }
                                    }
                                }

                                if (_mTotalHrMin > hrMin) _mTotalHrMin = hrMin;
                                if (_mTotalHrMax < hrMax) _mTotalHrMax = hrMax;
                                _mTotalHrMean += hrMean;
                                ++_mTotalHrCount;

                                if (bTable)
                                {
                                    if (_mSelectedHrMin > hrMin) { _mSelectedHrMin = hrMin; _mSelectedHrMinIX = _mSelectedTableN; }
                                    if (_mSelectedHrMax < hrMax) { _mSelectedHrMax = hrMax; _mSelectedHrMaxIX = _mSelectedTableN; }
                                    _mSelectedHrMean += hrMean;
                                    ++_mSelectedHrCount;
                                }
                            }
                            if (bTable)
                            {
                                ++_mSelectedTableN;
                                _mSelectedTableSet.mAddValue(analysisStr);
                                recTableSet += gridRow.Cells[(int)DFindingsTableVars.EventNr].Value.ToString() + ";";


                                if (gridRow.Cells[(int)DFindingsTableVars.RecordET].Value != null)
                                {
                                    try
                                    {
                                        double dblDT = (double)gridRow.Cells[(int)DFindingsTableVars.RecordET].Value;

                                        if (dblDT > 0.01)
                                        {
                                            DateTime eventDT = DateTime.FromOADate(dblDT);
                                            if (_mStartPeriodDT > eventDT) _mStartPeriodDT = eventDT;  // determin period
                                            if (_mEndPeriodDT < eventDT) _mEndPeriodDT = eventDT;
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        int zxyx = 3;
                                    }
                                }

                            }

                            classification = gridRow.Cells[(int)DFindingsTableVars.FindingsClass].Value.ToString(); //selectedAnalysis._mAnalysisClassSet.mGetValue(0);

                            if (classification == CDvtmsData._cFindings_Tachy)
                            {
                                ++_mTotalTachy;
                                if (bTable) { ++_mSelectedTachy; mCheckMinMax(nHr, hrMin, hrMax, ref _mTachyHrMin, ref _mTachyHrMax); }
                            }
                            else if (classification == CDvtmsData._cFindings_Brady)
                            {
                                ++_mTotalBrady;
                                if (bTable) { ++_mSelectedBrady; mCheckMinMax(nHr, hrMin, hrMax, ref _mBradyHrMin, ref _mBradyHrMax); }
                            }
                            else if (classification == CDvtmsData._cFindings_Pause)
                            {
                                ++_mTotalPause;
                                if (bTable) { ++_mSelectedPause; mCheckMinMax(nHr, hrMin, hrMax, ref _mPauseHrMin, ref _mPauseHrMax); }
                            }
                            else if (classification == CDvtmsData._cFindings_AF || classification == afib2)
                            {
                                ++_mTotalAfib;
                                if (bTable) { ++_mSelectedAfib; mCheckMinMax(nHr, hrMin, hrMax, ref _mAfibHrMin, ref _mAfibHrMax); }
                            }
                            else if (classification == CDvtmsData._cFindings_AFL)
                            {
                                ++_mTotalAFl;
                                if (bTable) { ++_mSelectedAFl; mCheckMinMax(nHr, hrMin, hrMax, ref _mAflHrMin, ref _mAflHrMax); } 
                            }
                            else if (classification == CDvtmsData._cFindings_PAC)
                            {
                                ++_mTotalPAC;
                                if (bTable) { ++_mSelectedPAC; mCheckMinMax(nHr, hrMin, hrMax, ref _mPacHrMin, ref _mPacHrMax); }
                            }
                            else if (classification == CDvtmsData._cFindings_PVC)
                            {
                                ++_mTotalPVC;
                                if (bTable) { ++_mSelectedPVC; mCheckMinMax(nHr, hrMin, hrMax, ref _mPvcHrMin, ref _mPvcHrMax); }
                            }
                            else if (classification == CDvtmsData._cFindings_Pace)
                            {
                                ++_mTotalPace;
                                if (bTable) { ++_mSelectedPace; mCheckMinMax(nHr, hrMin, hrMax, ref _mPaceHrMin, ref _mPaceHrMax); }
                            }
                            else if (classification == CDvtmsData._cFindings_VT)
                            {
                                ++_mTotalVT;
                                if (bTable) { ++_mSelectedVT; mCheckMinMax(nHr, hrMin, hrMax, ref _mVtHrMin, ref _mVtHrMax); }
                            }
                            else if (classification == CDvtmsData._cFindings_BL)
                            {
                                ++_mTotalBL;
                                if (bTable) { ++_mSelectedBL; mCheckMinMax(nHr, hrMin, hrMax, ref _mBlHrMin, ref _mBlHrMax); }
                            }
                            else if (classification == CDvtmsData._cFindings_Other)
                            {
                                ++_mTotalOther;
                                if (bTable) { ++_mSelectedOther; mCheckMinMax(nHr, hrMin, hrMax, ref _mOtherHrMin, ref _mOtherHrMax); }
                            }
                            else if (classification == CDvtmsData._cFindings_Normal)
                            {
                                ++_mTotalNorm;
                                if (bTable) { ++_mSelectedNorm; mCheckMinMax(nHr, hrMin, hrMax, ref _mNormHrMin, ref _mNormHrMax); }
                            }
                            else if (classification == CDvtmsData._cFindings_AbNormal)
                            {
                                ++_mTotalAbN;
                                if (bTable) { ++_mSelectedAbN; mCheckMinMax(nHr, hrMin, hrMax, ref _mAbNHrMin, ref _mAbNHrMax); }
                            }
                            else
                            {
                                ++_mTotalExtra;  // unknown classification
                            }
                            if (bTable)
                            {
                                string ma = gridRow.Cells[(int)DFindingsTableVars.EventMA].Value == null ? "" : gridRow.Cells[(int)DFindingsTableVars.EventMA].Value.ToString();
                                if (ma == "M")
                                {
                                    ++_mManualActivation;
                                }
                                else
                                {
                                    ++_mAutoActivation;
                                }
                                if (bIsDay)
                                {
                                    ++_mDayActivation;
                                }
                                else
                                {
                                    ++_mNightActivation;
                                }
                            }
                        }
                    }
                }
                string tableText = "-";
                string stripText = "-";
                string tachyText = "-";
                string bradyText = "-";
                string pauseText = "-";
                string afibText = "-";
                string aflText = "-";
                string pacText = "-";
                string pvcText = "-";
                string paceText = "-";
                string vtText = "-";
                string blText = "-";
                string otherText = "-";
                string manText = "-";
                string qcText = "";
                string normText = "";
                string abnText = "";
                string extraText = "";

                if (_mTotalN > 0)
                {
                    tableText = (_mbEditReport ? _mSelectedTableN.ToString() + "/ " : "") + _mTotalN.ToString();
                    stripText = (_mbEditReport ? _mSelectedStripN.ToString() + "/ " : "") + _mTotalN.ToString();
                    manText = (_mbEditReport ? _mSelectedManN.ToString() + "/ " : "") + _mTotalManN.ToString();
                    qcText = (_mbEditReport ? _mSelectedQcN.ToString() + "/ " : "") + _mTotalQCN.ToString();
                    tachyText = (_mbEditReport ? _mSelectedTachy.ToString() + "/ " : "") + _mTotalTachy.ToString();
                    bradyText = (_mbEditReport ? _mSelectedBrady.ToString() + "/ " : "") + _mTotalBrady.ToString();
                    pauseText = (_mbEditReport ? _mSelectedPause.ToString() + "/ " : "") + _mTotalPause.ToString();
                    afibText = (_mbEditReport ? _mSelectedAfib.ToString() + "/ " : "") + _mTotalAfib.ToString();
                    aflText = (_mbEditReport ? _mSelectedAFl.ToString() + "/ " : "") + _mTotalAFl.ToString();
                    pacText = (_mbEditReport ? _mSelectedPAC.ToString() + "/ " : "") + _mTotalPAC.ToString();
                    pvcText = (_mbEditReport ? _mSelectedPVC.ToString() + "/ " : "") + _mTotalPVC.ToString();
                    paceText = (_mbEditReport ? _mSelectedPace.ToString() + "/ " : "") + _mTotalPace.ToString();
                    vtText = (_mbEditReport ? _mSelectedVT.ToString() + "/ " : "") + _mTotalVT.ToString();
                    blText = (_mbEditReport ? _mSelectedBL.ToString() + "/ " : "") + _mTotalBL.ToString();
                    otherText = (_mbEditReport ? _mSelectedOther.ToString() + "/ " : "") + _mTotalOther.ToString();
                    normText = (_mbEditReport ? _mSelectedNorm.ToString() + "/ " : "") + _mTotalNorm.ToString();
                    abnText = (_mbEditReport ? _mSelectedAbN.ToString() + "/ " : "") + _mTotalAbN.ToString();
                    extraText = _mTotalExtra > 0 ? _mTotalExtra.ToString() + " unknown" : "";

                    string s = recTableSet;
                }

                labelListTable.Text = tableText;
                labelListStrips.Text = stripText;
                labelListManuals.Text = manText;
                labelListQC.Text = qcText;
                labelListTachy.Text = tachyText;
                labelListBrady.Text = bradyText;
                labelListPause.Text = pauseText;
                labelListAfib.Text = afibText;
                labelListAfl.Text = aflText;
                labelListPac.Text = pacText;
                labelListPVC.Text = pvcText;
                labelListPace.Text = paceText;
                labelListVT.Text = vtText;
                labelListBL.Text = blText;
                labelListOther.Text = otherText;
                labelListNorm.Text = normText;
                labelListAbN.Text = abnText;
                labelListExtra.Text = extraText;

                labelNrAnalyzed.Text = _mTotalN.ToString();

                string hrSelected = "";
                string tbpaoSelected = "";

                if (_mTotalHrCount > 0)
                {
                    _mTotalHrMean /= _mTotalHrCount;
                }
                if (_mSelectedHrCount > 0 && _mSelectedHrMin <= _mSelectedHrMax)
                {
                    bool bShowMinMax = _mSelectedHrMin != _mSelectedHrMax;
                    _mSelectedHrMean /= _mSelectedHrCount;
                    hrSelected = _mSelectedHrCount.ToString() + " HR: ";
                    tbpaoSelected = "HR: ";
                    if (bShowMinMax)
                    {
                        hrSelected += ((int)(_mSelectedHrMin + 0.5F)).ToString() + " <= ";
                    }
                    hrSelected += ((int)(_mSelectedHrMean + 0.5F)).ToString();
                    if (bShowMinMax)
                    {
                        hrSelected += " <= " + ((int)(_mSelectedHrMax + 0.5F)).ToString();
                    }
                    bool bPrev = false;
                    if (_mSelectedTachy > 0 && _mTachyHrMin <= _mTachyHrMax)
                    {
                        tbpaoSelected += "Tachy <= " + ((int)(_mTachyHrMax + 0.5F)).ToString();
                        bPrev = true;
                    }
                    if (_mSelectedBrady > 0 && _mBradyHrMin <= _mBradyHrMax)
                    {
                        if (bPrev) tbpaoSelected += ", ";
                        bPrev = true;
                        tbpaoSelected += ((int)(_mBradyHrMin + 0.5F)).ToString() + " <= Brady";
                    }
                    if (_mSelectedPause > 0 && _mPauseHrMin <= _mPauseHrMax)
                    {
                        if (bPrev) tbpaoSelected += ", ";
                        bPrev = true;
                        tbpaoSelected += ((int)(_mPauseHrMin + 0.5F)).ToString() + " <= Pause <= " + ((int)(_mPauseHrMax + 0.5F)).ToString();
                    }
                    if (_mSelectedAfib > 0 && _mAfibHrMin <= _mAfibHrMax)
                    {
                        if (bPrev) tbpaoSelected += ", ";
                        bPrev = true;
                        tbpaoSelected += ((int)(_mAfibHrMin + 0.5F)).ToString() + " <= AF <= " + ((int)(_mAfibHrMax + 0.5F)).ToString();
                    }
 
                     if (_mSelectedOther > 0 && _mOtherHrMin <= _mOtherHrMax)
                    {
                        if (bPrev) tbpaoSelected += ", ";
                        bPrev = true;
                        tbpaoSelected += ((int)(_mOtherHrMin + 0.5F)).ToString() + " <= Other <= " + ((int)(_mOtherHrMax + 0.5F)).ToString();
                    }
                    if (_mSelectedNorm > 0 && _mNormHrMin <= _mNormHrMax)
                    {
                        if (bPrev) tbpaoSelected += ", ";
                        bPrev = true;
                        tbpaoSelected += ((int)(_mNormHrMin + 0.5F)).ToString() + " <= Normal <= " + ((int)(_mNormHrMax + 0.5F)).ToString();
                    }
                    if (_mSelectedAbN > 0 && _mAbNHrMin <= _mAbNHrMax)
                    {
                        if (bPrev) tbpaoSelected += ", ";
                        bPrev = true;
                        tbpaoSelected += ((int)(_mAbNHrMin + 0.5F)).ToString() + " <= AbNormal <= " + ((int)(_mAbNHrMax + 0.5F)).ToString();
                    }
                }
                labelHR.Text = hrSelected;
                labelTBPAO.Text = tbpaoSelected;
            }
            catch (Exception ex)
            {
                CProgram.sLogException("PreviousFindingsForm() failed to calc stat", ex);
            }
        }

        private string mPrintNTotal( uint AN, UInt32 ATotal )
        {
            string s = "-";

            if( ATotal > 0)
            {
                s = AN.ToString() + " / " + ATotal.ToString();
            }
            return s;
        }
        private void mInitPatient()
        {
            int age = _mPatientInfo != null ? _mPatientInfo.mGetAgeYears() : -1;

            bool bHidePatient = checkBoxAnonymize.Checked;

            labelFullName.Text = bHidePatient ? "--Anonymized--" : (_mPatientInfo != null ? _mPatientInfo.mGetFullName() : "");
            labelDoB.Text = bHidePatient ? "" 
                : (_mPatientInfo != null && age >= 0 ? CProgram.sDateToString(_mPatientInfo._mPatientDateOfBirth.mDecryptToDate()) : "");

            labelAge.Text = _mPatientInfo != null && age > 0 ? age.ToString() : "";
            labelGender.Text = _mPatientInfo != null ? CPatientInfo.sGetGenderString((DGender)_mPatientInfo._mPatientGender_IX) : "";

        }

        private void mSetStudy()
        {
            if (_mStudyInfo != null)
            {
                labelStudyIndex.Text = _mStudyInfo.mIndex_KEY.ToString();

                string s = _mStudyInfo.mGetLabelNoteMark();

                labelStudyNote.Text = s;

                if (s.Length > 0)
                {
                    s = _mStudyInfo.mGetRemarkAndNote();

                    new ToolTip().SetToolTip(labelStudyNote, s);
                }

                mInitPatient();

                // study nr reports is noe used for all pdf doc reports

                if (_mStudyReport.mIndex_KEY == 0)
                {
                    labelReportResult.Text = ""; // "Report: new ??";
                }
                else
                {
                    labelReportResult.Text = "Report: #" + _mStudyReport.mIndex_KEY.ToString();
                }

                int n = _mRecordList == null ? 0 : _mRecordList.Count;
                CSqlDBaseConnection db = CDvtmsData.sGetDBaseConnection();

                UInt32 dbLimit = db == null ? 1 : db.mGetSqlRowLimit();
                if (n >= dbLimit)
                {
                    labelRecordInfo.BackColor = Color.Orange;
                }
            }
        }

        private bool mbIsDay( DateTime ADateTime )
        {
            bool bIsDay = false;

            if( ADateTime.Hour >= 6 ) // >= 6:00 && <= 23:59
            {
                if (ADateTime.Hour <= 23 )
                {
                    bIsDay = true;
                }
                else if(ADateTime.Hour == 23)
                {
                    bIsDay = ADateTime.Minute <= 59;
                }
            }
            return bIsDay;
        }

        public string mMaCodeString( bool AbManual )
        {
            return AbManual ? "M" : "A";
        }

        public bool mbLoadRecords(out bool ArbLimitReached)
        {
            bool bOk = false;
            bool bLimitReached = false;

            UInt32 dbLimit = 0;

            _mRecordsInfo = "? records";

            if (false == CStudyInfo._sbStudyCountRecStates)
            {
                _mRecordsInfo = "Record state not counted!";
                if (_mStudyInfo._mNrRecords >= dbLimit)
                {
                    _mRecordsInfo +=  "  " + _mStudyInfo._mNrRecords  + ">=" + dbLimit.ToString();
                    bLimitReached = true;
                }
            }
            else
            {
                //            string recInfo = _mInfo.mGetRecStatesAll(out nTotal, out nToDo, false);


                CSqlDBaseConnection db = CDvtmsData.sGetDBaseConnection();

                if (db != null)
                {
                    dbLimit = db.mGetSqlRowLimit();
                }

                CRecordMit recBase = new CRecordMit(db);

                if (recBase != null && _mRecordList != null)
                {
                    bool bError;

                    recBase.mStudy_IX = _mStudyIndex;


                    bOk = recBase.mbDoSqlSelectList(out bError, out _mRecordList, recBase.mGetValidMask(true), CRecordMit.sGetMask((UInt16)DRecordMitVars.Study_IX), true, "");

                    if (bOk)
                    {
                        int nStates = (int)DRecState.NrRecStates;
                        UInt32[] countStates = new UInt32[nStates];
                        Int32 nTotal = _mRecordList.Count;
                        Int32 nToDo = 0;
                        Int32 nAnalyzed = 0;
                        Int32 nOther = 0;
                        UInt16 state;


                        for (int i = 0; i < nStates; ++i)
                        {
                            countStates[i] = 0;
                        }

                        foreach (CRecordMit rec in _mRecordList)
                        {
                            state = rec.mRecState;
                            if (state >= nStates)
                            {
                                state = (UInt16)DRecState.Unknown;
                            }
                            ++countStates[state];

                            if (CRecordMit.sbIsStateAnalyzed(state, _mbUseExcluded))
                            {
                                ++nAnalyzed;
                            }
                            else if (CRecordMit.sbIsStateToDo(state))
                            {
                                ++nToDo;
                            }
                            else
                            {
                                ++nOther;
                            }
                        }
                        _mRecordsInfo = nTotal.ToString();

                        if (nTotal >= dbLimit)
                        {
                            _mRecordsInfo += ">=" + dbLimit.ToString();
                            bLimitReached = true;
                        }
                        _mRecordsInfo += " records: " + nToDo.ToString() + " ToDo, " + nAnalyzed.ToString()
                            + (_mbUseExcluded ? " Analyzed+excl (" : " Analyzed (");

                        bool bShowAll = CLicKeyDev.sbDeviceIsProgrammer();
                        bool bAddKomma = false;

                        for (UInt16 i = 0; i < nStates; ++i)
                        {
                            if (bShowAll || countStates[i] > 0)
                            {
                                if (bAddKomma)
                                {
                                    _mRecordsInfo += ", ";
                                }
                                _mRecordsInfo += countStates[i].ToString() + " " + CRecordMit.sGetRecStateUserString(i);
                                bAddKomma = true;
                            }
                        }
                        _mRecordsInfo += ")";
                    }
                }
            }
            ArbLimitReached = bLimitReached;
            return bOk;
        }

        public void mDumpRecAnalyzeInfo()
        {
            if (_mRecordList != null)
            {
                string cr = "\r\n";
                string tab = "\t";
                string text = _mRecordList.Count.ToString() + " Records for Study " + _mStudyIndex.ToString() + " at " + CProgram.sDateTimeToString(DateTime.Now) + cr;
                string line;
                string remark;
                DateTime dtReceived, dtEvent, dtChanged, dtStart;

                text += labelRecordInfo.Text + cr + cr;
                text += "Record" + tab + "Received" + tab + "Device" + tab + "DevID" + tab
                        + "Import" + tab + "S.subNr" + tab + "State"
                        + tab + "Event" + tab + "Zone(min)" + tab + "Type"
                        + tab + "Start" + tab + "Duration(sec)" + tab + "Channels"
                        + tab + "remark" + tab + "changed" + tab + "by" 
                        + tab + "Nann"+ tab + "Analysis" + tab + "Classification" + tab + "Rhythm" 
                        + tab + "Changed" + tab + "By"
                        + cr;
                int nUsedAnn = 0;
                foreach (CRecordMit rec in _mRecordList)
                {
                    dtReceived = CProgram.sDateTimeToLocal(rec.mReceivedUTC);

                    dtEvent = rec.mEventUTC.AddMinutes(rec.mTimeZoneOffsetMin);
                    dtChanged = CProgram.sDateTimeToLocal(rec.mChangedUTC);
                    dtStart = rec.mGetDeviceTime(rec.mBaseUTC);
                    remark = rec.mRecRemark.mDecrypt();
                    if (remark != null)
                    {
                        int pos = remark.IndexOf('\n');
                        if (pos > 0)
                        {
                            remark = remark.Substring(0, pos);
                        }
                    }
                    line = rec.mIndex_KEY.ToString() + tab + CProgram.sDateTimeToString(dtReceived) + tab + rec.mDeviceID + tab + rec.mDevice_IX.ToString()
                        + tab + ((DImportMethod)rec.mImportMethod).ToString() + tab + rec.mSeqNrInStudy.ToString() + tab + CRecordMit.sGetRecStateUserString( rec.mRecState)
                        + tab + CProgram.sDateTimeToString(dtEvent) + tab + rec.mTimeZoneOffsetMin.ToString() + tab + rec.mEventTypeString
                        + tab + CProgram.sDateTimeToString(dtStart) + tab + rec.mRecDurationSec.ToString("0.000") + tab + rec.mNrSignals.ToString()
                        + tab + "\"" + remark + "\"" + tab + CProgram.sDateTimeToString(dtChanged) + tab + CDvtmsData.sFindUserInitial(rec.mChangedBy_IX);

                    if (_mAnalysisList != null)
                    {
                        int nAnn = 0;
                        string annLine = "";

                        foreach (CRecAnalysis row in _mAnalysisList)
                        {
                            if( row._mRecording_IX == rec.mIndex_KEY)
                            {
                                ++nAnn;
                                if( 1 == nAnn)
                                {
                                    string rhythm = row._mAnalysisRhythmSet == null || row._mAnalysisRhythmSet.mbIsEmpty() ? "" : row._mAnalysisRhythmSet.mGetShowSet("+");
                                    string classification = "";
                                    string findingsClass = "";

                                    if (CDvtmsData._sEnumListFindingsClass != null && row._mAnalysisClassSet.mbIsNotEmpty())
                                    {
                                        findingsClass = row._mAnalysisClassSet.mGetValue(0);
                                        if (findingsClass != null && findingsClass.Length > 0)
                                        {
                                            classification = CDvtmsData._sEnumListFindingsClass.mGetLabel(findingsClass);
                                        }
                                    }
                                    annLine = tab + row.mIndex_KEY.ToString() + tab + "\"" + classification + "\"" + tab + "\"" + rhythm + "\""
                                            + tab + CProgram.sDateTimeToString(CProgram.sDateTimeToLocal(row.mChangedUTC)) + tab + CDvtmsData.sFindUserInitial(row.mChangedBy_IX);
                                    ++nUsedAnn;
                                }
                            }
                        }
                        line += tab + nAnn.ToString() + annLine;
                    }
                    text += line + cr;
                }
                text += nUsedAnn.ToString() + " records with an analysis" + cr;
                if (_mAnalysisList != null)
                {
                    CRecordMit rec;
                    UInt32 nLoaded = 0;
                    UInt32 nValid = 0;

                    text += cr + "Analysis" + tab + "Valid" + tab + "NR" 
                        + tab + "Event" + tab + "Zone(min)" + tab + "MA"
                        + tab + "Date" + tab + "Tech"
                        + tab + "Class" + tab + "Rhythm"
                        + tab + "HRmin" + tab + "HrMean" + tab + "HRmax" + tab + "HRn"
                        + tab + "Changed" + tab + "By"
                        + tab + "Record" + tab + "State"
                        + tab + "Start" + tab + "(sec)" + tab + "Channels"
                        + tab + "EventType"
                        + tab + "Changed" + tab + "By"
                        + cr;

                    foreach (CRecAnalysis row in _mAnalysisList)
                    {
                        ++nLoaded;
                        rec = mFindRecord(row._mRecording_IX);

                        bool bRec = rec != null && rec.mIndex_KEY > 0;

                        if (bRec && false == CRecordMit.sbIsStateAnalyzed(rec.mRecState, _mbUseExcluded))
                        {
                            bRec = false;
                            int subNr = rec.mSeqNrInStudy;

                        }
                        string maCode = mMaCodeString(row._mbManualEvent);
                        string analysisDate = row._mMeasuredUTC > DateTime.MinValue ? CProgram.sDateToString(CProgram.sDateTimeToLocal(row._mMeasuredUTC)) : "";
                        string classification = "";
                        string rhythm = row._mAnalysisRhythmSet == null || row._mAnalysisRhythmSet.mbIsEmpty() ? "" : row._mAnalysisRhythmSet.mGetShowSet("+");
                        string findingsClass = "";
                        string hrMin = "";
                        string hrMax = "";
                        string hrMean = "";
                        string nStr = "";

                        if (CDvtmsData._sEnumListFindingsClass != null && row._mAnalysisClassSet.mbIsNotEmpty())
                        {
                            findingsClass = row._mAnalysisClassSet.mGetValue(0);
                            if (findingsClass != null && findingsClass.Length > 0)
                            {
                                classification = CDvtmsData._sEnumListFindingsClass.mGetLabel(findingsClass);
                            }
                        }
                        if (row._mMeasuredHrN > 0)
                        {
                            nStr = row._mMeasuredHrN.ToString();
                            hrMean = row._mMeasuredHrMean.ToString("0");
                            if (row._mMeasuredHrN > 1)
                            {
                                hrMin = row._mMeasuredHrMin.ToString("0");
                                hrMax = row._mMeasuredHrMax.ToString("0");
                            }
                        }
                        if (rec == null )
                        {
                            line = row.mIndex_KEY.ToString() + tab + "0" + tab
                                + tab + tab + tab
                                + tab + analysisDate + tab + CDvtmsData.sFindUserInitial(row._mTechnician_IX)
                                + tab + "\"" + classification + "\"" + tab + "\"" + rhythm + "\""
                                + tab + hrMin + tab + hrMean + tab + hrMax + tab + nStr
                                + tab + CProgram.sDateTimeToString(CProgram.sDateTimeToLocal(row.mChangedUTC)) + tab + CDvtmsData.sFindUserInitial(row.mChangedBy_IX);
                        }
                        else
                        {
                            ++nValid;

                            dtEvent = rec.mEventUTC.AddMinutes(rec.mTimeZoneOffsetMin);
                            dtChanged = CProgram.sDateTimeToLocal(rec.mChangedUTC);
                            dtStart = rec.mGetDeviceTime(rec.mBaseUTC);

                            line = row.mIndex_KEY.ToString() + tab + (bRec ? "1": "0") + tab + rec.mSeqNrInStudy.ToString()
                                + tab + CProgram.sDateTimeToString(dtEvent) + tab + rec.mTimeZoneOffsetMin.ToString() + tab + maCode
                                + tab + analysisDate + tab + CDvtmsData.sFindUserInitial(row._mTechnician_IX)
                                + tab + "\"" + classification + "\"" + tab + "\"" + rhythm + "\""
                                + tab + hrMin + tab + hrMean + tab + hrMax + tab + nStr
                                + tab + CProgram.sDateTimeToString(CProgram.sDateTimeToLocal(row.mChangedUTC)) + tab + CDvtmsData.sFindUserInitial(row.mChangedBy_IX)
                                + tab + rec.mIndex_KEY.ToString() + tab + CRecordMit.sGetRecStateUserString(rec.mRecState)
                                + tab + CProgram.sDateTimeToString(dtStart) + tab + rec.mRecDurationSec.ToString("0.000") + tab + rec.mNrSignals.ToString()
                                + tab + rec.mEventTypeString
                                + tab + CProgram.sDateTimeToString(dtChanged) + tab + CDvtmsData.sFindUserInitial(rec.mChangedBy_IX);
                        }
                        text += line + cr;
                    }
                    text += nValid.ToString() + " used Analisis, " + nLoaded.ToString() + " loaded" + cr;
                }
                try
                {
                    string studyPath;

                    if (CDvtmsData.sbCreateStudyRecorderDir(out studyPath, _mStudyIndex))
                    {
                        string fileName = "RA_S" + _mStudyIndex.ToString("00000000") + "_" + CProgram.sDateTimeToYMDHMS(DateTime.Now) + ".txt";
                        string filePath = Path.Combine(studyPath, fileName);

                        using (StreamWriter sw = new StreamWriter(filePath))
                        {
                            if (sw != null)
                            {
                                sw.Write(text);
                                sw.Close();
                            }
                        }

                    }

                }
                catch ( Exception ex )
                {
                    CProgram.sLogException("failed write RecAnn dump S" + _mStudyIndex.ToString(), ex);
                }

                if (CProgram.sbAskOkCancel("Dump Study " + _mStudyIndex.ToString() + " records", "dump to clipboard?"))
                {
                    Clipboard.SetText(text);
                }

            }
        }

        public CRecordMit mFindRecord( UInt32 ARecordIx)
        {
            CRecordMit recFound = null;

            foreach(CRecordMit rec in _mRecordList)
            {
                if( rec.mIndex_KEY == ARecordIx)
                {
                    recFound = rec;
                    break;
                }
            }

            return recFound;
        }
        private void mSetReport()
        {
            if( _mStudyReport != null)
            {
                textBoxSummary.Text = _mStudyReport._mSummary.mDecrypt();
                textBoxConclusion.Text = _mStudyReport._mConclusion.mDecrypt();
            }
        }

        public bool mbSaveReport(string AExtraText)
        {
            bool bOk = false;
            string resultText = "";

            mUpdateStats();    // calculates sets

            if( _mStudyReport != null && CProgram.sbEnterProgUserArea(true))
            {
                _mStudyReport._mStudyIX = _mStudyInfo.mIndex_KEY;

                _mStudyReport._mReportTableSet.mSetSet(_mSelectedTableSet);    // show in table: analysis index numbers [- sample flags]
                _mStudyReport._mReportStripSet.mSetSet(_mSelectedStripSet);    // show in strips: analysis index numbers [- sample flags]

                _mStudyReport._mReportSectionMask = 0;
                if (radioButtonDay.Checked) { _mStudyReport._mReportSectionMask = 1 << (UInt16)DReportSection.Daily; }
                if (radioButtonWeek.Checked) { _mStudyReport._mReportSectionMask = 1 << (UInt16)DReportSection.Weekly; }
                if (radioButtonEOS.Checked) { _mStudyReport._mReportSectionMask = 1 << (UInt16)DReportSection.EndOfStudy; }
                if (radioButtonHour.Checked) { _mStudyReport._mReportSectionMask = 1 << (UInt16)DReportSection.Hour; }
                if (radioButtonPeriod.Checked) { _mStudyReport._mReportSectionMask = 1 << (UInt16)DReportSection.Period; }

                _mStudyReport._mSummary.mbEncrypt(textBoxSummary.Text);
                _mStudyReport._mConclusion.mbEncrypt(textBoxConclusion.Text);

                _mStudyReport._mReportUTC = DateTime.UtcNow;
                _mStudyReport._mReportedByIX = 0;
                //_mStudyReport._m 

                if( _mStudyReport._mReportNr == 0 )
                {
                    _mStudyReport._mReportNr = ++_mStudyInfo._mNrReports;

                    if( _mStudyReport.mbDoSqlInsert())
                    {                       
                        if( _mStudyInfo.mbDoSqlUpdate(CSqlDataTableRow.sGetMask((UInt16)DStudyInfoVars.NrReports), true))
                        {
                            resultText = "Inserted.";
                            bOk = true;
                        }
                        else
                        {
                            resultText = "Inserted, failed update study.";
                        }
                    }
                    else
                    {
                        resultText = "Failed insert!";
                    }
                }
                else
                {
                    if (_mStudyReport.mbDoSqlUpdate(_mStudyReport.mGetValidMask(true), true))
                    {
                        resultText = "Updated.";
                        bOk = true;
                    }
                    else
                    {
                        resultText = "Failed update!";
                    }
                }
                resultText = "Study Report: #" + _mStudyReport.mIndex_KEY.ToString() + ", " + resultText;
                if( AExtraText != null && AExtraText.Length > 0)
                {
                    resultText += ", " + AExtraText;
                }
            }
            labelReportResult.Text = resultText;
            return bOk;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            mbSaveReport("");
        }

        private void buttonSaveReport_Click(object sender, EventArgs e)
        {
            mbSaveReport("");
        }

        private void buttonClearConclusion_Click(object sender, EventArgs e)
        {
            textBoxConclusion.Text = "";
        }

        private void buttonClearSummery_Click(object sender, EventArgs e)
        {
            textBoxSummary.Text = "";
        }

        private void label13_Click_1(object sender, EventArgs e)
        {

        }

        private void textBoxSummary_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttonMCT_Click(object sender, EventArgs e)
        {
            if (_mStudyInfo != null && _mStudyInfo.mIndex_KEY > 0 )
            {
                bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;
                bool bSelectFiles = (Control.ModifierKeys & Keys.Alt) == Keys.Alt;

                if (bCtrl) bCtrl = CProgram.sbAskYesNo("MCT Full debug", "Do full debug logging?");

                DTriageTimerMode prevMode = FormEventBoard.sSetTriageTimerEntering(DTriageTimerMode.DialogSecond, "MCT");

                CMCTTrendDisplay form = new CMCTTrendDisplay(_mStudyInfo.mIndex_KEY, DDeviceType.Unknown, bCtrl, bSelectFiles);

                if (form != null )//&& form.bStatsLoaded)
                {
                    form.Show();
                }
                FormEventBoard.sSetTriageTimerLeaving(prevMode, "MCT");
            }
        }

        private void buttonViewPdf_Click(object sender, EventArgs e)
        {
            if (_mStudyInfo != null && _mStudyInfo.mIndex_KEY > 0)
            {
                try
                {
                    CStudyReportListForm form = new CStudyReportListForm(_mStudyInfo.mIndex_KEY);

                    if (form != null)
                    {
                        form.ShowDialog();
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Call pdf study list", ex);
                }
            }
        }

        private void mOpenStudyDir()
        {
            if( _mStudyIndex != 0)
            {
                string studyPath = null;

                if( CDvtmsData.sbGetStudyCaseDir(out studyPath, _mStudyIndex, true))
                {
                    // open explorer wit file at cursor
                    try
                    {
                        ProcessStartInfo startInfo = new ProcessStartInfo();
                        startInfo.FileName = @"explorer";
                        startInfo.Arguments = studyPath;
                        Process.Start(startInfo);
                    }
                    catch (Exception e2)
                    {
                        CProgram.sLogException("Failed open explorer", e2);
                    }
                }
            }
        }
        private void labelStudyIndex_DoubleClick(object sender, EventArgs e)
        {

            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl) //&& CLicKeyDev.sbDeviceIsProgrammer())
            {
                mOpenStudyDir();
            }

        }

        private void buttonEndStudy_Click(object sender, EventArgs e)
        {
            if( _mStudyInfo != null )
            {
                try
                {
                    string deviceText = "";

                    CDeviceInfo deviceInfo = new CDeviceInfo(CDvtmsData.sGetDBaseConnection());
                    if (_mStudyInfo._mStudyRecorder_IX > 0 && deviceInfo != null)
                    {
                        if (_mStudyInfo._mRecorderEndUTC > DateTime.UtcNow)
                        {
                            _mStudyInfo._mRecorderEndUTC = DateTime.UtcNow;
                        }
                        if( deviceInfo.mbDoSqlSelectIndex(_mStudyInfo._mStudyRecorder_IX, deviceInfo.mGetValidMask(true)))
                        {
                            if( deviceInfo._mActiveStudy_IX == _mStudyInfo.mIndex_KEY)
                            {
                                if(deviceInfo._mRecorderEndUTC > DateTime.UtcNow)
                                {
                                    deviceInfo._mRecorderEndUTC = DateTime.UtcNow;
                                   
                                    deviceText = deviceInfo._mDeviceSerialNr + " End Recording";

                                    deviceInfo.mbDoSqlUpdate(CSqlDataTableRow.sGetMask((UInt16)DStudyInfoVars.RecorderEndUTC), true);
                                }
                            }
                        }
                    }
                    mbSaveReport( deviceText );  // update study;
                }
                catch ( Exception ex )
                {
                    CProgram.sLogException("End study", ex);
                }
            }
        }

        private void label4_DoubleClick(object sender, EventArgs e)
        {

        }
        private void mDoSelection( bool AbSet)
        {
            bool bSelectTable = checkBoxTable.Checked;
            bool bSelectStrip = checkBoxStrip.Checked;
            bool bModifyTachy = checkBoxModifyTachy.Checked;
            bool bModifyBrady = checkBoxModifyBrady.Checked;
            bool bModifyPause = checkBoxModifyPause.Checked;
            bool bModifyAfib = checkBoxModifyAF.Checked;
            bool bModifyAFl = checkBoxModifyAfl.Checked;
            bool bModifyPac = checkBoxModifyPAC.Checked;
            bool bModifyPvc = checkBoxModifyPVC.Checked;
            bool bModifyPace = checkBoxModifyPace.Checked;
            bool bModifyVt = checkBoxModifyVT.Checked;
            bool bModifyBL = checkBoxModifyBL.Checked;
            bool bModifyOther = checkBoxModifyOther.Checked;
            bool bModifyNormal = checkBoxModifyNormal.Checked;
            bool bModifyAbn = checkBoxModifyAbN.Checked;

            bool bModifyAll = checkBoxModifyAll.Checked;
            bool bModifyMan = checkBoxModifyManual.Checked;
            bool bModifyQC = checkBoxModifyQC.Checked;
            bool bModifyHR0 = checkBoxModifyHR0.Checked;
            bool bModifyHR1 = checkBoxModifyHR1.Checked;
            bool bCheckHR1 = bModifyHR1 && false == (bModifyTachy || bModifyBrady || bModifyPause || bModifyAfib || bModifyAFl || bModifyBL);
            if( bSelectStrip )
            {
                bSelectTable = true;
            }
            string qcd = CRecordMit.sGetRecStateUserAbreviation((UInt16)DRecState.QCd);

            try
            {
                if (_mAnalysisList != null && _mAnalysisList.Count > 0 && dataGridView.Rows != null && dataGridView.Rows.Count > 0 && CDvtmsData._sEnumListFindingsClass != null)
                {
                    bool bTable, bStrip, bHR0, bDo, bQC, bMan;
                    int nRow = dataGridView.Rows.Count;
                    string afib2 = "Afib";
                    string classification;
                    DataGridViewRow gridRow;
                    UInt16 nTotal = 0;
                    UInt16 nSet = 0;

                    dataGridView.SuspendLayout();

                    for (int i = 0; i < nRow; ++i)
                    {
                        bDo = false;
                        gridRow = dataGridView.Rows[i];
                        classification = "";
                        if (gridRow != null && gridRow.Cells.Count > (int)DFindingsTableVars.FindingsClass)//AnalysisIndex)
                        {
                            bTable = bStrip = false;
                            ++nTotal;

                            if (false == AbSet)
                            {
                                ++nSet;
                            }
                            else if (gridRow.Cells[0].Value != null && gridRow.Cells[1].Value != null)
                            {
                                if (gridRow.Cells[(int)DFindingsTableVars.FindingsClass].Value != null)
                                {
                                    classification = gridRow.Cells[(int)DFindingsTableVars.FindingsClass].Value.ToString();
                                }
                                bHR0 = gridRow.Cells[(int)DFindingsTableVars.HrMean].Value == null || gridRow.Cells[(int)DFindingsTableVars.HrMean].Value.ToString().Length == 0;

                                if (bModifyAll)
                                {
                                    bDo = true;
                                }
                                else
                                {
                                    if (bModifyTachy ) bDo |= classification == CDvtmsData._cFindings_Tachy;
                                    if (bModifyBrady ) bDo |= classification == CDvtmsData._cFindings_Brady;
                                    if (bModifyPause ) bDo |= classification == CDvtmsData._cFindings_Pause;
                                    if (bModifyAfib )  bDo |= classification == CDvtmsData._cFindings_AF || classification == afib2;
                                    if (bModifyAFl) bDo |= classification == CDvtmsData._cFindings_AFL;
                                    if (bModifyPac) bDo |= classification == CDvtmsData._cFindings_PAC;
                                    if (bModifyPvc) bDo |= classification == CDvtmsData._cFindings_PVC;
                                    if (bModifyPace) bDo |= classification == CDvtmsData._cFindings_Pace;
                                    if (bModifyVt) bDo |= classification == CDvtmsData._cFindings_VT;
                                    if (bModifyBL) bDo |= classification == CDvtmsData._cFindings_BL;
                                    if (bModifyOther) bDo |= classification == CDvtmsData._cFindings_Other;
                                    if (bModifyNormal) bDo |= classification == CDvtmsData._cFindings_Normal;
                                    if (bModifyAbn) bDo |= classification == CDvtmsData._cFindings_AbNormal;

                                    if (bCheckHR1) bDo |= false == bHR0;
//                                    else if (bModifyHR0 && bHR0) bDo = true;
//                                    else if (bModifyHR1 && false == bHR0) bDo = true;
// modify use of HR0 
                                }
/*                                if( bDo && bModifyHR1 )
                                {
                                    bDo = false == bHR0;
                                }
*/                              if (bModifyMan)
                                {
                                    bMan = (string)gridRow.Cells[(int)DFindingsTableVars.EventMA].Value == "M";

                                    bDo |= bMan;
                                }
                                if (bModifyQC)
                                {
                                    bQC = (string)gridRow.Cells[(int)DFindingsTableVars.RecState].Value == qcd;
                                    if (bQC)
                                    {
                                        bDo |= bQC;
                                    }
                                }

                                if (bDo)
                                {
                                    bTable = bSelectTable;
                                    bStrip = bSelectStrip;
                                }
                            }
                            gridRow.Cells[0].Value = bTable;
                            gridRow.Cells[1].Value = bStrip;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("End study", ex);
            }
            finally
            {
                dataGridView.ResumeLayout();
                mUpdateStats();
            }
        }
        private void buttonSetSelection_Click(object sender, EventArgs e)
        {
            mDoSelection(true);
        }

        private void buttonResetSelection_Click(object sender, EventArgs e)
        {
            mDoSelection(false);
        }

        private void checkBoxModifyAll_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxModifyAll.Checked)
            {
                if (checkBoxModifyTachy.Checked) checkBoxModifyTachy.Checked = false;
                if (checkBoxModifyBrady.Checked) checkBoxModifyBrady.Checked = false;
                if (checkBoxModifyAF.Checked) checkBoxModifyAF.Checked = false;
                if (checkBoxModifyPause.Checked) checkBoxModifyPause.Checked = false;
                if (checkBoxModifyAfl.Checked) checkBoxModifyAfl.Checked = false;

                if (checkBoxModifyPAC.Checked) checkBoxModifyPAC.Checked = false;
                if (checkBoxModifyPVC.Checked) checkBoxModifyPVC.Checked = false;
                if (checkBoxModifyPace.Checked) checkBoxModifyPace.Checked = false;
                if (checkBoxModifyVT.Checked) checkBoxModifyVT.Checked = false;


                if (checkBoxModifyBL.Checked) checkBoxModifyBL.Checked = false;
                if (checkBoxModifyOther.Checked) checkBoxModifyOther.Checked = false;
                if (checkBoxModifyNormal.Checked) checkBoxModifyNormal.Checked = false;
                if (checkBoxModifyAbN.Checked) checkBoxModifyAbN.Checked = false;
                if (checkBoxModifyHR0.Checked) checkBoxModifyHR0.Checked = false;
                if (checkBoxModifyHR1.Checked) checkBoxModifyHR1.Checked = false;

                /*            if (checkBoxModifyTachy.Checked) checkBoxModifyTachy.Checked = false;
                            if (checkBoxModifyBrady.Checked) checkBoxModifyBrady.Checked = false;
                            if (checkBoxModifyAF.Checked) checkBoxModifyAF.Checked = false;
                            if (checkBoxModifyPause.Checked) checkBoxModifyPause.Checked = false;
                            if (checkBoxModifyBL.Checked) checkBoxModifyBL.Checked = false;
                            if (checkBoxModifyOther.Checked) checkBoxModifyOther.Checked = false;
                            if (checkBoxModifyHR0.Checked) checkBoxModifyHR0.Checked = false;
                            if (checkBoxModifyHR1.Checked) checkBoxModifyHR1.Checked = false;
                */
            }
        }
        private void checkBoxModifyTachy_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxModifyTachy.Checked)
            {
                if (checkBoxModifyAll.Checked)
                {
                    checkBoxModifyAll.Checked = false;
                }
            }
        }

        private void checkBoxModifyBrady_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxModifyBrady.Checked)
            {
                checkBoxModifyAll.Checked = false;
            }
        }

        private void checkBoxModifyPause_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxModifyPause.Checked)
            {
                checkBoxModifyAll.Checked = false;
            }

        }

        private void checkBoxModifyAF_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxModifyAF.Checked)
            {
                checkBoxModifyAll.Checked = false;
            }

        }

        private void checkBoxModifyOther_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxModifyOther.Checked)
            {
                checkBoxModifyAll.Checked = false;
            }

        }

        private void checkBoxModifyBL_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxModifyBL.Checked)
            {
                checkBoxModifyAll.Checked = false;
            }
        }

        private void buttonClearConclusion_Click_1(object sender, EventArgs e)
        {
            textBoxConclusion.Text = "";
        }

        private void checkBoxModifyManual_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxModifyManual.Checked)
            {
                checkBoxModifyAll.Checked = false;
            }
        }

        private void checkBoxModifyHR1_CheckedChanged_1(object sender, EventArgs e)
        {
            if (checkBoxModifyHR1.Checked)
            {
                checkBoxModifyAll.Checked = false;
            }
        }

        private void labelStudyNote_Click(object sender, EventArgs e)
        {
            if (_mStudyInfo != null)
            {
                string s = _mStudyInfo._mStudyRemarkLabel + "\r\n" + _mStudyInfo._mStudyPhysicianInstruction;

                
            }
        }
            private void checkBoxModifyQC_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxModifyQC.Checked)
            {
                checkBoxModifyAll.Checked = false;
            }
        }

        private void buttonClearFindings_Click(object sender, EventArgs e)
        {
            textBoxSummary.Text = "";
        }

        private void buttonAmplRange_Click(object sender, EventArgs e)
        {
            mSetAmplRange();
        }

        private void labelRecordInfo_Click(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl)
            {
                mDumpRecAnalyzeInfo();
            }

        }

        private void buttonSetSelection_Click_1(object sender, EventArgs e)
        {
            mDoSelection(true);
        }

        private void checkBoxAnonymize_CheckedChanged(object sender, EventArgs e)
        {
            mInitPatient();
        }

        private void buttonDisableAn_Click(object sender, EventArgs e)
        {
            mDisableAnalysis();
        }

        private void buttonUncheckAll_Click_1(object sender, EventArgs e)
        {
            mDoSelection(false);
        }

        private void checkBoxModifyVT_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxModifyVT.Checked)
            {
                checkBoxModifyAll.Checked = false;
            }

        }

        private void checkBoxModifyPace_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxModifyPause.Checked)
            {
                checkBoxModifyAll.Checked = false;
            }
        }

        private void checkBoxModifyPVC_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxModifyPVC.Checked)
            {
                checkBoxModifyAll.Checked = false;
            }
        }

        private void checkBoxModifyPAC_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxModifyPAC.Checked)
            {
                checkBoxModifyAll.Checked = false;
            }
        }

        private void checkBoxModifyAfl_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxModifyAfl.Checked)
            {
                checkBoxModifyAll.Checked = false;
            }
        }

        private void buttonUncheckAll_Click(object sender, EventArgs e)
        {
            mDoSelection(false);
        }

        private void checkBoxModifyHR0_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxModifyHR0.Checked)
            {
                checkBoxModifyAll.Checked = false;
            }

        }

        private void buttonOpenAnalysis_Click(object sender, EventArgs e)
        {
            mOpenAnalysis();
        }

        private void labelHR_Click(object sender, EventArgs e)
        {
            mUpdateStats();
        }

        private void buttonSortET_Click(object sender, EventArgs e)
        {
            try
            {
                dataGridView.Sort(dataGridView.Columns[(int)DFindingsTableVars.RecordET], ListSortDirection.Ascending);
            }
            catch( Exception ex)
            {
                CProgram.sLogException("Sort table", ex);
            }
 
        }

        private void button7_Click(object sender, EventArgs e)
        {

        }

        private void checkBoxModifyHR1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxModifyHR1.Checked)
            {
                checkBoxModifyAll.Checked = false;
            }

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void dataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if( e.RowIndex >= 0 && e.RowIndex < dataGridView.Rows.Count)
            {
                DataGridViewRow row = dataGridView.Rows[e.RowIndex];

                if( row != null )
                {
                    if (e.ColumnIndex == 0)
                    {
                        if( row.Cells[0].Value != null )
                        {
                            bool b = (bool)row.Cells[0].Value;
                            b = !b;
                            
                            row.Cells[0].Value = b;
                            if( b == false )
                            {
                                row.Cells[1].Value = false; // turn off strip when turning off table

                            }
                            mUpdateStats();
                        }

                    }
                    else if (e.ColumnIndex == 1)
                    {
                        if (row.Cells[1].Value != null)
                        {
                            bool b = (bool)row.Cells[1].Value;

                            b = !b;
                            row.Cells[1].Value = b;
                            if (b == true)
                            {
                                row.Cells[0].Value = true; // turn on table when turning on strip 
                            }

                            mUpdateStats();
                        }
                    }
                }
            }
        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void panel40_Paint(object sender, PaintEventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void buttonPrintPreviousFindings_Click(object sender, EventArgs e)
        {
            if( mbSaveReport("")) // collects data
            {
                // start and end period determined by SetStat
                /*
                                bool bInitials = _mCreadedByInitials != null && _mCreadedByInitials.Length > 0;

                                if (false == bInitials)
                                {
                                    if (CProgram.sbReqLabel("Print pdf", "User Initials", ref _mCreadedByInitials, ""))
                                    {
                                        bInitials = _mCreadedByInitials != null && _mCreadedByInitials.Length > 0;
                                    }
                                }
                                //                labelUser.Text = _mCreadedByInitials;
                                if (bInitials)
                                */
                //                labelUser.Text = CProgram.sGetProgUserInitials();

                bool bAnonymize = checkBoxAnonymize.Checked;
                bool bReduce = checkBoxReducePdf.Checked;
                bool bPlot2Ch = checkBoxPlot2ch.Checked;
                bool bBlackWhite = checkBoxBlackWhite.Checked;
                bool bQCd = checkBoxQCD.Checked;

                string printInitials; 

                if (CProgram.sbEnterProgUserArea(true)
                && FormEventBoard.sbGetPrintUserInitials("Findings", out printInitials, false, ""))
                {
                    DTriageTimerMode prevMode = FormEventBoard.sSetTriageTimerEntering(DTriageTimerMode.Print, "Print Study Events by " + printInitials);

                    using (CPrintStudyEventsForm form = new CPrintStudyEventsForm(this, dataGridView, true, printInitials,
                        _mStartPeriodDT, _mEndPeriodDT, bReduce, bPlot2Ch, bAnonymize, bBlackWhite, bQCd))
                    {
                        if (form != null && false == form.IsDisposed)
                        {
                            form.ShowDialog();

                            labelPdfNr.Text = "PDF #" + form._mStudyReportNr.ToString();

//                            buttonPrintPreviousFindings.Text = "Update and Print";
                        }
                    }
                    FormEventBoard.sSetTriageTimerLeaving(prevMode, "Print Study Events");

                    CProgram.sMemoryCleanup(true, true);
                }
            }
        }
        private void mDisableAnalysis()
        {
            string name = "";

            try
            {
                if (dataGridView.CurrentRow != null)
                {
                    CRecAnalysis selectedRow = null;

                    if (dataGridView.CurrentRow.Cells.Count > (int)DFindingsTableVars.AnalysisIndex
                        && dataGridView.CurrentRow.Cells[(int)DFindingsTableVars.AnalysisIndex].Value != null)
                    {
                        string analysisStr = dataGridView.CurrentRow.Cells[(int)DFindingsTableVars.AnalysisIndex].Value.ToString();

                        name = analysisStr;
                        if (_mAnalysisList != null && _mStudyInfo != null && analysisStr != null && analysisStr.Length > 0)
                        {
                            foreach (CRecAnalysis row in _mAnalysisList)
                            {
                                if (analysisStr == row.mIndex_KEY.ToString())
                                {
                                    selectedRow = row;
                                    break;
                                }
                            }
                        }
                    }

                    if (selectedRow != null)
                    {
                        CRecordMit rec = mFindRecord(selectedRow._mRecording_IX);
                        name = rec == null ? "0" : rec.mSeqNrInStudy.ToString();

                        name += "." + selectedRow._mSeqInRecording.ToString() + "(" + selectedRow.mIndex_KEY.ToString() + ")";

                        if (CProgram.sbAskYesNo("Study " + _mStudyIndex.ToString(), "Disable analysis " + name ))
                        {

                            selectedRow.mbActive = false;
                            if( selectedRow.mbDoSqlUpdate(CSqlDataTableRow.sGetMask((UInt16)DSqlDataTableColum.Active), false))
                            {
                                dataGridView.Rows.Remove(dataGridView.CurrentRow);
                                CProgram.sLogLine("Study " + _mStudyIndex.ToString() + " Disabled analysis " + name);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed to disable Analysis name", ex);
            }
        }

        private void mOpenAnalysis()
        {
            FormAnalyze formAnalyze = null;

            try
            {
                if (dataGridView.CurrentRow != null)
                {
                    CRecAnalysis selectedRow = null;

                    if (dataGridView.CurrentRow.Cells.Count > (int)DFindingsTableVars.AnalysisIndex
                        && dataGridView.CurrentRow.Cells[(int)DFindingsTableVars.AnalysisIndex].Value != null)
                    {
                        string analysisStr = dataGridView.CurrentRow.Cells[(int)DFindingsTableVars.AnalysisIndex].Value.ToString();

                        if (_mAnalysisList != null && _mStudyInfo != null && analysisStr != null && analysisStr.Length > 0)
                        {
                            foreach (CRecAnalysis row in _mAnalysisList)
                            {
                                if (analysisStr == row.mIndex_KEY.ToString())
                                {
                                    selectedRow = row;
                                    break;
                                }
                            }
                        }
                    }

                    if (selectedRow != null)
                    {
                        FormEventBoard eventBoardForm = FormEventBoard.sGetMainForm();
                        formAnalyze = new FormAnalyze(eventBoardForm);

                        if (eventBoardForm != null && formAnalyze != null)
                        {
                            UInt32 recordIndex = selectedRow._mRecording_IX;
                            UInt32 analysisIndex = selectedRow.mIndex_KEY;
                            UInt16 analysisNr = selectedRow._mSeqInRecording;
                            bool bReadOnly = FormEventBoard._sbTriageOnly;

                            if (formAnalyze.mbLoadRecord2(bReadOnly, recordIndex, false, analysisIndex, analysisNr, eventBoardForm._mDBaseServer, eventBoardForm._mRecordingDir))
                            {
                                formAnalyze.mInitScreens();

                                if (formAnalyze != null)
                                {
                                    try
                                    {
                                        formAnalyze.Show();
                                        formAnalyze = null;  // running so no cleanup needed
                                        //                                        formAnalyze.ShowDialog();
                                    }
                                    catch (Exception ex)
                                    {
                                        CProgram.sLogException("Analysis Form run", ex);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed to run Analysis Form", ex);
            }
            if( formAnalyze != null)
            {
                // Created but not in use => cleanup
                formAnalyze.mReleaseLock();               
            }
        }
        private void dataGridView_DoubleClick(object sender, EventArgs e)
        {
            mOpenAnalysis();
        }

        private void dataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
