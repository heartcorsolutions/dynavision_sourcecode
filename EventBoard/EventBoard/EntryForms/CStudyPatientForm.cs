﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Program_Base;
using Event_Base;
using EventBoard.EntryForms;
using EventBoard;
using System.Diagnostics;
using EventBoardEntryForms;

namespace EventboardEntryForms
{
  
    public partial class CStudyPatientForm : Form
    {
        public CPatientInfo _mPatientInfo;
        public CStudyInfo _mStudyInfo;
        public CClientInfo _mClient;
        public List<CSqlDataTableRow> _mClientList;
        public CTrialInfo _mTrialInfo;
        public List<CSqlDataTableRow> _mTrialList;

        public CPhysicianInfo _mPhysician;
        public List<CSqlDataTableRow> _mPhysicianList;

        public CDeviceModel _mDeviceModel;
        public List<CSqlDataTableRow> _mDeviceModelList;

        public CDeviceInfo _mDeviceInfo;    // needed for loading etc
        public CDeviceInfo _mActiveDevice;

        public List<CSqlDataTableRow> _mDeviceList;
        public List<CSqlDataTableRow> _mRecordList = null;  // loaded all records for stat and update records
        public bool mbAllowRecordUpdate = false;
        bool _mbUseExcluded = false;
        public bool mbInitFinished = false;

         public CStudyPatientForm(CStudyInfo AStudyInfo, CPatientInfo APatientInfo, bool AbReadOnly, bool AbAllowDevices, bool AbAllowFindings, bool AbAllowReports)
        {
            try
            {
                bool bProgrammer = CLicKeyDev.sbDeviceIsProgrammer();

                _mbUseExcluded = FormEventBoard._sbStudyUseExcluded;

                CDvtmsData.sbInitData();

                string studyStr = "New Study";

                string devRefIdDefault = EventBoard.Properties.Settings.Default.DevRefIDDefault;

                _mStudyInfo = new CStudyInfo(CDvtmsData.sGetDBaseConnection());
                if (_mStudyInfo != null)
                {
                    _mStudyInfo.mbActive = true;
                    _mStudyInfo._mStudyStartDate = DateTime.Now;
                    _mStudyInfo.mbCopyFrom(AStudyInfo);

                    if (_mStudyInfo.mIndex_KEY > 0)
                    {
                        studyStr = (AbReadOnly ? " View Study" : "Edit study ") + _mStudyInfo.mIndex_KEY.ToString();
                    }
                    else
                    {
                        if (FormEventBoard._sbForceAnonymize)
                        {
                            CProgram.sAskWarning("Add Study", "Add Study not allowed with Anonymized Patienty info");
                            Close();
                            return;
                        }
                    }
                }
                _mPatientInfo = new CPatientInfo(CDvtmsData.sGetDBaseConnection());
                if(_mPatientInfo != null)
                {
                    _mPatientInfo.mbActive = true;
                    _mPatientInfo.mbCopyFrom(APatientInfo);
                }


                _mClient = new CClientInfo(CDvtmsData.sGetDBaseConnection());
                _mClientList = new List<CSqlDataTableRow>();

                _mTrialInfo = new CTrialInfo(CDvtmsData.sGetDBaseConnection());
                _mTrialList = new List<CSqlDataTableRow>();

                _mPhysician = new CPhysicianInfo( CDvtmsData.sGetDBaseConnection());
                _mPhysicianList = new List<CSqlDataTableRow>();

                _mDeviceModel = new CDeviceModel(CDvtmsData.sGetDBaseConnection());
                _mDeviceModelList = new List<CSqlDataTableRow>();

                _mDeviceInfo = new CDeviceInfo(CDvtmsData.sGetDBaseConnection());
                _mActiveDevice = new CDeviceInfo(CDvtmsData.sGetDBaseConnection());
                _mDeviceList = new List<CSqlDataTableRow>();

                _mRecordList = new List<CSqlDataTableRow>();

                mbAllowRecordUpdate = bProgrammer 
                    || FormEventBoard._sbStudyRecordsAllowUpdate 
                            && (false == FormEventBoard._sbTriageOnly);            // pc with triage only should not mess with updating records info

                if (_mPatientInfo == null || _mStudyInfo == null || _mClient == null || _mClientList == null 
                    || _mPhysician == null || _mPhysicianList == null || _mDeviceModel == null || _mDeviceModelList == null 
                    || _mDeviceInfo == null || _mDeviceList == null || _mTrialInfo == null || _mTrialList == null)
                {
                    CProgram.sLogLine("Form creation failed");
                    Close();
                }
                else
                {
                    InitializeComponent();

                    if (FormEventBoard._sbForceAnonymize)
                    {
                        checkBoxAnonymizePatientInfo.Checked = true;
                        checkBoxAnonymizePatientInfo.Enabled = false;
                    }
                    if ( _mStudyInfo._mPatient_IX != 0 && _mPatientInfo.mIndex_KEY == 0)
                    {
                        // load patient belonging to study
                        _mPatientInfo.mbDoSqlSelectIndex(_mStudyInfo._mPatient_IX, _mPatientInfo.mMaskValid);
//                        studyStr = "Edit Study " + _mStudyInfo.mIndex_KEY.ToString();
                    }
                    bool bError;

                    _mClient.mbDoSqlSelectList(out bError, out _mClientList, CSqlDataTableRow.sGetMask((UInt16)DClientInfoVars.Label),
                                0, true, "", DSqlSort.Ascending, (UInt16)DClientInfoVars.Label);


                    //string trialWhere = "";

   // for now load all trials     start <= now <= stop or  trial = _mStudyInfo.Trial >0            _mTrialInfo.mSql

                    _mTrialInfo.mbDoSqlSelectList(out bError, out _mTrialList, _mTrialInfo.mMaskValid, 0, true, "",
                    DSqlSort.Ascending, (UInt16)DTrialInfoVars.Label);

                    if (_mTrialList != null)
                    {
                        _mTrialInfo.mbListFillComboBox(comboBoxTrial, _mTrialList, (UInt16)DClientInfoVars.Label, _mStudyInfo._mTrial_IX, "-- None --");
                    }


                    _mPhysician.mbDoSqlSelectList(out bError, out _mPhysicianList, CSqlDataTableRow.sGetMask((UInt16)DPhysicianInfoVars.Label )
                                        | CSqlDataTableRow.sGetMask( (UInt16)DPhysicianInfoVars.Instruction)
                                        | CSqlDataTableRow.sGetMask((UInt16)DPhysicianInfoVars.Hospital_IX),
                                0, true, "", DSqlSort.Ascending, (UInt16)DPhysicianInfoVars.Label);

                    _mDeviceModel.mbDoSqlSelectList(out bError, out _mDeviceModelList, CSqlDataTableRow.sGetMask((UInt16)DDeviceModelVars.ModelLabel), 
                                        0, true, "");


//                    labelUseExluded.Text = _mbUseExcluded ? "+Excl" : "";
                    Text = CProgram.sMakeProgTitle(studyStr, false, true);

                    if (devRefIdDefault != null && devRefIdDefault.Length > 0)
                    {
                        if (devRefIdDefault == radioButtonNone.Text)
                        {
                            radioButtonNone.Checked = true;
                        }
                        else if (devRefIdDefault == radioButtonPatID.Text)
                        {
                            radioButtonPatID.Checked = true;
                        }
                        else if (devRefIdDefault == radioButtonUniqueID.Text)
                        {
                            radioButtonUniqueID.Checked = true;
                        }
                        else if (devRefIdDefault == radioButtonOther.Text)
                        {
                            radioButtonOther.Checked = true;
                        }
                    }
                    buttonRandom.Visible = CLicKeyDev.sbDeviceIsProgrammer();

                    mLoadDeviceList();

                    labelStudyPermissions.Visible = bProgrammer;

                    textBoxStudyPermissions.Visible = bProgrammer;

                    checkBoxUpdateRecords.Visible = mbAllowRecordUpdate;
                    checkBoxUpdatePatInfo.Visible = mbAllowRecordUpdate;
                    labelLocation.Text = CDvtmsData._sStudyPermSiteText + ":";

                    int nList = CDvtmsData.sFillStudyPermissionsComboBox(comboBoxLocation, "---", _mStudyInfo._mStudyPermissions, CDvtmsData._sStudyPermSiteGroup);

                    mFillForm();
                    textBoxRefIdOther.Text = "";
                    if ( AbReadOnly )
                    {
                        buttonAddUpdate.Visible = false;
                        buttonClear.Visible = false;
                        buttonDup.Visible = false;
                    }
                    buttonDeviceList.Visible = AbAllowDevices;
                    buttonCreateReport.Visible = AbAllowFindings;

                    //
                    if( _mStudyInfo._mStudyRecorder_IX != 0 && _mStudyInfo._mStudyRecorder_IX == _mDeviceInfo.mIndex_KEY 
                        && _mDeviceInfo._mActiveStudy_IX == _mStudyInfo.mIndex_KEY)
                    {
                        string refID = _mDeviceInfo._mActiveRefID.mDecrypt();
                        string otherStr = "";

                        if (refID == null || refID.Length == 0)
                        {
                            radioButtonNone.Checked = true;
                        }
                        else if (_mPatientInfo.mIndex_KEY != 0 && false == _mPatientInfo.mbIsPatientIDEmpty() && _mPatientInfo.mbIsPatientIDEqual(refID))
                        {
                            radioButtonPatID.Checked = true;
                        }
                        else if (_mPatientInfo.mIndex_KEY != 0 && false == _mPatientInfo._mSocSecNr.mbIsEmpty() && _mPatientInfo._mSocSecNr.mDecrypt() == refID)
                        {
                            radioButtonUniqueID.Checked = true;
                        }
                        else
                        {
                            radioButtonOther.Checked = true;
                            otherStr = refID;
                        }
                        textBoxRefIdOther.Text = otherStr;
                    }
                    mbCheckForm(AStudyInfo == null ? "New" : "Edit", false);
                    // AbAllowReports
                    mbInitFinished = true;  // needed to signal create study that initialisation is done
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Form creation failed", ex);
                Close();
            }
        }

        private string mGetTrialText( UInt32 ATrialIX, out String ArDaysLeft)
        {
            string s = ATrialIX == 0 ? "---" : "T" + ATrialIX.ToString();
            string daysLeft = "";

            if( _mTrialInfo != null )
            {
                if( _mTrialInfo.mbLoadIndexFromList(_mTrialList, ATrialIX))
                {
                    s = _mTrialInfo._mLabel;

                    int nLeft = (int)(_mTrialInfo._mTrialStopDate - DateTime.Now).TotalDays;

                    if( nLeft > -30)
                    {
                        daysLeft = nLeft.ToString() + "d";
                    }
                }
            }
            ArDaysLeft = daysLeft;
            return s;
        }

        private void mLoadDeviceList()
        {
            if (_mDeviceInfo != null && _mActiveDevice != null )
            {
                bool bError;
                UInt64 loadMask = _mDeviceInfo.mGetValidMask(true);
                    /* just get all CSqlDataTableRow.sGetMask((UInt16)DDeviceInfoVars.DeviceSerialNr)
                                | CSqlDataTableRow.sGetMask((UInt16)DDeviceInfoVars.RemarkLabel,
                                                    //labelDeviceChanged.Text = CDvtmsData.sGetChangedBy(_mDeviceInfo.mChangedUTC, _mDeviceInfo.mChangedBy_IX);

                                
                                );
                                */
                UInt64 maskWhere = CSqlDataTableRow.sGetMask((UInt16)DDeviceInfoVars.State_IX);
                _mDeviceInfo._mState_IX = (UInt16)DDeviceState.Available;

                _mDeviceInfo.mbDoSqlSelectList(out bError, out _mDeviceList, loadMask, maskWhere, true, "", DSqlSort.Ascending, (UInt16)DDeviceInfoVars.DeviceSerialNr);

                if ( _mStudyInfo._mStudyRecorder_IX == 0)
                {
                    _mActiveDevice.mClear();
                }
                else
                {
                    _mActiveDevice.mbDoSqlSelectIndex(_mStudyInfo._mStudyRecorder_IX, _mActiveDevice.mGetValidMask(true));
                }

                if (_mDeviceModel != null)
                {
                    //_mDeviceModel.mbListFillComboBox(comboBoxStudyRecorder, _mDeviceModelList, (UInt16)DDeviceModelVars.ModelLabel, _mActiveDevice._mDeviceModel_IX, "-- Select --");
                }
                /*                _mDeviceInfo.mbListFillComboBox(comboBoxStudySerialNumberList, _mDeviceList, (UInt16)DDeviceInfoVars.DeviceSerialNr, _mStudyInfo._mStudyRecorder_IX, "-- Select --");
                                if ( _mActiveDevice.mIndex_KEY > 0 )
                                {
                                    comboBoxStudySerialNumberList.Items.Insert(0, _mActiveDevice._mDeviceSerialNr); // add active in front
                                     comboBoxStudySerialNumberList.Text = _mActiveDevice._mDeviceSerialNr; // if assigned already it will not be in the list
                                }
                                */
                mFillDeviceList();
            }
        }

        private void mFillAfterClientChange()
        {
            mFillRefPhysicianList();
            mFillPhysicianList();
            mFillDeviceList();
        }

        private void mFillPhysicianList()
        {
            mFillPhysiscienLists(comboBoxStudyPhysicianList, _mStudyInfo._mPhysisian_IX, checkBoxAllPhysician.Checked);
        }
        private void mFillRefPhysicianList()
        {
            mFillPhysiscienLists(comboBoxStudyRefPhysicianList, _mStudyInfo._mRefPhysisian_IX, checkBoxAllRefPhysician.Checked);
        }
        private void mFillDeviceList()
        {
            bool bAll = checkBoxAllRecorder.Checked;
           UInt32 clientIX = _mStudyInfo._mClient_IX;
            string clientName = comboBoxStudyHospitalNameList.Text;
            string firstLine = "-- Select --";
            UInt32 n = 0;


            if (_mClient != null)
            {
                _mClient.mbListParseComboBox(comboBoxStudyHospitalNameList, _mClientList, (UInt16)DClientInfoVars.Label, ref clientIX);
            }
            if( clientIX == 0)
            {
                bAll = true;
            }
            
            comboBoxStudySerialNumberList.Items.Clear();
            comboBoxStudySerialNumberList.Items.Add(firstLine);

            foreach( CDeviceInfo device in _mDeviceList)
            {               
                if( bAll
                    || device._mClient_IX != 0 && device._mClient_IX == clientIX
                    || device._mRemarkLabel.Contains(clientName))
                {
                    if (device._mDateNextService <= device._mDateFirstInService || device._mDateNextService > DateTime.Now)
                    {
                        if (device._mNextStateMode == (UInt16)DNextStateModeEnum.Normal)        // do not show devices that are in nead of service
                        {
                            comboBoxStudySerialNumberList.Items.Add(device._mDeviceSerialNr);
                            ++n;
                        }
                    }
                }
            }
            if (_mActiveDevice.mIndex_KEY > 0)
            {
                comboBoxStudySerialNumberList.Items.Insert(0, _mActiveDevice._mDeviceSerialNr); // add active in front
                comboBoxStudySerialNumberList.Text = _mActiveDevice._mDeviceSerialNr; // if assigned already it will not be in the list
            }
            else
            {
                if( n == 0)
                {
                    firstLine = "-- none --";
                    comboBoxStudySerialNumberList.Items.Clear();
                    comboBoxStudySerialNumberList.Items.Add(firstLine);
                }
                comboBoxStudySerialNumberList.Text = firstLine;
            }
        }

        private void mFillPhysiscienLists( ComboBox APhysisianBox, UInt32 APhysicianIX, bool AbAll )
        {
            bool bAll = AbAll;
            UInt32 clientIX = 0;
            string clientName = comboBoxStudyHospitalNameList.Text;
            string currentPhysician = "?" + APhysicianIX.ToString() + " not found?";
            string firstLine = "-- Select --";
            UInt32 n = 0;

            if (_mClient != null)
            {
                _mClient.mbListParseComboBox(comboBoxStudyHospitalNameList, _mClientList, (UInt16)DClientInfoVars.Label, ref clientIX);
            }
            if (clientIX == 0)
            {
                bAll = true;
            }

            APhysisianBox.Items.Clear();
            APhysisianBox.Items.Add(firstLine);

            foreach (CPhysicianInfo physician in _mPhysicianList)
            {
                if( physician.mIndex_KEY == APhysicianIX)
                {
                    currentPhysician = physician._mLabel;
                }
                if (bAll || physician._mHospital_IX == clientIX)
                {
                    APhysisianBox.Items.Add(physician._mLabel);
                    ++n;
                }
            }
            
            if (APhysicianIX > 0)
            {
                comboBoxStudySerialNumberList.Items.Insert(0, currentPhysician); // add active in front
                comboBoxStudySerialNumberList.Text = currentPhysician; // if assigned already it will not be in the list
            }
            else
            {
                if (n == 0)
                {
                    firstLine = "-- none --";
                    APhysisianBox.Items.Clear();
                    APhysisianBox.Items.Add(firstLine);
                }
                APhysisianBox.Text = firstLine;
            }
        }

        private void mFillStudyPermissions()
        {
            string text = CDvtmsData.sGetStudyPermissionsBitLabels(_mStudyInfo._mStudyPermissions, "\r\n");

            textBoxStudyPermissions.Text = text;
            CDvtmsData.sbSetStudyPermissionsComboBox(comboBoxLocation, _mStudyInfo._mStudyPermissions, CDvtmsData._sStudyPermSiteGroup);


        }

        private void mFillForm()
        {
            UInt32 i;

            mFillFormStudy();
            mFillFormPatient();
           

            mbCheckUpdateDevice(out i);
            mUpdateButtons();
        }

        private UInt32 mReadTrial()
        {
            UInt32 trial = 0;

            if (_mTrialInfo != null)
            {
                if (_mTrialInfo.mbListParseComboBox(comboBoxTrial, _mTrialList, (UInt16)DTrialInfoVars.Label, ref trial))
                {
                }
            }
            return trial;
        }

        public void mUpdateButtons()
        {
            if(_mStudyInfo != null )
            {
                bool bStudy = _mStudyInfo.mIndex_KEY > 0;
                if ( bStudy)
                {
                    textBoxStudyNr.Text = _mStudyInfo.mIndex_KEY.ToString();
                    buttonAddUpdate.Text = "Update";
                 }
                else
                {
                    textBoxStudyNr.Text = "";
                    buttonAddUpdate.Text = "Add";
                }
                buttonCreateReport.Visible = bStudy;
                buttonCreateReport.Visible = bStudy;
                buttonViewPdf.Visible = bStudy;
                buttonMCT.Visible = bStudy;


                labelStudyNr.Text = textBoxStudyNr.Text;

                if ( _mPatientInfo != null )
                {
                    string notActive = "";

                    if(false == _mPatientInfo.mbActive)
                    {
                        notActive = " na";
                    }
                    if( _mStudyInfo._mPatient_IX == 0 )
                    {
                        if( _mPatientInfo.mIndex_KEY == 0 )
                        {
                            labelPatientNr.Text = "";
                            buttonSearchPatientID.Enabled = true;
                            textBoxSocSecNr.ReadOnly = false;
                        }
                        else
                        {
                            labelPatientNr.Text = _mPatientInfo.mIndex_KEY.ToString() + notActive;
                            buttonSearchPatientID.Enabled = false;
                            textBoxSocSecNr.ReadOnly = true;
                        }
                    }
                    else
                    {
                        if (_mPatientInfo.mIndex_KEY == 0)
                        {
                            labelPatientNr.Text = _mStudyInfo.mIndex_KEY.ToString() + " != 0";
                            buttonSearchPatientID.Enabled = true;
                            textBoxSocSecNr.ReadOnly = false;

                        }
                        else if( _mPatientInfo.mIndex_KEY == _mStudyInfo._mPatient_IX )
                        {
                            labelPatientNr.Text = _mPatientInfo.mIndex_KEY.ToString() + notActive;
                            buttonSearchPatientID.Enabled = false;
                            textBoxSocSecNr.ReadOnly = true;
                        }
                        else
                        {
                            labelPatientNr.Text = _mStudyInfo._mPatient_IX.ToString() + " != " + _mPatientInfo.mIndex_KEY.ToString() + notActive;
                            buttonSearchPatientID.Enabled = false;
                            textBoxSocSecNr.ReadOnly = true;
                        }
                    }
                }
                bool bSelectDevice = comboBoxStudySerialNumberList.Text == null || comboBoxStudySerialNumberList.Text.Length == 0 || comboBoxStudySerialNumberList.Text[0] == '-';

                if (bSelectDevice && _mActiveDevice != null && _mStudyInfo._mStudyRecorder_IX > 0 && _mActiveDevice.mIndex_KEY == _mStudyInfo._mStudyRecorder_IX)
                {
                    comboBoxStudySerialNumberList.Text = _mActiveDevice._mDeviceSerialNr;
                    bSelectDevice = false;
                }
                buttonUseCurrentDevice.Visible = _mStudyInfo._mStudyRecorder_IX > 0 
                    && (_mActiveDevice == null || _mActiveDevice.mIndex_KEY != _mStudyInfo._mStudyRecorder_IX 
                        ||  _mActiveDevice._mDeviceSerialNr != comboBoxStudySerialNumberList.Text );

                buttonEndRecording.Visible = _mDeviceInfo != null && _mDeviceInfo.mIndex_KEY > 0 && _mDeviceInfo._mActiveStudy_IX == _mStudyInfo.mIndex_KEY;
            }
            else
            {
                textBoxStudyNr.Text = "";
                labelStudyNr.Text = textBoxStudyNr.Text;
                labelPatientNr.Text = "";
                labelDeviceCmp.Text = "";
                buttonCreateReport.Visible = false;
                buttonUseCurrentDevice.Visible = false;
                buttonEndRecording.Visible = false;

            }
        }

        public void mFillFormStudy()
        {
            // zet waardes van _mStudioInfo in velden
            if (_mStudyInfo != null)
            {
                try
                {
                    textBoxStudyNote.Text = _mStudyInfo._mStudyRemarkLabel;

                    // init trial
                    string trialLeft;
                    comboBoxTrial.Text = mGetTrialText(_mStudyInfo._mTrial_IX, out trialLeft);
                    labelTrialDays.Text = trialLeft;

                    // init client list

                    if (_mClient != null)
                    {
                        bool bError;
                        UInt64 maskLoad = CSqlDataTableRow.sGetMask((UInt16)DClientInfoVars.Label);
                        UInt64 maskWhere = CSqlDataTableRow.sGetMask((UInt16)DSqlDataTableColum.Active);

                        //        _mClient.mbDoSqlSelectList(out bError, out _mClientList, maskLoad, maskWhere, true, "");
                        _mClient.mbListFillComboBox(comboBoxStudyHospitalNameList, _mClientList, (UInt16)DClientInfoVars.Label, _mStudyInfo._mClient_IX, "-- Select --");
                    }
                    dateTimePickerStudyStartDate.Value = _mStudyInfo._mStudyStartDate == null || _mStudyInfo._mStudyStartDate == DateTime.MinValue 
                                                    ? DateTime.Now.AddHours(-1) : _mStudyInfo._mStudyStartDate;
                    textBoxHospitalRoom.Text = _mStudyInfo._mHospitalRoom;
                    textBoxHospitalBed.Text = _mStudyInfo._mHospitalBed;

                    if (_mPhysician != null)
                    {
                        bool bError;
                        UInt64 maskLoad = CSqlDataTableRow.sGetMask((UInt16)DPhysicianInfoVars.Label);
                        UInt64 maskWhere = CSqlDataTableRow.sGetMask((UInt16)DPhysicianInfoVars.Hospital_IX);
                        _mPhysician._mHospital_IX = _mStudyInfo._mClient_IX;

                        //                    _mPhysician.mbDoSqlSelectList(out bError, out _mPhysicianList, maskLoad, maskWhere, true, "");
                        _mPhysician.mbListFillComboBox(comboBoxStudyRefPhysicianList, _mPhysicianList, (UInt16)DPhysicianInfoVars.Label, _mStudyInfo._mRefPhysisian_IX, "-- Select --");
                        _mPhysician.mbListFillComboBox(comboBoxStudyPhysicianList, _mPhysicianList, (UInt16)DPhysicianInfoVars.Label, _mStudyInfo._mPhysisian_IX, "-- Select --");

                        CPhysicianInfo  physician = _mPhysician.mGetNodeFromList(_mPhysicianList, _mStudyInfo._mPhysisian_IX) as CPhysicianInfo;

                        textBoxStdInstructions.Text = physician == null ? "" : physician._mInstruction.mDecrypt();
                    }
 
                    comboBoxStudyMonitoringDaysList.Text = _mStudyInfo._mStudyMonitoringDays.ToString();

                    if (CDvtmsData._sEnumListStudyType != null)
                    {
                        CDvtmsData._sEnumListStudyType.mFillComboBox(comboBoxStudyTypeList, false, 0, false, ": ", _mStudyInfo._mStudyTypeCode, "--Select--");
                    }
                    if (CDvtmsData._sEnumListReportInterval != null)
                    {
                        CDvtmsData._sEnumListReportInterval.mFillCheckedListBox(checkedListBoxReportInterval, false, 0, false, ": ", _mStudyInfo._mReportIntervalSet, false);
                    }
                    if (CDvtmsData._sEnumListMctInterval != null)
                    {
                        CDvtmsData._sEnumListMctInterval.mFillCheckedListBox(checkedListBoxMctInterval, false, 0, false, ": ", _mStudyInfo._mMctIntervalSet, false);                      
                    }
                    textBoxStudyPhysicianInstruction.Text = _mStudyInfo._mStudyPhysicianInstruction;
                    if (CDvtmsData._sEnumListStudyProcedures != null)
                    {
                        CDvtmsData._sEnumListStudyProcedures.mFillListView(listViewStudyProcedures, false, 0, true, _mStudyInfo._mStudyProcCodeSet, true); // list only set
                    }
                    mFillAfterClientChange();

//                    string recInfo = _mStudyInfo._mNrRecords.ToString() + " records, " + _mStudyInfo._mNrReports.ToString() + " reports";
  //                  labelRecInfo.Text = recInfo;
                    mFillRecInfo();
                    mFillStudyPermissions();

                    panelCreateInfo.Visible = true;

                    labelStudyCreated.Text = CDvtmsData.sGetChangedBy(_mStudyInfo.mCreatedUTC, _mStudyInfo.mCreatedBy_IX);
                    labelStudyChanged.Text = CDvtmsData.sGetChangedBy(_mStudyInfo.mChangedUTC, _mStudyInfo.mChangedBy_IX);
                }
                catch ( Exception ex )
                {
                    CProgram.sLogException("FillFormStudy failed", ex);
                }
            }
        }

        bool mbCheckStudy(out string AErrorText, bool AbShowError)
        {
            string errorText = "init form failed";

            if( _mStudyInfo != null )
            {
                errorText = "";

                if ( _mStudyInfo._mClient_IX == 0)
                {
                    errorText = " Missing Client";
                }
/*                if( _mStudyInfo._mStudyStartDate.Year < 1801)//2016 )
                {
                    if( errorText != null && errorText.Length > 0 )
                    {
                        errorText += ", ";
                    }
                    errorText += " Bad Start Date";
                }
*/                if (AbShowError && errorText.Length > 0)
                {
                    CProgram.sPromptWarning(true, "Add Study", errorText);
                }
            }

            AErrorText = errorText;

            return errorText.Length == 0;
        }

        public void mReadFormStudy()
        {
            if (_mStudyInfo != null)
            {
                try
                {
                    _mStudyInfo._mStudyRemarkLabel = textBoxStudyNote.Text;

                    _mStudyInfo._mTrial_IX = mReadTrial();
                    if (_mClient != null)
                    {
                        _mClient.mbListParseComboBox(comboBoxStudyHospitalNameList, _mClientList, (UInt16)DClientInfoVars.Label, ref _mStudyInfo._mClient_IX);
                    }

                    _mStudyInfo._mTrial_IX = mReadTrial();
                    _mStudyInfo._mStudyStartDate = dateTimePickerStudyStartDate.Value;
                    _mStudyInfo._mHospitalRoom = textBoxHospitalRoom.Text;
                    _mStudyInfo._mHospitalBed = textBoxHospitalBed.Text;

                    if (_mPhysician != null)
                    {
                        _mPhysician.mbListParseComboBox(comboBoxStudyRefPhysicianList, _mPhysicianList, (UInt16)DPhysicianInfoVars.Label, ref _mStudyInfo._mRefPhysisian_IX);
                        _mPhysician.mbListParseComboBox(comboBoxStudyPhysicianList, _mPhysicianList, (UInt16)DPhysicianInfoVars.Label, ref _mStudyInfo._mPhysisian_IX);

                        if(_mStudyInfo._mPhysisian_IX == 0 )
                        {
                            textBoxStdInstructions.Text = "";
                        }
                        else
                        {
                            CPhysicianInfo physician = _mPhysician.mGetNodeFromList(_mPhysicianList, _mStudyInfo._mPhysisian_IX) as CPhysicianInfo;

                            textBoxStdInstructions.Text = physician == null ? "" : physician._mInstruction.mDecrypt();
                        }
                    }

                    UInt16.TryParse(comboBoxStudyMonitoringDaysList.Text, out _mStudyInfo._mStudyMonitoringDays);

                    if (CDvtmsData._sEnumListStudyType != null)
                    {
                        _mStudyInfo._mStudyTypeCode = CDvtmsData._sEnumListStudyType.mReadComboBoxCode(comboBoxStudyTypeList);
                    }
                    if (CDvtmsData._sEnumListReportInterval != null)
                    {
                        CDvtmsData._sEnumListReportInterval.mReadCheckedListBoxSet(checkedListBoxReportInterval, false, ": ", _mStudyInfo._mReportIntervalSet);
                    }
                    if (CDvtmsData._sEnumListMctInterval != null)
                    {
                        CDvtmsData._sEnumListMctInterval.mReadCheckedListBoxSet(checkedListBoxMctInterval, false, ": ", _mStudyInfo._mMctIntervalSet);
                    }
                    _mStudyInfo._mStudyPhysicianInstruction = textBoxStudyPhysicianInstruction.Text;
                    if (CDvtmsData._sEnumListStudyProcedures != null)
                    {
                        CDvtmsData._sEnumListStudyProcedures.mReadListViewSet(listViewStudyProcedures, true, _mStudyInfo._mStudyProcCodeSet); // list only set
                    }
                    _mStudyInfo._mStudyPermissions = CDvtmsData.sGetStudyPermissionsComboBox(comboBoxLocation, CDvtmsData._sStudyPermSiteGroup);

                }
                catch (Exception ex)
                {
                    CProgram.sLogException("ReadFormStudy failed", ex);
                }

            }
        }
        public void mFillFormPatient()
        {
            // zet waardes van _mPatientInfo in velden
            if (_mPatientInfo != null)
            {
                try
                {
                    Color patColor = textBoxSocSecNr.BackColor;

                    switch (CPatientInfo._sGetPatSocMode())
                    {
                        default:
                        case DPatSocID.PatPresent:
                            patColor = Color.LightGray;
                            break;
                        case DPatSocID.SocSec:
                            patColor = Color.Gray;
                            break;
                        case DPatSocID.PatID:
                            break;
                    }
                    textBoxPatientID.BackColor = patColor;

                    textBoxPatientID.Text = _mPatientInfo.mGetPatientIDValue();
                    textBoxSocSecNr.Text = _mPatientInfo._mSocSecNr.mDecrypt();
                    textBoxPatientFirstName.Text = _mPatientInfo._mPatientFirstName.mDecrypt();
                    textBoxPatientMiddleName.Text = _mPatientInfo._mPatientMiddleName.mDecrypt();
                    textBoxPatientLastName.Text = _mPatientInfo._mPatientLastName.mDecrypt();

                    UInt32 dateOfBirth = (UInt32)_mPatientInfo._mPatientDateOfBirth.mDecrypt();
                    UInt16 year, month, day;

                    if (CProgram.sbSplitYMD(dateOfBirth, out year, out month, out day))
                    {
                        textBoxPatientDOBDay.Text = day.ToString();
                        textBoxPatientDOBYear.Text = year.ToString();
                        textBoxAge.Text = CProgram.sCalcAgeYears(year, month, day, DateTime.Now).ToString();
                    }
                    else
                    {
                        textBoxPatientDOBDay.Text = "";
                        textBoxPatientDOBYear.Text = "";
                        textBoxAge.Text = "";
                    }
                    if (CDvtmsData._sEnumListMonthName != null) CDvtmsData._sEnumListMonthName.mFillComboBox(comboBoxPatientDOBMonth, false, 0, false, "", month.ToString(), null);

                    CPatientInfo.sFillCombobox(comboBoxPatientGenderList, _mPatientInfo._mPatientGender_IX);
                    //  Race is undefined

                    float heightMajor, heightMinor;

                    CProgram.mShowUserLength2(_mPatientInfo._mPatientHeightM, out heightMajor, out heightMinor);

                    textBoxPatientHeightMajor.Text = heightMajor.ToString();
                    textBoxPatientHeightMinor.Text = heightMinor.ToString("0.0");

                    labelUnitHeightMajor.Text = CProgram.mShowUserUnit();
                    labelUnitHeightMinor.Text = CProgram.mShowUserMinorUnit();

                    textBoxPatientWeight.Text = CProgram.sShowUserWeightValue(_mPatientInfo._mPatientWeightKg).ToString("0.0");
                    labelUnitWeight.Text = CProgram.sShowUserWeightUnit();
                    textBoxPatientLabel.Text = _mPatientInfo._mRemarkLabel;

                    textBoxPatientAddress.Text = _mPatientInfo._mPatientAddress.mDecrypt();
                    textBoxPatientZip.Text = _mPatientInfo._mPatientZip.mDecrypt();
                    textBoxPatientHouseNumber.Text = _mPatientInfo._mPatientHouseNumber.mDecrypt();

                    textBoxPatientCity.Text = _mPatientInfo._mPatientCity;
                    if (CDvtmsData._sEnumListCountries != null)
                    {
                        CDvtmsData._sEnumListCountries.mFillComboBox(comboBoxPatientCountryList, false, 0, false, "", _mPatientInfo._mPatientCountryCode, "-- Select--");
                    }
                    if (CDvtmsData._sEnumListStates != null)
                    {
                        CDvtmsData._sEnumListStates.mFillComboBox(comboBoxPatientStateList, false, 0, false, "", "", "");
                    }
                    comboBoxPatientStateList.Text = _mPatientInfo._mPatientState;
                    textBoxPatientPhone1.Text = _mPatientInfo._mPatientPhone1.mDecrypt();
                    textBoxPatientPhone2.Text = _mPatientInfo._mPatientPhone2.mDecrypt();
                    textBoxPatientCell.Text = _mPatientInfo._mPatientCell.mDecrypt();
                    textBoxPatientEmail.Text = _mPatientInfo._mPatientEmail.mDecrypt();
                    textBoxPatientSkype.Text = _mPatientInfo._mPatientSkype.mDecrypt();

                    if (CDvtmsData._sEnumListPatientHistories != null)
                    {
                        UInt16 group = (UInt16)CDvtmsData._sEnumListPatientHistories.mGetIntValue("$History");

                        CDvtmsData._sEnumListPatientHistories.mFillCheckedListBox(checkedListBoxHistoryList, true, group, false, "", _mPatientInfo._mHistoryStringSet, false);

                        group = (UInt16)CDvtmsData._sEnumListPatientHistories.mGetIntValue("$Allergies");

                        CDvtmsData._sEnumListPatientHistories.mFillCheckedListBox(checkedListBoxAllergiesList, true, group, false, "", _mPatientInfo._mAllergiesStringSet, false);

                        group = (UInt16)CDvtmsData._sEnumListPatientHistories.mGetIntValue("$Medication");

                        CDvtmsData._sEnumListPatientHistories.mFillCheckedListBox(checkedListBoxMedicationList, true, group, false, "", _mPatientInfo._mMedicationStringSet, false);

                        group = (UInt16)CDvtmsData._sEnumListPatientHistories.mGetIntValue("$Symptoms");

                        CDvtmsData._sEnumListPatientHistories.mFillCheckedListBox(checkedListBoxSymptomsList, true, group, false, "", _mPatientInfo._mSymptomsStringSet, false);
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("FillFormPatient failed", ex);
                }
            }
        }
        public void mReadForm()
        {

            mReadFormStudy();
            mReadFormPatient();
            mUpdateButtons();
        }
         public void mReadFormPatient()
        {
            if (_mPatientInfo != null)
            {
                try
                {
                    _mPatientInfo.mbSetPatientIDValue(textBoxPatientID.Text);
                    _mPatientInfo._mSocSecNr.mbEncrypt(textBoxSocSecNr.Text);
                    _mPatientInfo._mPatientFirstName.mbEncrypt(textBoxPatientFirstName.Text);
                    _mPatientInfo._mPatientMiddleName.mbEncrypt(textBoxPatientMiddleName.Text);
                    _mPatientInfo._mPatientLastName.mbEncrypt(textBoxPatientLastName.Text);
                    UInt16 age = 0;
                    mbReadBirthDateValuesCalcAge(out age);
                    DGender gender = DGender.Unknown;
                    CPatientInfo.sbParseGetGenderString(comboBoxPatientGenderList.Text, ref gender);

                    _mPatientInfo._mPatientGender_IX = (UInt16)gender;

                    //  Race is undefined

                    float heightMajor = 0.0F, heightMinor = 0.0F, weight = 0;

                    if (CProgram.sbParseFloat(textBoxPatientHeightMajor.Text, ref heightMajor)
                        && CProgram.sbParseFloat(textBoxPatientHeightMinor.Text, ref heightMinor))
                    {
                        _mPatientInfo._mPatientHeightM = CProgram.mConvertUserLength(heightMajor, heightMinor);
                    }
                    if (CProgram.sbParseFloat(textBoxPatientWeight.Text, ref weight))
                    {
                        _mPatientInfo._mPatientWeightKg = CProgram.sConvertUserWeight(weight);
                    }
                    _mPatientInfo._mRemarkLabel = textBoxPatientLabel.Text;

                    _mPatientInfo._mPatientAddress.mbEncrypt(textBoxPatientAddress.Text);
                    _mPatientInfo._mPatientZip.mbEncrypt(textBoxPatientZip.Text);
                    _mPatientInfo._mPatientHouseNumber.mbEncrypt(textBoxPatientHouseNumber.Text);
                    _mPatientInfo._mPatientState = comboBoxPatientStateList.Text;
                    _mPatientInfo._mPatientCity = textBoxPatientCity.Text;
                    if (CDvtmsData._sEnumListCountries != null)
                    {
                        _mPatientInfo._mPatientCountryCode = CDvtmsData._sEnumListCountries.mReadComboBoxCode(comboBoxPatientCountryList);
                    }

                    _mPatientInfo._mPatientPhone1.mbEncrypt(textBoxPatientPhone1.Text);
                    _mPatientInfo._mPatientPhone2.mbEncrypt(textBoxPatientPhone2.Text);
                    _mPatientInfo._mPatientCell.mbEncrypt(textBoxPatientCell.Text);
                    _mPatientInfo._mPatientEmail.mbEncrypt(textBoxPatientEmail.Text);
                    _mPatientInfo._mPatientSkype.mbEncrypt(textBoxPatientSkype.Text);

                    if (CDvtmsData._sEnumListPatientHistories != null && _mPatientInfo._mHistoryStringSet != null)
                    {
                        CDvtmsData._sEnumListPatientHistories.mReadCheckedListBoxSet(checkedListBoxHistoryList, false, "", _mPatientInfo._mHistoryStringSet);

                        CDvtmsData._sEnumListPatientHistories.mReadCheckedListBoxSet(checkedListBoxAllergiesList, false, "", _mPatientInfo._mAllergiesStringSet);

                        CDvtmsData._sEnumListPatientHistories.mReadCheckedListBoxSet(checkedListBoxMedicationList, false, "", _mPatientInfo._mMedicationStringSet);

                        CDvtmsData._sEnumListPatientHistories.mReadCheckedListBoxSet(checkedListBoxSymptomsList, false, "", _mPatientInfo._mSymptomsStringSet);
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("ReadFormPatient failed", ex);
                }

            }
        }

        private bool mbCheckPatient(out string ArErrorText, bool AbShowError)
        {
            string errorText = "Bad Init Patient";

            if( _mPatientInfo != null )
            {
                errorText = "";

                if (_mPatientInfo._mSocSecNr == null || _mPatientInfo._mSocSecNr.mbIsEmpty())
                {
                    errorText = " Missing Unique ID";

                }
                if (_mPatientInfo.mbIsPatientIDEmpty())
                {
                    if (radioButtonPatID.Checked)
                    {
                        if (errorText != null && errorText.Length > 0)
                        {
                            errorText += ", ";
                        }
                        errorText += " Empty Patient ID used as Ref ID";
                    }
                }
                /*                if (_mPatientInfo._mPatientID == null || _mPatientInfo._mPatientID.mbIsEmpty())
                                {
                                    errorText += " Missing Patient ID";
                                }
                */
                UInt16 age = 0;
                if( false == mbReadBirthDateValuesCalcAge(out age))
                {
                    if (errorText != null && errorText.Length > 0)
                    {
                        errorText += ", ";
                    }
                    errorText += " Patient DOB not ok"; 
                }

                if ( AbShowError && errorText.Length > 0 )
                {
                    CProgram.sPromptWarning(true, "Add Patient", errorText);
                }
            }
            ArErrorText = errorText;

            return errorText.Length == 0;
        }

        private bool mbCheckForm( string AState, bool AbShowError )
        {
            bool bOk = true;
            string errorText = "";
            string devTxt = "";

            if (radioButtonOther.Checked)
            {
                //                if (_mDeviceInfo._mActiveRefID == null || _mDeviceInfo._mActiveRefID.mbIsEmpty())
                if( textBoxRefIdOther.Text.Length == 0)
                {
                    devTxt = "!Other empty!";
                    bOk = false;
                }
            }
            labelDeviceError.Text = devTxt;
            if ( mbCheckStudy(out errorText, AbShowError))
            {
                labelStudyError.Text = "";
            }
            else
            {
                labelStudyError.Text = AState + ": " + errorText;
                bOk = false;
            }
            if (mbCheckPatient(out errorText, AbShowError))
            {
                labelPatientError.Text = "";
            }
            else
            {
                labelPatientError.Text = AState + ": " + errorText;
                bOk = false;
            }
            return bOk;
        }

        private void label1_Click(object sender, EventArgs e)
        {
            
        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void comboBox5_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void CPatientForm_Load(object sender, EventArgs e)
        {

        }

        private void label19_Click(object sender, EventArgs e)
        {

        }

        private void textBox18_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBox8_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox17_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBox9_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox14_TextChanged(object sender, EventArgs e)
        {

        }

        private void label36_Click(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label41_Click(object sender, EventArgs e)
        {

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox21_TextChanged(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void panel7_Paint(object sender, PaintEventArgs e)
        {
        
        }

        private void label17_Click(object sender, EventArgs e)
        {

        }

        private void label39_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void label46_Click(object sender, EventArgs e)
        {

        }

        private void comboBox6_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label48_Click(object sender, EventArgs e)
        {

        }

        private void dateTimePickerStudyStartDate_ValueChanged(object sender, EventArgs e)
        {

        }

        private void dateTimePickerStudyEndDate_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click_2(object sender, EventArgs e)
        {

        }

        private UInt32 mPhysicianInfo( ComboBox AComboBox, TextBox AInfoTextBox)
        {
            UInt32 ix = 0;
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;
            if (bCtrl)
            {

                _mPhysician.mbListParseComboBox(AComboBox, _mPhysicianList, (UInt16)DPhysicianInfoVars.Label, ref ix);

                //if (ix > 0)
                {
                    CPhysicianInfo physician = _mPhysician.mGetNodeFromList(_mPhysicianList, ix) as CPhysicianInfo;

                    CPhysiciansForm form = new CPhysiciansForm(physician, true);
                    //            CUserListForm form = new CUserListForm();

                    if (form != null)
                    {
                        form.ShowDialog();

                        if (form._mbSelected)
                        {
                            ix = form._mInfo.mIndex_KEY;
                            AComboBox.Text = form._mInfo._mLabel;
                            if(AInfoTextBox != null )
                            {
                                AInfoTextBox.Text = form._mInfo._mInstruction.mDecrypt();
                            }
                        }
                    }
                }
            }
            return ix;
        }

        private void labelStudyRefPhysician_Click(object sender, EventArgs e)
        {
            mPhysicianInfo(comboBoxStudyRefPhysicianList, null);
        }

        private void labelStudyLabel_Click(object sender, EventArgs e)
        {

        }

        private void checkedListBoxMedicationList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void buttonPrintPreviousFindings_Click(object sender, EventArgs e)
        {
            if( _mStudyInfo.mIndex_KEY > 0 && _mPatientInfo.mIndex_KEY > 0 && _mStudyInfo._mStudyRecorder_IX > 0 )
            {
                DialogResult = DialogResult.OK;
            }
            Close();
        }

        private void buttonRandom_Click(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            string dtStr = dt.ToString("yyyyMMddHHmmss");   // 14 char

            if ( _mStudyInfo != null )
            {
                _mStudyInfo.mClear();
                _mStudyInfo._mStudyRemarkLabel = "Std"+dtStr;

                _mStudyInfo._mStudyStartDate = dt;
                _mStudyInfo._mHospitalRoom = "HR"+dt.Month.ToString();
                _mStudyInfo._mHospitalBed = "HB" + dt.Month.ToString();


                _mStudyInfo._mStudyMonitoringDays = (UInt16)dt.Month;

                _mStudyInfo._mStudyPhysicianInstruction = "Instr" + dtStr;
                if (CDvtmsData._sEnumListStudyProcedures != null)
                {
                   _mStudyInfo._mStudyProcCodeSet.mAddValue("I49.2"); // list only set
                }
            }
            if (_mPatientInfo != null)
            {
                _mPatientInfo.mClear();
                _mPatientInfo.mbSetPatientIDValue("pi" + dtStr.Substring(2));
                _mPatientInfo._mSocSecNr.mbEncrypt("ui"+ dtStr.Substring(2));
                _mPatientInfo._mPatientFirstName.mbEncrypt("FN"+ dtStr);
                _mPatientInfo._mPatientMiddleName.mbEncrypt("MN"+dtStr.Substring(8));
                _mPatientInfo._mPatientLastName.mbEncrypt("LN"+dtStr);

                UInt16 year = (UInt16)(dt.Year - 37);
                _mPatientInfo._mPatientDateOfBirth.mEncrypt((int)CProgram.sCalcYMD(year, (UInt16)dt.Month, (UInt16)dt.Day));
                _mPatientInfo._mPatientBirthYear = year;

               _mPatientInfo._mPatientGender_IX = (UInt16)((dt.Second % 2 ) +1 );

                _mPatientInfo._mPatientHeightM = 1.0F + dt.Month * 0.1F;
                _mPatientInfo._mPatientWeightKg = dt.Month * 10.0F;
                
                _mPatientInfo._mRemarkLabel = "lbl" + dtStr;

                _mPatientInfo._mPatientAddress.mbEncrypt("Adr"+ dtStr);
                _mPatientInfo._mPatientZip.mbEncrypt("Zip"+ dt.Second.ToString());
                _mPatientInfo._mPatientHouseNumber.mbEncrypt("Hnr"+ dt.Month.ToString());
                _mPatientInfo._mPatientState = "State" + dt.Second.ToString();
                _mPatientInfo._mPatientCity = "City" + dt.Second.ToString();

                if (CDvtmsData._sEnumListCountries != null)
                {
                    _mPatientInfo._mPatientCountryCode = "NL";
                }

                _mPatientInfo._mPatientPhone1.mbEncrypt("P1-"+dtStr);
                _mPatientInfo._mPatientPhone2.mbEncrypt("P2-" + dtStr);
                _mPatientInfo._mPatientCell.mbEncrypt("C-" + dtStr);
                _mPatientInfo._mPatientEmail.mbEncrypt("E" + dtStr + "@email.com");
                _mPatientInfo._mPatientSkype.mbEncrypt("S-" + dtStr);

                if (CDvtmsData._sEnumListPatientHistories != null && _mPatientInfo._mHistoryStringSet != null)
                {
                    _mPatientInfo._mHistoryStringSet.mAddValue("Alc");
                    _mPatientInfo._mAllergiesStringSet.mAddValue("Insl");
                    _mPatientInfo._mMedicationStringSet.mAddValue("Betb");
                    _mPatientInfo._mSymptomsStringSet.mAddValue("Fatq");
                }
            }
            mFillForm();
            mbCheckForm("Random", false);
        }

  
        public bool mbReadBirthDateValuesCalcAge( out UInt16 ArAgeYears)
        {
            bool bOk = false;
            UInt16 age = 0;

            try
            {
                // a lot of test to protect entry of invalid date time

                 UInt32 ymd = 0;

                if (CDvtmsData.sbParseBirthDate(out ymd, textBoxPatientDOBYear.Text, 1900, comboBoxPatientDOBMonth.Text, textBoxPatientDOBDay.Text))
                {
                    if (ymd == 0)
                    {
                        if (_mPatientInfo != null)
                        {
                            _mPatientInfo._mPatientDateOfBirth.mEncrypt(0);
                            _mPatientInfo._mPatientBirthYear = 0;
                        }
                        age = 0;
                    }
                    else
                    {
                        if (_mPatientInfo != null)
                        {
                            _mPatientInfo._mPatientDateOfBirth.mEncrypt((int)ymd);
                            _mPatientInfo._mPatientBirthYear = CProgram.sGetYear(ymd);
                        }
                        age = CProgram.sCalcAgeYears(ymd, DateTime.Now);
                    }
                    bOk = true;
                }
            }
            catch (Exception ex)
            {
                bOk = false;
                age = 0;
            }
            ArAgeYears = age;
            return bOk;
        }


        private void textBoxPatientDOBYear_TextChanged(object sender, EventArgs e)
        {
            UInt16 age = 0;
            mbReadBirthDateValuesCalcAge( out age);

            textBoxAge.Text = age == 0 ? "" : age.ToString();
        }

        private void textBoxPatientDOBDay_TextChanged(object sender, EventArgs e)
        {
            UInt16 age = 0;
            mbReadBirthDateValuesCalcAge(out age);
 
            textBoxAge.Text = age == 0 ? "" : age.ToString();
        }

        private void comboBoxPatientDOBMonth_TextChanged(object sender, EventArgs e)
        {
            UInt16 age = 0;
            mbReadBirthDateValuesCalcAge(out age);

            textBoxAge.Text = age == 0 ? "" : age.ToString();
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            if (_mStudyInfo != null)
            {
                _mStudyInfo.mClear();
                _mStudyInfo.mbActive = true;
                _mStudyInfo._mStudyStartDate = DateTime.Now;

            }
            if (_mPatientInfo != null)
            {
                _mPatientInfo.mClear();
                _mPatientInfo.mbActive = true;
            }
            mFillForm();
            textBoxRefIdOther.Text = "";
            mbCheckForm("Cleared", false);
        }

        private void buttonDup_Click(object sender, EventArgs e)
        {
            if (_mStudyInfo != null)
            {
                _mStudyInfo.mIndex_KEY = 0;
            }
            if (_mPatientInfo != null)
            {
            }
            mFillForm();
            mbCheckForm("Duplicated", false);

        }

        private void mSetRefIDCheckbox(bool AbForce)
        {
            if ((AbForce || _mStudyInfo.mIndex_KEY == 0 )
                && _mDeviceInfo.mIndex_KEY != 0
                && _mDeviceInfo._mRefIdMode < (UInt16)DRefIDModeEnum.NrEnums)
            { // new study set refID from device
                switch ((DRefIDModeEnum)_mDeviceInfo._mRefIdMode)
                {
                    case DRefIDModeEnum.NotActive:
                        // set default as with old software
                        {
                            string devPath;
                            DDeviceType devType = CDvtmsData.sFindDeviceUnitType(out devPath, _mDeviceInfo._mDeviceSerialNr);

                            if( devType == DDeviceType.TZ)
                            {
                                radioButtonNone.Checked = true;
                            }
                            else
                            {
                                radioButtonPatID.Checked = true;
                            }
                        }
                        
                        break;
                    case DRefIDModeEnum.None:
                        radioButtonNone.Checked = true;
                        break;
                    case DRefIDModeEnum.PatientID:
                        radioButtonPatID.Checked = true;
                        break;
                    case DRefIDModeEnum.UniqueID:
                        radioButtonUniqueID.Checked = true;
                        break;
                    case DRefIDModeEnum.Other:
                        radioButtonOther.Checked = true;
                        break;
                    case DRefIDModeEnum.Study:              // S123456
                        break;
                    case DRefIDModeEnum.StudyInitialsYear:   // S123456FL1987
                        break;
                }
            }
        }
        private string mCheckRefID()
        {
            string s = "";

            if (_mDeviceInfo.mIndex_KEY != 0 
                && _mDeviceInfo._mRefIdMode < (UInt16)DRefIDModeEnum.NrEnums)
            { // check RefID is as Device RefID
                switch ((DRefIDModeEnum)_mDeviceInfo._mRefIdMode)
                {
                    case DRefIDModeEnum.NotActive:
                        // do nothing
                        break;
                    case DRefIDModeEnum.None:
                        if( false == radioButtonNone.Checked)
                        {
                            s = ", RefID!=None";
                        }
                        break;
                    case DRefIDModeEnum.PatientID:
                        if (false == radioButtonPatID.Checked)
                        {
                            s = ", RefID!=PatID";
                        }
                        break;
                    case DRefIDModeEnum.UniqueID:
                        if (false == radioButtonUniqueID.Checked)
                        {
                            s = ", RefID!=Unique";
                        }
                        break;
                    case DRefIDModeEnum.Other:
                        if (false == radioButtonOther.Checked)
                        {
                            s = ", RefID!=Other";
                        }
                        break;
                    case DRefIDModeEnum.Study:              // S123456
                        break;
                    case DRefIDModeEnum.StudyInitialsYear:   // S123456FL1987
                        break;
                }
            }
            if(radioButtonOther.Checked)
            {
                if( _mDeviceInfo._mActiveRefID == null || _mDeviceInfo._mActiveRefID.mbIsEmpty())
                {
                     s += " !RefID empty!";
                }
            }
            return s;
        }


        private bool mbCheckUpdateDevice( out UInt32 ArDeviceIX )
        {
            bool bUpdateStudy = false;
            UInt32 deviceIX = 0;

            try
            {

                bool bStudyRec = _mStudyInfo._mStudyRecorder_IX > 0;
                CDeviceInfo selDevice = null;
                UInt32 nStudyDevice = 0;

                string deviceStr = comboBoxStudySerialNumberList.Text;

                if (deviceStr != null && deviceStr.Length > 1 && deviceStr[0] != '-')
                {
                    if (_mDeviceList != null)
                    {
                        selDevice = _mDeviceInfo.mGetNodeFromList(_mDeviceList, (UInt16)DDeviceInfoVars.DeviceSerialNr, deviceStr) as CDeviceInfo;
                    }
                    if (selDevice != null)
                    {
                        if (false == _mDeviceInfo.mbDoSqlSelectIndex(selDevice.mIndex_KEY, _mDeviceInfo.mGetValidMask(true)))
                        {
                            _mDeviceInfo.mIndex_KEY = 0;
                        }
                        else
                        {
                            bUpdateStudy = true;
                        }
                    }
                    else
                    {
                        if (_mActiveDevice != null && _mActiveDevice._mDeviceSerialNr == deviceStr)
                        {
                            _mDeviceInfo.mbCopyFrom(_mActiveDevice);
                            bUpdateStudy = _mActiveDevice.mIndex_KEY > 0;
                        }

                    }
                }
                if (_mDeviceInfo.mIndex_KEY == 0)//|| _mStudyInfo.mIndex_KEY != 0 && _mDeviceInfo._mActiveStudy_IX != _mStudyInfo.mIndex_KEY)
                {
                    labelRecorderID.Text = "";
                    labelRecorderStudyNr.Text = "";
                    labelRecorderRefID.ForeColor = SystemColors.WindowText;
                    labelRecorderRefID.Text = "";
                    labelRecorderState.Text = "";
                    labelRecorderStartDate.Text = "";
                    labelRecordEndDate.Text = "";
                    labelDeviceClient.Text = "";
                    labelDeviceRemLabel.Text = "";
                    labelDeviceChanged.Text = "";
                    labelAddStudyMode.Text = "";
                    labelDefRefIdMode.Text = "";
                    labelNoMct.Text = "";
                    labelUseRefID.ForeColor = SystemColors.WindowText;
                }
                else
                {
                    deviceIX = _mDeviceInfo.mIndex_KEY;
                    bool bAddCurrentStudy = _mDeviceInfo._mAddStudyMode == (UInt16)DAddStudyModeEnum.ActiveStudy;
                    bool bStudy = _mDeviceInfo._mActiveStudy_IX > 0;
                    bool bNotStudy = _mDeviceInfo._mActiveStudy_IX != _mStudyInfo.mIndex_KEY;
                    bool bActive = _mDeviceInfo._mRecorderStartUTC <= DateTime.UtcNow && _mDeviceInfo._mRecorderEndUTC >= DateTime.UtcNow;
                    string activeStr = bStudy && bActive ? "  Active"  : "";
                    string clientName = "";
                    Color useRefIdColor = bAddCurrentStudy ? ( bNotStudy ? Color.Blue : SystemColors.WindowText) : Color.Orange;

                    if(_mDeviceInfo._mState_IX == (UInt16)DDeviceState.Available)
                    {
                        useRefIdColor = bAddCurrentStudy ? Color.DarkGreen : Color.LightGreen;
                    }

                    if (_mDeviceInfo._mClient_IX != 0)
                    {
                        if (_mClient.mbLoadIndexFromList(_mClientList, _mDeviceInfo._mClient_IX))
                        {
                            clientName = _mClient._mLabel;
                        }
                        else
                        {
                            clientName = "?C" + _mDeviceInfo._mClient_IX.ToString();
                        }
                    }

                    string extraStudy = bActive && bNotStudy ? "<> " + _mStudyInfo.mIndex_KEY.ToString() : "";

                    if (bAddCurrentStudy)
                    {
                        bUpdateStudy &= bActive == false || bNotStudy == false;
                        bUpdateStudy &= _mDeviceInfo._mState_IX == (UInt16)DDeviceState.Available || _mDeviceInfo._mState_IX == (UInt16)DDeviceState.Assigned && bNotStudy == false;

                        /*                        if( false == bNotStudy && deviceIX == _mStudyInfo._mStudyRecorder_IX)
                                                {
                                                    // update device RefID


                                                    string refID = "";

                                                    if (radioButtonOther.Checked)
                                                    {
                                                        refID = textBoxRefIdOther.Text;
                                                    }
                                                    else if (radioButtonPatID.Checked) refID = textBoxPatientID.Text;
                                                    else if (radioButtonUniqueID.Checked) refID = textBoxSocSecNr.Text;

                                                    _mDeviceInfo._mActiveRefID.mbEncrypt(refID);


                                                }
                        */
                    }
                    else
                    {
                        bUpdateStudy &= _mDeviceInfo._mState_IX == (UInt16)DDeviceState.Available; 
                    }
                    labelRecorderID.ForeColor = _mDeviceInfo.mIndex_KEY == _mStudyInfo._mStudyRecorder_IX ? SystemColors.WindowText : Color.Blue;
                    labelRecorderID.Text = _mDeviceInfo._mDeviceSerialNr;
                    labelRecorderStudyNr.ForeColor = _mStudyInfo.mIndex_KEY == _mDeviceInfo._mActiveStudy_IX ? SystemColors.WindowText : Color.Blue;
                    labelRecorderStudyNr.Text = bStudy ? _mDeviceInfo._mActiveStudy_IX.ToString() + extraStudy: "";
                    labelRecorderRefID.ForeColor = _mDeviceInfo.mCheckColorAddStudyModeRefID();
                    labelRecorderRefID.Text = _mDeviceInfo._mActiveRefID.mDecrypt();
                    labelRecorderState.ForeColor = bStudy && bActive ? SystemColors.WindowText : Color.Blue;
                    labelRecorderState.Text = CDeviceInfo.sGetStateString(_mDeviceInfo._mState_IX) + activeStr;
                    labelRecorderStartDate.Text = bStudy == false || _mDeviceInfo._mRecorderStartUTC == DateTime.MinValue ? "" : CProgram.sUtcToLocalString(_mDeviceInfo._mRecorderStartUTC);
                    labelRecordEndDate.ForeColor = _mDeviceInfo._mRecorderEndUTC <= DateTime.UtcNow ? Color.Blue : SystemColors.WindowText;
                    labelRecordEndDate.Text = bStudy == false || _mDeviceInfo._mRecorderEndUTC == DateTime.MinValue ? "" : CProgram.sUtcToLocalString(_mDeviceInfo._mRecorderEndUTC);
                    labelDeviceClient.Text = clientName;
                    labelDeviceRemLabel.Text = _mDeviceInfo._mRemarkLabel;
                    labelDeviceChanged.Text = CDvtmsData.sGetChangedBy(_mDeviceInfo.mChangedUTC, _mDeviceInfo.mChangedBy_IX);
                    labelAddStudyMode.Text = _mDeviceInfo.mGetAddStudyModeLabel(false);
                    labelDefRefIdMode.Text = "def: " + CDeviceInfo.sGetRefIDModeLabel( _mDeviceInfo._mRefIdMode );
                    labelNoMct.Text = _mDeviceInfo.mGetAddStudyModeMctLabel();
                    labelUseRefID.ForeColor = SystemColors.WindowText;

                    labelUseRefID.ForeColor = useRefIdColor;

                    mSetRefIDCheckbox(false);

                }
              labelStartRec.Text = bStudyRec ? CProgram.sUtcToLocalString(_mStudyInfo._mRecorderStartUTC) : "";
                labelEndRec.Text = bStudyRec ? CProgram.sUtcToLocalString(_mStudyInfo._mRecorderEndUTC) : "";

                string s = "";
                if (_mStudyInfo._mStudyRecorder_IX > 0 && deviceStr != _mActiveDevice._mDeviceSerialNr) s = "!= ";
                if (bUpdateStudy) s += "√";
                labelDeviceCmp.Text = s;

                if( bStudyRec && _mStudyInfo._mStudyRecorder_IX > 0 )
                {
                    CDeviceInfo searchDevice = new CDeviceInfo(CDvtmsData.sGetDBaseConnection() );

                    if (searchDevice != null)
                    {

                        UInt64 loadMask = CSqlDataTableRow.sGetMask((UInt16)DDeviceInfoVars.DeviceSerialNr);
                        UInt64 maskWhere = CSqlDataTableRow.sGetMask((UInt16)DDeviceInfoVars.ActiveStudy);
                        _mDeviceInfo._mActiveStudy_IX = _mStudyInfo.mIndex_KEY;
                        bool bError;
                        if (_mDeviceInfo.mbDoSqlCount(out bError,out nStudyDevice, maskWhere, true, ""))
                        {
                            //_mDeviceInfo.mbDoSqlSelectList(out bError, out _mDeviceList, loadMask, maskWhere, true, "",);
                        }
                    }

                }
                labelNdevices.Text = nStudyDevice.ToString();

            }
            catch (Exception ex)
            {
                CProgram.sLogException("CheckUpdateDevice failed", ex);
            }

            ArDeviceIX = deviceIX;

            return bUpdateStudy;
        }

        private void buttonAddUpdate_Click(object sender, EventArgs e)
        {
            mAddUpdate();
        }
        private void mAddUpdate()
        { 
 //           UInt32 index;
 //           mGetValues(out index);
//
            // check values

            if(_mPatientInfo != null && _mStudyInfo != null )
            {
                bool bUpdatedStudy = false;
                bool bUpdatedPatient = false;
                bool bUpdatedDevice = true;

                mReadForm();
                UInt32 deviceIX = 0;
                bool bUpdateDevice = mbCheckUpdateDevice(out deviceIX); // includes check to see if device may be updated =>_mDeviceInfo contains device

                _mPatientInfo._mState_IX = (UInt16)DPatientState.StudyStart;
                _mPatientInfo.mbActive = true;

                if( mbCheckForm( _mStudyInfo.mIndex_KEY == 0 ? "Add" : "Update", true))
                {
                    if( checkBoxAnonymizePatientInfo.Checked )
                    {
                        labelPatientError.Text = "Anonimyzed Patient not updated!";
                    }
                    else
                    if (_mPatientInfo.mIndex_KEY == 0)
                    {
                        if (_mPatientInfo._mSocSecNr != null && _mPatientInfo._mSocSecNr.mbNotEmpty()
                            && _mPatientInfo.mbDoSqlInsert())
                        {
                            labelPatientNr.Text = _mPatientInfo.mIndex_KEY.ToString();
                            _mStudyInfo._mPatient_IX = _mPatientInfo.mIndex_KEY;
                            labelPatientError.Text = "created Patient";
                        }
                        else
                        {
                            labelPatientError.Text = "Failed to Add Patient (use patient search to check if patient already exists)";
                        }
                    }
                    else
                    {
                        if (_mPatientInfo.mbDoSqlUpdate(_mPatientInfo.mMaskValid, _mPatientInfo.mbActive))
                        {
                            labelPatientNr.Text = "+" + _mPatientInfo.mIndex_KEY.ToString();
                            _mStudyInfo._mPatient_IX = _mPatientInfo.mIndex_KEY;
                            labelPatientError.Text = "Updated";
                            bUpdatedPatient = true;
                        }
                        else
                        {
                            labelPatientError.Text = "Failed to Update Patient";
                            CProgram.sPromptError(true, "Add Study & Patient", "Failed to update Patient in database");
                        }
                    }

                    if (bUpdateDevice)
                    {
                        if (_mDeviceInfo != null && bUpdateDevice && deviceIX > 0)  // bUpdateDevice has 
                        {
                            bool bAddCurrentStudy = _mDeviceInfo._mAddStudyMode == (UInt16)DAddStudyModeEnum.ActiveStudy;

                            _mStudyInfo._mStudyRecorder_IX = deviceIX;

                            if (_mDeviceInfo.mIndex_KEY != deviceIX)
                            {
                                labelDeviceError.Text = " Device not collected";
                                bUpdateDevice = false;
                            }
                            else
                            {
                                if (_mDeviceInfo._mState_IX == (UInt16)DDeviceState.Available
                                    || _mDeviceInfo._mState_IX == (UInt16)DDeviceState.Assigned && _mDeviceInfo._mActiveStudy_IX == _mStudyInfo.mIndex_KEY)
                                {
                                    //_mDeviceInfo.mIndex_KEY = _mStudyInfo._mStudyRecorder_IX;
                                    _mDeviceInfo._mActiveStudy_IX = _mStudyInfo.mIndex_KEY;
                                    _mStudyInfo._mStudyRecorder_IX = deviceIX;

                                    if (bAddCurrentStudy)
                                    {
                                        string refID = "";

                                        if (radioButtonOther.Checked)
                                        {
                                            refID = textBoxRefIdOther.Text;
                                        }
                                        else if (radioButtonPatID.Checked) refID = textBoxPatientID.Text;
                                        else if (radioButtonUniqueID.Checked) refID = textBoxSocSecNr.Text;

                                        _mDeviceInfo._mActiveRefID.mbEncrypt(refID);

                                        _mDeviceInfo._mActiveRefName.mbEncrypt("");
                                        _mStudyInfo._mRecorderStartUTC = _mDeviceInfo._mRecorderStartUTC = CProgram.sLocalToUTC(CProgram.sUseAsLocal(_mStudyInfo._mStudyStartDate));
                                        _mStudyInfo._mRecorderEndUTC = _mDeviceInfo._mRecorderEndUTC
                                                    = DateTime.SpecifyKind(DateTime.UtcNow.AddYears(10), DateTimeKind.Utc);    // sql does not exept the max value

                                        _mDeviceInfo._mState_IX = (UInt16)DDeviceState.Assigned;
                                    }
                                    else
                                    {
                                        string refID = _mDeviceInfo.mGetAddStudyModeRefID();
                                        
                                        _mDeviceInfo._mActiveRefID.mbEncrypt(refID);

                                        _mDeviceInfo._mActiveRefName.mbEncrypt("");

                                        _mStudyInfo._mRecorderStartUTC = CProgram.sLocalToUTC(CProgram.sUseAsLocal(_mStudyInfo._mStudyStartDate));
                                        _mStudyInfo._mRecorderEndUTC = DateTime.SpecifyKind(DateTime.UtcNow.AddYears(10), DateTimeKind.Utc);    // sql does not exept the max value

                                        _mDeviceInfo._mRecorderStartUTC = DateTime.UtcNow;  // time is last assign and past active
                                        _mDeviceInfo._mRecorderEndUTC = _mDeviceInfo._mRecorderStartUTC.AddSeconds(-1);

                                        // device stays at available _mDeviceInfo._mState_IX = (UInt16)DDeviceState.Assigned;
                                    }
                                  }
                                else
                                {
                                    labelDeviceError.Text = " Device not updated";
                                    bUpdateDevice = false;
                                }

                            }
                        }
                    }
                    if (_mStudyInfo.mIndex_KEY == 0)
                    {
                        if(_mStudyInfo._mPatient_IX == 0)
                        {
                            labelStudyError.Text = "Failed: No patient in database";
                            CProgram.sPromptError(true, "Add Study & Patient", "Failed, patient not added to database (use search patient to see if patient already exists)!");

                        }
                        else if (_mStudyInfo.mbDoSqlInsert())
                        {
                            textBoxStudyNr.Text = _mStudyInfo.mIndex_KEY.ToString();

                            labelStudyError.Text += "created Study";
                            bUpdatedStudy = true;
                            labelStudyChanged.Text = CDvtmsData.sGetChangedBy(_mStudyInfo.mChangedUTC, _mStudyInfo.mChangedBy_IX);

                        }
                        else
                        {
                            labelStudyError.Text = "Failed to Add Study";
                            CProgram.sPromptError(true, "Add Study & Patient", "Failed to add Study to database");
                        }
                        labelStudyNr.Text = textBoxStudyNr.Text;
                    }
                    else
                    {
                        UInt64 saveMask = _mStudyInfo.mMaskValid;
                        UInt64 counterMask = CSqlDataTableRow.sGetMask((UInt16)DStudyInfoVars.NrRecords)
                                | CSqlDataTableRow.sGetMask((UInt16)DStudyInfoVars.NrReports);
                        saveMask = saveMask & (~counterMask);

                        if (_mStudyInfo.mbDoSqlUpdate(saveMask, _mStudyInfo.mbActive))
                        {
                            textBoxStudyNr.Text = "+" + _mStudyInfo.mIndex_KEY.ToString();
                            labelStudyError.Text = "Updated";
                            bUpdatedStudy = true;
                            labelStudyChanged.Text = CDvtmsData.sGetChangedBy(_mStudyInfo.mChangedUTC, _mStudyInfo.mChangedBy_IX);

                        }
                        else
                        {
                            labelStudyError.Text = "Failed to Update Study";
                            CProgram.sPromptError(true, "Add Study & Patient", "Failed to update Study in database");

                        }
                        labelStudyNr.Text = textBoxStudyNr.Text;
                    }
                    if (bUpdatedStudy  && bUpdateDevice )
                    {
                        // todo: check ref id Study -> calc refID

                        UInt64 saveMask = CSqlDataTableRow.sGetMask((UInt16)DDeviceInfoVars.ActiveStudy)
                              | CSqlDataTableRow.sGetMask((UInt16)DDeviceInfoVars.ActiveRefName)
                              | CSqlDataTableRow.sGetMask((UInt16)DDeviceInfoVars.ActiveRefID)
                              | CSqlDataTableRow.sGetMask((UInt16)DDeviceInfoVars.RecorderStartUTC)
                              | CSqlDataTableRow.sGetMask((UInt16)DDeviceInfoVars.RecorderEndUTC)
                              | CSqlDataTableRow.sGetMask((UInt16)DDeviceInfoVars.State_IX);

                        _mDeviceInfo._mActiveStudy_IX = _mStudyInfo.mIndex_KEY; // study only known here
                        
                        if (_mDeviceInfo.mbDoSqlUpdate(saveMask, true))
                        {
                            labelDeviceError.Text = " Device updated";
                            _mActiveDevice.mbCopyFrom(_mDeviceInfo);
                        }
                        else
                        {
                            labelDeviceError.Text = " Device failed update";
                            CProgram.sPromptError(true, "Add Study & Patient", "Failed to update device in database");
                            bUpdatedDevice = false;
                        }
                    }
                    else
                    {
                        labelDeviceError.Text = " Device not updated";
                    }

                }
                labelDeviceError.Text += mCheckRefID();

                mbCheckUpdateDevice(out deviceIX);  // update device info (study etc has changed
                if(bUpdatedStudy && bUpdatedPatient && bUpdatedDevice)
                {
                    // we can update records if needed
                    mbUpdateStudyRecords();
                }
            }
            mUpdateButtons();
        }

        private void buttonAddICD_Click(object sender, EventArgs e)
        {
            if (_mStudyInfo != null)
            {
                CicdCodeForm form = new CicdCodeForm(_mStudyInfo._mStudyProcCodeSet);

                if( form != null )
                {
                    form.ShowDialog();
                    if (form._mSelectedSet != null)
                    {
                        _mStudyInfo._mStudyProcCodeSet = form._mSelectedSet;
                        if (CDvtmsData._sEnumListStudyProcedures != null)
                        {
                            CDvtmsData._sEnumListStudyProcedures.mFillListView(listViewStudyProcedures, false, 0, true, _mStudyInfo._mStudyProcCodeSet, true); // list only set
                        }
                        //mFillFormStudy();
                        mUpdateButtons();
                    }
                }
            }

        }

        private void buttonFindings_Click(object sender, EventArgs e)
        {
            if (_mPatientInfo != null && _mStudyInfo != null)
            {
                UInt32 studyNr = _mStudyInfo.mIndex_KEY;

                if (studyNr > 0)
                {
                    CPreviousFindingsForm form = new CPreviousFindingsForm(studyNr, true, 0, 0);

                    if (form != null)
                    {
                        form.ShowDialog();
                    }
                }
            }
        }

        private void comboBoxStudyMonitoringDaysList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBoxStudyPhysicianList_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void comboBoxStudyPhysicianList_TextChanged(object sender, EventArgs e)
        {
            if (_mPhysician != null)
            {
                _mPhysician.mbListParseComboBox(comboBoxStudyPhysicianList, _mPhysicianList, (UInt16)DPhysicianInfoVars.Label, ref _mStudyInfo._mPhysisian_IX);

                if (_mStudyInfo._mPhysisian_IX == 0)
                {
                    textBoxStdInstructions.Text = "";
                }
                else
                {
                    CPhysicianInfo physician = _mPhysician.mGetNodeFromList(_mPhysicianList, _mStudyInfo._mPhysisian_IX) as CPhysicianInfo;

                    textBoxStdInstructions.Text = physician == null ? "? not found ?" : physician._mInstruction.mDecrypt();
                }
            }

        }

        private void labelRefID_Click(object sender, EventArgs e)
        {
            textBoxRefIdOther.Text = labelRecorderRefID.Text;
        }

        private void labelStudyNr_Click(object sender, EventArgs e)
        {

        }

        private void labelRecorderID_Click(object sender, EventArgs e)
        {

        }

        private void labelEndDate_Click(object sender, EventArgs e)
        {

        }

        private void buttonDeviceList_Click(object sender, EventArgs e)
        {
            UInt32 devIX = 0;
            string devStr = "";

            if( mbCheckUpdateDevice( out devIX ))
            {

            }
            if( _mDeviceInfo.mIndex_KEY > 0 )
            {
                devStr = _mDeviceInfo._mDeviceSerialNr;
            }

            CDeviceForm form = new CDeviceForm(devStr, false);

            if (form != null)
            {
                form.ShowDialog();

                if( form._mFormSelectedStudy == _mStudyInfo.mIndex_KEY && _mStudyInfo.mIndex_KEY > 0)
                {
                    _mStudyInfo._mRecorderEndUTC = form._mFormSelectedEndUTC;
                    labelEndRec.Text =  CProgram.sUtcToLocalString(_mStudyInfo._mRecorderEndUTC);
                }
                mLoadDeviceList();

                UInt32 i;
                mbCheckUpdateDevice(out i);
                mUpdateButtons();
            }
        }

        private void comboBoxStudySerialNumberList_TextChanged(object sender, EventArgs e)
        {
            UInt32 i;

            mbCheckUpdateDevice(out i);
        }

        private void radioButtonPatID_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {
            UInt32 i;
            mbCheckUpdateDevice(out i);
            mUpdateButtons();
        }

        private void buttonUseCurrentDevice_Click(object sender, EventArgs e)
        {
            if( _mActiveDevice != null && _mActiveDevice.mIndex_KEY == _mStudyInfo._mStudyRecorder_IX)
            {
                comboBoxStudySerialNumberList.Text = _mActiveDevice._mDeviceSerialNr;
            }
        }

        private void CStudyPatientForm_Shown(object sender, EventArgs e)
        {
            UInt32 i;
            mbCheckUpdateDevice(out i);
            mUpdateButtons();
        }

        private void buttonSearchPatientID_Click(object sender, EventArgs e)
        {
            try
            {
                mReadFormPatient();

                UInt64 searchMask = 0;
                string name = "", var = "";

                if (_mPatientInfo._mSocSecNr.mbNotEmpty())
                {
                    searchMask = CSqlDataTableRow.sGetMask((UInt16)DPatientInfoVars.SocSecNr);
                    var = "Unique ID";
                    name = _mPatientInfo._mSocSecNr.mDecrypt();
                }
                else if (_mPatientInfo.mbIsPatientIDNotEmpty())
                {
                    searchMask = CSqlDataTableRow.sGetMask((UInt16)DPatientInfoVars.PatientID);
                    var = "Patient ID";
                    name = _mPatientInfo.mGetPatientIDValue();
                }
                if (searchMask != 0)
                {
                    List<CSqlDataTableRow> list;
                    bool bError;

                    if (_mPatientInfo.mbDoSqlSelectList(out bError, out list, _mPatientInfo.mMaskValid, searchMask, true, ""))
                    {
                        int n = list.Count;

                        if (n == 0)
                        {
                            CProgram.sPromptWarning(false, "Search Patient", "Did not find " + var + " = " + name);
                        }
                        else
                        {
                            UInt16 i = 1;
                            if (CProgram.sbReqUInt16("Search Patient", " Use result " + var + "[]", ref i, " <= " + n.ToString(), 1, (UInt16)n))
                            {
                                _mPatientInfo.mbCopyFrom(list[i-1]);
                                mFillFormPatient();
                                mUpdateButtons();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Search Patient failed", ex);
            }

         }

        public void mSetClient( string AClient )
        {
            comboBoxStudyHospitalNameList.Text = AClient;
        }
        public void mSetPhysician( string APhysician)
        {
            comboBoxStudyPhysicianList.Text = APhysician;
        }

        private void buttonClearNote_Click(object sender, EventArgs e)
        {
            textBoxStudyPhysicianInstruction.Text = "";
        }
        public void mSetRefIdOther( string ARefID )
        {
            textBoxRefIdOther.Text = ARefID;

            if( ARefID != null && ARefID.Length > 0)
            {
                radioButtonOther.Checked = true;
            }
        }

        private void buttonClearPatient_Click(object sender, EventArgs e)
        {
            _mPatientInfo.mClear();
            _mPatientInfo.mbActive = true;
            mFillFormPatient();
            mUpdateButtons();
        }

        private void buttonEndRecording_Click(object sender, EventArgs e)
        {
            UInt32 i;

            mbCheckUpdateDevice(out i);
            if (_mDeviceInfo != null && _mDeviceInfo.mIndex_KEY > 0 && _mDeviceInfo._mActiveStudy_IX == _mStudyInfo.mIndex_KEY)
            {
                DateTime utc = DateTime.UtcNow;
                if (_mDeviceInfo._mRecorderEndUTC > utc )
                {
                    DateTime oldEnd = _mDeviceInfo._mRecorderEndUTC;
                    _mDeviceInfo._mRecorderEndUTC = utc;

                    if (_mDeviceInfo.mbDoSqlUpdate(CSqlDataTableRow.sGetMask((UInt16)DDeviceInfoVars.RecorderEndUTC), true))
                    {
                        labelDeviceError.Text = " Device ends recording";
                        oldEnd = _mStudyInfo._mRecorderEndUTC;

                        if( _mActiveDevice != null && _mActiveDevice._mActiveStudy_IX == _mStudyInfo.mIndex_KEY)
                        {
                            _mActiveDevice._mRecorderEndUTC = utc;
                        }

                    }
                    else
                    {
                        labelDeviceError.Text = " Device not updated";
                        _mActiveDevice._mRecorderEndUTC = oldEnd;
                    }
                    oldEnd = _mStudyInfo._mRecorderEndUTC;
                    if (oldEnd > utc)
                    {
                        _mStudyInfo._mRecorderEndUTC = utc;
                        if (_mStudyInfo.mbDoSqlUpdate(CSqlDataTableRow.sGetMask((UInt16)DStudyInfoVars.RecorderEndUTC), _mStudyInfo.mbActive))
                        {
                            labelStudyError.Text = "Updated end recording";
                            labelEndRec.Text = CProgram.sUtcToLocalString(_mStudyInfo._mRecorderEndUTC);
                        }
                        else
                        {
                            labelStudyError.Text = "Failed to Update Study";
                            _mStudyInfo._mRecorderEndUTC = oldEnd;
                        }
                    }
                    mbCheckUpdateDevice(out i);
                    mUpdateButtons();
                }
            }
        }

        private void buttonMCT_Click(object sender, EventArgs e)
        {
            if( _mStudyInfo != null && _mStudyInfo.mIndex_KEY > 0 )
            {
                bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;
                bool bSelectFiles = (Control.ModifierKeys & Keys.Alt) == Keys.Alt;

                if (bCtrl) bCtrl = CProgram.sbAskYesNo("MCT Full debug", "Do full debug logging?");

                DTriageTimerMode prevMode = FormEventBoard.sSetTriageTimerEntering(DTriageTimerMode.DialogSecond, "MCT");

                CMCTTrendDisplay form = new CMCTTrendDisplay(_mStudyInfo.mIndex_KEY, DDeviceType.Unknown, bCtrl, bSelectFiles);

                if (form != null )//&& form.bStatsLoaded)
                {
                    form.Show();
                }
                FormEventBoard.sSetTriageTimerLeaving(prevMode, "MCT");

            }
        }

        private void buttonViewPdf_Click(object sender, EventArgs e)
        {
            if (_mStudyInfo != null && _mStudyInfo.mIndex_KEY > 0)
            {
                try
                {
                    CStudyReportListForm form = new CStudyReportListForm(_mStudyInfo.mIndex_KEY);

                    if (form != null)
                    {
                        form.ShowDialog();
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Call pdf study list", ex);
                }
            }
        }

        private void label22_DoubleClick(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl) //&& CLicKeyDev.sbDeviceIsProgrammer())
            {
                mOpenStudyDir();
            }

        }
        private void mOpenStudyDir()
        {
            
                if (_mStudyInfo != null && _mStudyInfo.mIndex_KEY > 0)
            {
                string studyPath;
                
                if( CDvtmsData.sbGetStudyCaseDir(out studyPath, _mStudyInfo.mIndex_KEY, true))
                {
                    // open explorer wit file at cursor
                    try
                    {
                        ProcessStartInfo startInfo = new ProcessStartInfo();
                        startInfo.FileName = @"explorer";
                        startInfo.Arguments = studyPath;
                        Process.Start(startInfo);
                    }
                    catch (Exception e2)
                    {
                        CProgram.sLogException("Failed open explorer", e2);
                    }
                }
            }
        }

        private void textBoxStudyNr_DoubleClick(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl) //&& CLicKeyDev.sbDeviceIsProgrammer())
            {
                mOpenStudyDir();
            }

        }

        private void label45_DoubleClick(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl) //&& CLicKeyDev.sbDeviceIsProgrammer())
            {
                mOpenStudyDir();
            }

        }

        private void buttonDobNA_Click(object sender, EventArgs e)
        {
            _mPatientInfo._mPatientDateOfBirth.mEncrypt(0);
            _mPatientInfo._mPatientBirthYear = 0;
            textBoxPatientDOBYear.Text = "";
            comboBoxPatientDOBMonth.Text = "";
            textBoxPatientDOBDay.Text = "";
            textBoxAge.Text = "";
        }

        private void textBoxAge_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxAge_Click(object sender, EventArgs e)
        {
            UInt16 age = 0;

            if (CProgram.sbReqUInt16("Study Patient Birth Date", "Age", ref age, "Years", 0, 200))
            {
                UInt16 year = (UInt16)(DateTime.Now.Year - age);

                _mPatientInfo._mPatientDateOfBirth.mEncrypt((Int32)CProgram.sCalcYMD(year, 1, 1));
                _mPatientInfo._mPatientBirthYear = year;
                textBoxPatientDOBYear.Text = year.ToString();
                comboBoxPatientDOBMonth.SelectedIndex = 0;
                textBoxPatientDOBDay.Text = "1";
                textBoxAge.Text = age.ToString();
            }
        }

        private void buttonPatientForm_Click(object sender, EventArgs e)
        {
            if (_mStudyInfo != null && _mPatientInfo != null)
            {
                string title = _mStudyInfo.mIndex_KEY > 0 ? "Modify Study " + _mStudyInfo.mIndex_KEY.ToString() : "New Study";

                string uniqueID = textBoxSocSecNr.Text;

                CPatientListForm form = new CPatientListForm(title, null, 0, true,
                    "", true, uniqueID, "", "", DateTime.MinValue,
                    true, false, false);

                if (form != null)
                {
                    form.ShowDialog();

                    if (form._mbSelected)
                    {
                        _mPatientInfo.mbCopyFrom(form.mGetSelected());

                        mFillFormPatient();

                        UInt32 i;
                        mbCheckUpdateDevice(out i);
                        mUpdateButtons();
                    }
                }
            }
        }

        private void buttonClearPatient_Click_1(object sender, EventArgs e)
        {
            _mPatientInfo.mClear();
            mFillFormPatient();

            UInt32 i;
            mbCheckUpdateDevice(out i);
            mUpdateButtons();

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void panel8_Paint(object sender, PaintEventArgs e)
        {

        }

        private void checkBoxAllRefPhysician_CheckedChanged(object sender, EventArgs e)
        {
            mFillRefPhysicianList();
        }

        private void checkBoxAllPhysician_CheckedChanged(object sender, EventArgs e)
        {
            mFillPhysicianList();
        }

        private void checkBoxAllRecorder_CheckedChanged(object sender, EventArgs e)
        {
            mFillDeviceList();
        }

        private void labelStudyHospitalName_Click(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl)
            {
                UInt32 clientIX = _mStudyInfo._mClient_IX;

                if (_mClient != null)
                {
                    _mClient.mbListParseComboBox(comboBoxStudyHospitalNameList, _mClientList, (UInt16)DClientInfoVars.Label, ref clientIX);
                }
                //if (clientIX > 0)
                {
                    CClientInfoForm clientForm = new CClientInfoForm(null, clientIX, true);

                    if (clientForm != null)
                    {
                        clientForm.ShowDialog();
                        if (clientForm._mbSelected)
                        {
                            comboBoxStudyHospitalNameList.Text = clientForm._mInfo._mLabel;
                        }
                    }
                }
            }

            mFillAfterClientChange();
        }

        private void comboBoxStudyHospitalNameList_SelectedIndexChanged(object sender, EventArgs e)
        {
            mFillAfterClientChange();
        }

        private void mFillRecInfo()
        {
            string recInfo = "";

            if (_mStudyInfo != null)
            {
                UInt32 nTotal = 0;
                UInt32 nToDo = 0;

                recInfo = _mStudyInfo.mGetRecStatesAll(out nTotal, out nToDo, false);
                CSqlDBaseConnection db = CDvtmsData.sGetDBaseConnection();

                UInt32 dbLimit = db == null ? 1 : db.mGetSqlRowLimit();
                if (nTotal >= dbLimit)
                {
                    labelRecInfo.BackColor = Color.Orange;
                }
            }
            labelRecInfo.Text = recInfo;
        }

        private void labelRecInfo_Click(object sender, EventArgs e)
        {
            mFillRecInfo();            
        }

        private void label22_Click(object sender, EventArgs e)
        {

        }

        private void labelStudyMin1_Click(object sender, EventArgs e)
        {
            dateTimePickerStudyStartDate.Value = dateTimePickerStudyStartDate.Value.AddHours(-1);
        }

        private void labelStudyMin6_Click(object sender, EventArgs e)
        {
            dateTimePickerStudyStartDate.Value = dateTimePickerStudyStartDate.Value.AddHours(+1);
        }

        private void labelStudyPlus1_Click(object sender, EventArgs e)
        {
            dateTimePickerStudyStartDate.Value = dateTimePickerStudyStartDate.Value.AddHours(-6);
        }

        private void labelStudyPlus6_Click(object sender, EventArgs e)
        {
            dateTimePickerStudyStartDate.Value = dateTimePickerStudyStartDate.Value.AddHours(+6);
        }

        private void checkBoxAnonymizePatientInfo_CheckStateChanged(object sender, EventArgs e)
        {
            bool bVisible = false == checkBoxAnonymizePatientInfo.Checked;

            textBoxPatientFirstName.Visible = bVisible;

            textBoxPatientMiddleName.Visible = bVisible;
            textBoxPatientLastName.Visible = bVisible;

            textBoxPatientDOBDay.Visible = bVisible;

            textBoxPatientDOBYear.Visible = bVisible;
            comboBoxPatientDOBMonth.Visible = bVisible;

            //            textBoxPatientLabel.Visible = bVisible;

            textBoxPatientAddress.Visible = bVisible;
            textBoxPatientZip.Visible = bVisible;
            textBoxPatientHouseNumber.Visible = bVisible;

            //            textBoxPatientCity.Visible = bVisible;
            textBoxPatientPhone1.Visible = bVisible;
            textBoxPatientPhone2.Visible = bVisible;
            textBoxPatientCell.Visible = bVisible;
            textBoxPatientEmail.Visible = bVisible;
            textBoxPatientSkype.Visible = bVisible;
        }

        private void textBoxStudyPermissions_TextChanged(object sender, EventArgs e)
        {
        }

        private void textBoxStudyPermissions_Click(object sender, EventArgs e)
        {
            CDvtmsData.sbEditStudyPermissionsBits("required permissions", ref _mStudyInfo._mStudyPermissions, 0);

            mFillStudyPermissions();
        }

        private void comboBoxTrial_Click(object sender, EventArgs e)
        {
            /*if (CProgram.sbReqUInt32("Study Trial", "Trial number", ref _mStudyInfo._mTrial_IX, "", 0, UInt32.MaxValue))
            {
                comboBoxTrial.Text = _mStudyInfo._mTrial_IX == 0 ? "----" : "T" + _mStudyInfo._mTrial_IX.ToString();
                !
            }
            */
        }

        private bool mbLoadDefaultsFromTrial()
        {
            bool bChanged = false;

            if( _mStudyInfo._mTrial_IX > 0)
            {
                CTrialInfo selectedTrial = new CTrialInfo(CDvtmsData.sGetDBaseConnection());

                if(selectedTrial != null )
                {
                    if(selectedTrial.mbLoadIndexFromList(_mTrialList, _mStudyInfo._mTrial_IX))
                    {
                        int studyDays = selectedTrial._mTrialStopDate == DateTime.MinValue ? 0
                            : (int)((selectedTrial._mTrialStopDate - DateTime.Now).TotalDays + 0.9);
                        if (studyDays < 0) studyDays = 0;
                        if (studyDays > selectedTrial._mStudyMonitoringDays) studyDays = selectedTrial._mStudyMonitoringDays;

                        bChanged |= _mStudyInfo._mStudyMonitoringDays != (UInt16)studyDays;
                        bChanged |= _mStudyInfo._mStudyTypeCode != selectedTrial._mStudyTypeCode;
                        if (selectedTrial._mReportIntervalSet != null) bChanged |= false == selectedTrial._mReportIntervalSet.mbEquals( _mStudyInfo._mReportIntervalSet);
                        if (_mStudyInfo._mMctIntervalSet != null) bChanged |= false == _mStudyInfo._mMctIntervalSet.mbEquals(_mStudyInfo._mMctIntervalSet);
                        bChanged |= false == selectedTrial._mStudyProcCodeSet.mbEquals( _mStudyInfo._mStudyProcCodeSet);
                        bChanged |= _mStudyInfo._mStudyRemarkLabel != selectedTrial._mStudyRemarkLabel;
                        bChanged |= _mStudyInfo._mStudyPhysicianInstruction != selectedTrial._mStudyFreeText;
                        bChanged |= selectedTrial._mStudyPermissions._mBitSet != _mStudyInfo._mStudyPermissions._mBitSet;

                        _mStudyInfo._mStudyMonitoringDays = (UInt16)studyDays;
                        _mStudyInfo._mStudyTypeCode = selectedTrial._mStudyTypeCode;
                        if(selectedTrial._mReportIntervalSet != null ) selectedTrial._mReportIntervalSet.mbCopyTo(ref _mStudyInfo._mReportIntervalSet);
                        if( _mStudyInfo._mMctIntervalSet != null ) _mStudyInfo._mMctIntervalSet.mbCopyTo(ref _mStudyInfo._mMctIntervalSet);
                        selectedTrial._mStudyProcCodeSet.mbCopyTo(ref _mStudyInfo._mStudyProcCodeSet);

                        _mStudyInfo._mStudyRemarkLabel = selectedTrial._mStudyRemarkLabel;
                        _mStudyInfo._mStudyPhysicianInstruction = selectedTrial._mStudyFreeText;
                        selectedTrial._mStudyPermissions.mCopyTo(ref _mStudyInfo._mStudyPermissions);
                    }
                }
            }
            return bChanged;
        }

        private void comboBoxTrial_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_mStudyInfo.mIndex_KEY == 0 && mbInitFinished)
            {
                UInt32 trial = mReadTrial();

                mReadFormStudy();
               
                if (mbLoadDefaultsFromTrial())
                {
                    mFillFormStudy();
                }
            }
        }

        private void label45_Click(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl) //&& CLicKeyDev.sbDeviceIsProgrammer())
            {
                string s = "S" + _mStudyInfo.mIndex_KEY.ToString();

                if (_mStudyInfo._mTrial_IX > 0)
                {
                    s += "_T" + _mStudyInfo._mTrial_IX.ToString();
                }
                if (_mStudyInfo._mClient_IX > 0)
                {
                    s += "_C" + _mStudyInfo._mClient_IX.ToString();
                }
                if (_mPatientInfo.mIndex_KEY > 0)
                {
                    s += "_P" + _mPatientInfo.mIndex_KEY.ToString();
                }
                if (_mStudyInfo._mStudyPermissions.mbIsNotEmpty())
                {
                    s += "_" + _mStudyInfo._mStudyPermissions._mBitSet.ToString();
                }
                Clipboard.SetText(s);
                CProgram.sAskOk("Study info", s + " to clipboard");

            }

        }

        public bool mbLoadRecords( out String ArInfoLine, UInt32 AStudyIX)
        {
            bool bOk = false;
            string recordsInfo = "? records";

            UInt32 dbLimit = 0;

            CSqlDBaseConnection db = CDvtmsData.sGetDBaseConnection();

            if (db != null)
            {
                dbLimit = db.mGetSqlRowLimit();
            }

            CRecordMit recBase = new CRecordMit(db);

            if (recBase != null && _mRecordList != null)
            {
                bool bError;

                recBase.mStudy_IX = AStudyIX;


                bOk = recBase.mbDoSqlSelectList(out bError, out _mRecordList, recBase.mGetValidMask(true), CRecordMit.sGetMask((UInt16)DRecordMitVars.Study_IX), true, "");

                if (bOk)
                {
                    int nStates = (int)DRecState.NrRecStates;
                    UInt32[] countStates = new UInt32[nStates];
                    Int32 nTotal = _mRecordList.Count;
                    Int32 nToDo = 0;
                    Int32 nAnalyzed = 0;
                    Int32 nOther = 0;
                    UInt16 state;

                    for (int i = 0; i < nStates; ++i)
                    {
                        countStates[i] = 0;
                    }

                    foreach (CRecordMit rec in _mRecordList)
                    {
                        state = rec.mRecState;
                        if (state >= nStates)
                        {
                            state = (UInt16)DRecState.Unknown;
                        }
                        ++countStates[state];

                        if (CRecordMit.sbIsStateAnalyzed(state, _mbUseExcluded))
                        {
                            ++nAnalyzed;
                        }
                        else if (CRecordMit.sbIsStateToDo(state))
                        {
                            ++nToDo;
                        }
                        else
                        {
                            ++nOther;
                        }
                    }
                    recordsInfo = nTotal.ToString();

                    if (nTotal >= dbLimit)
                    {
                        recordsInfo += ">=" + dbLimit.ToString();
                    }
                    recordsInfo += " records: " + nToDo.ToString() + " ToDo, " + nAnalyzed.ToString()
                          + (_mbUseExcluded ? " Analyzed+excl (" : " Analyzed (");

                    bool bShowAll = CLicKeyDev.sbDeviceIsProgrammer();
                    bool bAddKomma = false;

                    for (UInt16 i = 0; i < nStates; ++i)
                    {
                        if (bShowAll || countStates[i] > 0)
                        {
                            if (bAddKomma)
                            {
                                recordsInfo += ", ";
                            }
                            recordsInfo += countStates[i].ToString() + " " + CRecordMit.sGetRecStateUserString(i);
                            bAddKomma = true;
                        }
                    }
                    recordsInfo += ")";
                }
            }
            ArInfoLine = recordsInfo;
            CProgram.sLogLine("S" + AStudyIX.ToString() + "records loaded: " + recordsInfo);

            return bOk;
        }

        private string mSortedRange( ref string[] ArArray, string AName, bool AbLogAll)
        {
            string result = "";
            int nElms = ArArray != null ? ArArray.Length : 0;

            if(nElms > 0)
            {
                Array.Sort(ArArray);

                int nDiff = 1;
                int nCurrent = 1;
                string current = ArArray[0];
                string list = "";

                for( int i = 0; ++i < nElms;)
                {
                    if(ArArray[i] == current)
                    {
                        ++nCurrent;
                    }
                    else
                    {
                        if (list.Length > 0) list += ", ";
                        list += nCurrent.ToString() + "*" + current;

                        ++nDiff;
                        current = ArArray[i];
                        nCurrent = 1;
                    }
                }
                // add the last current (can also be the only one
                if (list.Length > 0) list += ", ";
                list += nCurrent.ToString() + "*" + current;

                if (AbLogAll || nDiff > 1)
                {
                    result = AName + "{" + list + "}";
                }

            }
            return result;

        }


        public bool mbCalcStatLine2(out String ArStatLine, UInt32 AStudyIX, UInt32 ASudyNrRecords, bool AbLogAll)
        {
            bool bOk = false;
            string statInfo = "? records";

            try
            {
                UInt32 dbLimit = 0;

                CSqlDBaseConnection db = CDvtmsData.sGetDBaseConnection();

                if (db != null)
                {
                    dbLimit = db.mGetSqlRowLimit();
                }

                CBitSet64 bitSet = new CBitSet64();
                CEncryptedString fullPatientNameES = new CEncryptedString();

                if (_mRecordList != null && AStudyIX != 0 && bitSet != null && fullPatientNameES != null)
                {
                    int nRecords = _mRecordList.Count;
                    int nMissing = (int)ASudyNrRecords - nRecords;

                    fullPatientNameES.mbEncrypt(_mPatientInfo.mGetFullName());

                    if (_mStudyInfo._mStudyPermissions != null)
                    {
                        _mStudyInfo._mStudyPermissions.mCopyTo(ref bitSet);
                    }

                    statInfo = "";
                    bOk = true;

                    if (0 != nMissing)
                    {
                        statInfo = nMissing.ToString() + " missing";
                    }
                    if (0 == nRecords)
                    {
                        if (statInfo.Length > 0) statInfo += ", ";
                        statInfo += "0 Event Records!";
                    }
                    else
                    {
                        string[] arrayPatID = new string[nRecords];
                        string[] arrayDevID = new string[nRecords];

                        int nWrongStudy = 0;
                        int index = 0;
                        int nSame = 0;
                        int nDifferentStudy = 0;
                        int nDifferentName = 0;
                        int nPermissions = 0;
                        int nTrial = 0;
                        int nClient = 0;
                        int nPatientIX = 0;
                        int nDOB = 0;
                        int nYear = 0;
                        int nGender = 0;
                        int nName = 0;

                        foreach (CRecordMit rec in _mRecordList)
                        {
                            arrayPatID[index] = rec.mPatientID.mDecrypt();
                            arrayDevID[index] = rec.mDeviceID;

                            ++index;

                            if (rec.mStudy_IX != AStudyIX)
                            {
                                ++nWrongStudy;
                            }
                            bool bDifferendStudy = false;

                            if (bitSet.mbIsNotEqual(rec._mStudyPermissions)) { bDifferendStudy = true; ++nPermissions; }
                            if (rec._mStudyTrial_IX != _mStudyInfo._mTrial_IX) { bDifferendStudy = true; ++nTrial; }
                            if (rec._mStudyClient_IX != _mStudyInfo._mClient_IX) { bDifferendStudy = true; ++nClient; }

                            if (rec.mPatient_IX != _mPatientInfo.mIndex_KEY) { bDifferendStudy = true; ++nPatientIX; }

                            if (bDifferendStudy)
                            {
                                ++nDifferentStudy;
                            }
                            bool bDifferendName = false;

                            if (rec.mPatientBirthDate.mbIsNotEqual(_mPatientInfo._mPatientDateOfBirth)) { bDifferendName = true; ++nDOB; }
                            if (rec.mPatientBirthYear != _mPatientInfo._mPatientBirthYear) { bDifferendName = true; ++nYear; }
                            if (rec.mPatientGender != _mPatientInfo._mPatientGender_IX) { bDifferendName = true; ++nGender; }
                            if (false == fullPatientNameES.mbIsEqual(rec.mPatientTotalName)) { bDifferendName = true; ++nName; }

                            if (bDifferendName)
                            {
                                ++nDifferentName;
                            }
                            if (false == (bDifferendStudy || bDifferendName))
                            {
                                ++nSame;
                            }
                        }

                

                        if (nWrongStudy != 0)
                        {
                            if (statInfo.Length > 0) statInfo += ", ";
                            statInfo += nWrongStudy.ToString();
                            bOk = false;
                            mbAllowRecordUpdate = false;
                        }

                        string patIDSet = mSortedRange(ref arrayPatID, "PatID", AbLogAll);
                        if (AbLogAll || patIDSet.Length > 0)
                        {
                            if (statInfo.Length > 0) statInfo += ", ";
                            statInfo += patIDSet; 
                        }
                        string devIDSet = mSortedRange(ref arrayDevID, "DevID", AbLogAll);
                        if (devIDSet.Length > 0)
                        {
                            if (statInfo.Length > 0) statInfo += ", ";
                            statInfo += devIDSet; 
                        }
                        if (statInfo.Length > 0) statInfo += ", ";
                        statInfo += nSame.ToString() + " same";
                        if (AbLogAll || nDifferentStudy != 0)
                        {
                            if (statInfo.Length > 0) statInfo += ", ";
                            statInfo += nDifferentStudy.ToString() + " Ndif!(";
                            if (AbLogAll || nPermissions > 0)
                            {
                                statInfo += nPermissions.ToString() + " Loc ";
                            }
                            if (AbLogAll || nTrial > 0)
                            {
                                statInfo += nTrial.ToString() + " Trial ";
                            }
                            if (AbLogAll || nClient > 0)
                            {
                                statInfo += nClient.ToString() + " Client ";
                            }
                            if (AbLogAll || nPatientIX > 0)
                            {
                                statInfo += nPatientIX.ToString() + " PatIX";
                            }
                            statInfo += ")!";
                        }
                        if (AbLogAll || nDifferentName != 0)
                        {
                            if (statInfo.Length > 0) statInfo += ", ";
                            statInfo += nDifferentName.ToString() + " NdifPat(";
                            if (AbLogAll || nDOB > 0)
                            {
                                statInfo += nDOB.ToString() + " DOB ";
                            }
                            if (AbLogAll || nYear > 0)
                            {
                                statInfo += nYear.ToString() + " Year ";
                            }
                            if (AbLogAll || nGender > 0)
                            {
                                statInfo += nGender.ToString() + " Gender ";
                            }
                            if (AbLogAll || nName > 0)
                            {
                                statInfo += nName.ToString() + " Name";
                            }
                            statInfo += ")";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                statInfo = "Exception! " + statInfo;
                CProgram.sLogError(statInfo);
                CProgram.sLogException("Calc stat records info failed", ex);
                bOk = false;
            }
            CProgram.sLogLine("S"+ AStudyIX.ToString() + "records info: " + statInfo);
            ArStatLine = statInfo;
            return bOk;
        }

        private bool mbLoadStatRecords(bool AbLogAll)
        {
            string line1 = "For records info press button [Records info]";
            string line2 = "";
            string recordsInfo = "?";
            string statInfo = "?";
            bool bEnable = false;

            UInt32 studyIX = _mStudyInfo.mIndex_KEY;

            if (studyIX == 0)
            {
                line2 = "No study";
            }
            else if (_mRecordList == null || false == mbLoadRecords(out recordsInfo, studyIX))
            {
                line2 = "Error: " + recordsInfo;
            }
            else
            {
                UInt64 perm = _mStudyInfo._mStudyPermissions == null ? 0 : _mStudyInfo._mStudyPermissions._mBitSet;

                recordsInfo += "{ S" + studyIX.ToString()
                    + ", T" + _mStudyInfo._mTrial_IX.ToString()
                    + ", C" + _mStudyInfo._mClient_IX.ToString()
                    + ", P" + _mStudyInfo._mPatient_IX.ToString()
                    + ", &&" + perm.ToString()
                    + "}";
                line1 = recordsInfo;
                if (false == mbCalcStatLine2(out statInfo, studyIX, _mStudyInfo._mNrRecords, AbLogAll))
                {
                    line2 = "Error: " + statInfo;
                }
                else
                {
                    line2 = statInfo;
                    bEnable = mbAllowRecordUpdate;

                    int n = _mRecordList == null ? 0 : _mRecordList.Count;
                    CSqlDBaseConnection db = CDvtmsData.sGetDBaseConnection();

                    UInt32 dbLimit = db == null ? 1 : db.mGetSqlRowLimit();
                    if( n >= dbLimit )
                    {
                        labelRecorsInfo.BackColor = Color.Orange;
                    }
                }
            }

            labelRecorsInfo.Text = line1 + "\r\n" + line2;
            checkBoxUpdateRecords.Enabled = bEnable;
            checkBoxUpdatePatInfo.Enabled = bEnable;

            return bEnable;
        }

        private bool mbUpdateStudyRecords()
        {
            bool bOk = false;

            bool bUpdateRecords = checkBoxUpdateRecords.Checked;
            bool bUpdatePatient = checkBoxUpdatePatInfo.Checked;

            UInt32 studyIX = _mStudyInfo.mIndex_KEY;

            string action = "? records";

            CBitSet64 bitSet = new CBitSet64();
            CEncryptedString fullPatientNameES = new CEncryptedString();
            CEncryptedString patientIdES = new CEncryptedString();

            if (_mRecordList != null && bUpdateRecords && studyIX != 0 && bitSet != null && fullPatientNameES != null && patientIdES != null)
            {
                int nRecords = _mRecordList.Count;

                action = "";
                bOk = true;

                if (0 == nRecords)
                {
                    if (action.Length > 0) action += ", ";
                    action += "0 Event Records!";
                }
                else 
                {
                    UInt64 saveMask = CSqlDataTableRow.sGetMask((UInt16)DRecordMitVars.Client_IX)
                            | CSqlDataTableRow.sGetMask((UInt16)DRecordMitVars.Trial_IX)
                            | CSqlDataTableRow.sGetMask((UInt16)DRecordMitVars.StudyPermissions)
                            ;

                    action = "fill " + nRecords.ToString() + " records with Client, Location and Trial";
                    if(bUpdatePatient)
                    {
                        action += " + Patient Info";
                        saveMask |= CSqlDataTableRow.sGetMask((UInt16)DRecordMitVars.Patient_IX)
                                | CSqlDataTableRow.sGetMask((UInt16)DRecordMitVars.PatientBirthDate_EI)
                                | CSqlDataTableRow.sGetMask((UInt16)DRecordMitVars.PatientBirthYear)
                                | CSqlDataTableRow.sGetMask((UInt16)DRecordMitVars.PatientGender)
                                | CSqlDataTableRow.sGetMask((UInt16)DRecordMitVars.PatientTotalName_ES)
                                | CSqlDataTableRow.sGetMask((UInt16)DRecordMitVars.PatientID_ES)
                                ;

                        fullPatientNameES.mbEncrypt(_mPatientInfo.mGetFullName());
                        patientIdES.mbEncrypt(_mPatientInfo.mGetPatientSocIDValue(false));
                    }
                    if (CProgram.sbAskOkCancel("Fill " + nRecords.ToString() + " records of study " + studyIX.ToString(), action + "?"))
                    {
                        panelButtons.Enabled = false;
                        DateTime nowUTC = DateTime.UtcNow; 

                        if (_mStudyInfo._mStudyPermissions != null)
                        {
                            _mStudyInfo._mStudyPermissions.mCopyTo(ref bitSet);
                        }

                        action = "Fill records of study " + studyIX.ToString() + ": ";
                        string total = " / " + nRecords.ToString();

                        int nSame = 0;
                        int nError = 0;
                        int index = 0;
                        int nChanged = 0;

                        foreach (CRecordMit rec in _mRecordList)
                        {
                            ++index;
                            string loop = action + index.ToString() + total;

                            string title = loop + "(" + nSame.ToString() + " same, " + nChanged.ToString();
                            if (nError > 0)
                            {
                                title += ", " + nError.ToString();
                            }
                            this.Text = title + ")";
                            if (1 == (index % 10))
                            {
                                Application.DoEvents();     // update title and make Eventboard responsive
                            }

                            if (rec.mStudy_IX != studyIX)
                            {
                                ++nError;
                                bOk = CProgram.sbAskOkCancel(loop, "Bad study nr found at " + studyIX.ToString() + "." + rec.mSeqNrInStudy.ToString() + ", Continue?");
                            }
                            else
                            {
                                bool bDifferend = false;

                                if (bitSet.mbIsNotEqual(rec._mStudyPermissions)) { bDifferend = true; bitSet.mCopyTo(ref rec._mStudyPermissions); }
                                if (rec._mStudyTrial_IX != _mStudyInfo._mTrial_IX) { bDifferend = true; rec._mStudyTrial_IX = _mStudyInfo._mTrial_IX; }
                                if (rec._mStudyClient_IX != _mStudyInfo._mClient_IX) { bDifferend = true; rec._mStudyClient_IX = _mStudyInfo._mClient_IX; }

                                if (bUpdatePatient)
                                {
                                    if (rec.mPatient_IX != _mPatientInfo.mIndex_KEY) { bDifferend = true; rec.mPatient_IX = _mPatientInfo.mIndex_KEY; }
                                    if (rec.mPatientBirthDate.mbIsNotEqual(_mPatientInfo._mPatientDateOfBirth)) { bDifferend = true; rec.mPatientBirthDate = _mPatientInfo._mPatientDateOfBirth; }
                                    if (rec.mPatientBirthYear != _mPatientInfo._mPatientBirthYear) { bDifferend = true; rec.mPatientBirthYear = _mPatientInfo._mPatientBirthYear; }
                                    if (rec.mPatientGender != _mPatientInfo._mPatientGender_IX) { bDifferend = true; rec.mPatientGender = _mPatientInfo._mPatientGender_IX; }
                                    if (false == fullPatientNameES.mbIsEqual(rec.mPatientTotalName)) { bDifferend = true; fullPatientNameES.mbCopyTo(ref rec.mPatientTotalName); }
                                    if (false == patientIdES.mbIsEqual(rec.mPatientID)) { bDifferend = true; patientIdES.mbCopyTo(ref rec.mPatientID); }
                                }
                                if (bDifferend)
                                {
                                    if (rec.mbDoSqlUpdate(saveMask, rec.mbActive))
                                    {
                                        ++nChanged;
                                    }
                                    else
                                    {
                                        ++nError;
                                        bOk = CProgram.sbAskOkCancel(loop, "Failed update event record at " + studyIX.ToString() + "." + rec.mSeqNrInStudy.ToString() + ", Continue?");
                                    }
                                }
                                else
                                {
                                    ++nSame;
                                }
                            }

                            if (false == bOk)
                            {
                                break;
                            }
                        }
                        double sec = (DateTime.UtcNow - nowUTC).TotalSeconds;
                        if (bOk && nError == 0)
                        {
                            string title = action + nRecords.ToString() + "(" + nSame.ToString() + " same, " + nChanged.ToString() + " changed";
                            if (nError > 0)
                            {
                                title += ", " + nError.ToString();
                            }
                            title += ") done updating.(" + sec.ToString( "0.0") + "s)";
                            this.Text = title;
                            CProgram.sLogLine("S" + studyIX.ToString() + "records update: " + title);
                            panelButtons.Enabled = true;
                        }
                        else
                        {
                            string title = action + nRecords.ToString() + (bOk ? " completed !(": " !failed! (" ) + nSame.ToString() + " same, " + nChanged.ToString() + " changed";
                            if (nError > 0)
                            {
                                title += ", " + nError.ToString();
                            }
                            title += ")(" + sec.ToString("0.0") + "s) !!!";
                            this.Text = title;
                            labelRecorsInfo.Text = title;
                            CProgram.sLogError("S" + studyIX.ToString() + "records update: " + title);
                        }
                        checkBoxUpdateRecords.Checked = false;
                        checkBoxUpdatePatInfo.Checked = false;
                    }
                }
            }
            return bOk;
        }

        private void buttonRecordsInfo_Click(object sender, EventArgs e)
        {
            mReadForm();
            mbCheckForm(_mStudyInfo.mIndex_KEY == 0 ? "Add" : "Update", true);
            mbLoadStatRecords(CLicKeyDev.sbDeviceIsProgrammer());
        }

        private void label10_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            mSetRefIDCheckbox( true );
        }

        private void buttonHolter_Click(object sender, EventArgs e)
        {
            if (_mPatientInfo != null && _mStudyInfo != null)
            {
                UInt32 studyNr = _mStudyInfo.mIndex_KEY;

                if (studyNr > 0)
                {
                    try
                    {
                        CHolterFilesForm form = new CHolterFilesForm(studyNr, true, 0, 0);

                        if (form != null)
                        {
                            form.ShowDialog();
                        }
                    }
                    catch( Exception ex )
                    {
                        CProgram.sLogException("start Holter screen", ex);
                    }
                    
                }
            }

        }

        private void labelDeviceCmp_Click(object sender, EventArgs e)
        {
            if (_mActiveDevice != null && _mActiveDevice.mIndex_KEY == _mStudyInfo._mStudyRecorder_IX)
            {
                comboBoxStudySerialNumberList.Text = _mActiveDevice._mDeviceSerialNr;
            }
        }

        private void labelStudyPhysician_Click(object sender, EventArgs e)
        {
            mPhysicianInfo(comboBoxStudyPhysicianList, textBoxStdInstructions);
        }

        private void labelTrialText_Click(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;
            if (bCtrl)
            {
                UInt32 trial = mReadTrial();

                CTrialInfoForm trialForm = new CTrialInfoForm(null, trial, true);

                if (trialForm != null)
                {
                    trialForm.ShowDialog();
                    if (trialForm._mbSelected)
                    {
                        comboBoxTrial.Text = trialForm._mInfo._mLabel;      // update by change
                        mReadFormStudy();

                        if (mbLoadDefaultsFromTrial())
                        {
                            mFillFormStudy();
                        }

                    }
                }
            }

        }

        private void textBoxStudyPhysicianInstruction_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
