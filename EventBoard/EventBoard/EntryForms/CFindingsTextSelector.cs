﻿using Program_Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventBoard.EntryForms
{
    public partial class CFindingsTextSelector : Form
    {
        private string _mOrgText;
        private string _mNewText;
        private bool _mbTextChanged;

        public CFindingsTextSelector(string ATitle, string AOrgText, UInt32 AMaxLength, string[]AOptionalLines)
        {
            _mOrgText = AOrgText;
            _mNewText = AOrgText;
            _mbTextChanged = false;
            InitializeComponent();

            textBox.MaxLength = (int)AMaxLength;
            textBox.Text = AOrgText == null ? "" : AOrgText.TrimEnd();

            if (AOptionalLines != null)
            {
                for (int i = 0; i < AOptionalLines.Length; ++i)
                {
                    dataGridView.Rows.Add(false, "", AOptionalLines[i]);
                }
            }
            Text = CProgram.sMakeProgTitle(ATitle, false, false);
        }

        public bool mbGetNewText( ref string ArText )
        {
            if(_mbTextChanged )
            {
                ArText = _mNewText;
            }
            return _mbTextChanged;
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            textBox.Text = "";
        }

        private void buttonPrevious_Click(object sender, EventArgs e)
        {
            textBox.Text = _mOrgText;
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            _mNewText = textBox.Text;
            _mbTextChanged = true;
            Close();
        }

        private void buttonSelect_Click(object sender, EventArgs e)
        {
            string newText = textBox.Text.TrimEnd();    // remove empty lines;

            for (int i = 0; i < dataGridView.Rows.Count; ++i)
            {
                bool bChecked = dataGridView.Rows[i].Cells[0].Value != null && (bool)dataGridView.Rows[i].Cells[0].Value;

                if (bChecked && dataGridView.Rows[i].Cells[2].Value != null)
                {
                    string line = dataGridView.Rows[i].Cells[2].Value.ToString();

                    if (newText.Length > 0) newText += "\r\n";  // only add new line if there is text
                    newText += line;

                    dataGridView.Rows[i].Cells[0].Value = false;
                }
            }
            textBox.Text = newText;

        }
        public static bool sbLoadOnceFindingsTextLines( ref bool ArbLoaded, ref String[] ArOptionalLines, string AFilePath)
        {
            bool bLoaded = false;

            if( false == ArbLoaded )
            {
                try
                {
                    ArbLoaded = true;   // if failes do not do it again

                    if( File.Exists(AFilePath))
                    {
                        ArOptionalLines = File.ReadAllLines(AFilePath);
                    }
                  }
                catch(Exception ex)
                {
                    CProgram.sLogException("Load TextLines failed from: " + AFilePath, ex);
                }
            }
            return bLoaded;
        }
        public static bool sbEditTextBox(TextBox ATextBox, string ATitle, ref bool ArbLoaded, ref String[] ArOptionalLines, string AFilePath)
        {
            bool bChanged = false;
            if (ATextBox != null)
            {
                string text = ATextBox.Text;
                UInt32 maxLength = (UInt32)ATextBox.MaxLength;

                sbLoadOnceFindingsTextLines(ref ArbLoaded, ref ArOptionalLines, AFilePath);

                try
                {
                    CFindingsTextSelector form = new CFindingsTextSelector(ATitle, text, maxLength, ArOptionalLines);

                    if (form != null)
                    {
                        form.ShowDialog();

                        bChanged = form.mbGetNewText(ref text);
                        if (bChanged)
                        {
                            ATextBox.Text = text;
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Edit TextLines failed from: " + AFilePath, ex);
                }

            }
            return bChanged;
        }

    }
}
