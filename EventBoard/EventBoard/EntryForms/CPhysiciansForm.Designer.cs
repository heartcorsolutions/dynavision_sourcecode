﻿namespace EventBoard.EntryForms
{
    partial class CPhysiciansForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelError = new System.Windows.Forms.Label();
            this.panel19 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.LabelPhysicianNr = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel43 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.buttonRandom = new System.Windows.Forms.Button();
            this.panel22 = new System.Windows.Forms.Panel();
            this.panel21 = new System.Windows.Forms.Panel();
            this.panel23 = new System.Windows.Forms.Panel();
            this.textBoxInstruction = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBoxCountryList = new System.Windows.Forms.ComboBox();
            this.textBoxSkype = new System.Windows.Forms.TextBox();
            this.textBoxEmail = new System.Windows.Forms.TextBox();
            this.labelUserEmail = new System.Windows.Forms.Label();
            this.labelUserSkype = new System.Windows.Forms.Label();
            this.textBoxPhone2 = new System.Windows.Forms.TextBox();
            this.labelUserPhone2 = new System.Windows.Forms.Label();
            this.textBoxHouseNumber = new System.Windows.Forms.TextBox();
            this.labelUserHouseNumber = new System.Windows.Forms.Label();
            this.textBoxCell = new System.Windows.Forms.TextBox();
            this.labelUserCell = new System.Windows.Forms.Label();
            this.textBoxAddress = new System.Windows.Forms.TextBox();
            this.labelUserAddress = new System.Windows.Forms.Label();
            this.comboBoxStateList = new System.Windows.Forms.ComboBox();
            this.textBoxPhone1 = new System.Windows.Forms.TextBox();
            this.labelUserPhone1 = new System.Windows.Forms.Label();
            this.labelUserCountry = new System.Windows.Forms.Label();
            this.textBoxCity = new System.Windows.Forms.TextBox();
            this.labelUserCity = new System.Windows.Forms.Label();
            this.labelUserState = new System.Windows.Forms.Label();
            this.textBoxZip = new System.Windows.Forms.TextBox();
            this.labelUserZip = new System.Windows.Forms.Label();
            this.panel20 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.checkBoxActive = new System.Windows.Forms.CheckBox();
            this.textBoxClientLabel = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxAgeResult = new System.Windows.Forms.TextBox();
            this.comboBoxHospitalList = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxGenderTitleList = new System.Windows.Forms.ComboBox();
            this.labelAge = new System.Windows.Forms.Label();
            this.comboBoxAcademicTitleList = new System.Windows.Forms.ComboBox();
            this.labelUserTitle = new System.Windows.Forms.Label();
            this.textBoxDateOfBirthYear = new System.Windows.Forms.TextBox();
            this.comboBoxDateOfBirthMonthList = new System.Windows.Forms.ComboBox();
            this.textBoxDateOfBirthDay = new System.Windows.Forms.TextBox();
            this.labelUserDateOfBirth = new System.Windows.Forms.Label();
            this.comboBoxGender = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxLastName = new System.Windows.Forms.TextBox();
            this.labelUserLastName = new System.Windows.Forms.Label();
            this.textBoxMiddleName = new System.Windows.Forms.TextBox();
            this.labelUserMiddleName = new System.Windows.Forms.Label();
            this.textBoxFirstName = new System.Windows.Forms.TextBox();
            this.labelUserFirstName = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel28 = new System.Windows.Forms.Panel();
            this.panel39 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.buttonAddUpdate = new System.Windows.Forms.Button();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel37 = new System.Windows.Forms.Panel();
            this.buttonClear = new System.Windows.Forms.Button();
            this.panel41 = new System.Windows.Forms.Panel();
            this.buttonDup = new System.Windows.Forms.Button();
            this.panel42 = new System.Windows.Forms.Panel();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.panel40 = new System.Windows.Forms.Panel();
            this.performanceCounter1 = new System.Diagnostics.PerformanceCounter();
            this.panel14 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.panel24 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.panel29 = new System.Windows.Forms.Panel();
            this.dataGridViewList = new System.Windows.Forms.DataGridView();
            this.ColumnIndexNr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnActive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1ClientName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnFullName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6ClientCity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7ClientCountry = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3ClientZip = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4ClientHouseNr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2ClientStreet = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5ClientState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnPhone1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnPhone2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnCell = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnSkype = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11ClientEmail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel26 = new System.Windows.Forms.Panel();
            this.panel25 = new System.Windows.Forms.Panel();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel27 = new System.Windows.Forms.Panel();
            this.labelSelected = new System.Windows.Forms.Label();
            this.buttonSelect = new System.Windows.Forms.Button();
            this.buttonDisable = new System.Windows.Forms.Button();
            this.panel38 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.panel36 = new System.Windows.Forms.Panel();
            this.labelListN = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelResultN = new System.Windows.Forms.Label();
            this.checkBoxShowSearchResult = new System.Windows.Forms.CheckBox();
            this.checkBoxShowDeactivated = new System.Windows.Forms.CheckBox();
            this.panel35 = new System.Windows.Forms.Panel();
            this.buttonDuplicate = new System.Windows.Forms.Button();
            this.panel34 = new System.Windows.Forms.Panel();
            this.panel33 = new System.Windows.Forms.Panel();
            this.buttonEdit = new System.Windows.Forms.Button();
            this.panel30 = new System.Windows.Forms.Panel();
            this.panel32 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel28.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel37.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.performanceCounter1)).BeginInit();
            this.panel14.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel29.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewList)).BeginInit();
            this.panel27.SuspendLayout();
            this.panel35.SuspendLayout();
            this.panel33.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel1.Controls.Add(this.labelError);
            this.panel1.Controls.Add(this.panel19);
            this.panel1.Controls.Add(this.panel17);
            this.panel1.Controls.Add(this.panel10);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.panel43);
            this.panel1.Controls.Add(this.panel18);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1211, 38);
            this.panel1.TabIndex = 0;
            // 
            // labelError
            // 
            this.labelError.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelError.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelError.Location = new System.Drawing.Point(659, 0);
            this.labelError.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelError.Name = "labelError";
            this.labelError.Size = new System.Drawing.Size(396, 38);
            this.labelError.TabIndex = 63;
            this.labelError.Text = "...";
            this.labelError.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.label4);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel19.Location = new System.Drawing.Point(437, 0);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(222, 38);
            this.panel19.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Left;
            this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(185, 38);
            this.label4.TabIndex = 62;
            this.label4.Text = "Contact details";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel17
            // 
            this.panel17.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel17.Location = new System.Drawing.Point(412, 0);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(25, 38);
            this.panel17.TabIndex = 2;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.label7);
            this.panel10.Controls.Add(this.label1);
            this.panel10.Controls.Add(this.LabelPhysicianNr);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel10.Location = new System.Drawing.Point(20, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(392, 38);
            this.panel10.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.Dock = System.Windows.Forms.DockStyle.Right;
            this.label7.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(227, 0);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(116, 38);
            this.label7.TabIndex = 68;
            this.label7.Text = "Physician #";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(199, 38);
            this.label1.TabIndex = 1;
            this.label1.Text = "Personal details";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LabelPhysicianNr
            // 
            this.LabelPhysicianNr.Dock = System.Windows.Forms.DockStyle.Right;
            this.LabelPhysicianNr.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.LabelPhysicianNr.Location = new System.Drawing.Point(343, 0);
            this.LabelPhysicianNr.Name = "LabelPhysicianNr";
            this.LabelPhysicianNr.Size = new System.Drawing.Size(49, 38);
            this.LabelPhysicianNr.TabIndex = 69;
            this.LabelPhysicianNr.Text = "1234";
            this.LabelPhysicianNr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel5
            // 
            this.panel5.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Margin = new System.Windows.Forms.Padding(4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(20, 38);
            this.panel5.TabIndex = 0;
            // 
            // panel43
            // 
            this.panel43.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel43.Location = new System.Drawing.Point(1078, 0);
            this.panel43.Name = "panel43";
            this.panel43.Size = new System.Drawing.Size(113, 38);
            this.panel43.TabIndex = 64;
            // 
            // panel18
            // 
            this.panel18.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel18.Location = new System.Drawing.Point(1191, 0);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(20, 38);
            this.panel18.TabIndex = 3;
            // 
            // panel2
            // 
            this.panel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel2.Controls.Add(this.buttonRandom);
            this.panel2.Controls.Add(this.panel22);
            this.panel2.Controls.Add(this.panel21);
            this.panel2.Controls.Add(this.panel20);
            this.panel2.Controls.Add(this.panel7);
            this.panel2.Controls.Add(this.panel6);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 38);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1211, 282);
            this.panel2.TabIndex = 1;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // buttonRandom
            // 
            this.buttonRandom.Location = new System.Drawing.Point(1063, 6);
            this.buttonRandom.Name = "buttonRandom";
            this.buttonRandom.Size = new System.Drawing.Size(21, 25);
            this.buttonRandom.TabIndex = 0;
            this.buttonRandom.Text = "R";
            this.buttonRandom.UseVisualStyleBackColor = true;
            this.buttonRandom.Click += new System.EventHandler(this.buttonRandom_Click);
            // 
            // panel22
            // 
            this.panel22.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel22.Location = new System.Drawing.Point(1191, 0);
            this.panel22.Margin = new System.Windows.Forms.Padding(4);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(20, 282);
            this.panel22.TabIndex = 6;
            // 
            // panel21
            // 
            this.panel21.Controls.Add(this.panel23);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel21.Location = new System.Drawing.Point(437, 0);
            this.panel21.Margin = new System.Windows.Forms.Padding(4);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(618, 282);
            this.panel21.TabIndex = 5;
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panel23.Controls.Add(this.textBoxInstruction);
            this.panel23.Controls.Add(this.label9);
            this.panel23.Controls.Add(this.comboBoxCountryList);
            this.panel23.Controls.Add(this.textBoxSkype);
            this.panel23.Controls.Add(this.textBoxEmail);
            this.panel23.Controls.Add(this.labelUserEmail);
            this.panel23.Controls.Add(this.labelUserSkype);
            this.panel23.Controls.Add(this.textBoxPhone2);
            this.panel23.Controls.Add(this.labelUserPhone2);
            this.panel23.Controls.Add(this.textBoxHouseNumber);
            this.panel23.Controls.Add(this.labelUserHouseNumber);
            this.panel23.Controls.Add(this.textBoxCell);
            this.panel23.Controls.Add(this.labelUserCell);
            this.panel23.Controls.Add(this.textBoxAddress);
            this.panel23.Controls.Add(this.labelUserAddress);
            this.panel23.Controls.Add(this.comboBoxStateList);
            this.panel23.Controls.Add(this.textBoxPhone1);
            this.panel23.Controls.Add(this.labelUserPhone1);
            this.panel23.Controls.Add(this.labelUserCountry);
            this.panel23.Controls.Add(this.textBoxCity);
            this.panel23.Controls.Add(this.labelUserCity);
            this.panel23.Controls.Add(this.labelUserState);
            this.panel23.Controls.Add(this.textBoxZip);
            this.panel23.Controls.Add(this.labelUserZip);
            this.panel23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel23.Font = new System.Drawing.Font("Arial", 10F);
            this.panel23.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel23.Location = new System.Drawing.Point(0, 0);
            this.panel23.Margin = new System.Windows.Forms.Padding(4);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(618, 282);
            this.panel23.TabIndex = 125;
            // 
            // textBoxInstruction
            // 
            this.textBoxInstruction.Location = new System.Drawing.Point(89, 204);
            this.textBoxInstruction.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxInstruction.Multiline = true;
            this.textBoxInstruction.Name = "textBoxInstruction";
            this.textBoxInstruction.Size = new System.Drawing.Size(495, 59);
            this.textBoxInstruction.TabIndex = 39;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(14, 203);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 16);
            this.label9.TabIndex = 38;
            this.label9.Text = "Instruction:";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // comboBoxCountryList
            // 
            this.comboBoxCountryList.DropDownWidth = 200;
            this.comboBoxCountryList.FormattingEnabled = true;
            this.comboBoxCountryList.Items.AddRange(new object[] {
            "USA",
            "Mexico"});
            this.comboBoxCountryList.Location = new System.Drawing.Point(396, 82);
            this.comboBoxCountryList.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxCountryList.Name = "comboBoxCountryList";
            this.comboBoxCountryList.Size = new System.Drawing.Size(188, 24);
            this.comboBoxCountryList.TabIndex = 27;
            // 
            // textBoxSkype
            // 
            this.textBoxSkype.Location = new System.Drawing.Point(396, 142);
            this.textBoxSkype.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxSkype.Name = "textBoxSkype";
            this.textBoxSkype.Size = new System.Drawing.Size(188, 23);
            this.textBoxSkype.TabIndex = 35;
            // 
            // textBoxEmail
            // 
            this.textBoxEmail.Location = new System.Drawing.Point(89, 169);
            this.textBoxEmail.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxEmail.Name = "textBoxEmail";
            this.textBoxEmail.Size = new System.Drawing.Size(193, 23);
            this.textBoxEmail.TabIndex = 37;
            // 
            // labelUserEmail
            // 
            this.labelUserEmail.AutoSize = true;
            this.labelUserEmail.Location = new System.Drawing.Point(14, 172);
            this.labelUserEmail.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelUserEmail.Name = "labelUserEmail";
            this.labelUserEmail.Size = new System.Drawing.Size(46, 16);
            this.labelUserEmail.TabIndex = 36;
            this.labelUserEmail.Text = "Email:";
            // 
            // labelUserSkype
            // 
            this.labelUserSkype.AutoSize = true;
            this.labelUserSkype.Location = new System.Drawing.Point(317, 145);
            this.labelUserSkype.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelUserSkype.Name = "labelUserSkype";
            this.labelUserSkype.Size = new System.Drawing.Size(51, 16);
            this.labelUserSkype.TabIndex = 34;
            this.labelUserSkype.Text = "Skype:";
            // 
            // textBoxPhone2
            // 
            this.textBoxPhone2.Location = new System.Drawing.Point(396, 113);
            this.textBoxPhone2.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPhone2.Name = "textBoxPhone2";
            this.textBoxPhone2.Size = new System.Drawing.Size(188, 23);
            this.textBoxPhone2.TabIndex = 31;
            // 
            // labelUserPhone2
            // 
            this.labelUserPhone2.AutoSize = true;
            this.labelUserPhone2.Location = new System.Drawing.Point(317, 116);
            this.labelUserPhone2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelUserPhone2.Name = "labelUserPhone2";
            this.labelUserPhone2.Size = new System.Drawing.Size(65, 16);
            this.labelUserPhone2.TabIndex = 30;
            this.labelUserPhone2.Text = "Phone 2:";
            // 
            // textBoxHouseNumber
            // 
            this.textBoxHouseNumber.Location = new System.Drawing.Point(213, 56);
            this.textBoxHouseNumber.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxHouseNumber.Name = "textBoxHouseNumber";
            this.textBoxHouseNumber.Size = new System.Drawing.Size(69, 23);
            this.textBoxHouseNumber.TabIndex = 21;
            // 
            // labelUserHouseNumber
            // 
            this.labelUserHouseNumber.AutoSize = true;
            this.labelUserHouseNumber.Location = new System.Drawing.Point(176, 59);
            this.labelUserHouseNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelUserHouseNumber.Name = "labelUserHouseNumber";
            this.labelUserHouseNumber.Size = new System.Drawing.Size(26, 16);
            this.labelUserHouseNumber.TabIndex = 20;
            this.labelUserHouseNumber.Text = "Nr:";
            // 
            // textBoxCell
            // 
            this.textBoxCell.Location = new System.Drawing.Point(89, 142);
            this.textBoxCell.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxCell.Name = "textBoxCell";
            this.textBoxCell.Size = new System.Drawing.Size(193, 23);
            this.textBoxCell.TabIndex = 33;
            // 
            // labelUserCell
            // 
            this.labelUserCell.AutoSize = true;
            this.labelUserCell.Location = new System.Drawing.Point(14, 145);
            this.labelUserCell.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelUserCell.Name = "labelUserCell";
            this.labelUserCell.Size = new System.Drawing.Size(36, 16);
            this.labelUserCell.TabIndex = 32;
            this.labelUserCell.Text = "Cell:";
            // 
            // textBoxAddress
            // 
            this.textBoxAddress.Location = new System.Drawing.Point(89, 8);
            this.textBoxAddress.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxAddress.Multiline = true;
            this.textBoxAddress.Name = "textBoxAddress";
            this.textBoxAddress.Size = new System.Drawing.Size(495, 42);
            this.textBoxAddress.TabIndex = 17;
            // 
            // labelUserAddress
            // 
            this.labelUserAddress.AutoSize = true;
            this.labelUserAddress.Location = new System.Drawing.Point(14, 12);
            this.labelUserAddress.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelUserAddress.Name = "labelUserAddress";
            this.labelUserAddress.Size = new System.Drawing.Size(64, 16);
            this.labelUserAddress.TabIndex = 16;
            this.labelUserAddress.Text = "Address:";
            // 
            // comboBoxStateList
            // 
            this.comboBoxStateList.DropDownWidth = 200;
            this.comboBoxStateList.FormattingEnabled = true;
            this.comboBoxStateList.Items.AddRange(new object[] {
            "NA",
            "Alabama",
            "Alaska",
            "Arizona",
            "Arkansas",
            "California",
            "Colorado",
            "Connecticut",
            "Delaware",
            "District of Columbia",
            "Florida",
            "Georgia",
            "Hawaii",
            "Idaho",
            "Illinois",
            "Indiana",
            "Iowa",
            "Kansas",
            "Kentucky",
            "Louisiana",
            "Maine",
            "Maryland",
            "Massachusetts",
            "Michigan",
            "Minnesota",
            "Mississippi",
            "Missouri",
            "Montana",
            "Nebraska",
            "Nevada",
            "New Hampshire",
            "New Jersey",
            "New Mexico",
            "New York",
            "North Carolina",
            "North Dakota",
            "Ohio",
            "Oklahoma",
            "Oregon",
            "Pennsylvania",
            "Rhode Island",
            "South Carolina",
            "South Dakota",
            "Tennessee",
            "Texas",
            "Utah",
            "Vermont",
            "Virginia",
            "Washington",
            "West Virginia",
            "Wisconsin"});
            this.comboBoxStateList.Location = new System.Drawing.Point(396, 55);
            this.comboBoxStateList.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxStateList.Name = "comboBoxStateList";
            this.comboBoxStateList.Size = new System.Drawing.Size(188, 24);
            this.comboBoxStateList.TabIndex = 23;
            // 
            // textBoxPhone1
            // 
            this.textBoxPhone1.Location = new System.Drawing.Point(89, 113);
            this.textBoxPhone1.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPhone1.Name = "textBoxPhone1";
            this.textBoxPhone1.Size = new System.Drawing.Size(193, 23);
            this.textBoxPhone1.TabIndex = 29;
            // 
            // labelUserPhone1
            // 
            this.labelUserPhone1.AutoSize = true;
            this.labelUserPhone1.Location = new System.Drawing.Point(14, 116);
            this.labelUserPhone1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelUserPhone1.Name = "labelUserPhone1";
            this.labelUserPhone1.Size = new System.Drawing.Size(65, 16);
            this.labelUserPhone1.TabIndex = 28;
            this.labelUserPhone1.Text = "Phone 1:";
            // 
            // labelUserCountry
            // 
            this.labelUserCountry.AutoSize = true;
            this.labelUserCountry.Location = new System.Drawing.Point(317, 86);
            this.labelUserCountry.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelUserCountry.Name = "labelUserCountry";
            this.labelUserCountry.Size = new System.Drawing.Size(62, 16);
            this.labelUserCountry.TabIndex = 26;
            this.labelUserCountry.Text = "Country:";
            // 
            // textBoxCity
            // 
            this.textBoxCity.Location = new System.Drawing.Point(89, 83);
            this.textBoxCity.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxCity.Name = "textBoxCity";
            this.textBoxCity.Size = new System.Drawing.Size(193, 23);
            this.textBoxCity.TabIndex = 25;
            // 
            // labelUserCity
            // 
            this.labelUserCity.AutoSize = true;
            this.labelUserCity.Location = new System.Drawing.Point(14, 86);
            this.labelUserCity.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelUserCity.Name = "labelUserCity";
            this.labelUserCity.Size = new System.Drawing.Size(36, 16);
            this.labelUserCity.TabIndex = 24;
            this.labelUserCity.Text = "City:";
            // 
            // labelUserState
            // 
            this.labelUserState.AutoSize = true;
            this.labelUserState.Location = new System.Drawing.Point(317, 59);
            this.labelUserState.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelUserState.Name = "labelUserState";
            this.labelUserState.Size = new System.Drawing.Size(45, 16);
            this.labelUserState.TabIndex = 22;
            this.labelUserState.Text = "State:";
            // 
            // textBoxZip
            // 
            this.textBoxZip.Location = new System.Drawing.Point(89, 56);
            this.textBoxZip.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxZip.Name = "textBoxZip";
            this.textBoxZip.Size = new System.Drawing.Size(77, 23);
            this.textBoxZip.TabIndex = 19;
            // 
            // labelUserZip
            // 
            this.labelUserZip.AutoSize = true;
            this.labelUserZip.Location = new System.Drawing.Point(14, 59);
            this.labelUserZip.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelUserZip.Name = "labelUserZip";
            this.labelUserZip.Size = new System.Drawing.Size(31, 16);
            this.labelUserZip.TabIndex = 18;
            this.labelUserZip.Text = "Zip:";
            // 
            // panel20
            // 
            this.panel20.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel20.Location = new System.Drawing.Point(412, 0);
            this.panel20.Margin = new System.Windows.Forms.Padding(4);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(25, 282);
            this.panel20.TabIndex = 4;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.panel8);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel7.Location = new System.Drawing.Point(20, 0);
            this.panel7.Margin = new System.Windows.Forms.Padding(4);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(392, 282);
            this.panel7.TabIndex = 2;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panel8.Controls.Add(this.checkBoxActive);
            this.panel8.Controls.Add(this.textBoxClientLabel);
            this.panel8.Controls.Add(this.label8);
            this.panel8.Controls.Add(this.textBoxAgeResult);
            this.panel8.Controls.Add(this.comboBoxHospitalList);
            this.panel8.Controls.Add(this.label2);
            this.panel8.Controls.Add(this.comboBoxGenderTitleList);
            this.panel8.Controls.Add(this.labelAge);
            this.panel8.Controls.Add(this.comboBoxAcademicTitleList);
            this.panel8.Controls.Add(this.labelUserTitle);
            this.panel8.Controls.Add(this.textBoxDateOfBirthYear);
            this.panel8.Controls.Add(this.comboBoxDateOfBirthMonthList);
            this.panel8.Controls.Add(this.textBoxDateOfBirthDay);
            this.panel8.Controls.Add(this.labelUserDateOfBirth);
            this.panel8.Controls.Add(this.comboBoxGender);
            this.panel8.Controls.Add(this.label6);
            this.panel8.Controls.Add(this.textBoxLastName);
            this.panel8.Controls.Add(this.labelUserLastName);
            this.panel8.Controls.Add(this.textBoxMiddleName);
            this.panel8.Controls.Add(this.labelUserMiddleName);
            this.panel8.Controls.Add(this.textBoxFirstName);
            this.panel8.Controls.Add(this.labelUserFirstName);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Font = new System.Drawing.Font("Arial", 10F);
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Margin = new System.Windows.Forms.Padding(4);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(392, 282);
            this.panel8.TabIndex = 99;
            // 
            // checkBoxActive
            // 
            this.checkBoxActive.AutoSize = true;
            this.checkBoxActive.Location = new System.Drawing.Point(313, 143);
            this.checkBoxActive.Name = "checkBoxActive";
            this.checkBoxActive.Size = new System.Drawing.Size(65, 20);
            this.checkBoxActive.TabIndex = 108;
            this.checkBoxActive.Text = "Active";
            this.checkBoxActive.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBoxActive.UseVisualStyleBackColor = true;
            // 
            // textBoxClientLabel
            // 
            this.textBoxClientLabel.Location = new System.Drawing.Point(124, 142);
            this.textBoxClientLabel.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxClientLabel.MaxLength = 16;
            this.textBoxClientLabel.Name = "textBoxClientLabel";
            this.textBoxClientLabel.Size = new System.Drawing.Size(182, 23);
            this.textBoxClientLabel.TabIndex = 107;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.label8.Location = new System.Drawing.Point(7, 145);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(109, 16);
            this.label8.TabIndex = 106;
            this.label8.Text = "Display name:*";
            // 
            // textBoxAgeResult
            // 
            this.textBoxAgeResult.Location = new System.Drawing.Point(124, 207);
            this.textBoxAgeResult.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxAgeResult.Name = "textBoxAgeResult";
            this.textBoxAgeResult.ReadOnly = true;
            this.textBoxAgeResult.Size = new System.Drawing.Size(38, 23);
            this.textBoxAgeResult.TabIndex = 105;
            // 
            // comboBoxHospitalList
            // 
            this.comboBoxHospitalList.DropDownWidth = 200;
            this.comboBoxHospitalList.FormattingEnabled = true;
            this.comboBoxHospitalList.Items.AddRange(new object[] {
            "USA",
            "Mexico"});
            this.comboBoxHospitalList.Location = new System.Drawing.Point(124, 239);
            this.comboBoxHospitalList.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxHospitalList.Name = "comboBoxHospitalList";
            this.comboBoxHospitalList.Size = new System.Drawing.Size(249, 24);
            this.comboBoxHospitalList.TabIndex = 103;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 242);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 16);
            this.label2.TabIndex = 102;
            this.label2.Text = "Hospital:";
            // 
            // comboBoxGenderTitleList
            // 
            this.comboBoxGenderTitleList.DropDownWidth = 200;
            this.comboBoxGenderTitleList.FormattingEnabled = true;
            this.comboBoxGenderTitleList.Items.AddRange(new object[] {
            "Mr.",
            "Mrs.",
            "Ms."});
            this.comboBoxGenderTitleList.Location = new System.Drawing.Point(351, 13);
            this.comboBoxGenderTitleList.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxGenderTitleList.Name = "comboBoxGenderTitleList";
            this.comboBoxGenderTitleList.Size = new System.Drawing.Size(22, 24);
            this.comboBoxGenderTitleList.TabIndex = 2;
            this.comboBoxGenderTitleList.Visible = false;
            // 
            // labelAge
            // 
            this.labelAge.AutoSize = true;
            this.labelAge.Location = new System.Drawing.Point(7, 210);
            this.labelAge.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelAge.Name = "labelAge";
            this.labelAge.Size = new System.Drawing.Size(37, 16);
            this.labelAge.TabIndex = 100;
            this.labelAge.Text = "Age:";
            // 
            // comboBoxAcademicTitleList
            // 
            this.comboBoxAcademicTitleList.DropDownWidth = 200;
            this.comboBoxAcademicTitleList.FormattingEnabled = true;
            this.comboBoxAcademicTitleList.Items.AddRange(new object[] {
            "Prof. Dr.",
            "Prof.",
            "Dr.",
            "Msc.",
            "Bsc.",
            "MBBS",
            "MBA"});
            this.comboBoxAcademicTitleList.Location = new System.Drawing.Point(124, 12);
            this.comboBoxAcademicTitleList.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxAcademicTitleList.Name = "comboBoxAcademicTitleList";
            this.comboBoxAcademicTitleList.Size = new System.Drawing.Size(115, 24);
            this.comboBoxAcademicTitleList.TabIndex = 3;
            // 
            // labelUserTitle
            // 
            this.labelUserTitle.AutoSize = true;
            this.labelUserTitle.Location = new System.Drawing.Point(7, 16);
            this.labelUserTitle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelUserTitle.Name = "labelUserTitle";
            this.labelUserTitle.Size = new System.Drawing.Size(38, 16);
            this.labelUserTitle.TabIndex = 1;
            this.labelUserTitle.Text = "Title:";
            // 
            // textBoxDateOfBirthYear
            // 
            this.textBoxDateOfBirthYear.Location = new System.Drawing.Point(311, 176);
            this.textBoxDateOfBirthYear.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxDateOfBirthYear.Name = "textBoxDateOfBirthYear";
            this.textBoxDateOfBirthYear.Size = new System.Drawing.Size(62, 23);
            this.textBoxDateOfBirthYear.TabIndex = 13;
            this.textBoxDateOfBirthYear.TextChanged += new System.EventHandler(this.textBoxDateOfBirthYear_TextChanged);
            // 
            // comboBoxDateOfBirthMonthList
            // 
            this.comboBoxDateOfBirthMonthList.DropDownWidth = 200;
            this.comboBoxDateOfBirthMonthList.FormattingEnabled = true;
            this.comboBoxDateOfBirthMonthList.Items.AddRange(new object[] {
            "January",
            "February",
            "March",
            "April",
            "May ",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"});
            this.comboBoxDateOfBirthMonthList.Location = new System.Drawing.Point(170, 175);
            this.comboBoxDateOfBirthMonthList.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxDateOfBirthMonthList.Name = "comboBoxDateOfBirthMonthList";
            this.comboBoxDateOfBirthMonthList.Size = new System.Drawing.Size(136, 24);
            this.comboBoxDateOfBirthMonthList.TabIndex = 12;
            this.comboBoxDateOfBirthMonthList.TextUpdate += new System.EventHandler(this.comboBoxDateOfBirthMonthList_TextUpdate);
            // 
            // textBoxDateOfBirthDay
            // 
            this.textBoxDateOfBirthDay.Location = new System.Drawing.Point(124, 176);
            this.textBoxDateOfBirthDay.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxDateOfBirthDay.Name = "textBoxDateOfBirthDay";
            this.textBoxDateOfBirthDay.Size = new System.Drawing.Size(38, 23);
            this.textBoxDateOfBirthDay.TabIndex = 11;
            this.textBoxDateOfBirthDay.TextChanged += new System.EventHandler(this.textBoxDateOfBirthDay_TextChanged);
            // 
            // labelUserDateOfBirth
            // 
            this.labelUserDateOfBirth.AutoSize = true;
            this.labelUserDateOfBirth.Location = new System.Drawing.Point(7, 179);
            this.labelUserDateOfBirth.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelUserDateOfBirth.Name = "labelUserDateOfBirth";
            this.labelUserDateOfBirth.Size = new System.Drawing.Size(90, 16);
            this.labelUserDateOfBirth.TabIndex = 10;
            this.labelUserDateOfBirth.Text = "Date of birth:";
            // 
            // comboBoxGender
            // 
            this.comboBoxGender.DropDownWidth = 200;
            this.comboBoxGender.FormattingEnabled = true;
            this.comboBoxGender.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.comboBoxGender.Location = new System.Drawing.Point(259, 206);
            this.comboBoxGender.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxGender.Name = "comboBoxGender";
            this.comboBoxGender.Size = new System.Drawing.Size(114, 24);
            this.comboBoxGender.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(191, 210);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 16);
            this.label6.TabIndex = 14;
            this.label6.Text = "Gender:";
            // 
            // textBoxLastName
            // 
            this.textBoxLastName.Location = new System.Drawing.Point(124, 113);
            this.textBoxLastName.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxLastName.MaxLength = 60;
            this.textBoxLastName.Name = "textBoxLastName";
            this.textBoxLastName.Size = new System.Drawing.Size(249, 23);
            this.textBoxLastName.TabIndex = 9;
            this.textBoxLastName.TextChanged += new System.EventHandler(this.textBoxLastName_TextChanged);
            // 
            // labelUserLastName
            // 
            this.labelUserLastName.AutoSize = true;
            this.labelUserLastName.Location = new System.Drawing.Point(7, 116);
            this.labelUserLastName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelUserLastName.Name = "labelUserLastName";
            this.labelUserLastName.Size = new System.Drawing.Size(78, 16);
            this.labelUserLastName.TabIndex = 8;
            this.labelUserLastName.Text = "Last name:";
            this.labelUserLastName.Click += new System.EventHandler(this.labelUserLastName_Click);
            // 
            // textBoxMiddleName
            // 
            this.textBoxMiddleName.Location = new System.Drawing.Point(124, 83);
            this.textBoxMiddleName.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxMiddleName.MaxLength = 30;
            this.textBoxMiddleName.Name = "textBoxMiddleName";
            this.textBoxMiddleName.Size = new System.Drawing.Size(249, 23);
            this.textBoxMiddleName.TabIndex = 7;
            // 
            // labelUserMiddleName
            // 
            this.labelUserMiddleName.AutoSize = true;
            this.labelUserMiddleName.Location = new System.Drawing.Point(7, 86);
            this.labelUserMiddleName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelUserMiddleName.Name = "labelUserMiddleName";
            this.labelUserMiddleName.Size = new System.Drawing.Size(92, 16);
            this.labelUserMiddleName.TabIndex = 6;
            this.labelUserMiddleName.Text = "Middle name:";
            // 
            // textBoxFirstName
            // 
            this.textBoxFirstName.Location = new System.Drawing.Point(124, 55);
            this.textBoxFirstName.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxFirstName.MaxLength = 60;
            this.textBoxFirstName.Name = "textBoxFirstName";
            this.textBoxFirstName.Size = new System.Drawing.Size(249, 23);
            this.textBoxFirstName.TabIndex = 5;
            // 
            // labelUserFirstName
            // 
            this.labelUserFirstName.AutoSize = true;
            this.labelUserFirstName.Location = new System.Drawing.Point(7, 58);
            this.labelUserFirstName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelUserFirstName.Name = "labelUserFirstName";
            this.labelUserFirstName.Size = new System.Drawing.Size(79, 16);
            this.labelUserFirstName.TabIndex = 4;
            this.labelUserFirstName.Text = "First name:";
            // 
            // panel6
            // 
            this.panel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Margin = new System.Windows.Forms.Padding(4);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(20, 282);
            this.panel6.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 320);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1211, 17);
            this.panel3.TabIndex = 2;
            // 
            // panel4
            // 
            this.panel4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel4.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.panel4.Controls.Add(this.panel28);
            this.panel4.Controls.Add(this.panel13);
            this.panel4.Controls.Add(this.panel12);
            this.panel4.Controls.Add(this.panel11);
            this.panel4.Controls.Add(this.panel9);
            this.panel4.Controls.Add(this.panel37);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 337);
            this.panel4.Margin = new System.Windows.Forms.Padding(4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1211, 47);
            this.panel4.TabIndex = 3;
            // 
            // panel28
            // 
            this.panel28.Controls.Add(this.panel39);
            this.panel28.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel28.Location = new System.Drawing.Point(416, 0);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(331, 47);
            this.panel28.TabIndex = 7;
            // 
            // panel39
            // 
            this.panel39.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel39.Location = new System.Drawing.Point(304, 0);
            this.panel39.Margin = new System.Windows.Forms.Padding(4);
            this.panel39.Name = "panel39";
            this.panel39.Size = new System.Drawing.Size(27, 47);
            this.panel39.TabIndex = 106;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.buttonAddUpdate);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel13.Location = new System.Drawing.Point(747, 0);
            this.panel13.Margin = new System.Windows.Forms.Padding(4);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(133, 47);
            this.panel13.TabIndex = 4;
            // 
            // buttonAddUpdate
            // 
            this.buttonAddUpdate.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonAddUpdate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonAddUpdate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonAddUpdate.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonAddUpdate.FlatAppearance.BorderSize = 0;
            this.buttonAddUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddUpdate.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonAddUpdate.ForeColor = System.Drawing.Color.White;
            this.buttonAddUpdate.Location = new System.Drawing.Point(0, 0);
            this.buttonAddUpdate.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.buttonAddUpdate.Name = "buttonAddUpdate";
            this.buttonAddUpdate.Size = new System.Drawing.Size(133, 47);
            this.buttonAddUpdate.TabIndex = 2;
            this.buttonAddUpdate.Text = "Add";
            this.buttonAddUpdate.UseVisualStyleBackColor = false;
            this.buttonAddUpdate.Click += new System.EventHandler(this.buttonAddUpdate_Click);
            // 
            // panel12
            // 
            this.panel12.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel12.Location = new System.Drawing.Point(880, 0);
            this.panel12.Margin = new System.Windows.Forms.Padding(4);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(20, 47);
            this.panel12.TabIndex = 3;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.buttonCancel);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel11.Location = new System.Drawing.Point(900, 0);
            this.panel11.Margin = new System.Windows.Forms.Padding(4);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(133, 47);
            this.panel11.TabIndex = 2;
            // 
            // buttonCancel
            // 
            this.buttonCancel.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonCancel.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonCancel.FlatAppearance.BorderSize = 0;
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCancel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonCancel.ForeColor = System.Drawing.Color.White;
            this.buttonCancel.Location = new System.Drawing.Point(0, 0);
            this.buttonCancel.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(133, 47);
            this.buttonCancel.TabIndex = 2;
            this.buttonCancel.Text = "Close";
            this.buttonCancel.UseVisualStyleBackColor = false;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // panel9
            // 
            this.panel9.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel9.Location = new System.Drawing.Point(1033, 0);
            this.panel9.Margin = new System.Windows.Forms.Padding(4);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(178, 47);
            this.panel9.TabIndex = 0;
            // 
            // panel37
            // 
            this.panel37.Controls.Add(this.buttonClear);
            this.panel37.Controls.Add(this.panel41);
            this.panel37.Controls.Add(this.buttonDup);
            this.panel37.Controls.Add(this.panel42);
            this.panel37.Controls.Add(this.buttonSearch);
            this.panel37.Controls.Add(this.panel40);
            this.panel37.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel37.Location = new System.Drawing.Point(0, 0);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(412, 47);
            this.panel37.TabIndex = 75;
            // 
            // buttonClear
            // 
            this.buttonClear.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonClear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonClear.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonClear.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonClear.FlatAppearance.BorderSize = 0;
            this.buttonClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonClear.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonClear.ForeColor = System.Drawing.Color.White;
            this.buttonClear.Location = new System.Drawing.Point(72, 0);
            this.buttonClear.Margin = new System.Windows.Forms.Padding(2);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(100, 47);
            this.buttonClear.TabIndex = 11;
            this.buttonClear.Text = "Clear";
            this.buttonClear.UseVisualStyleBackColor = false;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // panel41
            // 
            this.panel41.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel41.Location = new System.Drawing.Point(172, 0);
            this.panel41.Margin = new System.Windows.Forms.Padding(4);
            this.panel41.Name = "panel41";
            this.panel41.Size = new System.Drawing.Size(20, 47);
            this.panel41.TabIndex = 109;
            // 
            // buttonDup
            // 
            this.buttonDup.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonDup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonDup.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonDup.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonDup.FlatAppearance.BorderSize = 0;
            this.buttonDup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDup.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonDup.ForeColor = System.Drawing.Color.White;
            this.buttonDup.Location = new System.Drawing.Point(192, 0);
            this.buttonDup.Margin = new System.Windows.Forms.Padding(2);
            this.buttonDup.Name = "buttonDup";
            this.buttonDup.Size = new System.Drawing.Size(100, 47);
            this.buttonDup.TabIndex = 12;
            this.buttonDup.Text = "Duplicate";
            this.buttonDup.UseVisualStyleBackColor = false;
            this.buttonDup.Click += new System.EventHandler(this.buttonDup_Click);
            // 
            // panel42
            // 
            this.panel42.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel42.Location = new System.Drawing.Point(292, 0);
            this.panel42.Margin = new System.Windows.Forms.Padding(4);
            this.panel42.Name = "panel42";
            this.panel42.Size = new System.Drawing.Size(20, 47);
            this.panel42.TabIndex = 110;
            // 
            // buttonSearch
            // 
            this.buttonSearch.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonSearch.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonSearch.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonSearch.FlatAppearance.BorderSize = 0;
            this.buttonSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSearch.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonSearch.ForeColor = System.Drawing.Color.White;
            this.buttonSearch.Location = new System.Drawing.Point(312, 0);
            this.buttonSearch.Margin = new System.Windows.Forms.Padding(2);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(100, 47);
            this.buttonSearch.TabIndex = 13;
            this.buttonSearch.Text = "Search";
            this.buttonSearch.UseVisualStyleBackColor = false;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // panel40
            // 
            this.panel40.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel40.Location = new System.Drawing.Point(0, 0);
            this.panel40.Margin = new System.Windows.Forms.Padding(4);
            this.panel40.Name = "panel40";
            this.panel40.Size = new System.Drawing.Size(27, 47);
            this.panel40.TabIndex = 108;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.label3);
            this.panel14.Controls.Add(this.panel24);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel14.Location = new System.Drawing.Point(0, 384);
            this.panel14.Margin = new System.Windows.Forms.Padding(4);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(1211, 38);
            this.panel14.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Left;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(20, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(240, 38);
            this.label3.TabIndex = 2;
            this.label3.Text = "Physician list";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel24
            // 
            this.panel24.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel24.Location = new System.Drawing.Point(0, 0);
            this.panel24.Margin = new System.Windows.Forms.Padding(4);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(20, 38);
            this.panel24.TabIndex = 1;
            // 
            // panel15
            // 
            this.panel15.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel15.Controls.Add(this.panel29);
            this.panel15.Controls.Add(this.panel26);
            this.panel15.Controls.Add(this.panel25);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel15.Location = new System.Drawing.Point(0, 422);
            this.panel15.Margin = new System.Windows.Forms.Padding(4);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(1211, 221);
            this.panel15.TabIndex = 5;
            // 
            // panel29
            // 
            this.panel29.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel29.Controls.Add(this.dataGridViewList);
            this.panel29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel29.Location = new System.Drawing.Point(20, 0);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(1171, 221);
            this.panel29.TabIndex = 3;
            // 
            // dataGridViewList
            // 
            this.dataGridViewList.AllowUserToAddRows = false;
            this.dataGridViewList.AllowUserToDeleteRows = false;
            this.dataGridViewList.AllowUserToOrderColumns = true;
            this.dataGridViewList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewList.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.dataGridViewList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnIndexNr,
            this.ColumnActive,
            this.Column1ClientName,
            this.ColumnFullName,
            this.Column6ClientCity,
            this.Column7ClientCountry,
            this.Column3ClientZip,
            this.Column4ClientHouseNr,
            this.Column2ClientStreet,
            this.Column5ClientState,
            this.ColumnPhone1,
            this.ColumnPhone2,
            this.ColumnCell,
            this.ColumnSkype,
            this.Column11ClientEmail});
            this.dataGridViewList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewList.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewList.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridViewList.Name = "dataGridViewList";
            this.dataGridViewList.ReadOnly = true;
            this.dataGridViewList.RowHeadersVisible = false;
            this.dataGridViewList.RowTemplate.Height = 33;
            this.dataGridViewList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewList.ShowEditingIcon = false;
            this.dataGridViewList.Size = new System.Drawing.Size(1171, 221);
            this.dataGridViewList.TabIndex = 32;
            this.dataGridViewList.SelectionChanged += new System.EventHandler(this.dataGridViewList_SelectionChanged);
            this.dataGridViewList.Click += new System.EventHandler(this.dataGridViewList_Click);
            this.dataGridViewList.DoubleClick += new System.EventHandler(this.dataGridViewList_DoubleClick);
            // 
            // ColumnIndexNr
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.ColumnIndexNr.DefaultCellStyle = dataGridViewCellStyle1;
            this.ColumnIndexNr.HeaderText = "Nr";
            this.ColumnIndexNr.Name = "ColumnIndexNr";
            this.ColumnIndexNr.ReadOnly = true;
            this.ColumnIndexNr.Width = 48;
            // 
            // ColumnActive
            // 
            this.ColumnActive.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ColumnActive.HeaderText = "Active";
            this.ColumnActive.Name = "ColumnActive";
            this.ColumnActive.ReadOnly = true;
            this.ColumnActive.Width = 70;
            // 
            // Column1ClientName
            // 
            this.Column1ClientName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column1ClientName.HeaderText = "Display Name";
            this.Column1ClientName.MaxInputLength = 30;
            this.Column1ClientName.Name = "Column1ClientName";
            this.Column1ClientName.ReadOnly = true;
            this.Column1ClientName.Width = 120;
            // 
            // ColumnFullName
            // 
            this.ColumnFullName.HeaderText = "Full Name";
            this.ColumnFullName.Name = "ColumnFullName";
            this.ColumnFullName.ReadOnly = true;
            this.ColumnFullName.Width = 96;
            // 
            // Column6ClientCity
            // 
            this.Column6ClientCity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column6ClientCity.HeaderText = "City";
            this.Column6ClientCity.MaxInputLength = 30;
            this.Column6ClientCity.Name = "Column6ClientCity";
            this.Column6ClientCity.ReadOnly = true;
            this.Column6ClientCity.Width = 56;
            // 
            // Column7ClientCountry
            // 
            this.Column7ClientCountry.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column7ClientCountry.HeaderText = "Country";
            this.Column7ClientCountry.MaxInputLength = 32;
            this.Column7ClientCountry.Name = "Column7ClientCountry";
            this.Column7ClientCountry.ReadOnly = true;
            this.Column7ClientCountry.Width = 82;
            // 
            // Column3ClientZip
            // 
            this.Column3ClientZip.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column3ClientZip.HeaderText = "Zip";
            this.Column3ClientZip.MaxInputLength = 10;
            this.Column3ClientZip.Name = "Column3ClientZip";
            this.Column3ClientZip.ReadOnly = true;
            this.Column3ClientZip.Width = 53;
            // 
            // Column4ClientHouseNr
            // 
            this.Column4ClientHouseNr.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column4ClientHouseNr.HeaderText = "Nr";
            this.Column4ClientHouseNr.MaxInputLength = 8;
            this.Column4ClientHouseNr.Name = "Column4ClientHouseNr";
            this.Column4ClientHouseNr.ReadOnly = true;
            this.Column4ClientHouseNr.Width = 48;
            // 
            // Column2ClientStreet
            // 
            this.Column2ClientStreet.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column2ClientStreet.HeaderText = "Street";
            this.Column2ClientStreet.MaxInputLength = 30;
            this.Column2ClientStreet.Name = "Column2ClientStreet";
            this.Column2ClientStreet.ReadOnly = true;
            this.Column2ClientStreet.Width = 71;
            // 
            // Column5ClientState
            // 
            this.Column5ClientState.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column5ClientState.HeaderText = "State";
            this.Column5ClientState.MaxInputLength = 30;
            this.Column5ClientState.Name = "Column5ClientState";
            this.Column5ClientState.ReadOnly = true;
            this.Column5ClientState.Width = 66;
            // 
            // ColumnPhone1
            // 
            this.ColumnPhone1.HeaderText = "Phone1";
            this.ColumnPhone1.Name = "ColumnPhone1";
            this.ColumnPhone1.ReadOnly = true;
            this.ColumnPhone1.Width = 82;
            // 
            // ColumnPhone2
            // 
            this.ColumnPhone2.HeaderText = "Phone2";
            this.ColumnPhone2.Name = "ColumnPhone2";
            this.ColumnPhone2.ReadOnly = true;
            this.ColumnPhone2.Width = 82;
            // 
            // ColumnCell
            // 
            this.ColumnCell.HeaderText = "Cell phone";
            this.ColumnCell.Name = "ColumnCell";
            this.ColumnCell.ReadOnly = true;
            // 
            // ColumnSkype
            // 
            this.ColumnSkype.HeaderText = "Skype";
            this.ColumnSkype.Name = "ColumnSkype";
            this.ColumnSkype.ReadOnly = true;
            this.ColumnSkype.Width = 72;
            // 
            // Column11ClientEmail
            // 
            this.Column11ClientEmail.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column11ClientEmail.HeaderText = "Email";
            this.Column11ClientEmail.MaxInputLength = 25;
            this.Column11ClientEmail.Name = "Column11ClientEmail";
            this.Column11ClientEmail.ReadOnly = true;
            this.Column11ClientEmail.Width = 67;
            // 
            // panel26
            // 
            this.panel26.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel26.Location = new System.Drawing.Point(1191, 0);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(20, 221);
            this.panel26.TabIndex = 2;
            // 
            // panel25
            // 
            this.panel25.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel25.Location = new System.Drawing.Point(0, 0);
            this.panel25.Margin = new System.Windows.Forms.Padding(4);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(20, 221);
            this.panel25.TabIndex = 1;
            // 
            // panel16
            // 
            this.panel16.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel16.Location = new System.Drawing.Point(0, 643);
            this.panel16.Margin = new System.Windows.Forms.Padding(4);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(1211, 11);
            this.panel16.TabIndex = 6;
            // 
            // panel27
            // 
            this.panel27.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel27.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.panel27.Controls.Add(this.labelSelected);
            this.panel27.Controls.Add(this.buttonSelect);
            this.panel27.Controls.Add(this.buttonDisable);
            this.panel27.Controls.Add(this.panel38);
            this.panel27.Controls.Add(this.button1);
            this.panel27.Controls.Add(this.panel36);
            this.panel27.Controls.Add(this.labelListN);
            this.panel27.Controls.Add(this.label5);
            this.panel27.Controls.Add(this.labelResultN);
            this.panel27.Controls.Add(this.checkBoxShowSearchResult);
            this.panel27.Controls.Add(this.checkBoxShowDeactivated);
            this.panel27.Controls.Add(this.panel35);
            this.panel27.Controls.Add(this.panel34);
            this.panel27.Controls.Add(this.panel33);
            this.panel27.Controls.Add(this.panel30);
            this.panel27.Controls.Add(this.panel32);
            this.panel27.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel27.Location = new System.Drawing.Point(0, 654);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(1211, 40);
            this.panel27.TabIndex = 7;
            // 
            // labelSelected
            // 
            this.labelSelected.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSelected.Font = new System.Drawing.Font("Arial", 10F);
            this.labelSelected.ForeColor = System.Drawing.Color.White;
            this.labelSelected.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.labelSelected.Location = new System.Drawing.Point(662, 0);
            this.labelSelected.Name = "labelSelected";
            this.labelSelected.Size = new System.Drawing.Size(56, 40);
            this.labelSelected.TabIndex = 25;
            this.labelSelected.Text = "0";
            this.labelSelected.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // buttonSelect
            // 
            this.buttonSelect.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonSelect.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonSelect.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonSelect.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonSelect.FlatAppearance.BorderSize = 0;
            this.buttonSelect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSelect.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonSelect.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonSelect.Location = new System.Drawing.Point(566, 0);
            this.buttonSelect.Margin = new System.Windows.Forms.Padding(2);
            this.buttonSelect.Name = "buttonSelect";
            this.buttonSelect.Size = new System.Drawing.Size(96, 40);
            this.buttonSelect.TabIndex = 24;
            this.buttonSelect.Text = "Select";
            this.buttonSelect.UseVisualStyleBackColor = false;
            this.buttonSelect.Click += new System.EventHandler(this.buttonSelect_Click);
            // 
            // buttonDisable
            // 
            this.buttonDisable.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonDisable.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonDisable.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonDisable.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonDisable.FlatAppearance.BorderSize = 0;
            this.buttonDisable.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDisable.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonDisable.ForeColor = System.Drawing.Color.White;
            this.buttonDisable.Location = new System.Drawing.Point(690, 0);
            this.buttonDisable.Margin = new System.Windows.Forms.Padding(2);
            this.buttonDisable.Name = "buttonDisable";
            this.buttonDisable.Size = new System.Drawing.Size(111, 40);
            this.buttonDisable.TabIndex = 23;
            this.buttonDisable.Text = "Disable";
            this.buttonDisable.UseVisualStyleBackColor = false;
            this.buttonDisable.Click += new System.EventHandler(this.buttonDisable_Click);
            // 
            // panel38
            // 
            this.panel38.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel38.Location = new System.Drawing.Point(801, 0);
            this.panel38.Name = "panel38";
            this.panel38.Size = new System.Drawing.Size(20, 40);
            this.panel38.TabIndex = 22;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button1.Dock = System.Windows.Forms.DockStyle.Left;
            this.button1.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(445, 0);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(121, 40);
            this.button1.TabIndex = 17;
            this.button1.Text = "Refresh";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel36
            // 
            this.panel36.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel36.Location = new System.Drawing.Point(422, 0);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(23, 40);
            this.panel36.TabIndex = 18;
            // 
            // labelListN
            // 
            this.labelListN.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelListN.Font = new System.Drawing.Font("Arial", 10F);
            this.labelListN.ForeColor = System.Drawing.Color.White;
            this.labelListN.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.labelListN.Location = new System.Drawing.Point(372, 0);
            this.labelListN.Name = "labelListN";
            this.labelListN.Size = new System.Drawing.Size(50, 40);
            this.labelListN.TabIndex = 20;
            this.labelListN.Text = "0";
            this.labelListN.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Left;
            this.label5.Font = new System.Drawing.Font("Arial", 10F);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label5.Location = new System.Drawing.Point(356, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(16, 40);
            this.label5.TabIndex = 19;
            this.label5.Text = " / ";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelResultN
            // 
            this.labelResultN.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelResultN.Font = new System.Drawing.Font("Arial", 10F);
            this.labelResultN.ForeColor = System.Drawing.Color.White;
            this.labelResultN.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.labelResultN.Location = new System.Drawing.Point(314, 0);
            this.labelResultN.Name = "labelResultN";
            this.labelResultN.Size = new System.Drawing.Size(42, 40);
            this.labelResultN.TabIndex = 16;
            this.labelResultN.Text = "0";
            this.labelResultN.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // checkBoxShowSearchResult
            // 
            this.checkBoxShowSearchResult.AutoSize = true;
            this.checkBoxShowSearchResult.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxShowSearchResult.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxShowSearchResult.ForeColor = System.Drawing.Color.White;
            this.checkBoxShowSearchResult.Location = new System.Drawing.Point(160, 0);
            this.checkBoxShowSearchResult.Name = "checkBoxShowSearchResult";
            this.checkBoxShowSearchResult.Size = new System.Drawing.Size(154, 40);
            this.checkBoxShowSearchResult.TabIndex = 15;
            this.checkBoxShowSearchResult.Text = "Show Search Result";
            this.checkBoxShowSearchResult.UseVisualStyleBackColor = true;
            this.checkBoxShowSearchResult.CheckedChanged += new System.EventHandler(this.checkBoxShowSearchResult_CheckedChanged);
            // 
            // checkBoxShowDeactivated
            // 
            this.checkBoxShowDeactivated.AutoSize = true;
            this.checkBoxShowDeactivated.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxShowDeactivated.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxShowDeactivated.ForeColor = System.Drawing.Color.White;
            this.checkBoxShowDeactivated.Location = new System.Drawing.Point(20, 0);
            this.checkBoxShowDeactivated.Name = "checkBoxShowDeactivated";
            this.checkBoxShowDeactivated.Size = new System.Drawing.Size(140, 40);
            this.checkBoxShowDeactivated.TabIndex = 14;
            this.checkBoxShowDeactivated.Text = "Show Deactivated";
            this.checkBoxShowDeactivated.UseVisualStyleBackColor = true;
            this.checkBoxShowDeactivated.CheckedChanged += new System.EventHandler(this.checkBoxShowDeactivated_CheckedChanged);
            // 
            // panel35
            // 
            this.panel35.Controls.Add(this.buttonDuplicate);
            this.panel35.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel35.Location = new System.Drawing.Point(821, 0);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(175, 40);
            this.panel35.TabIndex = 5;
            // 
            // buttonDuplicate
            // 
            this.buttonDuplicate.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonDuplicate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonDuplicate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonDuplicate.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonDuplicate.FlatAppearance.BorderSize = 0;
            this.buttonDuplicate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDuplicate.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonDuplicate.ForeColor = System.Drawing.Color.White;
            this.buttonDuplicate.Location = new System.Drawing.Point(0, 0);
            this.buttonDuplicate.Margin = new System.Windows.Forms.Padding(2);
            this.buttonDuplicate.Name = "buttonDuplicate";
            this.buttonDuplicate.Size = new System.Drawing.Size(175, 40);
            this.buttonDuplicate.TabIndex = 2;
            this.buttonDuplicate.Text = "Duplicate Physician";
            this.buttonDuplicate.UseVisualStyleBackColor = false;
            this.buttonDuplicate.Click += new System.EventHandler(this.buttonDuplicate_Click);
            // 
            // panel34
            // 
            this.panel34.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel34.Location = new System.Drawing.Point(996, 0);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(20, 40);
            this.panel34.TabIndex = 4;
            // 
            // panel33
            // 
            this.panel33.Controls.Add(this.buttonEdit);
            this.panel33.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel33.Location = new System.Drawing.Point(1016, 0);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(175, 40);
            this.panel33.TabIndex = 3;
            // 
            // buttonEdit
            // 
            this.buttonEdit.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonEdit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonEdit.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonEdit.FlatAppearance.BorderSize = 0;
            this.buttonEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEdit.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonEdit.ForeColor = System.Drawing.Color.White;
            this.buttonEdit.Location = new System.Drawing.Point(0, 0);
            this.buttonEdit.Margin = new System.Windows.Forms.Padding(2);
            this.buttonEdit.Name = "buttonEdit";
            this.buttonEdit.Size = new System.Drawing.Size(175, 40);
            this.buttonEdit.TabIndex = 2;
            this.buttonEdit.Text = "Edit Physician";
            this.buttonEdit.UseVisualStyleBackColor = false;
            this.buttonEdit.Click += new System.EventHandler(this.buttonEdit_Click);
            // 
            // panel30
            // 
            this.panel30.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel30.Location = new System.Drawing.Point(1191, 0);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(20, 40);
            this.panel30.TabIndex = 0;
            // 
            // panel32
            // 
            this.panel32.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel32.Location = new System.Drawing.Point(0, 0);
            this.panel32.Margin = new System.Windows.Forms.Padding(4);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(20, 40);
            this.panel32.TabIndex = 21;
            // 
            // CPhysiciansForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(1211, 694);
            this.Controls.Add(this.panel15);
            this.Controls.Add(this.panel16);
            this.Controls.Add(this.panel14);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel27);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "CPhysiciansForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Physicians";
            this.panel1.ResumeLayout(false);
            this.panel19.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel21.ResumeLayout(false);
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel28.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel37.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.performanceCounter1)).EndInit();
            this.panel14.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel29.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewList)).EndInit();
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            this.panel35.ResumeLayout(false);
            this.panel33.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel9;
        private System.Diagnostics.PerformanceCounter performanceCounter1;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonAddUpdate;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.ComboBox comboBoxCountryList;
        private System.Windows.Forms.TextBox textBoxSkype;
        private System.Windows.Forms.TextBox textBoxEmail;
        private System.Windows.Forms.Label labelUserEmail;
        private System.Windows.Forms.Label labelUserSkype;
        private System.Windows.Forms.TextBox textBoxPhone2;
        private System.Windows.Forms.Label labelUserPhone2;
        private System.Windows.Forms.TextBox textBoxHouseNumber;
        private System.Windows.Forms.Label labelUserHouseNumber;
        private System.Windows.Forms.TextBox textBoxCell;
        private System.Windows.Forms.Label labelUserCell;
        private System.Windows.Forms.TextBox textBoxAddress;
        private System.Windows.Forms.Label labelUserAddress;
        private System.Windows.Forms.ComboBox comboBoxStateList;
        private System.Windows.Forms.TextBox textBoxPhone1;
        private System.Windows.Forms.Label labelUserPhone1;
        private System.Windows.Forms.Label labelUserCountry;
        private System.Windows.Forms.TextBox textBoxCity;
        private System.Windows.Forms.Label labelUserCity;
        private System.Windows.Forms.Label labelUserState;
        private System.Windows.Forms.TextBox textBoxZip;
        private System.Windows.Forms.Label labelUserZip;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.ComboBox comboBoxHospitalList;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelAge;
        private System.Windows.Forms.ComboBox comboBoxAcademicTitleList;
        private System.Windows.Forms.Label labelUserTitle;
        private System.Windows.Forms.ComboBox comboBoxGenderTitleList;
        private System.Windows.Forms.TextBox textBoxDateOfBirthYear;
        private System.Windows.Forms.ComboBox comboBoxDateOfBirthMonthList;
        private System.Windows.Forms.TextBox textBoxDateOfBirthDay;
        private System.Windows.Forms.Label labelUserDateOfBirth;
        private System.Windows.Forms.ComboBox comboBoxGender;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxLastName;
        private System.Windows.Forms.Label labelUserLastName;
        private System.Windows.Forms.TextBox textBoxMiddleName;
        private System.Windows.Forms.Label labelUserMiddleName;
        private System.Windows.Forms.TextBox textBoxFirstName;
        private System.Windows.Forms.Label labelUserFirstName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.DataGridView dataGridViewList;
        private System.Windows.Forms.Panel panel34;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Panel panel35;
        private System.Windows.Forms.Button buttonEdit;
        private System.Windows.Forms.Button buttonDuplicate;
        private System.Windows.Forms.TextBox textBoxAgeResult;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.Button buttonRandom;
        private System.Windows.Forms.Panel panel37;
        private System.Windows.Forms.Panel panel36;
        private System.Windows.Forms.Label labelListN;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelResultN;
        private System.Windows.Forms.CheckBox checkBoxShowSearchResult;
        private System.Windows.Forms.CheckBox checkBoxShowDeactivated;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxClientLabel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox checkBoxActive;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.Label LabelPhysicianNr;
        private System.Windows.Forms.Button buttonSearch;
        private System.Windows.Forms.Button buttonDup;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Label labelError;
        private System.Windows.Forms.Button buttonDisable;
        private System.Windows.Forms.Panel panel38;
        private System.Windows.Forms.TextBox textBoxInstruction;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel39;
        private System.Windows.Forms.Panel panel42;
        private System.Windows.Forms.Panel panel41;
        private System.Windows.Forms.Panel panel40;
        private System.Windows.Forms.Panel panel43;
        private System.Windows.Forms.Label labelSelected;
        private System.Windows.Forms.Button buttonSelect;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnIndexNr;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnActive;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1ClientName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnFullName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6ClientCity;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7ClientCountry;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3ClientZip;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4ClientHouseNr;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2ClientStreet;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5ClientState;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnPhone1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnPhone2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnCell;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnSkype;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11ClientEmail;
    }
}