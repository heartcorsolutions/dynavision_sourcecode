﻿using Event_Base;
using EventBoard.EntryForms;
using EventBoard.Event_Base;
using Program_Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TzDevice
{
    public partial class TZmedicalDevSet : Form
    {
        public CTzDeviceSettings _mSettings = null;
        public string _mSerialNr;
        private string _mOrgSnr;
        private string _mSnrRem;
        public string _mTzRoot;
        public string _mTzDevicePath;
        private CDeviceInfo _mDeviceInfo = null;
        bool _mbIsTzClarus = false;

        CTzAutoCollect _mCollectScp = null;
        CTzAutoCollect _mCollectTzr = null;


        /*        public const string _cSettingNumberExt = "settingNumber";
                public const string _cActionNumberExt = "actionNumber";
                public const string _cSettingFileExt = "tzs";
                public const string _cActionFileExt = "tza";
                public const string _cSettingSdcardName = "config";
                public const string _csToServerName = "ToServer";
        */
        public TZmedicalDevSet(string ASerialNumber, CDeviceInfo ADeviceInfo)
        {
            try
            {
                bool bProgrammer = CLicKeyDev.sbDeviceIsProgrammer();
                _mOrgSnr = ASerialNumber;
                _mSerialNr = CDvtmsData.sGetTzStoreSnrName(ASerialNumber);
                _mDeviceInfo = ADeviceInfo;

                _mSnrRem = "";
                if (_mSerialNr != _mOrgSnr)
                {
                    _mSnrRem = _mOrgSnr + "->";
                }

                _mbIsTzClarus = CDvtmsData.sbIsTzClarusSnrName(ASerialNumber);

                if (_mSerialNr != null && _mSerialNr.Length > 0 && CTzDeviceSettings.sbGetTzAeraDevicePath(out _mTzRoot))
                {
                    if (CTzDeviceSettings.sbGetTzAeraDeviceUnitPath(out _mTzDevicePath, _mSerialNr))
                    {
                        _mSettings = new CTzDeviceSettings();
                        string deviceModel = " TZ Aera";

                        if (_mSettings != null)
                        {
                            InitializeComponent();

                            _mSettings.mSetDefaults(_mSerialNr);
                            mSetResultText(true, "Defaults set");

                            bool bDevice = false;
                            bool bOk = mbLoadDeviceSettings(out bDevice);

                            mSetResultText(bOk, bDevice ? "last settings for " + _mSerialNr + " loaded" : "default settings loaded");
                            mFillForm();
                            if(_mbIsTzClarus )
                            {
                                deviceModel = " TZ Clarus";
                                mDisableClarusItems();
                            }
                            mUpdateFileTime();
                            Text = CProgram.sMakeProgTitle("TZ Setup for " + _mSnrRem + _mSerialNr + deviceModel, false, true);
                        }
                    }
                    else
                    {
                        CProgram.sPromptError(true, "TZDevice Form", "No TZ device directory for " + _mSnrRem + _mSerialNr);
                    }
                    labelMessageInfo.Visible = bProgrammer;
                    checkBoxSaveToAllDevices.Visible = bProgrammer;
                    if (CTzDeviceSettings._sbUseQueue)
                    {
                        labelActionQueue.Text = "Only one Setting + queue of Actions is stored on the server.";
                    }
                    else
                    {
                        labelInfoQueue.Text = "";
                    }
//                    panelAutoCollect.Visible = true; // bProgrammer;
                    //if (bProgrammer)
                    {
                        mbLoadCollectScp();
                        mbLoadCollectTzr();

                        bool bAutoCollect = _mCollectScp != null && _mCollectScp._mbEnabled
                            || _mCollectTzr != null && _mCollectTzr._mbEnabled;

                        checkBoxAutoCollect.Checked = bAutoCollect;
                        panelAutoCollect.Visible = bAutoCollect;
                        mbFillCollectScp();
                        mbFillCollectTzr();
                        mbFillStudyActive();
                    }
                }
                else
                {
                    Close();
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Init TzDevice Form " + _mSnrRem + _mSerialNr, ex);
            }
        }
        public void mSetResultText(bool AbOk, string AText)
        {
            string s = "";
            if (AText != null && AText != "")
            {
                s = AbOk ? "Ok: " : "Failed: ";
            }
            labelResult.Text = s + AText;
            labelResult.BackColor = AbOk ? Color.Green : Color.Red;
        }

        private void mFillBlockSettings()
        {
            UInt32 blockLength = _mSettings.mGetSettingUInt32(DTzSettingTags.SCP_LENGTH);

            textBoxEventPreTime.Text = (blockLength * _mSettings.mGetSettingUInt32(DTzSettingTags.REPORT_ONSET_PRE_TIME)).ToString();
            textBoxEventPostTime.Text = (blockLength * _mSettings.mGetSettingUInt32(DTzSettingTags.REPORT_ONSET_POST_TIME)).ToString();

            UInt32 studyLength = _mSettings.mGetSettingUInt32(DTzSettingTags.STUDY_LENGTH);
            textBoxStudyLength.Text = studyLength.ToString();

            UInt32 sec = studyLength * blockLength;
            double days = sec / 3600.0 / 24.0;
            string s = "*" + blockLength.ToString() + "sec= " + days.ToString("0.0") + " days"; ;

            if(_mbIsTzClarus && blockLength != 30)
            {
                s += ", Check scp block size!";
            }
            LabelStudyDays.Text = s;
          
            labelServerName.Text = _mSettings.mGetSettingString(DTzSettingTags.FTP_ADDRESS);
        }

        private void mDisableClarusItems()
        {
            bool b = false;
            checkBoxNewStudy.Enabled = b;

            textBoxBradyOffset.Enabled = b;
            textBoxTachyOffset.Enabled = b;
            textBoxTachyDuration.Enabled = b;
            textBoxAFIBduration.Enabled = b;
            checkBoxReportBradyOnset.Enabled = b;
            checkBoxReportTachyOnset.Enabled = b;
            checkBoxReportAFIBOnset.Enabled = b;
            checkBoxReportPause.Enabled = b;
            checkBoxReportBradyOffset.Enabled = b;
            checkBoxReportTachyOffset.Enabled = b;
            checkBoxReportPatEvent.Enabled = b;
            checkBoxReportAFIBOffset.Enabled = b;
            checkBoxReportPaceDetected.Enabled = b;
            checkBoxReportLeadDisconnected.Enabled = b;
            checkBoxReportStart.Enabled = b;
            checkBoxReportStop.Enabled = b;
            checkBoxReportBreak.Enabled = b;
            checkBoxReportResume.Enabled = b;
            checkBoxReportFull.Enabled = b;
            checkBoxReportBattLow.Enabled = b;
            checkBoxReportChargingStart.Enabled = b;
            checkBoxReportChargingStop.Enabled = b;
            comboBoxAnalysisMask.Enabled = b;
            labelCrossChannelAnalysis.Enabled = b;
            comboBoxCrossCh.Enabled = b;
            comboBoxLeadConfig.Enabled = b;
            comboBoxTZSampleRate.Enabled = b;
            checkBoxAutoAnswer.Enabled = b;
            checkBoxSilentMode.Enabled = b;
            checkBoxVibrOnClick.Enabled = b;
            comboBoxLowFilter.Enabled = b;
            checkBoxSyncTime.Enabled = b;
            checkBoxSMSenable.Enabled = b;
            checkBoxVoiceEnable.Enabled = b;
            textBoxStreamingDelay.Enabled = b;   
        }
        private void mFillForm()
        {
            labelDeviceSnr.Text = _mSerialNr;
            _mSettings.serialNumber = _mSerialNr;

            labelEstDelay.Text = (_mSettings.mGetSettingUInt32(DTzSettingTags.CHECK_ACTIONS_INTERVAL) * 2).ToString();

            // Patient
            textBoxPatientID.Text = _mSettings.mGetSettingString(DTzSettingTags.PATIENT_ID);

            // ECG Trigger settings
            textBoxBradyOnset.Text = _mSettings.mGetSettingUInt32(DTzSettingTags.BRADY_ONSET_BPM).ToString();
            textBoxBradyOffset.Text = _mSettings.mGetSettingUInt32(DTzSettingTags.BRADY_OFFSET_BPM).ToString();
            textBoxBradyDuration.Text = _mSettings.mGetSettingUInt32(DTzSettingTags.BRADY_DURATION).ToString();
            textBoxTachyOnset.Text = _mSettings.mGetSettingUInt32(DTzSettingTags.TACHY_ONSET_BPM).ToString();
            textBoxTachyOffset.Text = _mSettings.mGetSettingUInt32(DTzSettingTags.TACHY_OFFSET_BPM).ToString();
            textBoxTachyDuration.Text = _mSettings.mGetSettingUInt32(DTzSettingTags.TACHY_DURATION).ToString();
            //            textBoxAFIBonset.Text = _mSettings.mGetSettingUInt32(DTzSettingTags.AF).ToString();
            //            textBoxAFIBoffset.Text = _mSettings.mGetSettingUInt32(DTzSettingTags.).ToString();
            textBoxAFIBduration.Text = _mSettings.mGetSettingUInt32(DTzSettingTags.AF_DURATION).ToString();
            //            textBoxPauseOnset.Text = _mSettings.mGetSettingUInt32(DTzSettingTags.).ToString();
            //            textBoxPauseOffset.Text = _mSettings.mGetSettingUInt32(DTzSettingTags.).ToString();
            textBoxPauseDuration.Text = _mSettings.mGetSettingUInt32(DTzSettingTags.PAUSE_DURATION).ToString();


            bool bDetPM = _mSettings.mGetSettingBool(DTzSettingTags.DETECT_PACEMAKER);
            radioButtonPacemakerYes.Checked = bDetPM;
            radioButtonPacemakerNo.Checked = false == bDetPM;

            checkBoxReportBradyOnset.Checked = _mSettings.mGetSettingBool(DTzSettingTags.REPORT_BRADY_ONSET);
            checkBoxReportTachyOnset.Checked = _mSettings.mGetSettingBool(DTzSettingTags.REPORT_TACHY_ONSET);
            checkBoxReportAFIBOnset.Checked = _mSettings.mGetSettingBool(DTzSettingTags.REPORT_AF_ONSET);
            checkBoxReportPause.Checked = _mSettings.mGetSettingBool(DTzSettingTags.REPORT_PAUSE_ONSET);
            checkBoxReportBradyOffset.Checked = _mSettings.mGetSettingBool(DTzSettingTags.REPORT_BRADY_OFFSET);
            checkBoxReportTachyOffset.Checked = _mSettings.mGetSettingBool(DTzSettingTags.REPORT_TACHY_OFFSET);
            checkBoxTransmitIntervalReports.Checked = _mSettings.mGetSettingBool(DTzSettingTags.TRANSMIT_INTERVAL_REPORTS);
            checkBoxReportAFIBOffset.Checked = _mSettings.mGetSettingBool(DTzSettingTags.REPORT_AF_OFFSET);
            checkBoxReportPaceDetected.Checked = _mSettings.mGetSettingBool(DTzSettingTags.REPORT_PACE_DETECT);
            checkBoxReportLeadDisconnected.Checked = _mSettings.mGetSettingBool(DTzSettingTags.REPORT_LEAD_DISCONNECTED);
            checkBoxReportStart.Checked = _mSettings.mGetSettingBool(DTzSettingTags.REPORT_START);
            checkBoxReportStop.Checked = _mSettings.mGetSettingBool(DTzSettingTags.REPORT_STOP);
            checkBoxReportBreak.Checked = _mSettings.mGetSettingBool(DTzSettingTags.REPORT_BREAK);
            checkBoxReportResume.Checked = _mSettings.mGetSettingBool(DTzSettingTags.REPORT_RESUME);
            checkBoxReportFull.Checked = _mSettings.mGetSettingBool(DTzSettingTags.REPORT_FULL);
            checkBoxReportBattLow.Checked = _mSettings.mGetSettingBool(DTzSettingTags.REPORT_BATTERY_LOW);
            checkBoxReportChargingStart.Checked = _mSettings.mGetSettingBool(DTzSettingTags.REPORT_CHARGING_STARTED);
            checkBoxReportChargingStop.Checked = _mSettings.mGetSettingBool(DTzSettingTags.REPORT_CHARGING_STOPPED);
            checkBoxReportPatEvent.Checked = _mSettings.mGetSettingBool(DTzSettingTags.REPORT_PATIENT_EVENT);
            comboBoxAnalysisMask.Text = _mSettings.mGetSettingUInt32(DTzSettingTags.ANALYSIS_CHANNEL_MASK).ToString();
            comboBoxCrossCh.Text = _mSettings.mGetSettingUInt32(DTzSettingTags.CROSS_CHANNEL_ANALYSIS).ToString();

            // Technical settings
            comboBoxTZSampleRate.Text = _mSettings.mGetSettingUInt32(DTzSettingTags.SAMPLE_RATE).ToString();
            comboBoxDigHighFilter.Text = (_mSettings.mGetSettingUInt32(DTzSettingTags.DIGITAL_HP_FILTER) / 100.0).ToString("0.00");
            comboBoxNotchFilter.Text = _mSettings.mGetSettingUInt32(DTzSettingTags.DIGITAL_NOTCH_FILTER).ToString();
            comboBoxLowFilter.Text = _mSettings.mGetSettingUInt32(DTzSettingTags.DIGITAL_LP_FILTER).ToString();
            comboBoxFirstLow.Text = _mSettings.mGetSettingUInt32(DTzSettingTags.LP_FILTER).ToString();

            comboBoxLeadConfig.SelectedIndex = (int)_mSettings.mGetSettingUInt32(DTzSettingTags.LEAD_CONFIG);
            //comboBoxLeadConfig.Text = _mSettings.mGetSettingUInt32(DTzSettingTags.LEAD_CONFIG).ToString();
            //            textBoxStudyHours.Text = _mSettings.mGetSettingUInt32(DTzSettingTags.STUDY_HOURS).ToString();

            checkBoxAutoAnswer.Checked = _mSettings.mGetSettingBool(DTzSettingTags.AUTO_ANSWER);
            checkBoxSyncTime.Checked = _mSettings.mGetSettingBool(DTzSettingTags.SYNC_TIME);
            checkBoxSilentMode.Checked = _mSettings.mGetSettingBool(DTzSettingTags.SILENT_MODE);
            checkBoxVibrOnClick.Checked = _mSettings.mGetSettingBool(DTzSettingTags.VIB_CLICKS);
            checkBoxSMSenable.Checked = _mSettings.mGetSettingBool(DTzSettingTags.SMS_ENABLE);
            checkBoxVoiceEnable.Checked = _mSettings.mGetSettingBool(DTzSettingTags.VOICE_ENABLE);

            textBoxStreamingMode.Text = _mSettings.mGetSettingUInt32(DTzSettingTags.STREAMING_MODE).ToString();
            textBoxStreamingDelay.Text = _mSettings.mGetSettingUInt32(DTzSettingTags.MAX_STREAMING_DELAY).ToString();

            checkBoxSymptDiaryOn.Checked = _mSettings.mGetSettingUInt32(DTzSettingTags.SYMPTOM_DIARY_COUNT) > 0;
            checkBoxActDiaryOn.Checked = _mSettings.mGetSettingUInt32(DTzSettingTags.ACTIVITY_DIARY_COUNT) > 0;

            //            .Checked = _mSettings.mGetSettingBool(DTzSettingTags.);
            //            .Text = _mSettings.mGetSettingUInt32(DTzSettingTags.).ToString();

            textBoxSymp1.Text = _mSettings.mGetSettingString(DTzSettingTags.DIARY_SYMPTOM_1);
            textBoxAct1.Text = _mSettings.mGetSettingString(DTzSettingTags.DIARY_ACTIVITY_1);
            textBoxSymp2.Text = _mSettings.mGetSettingString(DTzSettingTags.DIARY_SYMPTOM_2);
            textBoxAct2.Text = _mSettings.mGetSettingString(DTzSettingTags.DIARY_ACTIVITY_2);
            textBoxSymp3.Text = _mSettings.mGetSettingString(DTzSettingTags.DIARY_SYMPTOM_3);
            textBoxAct3.Text = _mSettings.mGetSettingString(DTzSettingTags.DIARY_ACTIVITY_3);
            textBoxSymp4.Text = _mSettings.mGetSettingString(DTzSettingTags.DIARY_SYMPTOM_4);
            textBoxAct4.Text = _mSettings.mGetSettingString(DTzSettingTags.DIARY_ACTIVITY_4);
            textBoxSymp5.Text = _mSettings.mGetSettingString(DTzSettingTags.DIARY_SYMPTOM_5);
            textBoxAct5.Text = _mSettings.mGetSettingString(DTzSettingTags.DIARY_ACTIVITY_5);
            textBoxSymp6.Text = _mSettings.mGetSettingString(DTzSettingTags.DIARY_SYMPTOM_6);
            textBoxAct6.Text = _mSettings.mGetSettingString(DTzSettingTags.DIARY_ACTIVITY_6);
            textBoxSymp7.Text = _mSettings.mGetSettingString(DTzSettingTags.DIARY_SYMPTOM_7);
            textBoxAct7.Text = _mSettings.mGetSettingString(DTzSettingTags.DIARY_ACTIVITY_7);
            textBoxSymp8.Text = _mSettings.mGetSettingString(DTzSettingTags.DIARY_SYMPTOM_8);
            textBoxAct8.Text = _mSettings.mGetSettingString(DTzSettingTags.DIARY_ACTIVITY_8);
            textBoxSymp9.Text = _mSettings.mGetSettingString(DTzSettingTags.DIARY_SYMPTOM_9);
            textBoxAct9.Text = _mSettings.mGetSettingString(DTzSettingTags.DIARY_ACTIVITY_9);
            textBoxSymp10.Text = _mSettings.mGetSettingString(DTzSettingTags.DIARY_SYMPTOM_10);
            textBoxAct10.Text = _mSettings.mGetSettingString(DTzSettingTags.DIARY_ACTIVITY_10);


            mFillBlockSettings();
        }

        private void mReadUInt32(ref string AErrStr, string AText, DTzSettingTags ATag, string Label)
        {
            UInt32 u = 0;

            if (false == UInt32.TryParse(AText, out u)
                || false == _mSettings.mbSetSettingUInt32(ATag, u))
            {
                AErrStr += Label + ", ";
            }
        }
        private void mReadUInt32Block(ref string AErrStr, string AText, DTzSettingTags ATag1,/* DTzSettingTags ATag2,*/
            string Label, UInt32 ABlockLengthSec)
        {
            UInt32 u = 0;
            bool bOk = false;


            if (UInt32.TryParse(AText, out u))
            {
                UInt32 block = ABlockLengthSec == 0 ? 1 : ABlockLengthSec;
                u = (u + block / 2) / block;

                bOk = _mSettings.mbSetSettingUInt32(ATag1, u);
                    //&& _mSettings.mbSetSettingUInt32(ATag2, u);
            }
            if (false == bOk)
            {
                AErrStr += Label + ", ";
            }
        }
        private bool mbReadForm()
        {
            bool bOk = false;
            string err = "";

            try
            {
                string s;
                UInt16 countSymptom = 0;
                UInt16 countActivity = 0;
                UInt16 u;
                UInt32 blockLength = _mSettings.mGetSettingUInt32(DTzSettingTags.SCP_LENGTH);

                // Patient
                _mSettings.mbSetSettingString(DTzSettingTags.PATIENT_ID, textBoxPatientID.Text);

                // ECG Trigger settings

                mReadUInt32(ref err, textBoxBradyOnset.Text, DTzSettingTags.BRADY_ONSET_BPM, "BradyOn");

                mReadUInt32(ref err, textBoxBradyOffset.Text, DTzSettingTags.BRADY_OFFSET_BPM, "Brady Off");
                mReadUInt32(ref err, textBoxBradyDuration.Text, DTzSettingTags.BRADY_DURATION, "Brady Duration");
                mReadUInt32(ref err, textBoxTachyOnset.Text, DTzSettingTags.TACHY_ONSET_BPM, "Tachy On");
                mReadUInt32(ref err, textBoxTachyOffset.Text, DTzSettingTags.TACHY_OFFSET_BPM, "Tachy Off");
                if (_mbIsTzClarus)
                {
                    // use sane value as brady
                    mReadUInt32(ref err, textBoxBradyDuration.Text, DTzSettingTags.TACHY_DURATION, "Tachy Duration");
                }
                else
                {
                    mReadUInt32(ref err, textBoxTachyDuration.Text, DTzSettingTags.TACHY_DURATION, "Tachy Duration");
                }
                //            textBoxAFIBonset.Text = _mSettings.mGetSettingUInt32(DTzSettingTags.AF, "");
                //            textBoxAFIBoffset.Text = _mSettings.mGetSettingUInt32(DTzSettingTags., "");
                mReadUInt32(ref err, textBoxAFIBduration.Text, DTzSettingTags.AF_DURATION, "Afib Duration");
                //            textBoxPauseOnset.Text = _mSettings.mGetSettingUInt32(DTzSettingTags., "");
                //            textBoxPauseOffset.Text = _mSettings.mGetSettingUInt32(DTzSettingTags., "");
                mReadUInt32(ref err, textBoxPauseDuration.Text, DTzSettingTags.PAUSE_DURATION, "Pause Duration");
                mReadUInt32Block(ref err, textBoxEventPreTime.Text, DTzSettingTags.REPORT_ONSET_PRE_TIME,
                    /*DTzSettingTags.REPORT_OFFSET_PRE_TIME, do not use*/ "Event Pre Time", blockLength);
                mReadUInt32Block(ref err, textBoxEventPostTime.Text, DTzSettingTags.REPORT_ONSET_POST_TIME,
                    /*DTzSettingTags.REPORT_OFFSET_POST_TIME,*/ "Event Post Time", blockLength);

                _mSettings.mbSetSettingBool(DTzSettingTags.DETECT_PACEMAKER, radioButtonPacemakerYes.Checked);

                _mSettings.mbSetSettingBool(DTzSettingTags.REPORT_BRADY_ONSET, checkBoxReportBradyOnset.Checked);
                _mSettings.mbSetSettingBool(DTzSettingTags.REPORT_TACHY_ONSET, checkBoxReportTachyOnset.Checked);
                _mSettings.mbSetSettingBool(DTzSettingTags.REPORT_AF_ONSET, checkBoxReportAFIBOnset.Checked);
                _mSettings.mbSetSettingBool(DTzSettingTags.REPORT_PAUSE_ONSET, checkBoxReportPause.Checked);
                _mSettings.mbSetSettingBool(DTzSettingTags.REPORT_BRADY_OFFSET, checkBoxReportBradyOffset.Checked);
                _mSettings.mbSetSettingBool(DTzSettingTags.REPORT_TACHY_OFFSET, checkBoxReportTachyOffset.Checked);
                _mSettings.mbSetSettingBool(DTzSettingTags.REPORT_AF_OFFSET, checkBoxReportAFIBOffset.Checked);
                _mSettings.mbSetSettingBool(DTzSettingTags.TRANSMIT_INTERVAL_REPORTS, checkBoxTransmitIntervalReports.Checked);
                _mSettings.mbSetSettingBool(DTzSettingTags.REPORT_PACE_DETECT, checkBoxReportPaceDetected.Checked);
                _mSettings.mbSetSettingBool(DTzSettingTags.REPORT_LEAD_DISCONNECTED, checkBoxReportLeadDisconnected.Checked);
                _mSettings.mbSetSettingBool(DTzSettingTags.REPORT_START, checkBoxReportStart.Checked);
                _mSettings.mbSetSettingBool(DTzSettingTags.REPORT_STOP, checkBoxReportStop.Checked);
                _mSettings.mbSetSettingBool(DTzSettingTags.REPORT_BREAK, checkBoxReportBreak.Checked);
                _mSettings.mbSetSettingBool(DTzSettingTags.REPORT_RESUME, checkBoxReportResume.Checked);
                _mSettings.mbSetSettingBool(DTzSettingTags.REPORT_FULL, checkBoxReportFull.Checked);
                _mSettings.mbSetSettingBool(DTzSettingTags.REPORT_BATTERY_LOW, checkBoxReportBattLow.Checked);
                _mSettings.mbSetSettingBool(DTzSettingTags.REPORT_CHARGING_STARTED, checkBoxReportChargingStart.Checked);
                _mSettings.mbSetSettingBool(DTzSettingTags.REPORT_CHARGING_STOPPED, checkBoxReportChargingStop.Checked);
                _mSettings.mbSetSettingBool(DTzSettingTags.REPORT_PATIENT_EVENT, checkBoxReportPatEvent.Checked);
                mReadUInt32(ref err, comboBoxAnalysisMask.Text, DTzSettingTags.ANALYSIS_CHANNEL_MASK, "");
                mReadUInt32(ref err, comboBoxCrossCh.Text, DTzSettingTags.CROSS_CHANNEL_ANALYSIS, "");

                // Technical settings
                if (_mbIsTzClarus)
                {
                    _mSettings.mbSetSettingUInt32(DTzSettingTags.SAMPLE_RATE, 250);
                }
                else
                {
                    mReadUInt32(ref err, comboBoxTZSampleRate.Text, DTzSettingTags.SAMPLE_RATE, "SampleRate");
                }
                float hp = 0;
                if (CProgram.sbParseFloat(comboBoxDigHighFilter.Text, ref hp))
                {
                    _mSettings.mbSetSettingUInt32(DTzSettingTags.DIGITAL_HP_FILTER, (UInt32)(hp * 100 + 0.5));
                }
                mReadUInt32(ref err, comboBoxNotchFilter.Text, DTzSettingTags.DIGITAL_NOTCH_FILTER, "D-NF");
                mReadUInt32(ref err, comboBoxLowFilter.Text, DTzSettingTags.DIGITAL_LP_FILTER, "D-LP");
                mReadUInt32(ref err, comboBoxFirstLow.Text, DTzSettingTags.LP_FILTER, "LP");

                int index = comboBoxLeadConfig.SelectedIndex;
                if( _mbIsTzClarus)
                {
                    index = 0;  // not used so put on custom
                    _mSettings.mbSetSettingUInt32(DTzSettingTags.SCP_LENGTH, 30);   // make sure that stripsize is 30 for the clarus
                }
                if (index >= 0 && index <= 5)
                {
                    _mSettings.mbSetSettingUInt32(DTzSettingTags.LEAD_CONFIG, (UInt32)index);
                }
                //mReadUInt32(ref err, comboBoxLeadConfig.Text, DTzSettingTags.LEAD_CONFIG, "Lead");

                //                mReadUInt32(ref err, textBoxStudyHours.Text, DTzSettingTags.STUDY_HOURS, "Study Hours");
                mReadUInt32(ref err, textBoxStudyLength.Text, DTzSettingTags.STUDY_LENGTH, "Study Files");

                _mSettings.mbSetSettingBool(DTzSettingTags.AUTO_ANSWER, checkBoxAutoAnswer.Checked);
                _mSettings.mbSetSettingBool(DTzSettingTags.SYNC_TIME, checkBoxSyncTime.Checked);
                _mSettings.mbSetSettingBool(DTzSettingTags.SILENT_MODE, checkBoxSilentMode.Checked);
                _mSettings.mbSetSettingBool(DTzSettingTags.VIB_CLICKS, checkBoxVibrOnClick.Checked);
                _mSettings.mbSetSettingBool(DTzSettingTags.SMS_ENABLE, checkBoxSMSenable.Checked);
                _mSettings.mbSetSettingBool(DTzSettingTags.VOICE_ENABLE, checkBoxVoiceEnable.Checked);

                mReadUInt32(ref err, textBoxStreamingMode.Text, DTzSettingTags.STREAMING_MODE, "Smod");
                mReadUInt32(ref err, textBoxStreamingDelay.Text, DTzSettingTags.MAX_STREAMING_DELAY, "Sdly");

                //            .Checked = _mSettings.mGetSettingBool(DTzSettingTags.);
                //            .Text = _mSettings.mGetSettingUInt32(DTzSettingTags., "");

                s = textBoxSymp1.Text; _mSettings.mbSetSettingString(DTzSettingTags.DIARY_SYMPTOM_1, s); if (s.Length > 0) countSymptom = 1;
                s = textBoxAct1.Text; _mSettings.mbSetSettingString(DTzSettingTags.DIARY_ACTIVITY_1, s); if (s.Length > 0) countActivity = 1;
                s = textBoxSymp2.Text; _mSettings.mbSetSettingString(DTzSettingTags.DIARY_SYMPTOM_2, s); if (s.Length > 0) countSymptom = 2;
                s = textBoxAct2.Text; _mSettings.mbSetSettingString(DTzSettingTags.DIARY_ACTIVITY_2, s); if (s.Length > 0) countActivity = 2;
                s = textBoxSymp3.Text; _mSettings.mbSetSettingString(DTzSettingTags.DIARY_SYMPTOM_3, s); if (s.Length > 0) countSymptom = 3;
                s = textBoxAct3.Text; _mSettings.mbSetSettingString(DTzSettingTags.DIARY_ACTIVITY_3, s); if (s.Length > 0) countActivity = 3;
                s = textBoxSymp4.Text; _mSettings.mbSetSettingString(DTzSettingTags.DIARY_SYMPTOM_4, s); if (s.Length > 0) countSymptom = 4;
                s = textBoxAct4.Text; _mSettings.mbSetSettingString(DTzSettingTags.DIARY_ACTIVITY_4, s); if (s.Length > 0) countActivity = 4;
                s = textBoxSymp5.Text; _mSettings.mbSetSettingString(DTzSettingTags.DIARY_SYMPTOM_5, s); if (s.Length > 0) countSymptom = 5;
                s = textBoxAct5.Text; _mSettings.mbSetSettingString(DTzSettingTags.DIARY_ACTIVITY_5, s); if (s.Length > 0) countActivity = 5;
                s = textBoxSymp6.Text; _mSettings.mbSetSettingString(DTzSettingTags.DIARY_SYMPTOM_6, s); if (s.Length > 0) countSymptom = 6;
                s = textBoxAct6.Text; _mSettings.mbSetSettingString(DTzSettingTags.DIARY_ACTIVITY_6, s); if (s.Length > 0) countActivity = 6;
                s = textBoxSymp7.Text; _mSettings.mbSetSettingString(DTzSettingTags.DIARY_SYMPTOM_7, s); if (s.Length > 0) countSymptom = 7;
                s = textBoxAct7.Text; _mSettings.mbSetSettingString(DTzSettingTags.DIARY_ACTIVITY_7, s); if (s.Length > 0) countActivity = 7;
                s = textBoxSymp8.Text; _mSettings.mbSetSettingString(DTzSettingTags.DIARY_SYMPTOM_8, s); if (s.Length > 0) countSymptom = 8;
                s = textBoxAct8.Text; _mSettings.mbSetSettingString(DTzSettingTags.DIARY_ACTIVITY_8, s); if (s.Length > 0) countActivity = 8;
                s = textBoxSymp9.Text; _mSettings.mbSetSettingString(DTzSettingTags.DIARY_SYMPTOM_9, s); if (s.Length > 0) countSymptom = 9;
                s = textBoxAct9.Text; _mSettings.mbSetSettingString(DTzSettingTags.DIARY_ACTIVITY_9, s); if (s.Length > 0) countActivity = 9;
                s = textBoxSymp10.Text; _mSettings.mbSetSettingString(DTzSettingTags.DIARY_SYMPTOM_10, s); if (s.Length > 0) countSymptom = 10;
                s = textBoxAct10.Text; _mSettings.mbSetSettingString(DTzSettingTags.DIARY_ACTIVITY_10, s); if (s.Length > 0) countActivity = 10;

                _mSettings.mbSetSettingUInt32(DTzSettingTags.SYMPTOM_DIARY_COUNT, checkBoxSymptDiaryOn.Checked ? countSymptom : (UInt32)0);
                _mSettings.mbSetSettingUInt32(DTzSettingTags.ACTIVITY_DIARY_COUNT, checkBoxActDiaryOn.Checked ? countActivity : (UInt32)0);

                bOk = err.Length == 0;
                if (false == bOk)
                {
                    err = err.Substring(0, err.Length - 2); // remove last ', '
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Read TZ Settings form failed" + _mSnrRem + _mSerialNr, ex);
            }
            mSetResultText(bOk, err);
            return bOk;
        }
        public void mClearDoneFile(string ADevicePath, string ASerialNr, string AFileExt)
        {
            string fileName = Path.ChangeExtension(ASerialNr, AFileExt);

            try
            {
                string filePath = Path.Combine(ADevicePath, fileName);
                FileInfo fi = new FileInfo(filePath);

                if (fi != null && fi.Exists)
                {
                    fi.Delete();
                 }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Delete TZ done file failed " + _mSerialNr + " " + fileName, ex);
            }
        }

        public string mCheckDoneFile( string AText, string ADevicePath, string ASerialNr, string AFileExt)
        {
            string result = "";
            string fileName = Path.ChangeExtension(ASerialNr, AFileExt);

            try
            {
                string filePath = Path.Combine(ADevicePath, fileName);
                FileInfo fi = new FileInfo(filePath);

                if( fi != null && fi.Exists)
                {
                    string[] lines = File.ReadAllLines(filePath);
                    if( lines != null && lines.Length > 0)
                    {
                        result = AText + " " + lines[0].Trim() + " " + CProgram.sDateTimeToYMDHMS(fi.LastWriteTime);
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Read TZ done file failed "  + _mSerialNr + " " + fileName, ex);
            }
            return result;
        }

        private void mUpdateFileTime()
        {
            UInt32 settingIndex = 0;
            UInt32 actionIndex = 0;
            string settingStr = "";
            string actionStr = "";
            string LastMsgReceived = mCheckDoneFile("Msg", _mTzDevicePath, _mSerialNr, ".MsgReceived");
            string lastActionDone = mCheckDoneFile("Action", _mTzDevicePath, _mSerialNr, ".ActionDone");
            string lastSettingSet = mCheckDoneFile("Setting", _mTzDevicePath, _mSerialNr, ".SettingSet");
            string lastDone = lastActionDone;

            if( LastMsgReceived != null && LastMsgReceived.Length > 0)
            {
                if (lastDone != null && lastActionDone.Length > 0 )
                {
                    lastDone += ", ";
                }
                lastDone += LastMsgReceived;
            }
            if (lastSettingSet != null && lastSettingSet.Length > 0)
            {
                if (lastDone != null && lastActionDone.Length > 0)
                {
                    lastDone += ", ";
                }
                lastDone += lastSettingSet;
            }
            CTzDeviceSettings.sbCheckSettingsFile(out settingIndex, out settingStr, _mTzDevicePath, _mSerialNr);
            CTzDeviceSettings.sbCheckActionFile(out actionIndex, out actionStr, _mTzDevicePath, _mSerialNr);

            labelSettingFileID.Text = settingIndex.ToString();
            labelActionFileID.Text = actionIndex.ToString();
            labelSettingFileInfo.Text = settingStr;
            labelActionFileInfo.Text = actionStr;
            labelLastMsgAction.Text = lastDone;
        }
        public bool mbLoadDeviceSettings(out bool AbDeviceLoaded)
        {
            bool bOk = false;

            AbDeviceLoaded = false;
            string fileName = "";

            try
            {
                if (CTzDeviceSettings.sbMakeSettingFileName(out fileName, _mSerialNr))
                {
                    fileName = Path.Combine(_mTzDevicePath, fileName);

                    if (false == File.Exists(fileName))
                    {
                        AbDeviceLoaded = false;
                        CTzDeviceSettings.sbMakeSettingFileName(out fileName, CTzDeviceSettings._cSettingDefaultName);

                        fileName = Path.Combine(_mTzRoot, fileName);

                        if (false == File.Exists(fileName))
                        {
                            CTzDeviceSettings.sbMakeSettingFileName(out fileName, CTzDeviceSettings._cSettingSdcardName);

                            fileName = Path.Combine(_mTzRoot, fileName);
                        }
                    }
                    else
                    {
                        AbDeviceLoaded = true;
                    }
                    bOk = _mSettings.mbReadSettings(fileName, false, false, _mSerialNr);
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Read failed from " + fileName, ex);
                bOk = false;
            }
            _mSettings.serialNumber = _mSerialNr;

            return bOk;
        }
        public bool mbLoadDefaultSettings()
        {
            bool bOk = false;

            string fileName = "";

            try
            {
                CTzDeviceSettings.sbMakeSettingFileName(out fileName, CTzDeviceSettings._cSettingDefaultName);

                fileName = Path.Combine(_mTzRoot, fileName);

                if (false == File.Exists(fileName))
                {
                    CTzDeviceSettings.sbMakeSettingFileName(out fileName, CTzDeviceSettings._cSettingSdcardName);

                    fileName = Path.Combine(_mTzRoot, fileName);
                }
                bOk = _mSettings.mbReadSettings(fileName, false, false, _mSerialNr);
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Read failed from " + fileName, ex);
                bOk = false;
            }
            _mSettings.serialNumber = _mSerialNr;

            return bOk;
        }
        private void label31_Click(object sender, EventArgs e)
        {

        }

        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label42_Click(object sender, EventArgs e)
        {

        }

        private void panel11_Paint(object sender, PaintEventArgs e)
        {

        }

        private void labelServerSettings_Click(object sender, EventArgs e)
        {
            string pw = "";

            if (CProgram.sbReqLabel("TZ Config Server settings: " + _mSerialNr, "Password", ref pw, "", true))
            {
                if (pw == "tz4" + CProgram.sGetOrganisationLabel())
                {
                    TZserverSettings form = new TZserverSettings(_mSettings, _mSerialNr, _mbIsTzClarus);

                    if (form != null)
                    {
                        form.ShowDialog();
                        if (form.bChanged)
                        {
                            mSetResultText(true, "Server settings changed");

                            mFillBlockSettings();
                        }
                    }
                }
            }
        }

        private void checkBoxNewSetting_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void labelLoadCurrent_Click(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl && CLicKeyDev.sbDeviceIsProgrammer())
            {
                _mSettings.mClear(_mSerialNr);
                _mSettings.mSetDefaults(_mSerialNr);
                mSetResultText(true, "Program defaults set");
            }
            else
            {
                bool bDevice = false;
                bool bOk = mbLoadDeviceSettings(out bDevice);

                mSetResultText(bOk, bDevice ? "last settings for " + _mSerialNr + " loaded" : "default settings loaded");
            }
            _mSettings.serialNumber = _mSerialNr;
            mFillForm();
            mUpdateFileTime();
        }

        private void labelLoadSettings_Click(object sender, EventArgs e)
        {
            mLoadSettingsFrom();
        }

        private void mLoadSettingsFrom()
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;
            string fileName = "";

            if (bCtrl && CLicKeyDev.sbDeviceIsProgrammer())
            {
                openFileDialogTxt.InitialDirectory = _mTzRoot;

                if (DialogResult.OK == openFileDialogTxt.ShowDialog())
                {
                    fileName = openFileDialogTxt.FileName;

                    mSetResultText(_mSettings.mbReadSettingsTxt(fileName), "Read setting " + Path.GetFileName(fileName));
                }

            }
            else
            {
                openFileDialogTZS.InitialDirectory = _mTzRoot;

                if (DialogResult.OK == openFileDialogTZS.ShowDialog())
                {
                    fileName = openFileDialogTZS.FileName;

                    mSetResultText(_mSettings.mbReadSettings(fileName, false, false, _mSerialNr), "Read setting " + Path.GetFileName(fileName));
                }
            }
            _mSettings.serialNumber = _mSerialNr;
            mFillForm();
            mUpdateFileTime();
        }

        private void labelSaveAs_Click(object sender, EventArgs e)
        {
            mSaveSettingsAs();
        }

        private void mSaveSettingsAs()
        {
            if (mbReadForm())
            {

                string fileName = "";

                try
                {
                    if (CTzDeviceSettings.sbMakeSettingFileName(out fileName, _mSerialNr))
                    {
                        //                        fileName = Path.Combine(_mTzPath, fileName);

                        saveFileDialog.InitialDirectory = _mTzRoot;
                        saveFileDialog.FileName = fileName;

                        if (DialogResult.OK == saveFileDialog.ShowDialog())
                        {
                            fileName = saveFileDialog.FileName;

                            uint settingNumber = 0;
                            bool bResetFileNumber = checkBoxResetFileNumber.Checked;

                            if (bResetFileNumber)
                            {
                                CTzDeviceSettings.sbSetSettingSeqNr(0, _mTzDevicePath, _mSerialNr);
                            }
                            if (CTzDeviceSettings.sbIncSettingSeqNr(out settingNumber, _mTzDevicePath, _mSerialNr))
                            {
                                bool bCopyOk;
                                mSetResultText(_mSettings.mbWriteSettings(fileName, "", (UInt16)settingNumber, false, out bCopyOk),
                                    "Write general setting " + Path.GetFileName(fileName));
                            }
                            else
                            {
                                mSetResultText(false, "File index " + Path.GetFileName(fileName));
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("SaveAs failed from " + fileName, ex);
                }
            }
            mUpdateFileTime();
        }

        private void labelSaveToSdcard_Click(object sender, EventArgs e)
        {
            mSaveToSdCard();
        }
        private void mSaveToSdCard()
        {
            if (mbReadForm())
            {
                string fileName = "";

                try
                {
                    if (CTzDeviceSettings.sbMakeSettingSdFileName(out fileName, _mSerialNr))
                    {
                        string rootPath = Path.GetPathRoot(_mTzRoot);

                        saveFileDialog.InitialDirectory = rootPath;
                        saveFileDialog.FileName = fileName;

                        if (DialogResult.OK == saveFileDialog.ShowDialog())
                        {
                            fileName = saveFileDialog.FileName;

                            bool bExists = File.Exists(fileName);
                            bool bOk = true;
//                            uint settingNumber = 0;

                            bool bResetFileNumber = checkBoxResetFileNumber.Checked;
                            bool bStartStudy = checkBoxStartStudy.Checked;

                            if (bOk)
                            {
                                bool bOpenExplorer = false;
                                string toPath = Path.GetDirectoryName(fileName);
                                if (toPath.Length > 3 || toPath == "C:\\")
                                {
                                    bOk = CProgram.sbAskOkCancel("TZ save to sd card for device " + _mSerialNr, "Path to sd card (" + toPath + "), Continue?");
                                }
                                if (bOk)
                                {
                                    if (bExists)
                                    {
                                        // check and clean
                                      
                                        bOk = CProgram.sbReqBool("TZ save to sd card for device " + _mSerialNr, "Open Explorer, (Backup & Clean sd card), Continue to write settings file?", ref bOpenExplorer);
                                        if( bOk && bOpenExplorer)
                                        {
                                            ProcessStartInfo startInfo = new ProcessStartInfo();
                                            startInfo.FileName = @"explorer";
                                            startInfo.Arguments = toPath;
                                            Process.Start(startInfo);
                                        }
                                    }
                                    else
                                    {
                                        bOk = CProgram.sbAskOkCancel("TZ save to sd card for device " + _mSerialNr, "No old setting on sd card, Continue to write settings file?");
                                        // // new setting -> reset number
                                        checkBoxResetFileNumber.Checked = true;
                                        checkBoxStartStudy.Checked = true;
                                    }
                                }
                                if (bOk)
                                {
                                    if (false == bResetFileNumber || false == bStartStudy)
                                    {
                                        bool bDo = true;

                                        bOk = CProgram.sbReqBool("TZ save to sd card for device " + _mSerialNr, "Reset file number and Start Study", ref bDo);

                                        if (bOk && bDo)
                                        {
                                            checkBoxResetFileNumber.Checked = true;
                                            checkBoxStartStudy.Checked = true;
                                        }
                                    }
                                }
                                if (bOk)
                                {
                                    if (bResetFileNumber)
                                    {
                                        CTzDeviceSettings.sbSetSettingSeqNr(0, _mTzDevicePath, _mSerialNr);
                                    }
                                    //                bOk &= mbIncFileSerialNr(out actionNumber, _cActionNumberExt);
//                                    if (CTzDeviceSettings.sbIncSettingSeqNr(out settingNumber, _mTzDevicePath, _mSerialNr))
                                    {
                                        bool bCopyOk;
                                        bOk = _mSettings.mbWriteSettings(fileName, "", CTzDeviceSettings._cFileID_UNKNOWN, false, out bCopyOk);
                                        string result = /*settingNumber.ToString() + ": */ "Write config to sdcard " + Path.GetFileName(fileName);
                                        mSetResultText(bOk, result);

                                        mbSaveToServer(true, ref result, false);

                                        if( bOk && CProgram.sbAskOkCancel("TZ config.tzs saved to device " + _mSerialNr, 
                                                    "Open explorer to sd-card? (Wipe all files except config.tzs and Eject card!)"))
                                        {
                                            ProcessStartInfo startInfo = new ProcessStartInfo();
                                            startInfo.FileName = @"explorer";
                                            startInfo.Arguments = toPath;
                                            Process.Start(startInfo);
                                        }
                                    }
/*                                    else
                                    {
                                        mSetResultText(false, "File index " + Path.GetFileName(fileName));
                                        bOk = false;
                                    }
*/                                }
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("SaveAs failed from " + fileName, ex);
                }
            }
            mUpdateFileTime();
        }

        private void labelSaveToServer_Click(object sender, EventArgs e)
        {
            timerRefresh.Stop();

            string result = "";
            if (mbSaveToServer(true, ref result, false))
            {
                if (checkBoxSaveToAllDevices.Checked)
                {
                    if (mbSaveToAllDevices())
                    {
                        Close();
                    }
                }
            }
            timerRefresh.Start();
        }
        private bool mbSaveToAllDevices()
        {
            bool bClose = false;
            bool bDo = true;
            bool bOptions;
            string warnOptions = "";
            string options = "";

            if (checkBoxResetFileNumber.Checked) options += "+Reset file number";
            if (checkBoxStartStudy.Checked) options += "+Start Study";
            if (checkBoxEndStudy.Checked) options += "+End Study";
            if (checkBoxMessage.Checked) options += "+Message";
            if (checkBoxShutdown.Checked) options += "+Shutdown";
            if (checkBoxFormatSD.Checked) options += "+Format SD";
            if (checkBoxUpdateFirmware.Checked) options += "+Update Firmware";

            bOptions = options != null && options.Length > 0;

            if (bOptions)
            {
                warnOptions = " !Extra Options!";
                bDo = CProgram.sbAskOkCancel("TZ Save To Server To All" + warnOptions, options + ", Continue?");
            }
            if (bDo)
            {



                bClose = true;
            }
            return bClose;
        }
        private bool mbSaveToServer(bool AbReadForm, ref string AResultText, bool bSkipAsk)
        {
            string result = AResultText;
            bool bOk = false;

            if (AbReadForm == false || mbReadForm())
            {
                string fileName = "";
                if (result != null && result.Length > 0)
                {
                    result += "\n";
                }

                try
                {
                    if (CTzDeviceSettings.sbMakeSettingFileName(out fileName, _mSerialNr))
                    {
                        fileName = Path.Combine(_mTzDevicePath, fileName);
                        bOk = true;

                        uint settingNumber = 0;
                        uint actionNumber = 0;

                        bool bResetFileNumber = checkBoxResetFileNumber.Checked;

                        if (false == bResetFileNumber && false == File.Exists(fileName))
                        {
                            bResetFileNumber = true;
                        }

                        if (bResetFileNumber)
                        {
                            bool bDo = true;

                            CTzDeviceSettings.sbGetActionSeqNr(out actionNumber, _mTzDevicePath, _mSerialNr);
                            CTzDeviceSettings.sbGetSettingSeqNr(out settingNumber, _mTzDevicePath, _mSerialNr);

                            if (actionNumber > 1 || settingNumber > 1)
                            {
                                bOk = bSkipAsk || CProgram.sbReqBool("TZ save to server", "Reset file number and Start Study", ref bDo);

                                if (bOk)
                                {
                                    checkBoxResetFileNumber.Checked = bDo;
                                    checkBoxStartStudy.Checked = true;
                                }
                            }
                        }
                        if (bOk)
                        {
                            if (bResetFileNumber)
                            {
                                CTzDeviceSettings.sbSetSettingSeqNr(0, _mTzDevicePath, _mSerialNr);
                            }
                            if (CTzDeviceSettings.sbIncSettingSeqNr(out settingNumber, _mTzDevicePath, _mSerialNr))
                            {
                                bool bCopyOk;
                                bOk = _mSettings.mbWriteSettings(fileName, "", (UInt16)settingNumber, true, out bCopyOk);

                                if (bOk)
                                {
                                    result += "Writen setting " + Path.GetFileName(fileName);

                                    if (false == bCopyOk)
                                    {
                                        result += " failed copy to server";
                                    }
                                }
                                else
                                {
                                    result += "Failed write setting " + Path.GetFileName(fileName);
                                }
                            }
                            else
                            {
                                result = "File index " + Path.GetFileName(fileName);
                                bOk = false;
                            }
                            mSetResultText(bOk, result);
                        }

                        if (bOk)
                        {
                            string actionResult;
                            bool bNewSetting = checkBoxNewSetting.Checked;

                            if (false == bNewSetting)
                            {
                                checkBoxNewSetting.Checked = true;
                            }

                            bOk = mbSendAction(out actionResult, false);

                            result += ", Action: " + actionResult;

                            if (false == bNewSetting)
                            {
                                checkBoxNewSetting.Checked = false;
                            }
                            mSetResultText(bOk, result);
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("SaveToServer failed from " + fileName, ex);
                }
            }
            mUpdateFileTime();
            AResultText = result;
            return bOk;
        }

/*        private bool mbSaveToDevice(bool AbReadForm, ref string AResultText)
        {
            string result = AResultText;
            bool bOk = false;

            if (AbReadForm == false || mbReadForm())
            {
                string fileName = "";

                if (result != null && result.Length > 0)
                {
                    result += "\n";
                }

                try
                {
                    if (CTzDeviceSettings.sbMakeSettingFileName(out fileName, _mSerialNr))
                    {
                        fileName = Path.Combine(_mTzDevicePath, fileName);

                        uint settingNumber = 0;
                        uint actionNumber = 0;

                        bool bResetFileNumber = checkBoxResetFileNumber.Checked;

                        bOk = true;
                        if (bResetFileNumber)
                        {
                            bool bDo = true;

                            CTzDeviceSettings.sbGetActionSeqNr(out actionNumber, _mTzDevicePath, _mSerialNr);
                            CTzDeviceSettings.sbGetSettingSeqNr(out settingNumber, _mTzDevicePath, _mSerialNr);

                            if (actionNumber > 1 || settingNumber > 1)
                            {
                                bOk = CProgram.sbReqBool("TZ save to server", "Reset file number and Start Study", ref bDo);

                                if (bOk)
                                {
                                    checkBoxResetFileNumber.Checked = bDo;
                                    checkBoxStartStudy.Checked = true;
                                }
                            }
                        }
                        if (bOk)
                        {
                            if (bResetFileNumber)
                            {
                                CTzDeviceSettings.sbSetSettingSeqNr(0, _mTzDevicePath, _mSerialNr);
                            }
                            bool bCopyOk;
                            bOk = _mSettings.mbWriteSettings(fileName, _mSerialNr, CTzDeviceSettings._cFileID_, false, out bCopyOk);
                            if (bOk)
                            {
                                result += "Writen setting " + Path.GetFileName(fileName);
                            }
                            else
                            {
                                result = "failed write " + Path.GetFileName(fileName);
                                bOk = false;
                            }

                            mSetResultText(bOk, result);
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("SaveToDevice failed from " + fileName, ex);
                    result += "Error";
                    mSetResultText(false, result);
                }
            }
            mUpdateFileTime();
            AResultText = result;
            return bOk;
        }
*/

        private bool mbSaveDefaultAction(out string AResult)
        {
            string result = "";
            bool bOk = true;

            string message = "";
            string patID = textBoxPatientID.Text;
            UInt32 actionNumber = CTzDeviceSettings._cFileID_UNKNOWN; //0
            UInt32 settingNumber = CTzDeviceSettings._cFileID_UNKNOWN;//0

            bool bNewStudy = checkBoxNewStudy.Checked;
            bool bStartStudy = checkBoxStartStudy.Checked;
            bool bNewSetting = checkBoxNewSetting.Checked;
            bool bEndStudy = checkBoxEndStudy.Checked;
            bool bResetFileNumber = checkBoxResetFileNumber.Checked;
            bool bMsg = checkBoxMessage.Checked;


            if (bMsg)
            {
                message = textBoxMessage.Text.Trim();

                if (message == null || message == "")
                {
                    if (bNewStudy && bStartStudy) message = "New Start Study " + patID;
                    else if (bNewStudy) message = "New Study " + patID;
                    else if (bEndStudy) message = "End Study " + patID;

                    if (message == null || message == "")
                    {

                    }
                    else
                    {
                        message = "A" + actionNumber.ToString() + "S" + settingNumber.ToString() + ": " + message;
                    }
                }
            }
            if (bOk)
            {
                string fileName = "";

                UInt32 index;
                Byte[] buffer = null;
                try
                {
                    if (CTzDeviceSettings.sbMakeActionFileName(out fileName, CTzDeviceSettings._cSettingDefaultName))
                    {
                        fileName = Path.Combine(_mTzRoot, fileName);

                        buffer = _mSettings.mActionCreateBuffer(out index, "", (UInt16)actionNumber);

                        if (buffer != null)
                        {
                            if (bNewStudy)
                            {
                                bOk &= _mSettings.mbActionEnableNewStudy(buffer, ref index);
                            }
                            if (bStartStudy)
                            {
                                bOk &= _mSettings.mbActionStartStudy(buffer, ref index);
                            }
                            if (bNewSetting)
                            {
                                bOk &= _mSettings.mbActionUpdateSettings(buffer, ref index);
                            }
                            if (bEndStudy)
                            {
                                bOk &= _mSettings.mbActionEndStudy(buffer, ref index);
                            }
                            if (bMsg)
                            {
                                bOk &= _mSettings.mbActionDisplayMessage(buffer, ref index, message);
                            }
                            if (false == bOk)
                            {
                                result = "create";
                            }
                            else
                            {
                                result = "writen";
                                bool bCopyOk;
                                bOk = _mSettings.mbWriteAction(fileName, buffer, ref index, false, out bCopyOk, (UInt16)actionNumber);

                                if (false == bOk)
                                {
                                    result = " failed write";
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Generate default Action", ex);
                }
            }
            if (message.Length > 0) result += " (" + message + ")";
            AResult = result;
            mUpdateFileTime();
            return bOk;
        }

        private bool mbSaveEmptyAction(out string AResult)
        {
            string result = "";
            bool bOk = true;

            UInt32 actionNumber = CTzDeviceSettings._cFileID_EMPTY; //0;

            string fileName = "";

            UInt32 index;
            Byte[] buffer = null;
            try
            {
                if (CTzDeviceSettings.sbMakeActionFileName(out fileName, "empty"))
                {
                    fileName = Path.Combine(_mTzRoot, fileName);

                    buffer = _mSettings.mActionCreateBuffer(out index, "", (UInt16)actionNumber);

                    if (buffer != null)
                    {
                        result = "writen";
                        bool bCopyOk;
                        bOk = _mSettings.mbWriteAction(fileName, buffer, ref index, false, out bCopyOk, (UInt16)actionNumber);

                        if (false == bOk)
                        {
                            result = " failed write";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Generate empty Action", ex);
            }
            AResult = result;
            mUpdateFileTime();
            return bOk;
        }


        private void mSaveToDefault()
        {
            string fileName = "";
            string result = "";
            bool bOk = false;

            try
            {
                //                if (mbSaveToDevice(true, ref result))
                if (mbSaveToServer(true, ref result, false))
                {
                    if (result != null && result.Length > 0)
                    {
                        result += "\n";
                    }

                    CTzDeviceSettings.sbMakeSettingFileName(out fileName, CTzDeviceSettings._cSettingDefaultName);

                    fileName = Path.Combine(_mTzRoot, fileName);

                    bool bCopyOk;
                    bOk = _mSettings.mbWriteSettings(fileName, "", CTzDeviceSettings._cFileID_UNKNOWN, false, out bCopyOk);

                    if (bOk)
                    {
                        result += "Saved Defaults";

                        CTzDeviceSettings.sbMakeSettingFileName(out fileName, CTzDeviceSettings._cSettingSdcardName);

                        fileName = Path.Combine(_mTzRoot, fileName);

                        bOk = _mSettings.mbWriteSettings(fileName, "", CTzDeviceSettings._cFileID_UNKNOWN, false, out bCopyOk);  // no snr in config.tzs file

                        string actionResult = "";
                        mbSaveDefaultAction(out actionResult);
                        result += ", Action " + actionResult;

                        mbSaveEmptyAction(out actionResult);    // create empty action
                    }
                    else
                    {
                        result += "Failed save defaults";
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("SaveToDefault failed from " + fileName, ex);
                result += "Error";
                mSetResultText(false, result);
            }

            mSetResultText(bOk, result);
        }

        private bool mbSendAction(out string AResult, bool AbSpecial)
        {
            string result = "";
            bool bOk = true;

            bool bCtrl = AbSpecial && (Control.ModifierKeys & Keys.Control) == Keys.Control;
            bool bAlt = AbSpecial && (Control.ModifierKeys & Keys.Alt) == Keys.Alt;
            bool bShift = AbSpecial && (Control.ModifierKeys & Keys.Shift) == Keys.Shift;
            bool bExtra = true; // CLicKeyDev.sbDeviceIsProgrammer();
            string message = "";
            string patID = textBoxPatientID.Text;
            UInt32 actionNumber = 0;
            UInt32 settingNumber = 0;
            UInt32 scpStart1 = 0;
            UInt32 scpCount1 = 0;
            UInt32 scpStart2 = 0;
            UInt32 scpCount2 = 0;
            UInt32 scpStart3 = 0;
            UInt32 scpCount3 = 0;

            UInt32 tzrYear = 0;
            UInt32 tzrMonth = 0;
            UInt32 tzrDay = 0;
            UInt32 tzrHour = 0;

            bool bNewStudy = checkBoxNewStudy.Checked;
            bool bStartStudy = checkBoxStartStudy.Checked;
            bool bNewSetting = checkBoxNewSetting.Checked;
            bool bEndStudy = checkBoxEndStudy.Checked;
            bool bResetFileNumber = checkBoxResetFileNumber.Checked;
            bool bMsg = checkBoxMessage.Checked;

            if (bResetFileNumber)
            {
                bOk &= CTzDeviceSettings.sbSetActionSeqNr(0, _mTzDevicePath, _mSerialNr);
            }
            bOk &= CTzDeviceSettings.sbIncActionSeqNr(out actionNumber, _mTzDevicePath, _mSerialNr);
            bOk &= CTzDeviceSettings.sbGetSettingSeqNr(out settingNumber, _mTzDevicePath, _mSerialNr);

            if (bMsg)
            {
                message = textBoxMessage.Text.Trim();

                if (message == null || message == "")
                {
                    if (bNewStudy && bStartStudy) message = "New Start Study " + patID;
                    else if (bNewStudy) message = "New Study " + patID;
                    else if (bEndStudy) message = "End Study " + patID;

                    if (message == null || message == "")
                    {

                    }
                    else
                    {
                        message = "A" + actionNumber.ToString() + "S" + settingNumber.ToString() + ": " + message;
                    }
                }
            }
            if (bOk)
            {
                if (bCtrl && bExtra)
                {
                    bOk = CProgram.sbReqUInt32("TzAera Action: Request SCP strip", "SCP start number", ref scpStart1, "", 0, 9999999)
                        && CProgram.sbReqUInt32("TzAera Action: Request SCP strip", "nr of SCP files", ref scpCount1, "", 0, 254);

                    if (bOk)
                    {
                        if (CProgram.sbReqUInt32("TzAera Action: Request SCP strip 2", "SCP start number", ref scpStart2, "", 0, 9999999)
                            && CProgram.sbReqUInt32("TzAera Action: Request SCP strip 2", "nr of SCP files", ref scpCount2, "", 0, 254))
                        {
                            if (CProgram.sbReqUInt32("TzAera Action: Request SCP strip 3", "SCP start number", ref scpStart3, "", 0, 9999999))
                            {
                                CProgram.sbReqUInt32("TzAera Action: Request SCP strip 3", "nr of SCP files", ref scpCount3, "", 0, 254);
                            }
                        }
                    }
                }
                else if (bAlt && bExtra)
                {
                    DateTime dt = DateTime.Now.AddHours(-1);
                    tzrYear = (UInt32)dt.Year;
                    tzrMonth = (UInt32)dt.Month;
                    tzrDay = (UInt32)dt.Day;
                    tzrHour = (UInt32)dt.Hour;

                    bOk = CProgram.sbReqUInt32("TzAera Action: Request Trend file", "Year", ref tzrYear, "", 0, 2099)
                        && CProgram.sbReqUInt32("TzAera Action: Request Trend file", "month", ref tzrMonth, "", 0, 2099)
                        && CProgram.sbReqUInt32("TzAera Action: Request Trend file", "day", ref tzrDay, "", 0, 2099)
                        && CProgram.sbReqUInt32("TzAera Action: Request Trend file", "hour", ref tzrHour, "", 0, 2099);
                }
                else if (bShift && bExtra)
                {
                    bOk = CProgram.sbReqUInt32("TzAera Action: Request one SCP file", "SCP start number", ref scpStart1, "", 0, 9999999);
                        
                    if (bOk)
                    {
                        scpCount1 = 1;
                        if (CProgram.sbReqUInt32("TzAera Action: Request SCP file 2", "SCP start number", ref scpStart2, "", 0, 9999999))
                        {
                            scpCount2 = 1;
                            if( CProgram.sbReqUInt32("TzAera Action: Request one SCP file 3", "SCP start number", ref scpStart3, "", 0, 9999999))
                            {
                                scpCount3 = 1;
                            }
                            
                        }
                    }
                }
                else
                {

                }
            }
            if (bOk)
            {
                string fileName = "";

                UInt32 index;
                Byte[] buffer = null;
                try
                {
                    if (CTzDeviceSettings.sbMakeActionFileName(out fileName, _mSerialNr))
                    {
                        fileName = Path.Combine(_mTzDevicePath, fileName);
                        buffer = _mSettings.mActionCreateBuffer(out index, _mSerialNr, (UInt16)actionNumber);

                        if (buffer != null)
                        {
                            if (bNewStudy)
                            {
                                bOk &= _mSettings.mbActionEnableNewStudy(buffer, ref index);
                            }
                            if (bStartStudy)
                            {
                                bOk &= _mSettings.mbActionStartStudy(buffer, ref index);
                            }
                            if (bNewSetting)
                            {
                                bOk &= _mSettings.mbActionUpdateSettings(buffer, ref index);
                            }
                            if (bEndStudy)
                            {
                                bOk &= _mSettings.mbActionEndStudy(buffer, ref index);
                            }
                            if (bMsg)
                            {
                                bOk &= _mSettings.mbActionDisplayMessage(buffer, ref index, message);
                            }
                            if (scpCount1 > 0)
                            {
                                bOk &= bCtrl ? _mSettings.mbActionRequestScpBlock(buffer, ref index, scpStart1, (UInt16)scpCount1)
                                            : _mSettings.mbActionRetransmitScpFile(buffer, ref index, scpStart1);
                            }
                            if (scpCount2 > 0)
                            {
                                bOk &= bCtrl ? _mSettings.mbActionRequestScpBlock(buffer, ref index, scpStart2, (UInt16)scpCount2)
                                            : _mSettings.mbActionRetransmitScpFile(buffer, ref index, scpStart2);
                            }
                            if (scpCount3 > 0)
                            {
                                bOk &= bCtrl ? _mSettings.mbActionRequestScpBlock(buffer, ref index, scpStart3, (UInt16)scpCount3)
                                            : _mSettings.mbActionRetransmitScpFile(buffer, ref index, scpStart3);
                            }
                            if (tzrYear > 0)
                            {
                                bOk &= _mSettings.mbActionRequestTzrFile(buffer, ref index, (UInt16)tzrYear, (UInt16)tzrMonth, (UInt16)tzrDay, (UInt16)tzrHour);
                            }
                            if (false == bOk)
                            {
                                result = "create";
                            }
                            else
                            {
                                result = CTzDeviceSettings._sbUseQueue ? "writen to queue" : "writen";
                                bool bCopyOk;
                                bOk = _mSettings.mbWriteAction(fileName, buffer, ref index, true, out bCopyOk, (UInt16)actionNumber);

                                if (bOk)
                                {
                                    if (false == bCopyOk)
                                    {
                                        result += " failed copy";
                                    }
                                }
                                else
                                {
                                    result = "Failed write";
                                }

                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Send Action", ex);
                }
            }
            if (message.Length > 0) result += " (" + message + ")";
            AResult = result;
            mUpdateFileTime();
            return bOk;
        }

        private void buttonSendAction_Click(object sender, EventArgs e)
        {
            string result;
            bool bOk = true;
            bool bResetFileNumber = checkBoxResetFileNumber.Checked;

            if (bResetFileNumber)
            {
                bool bDo = true;

                uint settingNumber = 0;
                uint actionNumber = 0;

                CTzDeviceSettings.sbGetActionSeqNr(out actionNumber, _mTzDevicePath, _mSerialNr);
                CTzDeviceSettings.sbGetSettingSeqNr(out settingNumber, _mTzDevicePath, _mSerialNr);

                if (actionNumber > 1 || settingNumber > 1)
                {
                    bOk = CProgram.sbReqBool("TZ action to server", "Reset file number and Start Study", ref bDo);

                    if (bOk)
                    {
                        checkBoxResetFileNumber.Checked = bDo;
                        checkBoxStartStudy.Checked = true;
                    }
                }
            }
            if (bOk)
            {
                mSetResultText(mbSendAction(out result, true), result);
            }
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            timerRefresh.Stop();
            mUpdateFileTime();
            timerRefresh.Start();
        }

        private void labelClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void label62_Click(object sender, EventArgs e)
        {
            mSaveToDefault();
        }

        private void labelLoadCurrent_Click_1(object sender, EventArgs e)
        {
            bool bDevice = false;
            if (mbLoadDeviceSettings(out bDevice))
            {
                mSetResultText(true, bDevice ? "Device reloaded" : "Default loaded");
            }
            else
            {
                mSetResultText(false, bDevice ? "Failed reload device" : "Failed load defaults");
            }
            mFillForm();
            mUpdateFileTime();
        }

        private void labelLoadDefaults_Click(object sender, EventArgs e)
        {
            if (mbLoadDefaultSettings())
            {
                mSetResultText(true, "Defaults loaded");
            }
            else
            {
                mSetResultText(false, "Failed load defaults");
            }
            mFillForm();
            mUpdateFileTime();
        }

        private void labelLoadSettings_Click_1(object sender, EventArgs e)
        {
            mLoadSettingsFrom();
        }

        private void buttonClearAction_Click(object sender, EventArgs e)
        {
            timerRefresh.Stop();

            UInt32 nInQueue = 0;
            bool bOk = CTzDeviceSettings.sbClearToServer(out nInQueue, _mTzDevicePath, _mSerialNr, "");// CTzDeviceSettings._cActionFileExt);

            mClearDoneFile( _mTzDevicePath, _mSerialNr, ".MsgReceived");
            mClearDoneFile( _mTzDevicePath, _mSerialNr, ".ActionDone");
            mClearDoneFile(_mTzDevicePath, _mSerialNr, ".SettingSet");
            mSetResultText(bOk, "Cleared " + nInQueue.ToString() + " from Queue");
            mUpdateFileTime();
            timerRefresh.Start();
        }

        private void buttonTzrModeSave_Click(object sender, EventArgs e)
        {
            mSaveCollectTzr();
        }

        private bool mbLoadCollectScp()
        {
            bool bOk = false;

            _mCollectScp = new CTzAutoCollect("SCP", DTzCollectFileFormat.D6, ".scp");
            if (_mCollectScp != null)
            {
                bOk = mbUpdateInfoCollectScp();
            }
            return bOk;
        }

        private bool mbLoadCollectTzr()
        {
            bool bOk = false;

            _mCollectTzr = new CTzAutoCollect("TZR", DTzCollectFileFormat.YYYYMMDD_HH, ".tzr");
            if (_mCollectTzr != null)
            {
                bOk = mbUpdateInfoCollectTzr();
            }
            return bOk;
        }
 
        private bool mbFillCollectScp()
        {
            bool bOk = false;

            if (_mCollectScp != null)
            {
                bool bActive = mbCheckStudyActive();

                checkBoxCollectScpEnabled.Checked = _mCollectScp._mbEnabled;
                listBoxScpStudyMode.SelectedIndex = (int)_mCollectScp._mStudyMode;
                labelScpStudyNr.Text = _mCollectScp._mStudy_IX == 0 ? "" : _mCollectScp._mStudy_IX.ToString();
                listBoxScpStartMode.SelectedIndex = (int)_mCollectScp._mStartMode;
                labelScpStartFirst.Text = _mCollectScp._mFirstPresentFileID == 0 || _mCollectScp._mFirstPresentFileID > CTzAutoCollect._cMaxFileID ? "" : _mCollectScp._mFirstPresentFileID.ToString();
                textBoxScpStartNr.Text = _mCollectScp._mStartFileNr.ToString();
                listBoxScpStopMode.SelectedIndex = (int)_mCollectScp._mStopMode;
                labelScpStopLast.Text = _mCollectScp._mLastPresentFileID == 0 || _mCollectScp._mLastPresentFileID > CTzAutoCollect._cMaxFileID ? "" : _mCollectScp._mLastPresentFileID.ToString();
                textBoxScpStopNr.Text = _mCollectScp._mStopFileNr.ToString();
                bOk = true;
            }
            else
            {
                checkBoxCollectScpEnabled.Checked = false;
                listBoxScpStudyMode.SelectedIndex = 0;
                labelScpStudyNr.Text = "";
                listBoxScpStartMode.SelectedIndex = 0;
                labelScpStartFirst.Text = "";
                textBoxScpStartNr.Text = "";
                listBoxScpStopMode.SelectedIndex = 0;
                labelScpStopLast.Text = "";
                textBoxScpStopNr.Text = "";
            }
            mbCheckButtonCollectScp();
            return bOk;
        }
        private bool mbFillCollectTzr()
        {
            bool bOk = false;

            if (_mCollectTzr != null)
            {
                bool bActive = mbCheckStudyActive();

                checkBoxCollectTzrEnabled.Checked = _mCollectTzr._mbEnabled;
                listBoxTzrStudyMode.SelectedIndex = (int)_mCollectTzr._mStudyMode;
                labelTzrStudyNr.Text = _mCollectTzr._mStudy_IX == 0 ? "" : _mCollectTzr._mStudy_IX.ToString();
                listBoxTzrStartMode.SelectedIndex = (int)_mCollectTzr._mStartMode;
                labelTzrStartFirst.Text = _mCollectTzr._mFirstPresentFileID == 0 || _mCollectTzr._mFirstPresentFileID > CTzAutoCollect._cMaxFileID ? "" : _mCollectTzr._mFirstPresentFileID.ToString();
                textBoxTzrStartNr.Text = _mCollectTzr._mStartFileNr.ToString();
                listBoxTzrStopMode.SelectedIndex = (int)_mCollectTzr._mStopMode;
                labelTzrStopLast.Text = _mCollectTzr._mLastPresentFileID == 0 || _mCollectTzr._mLastPresentFileID > CTzAutoCollect._cMaxFileID ? "" : _mCollectTzr._mLastPresentFileID.ToString();
                textBoxTzrStopNr.Text = _mCollectTzr._mStopFileNr.ToString();
                bOk = true;
            }
            else
            {
                checkBoxCollectTzrEnabled.Checked = false;
                listBoxTzrStudyMode.SelectedIndex = 0;
                labelTzrStudyNr.Text = "";
                listBoxTzrStartMode.SelectedIndex = 0;
                labelTzrStartFirst.Text = "";
                textBoxTzrStartNr.Text = "";
                listBoxTzrStopMode.SelectedIndex = 0;
                labelTzrStopLast.Text = "";
                textBoxTzrStopNr.Text = "";
            }
            mbCheckButtonCollectTzr();
            return bOk;
        }
        private bool mbReadCollectScp()
        {
            bool bOk = false;

            try
            {
                if (_mCollectScp != null)
                {
                    bool bActive = mbCheckStudyActive();

                    _mCollectScp._mbEnabled = checkBoxCollectScpEnabled.Checked;

                    _mCollectScp._mStudyMode = (DTzCollectStudyMode)listBoxScpStudyMode.SelectedIndex;

                    UInt32 studyNr = 0;
                    UInt32.TryParse(labelScpStudyNr.Text, out studyNr);
                    _mCollectScp._mStudy_IX = studyNr;

                    _mCollectScp._mStartMode = (DTzCollectStartMode)listBoxScpStartMode.SelectedIndex;
                    UInt32 start = 0;
                    UInt32.TryParse(textBoxScpStartNr.Text, out start);
                    _mCollectScp._mStartFileNr = start;

                    _mCollectScp._mStopMode = (DTzCollectStopMode)listBoxScpStopMode.SelectedIndex;
                    UInt32 stop = 0;
                    UInt32.TryParse(textBoxScpStopNr.Text, out stop);
                    _mCollectScp._mStopFileNr = stop;

                    bOk = true;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Read Collect TZ", ex);
            }
            mbUpdateInfoCollectScp();
            return bOk;
        }
        private bool mbReadCollectTzr()
        {
            bool bOk = false;

            if (_mCollectTzr != null)
            {
                bool bActive = mbCheckStudyActive();

                _mCollectTzr._mbEnabled = checkBoxCollectTzrEnabled.Checked;

                _mCollectTzr._mStudyMode = (DTzCollectStudyMode)listBoxTzrStudyMode.SelectedIndex;

                UInt32 studyNr = 0;
                UInt32.TryParse(labelTzrStudyNr.Text, out studyNr);
                _mCollectTzr._mStudy_IX = studyNr;

                _mCollectTzr._mStartMode = (DTzCollectStartMode)listBoxTzrStartMode.SelectedIndex;
                UInt32 start = 0;
                UInt32.TryParse(textBoxTzrStartNr.Text, out start);
                _mCollectTzr._mStartFileNr = start;

                _mCollectTzr._mStopMode = (DTzCollectStopMode)listBoxTzrStopMode.SelectedIndex;
                UInt32 stop = 0;
                UInt32.TryParse(textBoxTzrStopNr.Text, out stop);
                _mCollectTzr._mStopFileNr = stop;

                bOk = true;
            }
            mbUpdateInfoCollectTzr();
            return bOk;
        }

        private bool mbUpdateInfoCollectScp()
        {
            bool bOk = false;
            string parmInfo = "-";
            string scanInfo = "-";

            if (_mCollectScp != null && _mDeviceInfo != null)
            {
                // parameters
                parmInfo = _mCollectScp.mParamFileInfoLoadNew(_mTzDevicePath, _mSerialNr, true);
                mbFillCollectScp();

                scanInfo = _mCollectScp.mScanInfoLoadMissing(_mTzDevicePath, _mSerialNr, _mDeviceInfo._mActiveStudy_IX, true);
                // 
            }
            labelScpSavedInfo.Text = parmInfo;
            labelScpLastScan.Text = scanInfo;
            mbCheckButtonCollectScp();
            return bOk;
        }
        private bool mbUpdateInfoCollectTzr()
        {
            bool bOk = false;
            string parmInfo = "-";
            string scanInfo = "-";

            if (_mCollectTzr != null && _mDeviceInfo != null)
            {
                // parameters
                parmInfo = _mCollectTzr.mParamFileInfoLoadNew(_mTzDevicePath, _mSerialNr, true);
                mbFillCollectTzr();

                scanInfo = _mCollectTzr.mScanInfoLoadMissing(_mTzDevicePath, _mSerialNr, _mDeviceInfo._mActiveStudy_IX, true);
                // 
            }
            labelTzrSavedInfo.Text = parmInfo;
            labelTzrLastScan.Text = scanInfo;
            mbCheckButtonCollectTzr();
            return bOk;
        }
        private bool mbCheckButtonCollectScp()
        {
            bool bOk = false;

            if (_mCollectScp != null)
            {
                bOk = mbCheckStudyActive();

                buttonScpCollectTest.Enabled = bOk;
            }
            return bOk;
        }
        private bool mbCheckButtonCollectTzr()
        {
            bool bOk = false;

            if (_mCollectTzr != null)
            {
                bOk = mbCheckStudyActive();

                buttonTzrCollectTest.Enabled = bOk;
            }
            return bOk;
        }
        private bool mbCheckStudyActive()
        {
            bool bActive = false;

            if (_mDeviceInfo != null && _mDeviceInfo._mAddStudyMode == (UInt16)DAddStudyModeEnum.ActiveStudy )
            {
                if (_mDeviceInfo._mActiveStudy_IX != 0 && _mDeviceInfo._mRecorderEndUTC != DateTime.MinValue)
                {
                    bActive = DateTime.UtcNow < _mDeviceInfo._mRecorderEndUTC;
                }
            }
            return bActive;
        }
        private bool mbFillStudyActive()
        {
            string s = "No Study";
            bool bActive = false;

            if (_mDeviceInfo != null)
            {
                if( _mDeviceInfo._mAddStudyMode != (UInt16)DAddStudyModeEnum.ActiveStudy)
                {
                    s = _mDeviceInfo.mGetAddStudyModeLabel(false) + " ! Study " + _mDeviceInfo._mActiveStudy_IX.ToString();
                }
                else if (_mDeviceInfo._mActiveStudy_IX != 0)
                {
                    s = "Study " + _mDeviceInfo._mActiveStudy_IX.ToString();

                    if (_mDeviceInfo._mRecorderEndUTC != DateTime.MinValue)
                    {
                        bActive = DateTime.UtcNow < _mDeviceInfo._mRecorderEndUTC;
                        if (bActive)
                        {
                            s += " Active, started " + CProgram.sDateTimeToString(_mDeviceInfo._mRecorderStartUTC) + " UTC";
                        }
                        else
                        {
                            s += " Ended " + CProgram.sDateTimeToString(_mDeviceInfo._mRecorderEndUTC) + " UTC";
                        }
                    }
                }
            }
            labelStudyActive.Text = s;

            return bActive;
        }

        private void mTestCollectTzr()
        {
            if (mbReadCollectTzr())
            {
                string result = "";
                bool bRunDone = false;
                bool bEnabled = false;
                bool bRunAutomatic = false;

                bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

                if (bCtrl )
                    {
                    if( false == CProgram.sbAskYesNo("TZ collect Trend TZR for " + _mDeviceInfo._mActiveStudy_IX.ToString(), "Run automatic scan for missing Trend TZR"))
                    {
                        return;
                    }
                    bRunAutomatic = true; // ?? false;
                }

                bool bOk = _mCollectTzr.mbRunCycle(out result, out bEnabled, out bRunDone, _mTzDevicePath, _mSerialNr, _mDeviceInfo._mActiveStudy_IX,
                    _mDeviceInfo._mRecorderStartUTC, _mDeviceInfo._mRecorderEndUTC, bRunAutomatic);

                mSetResultText(bOk, result);

                mbUpdateInfoCollectTzr();
                mUpdateFileTime();
            }
        }
        private void mTestCollectScp()
        {
            if (mbReadCollectScp())
            {
                string result = "";
                bool bRunDone = false;
                bool bEnabled = false;
                bool bRunAutomatic = false;

                bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

                if (bCtrl)
                {
                    if (false == CProgram.sbAskYesNo("TZ collect ECG SCP for " + _mDeviceInfo._mActiveStudy_IX.ToString(), "Run automatic scan for missing ECG SCP"))
                    {
                        return;
                    }
                    bRunAutomatic = false;
                }

                bool bOk = _mCollectScp.mbRunCycle(out result, out bEnabled,  out bRunDone, _mTzDevicePath, _mSerialNr, _mDeviceInfo._mActiveStudy_IX,
                    _mDeviceInfo._mRecorderStartUTC, _mDeviceInfo._mRecorderEndUTC, bRunAutomatic);

                mSetResultText(bOk, result);

                mbUpdateInfoCollectScp();
                mUpdateFileTime();
            }
        }
        private void buttonScpCollectTest_Click(object sender, EventArgs e)
        {
            mTestCollectScp();
        }

        private void mSaveCollectScp()
        {
            if (mbReadCollectScp())
            {
                mSetResultText(_mCollectScp.mbSaveParamFile(_mTzDevicePath, _mSerialNr), "save collect ecg scp");

                mbUpdateInfoCollectScp();
            }
        }
        private void mSaveCollectTzr()
        {
            if (mbReadCollectTzr())
            {
                mSetResultText(_mCollectTzr.mbSaveParamFile(_mTzDevicePath, _mSerialNr), "save collect trend tzr");

                mbUpdateInfoCollectTzr();
            }
        }

        private void buttonScpModeSave_Click(object sender, EventArgs e)
        {
            mSaveCollectScp();
        }

        private void buttonScpRefreshInfo_Click(object sender, EventArgs e)
        {
            mbUpdateInfoCollectScp();
        }

        private void labelDeviceSnr_DoubleClick(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl ) //&& CLicKeyDev.sbDeviceIsProgrammer())
            {
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = @"explorer";
                startInfo.Arguments = _mTzDevicePath;
                Process.Start(startInfo);
            }
        }

        private void buttonTzrRefreshInfo_Click(object sender, EventArgs e)
        {
            mbUpdateInfoCollectTzr();
        }

        private void labelStudyActive_DoubleClick(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl && _mDeviceInfo != null && _mDeviceInfo._mActiveStudy_IX > 0)
            {
                string studyPath;

                if(CDvtmsData.sbGetStudyRecorderDir(out studyPath, _mDeviceInfo._mActiveStudy_IX, true))
                    {
                    ProcessStartInfo startInfo = new ProcessStartInfo();
                    startInfo.FileName = @"explorer";
                    startInfo.Arguments = studyPath;
                    Process.Start(startInfo);
                }
            }
        }

        private void labelScpStudyNr_Click(object sender, EventArgs e)
        {
            if (_mCollectScp != null && _mDeviceInfo != null && _mDeviceInfo._mActiveStudy_IX > 0)
            {
                _mCollectScp._mStudy_IX = _mDeviceInfo._mActiveStudy_IX;
                labelScpStudyNr.Text = _mCollectScp._mStudy_IX.ToString();
            }
        }

        private void label80_Click(object sender, EventArgs e)
        {
            labelScpStudyNr_Click(sender, e);
        }

        private void labelTzrStudyNr_Click(object sender, EventArgs e)
        {
            if (_mCollectTzr != null && _mDeviceInfo != null && _mDeviceInfo._mActiveStudy_IX > 0)
            {
                _mCollectTzr._mStudy_IX = _mDeviceInfo._mActiveStudy_IX;
                labelTzrStudyNr.Text = _mCollectTzr._mStudy_IX.ToString();
            }

        }

        private void label102_Click(object sender, EventArgs e)
        {
            labelTzrStudyNr_Click(sender, e);
        }

        private void buttonTzrCollectTest_Click(object sender, EventArgs e)
        {
            mTestCollectTzr();
        }

        private void panel31_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label42_Click_1(object sender, EventArgs e)
        {

        }

        private void labelDeviceSnr_Click(object sender, EventArgs e)
        {

        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBoxAutoCollect_CheckedChanged(object sender, EventArgs e)
        {
            panelAutoCollect.Visible = checkBoxAutoCollect.Checked;
        }

        private void labelActionQueue_DoubleClick(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl && CLicKeyDev.sbDeviceIsProgrammer())
            {
                CTzDeviceSettings._sbUseQueue = !CTzDeviceSettings._sbUseQueue;
                labelActionQueue.Text = CTzDeviceSettings._sbUseQueue ? "Queue ON" : "Queue off";
            }

        }

        private void timerRefresh_Tick(object sender, EventArgs e)
        {

        }
    }
}