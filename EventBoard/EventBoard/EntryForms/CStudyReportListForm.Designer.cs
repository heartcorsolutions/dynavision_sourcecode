﻿namespace EventboardEntryForms
{
    partial class CStudyReportListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CStudyReportListForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel27 = new System.Windows.Forms.Panel();
            this.labelGender = new System.Windows.Forms.Label();
            this.panel26 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.panel25 = new System.Windows.Forms.Panel();
            this.labelAge = new System.Windows.Forms.Label();
            this.panel24 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.panel23 = new System.Windows.Forms.Panel();
            this.labelDoB = new System.Windows.Forms.Label();
            this.panel21 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel20 = new System.Windows.Forms.Panel();
            this.labelFullName = new System.Windows.Forms.Label();
            this.panel15 = new System.Windows.Forms.Panel();
            this.labelPreviousFindingsList = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.labelStudyIndex = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panelStudyButtons = new System.Windows.Forms.Panel();
            this.labelReportResult = new System.Windows.Forms.Label();
            this.panel46 = new System.Windows.Forms.Panel();
            this.panel19 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panelSelect = new System.Windows.Forms.Panel();
            this.panel87 = new System.Windows.Forms.Panel();
            this.button5 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.labelNrReports = new System.Windows.Forms.Label();
            this.panel81 = new System.Windows.Forms.Panel();
            this.labelMissingReports = new System.Windows.Forms.Label();
            this.buttonCheckNrReports = new System.Windows.Forms.Button();
            this.panel82 = new System.Windows.Forms.Panel();
            this.panel83 = new System.Windows.Forms.Panel();
            this.panel85 = new System.Windows.Forms.Panel();
            this.panel86 = new System.Windows.Forms.Panel();
            this.panel84 = new System.Windows.Forms.Panel();
            this.button9 = new System.Windows.Forms.Button();
            this.panel80 = new System.Windows.Forms.Panel();
            this.panel79 = new System.Windows.Forms.Panel();
            this.panelStudyReport1 = new System.Windows.Forms.Panel();
            this.panel55 = new System.Windows.Forms.Panel();
            this.panel61 = new System.Windows.Forms.Panel();
            this.textBoxConclusion = new System.Windows.Forms.TextBox();
            this.panel60 = new System.Windows.Forms.Panel();
            this.panel59 = new System.Windows.Forms.Panel();
            this.panel51 = new System.Windows.Forms.Panel();
            this.panel53 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.panel52 = new System.Windows.Forms.Panel();
            this.panel37 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.button4 = new System.Windows.Forms.Button();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.panel41 = new System.Windows.Forms.Panel();
            this.panel31 = new System.Windows.Forms.Panel();
            this.panel40 = new System.Windows.Forms.Panel();
            this.textBoxSummary = new System.Windows.Forms.TextBox();
            this.panel72 = new System.Windows.Forms.Panel();
            this.panel39 = new System.Windows.Forms.Panel();
            this.panel30 = new System.Windows.Forms.Panel();
            this.panel63 = new System.Windows.Forms.Panel();
            this.panel49 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.panel48 = new System.Windows.Forms.Panel();
            this.panel32 = new System.Windows.Forms.Panel();
            this.panelStudy = new System.Windows.Forms.Panel();
            this.panelRows = new System.Windows.Forms.Panel();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RefID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnAnalysisDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel43 = new System.Windows.Forms.Panel();
            this.panel29 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.labelStudyNote = new System.Windows.Forms.Label();
            this.panel6.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel26.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panelStudyButtons.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panelSelect.SuspendLayout();
            this.panel87.SuspendLayout();
            this.panel81.SuspendLayout();
            this.panel84.SuspendLayout();
            this.panelStudyReport1.SuspendLayout();
            this.panel55.SuspendLayout();
            this.panel61.SuspendLayout();
            this.panel51.SuspendLayout();
            this.panel53.SuspendLayout();
            this.panel37.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel31.SuspendLayout();
            this.panel40.SuspendLayout();
            this.panel30.SuspendLayout();
            this.panel49.SuspendLayout();
            this.panelStudy.SuspendLayout();
            this.panelRows.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // panel5
            // 
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1198, 18);
            this.panel5.TabIndex = 1;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.panel27);
            this.panel6.Controls.Add(this.panel26);
            this.panel6.Controls.Add(this.panel25);
            this.panel6.Controls.Add(this.panel24);
            this.panel6.Controls.Add(this.panel23);
            this.panel6.Controls.Add(this.panel21);
            this.panel6.Controls.Add(this.panel20);
            this.panel6.Controls.Add(this.panel15);
            this.panel6.Controls.Add(this.panel4);
            this.panel6.Controls.Add(this.panel3);
            this.panel6.Controls.Add(this.panel9);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 18);
            this.panel6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1198, 33);
            this.panel6.TabIndex = 2;
            // 
            // panel27
            // 
            this.panel27.Controls.Add(this.labelGender);
            this.panel27.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel27.Location = new System.Drawing.Point(1098, 0);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(76, 33);
            this.panel27.TabIndex = 10;
            // 
            // labelGender
            // 
            this.labelGender.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelGender.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelGender.Location = new System.Drawing.Point(0, 0);
            this.labelGender.Name = "labelGender";
            this.labelGender.Size = new System.Drawing.Size(66, 33);
            this.labelGender.TabIndex = 6;
            this.labelGender.Text = "Female";
            this.labelGender.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel26
            // 
            this.panel26.Controls.Add(this.label3);
            this.panel26.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel26.Location = new System.Drawing.Point(1018, 0);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(80, 33);
            this.panel26.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Left;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 33);
            this.label3.TabIndex = 5;
            this.label3.Text = "Gender:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel25
            // 
            this.panel25.Controls.Add(this.labelAge);
            this.panel25.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel25.Location = new System.Drawing.Point(972, 0);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(46, 33);
            this.panel25.TabIndex = 8;
            // 
            // labelAge
            // 
            this.labelAge.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelAge.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAge.Location = new System.Drawing.Point(0, 0);
            this.labelAge.Name = "labelAge";
            this.labelAge.Size = new System.Drawing.Size(43, 33);
            this.labelAge.TabIndex = 4;
            this.labelAge.Text = "102";
            this.labelAge.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel24
            // 
            this.panel24.Controls.Add(this.label5);
            this.panel24.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel24.Location = new System.Drawing.Point(917, 0);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(55, 33);
            this.panel24.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Right;
            this.label5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(6, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 33);
            this.label5.TabIndex = 3;
            this.label5.Text = "Age:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel23
            // 
            this.panel23.Controls.Add(this.labelDoB);
            this.panel23.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel23.Location = new System.Drawing.Point(815, 0);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(102, 33);
            this.panel23.TabIndex = 6;
            // 
            // labelDoB
            // 
            this.labelDoB.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelDoB.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelDoB.Location = new System.Drawing.Point(0, 0);
            this.labelDoB.Name = "labelDoB";
            this.labelDoB.Size = new System.Drawing.Size(94, 33);
            this.labelDoB.TabIndex = 3;
            this.labelDoB.Text = "12/31/1974";
            this.labelDoB.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel21
            // 
            this.panel21.Controls.Add(this.label2);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel21.Location = new System.Drawing.Point(687, 0);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(128, 33);
            this.panel21.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Right;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(122, 33);
            this.label2.TabIndex = 2;
            this.label2.Text = "Date of Birth:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.labelFullName);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel20.Location = new System.Drawing.Point(251, 0);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(436, 33);
            this.panel20.TabIndex = 4;
            // 
            // labelFullName
            // 
            this.labelFullName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelFullName.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelFullName.Location = new System.Drawing.Point(0, 0);
            this.labelFullName.Name = "labelFullName";
            this.labelFullName.Size = new System.Drawing.Size(436, 33);
            this.labelFullName.TabIndex = 2;
            this.labelFullName.Text = "Rutger Brest van Kempen";
            this.labelFullName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.labelPreviousFindingsList);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel15.Location = new System.Drawing.Point(188, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(63, 33);
            this.panel15.TabIndex = 3;
            // 
            // labelPreviousFindingsList
            // 
            this.labelPreviousFindingsList.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPreviousFindingsList.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelPreviousFindingsList.Location = new System.Drawing.Point(0, 0);
            this.labelPreviousFindingsList.Name = "labelPreviousFindingsList";
            this.labelPreviousFindingsList.Size = new System.Drawing.Size(60, 33);
            this.labelPreviousFindingsList.TabIndex = 1;
            this.labelPreviousFindingsList.Text = "Name:";
            this.labelPreviousFindingsList.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.labelStudyNote);
            this.panel4.Controls.Add(this.labelStudyIndex);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(97, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(91, 33);
            this.panel4.TabIndex = 2;
            // 
            // labelStudyIndex
            // 
            this.labelStudyIndex.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelStudyIndex.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelStudyIndex.Location = new System.Drawing.Point(0, 0);
            this.labelStudyIndex.Name = "labelStudyIndex";
            this.labelStudyIndex.Size = new System.Drawing.Size(68, 33);
            this.labelStudyIndex.TabIndex = 2;
            this.labelStudyIndex.Text = "313131";
            this.labelStudyIndex.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelStudyIndex.DoubleClick += new System.EventHandler(this.labelStudyIndex_DoubleClick);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(13, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(84, 33);
            this.panel3.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Left;
            this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 33);
            this.label4.TabIndex = 1;
            this.label4.Text = "Study #:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel9
            // 
            this.panel9.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(13, 33);
            this.panel9.TabIndex = 0;
            // 
            // panel7
            // 
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 51);
            this.panel7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1198, 18);
            this.panel7.TabIndex = 3;
            // 
            // panelStudyButtons
            // 
            this.panelStudyButtons.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.panelStudyButtons.Controls.Add(this.labelReportResult);
            this.panelStudyButtons.Controls.Add(this.panel46);
            this.panelStudyButtons.Controls.Add(this.panel19);
            this.panelStudyButtons.Controls.Add(this.panel18);
            this.panelStudyButtons.Controls.Add(this.panel17);
            this.panelStudyButtons.Controls.Add(this.panel16);
            this.panelStudyButtons.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelStudyButtons.Location = new System.Drawing.Point(0, 773);
            this.panelStudyButtons.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelStudyButtons.Name = "panelStudyButtons";
            this.panelStudyButtons.Size = new System.Drawing.Size(1198, 41);
            this.panelStudyButtons.TabIndex = 6;
            // 
            // labelReportResult
            // 
            this.labelReportResult.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelReportResult.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelReportResult.ForeColor = System.Drawing.Color.White;
            this.labelReportResult.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelReportResult.Location = new System.Drawing.Point(16, 0);
            this.labelReportResult.Name = "labelReportResult";
            this.labelReportResult.Size = new System.Drawing.Size(215, 41);
            this.labelReportResult.TabIndex = 22;
            this.labelReportResult.Text = "Report: #";
            this.labelReportResult.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel46
            // 
            this.panel46.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel46.Location = new System.Drawing.Point(0, 0);
            this.panel46.Name = "panel46";
            this.panel46.Size = new System.Drawing.Size(16, 41);
            this.panel46.TabIndex = 23;
            // 
            // panel19
            // 
            this.panel19.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel19.Location = new System.Drawing.Point(520, 0);
            this.panel19.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(552, 41);
            this.panel19.TabIndex = 4;
            // 
            // panel18
            // 
            this.panel18.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel18.Location = new System.Drawing.Point(1072, 0);
            this.panel18.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(18, 41);
            this.panel18.TabIndex = 3;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.button1);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel17.Location = new System.Drawing.Point(1090, 0);
            this.panel17.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(95, 41);
            this.panel17.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(0, 0);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(95, 41);
            this.button1.TabIndex = 7;
            this.button1.Text = "Close";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // panel16
            // 
            this.panel16.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel16.Location = new System.Drawing.Point(1185, 0);
            this.panel16.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(13, 41);
            this.panel16.TabIndex = 1;
            // 
            // panelSelect
            // 
            this.panelSelect.Controls.Add(this.panel87);
            this.panelSelect.Controls.Add(this.label1);
            this.panelSelect.Controls.Add(this.labelNrReports);
            this.panelSelect.Controls.Add(this.panel81);
            this.panelSelect.Controls.Add(this.panel82);
            this.panelSelect.Controls.Add(this.panel83);
            this.panelSelect.Controls.Add(this.panel85);
            this.panelSelect.Controls.Add(this.panel86);
            this.panelSelect.Controls.Add(this.panel84);
            this.panelSelect.Controls.Add(this.panel80);
            this.panelSelect.Controls.Add(this.panel79);
            this.panelSelect.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelSelect.Location = new System.Drawing.Point(0, 0);
            this.panelSelect.Name = "panelSelect";
            this.panelSelect.Size = new System.Drawing.Size(1198, 38);
            this.panelSelect.TabIndex = 0;
            // 
            // panel87
            // 
            this.panel87.Controls.Add(this.button5);
            this.panel87.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel87.Location = new System.Drawing.Point(139, 0);
            this.panel87.Name = "panel87";
            this.panel87.Size = new System.Drawing.Size(180, 38);
            this.panel87.TabIndex = 19;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.button5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button5.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.button5.Location = new System.Drawing.Point(0, 0);
            this.button5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(180, 38);
            this.button5.TabIndex = 5;
            this.button5.Text = "Open PDF report";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click_1);
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(65, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 38);
            this.label1.TabIndex = 20;
            this.label1.Text = "Reports";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelNrReports
            // 
            this.labelNrReports.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelNrReports.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelNrReports.Location = new System.Drawing.Point(13, 0);
            this.labelNrReports.Name = "labelNrReports";
            this.labelNrReports.Size = new System.Drawing.Size(52, 38);
            this.labelNrReports.TabIndex = 21;
            this.labelNrReports.Text = "9999";
            this.labelNrReports.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel81
            // 
            this.panel81.Controls.Add(this.labelMissingReports);
            this.panel81.Controls.Add(this.buttonCheckNrReports);
            this.panel81.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel81.Location = new System.Drawing.Point(381, 0);
            this.panel81.Name = "panel81";
            this.panel81.Size = new System.Drawing.Size(521, 38);
            this.panel81.TabIndex = 5;
            // 
            // labelMissingReports
            // 
            this.labelMissingReports.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelMissingReports.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelMissingReports.Location = new System.Drawing.Point(161, 0);
            this.labelMissingReports.Name = "labelMissingReports";
            this.labelMissingReports.Size = new System.Drawing.Size(354, 38);
            this.labelMissingReports.TabIndex = 21;
            this.labelMissingReports.Text = "^12 Reports missing, last report not ok";
            this.labelMissingReports.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // buttonCheckNrReports
            // 
            this.buttonCheckNrReports.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonCheckNrReports.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonCheckNrReports.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonCheckNrReports.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonCheckNrReports.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCheckNrReports.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCheckNrReports.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonCheckNrReports.Location = new System.Drawing.Point(0, 0);
            this.buttonCheckNrReports.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonCheckNrReports.Name = "buttonCheckNrReports";
            this.buttonCheckNrReports.Size = new System.Drawing.Size(161, 38);
            this.buttonCheckNrReports.TabIndex = 22;
            this.buttonCheckNrReports.Text = "Check NrReports";
            this.buttonCheckNrReports.UseVisualStyleBackColor = false;
            this.buttonCheckNrReports.Click += new System.EventHandler(this.buttonCheckNrReports_Click);
            // 
            // panel82
            // 
            this.panel82.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel82.Location = new System.Drawing.Point(902, 0);
            this.panel82.Name = "panel82";
            this.panel82.Size = new System.Drawing.Size(21, 38);
            this.panel82.TabIndex = 6;
            // 
            // panel83
            // 
            this.panel83.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel83.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel83.Location = new System.Drawing.Point(923, 0);
            this.panel83.Name = "panel83";
            this.panel83.Size = new System.Drawing.Size(52, 38);
            this.panel83.TabIndex = 13;
            // 
            // panel85
            // 
            this.panel85.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel85.Location = new System.Drawing.Point(975, 0);
            this.panel85.Name = "panel85";
            this.panel85.Size = new System.Drawing.Size(15, 38);
            this.panel85.TabIndex = 15;
            // 
            // panel86
            // 
            this.panel86.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel86.Location = new System.Drawing.Point(990, 0);
            this.panel86.Name = "panel86";
            this.panel86.Size = new System.Drawing.Size(15, 38);
            this.panel86.TabIndex = 17;
            // 
            // panel84
            // 
            this.panel84.Controls.Add(this.button9);
            this.panel84.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel84.Location = new System.Drawing.Point(1005, 0);
            this.panel84.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel84.Name = "panel84";
            this.panel84.Size = new System.Drawing.Size(180, 38);
            this.panel84.TabIndex = 9;
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.button9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button9.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.button9.Location = new System.Drawing.Point(0, 0);
            this.button9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(180, 38);
            this.button9.TabIndex = 4;
            this.button9.Text = "Resend PDF report";
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Visible = false;
            // 
            // panel80
            // 
            this.panel80.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel80.Location = new System.Drawing.Point(1185, 0);
            this.panel80.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel80.Name = "panel80";
            this.panel80.Size = new System.Drawing.Size(13, 38);
            this.panel80.TabIndex = 8;
            // 
            // panel79
            // 
            this.panel79.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel79.Location = new System.Drawing.Point(0, 0);
            this.panel79.Name = "panel79";
            this.panel79.Size = new System.Drawing.Size(13, 38);
            this.panel79.TabIndex = 3;
            // 
            // panelStudyReport1
            // 
            this.panelStudyReport1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelStudyReport1.Controls.Add(this.panel55);
            this.panelStudyReport1.Controls.Add(this.panel51);
            this.panelStudyReport1.Controls.Add(this.panel37);
            this.panelStudyReport1.Controls.Add(this.panel31);
            this.panelStudyReport1.Controls.Add(this.panel30);
            this.panelStudyReport1.Controls.Add(this.panel32);
            this.panelStudyReport1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelStudyReport1.Location = new System.Drawing.Point(0, 38);
            this.panelStudyReport1.Name = "panelStudyReport1";
            this.panelStudyReport1.Size = new System.Drawing.Size(1198, 324);
            this.panelStudyReport1.TabIndex = 1;
            this.panelStudyReport1.Visible = false;
            this.panelStudyReport1.Paint += new System.Windows.Forms.PaintEventHandler(this.panelStudyReport1_Paint);
            // 
            // panel55
            // 
            this.panel55.Controls.Add(this.panel61);
            this.panel55.Controls.Add(this.panel60);
            this.panel55.Controls.Add(this.panel59);
            this.panel55.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel55.Location = new System.Drawing.Point(0, 242);
            this.panel55.Name = "panel55";
            this.panel55.Size = new System.Drawing.Size(1198, 57);
            this.panel55.TabIndex = 15;
            // 
            // panel61
            // 
            this.panel61.Controls.Add(this.textBoxConclusion);
            this.panel61.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel61.Location = new System.Drawing.Point(13, 0);
            this.panel61.Name = "panel61";
            this.panel61.Size = new System.Drawing.Size(1172, 57);
            this.panel61.TabIndex = 5;
            // 
            // textBoxConclusion
            // 
            this.textBoxConclusion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxConclusion.Location = new System.Drawing.Point(0, 0);
            this.textBoxConclusion.MaxLength = 250;
            this.textBoxConclusion.Multiline = true;
            this.textBoxConclusion.Name = "textBoxConclusion";
            this.textBoxConclusion.Size = new System.Drawing.Size(1172, 57);
            this.textBoxConclusion.TabIndex = 2;
            this.textBoxConclusion.Text = "This is a manual  summary of the study";
            // 
            // panel60
            // 
            this.panel60.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel60.Location = new System.Drawing.Point(1185, 0);
            this.panel60.Name = "panel60";
            this.panel60.Size = new System.Drawing.Size(13, 57);
            this.panel60.TabIndex = 4;
            // 
            // panel59
            // 
            this.panel59.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel59.Location = new System.Drawing.Point(0, 0);
            this.panel59.Name = "panel59";
            this.panel59.Size = new System.Drawing.Size(13, 57);
            this.panel59.TabIndex = 3;
            // 
            // panel51
            // 
            this.panel51.Controls.Add(this.panel53);
            this.panel51.Controls.Add(this.panel52);
            this.panel51.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel51.Location = new System.Drawing.Point(0, 211);
            this.panel51.Name = "panel51";
            this.panel51.Size = new System.Drawing.Size(1198, 31);
            this.panel51.TabIndex = 14;
            // 
            // panel53
            // 
            this.panel53.Controls.Add(this.label14);
            this.panel53.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel53.Location = new System.Drawing.Point(13, 0);
            this.panel53.Name = "panel53";
            this.panel53.Size = new System.Drawing.Size(214, 31);
            this.panel53.TabIndex = 3;
            // 
            // label14
            // 
            this.label14.Dock = System.Windows.Forms.DockStyle.Left;
            this.label14.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(0, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(186, 31);
            this.label14.TabIndex = 4;
            this.label14.Text = "Physician Conclusion";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel52
            // 
            this.panel52.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel52.Location = new System.Drawing.Point(0, 0);
            this.panel52.Name = "panel52";
            this.panel52.Size = new System.Drawing.Size(13, 31);
            this.panel52.TabIndex = 2;
            // 
            // panel37
            // 
            this.panel37.Controls.Add(this.panel13);
            this.panel37.Controls.Add(this.panel12);
            this.panel37.Controls.Add(this.panel11);
            this.panel37.Controls.Add(this.panel10);
            this.panel37.Controls.Add(this.panel2);
            this.panel37.Controls.Add(this.panel41);
            this.panel37.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel37.Location = new System.Drawing.Point(0, 173);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(1198, 38);
            this.panel37.TabIndex = 13;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.button4);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel13.Location = new System.Drawing.Point(615, 0);
            this.panel13.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(180, 38);
            this.panel13.TabIndex = 18;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.button4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button4.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.button4.Location = new System.Drawing.Point(0, 0);
            this.button4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(180, 38);
            this.button4.TabIndex = 9;
            this.button4.Text = "Open Study";
            this.button4.UseVisualStyleBackColor = false;
            // 
            // panel12
            // 
            this.panel12.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel12.Location = new System.Drawing.Point(795, 0);
            this.panel12.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(15, 38);
            this.panel12.TabIndex = 17;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.button3);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel11.Location = new System.Drawing.Point(810, 0);
            this.panel11.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(180, 38);
            this.panel11.TabIndex = 16;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button3.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.button3.Location = new System.Drawing.Point(0, 0);
            this.button3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(180, 38);
            this.button3.TabIndex = 8;
            this.button3.Text = "Edit findings";
            this.button3.UseVisualStyleBackColor = false;
            // 
            // panel10
            // 
            this.panel10.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel10.Location = new System.Drawing.Point(990, 0);
            this.panel10.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(15, 38);
            this.panel10.TabIndex = 15;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(1005, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(180, 38);
            this.panel2.TabIndex = 14;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button2.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.button2.Location = new System.Drawing.Point(0, 0);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(180, 38);
            this.button2.TabIndex = 7;
            this.button2.Text = "Save findings";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // panel41
            // 
            this.panel41.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel41.Location = new System.Drawing.Point(1185, 0);
            this.panel41.Name = "panel41";
            this.panel41.Size = new System.Drawing.Size(13, 38);
            this.panel41.TabIndex = 4;
            // 
            // panel31
            // 
            this.panel31.Controls.Add(this.panel40);
            this.panel31.Controls.Add(this.panel72);
            this.panel31.Controls.Add(this.panel39);
            this.panel31.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel31.Location = new System.Drawing.Point(0, 43);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(1198, 130);
            this.panel31.TabIndex = 12;
            // 
            // panel40
            // 
            this.panel40.Controls.Add(this.textBoxSummary);
            this.panel40.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel40.Location = new System.Drawing.Point(13, 0);
            this.panel40.Name = "panel40";
            this.panel40.Size = new System.Drawing.Size(1172, 130);
            this.panel40.TabIndex = 5;
            // 
            // textBoxSummary
            // 
            this.textBoxSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxSummary.Location = new System.Drawing.Point(0, 0);
            this.textBoxSummary.MaxLength = 800;
            this.textBoxSummary.Multiline = true;
            this.textBoxSummary.Name = "textBoxSummary";
            this.textBoxSummary.Size = new System.Drawing.Size(1172, 130);
            this.textBoxSummary.TabIndex = 1;
            this.textBoxSummary.Text = resources.GetString("textBoxSummary.Text");
            this.textBoxSummary.TextChanged += new System.EventHandler(this.textBoxSummary_TextChanged);
            // 
            // panel72
            // 
            this.panel72.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel72.Location = new System.Drawing.Point(0, 0);
            this.panel72.Name = "panel72";
            this.panel72.Size = new System.Drawing.Size(13, 130);
            this.panel72.TabIndex = 4;
            // 
            // panel39
            // 
            this.panel39.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel39.Location = new System.Drawing.Point(1185, 0);
            this.panel39.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel39.Name = "panel39";
            this.panel39.Size = new System.Drawing.Size(13, 130);
            this.panel39.TabIndex = 2;
            // 
            // panel30
            // 
            this.panel30.Controls.Add(this.panel63);
            this.panel30.Controls.Add(this.panel49);
            this.panel30.Controls.Add(this.panel48);
            this.panel30.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel30.Location = new System.Drawing.Point(0, 10);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(1198, 33);
            this.panel30.TabIndex = 11;
            // 
            // panel63
            // 
            this.panel63.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel63.Location = new System.Drawing.Point(1185, 0);
            this.panel63.Name = "panel63";
            this.panel63.Size = new System.Drawing.Size(13, 33);
            this.panel63.TabIndex = 3;
            // 
            // panel49
            // 
            this.panel49.Controls.Add(this.label13);
            this.panel49.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel49.Location = new System.Drawing.Point(13, 0);
            this.panel49.Name = "panel49";
            this.panel49.Size = new System.Drawing.Size(214, 33);
            this.panel49.TabIndex = 2;
            // 
            // label13
            // 
            this.label13.Dock = System.Windows.Forms.DockStyle.Left;
            this.label13.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(0, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(197, 33);
            this.label13.TabIndex = 4;
            this.label13.Text = "Reported  Findings";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label13.Click += new System.EventHandler(this.label13_Click_1);
            // 
            // panel48
            // 
            this.panel48.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel48.Location = new System.Drawing.Point(0, 0);
            this.panel48.Name = "panel48";
            this.panel48.Size = new System.Drawing.Size(13, 33);
            this.panel48.TabIndex = 1;
            // 
            // panel32
            // 
            this.panel32.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel32.Location = new System.Drawing.Point(0, 0);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(1198, 10);
            this.panel32.TabIndex = 0;
            // 
            // panelStudy
            // 
            this.panelStudy.AutoSize = true;
            this.panelStudy.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelStudy.Controls.Add(this.panelSelect);
            this.panelStudy.Controls.Add(this.panelStudyReport1);
            this.panelStudy.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelStudy.Location = new System.Drawing.Point(0, 411);
            this.panelStudy.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelStudy.Name = "panelStudy";
            this.panelStudy.Size = new System.Drawing.Size(1198, 362);
            this.panelStudy.TabIndex = 5;
            // 
            // panelRows
            // 
            this.panelRows.AutoSize = true;
            this.panelRows.Controls.Add(this.dataGridView);
            this.panelRows.Controls.Add(this.panel43);
            this.panelRows.Controls.Add(this.panel29);
            this.panelRows.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelRows.Location = new System.Drawing.Point(0, 69);
            this.panelRows.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelRows.Name = "panelRows";
            this.panelRows.Size = new System.Drawing.Size(1198, 337);
            this.panelRows.TabIndex = 7;
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column7,
            this.Column16,
            this.Column6,
            this.Column12,
            this.RefID,
            this.Column14,
            this.Column15,
            this.Column2,
            this.Column17,
            this.FileName,
            this.Column11,
            this.ColumnAnalysisDate,
            this.Column1,
            this.Column4,
            this.Column5,
            this.Column8,
            this.Column9,
            this.Column10,
            this.Column3,
            this.Column13});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 10F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView.Location = new System.Drawing.Point(13, 0);
            this.dataGridView.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            this.dataGridView.RowHeadersVisible = false;
            this.dataGridView.RowTemplate.Height = 33;
            this.dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView.Size = new System.Drawing.Size(1172, 337);
            this.dataGridView.TabIndex = 3;
            this.dataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellClick);
            this.dataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellClick);
            this.dataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellDoubleClick);
            this.dataGridView.Click += new System.EventHandler(this.dataGridView_Click);
            this.dataGridView.DoubleClick += new System.EventHandler(this.dataGridView_DoubleClick);
            this.dataGridView.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dataGridView_MouseClick);
            // 
            // Column7
            // 
            this.Column7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Column7.DefaultCellStyle = dataGridViewCellStyle1;
            this.Column7.HeaderText = "Report #";
            this.Column7.MaxInputLength = 6;
            this.Column7.MinimumWidth = 10;
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Visible = false;
            // 
            // Column16
            // 
            this.Column16.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column16.HeaderText = "Report Nr";
            this.Column16.Name = "Column16";
            this.Column16.ReadOnly = true;
            this.Column16.Width = 87;
            // 
            // Column6
            // 
            this.Column6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column6.HeaderText = "Report date";
            this.Column6.MaxInputLength = 20;
            this.Column6.MinimumWidth = 100;
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // Column12
            // 
            this.Column12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column12.HeaderText = "Type";
            this.Column12.MaxInputLength = 8;
            this.Column12.MinimumWidth = 40;
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.Width = 64;
            // 
            // RefID
            // 
            this.RefID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.RefID.HeaderText = "event";
            this.RefID.Name = "RefID";
            this.RefID.ReadOnly = true;
            this.RefID.Width = 68;
            // 
            // Column14
            // 
            this.Column14.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.Column14.HeaderText = "From";
            this.Column14.MaxInputLength = 10;
            this.Column14.MinimumWidth = 100;
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            // 
            // Column15
            // 
            this.Column15.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.Column15.HeaderText = "Until";
            this.Column15.MaxInputLength = 10;
            this.Column15.MinimumWidth = 100;
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.Column2.HeaderText = "Tech";
            this.Column2.MaxInputLength = 6;
            this.Column2.MinimumWidth = 50;
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 50;
            // 
            // Column17
            // 
            this.Column17.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column17.HeaderText = "Created At";
            this.Column17.Name = "Column17";
            this.Column17.ReadOnly = true;
            this.Column17.Width = 92;
            // 
            // FileName
            // 
            this.FileName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.FileName.HeaderText = "PDF file name";
            this.FileName.Name = "FileName";
            this.FileName.ReadOnly = true;
            this.FileName.Width = 112;
            // 
            // Column11
            // 
            this.Column11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.Column11.HeaderText = "Diagnosis";
            this.Column11.MaxInputLength = 16;
            this.Column11.MinimumWidth = 100;
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.Visible = false;
            // 
            // ColumnAnalysisDate
            // 
            this.ColumnAnalysisDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.ColumnAnalysisDate.HeaderText = "Analysis Date";
            this.ColumnAnalysisDate.MaxInputLength = 10;
            this.ColumnAnalysisDate.MinimumWidth = 100;
            this.ColumnAnalysisDate.Name = "ColumnAnalysisDate";
            this.ColumnAnalysisDate.ReadOnly = true;
            this.ColumnAnalysisDate.Visible = false;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.Column1.HeaderText = "QC MD";
            this.Column1.MaxInputLength = 6;
            this.Column1.MinimumWidth = 50;
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Visible = false;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Column4.HeaderText = "Tachy\'s";
            this.Column4.MaxInputLength = 5;
            this.Column4.MinimumWidth = 50;
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Visible = false;
            // 
            // Column5
            // 
            this.Column5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Column5.HeaderText = "Brady\'s";
            this.Column5.MaxInputLength = 5;
            this.Column5.MinimumWidth = 50;
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Visible = false;
            // 
            // Column8
            // 
            this.Column8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Column8.HeaderText = "Pause\'s";
            this.Column8.MaxInputLength = 5;
            this.Column8.MinimumWidth = 50;
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Visible = false;
            // 
            // Column9
            // 
            this.Column9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Column9.HeaderText = "Afib\'s";
            this.Column9.MaxInputLength = 5;
            this.Column9.MinimumWidth = 50;
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column9.Visible = false;
            // 
            // Column10
            // 
            this.Column10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Column10.HeaderText = "Other\'s";
            this.Column10.MaxInputLength = 5;
            this.Column10.MinimumWidth = 50;
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Visible = false;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.Column3.HeaderText = "Sent date";
            this.Column3.MaxInputLength = 10;
            this.Column3.MinimumWidth = 100;
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Visible = false;
            // 
            // Column13
            // 
            this.Column13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.Column13.HeaderText = "Sent to";
            this.Column13.MaxInputLength = 32;
            this.Column13.MinimumWidth = 100;
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            this.Column13.Visible = false;
            // 
            // panel43
            // 
            this.panel43.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel43.Location = new System.Drawing.Point(1185, 0);
            this.panel43.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel43.Name = "panel43";
            this.panel43.Size = new System.Drawing.Size(13, 337);
            this.panel43.TabIndex = 2;
            // 
            // panel29
            // 
            this.panel29.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel29.Location = new System.Drawing.Point(0, 0);
            this.panel29.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(13, 337);
            this.panel29.TabIndex = 1;
            // 
            // panel8
            // 
            this.panel8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel8.Location = new System.Drawing.Point(0, 406);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(1198, 5);
            this.panel8.TabIndex = 8;
            // 
            // labelStudyNote
            // 
            this.labelStudyNote.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelStudyNote.Font = new System.Drawing.Font("Arial", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStudyNote.Location = new System.Drawing.Point(68, 0);
            this.labelStudyNote.Name = "labelStudyNote";
            this.labelStudyNote.Size = new System.Drawing.Size(22, 33);
            this.labelStudyNote.TabIndex = 4;
            this.labelStudyNote.Text = "sl\r\nsn";
            this.labelStudyNote.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // CStudyReportListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(1198, 814);
            this.Controls.Add(this.panelRows);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panelStudy);
            this.Controls.Add(this.panelStudyButtons);
            this.Font = new System.Drawing.Font("Arial", 10F);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "CStudyReportListForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Study Report List";
            this.panel6.ResumeLayout(false);
            this.panel27.ResumeLayout(false);
            this.panel26.ResumeLayout(false);
            this.panel25.ResumeLayout(false);
            this.panel24.ResumeLayout(false);
            this.panel23.ResumeLayout(false);
            this.panel21.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panelStudyButtons.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.panelSelect.ResumeLayout(false);
            this.panel87.ResumeLayout(false);
            this.panel81.ResumeLayout(false);
            this.panel84.ResumeLayout(false);
            this.panelStudyReport1.ResumeLayout(false);
            this.panel55.ResumeLayout(false);
            this.panel61.ResumeLayout(false);
            this.panel61.PerformLayout();
            this.panel51.ResumeLayout(false);
            this.panel53.ResumeLayout(false);
            this.panel37.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel31.ResumeLayout(false);
            this.panel40.ResumeLayout(false);
            this.panel40.PerformLayout();
            this.panel30.ResumeLayout(false);
            this.panel49.ResumeLayout(false);
            this.panelStudy.ResumeLayout(false);
            this.panelRows.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panelStudyButtons;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelPreviousFindingsList;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelGender;
        private System.Windows.Forms.Label labelAge;
        private System.Windows.Forms.Label labelDoB;
        private System.Windows.Forms.Label labelFullName;
        private System.Windows.Forms.Label labelStudyIndex;
        private System.Windows.Forms.Panel panelSelect;
        private System.Windows.Forms.Panel panel87;
        private System.Windows.Forms.Panel panel86;
        private System.Windows.Forms.Panel panel85;
        private System.Windows.Forms.Panel panel83;
        private System.Windows.Forms.Panel panel84;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Panel panel80;
        private System.Windows.Forms.Panel panel82;
        private System.Windows.Forms.Panel panel81;
        private System.Windows.Forms.Panel panel79;
        private System.Windows.Forms.Panel panelStudyReport1;
        private System.Windows.Forms.Panel panel55;
        private System.Windows.Forms.Panel panel61;
        private System.Windows.Forms.Panel panel60;
        private System.Windows.Forms.Panel panel59;
        private System.Windows.Forms.Panel panel51;
        private System.Windows.Forms.Panel panel53;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel52;
        private System.Windows.Forms.Panel panel37;
        private System.Windows.Forms.Panel panel41;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.Panel panel40;
        private System.Windows.Forms.Panel panel72;
        private System.Windows.Forms.Panel panel39;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Panel panel63;
        private System.Windows.Forms.Panel panel49;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel48;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.Panel panelStudy;
        private System.Windows.Forms.TextBox textBoxConclusion;
        private System.Windows.Forms.TextBox textBoxSummary;
        private System.Windows.Forms.Panel panelRows;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Panel panel43;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label labelReportResult;
        private System.Windows.Forms.Panel panel46;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelNrReports;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn RefID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewTextBoxColumn FileName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnAnalysisDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.Label labelMissingReports;
        private System.Windows.Forms.Button buttonCheckNrReports;
        private System.Windows.Forms.Label labelStudyNote;
    }
}