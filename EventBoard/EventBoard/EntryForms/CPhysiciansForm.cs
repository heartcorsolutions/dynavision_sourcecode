﻿using Event_Base;
using EventboardEntryForms;
using Program_Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventBoard.EntryForms
{
    public partial class CPhysiciansForm : Form
    {
        public CPhysicianInfo _mInfo = null;
        public CPhysicianInfo _mSearchInfo = null;
        private List<CSqlDataTableRow> _mList = null;
        private List<CSqlDataTableRow> _mClientList = null;
        private CClientInfo _mClient = null;
        private bool _mbLoadListError = false;

        private UInt32 _mSelectedIndex = 0;
        public bool _mbSelected = false;

        public CPhysiciansForm(CPhysicianInfo APhysician, bool AbEnableSelect)
        {
            try
            {
                InitializeComponent();  // seperate in try because it has a strange exception
            }
            catch (Exception)
            {
            }

            try
            {
//                Width = 1600;   // Nicer size then minimum 1240

                buttonSelect.Visible = AbEnableSelect;

                CDvtmsData.sbInitData();

                buttonRandom.Visible = CLicKeyDev.sbDeviceIsProgrammer();

                _mList = new List<CSqlDataTableRow>();
                _mClientList = new List<CSqlDataTableRow>();
                _mClient = new CClientInfo(CDvtmsData.sGetDBaseConnection());

                _mInfo = new CPhysicianInfo(CDvtmsData.sGetDBaseConnection());
                if (_mInfo == null || _mList == null || _mClient == null || _mClientList == null )
                {
                    CProgram.sLogLine(" PhysicianInfoForm() failed to init Client data");
                    Close();
                }
                else
                {
                    bool bError;
                    UInt64 loadMask = _mClient.mMaskIndexKey | CSqlDataTableRow.sGetMask((UInt16)DClientInfoVars.Label);

                    _mInfo.mbActive = true;

                    if( APhysician != null )
                    {
                        _mInfo.mbCopyFrom(APhysician);
                    }

                    _mClient.mbDoSqlSelectList(out bError, out _mClientList, loadMask, 0, true, "");
                    LabelPhysicianNr.Text = "";

                    mLoadList();
                    mSetValues();
                    mbCheckForm(APhysician == null ? "New" : "Edit");

                    Text = CProgram.sMakeProgTitle("Physician List", false, true);

                }
                labelSelected.Visible = CLicKeyDev.sbDeviceIsProgrammer();
            }
            catch (Exception ex)
            {
                CProgram.sLogException(" CLientInfoForm() failed to init", ex);
            }
        }


        public void mUpdateButtons()
        {
            if( _mInfo != null )
            {
                bool bNew = _mInfo.mIndex_KEY == 0;
                LabelPhysicianNr.Text = bNew ? "" : _mInfo.mIndex_KEY.ToString();
                buttonAddUpdate.Text = bNew ? "Add" : "Update";
                textBoxClientLabel.ReadOnly = false == bNew;
                buttonDisable.Text = _mInfo.mbActive ? "Disable" : "Enable";
                mGetCursorSelected();
            }
        }


        public void mSetValues()
        {
            // set the values from info on the form
            textBoxClientLabel.Text = _mInfo._mLabel;
            checkBoxActive.Checked = _mInfo.mbActive;
            comboBoxGenderTitleList.Text = _mInfo._mGenderTitle;
            comboBoxAcademicTitleList.Text = _mInfo._mAcademicTitle;
            textBoxFirstName.Text = _mInfo._mFirstName.mDecrypt();
            textBoxMiddleName.Text = _mInfo._mMiddleName.mDecrypt();
            textBoxLastName.Text = _mInfo._mLastName.mDecrypt();

            UInt32 dateOfBirth = (UInt32)_mInfo._mDateOfBirth.mDecrypt();
            UInt16 year, month, day;

            if( CProgram.sbSplitYMD( dateOfBirth, out year, out month, out day ))
            {
                textBoxDateOfBirthDay.Text = day.ToString();
                textBoxDateOfBirthYear.Text = year.ToString();
                textBoxAgeResult.Text = CProgram.sCalcAgeYears( year, month, day, DateTime.Now ).ToString();
            }
            else
            {
                textBoxDateOfBirthDay.Text = "";
                textBoxDateOfBirthYear.Text = "";
                textBoxAgeResult.Text = "";
            }
            if (CDvtmsData._sEnumListMonthName != null ) CDvtmsData._sEnumListMonthName.mFillComboBox( comboBoxDateOfBirthMonthList, false, 0, false, "",  month.ToString(), null );

            CPatientInfo.sFillCombobox(comboBoxGender, _mInfo._mGender_IX);

            if (_mClient != null) _mClient.mbListFillComboBox(comboBoxHospitalList, _mClientList, (UInt16)DClientInfoVars.Label, _mInfo._mHospital_IX, "-- Select --");


            textBoxAddress.Text = _mInfo._mAddress.mDecrypt();
            textBoxZip.Text = _mInfo._mZipCode.mDecrypt();
            textBoxHouseNumber.Text = _mInfo._mHouseNr.mDecrypt();
            comboBoxStateList.Text = _mInfo._mState;
            textBoxCity.Text = _mInfo._mCity;
            if (CDvtmsData._sEnumListCountries != null)
            {
                CDvtmsData._sEnumListCountries.mFillComboBox(comboBoxCountryList, false, 0, false, "", _mInfo._mCountryCode, "-- Select--");
            }

            //            comboBoxCountryList.Text = _mInfo._mCountryList;
            textBoxPhone1.Text = _mInfo._mPhone1.mDecrypt();
            textBoxPhone2.Text = _mInfo._mPhone2.mDecrypt();
            textBoxCell.Text = _mInfo._mCell.mDecrypt();
            textBoxSkype.Text = _mInfo._mSkype.mDecrypt();
            textBoxEmail.Text = _mInfo._mEmail.mDecrypt();

            textBoxInstruction.Text = _mInfo._mInstruction.mDecrypt();
        }

        private UInt32 mReadIndex(string AString)
        {
            UInt32 index = 0;

            if (AString != null && AString.Length > 0)
            {
                if (false == UInt32.TryParse(AString, out index))
                {
                    index = 0;
                }
            }
            return index;
        }

        public UInt16 mGetDateValuesAge()
        {
            UInt16 day, month, year;
            UInt16 age = 0;

            if (UInt16.TryParse(textBoxDateOfBirthDay.Text, out day)
             && CDvtmsData._sEnumListMonthName != null
             && UInt16.TryParse(CDvtmsData._sEnumListMonthName.mReadComboBoxCode(comboBoxDateOfBirthMonthList), out month)
             && UInt16.TryParse(textBoxDateOfBirthYear.Text, out year))
            {
                if (year < 100) year += 1900;
                //                if( day >= 1 && day <= 31 && month >= 1 && month <= 12 && year >= 1800 && year < 2200 )
                {
                    _mInfo._mDateOfBirth.mEncrypt((int)CProgram.sCalcYMD(year, month, day));
                }
                age = CProgram.sCalcAgeYears(year, month, day, DateTime.Now);
            }
            return age;
        }

        public void mGetValues(out UInt32 ArIndex)
        {
            // get the values from the form  and store in info

            ArIndex = mReadIndex(LabelPhysicianNr.Text);
            _mInfo._mLabel = textBoxClientLabel.Text;
            _mInfo.mbActive = checkBoxActive.Checked;

            _mInfo._mGenderTitle = comboBoxGenderTitleList.Text;
            _mInfo._mAcademicTitle = comboBoxAcademicTitleList.Text;
            _mInfo._mFirstName.mbEncrypt( textBoxFirstName.Text );
            _mInfo._mMiddleName.mbEncrypt( textBoxMiddleName.Text );
            _mInfo._mLastName.mbEncrypt( textBoxLastName.Text );

            mGetDateValuesAge();

            DGender gender = DGender.Unknown;
            CPatientInfo.sbParseGetGenderString(comboBoxGender.Text, ref gender);

            _mInfo._mGender_IX = (UInt16)gender;
            if (_mClient != null) _mClient.mbListParseComboBox(comboBoxHospitalList, _mClientList, (UInt16)DClientInfoVars.Label, ref _mInfo._mHospital_IX);

            _mInfo._mAddress.mbEncrypt( textBoxAddress.Text );
            _mInfo._mZipCode.mbEncrypt( textBoxZip.Text );
            _mInfo._mHouseNr.mbEncrypt( textBoxHouseNumber.Text );
            _mInfo._mState = comboBoxStateList.Text;
            _mInfo._mCity = textBoxCity.Text;
            if (CDvtmsData._sEnumListCountries != null)
            {
                _mInfo._mCountryCode = CDvtmsData._sEnumListCountries.mReadComboBoxCode(comboBoxCountryList, false, "");
            }
            _mInfo._mPhone1.mbEncrypt( textBoxPhone1.Text );
            _mInfo._mPhone2.mbEncrypt(textBoxPhone2.Text);
            _mInfo._mSkype.mbEncrypt( textBoxSkype.Text );
            _mInfo._mEmail.mbEncrypt( textBoxEmail.Text );

            _mInfo._mInstruction.mbEncrypt(textBoxInstruction.Text);
        }

        public void mUpdateList()
        {
            if (_mList != null && _mInfo != null)
            {
                int nList, nResult = 0, iCurrent = 0;

                dataGridViewList.Rows.Clear();

                if (_mList == null || (nList = _mList.Count()) == 0)
                {
                    labelListN.Text = "0" + (_mbLoadListError ? "!" : "");
                    labelResultN.Text = "-";
                }
                else
                {
                    bool bAll = checkBoxShowDeactivated.Checked;
                    bool bSearch = checkBoxShowSearchResult.Checked && _mSearchInfo != null;
                    bool bAdd;

                    foreach (CPhysicianInfo row in _mList)
                    {
                        if (bAll || row.mbActive)
                        {
                            bAdd = true;
                            if (bSearch)
                            {
                                if (bAdd && _mSearchInfo.mIndex_KEY != 0) bAdd = row.mIndex_KEY == _mSearchInfo.mIndex_KEY;
                                if (bAdd) bAdd = CProgram.sbCmpSearchString(row._mLabel, _mInfo._mLabel);
                                if (bAdd) bAdd = CProgram.sbCmpSearchString(row._mGenderTitle, _mInfo._mGenderTitle);
                                if (bAdd) bAdd = CProgram.sbCmpSearchString(row._mAcademicTitle, _mInfo._mAcademicTitle);
                                if (bAdd) bAdd = CEncryptedString.sbCmpSearchString(row._mFirstName, _mInfo._mFirstName);
                                if (bAdd) bAdd = CEncryptedString.sbCmpSearchString(row._mMiddleName, _mInfo._mMiddleName);
                                if (bAdd) bAdd = CEncryptedString.sbCmpSearchString(row._mLastName, _mInfo._mLastName);

                                if (bAdd && _mInfo._mDateOfBirth.mbNotZero()) bAdd = row._mDateOfBirth.mDecrypt() == _mInfo._mDateOfBirth.mDecrypt();
                                if (bAdd && _mInfo._mGender_IX > 0) bAdd = row._mGender_IX == _mInfo._mGender_IX;
                                if (bAdd && _mInfo._mHospital_IX > 0) bAdd = row._mHospital_IX == _mInfo._mHospital_IX;

                                if (bAdd) bAdd = CEncryptedString.sbCmpSearchString(row._mAddress, _mInfo._mAddress);
                                if (bAdd) bAdd = CEncryptedString.sbCmpSearchString(row._mZipCode, _mInfo._mZipCode);
                                if (bAdd) bAdd = CEncryptedString.sbCmpSearchString(row._mHouseNr, _mInfo._mHouseNr);
                                if (bAdd) bAdd = CProgram.sbCmpSearchString(row._mCity, _mInfo._mCity);
                                if (bAdd) bAdd = CProgram.sbCmpSearchString(row._mState, _mInfo._mState);
                                if (bAdd) bAdd = CProgram.sbCmpSearchString(row._mCountryCode, _mInfo._mCountryCode);


                                if (bAdd) bAdd = CEncryptedString.sbCmpSearchString(row._mPhone1, _mInfo._mPhone1);
                                if (bAdd) bAdd = CEncryptedString.sbCmpSearchString(row._mPhone2, _mInfo._mPhone2);
                                if (bAdd) bAdd = CEncryptedString.sbCmpSearchString(row._mCell, _mInfo._mCell);
                                if (bAdd) bAdd = CEncryptedString.sbCmpSearchString(row._mEmail, _mInfo._mEmail);
                                if (bAdd) bAdd = CEncryptedString.sbCmpSearchString(row._mSkype, _mInfo._mSkype);
                            }
                            if (bAdd)
                            {
                                if (row.mIndex_KEY == _mInfo.mIndex_KEY)
                                {
                                    iCurrent = nResult;
                                }


                                string fullName = row._mLastName.mDecrypt() + ", " + row._mMiddleName.mDecrypt() + ", " + row._mFirstName.mDecrypt();
                                dataGridViewList.Rows.Add(row.mIndex_KEY, row.mbActive.ToString(), row._mLabel, fullName, row._mCity, row._mCountryCode,
                                    row._mZipCode.mDecrypt(), row._mHouseNr.mDecrypt(), row._mAddress.mDecrypt(), 
                                    row._mState, row._mPhone1.mDecrypt(), row._mPhone2.mDecrypt(), row._mCell.mDecrypt(), row._mSkype.mDecrypt(), row._mEmail.mDecrypt(),
                                    row._mDateOfBirth.mDecrypt().ToString());
                                ++nResult;

                            }
                        }

                    }
                    if( nResult > 0)
                    {
                        dataGridViewList.CurrentCell = dataGridViewList.Rows[iCurrent].Cells[0];
                    }
                    labelListN.Text = nList.ToString() + (_mbLoadListError ? "!" : "");
                    labelResultN.Text = nResult.ToString();
                }
            }
        }

        public void mLoadList()
        {
            if (_mInfo != null)
            {
                _mInfo.mbDoSqlSelectList(out _mbLoadListError, out _mList, _mInfo.mMaskValid, 0, false, "");

                mUpdateList();
            }
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            if (_mInfo != null)
            {
                _mInfo.mClear();
                _mInfo.mbActive = true;
                mSetValues();
                mUpdateButtons();
                mbCheckForm("Cleared");
            }
        }

        private void buttonDup_Click(object sender, EventArgs e)
        {
            if (_mInfo != null)
            {
                _mInfo.mIndex_KEY = 0;
                _mInfo.mbActive = true;
                mSetValues();
                mUpdateButtons();
                mbCheckForm("Duplicated");
            }
        }

        private void buttonAddUpdate_Click(object sender, EventArgs e)
        {
            UInt32 index;
            mGetValues(out index);

            // check values
            if (false == mbCheckForm(_mInfo.mIndex_KEY == 0 ? "Add" : "Update"))
            {
                return;
            }

            if (_mInfo.mIndex_KEY == 0)
            {
                // new Client

                // check if Laber is not in list

                if (_mInfo.mbDoSqlInsert())
                {
                    LabelPhysicianNr.Text = _mInfo.mIndex_KEY.ToString();
                    labelError.Text = "Inserted";
                }
                else
                {
                    labelError.Text = "Failed Insert";
                }
            }
            else if (index > 0 && _mInfo.mIndex_KEY == index)
            {
                // update client

                // check if label is in list (not this)
                if (_mInfo.mbDoSqlUpdate(_mInfo.mMaskValid, _mInfo.mbActive))
                {
                    LabelPhysicianNr.Text = "+" + _mInfo.mIndex_KEY.ToString();
                    labelError.Text = "Updated";
                }
                else
                {
                    labelError.Text = "Failed Update";
                }
            }
            mLoadList();
            mUpdateButtons();
        }

        bool mbCheckPhysician(out string AErrorText)
        {
            string errorText = "init form failed";

            if (_mInfo != null)
            {
                errorText = "";

                if (_mInfo._mLabel == null || _mInfo._mLabel.Length == 0)
                {
                    errorText = " No Name";
                }
            }
            AErrorText = errorText;

            return errorText.Length == 0;
        }



        private bool mbCheckForm(string AState)
        {
            bool bOk = true;
            string errorText = "";

            if (mbCheckPhysician(out errorText))
            {
                labelError.Text = "";
            }
            else
            {
                labelError.Text = AState + ": " + errorText;
                bOk = false;
            }
            return bOk;
        }


        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            UInt32 index;

            mGetValues(out index);

            if (_mInfo != null)
            {
                _mSearchInfo = _mInfo.mCreateCopy() as CPhysicianInfo;
                checkBoxShowSearchResult.Checked = _mSearchInfo != null;
                if (_mSearchInfo != null) _mSearchInfo.mIndex_KEY = index;

            }
            mUpdateList();
            mUpdateButtons();
            mbCheckForm("List");

        }
        public bool mbCopySelected(bool AbNewEntry)
        {
            bool bCopied = false;

            if (dataGridViewList.Rows != null && dataGridViewList.Rows.Count > 0)
            {
                if (dataGridViewList.SelectedRows != null && dataGridViewList.SelectedRows.Count > 0 && dataGridViewList.SelectedRows[0] != null)
                {
                    string indexStr = dataGridViewList.SelectedRows[0].Cells[0].Value == null ? ""
                        : dataGridViewList.SelectedRows[0].Cells[0].Value.ToString();
                    int index;

                    if (int.TryParse(indexStr, out index) && index > 0)
                    {
                        foreach (CPhysicianInfo row in _mList)
                        {
                            if (index == row.mIndex_KEY)
                            {
                                // found selected
                                CPhysicianInfo copy = row.mCreateCopy() as CPhysicianInfo;

                                if (copy != null)
                                {
                                    _mInfo = copy;
                                    bCopied = true;
                                    if (AbNewEntry)
                                    {
                                        _mInfo.mIndex_KEY = 0;
                                    }
                                    mSetValues();
                                    mUpdateButtons();
                                    mbCheckForm("Copy");
                                }
                                break;
                            }
                        }
                    }
                }
            }
            return bCopied;
        }

        private void buttonDuplicate_Click(object sender, EventArgs e)
        {
            mbCopySelected(true);
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            mbCopySelected(false);
        }

        private void dataGridViewList_DoubleClick(object sender, EventArgs e)
        {
            mbCopySelected(false);
        }

        private void buttonRandom_Click(object sender, EventArgs e)
        {
            if (_mInfo != null)
            {
                DateTime dt = DateTime.Now;
                string label = "Ph" + dt.ToString("yyyyMMddHHmmss");

                _mInfo.mClear();
                _mInfo.mbActive = true;
                _mInfo._mLabel = label;
                _mInfo._mLastName.mbEncrypt( "LName " + label );
                _mInfo._mMiddleName.mbEncrypt("MName " + label);
                _mInfo._mFirstName.mbEncrypt( "FName " + label );
                _mInfo._mDateOfBirth.mEncrypt((int)CProgram.sCalcYMD(dt));
                _mInfo._mGender_IX =(UInt16)( 1 + (dt.Second % 2 ));
                _mInfo._mAddress.mbEncrypt( "Adress " + label );
                _mInfo._mZipCode.mbEncrypt( label.Substring(0, 6) );
                _mInfo._mHouseNr.mbEncrypt( label.Substring(7, 4));
                _mInfo._mCity = "City " + label;
                _mInfo._mState = "State " + label;
                _mInfo._mCountryCode = "NL";


                //         _mInfo.ClientCountryList = comboBoxClientCountryList.Text;
                _mInfo._mPhone1.mbEncrypt( "1-" + label.Substring(4, 10));
                _mInfo._mPhone2.mbEncrypt( "2-" + label.Substring(4, 10));
                _mInfo._mCell.mbEncrypt( "C-" + label.Substring(4, 10));
                _mInfo._mEmail.mbEncrypt( label.Substring(5, 7) + "@" + label.Substring(0, 6) + ".com");
                _mInfo._mSkype.mbEncrypt( "S-" + label );

                mSetValues();

                mUpdateButtons();
                mbCheckForm("Random");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            mLoadList();
        }

        private void checkBoxShowDeactivated_CheckedChanged(object sender, EventArgs e)
        {
            mUpdateList();
        }

        private void checkBoxShowSearchResult_CheckedChanged(object sender, EventArgs e)
        {
            mUpdateList();
        }

        private void textBoxDateOfBirthDay_TextChanged(object sender, EventArgs e)
        {
            uint age = mGetDateValuesAge();

            textBoxAgeResult.Text = age == 0 ? "" : age.ToString();
        }

        private void comboBoxDateOfBirthMonthList_TextUpdate(object sender, EventArgs e)
        {
            uint age = mGetDateValuesAge();

            textBoxAgeResult.Text = age == 0 ? "" : age.ToString();

        }

        private void textBoxDateOfBirthYear_TextChanged(object sender, EventArgs e)
        {
            uint age = mGetDateValuesAge();

            textBoxAgeResult.Text = age == 0 ? "" : age.ToString();

        }

        private void buttonDisable_Click(object sender, EventArgs e)
        {
            if (mbCopySelected(false))
            {
                UInt32 index;
                mGetValues(out index);

                // check values

                if (_mInfo.mIndex_KEY == 0)
                {
                    labelError.Text = "Not stored";
                }
                else if (index > 0 && _mInfo.mIndex_KEY == index)
                {
                    // update client
                    bool b = _mInfo.mbActive;
                    _mInfo.mbActive = !b;
                    // check if label is in list (not this)
                    if (_mInfo.mbDoSqlUpdate(_mInfo.mMaskValid, _mInfo.mbActive))
                    {
                        LabelPhysicianNr.Text = "+" + _mInfo.mIndex_KEY.ToString();
                        labelError.Text = b ? "Disabled" : "Enabled";
                    }
                    else
                    {
                        labelError.Text = b ? "Failed to Disable" : "Failed to Enable";
                    }
                }
            }
            mSetValues();
            mLoadList();
            mUpdateButtons();
            mGetCursorSelected();
        }
        private UInt32 mGetCursorSelected()
        {
            UInt32 index = 0;

            try
            {
                if (dataGridViewList.SelectedRows != null && dataGridViewList.SelectedRows.Count > 0)
                {
                    string s = dataGridViewList.SelectedRows[0].Cells[0].Value == null ? ""
                        : dataGridViewList.SelectedRows[0].Cells[0].Value.ToString();

                    if (false == UInt32.TryParse(s, out index))
                    {
                        index = 0;
                    }
                }
                labelSelected.Text = index > 0 ? index.ToString() : "";
                _mSelectedIndex = index;
                buttonSelect.Enabled = _mSelectedIndex > 0;

            }
            catch (Exception ex)
            {
                CProgram.sLogException("PhysicianForm() get cursor failed", ex);
            }
            return index;
        }
        private bool mbLoadSelected()
        {
            bool bLoaded = false;
            try
            {
                _mSelectedIndex = mGetCursorSelected();

                if (_mSelectedIndex > 0)
                {
                    foreach (CPhysicianInfo row in _mList)
                    {
                        if (row.mIndex_KEY == _mSelectedIndex)
                        {
                            row.mbCopyTo(_mInfo);
                            bLoaded = true;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("PhysicianForm() load Selected cursor failed: " + _mSelectedIndex.ToString(), ex);
            }
            return bLoaded;
        }


        private void dataGridViewList_Click(object sender, EventArgs e)
        {
            mUpdateButtons();
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void labelUserLastName_Click(object sender, EventArgs e)
        {

        }

        private void textBoxLastName_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttonSelect_Click(object sender, EventArgs e)
        {
            if (mbLoadSelected())
            {
                _mbSelected = true;
                Close();
            }
        }

        private void dataGridViewList_SelectionChanged(object sender, EventArgs e)
        {
            mGetCursorSelected();
        }
    }
}
