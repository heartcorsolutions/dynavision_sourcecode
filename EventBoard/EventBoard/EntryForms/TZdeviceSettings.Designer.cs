﻿namespace TzDevice
{
    partial class TZmedicalDevSet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TZmedicalDevSet));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label89 = new System.Windows.Forms.Label();
            this.labelServerName = new System.Windows.Forms.Label();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.label40 = new System.Windows.Forms.Label();
            this.textBoxPatientID = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.labelDeviceSnr = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.checkBoxSaveToAllDevices = new System.Windows.Forms.CheckBox();
            this.label71 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.checkBoxAutoCollect = new System.Windows.Forms.CheckBox();
            this.label86 = new System.Windows.Forms.Label();
            this.labelLastMsgAction = new System.Windows.Forms.Label();
            this.labelInfoQueue = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.buttonClearAction = new System.Windows.Forms.Button();
            this.labelActionQueue = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.labelEstDelay = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.labelResult = new System.Windows.Forms.Label();
            this.buttonRefresh = new System.Windows.Forms.Button();
            this.labelActionFileInfo = new System.Windows.Forms.Label();
            this.labelSettingFileInfo = new System.Windows.Forms.Label();
            this.labelActionFileID = new System.Windows.Forms.Label();
            this.labelSettingFileID = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label76 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.textBoxStreamingDelay = new System.Windows.Forms.TextBox();
            this.label74 = new System.Windows.Forms.Label();
            this.textBoxStreamingMode = new System.Windows.Forms.TextBox();
            this.comboBoxFirstLow = new System.Windows.Forms.ComboBox();
            this.label72 = new System.Windows.Forms.Label();
            this.comboBoxDigHighFilter = new System.Windows.Forms.ComboBox();
            this.label69 = new System.Windows.Forms.Label();
            this.comboBoxNotchFilter = new System.Windows.Forms.ComboBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.textBoxStudyHours = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.LabelStudyDays = new System.Windows.Forms.Label();
            this.checkBoxAutoAnswer = new System.Windows.Forms.CheckBox();
            this.comboBoxTZSampleRate = new System.Windows.Forms.ComboBox();
            this.label35 = new System.Windows.Forms.Label();
            this.textBoxStudyLength = new System.Windows.Forms.TextBox();
            this.comboBoxLowFilter = new System.Windows.Forms.ComboBox();
            this.label32 = new System.Windows.Forms.Label();
            this.comboBoxLeadConfig = new System.Windows.Forms.ComboBox();
            this.label34 = new System.Windows.Forms.Label();
            this.checkBoxSyncTime = new System.Windows.Forms.CheckBox();
            this.checkBoxSilentMode = new System.Windows.Forms.CheckBox();
            this.checkBoxVibrOnClick = new System.Windows.Forms.CheckBox();
            this.checkBoxSMSenable = new System.Windows.Forms.CheckBox();
            this.textBoxAct10 = new System.Windows.Forms.TextBox();
            this.checkBoxVoiceEnable = new System.Windows.Forms.CheckBox();
            this.textBoxAct9 = new System.Windows.Forms.TextBox();
            this.textBoxAct8 = new System.Windows.Forms.TextBox();
            this.textBoxAct7 = new System.Windows.Forms.TextBox();
            this.textBoxSymp10 = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.textBoxSymp6 = new System.Windows.Forms.TextBox();
            this.textBoxSymp9 = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.textBoxSymp8 = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.textBoxSymp1 = new System.Windows.Forms.TextBox();
            this.textBoxSymp7 = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.textBoxSymp2 = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.textBoxSymp3 = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.textBoxAct6 = new System.Windows.Forms.TextBox();
            this.textBoxSymp4 = new System.Windows.Forms.TextBox();
            this.textBoxAct5 = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.textBoxAct4 = new System.Windows.Forms.TextBox();
            this.textBoxSymp5 = new System.Windows.Forms.TextBox();
            this.textBoxAct3 = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.textBoxAct2 = new System.Windows.Forms.TextBox();
            this.textBoxAct1 = new System.Windows.Forms.TextBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.checkBoxActDiaryOn = new System.Windows.Forms.CheckBox();
            this.label28 = new System.Windows.Forms.Label();
            this.checkBoxSymptDiaryOn = new System.Windows.Forms.CheckBox();
            this.label13 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.labelMessageInfo = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.textBoxEventPreTime = new System.Windows.Forms.TextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.textBoxEventPostTime = new System.Windows.Forms.TextBox();
            this.checkBoxUpdateFirmware = new System.Windows.Forms.CheckBox();
            this.checkBoxFormatSD = new System.Windows.Forms.CheckBox();
            this.checkBoxShutdown = new System.Windows.Forms.CheckBox();
            this.comboBoxCrossCh = new System.Windows.Forms.ComboBox();
            this.labelCrossChannelAnalysis = new System.Windows.Forms.Label();
            this.comboBoxAnalysisMask = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.checkBoxResetFileNumber = new System.Windows.Forms.CheckBox();
            this.label26 = new System.Windows.Forms.Label();
            this.buttonSendAction = new System.Windows.Forms.Button();
            this.checkBoxTransmitIntervalReports = new System.Windows.Forms.CheckBox();
            this.radioButtonPacemakerNo = new System.Windows.Forms.RadioButton();
            this.checkBoxReportChargingStart = new System.Windows.Forms.CheckBox();
            this.textBoxMessage = new System.Windows.Forms.TextBox();
            this.checkBoxReportChargingStop = new System.Windows.Forms.CheckBox();
            this.radioButtonPacemakerYes = new System.Windows.Forms.RadioButton();
            this.checkBoxReportBattLow = new System.Windows.Forms.CheckBox();
            this.checkBoxMessage = new System.Windows.Forms.CheckBox();
            this.checkBoxReportPatEvent = new System.Windows.Forms.CheckBox();
            this.checkBoxStartStudy = new System.Windows.Forms.CheckBox();
            this.checkBoxReportLeadDisconnected = new System.Windows.Forms.CheckBox();
            this.checkBoxEndStudy = new System.Windows.Forms.CheckBox();
            this.checkBoxReportPaceDetected = new System.Windows.Forms.CheckBox();
            this.checkBoxNewSetting = new System.Windows.Forms.CheckBox();
            this.checkBoxReportFull = new System.Windows.Forms.CheckBox();
            this.checkBoxNewStudy = new System.Windows.Forms.CheckBox();
            this.checkBoxReportResume = new System.Windows.Forms.CheckBox();
            this.checkBoxReportBreak = new System.Windows.Forms.CheckBox();
            this.checkBoxReportStop = new System.Windows.Forms.CheckBox();
            this.textBoxBradyOnset = new System.Windows.Forms.TextBox();
            this.checkBoxReportStart = new System.Windows.Forms.CheckBox();
            this.checkBoxReportBradyOnset = new System.Windows.Forms.CheckBox();
            this.label20 = new System.Windows.Forms.Label();
            this.checkBoxReportBradyOffset = new System.Windows.Forms.CheckBox();
            this.label21 = new System.Windows.Forms.Label();
            this.checkBoxReportTachyOffset = new System.Windows.Forms.CheckBox();
            this.label22 = new System.Windows.Forms.Label();
            this.checkBoxReportTachyOnset = new System.Windows.Forms.CheckBox();
            this.textBoxPauseDuration = new System.Windows.Forms.TextBox();
            this.checkBoxReportAFIBOffset = new System.Windows.Forms.CheckBox();
            this.textBoxPauseOffset = new System.Windows.Forms.TextBox();
            this.checkBoxReportAFIBOnset = new System.Windows.Forms.CheckBox();
            this.textBoxPauseOnset = new System.Windows.Forms.TextBox();
            this.checkBoxReportPause = new System.Windows.Forms.CheckBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxAFIBduration = new System.Windows.Forms.TextBox();
            this.textBoxAFIBoffset = new System.Windows.Forms.TextBox();
            this.textBoxBradyOffset = new System.Windows.Forms.TextBox();
            this.textBoxAFIBonset = new System.Windows.Forms.TextBox();
            this.textBoxBradyDuration = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.textBoxTachyDuration = new System.Windows.Forms.TextBox();
            this.textBoxTachyOnset = new System.Windows.Forms.TextBox();
            this.textBoxTachyOffset = new System.Windows.Forms.TextBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label70 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.labelServerSettings = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.labelLoadDefaults = new System.Windows.Forms.Label();
            this.labelLoadSettings = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.labelSaveAs = new System.Windows.Forms.Label();
            this.labelSaveToSdcard = new System.Windows.Forms.Label();
            this.labelSaveToServer = new System.Windows.Forms.Label();
            this.labelClose = new System.Windows.Forms.Label();
            this.labelLoadCurrent = new System.Windows.Forms.Label();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialogTZS = new System.Windows.Forms.OpenFileDialog();
            this.openFileDialogTxt = new System.Windows.Forms.OpenFileDialog();
            this.panelAutoCollect = new System.Windows.Forms.Panel();
            this.panel22 = new System.Windows.Forms.Panel();
            this.panel23 = new System.Windows.Forms.Panel();
            this.panel24 = new System.Windows.Forms.Panel();
            this.labelTzrLastScan = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.panel25 = new System.Windows.Forms.Panel();
            this.labelTzrSavedInfo = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.buttonTzrRefreshInfo = new System.Windows.Forms.Button();
            this.panel26 = new System.Windows.Forms.Panel();
            this.buttonTzrCollectTest = new System.Windows.Forms.Button();
            this.panel27 = new System.Windows.Forms.Panel();
            this.panel28 = new System.Windows.Forms.Panel();
            this.textBoxTzrStopNr = new System.Windows.Forms.TextBox();
            this.labelTzrStopLast = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.listBoxTzrStopMode = new System.Windows.Forms.ListBox();
            this.label98 = new System.Windows.Forms.Label();
            this.panel29 = new System.Windows.Forms.Panel();
            this.labelTzrStartFirst = new System.Windows.Forms.Label();
            this.textBoxTzrStartNr = new System.Windows.Forms.TextBox();
            this.label100 = new System.Windows.Forms.Label();
            this.listBoxTzrStartMode = new System.Windows.Forms.ListBox();
            this.label101 = new System.Windows.Forms.Label();
            this.panel30 = new System.Windows.Forms.Panel();
            this.label102 = new System.Windows.Forms.Label();
            this.labelTzrStudyNr = new System.Windows.Forms.Label();
            this.listBoxTzrStudyMode = new System.Windows.Forms.ListBox();
            this.checkBoxCollectTzrEnabled = new System.Windows.Forms.CheckBox();
            this.label104 = new System.Windows.Forms.Label();
            this.buttonTzrModeSave = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.labelScpLastScan = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.panel19 = new System.Windows.Forms.Panel();
            this.labelScpSavedInfo = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.buttonScpRefreshInfo = new System.Windows.Forms.Button();
            this.panel17 = new System.Windows.Forms.Panel();
            this.buttonScpCollectTest = new System.Windows.Forms.Button();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.textBoxScpStopNr = new System.Windows.Forms.TextBox();
            this.labelScpStopLast = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.listBoxScpStopMode = new System.Windows.Forms.ListBox();
            this.label85 = new System.Windows.Forms.Label();
            this.panel15 = new System.Windows.Forms.Panel();
            this.labelScpStartFirst = new System.Windows.Forms.Label();
            this.textBoxScpStartNr = new System.Windows.Forms.TextBox();
            this.label83 = new System.Windows.Forms.Label();
            this.listBoxScpStartMode = new System.Windows.Forms.ListBox();
            this.label82 = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.label80 = new System.Windows.Forms.Label();
            this.labelScpStudyNr = new System.Windows.Forms.Label();
            this.listBoxScpStudyMode = new System.Windows.Forms.ListBox();
            this.checkBoxCollectScpEnabled = new System.Windows.Forms.CheckBox();
            this.label79 = new System.Windows.Forms.Label();
            this.buttonScpModeSave = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label88 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.labelStudyActive = new System.Windows.Forms.Label();
            this.panel31 = new System.Windows.Forms.Panel();
            this.timerRefresh = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panelAutoCollect.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel28.SuspendLayout();
            this.panel29.SuspendLayout();
            this.panel30.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label89);
            this.panel1.Controls.Add(this.labelServerName);
            this.panel1.Controls.Add(this.checkBox2);
            this.panel1.Controls.Add(this.label40);
            this.panel1.Controls.Add(this.textBoxPatientID);
            this.panel1.Controls.Add(this.label42);
            this.panel1.Controls.Add(this.labelDeviceSnr);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1196, 31);
            this.panel1.TabIndex = 0;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("Arial", 10F);
            this.label89.ForeColor = System.Drawing.Color.DarkBlue;
            this.label89.Location = new System.Drawing.Point(686, 9);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(288, 16);
            this.label89.TabIndex = 193;
            this.label89.Text = "Blue = TZ Aera only (Not used on TZ Clarus)";
            this.label89.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelServerName
            // 
            this.labelServerName.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelServerName.Font = new System.Drawing.Font("Arial", 10F);
            this.labelServerName.Location = new System.Drawing.Point(824, 0);
            this.labelServerName.Name = "labelServerName";
            this.labelServerName.Size = new System.Drawing.Size(372, 31);
            this.labelServerName.TabIndex = 192;
            this.labelServerName.Text = "^ServerName";
            this.labelServerName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Checked = true;
            this.checkBox2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox2.Enabled = false;
            this.checkBox2.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBox2.Location = new System.Drawing.Point(499, 9);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(179, 20);
            this.checkBox2.TabIndex = 191;
            this.checkBox2.Text = "TZ Aera compatible only";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Arial", 10F);
            this.label40.Location = new System.Drawing.Point(5, 8);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(73, 16);
            this.label40.TabIndex = 153;
            this.label40.Text = "Patient ID:";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxPatientID
            // 
            this.textBoxPatientID.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxPatientID.Location = new System.Drawing.Point(85, 4);
            this.textBoxPatientID.Name = "textBoxPatientID";
            this.textBoxPatientID.Size = new System.Drawing.Size(138, 23);
            this.textBoxPatientID.TabIndex = 154;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Arial", 10F);
            this.label42.Location = new System.Drawing.Point(248, 8);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(34, 16);
            this.label42.TabIndex = 167;
            this.label42.Text = "Snr:";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelDeviceSnr
            // 
            this.labelDeviceSnr.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelDeviceSnr.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelDeviceSnr.Location = new System.Drawing.Point(289, 5);
            this.labelDeviceSnr.Name = "labelDeviceSnr";
            this.labelDeviceSnr.Size = new System.Drawing.Size(115, 22);
            this.labelDeviceSnr.TabIndex = 186;
            this.labelDeviceSnr.Text = "H3R12345678";
            this.labelDeviceSnr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelDeviceSnr.DoubleClick += new System.EventHandler(this.labelDeviceSnr_DoubleClick);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel12);
            this.panel2.Controls.Add(this.panel11);
            this.panel2.Controls.Add(this.panel10);
            this.panel2.Controls.Add(this.panel9);
            this.panel2.Controls.Add(this.panel8);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 31);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1196, 596);
            this.panel2.TabIndex = 1;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.checkBoxSaveToAllDevices);
            this.panel5.Controls.Add(this.label71);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 569);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1196, 23);
            this.panel5.TabIndex = 184;
            // 
            // checkBoxSaveToAllDevices
            // 
            this.checkBoxSaveToAllDevices.AutoSize = true;
            this.checkBoxSaveToAllDevices.Dock = System.Windows.Forms.DockStyle.Right;
            this.checkBoxSaveToAllDevices.Location = new System.Drawing.Point(959, 0);
            this.checkBoxSaveToAllDevices.Name = "checkBoxSaveToAllDevices";
            this.checkBoxSaveToAllDevices.Size = new System.Drawing.Size(237, 23);
            this.checkBoxSaveToAllDevices.TabIndex = 185;
            this.checkBoxSaveToAllDevices.Text = "Save to Server to All Devices (Experimental) ";
            this.checkBoxSaveToAllDevices.UseVisualStyleBackColor = true;
            // 
            // label71
            // 
            this.label71.Dock = System.Windows.Forms.DockStyle.Left;
            this.label71.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label71.ForeColor = System.Drawing.Color.Black;
            this.label71.Location = new System.Drawing.Point(0, 0);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(730, 23);
            this.label71.TabIndex = 184;
            this.label71.Text = "Note: settings are stored on the server from where the device collects it - delay" +
    "s may occur !";
            this.label71.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.checkBoxAutoCollect);
            this.panel4.Controls.Add(this.label86);
            this.panel4.Controls.Add(this.labelLastMsgAction);
            this.panel4.Controls.Add(this.labelInfoQueue);
            this.panel4.Controls.Add(this.label78);
            this.panel4.Controls.Add(this.label77);
            this.panel4.Controls.Add(this.label63);
            this.panel4.Controls.Add(this.label43);
            this.panel4.Controls.Add(this.buttonClearAction);
            this.panel4.Controls.Add(this.labelActionQueue);
            this.panel4.Controls.Add(this.label39);
            this.panel4.Controls.Add(this.label38);
            this.panel4.Controls.Add(this.label27);
            this.panel4.Controls.Add(this.label37);
            this.panel4.Controls.Add(this.labelEstDelay);
            this.panel4.Controls.Add(this.label24);
            this.panel4.Controls.Add(this.labelResult);
            this.panel4.Controls.Add(this.buttonRefresh);
            this.panel4.Controls.Add(this.labelActionFileInfo);
            this.panel4.Controls.Add(this.labelSettingFileInfo);
            this.panel4.Controls.Add(this.labelActionFileID);
            this.panel4.Controls.Add(this.labelSettingFileID);
            this.panel4.Controls.Add(this.label36);
            this.panel4.Controls.Add(this.label33);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 426);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1196, 143);
            this.panel4.TabIndex = 73;
            // 
            // checkBoxAutoCollect
            // 
            this.checkBoxAutoCollect.AutoSize = true;
            this.checkBoxAutoCollect.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxAutoCollect.Location = new System.Drawing.Point(394, 69);
            this.checkBoxAutoCollect.Name = "checkBoxAutoCollect";
            this.checkBoxAutoCollect.Size = new System.Drawing.Size(135, 20);
            this.checkBoxAutoCollect.TabIndex = 231;
            this.checkBoxAutoCollect.Text = "View Auto Collect";
            this.checkBoxAutoCollect.UseVisualStyleBackColor = true;
            this.checkBoxAutoCollect.CheckedChanged += new System.EventHandler(this.checkBoxAutoCollect_CheckedChanged);
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Arial", 10F);
            this.label86.Location = new System.Drawing.Point(7, 45);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(75, 16);
            this.label86.TabIndex = 230;
            this.label86.Text = "Last done:";
            // 
            // labelLastMsgAction
            // 
            this.labelLastMsgAction.AutoSize = true;
            this.labelLastMsgAction.Font = new System.Drawing.Font("Arial", 10F);
            this.labelLastMsgAction.Location = new System.Drawing.Point(86, 45);
            this.labelLastMsgAction.Name = "labelLastMsgAction";
            this.labelLastMsgAction.Size = new System.Drawing.Size(581, 16);
            this.labelLastMsgAction.TabIndex = 229;
            this.labelLastMsgAction.Text = "Action ^^^^  20190918123445, Msg 9999 20190918123445, Setting 9999 20190918123445" +
    "";
            // 
            // labelInfoQueue
            // 
            this.labelInfoQueue.AutoSize = true;
            this.labelInfoQueue.Font = new System.Drawing.Font("Arial", 10F);
            this.labelInfoQueue.Location = new System.Drawing.Point(784, 86);
            this.labelInfoQueue.Name = "labelInfoQueue";
            this.labelInfoQueue.Size = new System.Drawing.Size(363, 16);
            this.labelInfoQueue.TabIndex = 228;
            this.labelInfoQueue.Text = "Send new setting action and wait for Queue to be empty.";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Arial", 10F);
            this.label78.Location = new System.Drawing.Point(784, 64);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(354, 16);
            this.label78.TabIndex = 227;
            this.label78.Text = "Open Study \\ Recorder folder to see last received files.";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Arial", 10F);
            this.label77.Location = new System.Drawing.Point(797, 85);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(0, 16);
            this.label77.TabIndex = 226;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.Location = new System.Drawing.Point(637, 64);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(115, 16);
            this.label63.TabIndex = 225;
            this.label63.Text = "Check activity by: ";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(784, 107);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(292, 16);
            this.label43.TabIndex = 224;
            this.label43.Text = "For Clarus use a scp block size of 30 seconds";
            // 
            // buttonClearAction
            // 
            this.buttonClearAction.BackColor = System.Drawing.Color.MediumPurple;
            this.buttonClearAction.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonClearAction.ForeColor = System.Drawing.Color.White;
            this.buttonClearAction.Location = new System.Drawing.Point(181, 64);
            this.buttonClearAction.Name = "buttonClearAction";
            this.buttonClearAction.Size = new System.Drawing.Size(193, 28);
            this.buttonClearAction.TabIndex = 223;
            this.buttonClearAction.Text = "Clear Queue";
            this.buttonClearAction.UseVisualStyleBackColor = false;
            this.buttonClearAction.Click += new System.EventHandler(this.buttonClearAction_Click);
            // 
            // labelActionQueue
            // 
            this.labelActionQueue.AutoSize = true;
            this.labelActionQueue.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelActionQueue.Location = new System.Drawing.Point(784, 123);
            this.labelActionQueue.Name = "labelActionQueue";
            this.labelActionQueue.Size = new System.Drawing.Size(317, 16);
            this.labelActionQueue.TabIndex = 222;
            this.labelActionQueue.Text = "Only one Setting + Action is stored on the server.";
            this.labelActionQueue.DoubleClick += new System.EventHandler(this.labelActionQueue_DoubleClick);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Arial", 10F);
            this.label39.Location = new System.Drawing.Point(784, 25);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(375, 16);
            this.label39.TabIndex = 221;
            this.label39.Text = "Use Send Action to receive message on the device display";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(636, 25);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(143, 16);
            this.label38.TabIndex = 220;
            this.label38.Text = "During setup in center: ";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Arial", 10F);
            this.label27.Location = new System.Drawing.Point(784, 45);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(370, 16);
            this.label27.TabIndex = 219;
            this.label27.Text = "to test communication between the device and the server.";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Arial", 10F);
            this.label37.Location = new System.Drawing.Point(826, 6);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(338, 16);
            this.label37.TabIndex = 218;
            this.label37.Text = "minutes (Clarus 60 min, delay may be up to 2 hours)";
            // 
            // labelEstDelay
            // 
            this.labelEstDelay.AutoSize = true;
            this.labelEstDelay.Font = new System.Drawing.Font("Arial", 10F);
            this.labelEstDelay.Location = new System.Drawing.Point(784, 6);
            this.labelEstDelay.Name = "labelEstDelay";
            this.labelEstDelay.Size = new System.Drawing.Size(37, 16);
            this.labelEstDelay.TabIndex = 217;
            this.labelEstDelay.Text = "^123";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(636, 6);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(146, 16);
            this.label24.TabIndex = 216;
            this.label24.Text = "Estimated Action delay:";
            // 
            // labelResult
            // 
            this.labelResult.BackColor = System.Drawing.Color.Green;
            this.labelResult.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelResult.ForeColor = System.Drawing.Color.White;
            this.labelResult.Location = new System.Drawing.Point(10, 92);
            this.labelResult.Name = "labelResult";
            this.labelResult.Size = new System.Drawing.Size(764, 48);
            this.labelResult.TabIndex = 215;
            this.labelResult.Text = "Result of the action\r\ngdsja\r\n";
            this.labelResult.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.BackColor = System.Drawing.Color.MediumPurple;
            this.buttonRefresh.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonRefresh.ForeColor = System.Drawing.Color.White;
            this.buttonRefresh.Location = new System.Drawing.Point(9, 64);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(155, 28);
            this.buttonRefresh.TabIndex = 214;
            this.buttonRefresh.Text = "Refresh File info";
            this.buttonRefresh.UseVisualStyleBackColor = false;
            this.buttonRefresh.Click += new System.EventHandler(this.buttonRefresh_Click);
            // 
            // labelActionFileInfo
            // 
            this.labelActionFileInfo.AutoSize = true;
            this.labelActionFileInfo.Font = new System.Drawing.Font("Arial", 10F);
            this.labelActionFileInfo.Location = new System.Drawing.Point(179, 24);
            this.labelActionFileInfo.Name = "labelActionFileInfo";
            this.labelActionFileInfo.Size = new System.Drawing.Size(211, 16);
            this.labelActionFileInfo.TabIndex = 213;
            this.labelActionFileInfo.Text = "^9999/12/22 12:15:24 uploading";
            // 
            // labelSettingFileInfo
            // 
            this.labelSettingFileInfo.AutoSize = true;
            this.labelSettingFileInfo.Font = new System.Drawing.Font("Arial", 10F);
            this.labelSettingFileInfo.Location = new System.Drawing.Point(178, 3);
            this.labelSettingFileInfo.Name = "labelSettingFileInfo";
            this.labelSettingFileInfo.Size = new System.Drawing.Size(211, 16);
            this.labelSettingFileInfo.TabIndex = 212;
            this.labelSettingFileInfo.Text = "^9999/12/22 12:15:24 uploading";
            // 
            // labelActionFileID
            // 
            this.labelActionFileID.AutoSize = true;
            this.labelActionFileID.Font = new System.Drawing.Font("Arial", 10F);
            this.labelActionFileID.Location = new System.Drawing.Point(120, 24);
            this.labelActionFileID.Name = "labelActionFileID";
            this.labelActionFileID.Size = new System.Drawing.Size(53, 16);
            this.labelActionFileID.TabIndex = 211;
            this.labelActionFileID.Text = "^12345";
            // 
            // labelSettingFileID
            // 
            this.labelSettingFileID.AutoSize = true;
            this.labelSettingFileID.Font = new System.Drawing.Font("Arial", 10F);
            this.labelSettingFileID.Location = new System.Drawing.Point(119, 3);
            this.labelSettingFileID.Name = "labelSettingFileID";
            this.labelSettingFileID.Size = new System.Drawing.Size(53, 16);
            this.labelSettingFileID.TabIndex = 210;
            this.labelSettingFileID.Text = "^12345";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Arial", 10F);
            this.label36.Location = new System.Drawing.Point(7, 24);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(103, 16);
            this.label36.TabIndex = 209;
            this.label36.Text = "Last Action file:";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Arial", 10F);
            this.label33.Location = new System.Drawing.Point(7, 3);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(109, 16);
            this.label33.TabIndex = 208;
            this.label33.Text = "Last Setting file:";
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.MediumPurple;
            this.panel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel12.Location = new System.Drawing.Point(0, 416);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(1196, 10);
            this.panel12.TabIndex = 72;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.label76);
            this.panel11.Controls.Add(this.label75);
            this.panel11.Controls.Add(this.label73);
            this.panel11.Controls.Add(this.textBoxStreamingDelay);
            this.panel11.Controls.Add(this.label74);
            this.panel11.Controls.Add(this.textBoxStreamingMode);
            this.panel11.Controls.Add(this.comboBoxFirstLow);
            this.panel11.Controls.Add(this.label72);
            this.panel11.Controls.Add(this.comboBoxDigHighFilter);
            this.panel11.Controls.Add(this.label69);
            this.panel11.Controls.Add(this.comboBoxNotchFilter);
            this.panel11.Controls.Add(this.label41);
            this.panel11.Controls.Add(this.label68);
            this.panel11.Controls.Add(this.textBoxStudyHours);
            this.panel11.Controls.Add(this.label49);
            this.panel11.Controls.Add(this.label50);
            this.panel11.Controls.Add(this.label51);
            this.panel11.Controls.Add(this.label60);
            this.panel11.Controls.Add(this.label61);
            this.panel11.Controls.Add(this.label44);
            this.panel11.Controls.Add(this.label45);
            this.panel11.Controls.Add(this.label46);
            this.panel11.Controls.Add(this.label47);
            this.panel11.Controls.Add(this.label48);
            this.panel11.Controls.Add(this.label30);
            this.panel11.Controls.Add(this.LabelStudyDays);
            this.panel11.Controls.Add(this.checkBoxAutoAnswer);
            this.panel11.Controls.Add(this.comboBoxTZSampleRate);
            this.panel11.Controls.Add(this.label35);
            this.panel11.Controls.Add(this.textBoxStudyLength);
            this.panel11.Controls.Add(this.comboBoxLowFilter);
            this.panel11.Controls.Add(this.label32);
            this.panel11.Controls.Add(this.comboBoxLeadConfig);
            this.panel11.Controls.Add(this.label34);
            this.panel11.Controls.Add(this.checkBoxSyncTime);
            this.panel11.Controls.Add(this.checkBoxSilentMode);
            this.panel11.Controls.Add(this.checkBoxVibrOnClick);
            this.panel11.Controls.Add(this.checkBoxSMSenable);
            this.panel11.Controls.Add(this.textBoxAct10);
            this.panel11.Controls.Add(this.checkBoxVoiceEnable);
            this.panel11.Controls.Add(this.textBoxAct9);
            this.panel11.Controls.Add(this.textBoxAct8);
            this.panel11.Controls.Add(this.textBoxAct7);
            this.panel11.Controls.Add(this.textBoxSymp10);
            this.panel11.Controls.Add(this.label58);
            this.panel11.Controls.Add(this.textBoxSymp6);
            this.panel11.Controls.Add(this.textBoxSymp9);
            this.panel11.Controls.Add(this.label59);
            this.panel11.Controls.Add(this.label29);
            this.panel11.Controls.Add(this.textBoxSymp8);
            this.panel11.Controls.Add(this.label54);
            this.panel11.Controls.Add(this.textBoxSymp1);
            this.panel11.Controls.Add(this.textBoxSymp7);
            this.panel11.Controls.Add(this.label55);
            this.panel11.Controls.Add(this.label31);
            this.panel11.Controls.Add(this.textBoxSymp2);
            this.panel11.Controls.Add(this.label53);
            this.panel11.Controls.Add(this.textBoxSymp3);
            this.panel11.Controls.Add(this.label52);
            this.panel11.Controls.Add(this.textBoxAct6);
            this.panel11.Controls.Add(this.textBoxSymp4);
            this.panel11.Controls.Add(this.textBoxAct5);
            this.panel11.Controls.Add(this.label57);
            this.panel11.Controls.Add(this.textBoxAct4);
            this.panel11.Controls.Add(this.textBoxSymp5);
            this.panel11.Controls.Add(this.textBoxAct3);
            this.panel11.Controls.Add(this.label56);
            this.panel11.Controls.Add(this.textBoxAct2);
            this.panel11.Controls.Add(this.textBoxAct1);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel11.Location = new System.Drawing.Point(0, 240);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(1196, 176);
            this.panel11.TabIndex = 71;
            this.panel11.Paint += new System.Windows.Forms.PaintEventHandler(this.panel11_Paint);
            // 
            // label76
            // 
            this.label76.Font = new System.Drawing.Font("Arial", 10F);
            this.label76.ForeColor = System.Drawing.Color.DarkBlue;
            this.label76.Location = new System.Drawing.Point(398, 143);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(104, 42);
            this.label76.TabIndex = 249;
            this.label76.Text = "Scp files, \r\nstream use 0";
            // 
            // label75
            // 
            this.label75.Font = new System.Drawing.Font("Arial", 10F);
            this.label75.Location = new System.Drawing.Point(397, 95);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(100, 48);
            this.label75.TabIndex = 248;
            this.label75.Text = "Scp files, \r\n0 = off, \r\n1 = stream";
            // 
            // label73
            // 
            this.label73.Font = new System.Drawing.Font("Arial", 10F);
            this.label73.ForeColor = System.Drawing.Color.DarkBlue;
            this.label73.Location = new System.Drawing.Point(204, 146);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(142, 23);
            this.label73.TabIndex = 246;
            this.label73.Text = "Backlog Skip after:";
            // 
            // textBoxStreamingDelay
            // 
            this.textBoxStreamingDelay.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxStreamingDelay.Location = new System.Drawing.Point(352, 146);
            this.textBoxStreamingDelay.Name = "textBoxStreamingDelay";
            this.textBoxStreamingDelay.Size = new System.Drawing.Size(37, 23);
            this.textBoxStreamingDelay.TabIndex = 247;
            this.textBoxStreamingDelay.Text = "^99";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Arial", 10F);
            this.label74.Location = new System.Drawing.Point(203, 96);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(146, 16);
            this.label74.TabIndex = 244;
            this.label74.Text = "Auto bulk upload size:";
            // 
            // textBoxStreamingMode
            // 
            this.textBoxStreamingMode.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxStreamingMode.Location = new System.Drawing.Point(352, 95);
            this.textBoxStreamingMode.Name = "textBoxStreamingMode";
            this.textBoxStreamingMode.Size = new System.Drawing.Size(37, 23);
            this.textBoxStreamingMode.TabIndex = 245;
            this.textBoxStreamingMode.Text = "^99";
            // 
            // comboBoxFirstLow
            // 
            this.comboBoxFirstLow.Font = new System.Drawing.Font("Arial", 10F);
            this.comboBoxFirstLow.FormattingEnabled = true;
            this.comboBoxFirstLow.Items.AddRange(new object[] {
            "40",
            "60",
            "100"});
            this.comboBoxFirstLow.Location = new System.Drawing.Point(100, 145);
            this.comboBoxFirstLow.Name = "comboBoxFirstLow";
            this.comboBoxFirstLow.Size = new System.Drawing.Size(100, 24);
            this.comboBoxFirstLow.TabIndex = 242;
            this.comboBoxFirstLow.Text = "^99";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Arial", 10F);
            this.label72.Location = new System.Drawing.Point(6, 148);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(60, 16);
            this.label72.TabIndex = 241;
            this.label72.Text = "LP filter:";
            // 
            // comboBoxDigHighFilter
            // 
            this.comboBoxDigHighFilter.Font = new System.Drawing.Font("Arial", 10F);
            this.comboBoxDigHighFilter.FormattingEnabled = true;
            this.comboBoxDigHighFilter.Items.AddRange(new object[] {
            "0",
            "0.05",
            "0.1",
            "0.5",
            "1.0"});
            this.comboBoxDigHighFilter.Location = new System.Drawing.Point(100, 64);
            this.comboBoxDigHighFilter.Name = "comboBoxDigHighFilter";
            this.comboBoxDigHighFilter.Size = new System.Drawing.Size(100, 24);
            this.comboBoxDigHighFilter.TabIndex = 240;
            this.comboBoxDigHighFilter.Text = "^99";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Arial", 10F);
            this.label69.Location = new System.Drawing.Point(6, 68);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(79, 16);
            this.label69.TabIndex = 239;
            this.label69.Text = "D. HP filter:";
            // 
            // comboBoxNotchFilter
            // 
            this.comboBoxNotchFilter.Font = new System.Drawing.Font("Arial", 10F);
            this.comboBoxNotchFilter.FormattingEnabled = true;
            this.comboBoxNotchFilter.Items.AddRange(new object[] {
            "0",
            "50",
            "60"});
            this.comboBoxNotchFilter.Location = new System.Drawing.Point(100, 92);
            this.comboBoxNotchFilter.Name = "comboBoxNotchFilter";
            this.comboBoxNotchFilter.Size = new System.Drawing.Size(100, 24);
            this.comboBoxNotchFilter.TabIndex = 238;
            this.comboBoxNotchFilter.Text = "^99";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Arial", 10F);
            this.label41.Location = new System.Drawing.Point(6, 96);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(97, 16);
            this.label41.TabIndex = 237;
            this.label41.Text = "D. Notch filter:";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Enabled = false;
            this.label68.Font = new System.Drawing.Font("Arial", 10F);
            this.label68.Location = new System.Drawing.Point(993, 150);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(84, 16);
            this.label68.TabIndex = 235;
            this.label68.Text = "Study hours";
            this.label68.Visible = false;
            // 
            // textBoxStudyHours
            // 
            this.textBoxStudyHours.Enabled = false;
            this.textBoxStudyHours.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxStudyHours.Location = new System.Drawing.Point(1087, 147);
            this.textBoxStudyHours.Name = "textBoxStudyHours";
            this.textBoxStudyHours.Size = new System.Drawing.Size(85, 23);
            this.textBoxStudyHours.TabIndex = 236;
            this.textBoxStudyHours.Text = "Niet actief";
            this.textBoxStudyHours.Visible = false;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Arial", 10F);
            this.label49.Location = new System.Drawing.Point(1001, 118);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(28, 16);
            this.label49.TabIndex = 234;
            this.label49.Text = "10:";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Arial", 10F);
            this.label50.Location = new System.Drawing.Point(1001, 91);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(20, 16);
            this.label50.TabIndex = 233;
            this.label50.Text = "9:";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Arial", 10F);
            this.label51.Location = new System.Drawing.Point(1001, 65);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(20, 16);
            this.label51.TabIndex = 232;
            this.label51.Text = "8:";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Arial", 10F);
            this.label60.Location = new System.Drawing.Point(1001, 38);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(20, 16);
            this.label60.TabIndex = 231;
            this.label60.Text = "7:";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Arial", 10F);
            this.label61.Location = new System.Drawing.Point(1001, 11);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(20, 16);
            this.label61.TabIndex = 230;
            this.label61.Text = "6:";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Arial", 10F);
            this.label44.Location = new System.Drawing.Point(840, 11);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(20, 16);
            this.label44.TabIndex = 225;
            this.label44.Text = "1:";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Arial", 10F);
            this.label45.Location = new System.Drawing.Point(840, 38);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(20, 16);
            this.label45.TabIndex = 226;
            this.label45.Text = "2:";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Arial", 10F);
            this.label46.Location = new System.Drawing.Point(840, 65);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(20, 16);
            this.label46.TabIndex = 227;
            this.label46.Text = "3:";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Arial", 10F);
            this.label47.Location = new System.Drawing.Point(840, 91);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(20, 16);
            this.label47.TabIndex = 228;
            this.label47.Text = "4:";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Arial", 10F);
            this.label48.Location = new System.Drawing.Point(840, 118);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(20, 16);
            this.label48.TabIndex = 229;
            this.label48.Text = "5:";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Arial", 10F);
            this.label30.ForeColor = System.Drawing.Color.DarkBlue;
            this.label30.Location = new System.Drawing.Point(6, 38);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(88, 16);
            this.label30.TabIndex = 125;
            this.label30.Text = "Sample rate:";
            // 
            // LabelStudyDays
            // 
            this.LabelStudyDays.AutoSize = true;
            this.LabelStudyDays.Font = new System.Drawing.Font("Arial", 10F);
            this.LabelStudyDays.Location = new System.Drawing.Point(704, 149);
            this.LabelStudyDays.Name = "LabelStudyDays";
            this.LabelStudyDays.Size = new System.Drawing.Size(134, 16);
            this.LabelStudyDays.TabIndex = 223;
            this.LabelStudyDays.Text = "^^*30sec= 11,3 days";
            this.LabelStudyDays.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // checkBoxAutoAnswer
            // 
            this.checkBoxAutoAnswer.AutoSize = true;
            this.checkBoxAutoAnswer.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxAutoAnswer.ForeColor = System.Drawing.Color.DarkBlue;
            this.checkBoxAutoAnswer.Location = new System.Drawing.Point(207, 11);
            this.checkBoxAutoAnswer.Name = "checkBoxAutoAnswer";
            this.checkBoxAutoAnswer.Size = new System.Drawing.Size(105, 20);
            this.checkBoxAutoAnswer.TabIndex = 138;
            this.checkBoxAutoAnswer.Text = "Auto answer";
            this.checkBoxAutoAnswer.UseVisualStyleBackColor = true;
            // 
            // comboBoxTZSampleRate
            // 
            this.comboBoxTZSampleRate.Font = new System.Drawing.Font("Arial", 10F);
            this.comboBoxTZSampleRate.FormattingEnabled = true;
            this.comboBoxTZSampleRate.Items.AddRange(new object[] {
            "128",
            "256"});
            this.comboBoxTZSampleRate.Location = new System.Drawing.Point(100, 34);
            this.comboBoxTZSampleRate.Name = "comboBoxTZSampleRate";
            this.comboBoxTZSampleRate.Size = new System.Drawing.Size(100, 24);
            this.comboBoxTZSampleRate.TabIndex = 126;
            this.comboBoxTZSampleRate.Text = "^99";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Arial", 10F);
            this.label35.Location = new System.Drawing.Point(529, 148);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(110, 16);
            this.label35.TabIndex = 135;
            this.label35.Text = "Max. Study files:";
            // 
            // textBoxStudyLength
            // 
            this.textBoxStudyLength.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxStudyLength.Location = new System.Drawing.Point(643, 147);
            this.textBoxStudyLength.Name = "textBoxStudyLength";
            this.textBoxStudyLength.Size = new System.Drawing.Size(55, 23);
            this.textBoxStudyLength.TabIndex = 136;
            this.textBoxStudyLength.Text = "^99999";
            // 
            // comboBoxLowFilter
            // 
            this.comboBoxLowFilter.Font = new System.Drawing.Font("Arial", 10F);
            this.comboBoxLowFilter.FormattingEnabled = true;
            this.comboBoxLowFilter.Items.AddRange(new object[] {
            "0",
            "40",
            "50",
            "60",
            "80",
            "100"});
            this.comboBoxLowFilter.Location = new System.Drawing.Point(100, 118);
            this.comboBoxLowFilter.Name = "comboBoxLowFilter";
            this.comboBoxLowFilter.Size = new System.Drawing.Size(100, 24);
            this.comboBoxLowFilter.TabIndex = 138;
            this.comboBoxLowFilter.Text = "^99";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Arial", 10F);
            this.label32.ForeColor = System.Drawing.Color.DarkBlue;
            this.label32.Location = new System.Drawing.Point(6, 122);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(78, 16);
            this.label32.TabIndex = 137;
            this.label32.Text = "D. LP filter:";
            // 
            // comboBoxLeadConfig
            // 
            this.comboBoxLeadConfig.Font = new System.Drawing.Font("Arial", 10F);
            this.comboBoxLeadConfig.FormattingEnabled = true;
            this.comboBoxLeadConfig.Items.AddRange(new object[] {
            "Custom",
            "EASIG",
            "5 electrode",
            "3 electrode",
            "4 electrode",
            "II and V5"});
            this.comboBoxLeadConfig.Location = new System.Drawing.Point(100, 7);
            this.comboBoxLeadConfig.Name = "comboBoxLeadConfig";
            this.comboBoxLeadConfig.Size = new System.Drawing.Size(100, 24);
            this.comboBoxLeadConfig.TabIndex = 140;
            this.comboBoxLeadConfig.Text = "^5 electrode";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Arial", 10F);
            this.label34.ForeColor = System.Drawing.Color.DarkBlue;
            this.label34.Location = new System.Drawing.Point(6, 10);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(86, 16);
            this.label34.TabIndex = 139;
            this.label34.Text = "Lead config:";
            // 
            // checkBoxSyncTime
            // 
            this.checkBoxSyncTime.AutoSize = true;
            this.checkBoxSyncTime.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxSyncTime.ForeColor = System.Drawing.Color.DarkBlue;
            this.checkBoxSyncTime.Location = new System.Drawing.Point(348, 11);
            this.checkBoxSyncTime.Name = "checkBoxSyncTime";
            this.checkBoxSyncTime.Size = new System.Drawing.Size(88, 20);
            this.checkBoxSyncTime.TabIndex = 145;
            this.checkBoxSyncTime.Text = "Sync time";
            this.checkBoxSyncTime.UseVisualStyleBackColor = true;
            // 
            // checkBoxSilentMode
            // 
            this.checkBoxSilentMode.AutoSize = true;
            this.checkBoxSilentMode.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxSilentMode.ForeColor = System.Drawing.Color.DarkBlue;
            this.checkBoxSilentMode.Location = new System.Drawing.Point(207, 38);
            this.checkBoxSilentMode.Name = "checkBoxSilentMode";
            this.checkBoxSilentMode.Size = new System.Drawing.Size(101, 20);
            this.checkBoxSilentMode.TabIndex = 147;
            this.checkBoxSilentMode.Text = "Silent mode";
            this.checkBoxSilentMode.UseVisualStyleBackColor = true;
            // 
            // checkBoxVibrOnClick
            // 
            this.checkBoxVibrOnClick.AutoSize = true;
            this.checkBoxVibrOnClick.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxVibrOnClick.ForeColor = System.Drawing.Color.DarkBlue;
            this.checkBoxVibrOnClick.Location = new System.Drawing.Point(207, 65);
            this.checkBoxVibrOnClick.Name = "checkBoxVibrOnClick";
            this.checkBoxVibrOnClick.Size = new System.Drawing.Size(130, 20);
            this.checkBoxVibrOnClick.TabIndex = 148;
            this.checkBoxVibrOnClick.Text = "Vibrate on clicks";
            this.checkBoxVibrOnClick.UseVisualStyleBackColor = true;
            // 
            // checkBoxSMSenable
            // 
            this.checkBoxSMSenable.AutoSize = true;
            this.checkBoxSMSenable.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxSMSenable.ForeColor = System.Drawing.Color.DarkBlue;
            this.checkBoxSMSenable.Location = new System.Drawing.Point(348, 38);
            this.checkBoxSMSenable.Name = "checkBoxSMSenable";
            this.checkBoxSMSenable.Size = new System.Drawing.Size(103, 20);
            this.checkBoxSMSenable.TabIndex = 152;
            this.checkBoxSMSenable.Text = "SMS enable";
            this.checkBoxSMSenable.UseVisualStyleBackColor = true;
            // 
            // textBoxAct10
            // 
            this.textBoxAct10.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxAct10.Location = new System.Drawing.Point(1031, 115);
            this.textBoxAct10.Name = "textBoxAct10";
            this.textBoxAct10.Size = new System.Drawing.Size(130, 23);
            this.textBoxAct10.TabIndex = 197;
            this.textBoxAct10.Text = "^Diary Activity";
            // 
            // checkBoxVoiceEnable
            // 
            this.checkBoxVoiceEnable.AutoSize = true;
            this.checkBoxVoiceEnable.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxVoiceEnable.ForeColor = System.Drawing.Color.DarkBlue;
            this.checkBoxVoiceEnable.Location = new System.Drawing.Point(348, 65);
            this.checkBoxVoiceEnable.Name = "checkBoxVoiceEnable";
            this.checkBoxVoiceEnable.Size = new System.Drawing.Size(108, 20);
            this.checkBoxVoiceEnable.TabIndex = 153;
            this.checkBoxVoiceEnable.Text = "Voice enable";
            this.checkBoxVoiceEnable.UseVisualStyleBackColor = true;
            // 
            // textBoxAct9
            // 
            this.textBoxAct9.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxAct9.Location = new System.Drawing.Point(1031, 88);
            this.textBoxAct9.Name = "textBoxAct9";
            this.textBoxAct9.Size = new System.Drawing.Size(130, 23);
            this.textBoxAct9.TabIndex = 195;
            this.textBoxAct9.Text = "^Diary Activity";
            // 
            // textBoxAct8
            // 
            this.textBoxAct8.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxAct8.Location = new System.Drawing.Point(1031, 62);
            this.textBoxAct8.Name = "textBoxAct8";
            this.textBoxAct8.Size = new System.Drawing.Size(130, 23);
            this.textBoxAct8.TabIndex = 193;
            this.textBoxAct8.Text = "^Diary Activity";
            // 
            // textBoxAct7
            // 
            this.textBoxAct7.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxAct7.Location = new System.Drawing.Point(1031, 35);
            this.textBoxAct7.Name = "textBoxAct7";
            this.textBoxAct7.Size = new System.Drawing.Size(130, 23);
            this.textBoxAct7.TabIndex = 191;
            this.textBoxAct7.Text = "^Diary Activity";
            // 
            // textBoxSymp10
            // 
            this.textBoxSymp10.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxSymp10.Location = new System.Drawing.Point(694, 115);
            this.textBoxSymp10.Name = "textBoxSymp10";
            this.textBoxSymp10.Size = new System.Drawing.Size(130, 23);
            this.textBoxSymp10.TabIndex = 177;
            this.textBoxSymp10.Text = "^Diary sympthom entry";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Arial", 10F);
            this.label58.Location = new System.Drawing.Point(668, 118);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(28, 16);
            this.label58.TabIndex = 176;
            this.label58.Text = "10:";
            // 
            // textBoxSymp6
            // 
            this.textBoxSymp6.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxSymp6.Location = new System.Drawing.Point(694, 8);
            this.textBoxSymp6.Name = "textBoxSymp6";
            this.textBoxSymp6.Size = new System.Drawing.Size(130, 23);
            this.textBoxSymp6.TabIndex = 169;
            this.textBoxSymp6.Text = "^Diary sympthom entry";
            // 
            // textBoxSymp9
            // 
            this.textBoxSymp9.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxSymp9.Location = new System.Drawing.Point(694, 88);
            this.textBoxSymp9.Name = "textBoxSymp9";
            this.textBoxSymp9.Size = new System.Drawing.Size(130, 23);
            this.textBoxSymp9.TabIndex = 175;
            this.textBoxSymp9.Text = "^Diary sympthom entry";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Arial", 10F);
            this.label59.Location = new System.Drawing.Point(668, 91);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(20, 16);
            this.label59.TabIndex = 174;
            this.label59.Text = "9:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Arial", 10F);
            this.label29.Location = new System.Drawing.Point(506, 11);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(20, 16);
            this.label29.TabIndex = 158;
            this.label29.Text = "1:";
            // 
            // textBoxSymp8
            // 
            this.textBoxSymp8.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxSymp8.Location = new System.Drawing.Point(694, 62);
            this.textBoxSymp8.Name = "textBoxSymp8";
            this.textBoxSymp8.Size = new System.Drawing.Size(130, 23);
            this.textBoxSymp8.TabIndex = 173;
            this.textBoxSymp8.Text = "^Diary sympthom entry";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Arial", 10F);
            this.label54.Location = new System.Drawing.Point(668, 65);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(20, 16);
            this.label54.TabIndex = 172;
            this.label54.Text = "8:";
            // 
            // textBoxSymp1
            // 
            this.textBoxSymp1.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxSymp1.Location = new System.Drawing.Point(532, 8);
            this.textBoxSymp1.Name = "textBoxSymp1";
            this.textBoxSymp1.Size = new System.Drawing.Size(130, 23);
            this.textBoxSymp1.TabIndex = 159;
            this.textBoxSymp1.Text = "^Diary sympthom entry";
            // 
            // textBoxSymp7
            // 
            this.textBoxSymp7.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxSymp7.Location = new System.Drawing.Point(694, 35);
            this.textBoxSymp7.Name = "textBoxSymp7";
            this.textBoxSymp7.Size = new System.Drawing.Size(130, 23);
            this.textBoxSymp7.TabIndex = 171;
            this.textBoxSymp7.Text = "^Diary sympthom entry";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Arial", 10F);
            this.label55.Location = new System.Drawing.Point(668, 38);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(20, 16);
            this.label55.TabIndex = 170;
            this.label55.Text = "7:";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Arial", 10F);
            this.label31.Location = new System.Drawing.Point(506, 38);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(20, 16);
            this.label31.TabIndex = 160;
            this.label31.Text = "2:";
            // 
            // textBoxSymp2
            // 
            this.textBoxSymp2.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxSymp2.Location = new System.Drawing.Point(532, 35);
            this.textBoxSymp2.Name = "textBoxSymp2";
            this.textBoxSymp2.Size = new System.Drawing.Size(130, 23);
            this.textBoxSymp2.TabIndex = 161;
            this.textBoxSymp2.Text = "^Diary sympthom entry";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Arial", 10F);
            this.label53.Location = new System.Drawing.Point(506, 65);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(20, 16);
            this.label53.TabIndex = 162;
            this.label53.Text = "3:";
            // 
            // textBoxSymp3
            // 
            this.textBoxSymp3.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxSymp3.Location = new System.Drawing.Point(532, 62);
            this.textBoxSymp3.Name = "textBoxSymp3";
            this.textBoxSymp3.Size = new System.Drawing.Size(130, 23);
            this.textBoxSymp3.TabIndex = 163;
            this.textBoxSymp3.Text = "^Diary sympthom entry";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Arial", 10F);
            this.label52.Location = new System.Drawing.Point(506, 91);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(20, 16);
            this.label52.TabIndex = 164;
            this.label52.Text = "4:";
            // 
            // textBoxAct6
            // 
            this.textBoxAct6.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxAct6.Location = new System.Drawing.Point(1031, 8);
            this.textBoxAct6.Name = "textBoxAct6";
            this.textBoxAct6.Size = new System.Drawing.Size(130, 23);
            this.textBoxAct6.TabIndex = 189;
            this.textBoxAct6.Text = "^Diary Activity";
            // 
            // textBoxSymp4
            // 
            this.textBoxSymp4.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxSymp4.Location = new System.Drawing.Point(532, 88);
            this.textBoxSymp4.Name = "textBoxSymp4";
            this.textBoxSymp4.Size = new System.Drawing.Size(130, 23);
            this.textBoxSymp4.TabIndex = 165;
            this.textBoxSymp4.Text = "^Diary sympthom entry";
            // 
            // textBoxAct5
            // 
            this.textBoxAct5.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxAct5.Location = new System.Drawing.Point(866, 115);
            this.textBoxAct5.Name = "textBoxAct5";
            this.textBoxAct5.Size = new System.Drawing.Size(130, 23);
            this.textBoxAct5.TabIndex = 187;
            this.textBoxAct5.Text = "^Diary Activity";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Arial", 10F);
            this.label57.Location = new System.Drawing.Point(506, 118);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(20, 16);
            this.label57.TabIndex = 166;
            this.label57.Text = "5:";
            // 
            // textBoxAct4
            // 
            this.textBoxAct4.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxAct4.Location = new System.Drawing.Point(866, 88);
            this.textBoxAct4.Name = "textBoxAct4";
            this.textBoxAct4.Size = new System.Drawing.Size(130, 23);
            this.textBoxAct4.TabIndex = 185;
            this.textBoxAct4.Text = "^Diary Activity";
            // 
            // textBoxSymp5
            // 
            this.textBoxSymp5.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxSymp5.Location = new System.Drawing.Point(532, 115);
            this.textBoxSymp5.Name = "textBoxSymp5";
            this.textBoxSymp5.Size = new System.Drawing.Size(130, 23);
            this.textBoxSymp5.TabIndex = 167;
            this.textBoxSymp5.Text = "^Diary sympthom entry";
            // 
            // textBoxAct3
            // 
            this.textBoxAct3.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxAct3.Location = new System.Drawing.Point(866, 62);
            this.textBoxAct3.Name = "textBoxAct3";
            this.textBoxAct3.Size = new System.Drawing.Size(130, 23);
            this.textBoxAct3.TabIndex = 183;
            this.textBoxAct3.Text = "^Diary Activity";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Arial", 10F);
            this.label56.Location = new System.Drawing.Point(668, 11);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(20, 16);
            this.label56.TabIndex = 168;
            this.label56.Text = "6:";
            // 
            // textBoxAct2
            // 
            this.textBoxAct2.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxAct2.Location = new System.Drawing.Point(866, 35);
            this.textBoxAct2.Name = "textBoxAct2";
            this.textBoxAct2.Size = new System.Drawing.Size(130, 23);
            this.textBoxAct2.TabIndex = 181;
            this.textBoxAct2.Text = "^Diary Activity";
            // 
            // textBoxAct1
            // 
            this.textBoxAct1.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxAct1.Location = new System.Drawing.Point(866, 8);
            this.textBoxAct1.Name = "textBoxAct1";
            this.textBoxAct1.Size = new System.Drawing.Size(130, 23);
            this.textBoxAct1.TabIndex = 179;
            this.textBoxAct1.Text = "^Diary Activity";
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.MediumPurple;
            this.panel10.Controls.Add(this.label16);
            this.panel10.Controls.Add(this.checkBoxActDiaryOn);
            this.panel10.Controls.Add(this.label28);
            this.panel10.Controls.Add(this.checkBoxSymptDiaryOn);
            this.panel10.Controls.Add(this.label13);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(0, 215);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(1196, 25);
            this.panel10.TabIndex = 70;
            // 
            // label16
            // 
            this.label16.Dock = System.Windows.Forms.DockStyle.Left;
            this.label16.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(865, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(212, 25);
            this.label16.TabIndex = 183;
            this.label16.Text = "Diary activities";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // checkBoxActDiaryOn
            // 
            this.checkBoxActDiaryOn.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxActDiaryOn.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxActDiaryOn.ForeColor = System.Drawing.SystemColors.Control;
            this.checkBoxActDiaryOn.Location = new System.Drawing.Point(845, 0);
            this.checkBoxActDiaryOn.Name = "checkBoxActDiaryOn";
            this.checkBoxActDiaryOn.Size = new System.Drawing.Size(20, 25);
            this.checkBoxActDiaryOn.TabIndex = 185;
            this.checkBoxActDiaryOn.Text = " ";
            this.checkBoxActDiaryOn.UseVisualStyleBackColor = true;
            // 
            // label28
            // 
            this.label28.Dock = System.Windows.Forms.DockStyle.Left;
            this.label28.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label28.ForeColor = System.Drawing.Color.White;
            this.label28.Location = new System.Drawing.Point(526, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(319, 25);
            this.label28.TabIndex = 182;
            this.label28.Text = "Diary symptoms";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // checkBoxSymptDiaryOn
            // 
            this.checkBoxSymptDiaryOn.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxSymptDiaryOn.Font = new System.Drawing.Font("Arial", 12F);
            this.checkBoxSymptDiaryOn.ForeColor = System.Drawing.SystemColors.Control;
            this.checkBoxSymptDiaryOn.Location = new System.Drawing.Point(510, 0);
            this.checkBoxSymptDiaryOn.Name = "checkBoxSymptDiaryOn";
            this.checkBoxSymptDiaryOn.Size = new System.Drawing.Size(16, 25);
            this.checkBoxSymptDiaryOn.TabIndex = 184;
            this.checkBoxSymptDiaryOn.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.Dock = System.Windows.Forms.DockStyle.Left;
            this.label13.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(0, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(510, 25);
            this.label13.TabIndex = 181;
            this.label13.Text = "Technical settings";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.labelMessageInfo);
            this.panel9.Controls.Add(this.label67);
            this.panel9.Controls.Add(this.label66);
            this.panel9.Controls.Add(this.textBoxEventPreTime);
            this.panel9.Controls.Add(this.label64);
            this.panel9.Controls.Add(this.label65);
            this.panel9.Controls.Add(this.textBoxEventPostTime);
            this.panel9.Controls.Add(this.checkBoxUpdateFirmware);
            this.panel9.Controls.Add(this.checkBoxFormatSD);
            this.panel9.Controls.Add(this.checkBoxShutdown);
            this.panel9.Controls.Add(this.comboBoxCrossCh);
            this.panel9.Controls.Add(this.labelCrossChannelAnalysis);
            this.panel9.Controls.Add(this.comboBoxAnalysisMask);
            this.panel9.Controls.Add(this.label25);
            this.panel9.Controls.Add(this.checkBoxResetFileNumber);
            this.panel9.Controls.Add(this.label26);
            this.panel9.Controls.Add(this.buttonSendAction);
            this.panel9.Controls.Add(this.checkBoxTransmitIntervalReports);
            this.panel9.Controls.Add(this.radioButtonPacemakerNo);
            this.panel9.Controls.Add(this.checkBoxReportChargingStart);
            this.panel9.Controls.Add(this.textBoxMessage);
            this.panel9.Controls.Add(this.checkBoxReportChargingStop);
            this.panel9.Controls.Add(this.radioButtonPacemakerYes);
            this.panel9.Controls.Add(this.checkBoxReportBattLow);
            this.panel9.Controls.Add(this.checkBoxMessage);
            this.panel9.Controls.Add(this.checkBoxReportPatEvent);
            this.panel9.Controls.Add(this.checkBoxStartStudy);
            this.panel9.Controls.Add(this.checkBoxReportLeadDisconnected);
            this.panel9.Controls.Add(this.checkBoxEndStudy);
            this.panel9.Controls.Add(this.checkBoxReportPaceDetected);
            this.panel9.Controls.Add(this.checkBoxNewSetting);
            this.panel9.Controls.Add(this.checkBoxReportFull);
            this.panel9.Controls.Add(this.checkBoxNewStudy);
            this.panel9.Controls.Add(this.checkBoxReportResume);
            this.panel9.Controls.Add(this.checkBoxReportBreak);
            this.panel9.Controls.Add(this.checkBoxReportStop);
            this.panel9.Controls.Add(this.textBoxBradyOnset);
            this.panel9.Controls.Add(this.checkBoxReportStart);
            this.panel9.Controls.Add(this.checkBoxReportBradyOnset);
            this.panel9.Controls.Add(this.label20);
            this.panel9.Controls.Add(this.checkBoxReportBradyOffset);
            this.panel9.Controls.Add(this.label21);
            this.panel9.Controls.Add(this.checkBoxReportTachyOffset);
            this.panel9.Controls.Add(this.label22);
            this.panel9.Controls.Add(this.checkBoxReportTachyOnset);
            this.panel9.Controls.Add(this.textBoxPauseDuration);
            this.panel9.Controls.Add(this.checkBoxReportAFIBOffset);
            this.panel9.Controls.Add(this.textBoxPauseOffset);
            this.panel9.Controls.Add(this.checkBoxReportAFIBOnset);
            this.panel9.Controls.Add(this.textBoxPauseOnset);
            this.panel9.Controls.Add(this.checkBoxReportPause);
            this.panel9.Controls.Add(this.label23);
            this.panel9.Controls.Add(this.label5);
            this.panel9.Controls.Add(this.label15);
            this.panel9.Controls.Add(this.label4);
            this.panel9.Controls.Add(this.label17);
            this.panel9.Controls.Add(this.label3);
            this.panel9.Controls.Add(this.label18);
            this.panel9.Controls.Add(this.label2);
            this.panel9.Controls.Add(this.textBoxAFIBduration);
            this.panel9.Controls.Add(this.textBoxAFIBoffset);
            this.panel9.Controls.Add(this.textBoxBradyOffset);
            this.panel9.Controls.Add(this.textBoxAFIBonset);
            this.panel9.Controls.Add(this.textBoxBradyDuration);
            this.panel9.Controls.Add(this.label19);
            this.panel9.Controls.Add(this.label7);
            this.panel9.Controls.Add(this.label6);
            this.panel9.Controls.Add(this.label8);
            this.panel9.Controls.Add(this.label11);
            this.panel9.Controls.Add(this.label9);
            this.panel9.Controls.Add(this.label12);
            this.panel9.Controls.Add(this.label14);
            this.panel9.Controls.Add(this.textBoxTachyDuration);
            this.panel9.Controls.Add(this.textBoxTachyOnset);
            this.panel9.Controls.Add(this.textBoxTachyOffset);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 25);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(1196, 190);
            this.panel9.TabIndex = 69;
            // 
            // labelMessageInfo
            // 
            this.labelMessageInfo.Font = new System.Drawing.Font("Arial", 10F);
            this.labelMessageInfo.Location = new System.Drawing.Point(11, 137);
            this.labelMessageInfo.Name = "labelMessageInfo";
            this.labelMessageInfo.Size = new System.Drawing.Size(153, 50);
            this.labelMessageInfo.TabIndex = 197;
            this.labelMessageInfo.Text = "<ctrl>=Req.Scp Strip\r\n<alt>=Req. Trend\r\n<shift>=Req. One Scp";
            // 
            // label67
            // 
            this.label67.BackColor = System.Drawing.Color.Transparent;
            this.label67.Font = new System.Drawing.Font("Arial", 10F);
            this.label67.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label67.Location = new System.Drawing.Point(480, 167);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(109, 19);
            this.label67.TabIndex = 196;
            this.label67.Text = "Post:";
            this.label67.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label66
            // 
            this.label66.BackColor = System.Drawing.Color.Transparent;
            this.label66.Font = new System.Drawing.Font("Arial", 10F);
            this.label66.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label66.Location = new System.Drawing.Point(480, 141);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(109, 19);
            this.label66.TabIndex = 195;
            this.label66.Text = "Event Strip Pre:";
            this.label66.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // textBoxEventPreTime
            // 
            this.textBoxEventPreTime.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxEventPreTime.Location = new System.Drawing.Point(593, 137);
            this.textBoxEventPreTime.Name = "textBoxEventPreTime";
            this.textBoxEventPreTime.Size = new System.Drawing.Size(40, 23);
            this.textBoxEventPreTime.TabIndex = 191;
            this.textBoxEventPreTime.Text = "^99";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Arial", 10F);
            this.label64.Location = new System.Drawing.Point(636, 166);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(30, 16);
            this.label64.TabIndex = 194;
            this.label64.Text = "sec";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Arial", 10F);
            this.label65.Location = new System.Drawing.Point(636, 140);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(30, 16);
            this.label65.TabIndex = 192;
            this.label65.Text = "sec";
            // 
            // textBoxEventPostTime
            // 
            this.textBoxEventPostTime.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxEventPostTime.Location = new System.Drawing.Point(593, 163);
            this.textBoxEventPostTime.Name = "textBoxEventPostTime";
            this.textBoxEventPostTime.Size = new System.Drawing.Size(40, 23);
            this.textBoxEventPostTime.TabIndex = 193;
            this.textBoxEventPostTime.Text = "^99";
            // 
            // checkBoxUpdateFirmware
            // 
            this.checkBoxUpdateFirmware.AutoSize = true;
            this.checkBoxUpdateFirmware.Enabled = false;
            this.checkBoxUpdateFirmware.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxUpdateFirmware.Location = new System.Drawing.Point(8, 113);
            this.checkBoxUpdateFirmware.Name = "checkBoxUpdateFirmware";
            this.checkBoxUpdateFirmware.Size = new System.Drawing.Size(134, 20);
            this.checkBoxUpdateFirmware.TabIndex = 190;
            this.checkBoxUpdateFirmware.Text = "Update Firmware";
            this.checkBoxUpdateFirmware.UseVisualStyleBackColor = true;
            this.checkBoxUpdateFirmware.Visible = false;
            // 
            // checkBoxFormatSD
            // 
            this.checkBoxFormatSD.AutoSize = true;
            this.checkBoxFormatSD.Enabled = false;
            this.checkBoxFormatSD.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxFormatSD.Location = new System.Drawing.Point(9, 87);
            this.checkBoxFormatSD.Name = "checkBoxFormatSD";
            this.checkBoxFormatSD.Size = new System.Drawing.Size(95, 20);
            this.checkBoxFormatSD.TabIndex = 189;
            this.checkBoxFormatSD.Text = "Format SD";
            this.checkBoxFormatSD.UseVisualStyleBackColor = true;
            this.checkBoxFormatSD.Visible = false;
            // 
            // checkBoxShutdown
            // 
            this.checkBoxShutdown.AutoSize = true;
            this.checkBoxShutdown.Enabled = false;
            this.checkBoxShutdown.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxShutdown.Location = new System.Drawing.Point(9, 62);
            this.checkBoxShutdown.Name = "checkBoxShutdown";
            this.checkBoxShutdown.Size = new System.Drawing.Size(89, 20);
            this.checkBoxShutdown.TabIndex = 188;
            this.checkBoxShutdown.Text = "Shutdown";
            this.checkBoxShutdown.UseVisualStyleBackColor = true;
            this.checkBoxShutdown.Visible = false;
            // 
            // comboBoxCrossCh
            // 
            this.comboBoxCrossCh.Font = new System.Drawing.Font("Arial", 10F);
            this.comboBoxCrossCh.FormattingEnabled = true;
            this.comboBoxCrossCh.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6"});
            this.comboBoxCrossCh.Location = new System.Drawing.Point(1125, 157);
            this.comboBoxCrossCh.Name = "comboBoxCrossCh";
            this.comboBoxCrossCh.Size = new System.Drawing.Size(47, 24);
            this.comboBoxCrossCh.TabIndex = 157;
            this.comboBoxCrossCh.Text = "^99";
            // 
            // labelCrossChannelAnalysis
            // 
            this.labelCrossChannelAnalysis.AutoSize = true;
            this.labelCrossChannelAnalysis.Font = new System.Drawing.Font("Arial", 10F);
            this.labelCrossChannelAnalysis.ForeColor = System.Drawing.Color.DarkBlue;
            this.labelCrossChannelAnalysis.Location = new System.Drawing.Point(998, 161);
            this.labelCrossChannelAnalysis.Name = "labelCrossChannelAnalysis";
            this.labelCrossChannelAnalysis.Size = new System.Drawing.Size(126, 16);
            this.labelCrossChannelAnalysis.TabIndex = 156;
            this.labelCrossChannelAnalysis.Text = "Cross Ch Analysis:";
            // 
            // comboBoxAnalysisMask
            // 
            this.comboBoxAnalysisMask.Font = new System.Drawing.Font("Arial", 10F);
            this.comboBoxAnalysisMask.FormattingEnabled = true;
            this.comboBoxAnalysisMask.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6"});
            this.comboBoxAnalysisMask.Location = new System.Drawing.Point(913, 157);
            this.comboBoxAnalysisMask.Name = "comboBoxAnalysisMask";
            this.comboBoxAnalysisMask.Size = new System.Drawing.Size(57, 24);
            this.comboBoxAnalysisMask.TabIndex = 152;
            this.comboBoxAnalysisMask.Text = "^99";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Arial", 10F);
            this.label25.ForeColor = System.Drawing.Color.DarkBlue;
            this.label25.Location = new System.Drawing.Point(807, 161);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(101, 16);
            this.label25.TabIndex = 151;
            this.label25.Text = "Analysis mask:";
            // 
            // checkBoxResetFileNumber
            // 
            this.checkBoxResetFileNumber.AutoSize = true;
            this.checkBoxResetFileNumber.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxResetFileNumber.Location = new System.Drawing.Point(193, 35);
            this.checkBoxResetFileNumber.Name = "checkBoxResetFileNumber";
            this.checkBoxResetFileNumber.Size = new System.Drawing.Size(144, 20);
            this.checkBoxResetFileNumber.TabIndex = 166;
            this.checkBoxResetFileNumber.Text = "Reset File Number";
            this.checkBoxResetFileNumber.UseVisualStyleBackColor = true;
            // 
            // label26
            // 
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Arial", 10F);
            this.label26.Location = new System.Drawing.Point(373, 146);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(52, 40);
            this.label26.TabIndex = 155;
            this.label26.Text = "Pace detect:";
            // 
            // buttonSendAction
            // 
            this.buttonSendAction.BackColor = System.Drawing.Color.MediumPurple;
            this.buttonSendAction.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonSendAction.ForeColor = System.Drawing.Color.White;
            this.buttonSendAction.Location = new System.Drawing.Point(190, 155);
            this.buttonSendAction.Name = "buttonSendAction";
            this.buttonSendAction.Size = new System.Drawing.Size(161, 32);
            this.buttonSendAction.TabIndex = 165;
            this.buttonSendAction.Text = "Send Action";
            this.buttonSendAction.UseVisualStyleBackColor = false;
            this.buttonSendAction.Click += new System.EventHandler(this.buttonSendAction_Click);
            // 
            // checkBoxTransmitIntervalReports
            // 
            this.checkBoxTransmitIntervalReports.AutoSize = true;
            this.checkBoxTransmitIntervalReports.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxTransmitIntervalReports.Location = new System.Drawing.Point(806, 33);
            this.checkBoxTransmitIntervalReports.Name = "checkBoxTransmitIntervalReports";
            this.checkBoxTransmitIntervalReports.Size = new System.Drawing.Size(180, 20);
            this.checkBoxTransmitIntervalReports.TabIndex = 150;
            this.checkBoxTransmitIntervalReports.Text = "Transmit interval reports";
            this.checkBoxTransmitIntervalReports.UseVisualStyleBackColor = true;
            // 
            // radioButtonPacemakerNo
            // 
            this.radioButtonPacemakerNo.AutoSize = true;
            this.radioButtonPacemakerNo.Font = new System.Drawing.Font("Arial", 10F);
            this.radioButtonPacemakerNo.Location = new System.Drawing.Point(431, 163);
            this.radioButtonPacemakerNo.Name = "radioButtonPacemakerNo";
            this.radioButtonPacemakerNo.Size = new System.Drawing.Size(43, 20);
            this.radioButtonPacemakerNo.TabIndex = 154;
            this.radioButtonPacemakerNo.TabStop = true;
            this.radioButtonPacemakerNo.Text = "No";
            this.radioButtonPacemakerNo.UseVisualStyleBackColor = true;
            // 
            // checkBoxReportChargingStart
            // 
            this.checkBoxReportChargingStart.AutoSize = true;
            this.checkBoxReportChargingStart.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxReportChargingStart.ForeColor = System.Drawing.Color.DarkBlue;
            this.checkBoxReportChargingStart.Location = new System.Drawing.Point(1001, 110);
            this.checkBoxReportChargingStart.Name = "checkBoxReportChargingStart";
            this.checkBoxReportChargingStart.Size = new System.Drawing.Size(133, 20);
            this.checkBoxReportChargingStart.TabIndex = 149;
            this.checkBoxReportChargingStart.Text = "Charging started";
            this.checkBoxReportChargingStart.UseVisualStyleBackColor = true;
            // 
            // textBoxMessage
            // 
            this.textBoxMessage.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxMessage.Location = new System.Drawing.Point(193, 113);
            this.textBoxMessage.Name = "textBoxMessage";
            this.textBoxMessage.Size = new System.Drawing.Size(158, 23);
            this.textBoxMessage.TabIndex = 164;
            // 
            // checkBoxReportChargingStop
            // 
            this.checkBoxReportChargingStop.AutoSize = true;
            this.checkBoxReportChargingStop.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxReportChargingStop.ForeColor = System.Drawing.Color.DarkBlue;
            this.checkBoxReportChargingStop.Location = new System.Drawing.Point(1001, 135);
            this.checkBoxReportChargingStop.Name = "checkBoxReportChargingStop";
            this.checkBoxReportChargingStop.Size = new System.Drawing.Size(140, 20);
            this.checkBoxReportChargingStop.TabIndex = 148;
            this.checkBoxReportChargingStop.Text = "Charging stopped";
            this.checkBoxReportChargingStop.UseVisualStyleBackColor = true;
            // 
            // radioButtonPacemakerYes
            // 
            this.radioButtonPacemakerYes.AutoSize = true;
            this.radioButtonPacemakerYes.Font = new System.Drawing.Font("Arial", 10F);
            this.radioButtonPacemakerYes.Location = new System.Drawing.Point(431, 140);
            this.radioButtonPacemakerYes.Name = "radioButtonPacemakerYes";
            this.radioButtonPacemakerYes.Size = new System.Drawing.Size(49, 20);
            this.radioButtonPacemakerYes.TabIndex = 153;
            this.radioButtonPacemakerYes.TabStop = true;
            this.radioButtonPacemakerYes.Text = "Yes";
            this.radioButtonPacemakerYes.UseVisualStyleBackColor = true;
            // 
            // checkBoxReportBattLow
            // 
            this.checkBoxReportBattLow.AutoSize = true;
            this.checkBoxReportBattLow.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxReportBattLow.ForeColor = System.Drawing.Color.DarkBlue;
            this.checkBoxReportBattLow.Location = new System.Drawing.Point(1001, 85);
            this.checkBoxReportBattLow.Name = "checkBoxReportBattLow";
            this.checkBoxReportBattLow.Size = new System.Drawing.Size(96, 20);
            this.checkBoxReportBattLow.TabIndex = 147;
            this.checkBoxReportBattLow.Text = "Battery low";
            this.checkBoxReportBattLow.UseVisualStyleBackColor = true;
            // 
            // checkBoxMessage
            // 
            this.checkBoxMessage.AutoSize = true;
            this.checkBoxMessage.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxMessage.Location = new System.Drawing.Point(193, 87);
            this.checkBoxMessage.Name = "checkBoxMessage";
            this.checkBoxMessage.Size = new System.Drawing.Size(88, 20);
            this.checkBoxMessage.TabIndex = 163;
            this.checkBoxMessage.Text = "Message:";
            this.checkBoxMessage.UseVisualStyleBackColor = true;
            // 
            // checkBoxReportPatEvent
            // 
            this.checkBoxReportPatEvent.AutoSize = true;
            this.checkBoxReportPatEvent.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxReportPatEvent.ForeColor = System.Drawing.Color.DarkBlue;
            this.checkBoxReportPatEvent.Location = new System.Drawing.Point(688, 159);
            this.checkBoxReportPatEvent.Name = "checkBoxReportPatEvent";
            this.checkBoxReportPatEvent.Size = new System.Drawing.Size(110, 20);
            this.checkBoxReportPatEvent.TabIndex = 146;
            this.checkBoxReportPatEvent.Text = "Patient event";
            this.checkBoxReportPatEvent.UseVisualStyleBackColor = true;
            // 
            // checkBoxStartStudy
            // 
            this.checkBoxStartStudy.AutoSize = true;
            this.checkBoxStartStudy.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxStartStudy.Location = new System.Drawing.Point(9, 12);
            this.checkBoxStartStudy.Name = "checkBoxStartStudy";
            this.checkBoxStartStudy.Size = new System.Drawing.Size(97, 20);
            this.checkBoxStartStudy.TabIndex = 162;
            this.checkBoxStartStudy.Text = "Start Study";
            this.checkBoxStartStudy.UseVisualStyleBackColor = true;
            // 
            // checkBoxReportLeadDisconnected
            // 
            this.checkBoxReportLeadDisconnected.AutoSize = true;
            this.checkBoxReportLeadDisconnected.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxReportLeadDisconnected.ForeColor = System.Drawing.Color.DarkBlue;
            this.checkBoxReportLeadDisconnected.Location = new System.Drawing.Point(806, 85);
            this.checkBoxReportLeadDisconnected.Name = "checkBoxReportLeadDisconnected";
            this.checkBoxReportLeadDisconnected.Size = new System.Drawing.Size(189, 20);
            this.checkBoxReportLeadDisconnected.TabIndex = 145;
            this.checkBoxReportLeadDisconnected.Text = "Report lead disconnected";
            this.checkBoxReportLeadDisconnected.UseVisualStyleBackColor = true;
            // 
            // checkBoxEndStudy
            // 
            this.checkBoxEndStudy.AutoSize = true;
            this.checkBoxEndStudy.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxEndStudy.Location = new System.Drawing.Point(9, 36);
            this.checkBoxEndStudy.Name = "checkBoxEndStudy";
            this.checkBoxEndStudy.Size = new System.Drawing.Size(92, 20);
            this.checkBoxEndStudy.TabIndex = 161;
            this.checkBoxEndStudy.Text = "End Study";
            this.checkBoxEndStudy.UseVisualStyleBackColor = true;
            // 
            // checkBoxReportPaceDetected
            // 
            this.checkBoxReportPaceDetected.AutoSize = true;
            this.checkBoxReportPaceDetected.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxReportPaceDetected.ForeColor = System.Drawing.Color.DarkBlue;
            this.checkBoxReportPaceDetected.Location = new System.Drawing.Point(806, 59);
            this.checkBoxReportPaceDetected.Name = "checkBoxReportPaceDetected";
            this.checkBoxReportPaceDetected.Size = new System.Drawing.Size(164, 20);
            this.checkBoxReportPaceDetected.TabIndex = 144;
            this.checkBoxReportPaceDetected.Text = "Report pace detected";
            this.checkBoxReportPaceDetected.UseVisualStyleBackColor = true;
            // 
            // checkBoxNewSetting
            // 
            this.checkBoxNewSetting.AutoSize = true;
            this.checkBoxNewSetting.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxNewSetting.Location = new System.Drawing.Point(193, 9);
            this.checkBoxNewSetting.Name = "checkBoxNewSetting";
            this.checkBoxNewSetting.Size = new System.Drawing.Size(101, 20);
            this.checkBoxNewSetting.TabIndex = 160;
            this.checkBoxNewSetting.Text = "New Setting";
            this.checkBoxNewSetting.UseVisualStyleBackColor = true;
            this.checkBoxNewSetting.CheckedChanged += new System.EventHandler(this.checkBoxNewSetting_CheckedChanged);
            // 
            // checkBoxReportFull
            // 
            this.checkBoxReportFull.AutoSize = true;
            this.checkBoxReportFull.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxReportFull.ForeColor = System.Drawing.Color.DarkBlue;
            this.checkBoxReportFull.Location = new System.Drawing.Point(1001, 59);
            this.checkBoxReportFull.Name = "checkBoxReportFull";
            this.checkBoxReportFull.Size = new System.Drawing.Size(92, 20);
            this.checkBoxReportFull.TabIndex = 143;
            this.checkBoxReportFull.Text = "Report full";
            this.checkBoxReportFull.UseVisualStyleBackColor = true;
            // 
            // checkBoxNewStudy
            // 
            this.checkBoxNewStudy.AutoSize = true;
            this.checkBoxNewStudy.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxNewStudy.ForeColor = System.Drawing.Color.DarkBlue;
            this.checkBoxNewStudy.Location = new System.Drawing.Point(193, 60);
            this.checkBoxNewStudy.Name = "checkBoxNewStudy";
            this.checkBoxNewStudy.Size = new System.Drawing.Size(141, 20);
            this.checkBoxNewStudy.TabIndex = 159;
            this.checkBoxNewStudy.Text = "Enable New Study";
            this.checkBoxNewStudy.UseVisualStyleBackColor = true;
            // 
            // checkBoxReportResume
            // 
            this.checkBoxReportResume.AutoSize = true;
            this.checkBoxReportResume.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxReportResume.ForeColor = System.Drawing.Color.DarkBlue;
            this.checkBoxReportResume.Location = new System.Drawing.Point(1001, 33);
            this.checkBoxReportResume.Name = "checkBoxReportResume";
            this.checkBoxReportResume.Size = new System.Drawing.Size(121, 20);
            this.checkBoxReportResume.TabIndex = 142;
            this.checkBoxReportResume.Text = "Report resume";
            this.checkBoxReportResume.UseVisualStyleBackColor = true;
            // 
            // checkBoxReportBreak
            // 
            this.checkBoxReportBreak.AutoSize = true;
            this.checkBoxReportBreak.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxReportBreak.ForeColor = System.Drawing.Color.DarkBlue;
            this.checkBoxReportBreak.Location = new System.Drawing.Point(1001, 8);
            this.checkBoxReportBreak.Name = "checkBoxReportBreak";
            this.checkBoxReportBreak.Size = new System.Drawing.Size(110, 20);
            this.checkBoxReportBreak.TabIndex = 141;
            this.checkBoxReportBreak.Text = "Report break";
            this.checkBoxReportBreak.UseVisualStyleBackColor = true;
            // 
            // checkBoxReportStop
            // 
            this.checkBoxReportStop.AutoSize = true;
            this.checkBoxReportStop.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxReportStop.ForeColor = System.Drawing.Color.DarkBlue;
            this.checkBoxReportStop.Location = new System.Drawing.Point(806, 135);
            this.checkBoxReportStop.Name = "checkBoxReportStop";
            this.checkBoxReportStop.Size = new System.Drawing.Size(101, 20);
            this.checkBoxReportStop.TabIndex = 140;
            this.checkBoxReportStop.Text = "Report stop";
            this.checkBoxReportStop.UseVisualStyleBackColor = true;
            // 
            // textBoxBradyOnset
            // 
            this.textBoxBradyOnset.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxBradyOnset.Location = new System.Drawing.Point(423, 31);
            this.textBoxBradyOnset.Name = "textBoxBradyOnset";
            this.textBoxBradyOnset.Size = new System.Drawing.Size(40, 23);
            this.textBoxBradyOnset.TabIndex = 64;
            this.textBoxBradyOnset.Text = "^99";
            // 
            // checkBoxReportStart
            // 
            this.checkBoxReportStart.AutoSize = true;
            this.checkBoxReportStart.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxReportStart.ForeColor = System.Drawing.Color.DarkBlue;
            this.checkBoxReportStart.Location = new System.Drawing.Point(806, 110);
            this.checkBoxReportStart.Name = "checkBoxReportStart";
            this.checkBoxReportStart.Size = new System.Drawing.Size(102, 20);
            this.checkBoxReportStart.TabIndex = 139;
            this.checkBoxReportStart.Text = "Report start";
            this.checkBoxReportStart.UseVisualStyleBackColor = true;
            // 
            // checkBoxReportBradyOnset
            // 
            this.checkBoxReportBradyOnset.AutoSize = true;
            this.checkBoxReportBradyOnset.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxReportBradyOnset.ForeColor = System.Drawing.Color.DarkBlue;
            this.checkBoxReportBradyOnset.Location = new System.Drawing.Point(688, 8);
            this.checkBoxReportBradyOnset.Name = "checkBoxReportBradyOnset";
            this.checkBoxReportBradyOnset.Size = new System.Drawing.Size(103, 20);
            this.checkBoxReportBradyOnset.TabIndex = 108;
            this.checkBoxReportBradyOnset.Text = "Brady onset";
            this.checkBoxReportBradyOnset.UseVisualStyleBackColor = true;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial", 10F);
            this.label20.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label20.Location = new System.Drawing.Point(637, 111);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(41, 16);
            this.label20.TabIndex = 90;
            this.label20.Text = "msec";
            // 
            // checkBoxReportBradyOffset
            // 
            this.checkBoxReportBradyOffset.AutoSize = true;
            this.checkBoxReportBradyOffset.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxReportBradyOffset.ForeColor = System.Drawing.Color.DarkBlue;
            this.checkBoxReportBradyOffset.Location = new System.Drawing.Point(688, 110);
            this.checkBoxReportBradyOffset.Name = "checkBoxReportBradyOffset";
            this.checkBoxReportBradyOffset.Size = new System.Drawing.Size(103, 20);
            this.checkBoxReportBradyOffset.TabIndex = 107;
            this.checkBoxReportBradyOffset.Text = "Brady offset";
            this.checkBoxReportBradyOffset.UseVisualStyleBackColor = true;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Enabled = false;
            this.label21.Font = new System.Drawing.Font("Arial", 10F);
            this.label21.Location = new System.Drawing.Point(551, 111);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(30, 16);
            this.label21.TabIndex = 89;
            this.label21.Text = "sec";
            this.label21.Visible = false;
            // 
            // checkBoxReportTachyOffset
            // 
            this.checkBoxReportTachyOffset.AutoSize = true;
            this.checkBoxReportTachyOffset.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxReportTachyOffset.ForeColor = System.Drawing.Color.DarkBlue;
            this.checkBoxReportTachyOffset.Location = new System.Drawing.Point(688, 135);
            this.checkBoxReportTachyOffset.Name = "checkBoxReportTachyOffset";
            this.checkBoxReportTachyOffset.Size = new System.Drawing.Size(103, 20);
            this.checkBoxReportTachyOffset.TabIndex = 109;
            this.checkBoxReportTachyOffset.Text = "Tachy offset";
            this.checkBoxReportTachyOffset.UseVisualStyleBackColor = true;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Enabled = false;
            this.label22.Font = new System.Drawing.Font("Arial", 10F);
            this.label22.Location = new System.Drawing.Point(467, 111);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(30, 16);
            this.label22.TabIndex = 88;
            this.label22.Text = "sec";
            this.label22.Visible = false;
            // 
            // checkBoxReportTachyOnset
            // 
            this.checkBoxReportTachyOnset.AutoSize = true;
            this.checkBoxReportTachyOnset.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxReportTachyOnset.ForeColor = System.Drawing.Color.DarkBlue;
            this.checkBoxReportTachyOnset.Location = new System.Drawing.Point(688, 33);
            this.checkBoxReportTachyOnset.Name = "checkBoxReportTachyOnset";
            this.checkBoxReportTachyOnset.Size = new System.Drawing.Size(103, 20);
            this.checkBoxReportTachyOnset.TabIndex = 110;
            this.checkBoxReportTachyOnset.Text = "Tachy onset";
            this.checkBoxReportTachyOnset.UseVisualStyleBackColor = true;
            // 
            // textBoxPauseDuration
            // 
            this.textBoxPauseDuration.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxPauseDuration.Location = new System.Drawing.Point(594, 108);
            this.textBoxPauseDuration.Name = "textBoxPauseDuration";
            this.textBoxPauseDuration.Size = new System.Drawing.Size(40, 23);
            this.textBoxPauseDuration.TabIndex = 87;
            this.textBoxPauseDuration.Text = "^99";
            // 
            // checkBoxReportAFIBOffset
            // 
            this.checkBoxReportAFIBOffset.AutoSize = true;
            this.checkBoxReportAFIBOffset.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxReportAFIBOffset.ForeColor = System.Drawing.Color.DarkBlue;
            this.checkBoxReportAFIBOffset.Location = new System.Drawing.Point(806, 8);
            this.checkBoxReportAFIBOffset.Name = "checkBoxReportAFIBOffset";
            this.checkBoxReportAFIBOffset.Size = new System.Drawing.Size(90, 20);
            this.checkBoxReportAFIBOffset.TabIndex = 111;
            this.checkBoxReportAFIBOffset.Text = "Afib offset";
            this.checkBoxReportAFIBOffset.UseVisualStyleBackColor = true;
            // 
            // textBoxPauseOffset
            // 
            this.textBoxPauseOffset.Enabled = false;
            this.textBoxPauseOffset.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxPauseOffset.Location = new System.Drawing.Point(508, 108);
            this.textBoxPauseOffset.Name = "textBoxPauseOffset";
            this.textBoxPauseOffset.ReadOnly = true;
            this.textBoxPauseOffset.Size = new System.Drawing.Size(40, 23);
            this.textBoxPauseOffset.TabIndex = 86;
            this.textBoxPauseOffset.Visible = false;
            // 
            // checkBoxReportAFIBOnset
            // 
            this.checkBoxReportAFIBOnset.AutoSize = true;
            this.checkBoxReportAFIBOnset.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxReportAFIBOnset.ForeColor = System.Drawing.Color.DarkBlue;
            this.checkBoxReportAFIBOnset.Location = new System.Drawing.Point(688, 59);
            this.checkBoxReportAFIBOnset.Name = "checkBoxReportAFIBOnset";
            this.checkBoxReportAFIBOnset.Size = new System.Drawing.Size(90, 20);
            this.checkBoxReportAFIBOnset.TabIndex = 112;
            this.checkBoxReportAFIBOnset.Text = "Afib onset";
            this.checkBoxReportAFIBOnset.UseVisualStyleBackColor = true;
            // 
            // textBoxPauseOnset
            // 
            this.textBoxPauseOnset.Enabled = false;
            this.textBoxPauseOnset.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxPauseOnset.Location = new System.Drawing.Point(423, 108);
            this.textBoxPauseOnset.Name = "textBoxPauseOnset";
            this.textBoxPauseOnset.ReadOnly = true;
            this.textBoxPauseOnset.Size = new System.Drawing.Size(40, 23);
            this.textBoxPauseOnset.TabIndex = 85;
            this.textBoxPauseOnset.Visible = false;
            // 
            // checkBoxReportPause
            // 
            this.checkBoxReportPause.AutoSize = true;
            this.checkBoxReportPause.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxReportPause.ForeColor = System.Drawing.Color.DarkBlue;
            this.checkBoxReportPause.Location = new System.Drawing.Point(688, 85);
            this.checkBoxReportPause.Name = "checkBoxReportPause";
            this.checkBoxReportPause.Size = new System.Drawing.Size(67, 20);
            this.checkBoxReportPause.TabIndex = 113;
            this.checkBoxReportPause.Text = "Pause";
            this.checkBoxReportPause.UseVisualStyleBackColor = true;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Arial", 10F);
            this.label23.Location = new System.Drawing.Point(373, 111);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(48, 16);
            this.label23.TabIndex = 84;
            this.label23.Text = "Pause";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label5.Location = new System.Drawing.Point(419, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 16);
            this.label5.TabIndex = 60;
            this.label5.Text = "Onset";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 10F);
            this.label15.ForeColor = System.Drawing.Color.DarkBlue;
            this.label15.Location = new System.Drawing.Point(637, 86);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(30, 16);
            this.label15.TabIndex = 83;
            this.label15.Text = "min";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 10F);
            this.label4.Location = new System.Drawing.Point(373, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 16);
            this.label4.TabIndex = 61;
            this.label4.Text = "Brady";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Enabled = false;
            this.label17.Font = new System.Drawing.Font("Arial", 10F);
            this.label17.Location = new System.Drawing.Point(551, 86);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(35, 16);
            this.label17.TabIndex = 82;
            this.label17.Text = "bpm";
            this.label17.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label3.Location = new System.Drawing.Point(590, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 16);
            this.label3.TabIndex = 62;
            this.label3.Text = "Duration";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Enabled = false;
            this.label18.Font = new System.Drawing.Font("Arial", 10F);
            this.label18.Location = new System.Drawing.Point(467, 86);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(35, 16);
            this.label18.TabIndex = 81;
            this.label18.Text = "bpm";
            this.label18.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.DarkBlue;
            this.label2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label2.Location = new System.Drawing.Point(505, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 16);
            this.label2.TabIndex = 63;
            this.label2.Text = "Offset";
            // 
            // textBoxAFIBduration
            // 
            this.textBoxAFIBduration.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxAFIBduration.Location = new System.Drawing.Point(594, 83);
            this.textBoxAFIBduration.Name = "textBoxAFIBduration";
            this.textBoxAFIBduration.Size = new System.Drawing.Size(40, 23);
            this.textBoxAFIBduration.TabIndex = 80;
            this.textBoxAFIBduration.Text = "^99";
            // 
            // textBoxAFIBoffset
            // 
            this.textBoxAFIBoffset.Enabled = false;
            this.textBoxAFIBoffset.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxAFIBoffset.Location = new System.Drawing.Point(508, 83);
            this.textBoxAFIBoffset.Name = "textBoxAFIBoffset";
            this.textBoxAFIBoffset.ReadOnly = true;
            this.textBoxAFIBoffset.Size = new System.Drawing.Size(40, 23);
            this.textBoxAFIBoffset.TabIndex = 79;
            this.textBoxAFIBoffset.Visible = false;
            // 
            // textBoxBradyOffset
            // 
            this.textBoxBradyOffset.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxBradyOffset.Location = new System.Drawing.Point(508, 31);
            this.textBoxBradyOffset.Name = "textBoxBradyOffset";
            this.textBoxBradyOffset.Size = new System.Drawing.Size(40, 23);
            this.textBoxBradyOffset.TabIndex = 65;
            this.textBoxBradyOffset.Text = "^99";
            // 
            // textBoxAFIBonset
            // 
            this.textBoxAFIBonset.Enabled = false;
            this.textBoxAFIBonset.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxAFIBonset.Location = new System.Drawing.Point(423, 83);
            this.textBoxAFIBonset.Name = "textBoxAFIBonset";
            this.textBoxAFIBonset.ReadOnly = true;
            this.textBoxAFIBonset.Size = new System.Drawing.Size(40, 23);
            this.textBoxAFIBonset.TabIndex = 78;
            this.textBoxAFIBonset.Visible = false;
            // 
            // textBoxBradyDuration
            // 
            this.textBoxBradyDuration.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxBradyDuration.Location = new System.Drawing.Point(594, 31);
            this.textBoxBradyDuration.Name = "textBoxBradyDuration";
            this.textBoxBradyDuration.Size = new System.Drawing.Size(40, 23);
            this.textBoxBradyDuration.TabIndex = 66;
            this.textBoxBradyDuration.Text = "^99";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Arial", 10F);
            this.label19.ForeColor = System.Drawing.Color.DarkBlue;
            this.label19.Location = new System.Drawing.Point(373, 86);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(32, 16);
            this.label19.TabIndex = 77;
            this.label19.Text = "Afib";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 10F);
            this.label7.Location = new System.Drawing.Point(467, 34);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 16);
            this.label7.TabIndex = 67;
            this.label7.Text = "bpm";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 10F);
            this.label6.ForeColor = System.Drawing.Color.DarkBlue;
            this.label6.Location = new System.Drawing.Point(637, 60);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 16);
            this.label6.TabIndex = 76;
            this.label6.Text = "sec";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 10F);
            this.label8.ForeColor = System.Drawing.Color.DarkBlue;
            this.label8.Location = new System.Drawing.Point(551, 34);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 16);
            this.label8.TabIndex = 68;
            this.label8.Text = "bpm";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 10F);
            this.label11.ForeColor = System.Drawing.Color.DarkBlue;
            this.label11.Location = new System.Drawing.Point(551, 60);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(35, 16);
            this.label11.TabIndex = 75;
            this.label11.Text = "bpm";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 10F);
            this.label9.Location = new System.Drawing.Point(637, 34);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(30, 16);
            this.label9.TabIndex = 69;
            this.label9.Text = "sec";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 10F);
            this.label12.Location = new System.Drawing.Point(467, 60);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(35, 16);
            this.label12.TabIndex = 74;
            this.label12.Text = "bpm";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 10F);
            this.label14.Location = new System.Drawing.Point(373, 60);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(45, 16);
            this.label14.TabIndex = 70;
            this.label14.Text = "Tachy";
            // 
            // textBoxTachyDuration
            // 
            this.textBoxTachyDuration.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxTachyDuration.Location = new System.Drawing.Point(594, 57);
            this.textBoxTachyDuration.Name = "textBoxTachyDuration";
            this.textBoxTachyDuration.Size = new System.Drawing.Size(40, 23);
            this.textBoxTachyDuration.TabIndex = 73;
            this.textBoxTachyDuration.Text = "^99";
            // 
            // textBoxTachyOnset
            // 
            this.textBoxTachyOnset.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxTachyOnset.Location = new System.Drawing.Point(423, 57);
            this.textBoxTachyOnset.Name = "textBoxTachyOnset";
            this.textBoxTachyOnset.Size = new System.Drawing.Size(40, 23);
            this.textBoxTachyOnset.TabIndex = 71;
            this.textBoxTachyOnset.Text = "^99";
            // 
            // textBoxTachyOffset
            // 
            this.textBoxTachyOffset.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxTachyOffset.Location = new System.Drawing.Point(509, 57);
            this.textBoxTachyOffset.Name = "textBoxTachyOffset";
            this.textBoxTachyOffset.Size = new System.Drawing.Size(40, 23);
            this.textBoxTachyOffset.TabIndex = 72;
            this.textBoxTachyOffset.Text = "^99";
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.MediumPurple;
            this.panel8.Controls.Add(this.label70);
            this.panel8.Controls.Add(this.label10);
            this.panel8.Controls.Add(this.labelServerSettings);
            this.panel8.Controls.Add(this.label1);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(1196, 25);
            this.panel8.TabIndex = 68;
            // 
            // label70
            // 
            this.label70.Dock = System.Windows.Forms.DockStyle.Left;
            this.label70.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label70.ForeColor = System.Drawing.Color.White;
            this.label70.Location = new System.Drawing.Point(679, 0);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(157, 25);
            this.label70.TabIndex = 185;
            this.label70.Text = "Report settings";
            this.label70.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label10
            // 
            this.label10.Dock = System.Windows.Forms.DockStyle.Left;
            this.label10.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(374, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(305, 25);
            this.label10.TabIndex = 184;
            this.label10.Text = "Triggers";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelServerSettings
            // 
            this.labelServerSettings.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelServerSettings.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelServerSettings.ForeColor = System.Drawing.Color.White;
            this.labelServerSettings.Location = new System.Drawing.Point(998, 0);
            this.labelServerSettings.Name = "labelServerSettings";
            this.labelServerSettings.Size = new System.Drawing.Size(198, 25);
            this.labelServerSettings.TabIndex = 183;
            this.labelServerSettings.Text = "Device Server Settings";
            this.labelServerSettings.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelServerSettings.Click += new System.EventHandler(this.labelServerSettings_Click);
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(374, 25);
            this.label1.TabIndex = 180;
            this.label1.Text = "Action details";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.SlateBlue;
            this.panel3.Controls.Add(this.labelLoadDefaults);
            this.panel3.Controls.Add(this.labelLoadSettings);
            this.panel3.Controls.Add(this.label62);
            this.panel3.Controls.Add(this.labelSaveAs);
            this.panel3.Controls.Add(this.labelSaveToSdcard);
            this.panel3.Controls.Add(this.labelSaveToServer);
            this.panel3.Controls.Add(this.labelClose);
            this.panel3.Controls.Add(this.labelLoadCurrent);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 627);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1196, 30);
            this.panel3.TabIndex = 2;
            // 
            // labelLoadDefaults
            // 
            this.labelLoadDefaults.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelLoadDefaults.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelLoadDefaults.ForeColor = System.Drawing.Color.White;
            this.labelLoadDefaults.Location = new System.Drawing.Point(301, 0);
            this.labelLoadDefaults.Name = "labelLoadDefaults";
            this.labelLoadDefaults.Size = new System.Drawing.Size(118, 30);
            this.labelLoadDefaults.TabIndex = 190;
            this.labelLoadDefaults.Text = "Load Defaults";
            this.labelLoadDefaults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelLoadDefaults.Click += new System.EventHandler(this.labelLoadDefaults_Click);
            // 
            // labelLoadSettings
            // 
            this.labelLoadSettings.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelLoadSettings.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelLoadSettings.ForeColor = System.Drawing.Color.White;
            this.labelLoadSettings.Location = new System.Drawing.Point(155, 0);
            this.labelLoadSettings.Name = "labelLoadSettings";
            this.labelLoadSettings.Size = new System.Drawing.Size(146, 30);
            this.labelLoadSettings.TabIndex = 185;
            this.labelLoadSettings.Text = "Load from File";
            this.labelLoadSettings.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelLoadSettings.Click += new System.EventHandler(this.labelLoadSettings_Click_1);
            // 
            // label62
            // 
            this.label62.Dock = System.Windows.Forms.DockStyle.Right;
            this.label62.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label62.ForeColor = System.Drawing.Color.White;
            this.label62.Location = new System.Drawing.Point(513, 0);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(137, 30);
            this.label62.TabIndex = 189;
            this.label62.Text = "Save as Default";
            this.label62.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label62.Click += new System.EventHandler(this.label62_Click);
            // 
            // labelSaveAs
            // 
            this.labelSaveAs.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSaveAs.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelSaveAs.ForeColor = System.Drawing.Color.White;
            this.labelSaveAs.Location = new System.Drawing.Point(650, 0);
            this.labelSaveAs.Name = "labelSaveAs";
            this.labelSaveAs.Size = new System.Drawing.Size(131, 30);
            this.labelSaveAs.TabIndex = 184;
            this.labelSaveAs.Text = "Save to File";
            this.labelSaveAs.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelSaveAs.Click += new System.EventHandler(this.labelSaveAs_Click);
            // 
            // labelSaveToSdcard
            // 
            this.labelSaveToSdcard.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSaveToSdcard.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelSaveToSdcard.ForeColor = System.Drawing.Color.White;
            this.labelSaveToSdcard.Location = new System.Drawing.Point(781, 0);
            this.labelSaveToSdcard.Name = "labelSaveToSdcard";
            this.labelSaveToSdcard.Size = new System.Drawing.Size(141, 30);
            this.labelSaveToSdcard.TabIndex = 182;
            this.labelSaveToSdcard.Text = "Save to SDcard";
            this.labelSaveToSdcard.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelSaveToSdcard.Click += new System.EventHandler(this.labelSaveToSdcard_Click);
            // 
            // labelSaveToServer
            // 
            this.labelSaveToServer.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSaveToServer.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelSaveToServer.ForeColor = System.Drawing.Color.White;
            this.labelSaveToServer.Location = new System.Drawing.Point(922, 0);
            this.labelSaveToServer.Name = "labelSaveToServer";
            this.labelSaveToServer.Size = new System.Drawing.Size(138, 30);
            this.labelSaveToServer.TabIndex = 2;
            this.labelSaveToServer.Text = "Save to Server";
            this.labelSaveToServer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelSaveToServer.Click += new System.EventHandler(this.labelSaveToServer_Click);
            // 
            // labelClose
            // 
            this.labelClose.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelClose.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelClose.ForeColor = System.Drawing.Color.White;
            this.labelClose.Location = new System.Drawing.Point(1060, 0);
            this.labelClose.Name = "labelClose";
            this.labelClose.Size = new System.Drawing.Size(136, 30);
            this.labelClose.TabIndex = 183;
            this.labelClose.Text = "Exit - No Save";
            this.labelClose.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelClose.Click += new System.EventHandler(this.labelClose_Click);
            // 
            // labelLoadCurrent
            // 
            this.labelLoadCurrent.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelLoadCurrent.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelLoadCurrent.ForeColor = System.Drawing.Color.White;
            this.labelLoadCurrent.Location = new System.Drawing.Point(0, 0);
            this.labelLoadCurrent.Name = "labelLoadCurrent";
            this.labelLoadCurrent.Size = new System.Drawing.Size(155, 30);
            this.labelLoadCurrent.TabIndex = 188;
            this.labelLoadCurrent.Text = "Reload Settings";
            this.labelLoadCurrent.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelLoadCurrent.Click += new System.EventHandler(this.labelLoadCurrent_Click_1);
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.Filter = "TZ Setting|*.tzs";
            this.saveFileDialog.Title = "Save TZ setting";
            // 
            // openFileDialogTZS
            // 
            this.openFileDialogTZS.FileName = "default.tzs";
            this.openFileDialogTZS.Filter = "TZ Setting|*.tzs";
            this.openFileDialogTZS.Title = "Select TZ Setting file";
            // 
            // openFileDialogTxt
            // 
            this.openFileDialogTxt.FileName = "input.txt";
            this.openFileDialogTxt.Filter = "TZ Setting Import|*.txt";
            this.openFileDialogTxt.Title = "Select TZ Setting import file";
            // 
            // panelAutoCollect
            // 
            this.panelAutoCollect.Controls.Add(this.panel22);
            this.panelAutoCollect.Controls.Add(this.panel7);
            this.panelAutoCollect.Controls.Add(this.panel6);
            this.panelAutoCollect.Controls.Add(this.panel31);
            this.panelAutoCollect.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelAutoCollect.Location = new System.Drawing.Point(0, 657);
            this.panelAutoCollect.Name = "panelAutoCollect";
            this.panelAutoCollect.Size = new System.Drawing.Size(1196, 114);
            this.panelAutoCollect.TabIndex = 188;
            // 
            // panel22
            // 
            this.panel22.Controls.Add(this.panel23);
            this.panel22.Controls.Add(this.buttonTzrRefreshInfo);
            this.panel22.Controls.Add(this.panel26);
            this.panel22.Controls.Add(this.buttonTzrCollectTest);
            this.panel22.Controls.Add(this.panel27);
            this.panel22.Controls.Add(this.panel28);
            this.panel22.Controls.Add(this.label97);
            this.panel22.Controls.Add(this.listBoxTzrStopMode);
            this.panel22.Controls.Add(this.label98);
            this.panel22.Controls.Add(this.panel29);
            this.panel22.Controls.Add(this.label100);
            this.panel22.Controls.Add(this.listBoxTzrStartMode);
            this.panel22.Controls.Add(this.label101);
            this.panel22.Controls.Add(this.panel30);
            this.panel22.Controls.Add(this.listBoxTzrStudyMode);
            this.panel22.Controls.Add(this.checkBoxCollectTzrEnabled);
            this.panel22.Controls.Add(this.label104);
            this.panel22.Controls.Add(this.buttonTzrModeSave);
            this.panel22.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel22.Location = new System.Drawing.Point(0, 65);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(1196, 45);
            this.panel22.TabIndex = 188;
            // 
            // panel23
            // 
            this.panel23.Controls.Add(this.panel24);
            this.panel23.Controls.Add(this.panel25);
            this.panel23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel23.Location = new System.Drawing.Point(859, 0);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(337, 45);
            this.panel23.TabIndex = 24;
            // 
            // panel24
            // 
            this.panel24.Controls.Add(this.labelTzrLastScan);
            this.panel24.Controls.Add(this.label93);
            this.panel24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel24.Location = new System.Drawing.Point(0, 19);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(337, 26);
            this.panel24.TabIndex = 24;
            // 
            // labelTzrLastScan
            // 
            this.labelTzrLastScan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelTzrLastScan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelTzrLastScan.Location = new System.Drawing.Point(58, 0);
            this.labelTzrLastScan.Name = "labelTzrLastScan";
            this.labelTzrLastScan.Size = new System.Drawing.Size(279, 26);
            this.labelTzrLastScan.TabIndex = 10;
            this.labelTzrLastScan.Text = "^YYYYMMDDHHmmss xx missing scp  hgfjslhsghshfjghj";
            this.labelTzrLastScan.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label93
            // 
            this.label93.Dock = System.Windows.Forms.DockStyle.Left;
            this.label93.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label93.Location = new System.Drawing.Point(0, 0);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(58, 26);
            this.label93.TabIndex = 11;
            this.label93.Text = "TZR scan:";
            this.label93.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // panel25
            // 
            this.panel25.Controls.Add(this.labelTzrSavedInfo);
            this.panel25.Controls.Add(this.label95);
            this.panel25.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel25.Location = new System.Drawing.Point(0, 0);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(337, 19);
            this.panel25.TabIndex = 23;
            // 
            // labelTzrSavedInfo
            // 
            this.labelTzrSavedInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelTzrSavedInfo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelTzrSavedInfo.Location = new System.Drawing.Point(58, 0);
            this.labelTzrSavedInfo.Name = "labelTzrSavedInfo";
            this.labelTzrSavedInfo.Size = new System.Drawing.Size(279, 19);
            this.labelTzrSavedInfo.TabIndex = 10;
            this.labelTzrSavedInfo.Text = "^YYYYMMDDHHmmss";
            this.labelTzrSavedInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label95
            // 
            this.label95.Dock = System.Windows.Forms.DockStyle.Left;
            this.label95.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label95.Location = new System.Drawing.Point(0, 0);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(58, 19);
            this.label95.TabIndex = 11;
            this.label95.Text = "Saved:";
            this.label95.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // buttonTzrRefreshInfo
            // 
            this.buttonTzrRefreshInfo.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonTzrRefreshInfo.Font = new System.Drawing.Font("Webdings", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.buttonTzrRefreshInfo.Location = new System.Drawing.Point(821, 0);
            this.buttonTzrRefreshInfo.Name = "buttonTzrRefreshInfo";
            this.buttonTzrRefreshInfo.Size = new System.Drawing.Size(38, 45);
            this.buttonTzrRefreshInfo.TabIndex = 26;
            this.buttonTzrRefreshInfo.Text = "q";
            this.buttonTzrRefreshInfo.UseVisualStyleBackColor = true;
            this.buttonTzrRefreshInfo.Click += new System.EventHandler(this.buttonTzrRefreshInfo_Click);
            // 
            // panel26
            // 
            this.panel26.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel26.Location = new System.Drawing.Point(810, 0);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(11, 45);
            this.panel26.TabIndex = 23;
            // 
            // buttonTzrCollectTest
            // 
            this.buttonTzrCollectTest.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonTzrCollectTest.Location = new System.Drawing.Point(774, 0);
            this.buttonTzrCollectTest.Name = "buttonTzrCollectTest";
            this.buttonTzrCollectTest.Size = new System.Drawing.Size(36, 45);
            this.buttonTzrCollectTest.TabIndex = 19;
            this.buttonTzrCollectTest.Text = "Test";
            this.buttonTzrCollectTest.UseVisualStyleBackColor = true;
            this.buttonTzrCollectTest.Click += new System.EventHandler(this.buttonTzrCollectTest_Click);
            // 
            // panel27
            // 
            this.panel27.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel27.Location = new System.Drawing.Point(764, 0);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(10, 45);
            this.panel27.TabIndex = 22;
            // 
            // panel28
            // 
            this.panel28.Controls.Add(this.textBoxTzrStopNr);
            this.panel28.Controls.Add(this.labelTzrStopLast);
            this.panel28.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel28.Location = new System.Drawing.Point(708, 0);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(56, 45);
            this.panel28.TabIndex = 14;
            // 
            // textBoxTzrStopNr
            // 
            this.textBoxTzrStopNr.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textBoxTzrStopNr.Location = new System.Drawing.Point(0, 25);
            this.textBoxTzrStopNr.Name = "textBoxTzrStopNr";
            this.textBoxTzrStopNr.Size = new System.Drawing.Size(56, 20);
            this.textBoxTzrStopNr.TabIndex = 10;
            this.textBoxTzrStopNr.Text = "^99999";
            // 
            // labelTzrStopLast
            // 
            this.labelTzrStopLast.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelTzrStopLast.Location = new System.Drawing.Point(0, 0);
            this.labelTzrStopLast.Name = "labelTzrStopLast";
            this.labelTzrStopLast.Size = new System.Drawing.Size(56, 20);
            this.labelTzrStopLast.TabIndex = 9;
            this.labelTzrStopLast.Text = "^999999";
            this.labelTzrStopLast.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label97
            // 
            this.label97.Dock = System.Windows.Forms.DockStyle.Left;
            this.label97.Location = new System.Drawing.Point(686, 0);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(22, 45);
            this.label97.TabIndex = 13;
            this.label97.Text = "=";
            this.label97.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // listBoxTzrStopMode
            // 
            this.listBoxTzrStopMode.Dock = System.Windows.Forms.DockStyle.Left;
            this.listBoxTzrStopMode.FormattingEnabled = true;
            this.listBoxTzrStopMode.Items.AddRange(new object[] {
            "End",
            "Last",
            "Nr"});
            this.listBoxTzrStopMode.Location = new System.Drawing.Point(635, 0);
            this.listBoxTzrStopMode.Name = "listBoxTzrStopMode";
            this.listBoxTzrStopMode.Size = new System.Drawing.Size(51, 45);
            this.listBoxTzrStopMode.TabIndex = 12;
            // 
            // label98
            // 
            this.label98.Dock = System.Windows.Forms.DockStyle.Left;
            this.label98.Location = new System.Drawing.Point(588, 0);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(47, 45);
            this.label98.TabIndex = 11;
            this.label98.Text = "Stop at file ID";
            this.label98.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel29
            // 
            this.panel29.Controls.Add(this.labelTzrStartFirst);
            this.panel29.Controls.Add(this.textBoxTzrStartNr);
            this.panel29.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel29.Location = new System.Drawing.Point(535, 0);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(53, 45);
            this.panel29.TabIndex = 21;
            // 
            // labelTzrStartFirst
            // 
            this.labelTzrStartFirst.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelTzrStartFirst.Location = new System.Drawing.Point(0, 0);
            this.labelTzrStartFirst.Name = "labelTzrStartFirst";
            this.labelTzrStartFirst.Size = new System.Drawing.Size(53, 20);
            this.labelTzrStartFirst.TabIndex = 8;
            this.labelTzrStartFirst.Text = "^999999";
            this.labelTzrStartFirst.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxTzrStartNr
            // 
            this.textBoxTzrStartNr.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textBoxTzrStartNr.Location = new System.Drawing.Point(0, 25);
            this.textBoxTzrStartNr.Name = "textBoxTzrStartNr";
            this.textBoxTzrStartNr.Size = new System.Drawing.Size(53, 20);
            this.textBoxTzrStartNr.TabIndex = 5;
            this.textBoxTzrStartNr.Text = "^99999";
            // 
            // label100
            // 
            this.label100.Dock = System.Windows.Forms.DockStyle.Left;
            this.label100.Location = new System.Drawing.Point(513, 0);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(22, 45);
            this.label100.TabIndex = 9;
            this.label100.Text = "=";
            this.label100.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // listBoxTzrStartMode
            // 
            this.listBoxTzrStartMode.Dock = System.Windows.Forms.DockStyle.Left;
            this.listBoxTzrStartMode.FormattingEnabled = true;
            this.listBoxTzrStartMode.Items.AddRange(new object[] {
            "Start",
            "First",
            "Nr"});
            this.listBoxTzrStartMode.Location = new System.Drawing.Point(462, 0);
            this.listBoxTzrStartMode.Name = "listBoxTzrStartMode";
            this.listBoxTzrStartMode.Size = new System.Drawing.Size(51, 45);
            this.listBoxTzrStartMode.TabIndex = 8;
            // 
            // label101
            // 
            this.label101.Dock = System.Windows.Forms.DockStyle.Left;
            this.label101.Location = new System.Drawing.Point(415, 0);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(47, 45);
            this.label101.TabIndex = 7;
            this.label101.Text = "Start at file ID";
            this.label101.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel30
            // 
            this.panel30.Controls.Add(this.label102);
            this.panel30.Controls.Add(this.labelTzrStudyNr);
            this.panel30.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel30.Location = new System.Drawing.Point(362, 0);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(53, 45);
            this.panel30.TabIndex = 20;
            // 
            // label102
            // 
            this.label102.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label102.Location = new System.Drawing.Point(0, 3);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(53, 20);
            this.label102.TabIndex = 6;
            this.label102.Text = "Study ";
            this.label102.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label102.Click += new System.EventHandler(this.label102_Click);
            // 
            // labelTzrStudyNr
            // 
            this.labelTzrStudyNr.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelTzrStudyNr.Location = new System.Drawing.Point(0, 23);
            this.labelTzrStudyNr.Name = "labelTzrStudyNr";
            this.labelTzrStudyNr.Size = new System.Drawing.Size(53, 22);
            this.labelTzrStudyNr.TabIndex = 7;
            this.labelTzrStudyNr.Text = "^999999";
            this.labelTzrStudyNr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelTzrStudyNr.Click += new System.EventHandler(this.labelTzrStudyNr_Click);
            // 
            // listBoxTzrStudyMode
            // 
            this.listBoxTzrStudyMode.Dock = System.Windows.Forms.DockStyle.Left;
            this.listBoxTzrStudyMode.FormattingEnabled = true;
            this.listBoxTzrStudyMode.Items.AddRange(new object[] {
            "No",
            "All",
            "One"});
            this.listBoxTzrStudyMode.Location = new System.Drawing.Point(319, 0);
            this.listBoxTzrStudyMode.Name = "listBoxTzrStudyMode";
            this.listBoxTzrStudyMode.Size = new System.Drawing.Size(43, 45);
            this.listBoxTzrStudyMode.TabIndex = 3;
            // 
            // checkBoxCollectTzrEnabled
            // 
            this.checkBoxCollectTzrEnabled.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxCollectTzrEnabled.Location = new System.Drawing.Point(215, 0);
            this.checkBoxCollectTzrEnabled.Name = "checkBoxCollectTzrEnabled";
            this.checkBoxCollectTzrEnabled.Size = new System.Drawing.Size(104, 45);
            this.checkBoxCollectTzrEnabled.TabIndex = 1;
            this.checkBoxCollectTzrEnabled.Text = "Auto Collect \r\nMissing \r\nTrend TZR files";
            this.checkBoxCollectTzrEnabled.UseVisualStyleBackColor = true;
            // 
            // label104
            // 
            this.label104.Dock = System.Windows.Forms.DockStyle.Left;
            this.label104.Location = new System.Drawing.Point(200, 0);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(15, 45);
            this.label104.TabIndex = 0;
            this.label104.Text = " :";
            this.label104.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // buttonTzrModeSave
            // 
            this.buttonTzrModeSave.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonTzrModeSave.Location = new System.Drawing.Point(0, 0);
            this.buttonTzrModeSave.Name = "buttonTzrModeSave";
            this.buttonTzrModeSave.Size = new System.Drawing.Size(200, 45);
            this.buttonTzrModeSave.TabIndex = 18;
            this.buttonTzrModeSave.Text = "Save Auto collect TREND TZR  mode";
            this.buttonTzrModeSave.UseVisualStyleBackColor = true;
            this.buttonTzrModeSave.Click += new System.EventHandler(this.buttonTzrModeSave_Click);
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.panel18);
            this.panel7.Controls.Add(this.buttonScpRefreshInfo);
            this.panel7.Controls.Add(this.panel17);
            this.panel7.Controls.Add(this.buttonScpCollectTest);
            this.panel7.Controls.Add(this.panel16);
            this.panel7.Controls.Add(this.panel13);
            this.panel7.Controls.Add(this.label84);
            this.panel7.Controls.Add(this.listBoxScpStopMode);
            this.panel7.Controls.Add(this.label85);
            this.panel7.Controls.Add(this.panel15);
            this.panel7.Controls.Add(this.label83);
            this.panel7.Controls.Add(this.listBoxScpStartMode);
            this.panel7.Controls.Add(this.label82);
            this.panel7.Controls.Add(this.panel14);
            this.panel7.Controls.Add(this.listBoxScpStudyMode);
            this.panel7.Controls.Add(this.checkBoxCollectScpEnabled);
            this.panel7.Controls.Add(this.label79);
            this.panel7.Controls.Add(this.buttonScpModeSave);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 20);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1196, 45);
            this.panel7.TabIndex = 187;
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.panel20);
            this.panel18.Controls.Add(this.panel19);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel18.Location = new System.Drawing.Point(859, 0);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(337, 45);
            this.panel18.TabIndex = 24;
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.labelScpLastScan);
            this.panel20.Controls.Add(this.label91);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel20.Location = new System.Drawing.Point(0, 19);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(337, 26);
            this.panel20.TabIndex = 24;
            // 
            // labelScpLastScan
            // 
            this.labelScpLastScan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelScpLastScan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelScpLastScan.Location = new System.Drawing.Point(58, 0);
            this.labelScpLastScan.Name = "labelScpLastScan";
            this.labelScpLastScan.Size = new System.Drawing.Size(279, 26);
            this.labelScpLastScan.TabIndex = 10;
            this.labelScpLastScan.Text = "^YYYYMMDDHHmmss xx missing scp";
            this.labelScpLastScan.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label91
            // 
            this.label91.Dock = System.Windows.Forms.DockStyle.Left;
            this.label91.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label91.Location = new System.Drawing.Point(0, 0);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(58, 26);
            this.label91.TabIndex = 11;
            this.label91.Text = "SCP scan:";
            this.label91.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.labelScpSavedInfo);
            this.panel19.Controls.Add(this.label87);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel19.Location = new System.Drawing.Point(0, 0);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(337, 19);
            this.panel19.TabIndex = 23;
            // 
            // labelScpSavedInfo
            // 
            this.labelScpSavedInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelScpSavedInfo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelScpSavedInfo.Location = new System.Drawing.Point(58, 0);
            this.labelScpSavedInfo.Name = "labelScpSavedInfo";
            this.labelScpSavedInfo.Size = new System.Drawing.Size(279, 19);
            this.labelScpSavedInfo.TabIndex = 10;
            this.labelScpSavedInfo.Text = "^YYYYMMDDHHmmss";
            this.labelScpSavedInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label87
            // 
            this.label87.Dock = System.Windows.Forms.DockStyle.Left;
            this.label87.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label87.Location = new System.Drawing.Point(0, 0);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(58, 19);
            this.label87.TabIndex = 11;
            this.label87.Text = "Saved:";
            this.label87.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // buttonScpRefreshInfo
            // 
            this.buttonScpRefreshInfo.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonScpRefreshInfo.Font = new System.Drawing.Font("Webdings", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.buttonScpRefreshInfo.Location = new System.Drawing.Point(821, 0);
            this.buttonScpRefreshInfo.Name = "buttonScpRefreshInfo";
            this.buttonScpRefreshInfo.Size = new System.Drawing.Size(38, 45);
            this.buttonScpRefreshInfo.TabIndex = 25;
            this.buttonScpRefreshInfo.Text = "q";
            this.buttonScpRefreshInfo.UseVisualStyleBackColor = true;
            this.buttonScpRefreshInfo.Click += new System.EventHandler(this.buttonScpRefreshInfo_Click);
            // 
            // panel17
            // 
            this.panel17.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel17.Location = new System.Drawing.Point(810, 0);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(11, 45);
            this.panel17.TabIndex = 23;
            // 
            // buttonScpCollectTest
            // 
            this.buttonScpCollectTest.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonScpCollectTest.Location = new System.Drawing.Point(774, 0);
            this.buttonScpCollectTest.Name = "buttonScpCollectTest";
            this.buttonScpCollectTest.Size = new System.Drawing.Size(36, 45);
            this.buttonScpCollectTest.TabIndex = 19;
            this.buttonScpCollectTest.Text = "Test";
            this.buttonScpCollectTest.UseVisualStyleBackColor = true;
            this.buttonScpCollectTest.Click += new System.EventHandler(this.buttonScpCollectTest_Click);
            // 
            // panel16
            // 
            this.panel16.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel16.Location = new System.Drawing.Point(764, 0);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(10, 45);
            this.panel16.TabIndex = 22;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.textBoxScpStopNr);
            this.panel13.Controls.Add(this.labelScpStopLast);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel13.Location = new System.Drawing.Point(708, 0);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(56, 45);
            this.panel13.TabIndex = 14;
            // 
            // textBoxScpStopNr
            // 
            this.textBoxScpStopNr.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textBoxScpStopNr.Location = new System.Drawing.Point(0, 25);
            this.textBoxScpStopNr.Name = "textBoxScpStopNr";
            this.textBoxScpStopNr.Size = new System.Drawing.Size(56, 20);
            this.textBoxScpStopNr.TabIndex = 10;
            this.textBoxScpStopNr.Text = "^99999";
            // 
            // labelScpStopLast
            // 
            this.labelScpStopLast.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelScpStopLast.Location = new System.Drawing.Point(0, 0);
            this.labelScpStopLast.Name = "labelScpStopLast";
            this.labelScpStopLast.Size = new System.Drawing.Size(56, 20);
            this.labelScpStopLast.TabIndex = 9;
            this.labelScpStopLast.Text = "^999999";
            this.labelScpStopLast.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label84
            // 
            this.label84.Dock = System.Windows.Forms.DockStyle.Left;
            this.label84.Location = new System.Drawing.Point(686, 0);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(22, 45);
            this.label84.TabIndex = 13;
            this.label84.Text = "=";
            this.label84.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // listBoxScpStopMode
            // 
            this.listBoxScpStopMode.Dock = System.Windows.Forms.DockStyle.Left;
            this.listBoxScpStopMode.FormattingEnabled = true;
            this.listBoxScpStopMode.Items.AddRange(new object[] {
            "End",
            "Last",
            "Nr"});
            this.listBoxScpStopMode.Location = new System.Drawing.Point(635, 0);
            this.listBoxScpStopMode.Name = "listBoxScpStopMode";
            this.listBoxScpStopMode.Size = new System.Drawing.Size(51, 45);
            this.listBoxScpStopMode.TabIndex = 12;
            // 
            // label85
            // 
            this.label85.Dock = System.Windows.Forms.DockStyle.Left;
            this.label85.Location = new System.Drawing.Point(588, 0);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(47, 45);
            this.label85.TabIndex = 11;
            this.label85.Text = "Stop at file ID";
            this.label85.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.labelScpStartFirst);
            this.panel15.Controls.Add(this.textBoxScpStartNr);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel15.Location = new System.Drawing.Point(535, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(53, 45);
            this.panel15.TabIndex = 21;
            // 
            // labelScpStartFirst
            // 
            this.labelScpStartFirst.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelScpStartFirst.Location = new System.Drawing.Point(0, 0);
            this.labelScpStartFirst.Name = "labelScpStartFirst";
            this.labelScpStartFirst.Size = new System.Drawing.Size(53, 20);
            this.labelScpStartFirst.TabIndex = 8;
            this.labelScpStartFirst.Text = "^999999";
            this.labelScpStartFirst.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxScpStartNr
            // 
            this.textBoxScpStartNr.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textBoxScpStartNr.Location = new System.Drawing.Point(0, 25);
            this.textBoxScpStartNr.Name = "textBoxScpStartNr";
            this.textBoxScpStartNr.Size = new System.Drawing.Size(53, 20);
            this.textBoxScpStartNr.TabIndex = 5;
            this.textBoxScpStartNr.Text = "^99999";
            // 
            // label83
            // 
            this.label83.Dock = System.Windows.Forms.DockStyle.Left;
            this.label83.Location = new System.Drawing.Point(513, 0);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(22, 45);
            this.label83.TabIndex = 9;
            this.label83.Text = "=";
            this.label83.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // listBoxScpStartMode
            // 
            this.listBoxScpStartMode.Dock = System.Windows.Forms.DockStyle.Left;
            this.listBoxScpStartMode.FormattingEnabled = true;
            this.listBoxScpStartMode.Items.AddRange(new object[] {
            "Start",
            "First",
            "Nr"});
            this.listBoxScpStartMode.Location = new System.Drawing.Point(462, 0);
            this.listBoxScpStartMode.Name = "listBoxScpStartMode";
            this.listBoxScpStartMode.Size = new System.Drawing.Size(51, 45);
            this.listBoxScpStartMode.TabIndex = 8;
            // 
            // label82
            // 
            this.label82.Dock = System.Windows.Forms.DockStyle.Left;
            this.label82.Location = new System.Drawing.Point(415, 0);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(47, 45);
            this.label82.TabIndex = 7;
            this.label82.Text = "Start at file ID";
            this.label82.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.label80);
            this.panel14.Controls.Add(this.labelScpStudyNr);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel14.Location = new System.Drawing.Point(362, 0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(53, 45);
            this.panel14.TabIndex = 20;
            // 
            // label80
            // 
            this.label80.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label80.Location = new System.Drawing.Point(0, 3);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(53, 20);
            this.label80.TabIndex = 6;
            this.label80.Text = "Study ";
            this.label80.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label80.Click += new System.EventHandler(this.label80_Click);
            // 
            // labelScpStudyNr
            // 
            this.labelScpStudyNr.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelScpStudyNr.Location = new System.Drawing.Point(0, 23);
            this.labelScpStudyNr.Name = "labelScpStudyNr";
            this.labelScpStudyNr.Size = new System.Drawing.Size(53, 22);
            this.labelScpStudyNr.TabIndex = 7;
            this.labelScpStudyNr.Text = "^999999";
            this.labelScpStudyNr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelScpStudyNr.Click += new System.EventHandler(this.labelScpStudyNr_Click);
            // 
            // listBoxScpStudyMode
            // 
            this.listBoxScpStudyMode.Dock = System.Windows.Forms.DockStyle.Left;
            this.listBoxScpStudyMode.FormattingEnabled = true;
            this.listBoxScpStudyMode.Items.AddRange(new object[] {
            "No",
            "All",
            "One"});
            this.listBoxScpStudyMode.Location = new System.Drawing.Point(319, 0);
            this.listBoxScpStudyMode.Name = "listBoxScpStudyMode";
            this.listBoxScpStudyMode.Size = new System.Drawing.Size(43, 45);
            this.listBoxScpStudyMode.TabIndex = 3;
            // 
            // checkBoxCollectScpEnabled
            // 
            this.checkBoxCollectScpEnabled.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxCollectScpEnabled.Location = new System.Drawing.Point(215, 0);
            this.checkBoxCollectScpEnabled.Name = "checkBoxCollectScpEnabled";
            this.checkBoxCollectScpEnabled.Size = new System.Drawing.Size(104, 45);
            this.checkBoxCollectScpEnabled.TabIndex = 1;
            this.checkBoxCollectScpEnabled.Text = "Auto Collect \r\nMissing \r\nECG SCP files";
            this.checkBoxCollectScpEnabled.UseVisualStyleBackColor = true;
            // 
            // label79
            // 
            this.label79.Dock = System.Windows.Forms.DockStyle.Left;
            this.label79.Location = new System.Drawing.Point(200, 0);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(15, 45);
            this.label79.TabIndex = 0;
            this.label79.Text = " :";
            this.label79.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // buttonScpModeSave
            // 
            this.buttonScpModeSave.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonScpModeSave.Location = new System.Drawing.Point(0, 0);
            this.buttonScpModeSave.Name = "buttonScpModeSave";
            this.buttonScpModeSave.Size = new System.Drawing.Size(200, 45);
            this.buttonScpModeSave.TabIndex = 18;
            this.buttonScpModeSave.Text = "Save Auto collect ECG SCP  mode";
            this.buttonScpModeSave.UseVisualStyleBackColor = true;
            this.buttonScpModeSave.Click += new System.EventHandler(this.buttonScpModeSave_Click);
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.label88);
            this.panel6.Controls.Add(this.label81);
            this.panel6.Controls.Add(this.labelStudyActive);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 4);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1196, 16);
            this.panel6.TabIndex = 190;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Dock = System.Windows.Forms.DockStyle.Left;
            this.label88.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.label88.Location = new System.Drawing.Point(384, 0);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(241, 14);
            this.label88.TabIndex = 195;
            this.label88.Text = "...New, to be tested as single auto collect...";
            this.label88.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Dock = System.Windows.Forms.DockStyle.Left;
            this.label81.Font = new System.Drawing.Font("Arial", 8F);
            this.label81.Location = new System.Drawing.Point(0, 0);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(384, 14);
            this.label81.TabIndex = 194;
            this.label81.Text = "Auto collect SCP works when file number is sequential, End will be estimated .";
            this.label81.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStudyActive
            // 
            this.labelStudyActive.AutoSize = true;
            this.labelStudyActive.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelStudyActive.Font = new System.Drawing.Font("Arial", 10F);
            this.labelStudyActive.Location = new System.Drawing.Point(896, 0);
            this.labelStudyActive.Name = "labelStudyActive";
            this.labelStudyActive.Size = new System.Drawing.Size(300, 16);
            this.labelStudyActive.TabIndex = 193;
            this.labelStudyActive.Text = "^Study 999999 Active / Ended  yyyymmmdd hh";
            this.labelStudyActive.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelStudyActive.DoubleClick += new System.EventHandler(this.labelStudyActive_DoubleClick);
            // 
            // panel31
            // 
            this.panel31.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel31.Location = new System.Drawing.Point(0, 0);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(1196, 4);
            this.panel31.TabIndex = 189;
            // 
            // timerRefresh
            // 
            this.timerRefresh.Enabled = true;
            this.timerRefresh.Interval = 15000;
            this.timerRefresh.Tick += new System.EventHandler(this.timerRefresh_Tick);
            // 
            // TZmedicalDevSet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1196, 771);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panelAutoCollect);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TZmedicalDevSet";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AeraCT and Clarus M device configuration settings";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panelAutoCollect.ResumeLayout(false);
            this.panel22.ResumeLayout(false);
            this.panel23.ResumeLayout(false);
            this.panel24.ResumeLayout(false);
            this.panel25.ResumeLayout(false);
            this.panel28.ResumeLayout(false);
            this.panel28.PerformLayout();
            this.panel29.ResumeLayout(false);
            this.panel29.PerformLayout();
            this.panel30.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel18.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.panel19.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.panel14.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label labelSaveToServer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxBradyDuration;
        private System.Windows.Forms.TextBox textBoxBradyOffset;
        private System.Windows.Forms.TextBox textBoxBradyOnset;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox textBoxPauseDuration;
        private System.Windows.Forms.TextBox textBoxPauseOffset;
        private System.Windows.Forms.TextBox textBoxPauseOnset;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBoxAFIBduration;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBoxTachyDuration;
        private System.Windows.Forms.TextBox textBoxTachyOffset;
        private System.Windows.Forms.TextBox textBoxTachyOnset;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckBox checkBoxReportAFIBOnset;
        private System.Windows.Forms.CheckBox checkBoxReportAFIBOffset;
        private System.Windows.Forms.CheckBox checkBoxReportTachyOnset;
        private System.Windows.Forms.CheckBox checkBoxReportTachyOffset;
        private System.Windows.Forms.CheckBox checkBoxReportBradyOnset;
        private System.Windows.Forms.CheckBox checkBoxReportBradyOffset;
        private System.Windows.Forms.CheckBox checkBoxReportPause;
        private System.Windows.Forms.CheckBox checkBoxAutoAnswer;
        private System.Windows.Forms.TextBox textBoxPatientID;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkBoxReportLeadDisconnected;
        private System.Windows.Forms.CheckBox checkBoxReportPaceDetected;
        private System.Windows.Forms.CheckBox checkBoxReportFull;
        private System.Windows.Forms.CheckBox checkBoxReportResume;
        private System.Windows.Forms.CheckBox checkBoxReportBreak;
        private System.Windows.Forms.CheckBox checkBoxReportStop;
        private System.Windows.Forms.CheckBox checkBoxReportStart;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.CheckBox checkBoxReportChargingStart;
        private System.Windows.Forms.CheckBox checkBoxReportChargingStop;
        private System.Windows.Forms.CheckBox checkBoxReportBattLow;
        private System.Windows.Forms.CheckBox checkBoxReportPatEvent;
        private System.Windows.Forms.CheckBox checkBoxTransmitIntervalReports;
        private System.Windows.Forms.TextBox textBoxAFIBonset;
        private System.Windows.Forms.TextBox textBoxAFIBoffset;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ComboBox comboBoxAnalysisMask;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.RadioButton radioButtonPacemakerNo;
        private System.Windows.Forms.RadioButton radioButtonPacemakerYes;
        private System.Windows.Forms.Label labelCrossChannelAnalysis;
        private System.Windows.Forms.ComboBox comboBoxCrossCh;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.ComboBox comboBoxTZSampleRate;
        private System.Windows.Forms.TextBox textBoxStudyLength;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.ComboBox comboBoxLowFilter;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.ComboBox comboBoxLeadConfig;
        private System.Windows.Forms.CheckBox checkBoxSyncTime;
        private System.Windows.Forms.CheckBox checkBoxSilentMode;
        private System.Windows.Forms.CheckBox checkBoxVibrOnClick;
        private System.Windows.Forms.CheckBox checkBoxVoiceEnable;
        private System.Windows.Forms.CheckBox checkBoxSMSenable;
        private System.Windows.Forms.TextBox textBoxAct10;
        private System.Windows.Forms.TextBox textBoxAct9;
        private System.Windows.Forms.TextBox textBoxAct8;
        private System.Windows.Forms.TextBox textBoxAct7;
        private System.Windows.Forms.TextBox textBoxAct6;
        private System.Windows.Forms.TextBox textBoxAct5;
        private System.Windows.Forms.TextBox textBoxAct4;
        private System.Windows.Forms.TextBox textBoxAct3;
        private System.Windows.Forms.TextBox textBoxAct2;
        private System.Windows.Forms.TextBox textBoxAct1;
        private System.Windows.Forms.TextBox textBoxSymp10;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.TextBox textBoxSymp9;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TextBox textBoxSymp8;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox textBoxSymp7;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox textBoxSymp6;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox textBoxSymp5;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.TextBox textBoxSymp4;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox textBoxSymp3;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox textBoxSymp2;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox textBoxSymp1;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label labelSaveToSdcard;
        private System.Windows.Forms.Label labelServerSettings;
        private System.Windows.Forms.Button buttonSendAction;
        private System.Windows.Forms.TextBox textBoxMessage;
        private System.Windows.Forms.CheckBox checkBoxMessage;
        private System.Windows.Forms.CheckBox checkBoxStartStudy;
        private System.Windows.Forms.CheckBox checkBoxEndStudy;
        private System.Windows.Forms.CheckBox checkBoxNewSetting;
        private System.Windows.Forms.Label labelClose;
        private System.Windows.Forms.Label labelDeviceSnr;
        private System.Windows.Forms.Label labelActionFileInfo;
        private System.Windows.Forms.Label labelSettingFileInfo;
        private System.Windows.Forms.Label labelActionFileID;
        private System.Windows.Forms.Label labelSettingFileID;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Button buttonRefresh;
        private System.Windows.Forms.CheckBox checkBoxResetFileNumber;
        private System.Windows.Forms.Label labelResult;
        private System.Windows.Forms.Label labelSaveAs;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialogTZS;
        private System.Windows.Forms.OpenFileDialog openFileDialogTxt;
        private System.Windows.Forms.CheckBox checkBoxNewStudy;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label labelEstDelay;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label LabelStudyDays;
        private System.Windows.Forms.Label labelLoadSettings;
        private System.Windows.Forms.Label labelLoadCurrent;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.CheckBox checkBoxUpdateFirmware;
        private System.Windows.Forms.CheckBox checkBoxFormatSD;
        private System.Windows.Forms.CheckBox checkBoxShutdown;
        private System.Windows.Forms.Label labelLoadDefaults;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label labelActionQueue;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.TextBox textBoxEventPreTime;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.TextBox textBoxEventPostTime;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.TextBox textBoxStudyHours;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.ComboBox comboBoxDigHighFilter;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.ComboBox comboBoxNotchFilter;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.ComboBox comboBoxFirstLow;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label labelMessageInfo;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.TextBox textBoxStreamingDelay;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.TextBox textBoxStreamingMode;
        private System.Windows.Forms.CheckBox checkBoxActDiaryOn;
        private System.Windows.Forms.CheckBox checkBoxSymptDiaryOn;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.CheckBox checkBoxSaveToAllDevices;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Button buttonClearAction;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label labelInfoQueue;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Panel panelAutoCollect;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Label labelTzrLastScan;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Label labelTzrSavedInfo;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Button buttonTzrCollectTest;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.TextBox textBoxTzrStopNr;
        private System.Windows.Forms.Label labelTzrStopLast;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.ListBox listBoxTzrStopMode;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.Label labelTzrStartFirst;
        private System.Windows.Forms.TextBox textBoxTzrStartNr;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.ListBox listBoxTzrStartMode;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label labelTzrStudyNr;
        private System.Windows.Forms.ListBox listBoxTzrStudyMode;
        private System.Windows.Forms.CheckBox checkBoxCollectTzrEnabled;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Button buttonTzrModeSave;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Label labelScpLastScan;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Label labelScpSavedInfo;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Button buttonScpCollectTest;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.TextBox textBoxScpStopNr;
        private System.Windows.Forms.Label labelScpStopLast;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.ListBox listBoxScpStopMode;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Label labelScpStartFirst;
        private System.Windows.Forms.TextBox textBoxScpStartNr;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.ListBox listBoxScpStartMode;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label labelScpStudyNr;
        private System.Windows.Forms.ListBox listBoxScpStudyMode;
        private System.Windows.Forms.CheckBox checkBoxCollectScpEnabled;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Button buttonScpModeSave;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.Button buttonTzrRefreshInfo;
        private System.Windows.Forms.Button buttonScpRefreshInfo;
        private System.Windows.Forms.Label labelServerName;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label labelStudyActive;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label labelLastMsgAction;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.CheckBox checkBoxAutoCollect;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Timer timerRefresh;
    }
}