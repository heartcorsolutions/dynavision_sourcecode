﻿using EventBoard;
using EventboardEntryForms;
using Program_Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventBoardEntryForms
{
    public partial class CPatientListForm : Form
    {
        private List<CSqlDataTableRow> _mList;
        private CPatientInfo _mInfo;
        public UInt32 _mSelectedIndex = 0;
        public bool _mbSelected = false;

        public bool _mbEnableSelect;
        public bool _mbEnableStudy;
        public bool _mbEnableDisable;
        bool _mbAnonymize;

        public CPatientListForm(string ATitleText, List<CSqlDataTableRow> AList, UInt32 AIndex, bool AbSearchAtStart,
            string AInfo, bool AbUniqueID, string ARefID, 
            string AFirstName, string ALastName, 
            DateTime ABirthDate,
            bool AbEnableSelect, bool AbEnableStudy, bool AbEnableDisable)
        {
            try
            {
                _mbEnableSelect = AbEnableSelect;
                _mbEnableStudy = AbEnableStudy;
                _mbEnableDisable = AbEnableDisable;

                _mbAnonymize = FormEventBoard._sbForceAnonymize;

                _mInfo = new CPatientInfo(Event_Base.CDvtmsData.sGetDBaseConnection());
                if (_mInfo == null)
                {
                    Close();
                }
                else
                { 
                    InitializeComponent();

                    Width = 1600;   // Nicer size then minimum 1240

                    Text = CProgram.sMakeProgTitle((_mbAnonymize ? "Anonymized Patient List: " : "Patient List: ") + ATitleText, false, true);

                    CPatientInfo.sFillCombobox(comboBoxGender, 0);

                    if (AList != null)
                    {
                        _mList = AList;
                        _mSelectedIndex = AIndex;
                        mSetValues();
                        labelError.Text = "Select from List";
                    }
                    else if( AIndex > 0 )
                    {
                        textBoxPatientIX.Text = AIndex.ToString();
                        _mSelectedIndex = AIndex;

                         mSearch();
                    }
                    else
                    {
                        mClear();
                        mSetValues();


                        if( AbUniqueID)
                        {
                            textBoxUniqueID.Text = ARefID;
                        }
                        else
                        {
                            textBoxPatientID.Text = ARefID;
                        }
                        textBoxFirstName.Text = AFirstName;
                        textBoxLastName.Text = ALastName;
                        if( ABirthDate != null && ABirthDate != DateTime.MinValue)
                        {
                            dateTimeDOB.Value = ABirthDate;
                        }

                        labelError.Text = " Press search to get List";
                    }
                    if (AbEnableSelect)
                    {
                        labelPatientSearch.Text = "Select Patient";
                    }
                    labelInfo.Text = AInfo;
                    checkBoxDeactivePatients.Visible = _mbEnableDisable;
                    buttonSelect.Visible = _mbEnableSelect;
                    buttonFindStudy.Visible = false; // _mbEnableStudy;
                    buttonOpenLastStudy.Visible = _mbEnableStudy;
                    labelSelected.Visible = CLicKeyDev.sbDeviceIsProgrammer();
                    labelError.Visible = CLicKeyDev.sbDeviceIsProgrammer();

                    if( AbSearchAtStart)
                    {
                        mSearch();
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("CPatientListForm() failed to init", ex);
            }
        }
        public CPatientInfo mGetSelected()
        {
            return _mbSelected ? _mInfo : null;
        }

       private void mSearch()
        {
            try
            {
                /*                UInt64 whereMask = 0;
                                DGender gender = DGender.Unknown;
                                CPatientInfo.sbParseGetGenderString(comboBoxGender.Text, ref gender);
                                UInt32 dob = CProgram.sCalcYMD(dateTimeDOB.Value);
                                bool bActive = false == checkBoxDeactivePatients.Checked;

                                _mInfo.mClear();
                                _mInfo.mbSetPatientIDValue(mAddWhereMaskText(ref whereMask, textBoxPatientID.Text, (UInt16)DPatientInfoVars.PatientID));
                                _mInfo._mSocSecNr.mbEncrypt(mAddWhereMaskText(ref whereMask, textBoxUniqueID.Text, (UInt16)DPatientInfoVars.SocSecNr));

                                _mInfo._mPatientFirstName.mbEncrypt(mAddWhereMaskText(ref whereMask, textBoxFirstName.Text, (UInt16)DPatientInfoVars.PatientFirstName ));
                                _mInfo._mPatientLastName.mbEncrypt(mAddWhereMaskText(ref whereMask, textBoxLastName.Text, (UInt16)DPatientInfoVars.PatientLastName));

                                _mInfo._mPatientGender_IX = (UInt16)gender; if( gender > 0) whereMask |= CSqlDataTableRow.sGetMask((UInt16)DPatientInfoVars.PatientGender_IX);

                                if (dateTimeDOB.Value > dateTimeDOB.MinDate)
                                {
                                    _mInfo._mPatientDateOfBirth.mbEncryptDate(dateTimeDOB.Value);
                                    whereMask |= CSqlDataTableRow.sGetMask((UInt16)DPatientInfoVars.PatientDateOfBirth);
                                }

                                _mInfo.mIndex_KEY = mAddWhereMaskUInt32(ref whereMask, textBoxPatientIX.Text, (UInt16)DSqlDataTableColum.Index_KEY);

                                bool bSearch = whereMask != 0;

                                _mInfo.mbActive = bActive; whereMask |= CSqlDataTableRow.sGetMask((UInt16)DSqlDataTableColum.Active);
              bool bError;
                UInt64 loadMask = _mInfo.mMaskValid;

                if (bSearch)
                {
                    _mInfo.mbDoSqlSelectList(out bError, out _mList, loadMask, whereMask, false, "", DSqlSort.Ascending, (UInt16)DPatientInfoVars.PatientLastName);
                    labelError.Text = bError ? "  search all error!" : " search all";
                }
                else
                {
                    _mInfo.mbDoSqlSelectList(out bError, out _mList, loadMask, whereMask, false, "", DSqlSort.Desending, (UInt16)DSqlDataTableColum.Index_KEY);
                    labelError.Text = bError ? "  search error!" : "";
                }
                  */
                DGender gender = DGender.Unknown;
                CPatientInfo.sbParseGetGenderString(comboBoxGender.Text, ref gender);
                UInt32 dob = CProgram.sCalcYMD(dateTimeDOB.Value);
                bool bActive = false == checkBoxDeactivePatients.Checked;

                string whereStr = "";

                _mInfo.mClear();
                _mInfo.mbSetPatientIDValue(textBoxPatientID.Text); _mInfo.mSqlWhereAndSearchString( ref whereStr, (UInt16)DPatientInfoVars.PatientID);
                _mInfo._mSocSecNr.mbEncrypt(textBoxUniqueID.Text); _mInfo.mSqlWhereAndSearchString(ref whereStr, (UInt16)DPatientInfoVars.SocSecNr);

                _mInfo._mPatientFirstName.mbEncrypt(textBoxFirstName.Text); _mInfo.mSqlWhereAndSearchString(ref whereStr, (UInt16)DPatientInfoVars.PatientFirstName);
                _mInfo._mPatientLastName.mbEncrypt(textBoxLastName.Text); _mInfo.mSqlWhereAndSearchString(ref whereStr, (UInt16)DPatientInfoVars.PatientLastName);

                _mInfo._mPatientGender_IX = (UInt16)gender; if (gender > 0) _mInfo.mSqlWhereAndSearchEqual( ref whereStr, (UInt16)DPatientInfoVars.PatientGender_IX);

                if (dateTimeDOB.Value > dateTimeDOB.MinDate)
                {
                    _mInfo._mPatientDateOfBirth.mbEncryptDate(dateTimeDOB.Value);
                    _mInfo.mSqlWhereAndSearchEqual(ref whereStr, (UInt16)DPatientInfoVars.PatientDateOfBirth);
                }

                if (UInt32.TryParse(textBoxPatientIX.Text, out _mInfo.mIndex_KEY) && _mInfo.mIndex_KEY > 0)
                {
                    _mInfo.mSqlWhereAndSearchEqual(ref whereStr, (UInt16)DSqlDataTableColum.Index_KEY);
                }
                bool bSearch = whereStr != null && whereStr.Length != 0;

                _mInfo.mbActive = bActive;
                _mInfo.mSqlWhereAndSearchEqual(ref whereStr, (UInt16)DSqlDataTableColum.Active);

     
                bool bError;
                UInt64 loadMask = _mInfo.mGetValidMask(true); //_mInfo.mMaskValid;

                if (bSearch)
                {
                    _mInfo.mbDoSqlSelectList(out bError, out _mList, loadMask, whereStr, DSqlSort.Ascending, (UInt16)DPatientInfoVars.PatientLastName);
                    labelError.Text = bError ? "  search error!" : "";
                }
                else
                {
                    _mInfo.mbDoSqlSelectList(out bError, out _mList, loadMask, whereStr, DSqlSort.Desending, (UInt16)DSqlDataTableColum.Index_KEY);
                    
                    labelError.Text = bError ? "  search all error!" : " search all";
                }
                mSetValues();

            }
            catch (Exception ex)
            {
                CProgram.sLogException("CPatientListForm() failed to search", ex);
            }
        }

        private void mSetValues()
        {
            try
            {
                int n = 0;

                dataGridView.Rows.Clear();
                if (_mList != null)
                {
                    CPatientInfo selectedRow = null;
                     DateTime dt = DateTime.Now;

                    foreach (CPatientInfo row in _mList)
                    {
                        DPatientState state = (DPatientState)row._mState_IX;
                        string stateStr = state.ToString();

                        string birthDate = _mbAnonymize ? row._mPatientBirthYear.ToString() : CProgram.sDateToString(row._mPatientDateOfBirth.mDecryptToDate());
                        int ageYear = CProgram.sCalcAgeYears((UInt32)row._mPatientDateOfBirth.mDecrypt(), dt);

                        string firstName = _mbAnonymize ? "--" : row._mPatientFirstName.mDecrypt();
                        string middleName = _mbAnonymize ? "--" : row._mPatientMiddleName.mDecrypt();
                        string lastName = _mbAnonymize ? "--" : row._mPatientLastName.mDecrypt();
                        string phone = _mbAnonymize ? "--" : row._mPatientPhone1.mDecrypt();

                        // row.mbActive.ToString()

                        dataGridView.Rows.Add(row.mIndex_KEY, row._mSocSecNr.mDecrypt(), row.mGetPatientIDValue(), stateStr,
                            firstName, middleName, lastName,
                                    birthDate, ageYear, CPatientInfo.sGetGenderString((DGender)row._mPatientGender_IX), phone);
                        if (_mSelectedIndex > 0 && row.mIndex_KEY == _mSelectedIndex)
                        {
                            selectedRow = row;
                            DataGridViewRow gridRow = dataGridView.Rows[n];

                            if (gridRow != null)
                            {
                                dataGridView.CurrentCell = gridRow.Cells[0];  // set cursor back to current
                            }
                        }
                        ++n;
                    }
                    if (selectedRow == null)
                    {
                        _mSelectedIndex = 0;
                    }
                }
                labelSelected.Text = _mSelectedIndex > 0 ? _mSelectedIndex.ToString() : "";

                bool bActive = false == checkBoxDeactivePatients.Checked;
                labelList.Text = n.ToString() + (bActive ? " Active" : " Deactivated");
                buttonDisable.Visible = bActive && _mbEnableDisable;
                ButtonEnable.Visible = !bActive && _mbEnableDisable;
                buttonSelect.Enabled = _mSelectedIndex > 0;
                buttonOpenLastStudy.Enabled = _mSelectedIndex > 0;
            }
            catch (Exception ex)
            {
                CProgram.sLogException("CPatientListForm() failed to search", ex);
            }
        }
        private void mUpdateButtons()
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private UInt32 mGetCursorSelected()
        {
            UInt32 index = 0;

            try
            {
                if (dataGridView.SelectedRows != null && dataGridView.SelectedRows.Count > 0)
                {
                    string s = dataGridView.SelectedRows[0].Cells[0].Value == null ? ""
                        : dataGridView.SelectedRows[0].Cells[0].Value.ToString();

                    if (false == UInt32.TryParse(s, out index))
                    {
                        index = 0;
                    }
                }
                labelSelected.Text = index > 0 ? index.ToString() : "";
            }
            catch (Exception ex)
            {
                CProgram.sLogException("CPatientListForm() get cursor failed", ex);
            }
            return index;
        }

        private bool mbLoadSelected()
        {
            bool bLoaded = false;
            try
            {
                _mSelectedIndex = mGetCursorSelected();
                buttonSelect.Enabled = _mSelectedIndex > 0;
                buttonOpenLastStudy.Enabled = _mSelectedIndex > 0;

                if (_mSelectedIndex > 0)
                {
                    foreach (CPatientInfo row in _mList)
                    {
                        DPatientState state = (DPatientState)row._mState_IX;
                        if (_mSelectedIndex > 0 && row.mIndex_KEY == _mSelectedIndex)
                        {
                            row.mbCopyTo(_mInfo);
                            bLoaded = true;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("CPatientListForm() load Selected cursor failed: " + _mSelectedIndex.ToString(), ex);
            }
            return bLoaded;
        }
        private void buttonSelect_Click(object sender, EventArgs e)
        {
            if(mbLoadSelected())
            {
                _mbSelected = true;
                Close();
            }
        }

        private void panel6_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            mClear();
        }
        private void mClear()
        { 
            textBoxPatientID.Text = "";
            textBoxUniqueID.Text = "";
            textBoxFirstName.Text = "";
            textBoxLastName.Text = "";
            comboBoxGender.Text = "";
            dateTimeDOB.Value = dateTimeDOB.MinDate;
            textBoxPatientIX.Text = "";
            checkBoxDeactivePatients.Checked = false;
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            mSearch();
        }

        private void dataGridView_SelectionChanged(object sender, EventArgs e)
        {
            _mSelectedIndex = mGetCursorSelected();
            buttonSelect.Enabled = _mSelectedIndex > 0;
            buttonOpenLastStudy.Enabled = _mSelectedIndex > 0;
        }

        private void mSetActive( bool AbActive )
        {
            try
            {
                _mSelectedIndex = mGetCursorSelected();
 
                if (_mSelectedIndex > 0)
                {
                    if (dataGridView.SelectedRows != null && dataGridView.SelectedRows.Count > 0)
                    {
                        UInt64 mask = CSqlDataTableRow.sGetMask((UInt16)DSqlDataTableColum.Active);

                        _mInfo.mClear();
                        _mInfo.mIndex_KEY = _mSelectedIndex;
                        _mInfo.mbActive = AbActive;

                        if (_mInfo.mbDoSqlUpdate(mask, AbActive))
                        {
                            if (dataGridView.SelectedRows != null && dataGridView.SelectedRows.Count > 0)
                            {
                                dataGridView.Rows.Remove(dataGridView.SelectedRows[0]);
                            }
                            labelList.Text = dataGridView.Rows.Count.ToString() + (checkBoxDeactivePatients.Checked ? " Deactivated": " Active");
                        }
                    }
                }
                buttonSelect.Enabled = _mSelectedIndex > 0;
                buttonOpenLastStudy.Enabled = _mSelectedIndex > 0;
            }
            catch (Exception ex)
            {
                CProgram.sLogException("CPatientListForm() set cursor " +  (AbActive ? "Active": "Disabled") + " failed: " + _mSelectedIndex.ToString(), ex);
            }
        }

        private void ButtonEnable_Click(object sender, EventArgs e)
        {
            mSetActive(true);
        }

        private void buttonDisable_Click(object sender, EventArgs e)
        {
            mSetActive(false);
        }

        public void mOpenLastStudy()
        {
            try
            {
                if (mbLoadSelected())
                {
                    CStudyInfo studyInfo = new CStudyInfo(Event_Base.CDvtmsData.sGetDBaseConnection());

                    if( studyInfo != null)
                    {
                        UInt64 loadMask = studyInfo.mGetValidMask(true);
                        UInt64 mask = CSqlDataTableRow.sGetMask((UInt16)DStudyInfoVars.Patient_IX);

                        studyInfo.mIndex_KEY = _mSelectedIndex;
                        studyInfo._mPatient_IX = _mSelectedIndex;

                        List<CSqlDataTableRow> studyList;
                        bool bError;

                        if( studyInfo.mbDoSqlSelectList(out bError, out studyList, loadMask, mask, true, ""))
                        {
                            int n = studyList == null ? 0 : studyList.Count;

                            int i = n;
                            if (n > 0)
                            {
                                if (n == 1
                                    || CProgram.sbReqInt32( n.ToString() + " Study for patient #" + _mSelectedIndex.ToString(),
                                        "Select index in list, " + i.ToString() + " is Last Study", ref i, "", 1, n))
                                {
                                    studyInfo.mbCopyFrom(studyList[i-1]);

                                    CStudyPatientForm form = new CStudyPatientForm(studyInfo, _mInfo, false, true, true, true);

                                    if (form != null)
                                    {
                                        form.ShowDialog();
                                    }
                                }
                            }
                            else
                            {
                                labelSelected.Text = " No study found for #" + _mSelectedIndex.ToString();
                            }
                        }
                    }
                }
                buttonSelect.Enabled = _mSelectedIndex > 0;
                buttonOpenLastStudy.Enabled = _mSelectedIndex > 0;
            }
            catch (Exception ex)
            {
                CProgram.sLogException("CPatientListForm() open last study of cursor failed: " + _mSelectedIndex.ToString(), ex);
            }

        }

        private void buttonOpenLastStudy_Click(object sender, EventArgs e)
        {
            mOpenLastStudy();
        }

        private void labelPatientID_DoubleClick(object sender, EventArgs e)
        {
            textBoxPatientID.Text = "";
        }

        private void labelUniqueID_DoubleClick(object sender, EventArgs e)
        {
            textBoxUniqueID.Text = "";
        }

        private void labelFirstName_Click(object sender, EventArgs e)
        {

        }

        private void labelFirstName_DoubleClick(object sender, EventArgs e)
        {
            textBoxFirstName.Text = "";
        }

        private void labelLastName_DoubleClick(object sender, EventArgs e)
        {
            textBoxLastName.Text = "";
        }

        private void labelGender_DoubleClick(object sender, EventArgs e)
        {
            comboBoxGender.SelectedIndex = 0;
        }

        private void labelBirthDate_DoubleClick(object sender, EventArgs e)
        {
            dateTimeDOB.Value = DateTime.MinValue;
        }

        private void labelPatient_DoubleClick(object sender, EventArgs e)
        {
            textBoxPatientIX.Text = "";
        }

        private void panel5_DoubleClick(object sender, EventArgs e)
        {
        }

        private void labelInfo_DoubleClick(object sender, EventArgs e)
        {
            string info = labelInfo.Text;
            if (info != null && info.Length > 0)
            {
                ReqTextForm.sbShowText("Patient List", "Info", info);
            }
        }
    }
}
