﻿namespace EventboardEntryForms
{
    partial class CHolterFilesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.labelNrXXX = new System.Windows.Forms.Label();
            this.labelHolterType = new System.Windows.Forms.Label();
            this.panel27 = new System.Windows.Forms.Panel();
            this.labelGender = new System.Windows.Forms.Label();
            this.panel25 = new System.Windows.Forms.Panel();
            this.labelAge = new System.Windows.Forms.Label();
            this.panel24 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.panel23 = new System.Windows.Forms.Panel();
            this.labelDoB = new System.Windows.Forms.Label();
            this.panel21 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel20 = new System.Windows.Forms.Panel();
            this.labelFullName = new System.Windows.Forms.Label();
            this.panel15 = new System.Windows.Forms.Panel();
            this.labelPreviousFindingsList = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.labelStudyNote = new System.Windows.Forms.Label();
            this.labelStudyIndex = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel33 = new System.Windows.Forms.Panel();
            this.labelRecordInfo = new System.Windows.Forms.Label();
            this.panelStudyButtons = new System.Windows.Forms.Panel();
            this.checkBoxShowResult = new System.Windows.Forms.CheckBox();
            this.checkBoxAnonymize = new System.Windows.Forms.CheckBox();
            this.buttonOpenFile2 = new System.Windows.Forms.Button();
            this.buttonPrintPreviousFindings = new System.Windows.Forms.Button();
            this.labelPdfNr = new System.Windows.Forms.Label();
            this.labelReportResult = new System.Windows.Forms.Label();
            this.buttonViewPdf = new System.Windows.Forms.Button();
            this.buttonMCT = new System.Windows.Forms.Button();
            this.buttonEndStudy = new System.Windows.Forms.Button();
            this.panelStudyPeriod = new System.Windows.Forms.Panel();
            this.panel37 = new System.Windows.Forms.Panel();
            this.panel22 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.buttonHolterFiles = new System.Windows.Forms.Button();
            this.buttonMegeFiles = new System.Windows.Forms.Button();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.checkBoxaddDtFile = new System.Windows.Forms.CheckBox();
            this.checkBoxCopyFirstJson = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.panelSetTime = new System.Windows.Forms.Panel();
            this.panel58 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.labelAllowedChannels = new System.Windows.Forms.Label();
            this.comboBoxChannelFormat = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel64 = new System.Windows.Forms.Panel();
            this.buttonP24 = new System.Windows.Forms.Button();
            this.buttonP6 = new System.Windows.Forms.Button();
            this.buttonP1 = new System.Windows.Forms.Button();
            this.button0h00 = new System.Windows.Forms.Button();
            this.buttonX00 = new System.Windows.Forms.Button();
            this.buttonM1 = new System.Windows.Forms.Button();
            this.buttonM6 = new System.Windows.Forms.Button();
            this.buttonM24 = new System.Windows.Forms.Button();
            this.panel31 = new System.Windows.Forms.Panel();
            this.panel62 = new System.Windows.Forms.Panel();
            this.labelReqDur = new System.Windows.Forms.Label();
            this.comboBoxToAM = new System.Windows.Forms.ComboBox();
            this.textBoxToMin = new System.Windows.Forms.TextBox();
            this.textBoxToHour = new System.Windows.Forms.TextBox();
            this.dateTimePickerTo = new System.Windows.Forms.DateTimePicker();
            this.labelTo = new System.Windows.Forms.Label();
            this.panel57 = new System.Windows.Forms.Panel();
            this.comboBoxFromAM = new System.Windows.Forms.ComboBox();
            this.textBoxFromMin = new System.Windows.Forms.TextBox();
            this.textBoxFromHour = new System.Windows.Forms.TextBox();
            this.dateTimePickerFrom = new System.Windows.Forms.DateTimePicker();
            this.labelFrom = new System.Windows.Forms.Label();
            this.panelPeriod = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.checkBoxInv10 = new System.Windows.Forms.CheckBox();
            this.checkBoxInv9 = new System.Windows.Forms.CheckBox();
            this.checkBoxInv8 = new System.Windows.Forms.CheckBox();
            this.checkBoxInv7 = new System.Windows.Forms.CheckBox();
            this.checkBoxInv6 = new System.Windows.Forms.CheckBox();
            this.checkBoxInv5 = new System.Windows.Forms.CheckBox();
            this.checkBoxInv4 = new System.Windows.Forms.CheckBox();
            this.checkBoxInv3 = new System.Windows.Forms.CheckBox();
            this.checkBoxInv2 = new System.Windows.Forms.CheckBox();
            this.checkBoxInv1 = new System.Windows.Forms.CheckBox();
            this.panel55 = new System.Windows.Forms.Panel();
            this.label28 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.checkBoxCh10 = new System.Windows.Forms.CheckBox();
            this.checkBoxCh9 = new System.Windows.Forms.CheckBox();
            this.checkBoxCh8 = new System.Windows.Forms.CheckBox();
            this.checkBoxCh7 = new System.Windows.Forms.CheckBox();
            this.checkBoxCh6 = new System.Windows.Forms.CheckBox();
            this.checkBoxCh5 = new System.Windows.Forms.CheckBox();
            this.checkBoxCh4 = new System.Windows.Forms.CheckBox();
            this.checkBoxCh3 = new System.Windows.Forms.CheckBox();
            this.checkBoxCh2 = new System.Windows.Forms.CheckBox();
            this.checkBoxCh1 = new System.Windows.Forms.CheckBox();
            this.checkBoxChAll = new System.Windows.Forms.CheckBox();
            this.panel54 = new System.Windows.Forms.Panel();
            this.radioButtonMonth = new System.Windows.Forms.RadioButton();
            this.radioButtonWeek = new System.Windows.Forms.RadioButton();
            this.radioButtonDay = new System.Windows.Forms.RadioButton();
            this.radioButton12Hour = new System.Windows.Forms.RadioButton();
            this.radioButton6Hour = new System.Windows.Forms.RadioButton();
            this.radioButtonHour = new System.Windows.Forms.RadioButton();
            this.radioButtonPeriod = new System.Windows.Forms.RadioButton();
            this.radioButtonAll = new System.Windows.Forms.RadioButton();
            this.label36 = new System.Windows.Forms.Label();
            this.panelStudy = new System.Windows.Forms.Panel();
            this.panel16 = new System.Windows.Forms.Panel();
            this.labelRecordStats = new System.Windows.Forms.Label();
            this.panelRows = new System.Windows.Forms.Panel();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.FileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnEventDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnAnalysisDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnFindings = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnRemarks = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonSortET = new System.Windows.Forms.Button();
            this.panel19 = new System.Windows.Forms.Panel();
            this.labelHolterSize = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonSetAll = new System.Windows.Forms.Button();
            this.labelMemInfo = new System.Windows.Forms.Label();
            this.buttonResetAll = new System.Windows.Forms.Button();
            this.panel29 = new System.Windows.Forms.Panel();
            this.buttonReloadList = new System.Windows.Forms.Button();
            this.panel28 = new System.Windows.Forms.Panel();
            this.buttonOpenAnalysis = new System.Windows.Forms.Button();
            this.panel32 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.checkBoxHolterOnly = new System.Windows.Forms.CheckBox();
            this.labelNrHolter = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelListTable = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel26 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonViewResult = new System.Windows.Forms.Button();
            this.buttonHolterResult = new System.Windows.Forms.Button();
            this.labelExecResults = new System.Windows.Forms.Label();
            this.panel30 = new System.Windows.Forms.Panel();
            this.panel40 = new System.Windows.Forms.Panel();
            this.labelFileStats = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.labelRecordRange = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.labelStudyRange = new System.Windows.Forms.Label();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.panel6.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel33.SuspendLayout();
            this.panelStudyButtons.SuspendLayout();
            this.panelStudyPeriod.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panelSetTime.SuspendLayout();
            this.panel58.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel64.SuspendLayout();
            this.panel62.SuspendLayout();
            this.panel57.SuspendLayout();
            this.panelPeriod.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel55.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel54.SuspendLayout();
            this.panelStudy.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panelRows.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.panel19.SuspendLayout();
            this.panel26.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel40.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel5
            // 
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1304, 5);
            this.panel5.TabIndex = 1;
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.labelNrXXX);
            this.panel6.Controls.Add(this.labelHolterType);
            this.panel6.Controls.Add(this.panel27);
            this.panel6.Controls.Add(this.panel25);
            this.panel6.Controls.Add(this.panel24);
            this.panel6.Controls.Add(this.panel23);
            this.panel6.Controls.Add(this.panel21);
            this.panel6.Controls.Add(this.panel20);
            this.panel6.Controls.Add(this.panel15);
            this.panel6.Controls.Add(this.panel4);
            this.panel6.Controls.Add(this.panel3);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 5);
            this.panel6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1304, 25);
            this.panel6.TabIndex = 2;
            // 
            // labelNrXXX
            // 
            this.labelNrXXX.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelNrXXX.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelNrXXX.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelNrXXX.Location = new System.Drawing.Point(1126, 0);
            this.labelNrXXX.Name = "labelNrXXX";
            this.labelNrXXX.Size = new System.Drawing.Size(58, 23);
            this.labelNrXXX.TabIndex = 12;
            this.labelNrXXX.Text = "^9999";
            this.labelNrXXX.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelHolterType
            // 
            this.labelHolterType.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelHolterType.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelHolterType.Location = new System.Drawing.Point(1184, 0);
            this.labelHolterType.Name = "labelHolterType";
            this.labelHolterType.Size = new System.Drawing.Size(118, 23);
            this.labelHolterType.TabIndex = 11;
            this.labelHolterType.Text = "xxxx";
            this.labelHolterType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel27
            // 
            this.panel27.Controls.Add(this.labelGender);
            this.panel27.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel27.Location = new System.Drawing.Point(1053, 0);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(78, 23);
            this.panel27.TabIndex = 10;
            // 
            // labelGender
            // 
            this.labelGender.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelGender.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelGender.Location = new System.Drawing.Point(0, 0);
            this.labelGender.Name = "labelGender";
            this.labelGender.Size = new System.Drawing.Size(66, 23);
            this.labelGender.TabIndex = 6;
            this.labelGender.Text = "^Gender";
            this.labelGender.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel25
            // 
            this.panel25.Controls.Add(this.labelAge);
            this.panel25.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel25.Location = new System.Drawing.Point(1007, 0);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(46, 23);
            this.panel25.TabIndex = 8;
            // 
            // labelAge
            // 
            this.labelAge.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelAge.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelAge.Location = new System.Drawing.Point(0, 0);
            this.labelAge.Name = "labelAge";
            this.labelAge.Size = new System.Drawing.Size(43, 23);
            this.labelAge.TabIndex = 4;
            this.labelAge.Text = "^99";
            this.labelAge.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel24
            // 
            this.panel24.Controls.Add(this.label5);
            this.panel24.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel24.Location = new System.Drawing.Point(952, 0);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(55, 23);
            this.panel24.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Right;
            this.label5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 23);
            this.label5.TabIndex = 3;
            this.label5.Text = "Age:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel23
            // 
            this.panel23.Controls.Add(this.labelDoB);
            this.panel23.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel23.Location = new System.Drawing.Point(850, 0);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(102, 23);
            this.panel23.TabIndex = 6;
            // 
            // labelDoB
            // 
            this.labelDoB.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelDoB.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelDoB.Location = new System.Drawing.Point(0, 0);
            this.labelDoB.Name = "labelDoB";
            this.labelDoB.Size = new System.Drawing.Size(94, 23);
            this.labelDoB.TabIndex = 3;
            this.labelDoB.Text = "^99/99/9999";
            this.labelDoB.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel21
            // 
            this.panel21.Controls.Add(this.label2);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel21.Location = new System.Drawing.Point(729, 0);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(121, 23);
            this.panel21.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Right;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 23);
            this.label2.TabIndex = 2;
            this.label2.Text = "Date of Birth:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.labelFullName);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel20.Location = new System.Drawing.Point(297, 0);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(432, 23);
            this.panel20.TabIndex = 4;
            // 
            // labelFullName
            // 
            this.labelFullName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelFullName.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelFullName.Location = new System.Drawing.Point(0, 0);
            this.labelFullName.Name = "labelFullName";
            this.labelFullName.Size = new System.Drawing.Size(432, 23);
            this.labelFullName.TabIndex = 2;
            this.labelFullName.Text = "^Ottovordemgentschenfelde";
            this.labelFullName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.labelPreviousFindingsList);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel15.Location = new System.Drawing.Point(234, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(63, 23);
            this.panel15.TabIndex = 3;
            // 
            // labelPreviousFindingsList
            // 
            this.labelPreviousFindingsList.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPreviousFindingsList.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPreviousFindingsList.Location = new System.Drawing.Point(0, 0);
            this.labelPreviousFindingsList.Name = "labelPreviousFindingsList";
            this.labelPreviousFindingsList.Size = new System.Drawing.Size(66, 23);
            this.labelPreviousFindingsList.TabIndex = 1;
            this.labelPreviousFindingsList.Text = "Name:";
            this.labelPreviousFindingsList.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.labelStudyNote);
            this.panel4.Controls.Add(this.labelStudyIndex);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(84, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(150, 23);
            this.panel4.TabIndex = 2;
            // 
            // labelStudyNote
            // 
            this.labelStudyNote.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelStudyNote.Font = new System.Drawing.Font("Arial", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStudyNote.Location = new System.Drawing.Point(127, 0);
            this.labelStudyNote.Name = "labelStudyNote";
            this.labelStudyNote.Size = new System.Drawing.Size(22, 23);
            this.labelStudyNote.TabIndex = 4;
            this.labelStudyNote.Text = "sl\r\nsn";
            this.labelStudyNote.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStudyIndex
            // 
            this.labelStudyIndex.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelStudyIndex.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelStudyIndex.Location = new System.Drawing.Point(0, 0);
            this.labelStudyIndex.Name = "labelStudyIndex";
            this.labelStudyIndex.Size = new System.Drawing.Size(127, 23);
            this.labelStudyIndex.TabIndex = 2;
            this.labelStudyIndex.Text = "^313131 T2344";
            this.labelStudyIndex.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelStudyIndex.DoubleClick += new System.EventHandler(this.labelStudyIndex_DoubleClick);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(84, 23);
            this.panel3.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Left;
            this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 23);
            this.label4.TabIndex = 1;
            this.label4.Text = "Study nr:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.panel33);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 30);
            this.panel7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1304, 20);
            this.panel7.TabIndex = 3;
            // 
            // panel33
            // 
            this.panel33.Controls.Add(this.labelRecordInfo);
            this.panel33.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel33.Location = new System.Drawing.Point(0, 0);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(1304, 19);
            this.panel33.TabIndex = 0;
            // 
            // labelRecordInfo
            // 
            this.labelRecordInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelRecordInfo.Location = new System.Drawing.Point(0, 0);
            this.labelRecordInfo.Name = "labelRecordInfo";
            this.labelRecordInfo.Size = new System.Drawing.Size(1304, 19);
            this.labelRecordInfo.TabIndex = 0;
            this.labelRecordInfo.Text = "?Records";
            this.labelRecordInfo.Click += new System.EventHandler(this.labelRecordInfo_Click);
            // 
            // panelStudyButtons
            // 
            this.panelStudyButtons.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.panelStudyButtons.Controls.Add(this.checkBoxShowResult);
            this.panelStudyButtons.Controls.Add(this.checkBoxAnonymize);
            this.panelStudyButtons.Controls.Add(this.buttonOpenFile2);
            this.panelStudyButtons.Controls.Add(this.buttonPrintPreviousFindings);
            this.panelStudyButtons.Controls.Add(this.labelPdfNr);
            this.panelStudyButtons.Controls.Add(this.labelReportResult);
            this.panelStudyButtons.Controls.Add(this.buttonViewPdf);
            this.panelStudyButtons.Controls.Add(this.buttonMCT);
            this.panelStudyButtons.Controls.Add(this.buttonEndStudy);
            this.panelStudyButtons.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelStudyButtons.Location = new System.Drawing.Point(0, 815);
            this.panelStudyButtons.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelStudyButtons.Name = "panelStudyButtons";
            this.panelStudyButtons.Size = new System.Drawing.Size(1304, 46);
            this.panelStudyButtons.TabIndex = 6;
            // 
            // checkBoxShowResult
            // 
            this.checkBoxShowResult.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.checkBoxShowResult.Dock = System.Windows.Forms.DockStyle.Right;
            this.checkBoxShowResult.ForeColor = System.Drawing.Color.White;
            this.checkBoxShowResult.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.checkBoxShowResult.Location = new System.Drawing.Point(863, 0);
            this.checkBoxShowResult.Name = "checkBoxShowResult";
            this.checkBoxShowResult.Size = new System.Drawing.Size(94, 46);
            this.checkBoxShowResult.TabIndex = 38;
            this.checkBoxShowResult.Text = "Show result";
            this.checkBoxShowResult.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.checkBoxShowResult.UseVisualStyleBackColor = true;
            // 
            // checkBoxAnonymize
            // 
            this.checkBoxAnonymize.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.checkBoxAnonymize.Dock = System.Windows.Forms.DockStyle.Right;
            this.checkBoxAnonymize.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxAnonymize.ForeColor = System.Drawing.Color.White;
            this.checkBoxAnonymize.Location = new System.Drawing.Point(957, 0);
            this.checkBoxAnonymize.Name = "checkBoxAnonymize";
            this.checkBoxAnonymize.Size = new System.Drawing.Size(100, 46);
            this.checkBoxAnonymize.TabIndex = 35;
            this.checkBoxAnonymize.Text = "Anonymize";
            this.checkBoxAnonymize.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.checkBoxAnonymize.UseVisualStyleBackColor = true;
            this.checkBoxAnonymize.Visible = false;
            this.checkBoxAnonymize.CheckedChanged += new System.EventHandler(this.checkBoxAnonymize_CheckedChanged);
            // 
            // buttonOpenFile2
            // 
            this.buttonOpenFile2.AutoSize = true;
            this.buttonOpenFile2.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonOpenFile2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonOpenFile2.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonOpenFile2.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonOpenFile2.FlatAppearance.BorderSize = 0;
            this.buttonOpenFile2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOpenFile2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonOpenFile2.ForeColor = System.Drawing.Color.White;
            this.buttonOpenFile2.Location = new System.Drawing.Point(1057, 0);
            this.buttonOpenFile2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonOpenFile2.Name = "buttonOpenFile2";
            this.buttonOpenFile2.Size = new System.Drawing.Size(101, 46);
            this.buttonOpenFile2.TabIndex = 33;
            this.buttonOpenFile2.Text = "Open File";
            this.buttonOpenFile2.UseVisualStyleBackColor = false;
            this.buttonOpenFile2.Click += new System.EventHandler(this.buttonOpenFile2_Click);
            // 
            // buttonPrintPreviousFindings
            // 
            this.buttonPrintPreviousFindings.AutoSize = true;
            this.buttonPrintPreviousFindings.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonPrintPreviousFindings.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonPrintPreviousFindings.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonPrintPreviousFindings.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonPrintPreviousFindings.FlatAppearance.BorderSize = 0;
            this.buttonPrintPreviousFindings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPrintPreviousFindings.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonPrintPreviousFindings.ForeColor = System.Drawing.Color.White;
            this.buttonPrintPreviousFindings.Location = new System.Drawing.Point(1158, 0);
            this.buttonPrintPreviousFindings.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonPrintPreviousFindings.Name = "buttonPrintPreviousFindings";
            this.buttonPrintPreviousFindings.Size = new System.Drawing.Size(146, 46);
            this.buttonPrintPreviousFindings.TabIndex = 32;
            this.buttonPrintPreviousFindings.Text = "XXXSave && Print\r\nReport";
            this.buttonPrintPreviousFindings.UseVisualStyleBackColor = false;
            this.buttonPrintPreviousFindings.Visible = false;
            this.buttonPrintPreviousFindings.Click += new System.EventHandler(this.buttonPrintPreviousFindings_Click);
            // 
            // labelPdfNr
            // 
            this.labelPdfNr.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPdfNr.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelPdfNr.ForeColor = System.Drawing.Color.White;
            this.labelPdfNr.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelPdfNr.Location = new System.Drawing.Point(600, 0);
            this.labelPdfNr.Name = "labelPdfNr";
            this.labelPdfNr.Size = new System.Drawing.Size(93, 46);
            this.labelPdfNr.TabIndex = 25;
            this.labelPdfNr.Text = "Not Visible";
            this.labelPdfNr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelReportResult
            // 
            this.labelReportResult.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelReportResult.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelReportResult.ForeColor = System.Drawing.Color.White;
            this.labelReportResult.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelReportResult.Location = new System.Drawing.Point(314, 0);
            this.labelReportResult.Name = "labelReportResult";
            this.labelReportResult.Size = new System.Drawing.Size(286, 46);
            this.labelReportResult.TabIndex = 22;
            this.labelReportResult.Text = "Not visable";
            this.labelReportResult.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelReportResult.Visible = false;
            // 
            // buttonViewPdf
            // 
            this.buttonViewPdf.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonViewPdf.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonViewPdf.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonViewPdf.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonViewPdf.FlatAppearance.BorderSize = 0;
            this.buttonViewPdf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonViewPdf.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonViewPdf.ForeColor = System.Drawing.Color.White;
            this.buttonViewPdf.Location = new System.Drawing.Point(212, 0);
            this.buttonViewPdf.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonViewPdf.Name = "buttonViewPdf";
            this.buttonViewPdf.Size = new System.Drawing.Size(102, 46);
            this.buttonViewPdf.TabIndex = 24;
            this.buttonViewPdf.Text = "Report List";
            this.buttonViewPdf.UseVisualStyleBackColor = false;
            this.buttonViewPdf.Click += new System.EventHandler(this.buttonViewPdf_Click);
            // 
            // buttonMCT
            // 
            this.buttonMCT.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonMCT.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonMCT.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonMCT.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonMCT.FlatAppearance.BorderSize = 0;
            this.buttonMCT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMCT.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonMCT.ForeColor = System.Drawing.Color.White;
            this.buttonMCT.Location = new System.Drawing.Point(102, 0);
            this.buttonMCT.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonMCT.Name = "buttonMCT";
            this.buttonMCT.Size = new System.Drawing.Size(110, 46);
            this.buttonMCT.TabIndex = 11;
            this.buttonMCT.Text = "MCT Data";
            this.buttonMCT.UseVisualStyleBackColor = false;
            this.buttonMCT.Click += new System.EventHandler(this.buttonMCT_Click);
            // 
            // buttonEndStudy
            // 
            this.buttonEndStudy.AutoSize = true;
            this.buttonEndStudy.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonEndStudy.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonEndStudy.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonEndStudy.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonEndStudy.FlatAppearance.BorderSize = 0;
            this.buttonEndStudy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEndStudy.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonEndStudy.ForeColor = System.Drawing.Color.White;
            this.buttonEndStudy.Location = new System.Drawing.Point(0, 0);
            this.buttonEndStudy.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonEndStudy.Name = "buttonEndStudy";
            this.buttonEndStudy.Size = new System.Drawing.Size(102, 46);
            this.buttonEndStudy.TabIndex = 37;
            this.buttonEndStudy.Text = "Stop study";
            this.buttonEndStudy.UseVisualStyleBackColor = false;
            this.buttonEndStudy.Click += new System.EventHandler(this.buttonEndStudy_Click);
            // 
            // panelStudyPeriod
            // 
            this.panelStudyPeriod.AutoSize = true;
            this.panelStudyPeriod.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelStudyPeriod.Controls.Add(this.panel37);
            this.panelStudyPeriod.Controls.Add(this.panel22);
            this.panelStudyPeriod.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelStudyPeriod.Location = new System.Drawing.Point(0, 24);
            this.panelStudyPeriod.Name = "panelStudyPeriod";
            this.panelStudyPeriod.Size = new System.Drawing.Size(1304, 91);
            this.panelStudyPeriod.TabIndex = 1;
            // 
            // panel37
            // 
            this.panel37.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel37.Location = new System.Drawing.Point(0, 89);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(1304, 2);
            this.panel37.TabIndex = 13;
            // 
            // panel22
            // 
            this.panel22.Controls.Add(this.panel14);
            this.panel22.Controls.Add(this.panel13);
            this.panel22.Controls.Add(this.panel12);
            this.panel22.Controls.Add(this.panelSetTime);
            this.panel22.Controls.Add(this.panelPeriod);
            this.panel22.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel22.Location = new System.Drawing.Point(0, 0);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(1304, 89);
            this.panel22.TabIndex = 9;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.buttonHolterFiles);
            this.panel14.Controls.Add(this.buttonMegeFiles);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel14.Location = new System.Drawing.Point(968, 0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(346, 89);
            this.panel14.TabIndex = 28;
            // 
            // buttonHolterFiles
            // 
            this.buttonHolterFiles.BackColor = System.Drawing.SystemColors.Info;
            this.buttonHolterFiles.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonHolterFiles.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonHolterFiles.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonHolterFiles.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonHolterFiles.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.buttonHolterFiles.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonHolterFiles.Location = new System.Drawing.Point(0, 55);
            this.buttonHolterFiles.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonHolterFiles.Name = "buttonHolterFiles";
            this.buttonHolterFiles.Size = new System.Drawing.Size(346, 34);
            this.buttonHolterFiles.TabIndex = 55;
            this.buttonHolterFiles.Text = "Create Holter with gap and overlap correction";
            this.buttonHolterFiles.UseVisualStyleBackColor = false;
            this.buttonHolterFiles.Click += new System.EventHandler(this.buttonHolterFiles_Click);
            // 
            // buttonMegeFiles
            // 
            this.buttonMegeFiles.BackColor = System.Drawing.Color.Transparent;
            this.buttonMegeFiles.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonMegeFiles.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonMegeFiles.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonMegeFiles.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMegeFiles.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.buttonMegeFiles.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonMegeFiles.Location = new System.Drawing.Point(0, 0);
            this.buttonMegeFiles.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonMegeFiles.Name = "buttonMegeFiles";
            this.buttonMegeFiles.Size = new System.Drawing.Size(346, 40);
            this.buttonMegeFiles.TabIndex = 54;
            this.buttonMegeFiles.Text = "Merge raw selected files without correction";
            this.buttonMegeFiles.UseVisualStyleBackColor = false;
            this.buttonMegeFiles.Click += new System.EventHandler(this.buttonMegeFiles_Click);
            // 
            // panel13
            // 
            this.panel13.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel13.Location = new System.Drawing.Point(966, 0);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(2, 89);
            this.panel13.TabIndex = 27;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.checkBoxaddDtFile);
            this.panel12.Controls.Add(this.checkBoxCopyFirstJson);
            this.panel12.Controls.Add(this.checkBox9);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel12.Location = new System.Drawing.Point(836, 0);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(130, 89);
            this.panel12.TabIndex = 26;
            // 
            // checkBoxaddDtFile
            // 
            this.checkBoxaddDtFile.AutoSize = true;
            this.checkBoxaddDtFile.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.checkBoxaddDtFile.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxaddDtFile.Location = new System.Drawing.Point(0, 49);
            this.checkBoxaddDtFile.Name = "checkBoxaddDtFile";
            this.checkBoxaddDtFile.Size = new System.Drawing.Size(130, 20);
            this.checkBoxaddDtFile.TabIndex = 42;
            this.checkBoxaddDtFile.Text = "Add date time";
            this.checkBoxaddDtFile.UseVisualStyleBackColor = true;
            // 
            // checkBoxCopyFirstJson
            // 
            this.checkBoxCopyFirstJson.AutoSize = true;
            this.checkBoxCopyFirstJson.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.checkBoxCopyFirstJson.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxCopyFirstJson.Location = new System.Drawing.Point(0, 69);
            this.checkBoxCopyFirstJson.Name = "checkBoxCopyFirstJson";
            this.checkBoxCopyFirstJson.Size = new System.Drawing.Size(130, 20);
            this.checkBoxCopyFirstJson.TabIndex = 41;
            this.checkBoxCopyFirstJson.Text = "Copy first json";
            this.checkBoxCopyFirstJson.UseVisualStyleBackColor = true;
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkBox9.Enabled = false;
            this.checkBox9.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBox9.Location = new System.Drawing.Point(0, 0);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(130, 36);
            this.checkBox9.TabIndex = 40;
            this.checkBox9.Text = "Fill Gaps and\r\n reduce overlap";
            this.checkBox9.UseVisualStyleBackColor = true;
            this.checkBox9.Visible = false;
            // 
            // panelSetTime
            // 
            this.panelSetTime.Controls.Add(this.panel58);
            this.panelSetTime.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelSetTime.Location = new System.Drawing.Point(487, 0);
            this.panelSetTime.Name = "panelSetTime";
            this.panelSetTime.Size = new System.Drawing.Size(349, 89);
            this.panelSetTime.TabIndex = 25;
            // 
            // panel58
            // 
            this.panel58.Controls.Add(this.panel17);
            this.panel58.Controls.Add(this.panel64);
            this.panel58.Controls.Add(this.panel62);
            this.panel58.Controls.Add(this.panel57);
            this.panel58.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel58.Location = new System.Drawing.Point(0, 0);
            this.panel58.Name = "panel58";
            this.panel58.Size = new System.Drawing.Size(343, 89);
            this.panel58.TabIndex = 26;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.labelAllowedChannels);
            this.panel17.Controls.Add(this.comboBoxChannelFormat);
            this.panel17.Controls.Add(this.label3);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel17.Location = new System.Drawing.Point(0, 68);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(343, 21);
            this.panel17.TabIndex = 55;
            // 
            // labelAllowedChannels
            // 
            this.labelAllowedChannels.AutoSize = true;
            this.labelAllowedChannels.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelAllowedChannels.Font = new System.Drawing.Font("Arial", 10F);
            this.labelAllowedChannels.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelAllowedChannels.Location = new System.Drawing.Point(295, 0);
            this.labelAllowedChannels.Name = "labelAllowedChannels";
            this.labelAllowedChannels.Size = new System.Drawing.Size(48, 16);
            this.labelAllowedChannels.TabIndex = 111;
            this.labelAllowedChannels.Text = "3;8;12";
            this.labelAllowedChannels.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comboBoxChannelFormat
            // 
            this.comboBoxChannelFormat.Dock = System.Windows.Forms.DockStyle.Left;
            this.comboBoxChannelFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxChannelFormat.Font = new System.Drawing.Font("Arial", 8F);
            this.comboBoxChannelFormat.FormattingEnabled = true;
            this.comboBoxChannelFormat.Items.AddRange(new object[] {
            "Selected Channels",
            "I, II, II-I"});
            this.comboBoxChannelFormat.Location = new System.Drawing.Point(80, 0);
            this.comboBoxChannelFormat.Name = "comboBoxChannelFormat";
            this.comboBoxChannelFormat.Size = new System.Drawing.Size(179, 22);
            this.comboBoxChannelFormat.TabIndex = 110;
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Left;
            this.label3.Font = new System.Drawing.Font("Arial", 10F);
            this.label3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 21);
            this.label3.TabIndex = 109;
            this.label3.Text = "Ch Format:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel64
            // 
            this.panel64.BackColor = System.Drawing.SystemColors.Info;
            this.panel64.Controls.Add(this.buttonP24);
            this.panel64.Controls.Add(this.buttonP6);
            this.panel64.Controls.Add(this.buttonP1);
            this.panel64.Controls.Add(this.button0h00);
            this.panel64.Controls.Add(this.buttonX00);
            this.panel64.Controls.Add(this.buttonM1);
            this.panel64.Controls.Add(this.buttonM6);
            this.panel64.Controls.Add(this.buttonM24);
            this.panel64.Controls.Add(this.panel31);
            this.panel64.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel64.Location = new System.Drawing.Point(0, 40);
            this.panel64.Name = "panel64";
            this.panel64.Size = new System.Drawing.Size(343, 21);
            this.panel64.TabIndex = 2;
            // 
            // buttonP24
            // 
            this.buttonP24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonP24.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonP24.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonP24.FlatAppearance.BorderSize = 0;
            this.buttonP24.Font = new System.Drawing.Font("Arial", 9F);
            this.buttonP24.Location = new System.Drawing.Point(290, 0);
            this.buttonP24.Name = "buttonP24";
            this.buttonP24.Size = new System.Drawing.Size(43, 21);
            this.buttonP24.TabIndex = 47;
            this.buttonP24.Text = "+24h";
            this.buttonP24.UseVisualStyleBackColor = false;
            this.buttonP24.Click += new System.EventHandler(this.buttonP24_Click);
            // 
            // buttonP6
            // 
            this.buttonP6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonP6.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonP6.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonP6.FlatAppearance.BorderSize = 0;
            this.buttonP6.Font = new System.Drawing.Font("Arial", 9F);
            this.buttonP6.Location = new System.Drawing.Point(250, 0);
            this.buttonP6.Name = "buttonP6";
            this.buttonP6.Size = new System.Drawing.Size(40, 21);
            this.buttonP6.TabIndex = 46;
            this.buttonP6.Text = "+6 h";
            this.buttonP6.UseVisualStyleBackColor = false;
            this.buttonP6.Click += new System.EventHandler(this.buttonP6_Click);
            // 
            // buttonP1
            // 
            this.buttonP1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonP1.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonP1.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonP1.FlatAppearance.BorderSize = 0;
            this.buttonP1.Font = new System.Drawing.Font("Arial", 9F);
            this.buttonP1.Location = new System.Drawing.Point(210, 0);
            this.buttonP1.Name = "buttonP1";
            this.buttonP1.Size = new System.Drawing.Size(40, 21);
            this.buttonP1.TabIndex = 45;
            this.buttonP1.Text = "+1 h";
            this.buttonP1.UseVisualStyleBackColor = false;
            this.buttonP1.Click += new System.EventHandler(this.buttonP1_Click);
            // 
            // button0h00
            // 
            this.button0h00.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.button0h00.Dock = System.Windows.Forms.DockStyle.Left;
            this.button0h00.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.button0h00.FlatAppearance.BorderSize = 0;
            this.button0h00.Font = new System.Drawing.Font("Arial", 9F);
            this.button0h00.Location = new System.Drawing.Point(170, 0);
            this.button0h00.Name = "button0h00";
            this.button0h00.Size = new System.Drawing.Size(40, 21);
            this.button0h00.TabIndex = 48;
            this.button0h00.Text = "0:00";
            this.button0h00.UseVisualStyleBackColor = false;
            this.button0h00.Click += new System.EventHandler(this.button0h00_Click);
            // 
            // buttonX00
            // 
            this.buttonX00.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonX00.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonX00.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonX00.FlatAppearance.BorderSize = 0;
            this.buttonX00.Font = new System.Drawing.Font("Arial", 9F);
            this.buttonX00.Location = new System.Drawing.Point(130, 0);
            this.buttonX00.Name = "buttonX00";
            this.buttonX00.Size = new System.Drawing.Size(40, 21);
            this.buttonX00.TabIndex = 49;
            this.buttonX00.Text = "x:00";
            this.buttonX00.UseVisualStyleBackColor = false;
            this.buttonX00.Click += new System.EventHandler(this.buttonX00_Click);
            // 
            // buttonM1
            // 
            this.buttonM1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonM1.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonM1.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonM1.FlatAppearance.BorderSize = 0;
            this.buttonM1.Font = new System.Drawing.Font("Arial", 9F);
            this.buttonM1.Location = new System.Drawing.Point(90, 0);
            this.buttonM1.Name = "buttonM1";
            this.buttonM1.Size = new System.Drawing.Size(40, 21);
            this.buttonM1.TabIndex = 44;
            this.buttonM1.Text = "-1 h";
            this.buttonM1.UseVisualStyleBackColor = false;
            this.buttonM1.Click += new System.EventHandler(this.buttonM1_Click);
            // 
            // buttonM6
            // 
            this.buttonM6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonM6.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonM6.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonM6.FlatAppearance.BorderSize = 0;
            this.buttonM6.Font = new System.Drawing.Font("Arial", 9F);
            this.buttonM6.Location = new System.Drawing.Point(50, 0);
            this.buttonM6.Name = "buttonM6";
            this.buttonM6.Size = new System.Drawing.Size(40, 21);
            this.buttonM6.TabIndex = 43;
            this.buttonM6.Text = "-6 h";
            this.buttonM6.UseVisualStyleBackColor = false;
            this.buttonM6.Click += new System.EventHandler(this.buttonM6_Click);
            // 
            // buttonM24
            // 
            this.buttonM24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonM24.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonM24.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonM24.FlatAppearance.BorderSize = 0;
            this.buttonM24.Font = new System.Drawing.Font("Arial", 9F);
            this.buttonM24.Location = new System.Drawing.Point(10, 0);
            this.buttonM24.Name = "buttonM24";
            this.buttonM24.Size = new System.Drawing.Size(40, 21);
            this.buttonM24.TabIndex = 42;
            this.buttonM24.Text = "-24h";
            this.buttonM24.UseVisualStyleBackColor = false;
            this.buttonM24.Click += new System.EventHandler(this.buttonM24_Click);
            // 
            // panel31
            // 
            this.panel31.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel31.Location = new System.Drawing.Point(0, 0);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(10, 21);
            this.panel31.TabIndex = 50;
            // 
            // panel62
            // 
            this.panel62.BackColor = System.Drawing.SystemColors.Info;
            this.panel62.Controls.Add(this.labelReqDur);
            this.panel62.Controls.Add(this.comboBoxToAM);
            this.panel62.Controls.Add(this.textBoxToMin);
            this.panel62.Controls.Add(this.textBoxToHour);
            this.panel62.Controls.Add(this.dateTimePickerTo);
            this.panel62.Controls.Add(this.labelTo);
            this.panel62.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel62.Location = new System.Drawing.Point(0, 20);
            this.panel62.Name = "panel62";
            this.panel62.Size = new System.Drawing.Size(343, 20);
            this.panel62.TabIndex = 1;
            // 
            // labelReqDur
            // 
            this.labelReqDur.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelReqDur.Font = new System.Drawing.Font("Arial", 10F);
            this.labelReqDur.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelReqDur.Location = new System.Drawing.Point(264, 0);
            this.labelReqDur.Name = "labelReqDur";
            this.labelReqDur.Size = new System.Drawing.Size(79, 20);
            this.labelReqDur.TabIndex = 152;
            this.labelReqDur.Text = "^99d24h22";
            this.labelReqDur.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelReqDur.Click += new System.EventHandler(this.labelReqDur_Click);
            // 
            // comboBoxToAM
            // 
            this.comboBoxToAM.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBoxToAM.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxToAM.Dock = System.Windows.Forms.DockStyle.Left;
            this.comboBoxToAM.Font = new System.Drawing.Font("Arial", 9F);
            this.comboBoxToAM.FormattingEnabled = true;
            this.comboBoxToAM.Items.AddRange(new object[] {
            "AM",
            "PM"});
            this.comboBoxToAM.Location = new System.Drawing.Point(219, 0);
            this.comboBoxToAM.Name = "comboBoxToAM";
            this.comboBoxToAM.Size = new System.Drawing.Size(45, 23);
            this.comboBoxToAM.TabIndex = 35;
            // 
            // textBoxToMin
            // 
            this.textBoxToMin.Dock = System.Windows.Forms.DockStyle.Left;
            this.textBoxToMin.Font = new System.Drawing.Font("Arial", 9F);
            this.textBoxToMin.Location = new System.Drawing.Point(183, 0);
            this.textBoxToMin.Name = "textBoxToMin";
            this.textBoxToMin.Size = new System.Drawing.Size(36, 21);
            this.textBoxToMin.TabIndex = 35;
            this.textBoxToMin.Text = "30";
            // 
            // textBoxToHour
            // 
            this.textBoxToHour.Dock = System.Windows.Forms.DockStyle.Left;
            this.textBoxToHour.Font = new System.Drawing.Font("Arial", 9F);
            this.textBoxToHour.Location = new System.Drawing.Point(151, 0);
            this.textBoxToHour.Name = "textBoxToHour";
            this.textBoxToHour.Size = new System.Drawing.Size(32, 21);
            this.textBoxToHour.TabIndex = 30;
            this.textBoxToHour.Text = "11";
            // 
            // dateTimePickerTo
            // 
            this.dateTimePickerTo.Dock = System.Windows.Forms.DockStyle.Left;
            this.dateTimePickerTo.Font = new System.Drawing.Font("Arial", 9F);
            this.dateTimePickerTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerTo.Location = new System.Drawing.Point(52, 0);
            this.dateTimePickerTo.Name = "dateTimePickerTo";
            this.dateTimePickerTo.Size = new System.Drawing.Size(99, 21);
            this.dateTimePickerTo.TabIndex = 27;
            // 
            // labelTo
            // 
            this.labelTo.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelTo.Font = new System.Drawing.Font("Arial", 10F);
            this.labelTo.Location = new System.Drawing.Point(0, 0);
            this.labelTo.Name = "labelTo";
            this.labelTo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labelTo.Size = new System.Drawing.Size(52, 20);
            this.labelTo.TabIndex = 13;
            this.labelTo.Text = "To:";
            this.labelTo.Click += new System.EventHandler(this.labelTo_Click);
            // 
            // panel57
            // 
            this.panel57.BackColor = System.Drawing.SystemColors.Info;
            this.panel57.Controls.Add(this.comboBoxFromAM);
            this.panel57.Controls.Add(this.textBoxFromMin);
            this.panel57.Controls.Add(this.textBoxFromHour);
            this.panel57.Controls.Add(this.dateTimePickerFrom);
            this.panel57.Controls.Add(this.labelFrom);
            this.panel57.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel57.Location = new System.Drawing.Point(0, 0);
            this.panel57.Name = "panel57";
            this.panel57.Size = new System.Drawing.Size(343, 20);
            this.panel57.TabIndex = 0;
            // 
            // comboBoxFromAM
            // 
            this.comboBoxFromAM.Dock = System.Windows.Forms.DockStyle.Left;
            this.comboBoxFromAM.Font = new System.Drawing.Font("Arial", 9F);
            this.comboBoxFromAM.FormattingEnabled = true;
            this.comboBoxFromAM.Items.AddRange(new object[] {
            "AM",
            "PM"});
            this.comboBoxFromAM.Location = new System.Drawing.Point(219, 0);
            this.comboBoxFromAM.Name = "comboBoxFromAM";
            this.comboBoxFromAM.Size = new System.Drawing.Size(45, 23);
            this.comboBoxFromAM.TabIndex = 34;
            // 
            // textBoxFromMin
            // 
            this.textBoxFromMin.Dock = System.Windows.Forms.DockStyle.Left;
            this.textBoxFromMin.Font = new System.Drawing.Font("Arial", 9F);
            this.textBoxFromMin.Location = new System.Drawing.Point(183, 0);
            this.textBoxFromMin.Name = "textBoxFromMin";
            this.textBoxFromMin.Size = new System.Drawing.Size(36, 21);
            this.textBoxFromMin.TabIndex = 34;
            this.textBoxFromMin.Text = "30";
            // 
            // textBoxFromHour
            // 
            this.textBoxFromHour.Dock = System.Windows.Forms.DockStyle.Left;
            this.textBoxFromHour.Font = new System.Drawing.Font("Arial", 9F);
            this.textBoxFromHour.Location = new System.Drawing.Point(151, 0);
            this.textBoxFromHour.Name = "textBoxFromHour";
            this.textBoxFromHour.Size = new System.Drawing.Size(32, 21);
            this.textBoxFromHour.TabIndex = 29;
            this.textBoxFromHour.Text = "11";
            // 
            // dateTimePickerFrom
            // 
            this.dateTimePickerFrom.CalendarMonthBackground = System.Drawing.SystemColors.Info;
            this.dateTimePickerFrom.Dock = System.Windows.Forms.DockStyle.Left;
            this.dateTimePickerFrom.Font = new System.Drawing.Font("Arial", 9F);
            this.dateTimePickerFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerFrom.Location = new System.Drawing.Point(52, 0);
            this.dateTimePickerFrom.Name = "dateTimePickerFrom";
            this.dateTimePickerFrom.Size = new System.Drawing.Size(99, 21);
            this.dateTimePickerFrom.TabIndex = 26;
            // 
            // labelFrom
            // 
            this.labelFrom.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelFrom.Font = new System.Drawing.Font("Arial", 10F);
            this.labelFrom.Location = new System.Drawing.Point(0, 0);
            this.labelFrom.Name = "labelFrom";
            this.labelFrom.Size = new System.Drawing.Size(52, 20);
            this.labelFrom.TabIndex = 12;
            this.labelFrom.Text = "From:";
            this.labelFrom.Click += new System.EventHandler(this.labelFrom_Click);
            // 
            // panelPeriod
            // 
            this.panelPeriod.Controls.Add(this.panel18);
            this.panelPeriod.Controls.Add(this.panel11);
            this.panelPeriod.Controls.Add(this.panel54);
            this.panelPeriod.Controls.Add(this.label36);
            this.panelPeriod.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelPeriod.Location = new System.Drawing.Point(0, 0);
            this.panelPeriod.Name = "panelPeriod";
            this.panelPeriod.Size = new System.Drawing.Size(487, 89);
            this.panelPeriod.TabIndex = 22;
            this.panelPeriod.Paint += new System.Windows.Forms.PaintEventHandler(this.panelPeriod_Paint);
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.checkBoxInv10);
            this.panel18.Controls.Add(this.checkBoxInv9);
            this.panel18.Controls.Add(this.checkBoxInv8);
            this.panel18.Controls.Add(this.checkBoxInv7);
            this.panel18.Controls.Add(this.checkBoxInv6);
            this.panel18.Controls.Add(this.checkBoxInv5);
            this.panel18.Controls.Add(this.checkBoxInv4);
            this.panel18.Controls.Add(this.checkBoxInv3);
            this.panel18.Controls.Add(this.checkBoxInv2);
            this.panel18.Controls.Add(this.checkBoxInv1);
            this.panel18.Controls.Add(this.panel55);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel18.Location = new System.Drawing.Point(0, 60);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(487, 29);
            this.panel18.TabIndex = 52;
            // 
            // checkBoxInv10
            // 
            this.checkBoxInv10.AutoSize = true;
            this.checkBoxInv10.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxInv10.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxInv10.Location = new System.Drawing.Point(425, 0);
            this.checkBoxInv10.Name = "checkBoxInv10";
            this.checkBoxInv10.Size = new System.Drawing.Size(43, 29);
            this.checkBoxInv10.TabIndex = 62;
            this.checkBoxInv10.Text = "10";
            this.checkBoxInv10.UseVisualStyleBackColor = true;
            // 
            // checkBoxInv9
            // 
            this.checkBoxInv9.AutoSize = true;
            this.checkBoxInv9.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxInv9.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxInv9.Location = new System.Drawing.Point(390, 0);
            this.checkBoxInv9.Name = "checkBoxInv9";
            this.checkBoxInv9.Size = new System.Drawing.Size(35, 29);
            this.checkBoxInv9.TabIndex = 61;
            this.checkBoxInv9.Text = "9";
            this.checkBoxInv9.UseVisualStyleBackColor = true;
            // 
            // checkBoxInv8
            // 
            this.checkBoxInv8.AutoSize = true;
            this.checkBoxInv8.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxInv8.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxInv8.Location = new System.Drawing.Point(355, 0);
            this.checkBoxInv8.Name = "checkBoxInv8";
            this.checkBoxInv8.Size = new System.Drawing.Size(35, 29);
            this.checkBoxInv8.TabIndex = 60;
            this.checkBoxInv8.Text = "8";
            this.checkBoxInv8.UseVisualStyleBackColor = true;
            // 
            // checkBoxInv7
            // 
            this.checkBoxInv7.AutoSize = true;
            this.checkBoxInv7.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxInv7.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxInv7.Location = new System.Drawing.Point(320, 0);
            this.checkBoxInv7.Name = "checkBoxInv7";
            this.checkBoxInv7.Size = new System.Drawing.Size(35, 29);
            this.checkBoxInv7.TabIndex = 59;
            this.checkBoxInv7.Text = "7";
            this.checkBoxInv7.UseVisualStyleBackColor = true;
            // 
            // checkBoxInv6
            // 
            this.checkBoxInv6.AutoSize = true;
            this.checkBoxInv6.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxInv6.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxInv6.Location = new System.Drawing.Point(285, 0);
            this.checkBoxInv6.Name = "checkBoxInv6";
            this.checkBoxInv6.Size = new System.Drawing.Size(35, 29);
            this.checkBoxInv6.TabIndex = 58;
            this.checkBoxInv6.Text = "6";
            this.checkBoxInv6.UseVisualStyleBackColor = true;
            // 
            // checkBoxInv5
            // 
            this.checkBoxInv5.AutoSize = true;
            this.checkBoxInv5.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxInv5.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxInv5.Location = new System.Drawing.Point(250, 0);
            this.checkBoxInv5.Name = "checkBoxInv5";
            this.checkBoxInv5.Size = new System.Drawing.Size(35, 29);
            this.checkBoxInv5.TabIndex = 57;
            this.checkBoxInv5.Text = "5";
            this.checkBoxInv5.UseVisualStyleBackColor = true;
            // 
            // checkBoxInv4
            // 
            this.checkBoxInv4.AutoSize = true;
            this.checkBoxInv4.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxInv4.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxInv4.Location = new System.Drawing.Point(215, 0);
            this.checkBoxInv4.Name = "checkBoxInv4";
            this.checkBoxInv4.Size = new System.Drawing.Size(35, 29);
            this.checkBoxInv4.TabIndex = 56;
            this.checkBoxInv4.Text = "4";
            this.checkBoxInv4.UseVisualStyleBackColor = true;
            // 
            // checkBoxInv3
            // 
            this.checkBoxInv3.AutoSize = true;
            this.checkBoxInv3.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxInv3.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxInv3.Location = new System.Drawing.Point(180, 0);
            this.checkBoxInv3.Name = "checkBoxInv3";
            this.checkBoxInv3.Size = new System.Drawing.Size(35, 29);
            this.checkBoxInv3.TabIndex = 55;
            this.checkBoxInv3.Text = "3";
            this.checkBoxInv3.UseVisualStyleBackColor = true;
            // 
            // checkBoxInv2
            // 
            this.checkBoxInv2.AutoSize = true;
            this.checkBoxInv2.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxInv2.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxInv2.Location = new System.Drawing.Point(145, 0);
            this.checkBoxInv2.Name = "checkBoxInv2";
            this.checkBoxInv2.Size = new System.Drawing.Size(35, 29);
            this.checkBoxInv2.TabIndex = 54;
            this.checkBoxInv2.Text = "2";
            this.checkBoxInv2.UseVisualStyleBackColor = true;
            // 
            // checkBoxInv1
            // 
            this.checkBoxInv1.AutoSize = true;
            this.checkBoxInv1.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxInv1.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxInv1.Location = new System.Drawing.Point(110, 0);
            this.checkBoxInv1.Margin = new System.Windows.Forms.Padding(0);
            this.checkBoxInv1.Name = "checkBoxInv1";
            this.checkBoxInv1.Size = new System.Drawing.Size(35, 29);
            this.checkBoxInv1.TabIndex = 53;
            this.checkBoxInv1.Text = "1";
            this.checkBoxInv1.UseVisualStyleBackColor = true;
            // 
            // panel55
            // 
            this.panel55.Controls.Add(this.label28);
            this.panel55.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel55.Location = new System.Drawing.Point(0, 0);
            this.panel55.Name = "panel55";
            this.panel55.Size = new System.Drawing.Size(110, 29);
            this.panel55.TabIndex = 64;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Dock = System.Windows.Forms.DockStyle.Left;
            this.label28.Location = new System.Drawing.Point(0, 0);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(104, 16);
            this.label28.TabIndex = 64;
            this.label28.Text = "Invert Channel:";
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.checkBoxCh10);
            this.panel11.Controls.Add(this.checkBoxCh9);
            this.panel11.Controls.Add(this.checkBoxCh8);
            this.panel11.Controls.Add(this.checkBoxCh7);
            this.panel11.Controls.Add(this.checkBoxCh6);
            this.panel11.Controls.Add(this.checkBoxCh5);
            this.panel11.Controls.Add(this.checkBoxCh4);
            this.panel11.Controls.Add(this.checkBoxCh3);
            this.panel11.Controls.Add(this.checkBoxCh2);
            this.panel11.Controls.Add(this.checkBoxCh1);
            this.panel11.Controls.Add(this.checkBoxChAll);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel11.Location = new System.Drawing.Point(0, 41);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(487, 20);
            this.panel11.TabIndex = 51;
            // 
            // checkBoxCh10
            // 
            this.checkBoxCh10.AutoSize = true;
            this.checkBoxCh10.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxCh10.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxCh10.Location = new System.Drawing.Point(425, 0);
            this.checkBoxCh10.Name = "checkBoxCh10";
            this.checkBoxCh10.Size = new System.Drawing.Size(43, 20);
            this.checkBoxCh10.TabIndex = 52;
            this.checkBoxCh10.Text = "10";
            this.checkBoxCh10.UseVisualStyleBackColor = true;
            this.checkBoxCh10.CheckStateChanged += new System.EventHandler(this.checkBoxCh10_CheckStateChanged);
            // 
            // checkBoxCh9
            // 
            this.checkBoxCh9.AutoSize = true;
            this.checkBoxCh9.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxCh9.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxCh9.Location = new System.Drawing.Point(390, 0);
            this.checkBoxCh9.Name = "checkBoxCh9";
            this.checkBoxCh9.Size = new System.Drawing.Size(35, 20);
            this.checkBoxCh9.TabIndex = 51;
            this.checkBoxCh9.Text = "9";
            this.checkBoxCh9.UseVisualStyleBackColor = true;
            this.checkBoxCh9.CheckedChanged += new System.EventHandler(this.checkBoxCh9_CheckedChanged);
            this.checkBoxCh9.CheckStateChanged += new System.EventHandler(this.checkBoxCh9_CheckStateChanged);
            // 
            // checkBoxCh8
            // 
            this.checkBoxCh8.AutoSize = true;
            this.checkBoxCh8.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxCh8.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxCh8.Location = new System.Drawing.Point(355, 0);
            this.checkBoxCh8.Name = "checkBoxCh8";
            this.checkBoxCh8.Size = new System.Drawing.Size(35, 20);
            this.checkBoxCh8.TabIndex = 50;
            this.checkBoxCh8.Text = "8";
            this.checkBoxCh8.UseVisualStyleBackColor = true;
            this.checkBoxCh8.CheckedChanged += new System.EventHandler(this.checkBoxCh8_CheckedChanged);
            this.checkBoxCh8.CheckStateChanged += new System.EventHandler(this.checkBoxCh8_CheckStateChanged);
            // 
            // checkBoxCh7
            // 
            this.checkBoxCh7.AutoSize = true;
            this.checkBoxCh7.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxCh7.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxCh7.Location = new System.Drawing.Point(320, 0);
            this.checkBoxCh7.Name = "checkBoxCh7";
            this.checkBoxCh7.Size = new System.Drawing.Size(35, 20);
            this.checkBoxCh7.TabIndex = 49;
            this.checkBoxCh7.Text = "7";
            this.checkBoxCh7.UseVisualStyleBackColor = true;
            this.checkBoxCh7.CheckStateChanged += new System.EventHandler(this.checkBoxCh7_CheckStateChanged);
            // 
            // checkBoxCh6
            // 
            this.checkBoxCh6.AutoSize = true;
            this.checkBoxCh6.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxCh6.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxCh6.Location = new System.Drawing.Point(285, 0);
            this.checkBoxCh6.Name = "checkBoxCh6";
            this.checkBoxCh6.Size = new System.Drawing.Size(35, 20);
            this.checkBoxCh6.TabIndex = 48;
            this.checkBoxCh6.Text = "6";
            this.checkBoxCh6.UseVisualStyleBackColor = true;
            this.checkBoxCh6.CheckStateChanged += new System.EventHandler(this.checkBoxCh6_CheckStateChanged);
            // 
            // checkBoxCh5
            // 
            this.checkBoxCh5.AutoSize = true;
            this.checkBoxCh5.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxCh5.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxCh5.Location = new System.Drawing.Point(250, 0);
            this.checkBoxCh5.Name = "checkBoxCh5";
            this.checkBoxCh5.Size = new System.Drawing.Size(35, 20);
            this.checkBoxCh5.TabIndex = 47;
            this.checkBoxCh5.Text = "5";
            this.checkBoxCh5.UseVisualStyleBackColor = true;
            this.checkBoxCh5.CheckStateChanged += new System.EventHandler(this.checkBoxCh5_CheckStateChanged);
            // 
            // checkBoxCh4
            // 
            this.checkBoxCh4.AutoSize = true;
            this.checkBoxCh4.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxCh4.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxCh4.Location = new System.Drawing.Point(215, 0);
            this.checkBoxCh4.Name = "checkBoxCh4";
            this.checkBoxCh4.Size = new System.Drawing.Size(35, 20);
            this.checkBoxCh4.TabIndex = 46;
            this.checkBoxCh4.Text = "4";
            this.checkBoxCh4.UseVisualStyleBackColor = true;
            this.checkBoxCh4.CheckedChanged += new System.EventHandler(this.checkBoxCh4_CheckedChanged);
            this.checkBoxCh4.CheckStateChanged += new System.EventHandler(this.checkBoxCh4_CheckStateChanged);
            // 
            // checkBoxCh3
            // 
            this.checkBoxCh3.AutoSize = true;
            this.checkBoxCh3.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxCh3.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxCh3.Location = new System.Drawing.Point(180, 0);
            this.checkBoxCh3.Name = "checkBoxCh3";
            this.checkBoxCh3.Size = new System.Drawing.Size(35, 20);
            this.checkBoxCh3.TabIndex = 45;
            this.checkBoxCh3.Text = "3";
            this.checkBoxCh3.UseVisualStyleBackColor = true;
            this.checkBoxCh3.CheckStateChanged += new System.EventHandler(this.checkBoxCh3_CheckStateChanged);
            // 
            // checkBoxCh2
            // 
            this.checkBoxCh2.AutoSize = true;
            this.checkBoxCh2.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxCh2.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxCh2.Location = new System.Drawing.Point(145, 0);
            this.checkBoxCh2.Name = "checkBoxCh2";
            this.checkBoxCh2.Size = new System.Drawing.Size(35, 20);
            this.checkBoxCh2.TabIndex = 44;
            this.checkBoxCh2.Text = "2";
            this.checkBoxCh2.UseVisualStyleBackColor = true;
            this.checkBoxCh2.CheckStateChanged += new System.EventHandler(this.checkBoxCh2_CheckStateChanged);
            // 
            // checkBoxCh1
            // 
            this.checkBoxCh1.AutoSize = true;
            this.checkBoxCh1.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxCh1.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxCh1.Location = new System.Drawing.Point(110, 0);
            this.checkBoxCh1.Name = "checkBoxCh1";
            this.checkBoxCh1.Size = new System.Drawing.Size(35, 20);
            this.checkBoxCh1.TabIndex = 43;
            this.checkBoxCh1.Text = "1";
            this.checkBoxCh1.UseVisualStyleBackColor = true;
            this.checkBoxCh1.CheckStateChanged += new System.EventHandler(this.checkBoxCh1_CheckStateChanged);
            // 
            // checkBoxChAll
            // 
            this.checkBoxChAll.AutoSize = true;
            this.checkBoxChAll.Checked = true;
            this.checkBoxChAll.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxChAll.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxChAll.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxChAll.Location = new System.Drawing.Point(0, 0);
            this.checkBoxChAll.Name = "checkBoxChAll";
            this.checkBoxChAll.Size = new System.Drawing.Size(110, 20);
            this.checkBoxChAll.TabIndex = 42;
            this.checkBoxChAll.Text = "All Channels:";
            this.checkBoxChAll.UseVisualStyleBackColor = true;
            // 
            // panel54
            // 
            this.panel54.BackColor = System.Drawing.SystemColors.Info;
            this.panel54.Controls.Add(this.radioButtonMonth);
            this.panel54.Controls.Add(this.radioButtonWeek);
            this.panel54.Controls.Add(this.radioButtonDay);
            this.panel54.Controls.Add(this.radioButton12Hour);
            this.panel54.Controls.Add(this.radioButton6Hour);
            this.panel54.Controls.Add(this.radioButtonHour);
            this.panel54.Controls.Add(this.radioButtonPeriod);
            this.panel54.Controls.Add(this.radioButtonAll);
            this.panel54.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel54.Location = new System.Drawing.Point(0, 17);
            this.panel54.Name = "panel54";
            this.panel54.Size = new System.Drawing.Size(487, 24);
            this.panel54.TabIndex = 37;
            // 
            // radioButtonMonth
            // 
            this.radioButtonMonth.AutoSize = true;
            this.radioButtonMonth.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioButtonMonth.Font = new System.Drawing.Font("Arial", 10F);
            this.radioButtonMonth.Location = new System.Drawing.Point(418, 0);
            this.radioButtonMonth.Name = "radioButtonMonth";
            this.radioButtonMonth.Size = new System.Drawing.Size(65, 24);
            this.radioButtonMonth.TabIndex = 45;
            this.radioButtonMonth.Text = "Month";
            this.radioButtonMonth.UseVisualStyleBackColor = true;
            this.radioButtonMonth.CheckedChanged += new System.EventHandler(this.radioButtonMonth_CheckedChanged);
            // 
            // radioButtonWeek
            // 
            this.radioButtonWeek.AutoSize = true;
            this.radioButtonWeek.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioButtonWeek.Font = new System.Drawing.Font("Arial", 10F);
            this.radioButtonWeek.Location = new System.Drawing.Point(356, 0);
            this.radioButtonWeek.Name = "radioButtonWeek";
            this.radioButtonWeek.Size = new System.Drawing.Size(62, 24);
            this.radioButtonWeek.TabIndex = 43;
            this.radioButtonWeek.Text = "Week";
            this.radioButtonWeek.UseVisualStyleBackColor = true;
            this.radioButtonWeek.CheckedChanged += new System.EventHandler(this.radioButtonWeek_CheckedChanged);
            // 
            // radioButtonDay
            // 
            this.radioButtonDay.AutoSize = true;
            this.radioButtonDay.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioButtonDay.Font = new System.Drawing.Font("Arial", 10F);
            this.radioButtonDay.Location = new System.Drawing.Point(305, 0);
            this.radioButtonDay.Name = "radioButtonDay";
            this.radioButtonDay.Size = new System.Drawing.Size(51, 24);
            this.radioButtonDay.TabIndex = 47;
            this.radioButtonDay.Text = "Day";
            this.radioButtonDay.UseVisualStyleBackColor = true;
            this.radioButtonDay.CheckedChanged += new System.EventHandler(this.radioButtonDay_CheckedChanged);
            // 
            // radioButton12Hour
            // 
            this.radioButton12Hour.AutoSize = true;
            this.radioButton12Hour.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioButton12Hour.Font = new System.Drawing.Font("Arial", 10F);
            this.radioButton12Hour.Location = new System.Drawing.Point(239, 0);
            this.radioButton12Hour.Name = "radioButton12Hour";
            this.radioButton12Hour.Size = new System.Drawing.Size(66, 24);
            this.radioButton12Hour.TabIndex = 46;
            this.radioButton12Hour.Text = "12 hrs";
            this.radioButton12Hour.UseVisualStyleBackColor = true;
            this.radioButton12Hour.CheckedChanged += new System.EventHandler(this.radioButton12Hour_CheckedChanged);
            // 
            // radioButton6Hour
            // 
            this.radioButton6Hour.AutoSize = true;
            this.radioButton6Hour.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioButton6Hour.Font = new System.Drawing.Font("Arial", 10F);
            this.radioButton6Hour.Location = new System.Drawing.Point(181, 0);
            this.radioButton6Hour.Name = "radioButton6Hour";
            this.radioButton6Hour.Size = new System.Drawing.Size(58, 24);
            this.radioButton6Hour.TabIndex = 44;
            this.radioButton6Hour.Text = "6 hrs";
            this.radioButton6Hour.UseVisualStyleBackColor = true;
            this.radioButton6Hour.CheckedChanged += new System.EventHandler(this.radioButton6Hour_CheckedChanged);
            // 
            // radioButtonHour
            // 
            this.radioButtonHour.AutoSize = true;
            this.radioButtonHour.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioButtonHour.Font = new System.Drawing.Font("Arial", 10F);
            this.radioButtonHour.Location = new System.Drawing.Point(125, 0);
            this.radioButtonHour.Name = "radioButtonHour";
            this.radioButtonHour.Size = new System.Drawing.Size(56, 24);
            this.radioButtonHour.TabIndex = 42;
            this.radioButtonHour.Text = "Hour";
            this.radioButtonHour.UseVisualStyleBackColor = true;
            this.radioButtonHour.CheckedChanged += new System.EventHandler(this.radioButtonHour_CheckedChanged);
            // 
            // radioButtonPeriod
            // 
            this.radioButtonPeriod.AutoSize = true;
            this.radioButtonPeriod.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioButtonPeriod.Font = new System.Drawing.Font("Arial", 10F);
            this.radioButtonPeriod.Location = new System.Drawing.Point(41, 0);
            this.radioButtonPeriod.Name = "radioButtonPeriod";
            this.radioButtonPeriod.Size = new System.Drawing.Size(84, 24);
            this.radioButtonPeriod.TabIndex = 48;
            this.radioButtonPeriod.Text = "Selection";
            this.radioButtonPeriod.UseVisualStyleBackColor = true;
            this.radioButtonPeriod.CheckedChanged += new System.EventHandler(this.radioButtonPeriod_CheckedChanged);
            // 
            // radioButtonAll
            // 
            this.radioButtonAll.AutoSize = true;
            this.radioButtonAll.Checked = true;
            this.radioButtonAll.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioButtonAll.Font = new System.Drawing.Font("Arial", 10F);
            this.radioButtonAll.Location = new System.Drawing.Point(0, 0);
            this.radioButtonAll.Name = "radioButtonAll";
            this.radioButtonAll.Size = new System.Drawing.Size(41, 24);
            this.radioButtonAll.TabIndex = 49;
            this.radioButtonAll.TabStop = true;
            this.radioButtonAll.Text = "All";
            this.radioButtonAll.UseVisualStyleBackColor = true;
            this.radioButtonAll.CheckedChanged += new System.EventHandler(this.radioButtonAll_CheckedChanged);
            // 
            // label36
            // 
            this.label36.BackColor = System.Drawing.SystemColors.Info;
            this.label36.Dock = System.Windows.Forms.DockStyle.Top;
            this.label36.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label36.Location = new System.Drawing.Point(0, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(487, 17);
            this.label36.TabIndex = 36;
            this.label36.Text = "Create Holter Period";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label36.Click += new System.EventHandler(this.label36_Click);
            // 
            // panelStudy
            // 
            this.panelStudy.AutoSize = true;
            this.panelStudy.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelStudy.Controls.Add(this.panel16);
            this.panelStudy.Controls.Add(this.panelStudyPeriod);
            this.panelStudy.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelStudy.Location = new System.Drawing.Point(0, 663);
            this.panelStudy.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelStudy.Name = "panelStudy";
            this.panelStudy.Size = new System.Drawing.Size(1304, 115);
            this.panelStudy.TabIndex = 5;
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.labelRecordStats);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel16.Location = new System.Drawing.Point(0, 0);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(1304, 24);
            this.panel16.TabIndex = 14;
            // 
            // labelRecordStats
            // 
            this.labelRecordStats.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelRecordStats.Font = new System.Drawing.Font("Arial", 10F);
            this.labelRecordStats.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelRecordStats.Location = new System.Drawing.Point(0, 0);
            this.labelRecordStats.Name = "labelRecordStats";
            this.labelRecordStats.Size = new System.Drawing.Size(1304, 24);
            this.labelRecordStats.TabIndex = 110;
            this.labelRecordStats.Text = "^Record stats: start hjkhgjks, End hghdfgkhj, duration , gap, overlap";
            this.labelRecordStats.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panelRows
            // 
            this.panelRows.Controls.Add(this.dataGridView);
            this.panelRows.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelRows.Location = new System.Drawing.Point(0, 88);
            this.panelRows.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelRows.Name = "panelRows";
            this.panelRows.Size = new System.Drawing.Size(1304, 523);
            this.panelRows.TabIndex = 7;
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.FileName,
            this.Column6,
            this.Column7,
            this.Column4,
            this.ColumnEventDate,
            this.Column5,
            this.ColumnAnalysisDate,
            this.Column2,
            this.Column3,
            this.ColumnFindings,
            this.Column14,
            this.ColumnRemarks});
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Arial", 10F);
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView.Location = new System.Drawing.Point(0, 0);
            this.dataGridView.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.RowHeadersVisible = false;
            this.dataGridView.RowTemplate.Height = 33;
            this.dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView.Size = new System.Drawing.Size(1304, 523);
            this.dataGridView.TabIndex = 3;
            this.dataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellClick);
            this.dataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellContentClick);
            this.dataGridView.Click += new System.EventHandler(this.dataGridView_Click);
            this.dataGridView.DoubleClick += new System.EventHandler(this.dataGridView_DoubleClick);
            this.dataGridView.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dataGridView_MouseClick);
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.Column1.HeaderText = "Add File";
            this.Column1.MinimumWidth = 54;
            this.Column1.Name = "Column1";
            this.Column1.Width = 54;
            // 
            // FileName
            // 
            this.FileName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.FileName.HeaderText = "File Name";
            this.FileName.Name = "FileName";
            this.FileName.ReadOnly = true;
            this.FileName.Width = 88;
            // 
            // Column6
            // 
            this.Column6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column6.HeaderText = "File date";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 81;
            // 
            // Column7
            // 
            this.Column7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Column7.DefaultCellStyle = dataGridViewCellStyle1;
            this.Column7.HeaderText = "File Size";
            this.Column7.MaxInputLength = 6;
            this.Column7.MinimumWidth = 60;
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 60;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Column4.DefaultCellStyle = dataGridViewCellStyle2;
            this.Column4.HeaderText = "File Strip Start";
            this.Column4.MaxInputLength = 6;
            this.Column4.MinimumWidth = 50;
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 50;
            // 
            // ColumnEventDate
            // 
            this.ColumnEventDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.ColumnEventDate.DefaultCellStyle = dataGridViewCellStyle3;
            this.ColumnEventDate.HeaderText = "File dur.";
            this.ColumnEventDate.MaxInputLength = 15;
            this.ColumnEventDate.Name = "ColumnEventDate";
            this.ColumnEventDate.ReadOnly = true;
            this.ColumnEventDate.Width = 78;
            // 
            // Column5
            // 
            this.Column5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Column5.DefaultCellStyle = dataGridViewCellStyle4;
            this.Column5.HeaderText = "Gap";
            this.Column5.Name = "Column5";
            this.Column5.Width = 60;
            // 
            // ColumnAnalysisDate
            // 
            this.ColumnAnalysisDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.ColumnAnalysisDate.HeaderText = "Record Start";
            this.ColumnAnalysisDate.MaxInputLength = 15;
            this.ColumnAnalysisDate.MinimumWidth = 100;
            this.ColumnAnalysisDate.Name = "ColumnAnalysisDate";
            this.ColumnAnalysisDate.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Column2.DefaultCellStyle = dataGridViewCellStyle5;
            this.Column2.HeaderText = "Strip";
            this.Column2.MaxInputLength = 4;
            this.Column2.MinimumWidth = 44;
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 62;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Column3.DefaultCellStyle = dataGridViewCellStyle6;
            this.Column3.HeaderText = "Ch";
            this.Column3.MaxInputLength = 9;
            this.Column3.MinimumWidth = 40;
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 40;
            // 
            // ColumnFindings
            // 
            this.ColumnFindings.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.ColumnFindings.DefaultCellStyle = dataGridViewCellStyle7;
            this.ColumnFindings.HeaderText = "Sps";
            this.ColumnFindings.MaxInputLength = 9;
            this.ColumnFindings.MinimumWidth = 60;
            this.ColumnFindings.Name = "ColumnFindings";
            this.ColumnFindings.ReadOnly = true;
            this.ColumnFindings.Width = 60;
            // 
            // Column14
            // 
            this.Column14.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Column14.DefaultCellStyle = dataGridViewCellStyle8;
            this.Column14.HeaderText = "Samples";
            this.Column14.Name = "Column14";
            this.Column14.Width = 87;
            // 
            // ColumnRemarks
            // 
            this.ColumnRemarks.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColumnRemarks.HeaderText = "Remark";
            this.ColumnRemarks.MaxInputLength = 32;
            this.ColumnRemarks.MinimumWidth = 200;
            this.ColumnRemarks.Name = "ColumnRemarks";
            this.ColumnRemarks.ReadOnly = true;
            this.ColumnRemarks.Width = 200;
            // 
            // buttonSortET
            // 
            this.buttonSortET.BackColor = System.Drawing.Color.Transparent;
            this.buttonSortET.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonSortET.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonSortET.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonSortET.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSortET.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.buttonSortET.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonSortET.Location = new System.Drawing.Point(1149, 0);
            this.buttonSortET.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonSortET.Name = "buttonSortET";
            this.buttonSortET.Size = new System.Drawing.Size(149, 28);
            this.buttonSortET.TabIndex = 53;
            this.buttonSortET.Text = "Sort by Event Time";
            this.buttonSortET.UseVisualStyleBackColor = false;
            this.buttonSortET.Visible = false;
            this.buttonSortET.Click += new System.EventHandler(this.buttonSortET_Click);
            // 
            // panel19
            // 
            this.panel19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel19.Controls.Add(this.labelHolterSize);
            this.panel19.Controls.Add(this.button1);
            this.panel19.Controls.Add(this.buttonSetAll);
            this.panel19.Controls.Add(this.labelMemInfo);
            this.panel19.Controls.Add(this.buttonResetAll);
            this.panel19.Controls.Add(this.panel29);
            this.panel19.Controls.Add(this.buttonReloadList);
            this.panel19.Controls.Add(this.panel28);
            this.panel19.Controls.Add(this.buttonOpenAnalysis);
            this.panel19.Controls.Add(this.panel32);
            this.panel19.Controls.Add(this.buttonSortET);
            this.panel19.Controls.Add(this.panel2);
            this.panel19.Controls.Add(this.checkBoxHolterOnly);
            this.panel19.Controls.Add(this.labelNrHolter);
            this.panel19.Controls.Add(this.label1);
            this.panel19.Controls.Add(this.labelListTable);
            this.panel19.Controls.Add(this.label8);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel19.Location = new System.Drawing.Point(0, 611);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(1304, 30);
            this.panel19.TabIndex = 9;
            // 
            // labelHolterSize
            // 
            this.labelHolterSize.AutoSize = true;
            this.labelHolterSize.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelHolterSize.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHolterSize.Location = new System.Drawing.Point(785, 0);
            this.labelHolterSize.Margin = new System.Windows.Forms.Padding(0);
            this.labelHolterSize.Name = "labelHolterSize";
            this.labelHolterSize.Size = new System.Drawing.Size(98, 16);
            this.labelHolterSize.TabIndex = 148;
            this.labelHolterSize.Text = "^999 holter size";
            this.labelHolterSize.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button1.Dock = System.Windows.Forms.DockStyle.Left;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.button1.Location = new System.Drawing.Point(686, 0);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(99, 28);
            this.button1.TabIndex = 153;
            this.button1.Text = "Set Period";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonSetAll
            // 
            this.buttonSetAll.BackColor = System.Drawing.Color.Transparent;
            this.buttonSetAll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonSetAll.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonSetAll.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonSetAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSetAll.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.buttonSetAll.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonSetAll.Location = new System.Drawing.Point(607, 0);
            this.buttonSetAll.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonSetAll.Name = "buttonSetAll";
            this.buttonSetAll.Size = new System.Drawing.Size(79, 28);
            this.buttonSetAll.TabIndex = 152;
            this.buttonSetAll.Text = "Set All";
            this.buttonSetAll.UseVisualStyleBackColor = false;
            this.buttonSetAll.Click += new System.EventHandler(this.buttonSetAll_Click);
            // 
            // labelMemInfo
            // 
            this.labelMemInfo.AutoSize = true;
            this.labelMemInfo.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelMemInfo.Font = new System.Drawing.Font("Arial", 9F);
            this.labelMemInfo.Location = new System.Drawing.Point(1004, 0);
            this.labelMemInfo.Name = "labelMemInfo";
            this.labelMemInfo.Size = new System.Drawing.Size(64, 15);
            this.labelMemInfo.TabIndex = 113;
            this.labelMemInfo.Text = "^mem info";
            this.labelMemInfo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // buttonResetAll
            // 
            this.buttonResetAll.BackColor = System.Drawing.Color.Transparent;
            this.buttonResetAll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonResetAll.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonResetAll.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonResetAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonResetAll.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.buttonResetAll.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonResetAll.Location = new System.Drawing.Point(528, 0);
            this.buttonResetAll.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonResetAll.Name = "buttonResetAll";
            this.buttonResetAll.Size = new System.Drawing.Size(79, 28);
            this.buttonResetAll.TabIndex = 117;
            this.buttonResetAll.Text = "Reset All";
            this.buttonResetAll.UseVisualStyleBackColor = false;
            this.buttonResetAll.Click += new System.EventHandler(this.buttonResetAll_Click);
            // 
            // panel29
            // 
            this.panel29.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel29.Location = new System.Drawing.Point(524, 0);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(4, 28);
            this.panel29.TabIndex = 116;
            // 
            // buttonReloadList
            // 
            this.buttonReloadList.BackColor = System.Drawing.Color.Transparent;
            this.buttonReloadList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonReloadList.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonReloadList.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonReloadList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonReloadList.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.buttonReloadList.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonReloadList.Location = new System.Drawing.Point(419, 0);
            this.buttonReloadList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonReloadList.Name = "buttonReloadList";
            this.buttonReloadList.Size = new System.Drawing.Size(105, 28);
            this.buttonReloadList.TabIndex = 115;
            this.buttonReloadList.Text = "Reload List";
            this.buttonReloadList.UseVisualStyleBackColor = false;
            this.buttonReloadList.Click += new System.EventHandler(this.buttonReloadList_Click);
            // 
            // panel28
            // 
            this.panel28.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel28.Location = new System.Drawing.Point(415, 0);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(4, 28);
            this.panel28.TabIndex = 114;
            // 
            // buttonOpenAnalysis
            // 
            this.buttonOpenAnalysis.BackColor = System.Drawing.Color.Transparent;
            this.buttonOpenAnalysis.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonOpenAnalysis.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonOpenAnalysis.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonOpenAnalysis.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOpenAnalysis.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.buttonOpenAnalysis.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonOpenAnalysis.Location = new System.Drawing.Point(1068, 0);
            this.buttonOpenAnalysis.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonOpenAnalysis.Name = "buttonOpenAnalysis";
            this.buttonOpenAnalysis.Size = new System.Drawing.Size(77, 28);
            this.buttonOpenAnalysis.TabIndex = 110;
            this.buttonOpenAnalysis.Text = "View";
            this.buttonOpenAnalysis.UseVisualStyleBackColor = false;
            this.buttonOpenAnalysis.Click += new System.EventHandler(this.buttonOpenAnalysis_Click);
            // 
            // panel32
            // 
            this.panel32.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel32.Location = new System.Drawing.Point(1145, 0);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(4, 28);
            this.panel32.TabIndex = 112;
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(1298, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(4, 28);
            this.panel2.TabIndex = 111;
            // 
            // checkBoxHolterOnly
            // 
            this.checkBoxHolterOnly.AutoSize = true;
            this.checkBoxHolterOnly.Checked = true;
            this.checkBoxHolterOnly.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxHolterOnly.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxHolterOnly.Location = new System.Drawing.Point(284, 0);
            this.checkBoxHolterOnly.Name = "checkBoxHolterOnly";
            this.checkBoxHolterOnly.Size = new System.Drawing.Size(131, 28);
            this.checkBoxHolterOnly.TabIndex = 149;
            this.checkBoxHolterOnly.Text = "Holter Files Only";
            this.checkBoxHolterOnly.UseVisualStyleBackColor = true;
            this.checkBoxHolterOnly.CheckedChanged += new System.EventHandler(this.checkBoxHolterOnly_CheckedChanged);
            // 
            // labelNrHolter
            // 
            this.labelNrHolter.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelNrHolter.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelNrHolter.Location = new System.Drawing.Point(201, 0);
            this.labelNrHolter.Name = "labelNrHolter";
            this.labelNrHolter.Size = new System.Drawing.Size(83, 28);
            this.labelNrHolter.TabIndex = 150;
            this.labelNrHolter.Text = "^123";
            this.labelNrHolter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Font = new System.Drawing.Font("Arial", 10F);
            this.label1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label1.Location = new System.Drawing.Point(140, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 28);
            this.label1.TabIndex = 151;
            this.label1.Text = "Holters:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelListTable
            // 
            this.labelListTable.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelListTable.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelListTable.Location = new System.Drawing.Point(59, 0);
            this.labelListTable.Name = "labelListTable";
            this.labelListTable.Size = new System.Drawing.Size(81, 28);
            this.labelListTable.TabIndex = 109;
            this.labelListTable.Text = "^105/ 9999";
            this.labelListTable.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.Dock = System.Windows.Forms.DockStyle.Left;
            this.label8.Font = new System.Drawing.Font("Arial", 10F);
            this.label8.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label8.Location = new System.Drawing.Point(0, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 28);
            this.label8.TabIndex = 108;
            this.label8.Text = "Files:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel10
            // 
            this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel10.Location = new System.Drawing.Point(0, 50);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(1304, 613);
            this.panel10.TabIndex = 11;
            // 
            // panel26
            // 
            this.panel26.Controls.Add(this.panel1);
            this.panel26.Controls.Add(this.panel30);
            this.panel26.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel26.Location = new System.Drawing.Point(0, 778);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(1304, 37);
            this.panel26.TabIndex = 14;
            this.panel26.Paint += new System.Windows.Forms.PaintEventHandler(this.panel26_Paint);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonViewResult);
            this.panel1.Controls.Add(this.buttonHolterResult);
            this.panel1.Controls.Add(this.labelExecResults);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1304, 26);
            this.panel1.TabIndex = 15;
            // 
            // buttonViewResult
            // 
            this.buttonViewResult.BackColor = System.Drawing.Color.Transparent;
            this.buttonViewResult.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonViewResult.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonViewResult.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonViewResult.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonViewResult.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.buttonViewResult.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonViewResult.Location = new System.Drawing.Point(1127, 0);
            this.buttonViewResult.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonViewResult.Name = "buttonViewResult";
            this.buttonViewResult.Size = new System.Drawing.Size(61, 26);
            this.buttonViewResult.TabIndex = 113;
            this.buttonViewResult.Text = "View";
            this.buttonViewResult.UseVisualStyleBackColor = false;
            this.buttonViewResult.Click += new System.EventHandler(this.buttonViewResult_Click);
            // 
            // buttonHolterResult
            // 
            this.buttonHolterResult.BackColor = System.Drawing.Color.Transparent;
            this.buttonHolterResult.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonHolterResult.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonHolterResult.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonHolterResult.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonHolterResult.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.buttonHolterResult.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonHolterResult.Location = new System.Drawing.Point(1188, 0);
            this.buttonHolterResult.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonHolterResult.Name = "buttonHolterResult";
            this.buttonHolterResult.Size = new System.Drawing.Size(116, 26);
            this.buttonHolterResult.TabIndex = 112;
            this.buttonHolterResult.Text = "View in Holter";
            this.buttonHolterResult.UseVisualStyleBackColor = false;
            this.buttonHolterResult.Click += new System.EventHandler(this.buttonHolterResult_Click);
            // 
            // labelExecResults
            // 
            this.labelExecResults.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelExecResults.Font = new System.Drawing.Font("Arial", 10F);
            this.labelExecResults.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelExecResults.Location = new System.Drawing.Point(0, 0);
            this.labelExecResults.Name = "labelExecResults";
            this.labelExecResults.Size = new System.Drawing.Size(1304, 26);
            this.labelExecResults.TabIndex = 110;
            this.labelExecResults.Text = "^merge stats: start hjkhgjks, End hghdfgkhj, duration , gap, overlap";
            this.labelExecResults.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel30
            // 
            this.panel30.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel30.Location = new System.Drawing.Point(0, 27);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(1304, 10);
            this.panel30.TabIndex = 14;
            // 
            // panel40
            // 
            this.panel40.Controls.Add(this.labelFileStats);
            this.panel40.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel40.Location = new System.Drawing.Point(0, 641);
            this.panel40.Name = "panel40";
            this.panel40.Size = new System.Drawing.Size(1304, 22);
            this.panel40.TabIndex = 15;
            // 
            // labelFileStats
            // 
            this.labelFileStats.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelFileStats.Font = new System.Drawing.Font("Arial", 10F);
            this.labelFileStats.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelFileStats.Location = new System.Drawing.Point(0, 0);
            this.labelFileStats.Name = "labelFileStats";
            this.labelFileStats.Size = new System.Drawing.Size(1304, 22);
            this.labelFileStats.TabIndex = 109;
            this.labelFileStats.Text = "^File stats: start hjkhgjks, End hghdfgkhj, duration , gap, overlap";
            this.labelFileStats.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.labelRecordRange);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(0, 50);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(1304, 19);
            this.panel8.TabIndex = 16;
            // 
            // labelRecordRange
            // 
            this.labelRecordRange.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelRecordRange.Location = new System.Drawing.Point(0, 0);
            this.labelRecordRange.Name = "labelRecordRange";
            this.labelRecordRange.Size = new System.Drawing.Size(1304, 19);
            this.labelRecordRange.TabIndex = 0;
            this.labelRecordRange.Text = "?Records  range";
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.labelStudyRange);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 69);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(1304, 19);
            this.panel9.TabIndex = 17;
            // 
            // labelStudyRange
            // 
            this.labelStudyRange.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelStudyRange.Location = new System.Drawing.Point(0, 0);
            this.labelStudyRange.Name = "labelStudyRange";
            this.labelStudyRange.Size = new System.Drawing.Size(1304, 19);
            this.labelStudyRange.TabIndex = 0;
            this.labelStudyRange.Text = "?Study range";
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog";
            this.openFileDialog.Filter = "Sirona *.d|*.dat|All Files|*.*";
            // 
            // CHolterFilesForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(1304, 861);
            this.Controls.Add(this.panelRows);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel19);
            this.Controls.Add(this.panel40);
            this.Controls.Add(this.panel10);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panelStudy);
            this.Controls.Add(this.panel26);
            this.Controls.Add(this.panelStudyButtons);
            this.Font = new System.Drawing.Font("Arial", 10F);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "CHolterFilesForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Holter Files";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CHolterFilesForm_FormClosed);
            this.panel6.ResumeLayout(false);
            this.panel27.ResumeLayout(false);
            this.panel25.ResumeLayout(false);
            this.panel24.ResumeLayout(false);
            this.panel23.ResumeLayout(false);
            this.panel21.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel33.ResumeLayout(false);
            this.panelStudyButtons.ResumeLayout(false);
            this.panelStudyButtons.PerformLayout();
            this.panelStudyPeriod.ResumeLayout(false);
            this.panel22.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panelSetTime.ResumeLayout(false);
            this.panel58.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.panel64.ResumeLayout(false);
            this.panel62.ResumeLayout(false);
            this.panel62.PerformLayout();
            this.panel57.ResumeLayout(false);
            this.panel57.PerformLayout();
            this.panelPeriod.ResumeLayout(false);
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            this.panel55.ResumeLayout(false);
            this.panel55.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel54.ResumeLayout(false);
            this.panel54.PerformLayout();
            this.panelStudy.ResumeLayout(false);
            this.panelStudy.PerformLayout();
            this.panel16.ResumeLayout(false);
            this.panelRows.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            this.panel26.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel40.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panelStudyButtons;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelPreviousFindingsList;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelGender;
        private System.Windows.Forms.Label labelAge;
        private System.Windows.Forms.Label labelDoB;
        private System.Windows.Forms.Label labelFullName;
        private System.Windows.Forms.Label labelStudyIndex;
        private System.Windows.Forms.Panel panelStudyPeriod;
        private System.Windows.Forms.Panel panel37;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Panel panelStudy;
        private System.Windows.Forms.Panel panelRows;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Label labelReportResult;
        private System.Windows.Forms.Button buttonViewPdf;
        private System.Windows.Forms.Label labelPdfNr;
        private System.Windows.Forms.Panel panelPeriod;
        private System.Windows.Forms.CheckBox checkBoxAnonymize;
        private System.Windows.Forms.Button buttonOpenFile2;
        private System.Windows.Forms.Button buttonPrintPreviousFindings;
        private System.Windows.Forms.Button buttonMCT;
        private System.Windows.Forms.Panel panelSetTime;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel58;
        private System.Windows.Forms.Panel panel64;
        private System.Windows.Forms.Button buttonP24;
        private System.Windows.Forms.Button buttonP6;
        private System.Windows.Forms.Button buttonP1;
        private System.Windows.Forms.Button button0h00;
        private System.Windows.Forms.Button buttonM1;
        private System.Windows.Forms.Button buttonM6;
        private System.Windows.Forms.Button buttonM24;
        private System.Windows.Forms.Panel panel62;
        private System.Windows.Forms.ComboBox comboBoxToAM;
        private System.Windows.Forms.TextBox textBoxToMin;
        private System.Windows.Forms.TextBox textBoxToHour;
        private System.Windows.Forms.DateTimePicker dateTimePickerTo;
        private System.Windows.Forms.Label labelTo;
        private System.Windows.Forms.Panel panel57;
        private System.Windows.Forms.ComboBox comboBoxFromAM;
        private System.Windows.Forms.TextBox textBoxFromMin;
        private System.Windows.Forms.TextBox textBoxFromHour;
        private System.Windows.Forms.DateTimePicker dateTimePickerFrom;
        private System.Windows.Forms.Label labelFrom;
        private System.Windows.Forms.Button buttonEndStudy;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Button buttonSortET;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Label labelListTable;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button buttonOpenAnalysis;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Panel panel40;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.Label labelNrXXX;
        private System.Windows.Forms.Label labelHolterType;
        private System.Windows.Forms.Label labelMemInfo;
        private System.Windows.Forms.Panel panel54;
        private System.Windows.Forms.RadioButton radioButtonHour;
        private System.Windows.Forms.RadioButton radioButton12Hour;
        private System.Windows.Forms.RadioButton radioButton6Hour;
        private System.Windows.Forms.RadioButton radioButtonMonth;
        private System.Windows.Forms.RadioButton radioButtonWeek;
        private System.Windows.Forms.RadioButton radioButtonDay;
        private System.Windows.Forms.RadioButton radioButtonPeriod;
        private System.Windows.Forms.RadioButton radioButtonAll;
        private System.Windows.Forms.CheckBox checkBoxShowResult;
        private System.Windows.Forms.Button buttonReloadList;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.Button buttonResetAll;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.Label labelHolterSize;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.Label labelRecordInfo;
        private System.Windows.Forms.CheckBox checkBoxHolterOnly;
        private System.Windows.Forms.Label labelRecordStats;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn FileName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnEventDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnAnalysisDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnFindings;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnRemarks;
        private System.Windows.Forms.Label labelNrHolter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelFileStats;
        private System.Windows.Forms.Button buttonHolterFiles;
        private System.Windows.Forms.Button buttonMegeFiles;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.CheckBox checkBoxCopyFirstJson;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonHolterResult;
        private System.Windows.Forms.Label labelExecResults;
        private System.Windows.Forms.Button buttonX00;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label labelRecordRange;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label labelStudyRange;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.CheckBox checkBoxCh10;
        private System.Windows.Forms.CheckBox checkBoxCh9;
        private System.Windows.Forms.CheckBox checkBoxCh8;
        private System.Windows.Forms.CheckBox checkBoxCh7;
        private System.Windows.Forms.CheckBox checkBoxCh6;
        private System.Windows.Forms.CheckBox checkBoxCh5;
        private System.Windows.Forms.CheckBox checkBoxCh4;
        private System.Windows.Forms.CheckBox checkBoxCh3;
        private System.Windows.Forms.CheckBox checkBoxCh2;
        private System.Windows.Forms.CheckBox checkBoxCh1;
        private System.Windows.Forms.CheckBox checkBoxChAll;
        private System.Windows.Forms.Button buttonViewResult;
        private System.Windows.Forms.Button buttonSetAll;
        private System.Windows.Forms.CheckBox checkBoxaddDtFile;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Label labelReqDur;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Label labelAllowedChannels;
        private System.Windows.Forms.ComboBox comboBoxChannelFormat;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.CheckBox checkBoxInv10;
        private System.Windows.Forms.CheckBox checkBoxInv9;
        private System.Windows.Forms.CheckBox checkBoxInv8;
        private System.Windows.Forms.CheckBox checkBoxInv7;
        private System.Windows.Forms.CheckBox checkBoxInv6;
        private System.Windows.Forms.CheckBox checkBoxInv5;
        private System.Windows.Forms.CheckBox checkBoxInv4;
        private System.Windows.Forms.CheckBox checkBoxInv3;
        private System.Windows.Forms.CheckBox checkBoxInv2;
        private System.Windows.Forms.CheckBox checkBoxInv1;
        private System.Windows.Forms.Panel panel55;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.Label labelStudyNote;
    }
}