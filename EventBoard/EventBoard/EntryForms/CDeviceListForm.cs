﻿using Program_Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventBoardEntryForms
{
    public partial class CDeviceListForm : Form
    {
        public CDeviceListForm()
        {
            InitializeComponent();
            Text = CProgram.sMakeProgTitle("Device List", false, true);

        }
    }
}
