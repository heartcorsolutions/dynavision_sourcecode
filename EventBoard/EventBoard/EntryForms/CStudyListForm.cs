﻿using EventboardEntryForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Program_Base;
using EventBoard.EntryForms;
using Event_Base;
using EventBoard;
using EventBoard.Program_Base;

namespace EventBoardEntryForms
{
    public partial class CStudyListForm : Form
    {
        //        FormEventBoard mEventBoardForm;

        private List<CSqlDataTableRow> _mList;
        public List<CSqlDataTableRow> _mClientList;
        public List<CSqlDataTableRow> _mDeviceList;
        public List<CSqlDataTableRow> _mPhysicianList;
        public List<CSqlDataTableRow> _mTrialList;

        private CStudyInfo _mInfo;
        public CPatientInfo _mPatientInfo;
        public CDeviceInfo _mDeviceInfo;
        public CClientInfo _mClientInfo;
        public CPhysicianInfo _mPhysician;
        public CTrialInfo _mTrialInfo;
        public UInt32 _mSelectedIndex = 0;
        public bool _mbSelected = false;
        static public Int32 _sStudyActiveRecDays = 1;
        public Color _mPatientBackColor;

//        public UInt32 _mSearchTrial = 0;
        public CBitSet64 _mSearchStudyPermissions = null;

        public static UInt32 _sStudyRowsLimit = 0;

        public bool _mbCheckEndsInDays = false;
        public Int32 _mEndInDaysFrom = 0;
        public Int32 _mEndInDaysTo = 0;
        public DateTime _mCompareDT = DateTime.MinValue;

        List<CSqlDataTableRow> _mModelList = null;
        CDeviceModel _mDeviceModel = null;

        public enum DTestDevState
        {
            NoTest = 0,             // same order as select box
            Recording,
            Assigned,
            InTransit,
            Returned,
            All,
            NrEnums         // must be last
        }
        public DTestDevState _mCheckDeviceState = DTestDevState.NoTest;

        public CStudyListForm(List<CSqlDataTableRow> AList, UInt32 AIndex, bool AbSearchAtStart,
            bool AbEnableSelect, string ATitle, string AInfo, 
            UInt32 APreSelectDevice, bool AbPreselectActiveRecording, UInt32 APreselectPatient)
        {
            try
            {
                bool bProgrammer = CLicKeyDev.sbDeviceIsProgrammer();

                _mInfo = new CStudyInfo(CDvtmsData.sGetDBaseConnection());
                _mPatientInfo = new CPatientInfo(CDvtmsData.sGetDBaseConnection());
                _mDeviceInfo = new CDeviceInfo(CDvtmsData.sGetDBaseConnection());
                _mClientInfo = new CClientInfo(CDvtmsData.sGetDBaseConnection());
                _mPhysician = new CPhysicianInfo(CDvtmsData.sGetDBaseConnection());
                _mTrialInfo = new CTrialInfo(CDvtmsData.sGetDBaseConnection());

                _mDeviceList = new List<CSqlDataTableRow>();
                _mClientList = new List<CSqlDataTableRow>();
                _mPhysicianList = new List<CSqlDataTableRow>();
                _mTrialList = new List<CSqlDataTableRow>();
                _mSearchStudyPermissions = new CBitSet64();

                if (_mInfo == null || _mPatientInfo == null || _mDeviceInfo == null || _mDeviceList == null || _mClientInfo == null || _mClientList == null
                    || _mPhysician == null || _mPhysicianList == null || _mTrialInfo == null || _mTrialList == null)
                {
                    Close();
                }
                else
                {
                    if (AList != null)
                    {
                        _mList = AList;
                        _mSelectedIndex = AIndex;
                    }
                    else
                    {
                        bool bError;
                        UInt64 loadMask = _mInfo.mMaskValid;

                        //                        _mInfo.mbDoSqlSelectList(out bError, out _mList, loadMask, 0, true, "", DSqlSort.Desending, (UInt16)DSqlDataTableColum.Index_KEY);   // just load all

                        _mClientInfo.mbDoSqlSelectList(out bError, out _mClientList, CSqlDataTableRow.sGetMask((UInt16)DClientInfoVars.Label), 0, true, "",
                            DSqlSort.Ascending, (UInt16)DClientInfoVars.Label);

                        UInt64 loadDeviceMask = _mDeviceInfo.mMaskAll;// CSqlDataTableRow.sGetMask((UInt16)DDeviceInfoVars.DeviceSerialNr);
                        _mDeviceInfo.mbDoSqlSelectList(out bError, out _mDeviceList, loadDeviceMask, 0, true, "",
                            DSqlSort.Ascending, (UInt16)DDeviceInfoVars.DeviceSerialNr);

                        _mPhysician.mbDoSqlSelectList(out bError, out _mPhysicianList, CSqlDataTableRow.sGetMask((UInt16)DPhysicianInfoVars.Label),0, true, "");
                        _mTrialInfo.mbDoSqlSelectList(out bError, out _mTrialList, CSqlDataTableRow.sGetMask((UInt16)DTrialInfoVars.Label), 0, true, "",
                            DSqlSort.Ascending, (UInt16)DTrialInfoVars.Label);
                    }
                    InitializeComponent();

                    Width = 1400;   // Nicer size then minimum 1240

                    _mInfo._mClient_IX = FormEventBoard._sSelectedClient;

                    if (AIndex == 0 && _mList != null && _mList.Count > 0)
                    {
                        _mSelectedIndex = _mList[0 /*_mList.Count - 1*/].mIndex_KEY;  // set on last in list
                    }
                    if (_mClientList != null)
                    {
                        _mClientInfo.mbListFillComboBox(comboBoxClient, _mClientList, (UInt16)DClientInfoVars.Label, _mInfo._mClient_IX, "-- Select --");
                    }
                    _mInfo._mTrial_IX = /*_mSearchTrial = */FormEventBoard._sSelectedTrial;
//                    checkBoxTrial.Checked = 
                    if (_mTrialList != null)
                    {
                        _mTrialInfo.mbListFillComboBox(comboBoxTrial, _mTrialList, (UInt16)DClientInfoVars.Label, _mInfo._mTrial_IX, "-- Select --");
                    }
                    if ( _mDeviceList != null)
                    {
                        _mDeviceInfo.mbListFillComboBox(comboBoxRecorder, _mDeviceList, (UInt16)DDeviceInfoVars.DeviceSerialNr, APreSelectDevice, "-- Select --");
                    }
                    if(_mPhysicianList != null)
                    {
                        _mPhysician.mbListFillComboBox(comboBoxPhysician, _mPhysicianList, (UInt16)DPhysicianInfoVars.Label, _mInfo._mPhysisian_IX, "-- Select --");
                    }
                    buttonSelect.Visible = AbEnableSelect;
                    dateTimePickerStart.Value = dateTimePickerStart.MinDate;
                    dateTimePickerEnd.Value = dateTimePickerEnd.MaxDate;

                    mCountDevices();
                    mSetValues();

                    string title = "Study List";

                    if( AbEnableSelect)
                    {
                        title = "Select Study";
                        if( ATitle != null && ATitle.Length > 0 )
                        {
                            title += " for " + ATitle;
                        }
                        labelStudySearch.Text = "Select Study";
                    }
                    Text = CProgram.sMakeProgTitle(title, false, true);
                    labelSelected.Visible = CLicKeyDev.sbDeviceIsProgrammer();
                    labelError.Visible = CLicKeyDev.sbDeviceIsProgrammer();
                    labelError.Text = "";
                    labelUpdTime.Text = "";

                    textBoxActiveRecording.Text = _sStudyActiveRecDays.ToString();

                    UInt32 dbLimit = CDvtmsData.sGetDBaseConnection().mGetSqlRowLimit();

                    if (_sStudyRowsLimit == 0)
                    {
                        _sStudyRowsLimit = dbLimit;
                        checkBoxUseLimit.Checked = false;
                    }
                    else
                    {
                        checkBoxUseLimit.Checked = true;
                    }
                    labelRowLimit.Text = _sStudyRowsLimit.ToString();

                    if (FormEventBoard._sStudyPermissionsActive != null)
                    {
                        FormEventBoard._sStudyPermissionsActive.mCopyTo(ref _mSearchStudyPermissions);
                    }
                    if( FormEventBoard._sbStudyPermissionsLocked )
                    {
                        checkBoxStudyPermissions.Enabled = false;
                    }
                    //                    checkBoxStudyPermissions.Text = "Study " + FormEventBoard._sStudyPermissionsText + "=";
                    checkBoxStudyPermissions.Text = CDvtmsData._sStudyPermSiteText + "=";
                    ColumnPermissions.HeaderText = CDvtmsData._sStudyPermSiteText;
                    labelStudyPermissions.Visible = bProgrammer;
                    int nList = CDvtmsData.sFillStudyPermissionsComboBox(comboBoxLocation, "---", _mInfo._mStudyPermissions, CDvtmsData._sStudyPermSiteGroup);

                    if ( FormEventBoard._sbSelectTrialLocked)
                    {
                        checkBoxTrial.Enabled = false;
                    }
                    mFillStudyPermissions();
                    //buttonSelect.Visible = AbEnableSelect;

                    labelInfo.Text = AInfo;

                    string patientName = "";
                    if (APreselectPatient > 0)
                    {
                        textBoxPatient.Text = APreselectPatient.ToString();
                        if (_mPatientInfo.mbDoSqlSelectIndex(APreselectPatient, _mPatientInfo.mMaskValid))
                        {
                            patientName = _mPatientInfo.mGetFullName();
                        }
                    }
                    checkBoxActiveRecording.Checked = AbPreselectActiveRecording;
                    if (FormEventBoard._sbForceAnonymize)
                    {
                        checkBoxAnonymize.Checked = true;
                        checkBoxAnonymize.Enabled = false;
                        patientName = "";
                    }

                    labelPatientName.Text = patientName;

                    _mPatientBackColor = labelPatInfo.BackColor;

                    if (AbSearchAtStart)
                    {
                        mSearch();
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("CStudyListForm() failed to init", ex);
            }
        }
        private void mFillStudyPermissions()
        {
            _mSearchStudyPermissions.mOrSet(FormEventBoard._sStudyPermissionsFixed);
            string text = CDvtmsData.sGetStudyPermissionsBitLabels(_mSearchStudyPermissions, "+");

            labelStudyPermissions.Text = text;

            CDvtmsData.sbSetStudyPermissionsComboBox(comboBoxLocation, _mInfo._mStudyPermissions, CDvtmsData._sStudyPermSiteGroup);


        }

        private void mSearch()
        {
            string updTime = ".";
            try
            {
                DateTime nowDT = DateTime.Now;
                string whereStr = "";
                DateTime startDT = dateTimePickerStart.Value;
                DateTime endDT = dateTimePickerEnd.Value;
                bool bStart = startDT > dateTimePickerStart.MinDate;
                bool bEnd = endDT < dateTimePickerEnd.MaxDate;
                bool bActive = false == checkBoxInactive.Checked;
                string endInDaysText = "";
                int compareHour = 0;

                _mbCheckEndsInDays = checkBoxEndsInDays.Checked;

                labelError.Text = "";

                Int32.TryParse(textBoxCmpOffset.Text, out compareHour);

                _mCompareDT = new DateTime(nowDT.Year, nowDT.Month, nowDT.Day, compareHour, 0, 0);
                endInDaysText = ", cmp " + CProgram.sDateTimeToString(_mCompareDT) + CProgram.sTimeZoneOffsetStringLong(CProgram.sGetLocalTimeZoneOffsetMin());

                if (_mbCheckEndsInDays)
                {
                    Int32 endInDaysFrom = 0;
                    UInt32 endInDaysN = 0;

                    if(textBoxEndsNrDays.Text == "")
                    {
                        endInDaysN = 1;
                    }
                    _mbCheckEndsInDays = Int32.TryParse(textBoxEndsInFrom.Text, out endInDaysFrom)
                        && (endInDaysN == 1 || UInt32.TryParse(textBoxEndsNrDays.Text, out endInDaysN));

                    if (_mbCheckEndsInDays)
                    {
                        if (endInDaysN == 0) endInDaysN = 1;
                       _mEndInDaysFrom = endInDaysFrom;    // filter after quiry in mSetValues;
                       _mEndInDaysTo = endInDaysFrom + (Int32)endInDaysN - 1;
                        
                        DateTime endFrom = _mCompareDT.AddDays(_mEndInDaysFrom);
                        int nrDays = _mEndInDaysTo - _mEndInDaysFrom + 1;
                        endInDaysText = ", Ends " + CProgram.sDateTimeToString(endFrom) 
                            + CProgram.sTimeZoneOffsetStringLong(CProgram.sGetLocalTimeZoneOffsetMin()) + " " + nrDays + "days";
                    }
                    else
                    {
                        checkBoxEndsInDays.Checked = false;
                    }
                }
                int devIndex = comboBoxDeviceState.SelectedIndex;
                _mCheckDeviceState = devIndex < 0 || devIndex >= (int)DTestDevState.NrEnums ? DTestDevState.NoTest : (DTestDevState)devIndex;

                //                UInt32 trialSearchIX = _mSearchTrial > 0 && checkBoxTrial.Checked ? _mSearchTrial : 0;

                UInt32 dbLimit = CDvtmsData.sGetDBaseConnection().mGetSqlRowLimit();
                UInt32 nLimit = checkBoxUseLimit.Checked ? _sStudyRowsLimit : dbLimit;

                _mInfo.mClear();
                if (UInt32.TryParse(textBoxStudy.Text, out _mInfo.mIndex_KEY) && _mInfo.mIndex_KEY > 0)
                {
                    _mInfo.mSqlWhereAndSearchEqual(ref whereStr, (UInt16)DSqlDataTableColum.Index_KEY);
                }
                if (_mDeviceInfo != null)
                {
                    if (_mDeviceInfo.mbListParseComboBox(comboBoxRecorder, _mDeviceList, (UInt16)DDeviceInfoVars.DeviceSerialNr, ref _mInfo._mStudyRecorder_IX))
                    {
                        _mInfo.mSqlWhereAndSearchEqual(ref whereStr, (UInt16)DStudyInfoVars.StudyRecorder_IX, true);
                    }
                }

                if (bStart || bEnd)
                {
                    _mInfo.mSqlWhereAndCmpDT(ref whereStr, (UInt16)DStudyInfoVars.StudyStartDate, startDT, endDT);
                }
                if (_mClientInfo != null)
                {
                    if (_mClientInfo.mbListParseComboBox(comboBoxClient, _mClientList, (UInt16)DClientInfoVars.Label, ref _mInfo._mClient_IX))
                    {
                        _mInfo.mSqlWhereAndSearchEqual(ref whereStr, (UInt16)DStudyInfoVars.Client_IX, true);
                    }
                }
                if (_mTrialInfo != null && checkBoxTrial.Checked)
                {
                    if (_mTrialInfo.mbListParseComboBox(comboBoxTrial, _mTrialList, (UInt16)DTrialInfoVars.Label, ref _mInfo._mTrial_IX))
                    {
                    }
                    _mInfo.mSqlWhereAndSearchEqual(ref whereStr, (UInt16)DStudyInfoVars.Trial_IX, false);   // allow trial == 0 search
                }
                if (_mPhysicianList != null)
                {
                    if(_mPhysician.mbListParseComboBox(comboBoxPhysician, _mPhysicianList, (UInt16)DPhysicianInfoVars.Label, ref _mInfo._mPhysisian_IX))
                    {
                        _mInfo._mRefPhysisian_IX = _mInfo._mPhysisian_IX;

                        string test1 = "";
                        string test2 = "";

                        _mInfo.mSqlWhereAndSearchEqual(ref test1, (UInt16)(UInt16)DStudyInfoVars.Physisian_IX);
                        _mInfo.mSqlWhereAndSearchEqual(ref test2, (UInt16)(UInt16)DStudyInfoVars.RefPhysisian_IX);

                        _mInfo.mSqlWhereOR(ref test1, test2);
                        test2 = _mInfo.mSqlWhereCompound(test1);

                        _mInfo.mSqlWhereAND(ref whereStr, test2);
                    }
                }
                 if (UInt32.TryParse(textBoxPatient.Text, out _mInfo._mPatient_IX) && _mInfo._mPatient_IX > 0)
                {
                    _mInfo.mSqlWhereAndSearchEqual(ref whereStr, (UInt16)(UInt16)DStudyInfoVars.Patient_IX);
                }

/*                if (trialSearchIX > 0)
                {
                    string varName = _mInfo.mVarGetName((UInt16)DStudyInfoVars.Trial_IX);
                    string s = varName + "=" + trialSearchIX.ToString();
                    _mInfo.mSqlWhereAND(ref whereStr, s);
                }
*/                if (checkBoxStudyPermissions.Checked && _mSearchStudyPermissions != null)
                {
                    _mInfo._mStudyPermissions = CDvtmsData.sGetStudyPermissionsComboBox(comboBoxLocation, CDvtmsData._sStudyPermSiteGroup);

//                    _mSearchStudyPermissions.mCopyTo(ref _mInfo._mStudyPermissions );
                    _mInfo.mSqlWhereAndCmpVar(ref whereStr, DSqlDataCmd.WhereEqual, (UInt16)DStudyInfoVars.StudyPermissions, false);
                }

                if (checkBoxActiveRecording.Checked)
                {
                    Int32.TryParse(textBoxActiveRecording.Text, out _sStudyActiveRecDays);

                    _mInfo._mRecorderEndUTC = DateTime.UtcNow.AddDays(-_sStudyActiveRecDays);

                    _mInfo.mSqlWhereAndCmpVar(ref whereStr, DSqlDataCmd.WhereHigherEqual, (UInt16)DStudyInfoVars.RecorderEndUTC, false);
                }

                bool bSearch = whereStr != null && whereStr.Length != 0;

                _mInfo.mbActive = bActive;
                _mInfo.mSqlWhereAndSearchEqual(ref whereStr, (UInt16)DSqlDataTableColum.Active);

                bool bError;
                UInt64 loadMask = _mInfo.mMaskValid;

                if (bSearch)
                {
                    DSqlSort sort = DSqlSort.Desending;
                    UInt16 sortVar = (UInt16)DSqlDataTableColum.Index_KEY;
                    _mInfo.mbDoSqlSelectList(out bError, out _mList, loadMask, whereStr, sort, sortVar, nLimit);
                    labelError.Text = ( bError ? "search error!" : "Selective search" ) + endInDaysText;
                }
                else
                {
                    _mInfo.mbDoSqlSelectList(out bError, out _mList, loadMask, whereStr, DSqlSort.Desending, 
                        (UInt16)DSqlDataTableColum.Index_KEY, nLimit);

                    labelError.Text = ( bError ? "search all error!" : "search all" ) + endInDaysText;
                }
                mSetValues();
                int n = _mList == null ? 0 : _mList.Count;
                bool bMaxLimit = n >= nLimit;
                updTime = (bMaxLimit ? "!Max! ": "" ) + n.ToString() + " rows in " + (DateTime.Now - nowDT).TotalSeconds.ToString("0.000") + " sec";
            }
            catch (Exception ex)
            {
                CProgram.sLogException("CStudyListForm() failed to search", ex);
                updTime = "---";
                labelError.Text = "exception";
            }
            labelUpdTime.Text = updTime;
        }

        private UInt32 mCountDevices( UInt32 AStudyIX )
        {
            UInt32 n = 0;

            if (AStudyIX > 0 && _mDeviceList != null)
            {
                foreach (CDeviceInfo device in _mDeviceList)
                {
                    if (device._mActiveStudy_IX == AStudyIX)
                    {
                        ++n;
                    }
                }
            }
            return n;
        }

        private void mCountDevices()
        {
            string s = "";
            int nTotal = 0;
            int nAssigned = 0;
            int nAvailable = 0;
            int nRunning = 0;
            DateTime nowUTC = DateTime.UtcNow;

            if (_mDeviceList != null)
            {
                foreach (CDeviceInfo device in _mDeviceList)
                {
                    ++nTotal;
                    if( device._mState_IX == (UInt16)DDeviceState.Available)
                    {
                        ++nAvailable;
                    }
                    else if( device.mbIsRecording())
                    {
                        ++nAssigned;
                    }
                    if( device._mRecorderEndUTC >= nowUTC)
                    {
                        ++nRunning;
                    }
                }
            }
            s = nTotal + " devices,  " + nAssigned + "  Assigned,  " + nAvailable + " Available, " + nRunning + " not ended";
            labelDeviceCount.Text = s;
        }

        private void mSetValues()
        {
            bool bMaxLimit = false;
            UInt32 studyMaxIX = 0;
            UInt32 studyMaxNrEvents = 0;

            try
            {
                bool bHidePatient = checkBoxAnonymize.Checked;

                //            dataGridView.SuspendLayout();
                dataGridView.Rows.Clear();
                if (_mList != null)
                {
                    CStudyInfo selectedRow = null;
                    DataGridViewRow selectedGridRow = null;
                    int n = 0;
                    int iSelected = 0;
                    UInt64 patMask = _mPatientInfo != null ? _mPatientInfo.mGetValidMask(true) : 0;
                    UInt64 devMask = _mDeviceInfo != null ? _mDeviceInfo.mGetValidMask(true) : 0;

                    foreach (CStudyInfo row in _mList)
                    {
                        bool bCheckDays = row._mStudyMonitoringDays > 0;
                        DateTime endDate = row._mStudyStartDate.AddDays(row._mStudyMonitoringDays);
                        int daysLeft = (int)(endDate - _mCompareDT).TotalDays;
                        string patName = row._mPatient_IX.ToString();
                        string client = "";
                        string recSnr = "";
                        string recNote = "";
//                        string totalEvents = row._mNrRecords.ToString();
//                        string totalReports = row._mNrReports.ToString();
                        uint totalEvents = row._mNrRecords;
                        uint totalReports = row._mNrReports;
                        string pendingEvents = "-";     // must come from counting records
                        string analyzedEvents = "-";
//                        string dueDays = days < -31 ? "" : days.ToString();
                        string reportDate = "-";
                        string reportedBy = "-";
                        string recorderReturned = "-";
                        string startDate = row._mStudyStartDate.ToString("yyyy / MM / dd");// CProgram.sDateToString(row._mStudyStartDate);
                        string studyLabel = row._mStudyRemarkLabel;
                        string trialLabel = row._mTrial_IX == 0 ? "" : "T" + row._mTrial_IX.ToString();
                        string permissions = CDvtmsData.sGetStudyPermissionsBitLabels(row._mStudyPermissions, "+");

                        bool bFoundDevice = false;

                        string dueDays = "";
                        if( n == 0 )
                        {
                            int a = n;
                        }
                        if( totalEvents > studyMaxNrEvents)
                        {
                            studyMaxIX = row.mIndex_KEY;
                            studyMaxNrEvents = totalEvents;
                        }
  
                        if (bCheckDays && daysLeft >= -100)
                        {
                            dueDays = daysLeft.ToString();

                            if (dueDays.Length < 3)
                            {
                                dueDays = ("   " + dueDays).Substring(dueDays.Length);
                            }
                        }
                        // row.mbActive.ToString()

                        if (row._mPatient_IX > 0 && patMask != 0)
                        {
                            if (bHidePatient)
                            {
                                patName = "--Anonymized--" + row._mPatient_IX.ToString();
                            }
                            else if (_mPatientInfo.mbDoSqlSelectIndex(row._mPatient_IX, patMask))
                            {
                                patName = _mPatientInfo.mGetFullName();
                                //                            patName = _mPatientInfo._mPatientID.mDecrypt();
                            }
                        }
                        if (row._mStudyRecorder_IX > 0 && devMask > 0)
                        {
//                            if (_mDeviceInfo.mbDoSqlSelectIndex(row._mStudyRecorder_IX, devMask))
                            if (_mDeviceInfo.mbLoadIndexFromList( _mDeviceList, row._mStudyRecorder_IX))
                            {
                                bFoundDevice = _mDeviceInfo._mActiveStudy_IX == row.mIndex_KEY; // device info found and device assigned to this study

                                recSnr = _mDeviceInfo._mDeviceSerialNr + " " + (row._mRecorderEndUTC > DateTime.UtcNow ? "+" : "-") + _mDeviceInfo.mGetAddStudyModeChar(true);
                                recSnr += (_mDeviceInfo._mActiveStudy_IX != row.mIndex_KEY ? "≠" : (_mDeviceInfo._mRecorderEndUTC > DateTime.UtcNow ? "√" : "○"));
                                recSnr += mCountDevices(row.mIndex_KEY).ToString();
                                recNote = _mDeviceInfo._mRemarkLabel;
                            }
                            else
                            {
                                recSnr = "?" + row._mStudyRecorder_IX.ToString();
                            }
                        }
                        if (row._mClient_IX > 0)
                        {
                            if (_mClientInfo.mbLoadIndexFromList(_mClientList, row._mClient_IX))
                            {
                                client = _mClientInfo._mLabel;
                            }
                        }
                        if( row._mTrial_IX > 0)
                        {
                            if (_mTrialInfo.mbLoadIndexFromList(_mTrialList, row._mTrial_IX))
                            {
                                trialLabel = _mTrialInfo._mLabel;
                            }
                        }
                        bool bAdd = true;
                        if (_mbCheckEndsInDays)
                        {
                            bAdd = bCheckDays && daysLeft >= _mEndInDaysFrom && daysLeft <= _mEndInDaysTo;
                        }
                        if(_mCheckDeviceState != DTestDevState.NoTest)
                        {
                            bAdd = false;

                            if(bFoundDevice)
                            {
                                DDeviceState state = _mDeviceInfo._mState_IX < (UInt16)DDeviceState.NrStates ? (DDeviceState)_mDeviceInfo._mState_IX : DDeviceState.NrStates;
                                switch (_mCheckDeviceState)
                                {
                                    case DTestDevState.Recording:
                                        bAdd = state == DDeviceState.Assigned || state == DDeviceState.Intransit;
                                        break;
                                    case DTestDevState.Assigned:
                                        bAdd = state == DDeviceState.Assigned;
                                        break;
                                    case DTestDevState.InTransit:
                                        bAdd = state == DDeviceState.Intransit;
                                        break;
                                    case DTestDevState.Returned:
                                        bAdd = state == DDeviceState.Returned;
                                        break;
                                    case DTestDevState.All:
                                        bAdd = true;
                                        break;
                                }
                            }
                        }

                        if (bAdd)
                        {

                            dataGridView.Rows.Add(row.mIndex_KEY, startDate, patName, row._mStudyProcCodeSet.mGetShowSet("+"),
                                client, recSnr, recNote, totalEvents, totalReports, dueDays, analyzedEvents, reportDate, reportedBy, studyLabel,
                                trialLabel, permissions);

                            if (_mSelectedIndex > 0 && row.mIndex_KEY == _mSelectedIndex)
                            {
                                selectedRow = row;
                                DataGridViewRow gridRow = dataGridView.Rows[n];
                                iSelected = n;

                                if (gridRow != null)
                                {
                                    selectedGridRow = gridRow;
                                    //                            dataGridView.CurrentCell = gridRow.Cells[0];  // set cursor back to current
                                }
                            }
                            ++n;
                        }
                    }
                    if (selectedRow == null)
                    {
                        if (dataGridView.Rows.Count > 0)
                        {
                            iSelected = 0; // first because decending sort -> top is last dataGridView.Rows.Count - 1;
                            selectedGridRow = dataGridView.Rows[iSelected];
                        }
                    }
                    if (selectedGridRow != null)
                    {
                        dataGridView.CurrentCell = selectedGridRow.Cells[0];  // set cursor back to current

                        int i = iSelected - 4;
                        dataGridView.FirstDisplayedScrollingRowIndex = i < 0 ? 0 : i;
                    }
                    UInt32 studyNr = mGetCursorStudyNr();

                    mbLoadStudyInfo(studyNr);
                    mUpdatePatientInfo(-1.0F);
                }
                string maxStr = "";
                Color color = labelRecorderInfo.BackColor;
                if( studyMaxIX > 0 )
                {
                    maxStr = "Max Study " + studyMaxIX.ToString() + ": " + studyMaxNrEvents.ToString() + " Events";

                    CSqlDBaseConnection db = CDvtmsData.sGetDBaseConnection();

                    UInt32 dbLimit = db == null ? 1 : db.mGetSqlRowLimit();
                    if (studyMaxNrEvents >= dbLimit)
                    {
                        color = Color.Orange;
                    }

                }
                labelMaxEvents.Text = maxStr;
                if (labelMaxEvents.BackColor != color) labelMaxEvents.BackColor = color;

            }
            catch ( Exception ex)
            {
                CProgram.sLogException("StudyList setValues", ex);
            }
            //dataGridView.ResumeLayout();
        }
        private void mUpdateButtons()
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonSelect_Click(object sender, EventArgs e)
        {
            
            _mSelectedIndex = 0;
            if (dataGridView.SelectedRows != null && dataGridView.SelectedRows.Count > 0)
            {
                UInt32 index = 0;
                string s = dataGridView.SelectedRows[0].Cells[0].Value == null ? "" 
                    : dataGridView.SelectedRows[0].Cells[0].Value.ToString();

                if (UInt32.TryParse(s, out index))
                {
                    if (mbLoadStudyInfo(index))
                    {
                        _mSelectedIndex = index;
                        _mbSelected = true;
                        Close();
                    }
                }
            }
        }

        private void dataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            UInt32 studyNr = mGetCursorStudyNr();

            if( studyNr > 0 )
            {
                bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;
                bool bSelectFiles = (Control.ModifierKeys & Keys.Alt) == Keys.Alt;

                if (bCtrl) bCtrl = CProgram.sbAskYesNo("MCT Full debug", "Do full debug logging?");

                DTriageTimerMode prevMode = FormEventBoard.sSetTriageTimerEntering(DTriageTimerMode.DialogSecond, "MCT");
 
                CMCTTrendDisplay form = new CMCTTrendDisplay(studyNr, DDeviceType.Unknown, bCtrl, bSelectFiles);

                if (form != null )// && form.bStatsLoaded)
                {
                    form.Show();
                }
                FormEventBoard.sSetTriageTimerLeaving(prevMode, "MCT");
            }
        }

        private UInt32 mGetCursorStudyNr()
        {
            UInt32 studyNr = 0;

            if (dataGridView.CurrentRow != null)
            {
                if (dataGridView.CurrentRow.Cells.Count >= 4)
                {
                    string studyText = dataGridView.CurrentRow.Cells[0].Value == null ? "" 
                        : dataGridView.CurrentRow.Cells[0].Value.ToString();

                    if (studyText != null && UInt32.TryParse(studyText, out studyNr))
                    {

                    }
                }
            }
            _mSelectedIndex = studyNr;
            labelSelected.Text = _mSelectedIndex.ToString();

            return studyNr;
        }
        
        private bool mbLoadStudyInfo(UInt32 AStudyIX)
        {
            bool bOk = false;

            try
            {
                if (_mInfo != null && _mPatientInfo != null && AStudyIX > 0)
                {
                    _mInfo.mIndex_KEY = 0;

                    if (_mInfo.mbDoSqlSelectIndex(AStudyIX, _mInfo.mGetValidMask(true)))
                    {
                        bOk = _mInfo.mIndex_KEY == AStudyIX;

                        _mPatientInfo.mIndex_KEY = 0;

                        if (_mPatientInfo.mbDoSqlSelectIndex(_mInfo._mPatient_IX, _mPatientInfo.mGetValidMask(true)))
                        {

                        }
                    }
                }
            }
            catch ( Exception ex)
            {
                CProgram.sLogException("LoadStudyInfo " + AStudyIX.ToString(), ex);
            }
            return bOk;
        }

        private string mGetPhysiscian(string AName, UInt32 AIndex)
        {
            string s = "";

            if (AIndex != 0 && _mPhysicianList != null)
            {
                foreach (CPhysicianInfo node in _mPhysicianList)
                {
                    if (node.mIndex_KEY == AIndex)
                    {
                        s = " " + AName + "#" + AIndex.ToString() + "= " + node._mLabel + ", ";
                        break;
                    }
                }
            }
            return s;
        }
        private string mGetRecorder(string AName, UInt32 AIndex)
        {
            string s = "";

            if (AIndex != 0 && _mDeviceList != null)
            {
                foreach (CDeviceInfo node in _mDeviceList)
                {
                    if (node.mIndex_KEY == AIndex)
                    {
                        s = " " + AName + "#" + AIndex.ToString() + "= " + node._mDeviceSerialNr + ", ";
                        break;
                    }
                }
            }
            return s;
        }

        private void mUpdatePatientInfo(float ASec)
        {
            string line = "";
            try
            {
                bool bHidePatient = checkBoxAnonymize.Checked;

                if (_mInfo != null)
                {
                        UInt32 nTotal = 0;
                        UInt32 nToDo = 0;

                    string recInfo = _mInfo.mGetRecStatesAll(out nTotal, out nToDo, false);
                    string activeRec = ( _mInfo._mRecorderEndUTC > DateTime.UtcNow ? " recording " : " Stopped ")
                        + CProgram.sDateTimeToYMDHMS( CProgram.sDateTimeToLocal(_mInfo._mRecorderEndUTC));
              
                    line += "Study #" + _mInfo.mIndex_KEY.ToString() + ": ";

                    line += mGetRecorder("recoder", _mInfo._mStudyRecorder_IX) + (_mInfo._mStudyRecorder_IX > 0  ? activeRec : "" );
                    line += mGetPhysiscian("RefPhys", _mInfo._mRefPhysisian_IX);
                    line += mGetPhysiscian("Phys", _mInfo._mRefPhysisian_IX);
                    line += "\n" + recInfo;

                    if (false == bHidePatient && _mPatientInfo != null && _mPatientInfo.mIndex_KEY == _mInfo._mPatient_IX)
                    {
                        if (_mPatientInfo.mIndex_KEY > 0)
                        {
                            DateTime date = _mPatientInfo._mPatientDateOfBirth.mDecryptToDate();

                            line += "\nPat# " + _mPatientInfo.mIndex_KEY.ToString() + "= " + _mPatientInfo.mGetFullName();

                            if (date > DateTime.MinValue)
                            {
                                line += ", " + CProgram.sDateToString(date);
                            }
                            if (_mPatientInfo._mPatientGender_IX > 0)
                            {
                                line += ", " + CPatientInfo.sGetGenderString((DGender)_mPatientInfo._mPatientGender_IX);

                            }
                            if (_mPatientInfo._mRemarkLabel.Length > 0)
                            {
                                line += ", " + _mPatientInfo._mRemarkLabel;
                            }
                            if (_mPatientInfo._mPatientPhone1.mbNotEmpty())
                            {
                                line += ", " + _mPatientInfo._mPatientPhone1.mDecrypt();

                            }
                            if (_mPatientInfo._mPatientPhone2.mbNotEmpty())
                            {
                                line += ", " + _mPatientInfo._mPatientPhone2.mDecrypt();
                            }
                        }
                        if (ASec >= 0.0F)
                        {
                            //                    line += " in " + ASec.ToString() + " sec";
                        }
                    }

                    Color color = _mPatientBackColor;
                    CSqlDBaseConnection db = CDvtmsData.sGetDBaseConnection();

                    UInt32 dbLimit = db == null ? 1 : db.mGetSqlRowLimit();
                    if (nTotal >= dbLimit)
                    {
                        color = Color.Orange;
                    }
                    if (labelPatInfo.BackColor != color) labelPatInfo.BackColor = color;
                }
            }
            catch ( Exception ex)
            {
                CProgram.sLogException("Update patient", ex);
                line = "Error, " + line;
            }
            labelPatInfo.Text = line;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            UInt32 studyNr = mGetCursorStudyNr();

            if (mbLoadStudyInfo(studyNr))
            {
                mUpdatePatientInfo(0.0F);

                CStudyPatientForm form = new CStudyPatientForm(_mInfo, null, false, true, true, true);

                if (form != null)
                {
                    form.ShowDialog();
                }
                
            }
            else
            {
                labelPatInfo.Text = "";
            }
        }

        private void mShowCursor()
        {
            DateTime dt = DateTime.Now;

            UInt32 studyNr = mGetCursorStudyNr();

            if (studyNr > 0)
            {
                mbLoadStudyInfo(studyNr);

                mUpdatePatientInfo((DateTime.Now - dt).Seconds);
            }
            else
            {
                labelPatInfo.Text = "";
            }

        }
        private void dataGridView_SelectionChanged(object sender, EventArgs e)
        {
            mShowCursor();
        }

        private void buttonFindings_Click(object sender, EventArgs e)
        {
            UInt32 studyNr = mGetCursorStudyNr();

            if (mbLoadStudyInfo(studyNr))
            {
                CPreviousFindingsForm form = new CPreviousFindingsForm(studyNr, true, 0, 0);

                if (form != null)
                {
                    form.ShowDialog();
                }
            }
        }

        private void buttonEndRecording_Click(object sender, EventArgs e)
        {
            UInt32 studyNr = mGetCursorStudyNr();
            DateTime utc = DateTime.UtcNow;
            DateTime oldEnd;
            string result = "";
            bool bOkDevice = false;

            try
            {
                if (mbLoadStudyInfo(studyNr))
                {
                    if (_mDeviceInfo.mbDoSqlSelectIndex(_mInfo._mStudyRecorder_IX, _mDeviceInfo.mMaskValid))
                    {
                        if (_mDeviceInfo != null && _mDeviceInfo.mIndex_KEY > 0 && _mDeviceInfo._mActiveStudy_IX == _mInfo.mIndex_KEY)
                        {
                            if (_mDeviceInfo._mRecorderEndUTC > utc)
                            {
                                oldEnd = _mDeviceInfo._mRecorderEndUTC;
                                _mDeviceInfo._mRecorderEndUTC = utc;

                                if (_mDeviceInfo.mbDoSqlUpdate(CSqlDataTableRow.sGetMask((UInt16)DDeviceInfoVars.RecorderEndUTC), true))
                                {
                                    result = " Device " + _mDeviceInfo._mDeviceSerialNr + " ends recording";
                                    bOkDevice = true;
                                }
                                else
                                {
                                    result = " Device " + _mDeviceInfo._mDeviceSerialNr + " failed update";
                                    //                                labelDeviceError.Text = " Device not updated";
                                    _mDeviceInfo._mRecorderEndUTC = oldEnd;
                                }                              
//                                mSetValues();
//                                mUpdateButtons();
                            }
                        }
                    }
                    oldEnd = _mInfo._mRecorderEndUTC;

                    if (oldEnd > utc)
                    {
                        _mInfo._mRecorderEndUTC = utc;
                        if (_mInfo.mbDoSqlUpdate(CSqlDataTableRow.sGetMask((UInt16)DStudyInfoVars.RecorderEndUTC), _mInfo.mbActive))
                        {
                            if( false == bOkDevice)
                            {
                                result = " Study " + _mInfo.mIndex_KEY.ToString() + " stopped recording";
                            }
                            //                                        labelStudyError.Text = "Updated end srecording";
                            //                                        labelEndRec.Text = CProgram.sUtcToLocalString(_mStudyInfo._mRecorderEndUTC);
                        }
                        else
                        {
                            //                                        labelStudyError.Text = "Failed to Update Study";
                            _mInfo._mRecorderEndUTC = oldEnd;
                            result += " Study " + _mInfo.mIndex_KEY.ToString() + " failed update!";
                        }
                    }
                    
                }
                labelError.Text = result;
                mUpdatePatientInfo(0);
            }
            catch (Exception ex)
            {
                CProgram.sLogException("End Recording S"+studyNr.ToString(), ex);
            }
        }

        private void panel16_Paint(object sender, PaintEventArgs e)
        {
        }

        private void buttonViewPdf_Click(object sender, EventArgs e)
        {
            _mSelectedIndex = 0;
            if (dataGridView.SelectedRows != null && dataGridView.SelectedRows.Count > 0)
            {
                UInt32 index = 0;
                string s = dataGridView.CurrentRow.Cells[0].Value == null ? "" 
                    : dataGridView.SelectedRows[0].Cells[0].Value.ToString();

                if (UInt32.TryParse(s, out index))
                {
                    _mSelectedIndex = index;

                    try
                    {
                        CStudyReportListForm form = new CStudyReportListForm(index);

                        if( form != null )
                        {
                            form.ShowDialog();
                        }
                    }
                    catch ( Exception ex )
                    {
                        CProgram.sLogException("Call pdf study list", ex);
                    }
                    //Close();
                }
            }
        }

        private void dataGridView_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
//            mUpdatePatientInfo(0.0F);
        }

        private void dataGridView_CurrentCellChanged(object sender, EventArgs e)
        {
            mShowCursor();
        }

        private void label6_Click(object sender, EventArgs e)
        {
            CClientInfoForm form = new CClientInfoForm(null, 0, true);

            if (form != null)
            {
                form.ShowDialog();
                if( form._mbSelected)
                {
                    comboBoxClient.Text = form._mInfo._mLabel;
                }
            }

        }
        private void mClear()
        {
            textBoxStudy.Text = "";
            comboBoxRecorder.Text = "";
            comboBoxClient.Text = "";
            textBoxPatient.Text = "";
            labelPatientName.Text = "";
            comboBoxTrial.Text = "";
            checkBoxInactive.Checked = false;
            dateTimePickerStart.Value = dateTimePickerStart.MinDate;
            dateTimePickerEnd.Value = dateTimePickerEnd.MaxDate;
            labelError.Text = "";
            checkBoxEndsInDays.Checked = false;
            textBoxEndsInFrom.Text = "";
            textBoxEndsNrDays.Text = "";
            textBoxCmpOffset.Text = "";
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            mClear();

        }

        private void label8_Click(object sender, EventArgs e)
        {
            string title = "Search patient";

            CPatientListForm form = new CPatientListForm(title, null, 0, false,
                "", false, "", "", "", DateTime.MinValue, 
                true, false, false);

            if (form != null)
            {
                form.ShowDialog();

                if (form._mbSelected)
                {
                    CPatientInfo patientInfo = form.mGetSelected();

                    textBoxPatient.Text = form._mSelectedIndex.ToString();
                    labelPatientName.Text = patientInfo == null ? "" : patientInfo.mGetFullName();
                }
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            mSearch();
        }

        private void labelPhysiscian_Click(object sender, EventArgs e)
        {
//            string title = "Search Physician";

            CPhysiciansForm form = new CPhysiciansForm(null, true);

            if (form != null)
            {
                form.ShowDialog();

                if (form._mbSelected)
                {
                    comboBoxPhysician.Text = form._mInfo._mLabel;
                }
             }
        }

        private void checkBoxAnonymize_CheckStateChanged(object sender, EventArgs e)
        {
            dataGridView.Rows.Clear();
            mUpdatePatientInfo(-1);

            labelInfo.Visible = false == checkBoxAnonymize.Checked;
        }

        private void labelRowLimit_Click(object sender, EventArgs e)
        {
            CSqlDBaseConnection db = CDvtmsData.sGetDBaseConnection();
            UInt32 dbLimit = db == null ? _sStudyRowsLimit : db.mGetSqlRowLimit();
            UInt32 nLimit = _sStudyRowsLimit;

            if (CProgram.sbReqUInt32(" Limit nr rows", "limit", ref nLimit, "rows", 1, dbLimit))
            {
                _sStudyRowsLimit = nLimit;
                checkBoxUseLimit.Checked = true;
                labelRowLimit.Text = nLimit.ToString();
            }
        }

        private void labelStudyPermissions_Click(object sender, EventArgs e)
        {
            if (false == FormEventBoard._sbStudyPermissionsLocked)
            {

                if (CDvtmsData.sbEditStudyPermissionsBits("Study list select permisions", ref _mSearchStudyPermissions))
                {
                    checkBoxStudyPermissions.Checked = _mSearchStudyPermissions.mbIsNotEmpty();
                }

                mFillStudyPermissions();
            }

        }

        private void comboBoxTrial_Click(object sender, EventArgs e)
        {
            if (false == FormEventBoard._sbSelectTrialLocked)
            {
                bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

                if (bCtrl)
                {
                    UInt32 searchTrial = 0;
                    bool bChanged = false;

                    if (_mTrialInfo != null)
                    {
                        CTrialInfoForm trialForm = new CTrialInfoForm(null, searchTrial, true);

                        if (trialForm != null
                            && _mTrialInfo.mbListParseComboBox(comboBoxTrial, _mTrialList, (UInt16)DTrialInfoVars.Label, ref searchTrial))
                        {
                            trialForm.ShowDialog();
                            if (trialForm._mbSelected)
                            {
                                searchTrial = trialForm._mSelectedIndex;
                                bChanged = true;
                            }
                        }
                        else
                        {
                            if (CProgram.sbReqUInt32("Search Study Trial", "Trial number", ref searchTrial, "", 0, UInt32.MaxValue))
                            {
                                checkBoxTrial.Checked = searchTrial > 0;
                                bChanged = true;
                            }
                        }
                        if (bChanged )
                        { 
                            if (searchTrial == 0)
                            {
                                comboBoxTrial.Text = "----";
                            }
                            else if(_mTrialInfo.mbLoadIndexFromList(_mTrialList, searchTrial))
                            {
                                comboBoxTrial.Text = _mTrialInfo._mLabel;
                            }
                            else
                            {
                                comboBoxTrial.Text = "T" + searchTrial.ToString();
                            }
                        }
                    }
                }
            }
        }

        private void comboBoxTrial_SelectedIndexChanged(object sender, EventArgs e)
        {
            if( comboBoxTrial.Items.Count > 0)
            {
                checkBoxTrial.Checked = comboBoxTrial.SelectedIndex > 0;
            }
        }

        private void comboBoxLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxLocation.Items.Count > 0)
            {
                checkBoxStudyPermissions.Checked = comboBoxLocation.SelectedIndex > 0;
            }

        }

        private void labelInfo_DoubleClick(object sender, EventArgs e)
        {
            string info = labelInfo.Text;
            if (info != null && info.Length > 0)
            {
                ReqTextForm.sbShowText("Study List", "Info", info);
            }
        }

        private void buttonHolter_Click(object sender, EventArgs e)
        {
            UInt32 studyNr = mGetCursorStudyNr();

            if (studyNr > 0)
            {
                try
                {
                    CHolterFilesForm form = new CHolterFilesForm(studyNr, true, 0, 0);

                    if (form != null)
                    {
                        form.ShowDialog();
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("start Holter screen", ex);
                }

            }
        }

        private void textBoxStudy_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                mSearch();
            }

        }

        private void buttonCsvExport_Click(object sender, EventArgs e)
        {
            mDoCsvOutput(true);
        }

        // define csv columns
        private CCsvColumn _mCsvLineNr = null;
        private CCsvColumn _mCsvDevModel = null;
        private CCsvColumn _mCsvDevSnr = null;
        private CCsvColumn _mCsvStudyNr = null;
        private CCsvColumn _mCsvPatientID = null;
        private CCsvColumn _mCsvClientName = null;
        private CCsvColumn _mCsvStudyType = null;
        private CCsvColumn _mCsvStudyStart = null;
        private CCsvColumn _mCsvStudyEnds = null;
        private CCsvColumn _mCsvPatPhone1 = null;
        private CCsvColumn _mCsvPatPhone2 = null;
        private CCsvColumn _mCsvPatientFullName = null;

        private CCsvFileWriter mCreateCsvWriter(bool bAll)
        {
            CCsvFileWriter csvWriter = null;


            try
            {
                csvWriter = new CCsvFileWriter();

                _mCsvLineNr = csvWriter.mAddColumn("LineNr", true, "line");
                _mCsvDevModel = csvWriter.mAddColumn("DevModel", true, "Device Type");
                _mCsvDevSnr = csvWriter.mAddColumn("DevSnr", true, "Serial");
                _mCsvStudyNr = csvWriter.mAddColumn("StudyNr", true, "Study #");
                _mCsvPatientID = csvWriter.mAddColumn("PatientID", bAll, "Patient ID");
                _mCsvClientName = csvWriter.mAddColumn("ClientName", bAll, "Client");
                _mCsvStudyType = csvWriter.mAddColumn("StudyType", bAll, "Study Type");
                _mCsvStudyStart = csvWriter.mAddColumn("StudyStart", bAll, "Started");
                _mCsvStudyEnds = csvWriter.mAddColumn("StudyEnds", bAll, "Ends in days");
                _mCsvPatPhone1 = csvWriter.mAddColumn("PatPhone1", true, "Phone");
                _mCsvPatPhone2 = csvWriter.mAddColumn("PatPhone2", true, "Cell");
                _mCsvPatientFullName = csvWriter.mAddColumn("PatFullName", bAll, "Patient Name");
            }
            catch (Exception ex)
            {
                CProgram.sLogException("StudyList CSV init colums", ex);
            }

            return csvWriter;

        }

        private bool mbReadCsvConfig(CCsvFileWriter ACsvWriter, bool AbLongCsv)
        {
            bool bReadOk = false;

            if (ACsvWriter != null)
            {
                string fileName = "StudyList_" + (AbLongCsv ? "Long" : "Short") + ".csvConfig"; 

                try
                {
                    ACsvWriter.mbReadCsvConfig( fileName );

                    bReadOk = ACsvWriter.mGetNrActiveColumns() > 0;
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("StudyList CSV init colums", ex);
                }
            }
            return bReadOk;
        }

        private void mCsvAddOneRow(CCsvFileWriter ACsvWriter, int ARowIndex)
        {
            try
            {
                if (dataGridView.Rows != null)
                {
                    DataGridViewRow row = dataGridView.Rows[ARowIndex];

                    if (row != null && row.Cells != null && row.Cells.Count > 12)
                    {
                        string s;
                        UInt32 studyNr = 0;
                        Int32 endInDays = 0;

                        _mCsvLineNr.mbSetValueInt32(ARowIndex + 1);

                        s = row.Cells[0].Value.ToString();
                        if (UInt32.TryParse(s, out studyNr))
                        {
                            _mCsvStudyNr.mbSetValueUInt32(studyNr);
                        }
                        s = row.Cells[9].Value.ToString();  // depends on data grid colums!!!
                        if (Int32.TryParse(s, out endInDays))
                        {
                            _mCsvStudyEnds.mbSetValueInt32(endInDays);
                        }

                        CStudyInfo study = (CStudyInfo)_mInfo.mGetNodeFromList(_mList, studyNr);

                        if (study != null)
                        {
                            _mCsvStudyStart.mbSetValueString(CProgram.sDateToString(study._mRecorderStartUTC));

                            _mCsvStudyType.mbSetValueString(study._mStudyTypeCode);

                            CDeviceInfo device = (CDeviceInfo)_mDeviceInfo.mGetNodeFromList(_mDeviceList, study._mStudyRecorder_IX);
                            if (device != null)
                            {
                                _mCsvDevSnr.mbSetValueString(device._mDeviceSerialNr);

                                if( null != _mDeviceList && null != _mDeviceModel)
                                {
                                    CDeviceModel model = _mDeviceModel.mGetNodeFromList(_mModelList, device._mDeviceModel_IX) as CDeviceModel;
                                    if (model != null)
                                    {
                                        _mCsvDevModel.mbSetValueString(model._mModelLabel);
                                    }
                                }
                            }
                            if (_mPatientInfo.mbDoSqlSelectIndex(study._mPatient_IX, _mPatientInfo.mMaskValid))
                            {
                                _mCsvPatientID.mbSetValueString(_mPatientInfo.mGetPatientIDValue());
                                _mCsvPatientFullName.mbSetValueString(_mPatientInfo.mGetFullName());
                                _mCsvPatPhone1.mbSetValueString(_mPatientInfo._mPatientPhone1.mDecrypt());
                                _mCsvPatPhone2.mbSetValueString(_mPatientInfo._mPatientPhone2.mDecrypt());
                            }
                            if (study._mClient_IX > 0)
                            {
                                if (_mClientInfo.mbLoadIndexFromList(_mClientList, study._mClient_IX))
                                {
                                    _mCsvClientName.mbSetValueString( _mClientInfo._mLabel );
                                }
                            }
                        }

                        ACsvWriter.mbWriteRow();
                    }
                }
            }
            catch(Exception ex)
            {
                CProgram.sLogException("StudyList CSV row[" + ARowIndex + "]", ex);
            }

}

        private void mDoCsvOutput( bool AbAll)
        {
            try
            {
                CCsvFileWriter csvWriter = mCreateCsvWriter(AbAll);

                if ( null != csvWriter )
                {
                    csvWriter.mbSaveCsvConfig("csvStudyListAll.txt", true);

                    if (mbReadCsvConfig(csvWriter, AbAll))
                    {
                        if (dataGridView != null && dataGridView.Rows != null)
                        {
                            int nRows = dataGridView.Rows.Count;

                            if (nRows > 0)
                            {
                                if (null == _mDeviceModel)
                                {
                                    _mDeviceModel = new CDeviceModel(CDvtmsData.sGetDBaseConnection());

                                    if (null != _mDeviceModel)
                                    {
                                        bool bError;
                                        _mDeviceModel.mbDoSqlSelectList(out bError, out _mModelList, _mDeviceModel.mGetValidMask(true), 0, true, "");
                                    }
                                }

                                if (csvWriter.mbWriteStart())
                                {
                                    for (int i = 0; i < nRows; ++i)
                                    {
                                        mCsvAddOneRow(csvWriter, i);
                                    }
                                }
                            }

                            string lines = csvWriter.mGetTotalLines();

                            if (ReqTextForm.sbShowText("Csv Study List", "csv", lines))
                            {
                                string fileName = "studyList" + CProgram.sDateTimeToYMDHMS(DateTime.Now) + "_" + CProgram.sGetPcName()
                                    + (AbAll ? "_Long" : "_Short") + ".csv";

                                saveFileDialogCsv.Title = "Save Study List to csv";
                                saveFileDialogCsv.FileName = fileName;

                                if (DialogResult.OK == saveFileDialogCsv.ShowDialog())
                                {
                                    csvWriter.mbSaveCsvLines(saveFileDialogCsv.FileName);
                                }
                            }
                        }
                    }
                }
            }
            catch( Exception ex)
            {
                CProgram.sLogException("StudyList CSV", ex);
            }
        }

        private void buttonCsvExportShort_Click(object sender, EventArgs e)
        {
            mDoCsvOutput(false);
        }
    }
}
