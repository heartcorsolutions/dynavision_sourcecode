﻿using Event_Base;
using EventBoard.EntryForms;
using TzDevice;
using Program_Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EventBoard.Event_Base;
using SironaDevice;

namespace EventboardEntryForms
{
    public partial class CDeviceForm : Form
    {
        CDeviceInfo _mInfo = null;
        CDeviceInfo _mSearchInfo = null;
        List<CSqlDataTableRow> _mModelList = null;
        private List<CSqlDataTableRow> _mList = null;
        private List<CSqlDataTableRow> _mClientList = null;
        CDeviceModel _mDeviceModel = null;
        private CClientInfo _mClient = null;
        private bool _mbLoadListError = false;
        private bool _mbAllowStudy;

        private string _mErrorDevicesList = "";
        private UInt32 _mErrorDeviceCount = 0;

        public string _mFormSelectedDevice;
        public UInt32 _mFormSelectedStudy = 0;    // store info to update StudyPatientForm
        public DateTime _mFormSelectedEndUTC;    

        public CDeviceForm(string ASelectDevice, bool AbAllowStudy)
        {
            try
            {
                CDvtmsData.sbInitData();

                _mFormSelectedDevice = ASelectDevice;
                _mbAllowStudy = AbAllowStudy;

                CSqlDBaseConnection dbConn = Event_Base.CDvtmsData.sGetDBaseConnection();

                _mInfo = new CDeviceInfo(dbConn);
                _mDeviceModel = new CDeviceModel(dbConn);
                _mClientList = new List<CSqlDataTableRow>();
                _mClient = new CClientInfo(CDvtmsData.sGetDBaseConnection());

                if (_mInfo == null && _mDeviceModel != null || _mClient == null || _mClientList == null)
                {
                    CProgram.sLogLine(" CDeviceForm() failed to init Client data");
                    Close();
                }
                else
                {
                    bool bError;
                    //                    UInt64 mLoadMask = _mInfo.mMaskValid;
                    UInt64 loadMask = _mDeviceModel.mMaskIndexKey
                            | CSqlDataTableRow.sGetMask((UInt16)DDeviceModelVars.ModelLabel)
                            | CSqlDataTableRow.sGetMask((UInt16)DDeviceModelVars.ServiceInterval);

                    InitializeComponent();
                    CRecDeviceList.sbAllDeviceListsCheck(true, false);
                    // load model list
                    _mDeviceModel.mbDoSqlSelectList(out bError, out _mModelList, loadMask, 0, true, "");

                    _mClient.mbDoSqlSelectList(out bError, out _mClientList, loadMask, 0, true, "");

                    if (_mClient != null)
                    {
                        _mClient.mbListFillComboBox(comboBoxUseClient, _mClientList, (UInt16)DClientInfoVars.Label, 0, "-- Select --");
                    }

                    CDeviceInfo.sFillRefIDModeComboBox(comboBoxRefID, _mInfo._mRefIdMode);
                    CDeviceInfo.sFillAddStudyModeComboBox(comboBoxAddStudy, _mInfo._mAddStudyMode);
                    CDeviceInfo.sFillTimeZoneModeComboBox(comboBoxTimeZone, _mInfo._mTimeZoneMode);
                    CDeviceInfo.sFillNextStateModeComboBox(comboBoxNextState, _mInfo._mNextStateMode);

                    mLoadList(ASelectDevice);
                    mSetValues();
                    if( _mInfo.mIndex_KEY == 0 && ASelectDevice != null && ASelectDevice.Length > 0 
                        && ( textBoxSerialNr.Text == null || textBoxSerialNr.Text.Length == 0))
                    {
                        textBoxSerialNr.Text = ASelectDevice;
                    }
                    buttonShowStudy.Visible = _mbAllowStudy;

                    mUpdateButtons();
                    mbCheckForm("New");
                  
                    Text = CProgram.sMakeProgTitle("Device List", false, true);

                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException(" CDeviceIForm() failed to init", ex);
            }
        }

        public void mUpdateButtons()
        {
            if (_mInfo != null)
            {
                bool bNew = _mInfo.mIndex_KEY == 0;
                bool bValid = !bNew;

                labelIndex.Text = bNew ? "" : _mInfo.mIndex_KEY.ToString();
                buttonAddUpdate.Text = bNew ? "Add" : "Update";
                textBoxSerialNr.ReadOnly = false == bNew;
                //comboBoxModel.Enabled = bNew;

                // toDo use the NextState
                //sbAvailNextState(UInt16 ANextStateMode, UInt16 ACurrentState, DateTime ANextServiceDT, DDeviceState ANewState)   // toDo
                //          public static bool sbAvailEndStudy(UInt16 ANextStateMode, UInt16 ACurrentState)    // toDo
                // buttonEndUse.Visible
                buttonShowStudy.Visible = _mbAllowStudy && bValid && _mInfo._mActiveStudy_IX > 0;
                buttonEndUse.Visible = bValid;
                buttonClearStudy.Visible = bValid;
                buttonChangeSNR.Visible = _mInfo._mState_IX == (UInt16)DDeviceState.InService || _mInfo._mState_IX == (UInt16)DDeviceState.New;

                /*
                                buttonEndUse.Visible = bValid;
                                buttonClearStudy.Visible = bValid;
                                buttonSetReceived.Visible = bValid;
                                buttonSetAvailable.Visible = bValid;
                                buttonSetAssigned.Visible = bValid;
                                buttonSetInService.Visible = bValid;
                                buttonSetUnavailable.Visible = bValid;
                                buttonSetDefective.Visible = bValid;
                                buttonSetIntransit.Visible = bValid;
                                buttonChangeSNR.Visible = bValid;

                                buttonSetAssigned.Visible = bValid && _mInfo._mActiveStudy_IX > 0;
                */
                buttonSetReceived.Visible = bValid && CDeviceInfo.sbAvailNextState(_mInfo._mNextStateMode, _mInfo._mState_IX, _mInfo._mDateFirstInService, _mInfo._mDateNextService, DDeviceState.Returned);
                buttonSetAvailable.Visible = bValid && CDeviceInfo.sbAvailNextState(_mInfo._mNextStateMode, _mInfo._mState_IX, _mInfo._mDateFirstInService, _mInfo._mDateNextService, DDeviceState.Available);
                buttonSetAssigned.Visible = bValid && CDeviceInfo.sbAvailNextState(_mInfo._mNextStateMode, _mInfo._mState_IX, _mInfo._mDateFirstInService, _mInfo._mDateNextService, DDeviceState.Assigned);
                buttonSetInService.Visible = bValid && CDeviceInfo.sbAvailNextState(_mInfo._mNextStateMode, _mInfo._mState_IX, _mInfo._mDateFirstInService, _mInfo._mDateNextService, DDeviceState.InService);
                buttonSetUnavailable.Visible = bValid && CDeviceInfo.sbAvailNextState(_mInfo._mNextStateMode, _mInfo._mState_IX, _mInfo._mDateFirstInService, _mInfo._mDateNextService, DDeviceState.Unavailable);
                buttonSetDefective.Visible = bValid && CDeviceInfo.sbAvailNextState(_mInfo._mNextStateMode, _mInfo._mState_IX, _mInfo._mDateFirstInService, _mInfo._mDateNextService, DDeviceState.Defective);
                buttonSetIntransit.Visible = bValid && CDeviceInfo.sbAvailNextState(_mInfo._mNextStateMode, _mInfo._mState_IX, _mInfo._mDateFirstInService, _mInfo._mDateNextService, DDeviceState.Intransit);
            }
        }

        public void mSetValues()
        {
            try
            {
                if (_mInfo._mState_IX == (UInt16)DDeviceState.Unknown)
                {
                    _mInfo._mState_IX = (UInt16)DDeviceState.New;
                }
                int interval = 0;
                if (_mModelList != null && _mDeviceModel != null)
                {
                    CDeviceModel row = _mDeviceModel.mGetNodeFromList(_mModelList, _mInfo._mDeviceModel_IX) as CDeviceModel;
                    if (row != null)
                    {
                        _mDeviceModel = row;
                        interval = row._mServiceIntervalDays;
                    }

                    _mDeviceModel.mbListFillComboBox(comboBoxModel, _mModelList, (UInt16)DDeviceModelVars.ModelLabel, _mInfo._mDeviceModel_IX, "-- Select--");
                }
                textBoxSerialNr.Text = _mInfo._mDeviceSerialNr;
                textBoxDeviceFirmwareVersion.Text = _mInfo._mFirmwareVersion;
                if (_mInfo._mDateFirstInService == DateTime.MinValue)
                {
                    _mInfo._mDateFirstInService = DateTime.Now;
                    _mInfo._mDateNextService = _mInfo._mDateFirstInService.AddDays(interval);

                }
                CProgram.sSetDateTimePicker(dateTimePickerFirstInService, _mInfo._mDateFirstInService);
                CProgram.sSetDateTimePicker(dateTimePickerNextService, _mInfo._mDateNextService);

                Color nextColor = labelFirstInService.ForeColor;
                if (_mInfo._mDateNextService <= _mInfo._mDateFirstInService)
                {
                    nextColor = Color.Gray;
                }
                else if (_mInfo._mDateNextService <= DateTime.Now.AddDays(1) )
                {
                    nextColor = Color.Red;
                }
                labelNextService.ForeColor = nextColor;

                bool bActive = /*_mInfo._mRecorderStartUTC > DateTime.MinValue &&*/ _mInfo._mRecorderStartUTC < DateTime.UtcNow
                                    && _mInfo._mRecorderEndUTC > DateTime.UtcNow;

                labelStudyNr.Text = _mInfo._mActiveStudy_IX == 0 ? "" : (_mInfo._mActiveStudy_IX.ToString() + (bActive ? " active " : " previous"));

                labelTextRefID.ForeColor = _mInfo.mCheckColorAddStudyModeRefID();
                labelRefID.Text = _mInfo._mActiveRefID.mDecrypt();
                labelRefName.Text = _mInfo._mActiveRefName.mDecrypt();
                labelStartDate.Text = _mInfo._mRecorderStartUTC == DateTime.MinValue ? "" : CProgram.sDateTimeToString(CProgram.sUtcToLocal(_mInfo._mRecorderStartUTC));
                labelTextEndDate.ForeColor = _mInfo._mRecorderEndUTC != DateTime.MinValue && _mInfo._mRecorderEndUTC < DateTime.UtcNow ? ( _mInfo.mbIsRecording() ? Color.Orange : Color.Blue ) : SystemColors.WindowText;
                labelEndDate.Text = _mInfo._mRecorderEndUTC == DateTime.MinValue ? "" : (CProgram.sDateTimeToString(CProgram.sUtcToLocal(_mInfo._mRecorderEndUTC)));

               
                labelState.Text = CDeviceInfo.sGetStateString(_mInfo._mState_IX);
                textBoxRemLabel.Text = _mInfo._mRemarkLabel;
                labelDeviceChanged.Text = CDvtmsData.sGetChangedBy(_mInfo.mChangedUTC, _mInfo.mChangedBy_IX);
                labelNoMct.Text = _mInfo.mGetAddStudyModeLabel(false) + _mInfo.mGetAddStudyModeMctLabel();

                bool bQueryActive = false;


                string tzStoreName = "";
                if (_mInfo != null && _mInfo.mIndex_KEY != 0)
                {
                    string deviceID = _mInfo._mDeviceSerialNr;
                    if (deviceID != null && deviceID != "")
                    {
                        string devicePath = "";
                        bQueryActive = CTzDeviceSettings.sbGetTzAeraDeviceUnitPath(out devicePath, deviceID);

                        if (bQueryActive)
                        {
                            string storeName = CDvtmsData.sGetTzStoreSnrName(deviceID);
                            if (storeName != deviceID)
                            {
                                tzStoreName = "Tz store name = " + storeName;
                            }
                            bQueryActive = _mInfo.mbActive; // disable button when device is not active
                        }
                    }
                }
                buttonTzConfig.Visible = bQueryActive;
                labelTzStoreName.Text = tzStoreName;
                labelShowSnr.Text = _mInfo._mDeviceSerialNr;

                bool bNeedsService = _mInfo._mDateNextService > _mInfo._mDateFirstInService && DateTime.Now >= _mInfo._mDateNextService;
                string activeTxt = _mInfo._mTempActiveStateText;

                if(bNeedsService )
                {
                    CProgram.sAddText(ref activeTxt, "Needs Service", ", ");
                }

                labelShowActiveState.Text = activeTxt;

                bQueryActive = false;

                if (_mInfo != null && _mInfo.mIndex_KEY != 0)
                {
                    string deviceID = _mInfo._mDeviceSerialNr;
                    if (deviceID != null && deviceID != "")
                    {
                        string devicePath = "";
                        bQueryActive = CSironaStateSettings.sbGetSironaDevicePath(out devicePath, deviceID);

                        if (bQueryActive)
                        {
                            bQueryActive = _mInfo.mbActive; //disable when device is not active
                        }
                    }
                }
                buttonSirConfig.Visible = bQueryActive;

                bQueryActive = false;

                if (_mInfo != null && _mInfo.mIndex_KEY != 0)
                {
                    string deviceID = _mInfo._mDeviceSerialNr;
                    if (deviceID != null && deviceID != "")
                    {
                        bQueryActive = CRecDeviceList.sbDV2IsInLicenseList(deviceID, false)
                                    || CRecDeviceList.sbDVXIsInLicenseList(deviceID, false);

                        if ( CLicKeyDev.sbDeviceIsProgrammer())
                        {
                            bQueryActive = true;
                        }
                        if (bQueryActive)
                        {
//                            bQueryActive = _mInfo.mbActive; //disable when device is not active
                        }
                    }
                }
                comboBoxTimeZone.Enabled = bQueryActive;
                textBoxTzDefault.Enabled = bQueryActive;
                textBoxTzOffset.Enabled = bQueryActive;

                // added 20190225 
                if (_mClient != null)
                {
                    if (_mInfo._mClient_IX != 0
                        && _mClient.mbLoadIndexFromList(_mClientList, _mInfo._mClient_IX))
                    {
                        comboBoxUseClient.Text = _mClient._mLabel;
                    }
                    else
                    {
                        _mClient.mClear();
                        comboBoxUseClient.SelectedIndex = 0;
                    }
                }
                comboBoxAddStudy.Text = CDeviceInfo.sGetAddStudyModeLabel(_mInfo._mAddStudyMode, false);
                comboBoxRefID.Text = CDeviceInfo.sGetRefIDModeLabel(_mInfo._mRefIdMode);
                comboBoxTimeZone.Text = CDeviceInfo.sGetTimeZoneModeLabel(_mInfo._mTimeZoneMode);
                textBoxDeployRemark.Text= _mInfo._mDeployText;
                textBoxServiceNote.Text = _mInfo._mServiceText;
                comboBoxNextState.Text = CDeviceInfo.sGetNextStateModeLabel(_mInfo._mNextStateMode);
                textBoxTzDefault.Text = CProgram.sTimeZoneOffsetStringLong(_mInfo._mTimeZoneDefaultMin);
                textBoxTzOffset.Text = CProgram.sTimeZoneOffsetStringLong(_mInfo._mTimeZoneOffsetMin );
                textBoxIReaderNR.Text = _mInfo._mIReaderNr == 0 ? "" : _mInfo._mIReaderNr.ToString();

                UInt32 invChannels = _mInfo._mInvertChannelsMask;

                checkBoxCh1.Checked = (invChannels & (1 << 0)) != 0;
                checkBoxCh2.Checked = (invChannels & (1 << 1)) != 0;
                checkBoxCh3.Checked = (invChannels & (1 << 2)) != 0;
                checkBoxCh4.Checked = (invChannels & (1 << 3)) != 0;
                checkBoxCh5.Checked = (invChannels & (1 << 4)) != 0;
                checkBoxCh6.Checked = (invChannels & (1 << 5)) != 0;
                checkBoxCh7.Checked = (invChannels & (1 << 6)) != 0;
                checkBoxCh8.Checked = (invChannels & (1 << 7)) != 0;
                checkBoxCh9.Checked = (invChannels & (1 << 8)) != 0;
                checkBoxCh10.Checked = (invChannels & (1 << 9)) != 0;

            }
            catch (Exception ex)
            {
                CProgram.sLogException(" CDeviceForm() failed to set values", ex);
            }
        }

        public void mLoadList(string ASelectedDevice)
        {
            if (_mInfo != null)
            {
                _mInfo.mbDoSqlSelectList(out _mbLoadListError, out _mList, _mInfo.mGetValidMask(true), 0, false, "");

                if( ASelectedDevice != null && ASelectedDevice.Length > 0 )
                {
                    string selectedDevice = ASelectedDevice.ToUpper();
                    foreach(CDeviceInfo row in _mList)
                    {
                        if ( row._mDeviceSerialNr.ToUpper() == selectedDevice)
                        {
                            _mInfo.mbCopyFrom(row);
                        }
                    }
                }
                mUpdateList();
            }
        }
        public void mUpdateList()
        {
            try
            {
                if (_mList != null && _mInfo != null)
                {
                    int nList, nResult = 0, iCurrent = 0;
                    string allActiveList = ",";

                    _mErrorDeviceCount = 0;
                    _mErrorDevicesList = ",";
                    dataGridViewList.Rows.Clear();

                    if (_mList == null || (nList = _mList.Count()) == 0)
                    {
                        labelListN.Text = "0" + (_mbLoadListError ? "!" : "");
                        labelResultN.Text = "-";
                    }
                    else
                    {
                        bool bAll = checkBoxShowDeactivated.Checked;
                        bool bSearch = checkBoxShowSearchResult.Checked && _mSearchInfo != null;
                        bool bAdd;
                        bool bDouble, bNoLic;
                        string activeStateText;
                        DateTime cmpServiceDT = DateTime.Now.AddDays(1);

                        foreach (CDeviceInfo row in _mList)
                        {
                            activeStateText = "Not listed";
                            if (bAll || row.mbActive)
                            {
                                activeStateText = "";
                                bDouble = false;
                                bNoLic = false;
                                
                                if (row.mbActive)
                                {
                                    string searchName = row._mDeviceSerialNr.ToUpper();
                                    string altName = CDvtmsData.sGetTzAltSnrName(searchName);
                                    bool bAltName = altName != null && altName.Length > 0;

                                    bool bError = false;

                                    if (DDeviceType.Unknown == CRecDeviceList.sFindInLicenseLists(row._mDeviceSerialNr, false))
                                    {
                                        bNoLic = true;
                                        bError = true;
                                        if (activeStateText.Length > 0) activeStateText += "+";
                                        activeStateText = "Not Licensed";
                                    }
                                    if ( allActiveList.Contains( ","+ searchName + ",") || bAltName && allActiveList.Contains("," + altName + ","))
                                    {
                                        bDouble = true;
                                        bError = true;
                                        if (activeStateText.Length > 0) activeStateText += "+";
                                        activeStateText = "Double";
                                    }
                                    if( bError )
                                    {
                                        _mErrorDevicesList += row._mDeviceSerialNr + ",";
                                        ++_mErrorDeviceCount;
                                    }
                                    allActiveList += searchName + ",";
                                    if( bAltName)
                                    {
                                        allActiveList += altName + ",";
                                    } 
                                    // test for recorder device license key
                                }
                                else
                                {
                                    activeStateText = "Not Active";
                                }

                                bAdd = true;
                                if (bSearch)
                                {
                                    if (bAdd && _mSearchInfo.mIndex_KEY != 0) bAdd = row.mIndex_KEY == _mSearchInfo.mIndex_KEY;
                                    if (bAdd) bAdd = CProgram.sbCmpSearchString(row._mDeviceSerialNr, _mSearchInfo._mDeviceSerialNr);
                                    if (bAdd && _mSearchInfo._mDeviceModel_IX != 0) bAdd = row._mDeviceModel_IX == _mSearchInfo._mDeviceModel_IX;
                                    if (bAdd) bAdd = CProgram.sbCmpSearchString(row._mFirmwareVersion, _mInfo._mFirmwareVersion);
//                                    if (bAdd && row._mDateFirstInService != DateTime.MinValue) bAdd = row._mDateFirstInService == _mInfo._mDateFirstInService;
//                                    if (bAdd && row._mDateNextService != DateTime.MinValue) bAdd = row._mDateNextService == _mInfo._mDateNextService;
                                    //if (bAdd && _mSearchInfo._mActiveStudy_IX != 0) bAdd = row._mActiveStudy_IX == _mSearchInfo._mActiveStudy_IX;
                                    if (bAdd && _mSearchInfo._mClient_IX != 0) bAdd = row._mClient_IX == _mSearchInfo._mClient_IX;

//                                    if (bAdd && row._mActiveRefName.mbNotEmpty()) bAdd = CProgram.sbCmpSearchString(row._mActiveRefName.mDecrypt(), _mInfo._mActiveRefName.mDecrypt());
                                    if (bAdd) bAdd = CProgram.sbCmpSearchString(row._mRemarkLabel, _mSearchInfo._mRemarkLabel);
                                    if (bAdd && _mSearchInfo._mIReaderNr != 0) bAdd = row._mIReaderNr == _mSearchInfo._mIReaderNr;
                                    if (bAdd) bAdd = CProgram.sbCmpSearchString(row._mDeployText, _mSearchInfo._mDeployText);
                                    if (bAdd) bAdd = CProgram.sbCmpSearchString(row._mServiceText, _mSearchInfo._mServiceText);
                                    if (bAdd && _mSearchInfo._mNextStateMode != 0) bAdd = row._mNextStateMode == _mSearchInfo._mNextStateMode;
                                }
                                if (bAdd)
                                {
                                    string modelStr = "";

                                    if( row.mIndex_KEY == _mInfo.mIndex_KEY)
                                    {
                                        iCurrent = nResult;
                                    }

                                    if (_mModelList != null && _mDeviceModel != null)
                                    {
                                        CDeviceModel model = _mDeviceModel.mGetNodeFromList(_mModelList, row._mDeviceModel_IX) as CDeviceModel;
                                        if (model != null)
                                        {
                                            modelStr = model._mModelLabel;
                                        }
                                    }
                                    bool bActive = row._mRecorderStartUTC > DateTime.MinValue && row._mRecorderStartUTC < DateTime.UtcNow
                                                            && row._mRecorderEndUTC > DateTime.UtcNow;
                                    string studyNr = row._mActiveStudy_IX == 0 ? "" : (row._mActiveStudy_IX.ToString() + (bActive ? " A" : ""));
                                    string state = CDeviceInfo.sGetStateString(row._mState_IX);
                                    string startDate = row._mRecorderStartUTC == DateTime.MinValue ? "" : CProgram.sDateTimeToString(CProgram.sUtcToLocal(row._mRecorderStartUTC));
                                    string endDate = row._mRecorderEndUTC == DateTime.MinValue ? "" : (CProgram.sDateTimeToString(CProgram.sUtcToLocal(row._mRecorderEndUTC)));
                                    string firstInService = row._mDateFirstInService == DateTime.MinValue ? "" : (CProgram.sDateToString(row._mDateFirstInService));
                                    string nextService = row._mDateNextService == DateTime.MinValue || row._mDateNextService <= row._mDateFirstInService ? "" 
                                        : ((CProgram.sDateToString(row._mDateNextService)) + (nextService = row._mDateNextService <= cmpServiceDT ? "!": ""));

                                    string active = row.mbActive.ToString();

                                    string clientName = ""; 
                                    string addStudy = CDeviceInfo.sGetAddStudyModeLabel( row._mAddStudyMode, false );
                                    string nextState = row._mNextStateMode == 0 ? "" : CDeviceInfo.sGetNextStateModeLabel(row._mNextStateMode);
                                    string ir = row._mIReaderNr == 0 ? "" : row._mIReaderNr.ToString();
                                    int endDays = (int)((row._mRecorderEndUTC - DateTime.Now).TotalDays);
                                    string ends = row._mRecorderEndUTC == DateTime.MinValue ? "" : endDays.ToString();
                                    string refID = row._mActiveRefID.mDecrypt();
                                    string defRefID = CDeviceInfo.sGetRefIDModeLabel( row._mRefIdMode );

                                    if( false == row.mbCheckOkAddStudyModeRefID())
                                    {
                                        refID = "!_" + refID;
                                    }

                                    if (row._mClient_IX != 0)
                                    {
                                        if (_mClient.mbLoadIndexFromList(_mClientList, row._mClient_IX))
                                        {
                                            clientName = _mClient._mLabel;
                                        }
                                        else
                                        {
                                            clientName = "?C" + row._mClient_IX.ToString();
                                        }
                                    }

                                        if (bDouble) active = "!" + active;
                                    if (bNoLic) active = "-" + active;
                                    dataGridViewList.Rows.Add(row.mIndex_KEY, active, modelStr, row._mDeviceSerialNr, state, clientName, row._mRemarkLabel, studyNr, 
                                        refID,  addStudy, defRefID, ir, ends, nextState, /*row._mActiveRefName.mDecrypt(),*/ startDate, endDate, row._mFirmwareVersion, firstInService, nextService);
                                    ++nResult;
                                }
                            }
                            if( nResult > 0)
                            {
                                dataGridViewList.CurrentCell = dataGridViewList.Rows[iCurrent].Cells[0];
                            }
                            row._mTempActiveStateText = activeStateText;
                        }
                        labelListN.Text = nList.ToString() + (_mbLoadListError ? "!" : "");
                        labelResultN.Text = nResult.ToString();
                    }
                }
                if(_mErrorDeviceCount == 0)
                {
                    labelErrorDevices.Text = "" + CRecDeviceList. sGetAllDeviceCount();
                    labelErrorInstruct.Visible = false;
                }
                else
                {
                    string s = _mErrorDevicesList.Substring(1, _mErrorDevicesList.Length - 2);
                    if (s.Length > 32 )
                    {
                        s = s.Substring(0, 32) + "...";
                    }
                    labelErrorDevices.Text = " E" + _mErrorDeviceCount.ToString() + ": " +s
                        + " " + CRecDeviceList.sGetAllDeviceCount();
                    labelErrorInstruct.Visible = true;
                }

            }
            catch (Exception ex)
            {
                CProgram.sLogException(" CDeviceForm() failed to update list", ex);
            }

        }

        private UInt32 mReadIndex(string AString)
        {
            UInt32 index = 0;

            if (AString != null && AString.Length > 0)
            {
                if (false == UInt32.TryParse(AString, out index))
                {
                    index = 0;
                }
            }
            return index;
        }

        public void mGetValues(out UInt32 ArIndex)
        {

            ArIndex = mReadIndex(labelIndex.Text);

            if (_mModelList != null && _mDeviceModel != null)
            {
                _mInfo._mDeviceModel_IX = _mDeviceModel.mGetIndexKeyFromList(_mModelList, (UInt16)DDeviceModelVars.ModelLabel, comboBoxModel.Text);
            }

            _mInfo._mDeviceSerialNr = textBoxSerialNr.Text;
            _mInfo._mFirmwareVersion = textBoxDeviceFirmwareVersion.Text;
            CProgram.sbGetDateTimePicker(dateTimePickerFirstInService, ref _mInfo._mDateFirstInService);
            CProgram.sbGetDateTimePicker(dateTimePickerNextService, ref _mInfo._mDateNextService);

            _mInfo._mRemarkLabel = textBoxRemLabel.Text;

            // added 20190225 
            if (_mClient != null)
            {
                _mInfo._mClient_IX = _mClient.mGetIndexKeyFromList(_mClientList, (UInt16)DClientInfoVars.Label, 
                    comboBoxUseClient.Text );
            }
            _mInfo._mAddStudyMode = CDeviceInfo.sReadAddStudyModeComboBox(comboBoxAddStudy, _mInfo._mAddStudyMode);
            _mInfo._mRefIdMode = CDeviceInfo.sReadRefIDModeComboBox(comboBoxRefID, _mInfo._mRefIdMode);
            _mInfo._mTimeZoneMode = CDeviceInfo.sReadTimeZoneModeComboBox(comboBoxTimeZone, _mInfo._mTimeZoneMode);
            _mInfo._mDeployText = textBoxDeployRemark.Text;
            _mInfo._mServiceText = textBoxServiceNote.Text;
            _mInfo._mNextStateMode = CDeviceInfo.sReadNextStateModeComboBox(comboBoxNextState, _mInfo._mNextStateMode);
            CProgram.sbParseTimeZoneOffsetString(textBoxTzDefault.Text, ref _mInfo._mTimeZoneDefaultMin);
            CProgram.sbParseTimeZoneOffsetString(textBoxTzOffset.Text, ref _mInfo._mTimeZoneOffsetMin);
            if( false == Int16.TryParse(textBoxIReaderNR.Text, out _mInfo._mIReaderNr))
            {
                _mInfo._mIReaderNr = 0;
            }
            else
            {
                if (_mInfo._mIReaderNr < 0) _mInfo._mIReaderNr = 0;
                if (_mInfo._mIReaderNr > 7) _mInfo._mIReaderNr = 7;
            }

            UInt32 invChannels = 0;

            if (checkBoxCh1.Checked) invChannels |= (1 << 0);
            if (checkBoxCh2.Checked) invChannels |= (1 << 1);
            if (checkBoxCh3.Checked) invChannels |= (1 << 2);
            if (checkBoxCh4.Checked) invChannels |= (1 << 3);
            if (checkBoxCh5.Checked) invChannels |= (1 << 4);
            if (checkBoxCh6.Checked) invChannels |= (1 << 5);
            if (checkBoxCh7.Checked) invChannels |= (1 << 6);
            if (checkBoxCh8.Checked) invChannels |= (1 << 7);
            if (checkBoxCh9.Checked) invChannels |= (1 << 8);
            if (checkBoxCh10.Checked) invChannels |= (1 << 9);

            _mInfo._mInvertChannelsMask = invChannels;

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void comboBoxManufacturerList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            if (_mInfo != null)
            {
                _mInfo.mClear();
/*                _mInfo.mIndex_KEY = 0;
                _mInfo._mDeviceSerialNr = "";
                _mInfo._mActiveStudy_IX = 0;
                _mInfo._mActiveRefID.mClear();
                _mInfo._mActiveRefName.mClear();
                _mInfo._mRemarkLabel = "";
*/                _mInfo._mRecorderStartUTC = DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Utc);
                _mInfo._mRecorderEndUTC = _mInfo._mRecorderStartUTC;      // (used for slow lookup of study number
                checkBoxShowSearchResult.Checked = false;

                _mInfo.mbActive = true;
                if (_mInfo._mState_IX != (UInt16)DDeviceState.Defective && _mInfo._mState_IX != (UInt16)DDeviceState.Unavailable)
                {
                    _mInfo._mState_IX = (UInt16)DDeviceState.Available;
                }
                labelError.Text = "cleared, use search to find";
                mSetValues();
                mUpdateButtons();
//                mUpdateList();
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonAddUpdate_Click(object sender, EventArgs e)
        {
            UInt32 index;
            mGetValues( out index);

            // check values
            if (false == mbCheckForm(_mInfo.mIndex_KEY == 0 ? "Add" : "Update"))
            {
                return;
            }

            if (_mInfo.mIndex_KEY == 0)
            {
                // new Client

                // check if serialNr is not in list

                CSqlDataTableRow row = _mInfo.mGetNodeFromList(_mList, (UInt16)DDeviceInfoVars.DeviceSerialNr, _mInfo._mDeviceSerialNr);

                _mInfo._mState_IX = (UInt16)DDeviceState.Available;

                if (row != null)
                {
                    labelError.Text = "Serial Number already exists";
                }
                else if (CProgram.sbAskOkCancel("Add Device " + _mInfo._mDeviceSerialNr + ", use i-Reader!",
                        "Use i-Reader to add device, continue to add?"))
                {
                    if (_mInfo.mbDoSqlInsert())
                    {
                        labelIndex.Text = _mInfo.mIndex_KEY.ToString();
                        labelError.Text = "Inserted";
                    }
                    else
                    {
                        labelError.Text = "Failed Insert";
                    }
                }
            }
            else if (_mInfo.mIndex_KEY > 0)
            {
                string newRefID;
                if (_mInfo.mbChecktAddStudyModeRefID(out newRefID, true))
                {
                    // update client
                    UInt64 modelMask = CSqlDataTableRow.sGetMask((UInt16)DDeviceInfoVars.DeviceModel_IX);

                    // check if label is in list (not this)
                    if (_mInfo.mbDoSqlUpdate(_mInfo.mMaskValid, _mInfo.mbActive, modelMask))
                    {
                        labelIndex.Text = "+" + _mInfo.mIndex_KEY.ToString();
                        labelError.Text = "Updated";
                    }
                    else
                    {
                        labelError.Text = "Failed Update";
                    }
                }
                else
                {
                    labelError.Text = "Canceled RefID check";
                }
            }
            mLoadList(_mInfo._mDeviceSerialNr);
            mSetValues();
            mUpdateButtons();
        }
        private void mChangeRefID()
        {
            UInt32 index;
            mGetValues(out index);

            // check values
            if (false == mbCheckForm(_mInfo.mIndex_KEY == 0 ? "Add" : "Update"))
            {
                return;
            }

            if (_mInfo.mIndex_KEY > 0)
            {
                string s = _mInfo._mActiveRefID.mDecrypt();
                if (CProgram.sbReqLabel("Force Device " + _mInfo._mDeviceSerialNr, "RefID", ref s, "", false))
                {
                    UInt64 updateMask = CSqlDataTableRow.sGetMask((UInt16)DDeviceInfoVars.ActiveRefID);

                    _mInfo._mActiveRefID.mbEncrypt(s);
                    // check if label is in list (not this)
                    if (_mInfo.mbDoSqlUpdate(updateMask, _mInfo.mbActive))
                    {
                        labelIndex.Text = "+" + _mInfo.mIndex_KEY.ToString();
                        labelError.Text = "RefID Updated";
                    }
                    else
                    {
                        labelError.Text = "Failed Update";
                    }
                }
                mLoadList(_mInfo._mDeviceSerialNr);
                mSetValues();
                mUpdateButtons();
            }
        }
        bool mbCheckDevice(out string AErrorText)
        {
            string errorText = "init form failed";

            if (_mInfo != null)
            {
                errorText = "";

                if (_mInfo._mDeviceSerialNr == null || _mInfo._mDeviceSerialNr.Length == 0)
                {
                    errorText = " No Serial Nr";
                }
                if (_mInfo._mDeviceModel_IX ==  0)
                {
                    errorText += " No Model";
                }
                if (_mInfo._mAddStudyMode != (UInt16)DAddStudyModeEnum.ActiveStudy)
                {
                    if (_mInfo._mState_IX == (UInt16)DDeviceState.Assigned)
                    {
                        errorText = "Assigned state not Allowed";
                    }
                }

            }
            AErrorText = errorText;

            return errorText.Length == 0;
        }

        private bool mbCheckForm(string AState)
        {
            bool bOk = true;
            string errorText = "";

            if(_mInfo._mState_IX == (UInt16)DDeviceState.Unknown)
            {
                _mInfo._mState_IX = (UInt16)DDeviceState.Available;
            }
            if(_mInfo._mAddStudyMode != (UInt16)DAddStudyModeEnum.ActiveStudy)
            {
                if( _mInfo._mRecorderEndUTC > _mInfo._mRecorderStartUTC)
                {
                    _mInfo._mRecorderEndUTC = DateTime.UtcNow;
                    _mInfo._mRecorderStartUTC = _mInfo._mRecorderEndUTC.AddSeconds(-1);
                }
                _mInfo._mActiveRefID.mbEncrypt(_mInfo.mGetAddStudyModeRefID());     // force refID if not addToStudy
            }
            if (mbCheckDevice(out errorText))
            {
                labelError.Text = "";
            }
            else
            {
                labelError.Text = AState + ": " + errorText;
                bOk = false;
            }
            return bOk;
        }

        private void buttonDuplicate_Click(object sender, EventArgs e)
        {
            mbCopySelected(true);
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            UInt32 index;

            mGetValues(out index);

            if (_mInfo != null)
            {
                _mSearchInfo = _mInfo.mCreateCopy() as CDeviceInfo;
                checkBoxShowSearchResult.Checked = _mSearchInfo != null;
                if (_mSearchInfo != null) _mSearchInfo.mIndex_KEY = index;
            }
            mUpdateList();
            mUpdateButtons();
            mbCheckForm("List");

        }

        private void buttonDup_Click(object sender, EventArgs e)
        {
            if (_mInfo != null)
            {
                _mInfo.mIndex_KEY = 0;
                _mInfo.mbActive = true;
                _mInfo._mActiveStudy_IX = 0;
                _mInfo._mState_IX = (UInt16)DDeviceState.Unknown;
                _mInfo._mRecorderStartUTC = DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Utc);
                _mInfo._mRecorderEndUTC = _mInfo._mRecorderStartUTC;      // (used for slow lookup of study number

                mSetValues();
                mUpdateButtons();
                mbCheckForm("Duplicated");
            }

        }
        public bool mbCopySelected(bool AbNewEntry)
        {
            bool bCopied = false;

            if (dataGridViewList.Rows != null && dataGridViewList.Rows.Count > 0)
            {
                if (dataGridViewList.SelectedRows != null && dataGridViewList.SelectedRows.Count > 0 && dataGridViewList.SelectedRows[0] != null)
                {
                    string indexStr = dataGridViewList.SelectedRows[0].Cells[0].Value == null ? ""
                        : dataGridViewList.SelectedRows[0].Cells[0].Value.ToString();
                    int index;

                    if (int.TryParse(indexStr, out index) && index > 0)
                    {
                        foreach (CDeviceInfo row in _mList)
                        {
                            if (index == row.mIndex_KEY)
                            {
                                // found selected
                                CDeviceInfo copy = row.mCreateCopy() as CDeviceInfo;

                                if (copy != null)
                                {
                                    _mInfo = copy;
                                    bCopied = true;
                                    if (AbNewEntry)
                                    {
                                        _mInfo.mIndex_KEY = 0;
                                    }
                                    mSetValues();
                                    mUpdateButtons();
                                    mbCheckForm("Copy");
                                }
                                break;
                            }
                        }
                    }
                }
            }
            return bCopied;
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            mbCopySelected(false);
        }

        private void dataGridViewList_DoubleClick(object sender, EventArgs e)
        {
            mbCopySelected(false);
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            mLoadList(null);
        }

        private void buttonDisable_Click(object sender, EventArgs e)
        {
            if (mbCopySelected(false))
            {
                UInt32 index;
                mGetValues(out index);

                // check values

                if (_mInfo.mIndex_KEY == 0)
                {
                    labelError.Text = "Not stored";
                }
                else if (index > 0 && _mInfo.mIndex_KEY == index)
                {
                    // update client
                    bool b = _mInfo.mbActive;
                    if( b && _mInfo._mState_IX != (UInt16)DDeviceState.Defective)
                    {
                        _mInfo._mState_IX = (UInt16)DDeviceState.Unavailable;
                    }
                    _mInfo.mbActive = !b;
                    // check if label is in list (not this)
                    if (_mInfo.mbDoSqlUpdate(_mInfo.mMaskValid, _mInfo.mbActive))
                    {
                        labelIndex.Text = "+" + _mInfo.mIndex_KEY.ToString();
                        labelError.Text = _mInfo.mIndex_KEY.ToString() + ( b ? " Disabled" : " Enabled" );
                    }
                    else
                    {
                        labelError.Text = _mInfo.mIndex_KEY.ToString() + (b ? " Failed to Disable" : " Failed to Enable");
                    }
                }
            }
            mSetValues();
            mLoadList(null);
            mUpdateButtons();

        }

        private void dataGridViewList_Click(object sender, EventArgs e)
        {
            mUpdateButtons();
        }

        private void comboBoxUseClient_SelectionChangeCommitted(object sender, EventArgs e)
        {
            // 20190328 client_IX is now active
/* disable            string s = comboBoxUseClient.Text;
            if (s != null && s.Length > 0 && s[0] != '-')
            {
                textBoxRemLabel.Text = s;
            }
*/
        }

        private void buttonEndUse_Click(object sender, EventArgs e)
        {
            if(_mInfo.mIndex_KEY > 0  && _mInfo._mActiveStudy_IX > 0)
            {
                if (_mInfo._mRecorderEndUTC > DateTime.UtcNow)
                {
                    _mInfo._mRecorderEndUTC = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc); 
                }
                mUpdateDevice("End Use", true);
            }
        }

        private void mUpdateDevice( string AAction, bool AbUpdateStudy )
        {
            try
            {
                UInt32 index;
                mGetValues(out index);

                if (_mInfo.mIndex_KEY > 0 && mbCheckForm(AAction))
                {
                    // update client

                    // check if label is in list (not this)
                    if (_mInfo.mbDoSqlUpdate(_mInfo.mMaskValid, _mInfo.mbActive))
                    {
                        labelIndex.Text = "+" + _mInfo.mIndex_KEY.ToString();
                        labelError.Text = AAction + ": Updated";

                        if (AbUpdateStudy && _mInfo._mActiveStudy_IX > 0)
                        {
                            CStudyInfo study = new CStudyInfo(CDvtmsData.sGetDBaseConnection());

                            if (study != null)
                            {
                                UInt64 loadMask = CSqlDataTableRow.sGetMask((UInt16)DSqlDataTableColum.Index_KEY)
                                               | CSqlDataTableRow.sGetMask((UInt16)DStudyInfoVars.StudyRecorder_IX)
                                               | CSqlDataTableRow.sGetMask((UInt16)DStudyInfoVars.RecorderStartUTC)
                                               | CSqlDataTableRow.sGetMask((UInt16)DStudyInfoVars.RecorderEndUTC);
                                if( study.mbDoSqlSelectIndex(_mInfo._mActiveStudy_IX, loadMask))
                                {
                                    if(_mInfo._mRecorderEndUTC == DateTime.MinValue )
                                    {
                                        _mInfo._mRecorderEndUTC = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc);
                                    }
                                        if ( _mInfo._mRecorderEndUTC < study._mRecorderEndUTC )
                                    {
                                        study._mRecorderEndUTC = _mInfo._mRecorderEndUTC;

                                        if( study.mbDoSqlUpdate(CSqlDataTableRow.sGetMask((UInt16)DStudyInfoVars.RecorderEndUTC), true))
                                        {
                                            labelError.Text = labelError.Text + ", Study updated";

                                            if (_mInfo._mDeviceSerialNr == _mFormSelectedDevice)
                                            {
                                                _mFormSelectedStudy = _mInfo._mActiveStudy_IX;  // store info to update StudyPatientForm
                                                _mFormSelectedEndUTC = study._mRecorderEndUTC;
                                            }
                                        }
                                        else
                                        {
                                            labelError.Text = labelError.Text + ", Study updated";
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        labelError.Text = AAction + ": Failed Update";
                    }
                }
                mLoadList(null);
                mSetValues();
                mUpdateButtons();

            }
            catch (Exception ex)
            {
                CProgram.sLogException(" CDeviceForm() failed to update device/study: " + AAction, ex);
            }

          }

        private void buttonClearStudy_Click(object sender, EventArgs e)
        {
            if (_mInfo.mIndex_KEY > 0)
            {
                bool bDo = true;
                if (_mInfo.mbActiveTime(DateTime.UtcNow))
                {
                    bDo = CProgram.sbAskOkCancel("Device " + _mInfo._mDeviceSerialNr + " Active ", "Device still active recording for Study " + _mInfo._mActiveStudy_IX.ToString() + ", Continue?");
                }
                if (bDo)
                {


                    _mInfo._mRecorderEndUTC = DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Utc);
                    _mInfo._mRecorderStartUTC = _mInfo._mRecorderEndUTC;
                    _mInfo._mActiveStudy_IX = 0;
                    _mInfo._mActiveRefID.mClear();
                    _mInfo._mActiveRefName.mClear();
                    mUpdateDevice("Clear", true);
                }
            }

        }

        private void buttonSetReceived_Click(object sender, EventArgs e)
        {
            if (_mInfo.mIndex_KEY > 0)
            {
                _mInfo._mState_IX = (UInt16)DDeviceState.Returned;
                if (_mInfo._mRecorderEndUTC > DateTime.UtcNow)
                {
                    _mInfo._mRecorderEndUTC = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc);
                }
                mUpdateDevice("Received", true);
                // update study
            }
        }

        private void buttonSetAvailable_Click(object sender, EventArgs e)
        {
            if (_mInfo.mIndex_KEY > 0 )
            {
                if( _mInfo.mbActiveTime( DateTime.UtcNow))
                {
                    CProgram.sAskOk("Device " + _mInfo._mDeviceSerialNr + " Active ", "Device still active recording for Study " + _mInfo._mActiveStudy_IX.ToString() + "!");
                }
                else if( _mErrorDevicesList.Contains(","+ _mInfo._mDeviceSerialNr + ","))
                {
                    CProgram.sAskOk("Device " + _mInfo._mDeviceSerialNr + " Active ", "Fix device error first !");
                }
                else
                {
                    _mInfo._mState_IX = (UInt16)DDeviceState.Available;

                    if (_mInfo._mRecorderEndUTC > DateTime.UtcNow)
                    {
                        _mInfo._mRecorderEndUTC = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc);
                    }
                    mUpdateDevice("Available", true);
                }
            }
        }

        private void buttonSetInService_Click(object sender, EventArgs e)
        {
            if (_mInfo.mIndex_KEY > 0 )
            {
                if (_mInfo.mbActiveTime(DateTime.UtcNow))
                {
                    CProgram.sAskOk("Device " + _mInfo._mDeviceSerialNr + " Active ", "Device still active recording for Study " + _mInfo._mActiveStudy_IX.ToString() + "!");
                }
                else
                {
                    _mInfo._mState_IX = (UInt16)DDeviceState.InService;
                    if (_mInfo._mRecorderEndUTC > DateTime.UtcNow)
                    {
                        _mInfo._mRecorderEndUTC = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc);
                    }
                    mUpdateDevice("InService", true);
                    // update study 
                }
            }

        }

        private void buttonSetUnavailable_Click(object sender, EventArgs e)
        {
            if (_mInfo.mIndex_KEY > 0)
            {
                if (_mInfo.mbActiveTime(DateTime.UtcNow))
                {
                    CProgram.sAskOk("Device " + _mInfo._mDeviceSerialNr + " Active ", "Device still active recording for Study " + _mInfo._mActiveStudy_IX.ToString() + "!");
                }
                else
                {
                    _mInfo._mState_IX = (UInt16)DDeviceState.Unavailable;
                    if (_mInfo._mRecorderEndUTC > DateTime.UtcNow)
                    {
                        _mInfo._mRecorderEndUTC = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc);
                    }
                    mUpdateDevice("Unavailable", true);
                    // update study 
                }
            }

        }

        private void buttonSetAssigned_Click(object sender, EventArgs e)
        {
            if (_mInfo.mIndex_KEY > 0 )
            {
                if (_mInfo.mbActiveTime(DateTime.UtcNow))
                {
                    CProgram.sAskOk("Device " + _mInfo._mDeviceSerialNr + " Active ", "Device still active recording for Study " + _mInfo._mActiveStudy_IX.ToString() + "!");
                }
                else if (_mInfo._mActiveStudy_IX > 0)
                {
                    if (_mInfo._mAddStudyMode != (UInt16)DAddStudyModeEnum.ActiveStudy)
                    {
                        CProgram.sAskOk("Device " + _mInfo._mDeviceSerialNr + " in " + _mInfo.mGetAddStudyModeLabel(false) + _mInfo.mGetAddStudyModeMctLabel(),
                            "Re-assign only alowed for device in Active Study!");
                    }

                    else if (CProgram.sbAskOkCancel("Device " + _mInfo._mDeviceSerialNr + " in " + _mInfo.mGetAddStudyModeLabel(false) + _mInfo.mGetAddStudyModeMctLabel(),
                        "Reactivate recording for Study " + _mInfo._mActiveStudy_IX.ToString() + "?"))
                    {
                        _mInfo._mState_IX = (UInt16)DDeviceState.Assigned;
                        _mInfo._mRecorderEndUTC = DateTime.SpecifyKind(DateTime.UtcNow.AddYears(10), DateTimeKind.Utc);    // sql does not exept the max value

                        //                _mInfo._mRecorderEndUTC = DateTime.SpecifyKind(DateTime.MaxValue, DateTimeKind.Utc);
                        mUpdateDevice("Re-assign", true);
                        // update study 
                    }
                }
            }
        }

        private void buttonSetDefective_Click(object sender, EventArgs e)
        {
            if (_mInfo.mIndex_KEY > 0)
            {
                if (_mInfo.mbActiveTime(DateTime.UtcNow))
                {
                    CProgram.sAskOk("Device " + _mInfo._mDeviceSerialNr + " Active ", "Device still active recording for Study " + _mInfo._mActiveStudy_IX.ToString() + "!");
                }
                else
                {
                    _mInfo._mState_IX = (UInt16)DDeviceState.Defective;
                    if (_mInfo._mRecorderEndUTC > DateTime.UtcNow)
                    {
                        _mInfo._mRecorderEndUTC = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc);
                    }
                    mUpdateDevice("Defective", true);
                    // update study 
                }
            }
        }

        private void buttonSetIntransit_Click(object sender, EventArgs e)
        {
            if (_mInfo.mIndex_KEY > 0)
            {
                _mInfo._mState_IX = (UInt16)DDeviceState.Intransit;
                mUpdateDevice("Intransit", true);
                // update study 
            }

        }

        private void panel49_Paint(object sender, PaintEventArgs e)
        {

        }

        private void buttonShowStudy_Click(object sender, EventArgs e)
        {
            if (_mInfo.mIndex_KEY > 0 && _mInfo._mActiveStudy_IX > 0)
            {
                CStudyInfo study = new CStudyInfo(CDvtmsData.sGetDBaseConnection());

                if (study != null)
                {
                    if (study.mbDoSqlSelectIndex(_mInfo._mActiveStudy_IX, study.mGetValidMask(true)))
                    {
                        CStudyPatientForm form = new CStudyPatientForm(study, null, false,false, true, true);

                        if (form != null)
                        {
                            form.ShowDialog();
                            mLoadList(_mInfo._mDeviceSerialNr);
                            mSetValues();
                            mUpdateButtons();
                        }
                    }
                }
            }
        }

        private void checkBoxShowDeactivated_CheckedChanged(object sender, EventArgs e)
        {
            mLoadList(null);
        }

        private void buttonTzConfig_Click(object sender, EventArgs e)
        {
            if (_mInfo.mIndex_KEY > 0)
            {
                bool b = _mInfo._mActiveStudy_IX > 0;

                if( false == b)
                {
                    b = CProgram.sbAskYesNo("TZ config: Create Study first", "No Study, Continue to config");
                }
                if( b)
                {
                    TZmedicalDevSet form = new TZmedicalDevSet(_mInfo._mDeviceSerialNr, _mInfo);

                    if (form != null)
                    {
                        form.ShowDialog();
                    }
                }
            }
        }

        private void buttonSirConfig_Click(object sender, EventArgs e)
        {
            if (_mInfo.mIndex_KEY > 0)
            {
                bool b = _mInfo._mActiveStudy_IX > 0;

                if (false == b)
                {
                    b = CProgram.sbAskYesNo("Sirona config: Create Study first", "No Study, Continue to config");
                }
                if (b)
                {
                    SironaDevSet form = new SironaDevSet(_mInfo._mDeviceSerialNr, _mInfo);

                    if (form != null)
                    {
                        form.ShowDialog();
                    }
                }
            }
        }

        private void buttonChangeSNR_Click(object sender, EventArgs e)
        {
            UInt32 index;
            mGetValues(out index);

            // check values
            if (false == mbCheckForm(_mInfo.mIndex_KEY == 0 ? "Add" : "Update"))
            {
                return;
            }

            if (_mInfo.mIndex_KEY == 0)
            {
                labelError.Text = "Only Change Serial Number of existing";
                return;
            }
            string oldSnr = _mInfo._mDeviceSerialNr;
            string newSnr = oldSnr;

            //string tzStoreName = "";
            if (_mInfo != null && _mInfo.mIndex_KEY != 0)
            {
                string deviceID = _mInfo._mDeviceSerialNr;
                if (deviceID != null && deviceID != "")
                {
                    string devicePath = "";
                    bool bQueryActive = CTzDeviceSettings.sbGetTzAeraDeviceUnitPath(out devicePath, deviceID);

                    if (bQueryActive)
                    {
                        /*                        string storeName = CDvtmsData.sGetTzStoreSnrName(deviceID);
                                                if (storeName != deviceID)
                                                {
                                                    tzStoreName = "Tz store name = " + storeName;
                                                }
                        */
                        newSnr = CDvtmsData.sGetTzAltSnrName(oldSnr);
                    }
                }
                if (false == _mInfo.mbActive)
                {
                    if (false == CProgram.sbAskOkCancel("Change Device " + _mInfo.mIndex_KEY.ToString() + " serial nr " + oldSnr,
                        "Device not active, Continue?"))
                    {
                        return;
                    }

                }
                if ( (UInt16)DDeviceState.InService != _mInfo._mState_IX && (UInt16)DDeviceState.New != _mInfo._mState_IX)
                {
                    labelError.Text = "Change SNR, not in Service";
                        return;
                }
                if (false == CProgram.sbReqLabel("Change Device " + _mInfo.mIndex_KEY.ToString() + " serial nr " + oldSnr,
                        "New Serial Number", ref newSnr, "", false))
                    {
                        return;
                    }
                if (false == CProgram.sbAskOkCancel("Change Device " + _mInfo.mIndex_KEY.ToString() + " serial nr " + oldSnr,
                    "Change Serial Number from " + oldSnr + " to " + newSnr + "?"))
                {
                    return;
                }
                if( newSnr == null || newSnr.Length == 0)
                {
                    labelError.Text = "Change to empty snr not allowed!";
                    return;
                }
                newSnr = newSnr.Trim();
                if (newSnr == null || newSnr.Length < 3)
                {
                    labelError.Text = "Change snr needs at least 3 characters!";
                    return;
                }

                // check if serialNr is not in list

                CSqlDataTableRow row = _mInfo.mGetNodeFromList(_mList, (UInt16)DDeviceInfoVars.DeviceSerialNr, newSnr);

                _mInfo._mState_IX = (UInt16)DDeviceState.Available;

                if (row != null)
                {
                    labelError.Text = "Serial Number already exists";
                }
                else
                {
                    UInt64 forceMask = CSqlDataTableRow.sGetMask((UInt16)DDeviceInfoVars.DeviceSerialNr);

                    _mInfo._mDeviceSerialNr = newSnr;
                    // check if label is in list (not this)
                    if (_mInfo.mbDoSqlUpdate(_mInfo.mMaskValid, _mInfo.mbActive, forceMask))
                    {
                        labelError.Text = "Updated+SNR changed";
                    }
                    else
                    {
                        labelError.Text = "Failed Update";
                    }
                }
                mLoadList(_mInfo._mDeviceSerialNr);
                mSetValues();
                mUpdateButtons();
            }
        }

        private void label17_Click(object sender, EventArgs e)
        {
            // 20190328 client_IX is now active
            string s = comboBoxUseClient.Text;
            if (s != null && s.Length > 0 && s[0] != '-')
            {
                textBoxRemLabel.Text = s;
            }

        }

        private void label2_Click(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl)
            {
                if (_mModelList != null && _mDeviceModel != null)
                {
                    UInt32 modelIX = _mDeviceModel.mGetIndexKeyFromList(_mModelList, (UInt16)DDeviceModelVars.ModelLabel, comboBoxModel.Text);

                    if( modelIX > 0)
                    {
                        if( _mDeviceModel.mbDoSqlSelectIndex( modelIX, _mDeviceModel.mGetValidMask(true)))
                        {
                            CDeviceModelForm form = new CDeviceModelForm(_mDeviceModel);

                            if (form != null)
                            {
                                form.ShowDialog();
                            }
                        }
                    }
                }
            }
        }

        private void labelNextServiceOff_Click(object sender, EventArgs e)
        {
            dateTimePickerNextService.Value = new DateTime(1900, 1, 1);
            labelNextService.ForeColor = Color.Gray;
        }

        private void labelRefID_DoubleClick(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl)
            {
                mChangeRefID();
            }
        }
    }
}
