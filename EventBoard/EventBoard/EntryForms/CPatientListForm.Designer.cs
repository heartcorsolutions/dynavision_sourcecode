﻿namespace EventBoardEntryForms
{
    partial class CPatientListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.StudyNr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Birthdate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Age = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Gender = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Phone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.labelPatientSearch = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.labelError = new System.Windows.Forms.Label();
            this.labelSelected = new System.Windows.Forms.Label();
            this.buttonOpenLastStudy = new System.Windows.Forms.Button();
            this.ButtonEnable = new System.Windows.Forms.Button();
            this.buttonFindStudy = new System.Windows.Forms.Button();
            this.buttonEdit = new System.Windows.Forms.Button();
            this.buttonDisable = new System.Windows.Forms.Button();
            this.buttonSelect = new System.Windows.Forms.Button();
            this.buttonClose = new System.Windows.Forms.Button();
            this.labelList = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.labelInfo = new System.Windows.Forms.Label();
            this.labelGender = new System.Windows.Forms.Label();
            this.comboBoxGender = new System.Windows.Forms.ComboBox();
            this.labelBirthDate = new System.Windows.Forms.Label();
            this.labelUniqueID = new System.Windows.Forms.Label();
            this.labelPatientID = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.buttonClear = new System.Windows.Forms.Button();
            this.textBoxPatientIX = new System.Windows.Forms.TextBox();
            this.labelPatient = new System.Windows.Forms.Label();
            this.textBoxLastName = new System.Windows.Forms.TextBox();
            this.textBoxFirstName = new System.Windows.Forms.TextBox();
            this.textBoxUniqueID = new System.Windows.Forms.TextBox();
            this.textBoxPatientID = new System.Windows.Forms.TextBox();
            this.checkBoxDeactivePatients = new System.Windows.Forms.CheckBox();
            this.labelLastName = new System.Windows.Forms.Label();
            this.labelFirstName = new System.Windows.Forms.Label();
            this.dateTimeDOB = new System.Windows.Forms.DateTimePicker();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 127);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1038, 10);
            this.panel1.TabIndex = 0;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.dataGridView);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel10.Location = new System.Drawing.Point(0, 137);
            this.panel10.Margin = new System.Windows.Forms.Padding(4);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(1038, 529);
            this.panel10.TabIndex = 3;
            // 
            // dataGridView
            // 
            this.dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.dataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.StudyNr,
            this.Column4,
            this.Column1,
            this.Column2,
            this.FirstName,
            this.Column3,
            this.LastName,
            this.Birthdate,
            this.Age,
            this.Gender,
            this.Phone});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView.Location = new System.Drawing.Point(0, 0);
            this.dataGridView.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            this.dataGridView.RowHeadersVisible = false;
            this.dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView.Size = new System.Drawing.Size(1038, 529);
            this.dataGridView.TabIndex = 0;
            this.dataGridView.SelectionChanged += new System.EventHandler(this.dataGridView_SelectionChanged);
            // 
            // StudyNr
            // 
            this.StudyNr.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.StudyNr.DefaultCellStyle = dataGridViewCellStyle1;
            this.StudyNr.HeaderText = "#";
            this.StudyNr.MaxInputLength = 10;
            this.StudyNr.Name = "StudyNr";
            this.StudyNr.ReadOnly = true;
            this.StudyNr.Width = 41;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Unique ID";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 95;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Pat. ID";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 75;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "State";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Visible = false;
            this.Column2.Width = 66;
            // 
            // FirstName
            // 
            this.FirstName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.FirstName.HeaderText = "First name";
            this.FirstName.MaxInputLength = 25;
            this.FirstName.Name = "FirstName";
            this.FirstName.ReadOnly = true;
            this.FirstName.Width = 99;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column3.HeaderText = "Middle Name";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 115;
            // 
            // LastName
            // 
            this.LastName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.LastName.HeaderText = "Last name";
            this.LastName.MaxInputLength = 35;
            this.LastName.Name = "LastName";
            this.LastName.ReadOnly = true;
            this.LastName.Width = 99;
            // 
            // Birthdate
            // 
            this.Birthdate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Birthdate.HeaderText = "Birthdate";
            this.Birthdate.MaxInputLength = 10;
            this.Birthdate.Name = "Birthdate";
            this.Birthdate.ReadOnly = true;
            this.Birthdate.Width = 90;
            // 
            // Age
            // 
            this.Age.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Age.DefaultCellStyle = dataGridViewCellStyle2;
            this.Age.HeaderText = "Age";
            this.Age.MaxInputLength = 3;
            this.Age.Name = "Age";
            this.Age.ReadOnly = true;
            this.Age.Width = 58;
            // 
            // Gender
            // 
            this.Gender.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Gender.HeaderText = "Gender";
            this.Gender.MaxInputLength = 6;
            this.Gender.Name = "Gender";
            this.Gender.ReadOnly = true;
            this.Gender.Width = 81;
            // 
            // Phone
            // 
            this.Phone.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Phone.HeaderText = "Phone number";
            this.Phone.MaxInputLength = 12;
            this.Phone.Name = "Phone";
            this.Phone.ReadOnly = true;
            this.Phone.Width = 126;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.MediumPurple;
            this.panel3.Controls.Add(this.labelPatientSearch);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 22);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1038, 25);
            this.panel3.TabIndex = 2;
            // 
            // labelPatientSearch
            // 
            this.labelPatientSearch.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPatientSearch.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelPatientSearch.ForeColor = System.Drawing.Color.White;
            this.labelPatientSearch.Location = new System.Drawing.Point(0, 0);
            this.labelPatientSearch.Name = "labelPatientSearch";
            this.labelPatientSearch.Size = new System.Drawing.Size(125, 25);
            this.labelPatientSearch.TabIndex = 3;
            this.labelPatientSearch.Text = "Patient search";
            this.labelPatientSearch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Controls.Add(this.buttonOpenLastStudy);
            this.panel4.Controls.Add(this.ButtonEnable);
            this.panel4.Controls.Add(this.buttonFindStudy);
            this.panel4.Controls.Add(this.buttonEdit);
            this.panel4.Controls.Add(this.buttonDisable);
            this.panel4.Controls.Add(this.buttonSelect);
            this.panel4.Controls.Add(this.buttonClose);
            this.panel4.Controls.Add(this.labelList);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(0, 666);
            this.panel4.Margin = new System.Windows.Forms.Padding(4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1038, 45);
            this.panel4.TabIndex = 3;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.labelError);
            this.panel6.Controls.Add(this.labelSelected);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel6.Location = new System.Drawing.Point(308, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(126, 45);
            this.panel6.TabIndex = 18;
            // 
            // labelError
            // 
            this.labelError.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelError.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labelError.Location = new System.Drawing.Point(0, 17);
            this.labelError.Name = "labelError";
            this.labelError.Size = new System.Drawing.Size(126, 28);
            this.labelError.TabIndex = 17;
            this.labelError.Text = "#";
            // 
            // labelSelected
            // 
            this.labelSelected.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelSelected.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labelSelected.Location = new System.Drawing.Point(0, 0);
            this.labelSelected.Name = "labelSelected";
            this.labelSelected.Size = new System.Drawing.Size(126, 17);
            this.labelSelected.TabIndex = 15;
            this.labelSelected.Text = "#";
            // 
            // buttonOpenLastStudy
            // 
            this.buttonOpenLastStudy.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonOpenLastStudy.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonOpenLastStudy.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonOpenLastStudy.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonOpenLastStudy.FlatAppearance.BorderSize = 0;
            this.buttonOpenLastStudy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOpenLastStudy.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonOpenLastStudy.ForeColor = System.Drawing.Color.White;
            this.buttonOpenLastStudy.Location = new System.Drawing.Point(436, 0);
            this.buttonOpenLastStudy.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonOpenLastStudy.Name = "buttonOpenLastStudy";
            this.buttonOpenLastStudy.Size = new System.Drawing.Size(167, 45);
            this.buttonOpenLastStudy.TabIndex = 17;
            this.buttonOpenLastStudy.Text = "Open Last Study";
            this.buttonOpenLastStudy.UseVisualStyleBackColor = false;
            this.buttonOpenLastStudy.Click += new System.EventHandler(this.buttonOpenLastStudy_Click);
            // 
            // ButtonEnable
            // 
            this.ButtonEnable.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.ButtonEnable.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ButtonEnable.Dock = System.Windows.Forms.DockStyle.Left;
            this.ButtonEnable.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.ButtonEnable.FlatAppearance.BorderSize = 0;
            this.ButtonEnable.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonEnable.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.ButtonEnable.ForeColor = System.Drawing.Color.White;
            this.ButtonEnable.Location = new System.Drawing.Point(208, 0);
            this.ButtonEnable.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ButtonEnable.Name = "ButtonEnable";
            this.ButtonEnable.Size = new System.Drawing.Size(100, 45);
            this.ButtonEnable.TabIndex = 13;
            this.ButtonEnable.Text = "Enable";
            this.ButtonEnable.UseVisualStyleBackColor = false;
            this.ButtonEnable.Visible = false;
            this.ButtonEnable.Click += new System.EventHandler(this.ButtonEnable_Click);
            // 
            // buttonFindStudy
            // 
            this.buttonFindStudy.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonFindStudy.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonFindStudy.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonFindStudy.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonFindStudy.FlatAppearance.BorderSize = 0;
            this.buttonFindStudy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFindStudy.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonFindStudy.ForeColor = System.Drawing.Color.White;
            this.buttonFindStudy.Location = new System.Drawing.Point(603, 0);
            this.buttonFindStudy.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonFindStudy.Name = "buttonFindStudy";
            this.buttonFindStudy.Size = new System.Drawing.Size(135, 45);
            this.buttonFindStudy.TabIndex = 12;
            this.buttonFindStudy.Text = "Find Study";
            this.buttonFindStudy.UseVisualStyleBackColor = false;
            this.buttonFindStudy.Visible = false;
            // 
            // buttonEdit
            // 
            this.buttonEdit.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonEdit.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonEdit.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonEdit.FlatAppearance.BorderSize = 0;
            this.buttonEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEdit.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonEdit.ForeColor = System.Drawing.Color.White;
            this.buttonEdit.Location = new System.Drawing.Point(738, 0);
            this.buttonEdit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonEdit.Name = "buttonEdit";
            this.buttonEdit.Size = new System.Drawing.Size(100, 45);
            this.buttonEdit.TabIndex = 3;
            this.buttonEdit.Text = "Edit";
            this.buttonEdit.UseVisualStyleBackColor = false;
            this.buttonEdit.Visible = false;
            this.buttonEdit.Click += new System.EventHandler(this.button3_Click);
            // 
            // buttonDisable
            // 
            this.buttonDisable.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonDisable.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonDisable.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonDisable.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonDisable.FlatAppearance.BorderSize = 0;
            this.buttonDisable.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDisable.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonDisable.ForeColor = System.Drawing.Color.White;
            this.buttonDisable.Location = new System.Drawing.Point(108, 0);
            this.buttonDisable.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonDisable.Name = "buttonDisable";
            this.buttonDisable.Size = new System.Drawing.Size(100, 45);
            this.buttonDisable.TabIndex = 3;
            this.buttonDisable.Text = "Disable";
            this.buttonDisable.UseVisualStyleBackColor = false;
            this.buttonDisable.Visible = false;
            this.buttonDisable.Click += new System.EventHandler(this.buttonDisable_Click);
            // 
            // buttonSelect
            // 
            this.buttonSelect.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonSelect.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonSelect.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonSelect.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonSelect.FlatAppearance.BorderSize = 0;
            this.buttonSelect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSelect.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonSelect.ForeColor = System.Drawing.Color.White;
            this.buttonSelect.Location = new System.Drawing.Point(838, 0);
            this.buttonSelect.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonSelect.Name = "buttonSelect";
            this.buttonSelect.Size = new System.Drawing.Size(100, 45);
            this.buttonSelect.TabIndex = 10;
            this.buttonSelect.Text = "Select";
            this.buttonSelect.UseVisualStyleBackColor = false;
            this.buttonSelect.Visible = false;
            this.buttonSelect.Click += new System.EventHandler(this.buttonSelect_Click);
            // 
            // buttonClose
            // 
            this.buttonClose.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonClose.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonClose.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonClose.FlatAppearance.BorderSize = 0;
            this.buttonClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonClose.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonClose.ForeColor = System.Drawing.Color.White;
            this.buttonClose.Location = new System.Drawing.Point(938, 0);
            this.buttonClose.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(100, 45);
            this.buttonClose.TabIndex = 3;
            this.buttonClose.Text = "Close";
            this.buttonClose.UseVisualStyleBackColor = false;
            this.buttonClose.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // labelList
            // 
            this.labelList.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelList.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labelList.Location = new System.Drawing.Point(0, 0);
            this.labelList.Name = "labelList";
            this.labelList.Size = new System.Drawing.Size(108, 45);
            this.labelList.TabIndex = 15;
            this.labelList.Text = "999 Active";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.labelInfo);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1038, 22);
            this.panel5.TabIndex = 4;
            this.panel5.DoubleClick += new System.EventHandler(this.panel5_DoubleClick);
            // 
            // labelInfo
            // 
            this.labelInfo.AutoSize = true;
            this.labelInfo.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelInfo.Font = new System.Drawing.Font("Arial", 12F);
            this.labelInfo.Location = new System.Drawing.Point(0, 0);
            this.labelInfo.Name = "labelInfo";
            this.labelInfo.Size = new System.Drawing.Size(129, 18);
            this.labelInfo.TabIndex = 28;
            this.labelInfo.Text = "^extra search info";
            this.labelInfo.DoubleClick += new System.EventHandler(this.labelInfo_DoubleClick);
            // 
            // labelGender
            // 
            this.labelGender.AutoSize = true;
            this.labelGender.Location = new System.Drawing.Point(477, 15);
            this.labelGender.Name = "labelGender";
            this.labelGender.Size = new System.Drawing.Size(60, 17);
            this.labelGender.TabIndex = 19;
            this.labelGender.Text = "Gender:";
            this.labelGender.DoubleClick += new System.EventHandler(this.labelGender_DoubleClick);
            // 
            // comboBoxGender
            // 
            this.comboBoxGender.FormattingEnabled = true;
            this.comboBoxGender.Items.AddRange(new object[] {
            "",
            "Male",
            "Female"});
            this.comboBoxGender.Location = new System.Drawing.Point(550, 11);
            this.comboBoxGender.Name = "comboBoxGender";
            this.comboBoxGender.Size = new System.Drawing.Size(89, 24);
            this.comboBoxGender.TabIndex = 18;
            // 
            // labelBirthDate
            // 
            this.labelBirthDate.AutoSize = true;
            this.labelBirthDate.Location = new System.Drawing.Point(477, 46);
            this.labelBirthDate.Name = "labelBirthDate";
            this.labelBirthDate.Size = new System.Drawing.Size(69, 17);
            this.labelBirthDate.TabIndex = 17;
            this.labelBirthDate.Text = "Birthdate:";
            this.labelBirthDate.DoubleClick += new System.EventHandler(this.labelBirthDate_DoubleClick);
            // 
            // labelUniqueID
            // 
            this.labelUniqueID.AutoSize = true;
            this.labelUniqueID.Location = new System.Drawing.Point(6, 46);
            this.labelUniqueID.Name = "labelUniqueID";
            this.labelUniqueID.Size = new System.Drawing.Size(74, 17);
            this.labelUniqueID.TabIndex = 15;
            this.labelUniqueID.Text = "Unique ID:";
            this.labelUniqueID.DoubleClick += new System.EventHandler(this.labelUniqueID_DoubleClick);
            // 
            // labelPatientID
            // 
            this.labelPatientID.AutoSize = true;
            this.labelPatientID.Location = new System.Drawing.Point(6, 15);
            this.labelPatientID.Name = "labelPatientID";
            this.labelPatientID.Size = new System.Drawing.Size(73, 17);
            this.labelPatientID.TabIndex = 13;
            this.labelPatientID.Text = "Patient ID:";
            this.labelPatientID.DoubleClick += new System.EventHandler(this.labelPatientID_DoubleClick);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.buttonClear);
            this.panel2.Controls.Add(this.textBoxPatientIX);
            this.panel2.Controls.Add(this.labelPatient);
            this.panel2.Controls.Add(this.textBoxLastName);
            this.panel2.Controls.Add(this.textBoxFirstName);
            this.panel2.Controls.Add(this.textBoxUniqueID);
            this.panel2.Controls.Add(this.textBoxPatientID);
            this.panel2.Controls.Add(this.checkBoxDeactivePatients);
            this.panel2.Controls.Add(this.labelLastName);
            this.panel2.Controls.Add(this.labelFirstName);
            this.panel2.Controls.Add(this.dateTimeDOB);
            this.panel2.Controls.Add(this.buttonSearch);
            this.panel2.Controls.Add(this.labelPatientID);
            this.panel2.Controls.Add(this.labelUniqueID);
            this.panel2.Controls.Add(this.labelGender);
            this.panel2.Controls.Add(this.comboBoxGender);
            this.panel2.Controls.Add(this.labelBirthDate);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 47);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1038, 80);
            this.panel2.TabIndex = 5;
            // 
            // buttonClear
            // 
            this.buttonClear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonClear.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttonClear.FlatAppearance.BorderSize = 0;
            this.buttonClear.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonClear.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonClear.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonClear.Location = new System.Drawing.Point(877, 6);
            this.buttonClear.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(149, 26);
            this.buttonClear.TabIndex = 37;
            this.buttonClear.Text = "Clear";
            this.buttonClear.UseVisualStyleBackColor = false;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // textBoxPatientIX
            // 
            this.textBoxPatientIX.Location = new System.Drawing.Point(738, 12);
            this.textBoxPatientIX.Name = "textBoxPatientIX";
            this.textBoxPatientIX.Size = new System.Drawing.Size(79, 23);
            this.textBoxPatientIX.TabIndex = 36;
            // 
            // labelPatient
            // 
            this.labelPatient.AutoSize = true;
            this.labelPatient.Location = new System.Drawing.Point(659, 15);
            this.labelPatient.Name = "labelPatient";
            this.labelPatient.Size = new System.Drawing.Size(68, 17);
            this.labelPatient.TabIndex = 35;
            this.labelPatient.Text = "Patient #:";
            this.labelPatient.DoubleClick += new System.EventHandler(this.labelPatient_DoubleClick);
            // 
            // textBoxLastName
            // 
            this.textBoxLastName.Location = new System.Drawing.Point(304, 43);
            this.textBoxLastName.Name = "textBoxLastName";
            this.textBoxLastName.Size = new System.Drawing.Size(167, 23);
            this.textBoxLastName.TabIndex = 34;
            // 
            // textBoxFirstName
            // 
            this.textBoxFirstName.Location = new System.Drawing.Point(304, 12);
            this.textBoxFirstName.Name = "textBoxFirstName";
            this.textBoxFirstName.Size = new System.Drawing.Size(167, 23);
            this.textBoxFirstName.TabIndex = 33;
            // 
            // textBoxUniqueID
            // 
            this.textBoxUniqueID.Location = new System.Drawing.Point(85, 43);
            this.textBoxUniqueID.Name = "textBoxUniqueID";
            this.textBoxUniqueID.Size = new System.Drawing.Size(129, 23);
            this.textBoxUniqueID.TabIndex = 32;
            // 
            // textBoxPatientID
            // 
            this.textBoxPatientID.Location = new System.Drawing.Point(85, 12);
            this.textBoxPatientID.Name = "textBoxPatientID";
            this.textBoxPatientID.Size = new System.Drawing.Size(129, 23);
            this.textBoxPatientID.TabIndex = 31;
            // 
            // checkBoxDeactivePatients
            // 
            this.checkBoxDeactivePatients.AutoSize = true;
            this.checkBoxDeactivePatients.Location = new System.Drawing.Point(662, 45);
            this.checkBoxDeactivePatients.Name = "checkBoxDeactivePatients";
            this.checkBoxDeactivePatients.Size = new System.Drawing.Size(148, 21);
            this.checkBoxDeactivePatients.TabIndex = 7;
            this.checkBoxDeactivePatients.Text = "Show in-active only";
            this.checkBoxDeactivePatients.UseVisualStyleBackColor = true;
            // 
            // labelLastName
            // 
            this.labelLastName.AutoSize = true;
            this.labelLastName.Location = new System.Drawing.Point(220, 46);
            this.labelLastName.Name = "labelLastName";
            this.labelLastName.Size = new System.Drawing.Size(78, 17);
            this.labelLastName.TabIndex = 29;
            this.labelLastName.Text = "Last name:";
            this.labelLastName.DoubleClick += new System.EventHandler(this.labelLastName_DoubleClick);
            // 
            // labelFirstName
            // 
            this.labelFirstName.AutoSize = true;
            this.labelFirstName.Location = new System.Drawing.Point(220, 15);
            this.labelFirstName.Name = "labelFirstName";
            this.labelFirstName.Size = new System.Drawing.Size(78, 17);
            this.labelFirstName.TabIndex = 27;
            this.labelFirstName.Text = "First name:";
            this.labelFirstName.Click += new System.EventHandler(this.labelFirstName_Click);
            this.labelFirstName.DoubleClick += new System.EventHandler(this.labelFirstName_DoubleClick);
            // 
            // dateTimeDOB
            // 
            this.dateTimeDOB.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimeDOB.Location = new System.Drawing.Point(550, 43);
            this.dateTimeDOB.Name = "dateTimeDOB";
            this.dateTimeDOB.Size = new System.Drawing.Size(89, 23);
            this.dateTimeDOB.TabIndex = 25;
            // 
            // buttonSearch
            // 
            this.buttonSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonSearch.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttonSearch.FlatAppearance.BorderSize = 0;
            this.buttonSearch.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonSearch.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonSearch.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonSearch.Location = new System.Drawing.Point(877, 37);
            this.buttonSearch.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(149, 26);
            this.buttonSearch.TabIndex = 24;
            this.buttonSearch.Text = "Search";
            this.buttonSearch.UseVisualStyleBackColor = false;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // CPatientListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(1038, 711);
            this.Controls.Add(this.panel10);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel5);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "CPatientListForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Patient List";
            this.panel10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Button buttonEdit;
        private System.Windows.Forms.Button buttonDisable;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Button buttonSelect;
        private System.Windows.Forms.Button buttonFindStudy;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label labelPatientSearch;
        private System.Windows.Forms.Label labelGender;
        private System.Windows.Forms.ComboBox comboBoxGender;
        private System.Windows.Forms.Label labelBirthDate;
        private System.Windows.Forms.Label labelUniqueID;
        private System.Windows.Forms.Label labelPatientID;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button buttonSearch;
        private System.Windows.Forms.Label labelLastName;
        private System.Windows.Forms.Label labelFirstName;
        private System.Windows.Forms.DateTimePicker dateTimeDOB;
        private System.Windows.Forms.CheckBox checkBoxDeactivePatients;
        private System.Windows.Forms.TextBox textBoxLastName;
        private System.Windows.Forms.TextBox textBoxFirstName;
        private System.Windows.Forms.TextBox textBoxUniqueID;
        private System.Windows.Forms.TextBox textBoxPatientID;
        private System.Windows.Forms.TextBox textBoxPatientIX;
        private System.Windows.Forms.Label labelPatient;
        private System.Windows.Forms.Button ButtonEnable;
        private System.Windows.Forms.Label labelList;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Button buttonOpenLastStudy;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label labelError;
        private System.Windows.Forms.Label labelSelected;
        private System.Windows.Forms.DataGridViewTextBoxColumn StudyNr;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn FirstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn LastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Birthdate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Age;
        private System.Windows.Forms.DataGridViewTextBoxColumn Gender;
        private System.Windows.Forms.DataGridViewTextBoxColumn Phone;
        private System.Windows.Forms.Label labelInfo;
    }
}