﻿namespace EventBoardEntryForms
{
    partial class CStudyListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.buttonViewPdf = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.buttonHolter = new System.Windows.Forms.Button();
            this.labelPatInfo = new System.Windows.Forms.Label();
            this.buttonEndRecording = new System.Windows.Forms.Button();
            this.buttonFindings = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.buttonSelect = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelInfo = new System.Windows.Forms.Label();
            this.labelUpdTime = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.labelSelected = new System.Windows.Forms.Label();
            this.labelError = new System.Windows.Forms.Label();
            this.labelStudySearch = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.buttonCsvExportShort = new System.Windows.Forms.Button();
            this.comboBoxDeviceState = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textBoxCmpOffset = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.buttonCsvExportLong = new System.Windows.Forms.Button();
            this.checkBoxEndsInDays = new System.Windows.Forms.CheckBox();
            this.textBoxEndsNrDays = new System.Windows.Forms.TextBox();
            this.textBoxEndsInFrom = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxActiveRecording = new System.Windows.Forms.TextBox();
            this.checkBoxActiveRecording = new System.Windows.Forms.CheckBox();
            this.comboBoxLocation = new System.Windows.Forms.ComboBox();
            this.checkBoxTrial = new System.Windows.Forms.CheckBox();
            this.checkBoxStudyPermissions = new System.Windows.Forms.CheckBox();
            this.labelStudyPermissions = new System.Windows.Forms.Label();
            this.labelRowLimit = new System.Windows.Forms.Label();
            this.checkBoxUseLimit = new System.Windows.Forms.CheckBox();
            this.checkBoxAnonymize = new System.Windows.Forms.CheckBox();
            this.labelPhysiscian = new System.Windows.Forms.Label();
            this.comboBoxPhysician = new System.Windows.Forms.ComboBox();
            this.buttonClear = new System.Windows.Forms.Button();
            this.dateTimePickerEnd = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxPatient = new System.Windows.Forms.TextBox();
            this.textBoxStudy = new System.Windows.Forms.TextBox();
            this.checkBoxInactive = new System.Windows.Forms.CheckBox();
            this.button3 = new System.Windows.Forms.Button();
            this.comboBoxRecorder = new System.Windows.Forms.ComboBox();
            this.dateTimePickerStart = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBoxTrial = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBoxClient = new System.Windows.Forms.ComboBox();
            this.labelPatientName = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.labelRecorderInfo = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.labelDeviceCount = new System.Windows.Forms.Label();
            this.labelMaxEvents = new System.Windows.Forms.Label();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StudyLabel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnPermissions = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.saveFileDialogCsv = new System.Windows.Forms.SaveFileDialog();
            this.panel5.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonViewPdf
            // 
            this.buttonViewPdf.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonViewPdf.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonViewPdf.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonViewPdf.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttonViewPdf.FlatAppearance.BorderSize = 0;
            this.buttonViewPdf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonViewPdf.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonViewPdf.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonViewPdf.Location = new System.Drawing.Point(918, 0);
            this.buttonViewPdf.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonViewPdf.Name = "buttonViewPdf";
            this.buttonViewPdf.Size = new System.Drawing.Size(88, 68);
            this.buttonViewPdf.TabIndex = 4;
            this.buttonViewPdf.Text = "Report  \r\nList";
            this.buttonViewPdf.UseVisualStyleBackColor = false;
            this.buttonViewPdf.Click += new System.EventHandler(this.buttonViewPdf_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button2.Dock = System.Windows.Forms.DockStyle.Right;
            this.button2.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.button2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button2.Location = new System.Drawing.Point(1142, 0);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(63, 68);
            this.button2.TabIndex = 3;
            this.button2.Text = "Edit Study";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button1.Dock = System.Windows.Forms.DockStyle.Right;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button1.Location = new System.Drawing.Point(1205, 0);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(66, 68);
            this.button1.TabIndex = 3;
            this.button1.Text = "Close";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.panel5.Controls.Add(this.buttonHolter);
            this.panel5.Controls.Add(this.labelPatInfo);
            this.panel5.Controls.Add(this.buttonEndRecording);
            this.panel5.Controls.Add(this.buttonFindings);
            this.panel5.Controls.Add(this.buttonViewPdf);
            this.panel5.Controls.Add(this.button5);
            this.panel5.Controls.Add(this.buttonSelect);
            this.panel5.Controls.Add(this.button2);
            this.panel5.Controls.Add(this.button1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(0, 630);
            this.panel5.Margin = new System.Windows.Forms.Padding(4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1271, 68);
            this.panel5.TabIndex = 8;
            // 
            // buttonHolter
            // 
            this.buttonHolter.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonHolter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonHolter.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonHolter.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonHolter.FlatAppearance.BorderSize = 0;
            this.buttonHolter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonHolter.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonHolter.ForeColor = System.Drawing.Color.White;
            this.buttonHolter.Location = new System.Drawing.Point(647, 0);
            this.buttonHolter.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonHolter.Name = "buttonHolter";
            this.buttonHolter.Size = new System.Drawing.Size(75, 68);
            this.buttonHolter.TabIndex = 28;
            this.buttonHolter.Text = "Create Holter File";
            this.buttonHolter.UseVisualStyleBackColor = false;
            this.buttonHolter.Click += new System.EventHandler(this.buttonHolter_Click);
            // 
            // labelPatInfo
            // 
            this.labelPatInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPatInfo.Font = new System.Drawing.Font("Arial", 10F);
            this.labelPatInfo.ForeColor = System.Drawing.Color.White;
            this.labelPatInfo.Location = new System.Drawing.Point(0, 0);
            this.labelPatInfo.Name = "labelPatInfo";
            this.labelPatInfo.Size = new System.Drawing.Size(722, 68);
            this.labelPatInfo.TabIndex = 12;
            this.labelPatInfo.Text = "Patient info\r\n2\r\n3\r\n4";
            // 
            // buttonEndRecording
            // 
            this.buttonEndRecording.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonEndRecording.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonEndRecording.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonEndRecording.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttonEndRecording.FlatAppearance.BorderSize = 0;
            this.buttonEndRecording.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEndRecording.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonEndRecording.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonEndRecording.Location = new System.Drawing.Point(722, 0);
            this.buttonEndRecording.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonEndRecording.Name = "buttonEndRecording";
            this.buttonEndRecording.Size = new System.Drawing.Size(110, 68);
            this.buttonEndRecording.TabIndex = 16;
            this.buttonEndRecording.Text = "End \r\nRecording";
            this.buttonEndRecording.UseVisualStyleBackColor = false;
            this.buttonEndRecording.Click += new System.EventHandler(this.buttonEndRecording_Click);
            // 
            // buttonFindings
            // 
            this.buttonFindings.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonFindings.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonFindings.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonFindings.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttonFindings.FlatAppearance.BorderSize = 0;
            this.buttonFindings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFindings.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonFindings.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonFindings.Location = new System.Drawing.Point(832, 0);
            this.buttonFindings.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonFindings.Name = "buttonFindings";
            this.buttonFindings.Size = new System.Drawing.Size(86, 68);
            this.buttonFindings.TabIndex = 14;
            this.buttonFindings.Text = "Create \r\nStudy Report";
            this.buttonFindings.UseVisualStyleBackColor = false;
            this.buttonFindings.Click += new System.EventHandler(this.buttonFindings_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.button5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button5.Dock = System.Windows.Forms.DockStyle.Right;
            this.button5.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.button5.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button5.Location = new System.Drawing.Point(1006, 0);
            this.button5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(70, 68);
            this.button5.TabIndex = 11;
            this.button5.Text = "MCT \r\nTrend";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // buttonSelect
            // 
            this.buttonSelect.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonSelect.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonSelect.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonSelect.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttonSelect.FlatAppearance.BorderSize = 0;
            this.buttonSelect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSelect.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonSelect.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonSelect.Location = new System.Drawing.Point(1076, 0);
            this.buttonSelect.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonSelect.Name = "buttonSelect";
            this.buttonSelect.Size = new System.Drawing.Size(66, 68);
            this.buttonSelect.TabIndex = 7;
            this.buttonSelect.Text = "Select";
            this.buttonSelect.UseVisualStyleBackColor = false;
            this.buttonSelect.Visible = false;
            this.buttonSelect.Click += new System.EventHandler(this.buttonSelect_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel1.Controls.Add(this.labelInfo);
            this.panel1.Controls.Add(this.labelUpdTime);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1271, 20);
            this.panel1.TabIndex = 5;
            // 
            // labelInfo
            // 
            this.labelInfo.AutoSize = true;
            this.labelInfo.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelInfo.Font = new System.Drawing.Font("Arial", 12F);
            this.labelInfo.Location = new System.Drawing.Point(0, 0);
            this.labelInfo.Name = "labelInfo";
            this.labelInfo.Size = new System.Drawing.Size(129, 18);
            this.labelInfo.TabIndex = 27;
            this.labelInfo.Text = "^extra search info";
            this.labelInfo.DoubleClick += new System.EventHandler(this.labelInfo_DoubleClick);
            // 
            // labelUpdTime
            // 
            this.labelUpdTime.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelUpdTime.Font = new System.Drawing.Font("Arial", 8F);
            this.labelUpdTime.Location = new System.Drawing.Point(1114, 0);
            this.labelUpdTime.Name = "labelUpdTime";
            this.labelUpdTime.Size = new System.Drawing.Size(157, 20);
            this.labelUpdTime.TabIndex = 26;
            this.labelUpdTime.Text = "^9999 sec";
            this.labelUpdTime.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Study nr:";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.MediumPurple;
            this.panel4.Controls.Add(this.labelSelected);
            this.panel4.Controls.Add(this.labelError);
            this.panel4.Controls.Add(this.labelStudySearch);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 20);
            this.panel4.Margin = new System.Windows.Forms.Padding(4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1271, 25);
            this.panel4.TabIndex = 9;
            // 
            // labelSelected
            // 
            this.labelSelected.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSelected.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labelSelected.Location = new System.Drawing.Point(125, 0);
            this.labelSelected.Name = "labelSelected";
            this.labelSelected.Size = new System.Drawing.Size(106, 25);
            this.labelSelected.TabIndex = 21;
            this.labelSelected.Text = "#";
            this.labelSelected.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelSelected.Visible = false;
            // 
            // labelError
            // 
            this.labelError.AutoSize = true;
            this.labelError.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelError.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labelError.Location = new System.Drawing.Point(1255, 0);
            this.labelError.Name = "labelError";
            this.labelError.Size = new System.Drawing.Size(16, 16);
            this.labelError.TabIndex = 20;
            this.labelError.Text = "#";
            this.labelError.Visible = false;
            // 
            // labelStudySearch
            // 
            this.labelStudySearch.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelStudySearch.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelStudySearch.ForeColor = System.Drawing.Color.White;
            this.labelStudySearch.Location = new System.Drawing.Point(0, 0);
            this.labelStudySearch.Name = "labelStudySearch";
            this.labelStudySearch.Size = new System.Drawing.Size(125, 25);
            this.labelStudySearch.TabIndex = 2;
            this.labelStudySearch.Text = "Study search";
            this.labelStudySearch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel6.Controls.Add(this.buttonCsvExportShort);
            this.panel6.Controls.Add(this.comboBoxDeviceState);
            this.panel6.Controls.Add(this.label12);
            this.panel6.Controls.Add(this.textBoxCmpOffset);
            this.panel6.Controls.Add(this.label11);
            this.panel6.Controls.Add(this.buttonCsvExportLong);
            this.panel6.Controls.Add(this.checkBoxEndsInDays);
            this.panel6.Controls.Add(this.textBoxEndsNrDays);
            this.panel6.Controls.Add(this.textBoxEndsInFrom);
            this.panel6.Controls.Add(this.label10);
            this.panel6.Controls.Add(this.label4);
            this.panel6.Controls.Add(this.label7);
            this.panel6.Controls.Add(this.textBoxActiveRecording);
            this.panel6.Controls.Add(this.checkBoxActiveRecording);
            this.panel6.Controls.Add(this.comboBoxLocation);
            this.panel6.Controls.Add(this.checkBoxTrial);
            this.panel6.Controls.Add(this.checkBoxStudyPermissions);
            this.panel6.Controls.Add(this.labelStudyPermissions);
            this.panel6.Controls.Add(this.labelRowLimit);
            this.panel6.Controls.Add(this.checkBoxUseLimit);
            this.panel6.Controls.Add(this.checkBoxAnonymize);
            this.panel6.Controls.Add(this.labelPhysiscian);
            this.panel6.Controls.Add(this.comboBoxPhysician);
            this.panel6.Controls.Add(this.buttonClear);
            this.panel6.Controls.Add(this.dateTimePickerEnd);
            this.panel6.Controls.Add(this.label9);
            this.panel6.Controls.Add(this.textBoxPatient);
            this.panel6.Controls.Add(this.textBoxStudy);
            this.panel6.Controls.Add(this.checkBoxInactive);
            this.panel6.Controls.Add(this.button3);
            this.panel6.Controls.Add(this.comboBoxRecorder);
            this.panel6.Controls.Add(this.dateTimePickerStart);
            this.panel6.Controls.Add(this.label5);
            this.panel6.Controls.Add(this.label8);
            this.panel6.Controls.Add(this.comboBoxTrial);
            this.panel6.Controls.Add(this.label6);
            this.panel6.Controls.Add(this.comboBoxClient);
            this.panel6.Controls.Add(this.labelPatientName);
            this.panel6.Controls.Add(this.label3);
            this.panel6.Controls.Add(this.label1);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 45);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1271, 112);
            this.panel6.TabIndex = 10;
            // 
            // buttonCsvExportShort
            // 
            this.buttonCsvExportShort.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonCsvExportShort.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttonCsvExportShort.FlatAppearance.BorderSize = 0;
            this.buttonCsvExportShort.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonCsvExportShort.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonCsvExportShort.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonCsvExportShort.Location = new System.Drawing.Point(1117, 56);
            this.buttonCsvExportShort.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonCsvExportShort.Name = "buttonCsvExportShort";
            this.buttonCsvExportShort.Size = new System.Drawing.Size(80, 26);
            this.buttonCsvExportShort.TabIndex = 120;
            this.buttonCsvExportShort.Text = "csv-S";
            this.buttonCsvExportShort.UseVisualStyleBackColor = false;
            this.buttonCsvExportShort.Click += new System.EventHandler(this.buttonCsvExportShort_Click);
            // 
            // comboBoxDeviceState
            // 
            this.comboBoxDeviceState.FormattingEnabled = true;
            this.comboBoxDeviceState.Items.AddRange(new object[] {
            "-",
            "Recording",
            "Assigned",
            "InTransit",
            "Returned",
            "* any"});
            this.comboBoxDeviceState.Location = new System.Drawing.Point(511, 82);
            this.comboBoxDeviceState.Name = "comboBoxDeviceState";
            this.comboBoxDeviceState.Size = new System.Drawing.Size(108, 24);
            this.comboBoxDeviceState.TabIndex = 119;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(461, 85);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(55, 16);
            this.label12.TabIndex = 118;
            this.label12.Text = "Device:";
            // 
            // textBoxCmpOffset
            // 
            this.textBoxCmpOffset.Location = new System.Drawing.Point(582, 57);
            this.textBoxCmpOffset.Name = "textBoxCmpOffset";
            this.textBoxCmpOffset.Size = new System.Drawing.Size(37, 23);
            this.textBoxCmpOffset.TabIndex = 117;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(479, 60);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(100, 16);
            this.label11.TabIndex = 116;
            this.label11.Text = "compare hour:";
            // 
            // buttonCsvExportLong
            // 
            this.buttonCsvExportLong.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonCsvExportLong.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttonCsvExportLong.FlatAppearance.BorderSize = 0;
            this.buttonCsvExportLong.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonCsvExportLong.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonCsvExportLong.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonCsvExportLong.Location = new System.Drawing.Point(1117, 27);
            this.buttonCsvExportLong.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonCsvExportLong.Name = "buttonCsvExportLong";
            this.buttonCsvExportLong.Size = new System.Drawing.Size(80, 26);
            this.buttonCsvExportLong.TabIndex = 115;
            this.buttonCsvExportLong.Text = "csv-L";
            this.buttonCsvExportLong.UseVisualStyleBackColor = false;
            this.buttonCsvExportLong.Click += new System.EventHandler(this.buttonCsvExport_Click);
            // 
            // checkBoxEndsInDays
            // 
            this.checkBoxEndsInDays.Font = new System.Drawing.Font("Arial", 12F);
            this.checkBoxEndsInDays.Location = new System.Drawing.Point(464, 5);
            this.checkBoxEndsInDays.Name = "checkBoxEndsInDays";
            this.checkBoxEndsInDays.Size = new System.Drawing.Size(121, 23);
            this.checkBoxEndsInDays.TabIndex = 114;
            this.checkBoxEndsInDays.Text = "Ends in Days";
            this.checkBoxEndsInDays.UseVisualStyleBackColor = true;
            // 
            // textBoxEndsNrDays
            // 
            this.textBoxEndsNrDays.Location = new System.Drawing.Point(591, 28);
            this.textBoxEndsNrDays.Name = "textBoxEndsNrDays";
            this.textBoxEndsNrDays.Size = new System.Drawing.Size(29, 23);
            this.textBoxEndsNrDays.TabIndex = 113;
            this.textBoxEndsNrDays.Text = "99";
            // 
            // textBoxEndsInFrom
            // 
            this.textBoxEndsInFrom.Location = new System.Drawing.Point(515, 28);
            this.textBoxEndsInFrom.Name = "textBoxEndsInFrom";
            this.textBoxEndsInFrom.Size = new System.Drawing.Size(37, 23);
            this.textBoxEndsInFrom.TabIndex = 112;
            this.textBoxEndsInFrom.Text = "-1";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(552, 29);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 16);
            this.label10.TabIndex = 111;
            this.label10.Text = "days:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(477, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 16);
            this.label4.TabIndex = 110;
            this.label4.Text = "from:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(210, 86);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 16);
            this.label7.TabIndex = 109;
            this.label7.Text = "days";
            // 
            // textBoxActiveRecording
            // 
            this.textBoxActiveRecording.Location = new System.Drawing.Point(157, 81);
            this.textBoxActiveRecording.Name = "textBoxActiveRecording";
            this.textBoxActiveRecording.Size = new System.Drawing.Size(47, 23);
            this.textBoxActiveRecording.TabIndex = 108;
            // 
            // checkBoxActiveRecording
            // 
            this.checkBoxActiveRecording.Font = new System.Drawing.Font("Arial", 12F);
            this.checkBoxActiveRecording.Location = new System.Drawing.Point(9, 81);
            this.checkBoxActiveRecording.Name = "checkBoxActiveRecording";
            this.checkBoxActiveRecording.Size = new System.Drawing.Size(160, 24);
            this.checkBoxActiveRecording.TabIndex = 107;
            this.checkBoxActiveRecording.Text = "Active recording Δ";
            this.checkBoxActiveRecording.UseVisualStyleBackColor = true;
            // 
            // comboBoxLocation
            // 
            this.comboBoxLocation.FormattingEnabled = true;
            this.comboBoxLocation.Location = new System.Drawing.Point(723, 51);
            this.comboBoxLocation.Name = "comboBoxLocation";
            this.comboBoxLocation.Size = new System.Drawing.Size(163, 24);
            this.comboBoxLocation.TabIndex = 106;
            this.comboBoxLocation.Text = "-- All --";
            this.comboBoxLocation.SelectedIndexChanged += new System.EventHandler(this.comboBoxLocation_SelectedIndexChanged);
            // 
            // checkBoxTrial
            // 
            this.checkBoxTrial.Font = new System.Drawing.Font("Arial", 12F);
            this.checkBoxTrial.Location = new System.Drawing.Point(631, 82);
            this.checkBoxTrial.Name = "checkBoxTrial";
            this.checkBoxTrial.Size = new System.Drawing.Size(65, 21);
            this.checkBoxTrial.TabIndex = 32;
            this.checkBoxTrial.Text = "Trial:";
            this.checkBoxTrial.UseVisualStyleBackColor = true;
            // 
            // checkBoxStudyPermissions
            // 
            this.checkBoxStudyPermissions.Font = new System.Drawing.Font("Arial", 12F);
            this.checkBoxStudyPermissions.Location = new System.Drawing.Point(630, 53);
            this.checkBoxStudyPermissions.Name = "checkBoxStudyPermissions";
            this.checkBoxStudyPermissions.Size = new System.Drawing.Size(98, 24);
            this.checkBoxStudyPermissions.TabIndex = 31;
            this.checkBoxStudyPermissions.Text = "Location:";
            this.checkBoxStudyPermissions.UseVisualStyleBackColor = true;
            // 
            // labelStudyPermissions
            // 
            this.labelStudyPermissions.BackColor = System.Drawing.Color.Bisque;
            this.labelStudyPermissions.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelStudyPermissions.Font = new System.Drawing.Font("Arial", 12F);
            this.labelStudyPermissions.Location = new System.Drawing.Point(894, 83);
            this.labelStudyPermissions.Name = "labelStudyPermissions";
            this.labelStudyPermissions.Size = new System.Drawing.Size(303, 20);
            this.labelStudyPermissions.TabIndex = 30;
            this.labelStudyPermissions.Text = "^Site1+MCT-master";
            this.labelStudyPermissions.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelStudyPermissions.Visible = false;
            this.labelStudyPermissions.Click += new System.EventHandler(this.labelStudyPermissions_Click);
            // 
            // labelRowLimit
            // 
            this.labelRowLimit.Font = new System.Drawing.Font("Arial", 9F);
            this.labelRowLimit.Location = new System.Drawing.Point(953, 61);
            this.labelRowLimit.Name = "labelRowLimit";
            this.labelRowLimit.Size = new System.Drawing.Size(56, 18);
            this.labelRowLimit.TabIndex = 27;
            this.labelRowLimit.Text = "^00000";
            this.labelRowLimit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelRowLimit.Click += new System.EventHandler(this.labelRowLimit_Click);
            // 
            // checkBoxUseLimit
            // 
            this.checkBoxUseLimit.AutoSize = true;
            this.checkBoxUseLimit.Font = new System.Drawing.Font("Arial", 9F);
            this.checkBoxUseLimit.Location = new System.Drawing.Point(894, 60);
            this.checkBoxUseLimit.Name = "checkBoxUseLimit";
            this.checkBoxUseLimit.Size = new System.Drawing.Size(53, 19);
            this.checkBoxUseLimit.TabIndex = 26;
            this.checkBoxUseLimit.Text = "Limit";
            this.checkBoxUseLimit.UseVisualStyleBackColor = true;
            // 
            // checkBoxAnonymize
            // 
            this.checkBoxAnonymize.AutoSize = true;
            this.checkBoxAnonymize.Location = new System.Drawing.Point(894, 26);
            this.checkBoxAnonymize.Name = "checkBoxAnonymize";
            this.checkBoxAnonymize.Size = new System.Drawing.Size(95, 20);
            this.checkBoxAnonymize.TabIndex = 24;
            this.checkBoxAnonymize.Text = "Anonymize";
            this.checkBoxAnonymize.UseVisualStyleBackColor = true;
            this.checkBoxAnonymize.CheckStateChanged += new System.EventHandler(this.checkBoxAnonymize_CheckStateChanged);
            // 
            // labelPhysiscian
            // 
            this.labelPhysiscian.AutoSize = true;
            this.labelPhysiscian.Font = new System.Drawing.Font("Arial", 10F);
            this.labelPhysiscian.Location = new System.Drawing.Point(213, 60);
            this.labelPhysiscian.Name = "labelPhysiscian";
            this.labelPhysiscian.Size = new System.Drawing.Size(72, 16);
            this.labelPhysiscian.TabIndex = 23;
            this.labelPhysiscian.Text = "Physician:";
            this.labelPhysiscian.Click += new System.EventHandler(this.labelPhysiscian_Click);
            // 
            // comboBoxPhysician
            // 
            this.comboBoxPhysician.FormattingEnabled = true;
            this.comboBoxPhysician.Location = new System.Drawing.Point(286, 56);
            this.comboBoxPhysician.Name = "comboBoxPhysician";
            this.comboBoxPhysician.Size = new System.Drawing.Size(163, 24);
            this.comboBoxPhysician.TabIndex = 22;
            // 
            // buttonClear
            // 
            this.buttonClear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonClear.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttonClear.FlatAppearance.BorderSize = 0;
            this.buttonClear.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonClear.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonClear.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonClear.Location = new System.Drawing.Point(1024, 56);
            this.buttonClear.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(87, 26);
            this.buttonClear.TabIndex = 21;
            this.buttonClear.Text = "Clear";
            this.buttonClear.UseVisualStyleBackColor = false;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // dateTimePickerEnd
            // 
            this.dateTimePickerEnd.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerEnd.Location = new System.Drawing.Point(79, 54);
            this.dateTimePickerEnd.Name = "dateTimePickerEnd";
            this.dateTimePickerEnd.Size = new System.Drawing.Size(124, 23);
            this.dateTimePickerEnd.TabIndex = 20;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 57);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 16);
            this.label9.TabIndex = 19;
            this.label9.Text = "End Start:";
            // 
            // textBoxPatient
            // 
            this.textBoxPatient.Location = new System.Drawing.Point(723, 6);
            this.textBoxPatient.Name = "textBoxPatient";
            this.textBoxPatient.Size = new System.Drawing.Size(69, 23);
            this.textBoxPatient.TabIndex = 17;
            // 
            // textBoxStudy
            // 
            this.textBoxStudy.Location = new System.Drawing.Point(80, 4);
            this.textBoxStudy.Name = "textBoxStudy";
            this.textBoxStudy.Size = new System.Drawing.Size(124, 23);
            this.textBoxStudy.TabIndex = 4;
            this.textBoxStudy.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxStudy_KeyPress);
            // 
            // checkBoxInactive
            // 
            this.checkBoxInactive.AutoSize = true;
            this.checkBoxInactive.Location = new System.Drawing.Point(894, 44);
            this.checkBoxInactive.Name = "checkBoxInactive";
            this.checkBoxInactive.Size = new System.Drawing.Size(124, 20);
            this.checkBoxInactive.TabIndex = 16;
            this.checkBoxInactive.Text = "Inactive studies";
            this.checkBoxInactive.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button3.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.button3.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.button3.Location = new System.Drawing.Point(1024, 27);
            this.button3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(87, 26);
            this.button3.TabIndex = 15;
            this.button3.Text = "Search";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // comboBoxRecorder
            // 
            this.comboBoxRecorder.FormattingEnabled = true;
            this.comboBoxRecorder.Location = new System.Drawing.Point(286, 4);
            this.comboBoxRecorder.Name = "comboBoxRecorder";
            this.comboBoxRecorder.Size = new System.Drawing.Size(163, 24);
            this.comboBoxRecorder.TabIndex = 6;
            // 
            // dateTimePickerStart
            // 
            this.dateTimePickerStart.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerStart.Location = new System.Drawing.Point(79, 29);
            this.dateTimePickerStart.Name = "dateTimePickerStart";
            this.dateTimePickerStart.Size = new System.Drawing.Size(124, 23);
            this.dateTimePickerStart.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(212, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 16);
            this.label5.TabIndex = 7;
            this.label5.Text = "Recorder:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 10F);
            this.label8.Location = new System.Drawing.Point(644, 7);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 16);
            this.label8.TabIndex = 13;
            this.label8.Text = "Patient nr:";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // comboBoxTrial
            // 
            this.comboBoxTrial.FormattingEnabled = true;
            this.comboBoxTrial.Location = new System.Drawing.Point(723, 80);
            this.comboBoxTrial.Name = "comboBoxTrial";
            this.comboBoxTrial.Size = new System.Drawing.Size(163, 24);
            this.comboBoxTrial.TabIndex = 10;
            this.comboBoxTrial.SelectedIndexChanged += new System.EventHandler(this.comboBoxTrial_SelectedIndexChanged);
            this.comboBoxTrial.Click += new System.EventHandler(this.comboBoxTrial_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 10F);
            this.label6.Location = new System.Drawing.Point(235, 35);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 16);
            this.label6.TabIndex = 9;
            this.label6.Text = "Client:";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // comboBoxClient
            // 
            this.comboBoxClient.FormattingEnabled = true;
            this.comboBoxClient.Location = new System.Drawing.Point(286, 30);
            this.comboBoxClient.Name = "comboBoxClient";
            this.comboBoxClient.Size = new System.Drawing.Size(163, 24);
            this.comboBoxClient.TabIndex = 8;
            // 
            // labelPatientName
            // 
            this.labelPatientName.AutoSize = true;
            this.labelPatientName.Location = new System.Drawing.Point(798, 8);
            this.labelPatientName.Name = "labelPatientName";
            this.labelPatientName.Size = new System.Drawing.Size(183, 16);
            this.labelPatientName.TabIndex = 5;
            this.labelPatientName.Text = "Patient name Last, mid, first";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 16);
            this.label3.TabIndex = 3;
            this.label3.Text = "Start date:";
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel9.Controls.Add(this.panel10);
            this.panel9.Controls.Add(this.panel8);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 157);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(1271, 35);
            this.panel9.TabIndex = 12;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.labelRecorderInfo);
            this.panel10.Controls.Add(this.label2);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel10.Location = new System.Drawing.Point(378, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(893, 35);
            this.panel10.TabIndex = 33;
            // 
            // labelRecorderInfo
            // 
            this.labelRecorderInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelRecorderInfo.Font = new System.Drawing.Font("Arial", 10F);
            this.labelRecorderInfo.Location = new System.Drawing.Point(0, 16);
            this.labelRecorderInfo.Name = "labelRecorderInfo";
            this.labelRecorderInfo.Size = new System.Drawing.Size(893, 16);
            this.labelRecorderInfo.TabIndex = 31;
            this.labelRecorderInfo.Text = "Recorder info: √= active, ○=out time, ≠ not assigned, 0123 devices \r\n";
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Arial", 10F);
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(893, 16);
            this.label2.TabIndex = 30;
            this.label2.Text = "Study Recording: +=Active, -= Stopped          Add study: a=active study, m=manua" +
    "l, u=unique id, i=patient id, f=find, y=...";
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.labelDeviceCount);
            this.panel8.Controls.Add(this.labelMaxEvents);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(375, 35);
            this.panel8.TabIndex = 32;
            // 
            // labelDeviceCount
            // 
            this.labelDeviceCount.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDeviceCount.Location = new System.Drawing.Point(0, 16);
            this.labelDeviceCount.Name = "labelDeviceCount";
            this.labelDeviceCount.Size = new System.Drawing.Size(375, 16);
            this.labelDeviceCount.TabIndex = 31;
            this.labelDeviceCount.Text = "^devices, xxx assigned, xxx available";
            // 
            // labelMaxEvents
            // 
            this.labelMaxEvents.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelMaxEvents.Location = new System.Drawing.Point(0, 0);
            this.labelMaxEvents.Name = "labelMaxEvents";
            this.labelMaxEvents.Size = new System.Drawing.Size(375, 16);
            this.labelMaxEvents.TabIndex = 30;
            this.labelMaxEvents.Text = "^^Max Study 234:  89786 Events";
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 10F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.SlateBlue;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column5,
            this.Column4,
            this.Column13,
            this.Column3,
            this.Column11,
            this.Column6,
            this.Column12,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10,
            this.StudyLabel,
            this.Column14,
            this.ColumnPermissions});
            this.dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView.Location = new System.Drawing.Point(0, 0);
            this.dataGridView.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            this.dataGridView.RowHeadersVisible = false;
            this.dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView.ShowEditingIcon = false;
            this.dataGridView.Size = new System.Drawing.Size(1271, 438);
            this.dataGridView.TabIndex = 3;
            this.dataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellClick);
            this.dataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellContentClick_1);
            this.dataGridView.CurrentCellChanged += new System.EventHandler(this.dataGridView_CurrentCellChanged);
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Column1.DefaultCellStyle = dataGridViewCellStyle2;
            this.Column1.HeaderText = "Study nr";
            this.Column1.MinimumWidth = 60;
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 60;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column2.HeaderText = "Start date";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 87;
            // 
            // Column5
            // 
            this.Column5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column5.HeaderText = "Patient name";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 106;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column4.HeaderText = "ICD-10";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 77;
            // 
            // Column13
            // 
            this.Column13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column13.HeaderText = "Client";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            this.Column13.Width = 69;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column3.HeaderText = "Recorder";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 92;
            // 
            // Column11
            // 
            this.Column11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.Column11.HeaderText = "Rec. Note (Client)";
            this.Column11.MinimumWidth = 96;
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.Width = 96;
            // 
            // Column6
            // 
            this.Column6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Column6.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column6.HeaderText = "Total events";
            this.Column6.MinimumWidth = 70;
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 70;
            // 
            // Column12
            // 
            this.Column12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Column12.DefaultCellStyle = dataGridViewCellStyle4;
            this.Column12.HeaderText = "Total Reports";
            this.Column12.MinimumWidth = 70;
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.Width = 70;
            // 
            // Column7
            // 
            this.Column7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Column7.DefaultCellStyle = dataGridViewCellStyle5;
            this.Column7.HeaderText = "End in Days";
            this.Column7.MinimumWidth = 60;
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 60;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "Analyzed";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Visible = false;
            this.Column8.Width = 90;
            // 
            // Column9
            // 
            this.Column9.HeaderText = "Last Report";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Visible = false;
            this.Column9.Width = 98;
            // 
            // Column10
            // 
            this.Column10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.Column10.HeaderText = "Report by";
            this.Column10.MinimumWidth = 60;
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Visible = false;
            // 
            // StudyLabel
            // 
            this.StudyLabel.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.StudyLabel.HeaderText = "Study Label";
            this.StudyLabel.Name = "StudyLabel";
            this.StudyLabel.ReadOnly = true;
            this.StudyLabel.Width = 99;
            // 
            // Column14
            // 
            this.Column14.HeaderText = "Trial";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            this.Column14.Width = 60;
            // 
            // ColumnPermissions
            // 
            this.ColumnPermissions.HeaderText = "Location";
            this.ColumnPermissions.Name = "ColumnPermissions";
            this.ColumnPermissions.ReadOnly = true;
            this.ColumnPermissions.Width = 87;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dataGridView);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 192);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1271, 438);
            this.panel2.TabIndex = 6;
            // 
            // saveFileDialogCsv
            // 
            this.saveFileDialogCsv.Filter = "*.csv|*.csv|*.*|*.*";
            // 
            // CStudyListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1271, 698);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Arial", 10F);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "CStudyListForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Study List";
            this.panel5.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonViewPdf;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonSelect;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label labelPatInfo;
        private System.Windows.Forms.Button buttonFindings;
        private System.Windows.Forms.Button buttonEndRecording;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label labelStudySearch;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.ComboBox comboBoxTrial;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBoxClient;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBoxRecorder;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DateTimePicker dateTimePickerStart;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.CheckBox checkBoxInactive;
        private System.Windows.Forms.TextBox textBoxStudy;
        private System.Windows.Forms.DateTimePicker dateTimePickerEnd;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxPatient;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labelPatientName;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Label labelPhysiscian;
        private System.Windows.Forms.ComboBox comboBoxPhysician;
        private System.Windows.Forms.Label labelSelected;
        private System.Windows.Forms.Label labelError;
        private System.Windows.Forms.CheckBox checkBoxAnonymize;
        private System.Windows.Forms.Label labelRowLimit;
        private System.Windows.Forms.CheckBox checkBoxUseLimit;
        private System.Windows.Forms.Label labelStudyPermissions;
        private System.Windows.Forms.CheckBox checkBoxStudyPermissions;
        private System.Windows.Forms.CheckBox checkBoxTrial;
        private System.Windows.Forms.ComboBox comboBoxLocation;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn StudyLabel;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnPermissions;
        private System.Windows.Forms.Label labelInfo;
        private System.Windows.Forms.Label labelUpdTime;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxActiveRecording;
        private System.Windows.Forms.CheckBox checkBoxActiveRecording;
        private System.Windows.Forms.Button buttonHolter;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label labelRecorderInfo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label labelDeviceCount;
        private System.Windows.Forms.Label labelMaxEvents;
        private System.Windows.Forms.Button buttonCsvExportLong;
        private System.Windows.Forms.CheckBox checkBoxEndsInDays;
        private System.Windows.Forms.TextBox textBoxEndsNrDays;
        private System.Windows.Forms.TextBox textBoxEndsInFrom;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxCmpOffset;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox comboBoxDeviceState;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button buttonCsvExportShort;
        private System.Windows.Forms.SaveFileDialog saveFileDialogCsv;
    }
}