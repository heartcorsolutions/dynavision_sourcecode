﻿using Event_Base;
using EventBoard.Event_Base;
using Program_Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventboardEntryForms
{
    public enum DStudyInfoVars
    {
        // primary keys
        // variables
        StudyState = 0,
        Client_IX,
        StudyStartDate,     // is used as primarykey for DSqlDataRow class
        HospitalRoom,
        HospitalBed,
        Physisian_IX,
        RefPhysisian_IX,
        StudyRecorder_IX,
        StudyMonitoringDays,
        StudyTypeCode,
        ReportIntervalSet,
        StudyPhysicianInstruction,
        StudyProcCodeSet,
        NrRecords,
        NrReports,
        StudyRemarkLabel,
        Patient_IX,
        BaseLineAnalysis_IX,
        // added 20170125
        MctIntervalSet,
        RecorderStartUTC,
        RecorderEndUTC,
        // added 20190225
        StudyPermissions,   // permissions bit mask (used for filtering records on site and operator skill grade
        Trial_IX,           // study belongs to trial


        NrSqlVars       // keep as last
    };

    public enum DStudyState
    {
        New = 0,
        Prepared,
        Started,
        Stopped,
        Reported,
        NrStudyStates
    }

    public enum DStudyPdfCode
    {
        Event, //   Single Event
        Day, // Day Study
        Week, //    Week Study
        Month, //   Month Study
        EOS, // End of Study
        MCT, // MCT Report
        Hour,
        Period,
        Monitoring
    }

    public class CStudyInfo : CSqlDataTableRow
    {
        public static bool _sbStudyCountRecStates = true;

        UInt16 _mStudyState;
        //public string _mStudyHospitalNameList;
        public UInt32 _mClient_IX;
        public DateTime _mStudyStartDate;
        public string _mHospitalRoom;
        public string _mHospitalBed;
        //public string _mStudyRefPhysicianList;
        public UInt32 _mPhysisian_IX;
        public UInt32 _mRefPhysisian_IX;
        //public string _mStudyPhysicianList;
        public UInt32 _mStudyRecorder_IX;
        //public string _mStudyRecorder;
        //public string _mStudySerialNumberList; nIndex_key
        //        public string _mStudyMonitoringDaysList;
        public UInt16 _mStudyMonitoringDays;
        public string _mStudyTypeCode;
        //public string _mStudyTypeList;
        public CStringSet _mReportIntervalSet;
        public string _mStudyPhysicianInstruction;  // the notes
        public CStringSet _mStudyProcCodeSet;
        public string _mStudyRemarkLabel;

        //        public string _mTempHospitalName;
        //       public string _mTempPhysician;

        public UInt16 _mNrRecords;
        public UInt16 _mNrReports;
        public UInt32 _mPatient_IX;             // record number of the DPatientInfo
        public UInt32 _mBaseLineAnalysis_IX;    // analysis report used for base line

        // 20170125
        public CStringSet _mMctIntervalSet;
        public DateTime _mRecorderStartUTC;    // device has been assigned within Start and End time
        public DateTime _mRecorderEndUTC;      // (used for slow lookup of study number
        // added 20190225
        public CBitSet64 _mStudyPermissions;   // permissions bit mask (used for filtering records on site and operator skill grade
        public UInt32 _mTrial_IX;           // study belongs to trial

        public CStudyInfo(CSqlDBaseConnection ASqlConnection) : base(ASqlConnection, "d_StudyInfo")
        {
            try
            {
                UInt64 maskPrimary = CSqlDataTableRow.sGetEmptyPrimairyMask(); // do not have a primary key, just use start date
                UInt64 maskValid = CSqlDataTableRow.sGetMaskRange((UInt16)0, (UInt16)DStudyInfoVars.NrSqlVars - 1);// mGetValidMask(false);

                mInitTableMasks(maskPrimary, maskValid);  // set primairyInsertFlags

                _mReportIntervalSet = new CStringSet();
                _mMctIntervalSet = new CStringSet();
                _mStudyProcCodeSet = new CStringSet();
                _mStudyPermissions = new CBitSet64();
                mClear();
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Init StudyInfo", ex);
            }
        }
        public override CSqlDataTableRow mCreateNewRow()
        {
            return new CStudyInfo(mGetSqlConnection());
        }
        public override bool mbCopyTo(CSqlDataTableRow ATo) // derived  public override void mbCopyTo() must call base.mbCopyTo(ATo)
        {
            bool bOk = false;

            if (ATo != null)
            {
                try
                {
                    CStudyInfo to = ATo as CStudyInfo;

                    if (to != null)
                    {
                        bOk = base.mbCopyTo(ATo);

                        to._mStudyState = _mStudyState;
                        to._mClient_IX = _mClient_IX;
                        to._mStudyStartDate = _mStudyStartDate;
                        to._mHospitalRoom = _mHospitalRoom;
                        to._mHospitalBed = _mHospitalBed;
                        to._mPhysisian_IX = _mPhysisian_IX;
                        to._mRefPhysisian_IX = _mRefPhysisian_IX;
                        to._mStudyRecorder_IX = _mStudyRecorder_IX;
                        to._mStudyMonitoringDays = _mStudyMonitoringDays;
                        to._mStudyTypeCode = _mStudyTypeCode;
                        _mReportIntervalSet.mbCopyTo(ref to._mReportIntervalSet);
                        to._mStudyRemarkLabel = _mStudyRemarkLabel;
                        to._mStudyPhysicianInstruction = _mStudyPhysicianInstruction;
                        _mStudyProcCodeSet.mbCopyTo(ref to._mStudyProcCodeSet);

                        to._mNrRecords = _mNrRecords;
                        to._mNrReports = _mNrReports;
                        to._mStudyRemarkLabel = _mStudyRemarkLabel;
                        to._mPatient_IX = _mPatient_IX;
                        to._mBaseLineAnalysis_IX = _mBaseLineAnalysis_IX;

                        // 20170125
                        to._mMctIntervalSet.mbCopyFrom(_mMctIntervalSet);
                        to._mRecorderStartUTC = _mRecorderStartUTC;
                        to._mRecorderEndUTC = _mRecorderEndUTC;      // (used for slow lookup of study number

                        // added 20190225
                        _mStudyPermissions.mCopyTo(ref to._mStudyPermissions);   // permissions bit mask (used for filtering records on site and operator skill grade
                        to._mTrial_IX = _mTrial_IX;           // study belongs to trial
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Study.CopyTo", ex);
                    bOk = false;
                }
            }
            return bOk;
        }
        public override void mClear()
        {
            base.mClear();
            _mStudyState = 0;
            _mClient_IX = 0;
            _mStudyStartDate = DateTime.MinValue;
            _mHospitalRoom = "";
            _mHospitalBed = "";
            _mPhysisian_IX = 0;
            _mRefPhysisian_IX = 0;
            _mStudyRecorder_IX = 0;
            _mStudyMonitoringDays = 0;
            _mStudyTypeCode = "";
            _mReportIntervalSet.mClear();
            _mStudyRemarkLabel = "";
            _mStudyPhysicianInstruction = "";
            _mStudyProcCodeSet.mClear();

            _mNrRecords = 0;
            _mNrReports = 0;
            _mStudyRemarkLabel = "";
            _mPatient_IX = 0;
            _mBaseLineAnalysis_IX = 0;

            // 20170125
            _mMctIntervalSet.mClear();
            _mRecorderStartUTC = DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Utc);
            _mRecorderEndUTC = _mRecorderStartUTC;      // (used for slow lookup of study number

            // added 20190225
            _mStudyPermissions.mClear();   // permissions bit mask (used for filtering records on site and operator skill grade
            _mTrial_IX = 0;           // study belongs to trial
        }
        public override DSqlDataType mVarGetSet(DSqlDataCmd ACmd, UInt16 AVarIndex, ref string AVarSqlName, ref string AStringValue, out bool ArbValid)
        {
            switch ((DStudyInfoVars)AVarIndex)
            {
                case DStudyInfoVars.StudyState:
                    return mSqlGetSetUInt16(ref _mStudyState, "StudyState", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DStudyInfoVars.Client_IX:
                    return mSqlGetSetUInt32(ref _mClient_IX, "Client_IX", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DStudyInfoVars.StudyStartDate:
                    return mSqlGetSetDate(ref _mStudyStartDate, "StudyStartDate", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DStudyInfoVars.HospitalRoom:
                    return mSqlGetSetString(ref _mHospitalRoom, "HospitalRoom", ACmd, ref AVarSqlName, ref AStringValue, 16, out ArbValid);
                case DStudyInfoVars.HospitalBed:
                    return mSqlGetSetString(ref _mHospitalBed, "HospitalBed", ACmd, ref AVarSqlName, ref AStringValue, 8, out ArbValid);
                case DStudyInfoVars.Physisian_IX:
                    return mSqlGetSetUInt32(ref _mPhysisian_IX, "Physisian_IX", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DStudyInfoVars.RefPhysisian_IX:
                    return mSqlGetSetUInt32(ref _mRefPhysisian_IX, "RefPhysisian_IX", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DStudyInfoVars.StudyRecorder_IX:
                    return mSqlGetSetUInt32(ref _mStudyRecorder_IX, "StudyRecorder_IX", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DStudyInfoVars.StudyMonitoringDays:
                    return mSqlGetSetUInt16(ref _mStudyMonitoringDays, "StudyMonitoringDays", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DStudyInfoVars.StudyTypeCode:
                    return mSqlGetSetString(ref _mStudyTypeCode, "StudyTypeCode", ACmd, ref AVarSqlName, ref AStringValue, 16, out ArbValid);
                case DStudyInfoVars.ReportIntervalSet:
                    return mSqlGetSetStringSet(ref _mReportIntervalSet, "ReportIntervalSet", ACmd, ref AVarSqlName, ref AStringValue, 64, out ArbValid);
                case DStudyInfoVars.StudyPhysicianInstruction:
                    return mSqlGetSetString(ref _mStudyPhysicianInstruction, "StudyPhysicianInstruction", ACmd, ref AVarSqlName, ref AStringValue, 30000, out ArbValid);
                case DStudyInfoVars.StudyProcCodeSet:
                    return mSqlGetSetStringSet(ref _mStudyProcCodeSet, "StudyProcCodeSet", ACmd, ref AVarSqlName, ref AStringValue, 64, out ArbValid);
                case DStudyInfoVars.NrRecords:
                    return mSqlGetSetUInt16(ref _mNrRecords, "NrRecords", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DStudyInfoVars.NrReports:
                    return mSqlGetSetUInt16(ref _mNrReports, "NrReports", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DStudyInfoVars.StudyRemarkLabel:
                    return mSqlGetSetString(ref _mStudyRemarkLabel, "StudyRemarkLabel", ACmd, ref AVarSqlName, ref AStringValue, 32, out ArbValid);
                case DStudyInfoVars.Patient_IX:
                    return mSqlGetSetUInt32(ref _mPatient_IX, "Patient_IX", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DStudyInfoVars.BaseLineAnalysis_IX:
                    return mSqlGetSetUInt32(ref _mBaseLineAnalysis_IX, "BaseLineAnalysis_IX", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);

                case DStudyInfoVars.MctIntervalSet:
                    return mSqlGetSetStringSet(ref _mMctIntervalSet, "MctIntervalSet", ACmd, ref AVarSqlName, ref AStringValue, 64, out ArbValid);
                case DStudyInfoVars.RecorderStartUTC:
                    return mSqlGetSetUTC(ref _mRecorderStartUTC, "RecorderStartUTC", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DStudyInfoVars.RecorderEndUTC:
                    return mSqlGetSetUTC(ref _mRecorderEndUTC, "RecorderEndUTC", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                // added 20190225
                case DStudyInfoVars.StudyPermissions:
                    return mSqlGetSetBitSet64(ref _mStudyPermissions, "StudyPermissions", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DStudyInfoVars.Trial_IX:
                    return mSqlGetSetUInt32(ref _mTrial_IX, "Trial_IX", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
            }
            return base.mVarGetSet(ACmd, AVarIndex, ref AVarSqlName, ref AStringValue, out ArbValid);
        }
        /*        public DSqlDataType mSqlGetSetStringSet(ref CStringSet ArVar, string AName, DSqlDataCmd ACmd, ref string ArSqlName, ref string ArStringValue, UInt16 ASizeLimit)
                {
                    DSqlDataType dataType = DSqlDataType.SqlError;

                    if (ArVar != null)
                    {
                        string s = ArVar.mGetSet();
                        dataType = mSqlGetSetString(ref s, AName, ACmd, ref ArSqlName, ref ArStringValue, ASizeLimit);
                        ArVar.mSetSet(s);
                    }
                    return dataType;
                }
        */
        static public string sGetStateString(UInt16 AState)
        {
            if (AState < (UInt16)DStudyState.NrStudyStates)
            {
                switch ((DStudyState)AState)
                {
                    case DStudyState.New: return "New";
                    case DStudyState.Prepared: return "Prepared";
                    case DStudyState.Started: return "Started";
                    case DStudyState.Stopped: return "Stopped";
                    case DStudyState.Reported: return "Reported";
                }
            }
            return "?StudyState";
        }

        public bool mbGetNextReportNr(out UInt16 ArNextReportNr)  // increases the report counter
        {
            UInt32 studyIX = mIndex_KEY;
            UInt16 nextReportNr = 0;
            bool bOk = false;

            try
            {
                if (studyIX == 0)
                {
                    bOk = true;
                    // when there is no study then the report number is always 0
                }
                else
                {
                    UInt64 reportNrMask = CSqlDataTableRow.sGetMask((UInt16)DStudyInfoVars.NrReports);

                    // reload study to get latest number (in case soneone did a study report in the mean time
                    if (mbDoSqlSelectIndex(studyIX, mGetValidMask(true)))
                    {
                        nextReportNr = (UInt16)(_mNrReports + 1);
                        bOk = nextReportNr < UInt16.MaxValue;
                        if (bOk)
                        {
                            _mNrReports = nextReportNr;
                            bOk = mbDoSqlUpdate(reportNrMask, true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Get Next report Nr", ex);
            }
            ArNextReportNr = nextReportNr;
            return bOk;
        }

        /*        public bool mbCheckNextReportNr(UInt16 ANextReportNr)
                {
                    bool bOk = false;

                    if (mIndex_KEY == 0)
                    {
                        // no study so it should be 0
                        if (ANextReportNr == 0)
                        {
                            bOk = true;
                        }
                        else
                        {
                            CProgram.sLogError("Next Report while study is not present");
                        }
                    }
                    else
                    {
                        if (ANextReportNr == _mNrReports + 1)
                        {

                        }
                    }

                        return bOk;
                }
        */
        public bool mbSetNextReportNr(UInt16 ANextReportNr)  // should do locking
        {
            UInt32 studyIX = mIndex_KEY;
            bool bOk = false;

            try
            {
                if (studyIX == 0)
                {
                    // when there is no study then the report number is always 0
                    if (ANextReportNr == 0)
                    {
                        bOk = true;
                    }
                    else
                    {
                        CProgram.sLogError("Invalid Report number while study is not present");
                    }
                }
                else
                {
                    bOk = true; // Nr Report is increased at getNextReportNr()
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Set Next report Nr", ex);
            }
            return bOk;
        }

        public bool mbReverseNextReportNr(UInt16 ANextReportNr)  // call when an error occurse
        {
            UInt32 studyIX = mIndex_KEY;
            bool bOk = false;

            try
            {
                if (studyIX == 0)
                {
                    // when there is no study then the report number is always 0
                    if (ANextReportNr == 0)
                    {
                        bOk = true;
                    }
                    else
                    {
                        CProgram.sLogError("Invalid Report number while study is not present");
                    }
                }
                else if (ANextReportNr > 0 && ANextReportNr < UInt16.MaxValue)
                {
                    CStudyInfo si = new CStudyInfo(mGetSqlConnection());
                    UInt64 reportNrMask = CSqlDataTableRow.sGetMask((UInt16)DStudyInfoVars.NrReports);

                    if (si != null)
                    {
                        if (si.mbDoSqlSelectIndex(studyIX, mGetValidMask(true)))
                        {
                            if (si._mNrReports == ANextReportNr)
                            {
                                --si._mNrReports;

                                 bOk = si.mbDoSqlUpdate(reportNrMask, true);
                                CProgram.sLogError("Reverse Report number " + ANextReportNr.ToString() + " for study " + studyIX.ToString());
                            }
                        }
                     }
                 }
                else
                {
                    CProgram.sLogError("Invalid Report number " + ANextReportNr.ToString() + " for study " + studyIX.ToString());
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Reverse Next report Nr", ex);
            }
            return bOk;
        }

        public bool mbSetNrReports(UInt16 ArNrReports)  // optional to call when an error occurse
        {
            UInt32 studyIX = mIndex_KEY;
            bool bOk = false;

            try
            {
                if (studyIX == 0)
                {
                    CProgram.sLogError("No study present");
                }
                else 
                {
                    CStudyInfo si = new CStudyInfo(mGetSqlConnection());
                    UInt64 reportNrMask = CSqlDataTableRow.sGetMask((UInt16)DStudyInfoVars.NrReports);

                    if (si != null)
                    {
                        if (si.mbDoSqlSelectIndex(studyIX, mGetValidMask(true)))
                        {
                            UInt16 oldNrReports = si._mNrReports;

                            if (oldNrReports == ArNrReports)
                            {
                                CProgram.sLogLine("Nr Reports " + ArNrReports.ToString() + " same for study " + studyIX.ToString());
                                bOk = true;
                            }
                            else
                            {
                                si._mNrReports = ArNrReports;
                                bOk = si.mbDoSqlUpdate(reportNrMask, true); // update quickly to prevent counter access duplicity

                                if (false == bOk)
                                {
                                    CProgram.sLogError("Update Nr Reports " + oldNrReports.ToString() + " set to " + ArNrReports.ToString()
                                        + " failed for study " + studyIX.ToString());
                                }
                                else if (ArNrReports > oldNrReports)
                                {
                                    CProgram.sLogLine("Nr Reports " + oldNrReports.ToString() + " set to " + ArNrReports.ToString()
                                        + (ArNrReports - oldNrReports).ToString() + " extra reports for study " + studyIX.ToString());
                                }
                                else
                                {
                                    CProgram.sLogLine("Nr Reports " + oldNrReports.ToString() + " set to " + ArNrReports.ToString()
                                        + (oldNrReports - ArNrReports).ToString() + " missing reports for study " + studyIX.ToString());
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Set Nr reports failed for study " + studyIX, ex);
            }
            return bOk;
        }
        public string mGetLabelNoteMark()
        {
           string s = "";

            if(_mStudyRemarkLabel != null && _mStudyRemarkLabel.Length > 1 )
            {
                s += "sr";
            }
            if( _mStudyPhysicianInstruction != null && _mStudyPhysicianInstruction.Length > 0)
            {
                s += "\r\nsn";
            }
            return s;
        }
        
        public string mGetRemarkAndNote()
        {
            string s = _mStudyRemarkLabel + "\r\n" + _mStudyPhysicianInstruction;

            return s;
        }

        public static string sMergeCodeLabelQC( string ACodeLabel, bool AbQC)
        {
            return AbQC ? "QC-" + ACodeLabel : ACodeLabel;
        }

        public static string sGetStudyPdfCode(DStudyPdfCode APdfCode, bool AbQC)
        {
            string s = "?" + APdfCode.ToString();
            switch (APdfCode)
            {
                case DStudyPdfCode.Event: s = "Event";           // RefID = Study == 0 ? Record number : Analysis number or record sub nr
                    break;
                case DStudyPdfCode.Day: s = "Day";               // RefID = StudyReport number
                    break;
                case DStudyPdfCode.Week: s = "Week";             // RefID = StudyReport number
                    break;
                case DStudyPdfCode.Month: s = "Month";           // RefID = StudyReport number
                    break;
                case DStudyPdfCode.EOS: s = "EOS";               // RefID = StudyReport number
                    break;
                case DStudyPdfCode.MCT: s = "MCT";              // RefID = 0 
                    break;
                case DStudyPdfCode.Hour: s = "Hour";             // RefID = StudyReport number
                    break;
                case DStudyPdfCode.Period: s = "Period";         // RefID = StudyReport number
                    break;
            }
            return sMergeCodeLabelQC(s, AbQC);
        }

        public List<CSqlCountItem> mGetRecStatesList(out UInt32 ArTotalCount)
        {
            List<CSqlCountItem> list = null;
            UInt32 n = 0;

            if (mIndex_KEY > 0)
            {
                bool bError;
                CRecordMit rec = new CRecordMit(CDvtmsData.sGetDBaseConnection());

                if (rec != null)
                {
                    UInt64 flags = CSqlDataTableRow.sGetMask((UInt16)DRecordMitVars.Study_IX);
                    UInt16 varNr = (UInt16)DRecordMitVars.RecState_IX;

                    rec.mStudy_IX = mIndex_KEY;

                    if (false == rec.mbDoSqlCountList(out bError, out n, out list, varNr, flags, true, ""))
                    {
                            list = null;
                    }
                }

            }
            ArTotalCount = n;

            return list;
        }
        public string mGetRecStatesAll(out UInt32 ArTotalCount, out UInt32 ArToDoCount, bool AbForceCount)
        {
            string recInfo = "";
            UInt32 n = 0;
            UInt32 nToDo = 0;

            if (_sbStudyCountRecStates || AbForceCount)
            {
                List<CSqlCountItem> list = mGetRecStatesList(out n);

                if (list != null)
                {
                    int nList = list.Count;

                    recInfo += n.ToString();
                    if (n != _mNrRecords)
                    {
                        recInfo += "<>" + _mNrRecords.ToString();
                    }
                    recInfo += ": ";

                    for (int i = 0; i < nList; ++i)
                    {
                        CSqlCountItem item = list[i];
                        UInt16 var = 0;

                        if (i > 0) recInfo += ", ";

                        if (item.mbGetVarUInt16(out var) && var <= (UInt16)DRecState.NrRecStates)
                        {
                            DRecState state = (DRecState)var;
                            recInfo += state.ToString();
                            if (state <= DRecState.Processed || state == DRecState.Triaged)
                            {
                                nToDo += item._mCount;
                            }
                        }
                        else
                        {
                            recInfo += "?" + item._mVarValue;
                        }
                        recInfo += "= " + item._mCount.ToString();
                    }
                    if (nToDo != 0)
                    {
                        recInfo = nToDo.ToString() + "->" + recInfo;
                    }
                }
            }
            else
            {
                recInfo = _mNrRecords.ToString() + " events: not counted!";
            }
            ArTotalCount = n;
            ArToDoCount = nToDo;
            return recInfo;
        }
    }
}
