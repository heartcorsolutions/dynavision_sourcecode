﻿using Event_Base;
using Program_Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventboardEntryForms
{
    public enum DClientInfoVars
    {
        // primary keys
        Label = 0,
        // variables
        FullName,
        Address,
        ZipCode,
        HouseNr,
        State,
        City,
        CountryCode,
        CentralTelNr,
        DirectTelNr,
        FaxNr,
        Email,
        DepartmentName,
        DepartmentHead,
        DepartmentTelNr,

        NrSqlVars       // keep as last
    };

    public class CClientInfo : CSqlDataTableRow
    {
        public string _mLabel;
        public string _mFullName;
        public string _mAddress;
        public string _mZipCode;
        public string _mHouseNr;
        public string _mState;
        public string _mCity;
        public string _mCountryCode;
        public string _mCentralTelNr;
        public string _mDirectTelNr;
        public string _mFaxNr;
        public string _mEmail;
        public string _mDepartmentName;
        public string _mDepartmentHead;
        public string _mDepartmentTelNr;
     
        public CClientInfo(CSqlDBaseConnection ASqlConnection):base( ASqlConnection, "d_ClientInfo")
        {
            try
            {
                UInt64 maskPrimary = CSqlDataTableRow.sGetMaskRange((UInt16)DClientInfoVars.Label, (UInt16)DClientInfoVars.Label);
                UInt64 maskValid = CSqlDataTableRow.sGetMaskRange((UInt16)0, (UInt16)DClientInfoVars.NrSqlVars - 1);// mGetValidMask(false);

                mInitTableMasks(maskPrimary, maskValid);  // set primairyInsertFlags

                 mClear();
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Init form", ex);
            }
        }
        public override CSqlDataTableRow mCreateNewRow()
        {
            return new CClientInfo(mGetSqlConnection());
        }
        public override bool mbCopyTo(CSqlDataTableRow ATo) // derived  public override void mbCopyTo() must call base.mbCopyTo(ATo)
        {
            bool bOk = false;

            if (ATo != null)
            {
                CClientInfo to = ATo as CClientInfo;

                if (to != null)
                {
                    bOk = base.mbCopyTo(ATo);
                    to._mLabel = _mLabel;
                    to._mFullName = _mFullName;
                    to._mAddress = _mAddress;
                    to._mZipCode = _mZipCode;
                    to._mHouseNr = _mHouseNr;
                    to._mState = _mState;
                    to._mCity = _mCity;
                    to._mCountryCode = _mCountryCode;
                    to._mCentralTelNr = _mCentralTelNr;
                    to._mDirectTelNr = _mDirectTelNr;
                    to._mFaxNr = _mFaxNr;
                    to._mEmail = _mEmail;
                    to._mDepartmentName = _mDepartmentName;
                    to._mDepartmentHead = _mDepartmentHead;
                    to._mDepartmentTelNr = _mDepartmentTelNr;
                }
            }
            return bOk;
        }
        public override void mClear()
        {
            base.mClear();
            _mLabel = "";
            _mFullName = "";
            _mAddress = "";
            _mZipCode = "";
            _mHouseNr = "";
            _mState = "";
            _mCity = "";
            _mCountryCode = "";
            _mCentralTelNr = "";
            _mDirectTelNr = "";
            _mFaxNr = "";
            _mEmail = "";
            _mDepartmentName = "";
            _mDepartmentHead = "";
            _mDepartmentTelNr = "";
        }
        public override DSqlDataType mVarGetSet(DSqlDataCmd ACmd, UInt16 AVarIndex, ref string AVarSqlName, ref string AStringValue, out bool ArbValid)
        {
            switch ((DClientInfoVars)AVarIndex)
            {
                // primary keys
                case DClientInfoVars.Label:
                    return mSqlGetSetString(ref _mLabel, "Label", ACmd, ref AVarSqlName, ref AStringValue, 32, out ArbValid);
                // variables
                case DClientInfoVars.FullName:
                    return mSqlGetSetString(ref _mFullName, "FullName", ACmd, ref AVarSqlName, ref AStringValue, 64, out ArbValid);
                case DClientInfoVars.Address:
                    return mSqlGetSetString(ref _mAddress, "Address", ACmd, ref AVarSqlName, ref AStringValue, 1024, out ArbValid);
                case DClientInfoVars.ZipCode:
                    return mSqlGetSetString(ref _mZipCode, "ZipCode", ACmd, ref AVarSqlName, ref AStringValue, 16, out ArbValid);
                case DClientInfoVars.HouseNr:
                    return mSqlGetSetString(ref _mHouseNr, "HouseNr", ACmd, ref AVarSqlName, ref AStringValue, 16, out ArbValid);
                case DClientInfoVars.State:
                    return mSqlGetSetString(ref _mState, "State", ACmd, ref AVarSqlName, ref AStringValue, 48, out ArbValid);
                case DClientInfoVars.City:
                    return mSqlGetSetString(ref _mCity, "City", ACmd, ref AVarSqlName, ref AStringValue, 48, out ArbValid);
                case DClientInfoVars.CountryCode:
                    return mSqlGetSetString(ref _mCountryCode, "CountryCode", ACmd, ref AVarSqlName, ref AStringValue, 4, out ArbValid);
                case DClientInfoVars.CentralTelNr:
                    return mSqlGetSetString(ref _mCentralTelNr, "CentralTelNr", ACmd, ref AVarSqlName, ref AStringValue, 32, out ArbValid);
                case DClientInfoVars.DirectTelNr:
                    return mSqlGetSetString(ref _mDirectTelNr, "DirectTelNr", ACmd, ref AVarSqlName, ref AStringValue, 32, out ArbValid);
                case DClientInfoVars.FaxNr:
                    return mSqlGetSetString(ref _mFaxNr, "FaxNr", ACmd, ref AVarSqlName, ref AStringValue, 32, out ArbValid);
                case DClientInfoVars.Email:
                    return mSqlGetSetString(ref _mEmail, "Email", ACmd, ref AVarSqlName, ref AStringValue, 128, out ArbValid);
                case DClientInfoVars.DepartmentName:
                    return mSqlGetSetString(ref _mDepartmentName, "DepartmentName", ACmd, ref AVarSqlName, ref AStringValue, 64, out ArbValid);
                case DClientInfoVars.DepartmentHead:
                    return mSqlGetSetString(ref _mDepartmentHead, "DepartmentHead", ACmd, ref AVarSqlName, ref AStringValue, 64, out ArbValid);
                case DClientInfoVars.DepartmentTelNr:
                    return mSqlGetSetString(ref _mDepartmentTelNr, "DepartmentTelNr", ACmd, ref AVarSqlName, ref AStringValue, 32, out ArbValid);
            }
            return base.mVarGetSet(ACmd, AVarIndex, ref AVarSqlName, ref AStringValue, out ArbValid);
        }
        public static void sFillPrintClientName(bool AbAnonymize, CStudyInfo AStudy, out string ArText1, out string ArText2, bool AbAddP)
        {
            string text1 = "";
            string text2 = "";

            if (false == AbAnonymize && AStudy != null && AStudy._mClient_IX > 0)
            {
                CClientInfo client = new CClientInfo(AStudy.mGetSqlConnection());

                if (client != null)
                {
                    client.mbDoSqlSelectIndex(AStudy._mClient_IX, client.mGetValidMask(true));// CSqlDataTableRow.sGetMask((UInt16)DClientInfoVars.Label));
                    text1 = client._mFullName;// _mLabel;
                    if (text1 == null || text1.Length == 0)
                    {
                        text1 = client._mLabel;
                    }
                    text2 = client._mDepartmentTelNr;
                    if (text2 != null && text2.Length > 0 && AbAddP)
                    {
                        text2 = "P:  " + text2;
                    }
                }
            }
            ArText1 = text1;
            ArText2 = text2;
        }
        public static void sFillPrintClientName(bool AbAnonymize, CStudyInfo AStudy, Label ALabel1, Label ALabel2, bool AbAddP)
        {
            string text1, text2;

            sFillPrintClientName(AbAnonymize, AStudy, out text1, out text2, AbAddP);

            if (ALabel1 != null) ALabel1.Text = text1;
            if (ALabel2 != null) ALabel2.Text = text2;
        }
        public static string sGetClientLabel(UInt32 AClient_IX, bool AbAddNr, string AEmptyText)
        {
            string s = "";

            if (AClient_IX > 0)
            {
                if (AbAddNr)
                {
                    s += "C" + AClient_IX.ToString() + "_";
                }
                CClientInfo info = new CClientInfo(CDvtmsData.sGetDBaseConnection());

                if (info != null)
                {
                    if (info.mbDoSqlSelectIndex(AClient_IX, info.mMaskAll))
                    {
                        s += info._mLabel;
                    }
                    else
                    {
                        s += "?" + AClient_IX.ToString();
                    }
                }
            }
            else
            {
                s = AEmptyText;
            }
            return s;
        }

    }
}
