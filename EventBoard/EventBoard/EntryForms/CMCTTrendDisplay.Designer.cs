﻿namespace EventBoard
{
    partial class CMCTTrendDisplay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CMCTTrendDisplay));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint1 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint2 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint3 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint4 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 0D);
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint5 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint6 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint7 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint8 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 0D);
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint9 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint10 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint11 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint12 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 0D);
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series7 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint13 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint14 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint15 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint16 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 0D);
            System.Windows.Forms.DataVisualization.Charting.Series series8 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series9 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint17 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint18 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint19 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint20 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 0D);
            System.Windows.Forms.DataVisualization.Charting.Series series10 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series11 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint21 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint22 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint23 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint24 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 0D);
            System.Windows.Forms.DataVisualization.Charting.Series series12 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea7 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series13 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint25 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint26 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint27 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint28 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 0D);
            System.Windows.Forms.DataVisualization.Charting.Series series14 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea8 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series15 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint29 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint30 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint31 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint32 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 0D);
            System.Windows.Forms.DataVisualization.Charting.Series series16 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea9 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series17 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint33 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint34 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint35 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint36 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 0D);
            System.Windows.Forms.DataVisualization.Charting.Series series18 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea10 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series19 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint37 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint38 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint39 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint40 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 0D);
            System.Windows.Forms.DataVisualization.Charting.Series series20 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea11 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series21 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint41 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint42 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint43 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint44 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 0D);
            System.Windows.Forms.DataVisualization.Charting.Series series22 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea12 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series23 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint45 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint46 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint47 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint48 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 0D);
            System.Windows.Forms.DataVisualization.Charting.Series series24 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea13 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series25 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint49 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint50 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint51 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint52 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 0D);
            System.Windows.Forms.DataVisualization.Charting.Series series26 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea14 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.CustomLabel customLabel1 = new System.Windows.Forms.DataVisualization.Charting.CustomLabel();
            System.Windows.Forms.DataVisualization.Charting.Series series27 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series28 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series29 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint53 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(125D, 175D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint54 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(150D, 180D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint55 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(175D, 225D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint56 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(190D, 140D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint57 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(210D, 150D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint58 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(225D, 135D);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea15 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series30 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint59 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 30D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint60 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 60D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint61 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 14D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint62 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 30D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint63 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(5D, 23D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint64 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(6D, 8D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint65 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(7D, 24D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint66 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(8D, 4D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint67 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(9D, 33D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint68 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(10D, 10D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint69 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(11D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint70 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(12D, 7D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint71 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(13D, 7D);
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelTimeZone = new System.Windows.Forms.Label();
            this.labelUseExluded = new System.Windows.Forms.Label();
            this.checkBoxQCD = new System.Windows.Forms.CheckBox();
            this.buttonQueryh00 = new System.Windows.Forms.Button();
            this.buttonQuery1h = new System.Windows.Forms.Button();
            this.buttonxh00 = new System.Windows.Forms.Button();
            this.buttonQueryTrend = new System.Windows.Forms.Button();
            this.checkBoxPrintDur = new System.Windows.Forms.CheckBox();
            this.checkBoxPrintStat = new System.Windows.Forms.CheckBox();
            this.labelAnalysisUse = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.labelRecorderID = new System.Windows.Forms.Label();
            this.radioButton12hours = new System.Windows.Forms.RadioButton();
            this.radioButton6hours = new System.Windows.Forms.RadioButton();
            this.buttonCopyFrom = new System.Windows.Forms.Button();
            this.labelQueryInfo = new System.Windows.Forms.Label();
            this.labelMinutes = new System.Windows.Forms.Label();
            this.textBoxLength = new System.Windows.Forms.TextBox();
            this.labelLength = new System.Windows.Forms.Label();
            this.textBoxStartMinute = new System.Windows.Forms.TextBox();
            this.comboBoxStart = new System.Windows.Forms.ComboBox();
            this.textBoxStartHour = new System.Windows.Forms.TextBox();
            this.dateTimeStart = new System.Windows.Forms.DateTimePicker();
            this.labelStart = new System.Windows.Forms.Label();
            this.button0h00 = new System.Windows.Forms.Button();
            this.buttonP24 = new System.Windows.Forms.Button();
            this.buttonP6 = new System.Windows.Forms.Button();
            this.buttonP1 = new System.Windows.Forms.Button();
            this.buttonM1 = new System.Windows.Forms.Button();
            this.buttonM6 = new System.Windows.Forms.Button();
            this.buttonM24 = new System.Windows.Forms.Button();
            this.radioButtonAll = new System.Windows.Forms.RadioButton();
            this.radioButtonDay = new System.Windows.Forms.RadioButton();
            this.buttonSetTimeFrame = new System.Windows.Forms.Button();
            this.textBoxToMin = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.comboBoxToAM = new System.Windows.Forms.ComboBox();
            this.textBoxToHour = new System.Windows.Forms.TextBox();
            this.dateTimePickerTo = new System.Windows.Forms.DateTimePicker();
            this.textBoxFromMin = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.comboBoxFromAM = new System.Windows.Forms.ComboBox();
            this.textBoxFromHour = new System.Windows.Forms.TextBox();
            this.dateTimePickerFrom = new System.Windows.Forms.DateTimePicker();
            this.buttonQueryECG = new System.Windows.Forms.Button();
            this.labelTo = new System.Windows.Forms.Label();
            this.labelFrom = new System.Windows.Forms.Label();
            this.radioButtonMonth = new System.Windows.Forms.RadioButton();
            this.radioButtonPeriod = new System.Windows.Forms.RadioButton();
            this.radioButtonWeek = new System.Windows.Forms.RadioButton();
            this.radioButtonHour = new System.Windows.Forms.RadioButton();
            this.panel13 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.labelNow = new System.Windows.Forms.Label();
            this.labelDaysHours = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.labelTrendTime = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.labelTrendEnd = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.labelTrendStart = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.labelNearSec = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel46 = new System.Windows.Forms.Panel();
            this.labelPatName = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.labelPatID = new System.Windows.Forms.Label();
            this.labelPatIDHeader = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelDoB = new System.Windows.Forms.Label();
            this.labelAgeGender = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.labelStartDate = new System.Windows.Forms.Label();
            this.labelStudyNote = new System.Windows.Forms.Label();
            this.labelStudy = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.labelColon = new System.Windows.Forms.Label();
            this.panel45 = new System.Windows.Forms.Panel();
            this.labelRunState = new System.Windows.Forms.Label();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.panel34 = new System.Windows.Forms.Panel();
            this.panel58 = new System.Windows.Forms.Panel();
            this.panel27 = new System.Windows.Forms.Panel();
            this.labelMouseInfo = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.labelUpodateTime = new System.Windows.Forms.Label();
            this.labelRecAnalysis = new System.Windows.Forms.Label();
            this.panel59 = new System.Windows.Forms.Panel();
            this.labelFoundRec = new System.Windows.Forms.Label();
            this.labelPrintResult = new System.Windows.Forms.Label();
            this.checkBoxAnonymize = new System.Windows.Forms.CheckBox();
            this.checkBoxBlackWhite = new System.Windows.Forms.CheckBox();
            this.checkBoxValidated = new System.Windows.Forms.CheckBox();
            this.labelPrint = new System.Windows.Forms.Label();
            this.checkBoxLogCalc = new System.Windows.Forms.CheckBox();
            this.panel28 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.checkBoxCountAnOnly = new System.Windows.Forms.CheckBox();
            this.checkBoxUseAnalysis = new System.Windows.Forms.CheckBox();
            this.checkBoxUseTrend = new System.Windows.Forms.CheckBox();
            this.checkBoxPulseOnOnly = new System.Windows.Forms.CheckBox();
            this.checkBoxTogleAfOnly = new System.Windows.Forms.CheckBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.labelFDL = new System.Windows.Forms.Label();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel60 = new System.Windows.Forms.Panel();
            this.chartAllEvents = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel61 = new System.Windows.Forms.Panel();
            this.label25 = new System.Windows.Forms.Label();
            this.panel55 = new System.Windows.Forms.Panel();
            this.chartManual = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel56 = new System.Windows.Forms.Panel();
            this.label54 = new System.Windows.Forms.Label();
            this.panel36 = new System.Windows.Forms.Panel();
            this.chartOther = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel37 = new System.Windows.Forms.Panel();
            this.label42 = new System.Windows.Forms.Label();
            this.panel19 = new System.Windows.Forms.Panel();
            this.chartAbN = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel20 = new System.Windows.Forms.Panel();
            this.label67 = new System.Windows.Forms.Label();
            this.panel33 = new System.Windows.Forms.Panel();
            this.chartVtach = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel35 = new System.Windows.Forms.Panel();
            this.label39 = new System.Windows.Forms.Label();
            this.panel53 = new System.Windows.Forms.Panel();
            this.chartPace = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel54 = new System.Windows.Forms.Panel();
            this.label38 = new System.Windows.Forms.Label();
            this.panel29 = new System.Windows.Forms.Panel();
            this.chartPVC = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel30 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.chartPAC = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel31 = new System.Windows.Forms.Panel();
            this.chartAflut = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel32 = new System.Windows.Forms.Panel();
            this.label30 = new System.Windows.Forms.Label();
            this.panel51 = new System.Windows.Forms.Panel();
            this.chartAfib = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel52 = new System.Windows.Forms.Panel();
            this.label36 = new System.Windows.Forms.Label();
            this.panel49 = new System.Windows.Forms.Panel();
            this.chartPause = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel50 = new System.Windows.Forms.Panel();
            this.label34 = new System.Windows.Forms.Label();
            this.panel44 = new System.Windows.Forms.Panel();
            this.chartBrady = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel48 = new System.Windows.Forms.Panel();
            this.label32 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.chartTachy = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel43 = new System.Windows.Forms.Panel();
            this.label53 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.chartHR = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label65 = new System.Windows.Forms.Label();
            this.labelHrGap = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel39 = new System.Windows.Forms.Panel();
            this.panel21 = new System.Windows.Forms.Panel();
            this.panel26 = new System.Windows.Forms.Panel();
            this.labelPerMan = new System.Windows.Forms.Label();
            this.labelPerTotal = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.labelPerPace = new System.Windows.Forms.Label();
            this.labelPerPVC = new System.Windows.Forms.Label();
            this.labelPerPAC = new System.Windows.Forms.Label();
            this.labelPerAfib = new System.Windows.Forms.Label();
            this.labelPerPause = new System.Windows.Forms.Label();
            this.labelPerBrady = new System.Windows.Forms.Label();
            this.labelPerTachy = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.panel25 = new System.Windows.Forms.Panel();
            this.labelDurDevice = new System.Windows.Forms.Label();
            this.labelDurMan = new System.Windows.Forms.Label();
            this.labelDurTotal = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.labelDurPace = new System.Windows.Forms.Label();
            this.labelDurPVC = new System.Windows.Forms.Label();
            this.labelDurPAC = new System.Windows.Forms.Label();
            this.labelDurAfib = new System.Windows.Forms.Label();
            this.labelDurPause = new System.Windows.Forms.Label();
            this.labelDurBrady = new System.Windows.Forms.Label();
            this.labelDurTachy = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.panel24 = new System.Windows.Forms.Panel();
            this.labelPlotSec = new System.Windows.Forms.Label();
            this.labelUser = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.labelEpMan = new System.Windows.Forms.Label();
            this.labelEpTotal = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.labelEpPace = new System.Windows.Forms.Label();
            this.labelEpPVC = new System.Windows.Forms.Label();
            this.labelEpPAC = new System.Windows.Forms.Label();
            this.labelEpAfib = new System.Windows.Forms.Label();
            this.labelEpPause = new System.Windows.Forms.Label();
            this.labelEpBrady = new System.Windows.Forms.Label();
            this.labelEpTachy = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.panel23 = new System.Windows.Forms.Panel();
            this.label57 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panel42 = new System.Windows.Forms.Panel();
            this.panel22 = new System.Windows.Forms.Panel();
            this.chartSummaryBar = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.panel16 = new System.Windows.Forms.Panel();
            this.label45 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.labelPerTotalVal = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.labelPerAbnVal = new System.Windows.Forms.Label();
            this.labelPerNormVal = new System.Windows.Forms.Label();
            this.labelPerOtherVal = new System.Windows.Forms.Label();
            this.labelPerVtachVal = new System.Windows.Forms.Label();
            this.labelPerPaceVal = new System.Windows.Forms.Label();
            this.labelPerPVCVal = new System.Windows.Forms.Label();
            this.labelPerPACVal = new System.Windows.Forms.Label();
            this.labelPerAflutVal = new System.Windows.Forms.Label();
            this.labelPerAfibVal = new System.Windows.Forms.Label();
            this.labelPerPauseVal = new System.Windows.Forms.Label();
            this.labelPerBradyVal = new System.Windows.Forms.Label();
            this.labelPerTachyVal = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.panel40 = new System.Windows.Forms.Panel();
            this.labelDurManVal = new System.Windows.Forms.Label();
            this.labelDurTotalVal = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.labelDurAbnVal = new System.Windows.Forms.Label();
            this.labelDurNormVal = new System.Windows.Forms.Label();
            this.labelDurOtherVal = new System.Windows.Forms.Label();
            this.labelDurVtachVal = new System.Windows.Forms.Label();
            this.labelDurPaceVal = new System.Windows.Forms.Label();
            this.labelDurPVCVal = new System.Windows.Forms.Label();
            this.labelDurPACVal = new System.Windows.Forms.Label();
            this.labelDurAflutVal = new System.Windows.Forms.Label();
            this.labelDurAfibVal = new System.Windows.Forms.Label();
            this.labelDurPauseVal = new System.Windows.Forms.Label();
            this.labelDurBradyVal = new System.Windows.Forms.Label();
            this.labelDurTachyVal = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.panel41 = new System.Windows.Forms.Panel();
            this.labelEpManVal = new System.Windows.Forms.Label();
            this.labelEpTotalVal = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.labelEpAbnVal = new System.Windows.Forms.Label();
            this.labelEpNormVal = new System.Windows.Forms.Label();
            this.labelEpOtherVal = new System.Windows.Forms.Label();
            this.labelEpVtachVal = new System.Windows.Forms.Label();
            this.labelEpPaceVal = new System.Windows.Forms.Label();
            this.labelEpPVCVal = new System.Windows.Forms.Label();
            this.labelEpPACVal = new System.Windows.Forms.Label();
            this.labelEpAflutVal = new System.Windows.Forms.Label();
            this.labelEpAfibVal = new System.Windows.Forms.Label();
            this.labelEpPauseVal = new System.Windows.Forms.Label();
            this.labelEpBradyVal = new System.Windows.Forms.Label();
            this.labelEpTachyVal = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.panel57 = new System.Windows.Forms.Panel();
            this.label82 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel46.SuspendLayout();
            this.panel45.SuspendLayout();
            this.panel34.SuspendLayout();
            this.panel58.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel59.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel60.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartAllEvents)).BeginInit();
            this.panel61.SuspendLayout();
            this.panel55.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartManual)).BeginInit();
            this.panel56.SuspendLayout();
            this.panel36.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartOther)).BeginInit();
            this.panel37.SuspendLayout();
            this.panel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartAbN)).BeginInit();
            this.panel20.SuspendLayout();
            this.panel33.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartVtach)).BeginInit();
            this.panel35.SuspendLayout();
            this.panel53.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartPace)).BeginInit();
            this.panel54.SuspendLayout();
            this.panel29.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartPVC)).BeginInit();
            this.panel30.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartPAC)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel31.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartAflut)).BeginInit();
            this.panel32.SuspendLayout();
            this.panel51.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartAfib)).BeginInit();
            this.panel52.SuspendLayout();
            this.panel49.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartPause)).BeginInit();
            this.panel50.SuspendLayout();
            this.panel44.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartBrady)).BeginInit();
            this.panel48.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartTachy)).BeginInit();
            this.panel43.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartHR)).BeginInit();
            this.panel12.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel39.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel26.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartSummaryBar)).BeginInit();
            this.panel15.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel40.SuspendLayout();
            this.panel41.SuspendLayout();
            this.panel57.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.labelTimeZone);
            this.panel1.Controls.Add(this.labelUseExluded);
            this.panel1.Controls.Add(this.checkBoxQCD);
            this.panel1.Controls.Add(this.buttonQueryh00);
            this.panel1.Controls.Add(this.buttonQuery1h);
            this.panel1.Controls.Add(this.buttonxh00);
            this.panel1.Controls.Add(this.buttonQueryTrend);
            this.panel1.Controls.Add(this.checkBoxPrintDur);
            this.panel1.Controls.Add(this.checkBoxPrintStat);
            this.panel1.Controls.Add(this.labelAnalysisUse);
            this.panel1.Controls.Add(this.label29);
            this.panel1.Controls.Add(this.label50);
            this.panel1.Controls.Add(this.labelRecorderID);
            this.panel1.Controls.Add(this.radioButton12hours);
            this.panel1.Controls.Add(this.radioButton6hours);
            this.panel1.Controls.Add(this.buttonCopyFrom);
            this.panel1.Controls.Add(this.labelQueryInfo);
            this.panel1.Controls.Add(this.labelMinutes);
            this.panel1.Controls.Add(this.textBoxLength);
            this.panel1.Controls.Add(this.labelLength);
            this.panel1.Controls.Add(this.textBoxStartMinute);
            this.panel1.Controls.Add(this.comboBoxStart);
            this.panel1.Controls.Add(this.textBoxStartHour);
            this.panel1.Controls.Add(this.dateTimeStart);
            this.panel1.Controls.Add(this.labelStart);
            this.panel1.Controls.Add(this.button0h00);
            this.panel1.Controls.Add(this.buttonP24);
            this.panel1.Controls.Add(this.buttonP6);
            this.panel1.Controls.Add(this.buttonP1);
            this.panel1.Controls.Add(this.buttonM1);
            this.panel1.Controls.Add(this.buttonM6);
            this.panel1.Controls.Add(this.buttonM24);
            this.panel1.Controls.Add(this.radioButtonAll);
            this.panel1.Controls.Add(this.radioButtonDay);
            this.panel1.Controls.Add(this.buttonSetTimeFrame);
            this.panel1.Controls.Add(this.textBoxToMin);
            this.panel1.Controls.Add(this.label26);
            this.panel1.Controls.Add(this.comboBoxToAM);
            this.panel1.Controls.Add(this.textBoxToHour);
            this.panel1.Controls.Add(this.dateTimePickerTo);
            this.panel1.Controls.Add(this.textBoxFromMin);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.comboBoxFromAM);
            this.panel1.Controls.Add(this.textBoxFromHour);
            this.panel1.Controls.Add(this.dateTimePickerFrom);
            this.panel1.Controls.Add(this.buttonQueryECG);
            this.panel1.Controls.Add(this.labelTo);
            this.panel1.Controls.Add(this.labelFrom);
            this.panel1.Controls.Add(this.radioButtonMonth);
            this.panel1.Controls.Add(this.radioButtonPeriod);
            this.panel1.Controls.Add(this.radioButtonWeek);
            this.panel1.Controls.Add(this.radioButtonHour);
            this.panel1.Controls.Add(this.panel13);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel46);
            this.panel1.Controls.Add(this.labelColon);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1288, 138);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // labelTimeZone
            // 
            this.labelTimeZone.AutoSize = true;
            this.labelTimeZone.Font = new System.Drawing.Font("Arial", 10F);
            this.labelTimeZone.Location = new System.Drawing.Point(1123, 29);
            this.labelTimeZone.Name = "labelTimeZone";
            this.labelTimeZone.Size = new System.Drawing.Size(57, 16);
            this.labelTimeZone.TabIndex = 71;
            this.labelTimeZone.Text = "^+12:00";
            // 
            // labelUseExluded
            // 
            this.labelUseExluded.AutoSize = true;
            this.labelUseExluded.Font = new System.Drawing.Font("Arial", 10F);
            this.labelUseExluded.Location = new System.Drawing.Point(1183, 91);
            this.labelUseExluded.Name = "labelUseExluded";
            this.labelUseExluded.Size = new System.Drawing.Size(46, 16);
            this.labelUseExluded.TabIndex = 70;
            this.labelUseExluded.Text = "^+Excl";
            this.labelUseExluded.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelUseExluded.Visible = false;
            // 
            // checkBoxQCD
            // 
            this.checkBoxQCD.AutoSize = true;
            this.checkBoxQCD.Location = new System.Drawing.Point(1189, 76);
            this.checkBoxQCD.Name = "checkBoxQCD";
            this.checkBoxQCD.Size = new System.Drawing.Size(41, 17);
            this.checkBoxQCD.TabIndex = 69;
            this.checkBoxQCD.Text = "QC";
            this.checkBoxQCD.UseVisualStyleBackColor = true;
            // 
            // buttonQueryh00
            // 
            this.buttonQueryh00.Font = new System.Drawing.Font("Arial", 9F);
            this.buttonQueryh00.Location = new System.Drawing.Point(743, 85);
            this.buttonQueryh00.Name = "buttonQueryh00";
            this.buttonQueryh00.Size = new System.Drawing.Size(37, 20);
            this.buttonQueryh00.TabIndex = 68;
            this.buttonQueryh00.Text = "x:00";
            this.buttonQueryh00.UseVisualStyleBackColor = true;
            this.buttonQueryh00.Click += new System.EventHandler(this.buttonQueryh00_Click);
            // 
            // buttonQuery1h
            // 
            this.buttonQuery1h.Font = new System.Drawing.Font("Arial", 9F);
            this.buttonQuery1h.Location = new System.Drawing.Point(705, 85);
            this.buttonQuery1h.Margin = new System.Windows.Forms.Padding(0);
            this.buttonQuery1h.Name = "buttonQuery1h";
            this.buttonQuery1h.Size = new System.Drawing.Size(33, 20);
            this.buttonQuery1h.TabIndex = 67;
            this.buttonQuery1h.Text = "1 h";
            this.buttonQuery1h.UseVisualStyleBackColor = true;
            this.buttonQuery1h.Click += new System.EventHandler(this.buttonQuery1h_Click);
            // 
            // buttonxh00
            // 
            this.buttonxh00.Font = new System.Drawing.Font("Arial", 9F);
            this.buttonxh00.Location = new System.Drawing.Point(313, 90);
            this.buttonxh00.Name = "buttonxh00";
            this.buttonxh00.Size = new System.Drawing.Size(43, 20);
            this.buttonxh00.TabIndex = 66;
            this.buttonxh00.Text = "x:00";
            this.buttonxh00.UseVisualStyleBackColor = true;
            this.buttonxh00.Click += new System.EventHandler(this.buttonxh00_Click);
            // 
            // buttonQueryTrend
            // 
            this.buttonQueryTrend.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.buttonQueryTrend.Location = new System.Drawing.Point(849, 30);
            this.buttonQueryTrend.Name = "buttonQueryTrend";
            this.buttonQueryTrend.Size = new System.Drawing.Size(58, 79);
            this.buttonQueryTrend.TabIndex = 65;
            this.buttonQueryTrend.Text = "Query for Trend data";
            this.buttonQueryTrend.UseVisualStyleBackColor = true;
            this.buttonQueryTrend.Visible = false;
            this.buttonQueryTrend.Click += new System.EventHandler(this.buttonQueryTrend_Click);
            // 
            // checkBoxPrintDur
            // 
            this.checkBoxPrintDur.Checked = true;
            this.checkBoxPrintDur.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxPrintDur.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.checkBoxPrintDur.Location = new System.Drawing.Point(1189, 52);
            this.checkBoxPrintDur.Name = "checkBoxPrintDur";
            this.checkBoxPrintDur.Size = new System.Drawing.Size(72, 21);
            this.checkBoxPrintDur.TabIndex = 64;
            this.checkBoxPrintDur.Text = "Print Dur.";
            this.checkBoxPrintDur.UseVisualStyleBackColor = true;
            // 
            // checkBoxPrintStat
            // 
            this.checkBoxPrintStat.Checked = true;
            this.checkBoxPrintStat.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxPrintStat.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.checkBoxPrintStat.Location = new System.Drawing.Point(1189, 30);
            this.checkBoxPrintStat.Name = "checkBoxPrintStat";
            this.checkBoxPrintStat.Size = new System.Drawing.Size(72, 21);
            this.checkBoxPrintStat.TabIndex = 63;
            this.checkBoxPrintStat.Text = "Print Stat";
            this.checkBoxPrintStat.UseVisualStyleBackColor = true;
            // 
            // labelAnalysisUse
            // 
            this.labelAnalysisUse.AutoSize = true;
            this.labelAnalysisUse.Font = new System.Drawing.Font("Arial", 10F);
            this.labelAnalysisUse.Location = new System.Drawing.Point(909, 93);
            this.labelAnalysisUse.Name = "labelAnalysisUse";
            this.labelAnalysisUse.Size = new System.Drawing.Size(203, 16);
            this.labelAnalysisUse.TabIndex = 62;
            this.labelAnalysisUse.Text = "^88 total, 12 added, 8 not used";
            // 
            // label29
            // 
            this.label29.Image = ((System.Drawing.Image)(resources.GetObject("label29.Image")));
            this.label29.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label29.Location = new System.Drawing.Point(1238, 71);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(41, 41);
            this.label29.TabIndex = 60;
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label29.Click += new System.EventHandler(this.label29_Click);
            // 
            // label50
            // 
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(914, 27);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(49, 20);
            this.label50.TabIndex = 58;
            this.label50.Text = "SNR:";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelRecorderID
            // 
            this.labelRecorderID.AutoSize = true;
            this.labelRecorderID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRecorderID.Location = new System.Drawing.Point(960, 29);
            this.labelRecorderID.Name = "labelRecorderID";
            this.labelRecorderID.Size = new System.Drawing.Size(118, 16);
            this.labelRecorderID.TabIndex = 59;
            this.labelRecorderID.Text = "^^H3R37777777";
            this.labelRecorderID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // radioButton12hours
            // 
            this.radioButton12hours.AutoSize = true;
            this.radioButton12hours.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.radioButton12hours.Location = new System.Drawing.Point(10, 69);
            this.radioButton12hours.Name = "radioButton12hours";
            this.radioButton12hours.Size = new System.Drawing.Size(69, 20);
            this.radioButton12hours.TabIndex = 55;
            this.radioButton12hours.TabStop = true;
            this.radioButton12hours.Text = "12 hrs";
            this.radioButton12hours.UseVisualStyleBackColor = true;
            this.radioButton12hours.CheckedChanged += new System.EventHandler(this.radioButton12hours_CheckedChanged);
            // 
            // radioButton6hours
            // 
            this.radioButton6hours.AutoSize = true;
            this.radioButton6hours.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.radioButton6hours.Location = new System.Drawing.Point(10, 49);
            this.radioButton6hours.Name = "radioButton6hours";
            this.radioButton6hours.Size = new System.Drawing.Size(61, 20);
            this.radioButton6hours.TabIndex = 54;
            this.radioButton6hours.TabStop = true;
            this.radioButton6hours.Text = "6 hrs";
            this.radioButton6hours.UseVisualStyleBackColor = true;
            this.radioButton6hours.CheckedChanged += new System.EventHandler(this.radioButton6hours_CheckedChanged);
            // 
            // buttonCopyFrom
            // 
            this.buttonCopyFrom.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.buttonCopyFrom.Location = new System.Drawing.Point(586, 58);
            this.buttonCopyFrom.Name = "buttonCopyFrom";
            this.buttonCopyFrom.Size = new System.Drawing.Size(40, 24);
            this.buttonCopyFrom.TabIndex = 52;
            this.buttonCopyFrom.Text = ">>";
            this.buttonCopyFrom.UseVisualStyleBackColor = true;
            this.buttonCopyFrom.Visible = false;
            this.buttonCopyFrom.Click += new System.EventHandler(this.buttonCopyFrom_Click);
            // 
            // labelQueryInfo
            // 
            this.labelQueryInfo.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQueryInfo.Location = new System.Drawing.Point(913, 47);
            this.labelQueryInfo.Name = "labelQueryInfo";
            this.labelQueryInfo.Size = new System.Drawing.Size(270, 46);
            this.labelQueryInfo.TabIndex = 50;
            this.labelQueryInfo.Text = "^^File ~123456, n = 123\r\nhgfhdsagk\r\nafdh";
            // 
            // labelMinutes
            // 
            this.labelMinutes.Font = new System.Drawing.Font("Arial", 10F);
            this.labelMinutes.Location = new System.Drawing.Point(675, 85);
            this.labelMinutes.Name = "labelMinutes";
            this.labelMinutes.Size = new System.Drawing.Size(31, 23);
            this.labelMinutes.TabIndex = 49;
            this.labelMinutes.Text = "Min";
            this.labelMinutes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxLength
            // 
            this.textBoxLength.Font = new System.Drawing.Font("Arial", 9F);
            this.textBoxLength.Location = new System.Drawing.Point(632, 86);
            this.textBoxLength.Name = "textBoxLength";
            this.textBoxLength.Size = new System.Drawing.Size(40, 21);
            this.textBoxLength.TabIndex = 48;
            this.textBoxLength.Text = "^9999";
            // 
            // labelLength
            // 
            this.labelLength.AutoSize = true;
            this.labelLength.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelLength.Location = new System.Drawing.Point(584, 88);
            this.labelLength.Name = "labelLength";
            this.labelLength.Size = new System.Drawing.Size(45, 16);
            this.labelLength.TabIndex = 47;
            this.labelLength.Text = "Time:";
            // 
            // textBoxStartMinute
            // 
            this.textBoxStartMinute.Font = new System.Drawing.Font("Arial", 9F);
            this.textBoxStartMinute.Location = new System.Drawing.Point(680, 58);
            this.textBoxStartMinute.Name = "textBoxStartMinute";
            this.textBoxStartMinute.Size = new System.Drawing.Size(26, 21);
            this.textBoxStartMinute.TabIndex = 46;
            this.textBoxStartMinute.Text = "99";
            // 
            // comboBoxStart
            // 
            this.comboBoxStart.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBoxStart.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxStart.Font = new System.Drawing.Font("Arial", 9F);
            this.comboBoxStart.FormattingEnabled = true;
            this.comboBoxStart.Items.AddRange(new object[] {
            "AM",
            "PM"});
            this.comboBoxStart.Location = new System.Drawing.Point(711, 57);
            this.comboBoxStart.Name = "comboBoxStart";
            this.comboBoxStart.Size = new System.Drawing.Size(44, 23);
            this.comboBoxStart.TabIndex = 45;
            // 
            // textBoxStartHour
            // 
            this.textBoxStartHour.Font = new System.Drawing.Font("Arial", 9F);
            this.textBoxStartHour.Location = new System.Drawing.Point(634, 59);
            this.textBoxStartHour.Name = "textBoxStartHour";
            this.textBoxStartHour.Size = new System.Drawing.Size(26, 21);
            this.textBoxStartHour.TabIndex = 44;
            this.textBoxStartHour.Text = "99";
            // 
            // dateTimeStart
            // 
            this.dateTimeStart.CustomFormat = "";
            this.dateTimeStart.Font = new System.Drawing.Font("Arial", 9F);
            this.dateTimeStart.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimeStart.Location = new System.Drawing.Point(634, 31);
            this.dateTimeStart.Name = "dateTimeStart";
            this.dateTimeStart.Size = new System.Drawing.Size(121, 21);
            this.dateTimeStart.TabIndex = 43;
            this.dateTimeStart.Value = new System.DateTime(1999, 1, 25, 0, 0, 0, 0);
            // 
            // labelStart
            // 
            this.labelStart.AutoSize = true;
            this.labelStart.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelStart.Location = new System.Drawing.Point(583, 34);
            this.labelStart.Name = "labelStart";
            this.labelStart.Size = new System.Drawing.Size(45, 16);
            this.labelStart.TabIndex = 42;
            this.labelStart.Text = "Start:";
            // 
            // button0h00
            // 
            this.button0h00.Font = new System.Drawing.Font("Arial", 9F);
            this.button0h00.Location = new System.Drawing.Point(268, 90);
            this.button0h00.Name = "button0h00";
            this.button0h00.Size = new System.Drawing.Size(43, 20);
            this.button0h00.TabIndex = 41;
            this.button0h00.Text = "0:00";
            this.button0h00.UseVisualStyleBackColor = true;
            this.button0h00.Click += new System.EventHandler(this.button0h_Click);
            // 
            // buttonP24
            // 
            this.buttonP24.Font = new System.Drawing.Font("Arial", 9F);
            this.buttonP24.Location = new System.Drawing.Point(446, 90);
            this.buttonP24.Name = "buttonP24";
            this.buttonP24.Size = new System.Drawing.Size(43, 20);
            this.buttonP24.TabIndex = 40;
            this.buttonP24.Text = "+24 h";
            this.buttonP24.UseVisualStyleBackColor = true;
            this.buttonP24.Click += new System.EventHandler(this.buttonP24_Click);
            // 
            // buttonP6
            // 
            this.buttonP6.Font = new System.Drawing.Font("Arial", 9F);
            this.buttonP6.Location = new System.Drawing.Point(402, 90);
            this.buttonP6.Name = "buttonP6";
            this.buttonP6.Size = new System.Drawing.Size(43, 20);
            this.buttonP6.TabIndex = 39;
            this.buttonP6.Text = "+6 h";
            this.buttonP6.UseVisualStyleBackColor = true;
            this.buttonP6.Click += new System.EventHandler(this.buttonP6_Click);
            // 
            // buttonP1
            // 
            this.buttonP1.Font = new System.Drawing.Font("Arial", 9F);
            this.buttonP1.Location = new System.Drawing.Point(357, 90);
            this.buttonP1.Name = "buttonP1";
            this.buttonP1.Size = new System.Drawing.Size(43, 20);
            this.buttonP1.TabIndex = 38;
            this.buttonP1.Text = "+1 h";
            this.buttonP1.UseVisualStyleBackColor = true;
            this.buttonP1.Click += new System.EventHandler(this.buttonP1_Click);
            // 
            // buttonM1
            // 
            this.buttonM1.Font = new System.Drawing.Font("Arial", 9F);
            this.buttonM1.Location = new System.Drawing.Point(223, 90);
            this.buttonM1.Name = "buttonM1";
            this.buttonM1.Size = new System.Drawing.Size(43, 20);
            this.buttonM1.TabIndex = 37;
            this.buttonM1.Text = "-1 h";
            this.buttonM1.UseVisualStyleBackColor = true;
            this.buttonM1.Click += new System.EventHandler(this.buttonM1_Click);
            // 
            // buttonM6
            // 
            this.buttonM6.Font = new System.Drawing.Font("Arial", 9F);
            this.buttonM6.Location = new System.Drawing.Point(178, 90);
            this.buttonM6.Name = "buttonM6";
            this.buttonM6.Size = new System.Drawing.Size(43, 20);
            this.buttonM6.TabIndex = 36;
            this.buttonM6.Text = "-6 h";
            this.buttonM6.UseVisualStyleBackColor = true;
            this.buttonM6.Click += new System.EventHandler(this.buttonM6_Click);
            // 
            // buttonM24
            // 
            this.buttonM24.Font = new System.Drawing.Font("Arial", 9F);
            this.buttonM24.Location = new System.Drawing.Point(133, 90);
            this.buttonM24.Name = "buttonM24";
            this.buttonM24.Size = new System.Drawing.Size(43, 20);
            this.buttonM24.TabIndex = 35;
            this.buttonM24.Text = "-24 h";
            this.buttonM24.UseVisualStyleBackColor = true;
            this.buttonM24.Click += new System.EventHandler(this.buttonM24_Click);
            // 
            // radioButtonAll
            // 
            this.radioButtonAll.AutoSize = true;
            this.radioButtonAll.Checked = true;
            this.radioButtonAll.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.radioButtonAll.Location = new System.Drawing.Point(79, 89);
            this.radioButtonAll.Name = "radioButtonAll";
            this.radioButtonAll.Size = new System.Drawing.Size(43, 20);
            this.radioButtonAll.TabIndex = 33;
            this.radioButtonAll.TabStop = true;
            this.radioButtonAll.Text = "All";
            this.radioButtonAll.UseVisualStyleBackColor = true;
            this.radioButtonAll.CheckedChanged += new System.EventHandler(this.radioButtonAll_CheckedChanged);
            // 
            // radioButtonDay
            // 
            this.radioButtonDay.AutoSize = true;
            this.radioButtonDay.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.radioButtonDay.Location = new System.Drawing.Point(10, 89);
            this.radioButtonDay.Name = "radioButtonDay";
            this.radioButtonDay.Size = new System.Drawing.Size(51, 20);
            this.radioButtonDay.TabIndex = 32;
            this.radioButtonDay.Text = "Day";
            this.radioButtonDay.UseVisualStyleBackColor = true;
            this.radioButtonDay.CheckedChanged += new System.EventHandler(this.radioButtonDay_CheckedChanged);
            // 
            // buttonSetTimeFrame
            // 
            this.buttonSetTimeFrame.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.buttonSetTimeFrame.Location = new System.Drawing.Point(495, 31);
            this.buttonSetTimeFrame.Name = "buttonSetTimeFrame";
            this.buttonSetTimeFrame.Size = new System.Drawing.Size(81, 80);
            this.buttonSetTimeFrame.TabIndex = 31;
            this.buttonSetTimeFrame.Text = "Set trend range to selection";
            this.buttonSetTimeFrame.UseVisualStyleBackColor = true;
            this.buttonSetTimeFrame.Click += new System.EventHandler(this.buttonSetTimeFrame_Click);
            // 
            // textBoxToMin
            // 
            this.textBoxToMin.Font = new System.Drawing.Font("Arial", 9F);
            this.textBoxToMin.Location = new System.Drawing.Point(386, 61);
            this.textBoxToMin.Name = "textBoxToMin";
            this.textBoxToMin.Size = new System.Drawing.Size(26, 21);
            this.textBoxToMin.TabIndex = 30;
            this.textBoxToMin.Text = "99";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.label26.Location = new System.Drawing.Point(337, 63);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(13, 16);
            this.label26.TabIndex = 28;
            this.label26.Text = "-";
            // 
            // comboBoxToAM
            // 
            this.comboBoxToAM.Font = new System.Drawing.Font("Arial", 9F);
            this.comboBoxToAM.FormattingEnabled = true;
            this.comboBoxToAM.Items.AddRange(new object[] {
            "AM",
            "PM"});
            this.comboBoxToAM.Location = new System.Drawing.Point(418, 60);
            this.comboBoxToAM.Name = "comboBoxToAM";
            this.comboBoxToAM.Size = new System.Drawing.Size(44, 23);
            this.comboBoxToAM.TabIndex = 27;
            // 
            // textBoxToHour
            // 
            this.textBoxToHour.Font = new System.Drawing.Font("Arial", 9F);
            this.textBoxToHour.Location = new System.Drawing.Point(353, 61);
            this.textBoxToHour.Name = "textBoxToHour";
            this.textBoxToHour.Size = new System.Drawing.Size(26, 21);
            this.textBoxToHour.TabIndex = 26;
            this.textBoxToHour.Text = "99";
            // 
            // dateTimePickerTo
            // 
            this.dateTimePickerTo.Font = new System.Drawing.Font("Arial", 9F);
            this.dateTimePickerTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerTo.Location = new System.Drawing.Point(238, 60);
            this.dateTimePickerTo.Name = "dateTimePickerTo";
            this.dateTimePickerTo.Size = new System.Drawing.Size(94, 21);
            this.dateTimePickerTo.TabIndex = 25;
            this.dateTimePickerTo.Value = new System.DateTime(1999, 1, 25, 0, 0, 0, 0);
            // 
            // textBoxFromMin
            // 
            this.textBoxFromMin.Font = new System.Drawing.Font("Arial", 9F);
            this.textBoxFromMin.Location = new System.Drawing.Point(386, 31);
            this.textBoxFromMin.Name = "textBoxFromMin";
            this.textBoxFromMin.Size = new System.Drawing.Size(26, 21);
            this.textBoxFromMin.TabIndex = 23;
            this.textBoxFromMin.Text = "99";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.label19.Location = new System.Drawing.Point(337, 32);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(13, 16);
            this.label19.TabIndex = 21;
            this.label19.Text = "-";
            // 
            // comboBoxFromAM
            // 
            this.comboBoxFromAM.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBoxFromAM.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxFromAM.Font = new System.Drawing.Font("Arial", 9F);
            this.comboBoxFromAM.FormattingEnabled = true;
            this.comboBoxFromAM.Items.AddRange(new object[] {
            "AM",
            "PM"});
            this.comboBoxFromAM.Location = new System.Drawing.Point(418, 30);
            this.comboBoxFromAM.Name = "comboBoxFromAM";
            this.comboBoxFromAM.Size = new System.Drawing.Size(44, 23);
            this.comboBoxFromAM.TabIndex = 20;
            // 
            // textBoxFromHour
            // 
            this.textBoxFromHour.Font = new System.Drawing.Font("Arial", 9F);
            this.textBoxFromHour.Location = new System.Drawing.Point(353, 31);
            this.textBoxFromHour.Name = "textBoxFromHour";
            this.textBoxFromHour.Size = new System.Drawing.Size(26, 21);
            this.textBoxFromHour.TabIndex = 18;
            this.textBoxFromHour.Text = "99";
            // 
            // dateTimePickerFrom
            // 
            this.dateTimePickerFrom.Font = new System.Drawing.Font("Arial", 9F);
            this.dateTimePickerFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerFrom.Location = new System.Drawing.Point(238, 30);
            this.dateTimePickerFrom.Name = "dateTimePickerFrom";
            this.dateTimePickerFrom.Size = new System.Drawing.Size(94, 21);
            this.dateTimePickerFrom.TabIndex = 13;
            this.dateTimePickerFrom.Value = new System.DateTime(1999, 1, 25, 0, 0, 0, 0);
            // 
            // buttonQueryECG
            // 
            this.buttonQueryECG.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.buttonQueryECG.Location = new System.Drawing.Point(785, 30);
            this.buttonQueryECG.Name = "buttonQueryECG";
            this.buttonQueryECG.Size = new System.Drawing.Size(58, 79);
            this.buttonQueryECG.TabIndex = 12;
            this.buttonQueryECG.Text = "Query for ECG data";
            this.buttonQueryECG.UseVisualStyleBackColor = true;
            this.buttonQueryECG.Visible = false;
            this.buttonQueryECG.Click += new System.EventHandler(this.button1_Click);
            // 
            // labelTo
            // 
            this.labelTo.AutoSize = true;
            this.labelTo.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelTo.Location = new System.Drawing.Point(209, 59);
            this.labelTo.Name = "labelTo";
            this.labelTo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labelTo.Size = new System.Drawing.Size(28, 16);
            this.labelTo.TabIndex = 11;
            this.labelTo.Text = "To:";
            // 
            // labelFrom
            // 
            this.labelFrom.AutoSize = true;
            this.labelFrom.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelFrom.Location = new System.Drawing.Point(189, 31);
            this.labelFrom.Name = "labelFrom";
            this.labelFrom.Size = new System.Drawing.Size(48, 16);
            this.labelFrom.TabIndex = 10;
            this.labelFrom.Text = "From:";
            // 
            // radioButtonMonth
            // 
            this.radioButtonMonth.AutoSize = true;
            this.radioButtonMonth.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.radioButtonMonth.Location = new System.Drawing.Point(79, 49);
            this.radioButtonMonth.Name = "radioButtonMonth";
            this.radioButtonMonth.Size = new System.Drawing.Size(71, 20);
            this.radioButtonMonth.TabIndex = 9;
            this.radioButtonMonth.Text = "Month";
            this.radioButtonMonth.UseVisualStyleBackColor = true;
            this.radioButtonMonth.CheckedChanged += new System.EventHandler(this.radioButtonMonth_CheckedChanged);
            // 
            // radioButtonPeriod
            // 
            this.radioButtonPeriod.AutoSize = true;
            this.radioButtonPeriod.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.radioButtonPeriod.Location = new System.Drawing.Point(79, 69);
            this.radioButtonPeriod.Name = "radioButtonPeriod";
            this.radioButtonPeriod.Size = new System.Drawing.Size(88, 20);
            this.radioButtonPeriod.TabIndex = 8;
            this.radioButtonPeriod.Text = "Selected";
            this.radioButtonPeriod.UseVisualStyleBackColor = true;
            this.radioButtonPeriod.CheckedChanged += new System.EventHandler(this.radioButtonPeriod_CheckedChanged);
            // 
            // radioButtonWeek
            // 
            this.radioButtonWeek.AutoSize = true;
            this.radioButtonWeek.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.radioButtonWeek.Location = new System.Drawing.Point(79, 28);
            this.radioButtonWeek.Name = "radioButtonWeek";
            this.radioButtonWeek.Size = new System.Drawing.Size(65, 20);
            this.radioButtonWeek.TabIndex = 7;
            this.radioButtonWeek.Text = "Week";
            this.radioButtonWeek.UseVisualStyleBackColor = true;
            this.radioButtonWeek.CheckedChanged += new System.EventHandler(this.radioButtonWeek_CheckedChanged);
            // 
            // radioButtonHour
            // 
            this.radioButtonHour.AutoSize = true;
            this.radioButtonHour.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.radioButtonHour.Location = new System.Drawing.Point(10, 27);
            this.radioButtonHour.Name = "radioButtonHour";
            this.radioButtonHour.Size = new System.Drawing.Size(53, 20);
            this.radioButtonHour.TabIndex = 6;
            this.radioButtonHour.Text = "1 hr";
            this.radioButtonHour.UseVisualStyleBackColor = true;
            this.radioButtonHour.CheckedChanged += new System.EventHandler(this.radioButtonHour_CheckedChanged);
            // 
            // panel13
            // 
            this.panel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel13.Controls.Add(this.label13);
            this.panel13.Controls.Add(this.label35);
            this.panel13.Controls.Add(this.labelNow);
            this.panel13.Controls.Add(this.labelDaysHours);
            this.panel13.Controls.Add(this.label33);
            this.panel13.Controls.Add(this.labelTrendTime);
            this.panel13.Controls.Add(this.label18);
            this.panel13.Controls.Add(this.labelTrendEnd);
            this.panel13.Controls.Add(this.label14);
            this.panel13.Controls.Add(this.labelTrendStart);
            this.panel13.Controls.Add(this.label20);
            this.panel13.Controls.Add(this.labelNearSec);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel13.Location = new System.Drawing.Point(0, 113);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(1288, 25);
            this.panel13.TabIndex = 5;
            // 
            // label13
            // 
            this.label13.Dock = System.Windows.Forms.DockStyle.Right;
            this.label13.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(1031, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(20, 23);
            this.label13.TabIndex = 7;
            this.label13.Text = "Total beats:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label13.Visible = false;
            // 
            // label35
            // 
            this.label35.Dock = System.Windows.Forms.DockStyle.Right;
            this.label35.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label35.Location = new System.Drawing.Point(1051, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(54, 23);
            this.label35.TabIndex = 18;
            this.label35.Text = "Today:";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label35.Visible = false;
            // 
            // labelNow
            // 
            this.labelNow.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelNow.Font = new System.Drawing.Font("Arial", 10F);
            this.labelNow.Location = new System.Drawing.Point(1105, 0);
            this.labelNow.Name = "labelNow";
            this.labelNow.Size = new System.Drawing.Size(74, 23);
            this.labelNow.TabIndex = 19;
            this.labelNow.Text = "12h 14m";
            this.labelNow.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelNow.Visible = false;
            // 
            // labelDaysHours
            // 
            this.labelDaysHours.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelDaysHours.Font = new System.Drawing.Font("Arial", 10F);
            this.labelDaysHours.Location = new System.Drawing.Point(941, 0);
            this.labelDaysHours.Name = "labelDaysHours";
            this.labelDaysHours.Size = new System.Drawing.Size(157, 23);
            this.labelDaysHours.TabIndex = 21;
            this.labelDaysHours.Text = "^10d-10h-16m ";
            this.labelDaysHours.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label33
            // 
            this.label33.Dock = System.Windows.Forms.DockStyle.Left;
            this.label33.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label33.Location = new System.Drawing.Point(818, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(123, 23);
            this.label33.TabIndex = 20;
            this.label33.Text = "Study Start time:";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelTrendTime
            // 
            this.labelTrendTime.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelTrendTime.Font = new System.Drawing.Font("Arial", 10F);
            this.labelTrendTime.Location = new System.Drawing.Point(670, 0);
            this.labelTrendTime.Name = "labelTrendTime";
            this.labelTrendTime.Size = new System.Drawing.Size(148, 23);
            this.labelTrendTime.TabIndex = 4;
            this.labelTrendTime.Text = "^17d - 13h -16m ";
            this.labelTrendTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label18
            // 
            this.label18.Dock = System.Windows.Forms.DockStyle.Left;
            this.label18.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(584, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(86, 23);
            this.label18.TabIndex = 3;
            this.label18.Text = "Trend time:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelTrendEnd
            // 
            this.labelTrendEnd.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelTrendEnd.Font = new System.Drawing.Font("Arial", 10F);
            this.labelTrendEnd.Location = new System.Drawing.Point(397, 0);
            this.labelTrendEnd.Name = "labelTrendEnd";
            this.labelTrendEnd.Size = new System.Drawing.Size(187, 23);
            this.labelTrendEnd.TabIndex = 10;
            this.labelTrendEnd.Text = "^Dec 10, 1900 - 10:00 AM";
            this.labelTrendEnd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label14
            // 
            this.label14.Dock = System.Windows.Forms.DockStyle.Left;
            this.label14.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(310, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(87, 23);
            this.label14.TabIndex = 9;
            this.label14.Text = "Trend End:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelTrendStart
            // 
            this.labelTrendStart.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelTrendStart.Font = new System.Drawing.Font("Arial", 10F);
            this.labelTrendStart.Location = new System.Drawing.Point(92, 0);
            this.labelTrendStart.Name = "labelTrendStart";
            this.labelTrendStart.Size = new System.Drawing.Size(218, 23);
            this.labelTrendStart.TabIndex = 2;
            this.labelTrendStart.Text = "^Dec 10, 1900 - 10:00 AM +12:00 ";
            this.labelTrendStart.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label20
            // 
            this.label20.Dock = System.Windows.Forms.DockStyle.Left;
            this.label20.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label20.Location = new System.Drawing.Point(0, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(92, 23);
            this.label20.TabIndex = 1;
            this.label20.Text = "Trend start:";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelNearSec
            // 
            this.labelNearSec.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelNearSec.Font = new System.Drawing.Font("Arial", 10F);
            this.labelNearSec.Location = new System.Drawing.Point(1179, 0);
            this.labelNearSec.Name = "labelNearSec";
            this.labelNearSec.Size = new System.Drawing.Size(107, 23);
            this.labelNearSec.TabIndex = 8;
            this.labelNearSec.Text = "near XX sec";
            this.labelNearSec.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelNearSec.Click += new System.EventHandler(this.labelNearSec_Click);
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 25);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1288, 1);
            this.panel3.TabIndex = 4;
            this.panel3.Visible = false;
            // 
            // panel46
            // 
            this.panel46.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel46.Controls.Add(this.labelPatName);
            this.panel46.Controls.Add(this.label56);
            this.panel46.Controls.Add(this.labelPatID);
            this.panel46.Controls.Add(this.labelPatIDHeader);
            this.panel46.Controls.Add(this.label5);
            this.panel46.Controls.Add(this.labelDoB);
            this.panel46.Controls.Add(this.labelAgeGender);
            this.panel46.Controls.Add(this.label31);
            this.panel46.Controls.Add(this.labelStartDate);
            this.panel46.Controls.Add(this.labelStudyNote);
            this.panel46.Controls.Add(this.labelStudy);
            this.panel46.Controls.Add(this.label52);
            this.panel46.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel46.Location = new System.Drawing.Point(0, 0);
            this.panel46.Name = "panel46";
            this.panel46.Size = new System.Drawing.Size(1288, 25);
            this.panel46.TabIndex = 3;
            // 
            // labelPatName
            // 
            this.labelPatName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPatName.Font = new System.Drawing.Font("Arial", 10F);
            this.labelPatName.Location = new System.Drawing.Point(429, 0);
            this.labelPatName.Name = "labelPatName";
            this.labelPatName.Size = new System.Drawing.Size(405, 23);
            this.labelPatName.TabIndex = 4;
            this.labelPatName.Text = "^^Ottovordemgentschenfelde";
            this.labelPatName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label56
            // 
            this.label56.Dock = System.Windows.Forms.DockStyle.Left;
            this.label56.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label56.Location = new System.Drawing.Point(378, 0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(51, 23);
            this.label56.TabIndex = 3;
            this.label56.Text = "Name:";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelPatID
            // 
            this.labelPatID.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPatID.Font = new System.Drawing.Font("Arial", 10F);
            this.labelPatID.Location = new System.Drawing.Point(239, 0);
            this.labelPatID.Name = "labelPatID";
            this.labelPatID.Size = new System.Drawing.Size(139, 23);
            this.labelPatID.TabIndex = 2;
            this.labelPatID.Text = "123456789101023";
            this.labelPatID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelPatIDHeader
            // 
            this.labelPatIDHeader.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPatIDHeader.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelPatIDHeader.Location = new System.Drawing.Point(151, 0);
            this.labelPatIDHeader.Name = "labelPatIDHeader";
            this.labelPatIDHeader.Size = new System.Drawing.Size(88, 23);
            this.labelPatIDHeader.TabIndex = 1;
            this.labelPatIDHeader.Text = "Patient. ID:";
            this.labelPatIDHeader.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Right;
            this.label5.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(834, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 23);
            this.label5.TabIndex = 9;
            this.label5.Text = "DoB:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelDoB
            // 
            this.labelDoB.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelDoB.Font = new System.Drawing.Font("Arial", 10F);
            this.labelDoB.Location = new System.Drawing.Point(875, 0);
            this.labelDoB.Name = "labelDoB";
            this.labelDoB.Size = new System.Drawing.Size(93, 23);
            this.labelDoB.TabIndex = 6;
            this.labelDoB.Text = "^99/99/9999";
            this.labelDoB.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelAgeGender
            // 
            this.labelAgeGender.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelAgeGender.Font = new System.Drawing.Font("Arial", 10F);
            this.labelAgeGender.Location = new System.Drawing.Point(968, 0);
            this.labelAgeGender.Name = "labelAgeGender";
            this.labelAgeGender.Size = new System.Drawing.Size(104, 23);
            this.labelAgeGender.TabIndex = 10;
            this.labelAgeGender.Text = "^Female 199y";
            this.labelAgeGender.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label31
            // 
            this.label31.Dock = System.Windows.Forms.DockStyle.Right;
            this.label31.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label31.Location = new System.Drawing.Point(1072, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(45, 23);
            this.label31.TabIndex = 13;
            this.label31.Text = "Start:";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStartDate
            // 
            this.labelStartDate.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelStartDate.Font = new System.Drawing.Font("Arial", 10F);
            this.labelStartDate.Location = new System.Drawing.Point(1117, 0);
            this.labelStartDate.Name = "labelStartDate";
            this.labelStartDate.Size = new System.Drawing.Size(169, 23);
            this.labelStartDate.TabIndex = 14;
            this.labelStartDate.Text = "^Dec 10 2019 - 10:00 ";
            this.labelStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStudyNote
            // 
            this.labelStudyNote.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelStudyNote.Font = new System.Drawing.Font("Arial", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStudyNote.Location = new System.Drawing.Point(135, 0);
            this.labelStudyNote.Name = "labelStudyNote";
            this.labelStudyNote.Size = new System.Drawing.Size(16, 23);
            this.labelStudyNote.TabIndex = 15;
            this.labelStudyNote.Text = "sl\r\nsn";
            this.labelStudyNote.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStudy
            // 
            this.labelStudy.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelStudy.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStudy.Location = new System.Drawing.Point(60, 0);
            this.labelStudy.Name = "labelStudy";
            this.labelStudy.Size = new System.Drawing.Size(75, 23);
            this.labelStudy.TabIndex = 11;
            this.labelStudy.Text = "87654321";
            this.labelStudy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelStudy.DoubleClick += new System.EventHandler(this.labelStudy_DoubleClick);
            // 
            // label52
            // 
            this.label52.Dock = System.Windows.Forms.DockStyle.Left;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(0, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(60, 23);
            this.label52.TabIndex = 12;
            this.label52.Text = "Study:";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelColon
            // 
            this.labelColon.Font = new System.Drawing.Font("Arial", 10F);
            this.labelColon.Location = new System.Drawing.Point(661, 57);
            this.labelColon.Name = "labelColon";
            this.labelColon.Size = new System.Drawing.Size(18, 23);
            this.labelColon.TabIndex = 51;
            this.labelColon.Text = ":";
            this.labelColon.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel45
            // 
            this.panel45.Controls.Add(this.labelRunState);
            this.panel45.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel45.Location = new System.Drawing.Point(0, 0);
            this.panel45.Name = "panel45";
            this.panel45.Size = new System.Drawing.Size(1288, 2);
            this.panel45.TabIndex = 9;
            // 
            // labelRunState
            // 
            this.labelRunState.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelRunState.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRunState.Location = new System.Drawing.Point(0, 0);
            this.labelRunState.Name = "labelRunState";
            this.labelRunState.Size = new System.Drawing.Size(117, 2);
            this.labelRunState.TabIndex = 9;
            this.labelRunState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "TZ Report|*.tzr";
            this.openFileDialog.Multiselect = true;
            this.openFileDialog.RestoreDirectory = true;
            this.openFileDialog.Title = "Select one of the TZ Report files from a directory. ";
            // 
            // panel34
            // 
            this.panel34.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel34.Controls.Add(this.panel58);
            this.panel34.Controls.Add(this.checkBoxAnonymize);
            this.panel34.Controls.Add(this.checkBoxBlackWhite);
            this.panel34.Controls.Add(this.checkBoxValidated);
            this.panel34.Controls.Add(this.labelPrint);
            this.panel34.Controls.Add(this.checkBoxLogCalc);
            this.panel34.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel34.Location = new System.Drawing.Point(0, 793);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(1286, 45);
            this.panel34.TabIndex = 0;
            // 
            // panel58
            // 
            this.panel58.Controls.Add(this.panel27);
            this.panel58.Controls.Add(this.panel10);
            this.panel58.Controls.Add(this.panel59);
            this.panel58.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel58.Location = new System.Drawing.Point(0, 0);
            this.panel58.Name = "panel58";
            this.panel58.Size = new System.Drawing.Size(1054, 45);
            this.panel58.TabIndex = 8;
            // 
            // panel27
            // 
            this.panel27.Controls.Add(this.labelMouseInfo);
            this.panel27.Controls.Add(this.label2);
            this.panel27.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel27.Location = new System.Drawing.Point(0, 0);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(1054, 15);
            this.panel27.TabIndex = 4;
            this.panel27.Paint += new System.Windows.Forms.PaintEventHandler(this.panel27_Paint);
            // 
            // labelMouseInfo
            // 
            this.labelMouseInfo.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelMouseInfo.Location = new System.Drawing.Point(656, 0);
            this.labelMouseInfo.Name = "labelMouseInfo";
            this.labelMouseInfo.Size = new System.Drawing.Size(398, 15);
            this.labelMouseInfo.TabIndex = 14;
            this.labelMouseInfo.Text = "Mouse left click: set Query time, <shift>=duration, <ctrl>: set From time, <alt> " +
    "set To time.";
            this.labelMouseInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelMouseInfo.Click += new System.EventHandler(this.labelMouseInfo_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Left;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(621, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Note: Only when \"Print Validated\" box is checked, validated data is printed! Do n" +
    "ot use unvalidated data for diagnostic purposes ! ";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.labelUpodateTime);
            this.panel10.Controls.Add(this.labelRecAnalysis);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel10.Location = new System.Drawing.Point(0, 15);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(1054, 15);
            this.panel10.TabIndex = 3;
            this.panel10.Paint += new System.Windows.Forms.PaintEventHandler(this.panel10_Paint_1);
            this.panel10.DoubleClick += new System.EventHandler(this.panel10_DoubleClick);
            // 
            // labelUpodateTime
            // 
            this.labelUpodateTime.AutoSize = true;
            this.labelUpodateTime.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelUpodateTime.Location = new System.Drawing.Point(1044, 0);
            this.labelUpodateTime.Name = "labelUpodateTime";
            this.labelUpodateTime.Size = new System.Drawing.Size(10, 13);
            this.labelUpodateTime.TabIndex = 3;
            this.labelUpodateTime.Text = ".";
            // 
            // labelRecAnalysis
            // 
            this.labelRecAnalysis.AutoSize = true;
            this.labelRecAnalysis.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelRecAnalysis.Location = new System.Drawing.Point(0, 0);
            this.labelRecAnalysis.Name = "labelRecAnalysis";
            this.labelRecAnalysis.Size = new System.Drawing.Size(150, 13);
            this.labelRecAnalysis.TabIndex = 0;
            this.labelRecAnalysis.Text = "^-123 Records, -234 Analyzed";
            this.labelRecAnalysis.Click += new System.EventHandler(this.labelRecAnalysis_Click);
            this.labelRecAnalysis.DoubleClick += new System.EventHandler(this.labelRecAnalysis_DoubleClick);
            // 
            // panel59
            // 
            this.panel59.Controls.Add(this.labelFoundRec);
            this.panel59.Controls.Add(this.labelPrintResult);
            this.panel59.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel59.Location = new System.Drawing.Point(0, 30);
            this.panel59.Name = "panel59";
            this.panel59.Size = new System.Drawing.Size(1054, 15);
            this.panel59.TabIndex = 2;
            this.panel59.Paint += new System.Windows.Forms.PaintEventHandler(this.panel59_Paint);
            // 
            // labelFoundRec
            // 
            this.labelFoundRec.AutoSize = true;
            this.labelFoundRec.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelFoundRec.Location = new System.Drawing.Point(0, 0);
            this.labelFoundRec.Name = "labelFoundRec";
            this.labelFoundRec.Size = new System.Drawing.Size(173, 13);
            this.labelFoundRec.TabIndex = 3;
            this.labelFoundRec.Text = "^date time: ^not found record 12.3 ";
            this.labelFoundRec.Visible = false;
            // 
            // labelPrintResult
            // 
            this.labelPrintResult.AutoSize = true;
            this.labelPrintResult.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelPrintResult.Location = new System.Drawing.Point(1044, 0);
            this.labelPrintResult.Name = "labelPrintResult";
            this.labelPrintResult.Size = new System.Drawing.Size(10, 13);
            this.labelPrintResult.TabIndex = 1;
            this.labelPrintResult.Text = ".";
            // 
            // checkBoxAnonymize
            // 
            this.checkBoxAnonymize.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.checkBoxAnonymize.Dock = System.Windows.Forms.DockStyle.Right;
            this.checkBoxAnonymize.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxAnonymize.Location = new System.Drawing.Point(1054, 0);
            this.checkBoxAnonymize.Name = "checkBoxAnonymize";
            this.checkBoxAnonymize.Size = new System.Drawing.Size(66, 45);
            this.checkBoxAnonymize.TabIndex = 13;
            this.checkBoxAnonymize.Text = "Anonymize";
            this.checkBoxAnonymize.UseVisualStyleBackColor = true;
            this.checkBoxAnonymize.CheckedChanged += new System.EventHandler(this.checkBoxAnonymize_CheckedChanged);
            // 
            // checkBoxBlackWhite
            // 
            this.checkBoxBlackWhite.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.checkBoxBlackWhite.Dock = System.Windows.Forms.DockStyle.Right;
            this.checkBoxBlackWhite.Location = new System.Drawing.Point(1120, 0);
            this.checkBoxBlackWhite.Name = "checkBoxBlackWhite";
            this.checkBoxBlackWhite.Size = new System.Drawing.Size(40, 45);
            this.checkBoxBlackWhite.TabIndex = 14;
            this.checkBoxBlackWhite.Text = "Print B&&W";
            this.checkBoxBlackWhite.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxBlackWhite.UseVisualStyleBackColor = true;
            // 
            // checkBoxValidated
            // 
            this.checkBoxValidated.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.checkBoxValidated.Checked = true;
            this.checkBoxValidated.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxValidated.Dock = System.Windows.Forms.DockStyle.Right;
            this.checkBoxValidated.Location = new System.Drawing.Point(1160, 0);
            this.checkBoxValidated.Margin = new System.Windows.Forms.Padding(0);
            this.checkBoxValidated.Name = "checkBoxValidated";
            this.checkBoxValidated.Size = new System.Drawing.Size(56, 45);
            this.checkBoxValidated.TabIndex = 12;
            this.checkBoxValidated.Text = "Print Validated";
            this.checkBoxValidated.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxValidated.UseVisualStyleBackColor = true;
            // 
            // labelPrint
            // 
            this.labelPrint.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelPrint.Image = ((System.Drawing.Image)(resources.GetObject("labelPrint.Image")));
            this.labelPrint.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.labelPrint.Location = new System.Drawing.Point(1216, 0);
            this.labelPrint.Name = "labelPrint";
            this.labelPrint.Size = new System.Drawing.Size(41, 45);
            this.labelPrint.TabIndex = 7;
            this.labelPrint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelPrint.Click += new System.EventHandler(this.labelPrint_Click);
            // 
            // checkBoxLogCalc
            // 
            this.checkBoxLogCalc.AutoSize = true;
            this.checkBoxLogCalc.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.checkBoxLogCalc.Checked = true;
            this.checkBoxLogCalc.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxLogCalc.Dock = System.Windows.Forms.DockStyle.Right;
            this.checkBoxLogCalc.Location = new System.Drawing.Point(1257, 0);
            this.checkBoxLogCalc.Name = "checkBoxLogCalc";
            this.checkBoxLogCalc.Size = new System.Drawing.Size(29, 45);
            this.checkBoxLogCalc.TabIndex = 10;
            this.checkBoxLogCalc.Text = "Log";
            this.checkBoxLogCalc.UseVisualStyleBackColor = true;
            this.checkBoxLogCalc.Visible = false;
            this.checkBoxLogCalc.CheckedChanged += new System.EventHandler(this.checkBoxLogCalc_CheckedChanged);
            // 
            // panel28
            // 
            this.panel28.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel28.Location = new System.Drawing.Point(0, 0);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(1286, 1);
            this.panel28.TabIndex = 10;
            // 
            // panel11
            // 
            this.panel11.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel11.Location = new System.Drawing.Point(0, 154);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(1286, 2);
            this.panel11.TabIndex = 15;
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.checkBoxCountAnOnly);
            this.panel18.Controls.Add(this.checkBoxUseAnalysis);
            this.panel18.Controls.Add(this.checkBoxUseTrend);
            this.panel18.Controls.Add(this.checkBoxPulseOnOnly);
            this.panel18.Controls.Add(this.checkBoxTogleAfOnly);
            this.panel18.Controls.Add(this.label28);
            this.panel18.Controls.Add(this.label88);
            this.panel18.Controls.Add(this.panel7);
            this.panel18.Controls.Add(this.label7);
            this.panel18.Controls.Add(this.labelFDL);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel18.Location = new System.Drawing.Point(0, 499);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(1286, 21);
            this.panel18.TabIndex = 7;
            // 
            // checkBoxCountAnOnly
            // 
            this.checkBoxCountAnOnly.AutoSize = true;
            this.checkBoxCountAnOnly.Checked = true;
            this.checkBoxCountAnOnly.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxCountAnOnly.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxCountAnOnly.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxCountAnOnly.Location = new System.Drawing.Point(831, 0);
            this.checkBoxCountAnOnly.Name = "checkBoxCountAnOnly";
            this.checkBoxCountAnOnly.Size = new System.Drawing.Size(123, 21);
            this.checkBoxCountAnOnly.TabIndex = 10;
            this.checkBoxCountAnOnly.Text = "Count Analysis Only";
            this.checkBoxCountAnOnly.UseVisualStyleBackColor = true;
            this.checkBoxCountAnOnly.CheckStateChanged += new System.EventHandler(this.checkBoxCountAnOnly_CheckStateChanged);
            // 
            // checkBoxUseAnalysis
            // 
            this.checkBoxUseAnalysis.AutoSize = true;
            this.checkBoxUseAnalysis.Checked = true;
            this.checkBoxUseAnalysis.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxUseAnalysis.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxUseAnalysis.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxUseAnalysis.Location = new System.Drawing.Point(742, 0);
            this.checkBoxUseAnalysis.Name = "checkBoxUseAnalysis";
            this.checkBoxUseAnalysis.Size = new System.Drawing.Size(89, 21);
            this.checkBoxUseAnalysis.TabIndex = 7;
            this.checkBoxUseAnalysis.Text = "Use Analysis";
            this.checkBoxUseAnalysis.UseVisualStyleBackColor = true;
            this.checkBoxUseAnalysis.CheckedChanged += new System.EventHandler(this.checkBoxUseAnalysis_CheckedChanged);
            // 
            // checkBoxUseTrend
            // 
            this.checkBoxUseTrend.AutoSize = true;
            this.checkBoxUseTrend.Checked = true;
            this.checkBoxUseTrend.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxUseTrend.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxUseTrend.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxUseTrend.Location = new System.Drawing.Point(666, 0);
            this.checkBoxUseTrend.Name = "checkBoxUseTrend";
            this.checkBoxUseTrend.Size = new System.Drawing.Size(76, 21);
            this.checkBoxUseTrend.TabIndex = 6;
            this.checkBoxUseTrend.Text = "Use Trend";
            this.checkBoxUseTrend.UseVisualStyleBackColor = true;
            this.checkBoxUseTrend.CheckStateChanged += new System.EventHandler(this.checkBoxUseTrend_CheckStateChanged);
            // 
            // checkBoxPulseOnOnly
            // 
            this.checkBoxPulseOnOnly.AutoSize = true;
            this.checkBoxPulseOnOnly.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxPulseOnOnly.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxPulseOnOnly.Location = new System.Drawing.Point(577, 0);
            this.checkBoxPulseOnOnly.Name = "checkBoxPulseOnOnly";
            this.checkBoxPulseOnOnly.Size = new System.Drawing.Size(89, 21);
            this.checkBoxPulseOnOnly.TabIndex = 9;
            this.checkBoxPulseOnOnly.Text = "Puls ON Only";
            this.checkBoxPulseOnOnly.UseVisualStyleBackColor = true;
            this.checkBoxPulseOnOnly.CheckedChanged += new System.EventHandler(this.checkBoxPulseOffOnly_CheckedChanged);
            // 
            // checkBoxTogleAfOnly
            // 
            this.checkBoxTogleAfOnly.AutoSize = true;
            this.checkBoxTogleAfOnly.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxTogleAfOnly.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxTogleAfOnly.Location = new System.Drawing.Point(461, 0);
            this.checkBoxTogleAfOnly.Name = "checkBoxTogleAfOnly";
            this.checkBoxTogleAfOnly.Size = new System.Drawing.Size(116, 21);
            this.checkBoxTogleAfOnly.TabIndex = 63;
            this.checkBoxTogleAfOnly.Text = "Toggle AF ON Only";
            this.checkBoxTogleAfOnly.UseVisualStyleBackColor = true;
            this.checkBoxTogleAfOnly.CheckedChanged += new System.EventHandler(this.checkBoxTogleAfOnly_CheckedChanged);
            // 
            // label28
            // 
            this.label28.Dock = System.Windows.Forms.DockStyle.Left;
            this.label28.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label28.Location = new System.Drawing.Point(296, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(165, 21);
            this.label28.TabIndex = 4;
            this.label28.Text = "Validated Episodes ";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label88
            // 
            this.label88.Dock = System.Windows.Forms.DockStyle.Right;
            this.label88.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label88.ForeColor = System.Drawing.Color.DimGray;
            this.label88.Location = new System.Drawing.Point(1012, 0);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(252, 21);
            this.label88.TabIndex = 3;
            this.label88.Text = "Device Generated Summary";
            this.label88.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel7
            // 
            this.panel7.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel7.Location = new System.Drawing.Point(263, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(33, 21);
            this.panel7.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.Dock = System.Windows.Forms.DockStyle.Left;
            this.label7.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(0, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(263, 21);
            this.label7.TabIndex = 1;
            this.label7.Text = "Validated Episodes Summary ";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelFDL
            // 
            this.labelFDL.AutoSize = true;
            this.labelFDL.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelFDL.Location = new System.Drawing.Point(1264, 0);
            this.labelFDL.Name = "labelFDL";
            this.labelFDL.Size = new System.Drawing.Size(22, 13);
            this.labelFDL.TabIndex = 62;
            this.labelFDL.Text = "DB";
            this.labelFDL.Click += new System.EventHandler(this.labelFDL_Click);
            // 
            // panel17
            // 
            this.panel17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel17.Controls.Add(this.panel60);
            this.panel17.Controls.Add(this.panel55);
            this.panel17.Controls.Add(this.panel36);
            this.panel17.Controls.Add(this.panel19);
            this.panel17.Controls.Add(this.panel33);
            this.panel17.Controls.Add(this.panel53);
            this.panel17.Controls.Add(this.panel29);
            this.panel17.Controls.Add(this.panel2);
            this.panel17.Controls.Add(this.panel31);
            this.panel17.Controls.Add(this.panel51);
            this.panel17.Controls.Add(this.panel49);
            this.panel17.Controls.Add(this.panel44);
            this.panel17.Controls.Add(this.panel9);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel17.Location = new System.Drawing.Point(0, 156);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(1286, 343);
            this.panel17.TabIndex = 6;
            // 
            // panel60
            // 
            this.panel60.Controls.Add(this.chartAllEvents);
            this.panel60.Controls.Add(this.panel61);
            this.panel60.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel60.Location = new System.Drawing.Point(0, 312);
            this.panel60.Name = "panel60";
            this.panel60.Size = new System.Drawing.Size(1284, 30);
            this.panel60.TabIndex = 9;
            // 
            // chartAllEvents
            // 
            chartArea1.AxisX.IsLabelAutoFit = false;
            chartArea1.AxisX.LabelStyle.Enabled = false;
            chartArea1.AxisX.LineColor = System.Drawing.Color.DarkGray;
            chartArea1.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea1.AxisX.MinorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea1.AxisX.ScaleBreakStyle.BreakLineStyle = System.Windows.Forms.DataVisualization.Charting.BreakLineStyle.None;
            chartArea1.AxisX.ScaleBreakStyle.LineColor = System.Drawing.Color.White;
            chartArea1.AxisX.ScaleBreakStyle.Spacing = 1D;
            chartArea1.AxisX2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea1.AxisX2.MajorGrid.Enabled = false;
            chartArea1.AxisX2.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea1.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea1.AxisY.IsLabelAutoFit = false;
            chartArea1.AxisY.LabelStyle.Enabled = false;
            chartArea1.AxisY.LineColor = System.Drawing.Color.LightGray;
            chartArea1.AxisY.MajorGrid.Enabled = false;
            chartArea1.AxisY.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea1.AxisY.MajorTickMark.Enabled = false;
            chartArea1.AxisY.Maximum = 1D;
            chartArea1.AxisY.Minimum = 0D;
            chartArea1.AxisY.ScaleView.MinSizeType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea1.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea1.BorderColor = System.Drawing.Color.DarkGray;
            chartArea1.InnerPlotPosition.Auto = false;
            chartArea1.InnerPlotPosition.Height = 80F;
            chartArea1.InnerPlotPosition.Width = 99F;
            chartArea1.InnerPlotPosition.X = 1F;
            chartArea1.InnerPlotPosition.Y = 1F;
            chartArea1.Name = "TachyEvents";
            this.chartAllEvents.ChartAreas.Add(chartArea1);
            this.chartAllEvents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartAllEvents.Location = new System.Drawing.Point(65, 0);
            this.chartAllEvents.Name = "chartAllEvents";
            series1.BorderWidth = 2;
            series1.ChartArea = "TachyEvents";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series1.Color = System.Drawing.Color.Blue;
            series1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series1.IsVisibleInLegend = false;
            series1.MarkerSize = 3;
            series1.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series1.Name = "Series1";
            series1.Points.Add(dataPoint1);
            series1.Points.Add(dataPoint2);
            series1.Points.Add(dataPoint3);
            series1.Points.Add(dataPoint4);
            series1.SmartLabelStyle.Enabled = false;
            series1.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series2.BorderWidth = 2;
            series2.ChartArea = "TachyEvents";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series2.Color = System.Drawing.Color.DimGray;
            series2.Name = "Series2";
            this.chartAllEvents.Series.Add(series1);
            this.chartAllEvents.Series.Add(series2);
            this.chartAllEvents.Size = new System.Drawing.Size(1219, 30);
            this.chartAllEvents.TabIndex = 1;
            this.chartAllEvents.Text = "chart1";
            this.chartAllEvents.MouseDown += new System.Windows.Forms.MouseEventHandler(this.chartAllEvents_MouseDown);
            // 
            // panel61
            // 
            this.panel61.Controls.Add(this.label25);
            this.panel61.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel61.Location = new System.Drawing.Point(0, 0);
            this.panel61.Name = "panel61";
            this.panel61.Size = new System.Drawing.Size(65, 30);
            this.panel61.TabIndex = 0;
            // 
            // label25
            // 
            this.label25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label25.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label25.Location = new System.Drawing.Point(0, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(65, 30);
            this.label25.TabIndex = 0;
            this.label25.Text = "All";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel55
            // 
            this.panel55.Controls.Add(this.chartManual);
            this.panel55.Controls.Add(this.panel56);
            this.panel55.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel55.Location = new System.Drawing.Point(0, 286);
            this.panel55.Name = "panel55";
            this.panel55.Size = new System.Drawing.Size(1284, 26);
            this.panel55.TabIndex = 8;
            // 
            // chartManual
            // 
            chartArea2.AxisX.IsLabelAutoFit = false;
            chartArea2.AxisX.LabelStyle.Enabled = false;
            chartArea2.AxisX.LineColor = System.Drawing.Color.LightGray;
            chartArea2.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea2.AxisX.MajorTickMark.Enabled = false;
            chartArea2.AxisX.ScaleBreakStyle.BreakLineStyle = System.Windows.Forms.DataVisualization.Charting.BreakLineStyle.None;
            chartArea2.AxisX.ScaleBreakStyle.LineColor = System.Drawing.Color.White;
            chartArea2.AxisX.ScaleBreakStyle.Spacing = 1D;
            chartArea2.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea2.AxisY.IsMarksNextToAxis = false;
            chartArea2.AxisY.LabelStyle.Enabled = false;
            chartArea2.AxisY.LineColor = System.Drawing.Color.LightGray;
            chartArea2.AxisY.MajorGrid.Enabled = false;
            chartArea2.AxisY.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea2.AxisY.MajorTickMark.Enabled = false;
            chartArea2.AxisY.Maximum = 1D;
            chartArea2.AxisY.Minimum = 0D;
            chartArea2.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea2.BorderColor = System.Drawing.Color.DarkGray;
            chartArea2.InnerPlotPosition.Auto = false;
            chartArea2.InnerPlotPosition.Height = 80F;
            chartArea2.InnerPlotPosition.Width = 99F;
            chartArea2.InnerPlotPosition.X = 1F;
            chartArea2.InnerPlotPosition.Y = 1F;
            chartArea2.Name = "TachyEvents";
            this.chartManual.ChartAreas.Add(chartArea2);
            this.chartManual.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartManual.Location = new System.Drawing.Point(65, 0);
            this.chartManual.Name = "chartManual";
            series3.ChartArea = "TachyEvents";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series3.Color = System.Drawing.Color.Red;
            series3.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series3.IsVisibleInLegend = false;
            series3.MarkerSize = 3;
            series3.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series3.Name = "Series1";
            series3.Points.Add(dataPoint5);
            series3.Points.Add(dataPoint6);
            series3.Points.Add(dataPoint7);
            series3.Points.Add(dataPoint8);
            series3.SmartLabelStyle.Enabled = false;
            series3.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series4.ChartArea = "TachyEvents";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series4.Color = System.Drawing.Color.DimGray;
            series4.Name = "Series2";
            this.chartManual.Series.Add(series3);
            this.chartManual.Series.Add(series4);
            this.chartManual.Size = new System.Drawing.Size(1219, 26);
            this.chartManual.TabIndex = 1;
            this.chartManual.Text = "chart1";
            this.chartManual.MouseDown += new System.Windows.Forms.MouseEventHandler(this.chartManual_MouseDown);
            // 
            // panel56
            // 
            this.panel56.Controls.Add(this.label54);
            this.panel56.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel56.Location = new System.Drawing.Point(0, 0);
            this.panel56.Name = "panel56";
            this.panel56.Size = new System.Drawing.Size(65, 26);
            this.panel56.TabIndex = 0;
            // 
            // label54
            // 
            this.label54.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label54.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label54.Location = new System.Drawing.Point(0, 0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(65, 26);
            this.label54.TabIndex = 0;
            this.label54.Text = "Manual";
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel36
            // 
            this.panel36.Controls.Add(this.chartOther);
            this.panel36.Controls.Add(this.panel37);
            this.panel36.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel36.Location = new System.Drawing.Point(0, 260);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(1284, 26);
            this.panel36.TabIndex = 14;
            // 
            // chartOther
            // 
            chartArea3.AxisX.IsLabelAutoFit = false;
            chartArea3.AxisX.LabelStyle.Enabled = false;
            chartArea3.AxisX.LineColor = System.Drawing.Color.DarkGray;
            chartArea3.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea3.AxisX.MinorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea3.AxisX.ScaleBreakStyle.BreakLineStyle = System.Windows.Forms.DataVisualization.Charting.BreakLineStyle.None;
            chartArea3.AxisX.ScaleBreakStyle.LineColor = System.Drawing.Color.White;
            chartArea3.AxisX.ScaleBreakStyle.Spacing = 1D;
            chartArea3.AxisX2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea3.AxisX2.MajorGrid.Enabled = false;
            chartArea3.AxisX2.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea3.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea3.AxisY.IsLabelAutoFit = false;
            chartArea3.AxisY.LabelStyle.Enabled = false;
            chartArea3.AxisY.LineColor = System.Drawing.Color.LightGray;
            chartArea3.AxisY.MajorGrid.Enabled = false;
            chartArea3.AxisY.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea3.AxisY.MajorTickMark.Enabled = false;
            chartArea3.AxisY.Maximum = 1D;
            chartArea3.AxisY.Minimum = 0D;
            chartArea3.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea3.BorderColor = System.Drawing.Color.DarkGray;
            chartArea3.InnerPlotPosition.Auto = false;
            chartArea3.InnerPlotPosition.Height = 80F;
            chartArea3.InnerPlotPosition.Width = 99F;
            chartArea3.InnerPlotPosition.X = 1F;
            chartArea3.InnerPlotPosition.Y = 1F;
            chartArea3.Name = "TachyEvents";
            this.chartOther.ChartAreas.Add(chartArea3);
            this.chartOther.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartOther.Location = new System.Drawing.Point(65, 0);
            this.chartOther.Name = "chartOther";
            series5.BorderWidth = 2;
            series5.ChartArea = "TachyEvents";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series5.Color = System.Drawing.Color.Blue;
            series5.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series5.IsVisibleInLegend = false;
            series5.MarkerSize = 3;
            series5.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series5.Name = "Series1";
            series5.Points.Add(dataPoint9);
            series5.Points.Add(dataPoint10);
            series5.Points.Add(dataPoint11);
            series5.Points.Add(dataPoint12);
            series5.SmartLabelStyle.Enabled = false;
            series5.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series6.BorderWidth = 2;
            series6.ChartArea = "TachyEvents";
            series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series6.Color = System.Drawing.Color.DimGray;
            series6.Name = "Series2";
            this.chartOther.Series.Add(series5);
            this.chartOther.Series.Add(series6);
            this.chartOther.Size = new System.Drawing.Size(1219, 26);
            this.chartOther.TabIndex = 1;
            this.chartOther.Text = "chart1";
            this.chartOther.MouseDown += new System.Windows.Forms.MouseEventHandler(this.chartOther_MouseDown);
            // 
            // panel37
            // 
            this.panel37.Controls.Add(this.label42);
            this.panel37.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel37.Location = new System.Drawing.Point(0, 0);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(65, 26);
            this.panel37.TabIndex = 0;
            // 
            // label42
            // 
            this.label42.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label42.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label42.Location = new System.Drawing.Point(0, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(65, 26);
            this.label42.TabIndex = 0;
            this.label42.Text = "Other";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.chartAbN);
            this.panel19.Controls.Add(this.panel20);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel19.Location = new System.Drawing.Point(0, 234);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(1284, 26);
            this.panel19.TabIndex = 15;
            // 
            // chartAbN
            // 
            chartArea4.AxisX.IsLabelAutoFit = false;
            chartArea4.AxisX.LabelStyle.Enabled = false;
            chartArea4.AxisX.LineColor = System.Drawing.Color.DarkGray;
            chartArea4.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea4.AxisX.MinorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea4.AxisX.ScaleBreakStyle.BreakLineStyle = System.Windows.Forms.DataVisualization.Charting.BreakLineStyle.None;
            chartArea4.AxisX.ScaleBreakStyle.LineColor = System.Drawing.Color.White;
            chartArea4.AxisX.ScaleBreakStyle.Spacing = 1D;
            chartArea4.AxisX2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea4.AxisX2.MajorGrid.Enabled = false;
            chartArea4.AxisX2.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea4.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea4.AxisY.IsLabelAutoFit = false;
            chartArea4.AxisY.LabelStyle.Enabled = false;
            chartArea4.AxisY.LineColor = System.Drawing.Color.LightGray;
            chartArea4.AxisY.MajorGrid.Enabled = false;
            chartArea4.AxisY.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea4.AxisY.MajorTickMark.Enabled = false;
            chartArea4.AxisY.Maximum = 1D;
            chartArea4.AxisY.Minimum = 0D;
            chartArea4.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea4.BorderColor = System.Drawing.Color.DarkGray;
            chartArea4.InnerPlotPosition.Auto = false;
            chartArea4.InnerPlotPosition.Height = 80F;
            chartArea4.InnerPlotPosition.Width = 99F;
            chartArea4.InnerPlotPosition.X = 1F;
            chartArea4.InnerPlotPosition.Y = 1F;
            chartArea4.Name = "TachyEvents";
            this.chartAbN.ChartAreas.Add(chartArea4);
            this.chartAbN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartAbN.Location = new System.Drawing.Point(65, 0);
            this.chartAbN.Name = "chartAbN";
            series7.BorderWidth = 2;
            series7.ChartArea = "TachyEvents";
            series7.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series7.Color = System.Drawing.Color.Blue;
            series7.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series7.IsVisibleInLegend = false;
            series7.MarkerSize = 3;
            series7.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series7.Name = "Series1";
            series7.Points.Add(dataPoint13);
            series7.Points.Add(dataPoint14);
            series7.Points.Add(dataPoint15);
            series7.Points.Add(dataPoint16);
            series7.SmartLabelStyle.Enabled = false;
            series7.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series8.BorderWidth = 2;
            series8.ChartArea = "TachyEvents";
            series8.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series8.Color = System.Drawing.Color.DimGray;
            series8.Name = "Series2";
            this.chartAbN.Series.Add(series7);
            this.chartAbN.Series.Add(series8);
            this.chartAbN.Size = new System.Drawing.Size(1219, 26);
            this.chartAbN.TabIndex = 1;
            this.chartAbN.Text = "chart1";
            this.chartAbN.MouseDown += new System.Windows.Forms.MouseEventHandler(this.chartAbN_MouseDown);
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.label67);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel20.Location = new System.Drawing.Point(0, 0);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(65, 26);
            this.panel20.TabIndex = 0;
            // 
            // label67
            // 
            this.label67.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label67.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label67.Location = new System.Drawing.Point(0, 0);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(65, 26);
            this.label67.TabIndex = 0;
            this.label67.Text = "AbNormal";
            this.label67.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel33
            // 
            this.panel33.Controls.Add(this.chartVtach);
            this.panel33.Controls.Add(this.panel35);
            this.panel33.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel33.Location = new System.Drawing.Point(0, 208);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(1284, 26);
            this.panel33.TabIndex = 13;
            // 
            // chartVtach
            // 
            chartArea5.AxisX.IsLabelAutoFit = false;
            chartArea5.AxisX.LabelStyle.Enabled = false;
            chartArea5.AxisX.LineColor = System.Drawing.Color.DarkGray;
            chartArea5.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea5.AxisX.MinorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea5.AxisX.ScaleBreakStyle.BreakLineStyle = System.Windows.Forms.DataVisualization.Charting.BreakLineStyle.None;
            chartArea5.AxisX.ScaleBreakStyle.LineColor = System.Drawing.Color.White;
            chartArea5.AxisX.ScaleBreakStyle.Spacing = 1D;
            chartArea5.AxisX2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea5.AxisX2.MajorGrid.Enabled = false;
            chartArea5.AxisX2.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea5.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea5.AxisY.IsLabelAutoFit = false;
            chartArea5.AxisY.LabelStyle.Enabled = false;
            chartArea5.AxisY.LineColor = System.Drawing.Color.LightGray;
            chartArea5.AxisY.MajorGrid.Enabled = false;
            chartArea5.AxisY.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea5.AxisY.MajorTickMark.Enabled = false;
            chartArea5.AxisY.Maximum = 1D;
            chartArea5.AxisY.Minimum = 0D;
            chartArea5.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea5.BorderColor = System.Drawing.Color.DarkGray;
            chartArea5.InnerPlotPosition.Auto = false;
            chartArea5.InnerPlotPosition.Height = 80F;
            chartArea5.InnerPlotPosition.Width = 99F;
            chartArea5.InnerPlotPosition.X = 1F;
            chartArea5.InnerPlotPosition.Y = 1F;
            chartArea5.Name = "TachyEvents";
            this.chartVtach.ChartAreas.Add(chartArea5);
            this.chartVtach.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartVtach.Location = new System.Drawing.Point(65, 0);
            this.chartVtach.Name = "chartVtach";
            series9.BorderWidth = 2;
            series9.ChartArea = "TachyEvents";
            series9.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series9.Color = System.Drawing.Color.Blue;
            series9.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series9.IsVisibleInLegend = false;
            series9.MarkerSize = 3;
            series9.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series9.Name = "Series1";
            series9.Points.Add(dataPoint17);
            series9.Points.Add(dataPoint18);
            series9.Points.Add(dataPoint19);
            series9.Points.Add(dataPoint20);
            series9.SmartLabelStyle.Enabled = false;
            series9.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series10.BorderWidth = 2;
            series10.ChartArea = "TachyEvents";
            series10.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series10.Color = System.Drawing.Color.DimGray;
            series10.Name = "Series2";
            this.chartVtach.Series.Add(series9);
            this.chartVtach.Series.Add(series10);
            this.chartVtach.Size = new System.Drawing.Size(1219, 26);
            this.chartVtach.TabIndex = 1;
            this.chartVtach.Text = "chart1";
            this.chartVtach.MouseDown += new System.Windows.Forms.MouseEventHandler(this.chartVtach_MouseDown);
            // 
            // panel35
            // 
            this.panel35.Controls.Add(this.label39);
            this.panel35.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel35.Location = new System.Drawing.Point(0, 0);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(65, 26);
            this.panel35.TabIndex = 0;
            // 
            // label39
            // 
            this.label39.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label39.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label39.Location = new System.Drawing.Point(0, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(65, 26);
            this.label39.TabIndex = 0;
            this.label39.Text = "VTach";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel53
            // 
            this.panel53.Controls.Add(this.chartPace);
            this.panel53.Controls.Add(this.panel54);
            this.panel53.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel53.Location = new System.Drawing.Point(0, 182);
            this.panel53.Name = "panel53";
            this.panel53.Size = new System.Drawing.Size(1284, 26);
            this.panel53.TabIndex = 7;
            // 
            // chartPace
            // 
            chartArea6.AxisX.IsLabelAutoFit = false;
            chartArea6.AxisX.LabelStyle.Enabled = false;
            chartArea6.AxisX.LineColor = System.Drawing.Color.LightGray;
            chartArea6.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea6.AxisX.MinorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea6.AxisX.ScaleBreakStyle.BreakLineStyle = System.Windows.Forms.DataVisualization.Charting.BreakLineStyle.None;
            chartArea6.AxisX.ScaleBreakStyle.LineColor = System.Drawing.Color.White;
            chartArea6.AxisX.ScaleBreakStyle.Spacing = 1D;
            chartArea6.AxisX2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea6.AxisX2.MajorGrid.Enabled = false;
            chartArea6.AxisX2.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea6.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea6.AxisY.IsLabelAutoFit = false;
            chartArea6.AxisY.LabelStyle.Enabled = false;
            chartArea6.AxisY.LineColor = System.Drawing.Color.LightGray;
            chartArea6.AxisY.MajorGrid.Enabled = false;
            chartArea6.AxisY.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea6.AxisY.MajorTickMark.Enabled = false;
            chartArea6.AxisY.Maximum = 1D;
            chartArea6.AxisY.Minimum = 0D;
            chartArea6.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea6.BorderColor = System.Drawing.Color.DarkGray;
            chartArea6.InnerPlotPosition.Auto = false;
            chartArea6.InnerPlotPosition.Height = 80F;
            chartArea6.InnerPlotPosition.Width = 99F;
            chartArea6.InnerPlotPosition.X = 1F;
            chartArea6.InnerPlotPosition.Y = 1F;
            chartArea6.Name = "TachyEvents";
            this.chartPace.ChartAreas.Add(chartArea6);
            this.chartPace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartPace.Location = new System.Drawing.Point(65, 0);
            this.chartPace.Name = "chartPace";
            series11.ChartArea = "TachyEvents";
            series11.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series11.Color = System.Drawing.Color.Blue;
            series11.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series11.IsVisibleInLegend = false;
            series11.MarkerSize = 3;
            series11.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series11.Name = "Series1";
            series11.Points.Add(dataPoint21);
            series11.Points.Add(dataPoint22);
            series11.Points.Add(dataPoint23);
            series11.Points.Add(dataPoint24);
            series11.SmartLabelStyle.Enabled = false;
            series11.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series12.ChartArea = "TachyEvents";
            series12.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series12.Color = System.Drawing.Color.DimGray;
            series12.Name = "Series2";
            this.chartPace.Series.Add(series11);
            this.chartPace.Series.Add(series12);
            this.chartPace.Size = new System.Drawing.Size(1219, 26);
            this.chartPace.TabIndex = 1;
            this.chartPace.Text = "chart1";
            this.chartPace.MouseDown += new System.Windows.Forms.MouseEventHandler(this.chartPace_MouseDown);
            // 
            // panel54
            // 
            this.panel54.Controls.Add(this.label38);
            this.panel54.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel54.Location = new System.Drawing.Point(0, 0);
            this.panel54.Name = "panel54";
            this.panel54.Size = new System.Drawing.Size(65, 26);
            this.panel54.TabIndex = 0;
            // 
            // label38
            // 
            this.label38.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label38.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label38.Location = new System.Drawing.Point(0, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(65, 26);
            this.label38.TabIndex = 0;
            this.label38.Text = "Pace";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel29
            // 
            this.panel29.Controls.Add(this.chartPVC);
            this.panel29.Controls.Add(this.panel30);
            this.panel29.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel29.Location = new System.Drawing.Point(0, 156);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(1284, 26);
            this.panel29.TabIndex = 11;
            // 
            // chartPVC
            // 
            chartArea7.AxisX.IsLabelAutoFit = false;
            chartArea7.AxisX.LabelStyle.Enabled = false;
            chartArea7.AxisX.LineColor = System.Drawing.Color.LightGray;
            chartArea7.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea7.AxisX.MinorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea7.AxisX.ScaleBreakStyle.BreakLineStyle = System.Windows.Forms.DataVisualization.Charting.BreakLineStyle.None;
            chartArea7.AxisX.ScaleBreakStyle.LineColor = System.Drawing.Color.White;
            chartArea7.AxisX.ScaleBreakStyle.Spacing = 1D;
            chartArea7.AxisX2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea7.AxisX2.MajorGrid.Enabled = false;
            chartArea7.AxisX2.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea7.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea7.AxisY.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea7.AxisY.IsLabelAutoFit = false;
            chartArea7.AxisY.LabelStyle.Enabled = false;
            chartArea7.AxisY.LineColor = System.Drawing.Color.LightGray;
            chartArea7.AxisY.MajorGrid.Enabled = false;
            chartArea7.AxisY.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea7.AxisY.MajorTickMark.Enabled = false;
            chartArea7.AxisY.Maximum = 1D;
            chartArea7.AxisY.Minimum = 0D;
            chartArea7.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea7.BorderColor = System.Drawing.Color.DarkGray;
            chartArea7.InnerPlotPosition.Auto = false;
            chartArea7.InnerPlotPosition.Height = 80F;
            chartArea7.InnerPlotPosition.Width = 99F;
            chartArea7.InnerPlotPosition.X = 1F;
            chartArea7.InnerPlotPosition.Y = 1F;
            chartArea7.Name = "TachyEvents";
            this.chartPVC.ChartAreas.Add(chartArea7);
            this.chartPVC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartPVC.Location = new System.Drawing.Point(65, 0);
            this.chartPVC.Name = "chartPVC";
            series13.ChartArea = "TachyEvents";
            series13.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series13.Color = System.Drawing.Color.Blue;
            series13.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series13.IsVisibleInLegend = false;
            series13.MarkerSize = 3;
            series13.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series13.Name = "Series1";
            series13.Points.Add(dataPoint25);
            series13.Points.Add(dataPoint26);
            series13.Points.Add(dataPoint27);
            series13.Points.Add(dataPoint28);
            series13.SmartLabelStyle.Enabled = false;
            series13.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series14.ChartArea = "TachyEvents";
            series14.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series14.Color = System.Drawing.Color.DimGray;
            series14.Name = "Series2";
            this.chartPVC.Series.Add(series13);
            this.chartPVC.Series.Add(series14);
            this.chartPVC.Size = new System.Drawing.Size(1219, 26);
            this.chartPVC.TabIndex = 1;
            this.chartPVC.Text = "chart1";
            this.chartPVC.Click += new System.EventHandler(this.chartPVC_Click);
            this.chartPVC.MouseDown += new System.Windows.Forms.MouseEventHandler(this.chartPVC_MouseDown);
            // 
            // panel30
            // 
            this.panel30.Controls.Add(this.label16);
            this.panel30.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel30.Location = new System.Drawing.Point(0, 0);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(65, 26);
            this.panel30.TabIndex = 0;
            // 
            // label16
            // 
            this.label16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label16.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(0, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(65, 26);
            this.label16.TabIndex = 0;
            this.label16.Text = "PVC-VEB";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.chartPAC);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 130);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1284, 26);
            this.panel2.TabIndex = 10;
            // 
            // chartPAC
            // 
            chartArea8.AxisX.IsLabelAutoFit = false;
            chartArea8.AxisX.LabelStyle.Enabled = false;
            chartArea8.AxisX.LineColor = System.Drawing.Color.LightGray;
            chartArea8.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea8.AxisX.MinorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea8.AxisX.ScaleBreakStyle.BreakLineStyle = System.Windows.Forms.DataVisualization.Charting.BreakLineStyle.None;
            chartArea8.AxisX.ScaleBreakStyle.LineColor = System.Drawing.Color.White;
            chartArea8.AxisX.ScaleBreakStyle.Spacing = 1D;
            chartArea8.AxisX2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea8.AxisX2.MajorGrid.Enabled = false;
            chartArea8.AxisX2.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea8.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea8.AxisY.IsLabelAutoFit = false;
            chartArea8.AxisY.LabelStyle.Enabled = false;
            chartArea8.AxisY.LineColor = System.Drawing.Color.LightGray;
            chartArea8.AxisY.MajorGrid.Enabled = false;
            chartArea8.AxisY.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea8.AxisY.MajorTickMark.Enabled = false;
            chartArea8.AxisY.Maximum = 1D;
            chartArea8.AxisY.Minimum = 0D;
            chartArea8.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea8.BorderColor = System.Drawing.Color.DarkGray;
            chartArea8.InnerPlotPosition.Auto = false;
            chartArea8.InnerPlotPosition.Height = 80F;
            chartArea8.InnerPlotPosition.Width = 99F;
            chartArea8.InnerPlotPosition.X = 1F;
            chartArea8.InnerPlotPosition.Y = 1F;
            chartArea8.Name = "TachyEvents";
            this.chartPAC.ChartAreas.Add(chartArea8);
            this.chartPAC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartPAC.Location = new System.Drawing.Point(65, 0);
            this.chartPAC.Name = "chartPAC";
            series15.ChartArea = "TachyEvents";
            series15.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series15.Color = System.Drawing.Color.Blue;
            series15.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series15.IsVisibleInLegend = false;
            series15.MarkerSize = 3;
            series15.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series15.Name = "Series1";
            series15.Points.Add(dataPoint29);
            series15.Points.Add(dataPoint30);
            series15.Points.Add(dataPoint31);
            series15.Points.Add(dataPoint32);
            series15.SmartLabelStyle.Enabled = false;
            series15.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series16.ChartArea = "TachyEvents";
            series16.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series16.Color = System.Drawing.Color.DimGray;
            series16.Name = "Series2";
            this.chartPAC.Series.Add(series15);
            this.chartPAC.Series.Add(series16);
            this.chartPAC.Size = new System.Drawing.Size(1219, 26);
            this.chartPAC.TabIndex = 1;
            this.chartPAC.Text = "chart1";
            this.chartPAC.MouseDown += new System.Windows.Forms.MouseEventHandler(this.chartPAC_MouseDown);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(65, 26);
            this.panel4.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "PAC-SVEB";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel31
            // 
            this.panel31.Controls.Add(this.chartAflut);
            this.panel31.Controls.Add(this.panel32);
            this.panel31.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel31.Location = new System.Drawing.Point(0, 104);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(1284, 26);
            this.panel31.TabIndex = 12;
            // 
            // chartAflut
            // 
            chartArea9.AxisX.IsLabelAutoFit = false;
            chartArea9.AxisX.LabelStyle.Enabled = false;
            chartArea9.AxisX.LineColor = System.Drawing.Color.LightGray;
            chartArea9.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea9.AxisX.MinorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea9.AxisX.ScaleBreakStyle.BreakLineStyle = System.Windows.Forms.DataVisualization.Charting.BreakLineStyle.None;
            chartArea9.AxisX.ScaleBreakStyle.LineColor = System.Drawing.Color.White;
            chartArea9.AxisX.ScaleBreakStyle.Spacing = 1D;
            chartArea9.AxisX2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea9.AxisX2.MajorGrid.Enabled = false;
            chartArea9.AxisX2.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea9.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea9.AxisY.IsLabelAutoFit = false;
            chartArea9.AxisY.LabelStyle.Enabled = false;
            chartArea9.AxisY.LineColor = System.Drawing.Color.LightGray;
            chartArea9.AxisY.MajorGrid.Enabled = false;
            chartArea9.AxisY.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea9.AxisY.MajorTickMark.Enabled = false;
            chartArea9.AxisY.Maximum = 1D;
            chartArea9.AxisY.Minimum = 0D;
            chartArea9.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea9.BorderColor = System.Drawing.Color.DarkGray;
            chartArea9.InnerPlotPosition.Auto = false;
            chartArea9.InnerPlotPosition.Height = 80F;
            chartArea9.InnerPlotPosition.Width = 99F;
            chartArea9.InnerPlotPosition.X = 1F;
            chartArea9.InnerPlotPosition.Y = 1F;
            chartArea9.Name = "TachyEvents";
            this.chartAflut.ChartAreas.Add(chartArea9);
            this.chartAflut.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartAflut.Location = new System.Drawing.Point(65, 0);
            this.chartAflut.Name = "chartAflut";
            series17.BorderWidth = 2;
            series17.ChartArea = "TachyEvents";
            series17.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series17.Color = System.Drawing.Color.Blue;
            series17.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series17.IsVisibleInLegend = false;
            series17.MarkerSize = 3;
            series17.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series17.Name = "Series1";
            series17.Points.Add(dataPoint33);
            series17.Points.Add(dataPoint34);
            series17.Points.Add(dataPoint35);
            series17.Points.Add(dataPoint36);
            series17.SmartLabelStyle.Enabled = false;
            series17.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series18.BorderWidth = 2;
            series18.ChartArea = "TachyEvents";
            series18.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series18.Color = System.Drawing.Color.DimGray;
            series18.Name = "Series2";
            this.chartAflut.Series.Add(series17);
            this.chartAflut.Series.Add(series18);
            this.chartAflut.Size = new System.Drawing.Size(1219, 26);
            this.chartAflut.TabIndex = 1;
            this.chartAflut.Text = "chart1";
            this.chartAflut.MouseDown += new System.Windows.Forms.MouseEventHandler(this.chartAflut_MouseDown);
            // 
            // panel32
            // 
            this.panel32.Controls.Add(this.label30);
            this.panel32.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel32.Location = new System.Drawing.Point(0, 0);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(65, 26);
            this.panel32.TabIndex = 0;
            // 
            // label30
            // 
            this.label30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label30.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label30.Location = new System.Drawing.Point(0, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(65, 26);
            this.label30.TabIndex = 0;
            this.label30.Text = "AFlut";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel51
            // 
            this.panel51.Controls.Add(this.chartAfib);
            this.panel51.Controls.Add(this.panel52);
            this.panel51.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel51.Location = new System.Drawing.Point(0, 78);
            this.panel51.Name = "panel51";
            this.panel51.Size = new System.Drawing.Size(1284, 26);
            this.panel51.TabIndex = 6;
            // 
            // chartAfib
            // 
            chartArea10.AxisX.IsLabelAutoFit = false;
            chartArea10.AxisX.LabelStyle.Enabled = false;
            chartArea10.AxisX.LineColor = System.Drawing.Color.DarkGray;
            chartArea10.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea10.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DarkGray;
            chartArea10.AxisX.ScaleBreakStyle.BreakLineStyle = System.Windows.Forms.DataVisualization.Charting.BreakLineStyle.None;
            chartArea10.AxisX.ScaleBreakStyle.LineColor = System.Drawing.Color.White;
            chartArea10.AxisX.ScaleBreakStyle.Spacing = 1D;
            chartArea10.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea10.AxisY.IsLabelAutoFit = false;
            chartArea10.AxisY.LabelStyle.Enabled = false;
            chartArea10.AxisY.LineColor = System.Drawing.Color.LightGray;
            chartArea10.AxisY.MajorGrid.Enabled = false;
            chartArea10.AxisY.MajorTickMark.Enabled = false;
            chartArea10.AxisY.Maximum = 1D;
            chartArea10.AxisY.Minimum = 0D;
            chartArea10.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea10.BorderColor = System.Drawing.Color.DarkGray;
            chartArea10.InnerPlotPosition.Auto = false;
            chartArea10.InnerPlotPosition.Height = 80F;
            chartArea10.InnerPlotPosition.Width = 99F;
            chartArea10.InnerPlotPosition.X = 1F;
            chartArea10.InnerPlotPosition.Y = 1F;
            chartArea10.Name = "TachyEvents";
            this.chartAfib.ChartAreas.Add(chartArea10);
            this.chartAfib.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartAfib.Location = new System.Drawing.Point(65, 0);
            this.chartAfib.Name = "chartAfib";
            series19.BorderWidth = 2;
            series19.ChartArea = "TachyEvents";
            series19.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series19.Color = System.Drawing.Color.Blue;
            series19.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series19.IsVisibleInLegend = false;
            series19.MarkerSize = 3;
            series19.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series19.Name = "Series1";
            series19.Points.Add(dataPoint37);
            series19.Points.Add(dataPoint38);
            series19.Points.Add(dataPoint39);
            series19.Points.Add(dataPoint40);
            series19.SmartLabelStyle.Enabled = false;
            series19.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series20.BorderWidth = 2;
            series20.ChartArea = "TachyEvents";
            series20.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series20.Color = System.Drawing.Color.DimGray;
            series20.Name = "Series2";
            this.chartAfib.Series.Add(series19);
            this.chartAfib.Series.Add(series20);
            this.chartAfib.Size = new System.Drawing.Size(1219, 26);
            this.chartAfib.TabIndex = 1;
            this.chartAfib.Text = "chart1";
            this.chartAfib.MouseDown += new System.Windows.Forms.MouseEventHandler(this.chartAfib_MouseDown);
            // 
            // panel52
            // 
            this.panel52.Controls.Add(this.label36);
            this.panel52.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel52.Location = new System.Drawing.Point(0, 0);
            this.panel52.Name = "panel52";
            this.panel52.Size = new System.Drawing.Size(65, 26);
            this.panel52.TabIndex = 0;
            // 
            // label36
            // 
            this.label36.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label36.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label36.Location = new System.Drawing.Point(0, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(65, 26);
            this.label36.TabIndex = 0;
            this.label36.Text = "AFib";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel49
            // 
            this.panel49.Controls.Add(this.chartPause);
            this.panel49.Controls.Add(this.panel50);
            this.panel49.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel49.Location = new System.Drawing.Point(0, 52);
            this.panel49.Name = "panel49";
            this.panel49.Size = new System.Drawing.Size(1284, 26);
            this.panel49.TabIndex = 5;
            // 
            // chartPause
            // 
            chartArea11.AxisX.IsLabelAutoFit = false;
            chartArea11.AxisX.LabelStyle.Enabled = false;
            chartArea11.AxisX.LineColor = System.Drawing.Color.DarkGray;
            chartArea11.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea11.AxisX.ScaleBreakStyle.BreakLineStyle = System.Windows.Forms.DataVisualization.Charting.BreakLineStyle.None;
            chartArea11.AxisX.ScaleBreakStyle.LineColor = System.Drawing.Color.White;
            chartArea11.AxisX.ScaleBreakStyle.Spacing = 1D;
            chartArea11.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea11.AxisY.IsLabelAutoFit = false;
            chartArea11.AxisY.LabelStyle.Enabled = false;
            chartArea11.AxisY.LineColor = System.Drawing.Color.LightGray;
            chartArea11.AxisY.MajorGrid.Enabled = false;
            chartArea11.AxisY.MajorTickMark.Enabled = false;
            chartArea11.AxisY.Maximum = 1D;
            chartArea11.AxisY.Minimum = 0D;
            chartArea11.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea11.InnerPlotPosition.Auto = false;
            chartArea11.InnerPlotPosition.Height = 80F;
            chartArea11.InnerPlotPosition.Width = 99F;
            chartArea11.InnerPlotPosition.X = 1F;
            chartArea11.InnerPlotPosition.Y = 1F;
            chartArea11.Name = "TachyEvents";
            this.chartPause.ChartAreas.Add(chartArea11);
            this.chartPause.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartPause.Location = new System.Drawing.Point(65, 0);
            this.chartPause.Name = "chartPause";
            series21.BorderWidth = 2;
            series21.ChartArea = "TachyEvents";
            series21.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series21.Color = System.Drawing.Color.Gray;
            series21.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series21.IsVisibleInLegend = false;
            series21.MarkerSize = 3;
            series21.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series21.Name = "Series1";
            series21.Points.Add(dataPoint41);
            series21.Points.Add(dataPoint42);
            series21.Points.Add(dataPoint43);
            series21.Points.Add(dataPoint44);
            series21.SmartLabelStyle.Enabled = false;
            series21.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series22.BorderWidth = 2;
            series22.ChartArea = "TachyEvents";
            series22.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series22.Color = System.Drawing.Color.DimGray;
            series22.Name = "Series2";
            this.chartPause.Series.Add(series21);
            this.chartPause.Series.Add(series22);
            this.chartPause.Size = new System.Drawing.Size(1219, 26);
            this.chartPause.TabIndex = 1;
            this.chartPause.Text = "chart1";
            this.chartPause.MouseDown += new System.Windows.Forms.MouseEventHandler(this.chartPause_MouseDown);
            // 
            // panel50
            // 
            this.panel50.Controls.Add(this.label34);
            this.panel50.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel50.Location = new System.Drawing.Point(0, 0);
            this.panel50.Name = "panel50";
            this.panel50.Size = new System.Drawing.Size(65, 26);
            this.panel50.TabIndex = 0;
            // 
            // label34
            // 
            this.label34.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label34.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label34.Location = new System.Drawing.Point(0, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(65, 26);
            this.label34.TabIndex = 0;
            this.label34.Text = "Pause";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel44
            // 
            this.panel44.Controls.Add(this.chartBrady);
            this.panel44.Controls.Add(this.panel48);
            this.panel44.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel44.Location = new System.Drawing.Point(0, 26);
            this.panel44.Name = "panel44";
            this.panel44.Size = new System.Drawing.Size(1284, 26);
            this.panel44.TabIndex = 4;
            // 
            // chartBrady
            // 
            chartArea12.AxisX.IsLabelAutoFit = false;
            chartArea12.AxisX.LabelStyle.Enabled = false;
            chartArea12.AxisX.LineColor = System.Drawing.Color.DarkGray;
            chartArea12.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea12.AxisX.ScaleBreakStyle.BreakLineStyle = System.Windows.Forms.DataVisualization.Charting.BreakLineStyle.None;
            chartArea12.AxisX.ScaleBreakStyle.LineColor = System.Drawing.Color.White;
            chartArea12.AxisX.ScaleBreakStyle.Spacing = 1D;
            chartArea12.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea12.AxisY.IsLabelAutoFit = false;
            chartArea12.AxisY.LabelStyle.Enabled = false;
            chartArea12.AxisY.LineColor = System.Drawing.Color.LightGray;
            chartArea12.AxisY.MajorGrid.Enabled = false;
            chartArea12.AxisY.MajorTickMark.Enabled = false;
            chartArea12.AxisY.Maximum = 1D;
            chartArea12.AxisY.Minimum = 0D;
            chartArea12.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea12.BorderColor = System.Drawing.Color.DarkGray;
            chartArea12.InnerPlotPosition.Auto = false;
            chartArea12.InnerPlotPosition.Height = 80F;
            chartArea12.InnerPlotPosition.Width = 99F;
            chartArea12.InnerPlotPosition.X = 1F;
            chartArea12.InnerPlotPosition.Y = 1F;
            chartArea12.Name = "TachyEvents";
            this.chartBrady.ChartAreas.Add(chartArea12);
            this.chartBrady.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartBrady.Location = new System.Drawing.Point(65, 0);
            this.chartBrady.Name = "chartBrady";
            series23.BorderWidth = 2;
            series23.ChartArea = "TachyEvents";
            series23.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series23.Color = System.Drawing.Color.Green;
            series23.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series23.IsVisibleInLegend = false;
            series23.MarkerSize = 3;
            series23.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series23.Name = "Series1";
            series23.Points.Add(dataPoint45);
            series23.Points.Add(dataPoint46);
            series23.Points.Add(dataPoint47);
            series23.Points.Add(dataPoint48);
            series23.SmartLabelStyle.Enabled = false;
            series23.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series24.BorderWidth = 2;
            series24.ChartArea = "TachyEvents";
            series24.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series24.Color = System.Drawing.Color.DimGray;
            series24.Name = "Series2";
            this.chartBrady.Series.Add(series23);
            this.chartBrady.Series.Add(series24);
            this.chartBrady.Size = new System.Drawing.Size(1219, 26);
            this.chartBrady.TabIndex = 1;
            this.chartBrady.Text = "chart1";
            this.chartBrady.Click += new System.EventHandler(this.chartBrady_Click);
            this.chartBrady.MouseDown += new System.Windows.Forms.MouseEventHandler(this.chartBrady_MouseDown);
            // 
            // panel48
            // 
            this.panel48.Controls.Add(this.label32);
            this.panel48.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel48.Location = new System.Drawing.Point(0, 0);
            this.panel48.Name = "panel48";
            this.panel48.Size = new System.Drawing.Size(65, 26);
            this.panel48.TabIndex = 0;
            // 
            // label32
            // 
            this.label32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label32.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label32.Location = new System.Drawing.Point(0, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(65, 26);
            this.label32.TabIndex = 0;
            this.label32.Text = "Brady";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.chartTachy);
            this.panel9.Controls.Add(this.panel43);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(1284, 26);
            this.panel9.TabIndex = 3;
            // 
            // chartTachy
            // 
            chartArea13.AxisX.IsLabelAutoFit = false;
            chartArea13.AxisX.LabelStyle.Enabled = false;
            chartArea13.AxisX.LineColor = System.Drawing.Color.DarkGray;
            chartArea13.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea13.AxisX.ScaleBreakStyle.BreakLineStyle = System.Windows.Forms.DataVisualization.Charting.BreakLineStyle.None;
            chartArea13.AxisX.ScaleBreakStyle.LineColor = System.Drawing.Color.White;
            chartArea13.AxisX.ScaleBreakStyle.Spacing = 1D;
            chartArea13.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea13.AxisY.IsLabelAutoFit = false;
            chartArea13.AxisY.LabelStyle.Enabled = false;
            chartArea13.AxisY.LineColor = System.Drawing.Color.LightGray;
            chartArea13.AxisY.MajorGrid.Enabled = false;
            chartArea13.AxisY.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea13.AxisY.MajorTickMark.Enabled = false;
            chartArea13.AxisY.Maximum = 1D;
            chartArea13.AxisY.Minimum = 0D;
            chartArea13.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea13.BorderColor = System.Drawing.Color.DarkGray;
            chartArea13.InnerPlotPosition.Auto = false;
            chartArea13.InnerPlotPosition.Height = 80F;
            chartArea13.InnerPlotPosition.Width = 99F;
            chartArea13.InnerPlotPosition.X = 1F;
            chartArea13.InnerPlotPosition.Y = 1F;
            chartArea13.Name = "TachyEvents";
            this.chartTachy.ChartAreas.Add(chartArea13);
            this.chartTachy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartTachy.Location = new System.Drawing.Point(65, 0);
            this.chartTachy.Name = "chartTachy";
            series25.BorderWidth = 2;
            series25.ChartArea = "TachyEvents";
            series25.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series25.Color = System.Drawing.Color.Orange;
            series25.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series25.IsVisibleInLegend = false;
            series25.MarkerSize = 3;
            series25.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series25.Name = "Series1";
            series25.Points.Add(dataPoint49);
            series25.Points.Add(dataPoint50);
            series25.Points.Add(dataPoint51);
            series25.Points.Add(dataPoint52);
            series25.SmartLabelStyle.Enabled = false;
            series25.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series26.BorderWidth = 2;
            series26.ChartArea = "TachyEvents";
            series26.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series26.Color = System.Drawing.Color.DimGray;
            series26.Name = "Series2";
            this.chartTachy.Series.Add(series25);
            this.chartTachy.Series.Add(series26);
            this.chartTachy.Size = new System.Drawing.Size(1219, 26);
            this.chartTachy.TabIndex = 1;
            this.chartTachy.Text = "chart2";
            this.chartTachy.MouseDown += new System.Windows.Forms.MouseEventHandler(this.chartTachy_MouseDown);
            // 
            // panel43
            // 
            this.panel43.Controls.Add(this.label53);
            this.panel43.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel43.Location = new System.Drawing.Point(0, 0);
            this.panel43.Name = "panel43";
            this.panel43.Size = new System.Drawing.Size(65, 26);
            this.panel43.TabIndex = 0;
            // 
            // label53
            // 
            this.label53.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label53.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label53.Location = new System.Drawing.Point(0, 0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(65, 26);
            this.label53.TabIndex = 0;
            this.label53.Text = "Tachy";
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.chartHR);
            this.panel8.Controls.Add(this.panel14);
            this.panel8.Controls.Add(this.panel12);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(0, 1);
            this.panel8.MinimumSize = new System.Drawing.Size(300, 100);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(1286, 153);
            this.panel8.TabIndex = 1;
            // 
            // chartHR
            // 
            this.chartHR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.chartHR.BorderlineWidth = 0;
            chartArea14.AxisX.LabelAutoFitStyle = ((System.Windows.Forms.DataVisualization.Charting.LabelAutoFitStyles)(((((System.Windows.Forms.DataVisualization.Charting.LabelAutoFitStyles.IncreaseFont | System.Windows.Forms.DataVisualization.Charting.LabelAutoFitStyles.DecreaseFont) 
            | System.Windows.Forms.DataVisualization.Charting.LabelAutoFitStyles.StaggeredLabels) 
            | System.Windows.Forms.DataVisualization.Charting.LabelAutoFitStyles.LabelsAngleStep90) 
            | System.Windows.Forms.DataVisualization.Charting.LabelAutoFitStyles.WordWrap)));
            chartArea14.AxisX.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea14.AxisX.LineColor = System.Drawing.Color.DarkGray;
            chartArea14.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea14.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DarkGray;
            chartArea14.AxisX.Maximum = 250D;
            chartArea14.AxisX.Minimum = 100D;
            chartArea14.AxisX2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea14.AxisY.LineColor = System.Drawing.Color.DarkGray;
            chartArea14.AxisY.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea14.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DarkGray;
            customLabel1.Text = "    ";
            chartArea14.AxisY2.CustomLabels.Add(customLabel1);
            chartArea14.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.True;
            chartArea14.AxisY2.IntervalOffsetType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea14.AxisY2.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea14.AxisY2.IsLabelAutoFit = false;
            chartArea14.AxisY2.MajorGrid.Enabled = false;
            chartArea14.AxisY2.MajorTickMark.Enabled = false;
            chartArea14.AxisY2.Maximum = 10D;
            chartArea14.AxisY2.MaximumAutoSize = 50F;
            chartArea14.AxisY2.Minimum = 0D;
            chartArea14.AxisY2.TextOrientation = System.Windows.Forms.DataVisualization.Charting.TextOrientation.Rotated90;
            chartArea14.BorderColor = System.Drawing.Color.Transparent;
            chartArea14.BorderWidth = 0;
            chartArea14.Name = "ChartArea1";
            chartArea14.Position.Auto = false;
            chartArea14.Position.Height = 95F;
            chartArea14.Position.Width = 99F;
            chartArea14.Position.Y = 1F;
            this.chartHR.ChartAreas.Add(chartArea14);
            this.chartHR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartHR.Location = new System.Drawing.Point(52, 0);
            this.chartHR.MinimumSize = new System.Drawing.Size(200, 100);
            this.chartHR.Name = "chartHR";
            series27.ChartArea = "ChartArea1";
            series27.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series27.Color = System.Drawing.Color.Lime;
            series27.Name = "HrMin";
            series27.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series28.ChartArea = "ChartArea1";
            series28.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series28.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            series28.Name = "HrMax";
            series28.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series29.BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.DiagonalLeft;
            series29.BackHatchStyle = System.Windows.Forms.DataVisualization.Charting.ChartHatchStyle.Cross;
            series29.ChartArea = "ChartArea1";
            series29.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series29.Color = System.Drawing.Color.Blue;
            series29.LegendText = "HR";
            series29.MarkerSize = 3;
            series29.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series29.Name = "Heart Rate";
            series29.Points.Add(dataPoint53);
            series29.Points.Add(dataPoint54);
            series29.Points.Add(dataPoint55);
            series29.Points.Add(dataPoint56);
            series29.Points.Add(dataPoint57);
            series29.Points.Add(dataPoint58);
            series29.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            this.chartHR.Series.Add(series27);
            this.chartHR.Series.Add(series28);
            this.chartHR.Series.Add(series29);
            this.chartHR.Size = new System.Drawing.Size(1232, 153);
            this.chartHR.TabIndex = 0;
            this.chartHR.Text = "chart1";
            this.chartHR.Click += new System.EventHandler(this.chart2_Click_1);
            this.chartHR.MouseDown += new System.Windows.Forms.MouseEventHandler(this.chartHR_MouseDown);
            // 
            // panel14
            // 
            this.panel14.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel14.Location = new System.Drawing.Point(1284, 0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(2, 153);
            this.panel14.TabIndex = 2;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.label65);
            this.panel12.Controls.Add(this.labelHrGap);
            this.panel12.Controls.Add(this.label3);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel12.Location = new System.Drawing.Point(0, 0);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(52, 153);
            this.panel12.TabIndex = 1;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Dock = System.Windows.Forms.DockStyle.Top;
            this.label65.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.label65.Location = new System.Drawing.Point(0, 41);
            this.label65.Margin = new System.Windows.Forms.Padding(0);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(36, 13);
            this.label65.TabIndex = 3;
            this.label65.Text = "(BPM)";
            // 
            // labelHrGap
            // 
            this.labelHrGap.AutoSize = true;
            this.labelHrGap.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelHrGap.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.labelHrGap.Location = new System.Drawing.Point(0, 62);
            this.labelHrGap.Margin = new System.Windows.Forms.Padding(0);
            this.labelHrGap.Name = "labelHrGap";
            this.labelHrGap.Size = new System.Drawing.Size(51, 91);
            this.labelHrGap.TabIndex = 2;
            this.labelHrGap.Text = "^>=1234\r\n~1234\r\n<= 1234\r\n^gap:\r\n100.1%\r\n=11h00m\r\n/13d11h";
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 41);
            this.label3.TabIndex = 0;
            this.label3.Text = "HR";
            this.label3.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.panel8);
            this.panel5.Controls.Add(this.panel11);
            this.panel5.Controls.Add(this.panel17);
            this.panel5.Controls.Add(this.panel28);
            this.panel5.Controls.Add(this.panel18);
            this.panel5.Controls.Add(this.panel39);
            this.panel5.Controls.Add(this.panel34);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 140);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1288, 840);
            this.panel5.TabIndex = 5;
            this.panel5.Paint += new System.Windows.Forms.PaintEventHandler(this.panel5_Paint);
            // 
            // panel39
            // 
            this.panel39.Controls.Add(this.panel21);
            this.panel39.Controls.Add(this.panel42);
            this.panel39.Controls.Add(this.panel22);
            this.panel39.Controls.Add(this.panel6);
            this.panel39.Controls.Add(this.panel15);
            this.panel39.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel39.Location = new System.Drawing.Point(0, 520);
            this.panel39.Name = "panel39";
            this.panel39.Size = new System.Drawing.Size(1286, 273);
            this.panel39.TabIndex = 18;
            // 
            // panel21
            // 
            this.panel21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel21.Controls.Add(this.panel26);
            this.panel21.Controls.Add(this.panel25);
            this.panel21.Controls.Add(this.panel24);
            this.panel21.Controls.Add(this.panel23);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel21.ForeColor = System.Drawing.Color.DimGray;
            this.panel21.Location = new System.Drawing.Point(1010, 0);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(276, 273);
            this.panel21.TabIndex = 7;
            // 
            // panel26
            // 
            this.panel26.Controls.Add(this.labelPerMan);
            this.panel26.Controls.Add(this.labelPerTotal);
            this.panel26.Controls.Add(this.label44);
            this.panel26.Controls.Add(this.labelPerPace);
            this.panel26.Controls.Add(this.labelPerPVC);
            this.panel26.Controls.Add(this.labelPerPAC);
            this.panel26.Controls.Add(this.labelPerAfib);
            this.panel26.Controls.Add(this.labelPerPause);
            this.panel26.Controls.Add(this.labelPerBrady);
            this.panel26.Controls.Add(this.labelPerTachy);
            this.panel26.Controls.Add(this.label27);
            this.panel26.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel26.Location = new System.Drawing.Point(219, 0);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(51, 271);
            this.panel26.TabIndex = 3;
            // 
            // labelPerMan
            // 
            this.labelPerMan.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerMan.Font = new System.Drawing.Font("Arial", 10F);
            this.labelPerMan.Location = new System.Drawing.Point(0, 178);
            this.labelPerMan.Name = "labelPerMan";
            this.labelPerMan.Size = new System.Drawing.Size(51, 18);
            this.labelPerMan.TabIndex = 5;
            this.labelPerMan.Text = "-";
            this.labelPerMan.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPerTotal
            // 
            this.labelPerTotal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerTotal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelPerTotal.Location = new System.Drawing.Point(0, 160);
            this.labelPerTotal.Name = "labelPerTotal";
            this.labelPerTotal.Size = new System.Drawing.Size(51, 18);
            this.labelPerTotal.TabIndex = 6;
            this.labelPerTotal.Text = "99.99";
            this.labelPerTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label44
            // 
            this.label44.Dock = System.Windows.Forms.DockStyle.Top;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(0, 150);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(51, 10);
            this.label44.TabIndex = 8;
            // 
            // labelPerPace
            // 
            this.labelPerPace.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerPace.Font = new System.Drawing.Font("Arial", 10F);
            this.labelPerPace.Location = new System.Drawing.Point(0, 132);
            this.labelPerPace.Name = "labelPerPace";
            this.labelPerPace.Size = new System.Drawing.Size(51, 18);
            this.labelPerPace.TabIndex = 16;
            this.labelPerPace.Text = "-";
            this.labelPerPace.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPerPVC
            // 
            this.labelPerPVC.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerPVC.Font = new System.Drawing.Font("Arial", 10F);
            this.labelPerPVC.Location = new System.Drawing.Point(0, 114);
            this.labelPerPVC.Name = "labelPerPVC";
            this.labelPerPVC.Size = new System.Drawing.Size(51, 18);
            this.labelPerPVC.TabIndex = 15;
            this.labelPerPVC.Text = "-";
            this.labelPerPVC.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPerPAC
            // 
            this.labelPerPAC.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerPAC.Font = new System.Drawing.Font("Arial", 10F);
            this.labelPerPAC.Location = new System.Drawing.Point(0, 96);
            this.labelPerPAC.Name = "labelPerPAC";
            this.labelPerPAC.Size = new System.Drawing.Size(51, 18);
            this.labelPerPAC.TabIndex = 14;
            this.labelPerPAC.Text = "-";
            this.labelPerPAC.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPerAfib
            // 
            this.labelPerAfib.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerAfib.Font = new System.Drawing.Font("Arial", 10F);
            this.labelPerAfib.Location = new System.Drawing.Point(0, 78);
            this.labelPerAfib.Name = "labelPerAfib";
            this.labelPerAfib.Size = new System.Drawing.Size(51, 18);
            this.labelPerAfib.TabIndex = 4;
            this.labelPerAfib.Text = "99.99";
            this.labelPerAfib.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPerPause
            // 
            this.labelPerPause.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerPause.Font = new System.Drawing.Font("Arial", 10F);
            this.labelPerPause.Location = new System.Drawing.Point(0, 60);
            this.labelPerPause.Name = "labelPerPause";
            this.labelPerPause.Size = new System.Drawing.Size(51, 18);
            this.labelPerPause.TabIndex = 3;
            this.labelPerPause.Text = "99.99";
            this.labelPerPause.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPerBrady
            // 
            this.labelPerBrady.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerBrady.Font = new System.Drawing.Font("Arial", 10F);
            this.labelPerBrady.Location = new System.Drawing.Point(0, 42);
            this.labelPerBrady.Name = "labelPerBrady";
            this.labelPerBrady.Size = new System.Drawing.Size(51, 18);
            this.labelPerBrady.TabIndex = 2;
            this.labelPerBrady.Text = "99.99";
            this.labelPerBrady.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPerTachy
            // 
            this.labelPerTachy.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerTachy.Font = new System.Drawing.Font("Arial", 10F);
            this.labelPerTachy.Location = new System.Drawing.Point(0, 24);
            this.labelPerTachy.Name = "labelPerTachy";
            this.labelPerTachy.Size = new System.Drawing.Size(51, 18);
            this.labelPerTachy.TabIndex = 1;
            this.labelPerTachy.Text = "99.99";
            this.labelPerTachy.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label27
            // 
            this.label27.Dock = System.Windows.Forms.DockStyle.Top;
            this.label27.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label27.Location = new System.Drawing.Point(0, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(51, 24);
            this.label27.TabIndex = 0;
            this.label27.Text = "%";
            this.label27.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel25
            // 
            this.panel25.Controls.Add(this.labelDurDevice);
            this.panel25.Controls.Add(this.labelDurMan);
            this.panel25.Controls.Add(this.labelDurTotal);
            this.panel25.Controls.Add(this.label41);
            this.panel25.Controls.Add(this.labelDurPace);
            this.panel25.Controls.Add(this.labelDurPVC);
            this.panel25.Controls.Add(this.labelDurPAC);
            this.panel25.Controls.Add(this.labelDurAfib);
            this.panel25.Controls.Add(this.labelDurPause);
            this.panel25.Controls.Add(this.labelDurBrady);
            this.panel25.Controls.Add(this.labelDurTachy);
            this.panel25.Controls.Add(this.label22);
            this.panel25.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel25.Location = new System.Drawing.Point(150, 0);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(69, 271);
            this.panel25.TabIndex = 2;
            // 
            // labelDurDevice
            // 
            this.labelDurDevice.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurDevice.Font = new System.Drawing.Font("Arial", 10F);
            this.labelDurDevice.Location = new System.Drawing.Point(0, 196);
            this.labelDurDevice.Name = "labelDurDevice";
            this.labelDurDevice.Size = new System.Drawing.Size(69, 18);
            this.labelDurDevice.TabIndex = 7;
            this.labelDurDevice.Text = "999:99:99";
            this.labelDurDevice.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDurMan
            // 
            this.labelDurMan.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurMan.Font = new System.Drawing.Font("Arial", 10F);
            this.labelDurMan.Location = new System.Drawing.Point(0, 178);
            this.labelDurMan.Name = "labelDurMan";
            this.labelDurMan.Size = new System.Drawing.Size(69, 18);
            this.labelDurMan.TabIndex = 5;
            this.labelDurMan.Text = "x";
            this.labelDurMan.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDurTotal
            // 
            this.labelDurTotal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurTotal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelDurTotal.Location = new System.Drawing.Point(0, 160);
            this.labelDurTotal.Name = "labelDurTotal";
            this.labelDurTotal.Size = new System.Drawing.Size(69, 18);
            this.labelDurTotal.TabIndex = 6;
            this.labelDurTotal.Text = "99:99:99";
            this.labelDurTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label41
            // 
            this.label41.Dock = System.Windows.Forms.DockStyle.Top;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(0, 150);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(69, 10);
            this.label41.TabIndex = 8;
            // 
            // labelDurPace
            // 
            this.labelDurPace.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurPace.Font = new System.Drawing.Font("Arial", 10F);
            this.labelDurPace.Location = new System.Drawing.Point(0, 132);
            this.labelDurPace.Name = "labelDurPace";
            this.labelDurPace.Size = new System.Drawing.Size(69, 18);
            this.labelDurPace.TabIndex = 16;
            this.labelDurPace.Text = "x";
            this.labelDurPace.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDurPVC
            // 
            this.labelDurPVC.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurPVC.Font = new System.Drawing.Font("Arial", 10F);
            this.labelDurPVC.Location = new System.Drawing.Point(0, 114);
            this.labelDurPVC.Name = "labelDurPVC";
            this.labelDurPVC.Size = new System.Drawing.Size(69, 18);
            this.labelDurPVC.TabIndex = 15;
            this.labelDurPVC.Text = "x";
            this.labelDurPVC.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDurPAC
            // 
            this.labelDurPAC.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurPAC.Font = new System.Drawing.Font("Arial", 10F);
            this.labelDurPAC.Location = new System.Drawing.Point(0, 96);
            this.labelDurPAC.Name = "labelDurPAC";
            this.labelDurPAC.Size = new System.Drawing.Size(69, 18);
            this.labelDurPAC.TabIndex = 14;
            this.labelDurPAC.Text = "x";
            this.labelDurPAC.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDurAfib
            // 
            this.labelDurAfib.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurAfib.Font = new System.Drawing.Font("Arial", 10F);
            this.labelDurAfib.Location = new System.Drawing.Point(0, 78);
            this.labelDurAfib.Name = "labelDurAfib";
            this.labelDurAfib.Size = new System.Drawing.Size(69, 18);
            this.labelDurAfib.TabIndex = 4;
            this.labelDurAfib.Text = "99:99:99";
            this.labelDurAfib.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDurPause
            // 
            this.labelDurPause.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurPause.Font = new System.Drawing.Font("Arial", 10F);
            this.labelDurPause.Location = new System.Drawing.Point(0, 60);
            this.labelDurPause.Name = "labelDurPause";
            this.labelDurPause.Size = new System.Drawing.Size(69, 18);
            this.labelDurPause.TabIndex = 3;
            this.labelDurPause.Text = "99:99:99";
            this.labelDurPause.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDurBrady
            // 
            this.labelDurBrady.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurBrady.Font = new System.Drawing.Font("Arial", 10F);
            this.labelDurBrady.Location = new System.Drawing.Point(0, 42);
            this.labelDurBrady.Name = "labelDurBrady";
            this.labelDurBrady.Size = new System.Drawing.Size(69, 18);
            this.labelDurBrady.TabIndex = 2;
            this.labelDurBrady.Text = "99:99:99";
            this.labelDurBrady.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDurTachy
            // 
            this.labelDurTachy.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurTachy.Font = new System.Drawing.Font("Arial", 10F);
            this.labelDurTachy.Location = new System.Drawing.Point(0, 24);
            this.labelDurTachy.Name = "labelDurTachy";
            this.labelDurTachy.Size = new System.Drawing.Size(69, 18);
            this.labelDurTachy.TabIndex = 1;
            this.labelDurTachy.Text = "99:99:99";
            this.labelDurTachy.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label22
            // 
            this.label22.Dock = System.Windows.Forms.DockStyle.Top;
            this.label22.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label22.Location = new System.Drawing.Point(0, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(69, 24);
            this.label22.TabIndex = 0;
            this.label22.Text = "Duration";
            this.label22.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel24
            // 
            this.panel24.Controls.Add(this.labelPlotSec);
            this.panel24.Controls.Add(this.labelUser);
            this.panel24.Controls.Add(this.label64);
            this.panel24.Controls.Add(this.label63);
            this.panel24.Controls.Add(this.labelEpMan);
            this.panel24.Controls.Add(this.labelEpTotal);
            this.panel24.Controls.Add(this.label40);
            this.panel24.Controls.Add(this.labelEpPace);
            this.panel24.Controls.Add(this.labelEpPVC);
            this.panel24.Controls.Add(this.labelEpPAC);
            this.panel24.Controls.Add(this.labelEpAfib);
            this.panel24.Controls.Add(this.labelEpPause);
            this.panel24.Controls.Add(this.labelEpBrady);
            this.panel24.Controls.Add(this.labelEpTachy);
            this.panel24.Controls.Add(this.label17);
            this.panel24.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel24.Location = new System.Drawing.Point(76, 0);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(74, 271);
            this.panel24.TabIndex = 1;
            // 
            // labelPlotSec
            // 
            this.labelPlotSec.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelPlotSec.Font = new System.Drawing.Font("Arial", 8F);
            this.labelPlotSec.Location = new System.Drawing.Point(0, 253);
            this.labelPlotSec.Name = "labelPlotSec";
            this.labelPlotSec.Size = new System.Drawing.Size(74, 18);
            this.labelPlotSec.TabIndex = 20;
            this.labelPlotSec.Text = "^99.99sec";
            this.labelPlotSec.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // labelUser
            // 
            this.labelUser.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelUser.Font = new System.Drawing.Font("Arial", 10F);
            this.labelUser.Location = new System.Drawing.Point(0, 224);
            this.labelUser.Name = "labelUser";
            this.labelUser.Size = new System.Drawing.Size(74, 18);
            this.labelUser.TabIndex = 9;
            this.labelUser.Text = "^99";
            this.labelUser.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label64
            // 
            this.label64.Dock = System.Windows.Forms.DockStyle.Top;
            this.label64.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.Location = new System.Drawing.Point(0, 214);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(74, 10);
            this.label64.TabIndex = 18;
            // 
            // label63
            // 
            this.label63.Dock = System.Windows.Forms.DockStyle.Top;
            this.label63.Font = new System.Drawing.Font("Arial", 10F);
            this.label63.Location = new System.Drawing.Point(0, 196);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(74, 18);
            this.label63.TabIndex = 16;
            this.label63.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelEpMan
            // 
            this.labelEpMan.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpMan.Font = new System.Drawing.Font("Arial", 10F);
            this.labelEpMan.Location = new System.Drawing.Point(0, 178);
            this.labelEpMan.Name = "labelEpMan";
            this.labelEpMan.Size = new System.Drawing.Size(74, 18);
            this.labelEpMan.TabIndex = 5;
            this.labelEpMan.Text = "^99";
            this.labelEpMan.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelEpTotal
            // 
            this.labelEpTotal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpTotal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelEpTotal.Location = new System.Drawing.Point(0, 160);
            this.labelEpTotal.Name = "labelEpTotal";
            this.labelEpTotal.Size = new System.Drawing.Size(74, 18);
            this.labelEpTotal.TabIndex = 6;
            this.labelEpTotal.Text = "^99";
            this.labelEpTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label40
            // 
            this.label40.Dock = System.Windows.Forms.DockStyle.Top;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(0, 150);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(74, 10);
            this.label40.TabIndex = 10;
            // 
            // labelEpPace
            // 
            this.labelEpPace.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpPace.Font = new System.Drawing.Font("Arial", 10F);
            this.labelEpPace.Location = new System.Drawing.Point(0, 132);
            this.labelEpPace.Name = "labelEpPace";
            this.labelEpPace.Size = new System.Drawing.Size(74, 18);
            this.labelEpPace.TabIndex = 19;
            this.labelEpPace.Text = "^99";
            this.labelEpPace.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelEpPVC
            // 
            this.labelEpPVC.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpPVC.Font = new System.Drawing.Font("Arial", 10F);
            this.labelEpPVC.Location = new System.Drawing.Point(0, 114);
            this.labelEpPVC.Name = "labelEpPVC";
            this.labelEpPVC.Size = new System.Drawing.Size(74, 18);
            this.labelEpPVC.TabIndex = 15;
            this.labelEpPVC.Text = "^99";
            this.labelEpPVC.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelEpPAC
            // 
            this.labelEpPAC.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpPAC.Font = new System.Drawing.Font("Arial", 10F);
            this.labelEpPAC.Location = new System.Drawing.Point(0, 96);
            this.labelEpPAC.Name = "labelEpPAC";
            this.labelEpPAC.Size = new System.Drawing.Size(74, 18);
            this.labelEpPAC.TabIndex = 14;
            this.labelEpPAC.Text = "^99";
            this.labelEpPAC.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelEpAfib
            // 
            this.labelEpAfib.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpAfib.Font = new System.Drawing.Font("Arial", 10F);
            this.labelEpAfib.Location = new System.Drawing.Point(0, 78);
            this.labelEpAfib.Name = "labelEpAfib";
            this.labelEpAfib.Size = new System.Drawing.Size(74, 18);
            this.labelEpAfib.TabIndex = 4;
            this.labelEpAfib.Text = "^99";
            this.labelEpAfib.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelEpPause
            // 
            this.labelEpPause.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpPause.Font = new System.Drawing.Font("Arial", 10F);
            this.labelEpPause.Location = new System.Drawing.Point(0, 60);
            this.labelEpPause.Name = "labelEpPause";
            this.labelEpPause.Size = new System.Drawing.Size(74, 18);
            this.labelEpPause.TabIndex = 3;
            this.labelEpPause.Text = "^99";
            this.labelEpPause.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelEpBrady
            // 
            this.labelEpBrady.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpBrady.Font = new System.Drawing.Font("Arial", 10F);
            this.labelEpBrady.Location = new System.Drawing.Point(0, 42);
            this.labelEpBrady.Name = "labelEpBrady";
            this.labelEpBrady.Size = new System.Drawing.Size(74, 18);
            this.labelEpBrady.TabIndex = 2;
            this.labelEpBrady.Text = "^99";
            this.labelEpBrady.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelEpTachy
            // 
            this.labelEpTachy.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpTachy.Font = new System.Drawing.Font("Arial", 10F);
            this.labelEpTachy.Location = new System.Drawing.Point(0, 24);
            this.labelEpTachy.Name = "labelEpTachy";
            this.labelEpTachy.Size = new System.Drawing.Size(74, 18);
            this.labelEpTachy.TabIndex = 1;
            this.labelEpTachy.Text = "^99";
            this.labelEpTachy.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label17
            // 
            this.label17.Dock = System.Windows.Forms.DockStyle.Top;
            this.label17.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(0, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(74, 24);
            this.label17.TabIndex = 0;
            this.label17.Text = "Episodes";
            this.label17.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel23
            // 
            this.panel23.Controls.Add(this.label57);
            this.panel23.Controls.Add(this.label24);
            this.panel23.Controls.Add(this.label61);
            this.panel23.Controls.Add(this.label15);
            this.panel23.Controls.Add(this.label4);
            this.panel23.Controls.Add(this.label6);
            this.panel23.Controls.Add(this.label37);
            this.panel23.Controls.Add(this.label49);
            this.panel23.Controls.Add(this.label58);
            this.panel23.Controls.Add(this.label60);
            this.panel23.Controls.Add(this.label12);
            this.panel23.Controls.Add(this.label11);
            this.panel23.Controls.Add(this.label10);
            this.panel23.Controls.Add(this.label9);
            this.panel23.Controls.Add(this.label8);
            this.panel23.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel23.Location = new System.Drawing.Point(0, 0);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(76, 271);
            this.panel23.TabIndex = 0;
            // 
            // label57
            // 
            this.label57.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label57.Font = new System.Drawing.Font("Arial", 8F);
            this.label57.Location = new System.Drawing.Point(0, 253);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(76, 18);
            this.label57.TabIndex = 19;
            this.label57.Text = "Plot:";
            this.label57.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // label24
            // 
            this.label24.Dock = System.Windows.Forms.DockStyle.Top;
            this.label24.Font = new System.Drawing.Font("Arial", 10F);
            this.label24.Location = new System.Drawing.Point(0, 224);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(76, 18);
            this.label24.TabIndex = 8;
            this.label24.Text = "Tech:";
            // 
            // label61
            // 
            this.label61.Dock = System.Windows.Forms.DockStyle.Top;
            this.label61.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.Location = new System.Drawing.Point(0, 214);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(76, 10);
            this.label61.TabIndex = 17;
            // 
            // label15
            // 
            this.label15.Dock = System.Windows.Forms.DockStyle.Top;
            this.label15.Font = new System.Drawing.Font("Arial", 10F);
            this.label15.Location = new System.Drawing.Point(0, 196);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(76, 18);
            this.label15.TabIndex = 7;
            this.label15.Text = "Device";
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Font = new System.Drawing.Font("Arial", 10F);
            this.label4.Location = new System.Drawing.Point(0, 178);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 18);
            this.label4.TabIndex = 5;
            this.label4.Text = "Manual";
            // 
            // label6
            // 
            this.label6.Dock = System.Windows.Forms.DockStyle.Top;
            this.label6.Font = new System.Drawing.Font("Arial", 10F);
            this.label6.Location = new System.Drawing.Point(0, 160);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 18);
            this.label6.TabIndex = 6;
            this.label6.Text = "Events";
            // 
            // label37
            // 
            this.label37.Dock = System.Windows.Forms.DockStyle.Top;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(0, 150);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(76, 10);
            this.label37.TabIndex = 9;
            // 
            // label49
            // 
            this.label49.Dock = System.Windows.Forms.DockStyle.Top;
            this.label49.Font = new System.Drawing.Font("Arial", 10F);
            this.label49.Location = new System.Drawing.Point(0, 132);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(76, 18);
            this.label49.TabIndex = 18;
            this.label49.Text = "Pace";
            // 
            // label58
            // 
            this.label58.Dock = System.Windows.Forms.DockStyle.Top;
            this.label58.Font = new System.Drawing.Font("Arial", 10F);
            this.label58.Location = new System.Drawing.Point(0, 114);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(76, 18);
            this.label58.TabIndex = 13;
            this.label58.Text = "PVC-VEB";
            // 
            // label60
            // 
            this.label60.Dock = System.Windows.Forms.DockStyle.Top;
            this.label60.Font = new System.Drawing.Font("Arial", 10F);
            this.label60.Location = new System.Drawing.Point(0, 96);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(76, 18);
            this.label60.TabIndex = 12;
            this.label60.Text = "PAC-SVEB";
            // 
            // label12
            // 
            this.label12.Dock = System.Windows.Forms.DockStyle.Top;
            this.label12.Font = new System.Drawing.Font("Arial", 10F);
            this.label12.Location = new System.Drawing.Point(0, 78);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(76, 18);
            this.label12.TabIndex = 4;
            this.label12.Text = "AFib";
            // 
            // label11
            // 
            this.label11.Dock = System.Windows.Forms.DockStyle.Top;
            this.label11.Font = new System.Drawing.Font("Arial", 10F);
            this.label11.Location = new System.Drawing.Point(0, 60);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(76, 18);
            this.label11.TabIndex = 3;
            this.label11.Text = "Pause";
            // 
            // label10
            // 
            this.label10.Dock = System.Windows.Forms.DockStyle.Top;
            this.label10.Font = new System.Drawing.Font("Arial", 10F);
            this.label10.Location = new System.Drawing.Point(0, 42);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(76, 18);
            this.label10.TabIndex = 2;
            this.label10.Text = "Brady";
            // 
            // label9
            // 
            this.label9.Dock = System.Windows.Forms.DockStyle.Top;
            this.label9.Font = new System.Drawing.Font("Arial", 10F);
            this.label9.Location = new System.Drawing.Point(0, 24);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(76, 18);
            this.label9.TabIndex = 1;
            this.label9.Text = "Tachy";
            // 
            // label8
            // 
            this.label8.Dock = System.Windows.Forms.DockStyle.Top;
            this.label8.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(0, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(76, 24);
            this.label8.TabIndex = 0;
            this.label8.Text = "Type";
            // 
            // panel42
            // 
            this.panel42.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel42.Location = new System.Drawing.Point(1000, 0);
            this.panel42.Name = "panel42";
            this.panel42.Size = new System.Drawing.Size(10, 273);
            this.panel42.TabIndex = 6;
            // 
            // panel22
            // 
            this.panel22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel22.Controls.Add(this.chartSummaryBar);
            this.panel22.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel22.Location = new System.Drawing.Point(292, 0);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(708, 273);
            this.panel22.TabIndex = 5;
            // 
            // chartSummaryBar
            // 
            chartArea15.AxisX.LabelStyle.Enabled = false;
            chartArea15.AxisX.LineColor = System.Drawing.Color.DarkGray;
            chartArea15.AxisX.MajorGrid.Enabled = false;
            chartArea15.AxisX2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea15.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea15.AxisY.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea15.AxisY.LabelStyle.Interval = 0D;
            chartArea15.AxisY.LabelStyle.IntervalOffsetType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea15.AxisY.LabelStyle.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea15.AxisY.LineColor = System.Drawing.Color.DarkGray;
            chartArea15.AxisY.MajorGrid.Interval = 0D;
            chartArea15.AxisY.MajorGrid.IntervalOffsetType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea15.AxisY.MajorGrid.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea15.AxisY.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea15.AxisY.MajorTickMark.Interval = 0D;
            chartArea15.AxisY.MajorTickMark.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea15.AxisY.ScaleView.MinSize = 10D;
            chartArea15.AxisY.ScaleView.SmallScrollMinSize = 10D;
            chartArea15.BorderWidth = 0;
            chartArea15.Name = "ChartArea1";
            this.chartSummaryBar.ChartAreas.Add(chartArea15);
            this.chartSummaryBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartSummaryBar.Location = new System.Drawing.Point(0, 0);
            this.chartSummaryBar.Name = "chartSummaryBar";
            series30.ChartArea = "ChartArea1";
            series30.IsVisibleInLegend = false;
            series30.Name = "Series1";
            dataPoint59.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            dataPoint59.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataPoint59.IsValueShownAsLabel = false;
            dataPoint59.IsVisibleInLegend = false;
            dataPoint59.Label = "Tachy";
            dataPoint59.LabelForeColor = System.Drawing.Color.Black;
            dataPoint60.Color = System.Drawing.Color.Green;
            dataPoint60.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataPoint60.Label = "Brady";
            dataPoint61.Color = System.Drawing.Color.Gray;
            dataPoint61.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataPoint61.Label = "Pause";
            dataPoint62.Color = System.Drawing.Color.Blue;
            dataPoint62.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataPoint62.Label = "AFib";
            dataPoint63.Color = System.Drawing.Color.Magenta;
            dataPoint63.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            dataPoint63.Label = "AFlut";
            dataPoint64.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            dataPoint64.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            dataPoint64.Label = "PAC\\nSVEB";
            dataPoint65.Color = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            dataPoint65.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            dataPoint65.Label = "PVC\\nVEB";
            dataPoint66.Color = System.Drawing.Color.Lime;
            dataPoint66.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            dataPoint66.Label = "Pace";
            dataPoint67.Color = System.Drawing.Color.Purple;
            dataPoint67.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            dataPoint67.Label = "VTach";
            dataPoint68.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            dataPoint68.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            dataPoint68.Label = "Other";
            dataPoint69.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            dataPoint69.Label = "Norm";
            dataPoint70.Color = System.Drawing.Color.Cyan;
            dataPoint70.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            dataPoint70.Label = "AbN";
            dataPoint71.Color = System.Drawing.Color.MediumSeaGreen;
            dataPoint71.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataPoint71.Label = "Manual";
            series30.Points.Add(dataPoint59);
            series30.Points.Add(dataPoint60);
            series30.Points.Add(dataPoint61);
            series30.Points.Add(dataPoint62);
            series30.Points.Add(dataPoint63);
            series30.Points.Add(dataPoint64);
            series30.Points.Add(dataPoint65);
            series30.Points.Add(dataPoint66);
            series30.Points.Add(dataPoint67);
            series30.Points.Add(dataPoint68);
            series30.Points.Add(dataPoint69);
            series30.Points.Add(dataPoint70);
            series30.Points.Add(dataPoint71);
            series30.SmartLabelStyle.MovingDirection = System.Windows.Forms.DataVisualization.Charting.LabelAlignmentStyles.Top;
            this.chartSummaryBar.Series.Add(series30);
            this.chartSummaryBar.Size = new System.Drawing.Size(706, 271);
            this.chartSummaryBar.TabIndex = 0;
            this.chartSummaryBar.Text = "EventPie";
            // 
            // panel6
            // 
            this.panel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel6.Location = new System.Drawing.Point(282, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(10, 273);
            this.panel6.TabIndex = 4;
            // 
            // panel15
            // 
            this.panel15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel15.Controls.Add(this.panel16);
            this.panel15.Controls.Add(this.panel40);
            this.panel15.Controls.Add(this.panel41);
            this.panel15.Controls.Add(this.panel57);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel15.Location = new System.Drawing.Point(0, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(282, 273);
            this.panel15.TabIndex = 3;
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.label45);
            this.panel16.Controls.Add(this.label69);
            this.panel16.Controls.Add(this.labelPerTotalVal);
            this.panel16.Controls.Add(this.label55);
            this.panel16.Controls.Add(this.labelPerAbnVal);
            this.panel16.Controls.Add(this.labelPerNormVal);
            this.panel16.Controls.Add(this.labelPerOtherVal);
            this.panel16.Controls.Add(this.labelPerVtachVal);
            this.panel16.Controls.Add(this.labelPerPaceVal);
            this.panel16.Controls.Add(this.labelPerPVCVal);
            this.panel16.Controls.Add(this.labelPerPACVal);
            this.panel16.Controls.Add(this.labelPerAflutVal);
            this.panel16.Controls.Add(this.labelPerAfibVal);
            this.panel16.Controls.Add(this.labelPerPauseVal);
            this.panel16.Controls.Add(this.labelPerBradyVal);
            this.panel16.Controls.Add(this.labelPerTachyVal);
            this.panel16.Controls.Add(this.label59);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel16.Location = new System.Drawing.Point(226, 0);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(49, 271);
            this.panel16.TabIndex = 3;
            // 
            // label45
            // 
            this.label45.Dock = System.Windows.Forms.DockStyle.Top;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(0, 271);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(49, 4);
            this.label45.TabIndex = 8;
            // 
            // label69
            // 
            this.label69.Dock = System.Windows.Forms.DockStyle.Top;
            this.label69.Font = new System.Drawing.Font("Arial", 10F);
            this.label69.Location = new System.Drawing.Point(0, 254);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(49, 17);
            this.label69.TabIndex = 22;
            this.label69.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPerTotalVal
            // 
            this.labelPerTotalVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerTotalVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelPerTotalVal.Location = new System.Drawing.Point(0, 236);
            this.labelPerTotalVal.Name = "labelPerTotalVal";
            this.labelPerTotalVal.Size = new System.Drawing.Size(49, 18);
            this.labelPerTotalVal.TabIndex = 5;
            this.labelPerTotalVal.Text = "-";
            this.labelPerTotalVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label55
            // 
            this.label55.Dock = System.Windows.Forms.DockStyle.Top;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(0, 232);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(49, 4);
            this.label55.TabIndex = 24;
            // 
            // labelPerAbnVal
            // 
            this.labelPerAbnVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerAbnVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelPerAbnVal.Location = new System.Drawing.Point(0, 214);
            this.labelPerAbnVal.Name = "labelPerAbnVal";
            this.labelPerAbnVal.Size = new System.Drawing.Size(49, 18);
            this.labelPerAbnVal.TabIndex = 23;
            this.labelPerAbnVal.Text = "-";
            this.labelPerAbnVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPerNormVal
            // 
            this.labelPerNormVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerNormVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelPerNormVal.Location = new System.Drawing.Point(0, 196);
            this.labelPerNormVal.Name = "labelPerNormVal";
            this.labelPerNormVal.Size = new System.Drawing.Size(49, 18);
            this.labelPerNormVal.TabIndex = 21;
            this.labelPerNormVal.Text = "-";
            this.labelPerNormVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPerOtherVal
            // 
            this.labelPerOtherVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerOtherVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelPerOtherVal.Location = new System.Drawing.Point(0, 178);
            this.labelPerOtherVal.Name = "labelPerOtherVal";
            this.labelPerOtherVal.Size = new System.Drawing.Size(49, 18);
            this.labelPerOtherVal.TabIndex = 20;
            this.labelPerOtherVal.Text = "-";
            this.labelPerOtherVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPerVtachVal
            // 
            this.labelPerVtachVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerVtachVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelPerVtachVal.Location = new System.Drawing.Point(0, 160);
            this.labelPerVtachVal.Name = "labelPerVtachVal";
            this.labelPerVtachVal.Size = new System.Drawing.Size(49, 18);
            this.labelPerVtachVal.TabIndex = 6;
            this.labelPerVtachVal.Text = "99.99";
            this.labelPerVtachVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPerPaceVal
            // 
            this.labelPerPaceVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerPaceVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelPerPaceVal.Location = new System.Drawing.Point(0, 142);
            this.labelPerPaceVal.Name = "labelPerPaceVal";
            this.labelPerPaceVal.Size = new System.Drawing.Size(49, 18);
            this.labelPerPaceVal.TabIndex = 19;
            this.labelPerPaceVal.Text = "-";
            this.labelPerPaceVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPerPVCVal
            // 
            this.labelPerPVCVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerPVCVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelPerPVCVal.Location = new System.Drawing.Point(0, 124);
            this.labelPerPVCVal.Name = "labelPerPVCVal";
            this.labelPerPVCVal.Size = new System.Drawing.Size(49, 18);
            this.labelPerPVCVal.TabIndex = 17;
            this.labelPerPVCVal.Text = "-";
            this.labelPerPVCVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPerPACVal
            // 
            this.labelPerPACVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerPACVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelPerPACVal.Location = new System.Drawing.Point(0, 106);
            this.labelPerPACVal.Name = "labelPerPACVal";
            this.labelPerPACVal.Size = new System.Drawing.Size(49, 18);
            this.labelPerPACVal.TabIndex = 16;
            this.labelPerPACVal.Text = "-";
            this.labelPerPACVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPerAflutVal
            // 
            this.labelPerAflutVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerAflutVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelPerAflutVal.Location = new System.Drawing.Point(0, 88);
            this.labelPerAflutVal.Name = "labelPerAflutVal";
            this.labelPerAflutVal.Size = new System.Drawing.Size(49, 18);
            this.labelPerAflutVal.TabIndex = 18;
            this.labelPerAflutVal.Text = "99.99";
            this.labelPerAflutVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPerAfibVal
            // 
            this.labelPerAfibVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerAfibVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelPerAfibVal.Location = new System.Drawing.Point(0, 70);
            this.labelPerAfibVal.Name = "labelPerAfibVal";
            this.labelPerAfibVal.Size = new System.Drawing.Size(49, 18);
            this.labelPerAfibVal.TabIndex = 4;
            this.labelPerAfibVal.Text = "99.99";
            this.labelPerAfibVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPerPauseVal
            // 
            this.labelPerPauseVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerPauseVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelPerPauseVal.Location = new System.Drawing.Point(0, 52);
            this.labelPerPauseVal.Name = "labelPerPauseVal";
            this.labelPerPauseVal.Size = new System.Drawing.Size(49, 18);
            this.labelPerPauseVal.TabIndex = 3;
            this.labelPerPauseVal.Text = "99.99";
            this.labelPerPauseVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPerBradyVal
            // 
            this.labelPerBradyVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerBradyVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelPerBradyVal.Location = new System.Drawing.Point(0, 34);
            this.labelPerBradyVal.Name = "labelPerBradyVal";
            this.labelPerBradyVal.Size = new System.Drawing.Size(49, 18);
            this.labelPerBradyVal.TabIndex = 2;
            this.labelPerBradyVal.Text = "99.99";
            this.labelPerBradyVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPerTachyVal
            // 
            this.labelPerTachyVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerTachyVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelPerTachyVal.Location = new System.Drawing.Point(0, 16);
            this.labelPerTachyVal.Name = "labelPerTachyVal";
            this.labelPerTachyVal.Size = new System.Drawing.Size(49, 18);
            this.labelPerTachyVal.TabIndex = 1;
            this.labelPerTachyVal.Text = "99.99";
            this.labelPerTachyVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label59
            // 
            this.label59.Dock = System.Windows.Forms.DockStyle.Top;
            this.label59.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label59.Location = new System.Drawing.Point(0, 0);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(49, 16);
            this.label59.TabIndex = 0;
            this.label59.Text = "%";
            this.label59.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel40
            // 
            this.panel40.Controls.Add(this.labelDurManVal);
            this.panel40.Controls.Add(this.labelDurTotalVal);
            this.panel40.Controls.Add(this.label62);
            this.panel40.Controls.Add(this.labelDurAbnVal);
            this.panel40.Controls.Add(this.labelDurNormVal);
            this.panel40.Controls.Add(this.labelDurOtherVal);
            this.panel40.Controls.Add(this.labelDurVtachVal);
            this.panel40.Controls.Add(this.labelDurPaceVal);
            this.panel40.Controls.Add(this.labelDurPVCVal);
            this.panel40.Controls.Add(this.labelDurPACVal);
            this.panel40.Controls.Add(this.labelDurAflutVal);
            this.panel40.Controls.Add(this.labelDurAfibVal);
            this.panel40.Controls.Add(this.labelDurPauseVal);
            this.panel40.Controls.Add(this.labelDurBradyVal);
            this.panel40.Controls.Add(this.labelDurTachyVal);
            this.panel40.Controls.Add(this.label68);
            this.panel40.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel40.Location = new System.Drawing.Point(151, 0);
            this.panel40.Name = "panel40";
            this.panel40.Size = new System.Drawing.Size(75, 271);
            this.panel40.TabIndex = 2;
            // 
            // labelDurManVal
            // 
            this.labelDurManVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurManVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelDurManVal.Location = new System.Drawing.Point(0, 254);
            this.labelDurManVal.Name = "labelDurManVal";
            this.labelDurManVal.Size = new System.Drawing.Size(75, 18);
            this.labelDurManVal.TabIndex = 5;
            this.labelDurManVal.Text = "x";
            this.labelDurManVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelDurManVal.Visible = false;
            // 
            // labelDurTotalVal
            // 
            this.labelDurTotalVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurTotalVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelDurTotalVal.Location = new System.Drawing.Point(0, 236);
            this.labelDurTotalVal.Name = "labelDurTotalVal";
            this.labelDurTotalVal.Size = new System.Drawing.Size(75, 18);
            this.labelDurTotalVal.TabIndex = 6;
            this.labelDurTotalVal.Text = "999:99:99";
            this.labelDurTotalVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label62
            // 
            this.label62.Dock = System.Windows.Forms.DockStyle.Top;
            this.label62.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.Location = new System.Drawing.Point(0, 232);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(75, 4);
            this.label62.TabIndex = 8;
            // 
            // labelDurAbnVal
            // 
            this.labelDurAbnVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurAbnVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelDurAbnVal.Location = new System.Drawing.Point(0, 214);
            this.labelDurAbnVal.Name = "labelDurAbnVal";
            this.labelDurAbnVal.Size = new System.Drawing.Size(75, 18);
            this.labelDurAbnVal.TabIndex = 22;
            this.labelDurAbnVal.Text = "99:99:99";
            this.labelDurAbnVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDurNormVal
            // 
            this.labelDurNormVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurNormVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelDurNormVal.Location = new System.Drawing.Point(0, 196);
            this.labelDurNormVal.Name = "labelDurNormVal";
            this.labelDurNormVal.Size = new System.Drawing.Size(75, 18);
            this.labelDurNormVal.TabIndex = 21;
            this.labelDurNormVal.Text = "99:99:99";
            this.labelDurNormVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDurOtherVal
            // 
            this.labelDurOtherVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurOtherVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelDurOtherVal.Location = new System.Drawing.Point(0, 178);
            this.labelDurOtherVal.Name = "labelDurOtherVal";
            this.labelDurOtherVal.Size = new System.Drawing.Size(75, 18);
            this.labelDurOtherVal.TabIndex = 19;
            this.labelDurOtherVal.Text = "99:99:99";
            this.labelDurOtherVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDurVtachVal
            // 
            this.labelDurVtachVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurVtachVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelDurVtachVal.Location = new System.Drawing.Point(0, 160);
            this.labelDurVtachVal.Name = "labelDurVtachVal";
            this.labelDurVtachVal.Size = new System.Drawing.Size(75, 18);
            this.labelDurVtachVal.TabIndex = 18;
            this.labelDurVtachVal.Text = "99:99:99";
            this.labelDurVtachVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDurPaceVal
            // 
            this.labelDurPaceVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurPaceVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelDurPaceVal.Location = new System.Drawing.Point(0, 142);
            this.labelDurPaceVal.Name = "labelDurPaceVal";
            this.labelDurPaceVal.Size = new System.Drawing.Size(75, 18);
            this.labelDurPaceVal.TabIndex = 17;
            this.labelDurPaceVal.Text = "x";
            this.labelDurPaceVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDurPVCVal
            // 
            this.labelDurPVCVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurPVCVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelDurPVCVal.Location = new System.Drawing.Point(0, 124);
            this.labelDurPVCVal.Name = "labelDurPVCVal";
            this.labelDurPVCVal.Size = new System.Drawing.Size(75, 18);
            this.labelDurPVCVal.TabIndex = 15;
            this.labelDurPVCVal.Text = "x";
            this.labelDurPVCVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDurPACVal
            // 
            this.labelDurPACVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurPACVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelDurPACVal.Location = new System.Drawing.Point(0, 106);
            this.labelDurPACVal.Name = "labelDurPACVal";
            this.labelDurPACVal.Size = new System.Drawing.Size(75, 18);
            this.labelDurPACVal.TabIndex = 14;
            this.labelDurPACVal.Text = "x";
            this.labelDurPACVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDurAflutVal
            // 
            this.labelDurAflutVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurAflutVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelDurAflutVal.Location = new System.Drawing.Point(0, 88);
            this.labelDurAflutVal.Name = "labelDurAflutVal";
            this.labelDurAflutVal.Size = new System.Drawing.Size(75, 18);
            this.labelDurAflutVal.TabIndex = 16;
            this.labelDurAflutVal.Text = "99:99:99";
            this.labelDurAflutVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDurAfibVal
            // 
            this.labelDurAfibVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurAfibVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelDurAfibVal.Location = new System.Drawing.Point(0, 70);
            this.labelDurAfibVal.Name = "labelDurAfibVal";
            this.labelDurAfibVal.Size = new System.Drawing.Size(75, 18);
            this.labelDurAfibVal.TabIndex = 4;
            this.labelDurAfibVal.Text = "99:99:99";
            this.labelDurAfibVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDurPauseVal
            // 
            this.labelDurPauseVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurPauseVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelDurPauseVal.Location = new System.Drawing.Point(0, 52);
            this.labelDurPauseVal.Name = "labelDurPauseVal";
            this.labelDurPauseVal.Size = new System.Drawing.Size(75, 18);
            this.labelDurPauseVal.TabIndex = 3;
            this.labelDurPauseVal.Text = "99:99:99";
            this.labelDurPauseVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDurBradyVal
            // 
            this.labelDurBradyVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurBradyVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelDurBradyVal.Location = new System.Drawing.Point(0, 34);
            this.labelDurBradyVal.Name = "labelDurBradyVal";
            this.labelDurBradyVal.Size = new System.Drawing.Size(75, 18);
            this.labelDurBradyVal.TabIndex = 2;
            this.labelDurBradyVal.Text = "99:99:99";
            this.labelDurBradyVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDurTachyVal
            // 
            this.labelDurTachyVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurTachyVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelDurTachyVal.Location = new System.Drawing.Point(0, 16);
            this.labelDurTachyVal.Name = "labelDurTachyVal";
            this.labelDurTachyVal.Size = new System.Drawing.Size(75, 18);
            this.labelDurTachyVal.TabIndex = 1;
            this.labelDurTachyVal.Text = "99:99:99";
            this.labelDurTachyVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label68
            // 
            this.label68.Dock = System.Windows.Forms.DockStyle.Top;
            this.label68.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label68.Location = new System.Drawing.Point(0, 0);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(75, 16);
            this.label68.TabIndex = 0;
            this.label68.Text = "Duration";
            this.label68.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel41
            // 
            this.panel41.Controls.Add(this.labelEpManVal);
            this.panel41.Controls.Add(this.labelEpTotalVal);
            this.panel41.Controls.Add(this.label70);
            this.panel41.Controls.Add(this.labelEpAbnVal);
            this.panel41.Controls.Add(this.labelEpNormVal);
            this.panel41.Controls.Add(this.labelEpOtherVal);
            this.panel41.Controls.Add(this.labelEpVtachVal);
            this.panel41.Controls.Add(this.labelEpPaceVal);
            this.panel41.Controls.Add(this.labelEpPVCVal);
            this.panel41.Controls.Add(this.labelEpPACVal);
            this.panel41.Controls.Add(this.labelEpAflutVal);
            this.panel41.Controls.Add(this.labelEpAfibVal);
            this.panel41.Controls.Add(this.labelEpPauseVal);
            this.panel41.Controls.Add(this.labelEpBradyVal);
            this.panel41.Controls.Add(this.labelEpTachyVal);
            this.panel41.Controls.Add(this.label77);
            this.panel41.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel41.Location = new System.Drawing.Point(77, 0);
            this.panel41.Name = "panel41";
            this.panel41.Size = new System.Drawing.Size(74, 271);
            this.panel41.TabIndex = 1;
            // 
            // labelEpManVal
            // 
            this.labelEpManVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpManVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelEpManVal.Location = new System.Drawing.Point(0, 254);
            this.labelEpManVal.Name = "labelEpManVal";
            this.labelEpManVal.Size = new System.Drawing.Size(74, 18);
            this.labelEpManVal.TabIndex = 5;
            this.labelEpManVal.Text = "^99";
            this.labelEpManVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelEpTotalVal
            // 
            this.labelEpTotalVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpTotalVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelEpTotalVal.Location = new System.Drawing.Point(0, 236);
            this.labelEpTotalVal.Name = "labelEpTotalVal";
            this.labelEpTotalVal.Size = new System.Drawing.Size(74, 18);
            this.labelEpTotalVal.TabIndex = 6;
            this.labelEpTotalVal.Text = "^99";
            this.labelEpTotalVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label70
            // 
            this.label70.Dock = System.Windows.Forms.DockStyle.Top;
            this.label70.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.Location = new System.Drawing.Point(0, 232);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(74, 4);
            this.label70.TabIndex = 10;
            // 
            // labelEpAbnVal
            // 
            this.labelEpAbnVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpAbnVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelEpAbnVal.Location = new System.Drawing.Point(0, 214);
            this.labelEpAbnVal.Name = "labelEpAbnVal";
            this.labelEpAbnVal.Size = new System.Drawing.Size(74, 18);
            this.labelEpAbnVal.TabIndex = 19;
            this.labelEpAbnVal.Text = "^99";
            this.labelEpAbnVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelEpNormVal
            // 
            this.labelEpNormVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpNormVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelEpNormVal.Location = new System.Drawing.Point(0, 196);
            this.labelEpNormVal.Name = "labelEpNormVal";
            this.labelEpNormVal.Size = new System.Drawing.Size(74, 18);
            this.labelEpNormVal.TabIndex = 18;
            this.labelEpNormVal.Text = "^99";
            this.labelEpNormVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelEpOtherVal
            // 
            this.labelEpOtherVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpOtherVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelEpOtherVal.Location = new System.Drawing.Point(0, 178);
            this.labelEpOtherVal.Name = "labelEpOtherVal";
            this.labelEpOtherVal.Size = new System.Drawing.Size(74, 18);
            this.labelEpOtherVal.TabIndex = 17;
            this.labelEpOtherVal.Text = "^99";
            this.labelEpOtherVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelEpVtachVal
            // 
            this.labelEpVtachVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpVtachVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelEpVtachVal.Location = new System.Drawing.Point(0, 160);
            this.labelEpVtachVal.Name = "labelEpVtachVal";
            this.labelEpVtachVal.Size = new System.Drawing.Size(74, 18);
            this.labelEpVtachVal.TabIndex = 16;
            this.labelEpVtachVal.Text = "^99";
            this.labelEpVtachVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelEpPaceVal
            // 
            this.labelEpPaceVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpPaceVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelEpPaceVal.Location = new System.Drawing.Point(0, 142);
            this.labelEpPaceVal.Name = "labelEpPaceVal";
            this.labelEpPaceVal.Size = new System.Drawing.Size(74, 18);
            this.labelEpPaceVal.TabIndex = 15;
            this.labelEpPaceVal.Text = "^99";
            this.labelEpPaceVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelEpPVCVal
            // 
            this.labelEpPVCVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpPVCVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelEpPVCVal.Location = new System.Drawing.Point(0, 124);
            this.labelEpPVCVal.Name = "labelEpPVCVal";
            this.labelEpPVCVal.Size = new System.Drawing.Size(74, 18);
            this.labelEpPVCVal.TabIndex = 13;
            this.labelEpPVCVal.Text = "^99";
            this.labelEpPVCVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelEpPACVal
            // 
            this.labelEpPACVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpPACVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelEpPACVal.Location = new System.Drawing.Point(0, 106);
            this.labelEpPACVal.Name = "labelEpPACVal";
            this.labelEpPACVal.Size = new System.Drawing.Size(74, 18);
            this.labelEpPACVal.TabIndex = 12;
            this.labelEpPACVal.Text = "^99";
            this.labelEpPACVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelEpAflutVal
            // 
            this.labelEpAflutVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpAflutVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelEpAflutVal.Location = new System.Drawing.Point(0, 88);
            this.labelEpAflutVal.Name = "labelEpAflutVal";
            this.labelEpAflutVal.Size = new System.Drawing.Size(74, 18);
            this.labelEpAflutVal.TabIndex = 14;
            this.labelEpAflutVal.Text = "^99";
            this.labelEpAflutVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelEpAfibVal
            // 
            this.labelEpAfibVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpAfibVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelEpAfibVal.Location = new System.Drawing.Point(0, 70);
            this.labelEpAfibVal.Name = "labelEpAfibVal";
            this.labelEpAfibVal.Size = new System.Drawing.Size(74, 18);
            this.labelEpAfibVal.TabIndex = 4;
            this.labelEpAfibVal.Text = "^99";
            this.labelEpAfibVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelEpPauseVal
            // 
            this.labelEpPauseVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpPauseVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelEpPauseVal.Location = new System.Drawing.Point(0, 52);
            this.labelEpPauseVal.Name = "labelEpPauseVal";
            this.labelEpPauseVal.Size = new System.Drawing.Size(74, 18);
            this.labelEpPauseVal.TabIndex = 3;
            this.labelEpPauseVal.Text = "^99";
            this.labelEpPauseVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelEpBradyVal
            // 
            this.labelEpBradyVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpBradyVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelEpBradyVal.Location = new System.Drawing.Point(0, 34);
            this.labelEpBradyVal.Name = "labelEpBradyVal";
            this.labelEpBradyVal.Size = new System.Drawing.Size(74, 18);
            this.labelEpBradyVal.TabIndex = 2;
            this.labelEpBradyVal.Text = "^99";
            this.labelEpBradyVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelEpTachyVal
            // 
            this.labelEpTachyVal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpTachyVal.Font = new System.Drawing.Font("Arial", 10F);
            this.labelEpTachyVal.Location = new System.Drawing.Point(0, 16);
            this.labelEpTachyVal.Name = "labelEpTachyVal";
            this.labelEpTachyVal.Size = new System.Drawing.Size(74, 18);
            this.labelEpTachyVal.TabIndex = 1;
            this.labelEpTachyVal.Text = "^99";
            this.labelEpTachyVal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label77
            // 
            this.label77.Dock = System.Windows.Forms.DockStyle.Top;
            this.label77.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label77.Location = new System.Drawing.Point(0, 0);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(74, 16);
            this.label77.TabIndex = 0;
            this.label77.Text = "Episodes";
            this.label77.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel57
            // 
            this.panel57.Controls.Add(this.label82);
            this.panel57.Controls.Add(this.label79);
            this.panel57.Controls.Add(this.label80);
            this.panel57.Controls.Add(this.label66);
            this.panel57.Controls.Add(this.label51);
            this.panel57.Controls.Add(this.label48);
            this.panel57.Controls.Add(this.label47);
            this.panel57.Controls.Add(this.label46);
            this.panel57.Controls.Add(this.label23);
            this.panel57.Controls.Add(this.label21);
            this.panel57.Controls.Add(this.label43);
            this.panel57.Controls.Add(this.label83);
            this.panel57.Controls.Add(this.label84);
            this.panel57.Controls.Add(this.label85);
            this.panel57.Controls.Add(this.label86);
            this.panel57.Controls.Add(this.label87);
            this.panel57.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel57.Location = new System.Drawing.Point(0, 0);
            this.panel57.Name = "panel57";
            this.panel57.Size = new System.Drawing.Size(77, 271);
            this.panel57.TabIndex = 0;
            // 
            // label82
            // 
            this.label82.Dock = System.Windows.Forms.DockStyle.Top;
            this.label82.Font = new System.Drawing.Font("Arial", 10F);
            this.label82.Location = new System.Drawing.Point(0, 254);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(77, 18);
            this.label82.TabIndex = 5;
            this.label82.Text = "Manual";
            // 
            // label79
            // 
            this.label79.Dock = System.Windows.Forms.DockStyle.Top;
            this.label79.Font = new System.Drawing.Font("Arial", 10F);
            this.label79.Location = new System.Drawing.Point(0, 236);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(77, 18);
            this.label79.TabIndex = 6;
            this.label79.Text = "Events";
            // 
            // label80
            // 
            this.label80.Dock = System.Windows.Forms.DockStyle.Top;
            this.label80.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.Location = new System.Drawing.Point(0, 232);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(77, 4);
            this.label80.TabIndex = 9;
            // 
            // label66
            // 
            this.label66.Dock = System.Windows.Forms.DockStyle.Top;
            this.label66.Font = new System.Drawing.Font("Arial", 10F);
            this.label66.Location = new System.Drawing.Point(0, 214);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(77, 18);
            this.label66.TabIndex = 17;
            this.label66.Text = "AbNormal";
            // 
            // label51
            // 
            this.label51.Dock = System.Windows.Forms.DockStyle.Top;
            this.label51.Font = new System.Drawing.Font("Arial", 10F);
            this.label51.Location = new System.Drawing.Point(0, 196);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(77, 18);
            this.label51.TabIndex = 16;
            this.label51.Text = "Normal";
            // 
            // label48
            // 
            this.label48.Dock = System.Windows.Forms.DockStyle.Top;
            this.label48.Font = new System.Drawing.Font("Arial", 10F);
            this.label48.Location = new System.Drawing.Point(0, 178);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(77, 18);
            this.label48.TabIndex = 15;
            this.label48.Text = "Other";
            // 
            // label47
            // 
            this.label47.Dock = System.Windows.Forms.DockStyle.Top;
            this.label47.Font = new System.Drawing.Font("Arial", 10F);
            this.label47.Location = new System.Drawing.Point(0, 160);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(77, 18);
            this.label47.TabIndex = 14;
            this.label47.Text = "Vtach";
            // 
            // label46
            // 
            this.label46.Dock = System.Windows.Forms.DockStyle.Top;
            this.label46.Font = new System.Drawing.Font("Arial", 10F);
            this.label46.Location = new System.Drawing.Point(0, 142);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(77, 18);
            this.label46.TabIndex = 13;
            this.label46.Text = "Pace";
            // 
            // label23
            // 
            this.label23.Dock = System.Windows.Forms.DockStyle.Top;
            this.label23.Font = new System.Drawing.Font("Arial", 10F);
            this.label23.Location = new System.Drawing.Point(0, 124);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(77, 18);
            this.label23.TabIndex = 11;
            this.label23.Text = "PVC-VEB";
            // 
            // label21
            // 
            this.label21.Dock = System.Windows.Forms.DockStyle.Top;
            this.label21.Font = new System.Drawing.Font("Arial", 10F);
            this.label21.Location = new System.Drawing.Point(0, 106);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(77, 18);
            this.label21.TabIndex = 10;
            this.label21.Text = "PAC-SVEB";
            // 
            // label43
            // 
            this.label43.Dock = System.Windows.Forms.DockStyle.Top;
            this.label43.Font = new System.Drawing.Font("Arial", 10F);
            this.label43.Location = new System.Drawing.Point(0, 88);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(77, 18);
            this.label43.TabIndex = 12;
            this.label43.Text = "AFlut";
            // 
            // label83
            // 
            this.label83.Dock = System.Windows.Forms.DockStyle.Top;
            this.label83.Font = new System.Drawing.Font("Arial", 10F);
            this.label83.Location = new System.Drawing.Point(0, 70);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(77, 18);
            this.label83.TabIndex = 4;
            this.label83.Text = "AFib";
            // 
            // label84
            // 
            this.label84.Dock = System.Windows.Forms.DockStyle.Top;
            this.label84.Font = new System.Drawing.Font("Arial", 10F);
            this.label84.Location = new System.Drawing.Point(0, 52);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(77, 18);
            this.label84.TabIndex = 3;
            this.label84.Text = "Pause";
            // 
            // label85
            // 
            this.label85.Dock = System.Windows.Forms.DockStyle.Top;
            this.label85.Font = new System.Drawing.Font("Arial", 10F);
            this.label85.Location = new System.Drawing.Point(0, 34);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(77, 18);
            this.label85.TabIndex = 2;
            this.label85.Text = "Brady";
            // 
            // label86
            // 
            this.label86.Dock = System.Windows.Forms.DockStyle.Top;
            this.label86.Font = new System.Drawing.Font("Arial", 10F);
            this.label86.Location = new System.Drawing.Point(0, 16);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(77, 18);
            this.label86.TabIndex = 1;
            this.label86.Text = "Tachy";
            // 
            // label87
            // 
            this.label87.Dock = System.Windows.Forms.DockStyle.Top;
            this.label87.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label87.Location = new System.Drawing.Point(0, 0);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(77, 16);
            this.label87.TabIndex = 0;
            this.label87.Text = "Type";
            // 
            // CMCTTrendDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(1288, 980);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel45);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CMCTTrendDisplay";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "^title";
            this.Load += new System.EventHandler(this.CMCTTrendDisplay_Load);
            this.Shown += new System.EventHandler(this.CMCTTrendDisplay_Shown);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel46.ResumeLayout(false);
            this.panel45.ResumeLayout(false);
            this.panel34.ResumeLayout(false);
            this.panel34.PerformLayout();
            this.panel58.ResumeLayout(false);
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel59.ResumeLayout(false);
            this.panel59.PerformLayout();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            this.panel17.ResumeLayout(false);
            this.panel60.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartAllEvents)).EndInit();
            this.panel61.ResumeLayout(false);
            this.panel55.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartManual)).EndInit();
            this.panel56.ResumeLayout(false);
            this.panel36.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartOther)).EndInit();
            this.panel37.ResumeLayout(false);
            this.panel19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartAbN)).EndInit();
            this.panel20.ResumeLayout(false);
            this.panel33.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartVtach)).EndInit();
            this.panel35.ResumeLayout(false);
            this.panel53.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartPace)).EndInit();
            this.panel54.ResumeLayout(false);
            this.panel29.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartPVC)).EndInit();
            this.panel30.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartPAC)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel31.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartAflut)).EndInit();
            this.panel32.ResumeLayout(false);
            this.panel51.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartAfib)).EndInit();
            this.panel52.ResumeLayout(false);
            this.panel49.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartPause)).EndInit();
            this.panel50.ResumeLayout(false);
            this.panel44.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartBrady)).EndInit();
            this.panel48.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartTachy)).EndInit();
            this.panel43.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartHR)).EndInit();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel39.ResumeLayout(false);
            this.panel21.ResumeLayout(false);
            this.panel26.ResumeLayout(false);
            this.panel25.ResumeLayout(false);
            this.panel24.ResumeLayout(false);
            this.panel23.ResumeLayout(false);
            this.panel22.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartSummaryBar)).EndInit();
            this.panel15.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel40.ResumeLayout(false);
            this.panel41.ResumeLayout(false);
            this.panel57.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel46;
        private System.Windows.Forms.Label labelDoB;
        private System.Windows.Forms.Label labelPatName;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label labelPatID;
        private System.Windows.Forms.Label labelPatIDHeader;
        private System.Windows.Forms.Panel panel45;
        private System.Windows.Forms.Label labelRunState;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label labelNearSec;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label labelTrendTime;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label labelTrendStart;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label labelTrendEnd;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label labelAgeGender;
        private System.Windows.Forms.DateTimePicker dateTimePickerFrom;
        private System.Windows.Forms.Button buttonQueryECG;
        private System.Windows.Forms.Label labelTo;
        private System.Windows.Forms.Label labelFrom;
        private System.Windows.Forms.RadioButton radioButtonMonth;
        private System.Windows.Forms.RadioButton radioButtonPeriod;
        private System.Windows.Forms.RadioButton radioButtonWeek;
        private System.Windows.Forms.RadioButton radioButtonHour;
        private System.Windows.Forms.ComboBox comboBoxFromAM;
        private System.Windows.Forms.TextBox textBoxFromHour;
        private System.Windows.Forms.TextBox textBoxFromMin;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBoxToMin;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ComboBox comboBoxToAM;
        private System.Windows.Forms.TextBox textBoxToHour;
        private System.Windows.Forms.DateTimePicker dateTimePickerTo;
        private System.Windows.Forms.Button buttonSetTimeFrame;
        private System.Windows.Forms.RadioButton radioButtonAll;
        private System.Windows.Forms.RadioButton radioButtonDay;
        private System.Windows.Forms.Button buttonP24;
        private System.Windows.Forms.Button buttonP6;
        private System.Windows.Forms.Button buttonP1;
        private System.Windows.Forms.Button buttonM1;
        private System.Windows.Forms.Button buttonM6;
        private System.Windows.Forms.Button buttonM24;
        private System.Windows.Forms.Button button0h00;
        private System.Windows.Forms.Label labelQueryInfo;
        private System.Windows.Forms.Label labelMinutes;
        private System.Windows.Forms.TextBox textBoxLength;
        private System.Windows.Forms.Label labelLength;
        private System.Windows.Forms.TextBox textBoxStartMinute;
        private System.Windows.Forms.ComboBox comboBoxStart;
        private System.Windows.Forms.TextBox textBoxStartHour;
        private System.Windows.Forms.DateTimePicker dateTimeStart;
        private System.Windows.Forms.Label labelStart;
        private System.Windows.Forms.Label labelColon;
        private System.Windows.Forms.Button buttonCopyFrom;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label labelStudy;
        private System.Windows.Forms.Label labelStartDate;
        private System.Windows.Forms.RadioButton radioButton12hours;
        private System.Windows.Forms.RadioButton radioButton6hours;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label labelNow;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label labelDaysHours;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label labelRecorderID;
        private System.Windows.Forms.Label labelAnalysisUse;
        private System.Windows.Forms.CheckBox checkBoxPrintDur;
        private System.Windows.Forms.CheckBox checkBoxPrintStat;
        private System.Windows.Forms.Panel panel34;
        private System.Windows.Forms.Panel panel58;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label labelUpodateTime;
        private System.Windows.Forms.Label labelRecAnalysis;
        private System.Windows.Forms.Panel panel59;
        private System.Windows.Forms.Label labelFoundRec;
        private System.Windows.Forms.Label labelPrintResult;
        private System.Windows.Forms.CheckBox checkBoxAnonymize;
        private System.Windows.Forms.CheckBox checkBoxBlackWhite;
        private System.Windows.Forms.CheckBox checkBoxValidated;
        private System.Windows.Forms.Label labelPrint;
        private System.Windows.Forms.CheckBox checkBoxLogCalc;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.CheckBox checkBoxCountAnOnly;
        private System.Windows.Forms.CheckBox checkBoxUseAnalysis;
        private System.Windows.Forms.CheckBox checkBoxUseTrend;
        private System.Windows.Forms.CheckBox checkBoxPulseOnOnly;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label labelFDL;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Panel panel60;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartAllEvents;
        private System.Windows.Forms.Panel panel61;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Panel panel55;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartManual;
        private System.Windows.Forms.Panel panel56;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Panel panel36;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartOther;
        private System.Windows.Forms.Panel panel37;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartVtach;
        private System.Windows.Forms.Panel panel35;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Panel panel53;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartPace;
        private System.Windows.Forms.Panel panel54;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartPVC;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartPAC;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartAflut;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Panel panel51;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartAfib;
        private System.Windows.Forms.Panel panel52;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Panel panel49;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartPause;
        private System.Windows.Forms.Panel panel50;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Panel panel44;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartBrady;
        private System.Windows.Forms.Panel panel48;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartTachy;
        private System.Windows.Forms.Panel panel43;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartHR;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel39;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Label labelPerTotalVal;
        private System.Windows.Forms.Label labelPerVtachVal;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label labelPerPVCVal;
        private System.Windows.Forms.Label labelPerPACVal;
        private System.Windows.Forms.Label labelPerAfibVal;
        private System.Windows.Forms.Label labelPerPauseVal;
        private System.Windows.Forms.Label labelPerBradyVal;
        private System.Windows.Forms.Label labelPerTachyVal;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Panel panel40;
        private System.Windows.Forms.Label labelDurManVal;
        private System.Windows.Forms.Label labelDurTotalVal;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label labelDurOtherVal;
        private System.Windows.Forms.Label labelDurVtachVal;
        private System.Windows.Forms.Label labelDurPaceVal;
        private System.Windows.Forms.Label labelDurPVCVal;
        private System.Windows.Forms.Label labelDurPACVal;
        private System.Windows.Forms.Label labelDurAflutVal;
        private System.Windows.Forms.Label labelDurAfibVal;
        private System.Windows.Forms.Label labelDurPauseVal;
        private System.Windows.Forms.Label labelDurBradyVal;
        private System.Windows.Forms.Label labelDurTachyVal;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Panel panel41;
        private System.Windows.Forms.Label labelEpManVal;
        private System.Windows.Forms.Label labelEpTotalVal;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label labelEpOtherVal;
        private System.Windows.Forms.Label labelEpVtachVal;
        private System.Windows.Forms.Label labelEpPaceVal;
        private System.Windows.Forms.Label labelEpPVCVal;
        private System.Windows.Forms.Label labelEpPACVal;
        private System.Windows.Forms.Label labelEpAflutVal;
        private System.Windows.Forms.Label labelEpAfibVal;
        private System.Windows.Forms.Label labelEpPauseVal;
        private System.Windows.Forms.Label labelEpBradyVal;
        private System.Windows.Forms.Label labelEpTachyVal;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Panel panel57;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Label labelPerMan;
        private System.Windows.Forms.Label labelPerTotal;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label labelPerPVC;
        private System.Windows.Forms.Label labelPerPAC;
        private System.Windows.Forms.Label labelPerAfib;
        private System.Windows.Forms.Label labelPerPause;
        private System.Windows.Forms.Label labelPerBrady;
        private System.Windows.Forms.Label labelPerTachy;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Label labelDurDevice;
        private System.Windows.Forms.Label labelDurMan;
        private System.Windows.Forms.Label labelDurTotal;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label labelDurPVC;
        private System.Windows.Forms.Label labelDurPAC;
        private System.Windows.Forms.Label labelDurAfib;
        private System.Windows.Forms.Label labelDurPause;
        private System.Windows.Forms.Label labelDurBrady;
        private System.Windows.Forms.Label labelDurTachy;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Label labelEpMan;
        private System.Windows.Forms.Label labelEpTotal;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label labelEpPVC;
        private System.Windows.Forms.Label labelEpPAC;
        private System.Windows.Forms.Label labelUser;
        private System.Windows.Forms.Label labelEpAfib;
        private System.Windows.Forms.Label labelEpPause;
        private System.Windows.Forms.Label labelEpBrady;
        private System.Windows.Forms.Label labelEpTachy;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel42;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartSummaryBar;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label labelPerOtherVal;
        private System.Windows.Forms.Label labelPerPaceVal;
        private System.Windows.Forms.Label labelPerAflutVal;
        private System.Windows.Forms.Label labelMouseInfo;
        private System.Windows.Forms.Label labelPerPace;
        private System.Windows.Forms.Label labelDurPace;
        private System.Windows.Forms.Label labelEpPace;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Button buttonQueryTrend;
        private System.Windows.Forms.Button buttonxh00;
        private System.Windows.Forms.Button buttonQueryh00;
        private System.Windows.Forms.Button buttonQuery1h;
        private System.Windows.Forms.Label labelPerNormVal;
        private System.Windows.Forms.Label labelDurNormVal;
        private System.Windows.Forms.Label labelEpNormVal;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label labelPlotSec;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.CheckBox checkBoxTogleAfOnly;
        private System.Windows.Forms.Label labelHrGap;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.CheckBox checkBoxQCD;
        private System.Windows.Forms.Label labelUseExluded;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label labelPerAbnVal;
        private System.Windows.Forms.Label labelDurAbnVal;
        private System.Windows.Forms.Label labelEpAbnVal;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartAbN;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label labelTimeZone;
        private System.Windows.Forms.Label labelStudyNote;
    }
}