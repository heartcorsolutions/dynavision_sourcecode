﻿using Event_Base;
using Program_Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventboardEntryForms
{
    public partial class CClientInfoForm : Form
    {
        public CClientInfo _mInfo;
        private List<CSqlDataTableRow> _mList;
        private CClientInfo _mSearchInfo = null;
        private bool _mbLoadListError = false;
        private bool _mbEnableSelect;
        public UInt32 _mSelectedIndex = 0;
        public bool _mbSelected = false;

        public CClientInfoForm(CClientInfo AClient, UInt32 APreSelectClientIX, bool AbEnableSelect)
        {
            try
            {
                CDvtmsData.sbInitData();

                _mList = new List<CSqlDataTableRow>();              
                _mInfo = new CClientInfo(CDvtmsData.sGetDBaseConnection());

                if (_mInfo == null || _mList == null)
                {
                    CProgram.sLogLine(" CLientInfoForm() failed to init Client data");
                    Close();
                }
                else
                {
                    _mInfo.mbActive = true;
                    if (AClient != null)
                    {
                        _mInfo.mbCopyFrom(AClient);
                        _mSelectedIndex = AClient.mIndex_KEY;
                    }
                    else if(APreSelectClientIX > 0)
                    {
                        _mSelectedIndex = APreSelectClientIX;
                    }
                    InitializeComponent();

                    Width = 1600;   // Nicer size then minimum 1240
                    buttonSelect.Visible = AbEnableSelect;
                    buttonRandom.Visible = CLicKeyDev.sbDeviceIsProgrammer();
                    mLoadList();
                    mSetValues();
                    mbCheckForm(AClient == null ? "New" : "Edit");

                    Text = CProgram.sMakeProgTitle("Client", false, true);

                }
                labelSelected.Visible = CLicKeyDev.sbDeviceIsProgrammer();
            }
            catch ( Exception ex )
            {
                CProgram.sLogException(" CLientInfoForm() failed to init", ex);
            }
         }

        public void mUpdateButtons()
        {
            if( _mInfo != null )
            {
                bool bNew = _mInfo.mIndex_KEY == 0;
                textBoxClientNr.Text = bNew ? "" : _mInfo.mIndex_KEY.ToString();
                buttonAddUpdate.Text = bNew ? "Add" : "Update";
                buttonDisable.Text = _mInfo.mbActive ? "Disable" : "Enable";
                textBoxClientLabel.ReadOnly = false == bNew;
                textBoxClientNr.ReadOnly = false == bNew;
                mGetCursorSelected();
            }
        }
        private UInt32 mGetCursorSelected()
        {
            UInt32 index = 0;

            try
            {
                if (dataGridViewList.SelectedRows != null && dataGridViewList.SelectedRows.Count > 0)
                {
                    string s = dataGridViewList.SelectedRows[0].Cells[0].Value == null ? ""
                        : dataGridViewList.SelectedRows[0].Cells[0].Value.ToString();

                    if (false == UInt32.TryParse(s, out index))
                    {
                        index = 0;
                    }
                }
                labelSelected.Text = index > 0 ? index.ToString() : "";
                _mSelectedIndex = index;
                buttonSelect.Enabled = _mSelectedIndex > 0;
             }
            catch (Exception ex)
            {
                CProgram.sLogException("CLientInfoForm() get cursor failed", ex);
            }
            return index;
        }
        private bool mbLoadSelected()
        {
            bool bLoaded = false;
            try
            {
                _mSelectedIndex = mGetCursorSelected();

                if (_mSelectedIndex > 0)
                {
                    foreach (CClientInfo row in _mList)
                    {
                        if (_mSelectedIndex > 0 && row.mIndex_KEY == _mSelectedIndex)
                        {
                            row.mbCopyTo(_mInfo);
                            bLoaded = true;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("CLientInfoForm() load Selected cursor failed: " + _mSelectedIndex.ToString(), ex);
            }
            return bLoaded;
        }

        public void mSetValues()
        {
            // set the values from info on the form

            textBoxClientNr.Text = _mInfo.mIndex_KEY == 0 ? "" : _mInfo.mIndex_KEY.ToString();
            checkBoxActive.Checked = _mInfo.mbActive;
            textBoxClientLabel.Text = _mInfo._mLabel;
            textBoxClientFullName.Text = _mInfo._mFullName;
            textBoxClientAddress.Text = _mInfo._mAddress;
            textBoxClientZip.Text = _mInfo._mZipCode;
            textBoxHouseNumber.Text = _mInfo._mHouseNr;
            textBoxClientCity.Text = _mInfo._mCity;
            comboClientStateList.Text = _mInfo._mState;
            if (CDvtmsData._sEnumListCountries != null)
            {
                CDvtmsData._sEnumListCountries.mFillComboBox(comboBoxClientCountryList, false, 0, false, "", _mInfo._mCountryCode, "-- Select--");
            }
            textBoxClientCentralNr.Text = _mInfo._mCentralTelNr;
            textBoxClientDirectNr.Text = _mInfo._mDirectTelNr;
            textBoxClientFax.Text = _mInfo._mFaxNr;
            textBoxClientEmail.Text = _mInfo._mEmail;
            textBoxDepartmentName.Text = _mInfo._mDepartmentName;
            textBoxDepartmentHead.Text = _mInfo._mDepartmentHead;
            textBoxDepartmentPhone.Text = _mInfo._mDepartmentTelNr;
        }

        private UInt32 mReadIndex( string AString )
        {
            UInt32 index = 0;

            if (AString != null && AString.Length > 0)
            {
                if (false == UInt32.TryParse(AString, out index))
                {
                    index = 0;
                }
            }
            return index;
        }

        public void mGetValues( out UInt32 ArIndex )
        {
            // get the values from the form  and store in info

            ArIndex = mReadIndex(textBoxClientNr.Text);
            _mInfo.mbActive = checkBoxActive.Checked;
            _mInfo._mLabel = textBoxClientLabel.Text;
            _mInfo._mFullName = textBoxClientFullName.Text;
            _mInfo._mAddress = textBoxClientAddress.Text;
            _mInfo._mZipCode = textBoxClientZip.Text;
             _mInfo._mHouseNr = textBoxHouseNumber.Text;
            _mInfo._mCity = textBoxClientCity.Text;
            _mInfo._mState = comboClientStateList.Text;
            if (CDvtmsData._sEnumListCountries != null)
            {
                _mInfo._mCountryCode = CDvtmsData._sEnumListCountries.mReadComboBoxCode(comboBoxClientCountryList, false, "");
            }

            _mInfo._mCentralTelNr = textBoxClientCentralNr.Text;
            _mInfo._mDirectTelNr = textBoxClientDirectNr.Text;
            _mInfo._mFaxNr = textBoxClientFax.Text;
            _mInfo._mEmail = textBoxClientEmail.Text;
            _mInfo._mDepartmentName = textBoxDepartmentName.Text;
            _mInfo._mDepartmentHead = textBoxDepartmentHead.Text;
            _mInfo._mDepartmentTelNr = textBoxDepartmentPhone.Text;

        }

        private void textBoxManufacturerAddress_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxClientName_TextChanged(object sender, EventArgs e)
        {

        }

        private void dataGridViewClientList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void CAddClientsForm_Load(object sender, EventArgs e)
        {

        }

        private void buttonAddClient_Click(object sender, EventArgs e)
        {

        }

 
        public void mUpdateList()
        {
            if (_mList != null && _mInfo != null)
            {
                int nList, nResult = 0;

                dataGridViewList.Rows.Clear();
 
                if (_mList == null || (nList = _mList.Count()) == 0)
                {
                    labelListN.Text = "0" + (_mbLoadListError ? "!" : "");
                    labelResultN.Text = "-";
                }
                else
                {
                    bool bAll = checkBoxShowDeactivated.Checked;
                    bool bSearch = checkBoxShowSearchResult.Checked && _mSearchInfo != null;
                    bool bAdd;

                    foreach (CClientInfo row in _mList)
                    {
                        if (bAll || row.mbActive)
                        {
                            bAdd = true;
                            if (bSearch)
                            {
                                if (bAdd && _mSearchInfo.mIndex_KEY != 0) bAdd = row.mIndex_KEY == _mSearchInfo.mIndex_KEY;
                                if (bAdd) bAdd = CProgram.sbCmpSearchString(row._mLabel, _mInfo._mLabel);
                                if (bAdd) bAdd = CProgram.sbCmpSearchString(row._mFullName, _mInfo._mFullName);
                                if (bAdd) bAdd = CProgram.sbCmpSearchString(row._mAddress, _mInfo._mAddress);
                                if (bAdd) bAdd = CProgram.sbCmpSearchString(row._mZipCode, _mInfo._mZipCode);
                                if (bAdd) bAdd = CProgram.sbCmpSearchString(row._mHouseNr, _mInfo._mHouseNr);
                                if (bAdd) bAdd = CProgram.sbCmpSearchString(row._mCity, _mInfo._mCity);
                                if (bAdd) bAdd = CProgram.sbCmpSearchString(row._mState, _mInfo._mState);
                                if (bAdd) bAdd = CProgram.sbCmpSearchString(row._mCountryCode, _mInfo._mCountryCode);
                                

                                if (bAdd) bAdd = CProgram.sbCmpSearchString(row._mCentralTelNr, _mInfo._mCentralTelNr);
                                if (bAdd) bAdd = CProgram.sbCmpSearchString(row._mDirectTelNr, _mInfo._mDirectTelNr);
                                if (bAdd) bAdd = CProgram.sbCmpSearchString(row._mFaxNr, _mInfo._mFaxNr);
                                if (bAdd) bAdd = CProgram.sbCmpSearchString(row._mEmail, _mInfo._mEmail);
                                if (bAdd) bAdd = CProgram.sbCmpSearchString(row._mDepartmentName, _mInfo._mDepartmentName);
                                if (bAdd) bAdd = CProgram.sbCmpSearchString(row._mDepartmentHead, _mInfo._mDepartmentHead);
                                if (bAdd) bAdd = CProgram.sbCmpSearchString(row._mDepartmentTelNr, _mInfo._mDepartmentTelNr);
                            }
                            if (bAdd)
                            {
                                dataGridViewList.Rows.Add(row.mIndex_KEY, row.mbActive.ToString(), row._mLabel, row._mFullName, row._mCity, row._mCountryCode,
                                    row._mDepartmentName, row._mDepartmentHead, row._mDepartmentTelNr, row._mAddress, row._mZipCode, row._mHouseNr,
                                    row._mState, row._mCentralTelNr, row._mDirectTelNr, row._mFaxNr, row._mEmail);
                                ++nResult;

                            }
                        }

                    }
                    labelListN.Text = nList.ToString() + (_mbLoadListError ? "!" : "");
                    labelResultN.Text = nResult.ToString();
                }
            }
        }

        public void mLoadList()
        {
            if (_mInfo != null)
            {
                _mInfo.mbDoSqlSelectList(out _mbLoadListError, out _mList, _mInfo.mMaskValid, 0, false, "");

                mUpdateList();
            }
        }

        private void buttonAddUpdate_Click(object sender, EventArgs e)
        {
            UInt32 index;
            mGetValues( out index);

            // check values

            if( false == mbCheckForm(_mInfo.mIndex_KEY == 0 ? "Add" : "Update" ))
            {
                return;
            }


            if( _mInfo.mIndex_KEY == 0)
            {
                // new Client

                // check if Laber is not in list

                if( _mInfo.mbDoSqlInsert())
                {
                    textBoxClientNr.Text = _mInfo.mIndex_KEY.ToString();
                    labelClientError.Text = "Inserted";
                }
                else
                {
                    labelClientError.Text = "Failed Insert";
                }
            }
            else if( index > 0 && _mInfo.mIndex_KEY == index )
             {
                // update client

                // check if label is in list (not this)
                if (_mInfo.mbDoSqlUpdate(_mInfo.mMaskValid, _mInfo.mbActive))
                {
                    textBoxClientNr.Text = "+" + _mInfo.mIndex_KEY.ToString();
                    labelClientError.Text = "Updated";
                }
                else
                {
                    labelClientError.Text = "Failed Update";
                }
            }
            mLoadList();
            mUpdateButtons();
        }

        private void buttonDup_Click(object sender, EventArgs e)
        {
            if( _mInfo != null )
            {
                _mInfo.mIndex_KEY = 0;
                _mInfo.mbActive = true;
                mSetValues();
                mUpdateButtons();
                mbCheckForm("Duplicated" );
            }
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            if (_mInfo != null)
            {
                _mInfo.mClear();
                _mInfo.mbActive = true;

                mSetValues();
                mUpdateButtons();
                mbCheckForm("Clear");

            }
        }

        private void buttonRandom_Click(object sender, EventArgs e)
        {
            if (_mInfo != null)
            {
                DateTime dt = DateTime.Now;
                string label = "H" + dt.ToString("yyyyMMddHHmmss");

                _mInfo.mClear();
                _mInfo.mbActive = true;
                _mInfo._mLabel = label;
                _mInfo._mFullName = "Name " + label;
                _mInfo._mAddress = "Adress " + label;
                _mInfo._mZipCode = label.Substring(0, 6);
                _mInfo._mHouseNr = label.Substring(7, 4);
                _mInfo._mCity = "City " + label;
                _mInfo._mState = "State " + label;
                _mInfo._mCountryCode = "NL";

                //         _mInfo.ClientCountryList = comboBoxClientCountryList.Text;
                _mInfo._mCentralTelNr = "CT" + label.Substring(4, 10);
                _mInfo._mDirectTelNr = "DT" + label.Substring(4, 10);
                _mInfo._mFaxNr = "FX" + label.Substring(4, 10);
                _mInfo._mEmail = label.Substring(5, 7) + "@" + label.Substring(0, 6) + ".com";
                _mInfo._mDepartmentName = "Dep-" + label;
                _mInfo._mDepartmentHead = "DH " + label;
                _mInfo._mDepartmentTelNr = "DepT" + label.Substring(6, 6);

                mSetValues();

                mUpdateButtons();
                mbCheckForm("Random");
            }

        }

        bool mbCheckClient(out string AErrorText)
        {
            string errorText = "init form failed";

            if (_mInfo != null)
            {
                errorText = "";

                if( _mInfo._mLabel == null || _mInfo._mLabel.Length == 0 )
                {
                      errorText = " No Name";
                }
            }
            AErrorText = errorText;

            return errorText.Length == 0;
        }

        private bool mbCheckForm(string AState)
        {
            bool bOk = true;
            string errorText = "";

            if (mbCheckClient(out errorText))
            {
                labelClientError.Text = "";
            }
            else
            {
                labelClientError.Text = AState + ": " + errorText;
                bOk = false;
            }
            return bOk;
        }


        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void checkBoxShowDeactivated_CheckedChanged(object sender, EventArgs e)
        {
            mUpdateList();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            mLoadList();
            mUpdateButtons();
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            UInt32 index;

            mGetValues(out index);

            if( _mInfo != null )
            {
                _mSearchInfo = _mInfo.mCreateCopy() as CClientInfo;
                checkBoxShowSearchResult.Checked = _mSearchInfo != null;
                if (_mSearchInfo != null) _mSearchInfo.mIndex_KEY = index;

            }
            mUpdateList();
            mUpdateButtons();
            mbCheckForm("List");

        }

        private void checkBoxShowSearchResult_CheckedChanged(object sender, EventArgs e)
        {
            mUpdateList();
            mUpdateButtons();
            mbCheckForm("List");
        }

        public bool mbCopySelected( bool AbNewEntry )
        {
            bool bCopied = false;

            if (dataGridViewList.Rows != null && dataGridViewList.Rows.Count > 0)
            {
                if (dataGridViewList.SelectedRows != null && dataGridViewList.SelectedRows.Count > 0 && dataGridViewList.SelectedRows[0] != null)
                {
                    string indexStr = dataGridViewList.SelectedRows[0].Cells[0].Value == null ? ""
                       : dataGridViewList.SelectedRows[0].Cells[0].Value.ToString();
                    int index;

                    if (int.TryParse(indexStr, out index) && index > 0)
                    {
                        foreach (CClientInfo row in _mList)
                        {
                            if (index == row.mIndex_KEY)
                            {
                                // found selected
                                CClientInfo copy = row.mCreateCopy() as CClientInfo;

                                if (copy != null)
                                {
                                    _mInfo = copy;
                                    bCopied = true;
                                    if (AbNewEntry)
                                    {
                                        _mInfo.mIndex_KEY = 0;
                                    }
                                    mSetValues();
                                    mUpdateButtons();
                                    mbCheckForm("Copy");
                                }
                                break;
                            }
                        }
                    }
                }
            }
            return bCopied;
        }
        private void buttonEdit_Click(object sender, EventArgs e)
        {
            mbCopySelected(false);
        }

        private void buttonDuplicate_Click(object sender, EventArgs e)
        {
            mbCopySelected(true);
        }

        private void dataGridClientList_DoubleClick(object sender, EventArgs e)
        {
            mbCopySelected(false);
        }

        private void buttonDisable_Click(object sender, EventArgs e)
        {
            if(mbCopySelected(false))
            {
                UInt32 index;
                mGetValues(out index);

                // check values

                if (_mInfo.mIndex_KEY == 0)
                {
                    labelClientError.Text = "Not stored";
                }
                else if (index > 0 && _mInfo.mIndex_KEY == index)
                {
                    // update client
                    bool b = _mInfo.mbActive;
                    _mInfo.mbActive = !b;
                    // check if label is in list (not this)
                    if (_mInfo.mbDoSqlUpdate(_mInfo.mMaskValid, _mInfo.mbActive))
                    {
                        textBoxClientNr.Text = "+" + _mInfo.mIndex_KEY.ToString();
                        labelClientError.Text = b ? "Disabled" : "Enabled";
                    }
                    else
                    {
                        labelClientError.Text = b ? "Failed to Disable" : "Failed to Enable";
                    }
                }
            }
            mSetValues();
            mLoadList();
            mUpdateButtons();
        }

        private void dataGridViewList_Click(object sender, EventArgs e)
        {
            mUpdateButtons();
        }

        private void buttonSelect_Click(object sender, EventArgs e)
        {
            if( mbLoadSelected())
            {
                _mbSelected = true;
                Close();
            }
        }

        private void labelSelected_Click(object sender, EventArgs e)
        {

        }
    }
}
