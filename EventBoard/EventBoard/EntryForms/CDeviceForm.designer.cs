﻿namespace EventboardEntryForms
{
    partial class CDeviceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.labelErrorDevices = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labelIndex = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.labelError = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel50 = new System.Windows.Forms.Panel();
            this.panel46 = new System.Windows.Forms.Panel();
            this.panel51 = new System.Windows.Forms.Panel();
            this.labelShowActiveState = new System.Windows.Forms.Label();
            this.buttonSetAssigned = new System.Windows.Forms.Button();
            this.buttonSetDefective = new System.Windows.Forms.Button();
            this.buttonSetInService = new System.Windows.Forms.Button();
            this.buttonSetUnavailable = new System.Windows.Forms.Button();
            this.buttonClearStudy = new System.Windows.Forms.Button();
            this.panel49 = new System.Windows.Forms.Panel();
            this.panel42 = new System.Windows.Forms.Panel();
            this.labelErrorInstruct = new System.Windows.Forms.Label();
            this.panel43 = new System.Windows.Forms.Panel();
            this.panel31 = new System.Windows.Forms.Panel();
            this.labelShowSnr = new System.Windows.Forms.Label();
            this.buttonSetAvailable = new System.Windows.Forms.Button();
            this.buttonSetReceived = new System.Windows.Forms.Button();
            this.buttonSetIntransit = new System.Windows.Forms.Button();
            this.buttonEndUse = new System.Windows.Forms.Button();
            this.panel41 = new System.Windows.Forms.Panel();
            this.panel28 = new System.Windows.Forms.Panel();
            this.label27 = new System.Windows.Forms.Label();
            this.labelNoMct = new System.Windows.Forms.Label();
            this.labelDeviceChanged = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.labelTzStoreName = new System.Windows.Forms.Label();
            this.labelState = new System.Windows.Forms.TextBox();
            this.labelEndDate = new System.Windows.Forms.TextBox();
            this.labelStartDate = new System.Windows.Forms.TextBox();
            this.labelRefName = new System.Windows.Forms.TextBox();
            this.labelRefID = new System.Windows.Forms.TextBox();
            this.labelStudyNr = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.labelTextEndDate = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelTextRefID = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.panel22 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel52 = new System.Windows.Forms.Panel();
            this.panel54 = new System.Windows.Forms.Panel();
            this.checkBoxCh10 = new System.Windows.Forms.CheckBox();
            this.checkBoxCh9 = new System.Windows.Forms.CheckBox();
            this.checkBoxCh8 = new System.Windows.Forms.CheckBox();
            this.checkBoxCh7 = new System.Windows.Forms.CheckBox();
            this.checkBoxCh6 = new System.Windows.Forms.CheckBox();
            this.checkBoxCh5 = new System.Windows.Forms.CheckBox();
            this.checkBoxCh4 = new System.Windows.Forms.CheckBox();
            this.checkBoxCh3 = new System.Windows.Forms.CheckBox();
            this.checkBoxCh2 = new System.Windows.Forms.CheckBox();
            this.checkBoxCh1 = new System.Windows.Forms.CheckBox();
            this.panel55 = new System.Windows.Forms.Panel();
            this.label28 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.textBoxTzOffset = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.comboBoxTimeZone = new System.Windows.Forms.ComboBox();
            this.comboBoxAddStudy = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.comboBoxNextState = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.textBoxTzDefault = new System.Windows.Forms.TextBox();
            this.comboBoxRefID = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.textBoxServiceNote = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.panel53 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.labelNextServiceOff = new System.Windows.Forms.Label();
            this.textBoxIReaderNR = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.textBoxDeployRemark = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBoxUseClient = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textBoxRemLabel = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textBoxSerialNr = new System.Windows.Forms.TextBox();
            this.comboBoxModel = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.labelManufacturer = new System.Windows.Forms.Label();
            this.labelNextService = new System.Windows.Forms.Label();
            this.dateTimePickerFirstInService = new System.Windows.Forms.DateTimePicker();
            this.labelFirstInService = new System.Windows.Forms.Label();
            this.dateTimePickerNextService = new System.Windows.Forms.DateTimePicker();
            this.textBoxDeviceFirmwareVersion = new System.Windows.Forms.TextBox();
            this.labelDeviceFirmwareVersion = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.buttonChangeSNR = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonSirConfig = new System.Windows.Forms.Button();
            this.buttonTzConfig = new System.Windows.Forms.Button();
            this.buttonShowStudy = new System.Windows.Forms.Button();
            this.buttonAddUpdate = new System.Windows.Forms.Button();
            this.panel40 = new System.Windows.Forms.Panel();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.panel39 = new System.Windows.Forms.Panel();
            this.buttonDup = new System.Windows.Forms.Button();
            this.panel23 = new System.Windows.Forms.Panel();
            this.buttonClear = new System.Windows.Forms.Button();
            this.panel16 = new System.Windows.Forms.Panel();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel19 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.panel24 = new System.Windows.Forms.Panel();
            this.panel27 = new System.Windows.Forms.Panel();
            this.buttonDisable = new System.Windows.Forms.Button();
            this.panel38 = new System.Windows.Forms.Panel();
            this.buttonRefresh = new System.Windows.Forms.Button();
            this.panel36 = new System.Windows.Forms.Panel();
            this.labelListN = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelResultN = new System.Windows.Forms.Label();
            this.checkBoxShowSearchResult = new System.Windows.Forms.CheckBox();
            this.checkBoxShowDeactivated = new System.Windows.Forms.CheckBox();
            this.panel35 = new System.Windows.Forms.Panel();
            this.buttonDuplicate = new System.Windows.Forms.Button();
            this.panel34 = new System.Windows.Forms.Panel();
            this.panel33 = new System.Windows.Forms.Panel();
            this.buttonEdit = new System.Windows.Forms.Button();
            this.panel30 = new System.Windows.Forms.Panel();
            this.panel32 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.panel21 = new System.Windows.Forms.Panel();
            this.panel29 = new System.Windows.Forms.Panel();
            this.dataGridViewList = new System.Windows.Forms.DataGridView();
            this.ColumnIndexNr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Active = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnActive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1ClientName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5ClientState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3ClientZip = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7ClientCountry = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2ClientStreet = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnFullName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6ClientCity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel26 = new System.Windows.Forms.Panel();
            this.panel25 = new System.Windows.Forms.Panel();
            this.panel2.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel46.SuspendLayout();
            this.panel51.SuspendLayout();
            this.panel42.SuspendLayout();
            this.panel31.SuspendLayout();
            this.panel28.SuspendLayout();
            this.panel52.SuspendLayout();
            this.panel54.SuspendLayout();
            this.panel55.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel35.SuspendLayout();
            this.panel33.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel29.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewList)).BeginInit();
            this.SuspendLayout();
            // 
            // panel5
            // 
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1681, 2);
            this.panel5.TabIndex = 5;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.labelErrorDevices);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.labelIndex);
            this.panel2.Controls.Add(this.panel8);
            this.panel2.Controls.Add(this.panel7);
            this.panel2.Controls.Add(this.panel18);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 2);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1681, 32);
            this.panel2.TabIndex = 6;
            // 
            // labelErrorDevices
            // 
            this.labelErrorDevices.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelErrorDevices.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelErrorDevices.Location = new System.Drawing.Point(434, 0);
            this.labelErrorDevices.Name = "labelErrorDevices";
            this.labelErrorDevices.Size = new System.Drawing.Size(1128, 32);
            this.labelErrorDevices.TabIndex = 5;
            this.labelErrorDevices.Text = "E7: ________________________________ +23TZ+45Sir+7DV2\r\n";
            this.labelErrorDevices.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Right;
            this.label3.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(1562, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 32);
            this.label3.TabIndex = 3;
            this.label3.Text = "# ";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelIndex
            // 
            this.labelIndex.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelIndex.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelIndex.Location = new System.Drawing.Point(1608, 0);
            this.labelIndex.Name = "labelIndex";
            this.labelIndex.Size = new System.Drawing.Size(63, 32);
            this.labelIndex.TabIndex = 2;
            this.labelIndex.Text = "1234";
            this.labelIndex.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.labelError);
            this.panel8.Controls.Add(this.label1);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel8.Location = new System.Drawing.Point(10, 0);
            this.panel8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(424, 32);
            this.panel8.TabIndex = 1;
            // 
            // labelError
            // 
            this.labelError.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelError.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelError.Location = new System.Drawing.Point(99, 0);
            this.labelError.Name = "labelError";
            this.labelError.Size = new System.Drawing.Size(325, 32);
            this.labelError.TabIndex = 1;
            this.labelError.Text = "....";
            this.labelError.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "Device";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // panel7
            // 
            this.panel7.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(10, 32);
            this.panel7.TabIndex = 0;
            // 
            // panel18
            // 
            this.panel18.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel18.Location = new System.Drawing.Point(1671, 0);
            this.panel18.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(10, 32);
            this.panel18.TabIndex = 4;
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 34);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1681, 2);
            this.panel3.TabIndex = 7;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.panel50);
            this.panel4.Controls.Add(this.panel46);
            this.panel4.Controls.Add(this.panel42);
            this.panel4.Controls.Add(this.panel41);
            this.panel4.Controls.Add(this.panel28);
            this.panel4.Controls.Add(this.panel22);
            this.panel4.Controls.Add(this.panel10);
            this.panel4.Controls.Add(this.panel52);
            this.panel4.Controls.Add(this.panel53);
            this.panel4.Controls.Add(this.panel11);
            this.panel4.Controls.Add(this.panel9);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 36);
            this.panel4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1681, 252);
            this.panel4.TabIndex = 8;
            // 
            // panel50
            // 
            this.panel50.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panel50.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel50.Location = new System.Drawing.Point(1294, 0);
            this.panel50.Name = "panel50";
            this.panel50.Size = new System.Drawing.Size(0, 252);
            this.panel50.TabIndex = 9;
            // 
            // panel46
            // 
            this.panel46.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panel46.Controls.Add(this.panel51);
            this.panel46.Controls.Add(this.buttonSetAssigned);
            this.panel46.Controls.Add(this.buttonSetDefective);
            this.panel46.Controls.Add(this.buttonSetInService);
            this.panel46.Controls.Add(this.buttonSetUnavailable);
            this.panel46.Controls.Add(this.buttonClearStudy);
            this.panel46.Controls.Add(this.panel49);
            this.panel46.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel46.Location = new System.Drawing.Point(1119, 0);
            this.panel46.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel46.Name = "panel46";
            this.panel46.Size = new System.Drawing.Size(175, 252);
            this.panel46.TabIndex = 8;
            // 
            // panel51
            // 
            this.panel51.Controls.Add(this.labelShowActiveState);
            this.panel51.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel51.Location = new System.Drawing.Point(0, 212);
            this.panel51.Name = "panel51";
            this.panel51.Size = new System.Drawing.Size(175, 40);
            this.panel51.TabIndex = 40;
            // 
            // labelShowActiveState
            // 
            this.labelShowActiveState.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelShowActiveState.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelShowActiveState.Location = new System.Drawing.Point(0, 0);
            this.labelShowActiveState.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelShowActiveState.Name = "labelShowActiveState";
            this.labelShowActiveState.Size = new System.Drawing.Size(175, 29);
            this.labelShowActiveState.TabIndex = 43;
            this.labelShowActiveState.Text = "^No License+Double";
            // 
            // buttonSetAssigned
            // 
            this.buttonSetAssigned.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonSetAssigned.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonSetAssigned.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonSetAssigned.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSetAssigned.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonSetAssigned.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonSetAssigned.Location = new System.Drawing.Point(0, 160);
            this.buttonSetAssigned.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonSetAssigned.Name = "buttonSetAssigned";
            this.buttonSetAssigned.Size = new System.Drawing.Size(175, 36);
            this.buttonSetAssigned.TabIndex = 27;
            this.buttonSetAssigned.Text = "Set Re-Assign";
            this.buttonSetAssigned.UseVisualStyleBackColor = false;
            this.buttonSetAssigned.Click += new System.EventHandler(this.buttonSetAssigned_Click);
            // 
            // buttonSetDefective
            // 
            this.buttonSetDefective.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonSetDefective.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonSetDefective.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonSetDefective.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSetDefective.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonSetDefective.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonSetDefective.Location = new System.Drawing.Point(0, 120);
            this.buttonSetDefective.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonSetDefective.Name = "buttonSetDefective";
            this.buttonSetDefective.Size = new System.Drawing.Size(175, 36);
            this.buttonSetDefective.TabIndex = 25;
            this.buttonSetDefective.Text = "Set Defective";
            this.buttonSetDefective.UseVisualStyleBackColor = false;
            this.buttonSetDefective.Click += new System.EventHandler(this.buttonSetDefective_Click);
            // 
            // buttonSetInService
            // 
            this.buttonSetInService.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonSetInService.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonSetInService.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonSetInService.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSetInService.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonSetInService.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonSetInService.Location = new System.Drawing.Point(0, 80);
            this.buttonSetInService.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonSetInService.Name = "buttonSetInService";
            this.buttonSetInService.Size = new System.Drawing.Size(175, 36);
            this.buttonSetInService.TabIndex = 21;
            this.buttonSetInService.Text = "Set In Service";
            this.buttonSetInService.UseVisualStyleBackColor = false;
            this.buttonSetInService.Click += new System.EventHandler(this.buttonSetInService_Click);
            // 
            // buttonSetUnavailable
            // 
            this.buttonSetUnavailable.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonSetUnavailable.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonSetUnavailable.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonSetUnavailable.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSetUnavailable.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonSetUnavailable.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonSetUnavailable.Location = new System.Drawing.Point(0, 40);
            this.buttonSetUnavailable.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonSetUnavailable.Name = "buttonSetUnavailable";
            this.buttonSetUnavailable.Size = new System.Drawing.Size(175, 36);
            this.buttonSetUnavailable.TabIndex = 23;
            this.buttonSetUnavailable.Text = "Set Unavailable";
            this.buttonSetUnavailable.UseVisualStyleBackColor = false;
            this.buttonSetUnavailable.Click += new System.EventHandler(this.buttonSetUnavailable_Click);
            // 
            // buttonClearStudy
            // 
            this.buttonClearStudy.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonClearStudy.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonClearStudy.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonClearStudy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonClearStudy.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonClearStudy.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonClearStudy.Location = new System.Drawing.Point(0, 0);
            this.buttonClearStudy.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonClearStudy.Name = "buttonClearStudy";
            this.buttonClearStudy.Size = new System.Drawing.Size(175, 36);
            this.buttonClearStudy.TabIndex = 28;
            this.buttonClearStudy.Text = "Unlink  Study";
            this.buttonClearStudy.UseVisualStyleBackColor = false;
            this.buttonClearStudy.Click += new System.EventHandler(this.buttonClearStudy_Click);
            // 
            // panel49
            // 
            this.panel49.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel49.Location = new System.Drawing.Point(0, 0);
            this.panel49.Name = "panel49";
            this.panel49.Size = new System.Drawing.Size(175, 4);
            this.panel49.TabIndex = 29;
            this.panel49.Click += new System.EventHandler(this.buttonClear_Click);
            this.panel49.Paint += new System.Windows.Forms.PaintEventHandler(this.panel49_Paint);
            // 
            // panel42
            // 
            this.panel42.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panel42.Controls.Add(this.labelErrorInstruct);
            this.panel42.Controls.Add(this.panel43);
            this.panel42.Controls.Add(this.panel31);
            this.panel42.Controls.Add(this.buttonSetAvailable);
            this.panel42.Controls.Add(this.buttonSetReceived);
            this.panel42.Controls.Add(this.buttonSetIntransit);
            this.panel42.Controls.Add(this.buttonEndUse);
            this.panel42.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel42.Location = new System.Drawing.Point(944, 0);
            this.panel42.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel42.Name = "panel42";
            this.panel42.Size = new System.Drawing.Size(175, 252);
            this.panel42.TabIndex = 7;
            // 
            // labelErrorInstruct
            // 
            this.labelErrorInstruct.AutoSize = true;
            this.labelErrorInstruct.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelErrorInstruct.Location = new System.Drawing.Point(0, 165);
            this.labelErrorInstruct.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelErrorInstruct.Name = "labelErrorInstruct";
            this.labelErrorInstruct.Size = new System.Drawing.Size(113, 34);
            this.labelErrorInstruct.TabIndex = 36;
            this.labelErrorInstruct.Text = "!= Double device\r\n-= Not in license\r\n";
            // 
            // panel43
            // 
            this.panel43.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel43.Location = new System.Drawing.Point(0, 199);
            this.panel43.Name = "panel43";
            this.panel43.Size = new System.Drawing.Size(175, 13);
            this.panel43.TabIndex = 18;
            // 
            // panel31
            // 
            this.panel31.Controls.Add(this.labelShowSnr);
            this.panel31.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel31.Location = new System.Drawing.Point(0, 212);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(175, 40);
            this.panel31.TabIndex = 39;
            // 
            // labelShowSnr
            // 
            this.labelShowSnr.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelShowSnr.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelShowSnr.Location = new System.Drawing.Point(0, 0);
            this.labelShowSnr.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelShowSnr.Name = "labelShowSnr";
            this.labelShowSnr.Size = new System.Drawing.Size(175, 29);
            this.labelShowSnr.TabIndex = 43;
            this.labelShowSnr.Text = "^H3R1234567";
            // 
            // buttonSetAvailable
            // 
            this.buttonSetAvailable.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonSetAvailable.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonSetAvailable.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonSetAvailable.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSetAvailable.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonSetAvailable.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonSetAvailable.Location = new System.Drawing.Point(0, 120);
            this.buttonSetAvailable.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonSetAvailable.Name = "buttonSetAvailable";
            this.buttonSetAvailable.Size = new System.Drawing.Size(175, 36);
            this.buttonSetAvailable.TabIndex = 23;
            this.buttonSetAvailable.Text = "Set Available";
            this.buttonSetAvailable.UseVisualStyleBackColor = false;
            this.buttonSetAvailable.Click += new System.EventHandler(this.buttonSetAvailable_Click);
            // 
            // buttonSetReceived
            // 
            this.buttonSetReceived.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonSetReceived.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonSetReceived.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonSetReceived.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSetReceived.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonSetReceived.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonSetReceived.Location = new System.Drawing.Point(0, 80);
            this.buttonSetReceived.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonSetReceived.Name = "buttonSetReceived";
            this.buttonSetReceived.Size = new System.Drawing.Size(175, 36);
            this.buttonSetReceived.TabIndex = 21;
            this.buttonSetReceived.Text = "Set Returned";
            this.buttonSetReceived.UseVisualStyleBackColor = false;
            this.buttonSetReceived.Click += new System.EventHandler(this.buttonSetReceived_Click);
            // 
            // buttonSetIntransit
            // 
            this.buttonSetIntransit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonSetIntransit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonSetIntransit.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonSetIntransit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSetIntransit.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonSetIntransit.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonSetIntransit.Location = new System.Drawing.Point(0, 40);
            this.buttonSetIntransit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonSetIntransit.Name = "buttonSetIntransit";
            this.buttonSetIntransit.Size = new System.Drawing.Size(175, 36);
            this.buttonSetIntransit.TabIndex = 25;
            this.buttonSetIntransit.Text = "Set In Transit";
            this.buttonSetIntransit.UseVisualStyleBackColor = false;
            this.buttonSetIntransit.Click += new System.EventHandler(this.buttonSetIntransit_Click);
            // 
            // buttonEndUse
            // 
            this.buttonEndUse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonEndUse.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonEndUse.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonEndUse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEndUse.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonEndUse.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonEndUse.Location = new System.Drawing.Point(0, 0);
            this.buttonEndUse.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonEndUse.Name = "buttonEndUse";
            this.buttonEndUse.Size = new System.Drawing.Size(175, 36);
            this.buttonEndUse.TabIndex = 17;
            this.buttonEndUse.Text = "End  Recording";
            this.buttonEndUse.UseVisualStyleBackColor = false;
            this.buttonEndUse.Click += new System.EventHandler(this.buttonEndUse_Click);
            // 
            // panel41
            // 
            this.panel41.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel41.Location = new System.Drawing.Point(934, 0);
            this.panel41.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel41.Name = "panel41";
            this.panel41.Size = new System.Drawing.Size(10, 252);
            this.panel41.TabIndex = 6;
            // 
            // panel28
            // 
            this.panel28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panel28.Controls.Add(this.label27);
            this.panel28.Controls.Add(this.labelNoMct);
            this.panel28.Controls.Add(this.labelDeviceChanged);
            this.panel28.Controls.Add(this.label26);
            this.panel28.Controls.Add(this.labelTzStoreName);
            this.panel28.Controls.Add(this.labelState);
            this.panel28.Controls.Add(this.labelEndDate);
            this.panel28.Controls.Add(this.labelStartDate);
            this.panel28.Controls.Add(this.labelRefName);
            this.panel28.Controls.Add(this.labelRefID);
            this.panel28.Controls.Add(this.labelStudyNr);
            this.panel28.Controls.Add(this.label14);
            this.panel28.Controls.Add(this.labelTextEndDate);
            this.panel28.Controls.Add(this.label7);
            this.panel28.Controls.Add(this.label8);
            this.panel28.Controls.Add(this.labelTextRefID);
            this.panel28.Controls.Add(this.label10);
            this.panel28.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel28.Font = new System.Drawing.Font("Arial", 10F);
            this.panel28.Location = new System.Drawing.Point(688, 0);
            this.panel28.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(246, 252);
            this.panel28.TabIndex = 5;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(3, 149);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(77, 16);
            this.label27.TabIndex = 110;
            this.label27.Text = "Add Study:";
            // 
            // labelNoMct
            // 
            this.labelNoMct.AutoSize = true;
            this.labelNoMct.Location = new System.Drawing.Point(80, 149);
            this.labelNoMct.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelNoMct.Name = "labelNoMct";
            this.labelNoMct.Size = new System.Drawing.Size(60, 16);
            this.labelNoMct.TabIndex = 48;
            this.labelNoMct.Text = "^NoMCT";
            // 
            // labelDeviceChanged
            // 
            this.labelDeviceChanged.AutoSize = true;
            this.labelDeviceChanged.Font = new System.Drawing.Font("Arial", 8F);
            this.labelDeviceChanged.Location = new System.Drawing.Point(55, 225);
            this.labelDeviceChanged.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelDeviceChanged.Name = "labelDeviceChanged";
            this.labelDeviceChanged.Size = new System.Drawing.Size(189, 14);
            this.labelDeviceChanged.TabIndex = 109;
            this.labelDeviceChanged.Text = "99/99/9999 99:99:99  AM+1200 initials";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Arial", 8F);
            this.label26.Location = new System.Drawing.Point(3, 225);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(53, 14);
            this.label26.TabIndex = 108;
            this.label26.Text = "Changed:";
            // 
            // labelTzStoreName
            // 
            this.labelTzStoreName.AutoSize = true;
            this.labelTzStoreName.Location = new System.Drawing.Point(80, 169);
            this.labelTzStoreName.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelTzStoreName.Name = "labelTzStoreName";
            this.labelTzStoreName.Size = new System.Drawing.Size(98, 16);
            this.labelTzStoreName.TabIndex = 42;
            this.labelTzStoreName.Text = "Tz store name";
            // 
            // labelState
            // 
            this.labelState.Font = new System.Drawing.Font("Arial", 15.75F);
            this.labelState.Location = new System.Drawing.Point(81, 188);
            this.labelState.Margin = new System.Windows.Forms.Padding(5);
            this.labelState.Name = "labelState";
            this.labelState.ReadOnly = true;
            this.labelState.Size = new System.Drawing.Size(157, 32);
            this.labelState.TabIndex = 41;
            this.labelState.Text = "state";
            // 
            // labelEndDate
            // 
            this.labelEndDate.Location = new System.Drawing.Point(81, 117);
            this.labelEndDate.Margin = new System.Windows.Forms.Padding(5);
            this.labelEndDate.Name = "labelEndDate";
            this.labelEndDate.ReadOnly = true;
            this.labelEndDate.Size = new System.Drawing.Size(157, 23);
            this.labelEndDate.TabIndex = 40;
            // 
            // labelStartDate
            // 
            this.labelStartDate.Location = new System.Drawing.Point(81, 88);
            this.labelStartDate.Margin = new System.Windows.Forms.Padding(5);
            this.labelStartDate.Name = "labelStartDate";
            this.labelStartDate.ReadOnly = true;
            this.labelStartDate.Size = new System.Drawing.Size(157, 23);
            this.labelStartDate.TabIndex = 39;
            // 
            // labelRefName
            // 
            this.labelRefName.Location = new System.Drawing.Point(81, 61);
            this.labelRefName.Margin = new System.Windows.Forms.Padding(5);
            this.labelRefName.Name = "labelRefName";
            this.labelRefName.ReadOnly = true;
            this.labelRefName.Size = new System.Drawing.Size(157, 23);
            this.labelRefName.TabIndex = 38;
            // 
            // labelRefID
            // 
            this.labelRefID.Location = new System.Drawing.Point(81, 33);
            this.labelRefID.Margin = new System.Windows.Forms.Padding(5);
            this.labelRefID.Name = "labelRefID";
            this.labelRefID.ReadOnly = true;
            this.labelRefID.Size = new System.Drawing.Size(157, 23);
            this.labelRefID.TabIndex = 38;
            this.labelRefID.DoubleClick += new System.EventHandler(this.labelRefID_DoubleClick);
            // 
            // labelStudyNr
            // 
            this.labelStudyNr.Location = new System.Drawing.Point(81, 7);
            this.labelStudyNr.Margin = new System.Windows.Forms.Padding(5);
            this.labelStudyNr.Name = "labelStudyNr";
            this.labelStudyNr.ReadOnly = true;
            this.labelStudyNr.Size = new System.Drawing.Size(157, 23);
            this.labelStudyNr.TabIndex = 37;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(3, 198);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(45, 16);
            this.label14.TabIndex = 35;
            this.label14.Text = "State:";
            // 
            // labelTextEndDate
            // 
            this.labelTextEndDate.AutoSize = true;
            this.labelTextEndDate.Location = new System.Drawing.Point(3, 123);
            this.labelTextEndDate.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelTextEndDate.Name = "labelTextEndDate";
            this.labelTextEndDate.Size = new System.Drawing.Size(69, 16);
            this.labelTextEndDate.TabIndex = 28;
            this.labelTextEndDate.Text = "End date:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 7);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(66, 16);
            this.label7.TabIndex = 25;
            this.label7.Text = "Study Nr:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 94);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(74, 16);
            this.label8.TabIndex = 23;
            this.label8.Text = "Start date:";
            // 
            // labelTextRefID
            // 
            this.labelTextRefID.AutoSize = true;
            this.labelTextRefID.Location = new System.Drawing.Point(3, 36);
            this.labelTextRefID.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelTextRefID.Name = "labelTextRefID";
            this.labelTextRefID.Size = new System.Drawing.Size(55, 16);
            this.labelTextRefID.TabIndex = 21;
            this.labelTextRefID.Text = "Ref. ID:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 64);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(78, 16);
            this.label10.TabIndex = 19;
            this.label10.Text = "Ref. Name:";
            // 
            // panel22
            // 
            this.panel22.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel22.Location = new System.Drawing.Point(678, 0);
            this.panel22.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(10, 252);
            this.panel22.TabIndex = 4;
            // 
            // panel10
            // 
            this.panel10.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel10.Location = new System.Drawing.Point(1679, 0);
            this.panel10.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(2, 252);
            this.panel10.TabIndex = 2;
            // 
            // panel52
            // 
            this.panel52.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panel52.Controls.Add(this.panel54);
            this.panel52.Controls.Add(this.label25);
            this.panel52.Controls.Add(this.textBoxTzOffset);
            this.panel52.Controls.Add(this.label23);
            this.panel52.Controls.Add(this.comboBoxTimeZone);
            this.panel52.Controls.Add(this.comboBoxAddStudy);
            this.panel52.Controls.Add(this.label19);
            this.panel52.Controls.Add(this.label18);
            this.panel52.Controls.Add(this.comboBoxNextState);
            this.panel52.Controls.Add(this.label16);
            this.panel52.Controls.Add(this.textBoxTzDefault);
            this.panel52.Controls.Add(this.comboBoxRefID);
            this.panel52.Controls.Add(this.label20);
            this.panel52.Controls.Add(this.label21);
            this.panel52.Controls.Add(this.textBoxServiceNote);
            this.panel52.Controls.Add(this.label22);
            this.panel52.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel52.Font = new System.Drawing.Font("Arial", 10F);
            this.panel52.Location = new System.Drawing.Point(353, 0);
            this.panel52.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel52.Name = "panel52";
            this.panel52.Size = new System.Drawing.Size(325, 252);
            this.panel52.TabIndex = 10;
            // 
            // panel54
            // 
            this.panel54.Controls.Add(this.checkBoxCh10);
            this.panel54.Controls.Add(this.checkBoxCh9);
            this.panel54.Controls.Add(this.checkBoxCh8);
            this.panel54.Controls.Add(this.checkBoxCh7);
            this.panel54.Controls.Add(this.checkBoxCh6);
            this.panel54.Controls.Add(this.checkBoxCh5);
            this.panel54.Controls.Add(this.checkBoxCh4);
            this.panel54.Controls.Add(this.checkBoxCh3);
            this.panel54.Controls.Add(this.checkBoxCh2);
            this.panel54.Controls.Add(this.checkBoxCh1);
            this.panel54.Controls.Add(this.panel55);
            this.panel54.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel54.Location = new System.Drawing.Point(0, 223);
            this.panel54.Name = "panel54";
            this.panel54.Size = new System.Drawing.Size(325, 29);
            this.panel54.TabIndex = 48;
            // 
            // checkBoxCh10
            // 
            this.checkBoxCh10.AutoSize = true;
            this.checkBoxCh10.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.checkBoxCh10.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxCh10.Font = new System.Drawing.Font("Arial", 8F);
            this.checkBoxCh10.Location = new System.Drawing.Point(272, 0);
            this.checkBoxCh10.Name = "checkBoxCh10";
            this.checkBoxCh10.Size = new System.Drawing.Size(23, 29);
            this.checkBoxCh10.TabIndex = 62;
            this.checkBoxCh10.Text = "10";
            this.checkBoxCh10.UseVisualStyleBackColor = true;
            // 
            // checkBoxCh9
            // 
            this.checkBoxCh9.AutoSize = true;
            this.checkBoxCh9.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.checkBoxCh9.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxCh9.Font = new System.Drawing.Font("Arial", 8F);
            this.checkBoxCh9.Location = new System.Drawing.Point(255, 0);
            this.checkBoxCh9.Name = "checkBoxCh9";
            this.checkBoxCh9.Size = new System.Drawing.Size(17, 29);
            this.checkBoxCh9.TabIndex = 61;
            this.checkBoxCh9.Text = "9";
            this.checkBoxCh9.UseVisualStyleBackColor = true;
            // 
            // checkBoxCh8
            // 
            this.checkBoxCh8.AutoSize = true;
            this.checkBoxCh8.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.checkBoxCh8.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxCh8.Font = new System.Drawing.Font("Arial", 8F);
            this.checkBoxCh8.Location = new System.Drawing.Point(238, 0);
            this.checkBoxCh8.Name = "checkBoxCh8";
            this.checkBoxCh8.Size = new System.Drawing.Size(17, 29);
            this.checkBoxCh8.TabIndex = 60;
            this.checkBoxCh8.Text = "8";
            this.checkBoxCh8.UseVisualStyleBackColor = true;
            // 
            // checkBoxCh7
            // 
            this.checkBoxCh7.AutoSize = true;
            this.checkBoxCh7.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.checkBoxCh7.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxCh7.Font = new System.Drawing.Font("Arial", 8F);
            this.checkBoxCh7.Location = new System.Drawing.Point(221, 0);
            this.checkBoxCh7.Name = "checkBoxCh7";
            this.checkBoxCh7.Size = new System.Drawing.Size(17, 29);
            this.checkBoxCh7.TabIndex = 59;
            this.checkBoxCh7.Text = "7";
            this.checkBoxCh7.UseVisualStyleBackColor = true;
            // 
            // checkBoxCh6
            // 
            this.checkBoxCh6.AutoSize = true;
            this.checkBoxCh6.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.checkBoxCh6.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxCh6.Font = new System.Drawing.Font("Arial", 8F);
            this.checkBoxCh6.Location = new System.Drawing.Point(204, 0);
            this.checkBoxCh6.Name = "checkBoxCh6";
            this.checkBoxCh6.Size = new System.Drawing.Size(17, 29);
            this.checkBoxCh6.TabIndex = 58;
            this.checkBoxCh6.Text = "6";
            this.checkBoxCh6.UseVisualStyleBackColor = true;
            // 
            // checkBoxCh5
            // 
            this.checkBoxCh5.AutoSize = true;
            this.checkBoxCh5.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.checkBoxCh5.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxCh5.Font = new System.Drawing.Font("Arial", 8F);
            this.checkBoxCh5.Location = new System.Drawing.Point(187, 0);
            this.checkBoxCh5.Name = "checkBoxCh5";
            this.checkBoxCh5.Size = new System.Drawing.Size(17, 29);
            this.checkBoxCh5.TabIndex = 57;
            this.checkBoxCh5.Text = "5";
            this.checkBoxCh5.UseVisualStyleBackColor = true;
            // 
            // checkBoxCh4
            // 
            this.checkBoxCh4.AutoSize = true;
            this.checkBoxCh4.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.checkBoxCh4.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxCh4.Font = new System.Drawing.Font("Arial", 8F);
            this.checkBoxCh4.Location = new System.Drawing.Point(170, 0);
            this.checkBoxCh4.Name = "checkBoxCh4";
            this.checkBoxCh4.Size = new System.Drawing.Size(17, 29);
            this.checkBoxCh4.TabIndex = 56;
            this.checkBoxCh4.Text = "4";
            this.checkBoxCh4.UseVisualStyleBackColor = true;
            // 
            // checkBoxCh3
            // 
            this.checkBoxCh3.AutoSize = true;
            this.checkBoxCh3.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.checkBoxCh3.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxCh3.Font = new System.Drawing.Font("Arial", 8F);
            this.checkBoxCh3.Location = new System.Drawing.Point(153, 0);
            this.checkBoxCh3.Name = "checkBoxCh3";
            this.checkBoxCh3.Size = new System.Drawing.Size(17, 29);
            this.checkBoxCh3.TabIndex = 55;
            this.checkBoxCh3.Text = "3";
            this.checkBoxCh3.UseVisualStyleBackColor = true;
            // 
            // checkBoxCh2
            // 
            this.checkBoxCh2.AutoSize = true;
            this.checkBoxCh2.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.checkBoxCh2.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxCh2.Font = new System.Drawing.Font("Arial", 8F);
            this.checkBoxCh2.Location = new System.Drawing.Point(136, 0);
            this.checkBoxCh2.Name = "checkBoxCh2";
            this.checkBoxCh2.Size = new System.Drawing.Size(17, 29);
            this.checkBoxCh2.TabIndex = 54;
            this.checkBoxCh2.Text = "2";
            this.checkBoxCh2.UseVisualStyleBackColor = true;
            // 
            // checkBoxCh1
            // 
            this.checkBoxCh1.AutoSize = true;
            this.checkBoxCh1.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.checkBoxCh1.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxCh1.Font = new System.Drawing.Font("Arial", 8F);
            this.checkBoxCh1.Location = new System.Drawing.Point(119, 0);
            this.checkBoxCh1.Margin = new System.Windows.Forms.Padding(0);
            this.checkBoxCh1.Name = "checkBoxCh1";
            this.checkBoxCh1.Size = new System.Drawing.Size(17, 29);
            this.checkBoxCh1.TabIndex = 53;
            this.checkBoxCh1.Text = "1";
            this.checkBoxCh1.UseVisualStyleBackColor = true;
            // 
            // panel55
            // 
            this.panel55.Controls.Add(this.label28);
            this.panel55.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel55.Location = new System.Drawing.Point(0, 0);
            this.panel55.Name = "panel55";
            this.panel55.Size = new System.Drawing.Size(119, 29);
            this.panel55.TabIndex = 64;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(3, 4);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(104, 16);
            this.label28.TabIndex = 64;
            this.label28.Text = "Invert Channel:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(51, 81);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(283, 16);
            this.label25.TabIndex = 47;
            this.label25.Text = "¹ = Use with DVX+DV2, (Can shift time UTC)";
            // 
            // textBoxTzOffset
            // 
            this.textBoxTzOffset.Location = new System.Drawing.Point(255, 98);
            this.textBoxTzOffset.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxTzOffset.Name = "textBoxTzOffset";
            this.textBoxTzOffset.Size = new System.Drawing.Size(61, 23);
            this.textBoxTzOffset.TabIndex = 44;
            this.textBoxTzOffset.Text = "^+12:59";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label23.Location = new System.Drawing.Point(202, 102);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(50, 16);
            this.label23.TabIndex = 43;
            this.label23.Text = "Offset:";
            // 
            // comboBoxTimeZone
            // 
            this.comboBoxTimeZone.FormattingEnabled = true;
            this.comboBoxTimeZone.Items.AddRange(new object[] {
            "Dyna-Vision",
            "Sirona B",
            "AeraCT"});
            this.comboBoxTimeZone.Location = new System.Drawing.Point(91, 56);
            this.comboBoxTimeZone.Name = "comboBoxTimeZone";
            this.comboBoxTimeZone.Size = new System.Drawing.Size(226, 24);
            this.comboBoxTimeZone.TabIndex = 42;
            // 
            // comboBoxAddStudy
            // 
            this.comboBoxAddStudy.FormattingEnabled = true;
            this.comboBoxAddStudy.Items.AddRange(new object[] {
            "????"});
            this.comboBoxAddStudy.Location = new System.Drawing.Point(92, 29);
            this.comboBoxAddStudy.Name = "comboBoxAddStudy";
            this.comboBoxAddStudy.Size = new System.Drawing.Size(226, 24);
            this.comboBoxAddStudy.TabIndex = 41;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(2, 32);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(77, 16);
            this.label19.TabIndex = 40;
            this.label19.Text = "Add Study:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(2, 10);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(81, 16);
            this.label18.TabIndex = 39;
            this.label18.Text = "Def. Ref ID:";
            // 
            // comboBoxNextState
            // 
            this.comboBoxNextState.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.comboBoxNextState.FormattingEnabled = true;
            this.comboBoxNextState.Items.AddRange(new object[] {
            "Dyna-Vision",
            "Sirona B",
            "AeraCT"});
            this.comboBoxNextState.Location = new System.Drawing.Point(90, 199);
            this.comboBoxNextState.Name = "comboBoxNextState";
            this.comboBoxNextState.Size = new System.Drawing.Size(227, 24);
            this.comboBoxNextState.TabIndex = 36;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(3, 204);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(76, 16);
            this.label16.TabIndex = 35;
            this.label16.Text = "Next State:";
            // 
            // textBoxTzDefault
            // 
            this.textBoxTzDefault.Location = new System.Drawing.Point(136, 99);
            this.textBoxTzDefault.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxTzDefault.Name = "textBoxTzDefault";
            this.textBoxTzDefault.Size = new System.Drawing.Size(61, 23);
            this.textBoxTzDefault.TabIndex = 32;
            this.textBoxTzDefault.Text = "^+12:59";
            // 
            // comboBoxRefID
            // 
            this.comboBoxRefID.FormattingEnabled = true;
            this.comboBoxRefID.Items.AddRange(new object[] {
            "??????"});
            this.comboBoxRefID.Location = new System.Drawing.Point(92, 3);
            this.comboBoxRefID.Name = "comboBoxRefID";
            this.comboBoxRefID.Size = new System.Drawing.Size(226, 24);
            this.comboBoxRefID.TabIndex = 28;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(69, 103);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(57, 16);
            this.label20.TabIndex = 23;
            this.label20.Text = "Default:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(2, 62);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(83, 16);
            this.label21.TabIndex = 21;
            this.label21.Text = "Time Zone¹:";
            // 
            // textBoxServiceNote
            // 
            this.textBoxServiceNote.Location = new System.Drawing.Point(54, 124);
            this.textBoxServiceNote.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxServiceNote.MaxLength = 30000;
            this.textBoxServiceNote.Multiline = true;
            this.textBoxServiceNote.Name = "textBoxServiceNote";
            this.textBoxServiceNote.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxServiceNote.Size = new System.Drawing.Size(263, 72);
            this.textBoxServiceNote.TabIndex = 20;
            this.textBoxServiceNote.Text = "^service1\r\n2\r\n3\r\n4\r\n5\r\n6";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(2, 125);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(59, 32);
            this.label22.TabIndex = 19;
            this.label22.Text = "Service \r\nNote:";
            // 
            // panel53
            // 
            this.panel53.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel53.Location = new System.Drawing.Point(343, 0);
            this.panel53.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel53.Name = "panel53";
            this.panel53.Size = new System.Drawing.Size(10, 252);
            this.panel53.TabIndex = 11;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panel11.Controls.Add(this.labelNextServiceOff);
            this.panel11.Controls.Add(this.textBoxIReaderNR);
            this.panel11.Controls.Add(this.label15);
            this.panel11.Controls.Add(this.label24);
            this.panel11.Controls.Add(this.label17);
            this.panel11.Controls.Add(this.textBoxDeployRemark);
            this.panel11.Controls.Add(this.label6);
            this.panel11.Controls.Add(this.comboBoxUseClient);
            this.panel11.Controls.Add(this.label13);
            this.panel11.Controls.Add(this.textBoxRemLabel);
            this.panel11.Controls.Add(this.label12);
            this.panel11.Controls.Add(this.textBoxSerialNr);
            this.panel11.Controls.Add(this.comboBoxModel);
            this.panel11.Controls.Add(this.label2);
            this.panel11.Controls.Add(this.labelManufacturer);
            this.panel11.Controls.Add(this.labelNextService);
            this.panel11.Controls.Add(this.dateTimePickerFirstInService);
            this.panel11.Controls.Add(this.labelFirstInService);
            this.panel11.Controls.Add(this.dateTimePickerNextService);
            this.panel11.Controls.Add(this.textBoxDeviceFirmwareVersion);
            this.panel11.Controls.Add(this.labelDeviceFirmwareVersion);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel11.Font = new System.Drawing.Font("Arial", 10F);
            this.panel11.Location = new System.Drawing.Point(2, 0);
            this.panel11.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(341, 252);
            this.panel11.TabIndex = 3;
            // 
            // labelNextServiceOff
            // 
            this.labelNextServiceOff.AutoSize = true;
            this.labelNextServiceOff.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNextServiceOff.ForeColor = System.Drawing.SystemColors.Highlight;
            this.labelNextServiceOff.Location = new System.Drawing.Point(86, 110);
            this.labelNextServiceOff.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelNextServiceOff.Name = "labelNextServiceOff";
            this.labelNextServiceOff.Size = new System.Drawing.Size(21, 14);
            this.labelNextServiceOff.TabIndex = 49;
            this.labelNextServiceOff.Text = "off";
            this.labelNextServiceOff.Click += new System.EventHandler(this.labelNextServiceOff_Click);
            // 
            // textBoxIReaderNR
            // 
            this.textBoxIReaderNR.Location = new System.Drawing.Point(109, 211);
            this.textBoxIReaderNR.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxIReaderNR.Name = "textBoxIReaderNR";
            this.textBoxIReaderNR.Size = new System.Drawing.Size(37, 23);
            this.textBoxIReaderNR.TabIndex = 48;
            this.textBoxIReaderNR.Text = "^0";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(155, 214);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(169, 16);
            this.label15.TabIndex = 46;
            this.label15.Text = "² = still to be implemented";
            this.label15.Visible = false;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(3, 214);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(83, 16);
            this.label24.TabIndex = 47;
            this.label24.Text = "IReader[nr]:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(88, 155);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(19, 18);
            this.label17.TabIndex = 39;
            this.label17.Text = "└";
            this.label17.Click += new System.EventHandler(this.label17_Click);
            // 
            // textBoxDeployRemark
            // 
            this.textBoxDeployRemark.Location = new System.Drawing.Point(108, 183);
            this.textBoxDeployRemark.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxDeployRemark.MaxLength = 254;
            this.textBoxDeployRemark.Name = "textBoxDeployRemark";
            this.textBoxDeployRemark.Size = new System.Drawing.Size(226, 23);
            this.textBoxDeployRemark.TabIndex = 38;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 186);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(104, 16);
            this.label6.TabIndex = 37;
            this.label6.Text = "Deploy remark:";
            // 
            // comboBoxUseClient
            // 
            this.comboBoxUseClient.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.comboBoxUseClient.FormattingEnabled = true;
            this.comboBoxUseClient.Items.AddRange(new object[] {
            "Dyna-Vision",
            "Sirona B",
            "AeraCT"});
            this.comboBoxUseClient.Location = new System.Drawing.Point(108, 131);
            this.comboBoxUseClient.Name = "comboBoxUseClient";
            this.comboBoxUseClient.Size = new System.Drawing.Size(227, 24);
            this.comboBoxUseClient.TabIndex = 36;
            this.comboBoxUseClient.TextChanged += new System.EventHandler(this.comboBoxUseClient_SelectionChangeCommitted);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(3, 137);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(97, 16);
            this.label13.TabIndex = 35;
            this.label13.Text = "Use for Client:";
            // 
            // textBoxRemLabel
            // 
            this.textBoxRemLabel.Location = new System.Drawing.Point(108, 158);
            this.textBoxRemLabel.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxRemLabel.MaxLength = 46;
            this.textBoxRemLabel.Name = "textBoxRemLabel";
            this.textBoxRemLabel.Size = new System.Drawing.Size(227, 23);
            this.textBoxRemLabel.TabIndex = 34;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 161);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(91, 16);
            this.label12.TabIndex = 33;
            this.label12.Text = "Note (Client):";
            // 
            // textBoxSerialNr
            // 
            this.textBoxSerialNr.Location = new System.Drawing.Point(108, 29);
            this.textBoxSerialNr.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxSerialNr.MaxLength = 30;
            this.textBoxSerialNr.Name = "textBoxSerialNr";
            this.textBoxSerialNr.Size = new System.Drawing.Size(227, 23);
            this.textBoxSerialNr.TabIndex = 32;
            // 
            // comboBoxModel
            // 
            this.comboBoxModel.FormattingEnabled = true;
            this.comboBoxModel.Items.AddRange(new object[] {
            "Dyna-Vision",
            "Sirona B",
            "AeraCT"});
            this.comboBoxModel.Location = new System.Drawing.Point(108, 3);
            this.comboBoxModel.Name = "comboBoxModel";
            this.comboBoxModel.Size = new System.Drawing.Size(226, 24);
            this.comboBoxModel.TabIndex = 28;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.label2.Location = new System.Drawing.Point(3, 7);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 16);
            this.label2.TabIndex = 27;
            this.label2.Text = "Model: *";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // labelManufacturer
            // 
            this.labelManufacturer.AutoSize = true;
            this.labelManufacturer.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelManufacturer.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.labelManufacturer.Location = new System.Drawing.Point(3, 32);
            this.labelManufacturer.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelManufacturer.Name = "labelManufacturer";
            this.labelManufacturer.Size = new System.Drawing.Size(77, 16);
            this.labelManufacturer.TabIndex = 25;
            this.labelManufacturer.Text = "Serial Nr: *";
            // 
            // labelNextService
            // 
            this.labelNextService.AutoSize = true;
            this.labelNextService.Location = new System.Drawing.Point(3, 110);
            this.labelNextService.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelNextService.Name = "labelNextService";
            this.labelNextService.Size = new System.Drawing.Size(88, 16);
            this.labelNextService.TabIndex = 23;
            this.labelNextService.Text = "Next service:";
            // 
            // dateTimePickerFirstInService
            // 
            this.dateTimePickerFirstInService.Location = new System.Drawing.Point(108, 79);
            this.dateTimePickerFirstInService.Margin = new System.Windows.Forms.Padding(5);
            this.dateTimePickerFirstInService.Name = "dateTimePickerFirstInService";
            this.dateTimePickerFirstInService.Size = new System.Drawing.Size(227, 23);
            this.dateTimePickerFirstInService.TabIndex = 22;
            // 
            // labelFirstInService
            // 
            this.labelFirstInService.AutoSize = true;
            this.labelFirstInService.Location = new System.Drawing.Point(3, 84);
            this.labelFirstInService.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelFirstInService.Name = "labelFirstInService";
            this.labelFirstInService.Size = new System.Drawing.Size(104, 16);
            this.labelFirstInService.TabIndex = 21;
            this.labelFirstInService.Text = "First in service:";
            // 
            // dateTimePickerNextService
            // 
            this.dateTimePickerNextService.Location = new System.Drawing.Point(108, 105);
            this.dateTimePickerNextService.Margin = new System.Windows.Forms.Padding(5);
            this.dateTimePickerNextService.Name = "dateTimePickerNextService";
            this.dateTimePickerNextService.Size = new System.Drawing.Size(227, 23);
            this.dateTimePickerNextService.TabIndex = 24;
            // 
            // textBoxDeviceFirmwareVersion
            // 
            this.textBoxDeviceFirmwareVersion.Location = new System.Drawing.Point(107, 53);
            this.textBoxDeviceFirmwareVersion.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxDeviceFirmwareVersion.MaxLength = 16;
            this.textBoxDeviceFirmwareVersion.Name = "textBoxDeviceFirmwareVersion";
            this.textBoxDeviceFirmwareVersion.Size = new System.Drawing.Size(227, 23);
            this.textBoxDeviceFirmwareVersion.TabIndex = 20;
            // 
            // labelDeviceFirmwareVersion
            // 
            this.labelDeviceFirmwareVersion.AutoSize = true;
            this.labelDeviceFirmwareVersion.Location = new System.Drawing.Point(3, 57);
            this.labelDeviceFirmwareVersion.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelDeviceFirmwareVersion.Name = "labelDeviceFirmwareVersion";
            this.labelDeviceFirmwareVersion.Size = new System.Drawing.Size(85, 16);
            this.labelDeviceFirmwareVersion.TabIndex = 19;
            this.labelDeviceFirmwareVersion.Text = "FW Version:";
            // 
            // panel9
            // 
            this.panel9.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(2, 252);
            this.panel9.TabIndex = 1;
            // 
            // panel6
            // 
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 288);
            this.panel6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1681, 2);
            this.panel6.TabIndex = 9;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.panel12.Controls.Add(this.panel17);
            this.panel12.Controls.Add(this.buttonSirConfig);
            this.panel12.Controls.Add(this.buttonTzConfig);
            this.panel12.Controls.Add(this.buttonShowStudy);
            this.panel12.Controls.Add(this.buttonAddUpdate);
            this.panel12.Controls.Add(this.panel40);
            this.panel12.Controls.Add(this.buttonSearch);
            this.panel12.Controls.Add(this.panel39);
            this.panel12.Controls.Add(this.buttonDup);
            this.panel12.Controls.Add(this.panel23);
            this.panel12.Controls.Add(this.buttonClear);
            this.panel12.Controls.Add(this.panel16);
            this.panel12.Controls.Add(this.panel13);
            this.panel12.Controls.Add(this.panel14);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel12.Location = new System.Drawing.Point(0, 290);
            this.panel12.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(1681, 45);
            this.panel12.TabIndex = 10;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.buttonChangeSNR);
            this.panel17.Controls.Add(this.panel1);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel17.Location = new System.Drawing.Point(1131, 0);
            this.panel17.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(142, 45);
            this.panel17.TabIndex = 22;
            // 
            // buttonChangeSNR
            // 
            this.buttonChangeSNR.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonChangeSNR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonChangeSNR.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonChangeSNR.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonChangeSNR.FlatAppearance.BorderSize = 0;
            this.buttonChangeSNR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonChangeSNR.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonChangeSNR.ForeColor = System.Drawing.Color.White;
            this.buttonChangeSNR.Location = new System.Drawing.Point(26, 0);
            this.buttonChangeSNR.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonChangeSNR.Name = "buttonChangeSNR";
            this.buttonChangeSNR.Size = new System.Drawing.Size(116, 45);
            this.buttonChangeSNR.TabIndex = 23;
            this.buttonChangeSNR.Text = "Change SNR";
            this.buttonChangeSNR.UseVisualStyleBackColor = false;
            this.buttonChangeSNR.Click += new System.EventHandler(this.buttonChangeSNR_Click);
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(20, 45);
            this.panel1.TabIndex = 22;
            // 
            // buttonSirConfig
            // 
            this.buttonSirConfig.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonSirConfig.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonSirConfig.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonSirConfig.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonSirConfig.FlatAppearance.BorderSize = 0;
            this.buttonSirConfig.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSirConfig.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonSirConfig.ForeColor = System.Drawing.Color.White;
            this.buttonSirConfig.Location = new System.Drawing.Point(1273, 0);
            this.buttonSirConfig.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonSirConfig.Name = "buttonSirConfig";
            this.buttonSirConfig.Size = new System.Drawing.Size(123, 45);
            this.buttonSirConfig.TabIndex = 24;
            this.buttonSirConfig.Text = "Sirona Config";
            this.buttonSirConfig.UseVisualStyleBackColor = false;
            this.buttonSirConfig.Click += new System.EventHandler(this.buttonSirConfig_Click);
            // 
            // buttonTzConfig
            // 
            this.buttonTzConfig.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonTzConfig.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonTzConfig.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonTzConfig.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonTzConfig.FlatAppearance.BorderSize = 0;
            this.buttonTzConfig.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTzConfig.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonTzConfig.ForeColor = System.Drawing.Color.White;
            this.buttonTzConfig.Location = new System.Drawing.Point(1396, 0);
            this.buttonTzConfig.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonTzConfig.Name = "buttonTzConfig";
            this.buttonTzConfig.Size = new System.Drawing.Size(98, 45);
            this.buttonTzConfig.TabIndex = 23;
            this.buttonTzConfig.Text = "TZ Config";
            this.buttonTzConfig.UseVisualStyleBackColor = false;
            this.buttonTzConfig.Click += new System.EventHandler(this.buttonTzConfig_Click);
            // 
            // buttonShowStudy
            // 
            this.buttonShowStudy.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonShowStudy.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonShowStudy.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonShowStudy.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonShowStudy.FlatAppearance.BorderSize = 0;
            this.buttonShowStudy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonShowStudy.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonShowStudy.ForeColor = System.Drawing.Color.White;
            this.buttonShowStudy.Location = new System.Drawing.Point(1494, 0);
            this.buttonShowStudy.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonShowStudy.Name = "buttonShowStudy";
            this.buttonShowStudy.Size = new System.Drawing.Size(100, 45);
            this.buttonShowStudy.TabIndex = 18;
            this.buttonShowStudy.Text = "Edit Study";
            this.buttonShowStudy.UseVisualStyleBackColor = false;
            this.buttonShowStudy.Click += new System.EventHandler(this.buttonShowStudy_Click);
            // 
            // buttonAddUpdate
            // 
            this.buttonAddUpdate.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonAddUpdate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonAddUpdate.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonAddUpdate.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonAddUpdate.FlatAppearance.BorderSize = 0;
            this.buttonAddUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddUpdate.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonAddUpdate.ForeColor = System.Drawing.Color.White;
            this.buttonAddUpdate.Location = new System.Drawing.Point(375, 0);
            this.buttonAddUpdate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonAddUpdate.Name = "buttonAddUpdate";
            this.buttonAddUpdate.Size = new System.Drawing.Size(88, 45);
            this.buttonAddUpdate.TabIndex = 21;
            this.buttonAddUpdate.Text = "Update";
            this.buttonAddUpdate.UseVisualStyleBackColor = false;
            this.buttonAddUpdate.Click += new System.EventHandler(this.buttonAddUpdate_Click);
            // 
            // panel40
            // 
            this.panel40.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel40.Location = new System.Drawing.Point(355, 0);
            this.panel40.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel40.Name = "panel40";
            this.panel40.Size = new System.Drawing.Size(20, 45);
            this.panel40.TabIndex = 20;
            // 
            // buttonSearch
            // 
            this.buttonSearch.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonSearch.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonSearch.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonSearch.FlatAppearance.BorderSize = 0;
            this.buttonSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSearch.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonSearch.ForeColor = System.Drawing.Color.White;
            this.buttonSearch.Location = new System.Drawing.Point(255, 0);
            this.buttonSearch.Margin = new System.Windows.Forms.Padding(2);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(100, 45);
            this.buttonSearch.TabIndex = 15;
            this.buttonSearch.Text = "Search";
            this.buttonSearch.UseVisualStyleBackColor = false;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // panel39
            // 
            this.panel39.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel39.Location = new System.Drawing.Point(235, 0);
            this.panel39.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel39.Name = "panel39";
            this.panel39.Size = new System.Drawing.Size(20, 45);
            this.panel39.TabIndex = 19;
            // 
            // buttonDup
            // 
            this.buttonDup.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonDup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonDup.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonDup.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonDup.FlatAppearance.BorderSize = 0;
            this.buttonDup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDup.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonDup.ForeColor = System.Drawing.Color.White;
            this.buttonDup.Location = new System.Drawing.Point(135, 0);
            this.buttonDup.Margin = new System.Windows.Forms.Padding(2);
            this.buttonDup.Name = "buttonDup";
            this.buttonDup.Size = new System.Drawing.Size(100, 45);
            this.buttonDup.TabIndex = 14;
            this.buttonDup.Text = "Duplicate";
            this.buttonDup.UseVisualStyleBackColor = false;
            this.buttonDup.Click += new System.EventHandler(this.buttonDup_Click);
            // 
            // panel23
            // 
            this.panel23.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel23.Location = new System.Drawing.Point(115, 0);
            this.panel23.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(20, 45);
            this.panel23.TabIndex = 12;
            // 
            // buttonClear
            // 
            this.buttonClear.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonClear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonClear.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonClear.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonClear.FlatAppearance.BorderSize = 0;
            this.buttonClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonClear.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonClear.ForeColor = System.Drawing.Color.White;
            this.buttonClear.Location = new System.Drawing.Point(10, 0);
            this.buttonClear.Margin = new System.Windows.Forms.Padding(2);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(105, 45);
            this.buttonClear.TabIndex = 9;
            this.buttonClear.Text = "Clear Form";
            this.buttonClear.UseVisualStyleBackColor = false;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.buttonCancel);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel16.Location = new System.Drawing.Point(1594, 0);
            this.panel16.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(77, 45);
            this.panel16.TabIndex = 6;
            // 
            // buttonCancel
            // 
            this.buttonCancel.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonCancel.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonCancel.FlatAppearance.BorderSize = 0;
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCancel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonCancel.ForeColor = System.Drawing.Color.White;
            this.buttonCancel.Location = new System.Drawing.Point(0, 0);
            this.buttonCancel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(77, 45);
            this.buttonCancel.TabIndex = 1;
            this.buttonCancel.Text = "Close";
            this.buttonCancel.UseVisualStyleBackColor = false;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // panel13
            // 
            this.panel13.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel13.Location = new System.Drawing.Point(1671, 0);
            this.panel13.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(10, 45);
            this.panel13.TabIndex = 3;
            // 
            // panel14
            // 
            this.panel14.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel14.Location = new System.Drawing.Point(0, 0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(10, 45);
            this.panel14.TabIndex = 10;
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.label4);
            this.panel19.Controls.Add(this.panel24);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel19.Location = new System.Drawing.Point(0, 335);
            this.panel19.Margin = new System.Windows.Forms.Padding(4);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(1681, 2);
            this.panel19.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Left;
            this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(10, 0);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(240, 2);
            this.label4.TabIndex = 2;
            this.label4.Text = "Device list";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label4.Visible = false;
            // 
            // panel24
            // 
            this.panel24.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel24.Location = new System.Drawing.Point(0, 0);
            this.panel24.Margin = new System.Windows.Forms.Padding(4);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(10, 2);
            this.panel24.TabIndex = 1;
            // 
            // panel27
            // 
            this.panel27.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.panel27.Controls.Add(this.buttonDisable);
            this.panel27.Controls.Add(this.panel38);
            this.panel27.Controls.Add(this.buttonRefresh);
            this.panel27.Controls.Add(this.panel36);
            this.panel27.Controls.Add(this.labelListN);
            this.panel27.Controls.Add(this.label5);
            this.panel27.Controls.Add(this.labelResultN);
            this.panel27.Controls.Add(this.checkBoxShowSearchResult);
            this.panel27.Controls.Add(this.checkBoxShowDeactivated);
            this.panel27.Controls.Add(this.panel35);
            this.panel27.Controls.Add(this.panel34);
            this.panel27.Controls.Add(this.panel33);
            this.panel27.Controls.Add(this.panel30);
            this.panel27.Controls.Add(this.panel32);
            this.panel27.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel27.Location = new System.Drawing.Point(0, 646);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(1681, 40);
            this.panel27.TabIndex = 13;
            // 
            // buttonDisable
            // 
            this.buttonDisable.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonDisable.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonDisable.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonDisable.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonDisable.FlatAppearance.BorderSize = 0;
            this.buttonDisable.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDisable.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonDisable.ForeColor = System.Drawing.Color.White;
            this.buttonDisable.Location = new System.Drawing.Point(1219, 0);
            this.buttonDisable.Margin = new System.Windows.Forms.Padding(2);
            this.buttonDisable.Name = "buttonDisable";
            this.buttonDisable.Size = new System.Drawing.Size(145, 40);
            this.buttonDisable.TabIndex = 23;
            this.buttonDisable.Text = "Change Active";
            this.buttonDisable.UseVisualStyleBackColor = false;
            this.buttonDisable.Click += new System.EventHandler(this.buttonDisable_Click);
            // 
            // panel38
            // 
            this.panel38.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel38.Location = new System.Drawing.Point(1364, 0);
            this.panel38.Name = "panel38";
            this.panel38.Size = new System.Drawing.Size(20, 40);
            this.panel38.TabIndex = 22;
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonRefresh.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonRefresh.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonRefresh.FlatAppearance.BorderSize = 0;
            this.buttonRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRefresh.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonRefresh.ForeColor = System.Drawing.Color.White;
            this.buttonRefresh.Location = new System.Drawing.Point(434, 0);
            this.buttonRefresh.Margin = new System.Windows.Forms.Padding(2);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(112, 40);
            this.buttonRefresh.TabIndex = 17;
            this.buttonRefresh.Text = "Refresh";
            this.buttonRefresh.UseVisualStyleBackColor = false;
            this.buttonRefresh.Click += new System.EventHandler(this.buttonRefresh_Click);
            // 
            // panel36
            // 
            this.panel36.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel36.Location = new System.Drawing.Point(416, 0);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(18, 40);
            this.panel36.TabIndex = 18;
            // 
            // labelListN
            // 
            this.labelListN.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelListN.Font = new System.Drawing.Font("Arial", 10F);
            this.labelListN.ForeColor = System.Drawing.Color.White;
            this.labelListN.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.labelListN.Location = new System.Drawing.Point(372, 0);
            this.labelListN.Name = "labelListN";
            this.labelListN.Size = new System.Drawing.Size(44, 40);
            this.labelListN.TabIndex = 20;
            this.labelListN.Text = "0";
            this.labelListN.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Left;
            this.label5.Font = new System.Drawing.Font("Arial", 10F);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label5.Location = new System.Drawing.Point(356, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(16, 40);
            this.label5.TabIndex = 19;
            this.label5.Text = " / ";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelResultN
            // 
            this.labelResultN.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelResultN.Font = new System.Drawing.Font("Arial", 10F);
            this.labelResultN.ForeColor = System.Drawing.Color.White;
            this.labelResultN.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.labelResultN.Location = new System.Drawing.Point(314, 0);
            this.labelResultN.Name = "labelResultN";
            this.labelResultN.Size = new System.Drawing.Size(42, 40);
            this.labelResultN.TabIndex = 16;
            this.labelResultN.Text = "0";
            this.labelResultN.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // checkBoxShowSearchResult
            // 
            this.checkBoxShowSearchResult.AutoSize = true;
            this.checkBoxShowSearchResult.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxShowSearchResult.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxShowSearchResult.ForeColor = System.Drawing.Color.White;
            this.checkBoxShowSearchResult.Location = new System.Drawing.Point(160, 0);
            this.checkBoxShowSearchResult.Name = "checkBoxShowSearchResult";
            this.checkBoxShowSearchResult.Size = new System.Drawing.Size(154, 40);
            this.checkBoxShowSearchResult.TabIndex = 15;
            this.checkBoxShowSearchResult.Text = "Show Search Result";
            this.checkBoxShowSearchResult.UseVisualStyleBackColor = true;
            // 
            // checkBoxShowDeactivated
            // 
            this.checkBoxShowDeactivated.AutoSize = true;
            this.checkBoxShowDeactivated.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxShowDeactivated.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxShowDeactivated.ForeColor = System.Drawing.Color.White;
            this.checkBoxShowDeactivated.Location = new System.Drawing.Point(20, 0);
            this.checkBoxShowDeactivated.Name = "checkBoxShowDeactivated";
            this.checkBoxShowDeactivated.Size = new System.Drawing.Size(140, 40);
            this.checkBoxShowDeactivated.TabIndex = 14;
            this.checkBoxShowDeactivated.Text = "Show Deactivated";
            this.checkBoxShowDeactivated.UseVisualStyleBackColor = true;
            this.checkBoxShowDeactivated.CheckedChanged += new System.EventHandler(this.checkBoxShowDeactivated_CheckedChanged);
            // 
            // panel35
            // 
            this.panel35.Controls.Add(this.buttonDuplicate);
            this.panel35.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel35.Location = new System.Drawing.Point(1384, 0);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(158, 40);
            this.panel35.TabIndex = 5;
            // 
            // buttonDuplicate
            // 
            this.buttonDuplicate.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonDuplicate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonDuplicate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonDuplicate.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonDuplicate.FlatAppearance.BorderSize = 0;
            this.buttonDuplicate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDuplicate.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonDuplicate.ForeColor = System.Drawing.Color.White;
            this.buttonDuplicate.Location = new System.Drawing.Point(0, 0);
            this.buttonDuplicate.Margin = new System.Windows.Forms.Padding(2);
            this.buttonDuplicate.Name = "buttonDuplicate";
            this.buttonDuplicate.Size = new System.Drawing.Size(158, 40);
            this.buttonDuplicate.TabIndex = 2;
            this.buttonDuplicate.Text = "Duplicate Device";
            this.buttonDuplicate.UseVisualStyleBackColor = false;
            this.buttonDuplicate.Click += new System.EventHandler(this.buttonDuplicate_Click);
            // 
            // panel34
            // 
            this.panel34.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel34.Location = new System.Drawing.Point(1542, 0);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(20, 40);
            this.panel34.TabIndex = 4;
            // 
            // panel33
            // 
            this.panel33.Controls.Add(this.buttonEdit);
            this.panel33.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel33.Location = new System.Drawing.Point(1562, 0);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(109, 40);
            this.panel33.TabIndex = 3;
            // 
            // buttonEdit
            // 
            this.buttonEdit.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonEdit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonEdit.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonEdit.FlatAppearance.BorderSize = 0;
            this.buttonEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEdit.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonEdit.ForeColor = System.Drawing.Color.White;
            this.buttonEdit.Location = new System.Drawing.Point(0, 0);
            this.buttonEdit.Margin = new System.Windows.Forms.Padding(2);
            this.buttonEdit.Name = "buttonEdit";
            this.buttonEdit.Size = new System.Drawing.Size(109, 40);
            this.buttonEdit.TabIndex = 2;
            this.buttonEdit.Text = "Edit Device";
            this.buttonEdit.UseVisualStyleBackColor = false;
            this.buttonEdit.Click += new System.EventHandler(this.buttonEdit_Click);
            // 
            // panel30
            // 
            this.panel30.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel30.Location = new System.Drawing.Point(1671, 0);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(10, 40);
            this.panel30.TabIndex = 0;
            // 
            // panel32
            // 
            this.panel32.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel32.Location = new System.Drawing.Point(0, 0);
            this.panel32.Margin = new System.Windows.Forms.Padding(4);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(20, 40);
            this.panel32.TabIndex = 21;
            // 
            // panel20
            // 
            this.panel20.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel20.Location = new System.Drawing.Point(0, 644);
            this.panel20.Margin = new System.Windows.Forms.Padding(4);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(1681, 2);
            this.panel20.TabIndex = 14;
            this.panel20.Visible = false;
            // 
            // panel21
            // 
            this.panel21.Controls.Add(this.panel29);
            this.panel21.Controls.Add(this.panel26);
            this.panel21.Controls.Add(this.panel25);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel21.Location = new System.Drawing.Point(0, 337);
            this.panel21.Margin = new System.Windows.Forms.Padding(4);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(1681, 307);
            this.panel21.TabIndex = 15;
            // 
            // panel29
            // 
            this.panel29.Controls.Add(this.dataGridViewList);
            this.panel29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel29.Location = new System.Drawing.Point(2, 0);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(1677, 307);
            this.panel29.TabIndex = 3;
            // 
            // dataGridViewList
            // 
            this.dataGridViewList.AllowUserToAddRows = false;
            this.dataGridViewList.AllowUserToDeleteRows = false;
            this.dataGridViewList.AllowUserToOrderColumns = true;
            this.dataGridViewList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewList.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.dataGridViewList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnIndexNr,
            this.Active,
            this.ColumnActive,
            this.Column1ClientName,
            this.Column5ClientState,
            this.Column5,
            this.Column4,
            this.Column3ClientZip,
            this.Column2,
            this.Column3,
            this.Column9,
            this.Column7,
            this.Column8,
            this.Column6,
            this.Column7ClientCountry,
            this.Column2ClientStreet,
            this.Column1,
            this.ColumnFullName,
            this.Column6ClientCity});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewList.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewList.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewList.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridViewList.Name = "dataGridViewList";
            this.dataGridViewList.ReadOnly = true;
            this.dataGridViewList.RowHeadersVisible = false;
            this.dataGridViewList.RowTemplate.Height = 33;
            this.dataGridViewList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewList.ShowEditingIcon = false;
            this.dataGridViewList.Size = new System.Drawing.Size(1677, 307);
            this.dataGridViewList.TabIndex = 32;
            this.dataGridViewList.Click += new System.EventHandler(this.dataGridViewList_Click);
            this.dataGridViewList.DoubleClick += new System.EventHandler(this.dataGridViewList_DoubleClick);
            // 
            // ColumnIndexNr
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.ColumnIndexNr.DefaultCellStyle = dataGridViewCellStyle1;
            this.ColumnIndexNr.HeaderText = "Nr";
            this.ColumnIndexNr.Name = "ColumnIndexNr";
            this.ColumnIndexNr.ReadOnly = true;
            this.ColumnIndexNr.Width = 48;
            // 
            // Active
            // 
            this.Active.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Active.HeaderText = "Active";
            this.Active.Name = "Active";
            this.Active.ReadOnly = true;
            this.Active.Width = 71;
            // 
            // ColumnActive
            // 
            this.ColumnActive.HeaderText = "Device Model";
            this.ColumnActive.Name = "ColumnActive";
            this.ColumnActive.ReadOnly = true;
            this.ColumnActive.Width = 118;
            // 
            // Column1ClientName
            // 
            this.Column1ClientName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column1ClientName.HeaderText = "Serial #";
            this.Column1ClientName.MaxInputLength = 30;
            this.Column1ClientName.Name = "Column1ClientName";
            this.Column1ClientName.ReadOnly = true;
            this.Column1ClientName.Width = 81;
            // 
            // Column5ClientState
            // 
            this.Column5ClientState.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column5ClientState.HeaderText = "State";
            this.Column5ClientState.MaxInputLength = 30;
            this.Column5ClientState.Name = "Column5ClientState";
            this.Column5ClientState.ReadOnly = true;
            this.Column5ClientState.Width = 66;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Client";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 68;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Note (Client)";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 112;
            // 
            // Column3ClientZip
            // 
            this.Column3ClientZip.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column3ClientZip.HeaderText = "Study #";
            this.Column3ClientZip.MaxInputLength = 10;
            this.Column3ClientZip.Name = "Column3ClientZip";
            this.Column3ClientZip.ReadOnly = true;
            this.Column3ClientZip.Width = 81;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Ref. ID";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 76;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Add Study";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 98;
            // 
            // Column9
            // 
            this.Column9.HeaderText = "Def. RefID";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 98;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "IR";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 46;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "Ends";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 65;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Next State";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 98;
            // 
            // Column7ClientCountry
            // 
            this.Column7ClientCountry.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column7ClientCountry.HeaderText = "Start date";
            this.Column7ClientCountry.MaxInputLength = 32;
            this.Column7ClientCountry.Name = "Column7ClientCountry";
            this.Column7ClientCountry.ReadOnly = true;
            this.Column7ClientCountry.Width = 95;
            // 
            // Column2ClientStreet
            // 
            this.Column2ClientStreet.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column2ClientStreet.HeaderText = "End date";
            this.Column2ClientStreet.MaxInputLength = 30;
            this.Column2ClientStreet.Name = "Column2ClientStreet";
            this.Column2ClientStreet.ReadOnly = true;
            this.Column2ClientStreet.Width = 90;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "FW version";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 104;
            // 
            // ColumnFullName
            // 
            this.ColumnFullName.HeaderText = "First in service";
            this.ColumnFullName.Name = "ColumnFullName";
            this.ColumnFullName.ReadOnly = true;
            this.ColumnFullName.Width = 124;
            // 
            // Column6ClientCity
            // 
            this.Column6ClientCity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column6ClientCity.HeaderText = "Next service";
            this.Column6ClientCity.MaxInputLength = 30;
            this.Column6ClientCity.Name = "Column6ClientCity";
            this.Column6ClientCity.ReadOnly = true;
            this.Column6ClientCity.Width = 110;
            // 
            // panel26
            // 
            this.panel26.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel26.Location = new System.Drawing.Point(1679, 0);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(2, 307);
            this.panel26.TabIndex = 2;
            // 
            // panel25
            // 
            this.panel25.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel25.Location = new System.Drawing.Point(0, 0);
            this.panel25.Margin = new System.Windows.Forms.Padding(4);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(2, 307);
            this.panel25.TabIndex = 1;
            // 
            // CDeviceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(1681, 686);
            this.Controls.Add(this.panel21);
            this.Controls.Add(this.panel20);
            this.Controls.Add(this.panel27);
            this.Controls.Add(this.panel19);
            this.Controls.Add(this.panel12);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel5);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "CDeviceForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Device";
            this.panel2.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel46.ResumeLayout(false);
            this.panel51.ResumeLayout(false);
            this.panel42.ResumeLayout(false);
            this.panel42.PerformLayout();
            this.panel31.ResumeLayout(false);
            this.panel28.ResumeLayout(false);
            this.panel28.PerformLayout();
            this.panel52.ResumeLayout(false);
            this.panel52.PerformLayout();
            this.panel54.ResumeLayout(false);
            this.panel54.PerformLayout();
            this.panel55.ResumeLayout(false);
            this.panel55.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel19.ResumeLayout(false);
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            this.panel35.ResumeLayout(false);
            this.panel33.ResumeLayout(false);
            this.panel21.ResumeLayout(false);
            this.panel29.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label labelNextService;
        private System.Windows.Forms.DateTimePicker dateTimePickerFirstInService;
        private System.Windows.Forms.Label labelFirstInService;
        private System.Windows.Forms.DateTimePicker dateTimePickerNextService;
        private System.Windows.Forms.TextBox textBoxDeviceFirmwareVersion;
        private System.Windows.Forms.Label labelDeviceFirmwareVersion;
        private System.Windows.Forms.ComboBox comboBoxModel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelManufacturer;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelIndex;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.TextBox textBoxSerialNr;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Label labelError;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Button buttonDisable;
        private System.Windows.Forms.Panel panel38;
        private System.Windows.Forms.Button buttonRefresh;
        private System.Windows.Forms.Panel panel36;
        private System.Windows.Forms.Label labelListN;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelResultN;
        private System.Windows.Forms.CheckBox checkBoxShowSearchResult;
        private System.Windows.Forms.CheckBox checkBoxShowDeactivated;
        private System.Windows.Forms.Panel panel35;
        private System.Windows.Forms.Button buttonDuplicate;
        private System.Windows.Forms.Panel panel34;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.Button buttonEdit;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.DataGridView dataGridViewList;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.Label labelTextEndDate;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labelTextRefID;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button buttonSearch;
        private System.Windows.Forms.Button buttonDup;
        private System.Windows.Forms.Label labelErrorDevices;
        private System.Windows.Forms.TextBox textBoxRemLabel;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button buttonAddUpdate;
        private System.Windows.Forms.Panel panel40;
        private System.Windows.Forms.Panel panel39;
        private System.Windows.Forms.Panel panel41;
        private System.Windows.Forms.Button buttonShowStudy;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Panel panel42;
        private System.Windows.Forms.Button buttonSetAvailable;
        private System.Windows.Forms.Button buttonSetReceived;
        private System.Windows.Forms.Panel panel43;
        private System.Windows.Forms.Button buttonEndUse;
        private System.Windows.Forms.ComboBox comboBoxUseClient;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel46;
        private System.Windows.Forms.Button buttonSetUnavailable;
        private System.Windows.Forms.Button buttonSetInService;
        private System.Windows.Forms.Button buttonSetDefective;
        private System.Windows.Forms.Button buttonSetAssigned;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox labelState;
        private System.Windows.Forms.TextBox labelEndDate;
        private System.Windows.Forms.TextBox labelStartDate;
        private System.Windows.Forms.TextBox labelRefName;
        private System.Windows.Forms.TextBox labelRefID;
        private System.Windows.Forms.TextBox labelStudyNr;
        private System.Windows.Forms.Button buttonSetIntransit;
        private System.Windows.Forms.Button buttonClearStudy;
        private System.Windows.Forms.Panel panel49;
        private System.Windows.Forms.Panel panel50;
        private System.Windows.Forms.Button buttonTzConfig;
        private System.Windows.Forms.Button buttonSirConfig;
        private System.Windows.Forms.Label labelTzStoreName;
        private System.Windows.Forms.Button buttonChangeSNR;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelErrorInstruct;
        private System.Windows.Forms.Label labelDeviceChanged;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.Label labelShowSnr;
        private System.Windows.Forms.Panel panel52;
        private System.Windows.Forms.ComboBox comboBoxNextState;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBoxTzDefault;
        private System.Windows.Forms.ComboBox comboBoxRefID;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBoxServiceNote;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Panel panel51;
        private System.Windows.Forms.Label labelShowActiveState;
        private System.Windows.Forms.TextBox textBoxDeployRemark;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxTzOffset;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox comboBoxTimeZone;
        private System.Windows.Forms.ComboBox comboBoxAddStudy;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Panel panel53;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox textBoxIReaderNR;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label labelNoMct;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Panel panel54;
        private System.Windows.Forms.CheckBox checkBoxCh10;
        private System.Windows.Forms.CheckBox checkBoxCh9;
        private System.Windows.Forms.CheckBox checkBoxCh8;
        private System.Windows.Forms.CheckBox checkBoxCh7;
        private System.Windows.Forms.CheckBox checkBoxCh6;
        private System.Windows.Forms.CheckBox checkBoxCh5;
        private System.Windows.Forms.CheckBox checkBoxCh4;
        private System.Windows.Forms.CheckBox checkBoxCh3;
        private System.Windows.Forms.CheckBox checkBoxCh2;
        private System.Windows.Forms.CheckBox checkBoxCh1;
        private System.Windows.Forms.Panel panel55;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label labelNextServiceOff;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnIndexNr;
        private System.Windows.Forms.DataGridViewTextBoxColumn Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnActive;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1ClientName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5ClientState;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3ClientZip;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7ClientCountry;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2ClientStreet;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnFullName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6ClientCity;
    }
}