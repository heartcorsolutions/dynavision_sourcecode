﻿using Program_Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventBoard.EntryForms
{
    public partial class CPhysicianListForm : Form
    {
        public CPhysicianListForm()
        {
            InitializeComponent();
            Text = CProgram.sMakeProgTitle("Physician List", false, true);

        }
    }
}
