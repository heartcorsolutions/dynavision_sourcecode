﻿namespace EventboardEntryForms
{
    partial class CDeviceModelForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CDeviceModelForm));
            this.panel19 = new System.Windows.Forms.Panel();
            this.buttonClear = new System.Windows.Forms.Button();
            this.panel24 = new System.Windows.Forms.Panel();
            this.buttonAddUpdate = new System.Windows.Forms.Button();
            this.panel22 = new System.Windows.Forms.Panel();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.labelIndex = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.labelError = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.textBoxLabel = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxServiceInterval = new System.Windows.Forms.TextBox();
            this.labelServiceInterval = new System.Windows.Forms.Label();
            this.textBoxDeviceColor = new System.Windows.Forms.TextBox();
            this.labelDeviceColor = new System.Windows.Forms.Label();
            this.textBoxDeviceType = new System.Windows.Forms.TextBox();
            this.labelDeviceType = new System.Windows.Forms.Label();
            this.textBoxManufacturer = new System.Windows.Forms.TextBox();
            this.labelManufacturer = new System.Windows.Forms.Label();
            this.panel15 = new System.Windows.Forms.Panel();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.comboBoxCountryList = new System.Windows.Forms.ComboBox();
            this.textBoxManufacturerSkype = new System.Windows.Forms.TextBox();
            this.textBoxManufacturerEmail = new System.Windows.Forms.TextBox();
            this.labelManufacturerEmail = new System.Windows.Forms.Label();
            this.labelManufacturerSkype = new System.Windows.Forms.Label();
            this.textBoxManufacturerPhone2 = new System.Windows.Forms.TextBox();
            this.labelManufacturerPhone2 = new System.Windows.Forms.Label();
            this.textBoxManufacturerStreetNr = new System.Windows.Forms.TextBox();
            this.labelManufacturerStreetNr = new System.Windows.Forms.Label();
            this.textBoxManufacturerCell = new System.Windows.Forms.TextBox();
            this.labelManufacturerCell = new System.Windows.Forms.Label();
            this.textBoxManufacturerAddress = new System.Windows.Forms.TextBox();
            this.labelManufacturerAddress = new System.Windows.Forms.Label();
            this.comboManufacturerStateList = new System.Windows.Forms.ComboBox();
            this.textBoxManufacturerPhone1 = new System.Windows.Forms.TextBox();
            this.labelManufacturerPhone1 = new System.Windows.Forms.Label();
            this.labelManufacturerCountry = new System.Windows.Forms.Label();
            this.textBoxManufacturerCity = new System.Windows.Forms.TextBox();
            this.labelManufacturerCity = new System.Windows.Forms.Label();
            this.labelManufacturerState = new System.Windows.Forms.Label();
            this.textBoxManufacturerZip = new System.Windows.Forms.TextBox();
            this.labelManufacturerZip = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel19.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.panel19.Controls.Add(this.buttonClear);
            this.panel19.Controls.Add(this.panel24);
            this.panel19.Controls.Add(this.panel22);
            this.panel19.Controls.Add(this.panel10);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel19.Location = new System.Drawing.Point(0, 272);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(827, 37);
            this.panel19.TabIndex = 141;
            // 
            // buttonClear
            // 
            this.buttonClear.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonClear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonClear.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonClear.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonClear.FlatAppearance.BorderSize = 0;
            this.buttonClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonClear.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonClear.ForeColor = System.Drawing.Color.White;
            this.buttonClear.Location = new System.Drawing.Point(565, 0);
            this.buttonClear.Margin = new System.Windows.Forms.Padding(2);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(107, 37);
            this.buttonClear.TabIndex = 5;
            this.buttonClear.Text = "Clear Form";
            this.buttonClear.UseVisualStyleBackColor = false;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // panel24
            // 
            this.panel24.Controls.Add(this.buttonAddUpdate);
            this.panel24.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel24.Location = new System.Drawing.Point(672, 0);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(75, 37);
            this.panel24.TabIndex = 3;
            // 
            // buttonAddUpdate
            // 
            this.buttonAddUpdate.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonAddUpdate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonAddUpdate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonAddUpdate.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonAddUpdate.FlatAppearance.BorderSize = 0;
            this.buttonAddUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddUpdate.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonAddUpdate.ForeColor = System.Drawing.Color.White;
            this.buttonAddUpdate.Location = new System.Drawing.Point(0, 0);
            this.buttonAddUpdate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonAddUpdate.Name = "buttonAddUpdate";
            this.buttonAddUpdate.Size = new System.Drawing.Size(75, 37);
            this.buttonAddUpdate.TabIndex = 6;
            this.buttonAddUpdate.Text = "Add";
            this.buttonAddUpdate.UseVisualStyleBackColor = false;
            this.buttonAddUpdate.Click += new System.EventHandler(this.buttonAddUpdate_Click);
            // 
            // panel22
            // 
            this.panel22.Controls.Add(this.buttonCancel);
            this.panel22.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel22.Location = new System.Drawing.Point(747, 0);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(80, 37);
            this.panel22.TabIndex = 1;
            // 
            // buttonCancel
            // 
            this.buttonCancel.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonCancel.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonCancel.FlatAppearance.BorderSize = 0;
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCancel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonCancel.ForeColor = System.Drawing.Color.White;
            this.buttonCancel.Location = new System.Drawing.Point(0, 0);
            this.buttonCancel.Margin = new System.Windows.Forms.Padding(2);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(80, 37);
            this.buttonCancel.TabIndex = 2;
            this.buttonCancel.Text = "Close";
            this.buttonCancel.UseVisualStyleBackColor = false;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // panel10
            // 
            this.panel10.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(18, 37);
            this.panel10.TabIndex = 6;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.label13);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel12.Location = new System.Drawing.Point(0, 0);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(188, 34);
            this.panel12.TabIndex = 2;
            // 
            // label13
            // 
            this.label13.Dock = System.Windows.Forms.DockStyle.Left;
            this.label13.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(0, 0);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(194, 34);
            this.label13.TabIndex = 70;
            this.label13.Text = "Device Model details";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.label1);
            this.panel13.Controls.Add(this.labelIndex);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel13.Location = new System.Drawing.Point(188, 0);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(110, 34);
            this.panel13.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Right;
            this.label1.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(14, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 34);
            this.label1.TabIndex = 1;
            this.label1.Text = "# ";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelIndex
            // 
            this.labelIndex.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelIndex.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelIndex.Location = new System.Drawing.Point(62, 0);
            this.labelIndex.Name = "labelIndex";
            this.labelIndex.Size = new System.Drawing.Size(48, 34);
            this.labelIndex.TabIndex = 0;
            this.labelIndex.Text = "1234";
            this.labelIndex.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel14
            // 
            this.panel14.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel14.Location = new System.Drawing.Point(298, 0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(30, 34);
            this.panel14.TabIndex = 4;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel17);
            this.panel1.Controls.Add(this.panel14);
            this.panel1.Controls.Add(this.panel13);
            this.panel1.Controls.Add(this.panel12);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(827, 34);
            this.panel1.TabIndex = 138;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.labelError);
            this.panel17.Controls.Add(this.label6);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel17.Location = new System.Drawing.Point(328, 0);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(478, 34);
            this.panel17.TabIndex = 5;
            // 
            // labelError
            // 
            this.labelError.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelError.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelError.Location = new System.Drawing.Point(169, 0);
            this.labelError.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelError.Name = "labelError";
            this.labelError.Size = new System.Drawing.Size(309, 34);
            this.labelError.TabIndex = 126;
            this.labelError.Text = "...";
            this.labelError.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.Dock = System.Windows.Forms.DockStyle.Left;
            this.label6.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(169, 34);
            this.label6.TabIndex = 125;
            this.label6.Text = "Contact details";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.panel2);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(317, 235);
            this.panel8.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panel2.Controls.Add(this.textBoxLabel);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.textBoxServiceInterval);
            this.panel2.Controls.Add(this.labelServiceInterval);
            this.panel2.Controls.Add(this.textBoxDeviceColor);
            this.panel2.Controls.Add(this.labelDeviceColor);
            this.panel2.Controls.Add(this.textBoxDeviceType);
            this.panel2.Controls.Add(this.labelDeviceType);
            this.panel2.Controls.Add(this.textBoxManufacturer);
            this.panel2.Controls.Add(this.labelManufacturer);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Font = new System.Drawing.Font("Arial", 10F);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(317, 235);
            this.panel2.TabIndex = 137;
            // 
            // textBoxLabel
            // 
            this.textBoxLabel.Location = new System.Drawing.Point(126, 36);
            this.textBoxLabel.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxLabel.MaxLength = 32;
            this.textBoxLabel.Name = "textBoxLabel";
            this.textBoxLabel.Size = new System.Drawing.Size(172, 23);
            this.textBoxLabel.TabIndex = 138;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.label2.Location = new System.Drawing.Point(7, 39);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 16);
            this.label2.TabIndex = 137;
            this.label2.Text = "Display Name: *";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(208, 137);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(50, 16);
            this.label9.TabIndex = 136;
            this.label9.Text = "(Days)";
            // 
            // textBoxServiceInterval
            // 
            this.textBoxServiceInterval.Location = new System.Drawing.Point(126, 134);
            this.textBoxServiceInterval.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxServiceInterval.Name = "textBoxServiceInterval";
            this.textBoxServiceInterval.Size = new System.Drawing.Size(62, 23);
            this.textBoxServiceInterval.TabIndex = 14;
            // 
            // labelServiceInterval
            // 
            this.labelServiceInterval.AutoSize = true;
            this.labelServiceInterval.Location = new System.Drawing.Point(7, 137);
            this.labelServiceInterval.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelServiceInterval.Name = "labelServiceInterval";
            this.labelServiceInterval.Size = new System.Drawing.Size(109, 16);
            this.labelServiceInterval.TabIndex = 13;
            this.labelServiceInterval.Text = "Service interval:";
            this.labelServiceInterval.Click += new System.EventHandler(this.label8_Click);
            // 
            // textBoxDeviceColor
            // 
            this.textBoxDeviceColor.Location = new System.Drawing.Point(126, 102);
            this.textBoxDeviceColor.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxDeviceColor.MaxLength = 32;
            this.textBoxDeviceColor.Name = "textBoxDeviceColor";
            this.textBoxDeviceColor.Size = new System.Drawing.Size(172, 23);
            this.textBoxDeviceColor.TabIndex = 10;
            // 
            // labelDeviceColor
            // 
            this.labelDeviceColor.AutoSize = true;
            this.labelDeviceColor.Location = new System.Drawing.Point(7, 105);
            this.labelDeviceColor.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelDeviceColor.Name = "labelDeviceColor";
            this.labelDeviceColor.Size = new System.Drawing.Size(46, 16);
            this.labelDeviceColor.TabIndex = 9;
            this.labelDeviceColor.Text = "Color:";
            // 
            // textBoxDeviceType
            // 
            this.textBoxDeviceType.Location = new System.Drawing.Point(126, 69);
            this.textBoxDeviceType.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxDeviceType.MaxLength = 32;
            this.textBoxDeviceType.Name = "textBoxDeviceType";
            this.textBoxDeviceType.Size = new System.Drawing.Size(172, 23);
            this.textBoxDeviceType.TabIndex = 4;
            // 
            // labelDeviceType
            // 
            this.labelDeviceType.AutoSize = true;
            this.labelDeviceType.Location = new System.Drawing.Point(7, 72);
            this.labelDeviceType.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelDeviceType.Name = "labelDeviceType";
            this.labelDeviceType.Size = new System.Drawing.Size(43, 16);
            this.labelDeviceType.TabIndex = 3;
            this.labelDeviceType.Text = "Type:";
            // 
            // textBoxManufacturer
            // 
            this.textBoxManufacturer.Location = new System.Drawing.Point(126, 5);
            this.textBoxManufacturer.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxManufacturer.MaxLength = 64;
            this.textBoxManufacturer.Name = "textBoxManufacturer";
            this.textBoxManufacturer.Size = new System.Drawing.Size(172, 23);
            this.textBoxManufacturer.TabIndex = 2;
            // 
            // labelManufacturer
            // 
            this.labelManufacturer.AutoSize = true;
            this.labelManufacturer.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelManufacturer.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.labelManufacturer.Location = new System.Drawing.Point(7, 8);
            this.labelManufacturer.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelManufacturer.Name = "labelManufacturer";
            this.labelManufacturer.Size = new System.Drawing.Size(116, 16);
            this.labelManufacturer.TabIndex = 1;
            this.labelManufacturer.Text = "Manufacturer: *";
            this.labelManufacturer.Click += new System.EventHandler(this.label2_Click);
            // 
            // panel15
            // 
            this.panel15.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel15.Location = new System.Drawing.Point(317, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(11, 235);
            this.panel15.TabIndex = 4;
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.panel7);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel16.Location = new System.Drawing.Point(328, 0);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(495, 235);
            this.panel16.TabIndex = 5;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panel7.Controls.Add(this.comboBoxCountryList);
            this.panel7.Controls.Add(this.textBoxManufacturerSkype);
            this.panel7.Controls.Add(this.textBoxManufacturerEmail);
            this.panel7.Controls.Add(this.labelManufacturerEmail);
            this.panel7.Controls.Add(this.labelManufacturerSkype);
            this.panel7.Controls.Add(this.textBoxManufacturerPhone2);
            this.panel7.Controls.Add(this.labelManufacturerPhone2);
            this.panel7.Controls.Add(this.textBoxManufacturerStreetNr);
            this.panel7.Controls.Add(this.labelManufacturerStreetNr);
            this.panel7.Controls.Add(this.textBoxManufacturerCell);
            this.panel7.Controls.Add(this.labelManufacturerCell);
            this.panel7.Controls.Add(this.textBoxManufacturerAddress);
            this.panel7.Controls.Add(this.labelManufacturerAddress);
            this.panel7.Controls.Add(this.comboManufacturerStateList);
            this.panel7.Controls.Add(this.textBoxManufacturerPhone1);
            this.panel7.Controls.Add(this.labelManufacturerPhone1);
            this.panel7.Controls.Add(this.labelManufacturerCountry);
            this.panel7.Controls.Add(this.textBoxManufacturerCity);
            this.panel7.Controls.Add(this.labelManufacturerCity);
            this.panel7.Controls.Add(this.labelManufacturerState);
            this.panel7.Controls.Add(this.textBoxManufacturerZip);
            this.panel7.Controls.Add(this.labelManufacturerZip);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Font = new System.Drawing.Font("Arial", 10F);
            this.panel7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Margin = new System.Windows.Forms.Padding(4);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(495, 235);
            this.panel7.TabIndex = 126;
            // 
            // comboBoxCountryList
            // 
            this.comboBoxCountryList.DropDownWidth = 200;
            this.comboBoxCountryList.FormattingEnabled = true;
            this.comboBoxCountryList.Items.AddRange(new object[] {
            "USA",
            "Mexico"});
            this.comboBoxCountryList.Location = new System.Drawing.Point(311, 132);
            this.comboBoxCountryList.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxCountryList.Name = "comboBoxCountryList";
            this.comboBoxCountryList.Size = new System.Drawing.Size(167, 24);
            this.comboBoxCountryList.TabIndex = 30;
            // 
            // textBoxManufacturerSkype
            // 
            this.textBoxManufacturerSkype.Location = new System.Drawing.Point(311, 195);
            this.textBoxManufacturerSkype.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxManufacturerSkype.MaxLength = 64;
            this.textBoxManufacturerSkype.Name = "textBoxManufacturerSkype";
            this.textBoxManufacturerSkype.Size = new System.Drawing.Size(167, 23);
            this.textBoxManufacturerSkype.TabIndex = 38;
            // 
            // textBoxManufacturerEmail
            // 
            this.textBoxManufacturerEmail.Location = new System.Drawing.Point(85, 195);
            this.textBoxManufacturerEmail.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxManufacturerEmail.MaxLength = 128;
            this.textBoxManufacturerEmail.Name = "textBoxManufacturerEmail";
            this.textBoxManufacturerEmail.Size = new System.Drawing.Size(149, 23);
            this.textBoxManufacturerEmail.TabIndex = 40;
            // 
            // labelManufacturerEmail
            // 
            this.labelManufacturerEmail.AutoSize = true;
            this.labelManufacturerEmail.Location = new System.Drawing.Point(6, 198);
            this.labelManufacturerEmail.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelManufacturerEmail.Name = "labelManufacturerEmail";
            this.labelManufacturerEmail.Size = new System.Drawing.Size(46, 16);
            this.labelManufacturerEmail.TabIndex = 39;
            this.labelManufacturerEmail.Text = "Email:";
            // 
            // labelManufacturerSkype
            // 
            this.labelManufacturerSkype.AutoSize = true;
            this.labelManufacturerSkype.Location = new System.Drawing.Point(244, 198);
            this.labelManufacturerSkype.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelManufacturerSkype.Name = "labelManufacturerSkype";
            this.labelManufacturerSkype.Size = new System.Drawing.Size(51, 16);
            this.labelManufacturerSkype.TabIndex = 37;
            this.labelManufacturerSkype.Text = "Skype:";
            // 
            // textBoxManufacturerPhone2
            // 
            this.textBoxManufacturerPhone2.Location = new System.Drawing.Point(311, 164);
            this.textBoxManufacturerPhone2.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxManufacturerPhone2.MaxLength = 32;
            this.textBoxManufacturerPhone2.Name = "textBoxManufacturerPhone2";
            this.textBoxManufacturerPhone2.Size = new System.Drawing.Size(167, 23);
            this.textBoxManufacturerPhone2.TabIndex = 34;
            // 
            // labelManufacturerPhone2
            // 
            this.labelManufacturerPhone2.AutoSize = true;
            this.labelManufacturerPhone2.Location = new System.Drawing.Point(244, 167);
            this.labelManufacturerPhone2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelManufacturerPhone2.Name = "labelManufacturerPhone2";
            this.labelManufacturerPhone2.Size = new System.Drawing.Size(65, 16);
            this.labelManufacturerPhone2.TabIndex = 33;
            this.labelManufacturerPhone2.Text = "Phone 2:";
            // 
            // textBoxManufacturerStreetNr
            // 
            this.textBoxManufacturerStreetNr.Location = new System.Drawing.Point(223, 69);
            this.textBoxManufacturerStreetNr.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxManufacturerStreetNr.MaxLength = 8;
            this.textBoxManufacturerStreetNr.Name = "textBoxManufacturerStreetNr";
            this.textBoxManufacturerStreetNr.Size = new System.Drawing.Size(89, 23);
            this.textBoxManufacturerStreetNr.TabIndex = 24;
            // 
            // labelManufacturerStreetNr
            // 
            this.labelManufacturerStreetNr.AutoSize = true;
            this.labelManufacturerStreetNr.Location = new System.Drawing.Point(185, 72);
            this.labelManufacturerStreetNr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelManufacturerStreetNr.Name = "labelManufacturerStreetNr";
            this.labelManufacturerStreetNr.Size = new System.Drawing.Size(26, 16);
            this.labelManufacturerStreetNr.TabIndex = 23;
            this.labelManufacturerStreetNr.Text = "Nr:";
            // 
            // textBoxManufacturerCell
            // 
            this.textBoxManufacturerCell.Location = new System.Drawing.Point(85, 164);
            this.textBoxManufacturerCell.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxManufacturerCell.MaxLength = 32;
            this.textBoxManufacturerCell.Name = "textBoxManufacturerCell";
            this.textBoxManufacturerCell.Size = new System.Drawing.Size(149, 23);
            this.textBoxManufacturerCell.TabIndex = 36;
            // 
            // labelManufacturerCell
            // 
            this.labelManufacturerCell.AutoSize = true;
            this.labelManufacturerCell.Location = new System.Drawing.Point(6, 167);
            this.labelManufacturerCell.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelManufacturerCell.Name = "labelManufacturerCell";
            this.labelManufacturerCell.Size = new System.Drawing.Size(36, 16);
            this.labelManufacturerCell.TabIndex = 35;
            this.labelManufacturerCell.Text = "Cell:";
            // 
            // textBoxManufacturerAddress
            // 
            this.textBoxManufacturerAddress.Location = new System.Drawing.Point(85, 5);
            this.textBoxManufacturerAddress.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxManufacturerAddress.MaxLength = 250;
            this.textBoxManufacturerAddress.Multiline = true;
            this.textBoxManufacturerAddress.Name = "textBoxManufacturerAddress";
            this.textBoxManufacturerAddress.Size = new System.Drawing.Size(393, 56);
            this.textBoxManufacturerAddress.TabIndex = 20;
            // 
            // labelManufacturerAddress
            // 
            this.labelManufacturerAddress.AutoSize = true;
            this.labelManufacturerAddress.Location = new System.Drawing.Point(6, 9);
            this.labelManufacturerAddress.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelManufacturerAddress.Name = "labelManufacturerAddress";
            this.labelManufacturerAddress.Size = new System.Drawing.Size(64, 16);
            this.labelManufacturerAddress.TabIndex = 19;
            this.labelManufacturerAddress.Text = "Address:";
            // 
            // comboManufacturerStateList
            // 
            this.comboManufacturerStateList.DropDownWidth = 200;
            this.comboManufacturerStateList.FormattingEnabled = true;
            this.comboManufacturerStateList.Items.AddRange(new object[] {
            "NA",
            "Alabama",
            "Alaska",
            "Arizona",
            "Arkansas",
            "California",
            "Colorado",
            "Connecticut",
            "Delaware",
            "District of Columbia",
            "Florida",
            "Georgia",
            "Hawaii",
            "Idaho",
            "Illinois",
            "Indiana",
            "Iowa",
            "Kansas",
            "Kentucky",
            "Louisiana",
            "Maine",
            "Maryland",
            "Massachusetts",
            "Michigan",
            "Minnesota",
            "Mississippi",
            "Missouri",
            "Montana",
            "Nebraska",
            "Nevada",
            "New Hampshire",
            "New Jersey",
            "New Mexico",
            "New York",
            "North Carolina",
            "North Dakota",
            "Ohio",
            "Oklahoma",
            "Oregon",
            "Pennsylvania",
            "Rhode Island",
            "South Carolina",
            "South Dakota",
            "Tennessee",
            "Texas",
            "Utah",
            "Vermont",
            "Virginia",
            "Washington",
            "West Virginia",
            "Wisconsin"});
            this.comboManufacturerStateList.Location = new System.Drawing.Point(311, 101);
            this.comboManufacturerStateList.Margin = new System.Windows.Forms.Padding(4);
            this.comboManufacturerStateList.MaxLength = 48;
            this.comboManufacturerStateList.Name = "comboManufacturerStateList";
            this.comboManufacturerStateList.Size = new System.Drawing.Size(167, 24);
            this.comboManufacturerStateList.TabIndex = 26;
            // 
            // textBoxManufacturerPhone1
            // 
            this.textBoxManufacturerPhone1.Location = new System.Drawing.Point(85, 133);
            this.textBoxManufacturerPhone1.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxManufacturerPhone1.MaxLength = 32;
            this.textBoxManufacturerPhone1.Name = "textBoxManufacturerPhone1";
            this.textBoxManufacturerPhone1.Size = new System.Drawing.Size(149, 23);
            this.textBoxManufacturerPhone1.TabIndex = 32;
            // 
            // labelManufacturerPhone1
            // 
            this.labelManufacturerPhone1.AutoSize = true;
            this.labelManufacturerPhone1.Location = new System.Drawing.Point(6, 136);
            this.labelManufacturerPhone1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelManufacturerPhone1.Name = "labelManufacturerPhone1";
            this.labelManufacturerPhone1.Size = new System.Drawing.Size(65, 16);
            this.labelManufacturerPhone1.TabIndex = 31;
            this.labelManufacturerPhone1.Text = "Phone 1:";
            // 
            // labelManufacturerCountry
            // 
            this.labelManufacturerCountry.AutoSize = true;
            this.labelManufacturerCountry.Location = new System.Drawing.Point(244, 136);
            this.labelManufacturerCountry.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelManufacturerCountry.Name = "labelManufacturerCountry";
            this.labelManufacturerCountry.Size = new System.Drawing.Size(62, 16);
            this.labelManufacturerCountry.TabIndex = 29;
            this.labelManufacturerCountry.Text = "Country:";
            // 
            // textBoxManufacturerCity
            // 
            this.textBoxManufacturerCity.Location = new System.Drawing.Point(85, 102);
            this.textBoxManufacturerCity.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxManufacturerCity.MaxLength = 48;
            this.textBoxManufacturerCity.Name = "textBoxManufacturerCity";
            this.textBoxManufacturerCity.Size = new System.Drawing.Size(149, 23);
            this.textBoxManufacturerCity.TabIndex = 28;
            // 
            // labelManufacturerCity
            // 
            this.labelManufacturerCity.AutoSize = true;
            this.labelManufacturerCity.Location = new System.Drawing.Point(6, 105);
            this.labelManufacturerCity.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelManufacturerCity.Name = "labelManufacturerCity";
            this.labelManufacturerCity.Size = new System.Drawing.Size(36, 16);
            this.labelManufacturerCity.TabIndex = 27;
            this.labelManufacturerCity.Text = "City:";
            // 
            // labelManufacturerState
            // 
            this.labelManufacturerState.AutoSize = true;
            this.labelManufacturerState.Location = new System.Drawing.Point(244, 105);
            this.labelManufacturerState.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelManufacturerState.Name = "labelManufacturerState";
            this.labelManufacturerState.Size = new System.Drawing.Size(45, 16);
            this.labelManufacturerState.TabIndex = 25;
            this.labelManufacturerState.Text = "State:";
            // 
            // textBoxManufacturerZip
            // 
            this.textBoxManufacturerZip.Location = new System.Drawing.Point(85, 69);
            this.textBoxManufacturerZip.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxManufacturerZip.MaxLength = 16;
            this.textBoxManufacturerZip.Name = "textBoxManufacturerZip";
            this.textBoxManufacturerZip.Size = new System.Drawing.Size(83, 23);
            this.textBoxManufacturerZip.TabIndex = 22;
            // 
            // labelManufacturerZip
            // 
            this.labelManufacturerZip.AutoSize = true;
            this.labelManufacturerZip.Location = new System.Drawing.Point(6, 72);
            this.labelManufacturerZip.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelManufacturerZip.Name = "labelManufacturerZip";
            this.labelManufacturerZip.Size = new System.Drawing.Size(31, 16);
            this.labelManufacturerZip.TabIndex = 21;
            this.labelManufacturerZip.Text = "Zip:";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel16);
            this.panel3.Controls.Add(this.panel15);
            this.panel3.Controls.Add(this.panel8);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 34);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(827, 235);
            this.panel3.TabIndex = 139;
            // 
            // CDeviceModelForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(827, 309);
            this.Controls.Add(this.panel19);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.Name = "CDeviceModelForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Device Model";
            this.panel19.ResumeLayout(false);
            this.panel24.ResumeLayout(false);
            this.panel22.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel16.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Button buttonAddUpdate;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelIndex;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Label labelError;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox textBoxLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxServiceInterval;
        private System.Windows.Forms.Label labelServiceInterval;
        private System.Windows.Forms.TextBox textBoxDeviceColor;
        private System.Windows.Forms.Label labelDeviceColor;
        private System.Windows.Forms.TextBox textBoxDeviceType;
        private System.Windows.Forms.Label labelDeviceType;
        private System.Windows.Forms.TextBox textBoxManufacturer;
        private System.Windows.Forms.Label labelManufacturer;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.ComboBox comboBoxCountryList;
        private System.Windows.Forms.TextBox textBoxManufacturerSkype;
        private System.Windows.Forms.TextBox textBoxManufacturerEmail;
        private System.Windows.Forms.Label labelManufacturerEmail;
        private System.Windows.Forms.Label labelManufacturerSkype;
        private System.Windows.Forms.TextBox textBoxManufacturerPhone2;
        private System.Windows.Forms.Label labelManufacturerPhone2;
        private System.Windows.Forms.TextBox textBoxManufacturerStreetNr;
        private System.Windows.Forms.Label labelManufacturerStreetNr;
        private System.Windows.Forms.TextBox textBoxManufacturerCell;
        private System.Windows.Forms.Label labelManufacturerCell;
        private System.Windows.Forms.TextBox textBoxManufacturerAddress;
        private System.Windows.Forms.Label labelManufacturerAddress;
        private System.Windows.Forms.ComboBox comboManufacturerStateList;
        private System.Windows.Forms.TextBox textBoxManufacturerPhone1;
        private System.Windows.Forms.Label labelManufacturerPhone1;
        private System.Windows.Forms.Label labelManufacturerCountry;
        private System.Windows.Forms.TextBox textBoxManufacturerCity;
        private System.Windows.Forms.Label labelManufacturerCity;
        private System.Windows.Forms.Label labelManufacturerState;
        private System.Windows.Forms.TextBox textBoxManufacturerZip;
        private System.Windows.Forms.Label labelManufacturerZip;
        private System.Windows.Forms.Panel panel3;
    }
}