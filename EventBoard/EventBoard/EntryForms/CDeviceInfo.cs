﻿using Event_Base;
using EventboardEntryForms;
using Program_Base;
using System;
using System.Drawing;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventBoard.EntryForms
{
    public enum DDeviceState
    {
        Unknown = 0,
        New,
        Available,  //2
        Assigned,   //3
        Recording,
        Returned,
        InService,
        Defective,
        Unavailable,
        Intransit,
        NrStates
    }

    public enum DDeviceInfoVars
    {
        // primary keys
        DeviceSerialNr = 0,
        DeviceModel_IX,
        // variables
        FirmwareVersion,
        DateFirstInService,
        DateNextService,
        RemarkLabel,
        LogBook,
        ActiveStudy,
        mActiveRecording,
        ActiveRefID,
        ActiveRefName,
        State_IX,
        OperatingMode,
        ChannelNames,
        ChannelUnits,
        InvertChannelsMask,
        // added 20170125
        RecorderStartUTC,   //16
        RecorderEndUTC,
        // added 20190225 
        Client_IX,  // still to implement
        AddStudyMode,   // determins how the study is found for the record
        RefIdMode,      // default RefID mode for new study
        DeployText,     // device deployment text e.g. Zip 
        ServiceText,    // Service note e.g. lead broken
        NextStateMode,  // limit next state
        TimeZoneMode,   // perform time zone correction when record get assingned to study (once)
        TimeZoneDefaultMin,
        TimeZoneOffsetMin,
        IReaderNr,      // forwarded to IReader[nr]

        NrSqlVars       // keep as last
    };
    public enum DAddStudyModeEnum
    {
        ActiveStudy = 0,    // old default use the Device Active Study
        Manual,
        FindPatientID,      // search patient with the PatientID (n=1), find study that is still active (start + nrDays), (n=1) then add rec to study
        FindUniqueID,       // search patient with the UniqueID (n=1), find study that is still active (start + nrDays), (n=1) then add rec to study
        FindStudy,          // patientID = S123456, open study and check still active (start + nrDays), add rec to study
        FindStudyInitialsYear, // patientID = S123456FL1987, open study and check still active (start + nrDays), 
                               // check patient initials and birth year, add rec to study
                               // FindPatientNameDOB       // patientName = Last, First patientDOB = YYYYMMdd open study and check still active (start + nrDays), 
                               // check patient initials and birth year, add rec to study

        NrEnums
    };
    public enum DRefIDModeEnum
    {
        NotActive = 0,
        None,
        PatientID,
        UniqueID,
        Other,
        Study,              // S123456
        StudyInitialsYear,   // S123456FL1987
        NrEnums
    };
    public enum DTimeZoneModeEnum   // correct time zone (once when deviceIX is added to Event Record)
    {
        NotActive,
        UsePC,                 // no TimeZone read correction: TimeZone = default value
        UseDefault,            // correct TimeZone = default value (+correct UTC with difference to get same device event time)
        UseOffset,             // correct TimeZone by TimeZoneOffsetMin (+correct UTC with difference to get same device event time)
        SetDefault,            // set TimeZone to default value (File has UTC)
        SetOffset,             // correct TimeZone by offset value (File has UTC)
        NrEnums
    };
    public enum DNextStateModeEnum
    {
        Normal,             // normal
        Locked,             // lock disables all next state buttons (must be taken off locked before continue
        ToService,          // next state device must be In Service
        CheckDevice,        // Device must be checked next state device must be In Service
        CollectData,        // collect the data before assigning to next patient
        UpdateSettings,     // update must be performed before assigning to next patient
        UpdateFirmware,     // update firmware must be performed before assigning to next patient
        ToManufacturer,     // send device to manufacturer
        NrEnums
    }
    public class CDeviceInfo : CSqlDataTableRow
    {
        public string _mDeviceSerialNr;
        public UInt32 _mDeviceModel_IX;

        public string _mFirmwareVersion;
        public DateTime _mDateFirstInService;

        public DateTime _mDateNextService;
        public string _mRemarkLabel;
        public string _mLogBook;

        public UInt32 _mActiveStudy_IX;
        public UInt32 _mActiveRecording_IX;
        // advice use StudyNr and first letters as name for patient annomidity
        public CEncryptedString _mActiveRefID;         // reference ID used in recording device e.g. PatientID or StudyID =>Checked for coupleing to study
        public CEncryptedString _mActiveRefName;       // reference name used in recording device as given name  =>Checked for coupleing to study

        public UInt16 _mState_IX;              // Device state
        public string _mOperatingMode;      // Operating Mode e.g 4 lead event
        public string _mChannelNames;
        public string _mChannelUnits;
        public UInt32 _mInvertChannelsMask;

        // 20170125                             // Timespan in dBase max 2038
        public DateTime _mRecorderStartUTC;    // device has been assigned within Start and End time
        public DateTime _mRecorderEndUTC;      // used for fast lookup of study number

        // added 20190225 toDo
        public UInt32 _mClient_IX;
        public UInt16 _mAddStudyMode;
        public UInt16 _mRefIdMode;
        public string _mDeployText;
        public string _mServiceText;
        public UInt16 _mNextStateMode;
        public UInt16 _mTimeZoneMode;
        public Int16 _mTimeZoneDefaultMin;
        public Int16 _mTimeZoneOffsetMin;
        public Int16 _mIReaderNr;

        // temp for showing in list
        public string _mTempActiveStateText;

        static public bool sbModelFromDeviceInfo = true; 

        public CDeviceInfo(CSqlDBaseConnection ASqlConnection) : base(ASqlConnection, "d_deviceinfo")
        {
            try
            {
                UInt64 maskPrimary = CSqlDataTableRow.sGetMaskRange((UInt16)DDeviceInfoVars.DeviceSerialNr, (UInt16)DDeviceInfoVars.DeviceModel_IX);
                UInt64 maskValid = CSqlDataTableRow.sGetMaskRange((UInt16)0, (UInt16)DDeviceInfoVars.NrSqlVars - 1);// mGetValidMask(false);

                _mActiveRefID = new CEncryptedString("RefID", DEncryptLevel.L1_Program, 32);
                _mActiveRefName = new CEncryptedString("RefName", DEncryptLevel.L1_Program, 64);

                mInitTableMasks(maskPrimary, maskValid);  // set primairyInsertFlags

                mClear();
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Init ", ex);
            }
        }
        public override CSqlDataTableRow mCreateNewRow()
        {
            return new CDeviceInfo(mGetSqlConnection());
        }
        public override bool mbCopyTo(CSqlDataTableRow ATo) // derived  public override void mbCopyTo() must call base.mbCopyTo(ATo)
        {
            bool bOk = false;

            if (ATo != null)
            {
                CDeviceInfo to = ATo as CDeviceInfo;

                if (to != null)
                {
                    bOk = base.mbCopyTo(ATo);
                    to._mDeviceSerialNr = _mDeviceSerialNr;
                    to._mDeviceModel_IX = _mDeviceModel_IX;
                    to._mFirmwareVersion = _mFirmwareVersion;
                    to._mDateFirstInService = _mDateFirstInService;
                    to._mDateNextService = _mDateNextService;
                    to._mRemarkLabel = _mRemarkLabel;
                    to._mLogBook = _mLogBook;
                    to._mActiveStudy_IX = _mActiveStudy_IX;
                    to._mActiveRecording_IX = _mActiveRecording_IX;
                    to._mActiveRefID.mbCopyFrom(_mActiveRefID);
                    to._mActiveRefName.mbCopyFrom(_mActiveRefName);
                    to._mState_IX = _mState_IX;
                    to._mOperatingMode = _mOperatingMode;
                    to._mChannelNames = _mChannelNames;
                    to._mChannelUnits = _mChannelUnits;
                    to._mInvertChannelsMask = _mInvertChannelsMask;

                    // 20170125
                    to._mRecorderStartUTC = _mRecorderStartUTC;
                    to._mRecorderEndUTC = _mRecorderEndUTC;      // (used for slow lookup of study number

                    // added 20190225 
                    to._mClient_IX = _mClient_IX;
                    to._mAddStudyMode = _mAddStudyMode;
                    to._mRefIdMode = _mRefIdMode;
                    to._mDeployText = _mDeployText;
                    to._mServiceText = _mServiceText;
                    to._mNextStateMode = _mNextStateMode;
                    to._mTimeZoneMode = _mTimeZoneMode;
                    to._mTimeZoneDefaultMin = _mTimeZoneDefaultMin;
                    to._mTimeZoneOffsetMin = _mTimeZoneOffsetMin;
                    to._mIReaderNr = _mIReaderNr;

                    to._mTempActiveStateText = _mTempActiveStateText;
                }
            }
            return bOk;
        }
        public override void mClear()
        {
            base.mClear();
            _mDeviceSerialNr = "";
            _mDeviceModel_IX = 0;
            _mFirmwareVersion = "";
            _mDateFirstInService = DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Utc);
            _mDateNextService = DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Utc);
            _mRemarkLabel = "";
            _mLogBook = "";
            _mActiveStudy_IX = 0;
            _mActiveRecording_IX = 0;
            _mActiveRefID.mClear();
            _mActiveRefName.mClear();
            _mState_IX = (UInt16)DDeviceState.Unknown;
            _mOperatingMode = "";
            _mChannelNames = "";
            _mChannelUnits = "";
            _mInvertChannelsMask = 0;
            //20170125
            _mRecorderStartUTC = DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Utc);
            _mRecorderEndUTC = _mRecorderStartUTC;      // (used for slow lookup of study number
            // added 20190225 
            _mClient_IX = 0;
            _mAddStudyMode = 0;
            _mRefIdMode = 0;
            _mDeployText = "";
            _mServiceText = "";
            _mNextStateMode = 0;
            _mTimeZoneMode = 0;
            _mTimeZoneDefaultMin = 0;
            _mTimeZoneOffsetMin = 0;
            _mIReaderNr = 0;

            _mTempActiveStateText = "";
        }
        public override DSqlDataType mVarGetSet(DSqlDataCmd ACmd, UInt16 AVarIndex, ref string AVarSqlName, ref string AStringValue, out bool ArbValid)
        {
            switch ((DDeviceInfoVars)AVarIndex)
            {
                case DDeviceInfoVars.DeviceSerialNr:
                    return mSqlGetSetString(ref _mDeviceSerialNr, "DeviceSerialNr", ACmd, ref AVarSqlName, ref AStringValue, 32, out ArbValid);
                case DDeviceInfoVars.DeviceModel_IX:
                    return mSqlGetSetUInt32(ref _mDeviceModel_IX, "DeviceModel_IX", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DDeviceInfoVars.FirmwareVersion:
                    return mSqlGetSetString(ref _mFirmwareVersion, "FirmwareVersion", ACmd, ref AVarSqlName, ref AStringValue, 16, out ArbValid);
                case DDeviceInfoVars.DateFirstInService:
                    return mSqlGetSetDate(ref _mDateFirstInService, "DateFirstInService", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DDeviceInfoVars.DateNextService:
                    return mSqlGetSetDate(ref _mDateNextService, "DateNextService", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DDeviceInfoVars.RemarkLabel:
                    return mSqlGetSetString(ref _mRemarkLabel, "RemarkLabel", ACmd, ref AVarSqlName, ref AStringValue, 48, out ArbValid);
                case DDeviceInfoVars.LogBook:
                    return mSqlGetSetString(ref _mLogBook, "LogBook", ACmd, ref AVarSqlName, ref AStringValue, 30000, out ArbValid);
                case DDeviceInfoVars.ActiveStudy:
                    return mSqlGetSetUInt32(ref _mActiveStudy_IX, "ActiveStudy_IX", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DDeviceInfoVars.mActiveRecording:
                    return mSqlGetSetUInt32(ref _mActiveRecording_IX, "ActiveRecording_IX", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DDeviceInfoVars.ActiveRefID:
                    return mSqlGetSetEncryptedString(ref _mActiveRefID, "ActiveRefID", ACmd, ref AVarSqlName, ref AStringValue, 32, out ArbValid);
                case DDeviceInfoVars.ActiveRefName:
                    return mSqlGetSetEncryptedString(ref _mActiveRefName, "ActiveRefName", ACmd, ref AVarSqlName, ref AStringValue, 32, out ArbValid);
                case DDeviceInfoVars.State_IX:
                    return mSqlGetSetUInt16(ref _mState_IX, "State_IX", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DDeviceInfoVars.OperatingMode:
                    return mSqlGetSetString(ref _mOperatingMode, "OperatingMode", ACmd, ref AVarSqlName, ref AStringValue, 32, out ArbValid);
                case DDeviceInfoVars.ChannelNames:
                    return mSqlGetSetString(ref _mChannelNames, "ChannelNames", ACmd, ref AVarSqlName, ref AStringValue, 250, out ArbValid);
                case DDeviceInfoVars.ChannelUnits:
                    return mSqlGetSetString(ref _mChannelUnits, "ChannelUnits", ACmd, ref AVarSqlName, ref AStringValue, 250, out ArbValid);
                case DDeviceInfoVars.InvertChannelsMask:
                    return mSqlGetSetUInt32(ref _mInvertChannelsMask, "InvertChannelsMask", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DDeviceInfoVars.RecorderStartUTC:
                    return mSqlGetSetUTC(ref _mRecorderStartUTC, "RecorderStartUTC", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DDeviceInfoVars.RecorderEndUTC:
                    return mSqlGetSetUTC(ref _mRecorderEndUTC, "RecorderEndUTC", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                // added 20190225
                case DDeviceInfoVars.Client_IX:
                    return mSqlGetSetUInt32(ref _mClient_IX, "Client_IX", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DDeviceInfoVars.AddStudyMode:
                    return mSqlGetSetUInt16(ref _mAddStudyMode, "AddStudyMode", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DDeviceInfoVars.RefIdMode:
                    return mSqlGetSetUInt16(ref _mRefIdMode, "RefIdMode", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DDeviceInfoVars.DeployText:
                    return mSqlGetSetString(ref _mDeployText, "DeployText", ACmd, ref AVarSqlName, ref AStringValue, 256, out ArbValid);
                case DDeviceInfoVars.ServiceText:
                    return mSqlGetSetString(ref _mServiceText, "ServiceText", ACmd, ref AVarSqlName, ref AStringValue, 256, out ArbValid);
                case DDeviceInfoVars.NextStateMode:
                    return mSqlGetSetUInt16(ref _mNextStateMode, "NextStateMode", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DDeviceInfoVars.TimeZoneMode:
                    return mSqlGetSetUInt16(ref _mTimeZoneMode, "TimeZoneMode", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DDeviceInfoVars.TimeZoneDefaultMin:
                    return mSqlGetSetInt16(ref _mTimeZoneDefaultMin, "TimeZoneDefaultMin", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DDeviceInfoVars.TimeZoneOffsetMin:
                    return mSqlGetSetInt16(ref _mTimeZoneOffsetMin, "TimeZoneOffsetMin", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DDeviceInfoVars.IReaderNr:
                    return mSqlGetSetInt16(ref _mIReaderNr, "IReaderNr", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);


            }
            return base.mVarGetSet(ACmd, AVarIndex, ref AVarSqlName, ref AStringValue, out ArbValid);
        }

        public UInt16 mFindSerialList(ref List<CSqlDataTableRow> AList, UInt32 AModelIx, string ASerialNr, UInt16 ADeviceState)
        {
            UInt16 n = 0;
            UInt64 searchFlags = CSqlDataTableRow.sGetMask((UInt16)DDeviceInfoVars.DeviceSerialNr);

            if (ASerialNr != null && ASerialNr.Length > 0)
            {
                _mDeviceSerialNr = ASerialNr;
                if (AModelIx > 0)
                {
                    searchFlags |= CSqlDataTableRow.sGetMask((UInt16)DDeviceInfoVars.DeviceModel_IX);
                    _mDeviceModel_IX = AModelIx;
                }
                if (ADeviceState > 0)
                {
                    searchFlags |= CSqlDataTableRow.sGetMask((UInt16)DDeviceInfoVars.State_IX);
                    _mState_IX = ADeviceState;
                }
                bool bError;
                if (mbDoSqlSelectList(out bError, out AList, mMaskValid, searchFlags, true, ""))
                {
                    if (AList != null)
                    {
                        n = (UInt16)AList.Count;
                    }
                }
            }
            // must check RefID as well !!!!

            return n;
        }

        public UInt16 mFindLoadSerial(UInt32 AModelIx, string ASerialNr, UInt16 ADeviceState)
        {
            List<CSqlDataTableRow> mList = null;
            UInt16 n = mFindSerialList(ref mList, AModelIx, ASerialNr, ADeviceState);


            if (n >= 1)
            {
                if (mList == null || mList.Count < 1)
                {
                    n = 0;
                }
                else
                {
                    mbCopyFrom(mList[0]);
                }
            }
            return n;
        }

        public bool mbIsRecording()
        {
            return _mState_IX == (UInt16)DDeviceState.Assigned
                || _mState_IX == (UInt16)DDeviceState.Intransit
                || _mState_IX == (UInt16)DDeviceState.Recording;
        }
        public bool mbSameRefID(CEncryptedString ARefID)
        {
            bool bSame = false;

            if (_mActiveRefID.mbIsEmpty())
            {
                bSame = true;
            }
            else
            {
                string recStr = CProgram.sTrimString(ARefID.mDecrypt().ToUpper());
                string devStr = CProgram.sTrimString(_mActiveRefID.mDecrypt().ToUpper());

                bSame = recStr == devStr;
            }
            return bSame;
        }

        public bool mbActiveTime(DateTime AUtc)
        {
            return _mActiveStudy_IX != 0 && _mRecorderStartUTC != DateTime.MinValue && _mRecorderEndUTC != DateTime.MinValue   // old Studies / Device start time not valid
             && AUtc >= _mRecorderStartUTC && AUtc <= _mRecorderEndUTC;
        }

        public bool mbDoTimeZoneCorrection(out string ArTimeRemark, CRecordMit ARecord, bool AbForce)
        {
            bool bDone = false;
            string timeRem = "";

            if (mIndex_KEY != 0 && ARecord != null && (AbForce || ARecord.mDevice_IX == 0))
            {   // only apply correction if it is the first load.
                if (_mTimeZoneMode < (Int16)DTimeZoneModeEnum.NrEnums)
                {
                    int utcDeltaMin = 0;

                    switch ((DTimeZoneModeEnum)_mTimeZoneMode)
                    {
                        case DTimeZoneModeEnum.NotActive:
                            break;
                        case DTimeZoneModeEnum.UsePC:                 // if no TimeZone read correct TimeZone = default value PC
                            break;
                        case DTimeZoneModeEnum.UseDefault:               // correct TimeZone = default value
                            utcDeltaMin = ARecord.mTimeZoneOffsetMin - _mTimeZoneDefaultMin;

                            timeRem = "setTZone " + CProgram.sTimeZoneOffsetStringLong(_mTimeZoneDefaultMin);
                            //                                + " UTC«" + CProgram.sTimeZoneOffsetStringLong((Int16)utcDeltaMin);

                            ARecord.mTimeZoneOffsetMin = _mTimeZoneDefaultMin;
                            bDone = true;
                            break;
                        case DTimeZoneModeEnum.UseOffset:              // correct TimeZone by TimeZoneOffsetMin

                            utcDeltaMin = -_mTimeZoneOffsetMin;
                            timeRem = "offsetTZone " + CProgram.sTimeZoneOffsetStringLong(_mTimeZoneOffsetMin);
                            //+ " UTC«" + CProgram.sTimeZoneOffsetStringLong((Int16)utcDeltaMin);
                            ARecord.mTimeZoneOffsetMin += _mTimeZoneOffsetMin;
                            bDone = true;
                            break;
                        case DTimeZoneModeEnum.SetDefault:            // set TimeZone to default value (File has UTC)
                            utcDeltaMin = 0;
                            timeRem = "setTZone " + CProgram.sTimeZoneOffsetStringLong(_mTimeZoneDefaultMin);
                            ARecord.mTimeZoneOffsetMin = _mTimeZoneDefaultMin;
                            bDone = true;
                            break;
                        case DTimeZoneModeEnum.SetOffset:             // correct TimeZone by offset value (File has UTC)
                            utcDeltaMin = 0;
                            timeRem = "offsetTZone " + CProgram.sTimeZoneOffsetStringLong(_mTimeZoneOffsetMin);
                            ARecord.mTimeZoneOffsetMin += _mTimeZoneOffsetMin;
                            bDone = true;
                            break;

                    }
                    if (bDone && utcDeltaMin != 0)
                    {
                        ARecord.mBaseUTC = ARecord.mBaseUTC.AddMinutes(utcDeltaMin);
                        ARecord.mEventUTC = ARecord.mEventUTC.AddMinutes(utcDeltaMin);
                    }
                }
            }
            ArTimeRemark = timeRem;
            return bDone;
        }

 


public bool mbLoadDeviceFromRecord(ref string ArDeviceName, bool AbForceUpdateRecord, CRecordMit ARecord, 
            bool AbDoCheckIR, bool AbDoCheckTZone, bool AbAddDeviceRemark)
        {
            bool bOk = false;

            if (ARecord != null)
            {
                string recRemark = "";
                string tailRemark = "";
                bool bUpdateRec = AbForceUpdateRecord;
                bool bChangedDeviceID = false;

                ArDeviceName = ARecord.mDeviceID;
                try
                {
                    if (ARecord.mDevice_IX == 0)
                    {
                        // find device
                        int nSerial = mFindLoadSerial(0, ARecord.mDeviceID, 0); // loads device

                        if (nSerial == 0)
                        {
                            string altDeviceStr = ARecord.mGetAlternateSerialNr();
                            if (altDeviceStr != null && altDeviceStr.Length > 0)
                            {
                                nSerial = mFindLoadSerial(0, altDeviceStr, 0);// needed for tz
                                if (nSerial > 0)
                                {
                                    ArDeviceName += "->" + altDeviceStr;
                                    tailRemark = ARecord.mDeviceID + "=" + altDeviceStr;
                                    ARecord.mDeviceID = altDeviceStr;
                                    bUpdateRec = true;
                                    bChangedDeviceID = true;
                                }
                            }
                        }
                        if (nSerial == 0 || mIndex_KEY == 0)
                        {
                            recRemark = mGetAddStudyModeChar() + "D?" + ARecord.mDeviceID;
                            CProgram.sLogLine("R#" + ARecord.mIndex_KEY.ToString() + " Failed to find deviceID: " + ArDeviceName);
                            // can not find recording device                     
                        }
                        else
                        {
                            // device found and loaded
                            if (_mRemarkLabel != null && _mRemarkLabel.Length > 0)
                            {
                                recRemark = mGetAddStudyModeChar() + "D:" + _mRemarkLabel;
                            }
                            else if (_mAddStudyMode != (UInt16)DAddStudyModeEnum.ActiveStudy)
                            {
                                recRemark = mGetAddStudyModeChar() + "D:";
                            }
                            if (AbDoCheckIR && _mIReaderNr > 0)
                            {
                                // checking if we are the right IReader
                                UInt16 irNr = CProgram.sGetProgRunNr();
                                if (irNr != _mIReaderNr)
                                {
                                    CProgram.sAddLine(ref recRemark, "IR[" + irNr.ToString() + "] wrong " + _mIReaderNr.ToString() + "!");
                                    CProgram.sLogWarning("IR[" + irNr.ToString() + "] wrong " + _mIReaderNr.ToString()
                                        + " check device " + _mDeviceSerialNr + "!");
                                }
                            }
                            // no device in Record -> first load -> do Time Zone Correction
                            if (AbDoCheckTZone)
                            {
                                string timeRemark = "";
                                if (mbDoTimeZoneCorrection(out timeRemark, ARecord, false))
                                {
                                    bUpdateRec = true;
                                }
                                CProgram.sAddLine(ref recRemark, timeRemark);
                            }
//                            if (AbAddDeviceRemark)
//                            {
//                                CProgram.sAddLine(ref recRemark, _mRemarkLabel);
//                            }
                        }
                        ARecord.mDevice_IX = mIndex_KEY;    // update device in record
                        if(sbModelFromDeviceInfo)
                        {
                            string modelLabel = CDeviceModel.sGetDeviceModelLabel(_mDeviceModel_IX);
                            if (modelLabel.Length > 0)
                            {
                                ARecord.mDeviceModel = modelLabel;
                            }
                            else
                            {
                                CProgram.sLogError("Failed get device model #"+ _mDeviceModel_IX 
                                    + " for device " + ArDeviceName + " #"+ mIndex_KEY 
                                    + " R"+ ARecord.mIndex_KEY + " using " + ARecord.mDeviceModel);
                            }
                        }
                        ARecord.mModifyInvertMask(_mInvertChannelsMask);    // use default invert mask.
                        bUpdateRec = true;
                        bOk = mIndex_KEY > 0;
                    }
                    else if (mIndex_KEY == ARecord.mDevice_IX)
                    {
                        bOk = true; // device already loaded
                    }
                    else
                    {
                        // load known device index
                        if (false == mbDoSqlSelectIndex(ARecord.mDevice_IX, mMaskValid))
                        {
                            CProgram.sLogLine("R#" + ARecord.mIndex_KEY.ToString() + " devID: " + ARecord.mDeviceID + " Failed to load deviceID: " + ARecord.mDeviceID);
                            mIndex_KEY = 0;
                        }
                        else
                        {
                            bOk = true;
                        }
                    }
                    if (recRemark.Length > 0 || tailRemark.Length > 0)
                    {
                        string remark = ARecord.mRecRemark.mDecrypt();

                        CProgram.sAddLine(ref remark, recRemark);
                        CProgram.sAddLine(ref remark, tailRemark);
                        bUpdateRec = true;
                        ARecord.mRecRemark.mbEncrypt(remark);
                    }
                    if (bOk && bUpdateRec && ARecord.mIndex_KEY > 0)                // do not update if record has no index (load from file)
                    {
                        UInt64 saveMask = ARecord.mMaskValid;
                        UInt64 forcePkMask = 0;

                        if (bChangedDeviceID)
                        {   // update DeviceID

                            forcePkMask |= CSqlDataTableRow.sGetMask((UInt16)DRecordMitVars.DeviceID);
                        }
                        bOk = ARecord.mbDoSqlUpdate(saveMask, true, forcePkMask);  // update record to contain new found info
                        if (false == bOk)
                        {
                            CProgram.sLogError("Failed update record " + ARecord.mIndex_KEY.ToString() + " with device " + ArDeviceName);
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Failed load device " + ArDeviceName + " from record R#" + ARecord.mIndex_KEY.ToString(), ex);
                    bOk = false;
                }
            }
            return bOk;
        }

        public bool mbLoadStudyPatientFromRecord(ref string ArShowError, CRecordMit ARecord, bool AbLoadStudyPatient, string ADeviceName, ref CStudyInfo ArStudy, ref CPatientInfo ArPatient )
        {

            bool bOk = false;
            string doStr = AbLoadStudyPatient ? "Study and Patient" : "Study";

            if (ARecord != null /*&& ARecord.mIndex_KEY > 0*/)
            {
                try
                {
                    doStr = "Study " + ARecord.mStudy_IX.ToString();
                    if (AbLoadStudyPatient)
                    {
                        doStr += " and Patient " + ARecord.mPatient_IX.ToString();
                    }

                    if (ARecord.mStudy_IX == 0)
                    {
                        bOk = false;    // no study to load
                    }
                    else
                    {
                        if (ArStudy == null)
                        {
                            ArStudy = new CStudyInfo(mGetSqlConnection());
                        }
                        if (ArStudy != null && ArStudy.mIndex_KEY != ARecord.mStudy_IX
                            && ArStudy.mbDoSqlSelectIndex(ARecord.mStudy_IX, ArStudy.mMaskValid))
                        {
                            bOk = true;

                            if( AbLoadStudyPatient )
                            {
                                UInt32 patientIX = ArStudy._mPatient_IX;

                                if( patientIX == 0)
                                {
                                    patientIX = ARecord.mPatient_IX;
                                }
                                if( patientIX == 0)
                                {
                                    CProgram.sLogError("No patient to load for study " + ARecord.mStudy_IX.ToString() + " for record R#" + ARecord.mIndex_KEY.ToString() + " " + ADeviceName);
                                    CProgram.sAddText(ref ArShowError, "No patient for study " + ARecord.mStudy_IX.ToString(), ", ");
                                    bOk = false;
                                    
                                }
                                else
                                {
                                    if( ARecord.mPatient_IX != patientIX)
                                    {
                                        CProgram.sLogError("Different patient " + ARecord.mPatient_IX.ToString() + " loading "+ patientIX.ToString() 
                                            + " study " + ARecord.mStudy_IX.ToString() + " for record R#" + ARecord.mIndex_KEY.ToString() + " " + ADeviceName);
                                        CProgram.sAddText(ref ArShowError, "Different patient", ", ");
                                        bOk = false;
                                    }

                                    if (ArPatient == null)
                                    {
                                        ArPatient = new CPatientInfo(mGetSqlConnection());
                                    }

                                    if (ArPatient != null && ArPatient.mIndex_KEY != patientIX && patientIX > 0)
                                    {
                                        if (false == ArPatient.mbDoSqlSelectIndex(patientIX, ArPatient.mMaskValid))
                                        {
                                            CProgram.sLogError("Failed to load patient " + patientIX.ToString() + " study " + ARecord.mStudy_IX.ToString() + " for record R#" + ARecord.mIndex_KEY.ToString() + " " + ADeviceName);
                                            CProgram.sAddText(ref ArShowError, "Failed load patient " + patientIX.ToString(), ", ");
                                            bOk = false;
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            CProgram.sLogError("Failed to load study " + ARecord.mStudy_IX.ToString() + " for record R#" + ARecord.mIndex_KEY.ToString() + " " + ADeviceName);
                            CProgram.sAddText(ref ArShowError, "Failed load study "+ ARecord.mStudy_IX.ToString(), ", ");
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Failed to load " + doStr + " for record R#" + ARecord.mIndex_KEY.ToString() + " " + ADeviceName, ex);
                    CProgram.sAddText(ref ArShowError, "Failed ex", ", ");

                    bOk = false;
                }
            }
            return bOk;
        }

        public void mCheckSetPatientID(CRecordMit ARecord, string APatientID, ref string ARemark ) // ArPatient.mGetPatientSocIDValue(false)
        {
            string recID = CProgram.sTrimString(ARecord.mPatientID.mDecrypt());
            string patID = CProgram.sTrimString(APatientID);
            string line = mGetAddStudyModeChar() + "Ref";

            if (patID.Length > 0)
            {
                if (_mActiveRefID != null && _mActiveRefID.mbNotEmpty())
                {
                    if (recID.ToLower() != patID.ToLower())
                    {
                        line += "!=" + recID;
                    }
                    else
                    {
                        line += "=" + recID;
                    }
                }
            }
            else
            {
                patID = "?" + recID;
            }
            CProgram.sAddLine(ref ARemark, line);
            ARecord.mPatientID.mbEncrypt(patID);
        }

        public bool mbAddRecordToStudy(ref string ArShowError, CRecordMit ARecord, string AAddRecRemark, string ADeviceName, UInt32 AStudyIX, out CStudyInfo ArStudy, out CPatientInfo ArPatient,
            bool AbUpdatePatientInfo, bool AbWriteRefFile)
        {
            bool bOk = false;
            CStudyInfo study = null;
            CPatientInfo patient = null;

            if (ARecord != null && ARecord.mIndex_KEY > 0 && AStudyIX > 0)
            {
                string recRemark = "";
                bool bUpdateRec = false;
                UInt16 nrRecInStudy = 0;

                try
                {
                    if (ARecord.mStudy_IX > 0 && ARecord.mStudy_IX != AStudyIX)
                    {
                        CProgram.sLogError(" record R#" + ARecord.mIndex_KEY.ToString() + " " + ADeviceName + " has Study " + ARecord.mStudy_IX.ToString() + " != " + AStudyIX.ToString());
                        CProgram.sAddText(ref ArShowError, "Study " + AStudyIX.ToString() + "!=" + ARecord.mStudy_IX.ToString() , ", ");
                    }
                    else
                    {
                        study = new CStudyInfo(mGetSqlConnection());

                        if (study != null)
                        {
                            bOk = study.mbDoSqlSelectIndex(AStudyIX, study.mMaskValid);
                        }
                        if (false == bOk)
                        {
                            CProgram.sLogError(" record R#" + ARecord.mIndex_KEY.ToString() + " " + ADeviceName + " failed load Study " + AStudyIX.ToString());
                            CProgram.sAddText(ref ArShowError, "Failed load study " + AStudyIX.ToString(), ", ");
                        }
                        else
                        {
                            if (ARecord.mStudy_IX == 0)
                            {
                                ARecord.mSeqNrInStudy = ++study._mNrRecords;

                                UInt64 saveMask = CSqlDataTableRow.sGetMask((UInt16)DStudyInfoVars.NrRecords);

                                bOk = study.mbDoSqlUpdate(saveMask, true);  // update study nrRecords

                                if (false == bOk)
                                {
                                    CProgram.sLogError(" record R#" + ARecord.mIndex_KEY.ToString() + " " + ADeviceName + " failed update Study "
                                        + AStudyIX.ToString() + " nrRecords=" + study._mNrRecords.ToString());
                                    ARecord.mSeqNrInStudy = 0;
                                    CProgram.sAddText(ref ArShowError, "Failed update study "+ AStudyIX.ToString(), ", ");

                                }
                                else
                                {
                                    ARecord.mStudy_IX = AStudyIX;
                                    // 20190227 copy trial and study permisions to record for use in selecting records
                                    ARecord._mStudyTrial_IX = study._mTrial_IX;
                                    ARecord._mStudyClient_IX = study._mClient_IX;
                                    ARecord.mPatient_IX = study._mPatient_IX;

                                    if (study._mStudyPermissions != null)
                                    {
                                        study._mStudyPermissions.mCopyTo(ref ARecord._mStudyPermissions);
                                    }
                                    bUpdateRec = true;
                                    if (study._mStudyRemarkLabel != null && study._mStudyRemarkLabel.Length > 0)
                                    {
                                        recRemark = mGetAddStudyModeChar() + "S: " + study._mStudyRemarkLabel;
                                    }
                                }
                            }
                            nrRecInStudy = ARecord.mSeqNrInStudy;

                        }
                        if (bOk && AbUpdatePatientInfo && ARecord.mPatient_IX > 0)
                        {
                            patient = new CPatientInfo(mGetSqlConnection());

                            if (patient != null && ARecord.mPatient_IX > 0)
                            {
                                if (false == patient.mbDoSqlSelectIndex(ARecord.mPatient_IX, patient.mMaskValid))
                                {
                                    CProgram.sLogError(" record R#" + ARecord.mIndex_KEY.ToString() + " " + ADeviceName + " failed load patient " + ARecord.mPatient_IX.ToString());
                                }
                                else
                                {
                                    // force use of patient name from study / patient info
                                    ARecord.mPatientBirthDate = patient._mPatientDateOfBirth;
                                    ARecord.mPatientBirthYear = patient._mPatientBirthYear;
                                    ARecord.mPatientGender = patient._mPatientGender_IX;
                                    if (patient._mRemarkLabel != null && patient._mRemarkLabel.Length > 0)
                                    {
                                        CProgram.sAddLine(ref recRemark, mGetAddStudyModeChar() + "P: " + patient._mRemarkLabel);
                                    }
                                    mCheckSetPatientID(ARecord, patient.mGetPatientSocIDValue(false), ref recRemark); // set patientID and add old refID to remark
                                    //  ARecord.mPatientID.mbEncrypt(patient.mGetPatientSocIDValue(false)); // patientID or patientSocID()
                                    ARecord.mPatientTotalName.mbEncrypt(patient.mGetFullName());
                                    bUpdateRec = true;
                                }
                            }
                        }
                        if (recRemark.Length > 0)
                        {
                            string remark = ARecord.mRecRemark.mDecrypt();

                            CProgram.sAddLine(ref remark, recRemark);
                            bUpdateRec = true;
                            ARecord.mRecRemark.mbEncrypt(remark);
                        }

                        if (bOk && bUpdateRec)
                        {
                            UInt64 saveMask = ARecord.mMaskValid;
                            bOk = ARecord.mbDoSqlUpdate(saveMask, true, 0);  // update record to contain new found info
                            if (false == bOk)
                            {
                                CProgram.sLogError("Failed update record " + ARecord.mIndex_KEY.ToString() + " with device " + ADeviceName);
                                CProgram.sAddText(ref ArShowError, "Failed update rec", ", ");

                            }
                        }

                        string done = bOk ? "Set " : "!Set failed ";

                        CProgram.sLogLine("R#" + ARecord.mIndex_KEY.ToString() + done + "Study #" + ARecord.mStudy_IX.ToString()
                                + "." + ARecord.mSeqNrInStudy.ToString() + " /" + nrRecInStudy.ToString()
                                + " P" + ARecord.mPatient_IX.ToString()
                                + " T" + ARecord._mStudyTrial_IX.ToString()
                                + " C" + ARecord._mStudyClient_IX.ToString()
                                + " for deviceID: " + ADeviceName);
                    }
                    if (bUpdateRec || AbWriteRefFile)
                    {
                        string recordingDir = CDvtmsData.sGetRecordingCaseDir(ARecord);

                        ARecord.mbWriteRefFile(recordingDir);
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Failed to add record R#" + ARecord.mIndex_KEY.ToString() + " " + ADeviceName + " to Study " + AStudyIX.ToString(), ex);
                    CProgram.sAddText(ref ArShowError, "Failed ex", ", ");

                    bOk = false;
                }
            }
            ArStudy = study;
            ArPatient = patient;
            return bOk;
        }

        public UInt32 mFindAddToStudyByPatient(ref string ArShowError, bool AbSearchUniqueID, CRecordMit ARecord, string ADeviceName)
        {
            UInt32 studyIX = 0;

            if (ARecord == null || ARecord.mIndex_KEY == 0)
            {
                CProgram.sLogError("Empty Record!");
            }
            else
            {
                try
                {
                    string refID = ARecord.mPatientID.mDecrypt().Trim();

                    CPatientInfo patient = new CPatientInfo(mGetSqlConnection());
                    CStudyInfo study = new CStudyInfo(mGetSqlConnection());
                    List<CSqlDataTableRow> listPatients = new List<CSqlDataTableRow>();
                    List<CSqlDataTableRow> listStudys = new List<CSqlDataTableRow>();

                    if (patient != null && study != null && listPatients != null && listStudys != null)
                    {
                        UInt64 whereMask = 0;
                        UInt64 loadMask = patient.mGetValidMask(false);
                        string searchStr;

                        if (AbSearchUniqueID)
                        {
                            whereMask = CSqlDataTableRow.sGetMask((UInt16)DPatientInfoVars.SocSecNr);
                            patient._mSocSecNr.mbEncrypt(refID);
                            searchStr = "uniqueID=" + refID;
                        }
                        else
                        {
                            whereMask = CSqlDataTableRow.sGetMask((UInt16)DPatientInfoVars.PatientID);
                            patient.mbSetPatientIDValue(refID);
                            searchStr = "patientID=" + refID;
                        }
                        bool bError;

                        patient.mbDoSqlSelectList(out bError, out listPatients, loadMask, whereMask, true, "", DSqlSort.Desending, (UInt16)DSqlDataTableColum.Index_KEY);

                        if (bError)
                        {
                            CProgram.sLogError("FindStudyByPatient: record R#" + ARecord.mIndex_KEY.ToString() + " " + ADeviceName + " error find " + searchStr);
                            CProgram.sAddText(ref ArShowError, "Error " + searchStr, ", ");
                        }
                        else if (listPatients == null || listPatients.Count == 0)
                        {
                            CProgram.sLogLine("FindStudyByPatient: record R#" + ARecord.mIndex_KEY.ToString() + " " + ADeviceName + " failed find " + searchStr);
                            CProgram.sAddText(ref ArShowError, "No " + searchStr, ", ");
                        }
                        else if (listPatients.Count > 1)
                        {
                            CProgram.sLogLine("FindStudyByPatient: record R#" + ARecord.mIndex_KEY.ToString() + " " + ADeviceName
                                + " " + listPatients.Count.ToString() + " found " + searchStr);
                            CProgram.sAddText(ref ArShowError, "Multiple " + searchStr, ", ");
                        }
                        else
                        {
                            CPatientInfo selectedPatient = (CPatientInfo)listPatients[0];
                            UInt32 patientIX = selectedPatient.mIndex_KEY;

                            loadMask = study.mMaskValid;
                            whereMask = CSqlDataTableRow.sGetMask((UInt16)DStudyInfoVars.Patient_IX);
                            study._mPatient_IX = patientIX;

                            study.mbDoSqlSelectList(out bError, out listStudys, loadMask, whereMask, true, "", DSqlSort.Desending, (UInt16)DSqlDataTableColum.Index_KEY);

                            if (bError)
                            {
                                CProgram.sLogError("FindStudyByPatient: record R#" + ARecord.mIndex_KEY.ToString() + " " + ADeviceName
                                    + "P " + patientIX.ToString() + " error find study for  " + searchStr);
                                CProgram.sAddText(ref ArShowError, "Study Error " + searchStr, ", ");
                            }
                            else if (listStudys == null || listStudys.Count == 0)
                            {
                                CProgram.sLogLine("FindStudyByPatient: record R#" + ARecord.mIndex_KEY.ToString() + " " + ADeviceName
                                    + "P " + patientIX.ToString() + " failed find study for " + searchStr);                               
                            }
                            else
                            {
                                foreach (CStudyInfo row in listStudys)
                                {
                                    if (row._mPatient_IX != patientIX)
                                    {
                                        CProgram.sLogError("FindStudyByPatient: record R#" + ARecord.mIndex_KEY.ToString() + " " + ADeviceName
                                            + "P " + patientIX.ToString() + " bad row p=" + row._mPatient_IX.ToString()
                                            + " find study for  " + searchStr);
                                    }
                                    else
                                    {
                                        bool bInRange = row._mRecorderStartUTC != DateTime.MinValue && row._mRecorderEndUTC != DateTime.MinValue   // old Studies / Device start time not valid
                                            && ARecord.mEventUTC >= row._mRecorderStartUTC && ARecord.mEventUTC <= row._mRecorderEndUTC;
                                        if (bInRange)
                                        {
                                            studyIX = row.mIndex_KEY;
                                            break;  // return latest study with event in recording range
                                        }
                                    }
                                }
                            }
                            if (studyIX > 0)
                            {
                                CProgram.sLogLine("FindStudyByPatient: record R#" + ARecord.mIndex_KEY.ToString() + " " + ADeviceName
                                    + "P " + patientIX.ToString() + " found S" + studyIX.ToString() + " for " + searchStr);
                            }
                            else 
                            {
                                CProgram.sLogLine("FindStudyByPatient: record R#" + ARecord.mIndex_KEY.ToString() + " " + ADeviceName
                                    + "P " + patientIX.ToString() + " " + listStudys.Count.ToString() + " found no active  " + searchStr);
                                CProgram.sAddText(ref ArShowError, "No active study " + searchStr, ", ");

                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Failed FindAddToStudyByActive:record R#" + ARecord.mIndex_KEY.ToString() + " " + ADeviceName
                        + " and test active study S" + _mActiveStudy_IX.ToString(), ex);
                    studyIX = 0;
                    CProgram.sAddText(ref ArShowError, "Failed ex", ", ");
                }
            }

            return studyIX;
        }

        public UInt32 mFindAddToStudyByActive(ref string ArShowError, CRecordMit ARecord, string ADeviceName )
        {
            UInt32 studyIX = 0;

            if (ARecord == null || ARecord.mIndex_KEY == 0)
            {
                CProgram.sLogError("Empty Record!");
            }
            else
            {
                try
                {
                    if (_mActiveStudy_IX == 0)
                    {
                        CProgram.sLogLine("R#" + ARecord.mIndex_KEY.ToString() + " devID: " + ADeviceName
                            + " found device #" + mIndex_KEY.ToString() + " but has no active study!");
                        CProgram.sAddText(ref ArShowError, "No study", ", ");
                    }
                    if( false == mbIsRecording())
                    {
                        CProgram.sLogLine("R#" + ARecord.mIndex_KEY.ToString() + " devID: " + ADeviceName
                            + " found device #" + mIndex_KEY.ToString() + " but device is not recording!");
                        CProgram.sAddText(ref ArShowError, "Not recording", ", ");
                    }
                    else
                    {
                        studyIX = _mActiveStudy_IX;

                        if (_mActiveRefID != null && _mActiveRefID.mbNotEmpty())
                        {
                            string recStr = CProgram.sTrimString(ARecord.mPatientID.mDecrypt().ToUpper());
                            string devStr = CProgram.sTrimString(_mActiveRefID.mDecrypt().ToUpper());

                            if (recStr != devStr)
                            {
                                CProgram.sLogLine("R#" + ARecord.mIndex_KEY.ToString() + " devID: " + ADeviceName
                                    + " found device #" + mIndex_KEY.ToString() + " S" + _mActiveStudy_IX.ToString()
                                    + " but failed test for RefID!(" + recStr + "!=" + devStr+")" );
                                studyIX = 0;
                                CProgram.sAddText(ref ArShowError, "RefID not equal", ", ");
                            }
                        }
                        if (studyIX > 0)
                        {
                            bool bInRange = _mRecorderStartUTC != DateTime.MinValue && _mRecorderEndUTC != DateTime.MinValue   // old Studies / Device start time not valid
                                && ARecord.mEventUTC >= _mRecorderStartUTC && ARecord.mEventUTC <= _mRecorderEndUTC;

                            if (false == bInRange)
                            {
                                CProgram.sLogLine("R#" + ARecord.mIndex_KEY.ToString() + " devID: " + ADeviceName
                                    + " found device #" + mIndex_KEY.ToString() + " S" + _mActiveStudy_IX.ToString()
                                    + " but failed test for recording time! UTC("
                                    + CProgram.sDateTimeToYMDHMS(_mRecorderStartUTC) + "<="
                                    + CProgram.sDateTimeToYMDHMS(ARecord.mEventUTC) + "<="
                                    + CProgram.sDateTimeToYMDHMS(_mRecorderEndUTC) + ")");

                                studyIX = 0;
                                CProgram.sAddText(ref ArShowError, "Not within Rec time", ", ");

                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Failed FindAddToStudyByActive:record R#" + ARecord.mIndex_KEY.ToString() + " " + ADeviceName 
                        + " and test active study S" + _mActiveStudy_IX.ToString(), ex);
                    CProgram.sAddText(ref ArShowError, "Failed ex", ", ");
                    studyIX = 0;
                }
            }
            return studyIX;
        }

        public bool mbLoadCheckFromRecordEB(ref string ArShowError, CRecordMit ARecord, bool AbLoadStudyPatient, 
            ref CStudyInfo ArStudy, ref CPatientInfo ArPatient, bool AbUpdateRecord)
        {
            bool bWriteRefFile = true;
            bool bDoCheckIR = false;
            bool bDoCheckTZone = true;
            bool bAddDeviceRemark = true;

            bool bOk = mbLoadCheckFromRecordNew(ref ArShowError, ARecord, AbLoadStudyPatient, ref ArStudy, ref ArPatient,
                            AbUpdateRecord, bWriteRefFile, bDoCheckIR, bDoCheckTZone, bAddDeviceRemark);

            return bOk;
        }
        public bool mbLoadCheckFromRecordIR(ref string ArShowError, CRecordMit ARecord, bool AbLoadStudyPatient, ref CStudyInfo ArStudy, ref CPatientInfo ArPatient,
                    bool AbUpdateRecord)
        {
            bool bWriteRefFile = true;
            bool bDoCheckIR = true;
            bool bDoCheckTZone = true;
            bool bAddDeviceRemark = true;

            bool bOk = mbLoadCheckFromRecordNew(ref ArShowError, ARecord, AbLoadStudyPatient, ref ArStudy, ref ArPatient,
                            AbUpdateRecord, bWriteRefFile, bDoCheckIR, bDoCheckTZone, bAddDeviceRemark);

            return bOk;
        }

        public bool mbLoadCheckFromRecordNew(ref string ArShowError, CRecordMit ARecord, bool AbLoadStudyPatient, 
                ref CStudyInfo ArStudy, ref CPatientInfo ArPatient,
                bool AbUpdateRecord, bool AbWriteRefFile, bool AbDoCheckIR, bool AbDoCheckTZone, 
                bool AbAddDeviceRemark)
        {
            bool bOk = false;

            if (ARecord == null /*&& ARecord.mIndex_KEY == 0*/)
            {
                CProgram.sLogError("Empty Record!");
            }
            else
            {
                string deviceName = "";

                try
                {
                    if (false == mbLoadDeviceFromRecord(ref deviceName, AbUpdateRecord, ARecord, AbDoCheckIR, AbDoCheckTZone, AbAddDeviceRemark)
                        || ARecord.mDevice_IX == 0 || mIndex_KEY == 0)
                    {
                        CProgram.sLogError("R#" + ARecord.mIndex_KEY.ToString() + " failed load device " + ARecord.mDeviceID + " " + ARecord.mDevice_IX.ToString() + "!" + mIndex_KEY.ToString());
                        CProgram.sAddText(ref ArShowError, "Failed load " + ARecord.mDeviceID, ", ");

                    }
                    else
                    {
                        if (ARecord.mStudy_IX > 0)
                        {
                            // already assigned to study
                            bOk = mbLoadStudyPatientFromRecord(ref ArShowError, ARecord, AbLoadStudyPatient, deviceName, ref ArStudy, ref ArPatient);
                        }
                        else
                        {
                            // determin study depending on device add to study
                            UInt32 studyIX = 0;

                            if (ArStudy != null && ArStudy.mIndex_KEY > 0)
                            {
                                studyIX = ArStudy.mIndex_KEY;   // study given
                            }
                            else
                            {
                                if(_mAddStudyMode == (UInt16)DAddStudyModeEnum.Manual )
                                {
                                    studyIX = 0;    // do not add to study
                                    bOk = true;
                                }
                                else if (_mAddStudyMode == (UInt16)DAddStudyModeEnum.FindPatientID)
                                {
                                    studyIX = mFindAddToStudyByPatient(ref ArShowError, false, ARecord, deviceName);
                                }
                                else if (_mAddStudyMode == (UInt16)DAddStudyModeEnum.FindUniqueID)
                                {
                                    studyIX = mFindAddToStudyByPatient(ref ArShowError, true, ARecord, deviceName);

                                }
                                /*                                else if (_mAddStudyMode == (UInt16)DAddStudyModeEnum.FindStudy)
                                                                {

                                                                }
                                                                else if (_mAddStudyMode == (UInt16)DAddStudyModeEnum.FindStudyInitialsYear)
                                                                {

                                                                }
                                */
                                else // DAddStudyModeEnum.ActiveStudy
                                {
                                    studyIX = mFindAddToStudyByActive(ref ArShowError, ARecord, deviceName);
                                }
                            }

                            if (studyIX > 0)
                            {
                                bOk = mbAddRecordToStudy(ref ArShowError, ARecord, "", deviceName, studyIX, out ArStudy, out ArPatient, true, true);
                            }
                            else
                            {
                                bOk = true;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Failed load device " + deviceName + " and find study S" + ARecord.mStudy_IX.ToString() + " from record R#" + ARecord.mIndex_KEY.ToString(), ex);
                    CProgram.sAddText(ref ArShowError, "Failed ex", ", ");
                    bOk = false;
                }
            }
            return bOk;
        }

        public bool mbLoadCheckFromRecordOld(bool AbForceAddStudy, CRecordMit ARecord, bool AbLoadStudyPatient, ref CStudyInfo ArStudy, ref CPatientInfo ArPatient,
            bool AbUpdateRecord, bool AbWriteRefFile)
        {
            if (ARecord == null || ARecord.mIndex_KEY == 0)
            {
                return false;
            }
            bool bOk = false;

            string deviceIxStr = "";
            string altDeviceStr = "";
            string remSnr = "";
            bool bUpdRec = AbUpdateRecord;
            bool bUpdDeviceID = false;
            bool bAddStudy = false;
            bool bAddPatient = false;
            string altRem = "";
            UInt16 nrRecInStudy = 0;
            bool bForceAddStudy = AbForceAddStudy;

            // check & load device
            if (ARecord.mDevice_IX == 0)
            {
                // find device
                int nSerial = mFindLoadSerial(0, ARecord.mDeviceID, 0); // loads device

                if (nSerial == 0)
                {
                    altDeviceStr = ARecord.mGetAlternateSerialNr();
                    if (altDeviceStr != null && altDeviceStr.Length > 0)
                    {
                        nSerial = mFindLoadSerial(0, altDeviceStr, 0);// needed for tz
                        if (nSerial > 0)
                        {
                            remSnr = "->" + altDeviceStr;
                            altRem = ARecord.mDeviceID + "=" + altDeviceStr;
                            ARecord.mDeviceID = altDeviceStr;
                            bUpdRec = true;
                            bUpdDeviceID = true;
                        }
                    }
                }
                deviceIxStr = mIndex_KEY.ToString();

                if (nSerial == 0 || mIndex_KEY == 0)
                {
                    CProgram.sLogLine("R#" + ARecord.mIndex_KEY.ToString() + " Failed to find deviceID: "
                        + ARecord.mDeviceID + remSnr);
                    // can not find recording device
                }
                else
                {
                    string recRemark = "";
                    // device found and loaded
                    if (_mRemarkLabel != null && _mRemarkLabel.Length > 0 )
                    {
                        recRemark = mGetAddStudyModeChar() + "D:" + _mRemarkLabel; 
                    }
                    else if(_mAddStudyMode != (UInt16)DAddStudyModeEnum.ActiveStudy)
                    {
                        recRemark = mGetAddStudyModeChar() + "D:";
                    }
                    if ( _mIReaderNr > 0 )
                    {
                        // checking if we are the right IReader
                        UInt16 irNr = CProgram.sGetProgNr();
                        if( irNr != _mIReaderNr)
                        {
                            if (recRemark != null && recRemark.Length > 0)
                            {
                                recRemark += "\r\n";
                            }
                            recRemark += "IR[" + irNr.ToString() + "] wrong " + _mIReaderNr.ToString() + "!";
                            CProgram.sLogWarning( "IR[" + irNr.ToString() + "] wrong " + _mIReaderNr.ToString()
                                + " check device " + _mDeviceSerialNr + "!" );
                        }
                    }
                    // no device in Record -> first load -> do Time Zone Correction
                    string timeRemark = "";
                    if (mbDoTimeZoneCorrection(out timeRemark, ARecord, false))
                    {
                        bUpdRec = true;
                    }
                    if (timeRemark != null && timeRemark.Length > 0)
                    {
                        if (recRemark != null && recRemark.Length > 0)
                        {
                            recRemark += "\r\n";
                        }
                        recRemark += timeRemark;
                    }
                     if (recRemark != null && recRemark.Length > 0)
                    {
                        ARecord.mRecRemark.mbEncrypt(ARecord.mRecRemark.mDecrypt() + "\r\n"+ recRemark);
                        bUpdRec = true;
                    }
                    ARecord.mDevice_IX = mIndex_KEY;    // update device in record
                    bUpdRec = mIndex_KEY > 0;
                    if (sbModelFromDeviceInfo)
                    {
                        string modelLabel = CDeviceModel.sGetDeviceModelLabel(_mDeviceModel_IX);
                        if (modelLabel.Length > 0)
                        {
                            ARecord.mDeviceModel = modelLabel;
                        }
                        else
                        {
                            CProgram.sLogError("Failed get device model #" + _mDeviceModel_IX
                                + " for device " + ARecord.mDeviceID + " #" + mIndex_KEY
                                + " R" + ARecord.mIndex_KEY + " using " + ARecord.mDeviceModel);
                        }
                    }

                }
            }
            else if (mIndex_KEY == ARecord.mDevice_IX)
            {
                deviceIxStr = ARecord.mDevice_IX.ToString();
            }
            else
            {
                deviceIxStr = ARecord.mDevice_IX.ToString();
                // load known device
                if (false == AbLoadStudyPatient)
                {
                    CProgram.sLogLine("R#" + ARecord.mIndex_KEY.ToString() + " devID: " + ARecord.mDeviceID + " invalid need to load deviceID: " + ARecord.mDeviceID);
                    return false;   // can not load recording device
                }
                if (false == AbLoadStudyPatient || false == mbDoSqlSelectIndex(ARecord.mDevice_IX, mMaskValid))
                {
                    CProgram.sLogLine("R#" + ARecord.mIndex_KEY.ToString() + " devID: " + ARecord.mDeviceID + " Failed to load deviceID: " + ARecord.mDeviceID);
                    mIndex_KEY = 0;
                    return false;   // can not load recording device
                }
            }
            // check & load device done

            bOk = mIndex_KEY > 0;   // should be true because device is loaded

            if (bOk)
            {
                //checked device add to study 
                if (ARecord.mStudy_IX > 0)
                {
                    bAddStudy = false;  // study already present
                }
                else
                {
                    // try to assign study by checking if device study is applicable
                    bAddStudy = true;   // mbSameRefID( CEncryptedString ARefID )

                    if (_mAddStudyMode != (UInt16)DAddStudyModeEnum.ActiveStudy)
                    {
                        bAddStudy = bForceAddStudy;
                    }
                    else
                    {
                        if (_mActiveStudy_IX == 0)
                        {
                            CProgram.sLogLine("R#" + ARecord.mIndex_KEY.ToString() + " devID: " + ARecord.mDeviceID + " found device #" + deviceIxStr + " but failed device study not assigned!");
                            bAddStudy = false;
                        }
                        else
                        if (_mActiveRefID != null && _mActiveRefID.mbNotEmpty() && false == bForceAddStudy)
                        {
                            string recStr = CProgram.sTrimString(ARecord.mPatientID.mDecrypt().ToUpper());
                            string devStr = CProgram.sTrimString(_mActiveRefID.mDecrypt().ToUpper());

                            if (recStr != devStr)
                            {
                                CProgram.sLogLine("R#" + ARecord.mIndex_KEY.ToString() + " devID: " + ARecord.mDeviceID + " found device #" + deviceIxStr + " but failed test for RefID!");
                                bAddStudy = false;
                            }
                        }

                        if (bAddStudy)
                        {
                            bool bInRange = _mRecorderStartUTC != DateTime.MinValue && _mRecorderEndUTC != DateTime.MinValue   // old Studies / Device start time not valid
                                && ARecord.mEventUTC >= _mRecorderStartUTC && ARecord.mEventUTC <= _mRecorderEndUTC;

                            if (false == bInRange)
                            {
                                CProgram.sLogLine("R#" + ARecord.mIndex_KEY.ToString() + " devID: " + ARecord.mDeviceID + " found device #" + deviceIxStr + " but failed test for recording time range!");
                                bAddStudy = false;
                            }
                        }
                    }
                    // add optional patient test
                    if (bAddStudy)
                    {
                        ARecord.mStudy_IX = _mActiveStudy_IX;   // Add study number to record
                        bUpdRec |= _mActiveStudy_IX > 0;
                    }
                }

                // load study data
                if (ARecord.mStudy_IX > 0)
                {
                    if (ArStudy == null && AbLoadStudyPatient) ArStudy = new CStudyInfo(mGetSqlConnection());

                    if (ArStudy != null)
                    {
                        if (ArStudy.mIndex_KEY != ARecord.mStudy_IX && AbLoadStudyPatient)
                        {
                            ArStudy.mbDoSqlSelectIndex(ARecord.mStudy_IX, ArStudy.mMaskValid); // no tested if ok old code
                        }

                        if (ArStudy.mIndex_KEY > 0 && ArStudy.mIndex_KEY == ARecord.mStudy_IX)
                        {
                            if (ARecord.mSeqNrInStudy == 0)
                            {
                                ARecord.mSeqNrInStudy = ++ArStudy._mNrRecords;

                                UInt64 saveMask = CSqlDataTableRow.sGetMask((UInt16)DStudyInfoVars.NrRecords);

                                bUpdRec |= ArStudy.mbDoSqlUpdate(saveMask, true);  // update study nrRecords
                            }
                            if (bAddStudy)
                            {
                                if (ArStudy._mStudyRemarkLabel != null && ArStudy._mStudyRemarkLabel.Length > 0)
                                {
                                    ARecord.mRecRemark.mbEncrypt(ARecord.mRecRemark.mDecrypt() + "\r\nS: " + ArStudy._mStudyRemarkLabel);
                                }
                                // 20190227 copy trial and study permisions to record for use in selecting records
                                ARecord._mStudyTrial_IX = ArStudy._mTrial_IX;
                                ARecord._mStudyClient_IX = ArStudy._mClient_IX;
                                if (ArStudy._mStudyPermissions != null)
                                {
                                    ArStudy._mStudyPermissions.mCopyTo(ref ARecord._mStudyPermissions);
                                }
                            }
                        }
                        nrRecInStudy = ArStudy._mNrRecords;
                    }
                }
                // check study present to find patient
                if (ARecord.mPatient_IX == 0 && mIndex_KEY > 0 && ArStudy != null && ARecord.mStudy_IX > 0 && ARecord.mStudy_IX == ArStudy.mIndex_KEY)
                {
                    ARecord.mPatient_IX = ArStudy._mPatient_IX;

                    if (ArStudy._mPatient_IX > 0)
                    {
                        bAddPatient = true;
                        bUpdRec = true;
                    }
                }
                // load patient data
                if (ARecord.mPatient_IX > 0)
                {
                    if (ArPatient == null && AbLoadStudyPatient) ArPatient = new CPatientInfo(mGetSqlConnection());

                    if (ArPatient != null)
                    {
                        if (ArPatient.mIndex_KEY != ARecord.mPatient_IX && AbLoadStudyPatient)
                        {
                            ArPatient.mbDoSqlSelectIndex(ARecord.mPatient_IX, ArPatient.mMaskValid);
                        }

                        if (ArPatient.mIndex_KEY > 0 && ArPatient.mIndex_KEY == ARecord.mPatient_IX)
                        {
                            if (bAddPatient)
                            {
                                if (ArPatient._mRemarkLabel != null && ArPatient._mRemarkLabel.Length > 0)
                                {
                                    ARecord.mRecRemark.mbEncrypt(ARecord.mRecRemark.mDecrypt() + "\r\nP: " + ArPatient._mRemarkLabel);
                                }
                            }
                        }
                    }
                }
            }
            if (bUpdRec)
            {
                UInt64 saveMask = ARecord.mMaskValid;
                UInt64 forcePkMask = 0;


                if (ArPatient != null && ArPatient.mIndex_KEY > 0)
                {
                    // force use of patient name from study / patient info
                    ARecord.mPatientBirthDate = ArPatient._mPatientDateOfBirth;
                    ARecord.mPatientBirthYear = ArPatient._mPatientBirthYear;
                    ARecord.mPatientGender = ArPatient._mPatientGender_IX;
                    mCheckSetPatientID(ARecord, ArPatient.mGetPatientSocIDValue(false), ref altRem); // set patientID and add old refID to remark
                    //ARecord.mPatientID.mbEncrypt(ArPatient.mGetPatientSocIDValue(false)); // patientID or patientSocID()
                    ARecord.mPatientTotalName.mbEncrypt(ArPatient.mGetFullName());
                }
                if (altRem != null && altRem.Length > 0)
                {
                    ARecord.mRecRemark.mbEncrypt(ARecord.mRecRemark.mDecrypt() + "\r\n" + altRem);
                }
                if (bUpdDeviceID)
                {   // update DeviceID

                    forcePkMask |= CSqlDataTableRow.sGetMask((UInt16)DRecordMitVars.DeviceID);
                }
                bOk = ARecord.mbDoSqlUpdate(saveMask, true, forcePkMask);  // update record to contain new found info
                string done = bOk ? "Set " : "!Set failed ";

                CProgram.sLogLine("R#" + ARecord.mIndex_KEY.ToString() + done + "Study #" + ARecord.mStudy_IX.ToString()
                        + "." + ARecord.mSeqNrInStudy.ToString() + " /" + nrRecInStudy.ToString()
                        + " P" + ARecord.mPatient_IX.ToString()
                        + " T" + ARecord._mStudyTrial_IX.ToString()
                        + " C" + ARecord._mStudyClient_IX.ToString()
                        + " for deviceID: " + _mDeviceSerialNr + remSnr);
            }
            if (bUpdRec || AbWriteRefFile)
            {
                string recordingDir = CDvtmsData.sGetRecordingCaseDir(ARecord);

                ARecord.mbWriteRefFile(recordingDir);
            }
            return bOk;
        }

        static public string sGetStateString(UInt16 AState)
        {
            if (AState < (UInt16)DDeviceState.NrStates)
            {
                switch ((DDeviceState)AState)
                {
                    case DDeviceState.New: return "New";
                    case DDeviceState.Available: return "Available";
                    case DDeviceState.Assigned: return "Assigned";
                    case DDeviceState.Recording: return "Recording";
                    case DDeviceState.Returned: return "Returned";
                    case DDeviceState.InService: return "In Service";
                    case DDeviceState.Defective: return "Defective";
                    case DDeviceState.Unavailable: return "Unavailable";
                    case DDeviceState.Intransit: return "In Transit";
                }
            }
            return "?DeviceState?";
        }
        public static UInt16 sGetStateIndex(string AStateString)
        {
            UInt16 state = 0;

            if (AStateString != null && AStateString.Length > 0)
            {

            }
            for (UInt16 i = 1; i < (UInt16)DDeviceState.NrStates; ++i)
            {
                if (AStateString == sGetStateString(i))
                {
                    state = i;
                    break;
                }
            }
            return state;
        }

        public static string sGetAddStudyModeMctLabel(UInt16 AEnum)
        {
            string mctText = "";

            if (AEnum < (UInt16)DAddStudyModeEnum.NrEnums)
            {
                switch ((DAddStudyModeEnum)AEnum)
                {
                    case DAddStudyModeEnum.ActiveStudy:    // old default use the Device Active Study
                        break;
                    case DAddStudyModeEnum.Manual:              // add record manualy in Eventboard
                        mctText = "(No MCT)";
                        break;
                    case DAddStudyModeEnum.FindPatientID:      // search patient with the PatientID (=1), find study that is still active (start + nrDays), 1 then add rec to study
                        mctText = "(No MCT)";
                        break;
                    case DAddStudyModeEnum.FindUniqueID:       // search patient with the UniqueID (=1), find study that is still active (start + nrDays), 1 then add rec to study
                        mctText = "(No MCT)";
                        break;
                    case DAddStudyModeEnum.FindStudy:          // patientID = S123456, open study S123456 and check still active (start + nrDays), add rec to study
                                                               //                        s = "Find Study S#";
                                //                        mctText = "(No MCT)";
                        break;
                    case DAddStudyModeEnum.FindStudyInitialsYear: // patientID = S123456FL1987, open study and check still active (start + nrDays), 
                                                                  // check patient initials and birth year, add rec to study
                                                                  //s = "Find Study S# Initials Year";
                                //                        mctText = "(No MCT)";
                        break;
                }
            }
            return mctText;
        }
        public static string sGetAddStudyModeLabel(UInt16 AEnum, bool AbValidOnly)
        {
            string s = "?" + AEnum.ToString();

            if (AEnum < (UInt16)DAddStudyModeEnum.NrEnums)
            {
                switch ((DAddStudyModeEnum)AEnum)
                {
                    case DAddStudyModeEnum.ActiveStudy:    // old default use the Device Active Study
                        s = "Active Study";
                        break;
                    case DAddStudyModeEnum.Manual:              // add record manualy in Eventboard
                        s = "Manual";
                        break;
                    case DAddStudyModeEnum.FindPatientID:      // search patient with the PatientID (=1), find study that is still active (start + nrDays), 1 then add rec to study
                        s = "PatientID";
                        break;
                    case DAddStudyModeEnum.FindUniqueID:       // search patient with the UniqueID (=1), find study that is still active (start + nrDays), 1 then add rec to study
                        s = "UniqueID";
                        break;
                    case DAddStudyModeEnum.FindStudy:          // patientID = S123456, open study S123456 and check still active (start + nrDays), add rec to study
                                                               //                        s = "Find Study S#";
                        s = AbValidOnly ? "" : "Find Study"; // off
                        break;
                    case DAddStudyModeEnum.FindStudyInitialsYear: // patientID = S123456FL1987, open study and check still active (start + nrDays), 
                                                                  // check patient initials and birth year, add rec to study
                                                                  //s = "Find Study S# Initials Year";
                        s = AbValidOnly ? "" : "Find SIY"; // off
                        break;
                }
            }
            return s;
        }
        public string mGetAddStudyModeLabel(bool AbValidOnly)
        {
            return sGetAddStudyModeLabel(_mAddStudyMode, AbValidOnly);
        }
        public string mGetAddStudyModeMctLabel()
        {
            return sGetAddStudyModeMctLabel(_mAddStudyMode);
        }
        public static string sGetAddStudyModeChar(UInt16 AEnum, bool AbShowActive)
        {
            string s = "?" + AEnum.ToString();

            if (AEnum < (UInt16)DAddStudyModeEnum.NrEnums)
            {
                switch ((DAddStudyModeEnum)AEnum)
                {
                    case DAddStudyModeEnum.ActiveStudy:    // old default use the Device Active Study
                        s = AbShowActive ? "a" : "";
                        break;
                    case DAddStudyModeEnum.Manual:              // add record manualy in Eventboard
                        s = "m";
                        break;
                    case DAddStudyModeEnum.FindPatientID:      // search patient with the PatientID (=1), find study that is still active (start + nrDays), 1 then add rec to study
                        s = "i";
                        break;
                    case DAddStudyModeEnum.FindUniqueID:       // search patient with the UniqueID (=1), find study that is still active (start + nrDays), 1 then add rec to study
                        s = "u";
                        break;
                    case DAddStudyModeEnum.FindStudy:          // patientID = S123456, open study S123456 and check still active (start + nrDays), add rec to study
                                                               //                        s = "Find Study S#";
                        s = "f"; 
                        break;
                    case DAddStudyModeEnum.FindStudyInitialsYear: // patientID = S123456FL1987, open study and check still active (start + nrDays), 
                                                                  // check patient initials and birth year, add rec to study
                                                                  //s = "Find Study S# Initials Year";
                        s = "y"; 
                        break;
                }
            }
            return s;
        }
        public string mGetAddStudyModeChar( bool AbShowActive = true)
        {
            return sGetAddStudyModeChar(_mAddStudyMode, AbShowActive);
        }
        public string mGetAddStudyModeRefID()
        {
            string refID = "";

            if (_mAddStudyMode != (int)DAddStudyModeEnum.ActiveStudy)
            {
                refID = "*.*" + mGetAddStudyModeChar();
            }

            return refID;
        }

        public Color mCheckColorAddStudyModeRefID()
        {
            Color color = SystemColors.WindowText;

            if (_mAddStudyMode == (int)DAddStudyModeEnum.ActiveStudy)
            {
                if (_mRefIdMode == (int)DRefIDModeEnum.None)
                {
                    if (false == _mActiveRefID.mbIsEmpty())
                    {
                        color = Color.Orange;
                    }
                }
            }
            else
            {
                string refID = _mActiveRefID.mDecrypt();
                if (refID != mGetAddStudyModeRefID())
                {
                    color = Color.Red;
                }
            }
            return color;
        }
        public bool mbCheckOkAddStudyModeRefID()
        {
            bool bOk = true;

            if (_mAddStudyMode == (int)DAddStudyModeEnum.ActiveStudy)
            {
                if (_mRefIdMode == (int)DRefIDModeEnum.None)
                {
                    if (false == _mActiveRefID.mbIsEmpty())
                    {
                        bOk = false;
                    }
                }
            }
            else
            {
                string refID = _mActiveRefID.mDecrypt();
                if (refID != mGetAddStudyModeRefID())
                {
                    bOk = false;
                }
            }
            return bOk;
        }

        public bool mbChecktAddStudyModeRefID(out string ArNewRefID, bool bAskChange)
        {
            bool bOk = false;
            string newRefID = "";

            if (_mAddStudyMode == (int)DAddStudyModeEnum.ActiveStudy)
            {
                newRefID = _mActiveRefID.mDecrypt();

                if (newRefID != null && newRefID.StartsWith("*"))
                {
                    newRefID = "";
                    bOk = false;
                }
                else
                {
                    if (mbIsRecording())
                    {
                        // do not change
                        bOk = true;
                    }
                    else if (_mRefIdMode == (int)DRefIDModeEnum.None)
                    {
                        newRefID = "";
                        bOk = _mActiveRefID.mbIsEmpty();
                    }
                    else
                    {
                        bOk = true;
                    }
                }
            }
            else
            {
                if (mbIsRecording())
                {
                    newRefID = _mActiveRefID.mDecrypt();
                    bOk = true;
                }
                else
                {
                    newRefID = mGetAddStudyModeRefID(); // not recording just change to correct value
                    _mActiveRefID.mbEncrypt(newRefID);
                    bOk = true;
                }
            }

            if (false == bOk && bAskChange)
            {
                string oldRefID = _mActiveRefID.mDecrypt();
                string title = "RefID " + _mDeviceSerialNr;
                bool bChange = false == mbIsRecording();

                if (_mActiveStudy_IX > 0)
                {
                    title += " S" + _mActiveStudy_IX;
                }
                if (CProgram.sbReqBool(title, "RefID not as expected, change '" + oldRefID + "' to '" + newRefID + "'", ref bChange))
                {
                    if (bChange)
                    {
                        _mActiveRefID.mbEncrypt(newRefID);
                    }
                    bOk = true;
                }
            }
            ArNewRefID = newRefID;
            return bOk;
        }


        public static void sFillAddStudyModeComboBox(ComboBox AComboBox, UInt16 AValue)
        {
            if (AComboBox != null)
            {
                string cursor = "?";
                UInt16 last = (UInt16)(DAddStudyModeEnum.NrEnums - 1);

                AComboBox.Items.Clear();
                for (UInt16 i = 0; i <= last; ++i)
                {
                    string s = sGetAddStudyModeLabel(i, true);

                    if (s != null && s.Length > 0) // skip empty values
                    {
                        AComboBox.Items.Add(s);

                        if (i == AValue)
                        {
                            cursor = s;     // set cursor
                        }
                    }
                }
                AComboBox.Text = cursor;
            }
        }
        public static UInt16 sReadAddStudyModeComboBox(ComboBox AComboBox, UInt16 ADefaultValue = 0)
        {
            UInt16 value = ADefaultValue;

            if (AComboBox != null)
            {
                string cursor = AComboBox.Text;
                UInt16 last = (UInt16)(DAddStudyModeEnum.NrEnums - 1);

                for (UInt16 i = 0; i <= last; ++i)
                {
                    string s = sGetAddStudyModeLabel(i, true);

                    if (s != null && s.Length > 0) // skip empty values
                    {
                        if (cursor == s)
                        {
                            value = i;
                        }
                    }
                }
            }
            return value;
        }
        public static string sGetRefIDModeLabel(UInt16 AEnum)
        {
            string s = "?" + AEnum.ToString();

            if (AEnum < (UInt16)DRefIDModeEnum.NrEnums)
            {
                switch ((DRefIDModeEnum)AEnum)
                {
                    case DRefIDModeEnum.NotActive:
                        s = "---";
                        break;
                    case DRefIDModeEnum.None:
                        s = "None";
                        break;
                    case DRefIDModeEnum.PatientID:
                        s = "Patient ID";
                        break;
                    case DRefIDModeEnum.UniqueID:
                        s = "Unique ID";
                        break;
                    case DRefIDModeEnum.Other:
                        s = "Other";
                        break;
                    case DRefIDModeEnum.Study:              // S123456
                        s = ""; // disabled "Study number";
                        break;
                    case DRefIDModeEnum.StudyInitialsYear:   // S123456FL1987
                        s = ""; // disabled "Study Initials Year";
                        break;
                }
            }
            return s;
        }
        public static void sFillRefIDModeComboBox(ComboBox AComboBox, UInt16 AValue)
        {
            if (AComboBox != null)
            {
                string cursor = "?";
                UInt16 last = (UInt16)(DRefIDModeEnum.NrEnums - 1);

                AComboBox.Items.Clear();
                for (UInt16 i = 0; i <= last; ++i)
                {
                    string s = sGetRefIDModeLabel(i);

                    if (s != null && s.Length > 0) // skip empty values
                    {
                        AComboBox.Items.Add(s);

                        if (i == AValue)
                        {
                            cursor = s;     // set cursor
                        }
                    }
                }
                AComboBox.Text = cursor;
            }
        }
        public static UInt16 sReadRefIDModeComboBox(ComboBox AComboBox, UInt16 ADefaultValue = 0)
        {
            UInt16 value = ADefaultValue;

            if (AComboBox != null)
            {
                string cursor = AComboBox.Text;
                UInt16 last = (UInt16)(DRefIDModeEnum.NrEnums - 1);

                for (UInt16 i = 0; i <= last; ++i)
                {
                    string s = sGetRefIDModeLabel(i);

                    if (s != null && s.Length > 0) // skip empty values
                    {
                        if (cursor == s)
                        {
                            value = i;
                        }
                    }
                }
            }
            return value;
        }
        public static string sGetTimeZoneModeLabel(UInt16 AEnum)
        {
            string s = "?" + AEnum.ToString();  

            if (AEnum < (UInt16)DTimeZoneModeEnum.NrEnums)
            { 
                switch ((DTimeZoneModeEnum)AEnum)
                {
                    case DTimeZoneModeEnum.NotActive:  
                        s = "---";
                        break;
                    case DTimeZoneModeEnum.UsePC:        // no TimeZone read correction: TimeZone = default value
                        s = "Use PC";
                        break;
                    case DTimeZoneModeEnum.UseDefault:   // correct TimeZone = default value (+correct UTC with difference to get same device event time)
                        s = "Use Default (UTC-Δ)";
                        break;
                    case DTimeZoneModeEnum.UseOffset:    // correct TimeZone by TimeZoneOffsetMin (+correct UTC with difference to get same device event time)
                        s = "Use Offset (UTC-Δ)";
                        break;
                    case DTimeZoneModeEnum.SetDefault:   // set TimeZone = default value (UTC in file)
                        s = "Set Default (UTC-file)";
                        break;
                    case DTimeZoneModeEnum.SetOffset:    // correct TimeZone by TimeZoneOffsetMin (UTC in file)
                        s = "Set Offset (UTC-file)";
                        break;
                }
            }
            return s;
        }
        public static void sFillTimeZoneModeComboBox( ComboBox AComboBox, UInt16 AValue)
        {
            if( AComboBox != null )
            {
                string cursor = "?";
                UInt16 last = (UInt16)(DTimeZoneModeEnum.NrEnums - 1);

                AComboBox.Items.Clear();
                for( UInt16 i = 0; i <= last; ++i)
                {
                    string s = sGetTimeZoneModeLabel(i);

                    if( s != null && s.Length > 0 ) // skip empty values
                    {
                        AComboBox.Items.Add(s);

                        if( i == AValue)
                        {
                            cursor = s;     // set cursor
                        }
                    }
                }
                AComboBox.Text = cursor;
            }
        }
        public static UInt16 sReadTimeZoneModeComboBox(ComboBox AComboBox, UInt16 ADefaultValue = 0)
        {
            UInt16 value = ADefaultValue;

            if (AComboBox != null)
            {
                string cursor = AComboBox.Text;
                UInt16 last = (UInt16)(DTimeZoneModeEnum.NrEnums - 1);

                for (UInt16 i = 0; i <= last; ++i)
                {
                    string s = sGetTimeZoneModeLabel(i);

                    if (s != null && s.Length > 0) // skip empty values
                    {
                        if( cursor == s )
                        {
                            value = i;
                        }
                    }
                }
            }
            return value;
        }

        public static string sGetNextStateModeLabel(UInt16 AEnum)
        {
            string s = "?" + AEnum.ToString();

            if (AEnum < (UInt16)DNextStateModeEnum.NrEnums)
            {
                switch ((DNextStateModeEnum)AEnum)
                {
                    case DNextStateModeEnum.Normal:             // normal
                        s = "---";
                        break;
                    case DNextStateModeEnum.Locked:             // lock disables all next state buttons (must be taken off locked before continue
                        s = "Locked";
                        break;
                    case DNextStateModeEnum.ToService:          // next state device must be In Service
                        s = "In Service";
                        break;
                    case DNextStateModeEnum.CheckDevice:        // Device must be checked next state device must be In Service
                        s = "Check Device";
                        break;
                    case DNextStateModeEnum.CollectData:        // collect the data before assigning to next patient
                        s = "Collect Data";
                        break;
                    case DNextStateModeEnum.UpdateSettings:     // update must be performed before assigning to next patient
                        s = "Update Settings";
                        break;
                    case DNextStateModeEnum.UpdateFirmware:     // update firmware must be performed before assigning to next patient
                        s = "Update Firmware";
                        break;
                    case DNextStateModeEnum.ToManufacturer:     // send device to manufacturer
                        s = "To Manufacturer";
                        break;
                }
            }
            return s;
        }
        public static void sFillNextStateModeComboBox(ComboBox AComboBox, UInt16 AValue)
        {
            if (AComboBox != null)
            {
                string cursor = "?";
                UInt16 last = (UInt16)(DNextStateModeEnum.NrEnums - 1);

                AComboBox.Items.Clear();
                for (UInt16 i = 0; i <= last; ++i)
                {
                    string s = sGetNextStateModeLabel(i);

                    if (s != null && s.Length > 0) // skip empty values
                    {
                        AComboBox.Items.Add(s);

                        if (i == AValue)
                        {
                            cursor = s;     // set cursor
                        }
                    }
                }
                AComboBox.Text = cursor;
            }
        }
        public static UInt16 sReadNextStateModeComboBox(ComboBox AComboBox, UInt16 ADefaultValue = 0)
        {
            UInt16 value = ADefaultValue;

            if (AComboBox != null)
            {
                string cursor = AComboBox.Text;
                UInt16 last = (UInt16)(DNextStateModeEnum.NrEnums - 1);

                for (UInt16 i = 0; i <= last; ++i)
                {
                    string s = sGetNextStateModeLabel(i);

                    if (s != null && s.Length > 0) // skip empty values
                    {
                        if (cursor == s)
                        {
                            value = i;
                        }
                    }
                }
            }
            return value;
        }
        public static bool sbAvailNextState(UInt16 ANextStateMode, UInt16 ACurrentState, DateTime AFirstServiceDT, DateTime ANextServiceDT, DDeviceState ANewState)   // toDo
        {
            bool bAvail = false;

            if (ANextStateMode < (UInt16)DNextStateModeEnum.NrEnums 
                && ACurrentState < (UInt16)DDeviceState.NrStates
                && ANewState < DDeviceState.NrStates)
            {
                DDeviceState currentState = (DDeviceState)ACurrentState;
                DNextStateModeEnum nextStateMode = (DNextStateModeEnum)ANextStateMode;
                bool bDoService = AFirstServiceDT != DateTime.MinValue && AFirstServiceDT != DateTime.MaxValue
                    && ANextServiceDT > AFirstServiceDT
                    && ANextServiceDT <= DateTime.Now.AddHours(-24);
                bool bRecording = false;
                bool bNextSetAvail = false;

                bAvail = true;

                switch (currentState)
                {
                    case DDeviceState.New:
                        bNextSetAvail = true;
                        break;
                    case DDeviceState.Available:
                        bNextSetAvail = true;
                        break;
                    case DDeviceState.Assigned:
                        bRecording = true;
                        break;
                    case DDeviceState.Recording:
                        bRecording = true;
                        break;
                    case DDeviceState.Returned:
                        bNextSetAvail = true;
                        bRecording = true;
                        break;
                    case DDeviceState.InService:
                        bNextSetAvail = true;
                        return true;        // all buttons available
//                        bAvail = true;
                        break;
                    case DDeviceState.Defective:
                        bDoService = true;
                        bAvail = false;
                        break;
                    case DDeviceState.Unavailable:
                        bDoService = true;
                        bAvail = false;
                        break;
                    case DDeviceState.Intransit:
                        bRecording = true;
                        break;

                }

                // reassign same functional modes
                switch (nextStateMode)
                {
                    case DNextStateModeEnum.Normal:             // normal
                        break;
                    case DNextStateModeEnum.Locked:             // lock disables all next state buttons (must be taken off locked before continue
                        break;
                    case DNextStateModeEnum.ToService:          // next state device must be In Service
                        break;
                    case DNextStateModeEnum.CheckDevice:        // Device must be checked next state device must be In Service
                        break;
                    case DNextStateModeEnum.CollectData:        // collect the data before assigning to next patient
                        break;
                    case DNextStateModeEnum.UpdateSettings:     // update must be performed before assigning to next patient
                        nextStateMode = DNextStateModeEnum.ToService;
                        break;
                    case DNextStateModeEnum.UpdateFirmware:     // update firmware must be performed before assigning to next patient
                        nextStateMode = DNextStateModeEnum.ToService;
                        break;
                }
                //   determin availability
                switch (nextStateMode)
                {
                    case DNextStateModeEnum.Normal:             // normal
                        break;
                    case DNextStateModeEnum.Locked:             // lock disables all next state buttons (must be taken off locked before continue                       
                        bAvail = false;
                        return false;
                        break;
                    case DNextStateModeEnum.ToService:          // next state device must be In Service
                        bDoService = true;
                        break;
                    case DNextStateModeEnum.CheckDevice:        // Device must be checked next state device must be In Service
                        bDoService = true;
                        break;
                    case DNextStateModeEnum.CollectData:        // collect the data before assigning to next patient
                        bDoService = true;
                        break;
                    case DNextStateModeEnum.UpdateSettings:     // update must be performed before assigning to next patient
                        bDoService = true;
                        break;
                    case DNextStateModeEnum.UpdateFirmware:     // update firmware must be performed before assigning to next patient
                        bDoService = true;
                        break;
                }
                switch(ANewState)
                {
                    case DDeviceState.New:
                        bAvail = false;
                        break;
                    case DDeviceState.Available:
                        bAvail = false == bDoService
                            && bNextSetAvail;
                        break;
                    case DDeviceState.Assigned:
                        bAvail = false == bDoService;
                        break;
                    case DDeviceState.Recording:
                        bAvail = bRecording;
                        break;
                    case DDeviceState.Returned:
                        bAvail = bRecording;
                        break;
                    case DDeviceState.InService:
                        bAvail = true;
                        break;
                    case DDeviceState.Defective:
                        bAvail = false == bRecording;
                        break;
                    case DDeviceState.Unavailable:
                        bAvail = false == bRecording;
                        break;
                    case DDeviceState.Intransit:
                        bAvail = bRecording;
                        break;
                }
            }
            return bAvail;
        }
        public static bool sbAvailEndStudy(UInt16 ANextStateMode, UInt16 ACurrentState)    // toDo
        {
            bool bAvail = false;
            if (ANextStateMode < (UInt16)DNextStateModeEnum.NrEnums
                && ACurrentState < (UInt16)DDeviceState.NrStates)
            {
                DDeviceState currentState = (DDeviceState)ACurrentState;
                DNextStateModeEnum nextStateMode = (DNextStateModeEnum)ANextStateMode;
                bAvail = true;

                // reassign same functional modes
                switch (nextStateMode)
                {
                    case DNextStateModeEnum.Normal:             // normal
                        break;
                    case DNextStateModeEnum.Locked:             // lock disables all next state buttons (must be taken off locked before continue
                        break;
                    case DNextStateModeEnum.ToService:          // next state device must be In Service
                        break;
                    case DNextStateModeEnum.CheckDevice:        // Device must be checked next state device must be In Service
                        break;
                    case DNextStateModeEnum.CollectData:        // collect the data before assigning to next patient
                        break;
                    case DNextStateModeEnum.UpdateSettings:     // update must be performed before assigning to next patient
                        nextStateMode = DNextStateModeEnum.ToService;
                        break;
                    case DNextStateModeEnum.UpdateFirmware:     // update firmware must be performed before assigning to next patient
                        nextStateMode = DNextStateModeEnum.ToService;
                        break;
                }
                //   determin availability
                switch (nextStateMode)
                {
                    case DNextStateModeEnum.Normal:             // normal
                        break;
                    case DNextStateModeEnum.Locked:             // lock disables all next state buttons (must be taken off locked before continue                       
                        break;
                    case DNextStateModeEnum.ToService:          // next state device must be In Service
                        break;
                    case DNextStateModeEnum.CheckDevice:        // Device must be checked next state device must be In Service
                        break;
                    case DNextStateModeEnum.CollectData:        // collect the data before assigning to next patient
                        break;
                    case DNextStateModeEnum.UpdateSettings:     // update must be performed before assigning to next patient
                        break;
                    case DNextStateModeEnum.UpdateFirmware:     // update firmware must be performed before assigning to next patient
                        break;
                }
            }
            return bAvail;
        }
    }
}
