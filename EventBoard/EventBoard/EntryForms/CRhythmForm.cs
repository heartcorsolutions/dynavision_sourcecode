﻿using Event_Base;
using Program_Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventBoard.EntryForms
{
    public partial class CRhythmForm : Form
    {
        public CStringSet _mSelectedSet = null;

        public CRhythmForm(CStringSet ACodeSet)
        {
            _mSelectedSet = new CStringSet();

            if( _mSelectedSet == null )
            {
                Close();
            }
            else
            {
                if (ACodeSet != null)
                {
                    _mSelectedSet.mbCopyFrom(ACodeSet);
                }
                CDvtmsData.sbInitData();

                InitializeComponent();
                mFillForm();
                Text = CProgram.sMakeProgTitle("Select Rhythm", false, true);

            }
        }
        private void mFillForm()
        {
            /*
$SR	Sinus Rhythm
$AR	Atrial Rhythm
$JR	Junctional Rhythm
$VR	Ventricular Rhythm
$AVR AtrioVentricular Rhythm
$PR	Pacemaker Rhythm
              */
            if (CDvtmsData._sEnumListStudyRhythms != null && _mSelectedSet != null)
            {
                UInt16 group = (UInt16)CDvtmsData._sEnumListStudyRhythms.mGetIntValue("$SR");

                CDvtmsData._sEnumListStudyRhythms.mFillCheckedListBox(checkedListBoxSR, true, group, true, " ", _mSelectedSet, false);

                group = (UInt16)CDvtmsData._sEnumListStudyRhythms.mGetIntValue("$AR");

                CDvtmsData._sEnumListStudyRhythms.mFillCheckedListBox(checkedListBoxAR, true, group, true, " ", _mSelectedSet, false);

                group = (UInt16)CDvtmsData._sEnumListStudyRhythms.mGetIntValue("$JR");

                CDvtmsData._sEnumListStudyRhythms.mFillCheckedListBox(checkedListBoxJR, true, group, true, " ", _mSelectedSet, false);

                group = (UInt16)CDvtmsData._sEnumListStudyRhythms.mGetIntValue("$VR");

                CDvtmsData._sEnumListStudyRhythms.mFillCheckedListBox(checkedListBoxVR, true, group, true, " ", _mSelectedSet, false);

                group = (UInt16)CDvtmsData._sEnumListStudyRhythms.mGetIntValue("$AVR");

                CDvtmsData._sEnumListStudyRhythms.mFillCheckedListBox(checkedListBoxAVR, true, group, true, " ", _mSelectedSet, false);

                group = (UInt16)CDvtmsData._sEnumListStudyRhythms.mGetIntValue("$PR");

                CDvtmsData._sEnumListStudyRhythms.mFillCheckedListBox(checkedListBoxPR, true, group, true, " ", _mSelectedSet, false);
            }
        }

        private void mReadForm()
        {
            if (CDvtmsData._sEnumListStudyRhythms != null && _mSelectedSet != null)
            {
                CStringSet set = new CStringSet();
                CStringSet newSet = new CStringSet();

                CDvtmsData._sEnumListStudyRhythms.mReadCheckedListBoxSet(checkedListBoxSR, true, " ", set);

                newSet.mAddSet(set);

                CDvtmsData._sEnumListStudyRhythms.mReadCheckedListBoxSet(checkedListBoxAR, true, " ", set);

                newSet.mAddSet(set);

                CDvtmsData._sEnumListStudyRhythms.mReadCheckedListBoxSet(checkedListBoxJR, true, " ", set);

                newSet.mAddSet(set);

                CDvtmsData._sEnumListStudyRhythms.mReadCheckedListBoxSet(checkedListBoxVR, true, " ", set);

                newSet.mAddSet(set);

                CDvtmsData._sEnumListStudyRhythms.mReadCheckedListBoxSet(checkedListBoxAVR, true, " ", set);

                newSet.mAddSet(set);

                CDvtmsData._sEnumListStudyRhythms.mReadCheckedListBoxSet(checkedListBoxPR, true, " ", set);

                newSet.mAddSet(set);

                newSet.mSortSet(_mSelectedSet);
                _mSelectedSet = newSet;
            }
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            _mSelectedSet = null;
            Close();
        }

        private void buttonSelect_Click(object sender, EventArgs e)
        {
            mReadForm();
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if( _mSelectedSet != null )
            {
                _mSelectedSet.mClear();
                mFillForm();
            }
        }
    }
}
