﻿namespace EventboardEntryForms
{
    partial class CStudyPatientForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CStudyPatientForm));
            this.labelStudyPhysician = new System.Windows.Forms.Label();
            this.comboBoxStudyPhysicianList = new System.Windows.Forms.ComboBox();
            this.labelStudyRefPhysician = new System.Windows.Forms.Label();
            this.comboBoxStudyRefPhysicianList = new System.Windows.Forms.ComboBox();
            this.comboBoxStudyHospitalNameList = new System.Windows.Forms.ComboBox();
            this.labelStudyHospitalName = new System.Windows.Forms.Label();
            this.labelHospitalRoom = new System.Windows.Forms.Label();
            this.textBoxHospitalRoom = new System.Windows.Forms.TextBox();
            this.labelStudySerialNumber = new System.Windows.Forms.Label();
            this.comboBoxStudySerialNumberList = new System.Windows.Forms.ComboBox();
            this.textBoxHospitalBed = new System.Windows.Forms.TextBox();
            this.labelHospitalBed = new System.Windows.Forms.Label();
            this.labelStudyStartDate = new System.Windows.Forms.Label();
            this.dateTimePickerStudyStartDate = new System.Windows.Forms.DateTimePicker();
            this.labelStudyType = new System.Windows.Forms.Label();
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            this.panel8 = new System.Windows.Forms.Panel();
            this.labelTrialDays = new System.Windows.Forms.Label();
            this.labelLocation = new System.Windows.Forms.Label();
            this.comboBoxLocation = new System.Windows.Forms.ComboBox();
            this.textBoxStudyPermissions = new System.Windows.Forms.TextBox();
            this.labelStudyPermissions = new System.Windows.Forms.Label();
            this.labelNdevices = new System.Windows.Forms.Label();
            this.labelStudyPlus1 = new System.Windows.Forms.Label();
            this.labelStudyPlus6 = new System.Windows.Forms.Label();
            this.labelStudyMin1 = new System.Windows.Forms.Label();
            this.labelStudyMin6 = new System.Windows.Forms.Label();
            this.labelRecInfo = new System.Windows.Forms.Label();
            this.checkBoxAllRefPhysician = new System.Windows.Forms.CheckBox();
            this.checkBoxAllPhysician = new System.Windows.Forms.CheckBox();
            this.checkBoxAllRecorder = new System.Windows.Forms.CheckBox();
            this.comboBoxTrial = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonClearNote = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.labelEndRec = new System.Windows.Forms.Label();
            this.labelStartRec = new System.Windows.Forms.Label();
            this.buttonUseCurrentDevice = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.checkedListBoxMctInterval = new System.Windows.Forms.CheckedListBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxStdInstructions = new System.Windows.Forms.TextBox();
            this.labelDeviceCmp = new System.Windows.Forms.Label();
            this.checkedListBoxReportInterval = new System.Windows.Forms.CheckedListBox();
            this.labelStudyLabel = new System.Windows.Forms.Label();
            this.textBoxStudyNote = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxStudyPhysicianInstruction = new System.Windows.Forms.TextBox();
            this.labelStudyPhysicianInstruction = new System.Windows.Forms.Label();
            this.comboBoxStudyMonitoringDaysList = new System.Windows.Forms.ComboBox();
            this.labelStudyMonitoringDays = new System.Windows.Forms.Label();
            this.comboBoxStudyTypeList = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label45 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.textBoxStudyNr = new System.Windows.Forms.TextBox();
            this.panelCreateInfo = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelStudyChanged = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.labelStudyCreated = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelDeviceError = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.labelStudyError = new System.Windows.Forms.Label();
            this.labelStudyNr = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel25 = new System.Windows.Forms.Panel();
            this.panel33 = new System.Windows.Forms.Panel();
            this.labelDefRefIdMode = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label27 = new System.Windows.Forms.Label();
            this.labelAddStudyMode = new System.Windows.Forms.Label();
            this.labelNoMct = new System.Windows.Forms.Label();
            this.labelDeviceChanged = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.labelDeviceClient = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.labelDeviceRemLabel = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.textBoxRefIdOther = new System.Windows.Forms.TextBox();
            this.labelRecordEndDate = new System.Windows.Forms.Label();
            this.labelRecorderStartDate = new System.Windows.Forms.Label();
            this.radioButtonOther = new System.Windows.Forms.RadioButton();
            this.label12 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.radioButtonUniqueID = new System.Windows.Forms.RadioButton();
            this.labelRecorderStudyNr = new System.Windows.Forms.Label();
            this.labelRecorderState = new System.Windows.Forms.Label();
            this.radioButtonPatID = new System.Windows.Forms.RadioButton();
            this.labelRecorderRefID = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.radioButtonNone = new System.Windows.Forms.RadioButton();
            this.label11 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.labelRecorderID = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.listViewStudyProcedures = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.buttonAddICD = new System.Windows.Forms.Button();
            this.labelUseRefID = new System.Windows.Forms.Label();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel21 = new System.Windows.Forms.Panel();
            this.checkBoxAnonymizePatientInfo = new System.Windows.Forms.CheckBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.labelPatientError = new System.Windows.Forms.Label();
            this.labelPatientNr = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel20 = new System.Windows.Forms.Panel();
            this.panel19 = new System.Windows.Forms.Panel();
            this.panel23 = new System.Windows.Forms.Panel();
            this.panelButtons = new System.Windows.Forms.Panel();
            this.buttonHolter = new System.Windows.Forms.Button();
            this.buttonRecordsInfo = new System.Windows.Forms.Button();
            this.buttonCreateReport = new System.Windows.Forms.Button();
            this.buttonEndRecording = new System.Windows.Forms.Button();
            this.buttonViewPdf = new System.Windows.Forms.Button();
            this.buttonClear = new System.Windows.Forms.Button();
            this.buttonDup = new System.Windows.Forms.Button();
            this.buttonAddUpdate = new System.Windows.Forms.Button();
            this.buttonPrintPreviousFindings = new System.Windows.Forms.Button();
            this.buttonMCT = new System.Windows.Forms.Button();
            this.buttonDeviceList = new System.Windows.Forms.Button();
            this.panel31 = new System.Windows.Forms.Panel();
            this.panel34 = new System.Windows.Forms.Panel();
            this.panel29 = new System.Windows.Forms.Panel();
            this.panel37 = new System.Windows.Forms.Panel();
            this.checkedListBoxSymptomsList = new System.Windows.Forms.CheckedListBox();
            this.labelPatientHistory = new System.Windows.Forms.Label();
            this.labelPatientSymptoms = new System.Windows.Forms.Label();
            this.checkedListBoxMedicationList = new System.Windows.Forms.CheckedListBox();
            this.checkedListBoxAllergiesList = new System.Windows.Forms.CheckedListBox();
            this.checkedListBoxHistoryList = new System.Windows.Forms.CheckedListBox();
            this.labelPatientAllergies = new System.Windows.Forms.Label();
            this.labelPatientMedication = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.buttonPatientForm = new System.Windows.Forms.Button();
            this.buttonClearPatient = new System.Windows.Forms.Button();
            this.buttonDobNA = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxPatientLabel = new System.Windows.Forms.TextBox();
            this.labelUnitWeight = new System.Windows.Forms.Label();
            this.labelUnitHeightMinor = new System.Windows.Forms.Label();
            this.textBoxPatientHeightMinor = new System.Windows.Forms.TextBox();
            this.labelUnitHeightMajor = new System.Windows.Forms.Label();
            this.textBoxAge = new System.Windows.Forms.TextBox();
            this.labelPatientAge = new System.Windows.Forms.Label();
            this.buttonSearchPatientID = new System.Windows.Forms.Button();
            this.comboBoxPatientRaceList = new System.Windows.Forms.ComboBox();
            this.labelPatientRace = new System.Windows.Forms.Label();
            this.textBoxPatientWeight = new System.Windows.Forms.TextBox();
            this.labelPatientWeight = new System.Windows.Forms.Label();
            this.textBoxPatientHeightMajor = new System.Windows.Forms.TextBox();
            this.labelPatientHeight = new System.Windows.Forms.Label();
            this.textBoxPatientID = new System.Windows.Forms.TextBox();
            this.labelPatientID = new System.Windows.Forms.Label();
            this.textBoxPatientDOBYear = new System.Windows.Forms.TextBox();
            this.comboBoxPatientDOBMonth = new System.Windows.Forms.ComboBox();
            this.textBoxPatientDOBDay = new System.Windows.Forms.TextBox();
            this.labelPatientDOB = new System.Windows.Forms.Label();
            this.comboBoxPatientGenderList = new System.Windows.Forms.ComboBox();
            this.labelPatientGender = new System.Windows.Forms.Label();
            this.textBoxPatientLastName = new System.Windows.Forms.TextBox();
            this.labelPatientLastName = new System.Windows.Forms.Label();
            this.textBoxPatientMiddleName = new System.Windows.Forms.TextBox();
            this.labelPatientMiddleName = new System.Windows.Forms.Label();
            this.textBoxPatientFirstName = new System.Windows.Forms.TextBox();
            this.labelPatientFirstName = new System.Windows.Forms.Label();
            this.labelSocSecNr = new System.Windows.Forms.Label();
            this.textBoxSocSecNr = new System.Windows.Forms.TextBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.buttonUploadPicture = new System.Windows.Forms.Button();
            this.comboBoxPatientCountryList = new System.Windows.Forms.ComboBox();
            this.textBoxPatientSkype = new System.Windows.Forms.TextBox();
            this.labelPatientSkype = new System.Windows.Forms.Label();
            this.textBoxPatientEmail = new System.Windows.Forms.TextBox();
            this.labelPatientEmail = new System.Windows.Forms.Label();
            this.textBoxPatientPhone2 = new System.Windows.Forms.TextBox();
            this.labelPatientPhone2 = new System.Windows.Forms.Label();
            this.textBoxPatientHouseNumber = new System.Windows.Forms.TextBox();
            this.buttonRandom = new System.Windows.Forms.Button();
            this.labelPatientHouseNumber = new System.Windows.Forms.Label();
            this.textBoxPatientCell = new System.Windows.Forms.TextBox();
            this.labelPatientCell = new System.Windows.Forms.Label();
            this.textBoxPatientAddress = new System.Windows.Forms.TextBox();
            this.labelPatientAddress = new System.Windows.Forms.Label();
            this.comboBoxPatientStateList = new System.Windows.Forms.ComboBox();
            this.textBoxPatientPhone1 = new System.Windows.Forms.TextBox();
            this.labelPatientPhone1 = new System.Windows.Forms.Label();
            this.labelPatientCountry = new System.Windows.Forms.Label();
            this.textBoxPatientCity = new System.Windows.Forms.TextBox();
            this.labelPatientCity = new System.Windows.Forms.Label();
            this.labelPatientState = new System.Windows.Forms.Label();
            this.textBoxPatientZip = new System.Windows.Forms.TextBox();
            this.labelPatientZip = new System.Windows.Forms.Label();
            this.panel22 = new System.Windows.Forms.Panel();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.panel9 = new System.Windows.Forms.Panel();
            this.labelRecorsInfo = new System.Windows.Forms.Label();
            this.checkBoxUpdateRecords = new System.Windows.Forms.CheckBox();
            this.checkBoxUpdatePatInfo = new System.Windows.Forms.CheckBox();
            this.panel8.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panelCreateInfo.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel33.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panelButtons.SuspendLayout();
            this.panel34.SuspendLayout();
            this.panel29.SuspendLayout();
            this.panel37.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel22.SuspendLayout();
            this.panel9.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelStudyPhysician
            // 
            this.labelStudyPhysician.AutoSize = true;
            this.labelStudyPhysician.Font = new System.Drawing.Font("Arial", 10F);
            this.labelStudyPhysician.Location = new System.Drawing.Point(6, 194);
            this.labelStudyPhysician.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelStudyPhysician.Name = "labelStudyPhysician";
            this.labelStudyPhysician.Size = new System.Drawing.Size(72, 16);
            this.labelStudyPhysician.TabIndex = 16;
            this.labelStudyPhysician.Text = "Physician:";
            this.labelStudyPhysician.Click += new System.EventHandler(this.labelStudyPhysician_Click);
            // 
            // comboBoxStudyPhysicianList
            // 
            this.comboBoxStudyPhysicianList.DropDownWidth = 200;
            this.comboBoxStudyPhysicianList.FormattingEnabled = true;
            this.comboBoxStudyPhysicianList.Items.AddRange(new object[] {
            "Dr. James Birkley",
            "Dr. Mike Stanley"});
            this.comboBoxStudyPhysicianList.Location = new System.Drawing.Point(105, 190);
            this.comboBoxStudyPhysicianList.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxStudyPhysicianList.Name = "comboBoxStudyPhysicianList";
            this.comboBoxStudyPhysicianList.Size = new System.Drawing.Size(175, 24);
            this.comboBoxStudyPhysicianList.TabIndex = 17;
            this.comboBoxStudyPhysicianList.Text = "^Txt";
            this.comboBoxStudyPhysicianList.SelectedIndexChanged += new System.EventHandler(this.comboBoxStudyPhysicianList_SelectedIndexChanged);
            this.comboBoxStudyPhysicianList.TextChanged += new System.EventHandler(this.comboBoxStudyPhysicianList_TextChanged);
            // 
            // labelStudyRefPhysician
            // 
            this.labelStudyRefPhysician.AutoSize = true;
            this.labelStudyRefPhysician.Font = new System.Drawing.Font("Arial", 10F);
            this.labelStudyRefPhysician.Location = new System.Drawing.Point(6, 166);
            this.labelStudyRefPhysician.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelStudyRefPhysician.Name = "labelStudyRefPhysician";
            this.labelStudyRefPhysician.Size = new System.Drawing.Size(98, 16);
            this.labelStudyRefPhysician.TabIndex = 14;
            this.labelStudyRefPhysician.Text = "Ref Physician:";
            this.labelStudyRefPhysician.Click += new System.EventHandler(this.labelStudyRefPhysician_Click);
            // 
            // comboBoxStudyRefPhysicianList
            // 
            this.comboBoxStudyRefPhysicianList.DropDownWidth = 200;
            this.comboBoxStudyRefPhysicianList.FormattingEnabled = true;
            this.comboBoxStudyRefPhysicianList.Items.AddRange(new object[] {
            "Dr. Jason White",
            "Dr. Albert Wistley"});
            this.comboBoxStudyRefPhysicianList.Location = new System.Drawing.Point(105, 162);
            this.comboBoxStudyRefPhysicianList.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxStudyRefPhysicianList.Name = "comboBoxStudyRefPhysicianList";
            this.comboBoxStudyRefPhysicianList.Size = new System.Drawing.Size(175, 24);
            this.comboBoxStudyRefPhysicianList.TabIndex = 15;
            this.comboBoxStudyRefPhysicianList.Text = "^Txt";
            // 
            // comboBoxStudyHospitalNameList
            // 
            this.comboBoxStudyHospitalNameList.DropDownWidth = 200;
            this.comboBoxStudyHospitalNameList.FormattingEnabled = true;
            this.comboBoxStudyHospitalNameList.Items.AddRange(new object[] {
            "Floria Hospital",
            "New York Hospital"});
            this.comboBoxStudyHospitalNameList.Location = new System.Drawing.Point(105, 11);
            this.comboBoxStudyHospitalNameList.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxStudyHospitalNameList.MaxDropDownItems = 7;
            this.comboBoxStudyHospitalNameList.Name = "comboBoxStudyHospitalNameList";
            this.comboBoxStudyHospitalNameList.Size = new System.Drawing.Size(175, 24);
            this.comboBoxStudyHospitalNameList.TabIndex = 35;
            this.comboBoxStudyHospitalNameList.Text = "^Txt";
            this.comboBoxStudyHospitalNameList.SelectedIndexChanged += new System.EventHandler(this.comboBoxStudyHospitalNameList_SelectedIndexChanged);
            // 
            // labelStudyHospitalName
            // 
            this.labelStudyHospitalName.AutoSize = true;
            this.labelStudyHospitalName.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelStudyHospitalName.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.labelStudyHospitalName.Location = new System.Drawing.Point(6, 16);
            this.labelStudyHospitalName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelStudyHospitalName.Name = "labelStudyHospitalName";
            this.labelStudyHospitalName.Size = new System.Drawing.Size(62, 16);
            this.labelStudyHospitalName.TabIndex = 6;
            this.labelStudyHospitalName.Text = "Client: *";
            this.labelStudyHospitalName.Click += new System.EventHandler(this.labelStudyHospitalName_Click);
            // 
            // labelHospitalRoom
            // 
            this.labelHospitalRoom.AutoSize = true;
            this.labelHospitalRoom.Font = new System.Drawing.Font("Arial", 10F);
            this.labelHospitalRoom.Location = new System.Drawing.Point(6, 105);
            this.labelHospitalRoom.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelHospitalRoom.Name = "labelHospitalRoom";
            this.labelHospitalRoom.Size = new System.Drawing.Size(49, 16);
            this.labelHospitalRoom.TabIndex = 10;
            this.labelHospitalRoom.Text = "Room:";
            // 
            // textBoxHospitalRoom
            // 
            this.textBoxHospitalRoom.Location = new System.Drawing.Point(105, 102);
            this.textBoxHospitalRoom.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxHospitalRoom.Name = "textBoxHospitalRoom";
            this.textBoxHospitalRoom.Size = new System.Drawing.Size(65, 23);
            this.textBoxHospitalRoom.TabIndex = 11;
            this.textBoxHospitalRoom.Text = "^Txt";
            // 
            // labelStudySerialNumber
            // 
            this.labelStudySerialNumber.AutoSize = true;
            this.labelStudySerialNumber.Font = new System.Drawing.Font("Arial", 10F);
            this.labelStudySerialNumber.Location = new System.Drawing.Point(367, 15);
            this.labelStudySerialNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelStudySerialNumber.Name = "labelStudySerialNumber";
            this.labelStudySerialNumber.Size = new System.Drawing.Size(48, 16);
            this.labelStudySerialNumber.TabIndex = 24;
            this.labelStudySerialNumber.Text = "Serial:";
            // 
            // comboBoxStudySerialNumberList
            // 
            this.comboBoxStudySerialNumberList.DropDownWidth = 200;
            this.comboBoxStudySerialNumberList.FormattingEnabled = true;
            this.comboBoxStudySerialNumberList.Items.AddRange(new object[] {
            "123123123",
            "124213115",
            "123555321",
            "122134121"});
            this.comboBoxStudySerialNumberList.Location = new System.Drawing.Point(466, 11);
            this.comboBoxStudySerialNumberList.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxStudySerialNumberList.Name = "comboBoxStudySerialNumberList";
            this.comboBoxStudySerialNumberList.Size = new System.Drawing.Size(210, 24);
            this.comboBoxStudySerialNumberList.TabIndex = 25;
            this.comboBoxStudySerialNumberList.Text = "^^^select serial nr";
            this.comboBoxStudySerialNumberList.SelectedIndexChanged += new System.EventHandler(this.comboBoxStudySerialNumberList_TextChanged);
            this.comboBoxStudySerialNumberList.TextChanged += new System.EventHandler(this.comboBoxStudySerialNumberList_TextChanged);
            // 
            // textBoxHospitalBed
            // 
            this.textBoxHospitalBed.Location = new System.Drawing.Point(224, 102);
            this.textBoxHospitalBed.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxHospitalBed.Name = "textBoxHospitalBed";
            this.textBoxHospitalBed.Size = new System.Drawing.Size(56, 23);
            this.textBoxHospitalBed.TabIndex = 13;
            this.textBoxHospitalBed.Text = "^Txt";
            // 
            // labelHospitalBed
            // 
            this.labelHospitalBed.AutoSize = true;
            this.labelHospitalBed.Location = new System.Drawing.Point(183, 105);
            this.labelHospitalBed.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelHospitalBed.Name = "labelHospitalBed";
            this.labelHospitalBed.Size = new System.Drawing.Size(37, 16);
            this.labelHospitalBed.TabIndex = 12;
            this.labelHospitalBed.Text = "Bed:";
            // 
            // labelStudyStartDate
            // 
            this.labelStudyStartDate.AutoSize = true;
            this.labelStudyStartDate.Font = new System.Drawing.Font("Arial", 10F);
            this.labelStudyStartDate.Location = new System.Drawing.Point(6, 135);
            this.labelStudyStartDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelStudyStartDate.Name = "labelStudyStartDate";
            this.labelStudyStartDate.Size = new System.Drawing.Size(74, 16);
            this.labelStudyStartDate.TabIndex = 2;
            this.labelStudyStartDate.Text = "Start date:";
            this.labelStudyStartDate.Click += new System.EventHandler(this.label39_Click);
            // 
            // dateTimePickerStudyStartDate
            // 
            this.dateTimePickerStudyStartDate.CustomFormat = "yyyy-MM-dd HH:mm";
            this.dateTimePickerStudyStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerStudyStartDate.Location = new System.Drawing.Point(105, 132);
            this.dateTimePickerStudyStartDate.Margin = new System.Windows.Forms.Padding(4);
            this.dateTimePickerStudyStartDate.Name = "dateTimePickerStudyStartDate";
            this.dateTimePickerStudyStartDate.Size = new System.Drawing.Size(175, 23);
            this.dateTimePickerStudyStartDate.TabIndex = 3;
            this.dateTimePickerStudyStartDate.ValueChanged += new System.EventHandler(this.dateTimePickerStudyStartDate_ValueChanged);
            // 
            // labelStudyType
            // 
            this.labelStudyType.AutoSize = true;
            this.labelStudyType.Location = new System.Drawing.Point(365, 44);
            this.labelStudyType.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelStudyType.Name = "labelStudyType";
            this.labelStudyType.Size = new System.Drawing.Size(79, 16);
            this.labelStudyType.TabIndex = 28;
            this.labelStudyType.Text = "Study type:";
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panel8.Controls.Add(this.labelTrialDays);
            this.panel8.Controls.Add(this.labelLocation);
            this.panel8.Controls.Add(this.comboBoxLocation);
            this.panel8.Controls.Add(this.textBoxStudyPermissions);
            this.panel8.Controls.Add(this.labelStudyPermissions);
            this.panel8.Controls.Add(this.labelNdevices);
            this.panel8.Controls.Add(this.labelStudyPlus1);
            this.panel8.Controls.Add(this.labelStudyPlus6);
            this.panel8.Controls.Add(this.labelStudyMin1);
            this.panel8.Controls.Add(this.labelStudyMin6);
            this.panel8.Controls.Add(this.labelRecInfo);
            this.panel8.Controls.Add(this.checkBoxAllRefPhysician);
            this.panel8.Controls.Add(this.checkBoxAllPhysician);
            this.panel8.Controls.Add(this.checkBoxAllRecorder);
            this.panel8.Controls.Add(this.comboBoxTrial);
            this.panel8.Controls.Add(this.label4);
            this.panel8.Controls.Add(this.buttonClearNote);
            this.panel8.Controls.Add(this.label19);
            this.panel8.Controls.Add(this.label18);
            this.panel8.Controls.Add(this.labelEndRec);
            this.panel8.Controls.Add(this.labelStartRec);
            this.panel8.Controls.Add(this.buttonUseCurrentDevice);
            this.panel8.Controls.Add(this.label7);
            this.panel8.Controls.Add(this.checkedListBoxMctInterval);
            this.panel8.Controls.Add(this.label6);
            this.panel8.Controls.Add(this.textBoxStdInstructions);
            this.panel8.Controls.Add(this.labelDeviceCmp);
            this.panel8.Controls.Add(this.checkedListBoxReportInterval);
            this.panel8.Controls.Add(this.labelStudyLabel);
            this.panel8.Controls.Add(this.textBoxStudyNote);
            this.panel8.Controls.Add(this.label1);
            this.panel8.Controls.Add(this.textBoxStudyPhysicianInstruction);
            this.panel8.Controls.Add(this.labelStudyPhysicianInstruction);
            this.panel8.Controls.Add(this.comboBoxStudyMonitoringDaysList);
            this.panel8.Controls.Add(this.labelStudyMonitoringDays);
            this.panel8.Controls.Add(this.comboBoxStudyTypeList);
            this.panel8.Controls.Add(this.labelStudyStartDate);
            this.panel8.Controls.Add(this.dateTimePickerStudyStartDate);
            this.panel8.Controls.Add(this.labelStudyHospitalName);
            this.panel8.Controls.Add(this.comboBoxStudyHospitalNameList);
            this.panel8.Controls.Add(this.labelStudyType);
            this.panel8.Controls.Add(this.comboBoxStudySerialNumberList);
            this.panel8.Controls.Add(this.labelHospitalRoom);
            this.panel8.Controls.Add(this.labelStudySerialNumber);
            this.panel8.Controls.Add(this.textBoxHospitalRoom);
            this.panel8.Controls.Add(this.textBoxHospitalBed);
            this.panel8.Controls.Add(this.labelHospitalBed);
            this.panel8.Controls.Add(this.labelStudyRefPhysician);
            this.panel8.Controls.Add(this.comboBoxStudyRefPhysicianList);
            this.panel8.Controls.Add(this.labelStudyPhysician);
            this.panel8.Controls.Add(this.comboBoxStudyPhysicianList);
            this.panel8.Font = new System.Drawing.Font("Arial", 10F);
            this.panel8.Location = new System.Drawing.Point(0, 4);
            this.panel8.Margin = new System.Windows.Forms.Padding(4);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(912, 297);
            this.panel8.TabIndex = 55;
            this.panel8.Paint += new System.Windows.Forms.PaintEventHandler(this.panel8_Paint);
            // 
            // labelTrialDays
            // 
            this.labelTrialDays.AutoSize = true;
            this.labelTrialDays.Location = new System.Drawing.Point(287, 44);
            this.labelTrialDays.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTrialDays.Name = "labelTrialDays";
            this.labelTrialDays.Size = new System.Drawing.Size(61, 16);
            this.labelTrialDays.TabIndex = 107;
            this.labelTrialDays.Text = "^22222d";
            // 
            // labelLocation
            // 
            this.labelLocation.AutoSize = true;
            this.labelLocation.Font = new System.Drawing.Font("Arial", 10F);
            this.labelLocation.Location = new System.Drawing.Point(6, 73);
            this.labelLocation.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelLocation.Name = "labelLocation";
            this.labelLocation.Size = new System.Drawing.Size(66, 16);
            this.labelLocation.TabIndex = 106;
            this.labelLocation.Text = "Location:";
            // 
            // comboBoxLocation
            // 
            this.comboBoxLocation.FormattingEnabled = true;
            this.comboBoxLocation.Location = new System.Drawing.Point(105, 70);
            this.comboBoxLocation.Name = "comboBoxLocation";
            this.comboBoxLocation.Size = new System.Drawing.Size(175, 24);
            this.comboBoxLocation.TabIndex = 105;
            this.comboBoxLocation.Text = "-- All --";
            // 
            // textBoxStudyPermissions
            // 
            this.textBoxStudyPermissions.BackColor = System.Drawing.Color.Bisque;
            this.textBoxStudyPermissions.Location = new System.Drawing.Point(755, 219);
            this.textBoxStudyPermissions.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxStudyPermissions.Multiline = true;
            this.textBoxStudyPermissions.Name = "textBoxStudyPermissions";
            this.textBoxStudyPermissions.ReadOnly = true;
            this.textBoxStudyPermissions.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxStudyPermissions.Size = new System.Drawing.Size(147, 72);
            this.textBoxStudyPermissions.TabIndex = 103;
            this.textBoxStudyPermissions.Text = "^line1\r\nline2\r\nline3\r\n";
            this.textBoxStudyPermissions.Visible = false;
            this.textBoxStudyPermissions.Click += new System.EventHandler(this.textBoxStudyPermissions_Click);
            this.textBoxStudyPermissions.TextChanged += new System.EventHandler(this.textBoxStudyPermissions_TextChanged);
            // 
            // labelStudyPermissions
            // 
            this.labelStudyPermissions.BackColor = System.Drawing.Color.Bisque;
            this.labelStudyPermissions.Location = new System.Drawing.Point(755, 204);
            this.labelStudyPermissions.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelStudyPermissions.Name = "labelStudyPermissions";
            this.labelStudyPermissions.Size = new System.Drawing.Size(156, 21);
            this.labelStudyPermissions.TabIndex = 102;
            this.labelStudyPermissions.Text = "Required permissions:";
            this.labelStudyPermissions.Visible = false;
            // 
            // labelNdevices
            // 
            this.labelNdevices.AutoSize = true;
            this.labelNdevices.Location = new System.Drawing.Point(344, 15);
            this.labelNdevices.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelNdevices.Name = "labelNdevices";
            this.labelNdevices.Size = new System.Drawing.Size(24, 16);
            this.labelNdevices.TabIndex = 101;
            this.labelNdevices.Text = ".1.";
            // 
            // labelStudyPlus1
            // 
            this.labelStudyPlus1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelStudyPlus1.Location = new System.Drawing.Point(324, 125);
            this.labelStudyPlus1.Name = "labelStudyPlus1";
            this.labelStudyPlus1.Size = new System.Drawing.Size(34, 18);
            this.labelStudyPlus1.TabIndex = 100;
            this.labelStudyPlus1.Text = "-6h";
            this.labelStudyPlus1.Click += new System.EventHandler(this.labelStudyPlus1_Click);
            // 
            // labelStudyPlus6
            // 
            this.labelStudyPlus6.AutoSize = true;
            this.labelStudyPlus6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelStudyPlus6.Location = new System.Drawing.Point(324, 145);
            this.labelStudyPlus6.Name = "labelStudyPlus6";
            this.labelStudyPlus6.Size = new System.Drawing.Size(34, 18);
            this.labelStudyPlus6.TabIndex = 99;
            this.labelStudyPlus6.Text = "+6h";
            this.labelStudyPlus6.Click += new System.EventHandler(this.labelStudyPlus6_Click);
            // 
            // labelStudyMin1
            // 
            this.labelStudyMin1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelStudyMin1.Location = new System.Drawing.Point(287, 125);
            this.labelStudyMin1.Name = "labelStudyMin1";
            this.labelStudyMin1.Size = new System.Drawing.Size(34, 18);
            this.labelStudyMin1.TabIndex = 98;
            this.labelStudyMin1.Text = "-1h";
            this.labelStudyMin1.Click += new System.EventHandler(this.labelStudyMin1_Click);
            // 
            // labelStudyMin6
            // 
            this.labelStudyMin6.AutoSize = true;
            this.labelStudyMin6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelStudyMin6.Location = new System.Drawing.Point(287, 145);
            this.labelStudyMin6.Name = "labelStudyMin6";
            this.labelStudyMin6.Size = new System.Drawing.Size(34, 18);
            this.labelStudyMin6.TabIndex = 97;
            this.labelStudyMin6.Text = "+1h";
            this.labelStudyMin6.Click += new System.EventHandler(this.labelStudyMin6_Click);
            // 
            // labelRecInfo
            // 
            this.labelRecInfo.Location = new System.Drawing.Point(372, 250);
            this.labelRecInfo.Name = "labelRecInfo";
            this.labelRecInfo.Size = new System.Drawing.Size(358, 46);
            this.labelRecInfo.TabIndex = 95;
            this.labelRecInfo.Text = ".^xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\r\n.^xxxxxxxxxxxxxxxxxxxxxxx" +
    "xxxxxxxxxxxxxxxxxxxxxxxxxxxxx\r\n.^xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" +
    "xxxxx\r\n\r\n.\r\n.\r\n";
            this.labelRecInfo.Click += new System.EventHandler(this.labelRecInfo_Click);
            // 
            // checkBoxAllRefPhysician
            // 
            this.checkBoxAllRefPhysician.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxAllRefPhysician.Location = new System.Drawing.Point(287, 164);
            this.checkBoxAllRefPhysician.Name = "checkBoxAllRefPhysician";
            this.checkBoxAllRefPhysician.Size = new System.Drawing.Size(45, 20);
            this.checkBoxAllRefPhysician.TabIndex = 94;
            this.checkBoxAllRefPhysician.Text = "All";
            this.checkBoxAllRefPhysician.UseVisualStyleBackColor = true;
            this.checkBoxAllRefPhysician.CheckedChanged += new System.EventHandler(this.checkBoxAllRefPhysician_CheckedChanged);
            // 
            // checkBoxAllPhysician
            // 
            this.checkBoxAllPhysician.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxAllPhysician.Location = new System.Drawing.Point(287, 193);
            this.checkBoxAllPhysician.Name = "checkBoxAllPhysician";
            this.checkBoxAllPhysician.Size = new System.Drawing.Size(45, 20);
            this.checkBoxAllPhysician.TabIndex = 93;
            this.checkBoxAllPhysician.Text = "All";
            this.checkBoxAllPhysician.UseVisualStyleBackColor = true;
            this.checkBoxAllPhysician.CheckedChanged += new System.EventHandler(this.checkBoxAllPhysician_CheckedChanged);
            // 
            // checkBoxAllRecorder
            // 
            this.checkBoxAllRecorder.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxAllRecorder.Location = new System.Drawing.Point(680, 13);
            this.checkBoxAllRecorder.Name = "checkBoxAllRecorder";
            this.checkBoxAllRecorder.Size = new System.Drawing.Size(48, 20);
            this.checkBoxAllRecorder.TabIndex = 92;
            this.checkBoxAllRecorder.Text = "All";
            this.checkBoxAllRecorder.UseVisualStyleBackColor = true;
            this.checkBoxAllRecorder.CheckedChanged += new System.EventHandler(this.checkBoxAllRecorder_CheckedChanged);
            // 
            // comboBoxTrial
            // 
            this.comboBoxTrial.FormattingEnabled = true;
            this.comboBoxTrial.Location = new System.Drawing.Point(105, 40);
            this.comboBoxTrial.Name = "comboBoxTrial";
            this.comboBoxTrial.Size = new System.Drawing.Size(175, 24);
            this.comboBoxTrial.TabIndex = 87;
            this.comboBoxTrial.Text = "^";
            this.comboBoxTrial.SelectedIndexChanged += new System.EventHandler(this.comboBoxTrial_SelectedIndexChanged);
            this.comboBoxTrial.Click += new System.EventHandler(this.comboBoxTrial_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 10F);
            this.label4.Location = new System.Drawing.Point(8, 44);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 16);
            this.label4.TabIndex = 86;
            this.label4.Text = "Trial:";
            this.label4.Click += new System.EventHandler(this.labelTrialText_Click);
            // 
            // buttonClearNote
            // 
            this.buttonClearNote.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonClearNote.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonClearNote.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonClearNote.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonClearNote.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonClearNote.Location = new System.Drawing.Point(347, 170);
            this.buttonClearNote.Margin = new System.Windows.Forms.Padding(4);
            this.buttonClearNote.Name = "buttonClearNote";
            this.buttonClearNote.Size = new System.Drawing.Size(54, 44);
            this.buttonClearNote.TabIndex = 85;
            this.buttonClearNote.Text = "Clear\r\nNote";
            this.buttonClearNote.UseVisualStyleBackColor = false;
            this.buttonClearNote.Click += new System.EventHandler(this.buttonClearNote_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(367, 234);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(74, 16);
            this.label19.TabIndex = 84;
            this.label19.Text = "Stop Rec.:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(366, 218);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(75, 16);
            this.label18.TabIndex = 83;
            this.label18.Text = "Start Rec.:";
            // 
            // labelEndRec
            // 
            this.labelEndRec.AutoSize = true;
            this.labelEndRec.Location = new System.Drawing.Point(468, 234);
            this.labelEndRec.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelEndRec.Name = "labelEndRec";
            this.labelEndRec.Size = new System.Drawing.Size(227, 16);
            this.labelEndRec.TabIndex = 82;
            this.labelEndRec.Text = "DD/MM/YYYY HH:MM:SS AM+1200";
            // 
            // labelStartRec
            // 
            this.labelStartRec.AutoSize = true;
            this.labelStartRec.Location = new System.Drawing.Point(467, 218);
            this.labelStartRec.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelStartRec.Name = "labelStartRec";
            this.labelStartRec.Size = new System.Drawing.Size(227, 16);
            this.labelStartRec.TabIndex = 81;
            this.labelStartRec.Text = "DD/MM/YYYY HH:MM:SS AM+1200";
            // 
            // buttonUseCurrentDevice
            // 
            this.buttonUseCurrentDevice.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonUseCurrentDevice.Font = new System.Drawing.Font("Wingdings", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.buttonUseCurrentDevice.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonUseCurrentDevice.Location = new System.Drawing.Point(410, 10);
            this.buttonUseCurrentDevice.Margin = new System.Windows.Forms.Padding(4);
            this.buttonUseCurrentDevice.Name = "buttonUseCurrentDevice";
            this.buttonUseCurrentDevice.Size = new System.Drawing.Size(20, 26);
            this.buttonUseCurrentDevice.TabIndex = 80;
            this.buttonUseCurrentDevice.Text = "è";
            this.buttonUseCurrentDevice.UseVisualStyleBackColor = false;
            this.buttonUseCurrentDevice.Click += new System.EventHandler(this.buttonUseCurrentDevice_Click);
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(364, 132);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 38);
            this.label7.TabIndex = 45;
            this.label7.Text = "Free \r\nnote:";
            // 
            // checkedListBoxMctInterval
            // 
            this.checkedListBoxMctInterval.CheckOnClick = true;
            this.checkedListBoxMctInterval.FormattingEnabled = true;
            this.checkedListBoxMctInterval.HorizontalScrollbar = true;
            this.checkedListBoxMctInterval.Items.AddRange(new object[] {
            "^Daily",
            "^Weekly",
            "^EOS"});
            this.checkedListBoxMctInterval.Location = new System.Drawing.Point(755, 139);
            this.checkedListBoxMctInterval.Name = "checkedListBoxMctInterval";
            this.checkedListBoxMctInterval.Size = new System.Drawing.Size(147, 58);
            this.checkedListBoxMctInterval.TabIndex = 44;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(755, 121);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(103, 21);
            this.label6.TabIndex = 43;
            this.label6.Text = "MCT Reports:";
            // 
            // textBoxStdInstructions
            // 
            this.textBoxStdInstructions.Location = new System.Drawing.Point(105, 219);
            this.textBoxStdInstructions.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxStdInstructions.Multiline = true;
            this.textBoxStdInstructions.Name = "textBoxStdInstructions";
            this.textBoxStdInstructions.ReadOnly = true;
            this.textBoxStdInstructions.Size = new System.Drawing.Size(253, 73);
            this.textBoxStdInstructions.TabIndex = 42;
            this.textBoxStdInstructions.Text = "^^Default instruction from Physician";
            // 
            // labelDeviceCmp
            // 
            this.labelDeviceCmp.AutoSize = true;
            this.labelDeviceCmp.Location = new System.Drawing.Point(430, 15);
            this.labelDeviceCmp.Margin = new System.Windows.Forms.Padding(0);
            this.labelDeviceCmp.Name = "labelDeviceCmp";
            this.labelDeviceCmp.Size = new System.Drawing.Size(37, 16);
            this.labelDeviceCmp.TabIndex = 41;
            this.labelDeviceCmp.Text = "!=XX";
            this.labelDeviceCmp.Click += new System.EventHandler(this.labelDeviceCmp_Click);
            // 
            // checkedListBoxReportInterval
            // 
            this.checkedListBoxReportInterval.CheckOnClick = true;
            this.checkedListBoxReportInterval.FormattingEnabled = true;
            this.checkedListBoxReportInterval.Items.AddRange(new object[] {
            "Item1",
            "item2",
            "item3",
            "item4",
            "item5"});
            this.checkedListBoxReportInterval.Location = new System.Drawing.Point(755, 20);
            this.checkedListBoxReportInterval.Name = "checkedListBoxReportInterval";
            this.checkedListBoxReportInterval.Size = new System.Drawing.Size(147, 94);
            this.checkedListBoxReportInterval.TabIndex = 40;
            // 
            // labelStudyLabel
            // 
            this.labelStudyLabel.AutoSize = true;
            this.labelStudyLabel.Location = new System.Drawing.Point(367, 105);
            this.labelStudyLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelStudyLabel.Name = "labelStudyLabel";
            this.labelStudyLabel.Size = new System.Drawing.Size(80, 16);
            this.labelStudyLabel.TabIndex = 39;
            this.labelStudyLabel.Text = "Study note:";
            this.labelStudyLabel.Click += new System.EventHandler(this.labelStudyLabel_Click);
            // 
            // textBoxStudyNote
            // 
            this.textBoxStudyNote.Location = new System.Drawing.Point(467, 102);
            this.textBoxStudyNote.MaxLength = 30;
            this.textBoxStudyNote.Name = "textBoxStudyNote";
            this.textBoxStudyNote.Size = new System.Drawing.Size(211, 23);
            this.textBoxStudyNote.TabIndex = 38;
            this.textBoxStudyNote.Text = "^Txt";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(755, 4);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 21);
            this.label1.TabIndex = 36;
            this.label1.Text = "Event Reports:";
            // 
            // textBoxStudyPhysicianInstruction
            // 
            this.textBoxStudyPhysicianInstruction.Location = new System.Drawing.Point(409, 132);
            this.textBoxStudyPhysicianInstruction.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxStudyPhysicianInstruction.MaxLength = 30000;
            this.textBoxStudyPhysicianInstruction.Multiline = true;
            this.textBoxStudyPhysicianInstruction.Name = "textBoxStudyPhysicianInstruction";
            this.textBoxStudyPhysicianInstruction.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxStudyPhysicianInstruction.Size = new System.Drawing.Size(339, 84);
            this.textBoxStudyPhysicianInstruction.TabIndex = 21;
            this.textBoxStudyPhysicianInstruction.Text = "^Txt\r\n2\r\n3\r\n4\r\n5\r\n6\r\n7\r\n8\r\n";
            this.textBoxStudyPhysicianInstruction.TextChanged += new System.EventHandler(this.textBoxStudyPhysicianInstruction_TextChanged);
            // 
            // labelStudyPhysicianInstruction
            // 
            this.labelStudyPhysicianInstruction.AutoSize = true;
            this.labelStudyPhysicianInstruction.Font = new System.Drawing.Font("Arial", 10F);
            this.labelStudyPhysicianInstruction.Location = new System.Drawing.Point(6, 226);
            this.labelStudyPhysicianInstruction.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelStudyPhysicianInstruction.Name = "labelStudyPhysicianInstruction";
            this.labelStudyPhysicianInstruction.Size = new System.Drawing.Size(84, 16);
            this.labelStudyPhysicianInstruction.TabIndex = 20;
            this.labelStudyPhysicianInstruction.Text = "Instructions:";
            // 
            // comboBoxStudyMonitoringDaysList
            // 
            this.comboBoxStudyMonitoringDaysList.DropDownWidth = 200;
            this.comboBoxStudyMonitoringDaysList.FormattingEnabled = true;
            this.comboBoxStudyMonitoringDaysList.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "21",
            "28",
            "30",
            "31"});
            this.comboBoxStudyMonitoringDaysList.Location = new System.Drawing.Point(467, 70);
            this.comboBoxStudyMonitoringDaysList.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxStudyMonitoringDaysList.Name = "comboBoxStudyMonitoringDaysList";
            this.comboBoxStudyMonitoringDaysList.Size = new System.Drawing.Size(80, 24);
            this.comboBoxStudyMonitoringDaysList.TabIndex = 27;
            this.comboBoxStudyMonitoringDaysList.Text = "^Txt321";
            this.comboBoxStudyMonitoringDaysList.SelectedIndexChanged += new System.EventHandler(this.comboBoxStudyMonitoringDaysList_SelectedIndexChanged);
            // 
            // labelStudyMonitoringDays
            // 
            this.labelStudyMonitoringDays.AutoSize = true;
            this.labelStudyMonitoringDays.Location = new System.Drawing.Point(367, 78);
            this.labelStudyMonitoringDays.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelStudyMonitoringDays.Name = "labelStudyMonitoringDays";
            this.labelStudyMonitoringDays.Size = new System.Drawing.Size(44, 16);
            this.labelStudyMonitoringDays.TabIndex = 26;
            this.labelStudyMonitoringDays.Text = "Days:";
            // 
            // comboBoxStudyTypeList
            // 
            this.comboBoxStudyTypeList.DropDownWidth = 200;
            this.comboBoxStudyTypeList.FormattingEnabled = true;
            this.comboBoxStudyTypeList.Items.AddRange(new object[] {
            "! 2L - 1 ch event",
            "! 3L - 1 chl event",
            "! 3L - 2 ch event",
            "! 2L - 1 ch MCT",
            "! 3L - 1 chl MCT",
            "! 3L - 2 ch MCT",
            "! 5L - 2 ch holter",
            "! 5L - 5 ch real-time monitoring",
            "! POST TTM"});
            this.comboBoxStudyTypeList.Location = new System.Drawing.Point(467, 40);
            this.comboBoxStudyTypeList.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxStudyTypeList.Name = "comboBoxStudyTypeList";
            this.comboBoxStudyTypeList.Size = new System.Drawing.Size(211, 24);
            this.comboBoxStudyTypeList.TabIndex = 29;
            this.comboBoxStudyTypeList.Text = "^Txt";
            // 
            // label13
            // 
            this.label13.Dock = System.Windows.Forms.DockStyle.Left;
            this.label13.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(0, 0);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(163, 26);
            this.label13.TabIndex = 57;
            this.label13.Text = "Patient information";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label45);
            this.panel4.Controls.Add(this.panel2);
            this.panel4.Controls.Add(this.panelCreateInfo);
            this.panel4.Controls.Add(this.labelDeviceError);
            this.panel4.Controls.Add(this.label21);
            this.panel4.Controls.Add(this.labelStudyError);
            this.panel4.Controls.Add(this.labelStudyNr);
            this.panel4.Controls.Add(this.label22);
            this.panel4.Controls.Add(this.panel12);
            this.panel4.Controls.Add(this.panel11);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1274, 29);
            this.panel4.TabIndex = 61;
            // 
            // label45
            // 
            this.label45.Dock = System.Windows.Forms.DockStyle.Right;
            this.label45.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label45.Location = new System.Drawing.Point(797, 0);
            this.label45.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(129, 29);
            this.label45.TabIndex = 66;
            this.label45.Text = "Study Number";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label45.Click += new System.EventHandler(this.label45_Click);
            this.label45.DoubleClick += new System.EventHandler(this.label45_DoubleClick);
            // 
            // panel2
            // 
            this.panel2.AutoSize = true;
            this.panel2.Controls.Add(this.textBoxStudyNr);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(926, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(84, 29);
            this.panel2.TabIndex = 68;
            // 
            // textBoxStudyNr
            // 
            this.textBoxStudyNr.Dock = System.Windows.Forms.DockStyle.Left;
            this.textBoxStudyNr.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxStudyNr.Location = new System.Drawing.Point(0, 0);
            this.textBoxStudyNr.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxStudyNr.Multiline = true;
            this.textBoxStudyNr.Name = "textBoxStudyNr";
            this.textBoxStudyNr.ReadOnly = true;
            this.textBoxStudyNr.Size = new System.Drawing.Size(84, 29);
            this.textBoxStudyNr.TabIndex = 1;
            this.textBoxStudyNr.Text = "^000000";
            this.textBoxStudyNr.DoubleClick += new System.EventHandler(this.textBoxStudyNr_DoubleClick);
            // 
            // panelCreateInfo
            // 
            this.panelCreateInfo.Controls.Add(this.panel1);
            this.panelCreateInfo.Controls.Add(this.panel5);
            this.panelCreateInfo.Controls.Add(this.label2);
            this.panelCreateInfo.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelCreateInfo.Location = new System.Drawing.Point(1010, 0);
            this.panelCreateInfo.Name = "panelCreateInfo";
            this.panelCreateInfo.Size = new System.Drawing.Size(264, 29);
            this.panelCreateInfo.TabIndex = 67;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.labelStudyChanged);
            this.panel1.Controls.Add(this.label25);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 15);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(264, 18);
            this.panel1.TabIndex = 2;
            // 
            // labelStudyChanged
            // 
            this.labelStudyChanged.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelStudyChanged.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.labelStudyChanged.Location = new System.Drawing.Point(54, 0);
            this.labelStudyChanged.Name = "labelStudyChanged";
            this.labelStudyChanged.Size = new System.Drawing.Size(210, 18);
            this.labelStudyChanged.TabIndex = 1;
            this.labelStudyChanged.Text = "DD/MM/YYYY HH:MM:SS AM initial";
            // 
            // label25
            // 
            this.label25.Dock = System.Windows.Forms.DockStyle.Left;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.label25.Location = new System.Drawing.Point(0, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(54, 18);
            this.label25.TabIndex = 0;
            this.label25.Text = "Changed:";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.labelStudyCreated);
            this.panel5.Controls.Add(this.label23);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(264, 15);
            this.panel5.TabIndex = 1;
            // 
            // labelStudyCreated
            // 
            this.labelStudyCreated.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelStudyCreated.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.labelStudyCreated.Location = new System.Drawing.Point(54, 0);
            this.labelStudyCreated.Name = "labelStudyCreated";
            this.labelStudyCreated.Size = new System.Drawing.Size(210, 15);
            this.labelStudyCreated.TabIndex = 1;
            this.labelStudyCreated.Text = "DD/MM/YYYY HH:MM:SS AM initial";
            // 
            // label23
            // 
            this.label23.Dock = System.Windows.Forms.DockStyle.Left;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.label23.Location = new System.Drawing.Point(0, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(54, 15);
            this.label23.TabIndex = 0;
            this.label23.Text = "Created:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 17);
            this.label2.TabIndex = 0;
            // 
            // labelDeviceError
            // 
            this.labelDeviceError.AutoSize = true;
            this.labelDeviceError.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelDeviceError.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelDeviceError.Location = new System.Drawing.Point(331, 0);
            this.labelDeviceError.Name = "labelDeviceError";
            this.labelDeviceError.Size = new System.Drawing.Size(25, 19);
            this.labelDeviceError.TabIndex = 62;
            this.labelDeviceError.Text = "....";
            this.labelDeviceError.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Dock = System.Windows.Forms.DockStyle.Left;
            this.label21.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(308, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(23, 18);
            this.label21.TabIndex = 65;
            this.label21.Text = "   ";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStudyError
            // 
            this.labelStudyError.AutoSize = true;
            this.labelStudyError.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelStudyError.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelStudyError.Location = new System.Drawing.Point(283, 0);
            this.labelStudyError.Name = "labelStudyError";
            this.labelStudyError.Size = new System.Drawing.Size(25, 19);
            this.labelStudyError.TabIndex = 61;
            this.labelStudyError.Text = "....";
            this.labelStudyError.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStudyNr
            // 
            this.labelStudyNr.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelStudyNr.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelStudyNr.Location = new System.Drawing.Point(205, 0);
            this.labelStudyNr.Name = "labelStudyNr";
            this.labelStudyNr.Size = new System.Drawing.Size(78, 29);
            this.labelStudyNr.TabIndex = 64;
            this.labelStudyNr.Text = "-";
            this.labelStudyNr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelStudyNr.DoubleClick += new System.EventHandler(this.label22_DoubleClick);
            // 
            // label22
            // 
            this.label22.Dock = System.Windows.Forms.DockStyle.Left;
            this.label22.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label22.Location = new System.Drawing.Point(173, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(32, 29);
            this.label22.TabIndex = 63;
            this.label22.Text = "#";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label22.Click += new System.EventHandler(this.label22_Click);
            this.label22.DoubleClick += new System.EventHandler(this.label22_DoubleClick);
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.label15);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel12.Location = new System.Drawing.Point(3, 0);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(170, 29);
            this.panel12.TabIndex = 60;
            // 
            // label15
            // 
            this.label15.Dock = System.Windows.Forms.DockStyle.Left;
            this.label15.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(0, 0);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(170, 29);
            this.label15.TabIndex = 58;
            this.label15.Text = "Study information";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel11
            // 
            this.panel11.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel11.Location = new System.Drawing.Point(0, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(3, 29);
            this.panel11.TabIndex = 59;
            // 
            // panel13
            // 
            this.panel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel13.Location = new System.Drawing.Point(0, 29);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(1274, 1);
            this.panel13.TabIndex = 62;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.panel17);
            this.panel14.Controls.Add(this.panel16);
            this.panel14.Controls.Add(this.panel15);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel14.Location = new System.Drawing.Point(0, 30);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(1274, 302);
            this.panel14.TabIndex = 63;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.panel25);
            this.panel17.Controls.Add(this.panel8);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel17.Location = new System.Drawing.Point(3, 0);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(1271, 302);
            this.panel17.TabIndex = 58;
            // 
            // panel25
            // 
            this.panel25.Controls.Add(this.panel33);
            this.panel25.Location = new System.Drawing.Point(919, 0);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(348, 304);
            this.panel25.TabIndex = 56;
            // 
            // panel33
            // 
            this.panel33.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panel33.Controls.Add(this.labelDefRefIdMode);
            this.panel33.Controls.Add(this.panel10);
            this.panel33.Controls.Add(this.labelNoMct);
            this.panel33.Controls.Add(this.labelDeviceChanged);
            this.panel33.Controls.Add(this.label26);
            this.panel33.Controls.Add(this.labelDeviceClient);
            this.panel33.Controls.Add(this.label20);
            this.panel33.Controls.Add(this.labelDeviceRemLabel);
            this.panel33.Controls.Add(this.label16);
            this.panel33.Controls.Add(this.button1);
            this.panel33.Controls.Add(this.textBoxRefIdOther);
            this.panel33.Controls.Add(this.labelRecordEndDate);
            this.panel33.Controls.Add(this.labelRecorderStartDate);
            this.panel33.Controls.Add(this.radioButtonOther);
            this.panel33.Controls.Add(this.label12);
            this.panel33.Controls.Add(this.label17);
            this.panel33.Controls.Add(this.radioButtonUniqueID);
            this.panel33.Controls.Add(this.labelRecorderStudyNr);
            this.panel33.Controls.Add(this.labelRecorderState);
            this.panel33.Controls.Add(this.radioButtonPatID);
            this.panel33.Controls.Add(this.labelRecorderRefID);
            this.panel33.Controls.Add(this.label9);
            this.panel33.Controls.Add(this.radioButtonNone);
            this.panel33.Controls.Add(this.label11);
            this.panel33.Controls.Add(this.label14);
            this.panel33.Controls.Add(this.labelRecorderID);
            this.panel33.Controls.Add(this.label8);
            this.panel33.Controls.Add(this.listViewStudyProcedures);
            this.panel33.Controls.Add(this.buttonAddICD);
            this.panel33.Controls.Add(this.labelUseRefID);
            this.panel33.Location = new System.Drawing.Point(3, 4);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(343, 297);
            this.panel33.TabIndex = 0;
            // 
            // labelDefRefIdMode
            // 
            this.labelDefRefIdMode.AutoSize = true;
            this.labelDefRefIdMode.Font = new System.Drawing.Font("Arial", 8F);
            this.labelDefRefIdMode.Location = new System.Drawing.Point(262, 218);
            this.labelDefRefIdMode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelDefRefIdMode.Name = "labelDefRefIdMode";
            this.labelDefRefIdMode.Size = new System.Drawing.Size(79, 14);
            this.labelDefRefIdMode.TabIndex = 112;
            this.labelDefRefIdMode.Text = "^def: Unique ID";
            this.labelDefRefIdMode.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.label27);
            this.panel10.Controls.Add(this.labelAddStudyMode);
            this.panel10.Location = new System.Drawing.Point(133, 233);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(211, 19);
            this.panel10.TabIndex = 111;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Dock = System.Windows.Forms.DockStyle.Right;
            this.label27.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label27.Location = new System.Drawing.Point(15, 0);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(82, 16);
            this.label27.TabIndex = 111;
            this.label27.Text = "Add Study:";
            // 
            // labelAddStudyMode
            // 
            this.labelAddStudyMode.AutoSize = true;
            this.labelAddStudyMode.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelAddStudyMode.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelAddStudyMode.Location = new System.Drawing.Point(97, 0);
            this.labelAddStudyMode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelAddStudyMode.Name = "labelAddStudyMode";
            this.labelAddStudyMode.Size = new System.Drawing.Size(114, 16);
            this.labelAddStudyMode.TabIndex = 110;
            this.labelAddStudyMode.Text = "^Find PatientID";
            // 
            // labelNoMct
            // 
            this.labelNoMct.AutoSize = true;
            this.labelNoMct.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelNoMct.Location = new System.Drawing.Point(269, 254);
            this.labelNoMct.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelNoMct.Name = "labelNoMct";
            this.labelNoMct.Size = new System.Drawing.Size(72, 16);
            this.labelNoMct.TabIndex = 110;
            this.labelNoMct.Text = "^NO MCT";
            // 
            // labelDeviceChanged
            // 
            this.labelDeviceChanged.AutoSize = true;
            this.labelDeviceChanged.Font = new System.Drawing.Font("Arial", 8F);
            this.labelDeviceChanged.Location = new System.Drawing.Point(67, 218);
            this.labelDeviceChanged.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelDeviceChanged.Name = "labelDeviceChanged";
            this.labelDeviceChanged.Size = new System.Drawing.Size(189, 14);
            this.labelDeviceChanged.TabIndex = 107;
            this.labelDeviceChanged.Text = "99/99/9999 99:99:99  AM+1200 initials";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Arial", 8F);
            this.label26.Location = new System.Drawing.Point(7, 218);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(53, 14);
            this.label26.TabIndex = 106;
            this.label26.Text = "Changed:";
            // 
            // labelDeviceClient
            // 
            this.labelDeviceClient.AutoSize = true;
            this.labelDeviceClient.Font = new System.Drawing.Font("Arial", 10F);
            this.labelDeviceClient.Location = new System.Drawing.Point(64, 200);
            this.labelDeviceClient.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelDeviceClient.Name = "labelDeviceClient";
            this.labelDeviceClient.Size = new System.Drawing.Size(32, 16);
            this.labelDeviceClient.TabIndex = 105;
            this.labelDeviceClient.Text = "^Txt";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial", 10F);
            this.label20.Location = new System.Drawing.Point(7, 200);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(48, 16);
            this.label20.TabIndex = 104;
            this.label20.Text = "Client:";
            // 
            // labelDeviceRemLabel
            // 
            this.labelDeviceRemLabel.AutoSize = true;
            this.labelDeviceRemLabel.Font = new System.Drawing.Font("Arial", 10F);
            this.labelDeviceRemLabel.Location = new System.Drawing.Point(219, 143);
            this.labelDeviceRemLabel.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelDeviceRemLabel.Name = "labelDeviceRemLabel";
            this.labelDeviceRemLabel.Size = new System.Drawing.Size(32, 16);
            this.labelDeviceRemLabel.TabIndex = 103;
            this.labelDeviceRemLabel.Text = "^Txt";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 10F);
            this.label16.Location = new System.Drawing.Point(179, 143);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(41, 16);
            this.label16.TabIndex = 102;
            this.label16.Text = "Note:";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button1.Font = new System.Drawing.Font("Webdings", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Location = new System.Drawing.Point(308, 6);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(25, 26);
            this.button1.TabIndex = 101;
            this.button1.Text = "q";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBoxRefIdOther
            // 
            this.textBoxRefIdOther.Location = new System.Drawing.Point(164, 270);
            this.textBoxRefIdOther.MaxLength = 28;
            this.textBoxRefIdOther.Name = "textBoxRefIdOther";
            this.textBoxRefIdOther.Size = new System.Drawing.Size(170, 23);
            this.textBoxRefIdOther.TabIndex = 100;
            this.textBoxRefIdOther.Text = "^Txt";
            // 
            // labelRecordEndDate
            // 
            this.labelRecordEndDate.AutoSize = true;
            this.labelRecordEndDate.Font = new System.Drawing.Font("Arial", 10F);
            this.labelRecordEndDate.Location = new System.Drawing.Point(64, 181);
            this.labelRecordEndDate.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelRecordEndDate.Name = "labelRecordEndDate";
            this.labelRecordEndDate.Size = new System.Drawing.Size(207, 16);
            this.labelRecordEndDate.TabIndex = 95;
            this.labelRecordEndDate.Text = "99/99/9999 99:99:99  AM+1200";
            this.labelRecordEndDate.Click += new System.EventHandler(this.labelEndDate_Click);
            // 
            // labelRecorderStartDate
            // 
            this.labelRecorderStartDate.AutoSize = true;
            this.labelRecorderStartDate.Font = new System.Drawing.Font("Arial", 10F);
            this.labelRecorderStartDate.Location = new System.Drawing.Point(64, 162);
            this.labelRecorderStartDate.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelRecorderStartDate.Name = "labelRecorderStartDate";
            this.labelRecorderStartDate.Size = new System.Drawing.Size(207, 16);
            this.labelRecorderStartDate.TabIndex = 94;
            this.labelRecorderStartDate.Text = "99/99/9999 99:99:99  AM+1200";
            // 
            // radioButtonOther
            // 
            this.radioButtonOther.AutoSize = true;
            this.radioButtonOther.Font = new System.Drawing.Font("Arial", 10F);
            this.radioButtonOther.Location = new System.Drawing.Point(79, 271);
            this.radioButtonOther.Name = "radioButtonOther";
            this.radioButtonOther.Size = new System.Drawing.Size(66, 20);
            this.radioButtonOther.TabIndex = 99;
            this.radioButtonOther.Text = "Other:";
            this.radioButtonOther.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 10F);
            this.label12.Location = new System.Drawing.Point(7, 181);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(37, 16);
            this.label12.TabIndex = 93;
            this.label12.Text = "End:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 10F);
            this.label17.Location = new System.Drawing.Point(7, 162);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(42, 16);
            this.label17.TabIndex = 92;
            this.label17.Text = "Start:";
            // 
            // radioButtonUniqueID
            // 
            this.radioButtonUniqueID.AutoSize = true;
            this.radioButtonUniqueID.Font = new System.Drawing.Font("Arial", 10F);
            this.radioButtonUniqueID.Location = new System.Drawing.Point(79, 252);
            this.radioButtonUniqueID.Name = "radioButtonUniqueID";
            this.radioButtonUniqueID.Size = new System.Drawing.Size(87, 20);
            this.radioButtonUniqueID.TabIndex = 98;
            this.radioButtonUniqueID.Text = "Unique ID";
            this.radioButtonUniqueID.UseVisualStyleBackColor = true;
            // 
            // labelRecorderStudyNr
            // 
            this.labelRecorderStudyNr.AutoSize = true;
            this.labelRecorderStudyNr.Font = new System.Drawing.Font("Arial", 10F);
            this.labelRecorderStudyNr.Location = new System.Drawing.Point(244, 124);
            this.labelRecorderStudyNr.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelRecorderStudyNr.Name = "labelRecorderStudyNr";
            this.labelRecorderStudyNr.Size = new System.Drawing.Size(78, 16);
            this.labelRecorderStudyNr.TabIndex = 91;
            this.labelRecorderStudyNr.Text = "<Study Nr>";
            this.labelRecorderStudyNr.Click += new System.EventHandler(this.labelStudyNr_Click);
            // 
            // labelRecorderState
            // 
            this.labelRecorderState.AutoSize = true;
            this.labelRecorderState.Font = new System.Drawing.Font("Arial", 10F);
            this.labelRecorderState.Location = new System.Drawing.Point(64, 142);
            this.labelRecorderState.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelRecorderState.Name = "labelRecorderState";
            this.labelRecorderState.Size = new System.Drawing.Size(32, 16);
            this.labelRecorderState.TabIndex = 33;
            this.labelRecorderState.Text = "^Txt";
            // 
            // radioButtonPatID
            // 
            this.radioButtonPatID.AutoSize = true;
            this.radioButtonPatID.Checked = true;
            this.radioButtonPatID.Font = new System.Drawing.Font("Arial", 10F);
            this.radioButtonPatID.Location = new System.Drawing.Point(9, 271);
            this.radioButtonPatID.Name = "radioButtonPatID";
            this.radioButtonPatID.Size = new System.Drawing.Size(64, 20);
            this.radioButtonPatID.TabIndex = 97;
            this.radioButtonPatID.TabStop = true;
            this.radioButtonPatID.Text = "Pat.ID";
            this.radioButtonPatID.UseVisualStyleBackColor = true;
            this.radioButtonPatID.CheckedChanged += new System.EventHandler(this.radioButtonPatID_CheckedChanged);
            // 
            // labelRecorderRefID
            // 
            this.labelRecorderRefID.AutoSize = true;
            this.labelRecorderRefID.Font = new System.Drawing.Font("Arial", 10F);
            this.labelRecorderRefID.Location = new System.Drawing.Point(234, 200);
            this.labelRecorderRefID.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelRecorderRefID.Name = "labelRecorderRefID";
            this.labelRecorderRefID.Size = new System.Drawing.Size(32, 16);
            this.labelRecorderRefID.TabIndex = 90;
            this.labelRecorderRefID.Text = "^Txt";
            this.labelRecorderRefID.Click += new System.EventHandler(this.labelRefID_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 10F);
            this.label9.Location = new System.Drawing.Point(179, 124);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(66, 16);
            this.label9.TabIndex = 88;
            this.label9.Text = "Study Nr:";
            // 
            // radioButtonNone
            // 
            this.radioButtonNone.AutoSize = true;
            this.radioButtonNone.Font = new System.Drawing.Font("Arial", 10F);
            this.radioButtonNone.Location = new System.Drawing.Point(9, 252);
            this.radioButtonNone.Name = "radioButtonNone";
            this.radioButtonNone.Size = new System.Drawing.Size(59, 20);
            this.radioButtonNone.TabIndex = 96;
            this.radioButtonNone.Text = "None";
            this.radioButtonNone.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 10F);
            this.label11.Location = new System.Drawing.Point(179, 200);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(55, 16);
            this.label11.TabIndex = 87;
            this.label11.Text = "Ref. ID:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 10F);
            this.label14.Location = new System.Drawing.Point(7, 142);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(45, 16);
            this.label14.TabIndex = 27;
            this.label14.Text = "State:";
            // 
            // labelRecorderID
            // 
            this.labelRecorderID.AutoSize = true;
            this.labelRecorderID.Font = new System.Drawing.Font("Arial", 10F);
            this.labelRecorderID.Location = new System.Drawing.Point(64, 122);
            this.labelRecorderID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelRecorderID.Name = "labelRecorderID";
            this.labelRecorderID.Size = new System.Drawing.Size(32, 16);
            this.labelRecorderID.TabIndex = 83;
            this.labelRecorderID.Text = "^Txt";
            this.labelRecorderID.Click += new System.EventHandler(this.labelRecorderID_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 10F);
            this.label8.Location = new System.Drawing.Point(7, 122);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 16);
            this.label8.TabIndex = 82;
            this.label8.Text = "Serial:";
            // 
            // listViewStudyProcedures
            // 
            this.listViewStudyProcedures.CheckBoxes = true;
            this.listViewStudyProcedures.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.listViewStudyProcedures.FullRowSelect = true;
            this.listViewStudyProcedures.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.listViewStudyProcedures.Location = new System.Drawing.Point(7, 37);
            this.listViewStudyProcedures.Name = "listViewStudyProcedures";
            this.listViewStudyProcedures.Size = new System.Drawing.Size(327, 84);
            this.listViewStudyProcedures.TabIndex = 81;
            this.listViewStudyProcedures.UseCompatibleStateImageBehavior = false;
            this.listViewStudyProcedures.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Code";
            this.columnHeader1.Width = 80;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Description";
            this.columnHeader2.Width = 390;
            // 
            // buttonAddICD
            // 
            this.buttonAddICD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonAddICD.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonAddICD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddICD.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonAddICD.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonAddICD.Location = new System.Drawing.Point(7, 5);
            this.buttonAddICD.Margin = new System.Windows.Forms.Padding(4);
            this.buttonAddICD.Name = "buttonAddICD";
            this.buttonAddICD.Size = new System.Drawing.Size(287, 25);
            this.buttonAddICD.TabIndex = 30;
            this.buttonAddICD.Text = "Select ICD10 Code(s)";
            this.buttonAddICD.UseVisualStyleBackColor = false;
            this.buttonAddICD.Click += new System.EventHandler(this.buttonAddICD_Click);
            // 
            // labelUseRefID
            // 
            this.labelUseRefID.AutoSize = true;
            this.labelUseRefID.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelUseRefID.Location = new System.Drawing.Point(9, 234);
            this.labelUseRefID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelUseRefID.Name = "labelUseRefID";
            this.labelUseRefID.Size = new System.Drawing.Size(109, 16);
            this.labelUseRefID.TabIndex = 85;
            this.labelUseRefID.Text = "Use for Ref ID:";
            this.labelUseRefID.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.label10_MouseDoubleClick);
            // 
            // panel16
            // 
            this.panel16.Location = new System.Drawing.Point(1656, 0);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(37, 297);
            this.panel16.TabIndex = 57;
            // 
            // panel15
            // 
            this.panel15.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel15.Location = new System.Drawing.Point(0, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(3, 302);
            this.panel15.TabIndex = 56;
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.panel21);
            this.panel18.Controls.Add(this.panel20);
            this.panel18.Controls.Add(this.panel19);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel18.Location = new System.Drawing.Point(0, 332);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(1274, 26);
            this.panel18.TabIndex = 64;
            // 
            // panel21
            // 
            this.panel21.Controls.Add(this.checkBoxAnonymizePatientInfo);
            this.panel21.Controls.Add(this.panel3);
            this.panel21.Controls.Add(this.labelPatientError);
            this.panel21.Controls.Add(this.labelPatientNr);
            this.panel21.Controls.Add(this.label5);
            this.panel21.Controls.Add(this.label13);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel21.Location = new System.Drawing.Point(3, 0);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(1271, 26);
            this.panel21.TabIndex = 2;
            // 
            // checkBoxAnonymizePatientInfo
            // 
            this.checkBoxAnonymizePatientInfo.AutoSize = true;
            this.checkBoxAnonymizePatientInfo.Dock = System.Windows.Forms.DockStyle.Right;
            this.checkBoxAnonymizePatientInfo.Location = new System.Drawing.Point(920, 0);
            this.checkBoxAnonymizePatientInfo.Name = "checkBoxAnonymizePatientInfo";
            this.checkBoxAnonymizePatientInfo.Size = new System.Drawing.Size(171, 26);
            this.checkBoxAnonymizePatientInfo.TabIndex = 62;
            this.checkBoxAnonymizePatientInfo.Text = "Anonymize Patient Info";
            this.checkBoxAnonymizePatientInfo.UseVisualStyleBackColor = true;
            this.checkBoxAnonymizePatientInfo.CheckStateChanged += new System.EventHandler(this.checkBoxAnonymizePatientInfo_CheckStateChanged);
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(1091, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(180, 26);
            this.panel3.TabIndex = 61;
            // 
            // labelPatientError
            // 
            this.labelPatientError.AutoSize = true;
            this.labelPatientError.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPatientError.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelPatientError.Location = new System.Drawing.Point(273, 0);
            this.labelPatientError.Name = "labelPatientError";
            this.labelPatientError.Size = new System.Drawing.Size(25, 19);
            this.labelPatientError.TabIndex = 60;
            this.labelPatientError.Text = "....";
            this.labelPatientError.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelPatientNr
            // 
            this.labelPatientNr.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPatientNr.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelPatientNr.Location = new System.Drawing.Point(195, 0);
            this.labelPatientNr.Name = "labelPatientNr";
            this.labelPatientNr.Size = new System.Drawing.Size(78, 26);
            this.labelPatientNr.TabIndex = 59;
            this.labelPatientNr.Text = "-";
            this.labelPatientNr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Left;
            this.label5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(163, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 26);
            this.label5.TabIndex = 58;
            this.label5.Text = "  #";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel20
            // 
            this.panel20.Location = new System.Drawing.Point(1656, 0);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(37, 23);
            this.panel20.TabIndex = 1;
            // 
            // panel19
            // 
            this.panel19.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel19.Location = new System.Drawing.Point(0, 0);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(3, 26);
            this.panel19.TabIndex = 0;
            // 
            // panel23
            // 
            this.panel23.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel23.Controls.Add(this.panelButtons);
            this.panel23.Controls.Add(this.panel31);
            this.panel23.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel23.Location = new System.Drawing.Point(0, 664);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(1274, 37);
            this.panel23.TabIndex = 66;
            // 
            // panelButtons
            // 
            this.panelButtons.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelButtons.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.panelButtons.Controls.Add(this.buttonHolter);
            this.panelButtons.Controls.Add(this.buttonRecordsInfo);
            this.panelButtons.Controls.Add(this.buttonCreateReport);
            this.panelButtons.Controls.Add(this.buttonEndRecording);
            this.panelButtons.Controls.Add(this.buttonViewPdf);
            this.panelButtons.Controls.Add(this.buttonClear);
            this.panelButtons.Controls.Add(this.buttonDup);
            this.panelButtons.Controls.Add(this.buttonAddUpdate);
            this.panelButtons.Controls.Add(this.buttonPrintPreviousFindings);
            this.panelButtons.Controls.Add(this.buttonMCT);
            this.panelButtons.Controls.Add(this.buttonDeviceList);
            this.panelButtons.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelButtons.Location = new System.Drawing.Point(0, 0);
            this.panelButtons.Name = "panelButtons";
            this.panelButtons.Size = new System.Drawing.Size(1274, 37);
            this.panelButtons.TabIndex = 3;
            // 
            // buttonHolter
            // 
            this.buttonHolter.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonHolter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonHolter.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonHolter.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonHolter.FlatAppearance.BorderSize = 0;
            this.buttonHolter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonHolter.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonHolter.ForeColor = System.Drawing.Color.White;
            this.buttonHolter.Location = new System.Drawing.Point(601, 0);
            this.buttonHolter.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonHolter.Name = "buttonHolter";
            this.buttonHolter.Size = new System.Drawing.Size(83, 37);
            this.buttonHolter.TabIndex = 27;
            this.buttonHolter.Text = "Holter";
            this.buttonHolter.UseVisualStyleBackColor = false;
            this.buttonHolter.Click += new System.EventHandler(this.buttonHolter_Click);
            // 
            // buttonRecordsInfo
            // 
            this.buttonRecordsInfo.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonRecordsInfo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonRecordsInfo.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonRecordsInfo.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonRecordsInfo.FlatAppearance.BorderSize = 0;
            this.buttonRecordsInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRecordsInfo.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonRecordsInfo.ForeColor = System.Drawing.Color.White;
            this.buttonRecordsInfo.Location = new System.Drawing.Point(689, 0);
            this.buttonRecordsInfo.Margin = new System.Windows.Forms.Padding(2);
            this.buttonRecordsInfo.Name = "buttonRecordsInfo";
            this.buttonRecordsInfo.Size = new System.Drawing.Size(134, 37);
            this.buttonRecordsInfo.TabIndex = 26;
            this.buttonRecordsInfo.Text = "Records Info";
            this.buttonRecordsInfo.UseVisualStyleBackColor = false;
            this.buttonRecordsInfo.Click += new System.EventHandler(this.buttonRecordsInfo_Click);
            // 
            // buttonCreateReport
            // 
            this.buttonCreateReport.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonCreateReport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonCreateReport.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonCreateReport.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonCreateReport.FlatAppearance.BorderSize = 0;
            this.buttonCreateReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCreateReport.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonCreateReport.ForeColor = System.Drawing.Color.White;
            this.buttonCreateReport.Location = new System.Drawing.Point(473, 0);
            this.buttonCreateReport.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonCreateReport.Name = "buttonCreateReport";
            this.buttonCreateReport.Size = new System.Drawing.Size(128, 37);
            this.buttonCreateReport.TabIndex = 15;
            this.buttonCreateReport.Text = "Create Report";
            this.buttonCreateReport.UseVisualStyleBackColor = false;
            this.buttonCreateReport.Click += new System.EventHandler(this.buttonFindings_Click);
            // 
            // buttonEndRecording
            // 
            this.buttonEndRecording.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonEndRecording.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonEndRecording.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonEndRecording.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonEndRecording.FlatAppearance.BorderSize = 0;
            this.buttonEndRecording.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEndRecording.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonEndRecording.ForeColor = System.Drawing.Color.White;
            this.buttonEndRecording.Location = new System.Drawing.Point(335, 0);
            this.buttonEndRecording.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonEndRecording.Name = "buttonEndRecording";
            this.buttonEndRecording.Size = new System.Drawing.Size(138, 37);
            this.buttonEndRecording.TabIndex = 20;
            this.buttonEndRecording.Text = "End recording";
            this.buttonEndRecording.UseVisualStyleBackColor = false;
            this.buttonEndRecording.Click += new System.EventHandler(this.buttonEndRecording_Click);
            // 
            // buttonViewPdf
            // 
            this.buttonViewPdf.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonViewPdf.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonViewPdf.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonViewPdf.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonViewPdf.FlatAppearance.BorderSize = 0;
            this.buttonViewPdf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonViewPdf.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonViewPdf.ForeColor = System.Drawing.Color.White;
            this.buttonViewPdf.Location = new System.Drawing.Point(223, 0);
            this.buttonViewPdf.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonViewPdf.Name = "buttonViewPdf";
            this.buttonViewPdf.Size = new System.Drawing.Size(112, 37);
            this.buttonViewPdf.TabIndex = 25;
            this.buttonViewPdf.Text = "Report List";
            this.buttonViewPdf.UseVisualStyleBackColor = false;
            this.buttonViewPdf.Click += new System.EventHandler(this.buttonViewPdf_Click);
            // 
            // buttonClear
            // 
            this.buttonClear.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonClear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonClear.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonClear.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonClear.FlatAppearance.BorderSize = 0;
            this.buttonClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonClear.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonClear.ForeColor = System.Drawing.Color.White;
            this.buttonClear.Location = new System.Drawing.Point(823, 0);
            this.buttonClear.Margin = new System.Windows.Forms.Padding(2);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(109, 37);
            this.buttonClear.TabIndex = 10;
            this.buttonClear.Text = "Clear All";
            this.buttonClear.UseVisualStyleBackColor = false;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // buttonDup
            // 
            this.buttonDup.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonDup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonDup.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonDup.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonDup.FlatAppearance.BorderSize = 0;
            this.buttonDup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDup.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonDup.ForeColor = System.Drawing.Color.White;
            this.buttonDup.Location = new System.Drawing.Point(932, 0);
            this.buttonDup.Margin = new System.Windows.Forms.Padding(2);
            this.buttonDup.Name = "buttonDup";
            this.buttonDup.Size = new System.Drawing.Size(160, 37);
            this.buttonDup.TabIndex = 11;
            this.buttonDup.Text = "Duplicate Study";
            this.buttonDup.UseVisualStyleBackColor = false;
            this.buttonDup.Click += new System.EventHandler(this.buttonDup_Click);
            // 
            // buttonAddUpdate
            // 
            this.buttonAddUpdate.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonAddUpdate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonAddUpdate.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonAddUpdate.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonAddUpdate.FlatAppearance.BorderSize = 0;
            this.buttonAddUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddUpdate.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonAddUpdate.ForeColor = System.Drawing.Color.White;
            this.buttonAddUpdate.Location = new System.Drawing.Point(1092, 0);
            this.buttonAddUpdate.Margin = new System.Windows.Forms.Padding(2);
            this.buttonAddUpdate.Name = "buttonAddUpdate";
            this.buttonAddUpdate.Size = new System.Drawing.Size(108, 37);
            this.buttonAddUpdate.TabIndex = 1;
            this.buttonAddUpdate.Text = "Add";
            this.buttonAddUpdate.UseVisualStyleBackColor = false;
            this.buttonAddUpdate.Click += new System.EventHandler(this.buttonAddUpdate_Click);
            // 
            // buttonPrintPreviousFindings
            // 
            this.buttonPrintPreviousFindings.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonPrintPreviousFindings.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonPrintPreviousFindings.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonPrintPreviousFindings.FlatAppearance.BorderColor = System.Drawing.Color.SlateBlue;
            this.buttonPrintPreviousFindings.FlatAppearance.BorderSize = 0;
            this.buttonPrintPreviousFindings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPrintPreviousFindings.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonPrintPreviousFindings.ForeColor = System.Drawing.Color.White;
            this.buttonPrintPreviousFindings.Location = new System.Drawing.Point(1200, 0);
            this.buttonPrintPreviousFindings.Margin = new System.Windows.Forms.Padding(0);
            this.buttonPrintPreviousFindings.Name = "buttonPrintPreviousFindings";
            this.buttonPrintPreviousFindings.Size = new System.Drawing.Size(74, 37);
            this.buttonPrintPreviousFindings.TabIndex = 1;
            this.buttonPrintPreviousFindings.Text = "Close";
            this.buttonPrintPreviousFindings.UseVisualStyleBackColor = false;
            this.buttonPrintPreviousFindings.Click += new System.EventHandler(this.buttonPrintPreviousFindings_Click);
            // 
            // buttonMCT
            // 
            this.buttonMCT.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonMCT.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonMCT.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonMCT.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonMCT.FlatAppearance.BorderSize = 0;
            this.buttonMCT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMCT.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonMCT.ForeColor = System.Drawing.Color.White;
            this.buttonMCT.Location = new System.Drawing.Point(113, 0);
            this.buttonMCT.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonMCT.Name = "buttonMCT";
            this.buttonMCT.Size = new System.Drawing.Size(110, 37);
            this.buttonMCT.TabIndex = 23;
            this.buttonMCT.Text = "MCT Trend";
            this.buttonMCT.UseVisualStyleBackColor = false;
            this.buttonMCT.Click += new System.EventHandler(this.buttonMCT_Click);
            // 
            // buttonDeviceList
            // 
            this.buttonDeviceList.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonDeviceList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonDeviceList.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonDeviceList.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonDeviceList.FlatAppearance.BorderSize = 0;
            this.buttonDeviceList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDeviceList.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonDeviceList.ForeColor = System.Drawing.Color.White;
            this.buttonDeviceList.Location = new System.Drawing.Point(0, 0);
            this.buttonDeviceList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonDeviceList.Name = "buttonDeviceList";
            this.buttonDeviceList.Size = new System.Drawing.Size(113, 37);
            this.buttonDeviceList.TabIndex = 17;
            this.buttonDeviceList.Text = "Device List";
            this.buttonDeviceList.UseVisualStyleBackColor = false;
            this.buttonDeviceList.Click += new System.EventHandler(this.buttonDeviceList_Click);
            // 
            // panel31
            // 
            this.panel31.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.panel31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel31.Location = new System.Drawing.Point(0, 0);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(1274, 37);
            this.panel31.TabIndex = 2;
            // 
            // panel34
            // 
            this.panel34.Controls.Add(this.panel29);
            this.panel34.Controls.Add(this.panel6);
            this.panel34.Controls.Add(this.panel7);
            this.panel34.Location = new System.Drawing.Point(3, 0);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(1265, 274);
            this.panel34.TabIndex = 3;
            // 
            // panel29
            // 
            this.panel29.Controls.Add(this.panel37);
            this.panel29.Location = new System.Drawing.Point(920, 3);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(345, 273);
            this.panel29.TabIndex = 57;
            // 
            // panel37
            // 
            this.panel37.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panel37.Controls.Add(this.checkedListBoxSymptomsList);
            this.panel37.Controls.Add(this.labelPatientHistory);
            this.panel37.Controls.Add(this.labelPatientSymptoms);
            this.panel37.Controls.Add(this.checkedListBoxMedicationList);
            this.panel37.Controls.Add(this.checkedListBoxAllergiesList);
            this.panel37.Controls.Add(this.checkedListBoxHistoryList);
            this.panel37.Controls.Add(this.labelPatientAllergies);
            this.panel37.Controls.Add(this.labelPatientMedication);
            this.panel37.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel37.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel37.Location = new System.Drawing.Point(0, 0);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(345, 273);
            this.panel37.TabIndex = 0;
            // 
            // checkedListBoxSymptomsList
            // 
            this.checkedListBoxSymptomsList.BackColor = System.Drawing.SystemColors.Window;
            this.checkedListBoxSymptomsList.CheckOnClick = true;
            this.checkedListBoxSymptomsList.ColumnWidth = 225;
            this.checkedListBoxSymptomsList.Font = new System.Drawing.Font("Arial", 10F);
            this.checkedListBoxSymptomsList.FormattingEnabled = true;
            this.checkedListBoxSymptomsList.IntegralHeight = false;
            this.checkedListBoxSymptomsList.Items.AddRange(new object[] {
            "Chest pain",
            "Dizziness",
            "Fainting",
            "Fatique",
            "Palpitations",
            "Syncope"});
            this.checkedListBoxSymptomsList.Location = new System.Drawing.Point(171, 153);
            this.checkedListBoxSymptomsList.Margin = new System.Windows.Forms.Padding(4);
            this.checkedListBoxSymptomsList.Name = "checkedListBoxSymptomsList";
            this.checkedListBoxSymptomsList.Size = new System.Drawing.Size(165, 112);
            this.checkedListBoxSymptomsList.TabIndex = 86;
            // 
            // labelPatientHistory
            // 
            this.labelPatientHistory.AutoSize = true;
            this.labelPatientHistory.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelPatientHistory.Location = new System.Drawing.Point(6, 1);
            this.labelPatientHistory.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPatientHistory.Name = "labelPatientHistory";
            this.labelPatientHistory.Size = new System.Drawing.Size(61, 16);
            this.labelPatientHistory.TabIndex = 79;
            this.labelPatientHistory.Text = "History:";
            // 
            // labelPatientSymptoms
            // 
            this.labelPatientSymptoms.AutoSize = true;
            this.labelPatientSymptoms.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelPatientSymptoms.Location = new System.Drawing.Point(168, 135);
            this.labelPatientSymptoms.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPatientSymptoms.Name = "labelPatientSymptoms";
            this.labelPatientSymptoms.Size = new System.Drawing.Size(83, 16);
            this.labelPatientSymptoms.TabIndex = 85;
            this.labelPatientSymptoms.Text = "Symptoms:";
            // 
            // checkedListBoxMedicationList
            // 
            this.checkedListBoxMedicationList.BackColor = System.Drawing.SystemColors.Window;
            this.checkedListBoxMedicationList.CheckOnClick = true;
            this.checkedListBoxMedicationList.ColumnWidth = 225;
            this.checkedListBoxMedicationList.Font = new System.Drawing.Font("Arial", 10F);
            this.checkedListBoxMedicationList.FormattingEnabled = true;
            this.checkedListBoxMedicationList.IntegralHeight = false;
            this.checkedListBoxMedicationList.Items.AddRange(new object[] {
            "Antibiotics",
            "Aspirin",
            "Betablocker",
            "Nitroglycerine",
            "NSAID\'s",
            "Steroid"});
            this.checkedListBoxMedicationList.Location = new System.Drawing.Point(9, 153);
            this.checkedListBoxMedicationList.Margin = new System.Windows.Forms.Padding(4);
            this.checkedListBoxMedicationList.Name = "checkedListBoxMedicationList";
            this.checkedListBoxMedicationList.Size = new System.Drawing.Size(149, 112);
            this.checkedListBoxMedicationList.TabIndex = 84;
            this.checkedListBoxMedicationList.SelectedIndexChanged += new System.EventHandler(this.checkedListBoxMedicationList_SelectedIndexChanged);
            // 
            // checkedListBoxAllergiesList
            // 
            this.checkedListBoxAllergiesList.BackColor = System.Drawing.SystemColors.Window;
            this.checkedListBoxAllergiesList.CheckOnClick = true;
            this.checkedListBoxAllergiesList.ColumnWidth = 225;
            this.checkedListBoxAllergiesList.Font = new System.Drawing.Font("Arial", 10F);
            this.checkedListBoxAllergiesList.FormattingEnabled = true;
            this.checkedListBoxAllergiesList.IntegralHeight = false;
            this.checkedListBoxAllergiesList.Items.AddRange(new object[] {
            "Antibiotics",
            "Insuline",
            "Peanuts"});
            this.checkedListBoxAllergiesList.Location = new System.Drawing.Point(171, 18);
            this.checkedListBoxAllergiesList.Margin = new System.Windows.Forms.Padding(4);
            this.checkedListBoxAllergiesList.Name = "checkedListBoxAllergiesList";
            this.checkedListBoxAllergiesList.Size = new System.Drawing.Size(165, 115);
            this.checkedListBoxAllergiesList.TabIndex = 82;
            // 
            // checkedListBoxHistoryList
            // 
            this.checkedListBoxHistoryList.BackColor = System.Drawing.SystemColors.Window;
            this.checkedListBoxHistoryList.CheckOnClick = true;
            this.checkedListBoxHistoryList.ColumnWidth = 225;
            this.checkedListBoxHistoryList.Font = new System.Drawing.Font("Arial", 10F);
            this.checkedListBoxHistoryList.FormattingEnabled = true;
            this.checkedListBoxHistoryList.IntegralHeight = false;
            this.checkedListBoxHistoryList.Items.AddRange(new object[] {
            "Alcohol",
            "ICD",
            "Myocardial Infarct",
            "Pacemaker",
            "PCA / Stenting",
            "Smoking"});
            this.checkedListBoxHistoryList.Location = new System.Drawing.Point(9, 18);
            this.checkedListBoxHistoryList.Margin = new System.Windows.Forms.Padding(4);
            this.checkedListBoxHistoryList.Name = "checkedListBoxHistoryList";
            this.checkedListBoxHistoryList.Size = new System.Drawing.Size(149, 115);
            this.checkedListBoxHistoryList.TabIndex = 80;
            // 
            // labelPatientAllergies
            // 
            this.labelPatientAllergies.AutoSize = true;
            this.labelPatientAllergies.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelPatientAllergies.Location = new System.Drawing.Point(168, 1);
            this.labelPatientAllergies.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPatientAllergies.Name = "labelPatientAllergies";
            this.labelPatientAllergies.Size = new System.Drawing.Size(74, 16);
            this.labelPatientAllergies.TabIndex = 81;
            this.labelPatientAllergies.Text = "Allergies:";
            // 
            // labelPatientMedication
            // 
            this.labelPatientMedication.AutoSize = true;
            this.labelPatientMedication.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelPatientMedication.Location = new System.Drawing.Point(8, 136);
            this.labelPatientMedication.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPatientMedication.Name = "labelPatientMedication";
            this.labelPatientMedication.Size = new System.Drawing.Size(90, 16);
            this.labelPatientMedication.TabIndex = 83;
            this.labelPatientMedication.Text = "Medication:";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panel6.Controls.Add(this.buttonPatientForm);
            this.panel6.Controls.Add(this.buttonClearPatient);
            this.panel6.Controls.Add(this.buttonDobNA);
            this.panel6.Controls.Add(this.label3);
            this.panel6.Controls.Add(this.textBoxPatientLabel);
            this.panel6.Controls.Add(this.labelUnitWeight);
            this.panel6.Controls.Add(this.labelUnitHeightMinor);
            this.panel6.Controls.Add(this.textBoxPatientHeightMinor);
            this.panel6.Controls.Add(this.labelUnitHeightMajor);
            this.panel6.Controls.Add(this.textBoxAge);
            this.panel6.Controls.Add(this.labelPatientAge);
            this.panel6.Controls.Add(this.buttonSearchPatientID);
            this.panel6.Controls.Add(this.comboBoxPatientRaceList);
            this.panel6.Controls.Add(this.labelPatientRace);
            this.panel6.Controls.Add(this.textBoxPatientWeight);
            this.panel6.Controls.Add(this.labelPatientWeight);
            this.panel6.Controls.Add(this.textBoxPatientHeightMajor);
            this.panel6.Controls.Add(this.labelPatientHeight);
            this.panel6.Controls.Add(this.textBoxPatientID);
            this.panel6.Controls.Add(this.labelPatientID);
            this.panel6.Controls.Add(this.textBoxPatientDOBYear);
            this.panel6.Controls.Add(this.comboBoxPatientDOBMonth);
            this.panel6.Controls.Add(this.textBoxPatientDOBDay);
            this.panel6.Controls.Add(this.labelPatientDOB);
            this.panel6.Controls.Add(this.comboBoxPatientGenderList);
            this.panel6.Controls.Add(this.labelPatientGender);
            this.panel6.Controls.Add(this.textBoxPatientLastName);
            this.panel6.Controls.Add(this.labelPatientLastName);
            this.panel6.Controls.Add(this.textBoxPatientMiddleName);
            this.panel6.Controls.Add(this.labelPatientMiddleName);
            this.panel6.Controls.Add(this.textBoxPatientFirstName);
            this.panel6.Controls.Add(this.labelPatientFirstName);
            this.panel6.Controls.Add(this.labelSocSecNr);
            this.panel6.Controls.Add(this.textBoxSocSecNr);
            this.panel6.Font = new System.Drawing.Font("Arial", 10F);
            this.panel6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel6.Location = new System.Drawing.Point(3, 1);
            this.panel6.Margin = new System.Windows.Forms.Padding(4);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(448, 275);
            this.panel6.TabIndex = 5;
            // 
            // buttonPatientForm
            // 
            this.buttonPatientForm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonPatientForm.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonPatientForm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPatientForm.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonPatientForm.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonPatientForm.Location = new System.Drawing.Point(143, 227);
            this.buttonPatientForm.Margin = new System.Windows.Forms.Padding(4);
            this.buttonPatientForm.Name = "buttonPatientForm";
            this.buttonPatientForm.Size = new System.Drawing.Size(158, 35);
            this.buttonPatientForm.TabIndex = 65;
            this.buttonPatientForm.Text = "Search Patient";
            this.buttonPatientForm.UseVisualStyleBackColor = false;
            this.buttonPatientForm.Click += new System.EventHandler(this.buttonPatientForm_Click);
            // 
            // buttonClearPatient
            // 
            this.buttonClearPatient.BackColor = System.Drawing.Color.Transparent;
            this.buttonClearPatient.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonClearPatient.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonClearPatient.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonClearPatient.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonClearPatient.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonClearPatient.Location = new System.Drawing.Point(8, 227);
            this.buttonClearPatient.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonClearPatient.Name = "buttonClearPatient";
            this.buttonClearPatient.Size = new System.Drawing.Size(116, 35);
            this.buttonClearPatient.TabIndex = 64;
            this.buttonClearPatient.Text = "Clear Patient";
            this.buttonClearPatient.UseVisualStyleBackColor = false;
            this.buttonClearPatient.Click += new System.EventHandler(this.buttonClearPatient_Click_1);
            // 
            // buttonDobNA
            // 
            this.buttonDobNA.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonDobNA.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonDobNA.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonDobNA.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDobNA.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonDobNA.ImageAlign = System.Drawing.ContentAlignment.BottomRight;
            this.buttonDobNA.Location = new System.Drawing.Point(313, 107);
            this.buttonDobNA.Margin = new System.Windows.Forms.Padding(0);
            this.buttonDobNA.Name = "buttonDobNA";
            this.buttonDobNA.Size = new System.Drawing.Size(35, 21);
            this.buttonDobNA.TabIndex = 63;
            this.buttonDobNA.Text = "NA";
            this.buttonDobNA.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.buttonDobNA.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.buttonDobNA.UseVisualStyleBackColor = false;
            this.buttonDobNA.Click += new System.EventHandler(this.buttonDobNA_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 201);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 16);
            this.label3.TabIndex = 62;
            this.label3.Text = "Pat. note:";
            // 
            // textBoxPatientLabel
            // 
            this.textBoxPatientLabel.Location = new System.Drawing.Point(87, 198);
            this.textBoxPatientLabel.MaxLength = 18;
            this.textBoxPatientLabel.Name = "textBoxPatientLabel";
            this.textBoxPatientLabel.Size = new System.Drawing.Size(181, 23);
            this.textBoxPatientLabel.TabIndex = 61;
            this.textBoxPatientLabel.Text = "^Txt";
            // 
            // labelUnitWeight
            // 
            this.labelUnitWeight.AutoSize = true;
            this.labelUnitWeight.Location = new System.Drawing.Point(409, 171);
            this.labelUnitWeight.Name = "labelUnitWeight";
            this.labelUnitWeight.Size = new System.Drawing.Size(26, 16);
            this.labelUnitWeight.TabIndex = 60;
            this.labelUnitWeight.Text = "lbs";
            // 
            // labelUnitHeightMinor
            // 
            this.labelUnitHeightMinor.AutoSize = true;
            this.labelUnitHeightMinor.Location = new System.Drawing.Point(237, 171);
            this.labelUnitHeightMinor.Name = "labelUnitHeightMinor";
            this.labelUnitHeightMinor.Size = new System.Drawing.Size(34, 16);
            this.labelUnitHeightMinor.TabIndex = 59;
            this.labelUnitHeightMinor.Text = "inch";
            // 
            // textBoxPatientHeightMinor
            // 
            this.textBoxPatientHeightMinor.Location = new System.Drawing.Point(182, 168);
            this.textBoxPatientHeightMinor.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPatientHeightMinor.Name = "textBoxPatientHeightMinor";
            this.textBoxPatientHeightMinor.Size = new System.Drawing.Size(50, 23);
            this.textBoxPatientHeightMinor.TabIndex = 58;
            this.textBoxPatientHeightMinor.Text = "^Txt";
            // 
            // labelUnitHeightMajor
            // 
            this.labelUnitHeightMajor.AutoSize = true;
            this.labelUnitHeightMajor.Location = new System.Drawing.Point(148, 171);
            this.labelUnitHeightMajor.Name = "labelUnitHeightMajor";
            this.labelUnitHeightMajor.Size = new System.Drawing.Size(32, 16);
            this.labelUnitHeightMajor.TabIndex = 57;
            this.labelUnitHeightMajor.Text = "feet";
            // 
            // textBoxAge
            // 
            this.textBoxAge.Location = new System.Drawing.Point(396, 106);
            this.textBoxAge.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxAge.Name = "textBoxAge";
            this.textBoxAge.ReadOnly = true;
            this.textBoxAge.Size = new System.Drawing.Size(34, 23);
            this.textBoxAge.TabIndex = 56;
            this.textBoxAge.Text = "199";
            this.textBoxAge.Click += new System.EventHandler(this.textBoxAge_Click);
            // 
            // labelPatientAge
            // 
            this.labelPatientAge.AutoSize = true;
            this.labelPatientAge.Location = new System.Drawing.Point(355, 109);
            this.labelPatientAge.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPatientAge.Name = "labelPatientAge";
            this.labelPatientAge.Size = new System.Drawing.Size(37, 16);
            this.labelPatientAge.TabIndex = 45;
            this.labelPatientAge.Text = "Age:";
            // 
            // buttonSearchPatientID
            // 
            this.buttonSearchPatientID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonSearchPatientID.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonSearchPatientID.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSearchPatientID.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonSearchPatientID.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonSearchPatientID.Location = new System.Drawing.Point(313, 227);
            this.buttonSearchPatientID.Margin = new System.Windows.Forms.Padding(4);
            this.buttonSearchPatientID.Name = "buttonSearchPatientID";
            this.buttonSearchPatientID.Size = new System.Drawing.Size(119, 35);
            this.buttonSearchPatientID.TabIndex = 55;
            this.buttonSearchPatientID.Text = "Search ID";
            this.buttonSearchPatientID.UseVisualStyleBackColor = false;
            this.buttonSearchPatientID.Click += new System.EventHandler(this.buttonSearchPatientID_Click);
            // 
            // comboBoxPatientRaceList
            // 
            this.comboBoxPatientRaceList.DropDownWidth = 200;
            this.comboBoxPatientRaceList.Enabled = false;
            this.comboBoxPatientRaceList.FormattingEnabled = true;
            this.comboBoxPatientRaceList.Items.AddRange(new object[] {
            "Caucasian",
            "Asian",
            "African",
            "Hispanic",
            "Indigenous Australian",
            "Other"});
            this.comboBoxPatientRaceList.Location = new System.Drawing.Point(309, 136);
            this.comboBoxPatientRaceList.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxPatientRaceList.Name = "comboBoxPatientRaceList";
            this.comboBoxPatientRaceList.Size = new System.Drawing.Size(121, 24);
            this.comboBoxPatientRaceList.TabIndex = 50;
            this.comboBoxPatientRaceList.Visible = false;
            this.comboBoxPatientRaceList.SelectedIndexChanged += new System.EventHandler(this.comboBox3_SelectedIndexChanged);
            // 
            // labelPatientRace
            // 
            this.labelPatientRace.AutoSize = true;
            this.labelPatientRace.Location = new System.Drawing.Point(242, 139);
            this.labelPatientRace.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPatientRace.Name = "labelPatientRace";
            this.labelPatientRace.Size = new System.Drawing.Size(65, 16);
            this.labelPatientRace.TabIndex = 49;
            this.labelPatientRace.Text = "Ethnicity:";
            this.labelPatientRace.Visible = false;
            this.labelPatientRace.Click += new System.EventHandler(this.label11_Click);
            // 
            // textBoxPatientWeight
            // 
            this.textBoxPatientWeight.Location = new System.Drawing.Point(354, 168);
            this.textBoxPatientWeight.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPatientWeight.Name = "textBoxPatientWeight";
            this.textBoxPatientWeight.Size = new System.Drawing.Size(51, 23);
            this.textBoxPatientWeight.TabIndex = 54;
            // 
            // labelPatientWeight
            // 
            this.labelPatientWeight.AutoSize = true;
            this.labelPatientWeight.Location = new System.Drawing.Point(295, 171);
            this.labelPatientWeight.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPatientWeight.Name = "labelPatientWeight";
            this.labelPatientWeight.Size = new System.Drawing.Size(56, 16);
            this.labelPatientWeight.TabIndex = 53;
            this.labelPatientWeight.Text = "Weight:";
            // 
            // textBoxPatientHeightMajor
            // 
            this.textBoxPatientHeightMajor.Location = new System.Drawing.Point(87, 168);
            this.textBoxPatientHeightMajor.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPatientHeightMajor.Name = "textBoxPatientHeightMajor";
            this.textBoxPatientHeightMajor.Size = new System.Drawing.Size(50, 23);
            this.textBoxPatientHeightMajor.TabIndex = 52;
            this.textBoxPatientHeightMajor.Text = "^Txt";
            // 
            // labelPatientHeight
            // 
            this.labelPatientHeight.AutoSize = true;
            this.labelPatientHeight.Location = new System.Drawing.Point(5, 171);
            this.labelPatientHeight.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPatientHeight.Name = "labelPatientHeight";
            this.labelPatientHeight.Size = new System.Drawing.Size(52, 16);
            this.labelPatientHeight.TabIndex = 51;
            this.labelPatientHeight.Text = "Height:";
            this.labelPatientHeight.Click += new System.EventHandler(this.label9_Click);
            // 
            // textBoxPatientID
            // 
            this.textBoxPatientID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxPatientID.Location = new System.Drawing.Point(87, 7);
            this.textBoxPatientID.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPatientID.MaxLength = 14;
            this.textBoxPatientID.Name = "textBoxPatientID";
            this.textBoxPatientID.Size = new System.Drawing.Size(125, 23);
            this.textBoxPatientID.TabIndex = 32;
            this.textBoxPatientID.Text = "^Txt";
            this.textBoxPatientID.TextChanged += new System.EventHandler(this.textBox7_TextChanged);
            // 
            // labelPatientID
            // 
            this.labelPatientID.AutoSize = true;
            this.labelPatientID.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelPatientID.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.labelPatientID.Location = new System.Drawing.Point(5, 10);
            this.labelPatientID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPatientID.Name = "labelPatientID";
            this.labelPatientID.Size = new System.Drawing.Size(79, 16);
            this.labelPatientID.TabIndex = 31;
            this.labelPatientID.Text = "Patient ID:";
            this.labelPatientID.Click += new System.EventHandler(this.label8_Click);
            // 
            // textBoxPatientDOBYear
            // 
            this.textBoxPatientDOBYear.Location = new System.Drawing.Point(247, 106);
            this.textBoxPatientDOBYear.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPatientDOBYear.Name = "textBoxPatientDOBYear";
            this.textBoxPatientDOBYear.Size = new System.Drawing.Size(58, 23);
            this.textBoxPatientDOBYear.TabIndex = 44;
            this.textBoxPatientDOBYear.Text = "^Txt";
            this.textBoxPatientDOBYear.TextChanged += new System.EventHandler(this.textBoxPatientDOBYear_TextChanged);
            // 
            // comboBoxPatientDOBMonth
            // 
            this.comboBoxPatientDOBMonth.DropDownHeight = 300;
            this.comboBoxPatientDOBMonth.DropDownWidth = 200;
            this.comboBoxPatientDOBMonth.FormattingEnabled = true;
            this.comboBoxPatientDOBMonth.IntegralHeight = false;
            this.comboBoxPatientDOBMonth.Items.AddRange(new object[] {
            "January",
            "February",
            "March",
            "April",
            "May ",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"});
            this.comboBoxPatientDOBMonth.Location = new System.Drawing.Point(126, 106);
            this.comboBoxPatientDOBMonth.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxPatientDOBMonth.MaxDropDownItems = 12;
            this.comboBoxPatientDOBMonth.Name = "comboBoxPatientDOBMonth";
            this.comboBoxPatientDOBMonth.Size = new System.Drawing.Size(118, 24);
            this.comboBoxPatientDOBMonth.TabIndex = 43;
            this.comboBoxPatientDOBMonth.TextChanged += new System.EventHandler(this.comboBoxPatientDOBMonth_TextChanged);
            // 
            // textBoxPatientDOBDay
            // 
            this.textBoxPatientDOBDay.Location = new System.Drawing.Point(87, 106);
            this.textBoxPatientDOBDay.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPatientDOBDay.Name = "textBoxPatientDOBDay";
            this.textBoxPatientDOBDay.Size = new System.Drawing.Size(36, 23);
            this.textBoxPatientDOBDay.TabIndex = 42;
            this.textBoxPatientDOBDay.Text = "^Txt";
            this.textBoxPatientDOBDay.TextChanged += new System.EventHandler(this.textBoxPatientDOBDay_TextChanged);
            // 
            // labelPatientDOB
            // 
            this.labelPatientDOB.AutoSize = true;
            this.labelPatientDOB.Location = new System.Drawing.Point(5, 109);
            this.labelPatientDOB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPatientDOB.Name = "labelPatientDOB";
            this.labelPatientDOB.Size = new System.Drawing.Size(69, 16);
            this.labelPatientDOB.TabIndex = 41;
            this.labelPatientDOB.Text = "Birthdate:";
            // 
            // comboBoxPatientGenderList
            // 
            this.comboBoxPatientGenderList.DropDownWidth = 200;
            this.comboBoxPatientGenderList.FormattingEnabled = true;
            this.comboBoxPatientGenderList.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.comboBoxPatientGenderList.Location = new System.Drawing.Point(87, 136);
            this.comboBoxPatientGenderList.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxPatientGenderList.Name = "comboBoxPatientGenderList";
            this.comboBoxPatientGenderList.Size = new System.Drawing.Size(145, 24);
            this.comboBoxPatientGenderList.TabIndex = 48;
            this.comboBoxPatientGenderList.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // labelPatientGender
            // 
            this.labelPatientGender.AutoSize = true;
            this.labelPatientGender.Location = new System.Drawing.Point(5, 139);
            this.labelPatientGender.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPatientGender.Name = "labelPatientGender";
            this.labelPatientGender.Size = new System.Drawing.Size(60, 16);
            this.labelPatientGender.TabIndex = 47;
            this.labelPatientGender.Text = "Gender:";
            this.labelPatientGender.Click += new System.EventHandler(this.label6_Click);
            // 
            // textBoxPatientLastName
            // 
            this.textBoxPatientLastName.Location = new System.Drawing.Point(87, 73);
            this.textBoxPatientLastName.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPatientLastName.MaxLength = 62;
            this.textBoxPatientLastName.Name = "textBoxPatientLastName";
            this.textBoxPatientLastName.Size = new System.Drawing.Size(343, 23);
            this.textBoxPatientLastName.TabIndex = 40;
            this.textBoxPatientLastName.Text = "^Txt";
            // 
            // labelPatientLastName
            // 
            this.labelPatientLastName.AutoSize = true;
            this.labelPatientLastName.Location = new System.Drawing.Point(5, 76);
            this.labelPatientLastName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPatientLastName.Name = "labelPatientLastName";
            this.labelPatientLastName.Size = new System.Drawing.Size(78, 16);
            this.labelPatientLastName.TabIndex = 39;
            this.labelPatientLastName.Text = "Last name:";
            this.labelPatientLastName.Click += new System.EventHandler(this.label4_Click);
            // 
            // textBoxPatientMiddleName
            // 
            this.textBoxPatientMiddleName.Location = new System.Drawing.Point(305, 40);
            this.textBoxPatientMiddleName.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPatientMiddleName.MaxLength = 30;
            this.textBoxPatientMiddleName.Name = "textBoxPatientMiddleName";
            this.textBoxPatientMiddleName.Size = new System.Drawing.Size(125, 23);
            this.textBoxPatientMiddleName.TabIndex = 38;
            this.textBoxPatientMiddleName.Text = "^Txt";
            // 
            // labelPatientMiddleName
            // 
            this.labelPatientMiddleName.AutoSize = true;
            this.labelPatientMiddleName.Location = new System.Drawing.Point(216, 43);
            this.labelPatientMiddleName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPatientMiddleName.Name = "labelPatientMiddleName";
            this.labelPatientMiddleName.Size = new System.Drawing.Size(92, 16);
            this.labelPatientMiddleName.TabIndex = 37;
            this.labelPatientMiddleName.Text = "Middle name:";
            // 
            // textBoxPatientFirstName
            // 
            this.textBoxPatientFirstName.Location = new System.Drawing.Point(87, 40);
            this.textBoxPatientFirstName.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPatientFirstName.MaxLength = 30;
            this.textBoxPatientFirstName.Name = "textBoxPatientFirstName";
            this.textBoxPatientFirstName.Size = new System.Drawing.Size(125, 23);
            this.textBoxPatientFirstName.TabIndex = 36;
            this.textBoxPatientFirstName.Text = "^Txt";
            // 
            // labelPatientFirstName
            // 
            this.labelPatientFirstName.AutoSize = true;
            this.labelPatientFirstName.Location = new System.Drawing.Point(5, 43);
            this.labelPatientFirstName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPatientFirstName.Name = "labelPatientFirstName";
            this.labelPatientFirstName.Size = new System.Drawing.Size(79, 16);
            this.labelPatientFirstName.TabIndex = 35;
            this.labelPatientFirstName.Text = "First name:";
            this.labelPatientFirstName.Click += new System.EventHandler(this.label2_Click);
            // 
            // labelSocSecNr
            // 
            this.labelSocSecNr.AutoSize = true;
            this.labelSocSecNr.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelSocSecNr.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.labelSocSecNr.Location = new System.Drawing.Point(216, 10);
            this.labelSocSecNr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelSocSecNr.Name = "labelSocSecNr";
            this.labelSocSecNr.Size = new System.Drawing.Size(85, 16);
            this.labelSocSecNr.TabIndex = 33;
            this.labelSocSecNr.Text = "Unique ID*:";
            // 
            // textBoxSocSecNr
            // 
            this.textBoxSocSecNr.Location = new System.Drawing.Point(305, 6);
            this.textBoxSocSecNr.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxSocSecNr.MaxLength = 14;
            this.textBoxSocSecNr.Name = "textBoxSocSecNr";
            this.textBoxSocSecNr.Size = new System.Drawing.Size(125, 23);
            this.textBoxSocSecNr.TabIndex = 34;
            this.textBoxSocSecNr.Text = "^Txt";
            this.textBoxSocSecNr.TextChanged += new System.EventHandler(this.textBox21_TextChanged);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panel7.Controls.Add(this.pictureBox1);
            this.panel7.Controls.Add(this.buttonUploadPicture);
            this.panel7.Controls.Add(this.comboBoxPatientCountryList);
            this.panel7.Controls.Add(this.textBoxPatientSkype);
            this.panel7.Controls.Add(this.labelPatientSkype);
            this.panel7.Controls.Add(this.textBoxPatientEmail);
            this.panel7.Controls.Add(this.labelPatientEmail);
            this.panel7.Controls.Add(this.textBoxPatientPhone2);
            this.panel7.Controls.Add(this.labelPatientPhone2);
            this.panel7.Controls.Add(this.textBoxPatientHouseNumber);
            this.panel7.Controls.Add(this.buttonRandom);
            this.panel7.Controls.Add(this.labelPatientHouseNumber);
            this.panel7.Controls.Add(this.textBoxPatientCell);
            this.panel7.Controls.Add(this.labelPatientCell);
            this.panel7.Controls.Add(this.textBoxPatientAddress);
            this.panel7.Controls.Add(this.labelPatientAddress);
            this.panel7.Controls.Add(this.comboBoxPatientStateList);
            this.panel7.Controls.Add(this.textBoxPatientPhone1);
            this.panel7.Controls.Add(this.labelPatientPhone1);
            this.panel7.Controls.Add(this.labelPatientCountry);
            this.panel7.Controls.Add(this.textBoxPatientCity);
            this.panel7.Controls.Add(this.labelPatientCity);
            this.panel7.Controls.Add(this.labelPatientState);
            this.panel7.Controls.Add(this.textBoxPatientZip);
            this.panel7.Controls.Add(this.labelPatientZip);
            this.panel7.Font = new System.Drawing.Font("Arial", 10F);
            this.panel7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel7.Location = new System.Drawing.Point(459, 0);
            this.panel7.Margin = new System.Windows.Forms.Padding(4);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(453, 276);
            this.panel7.TabIndex = 22;
            this.panel7.Paint += new System.Windows.Forms.PaintEventHandler(this.panel7_Paint);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(335, 238);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(0, 0);
            this.pictureBox1.TabIndex = 79;
            this.pictureBox1.TabStop = false;
            // 
            // buttonUploadPicture
            // 
            this.buttonUploadPicture.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonUploadPicture.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonUploadPicture.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonUploadPicture.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonUploadPicture.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonUploadPicture.Location = new System.Drawing.Point(283, 165);
            this.buttonUploadPicture.Margin = new System.Windows.Forms.Padding(4);
            this.buttonUploadPicture.Name = "buttonUploadPicture";
            this.buttonUploadPicture.Size = new System.Drawing.Size(151, 28);
            this.buttonUploadPicture.TabIndex = 78;
            this.buttonUploadPicture.Text = "Upload Picture";
            this.buttonUploadPicture.UseVisualStyleBackColor = false;
            this.buttonUploadPicture.Visible = false;
            this.buttonUploadPicture.Click += new System.EventHandler(this.button4_Click);
            // 
            // comboBoxPatientCountryList
            // 
            this.comboBoxPatientCountryList.DropDownWidth = 200;
            this.comboBoxPatientCountryList.FormattingEnabled = true;
            this.comboBoxPatientCountryList.Items.AddRange(new object[] {
            "USA",
            "Mexico"});
            this.comboBoxPatientCountryList.Location = new System.Drawing.Point(283, 73);
            this.comboBoxPatientCountryList.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxPatientCountryList.Name = "comboBoxPatientCountryList";
            this.comboBoxPatientCountryList.Size = new System.Drawing.Size(151, 24);
            this.comboBoxPatientCountryList.TabIndex = 67;
            this.comboBoxPatientCountryList.Text = "^Txt";
            // 
            // textBoxPatientSkype
            // 
            this.textBoxPatientSkype.Location = new System.Drawing.Point(68, 167);
            this.textBoxPatientSkype.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPatientSkype.Name = "textBoxPatientSkype";
            this.textBoxPatientSkype.Size = new System.Drawing.Size(133, 23);
            this.textBoxPatientSkype.TabIndex = 77;
            this.textBoxPatientSkype.Text = "^Txt";
            // 
            // labelPatientSkype
            // 
            this.labelPatientSkype.AutoSize = true;
            this.labelPatientSkype.Location = new System.Drawing.Point(5, 172);
            this.labelPatientSkype.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPatientSkype.Name = "labelPatientSkype";
            this.labelPatientSkype.Size = new System.Drawing.Size(51, 16);
            this.labelPatientSkype.TabIndex = 76;
            this.labelPatientSkype.Text = "Skype:";
            // 
            // textBoxPatientEmail
            // 
            this.textBoxPatientEmail.Location = new System.Drawing.Point(283, 137);
            this.textBoxPatientEmail.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPatientEmail.Name = "textBoxPatientEmail";
            this.textBoxPatientEmail.Size = new System.Drawing.Size(151, 23);
            this.textBoxPatientEmail.TabIndex = 75;
            this.textBoxPatientEmail.Text = "^Txt";
            // 
            // labelPatientEmail
            // 
            this.labelPatientEmail.AutoSize = true;
            this.labelPatientEmail.Location = new System.Drawing.Point(224, 141);
            this.labelPatientEmail.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPatientEmail.Name = "labelPatientEmail";
            this.labelPatientEmail.Size = new System.Drawing.Size(46, 16);
            this.labelPatientEmail.TabIndex = 74;
            this.labelPatientEmail.Text = "Email:";
            // 
            // textBoxPatientPhone2
            // 
            this.textBoxPatientPhone2.Location = new System.Drawing.Point(283, 107);
            this.textBoxPatientPhone2.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPatientPhone2.Name = "textBoxPatientPhone2";
            this.textBoxPatientPhone2.Size = new System.Drawing.Size(151, 23);
            this.textBoxPatientPhone2.TabIndex = 71;
            this.textBoxPatientPhone2.Text = "^Txt";
            // 
            // labelPatientPhone2
            // 
            this.labelPatientPhone2.AutoSize = true;
            this.labelPatientPhone2.Location = new System.Drawing.Point(224, 112);
            this.labelPatientPhone2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPatientPhone2.Name = "labelPatientPhone2";
            this.labelPatientPhone2.Size = new System.Drawing.Size(34, 16);
            this.labelPatientPhone2.TabIndex = 70;
            this.labelPatientPhone2.Text = "ICE:";
            // 
            // textBoxPatientHouseNumber
            // 
            this.textBoxPatientHouseNumber.Location = new System.Drawing.Point(68, 41);
            this.textBoxPatientHouseNumber.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPatientHouseNumber.Name = "textBoxPatientHouseNumber";
            this.textBoxPatientHouseNumber.Size = new System.Drawing.Size(45, 23);
            this.textBoxPatientHouseNumber.TabIndex = 61;
            this.textBoxPatientHouseNumber.Text = "^Txt";
            // 
            // buttonRandom
            // 
            this.buttonRandom.Location = new System.Drawing.Point(227, 166);
            this.buttonRandom.Name = "buttonRandom";
            this.buttonRandom.Size = new System.Drawing.Size(21, 25);
            this.buttonRandom.TabIndex = 12;
            this.buttonRandom.Text = "R";
            this.buttonRandom.UseVisualStyleBackColor = true;
            this.buttonRandom.Click += new System.EventHandler(this.buttonRandom_Click);
            // 
            // labelPatientHouseNumber
            // 
            this.labelPatientHouseNumber.AutoSize = true;
            this.labelPatientHouseNumber.Location = new System.Drawing.Point(5, 44);
            this.labelPatientHouseNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPatientHouseNumber.Name = "labelPatientHouseNumber";
            this.labelPatientHouseNumber.Size = new System.Drawing.Size(26, 16);
            this.labelPatientHouseNumber.TabIndex = 60;
            this.labelPatientHouseNumber.Text = "Nr:";
            // 
            // textBoxPatientCell
            // 
            this.textBoxPatientCell.Location = new System.Drawing.Point(68, 136);
            this.textBoxPatientCell.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPatientCell.Name = "textBoxPatientCell";
            this.textBoxPatientCell.Size = new System.Drawing.Size(133, 23);
            this.textBoxPatientCell.TabIndex = 73;
            this.textBoxPatientCell.Text = "^Txt";
            // 
            // labelPatientCell
            // 
            this.labelPatientCell.AutoSize = true;
            this.labelPatientCell.Location = new System.Drawing.Point(5, 141);
            this.labelPatientCell.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPatientCell.Name = "labelPatientCell";
            this.labelPatientCell.Size = new System.Drawing.Size(36, 16);
            this.labelPatientCell.TabIndex = 72;
            this.labelPatientCell.Text = "Cell:";
            // 
            // textBoxPatientAddress
            // 
            this.textBoxPatientAddress.Location = new System.Drawing.Point(68, 6);
            this.textBoxPatientAddress.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPatientAddress.MaxLength = 124;
            this.textBoxPatientAddress.Multiline = true;
            this.textBoxPatientAddress.Name = "textBoxPatientAddress";
            this.textBoxPatientAddress.Size = new System.Drawing.Size(366, 29);
            this.textBoxPatientAddress.TabIndex = 57;
            this.textBoxPatientAddress.Text = "^Txt";
            // 
            // labelPatientAddress
            // 
            this.labelPatientAddress.AutoSize = true;
            this.labelPatientAddress.Location = new System.Drawing.Point(5, 10);
            this.labelPatientAddress.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPatientAddress.Name = "labelPatientAddress";
            this.labelPatientAddress.Size = new System.Drawing.Size(64, 16);
            this.labelPatientAddress.TabIndex = 56;
            this.labelPatientAddress.Text = "Address:";
            this.labelPatientAddress.Click += new System.EventHandler(this.label19_Click);
            // 
            // comboBoxPatientStateList
            // 
            this.comboBoxPatientStateList.DropDownWidth = 200;
            this.comboBoxPatientStateList.FormattingEnabled = true;
            this.comboBoxPatientStateList.Items.AddRange(new object[] {
            "NA",
            "Alabama",
            "Alaska",
            "Arizona",
            "Arkansas",
            "California",
            "Colorado",
            "Connecticut",
            "Delaware",
            "District of Columbia",
            "Florida",
            "Georgia",
            "Hawaii",
            "Idaho",
            "Illinois",
            "Indiana",
            "Iowa",
            "Kansas",
            "Kentucky",
            "Louisiana",
            "Maine",
            "Maryland",
            "Massachusetts",
            "Michigan",
            "Minnesota",
            "Mississippi",
            "Missouri",
            "Montana",
            "Nebraska",
            "Nevada",
            "New Hampshire",
            "New Jersey",
            "New Mexico",
            "New York",
            "North Carolina",
            "North Dakota",
            "Ohio",
            "Oklahoma",
            "Oregon",
            "Pennsylvania",
            "Rhode Island",
            "South Carolina",
            "South Dakota",
            "Tennessee",
            "Texas",
            "Utah",
            "Vermont",
            "Virginia",
            "Washington",
            "West Virginia",
            "Wisconsin"});
            this.comboBoxPatientStateList.Location = new System.Drawing.Point(283, 41);
            this.comboBoxPatientStateList.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxPatientStateList.Name = "comboBoxPatientStateList";
            this.comboBoxPatientStateList.Size = new System.Drawing.Size(151, 24);
            this.comboBoxPatientStateList.TabIndex = 63;
            this.comboBoxPatientStateList.Text = "^Txt";
            this.comboBoxPatientStateList.SelectedIndexChanged += new System.EventHandler(this.comboBox9_SelectedIndexChanged);
            // 
            // textBoxPatientPhone1
            // 
            this.textBoxPatientPhone1.Location = new System.Drawing.Point(68, 108);
            this.textBoxPatientPhone1.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPatientPhone1.Name = "textBoxPatientPhone1";
            this.textBoxPatientPhone1.Size = new System.Drawing.Size(133, 23);
            this.textBoxPatientPhone1.TabIndex = 69;
            this.textBoxPatientPhone1.Text = "^Txt";
            this.textBoxPatientPhone1.TextChanged += new System.EventHandler(this.textBox14_TextChanged);
            // 
            // labelPatientPhone1
            // 
            this.labelPatientPhone1.AutoSize = true;
            this.labelPatientPhone1.Location = new System.Drawing.Point(5, 113);
            this.labelPatientPhone1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPatientPhone1.Name = "labelPatientPhone1";
            this.labelPatientPhone1.Size = new System.Drawing.Size(53, 16);
            this.labelPatientPhone1.TabIndex = 68;
            this.labelPatientPhone1.Text = "Phone:";
            // 
            // labelPatientCountry
            // 
            this.labelPatientCountry.AutoSize = true;
            this.labelPatientCountry.Location = new System.Drawing.Point(224, 77);
            this.labelPatientCountry.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPatientCountry.Name = "labelPatientCountry";
            this.labelPatientCountry.Size = new System.Drawing.Size(62, 16);
            this.labelPatientCountry.TabIndex = 66;
            this.labelPatientCountry.Text = "Country:";
            // 
            // textBoxPatientCity
            // 
            this.textBoxPatientCity.Location = new System.Drawing.Point(68, 74);
            this.textBoxPatientCity.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPatientCity.MaxLength = 30;
            this.textBoxPatientCity.Name = "textBoxPatientCity";
            this.textBoxPatientCity.Size = new System.Drawing.Size(133, 23);
            this.textBoxPatientCity.TabIndex = 65;
            this.textBoxPatientCity.Text = "^Txt";
            // 
            // labelPatientCity
            // 
            this.labelPatientCity.AutoSize = true;
            this.labelPatientCity.Location = new System.Drawing.Point(5, 77);
            this.labelPatientCity.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPatientCity.Name = "labelPatientCity";
            this.labelPatientCity.Size = new System.Drawing.Size(36, 16);
            this.labelPatientCity.TabIndex = 64;
            this.labelPatientCity.Text = "City:";
            // 
            // labelPatientState
            // 
            this.labelPatientState.AutoSize = true;
            this.labelPatientState.Location = new System.Drawing.Point(224, 45);
            this.labelPatientState.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPatientState.Name = "labelPatientState";
            this.labelPatientState.Size = new System.Drawing.Size(45, 16);
            this.labelPatientState.TabIndex = 62;
            this.labelPatientState.Text = "State:";
            // 
            // textBoxPatientZip
            // 
            this.textBoxPatientZip.Location = new System.Drawing.Point(151, 41);
            this.textBoxPatientZip.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPatientZip.MaxLength = 14;
            this.textBoxPatientZip.Name = "textBoxPatientZip";
            this.textBoxPatientZip.Size = new System.Drawing.Size(50, 23);
            this.textBoxPatientZip.TabIndex = 59;
            this.textBoxPatientZip.Text = "^Txt";
            this.textBoxPatientZip.TextChanged += new System.EventHandler(this.textBox18_TextChanged);
            // 
            // labelPatientZip
            // 
            this.labelPatientZip.AutoSize = true;
            this.labelPatientZip.Location = new System.Drawing.Point(119, 44);
            this.labelPatientZip.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPatientZip.Name = "labelPatientZip";
            this.labelPatientZip.Size = new System.Drawing.Size(31, 16);
            this.labelPatientZip.TabIndex = 58;
            this.labelPatientZip.Text = "Zip:";
            // 
            // panel22
            // 
            this.panel22.Controls.Add(this.panel34);
            this.panel22.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel22.Location = new System.Drawing.Point(0, 358);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(1274, 274);
            this.panel22.TabIndex = 65;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(61, 4);
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.labelRecorsInfo);
            this.panel9.Controls.Add(this.checkBoxUpdateRecords);
            this.panel9.Controls.Add(this.checkBoxUpdatePatInfo);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel9.Location = new System.Drawing.Point(0, 633);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(1274, 31);
            this.panel9.TabIndex = 67;
            // 
            // labelRecorsInfo
            // 
            this.labelRecorsInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelRecorsInfo.Font = new System.Drawing.Font("Arial Narrow", 8F);
            this.labelRecorsInfo.Location = new System.Drawing.Point(0, 0);
            this.labelRecorsInfo.Name = "labelRecorsInfo";
            this.labelRecorsInfo.Size = new System.Drawing.Size(978, 31);
            this.labelRecorsInfo.TabIndex = 1;
            this.labelRecorsInfo.Text = "For records info \r\npress [Records info]";
            // 
            // checkBoxUpdateRecords
            // 
            this.checkBoxUpdateRecords.AutoSize = true;
            this.checkBoxUpdateRecords.Dock = System.Windows.Forms.DockStyle.Right;
            this.checkBoxUpdateRecords.Enabled = false;
            this.checkBoxUpdateRecords.Location = new System.Drawing.Point(978, 0);
            this.checkBoxUpdateRecords.Name = "checkBoxUpdateRecords";
            this.checkBoxUpdateRecords.Size = new System.Drawing.Size(182, 31);
            this.checkBoxUpdateRecords.TabIndex = 0;
            this.checkBoxUpdateRecords.Text = "Update all event records";
            this.checkBoxUpdateRecords.UseVisualStyleBackColor = true;
            // 
            // checkBoxUpdatePatInfo
            // 
            this.checkBoxUpdatePatInfo.AutoSize = true;
            this.checkBoxUpdatePatInfo.Dock = System.Windows.Forms.DockStyle.Right;
            this.checkBoxUpdatePatInfo.Enabled = false;
            this.checkBoxUpdatePatInfo.Location = new System.Drawing.Point(1160, 0);
            this.checkBoxUpdatePatInfo.Name = "checkBoxUpdatePatInfo";
            this.checkBoxUpdatePatInfo.Size = new System.Drawing.Size(114, 31);
            this.checkBoxUpdatePatInfo.TabIndex = 2;
            this.checkBoxUpdatePatInfo.Text = "Refill Pat. Info";
            this.checkBoxUpdatePatInfo.UseVisualStyleBackColor = true;
            // 
            // CStudyPatientForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(1274, 701);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel23);
            this.Controls.Add(this.panel22);
            this.Controls.Add(this.panel18);
            this.Controls.Add(this.panel14);
            this.Controls.Add(this.panel13);
            this.Controls.Add(this.panel4);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.Name = "CStudyPatientForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Study Details";
            this.Load += new System.EventHandler(this.CPatientForm_Load);
            this.Shown += new System.EventHandler(this.CStudyPatientForm_Shown);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panelCreateInfo.ResumeLayout(false);
            this.panelCreateInfo.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.panel25.ResumeLayout(false);
            this.panel33.ResumeLayout(false);
            this.panel33.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel18.ResumeLayout(false);
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            this.panel23.ResumeLayout(false);
            this.panelButtons.ResumeLayout(false);
            this.panel34.ResumeLayout(false);
            this.panel29.ResumeLayout(false);
            this.panel37.ResumeLayout(false);
            this.panel37.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel22.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label labelStudyPhysician;
        private System.Windows.Forms.ComboBox comboBoxStudyPhysicianList;
        private System.Windows.Forms.Label labelStudyRefPhysician;
        private System.Windows.Forms.ComboBox comboBoxStudyRefPhysicianList;
        private System.Windows.Forms.ComboBox comboBoxStudyHospitalNameList;
        private System.Windows.Forms.Label labelStudyHospitalName;
        private System.Windows.Forms.Label labelHospitalRoom;
        private System.Windows.Forms.TextBox textBoxHospitalRoom;
        private System.Windows.Forms.Label labelStudySerialNumber;
        private System.Windows.Forms.ComboBox comboBoxStudySerialNumberList;
        private System.Windows.Forms.TextBox textBoxHospitalBed;
        private System.Windows.Forms.Label labelHospitalBed;
        private System.Windows.Forms.Label labelStudyStartDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerStudyStartDate;
        private System.Windows.Forms.Label labelStudyType;
        private System.Windows.Forms.HelpProvider helpProvider1;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.ComboBox comboBoxStudyMonitoringDaysList;
        private System.Windows.Forms.Label labelStudyMonitoringDays;
        private System.Windows.Forms.ComboBox comboBoxStudyTypeList;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBoxStudyPhysicianInstruction;
        private System.Windows.Forms.Label labelStudyPhysicianInstruction;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Panel panelButtons;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.Button buttonAddUpdate;
        private System.Windows.Forms.Button buttonPrintPreviousFindings;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelStudyLabel;
        private System.Windows.Forms.TextBox textBoxStudyNote;
        private System.Windows.Forms.CheckedListBox checkedListBoxReportInterval;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label labelPatientNr;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelDeviceCmp;
        private System.Windows.Forms.Button buttonDup;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Label labelPatientError;
        private System.Windows.Forms.Label labelStudyError;
        private System.Windows.Forms.Label labelDeviceError;
        private System.Windows.Forms.Button buttonCreateReport;
        private System.Windows.Forms.TextBox textBoxStdInstructions;
        private System.Windows.Forms.CheckedListBox checkedListBoxMctInterval;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button buttonUseCurrentDevice;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label labelEndRec;
        private System.Windows.Forms.Label labelStartRec;
        private System.Windows.Forms.Button buttonDeviceList;
        private System.Windows.Forms.Button buttonClearNote;
        private System.Windows.Forms.Label labelStudyNr;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button buttonEndRecording;
        private System.Windows.Forms.Button buttonMCT;
        private System.Windows.Forms.Button buttonViewPdf;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox textBoxStudyNr;
        private System.Windows.Forms.Panel panelCreateInfo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel34;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.Panel panel37;
        private System.Windows.Forms.CheckedListBox checkedListBoxSymptomsList;
        private System.Windows.Forms.Label labelPatientHistory;
        private System.Windows.Forms.Label labelPatientSymptoms;
        private System.Windows.Forms.CheckedListBox checkedListBoxMedicationList;
        private System.Windows.Forms.CheckedListBox checkedListBoxAllergiesList;
        private System.Windows.Forms.CheckedListBox checkedListBoxHistoryList;
        private System.Windows.Forms.Label labelPatientAllergies;
        private System.Windows.Forms.Label labelPatientMedication;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button buttonDobNA;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxPatientLabel;
        private System.Windows.Forms.Label labelUnitWeight;
        private System.Windows.Forms.Label labelUnitHeightMinor;
        private System.Windows.Forms.TextBox textBoxPatientHeightMinor;
        private System.Windows.Forms.Label labelUnitHeightMajor;
        private System.Windows.Forms.TextBox textBoxAge;
        private System.Windows.Forms.Label labelPatientAge;
        private System.Windows.Forms.Button buttonSearchPatientID;
        private System.Windows.Forms.ComboBox comboBoxPatientRaceList;
        private System.Windows.Forms.Label labelPatientRace;
        private System.Windows.Forms.TextBox textBoxPatientWeight;
        private System.Windows.Forms.Label labelPatientWeight;
        private System.Windows.Forms.TextBox textBoxPatientHeightMajor;
        private System.Windows.Forms.Label labelPatientHeight;
        private System.Windows.Forms.TextBox textBoxPatientID;
        private System.Windows.Forms.Label labelPatientID;
        private System.Windows.Forms.TextBox textBoxPatientDOBYear;
        private System.Windows.Forms.ComboBox comboBoxPatientDOBMonth;
        private System.Windows.Forms.TextBox textBoxPatientDOBDay;
        private System.Windows.Forms.Label labelPatientDOB;
        private System.Windows.Forms.ComboBox comboBoxPatientGenderList;
        private System.Windows.Forms.Label labelPatientGender;
        private System.Windows.Forms.TextBox textBoxPatientLastName;
        private System.Windows.Forms.Label labelPatientLastName;
        private System.Windows.Forms.TextBox textBoxPatientMiddleName;
        private System.Windows.Forms.Label labelPatientMiddleName;
        private System.Windows.Forms.TextBox textBoxPatientFirstName;
        private System.Windows.Forms.Label labelPatientFirstName;
        private System.Windows.Forms.Label labelSocSecNr;
        private System.Windows.Forms.TextBox textBoxSocSecNr;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button buttonUploadPicture;
        private System.Windows.Forms.ComboBox comboBoxPatientCountryList;
        private System.Windows.Forms.TextBox textBoxPatientSkype;
        private System.Windows.Forms.Label labelPatientSkype;
        private System.Windows.Forms.TextBox textBoxPatientEmail;
        private System.Windows.Forms.Label labelPatientEmail;
        private System.Windows.Forms.TextBox textBoxPatientPhone2;
        private System.Windows.Forms.Label labelPatientPhone2;
        private System.Windows.Forms.TextBox textBoxPatientHouseNumber;
        private System.Windows.Forms.Button buttonRandom;
        private System.Windows.Forms.Label labelPatientHouseNumber;
        private System.Windows.Forms.TextBox textBoxPatientCell;
        private System.Windows.Forms.Label labelPatientCell;
        private System.Windows.Forms.TextBox textBoxPatientAddress;
        private System.Windows.Forms.Label labelPatientAddress;
        private System.Windows.Forms.ComboBox comboBoxPatientStateList;
        private System.Windows.Forms.TextBox textBoxPatientPhone1;
        private System.Windows.Forms.Label labelPatientPhone1;
        private System.Windows.Forms.Label labelPatientCountry;
        private System.Windows.Forms.TextBox textBoxPatientCity;
        private System.Windows.Forms.Label labelPatientCity;
        private System.Windows.Forms.Label labelPatientState;
        private System.Windows.Forms.TextBox textBoxPatientZip;
        private System.Windows.Forms.Label labelPatientZip;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ComboBox comboBoxTrial;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonClearPatient;
        private System.Windows.Forms.Button buttonPatientForm;
        private System.Windows.Forms.CheckBox checkBoxAllRecorder;
        private System.Windows.Forms.CheckBox checkBoxAllPhysician;
        private System.Windows.Forms.CheckBox checkBoxAllRefPhysician;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.Label labelDeviceClient;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label labelDeviceRemLabel;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBoxRefIdOther;
        private System.Windows.Forms.Label labelRecordEndDate;
        private System.Windows.Forms.Label labelRecorderStartDate;
        private System.Windows.Forms.RadioButton radioButtonOther;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.RadioButton radioButtonUniqueID;
        private System.Windows.Forms.Label labelRecorderStudyNr;
        private System.Windows.Forms.Label labelRecorderState;
        private System.Windows.Forms.RadioButton radioButtonPatID;
        private System.Windows.Forms.Label labelRecorderRefID;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.RadioButton radioButtonNone;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label labelRecorderID;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ListView listViewStudyProcedures;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Button buttonAddICD;
        private System.Windows.Forms.Label labelUseRefID;
        private System.Windows.Forms.Label labelRecInfo;
        private System.Windows.Forms.Label labelStudyPlus1;
        private System.Windows.Forms.Label labelStudyPlus6;
        private System.Windows.Forms.Label labelStudyMin1;
        private System.Windows.Forms.Label labelStudyMin6;
        private System.Windows.Forms.CheckBox checkBoxAnonymizePatientInfo;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label labelStudyCreated;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelStudyChanged;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label labelDeviceChanged;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label labelNdevices;
        private System.Windows.Forms.TextBox textBoxStudyPermissions;
        private System.Windows.Forms.Label labelStudyPermissions;
        private System.Windows.Forms.Label labelLocation;
        private System.Windows.Forms.ComboBox comboBoxLocation;
        private System.Windows.Forms.Button buttonRecordsInfo;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label labelRecorsInfo;
        private System.Windows.Forms.CheckBox checkBoxUpdateRecords;
        private System.Windows.Forms.CheckBox checkBoxUpdatePatInfo;
        private System.Windows.Forms.Label labelTrialDays;
        private System.Windows.Forms.Label labelNoMct;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label labelAddStudyMode;
        private System.Windows.Forms.Button buttonHolter;
        private System.Windows.Forms.Label labelDefRefIdMode;
    }
}