﻿namespace EventboardEntryForms
{
    partial class CTrialInfoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CTrialInfoForm));
            this.labelTrialDetails = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.buttonCalcDays = new System.Windows.Forms.Button();
            this.labelStudyLabel = new System.Windows.Forms.Label();
            this.textBoxStudyNote = new System.Windows.Forms.TextBox();
            this.labelSite = new System.Windows.Forms.Label();
            this.comboBoxLocation = new System.Windows.Forms.ComboBox();
            this.textBoxStudyFreeText = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.listViewStudyProcedures = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.buttonAddICD = new System.Windows.Forms.Button();
            this.checkedListBoxReportInterval = new System.Windows.Forms.CheckedListBox();
            this.label8 = new System.Windows.Forms.Label();
            this.checkedListBoxMctInterval = new System.Windows.Forms.CheckedListBox();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBoxStudyMonitoringDaysList = new System.Windows.Forms.ComboBox();
            this.labelStudyMonitoringDays = new System.Windows.Forms.Label();
            this.comboBoxStudyTypeList = new System.Windows.Forms.ComboBox();
            this.labelStudyType = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dateTimePickerStopDate = new System.Windows.Forms.DateTimePicker();
            this.textBoxStudyPermissions = new System.Windows.Forms.TextBox();
            this.labelStudyPermissions = new System.Windows.Forms.Label();
            this.labelStudyPhysicianInstruction = new System.Windows.Forms.Label();
            this.labelStudyStartDate = new System.Windows.Forms.Label();
            this.dateTimePickerStartDate = new System.Windows.Forms.DateTimePicker();
            this.labelStudyHospitalName = new System.Windows.Forms.Label();
            this.comboBoxStudyHospitalNameList = new System.Windows.Forms.ComboBox();
            this.buttonRandom = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.checkBoxActive = new System.Windows.Forms.CheckBox();
            this.textBoxNr = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxLabel = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridViewList = new System.Windows.Forms.DataGridView();
            this.TrialNr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnActive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1ClientName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnMainClient = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnStartDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnStopDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnDays = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnProcCodes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnPermissions = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.labelTrialError = new System.Windows.Forms.Label();
            this.panel38 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.buttonAddUpdate = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.buttonDup = new System.Windows.Forms.Button();
            this.buttonClear = new System.Windows.Forms.Button();
            this.panel21 = new System.Windows.Forms.Panel();
            this.panel25 = new System.Windows.Forms.Panel();
            this.panel29 = new System.Windows.Forms.Panel();
            this.panel26 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelSelected = new System.Windows.Forms.Label();
            this.buttonSelect = new System.Windows.Forms.Button();
            this.labelListN = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelResultN = new System.Windows.Forms.Label();
            this.checkBoxShowSearchResult = new System.Windows.Forms.CheckBox();
            this.checkBoxShowDeactivated = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonDuplicate = new System.Windows.Forms.Button();
            this.buttonEdit = new System.Windows.Forms.Button();
            this.buttonDisable = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewList)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel29.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelTrialDetails
            // 
            this.labelTrialDetails.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelTrialDetails.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelTrialDetails.Location = new System.Drawing.Point(0, 0);
            this.labelTrialDetails.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTrialDetails.Name = "labelTrialDetails";
            this.labelTrialDetails.Size = new System.Drawing.Size(120, 20);
            this.labelTrialDetails.TabIndex = 72;
            this.labelTrialDetails.Text = "Trial details";
            this.labelTrialDetails.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel5
            // 
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Margin = new System.Windows.Forms.Padding(4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1102, 10);
            this.panel5.TabIndex = 71;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panel7.Controls.Add(this.buttonCalcDays);
            this.panel7.Controls.Add(this.labelStudyLabel);
            this.panel7.Controls.Add(this.textBoxStudyNote);
            this.panel7.Controls.Add(this.labelSite);
            this.panel7.Controls.Add(this.comboBoxLocation);
            this.panel7.Controls.Add(this.textBoxStudyFreeText);
            this.panel7.Controls.Add(this.button2);
            this.panel7.Controls.Add(this.listViewStudyProcedures);
            this.panel7.Controls.Add(this.buttonAddICD);
            this.panel7.Controls.Add(this.checkedListBoxReportInterval);
            this.panel7.Controls.Add(this.label8);
            this.panel7.Controls.Add(this.checkedListBoxMctInterval);
            this.panel7.Controls.Add(this.label6);
            this.panel7.Controls.Add(this.comboBoxStudyMonitoringDaysList);
            this.panel7.Controls.Add(this.labelStudyMonitoringDays);
            this.panel7.Controls.Add(this.comboBoxStudyTypeList);
            this.panel7.Controls.Add(this.labelStudyType);
            this.panel7.Controls.Add(this.label2);
            this.panel7.Controls.Add(this.dateTimePickerStopDate);
            this.panel7.Controls.Add(this.textBoxStudyPermissions);
            this.panel7.Controls.Add(this.labelStudyPermissions);
            this.panel7.Controls.Add(this.labelStudyPhysicianInstruction);
            this.panel7.Controls.Add(this.labelStudyStartDate);
            this.panel7.Controls.Add(this.dateTimePickerStartDate);
            this.panel7.Controls.Add(this.labelStudyHospitalName);
            this.panel7.Controls.Add(this.comboBoxStudyHospitalNameList);
            this.panel7.Controls.Add(this.buttonRandom);
            this.panel7.Controls.Add(this.label7);
            this.panel7.Controls.Add(this.checkBoxActive);
            this.panel7.Controls.Add(this.textBoxNr);
            this.panel7.Controls.Add(this.label3);
            this.panel7.Controls.Add(this.textBoxLabel);
            this.panel7.Controls.Add(this.label1);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Font = new System.Drawing.Font("Arial", 10F);
            this.panel7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Margin = new System.Windows.Forms.Padding(4);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1102, 202);
            this.panel7.TabIndex = 128;
            // 
            // buttonCalcDays
            // 
            this.buttonCalcDays.Location = new System.Drawing.Point(224, 71);
            this.buttonCalcDays.Name = "buttonCalcDays";
            this.buttonCalcDays.Size = new System.Drawing.Size(44, 20);
            this.buttonCalcDays.TabIndex = 130;
            this.buttonCalcDays.Text = ">>";
            this.buttonCalcDays.UseVisualStyleBackColor = true;
            this.buttonCalcDays.Click += new System.EventHandler(this.buttonCalcDays_Click);
            // 
            // labelStudyLabel
            // 
            this.labelStudyLabel.AutoSize = true;
            this.labelStudyLabel.Location = new System.Drawing.Point(283, 104);
            this.labelStudyLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelStudyLabel.Name = "labelStudyLabel";
            this.labelStudyLabel.Size = new System.Drawing.Size(80, 16);
            this.labelStudyLabel.TabIndex = 129;
            this.labelStudyLabel.Text = "Study note:";
            // 
            // textBoxStudyNote
            // 
            this.textBoxStudyNote.Location = new System.Drawing.Point(374, 101);
            this.textBoxStudyNote.MaxLength = 18;
            this.textBoxStudyNote.Name = "textBoxStudyNote";
            this.textBoxStudyNote.Size = new System.Drawing.Size(211, 23);
            this.textBoxStudyNote.TabIndex = 128;
            this.textBoxStudyNote.Text = "^Txt";
            // 
            // labelSite
            // 
            this.labelSite.AutoSize = true;
            this.labelSite.Font = new System.Drawing.Font("Arial", 10F);
            this.labelSite.Location = new System.Drawing.Point(7, 132);
            this.labelSite.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelSite.Name = "labelSite";
            this.labelSite.Size = new System.Drawing.Size(66, 16);
            this.labelSite.TabIndex = 127;
            this.labelSite.Text = "Location:";
            // 
            // comboBoxLocation
            // 
            this.comboBoxLocation.FormattingEnabled = true;
            this.comboBoxLocation.Location = new System.Drawing.Point(94, 129);
            this.comboBoxLocation.Name = "comboBoxLocation";
            this.comboBoxLocation.Size = new System.Drawing.Size(175, 24);
            this.comboBoxLocation.TabIndex = 126;
            this.comboBoxLocation.Text = "-- All --";
            // 
            // textBoxStudyFreeText
            // 
            this.textBoxStudyFreeText.Location = new System.Drawing.Point(287, 132);
            this.textBoxStudyFreeText.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxStudyFreeText.Multiline = true;
            this.textBoxStudyFreeText.Name = "textBoxStudyFreeText";
            this.textBoxStudyFreeText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxStudyFreeText.Size = new System.Drawing.Size(298, 58);
            this.textBoxStudyFreeText.TabIndex = 125;
            this.textBoxStudyFreeText.Text = "^Txt";
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button2.Font = new System.Drawing.Font("Webdings", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.button2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button2.Location = new System.Drawing.Point(1065, 3);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(25, 26);
            this.button2.TabIndex = 124;
            this.button2.Text = "q";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // listViewStudyProcedures
            // 
            this.listViewStudyProcedures.CheckBoxes = true;
            this.listViewStudyProcedures.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.listViewStudyProcedures.FullRowSelect = true;
            this.listViewStudyProcedures.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.listViewStudyProcedures.Location = new System.Drawing.Point(764, 32);
            this.listViewStudyProcedures.Name = "listViewStudyProcedures";
            this.listViewStudyProcedures.Size = new System.Drawing.Size(327, 84);
            this.listViewStudyProcedures.TabIndex = 123;
            this.listViewStudyProcedures.UseCompatibleStateImageBehavior = false;
            this.listViewStudyProcedures.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Code";
            this.columnHeader1.Width = 80;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Description";
            this.columnHeader2.Width = 390;
            // 
            // buttonAddICD
            // 
            this.buttonAddICD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonAddICD.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonAddICD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddICD.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonAddICD.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonAddICD.Location = new System.Drawing.Point(764, 3);
            this.buttonAddICD.Margin = new System.Windows.Forms.Padding(4);
            this.buttonAddICD.Name = "buttonAddICD";
            this.buttonAddICD.Size = new System.Drawing.Size(287, 25);
            this.buttonAddICD.TabIndex = 122;
            this.buttonAddICD.Text = "Select ICD10 Code(s)";
            this.buttonAddICD.UseVisualStyleBackColor = false;
            this.buttonAddICD.Click += new System.EventHandler(this.buttonAddICD_Click);
            // 
            // checkedListBoxReportInterval
            // 
            this.checkedListBoxReportInterval.CheckOnClick = true;
            this.checkedListBoxReportInterval.FormattingEnabled = true;
            this.checkedListBoxReportInterval.Items.AddRange(new object[] {
            "Item1",
            "item2",
            "item3",
            "item4",
            "item5"});
            this.checkedListBoxReportInterval.Location = new System.Drawing.Point(604, 22);
            this.checkedListBoxReportInterval.Name = "checkedListBoxReportInterval";
            this.checkedListBoxReportInterval.Size = new System.Drawing.Size(147, 94);
            this.checkedListBoxReportInterval.TabIndex = 121;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(604, 4);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(105, 21);
            this.label8.TabIndex = 120;
            this.label8.Text = "Event Reports:";
            // 
            // checkedListBoxMctInterval
            // 
            this.checkedListBoxMctInterval.CheckOnClick = true;
            this.checkedListBoxMctInterval.FormattingEnabled = true;
            this.checkedListBoxMctInterval.HorizontalScrollbar = true;
            this.checkedListBoxMctInterval.Items.AddRange(new object[] {
            "^Daily",
            "^Weekly",
            "^EOS"});
            this.checkedListBoxMctInterval.Location = new System.Drawing.Point(604, 132);
            this.checkedListBoxMctInterval.Name = "checkedListBoxMctInterval";
            this.checkedListBoxMctInterval.Size = new System.Drawing.Size(147, 58);
            this.checkedListBoxMctInterval.TabIndex = 119;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(606, 115);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(103, 21);
            this.label6.TabIndex = 118;
            this.label6.Text = "MCT Reports:";
            // 
            // comboBoxStudyMonitoringDaysList
            // 
            this.comboBoxStudyMonitoringDaysList.DropDownWidth = 200;
            this.comboBoxStudyMonitoringDaysList.FormattingEnabled = true;
            this.comboBoxStudyMonitoringDaysList.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "21",
            "28",
            "30",
            "31"});
            this.comboBoxStudyMonitoringDaysList.Location = new System.Drawing.Point(374, 67);
            this.comboBoxStudyMonitoringDaysList.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxStudyMonitoringDaysList.Name = "comboBoxStudyMonitoringDaysList";
            this.comboBoxStudyMonitoringDaysList.Size = new System.Drawing.Size(96, 24);
            this.comboBoxStudyMonitoringDaysList.TabIndex = 115;
            this.comboBoxStudyMonitoringDaysList.Text = "^Txt";
            // 
            // labelStudyMonitoringDays
            // 
            this.labelStudyMonitoringDays.AutoSize = true;
            this.labelStudyMonitoringDays.Location = new System.Drawing.Point(284, 71);
            this.labelStudyMonitoringDays.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelStudyMonitoringDays.Name = "labelStudyMonitoringDays";
            this.labelStudyMonitoringDays.Size = new System.Drawing.Size(44, 16);
            this.labelStudyMonitoringDays.TabIndex = 114;
            this.labelStudyMonitoringDays.Text = "Days:";
            // 
            // comboBoxStudyTypeList
            // 
            this.comboBoxStudyTypeList.DropDownWidth = 200;
            this.comboBoxStudyTypeList.FormattingEnabled = true;
            this.comboBoxStudyTypeList.Items.AddRange(new object[] {
            "! 2L - 1 ch event",
            "! 3L - 1 chl event",
            "! 3L - 2 ch event",
            "! 2L - 1 ch MCT",
            "! 3L - 1 chl MCT",
            "! 3L - 2 ch MCT",
            "! 5L - 2 ch holter",
            "! 5L - 5 ch real-time monitoring",
            "! POST TTM"});
            this.comboBoxStudyTypeList.Location = new System.Drawing.Point(374, 38);
            this.comboBoxStudyTypeList.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxStudyTypeList.Name = "comboBoxStudyTypeList";
            this.comboBoxStudyTypeList.Size = new System.Drawing.Size(211, 24);
            this.comboBoxStudyTypeList.TabIndex = 117;
            this.comboBoxStudyTypeList.Text = "^Txt";
            // 
            // labelStudyType
            // 
            this.labelStudyType.AutoSize = true;
            this.labelStudyType.Location = new System.Drawing.Point(284, 46);
            this.labelStudyType.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelStudyType.Name = "labelStudyType";
            this.labelStudyType.Size = new System.Drawing.Size(79, 16);
            this.labelStudyType.TabIndex = 116;
            this.labelStudyType.Text = "Study type:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 10F);
            this.label2.Location = new System.Drawing.Point(5, 99);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 16);
            this.label2.TabIndex = 112;
            this.label2.Text = "Stop date:";
            // 
            // dateTimePickerStopDate
            // 
            this.dateTimePickerStopDate.CustomFormat = "yyyy-MM-dd";
            this.dateTimePickerStopDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerStopDate.Location = new System.Drawing.Point(94, 99);
            this.dateTimePickerStopDate.Margin = new System.Windows.Forms.Padding(4);
            this.dateTimePickerStopDate.Name = "dateTimePickerStopDate";
            this.dateTimePickerStopDate.Size = new System.Drawing.Size(113, 23);
            this.dateTimePickerStopDate.TabIndex = 113;
            // 
            // textBoxStudyPermissions
            // 
            this.textBoxStudyPermissions.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.textBoxStudyPermissions.Location = new System.Drawing.Point(942, 132);
            this.textBoxStudyPermissions.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxStudyPermissions.Multiline = true;
            this.textBoxStudyPermissions.Name = "textBoxStudyPermissions";
            this.textBoxStudyPermissions.ReadOnly = true;
            this.textBoxStudyPermissions.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxStudyPermissions.Size = new System.Drawing.Size(147, 61);
            this.textBoxStudyPermissions.TabIndex = 111;
            this.textBoxStudyPermissions.Text = "^line1\r\nline2\r\nline3\r\n";
            this.textBoxStudyPermissions.Visible = false;
            this.textBoxStudyPermissions.Click += new System.EventHandler(this.textBoxStudyPermissions_Click);
            // 
            // labelStudyPermissions
            // 
            this.labelStudyPermissions.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.labelStudyPermissions.Location = new System.Drawing.Point(778, 135);
            this.labelStudyPermissions.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelStudyPermissions.Name = "labelStudyPermissions";
            this.labelStudyPermissions.Size = new System.Drawing.Size(156, 21);
            this.labelStudyPermissions.TabIndex = 110;
            this.labelStudyPermissions.Text = "Required permissions:";
            this.labelStudyPermissions.Visible = false;
            // 
            // labelStudyPhysicianInstruction
            // 
            this.labelStudyPhysicianInstruction.AutoSize = true;
            this.labelStudyPhysicianInstruction.Font = new System.Drawing.Font("Arial", 10F);
            this.labelStudyPhysicianInstruction.Location = new System.Drawing.Point(177, 174);
            this.labelStudyPhysicianInstruction.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelStudyPhysicianInstruction.Name = "labelStudyPhysicianInstruction";
            this.labelStudyPhysicianInstruction.Size = new System.Drawing.Size(107, 16);
            this.labelStudyPhysicianInstruction.TabIndex = 107;
            this.labelStudyPhysicianInstruction.Text = "Study free text::";
            // 
            // labelStudyStartDate
            // 
            this.labelStudyStartDate.AutoSize = true;
            this.labelStudyStartDate.Font = new System.Drawing.Font("Arial", 10F);
            this.labelStudyStartDate.Location = new System.Drawing.Point(5, 68);
            this.labelStudyStartDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelStudyStartDate.Name = "labelStudyStartDate";
            this.labelStudyStartDate.Size = new System.Drawing.Size(74, 16);
            this.labelStudyStartDate.TabIndex = 104;
            this.labelStudyStartDate.Text = "Start date:";
            // 
            // dateTimePickerStartDate
            // 
            this.dateTimePickerStartDate.CustomFormat = "yyyy-MM-dd";
            this.dateTimePickerStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerStartDate.Location = new System.Drawing.Point(94, 68);
            this.dateTimePickerStartDate.Margin = new System.Windows.Forms.Padding(4);
            this.dateTimePickerStartDate.Name = "dateTimePickerStartDate";
            this.dateTimePickerStartDate.Size = new System.Drawing.Size(113, 23);
            this.dateTimePickerStartDate.TabIndex = 105;
            // 
            // labelStudyHospitalName
            // 
            this.labelStudyHospitalName.AutoSize = true;
            this.labelStudyHospitalName.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelStudyHospitalName.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.labelStudyHospitalName.Location = new System.Drawing.Point(4, 41);
            this.labelStudyHospitalName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelStudyHospitalName.Name = "labelStudyHospitalName";
            this.labelStudyHospitalName.Size = new System.Drawing.Size(80, 16);
            this.labelStudyHospitalName.TabIndex = 106;
            this.labelStudyHospitalName.Text = "Sponsor: *";
            // 
            // comboBoxStudyHospitalNameList
            // 
            this.comboBoxStudyHospitalNameList.DropDownWidth = 200;
            this.comboBoxStudyHospitalNameList.FormattingEnabled = true;
            this.comboBoxStudyHospitalNameList.Items.AddRange(new object[] {
            "Floria Hospital",
            "New York Hospital"});
            this.comboBoxStudyHospitalNameList.Location = new System.Drawing.Point(94, 38);
            this.comboBoxStudyHospitalNameList.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxStudyHospitalNameList.MaxDropDownItems = 7;
            this.comboBoxStudyHospitalNameList.Name = "comboBoxStudyHospitalNameList";
            this.comboBoxStudyHospitalNameList.Size = new System.Drawing.Size(175, 24);
            this.comboBoxStudyHospitalNameList.TabIndex = 108;
            this.comboBoxStudyHospitalNameList.Text = "^Txt";
            // 
            // buttonRandom
            // 
            this.buttonRandom.Location = new System.Drawing.Point(109, 165);
            this.buttonRandom.Name = "buttonRandom";
            this.buttonRandom.Size = new System.Drawing.Size(21, 25);
            this.buttonRandom.TabIndex = 0;
            this.buttonRandom.Text = "R";
            this.buttonRandom.UseVisualStyleBackColor = true;
            this.buttonRandom.Click += new System.EventHandler(this.buttonRandom_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(211, 12);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 16);
            this.label7.TabIndex = 73;
            this.label7.Text = "max 16";
            // 
            // checkBoxActive
            // 
            this.checkBoxActive.AutoSize = true;
            this.checkBoxActive.Location = new System.Drawing.Point(479, 10);
            this.checkBoxActive.Name = "checkBoxActive";
            this.checkBoxActive.Size = new System.Drawing.Size(65, 20);
            this.checkBoxActive.TabIndex = 68;
            this.checkBoxActive.Text = "Active";
            this.checkBoxActive.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBoxActive.UseVisualStyleBackColor = true;
            // 
            // textBoxNr
            // 
            this.textBoxNr.Location = new System.Drawing.Point(374, 9);
            this.textBoxNr.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxNr.Name = "textBoxNr";
            this.textBoxNr.Size = new System.Drawing.Size(96, 23);
            this.textBoxNr.TabIndex = 67;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(284, 12);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 16);
            this.label3.TabIndex = 66;
            this.label3.Text = "Trial nr:";
            // 
            // textBoxLabel
            // 
            this.textBoxLabel.Location = new System.Drawing.Point(94, 9);
            this.textBoxLabel.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxLabel.MaxLength = 18;
            this.textBoxLabel.Name = "textBoxLabel";
            this.textBoxLabel.Size = new System.Drawing.Size(113, 23);
            this.textBoxLabel.TabIndex = 65;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.label1.Location = new System.Drawing.Point(5, 12);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 16);
            this.label1.TabIndex = 64;
            this.label1.Text = "Name: *";
            // 
            // dataGridViewList
            // 
            this.dataGridViewList.AllowDrop = true;
            this.dataGridViewList.AllowUserToAddRows = false;
            this.dataGridViewList.AllowUserToDeleteRows = false;
            this.dataGridViewList.AllowUserToOrderColumns = true;
            this.dataGridViewList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewList.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.dataGridViewList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TrialNr,
            this.ColumnActive,
            this.Column1ClientName,
            this.ColumnMainClient,
            this.ColumnStartDate,
            this.ColumnStopDate,
            this.ColumnDays,
            this.ColumnProcCodes,
            this.ColumnPermissions});
            this.dataGridViewList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewList.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewList.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridViewList.Name = "dataGridViewList";
            this.dataGridViewList.ReadOnly = true;
            this.dataGridViewList.RowHeadersVisible = false;
            this.dataGridViewList.RowTemplate.Height = 33;
            this.dataGridViewList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewList.ShowEditingIcon = false;
            this.dataGridViewList.Size = new System.Drawing.Size(1102, 252);
            this.dataGridViewList.TabIndex = 31;
            this.dataGridViewList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dataGridViewList.Click += new System.EventHandler(this.dataGridViewList_Click);
            this.dataGridViewList.DoubleClick += new System.EventHandler(this.dataGridClientList_DoubleClick);
            // 
            // TrialNr
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.TrialNr.DefaultCellStyle = dataGridViewCellStyle1;
            this.TrialNr.HeaderText = "Trial Nr";
            this.TrialNr.Name = "TrialNr";
            this.TrialNr.ReadOnly = true;
            this.TrialNr.Width = 80;
            // 
            // ColumnActive
            // 
            this.ColumnActive.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ColumnActive.HeaderText = "Active";
            this.ColumnActive.Name = "ColumnActive";
            this.ColumnActive.ReadOnly = true;
            this.ColumnActive.Width = 70;
            // 
            // Column1ClientName
            // 
            this.Column1ClientName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column1ClientName.HeaderText = "Name";
            this.Column1ClientName.MaxInputLength = 30;
            this.Column1ClientName.Name = "Column1ClientName";
            this.Column1ClientName.ReadOnly = true;
            this.Column1ClientName.Width = 70;
            // 
            // ColumnMainClient
            // 
            this.ColumnMainClient.HeaderText = "Main Client";
            this.ColumnMainClient.Name = "ColumnMainClient";
            this.ColumnMainClient.ReadOnly = true;
            this.ColumnMainClient.Width = 102;
            // 
            // ColumnStartDate
            // 
            this.ColumnStartDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColumnStartDate.HeaderText = "Start Date";
            this.ColumnStartDate.MaxInputLength = 30;
            this.ColumnStartDate.Name = "ColumnStartDate";
            this.ColumnStartDate.ReadOnly = true;
            this.ColumnStartDate.Width = 97;
            // 
            // ColumnStopDate
            // 
            this.ColumnStopDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColumnStopDate.HeaderText = "Stop Date";
            this.ColumnStopDate.MaxInputLength = 32;
            this.ColumnStopDate.Name = "ColumnStopDate";
            this.ColumnStopDate.ReadOnly = true;
            this.ColumnStopDate.Width = 96;
            // 
            // ColumnDays
            // 
            this.ColumnDays.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColumnDays.HeaderText = "Days";
            this.ColumnDays.MaxInputLength = 25;
            this.ColumnDays.Name = "ColumnDays";
            this.ColumnDays.ReadOnly = true;
            this.ColumnDays.Width = 65;
            // 
            // ColumnProcCodes
            // 
            this.ColumnProcCodes.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColumnProcCodes.HeaderText = "ICD10";
            this.ColumnProcCodes.MaxInputLength = 20;
            this.ColumnProcCodes.Name = "ColumnProcCodes";
            this.ColumnProcCodes.ReadOnly = true;
            this.ColumnProcCodes.Width = 71;
            // 
            // ColumnPermissions
            // 
            this.ColumnPermissions.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColumnPermissions.HeaderText = "Location";
            this.ColumnPermissions.MaxInputLength = 15;
            this.ColumnPermissions.Name = "ColumnPermissions";
            this.ColumnPermissions.ReadOnly = true;
            this.ColumnPermissions.Width = 87;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel8);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 10);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1102, 20);
            this.panel3.TabIndex = 135;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.labelTrialError);
            this.panel8.Controls.Add(this.panel38);
            this.panel8.Controls.Add(this.labelTrialDetails);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(1102, 20);
            this.panel8.TabIndex = 75;
            // 
            // labelTrialError
            // 
            this.labelTrialError.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelTrialError.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTrialError.Location = new System.Drawing.Point(190, 0);
            this.labelTrialError.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTrialError.Name = "labelTrialError";
            this.labelTrialError.Size = new System.Drawing.Size(263, 20);
            this.labelTrialError.TabIndex = 75;
            this.labelTrialError.Text = "...";
            this.labelTrialError.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel38
            // 
            this.panel38.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel38.Location = new System.Drawing.Point(120, 0);
            this.panel38.Name = "panel38";
            this.panel38.Size = new System.Drawing.Size(70, 20);
            this.panel38.TabIndex = 73;
            // 
            // panel9
            // 
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 30);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(1102, 15);
            this.panel9.TabIndex = 136;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.panel14);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(0, 45);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(1102, 202);
            this.panel10.TabIndex = 137;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.panel7);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel14.Location = new System.Drawing.Point(0, 0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(1102, 202);
            this.panel14.TabIndex = 2;
            // 
            // panel11
            // 
            this.panel11.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel11.Location = new System.Drawing.Point(0, 247);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(1102, 10);
            this.panel11.TabIndex = 138;
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.panel15.Controls.Add(this.buttonAddUpdate);
            this.panel15.Controls.Add(this.buttonCancel);
            this.panel15.Controls.Add(this.buttonSearch);
            this.panel15.Controls.Add(this.buttonDup);
            this.panel15.Controls.Add(this.buttonClear);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel15.Location = new System.Drawing.Point(0, 257);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(1102, 26);
            this.panel15.TabIndex = 139;
            // 
            // buttonAddUpdate
            // 
            this.buttonAddUpdate.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonAddUpdate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonAddUpdate.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonAddUpdate.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonAddUpdate.FlatAppearance.BorderSize = 0;
            this.buttonAddUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddUpdate.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonAddUpdate.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonAddUpdate.Location = new System.Drawing.Point(906, 0);
            this.buttonAddUpdate.Margin = new System.Windows.Forms.Padding(2);
            this.buttonAddUpdate.Name = "buttonAddUpdate";
            this.buttonAddUpdate.Size = new System.Drawing.Size(123, 26);
            this.buttonAddUpdate.TabIndex = 1;
            this.buttonAddUpdate.Text = "Add";
            this.buttonAddUpdate.UseVisualStyleBackColor = false;
            this.buttonAddUpdate.Click += new System.EventHandler(this.buttonAddUpdate_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonCancel.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonCancel.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonCancel.FlatAppearance.BorderSize = 0;
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCancel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonCancel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonCancel.Location = new System.Drawing.Point(1029, 0);
            this.buttonCancel.Margin = new System.Windows.Forms.Padding(2);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(73, 26);
            this.buttonCancel.TabIndex = 1;
            this.buttonCancel.Text = "Close";
            this.buttonCancel.UseVisualStyleBackColor = false;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonSearch
            // 
            this.buttonSearch.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonSearch.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonSearch.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonSearch.FlatAppearance.BorderSize = 0;
            this.buttonSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSearch.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonSearch.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonSearch.Location = new System.Drawing.Point(161, 0);
            this.buttonSearch.Margin = new System.Windows.Forms.Padding(2);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(78, 26);
            this.buttonSearch.TabIndex = 6;
            this.buttonSearch.Text = "Search";
            this.buttonSearch.UseVisualStyleBackColor = false;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // buttonDup
            // 
            this.buttonDup.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonDup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonDup.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonDup.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonDup.FlatAppearance.BorderSize = 0;
            this.buttonDup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDup.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonDup.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonDup.Location = new System.Drawing.Point(65, 0);
            this.buttonDup.Margin = new System.Windows.Forms.Padding(2);
            this.buttonDup.Name = "buttonDup";
            this.buttonDup.Size = new System.Drawing.Size(96, 26);
            this.buttonDup.TabIndex = 5;
            this.buttonDup.Text = "Duplicate";
            this.buttonDup.UseVisualStyleBackColor = false;
            this.buttonDup.Click += new System.EventHandler(this.buttonDup_Click);
            // 
            // buttonClear
            // 
            this.buttonClear.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonClear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonClear.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonClear.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonClear.FlatAppearance.BorderSize = 0;
            this.buttonClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonClear.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonClear.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonClear.Location = new System.Drawing.Point(0, 0);
            this.buttonClear.Margin = new System.Windows.Forms.Padding(2);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(65, 26);
            this.buttonClear.TabIndex = 4;
            this.buttonClear.Text = "Clear";
            this.buttonClear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonClear.UseVisualStyleBackColor = false;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // panel21
            // 
            this.panel21.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel21.Location = new System.Drawing.Point(0, 283);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(1102, 10);
            this.panel21.TabIndex = 141;
            // 
            // panel25
            // 
            this.panel25.Controls.Add(this.panel29);
            this.panel25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel25.Location = new System.Drawing.Point(0, 293);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(1102, 252);
            this.panel25.TabIndex = 142;
            // 
            // panel29
            // 
            this.panel29.Controls.Add(this.dataGridViewList);
            this.panel29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel29.Location = new System.Drawing.Point(0, 0);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(1102, 252);
            this.panel29.TabIndex = 3;
            // 
            // panel26
            // 
            this.panel26.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel26.Location = new System.Drawing.Point(0, 545);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(1102, 18);
            this.panel26.TabIndex = 143;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.panel1.Controls.Add(this.labelSelected);
            this.panel1.Controls.Add(this.buttonSelect);
            this.panel1.Controls.Add(this.labelListN);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.labelResultN);
            this.panel1.Controls.Add(this.checkBoxShowSearchResult);
            this.panel1.Controls.Add(this.checkBoxShowDeactivated);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.buttonDuplicate);
            this.panel1.Controls.Add(this.buttonEdit);
            this.panel1.Controls.Add(this.buttonDisable);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 563);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1102, 26);
            this.panel1.TabIndex = 144;
            // 
            // labelSelected
            // 
            this.labelSelected.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSelected.Font = new System.Drawing.Font("Arial", 10F);
            this.labelSelected.ForeColor = System.Drawing.Color.White;
            this.labelSelected.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.labelSelected.Location = new System.Drawing.Point(488, 0);
            this.labelSelected.Name = "labelSelected";
            this.labelSelected.Size = new System.Drawing.Size(56, 26);
            this.labelSelected.TabIndex = 16;
            this.labelSelected.Text = "0";
            this.labelSelected.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelSelected.Click += new System.EventHandler(this.labelSelected_Click);
            // 
            // buttonSelect
            // 
            this.buttonSelect.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonSelect.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonSelect.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonSelect.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonSelect.FlatAppearance.BorderSize = 0;
            this.buttonSelect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSelect.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonSelect.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonSelect.Location = new System.Drawing.Point(392, 0);
            this.buttonSelect.Margin = new System.Windows.Forms.Padding(2);
            this.buttonSelect.Name = "buttonSelect";
            this.buttonSelect.Size = new System.Drawing.Size(96, 26);
            this.buttonSelect.TabIndex = 15;
            this.buttonSelect.Text = "Select";
            this.buttonSelect.UseVisualStyleBackColor = false;
            this.buttonSelect.Click += new System.EventHandler(this.buttonSelect_Click);
            // 
            // labelListN
            // 
            this.labelListN.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelListN.Font = new System.Drawing.Font("Arial", 10F);
            this.labelListN.ForeColor = System.Drawing.Color.White;
            this.labelListN.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.labelListN.Location = new System.Drawing.Point(342, 0);
            this.labelListN.Name = "labelListN";
            this.labelListN.Size = new System.Drawing.Size(50, 26);
            this.labelListN.TabIndex = 13;
            this.labelListN.Text = "0";
            this.labelListN.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Left;
            this.label4.Font = new System.Drawing.Font("Arial", 10F);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label4.Location = new System.Drawing.Point(326, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(16, 26);
            this.label4.TabIndex = 12;
            this.label4.Text = " / ";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelResultN
            // 
            this.labelResultN.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelResultN.Font = new System.Drawing.Font("Arial", 10F);
            this.labelResultN.ForeColor = System.Drawing.Color.White;
            this.labelResultN.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.labelResultN.Location = new System.Drawing.Point(284, 0);
            this.labelResultN.Name = "labelResultN";
            this.labelResultN.Size = new System.Drawing.Size(42, 26);
            this.labelResultN.TabIndex = 11;
            this.labelResultN.Text = "0";
            this.labelResultN.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // checkBoxShowSearchResult
            // 
            this.checkBoxShowSearchResult.AutoSize = true;
            this.checkBoxShowSearchResult.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxShowSearchResult.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxShowSearchResult.ForeColor = System.Drawing.Color.White;
            this.checkBoxShowSearchResult.Location = new System.Drawing.Point(130, 0);
            this.checkBoxShowSearchResult.Name = "checkBoxShowSearchResult";
            this.checkBoxShowSearchResult.Size = new System.Drawing.Size(154, 26);
            this.checkBoxShowSearchResult.TabIndex = 10;
            this.checkBoxShowSearchResult.Text = "Show Search Result";
            this.checkBoxShowSearchResult.UseVisualStyleBackColor = true;
            this.checkBoxShowSearchResult.CheckedChanged += new System.EventHandler(this.checkBoxShowSearchResult_CheckedChanged);
            // 
            // checkBoxShowDeactivated
            // 
            this.checkBoxShowDeactivated.AutoSize = true;
            this.checkBoxShowDeactivated.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxShowDeactivated.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxShowDeactivated.ForeColor = System.Drawing.Color.White;
            this.checkBoxShowDeactivated.Location = new System.Drawing.Point(10, 0);
            this.checkBoxShowDeactivated.Name = "checkBoxShowDeactivated";
            this.checkBoxShowDeactivated.Size = new System.Drawing.Size(120, 26);
            this.checkBoxShowDeactivated.TabIndex = 9;
            this.checkBoxShowDeactivated.Text = "Show Disabled";
            this.checkBoxShowDeactivated.UseVisualStyleBackColor = true;
            this.checkBoxShowDeactivated.CheckedChanged += new System.EventHandler(this.checkBoxShowDeactivated_CheckedChanged);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button1.Dock = System.Windows.Forms.DockStyle.Right;
            this.button1.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button1.Location = new System.Drawing.Point(644, 0);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(96, 26);
            this.button1.TabIndex = 11;
            this.button1.Text = "Refresh";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonDuplicate
            // 
            this.buttonDuplicate.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonDuplicate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonDuplicate.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonDuplicate.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonDuplicate.FlatAppearance.BorderSize = 0;
            this.buttonDuplicate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDuplicate.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonDuplicate.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonDuplicate.Location = new System.Drawing.Point(740, 0);
            this.buttonDuplicate.Margin = new System.Windows.Forms.Padding(2);
            this.buttonDuplicate.Name = "buttonDuplicate";
            this.buttonDuplicate.Size = new System.Drawing.Size(138, 26);
            this.buttonDuplicate.TabIndex = 1;
            this.buttonDuplicate.Text = "Duplicate Trial";
            this.buttonDuplicate.UseVisualStyleBackColor = false;
            this.buttonDuplicate.Click += new System.EventHandler(this.buttonDuplicate_Click);
            // 
            // buttonEdit
            // 
            this.buttonEdit.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonEdit.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonEdit.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonEdit.FlatAppearance.BorderSize = 0;
            this.buttonEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEdit.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonEdit.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonEdit.Location = new System.Drawing.Point(878, 0);
            this.buttonEdit.Margin = new System.Windows.Forms.Padding(2);
            this.buttonEdit.Name = "buttonEdit";
            this.buttonEdit.Size = new System.Drawing.Size(102, 26);
            this.buttonEdit.TabIndex = 1;
            this.buttonEdit.Text = "Edit Trial";
            this.buttonEdit.UseVisualStyleBackColor = false;
            this.buttonEdit.Click += new System.EventHandler(this.buttonEdit_Click);
            // 
            // buttonDisable
            // 
            this.buttonDisable.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonDisable.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonDisable.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonDisable.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonDisable.FlatAppearance.BorderSize = 0;
            this.buttonDisable.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDisable.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonDisable.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonDisable.Location = new System.Drawing.Point(980, 0);
            this.buttonDisable.Margin = new System.Windows.Forms.Padding(2);
            this.buttonDisable.Name = "buttonDisable";
            this.buttonDisable.Size = new System.Drawing.Size(122, 26);
            this.buttonDisable.TabIndex = 1;
            this.buttonDisable.Text = "Disable Client";
            this.buttonDisable.UseVisualStyleBackColor = false;
            this.buttonDisable.Click += new System.EventHandler(this.buttonDisable_Click);
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(10, 26);
            this.panel2.TabIndex = 14;
            // 
            // CTrialInfoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(1102, 589);
            this.Controls.Add(this.panel25);
            this.Controls.Add(this.panel26);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel21);
            this.Controls.Add(this.panel15);
            this.Controls.Add(this.panel11);
            this.Controls.Add(this.panel10);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel5);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "CTrialInfoForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Trials";
            this.Load += new System.EventHandler(this.CAddClientsForm_Load);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewList)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel25.ResumeLayout(false);
            this.panel29.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label labelTrialDetails;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.DataGridView dataGridViewList;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonDuplicate;
        private System.Windows.Forms.Button buttonEdit;
        private System.Windows.Forms.Button buttonDisable;
        private System.Windows.Forms.TextBox textBoxLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonDup;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Button buttonRandom;
        private System.Windows.Forms.TextBox textBoxNr;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonSearch;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox checkBoxActive;
        private System.Windows.Forms.Label labelTrialError;
        private System.Windows.Forms.Panel panel38;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label labelListN;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelResultN;
        private System.Windows.Forms.CheckBox checkBoxShowSearchResult;
        private System.Windows.Forms.CheckBox checkBoxShowDeactivated;
        private System.Windows.Forms.Button buttonAddUpdate;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label labelSelected;
        private System.Windows.Forms.Button buttonSelect;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePickerStopDate;
        private System.Windows.Forms.TextBox textBoxStudyPermissions;
        private System.Windows.Forms.Label labelStudyPermissions;
        private System.Windows.Forms.Label labelStudyPhysicianInstruction;
        private System.Windows.Forms.Label labelStudyStartDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerStartDate;
        private System.Windows.Forms.Label labelStudyHospitalName;
        private System.Windows.Forms.ComboBox comboBoxStudyHospitalNameList;
        private System.Windows.Forms.CheckedListBox checkedListBoxMctInterval;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBoxStudyMonitoringDaysList;
        private System.Windows.Forms.Label labelStudyMonitoringDays;
        private System.Windows.Forms.ComboBox comboBoxStudyTypeList;
        private System.Windows.Forms.Label labelStudyType;
        private System.Windows.Forms.CheckedListBox checkedListBoxReportInterval;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ListView listViewStudyProcedures;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Button buttonAddICD;
        private System.Windows.Forms.TextBox textBoxStudyFreeText;
        private System.Windows.Forms.Label labelSite;
        private System.Windows.Forms.ComboBox comboBoxLocation;
        private System.Windows.Forms.Label labelStudyLabel;
        private System.Windows.Forms.TextBox textBoxStudyNote;
        private System.Windows.Forms.Button buttonCalcDays;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrialNr;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnActive;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1ClientName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnMainClient;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnStartDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnStopDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDays;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnProcCodes;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnPermissions;
    }
}