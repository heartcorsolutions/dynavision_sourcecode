﻿namespace EventboardEntryForms
{
    partial class CPreviousFindingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CPreviousFindingsForm));
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.labelNrAnalyzed = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.panel27 = new System.Windows.Forms.Panel();
            this.labelGender = new System.Windows.Forms.Label();
            this.panel25 = new System.Windows.Forms.Panel();
            this.labelAge = new System.Windows.Forms.Label();
            this.panel24 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.panel23 = new System.Windows.Forms.Panel();
            this.labelDoB = new System.Windows.Forms.Label();
            this.panel21 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel20 = new System.Windows.Forms.Panel();
            this.labelFullName = new System.Windows.Forms.Label();
            this.panel15 = new System.Windows.Forms.Panel();
            this.labelPreviousFindingsList = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.labelStudyNote = new System.Windows.Forms.Label();
            this.labelStudyIndex = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel33 = new System.Windows.Forms.Panel();
            this.labelRecordInfo = new System.Windows.Forms.Label();
            this.panelStudyButtons = new System.Windows.Forms.Panel();
            this.buttonAmplRange = new System.Windows.Forms.Button();
            this.checkBoxBlackWhite = new System.Windows.Forms.CheckBox();
            this.checkBoxPlot2ch = new System.Windows.Forms.CheckBox();
            this.checkBoxAnonymize = new System.Windows.Forms.CheckBox();
            this.checkBoxReducePdf = new System.Windows.Forms.CheckBox();
            this.button2 = new System.Windows.Forms.Button();
            this.buttonPrintPreviousFindings = new System.Windows.Forms.Button();
            this.labelPdfNr = new System.Windows.Forms.Label();
            this.labelReportResult = new System.Windows.Forms.Label();
            this.buttonViewPdf = new System.Windows.Forms.Button();
            this.buttonMCT = new System.Windows.Forms.Button();
            this.buttonEndStudy = new System.Windows.Forms.Button();
            this.panelSelect = new System.Windows.Forms.Panel();
            this.checkBoxModifyAbN = new System.Windows.Forms.CheckBox();
            this.checkBoxModifyManual = new System.Windows.Forms.CheckBox();
            this.checkBoxModifyQC = new System.Windows.Forms.CheckBox();
            this.checkBoxModifyNormal = new System.Windows.Forms.CheckBox();
            this.checkBoxModifyHR1 = new System.Windows.Forms.CheckBox();
            this.checkBoxModifyHR0 = new System.Windows.Forms.CheckBox();
            this.panel41 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonSetSelection = new System.Windows.Forms.Button();
            this.panel11 = new System.Windows.Forms.Panel();
            this.checkBoxModifyOther = new System.Windows.Forms.CheckBox();
            this.checkBoxModifyBL = new System.Windows.Forms.CheckBox();
            this.checkBoxModifyVT = new System.Windows.Forms.CheckBox();
            this.checkBoxModifyPace = new System.Windows.Forms.CheckBox();
            this.checkBoxModifyPVC = new System.Windows.Forms.CheckBox();
            this.checkBoxModifyPAC = new System.Windows.Forms.CheckBox();
            this.checkBoxModifyAfl = new System.Windows.Forms.CheckBox();
            this.checkBoxModifyAF = new System.Windows.Forms.CheckBox();
            this.checkBoxModifyPause = new System.Windows.Forms.CheckBox();
            this.checkBoxModifyBrady = new System.Windows.Forms.CheckBox();
            this.checkBoxModifyTachy = new System.Windows.Forms.CheckBox();
            this.checkBoxModifyAll = new System.Windows.Forms.CheckBox();
            this.label18 = new System.Windows.Forms.Label();
            this.checkBoxStrip = new System.Windows.Forms.CheckBox();
            this.checkBoxTable = new System.Windows.Forms.CheckBox();
            this.label24 = new System.Windows.Forms.Label();
            this.buttonUncheckAll = new System.Windows.Forms.Button();
            this.panelStudyPeriod = new System.Windows.Forms.Panel();
            this.panel37 = new System.Windows.Forms.Panel();
            this.panel22 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.labelTBPAO = new System.Windows.Forms.Label();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel65 = new System.Windows.Forms.Panel();
            this.panel58 = new System.Windows.Forms.Panel();
            this.panel64 = new System.Windows.Forms.Panel();
            this.buttonP24 = new System.Windows.Forms.Button();
            this.buttonP6 = new System.Windows.Forms.Button();
            this.buttonP1 = new System.Windows.Forms.Button();
            this.button0h = new System.Windows.Forms.Button();
            this.buttonM1 = new System.Windows.Forms.Button();
            this.buttonM6 = new System.Windows.Forms.Button();
            this.buttonM24 = new System.Windows.Forms.Button();
            this.panel62 = new System.Windows.Forms.Panel();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBoxToHour = new System.Windows.Forms.TextBox();
            this.dateTimePickerTo = new System.Windows.Forms.DateTimePicker();
            this.labelTo = new System.Windows.Forms.Label();
            this.panel57 = new System.Windows.Forms.Panel();
            this.buttonSetTimeFrame = new System.Windows.Forms.Button();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBoxFromHour = new System.Windows.Forms.TextBox();
            this.dateTimePickerFrom = new System.Windows.Forms.DateTimePicker();
            this.labelFrom = new System.Windows.Forms.Label();
            this.panel35 = new System.Windows.Forms.Panel();
            this.panel54 = new System.Windows.Forms.Panel();
            this.radioButtonHour = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButtonMonth = new System.Windows.Forms.RadioButton();
            this.radioButtonPeriod = new System.Windows.Forms.RadioButton();
            this.radioButtonEOS = new System.Windows.Forms.RadioButton();
            this.radioButtonWeek = new System.Windows.Forms.RadioButton();
            this.radioButtonDay = new System.Windows.Forms.RadioButton();
            this.label36 = new System.Windows.Forms.Label();
            this.panelStudy = new System.Windows.Forms.Panel();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panelRows = new System.Windows.Forms.Panel();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RecState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnEventDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnSampleNr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnAnalysisDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnFindings = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnRemarks = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.N = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.findingsClass = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RecordIX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RecordET = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RecordingType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel8 = new System.Windows.Forms.Panel();
            this.labelListAbN = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.labelListNorm = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.labelListOther = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.labelListBL = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.labelListVT = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.labelListPace = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.labelListPVC = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.labelListPac = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.labelListAfl = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.labelListAfib = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.labelListPause = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.labelListBrady = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labelListTachy = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.buttonSortET = new System.Windows.Forms.Button();
            this.panel19 = new System.Windows.Forms.Panel();
            this.labelHR = new System.Windows.Forms.Label();
            this.labelListExtra = new System.Windows.Forms.Label();
            this.buttonDisableAn = new System.Windows.Forms.Button();
            this.panel29 = new System.Windows.Forms.Panel();
            this.buttonReloadList = new System.Windows.Forms.Button();
            this.panel28 = new System.Windows.Forms.Panel();
            this.buttonOpenAnalysis = new System.Windows.Forms.Button();
            this.panel32 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.labelListQC = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.labelListManuals = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labelListStrips = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelListTable = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel26 = new System.Windows.Forms.Panel();
            this.splitContainerFindingsConclusion = new System.Windows.Forms.SplitContainer();
            this.textBoxSummary = new System.Windows.Forms.TextBox();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel34 = new System.Windows.Forms.Panel();
            this.labelUseExluded = new System.Windows.Forms.Label();
            this.checkBoxQCD = new System.Windows.Forms.CheckBox();
            this.buttonClearFindings = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.panel39 = new System.Windows.Forms.Panel();
            this.textBoxConclusion = new System.Windows.Forms.TextBox();
            this.panel31 = new System.Windows.Forms.Panel();
            this.panel38 = new System.Windows.Forms.Panel();
            this.buttonClearConclusion = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.panel36 = new System.Windows.Forms.Panel();
            this.panel42 = new System.Windows.Forms.Panel();
            this.panel30 = new System.Windows.Forms.Panel();
            this.panel40 = new System.Windows.Forms.Panel();
            this.panel6.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel33.SuspendLayout();
            this.panelStudyButtons.SuspendLayout();
            this.panelSelect.SuspendLayout();
            this.panelStudyPeriod.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel65.SuspendLayout();
            this.panel58.SuspendLayout();
            this.panel64.SuspendLayout();
            this.panel62.SuspendLayout();
            this.panel57.SuspendLayout();
            this.panel35.SuspendLayout();
            this.panel54.SuspendLayout();
            this.panelStudy.SuspendLayout();
            this.panelRows.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.panel8.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel26.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerFindingsConclusion)).BeginInit();
            this.splitContainerFindingsConclusion.Panel1.SuspendLayout();
            this.splitContainerFindingsConclusion.Panel2.SuspendLayout();
            this.splitContainerFindingsConclusion.SuspendLayout();
            this.panel34.SuspendLayout();
            this.panel38.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel5
            // 
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1364, 5);
            this.panel5.TabIndex = 1;
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.labelNrAnalyzed);
            this.panel6.Controls.Add(this.label21);
            this.panel6.Controls.Add(this.panel27);
            this.panel6.Controls.Add(this.panel25);
            this.panel6.Controls.Add(this.panel24);
            this.panel6.Controls.Add(this.panel23);
            this.panel6.Controls.Add(this.panel21);
            this.panel6.Controls.Add(this.panel20);
            this.panel6.Controls.Add(this.panel15);
            this.panel6.Controls.Add(this.panel4);
            this.panel6.Controls.Add(this.panel3);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 5);
            this.panel6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1364, 25);
            this.panel6.TabIndex = 2;
            // 
            // labelNrAnalyzed
            // 
            this.labelNrAnalyzed.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelNrAnalyzed.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelNrAnalyzed.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelNrAnalyzed.Location = new System.Drawing.Point(1202, 0);
            this.labelNrAnalyzed.Name = "labelNrAnalyzed";
            this.labelNrAnalyzed.Size = new System.Drawing.Size(72, 23);
            this.labelNrAnalyzed.TabIndex = 12;
            this.labelNrAnalyzed.Text = "^9999";
            this.labelNrAnalyzed.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label21
            // 
            this.label21.Dock = System.Windows.Forms.DockStyle.Right;
            this.label21.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label21.Location = new System.Drawing.Point(1274, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(88, 23);
            this.label21.TabIndex = 11;
            this.label21.Text = "Analyzed";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel27
            // 
            this.panel27.Controls.Add(this.labelGender);
            this.panel27.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel27.Location = new System.Drawing.Point(1053, 0);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(78, 23);
            this.panel27.TabIndex = 10;
            // 
            // labelGender
            // 
            this.labelGender.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelGender.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelGender.Location = new System.Drawing.Point(0, 0);
            this.labelGender.Name = "labelGender";
            this.labelGender.Size = new System.Drawing.Size(66, 23);
            this.labelGender.TabIndex = 6;
            this.labelGender.Text = "^Gender";
            this.labelGender.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel25
            // 
            this.panel25.Controls.Add(this.labelAge);
            this.panel25.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel25.Location = new System.Drawing.Point(1007, 0);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(46, 23);
            this.panel25.TabIndex = 8;
            // 
            // labelAge
            // 
            this.labelAge.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelAge.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelAge.Location = new System.Drawing.Point(0, 0);
            this.labelAge.Name = "labelAge";
            this.labelAge.Size = new System.Drawing.Size(43, 23);
            this.labelAge.TabIndex = 4;
            this.labelAge.Text = "^99";
            this.labelAge.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel24
            // 
            this.panel24.Controls.Add(this.label5);
            this.panel24.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel24.Location = new System.Drawing.Point(952, 0);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(55, 23);
            this.panel24.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Right;
            this.label5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 23);
            this.label5.TabIndex = 3;
            this.label5.Text = "Age:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel23
            // 
            this.panel23.Controls.Add(this.labelDoB);
            this.panel23.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel23.Location = new System.Drawing.Point(850, 0);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(102, 23);
            this.panel23.TabIndex = 6;
            // 
            // labelDoB
            // 
            this.labelDoB.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelDoB.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelDoB.Location = new System.Drawing.Point(0, 0);
            this.labelDoB.Name = "labelDoB";
            this.labelDoB.Size = new System.Drawing.Size(94, 23);
            this.labelDoB.TabIndex = 3;
            this.labelDoB.Text = "^99/99/9999";
            this.labelDoB.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel21
            // 
            this.panel21.Controls.Add(this.label2);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel21.Location = new System.Drawing.Point(729, 0);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(121, 23);
            this.panel21.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Right;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 23);
            this.label2.TabIndex = 2;
            this.label2.Text = "Date of Birth:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.labelFullName);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel20.Location = new System.Drawing.Point(297, 0);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(432, 23);
            this.panel20.TabIndex = 4;
            // 
            // labelFullName
            // 
            this.labelFullName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelFullName.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelFullName.Location = new System.Drawing.Point(0, 0);
            this.labelFullName.Name = "labelFullName";
            this.labelFullName.Size = new System.Drawing.Size(432, 23);
            this.labelFullName.TabIndex = 2;
            this.labelFullName.Text = "^Ottovordemgentschenfelde";
            this.labelFullName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.labelPreviousFindingsList);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel15.Location = new System.Drawing.Point(234, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(63, 23);
            this.panel15.TabIndex = 3;
            // 
            // labelPreviousFindingsList
            // 
            this.labelPreviousFindingsList.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPreviousFindingsList.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPreviousFindingsList.Location = new System.Drawing.Point(0, 0);
            this.labelPreviousFindingsList.Name = "labelPreviousFindingsList";
            this.labelPreviousFindingsList.Size = new System.Drawing.Size(66, 23);
            this.labelPreviousFindingsList.TabIndex = 1;
            this.labelPreviousFindingsList.Text = "Name:";
            this.labelPreviousFindingsList.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.labelStudyNote);
            this.panel4.Controls.Add(this.labelStudyIndex);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(84, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(150, 23);
            this.panel4.TabIndex = 2;
            // 
            // labelStudyNote
            // 
            this.labelStudyNote.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelStudyNote.Font = new System.Drawing.Font("Arial", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStudyNote.Location = new System.Drawing.Point(122, 0);
            this.labelStudyNote.Name = "labelStudyNote";
            this.labelStudyNote.Size = new System.Drawing.Size(22, 23);
            this.labelStudyNote.TabIndex = 3;
            this.labelStudyNote.Text = "sl\r\nsn";
            this.labelStudyNote.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelStudyNote.Click += new System.EventHandler(this.labelStudyNote_Click);
            // 
            // labelStudyIndex
            // 
            this.labelStudyIndex.AutoSize = true;
            this.labelStudyIndex.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelStudyIndex.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelStudyIndex.Location = new System.Drawing.Point(0, 0);
            this.labelStudyIndex.Name = "labelStudyIndex";
            this.labelStudyIndex.Size = new System.Drawing.Size(122, 19);
            this.labelStudyIndex.TabIndex = 2;
            this.labelStudyIndex.Text = "^313131 T2344";
            this.labelStudyIndex.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelStudyIndex.DoubleClick += new System.EventHandler(this.labelStudyIndex_DoubleClick);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(84, 23);
            this.panel3.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Left;
            this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 23);
            this.label4.TabIndex = 1;
            this.label4.Text = "Study nr:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label4.DoubleClick += new System.EventHandler(this.label4_DoubleClick);
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.panel33);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 30);
            this.panel7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1364, 20);
            this.panel7.TabIndex = 3;
            // 
            // panel33
            // 
            this.panel33.Controls.Add(this.labelRecordInfo);
            this.panel33.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel33.Location = new System.Drawing.Point(0, 0);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(1364, 19);
            this.panel33.TabIndex = 0;
            // 
            // labelRecordInfo
            // 
            this.labelRecordInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelRecordInfo.Location = new System.Drawing.Point(0, 0);
            this.labelRecordInfo.Name = "labelRecordInfo";
            this.labelRecordInfo.Size = new System.Drawing.Size(1364, 19);
            this.labelRecordInfo.TabIndex = 0;
            this.labelRecordInfo.Text = "?Records";
            this.labelRecordInfo.Click += new System.EventHandler(this.labelRecordInfo_Click);
            // 
            // panelStudyButtons
            // 
            this.panelStudyButtons.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.panelStudyButtons.Controls.Add(this.buttonAmplRange);
            this.panelStudyButtons.Controls.Add(this.checkBoxBlackWhite);
            this.panelStudyButtons.Controls.Add(this.checkBoxPlot2ch);
            this.panelStudyButtons.Controls.Add(this.checkBoxAnonymize);
            this.panelStudyButtons.Controls.Add(this.checkBoxReducePdf);
            this.panelStudyButtons.Controls.Add(this.button2);
            this.panelStudyButtons.Controls.Add(this.buttonPrintPreviousFindings);
            this.panelStudyButtons.Controls.Add(this.labelPdfNr);
            this.panelStudyButtons.Controls.Add(this.labelReportResult);
            this.panelStudyButtons.Controls.Add(this.buttonViewPdf);
            this.panelStudyButtons.Controls.Add(this.buttonMCT);
            this.panelStudyButtons.Controls.Add(this.buttonEndStudy);
            this.panelStudyButtons.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelStudyButtons.Location = new System.Drawing.Point(0, 815);
            this.panelStudyButtons.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelStudyButtons.Name = "panelStudyButtons";
            this.panelStudyButtons.Size = new System.Drawing.Size(1364, 46);
            this.panelStudyButtons.TabIndex = 6;
            // 
            // buttonAmplRange
            // 
            this.buttonAmplRange.AutoSize = true;
            this.buttonAmplRange.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonAmplRange.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonAmplRange.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonAmplRange.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonAmplRange.FlatAppearance.BorderSize = 0;
            this.buttonAmplRange.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAmplRange.Font = new System.Drawing.Font("Arial", 10F);
            this.buttonAmplRange.ForeColor = System.Drawing.Color.White;
            this.buttonAmplRange.Location = new System.Drawing.Point(741, 0);
            this.buttonAmplRange.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonAmplRange.Name = "buttonAmplRange";
            this.buttonAmplRange.Size = new System.Drawing.Size(82, 46);
            this.buttonAmplRange.TabIndex = 39;
            this.buttonAmplRange.Text = "Auto 4 \r\nAmpl\r\n\r\n";
            this.buttonAmplRange.UseVisualStyleBackColor = false;
            this.buttonAmplRange.Visible = false;
            this.buttonAmplRange.Click += new System.EventHandler(this.buttonAmplRange_Click);
            // 
            // checkBoxBlackWhite
            // 
            this.checkBoxBlackWhite.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.checkBoxBlackWhite.Dock = System.Windows.Forms.DockStyle.Right;
            this.checkBoxBlackWhite.ForeColor = System.Drawing.Color.White;
            this.checkBoxBlackWhite.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.checkBoxBlackWhite.Location = new System.Drawing.Point(823, 0);
            this.checkBoxBlackWhite.Name = "checkBoxBlackWhite";
            this.checkBoxBlackWhite.Size = new System.Drawing.Size(56, 46);
            this.checkBoxBlackWhite.TabIndex = 38;
            this.checkBoxBlackWhite.Text = "B&&W";
            this.checkBoxBlackWhite.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.checkBoxBlackWhite.UseVisualStyleBackColor = true;
            // 
            // checkBoxPlot2ch
            // 
            this.checkBoxPlot2ch.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.checkBoxPlot2ch.Dock = System.Windows.Forms.DockStyle.Right;
            this.checkBoxPlot2ch.ForeColor = System.Drawing.Color.White;
            this.checkBoxPlot2ch.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.checkBoxPlot2ch.Location = new System.Drawing.Point(879, 0);
            this.checkBoxPlot2ch.Name = "checkBoxPlot2ch";
            this.checkBoxPlot2ch.Size = new System.Drawing.Size(77, 46);
            this.checkBoxPlot2ch.TabIndex = 36;
            this.checkBoxPlot2ch.Text = "2nd Ch";
            this.checkBoxPlot2ch.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.checkBoxPlot2ch.UseVisualStyleBackColor = true;
            // 
            // checkBoxAnonymize
            // 
            this.checkBoxAnonymize.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.checkBoxAnonymize.Dock = System.Windows.Forms.DockStyle.Right;
            this.checkBoxAnonymize.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxAnonymize.ForeColor = System.Drawing.Color.White;
            this.checkBoxAnonymize.Location = new System.Drawing.Point(956, 0);
            this.checkBoxAnonymize.Name = "checkBoxAnonymize";
            this.checkBoxAnonymize.Size = new System.Drawing.Size(100, 46);
            this.checkBoxAnonymize.TabIndex = 35;
            this.checkBoxAnonymize.Text = "Anonymize";
            this.checkBoxAnonymize.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.checkBoxAnonymize.UseVisualStyleBackColor = true;
            this.checkBoxAnonymize.CheckedChanged += new System.EventHandler(this.checkBoxAnonymize_CheckedChanged);
            // 
            // checkBoxReducePdf
            // 
            this.checkBoxReducePdf.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.checkBoxReducePdf.Dock = System.Windows.Forms.DockStyle.Right;
            this.checkBoxReducePdf.ForeColor = System.Drawing.Color.White;
            this.checkBoxReducePdf.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.checkBoxReducePdf.Location = new System.Drawing.Point(1056, 0);
            this.checkBoxReducePdf.Name = "checkBoxReducePdf";
            this.checkBoxReducePdf.Size = new System.Drawing.Size(66, 46);
            this.checkBoxReducePdf.TabIndex = 34;
            this.checkBoxReducePdf.Text = "< PDF";
            this.checkBoxReducePdf.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.checkBoxReducePdf.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.AutoSize = true;
            this.button2.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button2.Dock = System.Windows.Forms.DockStyle.Right;
            this.button2.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(1122, 0);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(88, 46);
            this.button2.TabIndex = 33;
            this.button2.Text = "Save \r\nReport";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.buttonSaveReport_Click);
            // 
            // buttonPrintPreviousFindings
            // 
            this.buttonPrintPreviousFindings.AutoSize = true;
            this.buttonPrintPreviousFindings.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonPrintPreviousFindings.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonPrintPreviousFindings.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonPrintPreviousFindings.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonPrintPreviousFindings.FlatAppearance.BorderSize = 0;
            this.buttonPrintPreviousFindings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPrintPreviousFindings.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonPrintPreviousFindings.ForeColor = System.Drawing.Color.White;
            this.buttonPrintPreviousFindings.Location = new System.Drawing.Point(1210, 0);
            this.buttonPrintPreviousFindings.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonPrintPreviousFindings.Name = "buttonPrintPreviousFindings";
            this.buttonPrintPreviousFindings.Size = new System.Drawing.Size(154, 46);
            this.buttonPrintPreviousFindings.TabIndex = 32;
            this.buttonPrintPreviousFindings.Text = "Save && Print\r\nReport";
            this.buttonPrintPreviousFindings.UseVisualStyleBackColor = false;
            this.buttonPrintPreviousFindings.Click += new System.EventHandler(this.buttonPrintPreviousFindings_Click);
            // 
            // labelPdfNr
            // 
            this.labelPdfNr.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPdfNr.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelPdfNr.ForeColor = System.Drawing.Color.White;
            this.labelPdfNr.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelPdfNr.Location = new System.Drawing.Point(600, 0);
            this.labelPdfNr.Name = "labelPdfNr";
            this.labelPdfNr.Size = new System.Drawing.Size(93, 46);
            this.labelPdfNr.TabIndex = 25;
            this.labelPdfNr.Text = "PDF #";
            this.labelPdfNr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelReportResult
            // 
            this.labelReportResult.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelReportResult.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelReportResult.ForeColor = System.Drawing.Color.White;
            this.labelReportResult.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelReportResult.Location = new System.Drawing.Point(314, 0);
            this.labelReportResult.Name = "labelReportResult";
            this.labelReportResult.Size = new System.Drawing.Size(286, 46);
            this.labelReportResult.TabIndex = 22;
            this.labelReportResult.Text = "Report: #";
            this.labelReportResult.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // buttonViewPdf
            // 
            this.buttonViewPdf.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonViewPdf.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonViewPdf.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonViewPdf.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonViewPdf.FlatAppearance.BorderSize = 0;
            this.buttonViewPdf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonViewPdf.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonViewPdf.ForeColor = System.Drawing.Color.White;
            this.buttonViewPdf.Location = new System.Drawing.Point(212, 0);
            this.buttonViewPdf.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonViewPdf.Name = "buttonViewPdf";
            this.buttonViewPdf.Size = new System.Drawing.Size(102, 46);
            this.buttonViewPdf.TabIndex = 24;
            this.buttonViewPdf.Text = "Report List";
            this.buttonViewPdf.UseVisualStyleBackColor = false;
            this.buttonViewPdf.Click += new System.EventHandler(this.buttonViewPdf_Click);
            // 
            // buttonMCT
            // 
            this.buttonMCT.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonMCT.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonMCT.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonMCT.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonMCT.FlatAppearance.BorderSize = 0;
            this.buttonMCT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMCT.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonMCT.ForeColor = System.Drawing.Color.White;
            this.buttonMCT.Location = new System.Drawing.Point(102, 0);
            this.buttonMCT.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonMCT.Name = "buttonMCT";
            this.buttonMCT.Size = new System.Drawing.Size(110, 46);
            this.buttonMCT.TabIndex = 11;
            this.buttonMCT.Text = "MCT Data";
            this.buttonMCT.UseVisualStyleBackColor = false;
            this.buttonMCT.Click += new System.EventHandler(this.buttonMCT_Click);
            // 
            // buttonEndStudy
            // 
            this.buttonEndStudy.AutoSize = true;
            this.buttonEndStudy.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonEndStudy.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonEndStudy.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonEndStudy.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonEndStudy.FlatAppearance.BorderSize = 0;
            this.buttonEndStudy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEndStudy.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonEndStudy.ForeColor = System.Drawing.Color.White;
            this.buttonEndStudy.Location = new System.Drawing.Point(0, 0);
            this.buttonEndStudy.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonEndStudy.Name = "buttonEndStudy";
            this.buttonEndStudy.Size = new System.Drawing.Size(102, 46);
            this.buttonEndStudy.TabIndex = 37;
            this.buttonEndStudy.Text = "Stop study";
            this.buttonEndStudy.UseVisualStyleBackColor = false;
            this.buttonEndStudy.Click += new System.EventHandler(this.buttonEndStudy_Click);
            // 
            // panelSelect
            // 
            this.panelSelect.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelSelect.Controls.Add(this.checkBoxModifyAbN);
            this.panelSelect.Controls.Add(this.checkBoxModifyManual);
            this.panelSelect.Controls.Add(this.checkBoxModifyQC);
            this.panelSelect.Controls.Add(this.checkBoxModifyNormal);
            this.panelSelect.Controls.Add(this.checkBoxModifyHR1);
            this.panelSelect.Controls.Add(this.checkBoxModifyHR0);
            this.panelSelect.Controls.Add(this.panel41);
            this.panelSelect.Controls.Add(this.panel1);
            this.panelSelect.Controls.Add(this.buttonSetSelection);
            this.panelSelect.Controls.Add(this.panel11);
            this.panelSelect.Controls.Add(this.checkBoxModifyOther);
            this.panelSelect.Controls.Add(this.checkBoxModifyBL);
            this.panelSelect.Controls.Add(this.checkBoxModifyVT);
            this.panelSelect.Controls.Add(this.checkBoxModifyPace);
            this.panelSelect.Controls.Add(this.checkBoxModifyPVC);
            this.panelSelect.Controls.Add(this.checkBoxModifyPAC);
            this.panelSelect.Controls.Add(this.checkBoxModifyAfl);
            this.panelSelect.Controls.Add(this.checkBoxModifyAF);
            this.panelSelect.Controls.Add(this.checkBoxModifyPause);
            this.panelSelect.Controls.Add(this.checkBoxModifyBrady);
            this.panelSelect.Controls.Add(this.checkBoxModifyTachy);
            this.panelSelect.Controls.Add(this.checkBoxModifyAll);
            this.panelSelect.Controls.Add(this.label18);
            this.panelSelect.Controls.Add(this.checkBoxStrip);
            this.panelSelect.Controls.Add(this.checkBoxTable);
            this.panelSelect.Controls.Add(this.label24);
            this.panelSelect.Controls.Add(this.buttonUncheckAll);
            this.panelSelect.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelSelect.Location = new System.Drawing.Point(0, 0);
            this.panelSelect.Name = "panelSelect";
            this.panelSelect.Size = new System.Drawing.Size(1364, 30);
            this.panelSelect.TabIndex = 0;
            // 
            // checkBoxModifyAbN
            // 
            this.checkBoxModifyAbN.AutoSize = true;
            this.checkBoxModifyAbN.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxModifyAbN.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxModifyAbN.Location = new System.Drawing.Point(965, 0);
            this.checkBoxModifyAbN.Name = "checkBoxModifyAbN";
            this.checkBoxModifyAbN.Size = new System.Drawing.Size(77, 28);
            this.checkBoxModifyAbN.TabIndex = 61;
            this.checkBoxModifyAbN.Text = "AbNorm";
            this.checkBoxModifyAbN.UseVisualStyleBackColor = true;
            // 
            // checkBoxModifyManual
            // 
            this.checkBoxModifyManual.AutoSize = true;
            this.checkBoxModifyManual.Dock = System.Windows.Forms.DockStyle.Right;
            this.checkBoxModifyManual.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxModifyManual.Location = new System.Drawing.Point(1044, 0);
            this.checkBoxModifyManual.Name = "checkBoxModifyManual";
            this.checkBoxModifyManual.Size = new System.Drawing.Size(54, 28);
            this.checkBoxModifyManual.TabIndex = 60;
            this.checkBoxModifyManual.Text = "Man";
            this.checkBoxModifyManual.UseVisualStyleBackColor = true;
            this.checkBoxModifyManual.CheckedChanged += new System.EventHandler(this.checkBoxModifyManual_CheckedChanged);
            // 
            // checkBoxModifyQC
            // 
            this.checkBoxModifyQC.AutoSize = true;
            this.checkBoxModifyQC.Dock = System.Windows.Forms.DockStyle.Right;
            this.checkBoxModifyQC.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxModifyQC.Location = new System.Drawing.Point(1098, 0);
            this.checkBoxModifyQC.Name = "checkBoxModifyQC";
            this.checkBoxModifyQC.Size = new System.Drawing.Size(48, 28);
            this.checkBoxModifyQC.TabIndex = 59;
            this.checkBoxModifyQC.Text = "QC";
            this.checkBoxModifyQC.UseVisualStyleBackColor = true;
            this.checkBoxModifyQC.CheckedChanged += new System.EventHandler(this.checkBoxModifyQC_CheckedChanged);
            // 
            // checkBoxModifyNormal
            // 
            this.checkBoxModifyNormal.AutoSize = true;
            this.checkBoxModifyNormal.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxModifyNormal.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxModifyNormal.Location = new System.Drawing.Point(894, 0);
            this.checkBoxModifyNormal.Name = "checkBoxModifyNormal";
            this.checkBoxModifyNormal.Size = new System.Drawing.Size(71, 28);
            this.checkBoxModifyNormal.TabIndex = 58;
            this.checkBoxModifyNormal.Text = "Normal";
            this.checkBoxModifyNormal.UseVisualStyleBackColor = true;
            // 
            // checkBoxModifyHR1
            // 
            this.checkBoxModifyHR1.AutoSize = true;
            this.checkBoxModifyHR1.Dock = System.Windows.Forms.DockStyle.Right;
            this.checkBoxModifyHR1.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxModifyHR1.Location = new System.Drawing.Point(1146, 0);
            this.checkBoxModifyHR1.Name = "checkBoxModifyHR1";
            this.checkBoxModifyHR1.Size = new System.Drawing.Size(62, 28);
            this.checkBoxModifyHR1.TabIndex = 35;
            this.checkBoxModifyHR1.Text = "HR>0";
            this.checkBoxModifyHR1.UseVisualStyleBackColor = true;
            this.checkBoxModifyHR1.CheckedChanged += new System.EventHandler(this.checkBoxModifyHR1_CheckedChanged_1);
            // 
            // checkBoxModifyHR0
            // 
            this.checkBoxModifyHR0.AutoSize = true;
            this.checkBoxModifyHR0.Dock = System.Windows.Forms.DockStyle.Right;
            this.checkBoxModifyHR0.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxModifyHR0.Location = new System.Drawing.Point(1208, 0);
            this.checkBoxModifyHR0.Name = "checkBoxModifyHR0";
            this.checkBoxModifyHR0.Size = new System.Drawing.Size(62, 28);
            this.checkBoxModifyHR0.TabIndex = 33;
            this.checkBoxModifyHR0.Text = "HR=0";
            this.checkBoxModifyHR0.UseVisualStyleBackColor = true;
            this.checkBoxModifyHR0.Visible = false;
            this.checkBoxModifyHR0.CheckedChanged += new System.EventHandler(this.checkBoxModifyHR0_CheckedChanged);
            // 
            // panel41
            // 
            this.panel41.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel41.Location = new System.Drawing.Point(1270, 0);
            this.panel41.Name = "panel41";
            this.panel41.Size = new System.Drawing.Size(4, 28);
            this.panel41.TabIndex = 57;
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(1274, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(4, 28);
            this.panel1.TabIndex = 55;
            // 
            // buttonSetSelection
            // 
            this.buttonSetSelection.BackColor = System.Drawing.Color.Transparent;
            this.buttonSetSelection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonSetSelection.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonSetSelection.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonSetSelection.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSetSelection.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.buttonSetSelection.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonSetSelection.Location = new System.Drawing.Point(1278, 0);
            this.buttonSetSelection.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonSetSelection.Name = "buttonSetSelection";
            this.buttonSetSelection.Size = new System.Drawing.Size(80, 28);
            this.buttonSetSelection.TabIndex = 52;
            this.buttonSetSelection.Text = "Set table";
            this.buttonSetSelection.UseVisualStyleBackColor = false;
            this.buttonSetSelection.Click += new System.EventHandler(this.buttonSetSelection_Click_1);
            // 
            // panel11
            // 
            this.panel11.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel11.Location = new System.Drawing.Point(1358, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(4, 28);
            this.panel11.TabIndex = 56;
            // 
            // checkBoxModifyOther
            // 
            this.checkBoxModifyOther.AutoSize = true;
            this.checkBoxModifyOther.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxModifyOther.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxModifyOther.Location = new System.Drawing.Point(831, 0);
            this.checkBoxModifyOther.Name = "checkBoxModifyOther";
            this.checkBoxModifyOther.Size = new System.Drawing.Size(63, 28);
            this.checkBoxModifyOther.TabIndex = 22;
            this.checkBoxModifyOther.Text = "Other";
            this.checkBoxModifyOther.UseVisualStyleBackColor = true;
            this.checkBoxModifyOther.CheckedChanged += new System.EventHandler(this.checkBoxModifyOther_CheckedChanged);
            // 
            // checkBoxModifyBL
            // 
            this.checkBoxModifyBL.AutoSize = true;
            this.checkBoxModifyBL.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxModifyBL.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxModifyBL.Location = new System.Drawing.Point(787, 0);
            this.checkBoxModifyBL.Name = "checkBoxModifyBL";
            this.checkBoxModifyBL.Size = new System.Drawing.Size(44, 28);
            this.checkBoxModifyBL.TabIndex = 29;
            this.checkBoxModifyBL.Text = "BL";
            this.checkBoxModifyBL.UseVisualStyleBackColor = true;
            this.checkBoxModifyBL.CheckedChanged += new System.EventHandler(this.checkBoxModifyBL_CheckedChanged);
            // 
            // checkBoxModifyVT
            // 
            this.checkBoxModifyVT.AutoSize = true;
            this.checkBoxModifyVT.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxModifyVT.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxModifyVT.Location = new System.Drawing.Point(742, 0);
            this.checkBoxModifyVT.Name = "checkBoxModifyVT";
            this.checkBoxModifyVT.Size = new System.Drawing.Size(45, 28);
            this.checkBoxModifyVT.TabIndex = 48;
            this.checkBoxModifyVT.Text = "VT";
            this.checkBoxModifyVT.UseVisualStyleBackColor = true;
            this.checkBoxModifyVT.CheckedChanged += new System.EventHandler(this.checkBoxModifyVT_CheckedChanged);
            // 
            // checkBoxModifyPace
            // 
            this.checkBoxModifyPace.AutoSize = true;
            this.checkBoxModifyPace.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxModifyPace.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxModifyPace.Location = new System.Drawing.Point(683, 0);
            this.checkBoxModifyPace.Name = "checkBoxModifyPace";
            this.checkBoxModifyPace.Size = new System.Drawing.Size(59, 28);
            this.checkBoxModifyPace.TabIndex = 50;
            this.checkBoxModifyPace.Text = "Pace";
            this.checkBoxModifyPace.UseVisualStyleBackColor = true;
            this.checkBoxModifyPace.CheckedChanged += new System.EventHandler(this.checkBoxModifyPace_CheckedChanged);
            // 
            // checkBoxModifyPVC
            // 
            this.checkBoxModifyPVC.AutoSize = true;
            this.checkBoxModifyPVC.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxModifyPVC.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxModifyPVC.Location = new System.Drawing.Point(628, 0);
            this.checkBoxModifyPVC.Name = "checkBoxModifyPVC";
            this.checkBoxModifyPVC.Size = new System.Drawing.Size(55, 28);
            this.checkBoxModifyPVC.TabIndex = 47;
            this.checkBoxModifyPVC.Text = "PVC";
            this.checkBoxModifyPVC.UseVisualStyleBackColor = true;
            this.checkBoxModifyPVC.CheckedChanged += new System.EventHandler(this.checkBoxModifyPVC_CheckedChanged);
            // 
            // checkBoxModifyPAC
            // 
            this.checkBoxModifyPAC.AutoSize = true;
            this.checkBoxModifyPAC.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxModifyPAC.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxModifyPAC.Location = new System.Drawing.Point(574, 0);
            this.checkBoxModifyPAC.Name = "checkBoxModifyPAC";
            this.checkBoxModifyPAC.Size = new System.Drawing.Size(54, 28);
            this.checkBoxModifyPAC.TabIndex = 46;
            this.checkBoxModifyPAC.Text = "PAC";
            this.checkBoxModifyPAC.UseVisualStyleBackColor = true;
            this.checkBoxModifyPAC.CheckedChanged += new System.EventHandler(this.checkBoxModifyPAC_CheckedChanged);
            // 
            // checkBoxModifyAfl
            // 
            this.checkBoxModifyAfl.AutoSize = true;
            this.checkBoxModifyAfl.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxModifyAfl.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxModifyAfl.Location = new System.Drawing.Point(531, 0);
            this.checkBoxModifyAfl.Name = "checkBoxModifyAfl";
            this.checkBoxModifyAfl.Size = new System.Drawing.Size(43, 28);
            this.checkBoxModifyAfl.TabIndex = 51;
            this.checkBoxModifyAfl.Text = "Afl";
            this.checkBoxModifyAfl.UseVisualStyleBackColor = true;
            this.checkBoxModifyAfl.CheckedChanged += new System.EventHandler(this.checkBoxModifyAfl_CheckedChanged);
            // 
            // checkBoxModifyAF
            // 
            this.checkBoxModifyAF.AutoSize = true;
            this.checkBoxModifyAF.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxModifyAF.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxModifyAF.Location = new System.Drawing.Point(486, 0);
            this.checkBoxModifyAF.Name = "checkBoxModifyAF";
            this.checkBoxModifyAF.Size = new System.Drawing.Size(45, 28);
            this.checkBoxModifyAF.TabIndex = 20;
            this.checkBoxModifyAF.Text = "AF";
            this.checkBoxModifyAF.UseVisualStyleBackColor = true;
            this.checkBoxModifyAF.CheckedChanged += new System.EventHandler(this.checkBoxModifyAF_CheckedChanged);
            // 
            // checkBoxModifyPause
            // 
            this.checkBoxModifyPause.AutoSize = true;
            this.checkBoxModifyPause.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxModifyPause.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxModifyPause.Location = new System.Drawing.Point(442, 0);
            this.checkBoxModifyPause.Name = "checkBoxModifyPause";
            this.checkBoxModifyPause.Size = new System.Drawing.Size(44, 28);
            this.checkBoxModifyPause.TabIndex = 18;
            this.checkBoxModifyPause.Text = "Pa";
            this.checkBoxModifyPause.UseVisualStyleBackColor = true;
            this.checkBoxModifyPause.CheckedChanged += new System.EventHandler(this.checkBoxModifyPause_CheckedChanged);
            // 
            // checkBoxModifyBrady
            // 
            this.checkBoxModifyBrady.AutoSize = true;
            this.checkBoxModifyBrady.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxModifyBrady.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxModifyBrady.Location = new System.Drawing.Point(399, 0);
            this.checkBoxModifyBrady.Name = "checkBoxModifyBrady";
            this.checkBoxModifyBrady.Size = new System.Drawing.Size(43, 28);
            this.checkBoxModifyBrady.TabIndex = 16;
            this.checkBoxModifyBrady.Text = "By";
            this.checkBoxModifyBrady.UseVisualStyleBackColor = true;
            this.checkBoxModifyBrady.CheckedChanged += new System.EventHandler(this.checkBoxModifyBrady_CheckedChanged);
            // 
            // checkBoxModifyTachy
            // 
            this.checkBoxModifyTachy.AutoSize = true;
            this.checkBoxModifyTachy.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxModifyTachy.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxModifyTachy.Location = new System.Drawing.Point(357, 0);
            this.checkBoxModifyTachy.Name = "checkBoxModifyTachy";
            this.checkBoxModifyTachy.Size = new System.Drawing.Size(42, 28);
            this.checkBoxModifyTachy.TabIndex = 14;
            this.checkBoxModifyTachy.Text = "Ty";
            this.checkBoxModifyTachy.UseVisualStyleBackColor = true;
            this.checkBoxModifyTachy.CheckedChanged += new System.EventHandler(this.checkBoxModifyTachy_CheckedChanged);
            // 
            // checkBoxModifyAll
            // 
            this.checkBoxModifyAll.AutoSize = true;
            this.checkBoxModifyAll.Checked = true;
            this.checkBoxModifyAll.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxModifyAll.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxModifyAll.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.checkBoxModifyAll.Location = new System.Drawing.Point(313, 0);
            this.checkBoxModifyAll.Name = "checkBoxModifyAll";
            this.checkBoxModifyAll.Size = new System.Drawing.Size(44, 28);
            this.checkBoxModifyAll.TabIndex = 27;
            this.checkBoxModifyAll.Text = "All";
            this.checkBoxModifyAll.UseVisualStyleBackColor = true;
            this.checkBoxModifyAll.CheckedChanged += new System.EventHandler(this.checkBoxModifyAll_CheckedChanged);
            // 
            // label18
            // 
            this.label18.Dock = System.Windows.Forms.DockStyle.Left;
            this.label18.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(239, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(74, 28);
            this.label18.TabIndex = 36;
            this.label18.Text = "Selected:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // checkBoxStrip
            // 
            this.checkBoxStrip.AutoSize = true;
            this.checkBoxStrip.Checked = true;
            this.checkBoxStrip.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxStrip.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxStrip.Location = new System.Drawing.Point(183, 0);
            this.checkBoxStrip.Name = "checkBoxStrip";
            this.checkBoxStrip.Size = new System.Drawing.Size(56, 28);
            this.checkBoxStrip.TabIndex = 39;
            this.checkBoxStrip.Text = "Strip";
            this.checkBoxStrip.UseVisualStyleBackColor = true;
            // 
            // checkBoxTable
            // 
            this.checkBoxTable.AutoSize = true;
            this.checkBoxTable.Checked = true;
            this.checkBoxTable.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxTable.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxTable.Location = new System.Drawing.Point(122, 0);
            this.checkBoxTable.Name = "checkBoxTable";
            this.checkBoxTable.Size = new System.Drawing.Size(61, 28);
            this.checkBoxTable.TabIndex = 37;
            this.checkBoxTable.Text = "Table";
            this.checkBoxTable.UseVisualStyleBackColor = true;
            // 
            // label24
            // 
            this.label24.Dock = System.Windows.Forms.DockStyle.Left;
            this.label24.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label24.Location = new System.Drawing.Point(83, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(39, 28);
            this.label24.TabIndex = 25;
            this.label24.Text = "Add:";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // buttonUncheckAll
            // 
            this.buttonUncheckAll.BackColor = System.Drawing.Color.Transparent;
            this.buttonUncheckAll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonUncheckAll.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonUncheckAll.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonUncheckAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonUncheckAll.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.buttonUncheckAll.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonUncheckAll.Location = new System.Drawing.Point(0, 0);
            this.buttonUncheckAll.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonUncheckAll.Name = "buttonUncheckAll";
            this.buttonUncheckAll.Size = new System.Drawing.Size(83, 28);
            this.buttonUncheckAll.TabIndex = 53;
            this.buttonUncheckAll.Text = "Unselect";
            this.buttonUncheckAll.UseVisualStyleBackColor = false;
            this.buttonUncheckAll.Click += new System.EventHandler(this.buttonUncheckAll_Click_1);
            // 
            // panelStudyPeriod
            // 
            this.panelStudyPeriod.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelStudyPeriod.Controls.Add(this.panel37);
            this.panelStudyPeriod.Controls.Add(this.panel22);
            this.panelStudyPeriod.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelStudyPeriod.Location = new System.Drawing.Point(0, 40);
            this.panelStudyPeriod.Name = "panelStudyPeriod";
            this.panelStudyPeriod.Size = new System.Drawing.Size(1364, 45);
            this.panelStudyPeriod.TabIndex = 1;
            // 
            // panel37
            // 
            this.panel37.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel37.Location = new System.Drawing.Point(0, 35);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(1364, 10);
            this.panel37.TabIndex = 13;
            // 
            // panel22
            // 
            this.panel22.Controls.Add(this.panel18);
            this.panel22.Controls.Add(this.panel17);
            this.panel22.Controls.Add(this.panel65);
            this.panel22.Controls.Add(this.panel35);
            this.panel22.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel22.Location = new System.Drawing.Point(0, 0);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(1364, 38);
            this.panel22.TabIndex = 9;
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.labelTBPAO);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel18.Location = new System.Drawing.Point(1027, 0);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(337, 38);
            this.panel18.TabIndex = 30;
            // 
            // labelTBPAO
            // 
            this.labelTBPAO.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelTBPAO.Location = new System.Drawing.Point(0, 0);
            this.labelTBPAO.Name = "labelTBPAO";
            this.labelTBPAO.Size = new System.Drawing.Size(337, 38);
            this.labelTBPAO.TabIndex = 41;
            this.labelTBPAO.Text = "^HR: Tachy <= 234 Brady >= 77 55 <= Pause <= 99";
            // 
            // panel17
            // 
            this.panel17.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel17.Location = new System.Drawing.Point(1017, 0);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(10, 38);
            this.panel17.TabIndex = 29;
            // 
            // panel65
            // 
            this.panel65.Controls.Add(this.panel58);
            this.panel65.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel65.Location = new System.Drawing.Point(534, 0);
            this.panel65.Name = "panel65";
            this.panel65.Size = new System.Drawing.Size(483, 38);
            this.panel65.TabIndex = 25;
            this.panel65.Visible = false;
            // 
            // panel58
            // 
            this.panel58.Controls.Add(this.panel64);
            this.panel58.Controls.Add(this.panel62);
            this.panel58.Controls.Add(this.panel57);
            this.panel58.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel58.Enabled = false;
            this.panel58.Location = new System.Drawing.Point(0, 0);
            this.panel58.Name = "panel58";
            this.panel58.Size = new System.Drawing.Size(474, 38);
            this.panel58.TabIndex = 26;
            // 
            // panel64
            // 
            this.panel64.Controls.Add(this.buttonP24);
            this.panel64.Controls.Add(this.buttonP6);
            this.panel64.Controls.Add(this.buttonP1);
            this.panel64.Controls.Add(this.button0h);
            this.panel64.Controls.Add(this.buttonM1);
            this.panel64.Controls.Add(this.buttonM6);
            this.panel64.Controls.Add(this.buttonM24);
            this.panel64.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel64.Location = new System.Drawing.Point(0, 46);
            this.panel64.Name = "panel64";
            this.panel64.Size = new System.Drawing.Size(474, 28);
            this.panel64.TabIndex = 2;
            // 
            // buttonP24
            // 
            this.buttonP24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonP24.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonP24.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonP24.FlatAppearance.BorderSize = 0;
            this.buttonP24.Font = new System.Drawing.Font("Arial", 9F);
            this.buttonP24.Location = new System.Drawing.Point(258, 0);
            this.buttonP24.Name = "buttonP24";
            this.buttonP24.Size = new System.Drawing.Size(53, 28);
            this.buttonP24.TabIndex = 47;
            this.buttonP24.Text = "+24 h";
            this.buttonP24.UseVisualStyleBackColor = false;
            // 
            // buttonP6
            // 
            this.buttonP6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonP6.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonP6.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonP6.FlatAppearance.BorderSize = 0;
            this.buttonP6.Font = new System.Drawing.Font("Arial", 9F);
            this.buttonP6.Location = new System.Drawing.Point(215, 0);
            this.buttonP6.Name = "buttonP6";
            this.buttonP6.Size = new System.Drawing.Size(43, 28);
            this.buttonP6.TabIndex = 46;
            this.buttonP6.Text = "+6 h";
            this.buttonP6.UseVisualStyleBackColor = false;
            // 
            // buttonP1
            // 
            this.buttonP1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonP1.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonP1.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonP1.FlatAppearance.BorderSize = 0;
            this.buttonP1.Font = new System.Drawing.Font("Arial", 9F);
            this.buttonP1.Location = new System.Drawing.Point(172, 0);
            this.buttonP1.Name = "buttonP1";
            this.buttonP1.Size = new System.Drawing.Size(43, 28);
            this.buttonP1.TabIndex = 45;
            this.buttonP1.Text = "+1 h";
            this.buttonP1.UseVisualStyleBackColor = false;
            // 
            // button0h
            // 
            this.button0h.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.button0h.Dock = System.Windows.Forms.DockStyle.Left;
            this.button0h.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.button0h.FlatAppearance.BorderSize = 0;
            this.button0h.Font = new System.Drawing.Font("Arial", 9F);
            this.button0h.Location = new System.Drawing.Point(129, 0);
            this.button0h.Name = "button0h";
            this.button0h.Size = new System.Drawing.Size(43, 28);
            this.button0h.TabIndex = 48;
            this.button0h.Text = "0:00";
            this.button0h.UseVisualStyleBackColor = false;
            // 
            // buttonM1
            // 
            this.buttonM1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonM1.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonM1.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonM1.FlatAppearance.BorderSize = 0;
            this.buttonM1.Font = new System.Drawing.Font("Arial", 9F);
            this.buttonM1.Location = new System.Drawing.Point(86, 0);
            this.buttonM1.Name = "buttonM1";
            this.buttonM1.Size = new System.Drawing.Size(43, 28);
            this.buttonM1.TabIndex = 44;
            this.buttonM1.Text = "-1 h";
            this.buttonM1.UseVisualStyleBackColor = false;
            // 
            // buttonM6
            // 
            this.buttonM6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonM6.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonM6.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonM6.FlatAppearance.BorderSize = 0;
            this.buttonM6.Font = new System.Drawing.Font("Arial", 9F);
            this.buttonM6.Location = new System.Drawing.Point(43, 0);
            this.buttonM6.Name = "buttonM6";
            this.buttonM6.Size = new System.Drawing.Size(43, 28);
            this.buttonM6.TabIndex = 43;
            this.buttonM6.Text = "-6 h";
            this.buttonM6.UseVisualStyleBackColor = false;
            // 
            // buttonM24
            // 
            this.buttonM24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.buttonM24.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonM24.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonM24.FlatAppearance.BorderSize = 0;
            this.buttonM24.Font = new System.Drawing.Font("Arial", 9F);
            this.buttonM24.Location = new System.Drawing.Point(0, 0);
            this.buttonM24.Name = "buttonM24";
            this.buttonM24.Size = new System.Drawing.Size(43, 28);
            this.buttonM24.TabIndex = 42;
            this.buttonM24.Text = "-24 h";
            this.buttonM24.UseVisualStyleBackColor = false;
            // 
            // panel62
            // 
            this.panel62.Controls.Add(this.comboBox1);
            this.panel62.Controls.Add(this.textBox1);
            this.panel62.Controls.Add(this.textBoxToHour);
            this.panel62.Controls.Add(this.dateTimePickerTo);
            this.panel62.Controls.Add(this.labelTo);
            this.panel62.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel62.Location = new System.Drawing.Point(0, 26);
            this.panel62.Name = "panel62";
            this.panel62.Size = new System.Drawing.Size(474, 20);
            this.panel62.TabIndex = 1;
            // 
            // comboBox1
            // 
            this.comboBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.comboBox1.Font = new System.Drawing.Font("Arial", 9F);
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "AM",
            "PM"});
            this.comboBox1.Location = new System.Drawing.Point(237, 0);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(45, 23);
            this.comboBox1.TabIndex = 35;
            // 
            // textBox1
            // 
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.textBox1.Font = new System.Drawing.Font("Arial", 9F);
            this.textBox1.Location = new System.Drawing.Point(201, 0);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(36, 21);
            this.textBox1.TabIndex = 35;
            this.textBox1.Text = "30";
            // 
            // textBoxToHour
            // 
            this.textBoxToHour.Dock = System.Windows.Forms.DockStyle.Left;
            this.textBoxToHour.Font = new System.Drawing.Font("Arial", 9F);
            this.textBoxToHour.Location = new System.Drawing.Point(169, 0);
            this.textBoxToHour.Name = "textBoxToHour";
            this.textBoxToHour.Size = new System.Drawing.Size(32, 21);
            this.textBoxToHour.TabIndex = 30;
            this.textBoxToHour.Text = "11";
            // 
            // dateTimePickerTo
            // 
            this.dateTimePickerTo.Dock = System.Windows.Forms.DockStyle.Left;
            this.dateTimePickerTo.Font = new System.Drawing.Font("Arial", 9F);
            this.dateTimePickerTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerTo.Location = new System.Drawing.Point(70, 0);
            this.dateTimePickerTo.Name = "dateTimePickerTo";
            this.dateTimePickerTo.Size = new System.Drawing.Size(99, 21);
            this.dateTimePickerTo.TabIndex = 27;
            // 
            // labelTo
            // 
            this.labelTo.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelTo.Font = new System.Drawing.Font("Arial", 10F);
            this.labelTo.Location = new System.Drawing.Point(0, 0);
            this.labelTo.Name = "labelTo";
            this.labelTo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labelTo.Size = new System.Drawing.Size(70, 20);
            this.labelTo.TabIndex = 13;
            this.labelTo.Text = "To:";
            // 
            // panel57
            // 
            this.panel57.Controls.Add(this.buttonSetTimeFrame);
            this.panel57.Controls.Add(this.comboBox2);
            this.panel57.Controls.Add(this.textBox2);
            this.panel57.Controls.Add(this.textBoxFromHour);
            this.panel57.Controls.Add(this.dateTimePickerFrom);
            this.panel57.Controls.Add(this.labelFrom);
            this.panel57.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel57.Location = new System.Drawing.Point(0, 0);
            this.panel57.Name = "panel57";
            this.panel57.Size = new System.Drawing.Size(474, 26);
            this.panel57.TabIndex = 0;
            // 
            // buttonSetTimeFrame
            // 
            this.buttonSetTimeFrame.BackColor = System.Drawing.Color.Transparent;
            this.buttonSetTimeFrame.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonSetTimeFrame.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonSetTimeFrame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSetTimeFrame.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.buttonSetTimeFrame.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonSetTimeFrame.Location = new System.Drawing.Point(314, 0);
            this.buttonSetTimeFrame.Name = "buttonSetTimeFrame";
            this.buttonSetTimeFrame.Size = new System.Drawing.Size(160, 26);
            this.buttonSetTimeFrame.TabIndex = 36;
            this.buttonSetTimeFrame.Text = "Set data time frame";
            this.buttonSetTimeFrame.UseVisualStyleBackColor = false;
            // 
            // comboBox2
            // 
            this.comboBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.comboBox2.Font = new System.Drawing.Font("Arial", 9F);
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "AM",
            "PM"});
            this.comboBox2.Location = new System.Drawing.Point(237, 0);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(45, 23);
            this.comboBox2.TabIndex = 34;
            // 
            // textBox2
            // 
            this.textBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.textBox2.Font = new System.Drawing.Font("Arial", 9F);
            this.textBox2.Location = new System.Drawing.Point(201, 0);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(36, 21);
            this.textBox2.TabIndex = 34;
            this.textBox2.Text = "30";
            // 
            // textBoxFromHour
            // 
            this.textBoxFromHour.Dock = System.Windows.Forms.DockStyle.Left;
            this.textBoxFromHour.Font = new System.Drawing.Font("Arial", 9F);
            this.textBoxFromHour.Location = new System.Drawing.Point(169, 0);
            this.textBoxFromHour.Name = "textBoxFromHour";
            this.textBoxFromHour.Size = new System.Drawing.Size(32, 21);
            this.textBoxFromHour.TabIndex = 29;
            this.textBoxFromHour.Text = "11";
            // 
            // dateTimePickerFrom
            // 
            this.dateTimePickerFrom.Dock = System.Windows.Forms.DockStyle.Left;
            this.dateTimePickerFrom.Font = new System.Drawing.Font("Arial", 9F);
            this.dateTimePickerFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerFrom.Location = new System.Drawing.Point(70, 0);
            this.dateTimePickerFrom.Name = "dateTimePickerFrom";
            this.dateTimePickerFrom.Size = new System.Drawing.Size(99, 21);
            this.dateTimePickerFrom.TabIndex = 26;
            // 
            // labelFrom
            // 
            this.labelFrom.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelFrom.Font = new System.Drawing.Font("Arial", 10F);
            this.labelFrom.Location = new System.Drawing.Point(0, 0);
            this.labelFrom.Name = "labelFrom";
            this.labelFrom.Size = new System.Drawing.Size(70, 26);
            this.labelFrom.TabIndex = 12;
            this.labelFrom.Text = "From:";
            // 
            // panel35
            // 
            this.panel35.Controls.Add(this.panel54);
            this.panel35.Controls.Add(this.label36);
            this.panel35.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel35.Location = new System.Drawing.Point(0, 0);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(534, 38);
            this.panel35.TabIndex = 22;
            // 
            // panel54
            // 
            this.panel54.Controls.Add(this.radioButtonHour);
            this.panel54.Controls.Add(this.radioButton1);
            this.panel54.Controls.Add(this.radioButton2);
            this.panel54.Controls.Add(this.radioButtonMonth);
            this.panel54.Controls.Add(this.radioButtonPeriod);
            this.panel54.Controls.Add(this.radioButtonEOS);
            this.panel54.Controls.Add(this.radioButtonWeek);
            this.panel54.Controls.Add(this.radioButtonDay);
            this.panel54.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel54.Location = new System.Drawing.Point(0, 16);
            this.panel54.Name = "panel54";
            this.panel54.Size = new System.Drawing.Size(534, 22);
            this.panel54.TabIndex = 37;
            // 
            // radioButtonHour
            // 
            this.radioButtonHour.AutoSize = true;
            this.radioButtonHour.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioButtonHour.Font = new System.Drawing.Font("Arial", 10F);
            this.radioButtonHour.Location = new System.Drawing.Point(453, 0);
            this.radioButtonHour.Name = "radioButtonHour";
            this.radioButtonHour.Size = new System.Drawing.Size(56, 22);
            this.radioButtonHour.TabIndex = 42;
            this.radioButtonHour.Text = "Hour";
            this.radioButtonHour.UseVisualStyleBackColor = true;
            this.radioButtonHour.Visible = false;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioButton1.Font = new System.Drawing.Font("Arial", 10F);
            this.radioButton1.Location = new System.Drawing.Point(387, 0);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(66, 22);
            this.radioButton1.TabIndex = 46;
            this.radioButton1.Text = "12 hrs";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.Visible = false;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioButton2.Font = new System.Drawing.Font("Arial", 10F);
            this.radioButton2.Location = new System.Drawing.Point(329, 0);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(58, 22);
            this.radioButton2.TabIndex = 44;
            this.radioButton2.Text = "6 hrs";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.Visible = false;
            // 
            // radioButtonMonth
            // 
            this.radioButtonMonth.AutoSize = true;
            this.radioButtonMonth.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioButtonMonth.Font = new System.Drawing.Font("Arial", 10F);
            this.radioButtonMonth.Location = new System.Drawing.Point(264, 0);
            this.radioButtonMonth.Name = "radioButtonMonth";
            this.radioButtonMonth.Size = new System.Drawing.Size(65, 22);
            this.radioButtonMonth.TabIndex = 45;
            this.radioButtonMonth.Text = "Month";
            this.radioButtonMonth.UseVisualStyleBackColor = true;
            this.radioButtonMonth.Visible = false;
            // 
            // radioButtonPeriod
            // 
            this.radioButtonPeriod.AutoSize = true;
            this.radioButtonPeriod.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioButtonPeriod.Enabled = false;
            this.radioButtonPeriod.Font = new System.Drawing.Font("Arial", 10F);
            this.radioButtonPeriod.Location = new System.Drawing.Point(180, 0);
            this.radioButtonPeriod.Name = "radioButtonPeriod";
            this.radioButtonPeriod.Size = new System.Drawing.Size(84, 22);
            this.radioButtonPeriod.TabIndex = 48;
            this.radioButtonPeriod.Text = "Selection";
            this.radioButtonPeriod.UseVisualStyleBackColor = true;
            this.radioButtonPeriod.Visible = false;
            // 
            // radioButtonEOS
            // 
            this.radioButtonEOS.AutoSize = true;
            this.radioButtonEOS.Checked = true;
            this.radioButtonEOS.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioButtonEOS.Font = new System.Drawing.Font("Arial", 10F);
            this.radioButtonEOS.Location = new System.Drawing.Point(113, 0);
            this.radioButtonEOS.Name = "radioButtonEOS";
            this.radioButtonEOS.Size = new System.Drawing.Size(67, 22);
            this.radioButtonEOS.TabIndex = 49;
            this.radioButtonEOS.TabStop = true;
            this.radioButtonEOS.Text = "E.O.S.";
            this.radioButtonEOS.UseVisualStyleBackColor = true;
            // 
            // radioButtonWeek
            // 
            this.radioButtonWeek.AutoSize = true;
            this.radioButtonWeek.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioButtonWeek.Font = new System.Drawing.Font("Arial", 10F);
            this.radioButtonWeek.Location = new System.Drawing.Point(51, 0);
            this.radioButtonWeek.Name = "radioButtonWeek";
            this.radioButtonWeek.Size = new System.Drawing.Size(62, 22);
            this.radioButtonWeek.TabIndex = 43;
            this.radioButtonWeek.Text = "Week";
            this.radioButtonWeek.UseVisualStyleBackColor = true;
            // 
            // radioButtonDay
            // 
            this.radioButtonDay.AutoSize = true;
            this.radioButtonDay.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioButtonDay.Font = new System.Drawing.Font("Arial", 10F);
            this.radioButtonDay.Location = new System.Drawing.Point(0, 0);
            this.radioButtonDay.Name = "radioButtonDay";
            this.radioButtonDay.Size = new System.Drawing.Size(51, 22);
            this.radioButtonDay.TabIndex = 47;
            this.radioButtonDay.Text = "Day";
            this.radioButtonDay.UseVisualStyleBackColor = true;
            // 
            // label36
            // 
            this.label36.Dock = System.Windows.Forms.DockStyle.Top;
            this.label36.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label36.Location = new System.Drawing.Point(0, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(534, 20);
            this.label36.TabIndex = 36;
            this.label36.Text = "Period";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panelStudy
            // 
            this.panelStudy.AutoSize = true;
            this.panelStudy.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelStudy.Controls.Add(this.panelSelect);
            this.panelStudy.Controls.Add(this.panel16);
            this.panelStudy.Controls.Add(this.panelStudyPeriod);
            this.panelStudy.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelStudy.Location = new System.Drawing.Point(0, 540);
            this.panelStudy.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelStudy.Name = "panelStudy";
            this.panelStudy.Size = new System.Drawing.Size(1364, 85);
            this.panelStudy.TabIndex = 5;
            // 
            // panel16
            // 
            this.panel16.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel16.Location = new System.Drawing.Point(0, 30);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(1364, 10);
            this.panel16.TabIndex = 14;
            // 
            // panelRows
            // 
            this.panelRows.Controls.Add(this.dataGridView);
            this.panelRows.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelRows.Location = new System.Drawing.Point(0, 50);
            this.panelRows.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelRows.Name = "panelRows";
            this.panelRows.Size = new System.Drawing.Size(1364, 429);
            this.panelRows.TabIndex = 7;
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column4,
            this.RecState,
            this.ColumnEventDate,
            this.Column12,
            this.ColumnSampleNr,
            this.ColumnAnalysisDate,
            this.Column2,
            this.Column3,
            this.ColumnFindings,
            this.Column14,
            this.ColumnRemarks,
            this.Column9,
            this.Column11,
            this.Column10,
            this.N,
            this.Column8,
            this.Column13,
            this.findingsClass,
            this.RecordIX,
            this.RecordET,
            this.RecordingType});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Arial", 10F);
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView.DefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView.Location = new System.Drawing.Point(0, 0);
            this.dataGridView.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.RowHeadersVisible = false;
            this.dataGridView.RowTemplate.Height = 33;
            this.dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView.Size = new System.Drawing.Size(1364, 429);
            this.dataGridView.TabIndex = 3;
            this.dataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellClick);
            this.dataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellContentClick);
            this.dataGridView.Click += new System.EventHandler(this.dataGridView_Click);
            this.dataGridView.DoubleClick += new System.EventHandler(this.dataGridView_DoubleClick);
            this.dataGridView.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dataGridView_MouseClick);
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.Column1.HeaderText = "Add table";
            this.Column1.MinimumWidth = 54;
            this.Column1.Name = "Column1";
            this.Column1.Width = 54;
            // 
            // Column5
            // 
            this.Column5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.Column5.HeaderText = "Print strip";
            this.Column5.MinimumWidth = 54;
            this.Column5.Name = "Column5";
            this.Column5.Width = 54;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Report date";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Visible = false;
            // 
            // Column7
            // 
            this.Column7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.Column7.HeaderText = "Report #";
            this.Column7.MaxInputLength = 6;
            this.Column7.MinimumWidth = 60;
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Visible = false;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Column4.DefaultCellStyle = dataGridViewCellStyle1;
            this.Column4.HeaderText = "Event #";
            this.Column4.MaxInputLength = 6;
            this.Column4.MinimumWidth = 50;
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 50;
            // 
            // RecState
            // 
            this.RecState.HeaderText = "sta";
            this.RecState.Name = "RecState";
            this.RecState.Width = 36;
            // 
            // ColumnEventDate
            // 
            this.ColumnEventDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColumnEventDate.HeaderText = "Event Date Time";
            this.ColumnEventDate.MaxInputLength = 15;
            this.ColumnEventDate.Name = "ColumnEventDate";
            this.ColumnEventDate.ReadOnly = true;
            this.ColumnEventDate.Width = 98;
            // 
            // Column12
            // 
            this.Column12.HeaderText = "M/A";
            this.Column12.MaxInputLength = 3;
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.Width = 40;
            // 
            // ColumnSampleNr
            // 
            this.ColumnSampleNr.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.ColumnSampleNr.HeaderText = "A #";
            this.ColumnSampleNr.MaxInputLength = 2;
            this.ColumnSampleNr.MinimumWidth = 25;
            this.ColumnSampleNr.Name = "ColumnSampleNr";
            this.ColumnSampleNr.ReadOnly = true;
            this.ColumnSampleNr.Visible = false;
            // 
            // ColumnAnalysisDate
            // 
            this.ColumnAnalysisDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.ColumnAnalysisDate.HeaderText = "Analysis Date";
            this.ColumnAnalysisDate.MaxInputLength = 15;
            this.ColumnAnalysisDate.MinimumWidth = 100;
            this.ColumnAnalysisDate.Name = "ColumnAnalysisDate";
            this.ColumnAnalysisDate.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column2.HeaderText = "Tech";
            this.Column2.MaxInputLength = 4;
            this.Column2.MinimumWidth = 44;
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 63;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.Column3.HeaderText = "Class";
            this.Column3.MaxInputLength = 9;
            this.Column3.MinimumWidth = 60;
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 60;
            // 
            // ColumnFindings
            // 
            this.ColumnFindings.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.ColumnFindings.HeaderText = "Rhythm";
            this.ColumnFindings.MaxInputLength = 9;
            this.ColumnFindings.MinimumWidth = 60;
            this.ColumnFindings.Name = "ColumnFindings";
            this.ColumnFindings.ReadOnly = true;
            this.ColumnFindings.Width = 60;
            // 
            // Column14
            // 
            this.Column14.HeaderText = "Findings";
            this.Column14.Name = "Column14";
            this.Column14.Width = 200;
            // 
            // ColumnRemarks
            // 
            this.ColumnRemarks.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.ColumnRemarks.HeaderText = "Symptoms";
            this.ColumnRemarks.MaxInputLength = 32;
            this.ColumnRemarks.MinimumWidth = 200;
            this.ColumnRemarks.Name = "ColumnRemarks";
            this.ColumnRemarks.ReadOnly = true;
            this.ColumnRemarks.Width = 200;
            // 
            // Column9
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Column9.DefaultCellStyle = dataGridViewCellStyle2;
            this.Column9.HeaderText = "HR Min";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 44;
            // 
            // Column11
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Column11.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column11.HeaderText = "HR Mean";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.Width = 44;
            // 
            // Column10
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Column10.DefaultCellStyle = dataGridViewCellStyle4;
            this.Column10.HeaderText = "HR Max";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Width = 44;
            // 
            // N
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Gray;
            this.N.DefaultCellStyle = dataGridViewCellStyle5;
            this.N.HeaderText = "HR N";
            this.N.Name = "N";
            this.N.Width = 40;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "AnalysisIndex";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Visible = false;
            // 
            // Column13
            // 
            this.Column13.HeaderText = "IsDay";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            this.Column13.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column13.Visible = false;
            // 
            // findingsClass
            // 
            this.findingsClass.HeaderText = "FindingsClass";
            this.findingsClass.Name = "findingsClass";
            this.findingsClass.Visible = false;
            // 
            // RecordIX
            // 
            this.RecordIX.HeaderText = "RecordIX";
            this.RecordIX.Name = "RecordIX";
            this.RecordIX.Visible = false;
            // 
            // RecordET
            // 
            this.RecordET.HeaderText = "RecordET";
            this.RecordET.Name = "RecordET";
            this.RecordET.Visible = false;
            // 
            // RecordingType
            // 
            this.RecordingType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.RecordingType.HeaderText = "Rec. Type";
            this.RecordingType.MinimumWidth = 50;
            this.RecordingType.Name = "RecordingType";
            this.RecordingType.Width = 50;
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.labelListAbN);
            this.panel8.Controls.Add(this.label6);
            this.panel8.Controls.Add(this.labelListNorm);
            this.panel8.Controls.Add(this.label11);
            this.panel8.Controls.Add(this.labelListOther);
            this.panel8.Controls.Add(this.label20);
            this.panel8.Controls.Add(this.labelListBL);
            this.panel8.Controls.Add(this.label34);
            this.panel8.Controls.Add(this.labelListVT);
            this.panel8.Controls.Add(this.label32);
            this.panel8.Controls.Add(this.labelListPace);
            this.panel8.Controls.Add(this.label30);
            this.panel8.Controls.Add(this.labelListPVC);
            this.panel8.Controls.Add(this.label23);
            this.panel8.Controls.Add(this.labelListPac);
            this.panel8.Controls.Add(this.label19);
            this.panel8.Controls.Add(this.labelListAfl);
            this.panel8.Controls.Add(this.label28);
            this.panel8.Controls.Add(this.labelListAfib);
            this.panel8.Controls.Add(this.label17);
            this.panel8.Controls.Add(this.labelListPause);
            this.panel8.Controls.Add(this.label12);
            this.panel8.Controls.Add(this.labelListBrady);
            this.panel8.Controls.Add(this.label9);
            this.panel8.Controls.Add(this.labelListTachy);
            this.panel8.Controls.Add(this.label7);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel8.Location = new System.Drawing.Point(0, 509);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(1364, 21);
            this.panel8.TabIndex = 8;
            // 
            // labelListAbN
            // 
            this.labelListAbN.AutoSize = true;
            this.labelListAbN.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelListAbN.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelListAbN.Location = new System.Drawing.Point(1339, 0);
            this.labelListAbN.Margin = new System.Windows.Forms.Padding(0);
            this.labelListAbN.Name = "labelListAbN";
            this.labelListAbN.Size = new System.Drawing.Size(72, 16);
            this.labelListAbN.TabIndex = 149;
            this.labelListAbN.Text = "^05/ 1999";
            this.labelListAbN.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.Dock = System.Windows.Forms.DockStyle.Left;
            this.label6.Font = new System.Drawing.Font("Arial", 10F);
            this.label6.Location = new System.Drawing.Point(1293, 0);
            this.label6.Margin = new System.Windows.Forms.Padding(0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 19);
            this.label6.TabIndex = 148;
            this.label6.Text = "AbN:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelListNorm
            // 
            this.labelListNorm.AutoSize = true;
            this.labelListNorm.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelListNorm.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelListNorm.Location = new System.Drawing.Point(1221, 0);
            this.labelListNorm.Margin = new System.Windows.Forms.Padding(0);
            this.labelListNorm.Name = "labelListNorm";
            this.labelListNorm.Size = new System.Drawing.Size(72, 16);
            this.labelListNorm.TabIndex = 147;
            this.labelListNorm.Text = "^05/ 1999";
            this.labelListNorm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label11
            // 
            this.label11.Dock = System.Windows.Forms.DockStyle.Left;
            this.label11.Font = new System.Drawing.Font("Arial", 10F);
            this.label11.Location = new System.Drawing.Point(1175, 0);
            this.label11.Margin = new System.Windows.Forms.Padding(0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(46, 19);
            this.label11.TabIndex = 146;
            this.label11.Text = "Norm:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelListOther
            // 
            this.labelListOther.AutoSize = true;
            this.labelListOther.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelListOther.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelListOther.Location = new System.Drawing.Point(1103, 0);
            this.labelListOther.Margin = new System.Windows.Forms.Padding(0);
            this.labelListOther.Name = "labelListOther";
            this.labelListOther.Size = new System.Drawing.Size(72, 16);
            this.labelListOther.TabIndex = 135;
            this.labelListOther.Text = "^05/ 1999";
            this.labelListOther.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label20
            // 
            this.label20.Dock = System.Windows.Forms.DockStyle.Left;
            this.label20.Font = new System.Drawing.Font("Arial", 10F);
            this.label20.Location = new System.Drawing.Point(1051, 0);
            this.label20.Margin = new System.Windows.Forms.Padding(0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(52, 19);
            this.label20.TabIndex = 134;
            this.label20.Text = "Other:";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelListBL
            // 
            this.labelListBL.AutoSize = true;
            this.labelListBL.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelListBL.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelListBL.Location = new System.Drawing.Point(979, 0);
            this.labelListBL.Margin = new System.Windows.Forms.Padding(0);
            this.labelListBL.Name = "labelListBL";
            this.labelListBL.Size = new System.Drawing.Size(72, 16);
            this.labelListBL.TabIndex = 141;
            this.labelListBL.Text = "^05/ 1999";
            this.labelListBL.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label34
            // 
            this.label34.Dock = System.Windows.Forms.DockStyle.Left;
            this.label34.Font = new System.Drawing.Font("Arial", 10F);
            this.label34.Location = new System.Drawing.Point(949, 0);
            this.label34.Margin = new System.Windows.Forms.Padding(0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(30, 19);
            this.label34.TabIndex = 140;
            this.label34.Text = "BL:";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelListVT
            // 
            this.labelListVT.AutoSize = true;
            this.labelListVT.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelListVT.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelListVT.Location = new System.Drawing.Point(877, 0);
            this.labelListVT.Margin = new System.Windows.Forms.Padding(0);
            this.labelListVT.Name = "labelListVT";
            this.labelListVT.Size = new System.Drawing.Size(72, 16);
            this.labelListVT.TabIndex = 139;
            this.labelListVT.Text = "^05/ 1999";
            this.labelListVT.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label32
            // 
            this.label32.Dock = System.Windows.Forms.DockStyle.Left;
            this.label32.Font = new System.Drawing.Font("Arial", 10F);
            this.label32.Location = new System.Drawing.Point(847, 0);
            this.label32.Margin = new System.Windows.Forms.Padding(0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(30, 19);
            this.label32.TabIndex = 138;
            this.label32.Text = "VT:";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelListPace
            // 
            this.labelListPace.AutoSize = true;
            this.labelListPace.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelListPace.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelListPace.Location = new System.Drawing.Point(775, 0);
            this.labelListPace.Margin = new System.Windows.Forms.Padding(0);
            this.labelListPace.Name = "labelListPace";
            this.labelListPace.Size = new System.Drawing.Size(72, 16);
            this.labelListPace.TabIndex = 137;
            this.labelListPace.Text = "^05/ 1999";
            this.labelListPace.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label30
            // 
            this.label30.Dock = System.Windows.Forms.DockStyle.Left;
            this.label30.Font = new System.Drawing.Font("Arial", 10F);
            this.label30.Location = new System.Drawing.Point(731, 0);
            this.label30.Margin = new System.Windows.Forms.Padding(0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(44, 19);
            this.label30.TabIndex = 136;
            this.label30.Text = "Pace:";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelListPVC
            // 
            this.labelListPVC.AutoSize = true;
            this.labelListPVC.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelListPVC.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelListPVC.Location = new System.Drawing.Point(659, 0);
            this.labelListPVC.Margin = new System.Windows.Forms.Padding(0);
            this.labelListPVC.Name = "labelListPVC";
            this.labelListPVC.Size = new System.Drawing.Size(72, 16);
            this.labelListPVC.TabIndex = 129;
            this.labelListPVC.Text = "^05/ 1999";
            this.labelListPVC.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label23
            // 
            this.label23.Dock = System.Windows.Forms.DockStyle.Left;
            this.label23.Font = new System.Drawing.Font("Arial", 10F);
            this.label23.Location = new System.Drawing.Point(615, 0);
            this.label23.Margin = new System.Windows.Forms.Padding(0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(44, 19);
            this.label23.TabIndex = 128;
            this.label23.Text = "PVC:";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelListPac
            // 
            this.labelListPac.AutoSize = true;
            this.labelListPac.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelListPac.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelListPac.Location = new System.Drawing.Point(543, 0);
            this.labelListPac.Margin = new System.Windows.Forms.Padding(0);
            this.labelListPac.Name = "labelListPac";
            this.labelListPac.Size = new System.Drawing.Size(72, 16);
            this.labelListPac.TabIndex = 127;
            this.labelListPac.Text = "^05/ 1999";
            this.labelListPac.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label19
            // 
            this.label19.Dock = System.Windows.Forms.DockStyle.Left;
            this.label19.Font = new System.Drawing.Font("Arial", 10F);
            this.label19.Location = new System.Drawing.Point(504, 0);
            this.label19.Margin = new System.Windows.Forms.Padding(0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(39, 19);
            this.label19.TabIndex = 126;
            this.label19.Text = "PAC:";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelListAfl
            // 
            this.labelListAfl.AutoSize = true;
            this.labelListAfl.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelListAfl.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelListAfl.Location = new System.Drawing.Point(432, 0);
            this.labelListAfl.Margin = new System.Windows.Forms.Padding(0);
            this.labelListAfl.Name = "labelListAfl";
            this.labelListAfl.Size = new System.Drawing.Size(72, 16);
            this.labelListAfl.TabIndex = 133;
            this.labelListAfl.Text = "^05/ 1999";
            this.labelListAfl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label28
            // 
            this.label28.Dock = System.Windows.Forms.DockStyle.Left;
            this.label28.Font = new System.Drawing.Font("Arial", 10F);
            this.label28.Location = new System.Drawing.Point(404, 0);
            this.label28.Margin = new System.Windows.Forms.Padding(0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(28, 19);
            this.label28.TabIndex = 132;
            this.label28.Text = "Afl:";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelListAfib
            // 
            this.labelListAfib.AutoSize = true;
            this.labelListAfib.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelListAfib.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelListAfib.Location = new System.Drawing.Point(332, 0);
            this.labelListAfib.Margin = new System.Windows.Forms.Padding(0);
            this.labelListAfib.Name = "labelListAfib";
            this.labelListAfib.Size = new System.Drawing.Size(72, 16);
            this.labelListAfib.TabIndex = 131;
            this.labelListAfib.Text = "^05/ 1999";
            this.labelListAfib.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label17
            // 
            this.label17.Dock = System.Windows.Forms.DockStyle.Left;
            this.label17.Font = new System.Drawing.Font("Arial", 10F);
            this.label17.Location = new System.Drawing.Point(302, 0);
            this.label17.Margin = new System.Windows.Forms.Padding(0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(30, 19);
            this.label17.TabIndex = 130;
            this.label17.Text = "AF:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelListPause
            // 
            this.labelListPause.AutoSize = true;
            this.labelListPause.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelListPause.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelListPause.Location = new System.Drawing.Point(230, 0);
            this.labelListPause.Margin = new System.Windows.Forms.Padding(0);
            this.labelListPause.Name = "labelListPause";
            this.labelListPause.Size = new System.Drawing.Size(72, 16);
            this.labelListPause.TabIndex = 125;
            this.labelListPause.Text = "^05/ 1999";
            this.labelListPause.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label12
            // 
            this.label12.Dock = System.Windows.Forms.DockStyle.Left;
            this.label12.Font = new System.Drawing.Font("Arial", 10F);
            this.label12.Location = new System.Drawing.Point(200, 0);
            this.label12.Margin = new System.Windows.Forms.Padding(0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(30, 19);
            this.label12.TabIndex = 124;
            this.label12.Text = "Pa:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelListBrady
            // 
            this.labelListBrady.AutoSize = true;
            this.labelListBrady.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelListBrady.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelListBrady.Location = new System.Drawing.Point(128, 0);
            this.labelListBrady.Margin = new System.Windows.Forms.Padding(0);
            this.labelListBrady.Name = "labelListBrady";
            this.labelListBrady.Size = new System.Drawing.Size(72, 16);
            this.labelListBrady.TabIndex = 123;
            this.labelListBrady.Text = "^05/ 1999";
            this.labelListBrady.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.Dock = System.Windows.Forms.DockStyle.Left;
            this.label9.Font = new System.Drawing.Font("Arial", 10F);
            this.label9.Location = new System.Drawing.Point(100, 0);
            this.label9.Margin = new System.Windows.Forms.Padding(0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(28, 19);
            this.label9.TabIndex = 122;
            this.label9.Text = "By:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelListTachy
            // 
            this.labelListTachy.AutoSize = true;
            this.labelListTachy.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelListTachy.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelListTachy.Location = new System.Drawing.Point(28, 0);
            this.labelListTachy.Margin = new System.Windows.Forms.Padding(0);
            this.labelListTachy.Name = "labelListTachy";
            this.labelListTachy.Size = new System.Drawing.Size(72, 16);
            this.labelListTachy.TabIndex = 121;
            this.labelListTachy.Text = "^05/ 1999";
            this.labelListTachy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.Dock = System.Windows.Forms.DockStyle.Left;
            this.label7.Font = new System.Drawing.Font("Arial", 10F);
            this.label7.Location = new System.Drawing.Point(0, 0);
            this.label7.Margin = new System.Windows.Forms.Padding(0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(28, 19);
            this.label7.TabIndex = 120;
            this.label7.Text = "Ty:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // buttonSortET
            // 
            this.buttonSortET.BackColor = System.Drawing.Color.Transparent;
            this.buttonSortET.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonSortET.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonSortET.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonSortET.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSortET.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.buttonSortET.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonSortET.Location = new System.Drawing.Point(1209, 0);
            this.buttonSortET.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonSortET.Name = "buttonSortET";
            this.buttonSortET.Size = new System.Drawing.Size(149, 28);
            this.buttonSortET.TabIndex = 53;
            this.buttonSortET.Text = "Sort by Event Time";
            this.buttonSortET.UseVisualStyleBackColor = false;
            this.buttonSortET.Click += new System.EventHandler(this.buttonSortET_Click);
            // 
            // panel19
            // 
            this.panel19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel19.Controls.Add(this.labelHR);
            this.panel19.Controls.Add(this.labelListExtra);
            this.panel19.Controls.Add(this.buttonDisableAn);
            this.panel19.Controls.Add(this.panel29);
            this.panel19.Controls.Add(this.buttonReloadList);
            this.panel19.Controls.Add(this.panel28);
            this.panel19.Controls.Add(this.buttonOpenAnalysis);
            this.panel19.Controls.Add(this.panel32);
            this.panel19.Controls.Add(this.buttonSortET);
            this.panel19.Controls.Add(this.panel2);
            this.panel19.Controls.Add(this.labelListQC);
            this.panel19.Controls.Add(this.label22);
            this.panel19.Controls.Add(this.labelListManuals);
            this.panel19.Controls.Add(this.label3);
            this.panel19.Controls.Add(this.labelListStrips);
            this.panel19.Controls.Add(this.label1);
            this.panel19.Controls.Add(this.labelListTable);
            this.panel19.Controls.Add(this.label8);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel19.Location = new System.Drawing.Point(0, 479);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(1364, 30);
            this.panel19.TabIndex = 9;
            // 
            // labelHR
            // 
            this.labelHR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelHR.Location = new System.Drawing.Point(858, 0);
            this.labelHR.Name = "labelHR";
            this.labelHR.Size = new System.Drawing.Size(270, 28);
            this.labelHR.TabIndex = 113;
            this.labelHR.Text = "^HR  = ^12 <1 25 < 255 ";
            this.labelHR.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelListExtra
            // 
            this.labelListExtra.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelListExtra.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelListExtra.Location = new System.Drawing.Point(747, 0);
            this.labelListExtra.Margin = new System.Windows.Forms.Padding(0);
            this.labelListExtra.Name = "labelListExtra";
            this.labelListExtra.Size = new System.Drawing.Size(111, 28);
            this.labelListExtra.TabIndex = 148;
            this.labelListExtra.Text = "^999 unknown";
            this.labelListExtra.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // buttonDisableAn
            // 
            this.buttonDisableAn.BackColor = System.Drawing.Color.Transparent;
            this.buttonDisableAn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonDisableAn.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonDisableAn.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonDisableAn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDisableAn.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.buttonDisableAn.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonDisableAn.Location = new System.Drawing.Point(668, 0);
            this.buttonDisableAn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonDisableAn.Name = "buttonDisableAn";
            this.buttonDisableAn.Size = new System.Drawing.Size(79, 28);
            this.buttonDisableAn.TabIndex = 117;
            this.buttonDisableAn.Text = "Disable";
            this.buttonDisableAn.UseVisualStyleBackColor = false;
            this.buttonDisableAn.Click += new System.EventHandler(this.buttonDisableAn_Click);
            // 
            // panel29
            // 
            this.panel29.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel29.Location = new System.Drawing.Point(664, 0);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(4, 28);
            this.panel29.TabIndex = 116;
            // 
            // buttonReloadList
            // 
            this.buttonReloadList.BackColor = System.Drawing.Color.Transparent;
            this.buttonReloadList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonReloadList.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonReloadList.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonReloadList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonReloadList.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.buttonReloadList.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonReloadList.Location = new System.Drawing.Point(559, 0);
            this.buttonReloadList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonReloadList.Name = "buttonReloadList";
            this.buttonReloadList.Size = new System.Drawing.Size(105, 28);
            this.buttonReloadList.TabIndex = 115;
            this.buttonReloadList.Text = "Reload List";
            this.buttonReloadList.UseVisualStyleBackColor = false;
            this.buttonReloadList.Click += new System.EventHandler(this.buttonReloadList_Click);
            // 
            // panel28
            // 
            this.panel28.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel28.Location = new System.Drawing.Point(555, 0);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(4, 28);
            this.panel28.TabIndex = 114;
            // 
            // buttonOpenAnalysis
            // 
            this.buttonOpenAnalysis.BackColor = System.Drawing.Color.Transparent;
            this.buttonOpenAnalysis.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonOpenAnalysis.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonOpenAnalysis.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonOpenAnalysis.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOpenAnalysis.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.buttonOpenAnalysis.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonOpenAnalysis.Location = new System.Drawing.Point(1128, 0);
            this.buttonOpenAnalysis.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonOpenAnalysis.Name = "buttonOpenAnalysis";
            this.buttonOpenAnalysis.Size = new System.Drawing.Size(77, 28);
            this.buttonOpenAnalysis.TabIndex = 110;
            this.buttonOpenAnalysis.Text = "Analyse";
            this.buttonOpenAnalysis.UseVisualStyleBackColor = false;
            this.buttonOpenAnalysis.Click += new System.EventHandler(this.buttonOpenAnalysis_Click);
            // 
            // panel32
            // 
            this.panel32.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel32.Location = new System.Drawing.Point(1205, 0);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(4, 28);
            this.panel32.TabIndex = 112;
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(1358, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(4, 28);
            this.panel2.TabIndex = 111;
            // 
            // labelListQC
            // 
            this.labelListQC.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelListQC.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelListQC.Location = new System.Drawing.Point(474, 0);
            this.labelListQC.Name = "labelListQC";
            this.labelListQC.Size = new System.Drawing.Size(81, 28);
            this.labelListQC.TabIndex = 150;
            this.labelListQC.Text = "^105/ 9999";
            this.labelListQC.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label22
            // 
            this.label22.Dock = System.Windows.Forms.DockStyle.Left;
            this.label22.Font = new System.Drawing.Font("Arial", 10F);
            this.label22.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label22.Location = new System.Drawing.Point(432, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(42, 28);
            this.label22.TabIndex = 149;
            this.label22.Text = "QC:";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelListManuals
            // 
            this.labelListManuals.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelListManuals.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelListManuals.Location = new System.Drawing.Point(345, 0);
            this.labelListManuals.Name = "labelListManuals";
            this.labelListManuals.Size = new System.Drawing.Size(87, 28);
            this.labelListManuals.TabIndex = 119;
            this.labelListManuals.Text = "^105/ 9999";
            this.labelListManuals.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Left;
            this.label3.Font = new System.Drawing.Font("Arial", 10F);
            this.label3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label3.Location = new System.Drawing.Point(277, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 28);
            this.label3.TabIndex = 118;
            this.label3.Text = "Manuals:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelListStrips
            // 
            this.labelListStrips.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelListStrips.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelListStrips.Location = new System.Drawing.Point(194, 0);
            this.labelListStrips.Name = "labelListStrips";
            this.labelListStrips.Size = new System.Drawing.Size(83, 28);
            this.labelListStrips.TabIndex = 107;
            this.labelListStrips.Text = "^105/ 9999";
            this.labelListStrips.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Font = new System.Drawing.Font("Arial", 10F);
            this.label1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label1.Location = new System.Drawing.Point(144, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 28);
            this.label1.TabIndex = 106;
            this.label1.Text = "Strips:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelListTable
            // 
            this.labelListTable.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelListTable.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelListTable.Location = new System.Drawing.Point(61, 0);
            this.labelListTable.Name = "labelListTable";
            this.labelListTable.Size = new System.Drawing.Size(83, 28);
            this.labelListTable.TabIndex = 109;
            this.labelListTable.Text = "^105/ 9999";
            this.labelListTable.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.Dock = System.Windows.Forms.DockStyle.Left;
            this.label8.Font = new System.Drawing.Font("Arial", 10F);
            this.label8.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label8.Location = new System.Drawing.Point(0, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 28);
            this.label8.TabIndex = 108;
            this.label8.Text = "Tables:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel10
            // 
            this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel10.Location = new System.Drawing.Point(0, 50);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(1364, 490);
            this.panel10.TabIndex = 11;
            // 
            // panel26
            // 
            this.panel26.Controls.Add(this.splitContainerFindingsConclusion);
            this.panel26.Controls.Add(this.panel30);
            this.panel26.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel26.Location = new System.Drawing.Point(0, 625);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(1364, 190);
            this.panel26.TabIndex = 14;
            // 
            // splitContainerFindingsConclusion
            // 
            this.splitContainerFindingsConclusion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainerFindingsConclusion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerFindingsConclusion.Location = new System.Drawing.Point(0, 0);
            this.splitContainerFindingsConclusion.Name = "splitContainerFindingsConclusion";
            // 
            // splitContainerFindingsConclusion.Panel1
            // 
            this.splitContainerFindingsConclusion.Panel1.Controls.Add(this.textBoxSummary);
            this.splitContainerFindingsConclusion.Panel1.Controls.Add(this.panel9);
            this.splitContainerFindingsConclusion.Panel1.Controls.Add(this.panel34);
            // 
            // splitContainerFindingsConclusion.Panel2
            // 
            this.splitContainerFindingsConclusion.Panel2.Controls.Add(this.textBoxConclusion);
            this.splitContainerFindingsConclusion.Panel2.Controls.Add(this.panel31);
            this.splitContainerFindingsConclusion.Panel2.Controls.Add(this.panel38);
            this.splitContainerFindingsConclusion.Size = new System.Drawing.Size(1364, 180);
            this.splitContainerFindingsConclusion.SplitterDistance = 667;
            this.splitContainerFindingsConclusion.TabIndex = 15;
            // 
            // textBoxSummary
            // 
            this.textBoxSummary.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.textBoxSummary.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxSummary.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxSummary.Location = new System.Drawing.Point(0, 34);
            this.textBoxSummary.MaxLength = 800;
            this.textBoxSummary.Multiline = true;
            this.textBoxSummary.Name = "textBoxSummary";
            this.textBoxSummary.Size = new System.Drawing.Size(655, 144);
            this.textBoxSummary.TabIndex = 2;
            this.textBoxSummary.Text = resources.GetString("textBoxSummary.Text");
            // 
            // panel9
            // 
            this.panel9.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel9.Location = new System.Drawing.Point(655, 34);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(10, 144);
            this.panel9.TabIndex = 3;
            // 
            // panel34
            // 
            this.panel34.Controls.Add(this.labelUseExluded);
            this.panel34.Controls.Add(this.checkBoxQCD);
            this.panel34.Controls.Add(this.buttonClearFindings);
            this.panel34.Controls.Add(this.label13);
            this.panel34.Controls.Add(this.panel39);
            this.panel34.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel34.Location = new System.Drawing.Point(0, 0);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(665, 34);
            this.panel34.TabIndex = 0;
            // 
            // labelUseExluded
            // 
            this.labelUseExluded.AutoSize = true;
            this.labelUseExluded.Font = new System.Drawing.Font("Arial", 10F);
            this.labelUseExluded.Location = new System.Drawing.Point(294, 8);
            this.labelUseExluded.Name = "labelUseExluded";
            this.labelUseExluded.Size = new System.Drawing.Size(83, 16);
            this.labelUseExluded.TabIndex = 71;
            this.labelUseExluded.Text = "^+Excluded^";
            this.labelUseExluded.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelUseExluded.Visible = false;
            // 
            // checkBoxQCD
            // 
            this.checkBoxQCD.AutoSize = true;
            this.checkBoxQCD.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxQCD.Location = new System.Drawing.Point(204, 0);
            this.checkBoxQCD.Name = "checkBoxQCD";
            this.checkBoxQCD.Size = new System.Drawing.Size(48, 34);
            this.checkBoxQCD.TabIndex = 22;
            this.checkBoxQCD.Text = "QC";
            this.checkBoxQCD.UseVisualStyleBackColor = true;
            // 
            // buttonClearFindings
            // 
            this.buttonClearFindings.BackColor = System.Drawing.Color.Transparent;
            this.buttonClearFindings.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonClearFindings.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonClearFindings.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonClearFindings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonClearFindings.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonClearFindings.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonClearFindings.Location = new System.Drawing.Point(480, 0);
            this.buttonClearFindings.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonClearFindings.Name = "buttonClearFindings";
            this.buttonClearFindings.Size = new System.Drawing.Size(175, 34);
            this.buttonClearFindings.TabIndex = 18;
            this.buttonClearFindings.Text = "Clear Findings";
            this.buttonClearFindings.UseVisualStyleBackColor = false;
            this.buttonClearFindings.Click += new System.EventHandler(this.buttonClearFindings_Click);
            // 
            // label13
            // 
            this.label13.Dock = System.Windows.Forms.DockStyle.Left;
            this.label13.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(0, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(204, 34);
            this.label13.TabIndex = 6;
            this.label13.Text = "Report Findings";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel39
            // 
            this.panel39.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel39.Location = new System.Drawing.Point(655, 0);
            this.panel39.Name = "panel39";
            this.panel39.Size = new System.Drawing.Size(10, 34);
            this.panel39.TabIndex = 19;
            // 
            // textBoxConclusion
            // 
            this.textBoxConclusion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.textBoxConclusion.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxConclusion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxConclusion.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxConclusion.Location = new System.Drawing.Point(10, 34);
            this.textBoxConclusion.MaxLength = 250;
            this.textBoxConclusion.Multiline = true;
            this.textBoxConclusion.Name = "textBoxConclusion";
            this.textBoxConclusion.Size = new System.Drawing.Size(681, 144);
            this.textBoxConclusion.TabIndex = 3;
            this.textBoxConclusion.Text = "^This is a manual  summary of the study";
            // 
            // panel31
            // 
            this.panel31.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel31.Location = new System.Drawing.Point(0, 34);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(10, 144);
            this.panel31.TabIndex = 4;
            // 
            // panel38
            // 
            this.panel38.Controls.Add(this.buttonClearConclusion);
            this.panel38.Controls.Add(this.label14);
            this.panel38.Controls.Add(this.panel36);
            this.panel38.Controls.Add(this.panel42);
            this.panel38.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel38.Location = new System.Drawing.Point(0, 0);
            this.panel38.Name = "panel38";
            this.panel38.Size = new System.Drawing.Size(691, 34);
            this.panel38.TabIndex = 1;
            // 
            // buttonClearConclusion
            // 
            this.buttonClearConclusion.BackColor = System.Drawing.Color.Transparent;
            this.buttonClearConclusion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonClearConclusion.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonClearConclusion.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonClearConclusion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonClearConclusion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonClearConclusion.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonClearConclusion.Location = new System.Drawing.Point(512, 0);
            this.buttonClearConclusion.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonClearConclusion.Name = "buttonClearConclusion";
            this.buttonClearConclusion.Size = new System.Drawing.Size(175, 34);
            this.buttonClearConclusion.TabIndex = 20;
            this.buttonClearConclusion.Text = "Clear Conclusion";
            this.buttonClearConclusion.UseVisualStyleBackColor = false;
            this.buttonClearConclusion.Click += new System.EventHandler(this.buttonClearConclusion_Click);
            // 
            // label14
            // 
            this.label14.Dock = System.Windows.Forms.DockStyle.Left;
            this.label14.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(10, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(186, 34);
            this.label14.TabIndex = 7;
            this.label14.Text = "Physician Conclusion";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel36
            // 
            this.panel36.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel36.Location = new System.Drawing.Point(0, 0);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(10, 34);
            this.panel36.TabIndex = 21;
            // 
            // panel42
            // 
            this.panel42.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel42.Location = new System.Drawing.Point(687, 0);
            this.panel42.Name = "panel42";
            this.panel42.Size = new System.Drawing.Size(4, 34);
            this.panel42.TabIndex = 58;
            // 
            // panel30
            // 
            this.panel30.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel30.Location = new System.Drawing.Point(0, 180);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(1364, 10);
            this.panel30.TabIndex = 14;
            // 
            // panel40
            // 
            this.panel40.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel40.Location = new System.Drawing.Point(0, 530);
            this.panel40.Name = "panel40";
            this.panel40.Size = new System.Drawing.Size(1364, 10);
            this.panel40.TabIndex = 15;
            // 
            // CPreviousFindingsForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(1364, 861);
            this.Controls.Add(this.panelRows);
            this.Controls.Add(this.panel19);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel40);
            this.Controls.Add(this.panel10);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panelStudy);
            this.Controls.Add(this.panel26);
            this.Controls.Add(this.panelStudyButtons);
            this.Font = new System.Drawing.Font("Arial", 10F);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "CPreviousFindingsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Report Center";
            this.panel6.ResumeLayout(false);
            this.panel27.ResumeLayout(false);
            this.panel25.ResumeLayout(false);
            this.panel24.ResumeLayout(false);
            this.panel23.ResumeLayout(false);
            this.panel21.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel33.ResumeLayout(false);
            this.panelStudyButtons.ResumeLayout(false);
            this.panelStudyButtons.PerformLayout();
            this.panelSelect.ResumeLayout(false);
            this.panelSelect.PerformLayout();
            this.panelStudyPeriod.ResumeLayout(false);
            this.panel22.ResumeLayout(false);
            this.panel18.ResumeLayout(false);
            this.panel65.ResumeLayout(false);
            this.panel58.ResumeLayout(false);
            this.panel64.ResumeLayout(false);
            this.panel62.ResumeLayout(false);
            this.panel62.PerformLayout();
            this.panel57.ResumeLayout(false);
            this.panel57.PerformLayout();
            this.panel35.ResumeLayout(false);
            this.panel54.ResumeLayout(false);
            this.panel54.PerformLayout();
            this.panelStudy.ResumeLayout(false);
            this.panelRows.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel19.ResumeLayout(false);
            this.panel26.ResumeLayout(false);
            this.splitContainerFindingsConclusion.Panel1.ResumeLayout(false);
            this.splitContainerFindingsConclusion.Panel1.PerformLayout();
            this.splitContainerFindingsConclusion.Panel2.ResumeLayout(false);
            this.splitContainerFindingsConclusion.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerFindingsConclusion)).EndInit();
            this.splitContainerFindingsConclusion.ResumeLayout(false);
            this.panel34.ResumeLayout(false);
            this.panel34.PerformLayout();
            this.panel38.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panelStudyButtons;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelPreviousFindingsList;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelGender;
        private System.Windows.Forms.Label labelAge;
        private System.Windows.Forms.Label labelDoB;
        private System.Windows.Forms.Label labelFullName;
        private System.Windows.Forms.Label labelStudyIndex;
        private System.Windows.Forms.Panel panelSelect;
        private System.Windows.Forms.CheckBox checkBoxModifyAF;
        private System.Windows.Forms.CheckBox checkBoxModifyPause;
        private System.Windows.Forms.CheckBox checkBoxModifyBrady;
        private System.Windows.Forms.CheckBox checkBoxModifyTachy;
        private System.Windows.Forms.Panel panelStudyPeriod;
        private System.Windows.Forms.Panel panel37;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Panel panelStudy;
        private System.Windows.Forms.Panel panelRows;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label labelReportResult;
        private System.Windows.Forms.Button buttonViewPdf;
        private System.Windows.Forms.Label labelPdfNr;
        private System.Windows.Forms.CheckBox checkBoxModifyOther;
        private System.Windows.Forms.Panel panel35;
        private System.Windows.Forms.CheckBox checkBoxModifyAll;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.CheckBox checkBoxModifyBL;
        private System.Windows.Forms.CheckBox checkBoxModifyHR1;
        private System.Windows.Forms.CheckBox checkBoxModifyHR0;
        private System.Windows.Forms.CheckBox checkBoxTable;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.CheckBox checkBoxStrip;
        private System.Windows.Forms.Label labelTBPAO;
        private System.Windows.Forms.CheckBox checkBoxPlot2ch;
        private System.Windows.Forms.CheckBox checkBoxAnonymize;
        private System.Windows.Forms.CheckBox checkBoxReducePdf;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button buttonPrintPreviousFindings;
        private System.Windows.Forms.Button buttonMCT;
        private System.Windows.Forms.Panel panel65;
        private System.Windows.Forms.Panel panel58;
        private System.Windows.Forms.Panel panel64;
        private System.Windows.Forms.Button buttonP24;
        private System.Windows.Forms.Button buttonP6;
        private System.Windows.Forms.Button buttonP1;
        private System.Windows.Forms.Button button0h;
        private System.Windows.Forms.Button buttonM1;
        private System.Windows.Forms.Button buttonM6;
        private System.Windows.Forms.Button buttonM24;
        private System.Windows.Forms.Panel panel62;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBoxToHour;
        private System.Windows.Forms.DateTimePicker dateTimePickerTo;
        private System.Windows.Forms.Label labelTo;
        private System.Windows.Forms.Button buttonEndStudy;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.CheckBox checkBoxModifyPVC;
        private System.Windows.Forms.CheckBox checkBoxModifyPAC;
        private System.Windows.Forms.Button buttonUncheckAll;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonSetSelection;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.CheckBox checkBoxModifyVT;
        private System.Windows.Forms.CheckBox checkBoxModifyPace;
        private System.Windows.Forms.CheckBox checkBoxModifyAfl;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label labelListOther;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label labelListBL;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label labelListVT;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label labelListPace;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label labelListPVC;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label labelListPac;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label labelListAfl;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label labelListAfib;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label labelListPause;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label labelListBrady;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelListTachy;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button buttonSortET;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Label labelListStrips;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelListTable;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button buttonOpenAnalysis;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.SplitContainer splitContainerFindingsConclusion;
        private System.Windows.Forms.TextBox textBoxSummary;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel34;
        private System.Windows.Forms.Button buttonClearFindings;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel39;
        private System.Windows.Forms.TextBox textBoxConclusion;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.Panel panel38;
        private System.Windows.Forms.Button buttonClearConclusion;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel36;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Panel panel40;
        private System.Windows.Forms.Panel panel41;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.Panel panel42;
        private System.Windows.Forms.Label labelNrAnalyzed;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label labelHR;
        private System.Windows.Forms.Panel panel54;
        private System.Windows.Forms.RadioButton radioButtonHour;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButtonMonth;
        private System.Windows.Forms.RadioButton radioButtonWeek;
        private System.Windows.Forms.RadioButton radioButtonDay;
        private System.Windows.Forms.RadioButton radioButtonPeriod;
        private System.Windows.Forms.RadioButton radioButtonEOS;
        private System.Windows.Forms.CheckBox checkBoxBlackWhite;
        private System.Windows.Forms.Button buttonReloadList;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.Button buttonDisableAn;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.Label labelListManuals;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox checkBoxModifyNormal;
        private System.Windows.Forms.Label labelListNorm;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label labelListExtra;
        private System.Windows.Forms.Button buttonAmplRange;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.Label labelRecordInfo;
        private System.Windows.Forms.CheckBox checkBoxQCD;
        private System.Windows.Forms.Label labelUseExluded;
        private System.Windows.Forms.CheckBox checkBoxModifyQC;
        private System.Windows.Forms.Label labelListQC;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.CheckBox checkBoxModifyManual;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn RecState;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnEventDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnSampleNr;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnAnalysisDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnFindings;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnRemarks;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn N;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn findingsClass;
        private System.Windows.Forms.DataGridViewTextBoxColumn RecordIX;
        private System.Windows.Forms.DataGridViewTextBoxColumn RecordET;
        private System.Windows.Forms.DataGridViewTextBoxColumn RecordingType;
        private System.Windows.Forms.CheckBox checkBoxModifyAbN;
        private System.Windows.Forms.Label labelListAbN;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel57;
        private System.Windows.Forms.Button buttonSetTimeFrame;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBoxFromHour;
        private System.Windows.Forms.DateTimePicker dateTimePickerFrom;
        private System.Windows.Forms.Label labelFrom;
        private System.Windows.Forms.Label labelStudyNote;
    }
}