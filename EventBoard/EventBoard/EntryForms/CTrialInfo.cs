﻿using Event_Base;
using Program_Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventboardEntryForms
{
    public enum DTrialInfoVars
    {
        // primary keys
        Label = 0,
        // variables
        SponsorClient,
        TrialStartDate,
        TrialStopDate,
        StudyMonitoringDays,
        StudyTypeCode,
        StudyProcCodeSet,
        ReportIntervalSet,
        MctIntervalSet,
        StudyRemarklabel,
        StudyFreeText,
        StudyPermissions,
        TrialState,
  
        NrSqlVars       // keep as last
    };

    public class CTrialInfo : CSqlDataTableRow
    {
        public string _mLabel;

        public UInt32 _mSponsorClient_IX;      // main client
        public DateTime _mTrialStartDate;
        public DateTime _mTrialStopDate;
        public UInt16 _mStudyMonitoringDays;
        public string _mStudyTypeCode;
        public CStringSet _mStudyProcCodeSet;

        public CStringSet _mMctIntervalSet;
        public CStringSet _mReportIntervalSet;
        public string _mStudyRemarkLabel;
        public string _mStudyFreeText;
        public CBitSet64 _mStudyPermissions;   // permissions bit mask (used for filtering records on site and operator skill grade
        public UInt16 _mTrialState;
        
        public CTrialInfo(CSqlDBaseConnection ASqlConnection):base( ASqlConnection, "d_TrialInfo")
        {
            try
            {
                UInt64 maskPrimary = CSqlDataTableRow.sGetMaskRange((UInt16)DTrialInfoVars.Label, (UInt16)DTrialInfoVars.Label);
                UInt64 maskValid = CSqlDataTableRow.sGetMaskRange((UInt16)0, (UInt16)DTrialInfoVars.NrSqlVars - 1);// mGetValidMask(false);

                mInitTableMasks(maskPrimary, maskValid);  // set primairyInsertFlags

                _mStudyProcCodeSet = new CStringSet();
                _mReportIntervalSet = new CStringSet();
                _mMctIntervalSet = new CStringSet();
                _mStudyPermissions = new CBitSet64();
                mClear();
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Init", ex);
            }
        }
        public override CSqlDataTableRow mCreateNewRow()
        {
            return new CTrialInfo(mGetSqlConnection());
        }
        public override bool mbCopyTo(CSqlDataTableRow ATo) // derived  public override void mbCopyTo() must call base.mbCopyTo(ATo)
        {
            bool bOk = false;

            if (ATo != null)
            {
                CTrialInfo to = ATo as CTrialInfo;

                if (to != null)
                {
                    bOk = base.mbCopyTo(ATo);

                    to._mLabel = _mLabel;
                    to._mSponsorClient_IX = _mSponsorClient_IX;      // main client
                    to._mTrialStartDate = _mTrialStartDate;
                    to._mTrialStopDate = _mTrialStopDate;
                    to._mStudyMonitoringDays = _mStudyMonitoringDays;
                    to._mStudyTypeCode = _mStudyTypeCode;
                    _mReportIntervalSet.mbCopyTo(ref to._mReportIntervalSet);
                    to._mMctIntervalSet.mbCopyFrom(_mMctIntervalSet);
                    _mStudyProcCodeSet.mbCopyTo(ref to._mStudyProcCodeSet);

                    to._mStudyRemarkLabel = _mStudyRemarkLabel;
                    to._mStudyFreeText = _mStudyFreeText;
                    _mStudyPermissions.mCopyTo(ref to._mStudyPermissions);
                    to._mTrialState = _mTrialState;
                }
            }
            return bOk;
        }
        public override void mClear()
        {
            base.mClear();
            _mLabel = "";
            _mSponsorClient_IX = 0;      // main client
            _mTrialStartDate = DateTime.MinValue;
            _mTrialStopDate = DateTime.MinValue;
            _mStudyMonitoringDays = 0;
            _mStudyTypeCode = "";
            _mStudyProcCodeSet.mClear();

            _mReportIntervalSet.mClear();
            _mMctIntervalSet.mClear();
            _mStudyRemarkLabel = "";
            _mStudyFreeText = "";
            _mStudyPermissions.mClear();
            _mTrialState = 0;
        }
        public override DSqlDataType mVarGetSet(DSqlDataCmd ACmd, UInt16 AVarIndex, ref string AVarSqlName, ref string AStringValue, out bool ArbValid)
        {
            switch ((DTrialInfoVars)AVarIndex)
            {
                // primary keys
                case DTrialInfoVars.Label:
                    return mSqlGetSetString(ref _mLabel, "Label", ACmd, ref AVarSqlName, ref AStringValue, 32, out ArbValid);
                // variables

                case DTrialInfoVars.SponsorClient:
                    return mSqlGetSetUInt32(ref _mSponsorClient_IX, "SponsorClient_IX", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DTrialInfoVars.TrialStartDate:
                    return mSqlGetSetDate(ref _mTrialStartDate, "TrialStartDate", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);

                case DTrialInfoVars.TrialStopDate:
                    return mSqlGetSetDate(ref _mTrialStopDate, "TrialStopDate", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DTrialInfoVars.StudyMonitoringDays:
                    return mSqlGetSetUInt16(ref _mStudyMonitoringDays, "StudyMonitoringDays", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DTrialInfoVars.StudyTypeCode:
                    return mSqlGetSetString(ref _mStudyTypeCode, "StudyTypeCode", ACmd, ref AVarSqlName, ref AStringValue, 16, out ArbValid);
                case DTrialInfoVars.StudyProcCodeSet:
                    return mSqlGetSetStringSet(ref _mStudyProcCodeSet, "StudyProcCodeSet", ACmd, ref AVarSqlName, ref AStringValue, 64, out ArbValid);
                case DTrialInfoVars.MctIntervalSet:
                    return mSqlGetSetStringSet(ref _mMctIntervalSet, "MctIntervalSet", ACmd, ref AVarSqlName, ref AStringValue, 64, out ArbValid);
                case DTrialInfoVars.ReportIntervalSet:
                    return mSqlGetSetStringSet(ref _mReportIntervalSet, "ReportIntervalSet", ACmd, ref AVarSqlName, ref AStringValue, 64, out ArbValid);
                case DTrialInfoVars.StudyRemarklabel:
                    return mSqlGetSetString(ref _mStudyRemarkLabel, "StudyRemarkLabel", ACmd, ref AVarSqlName, ref AStringValue, 256, out ArbValid);
                case DTrialInfoVars.StudyFreeText:
                    return mSqlGetSetString(ref _mStudyFreeText, "StudyFreeText", ACmd, ref AVarSqlName, ref AStringValue, 256, out ArbValid);
                case DTrialInfoVars.StudyPermissions:
                    return mSqlGetSetBitSet64(ref _mStudyPermissions, "StudyPermissions", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DTrialInfoVars.TrialState:
                    return mSqlGetSetUInt16(ref _mTrialState, "TrialState", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
            }
            return base.mVarGetSet(ACmd, AVarIndex, ref AVarSqlName, ref AStringValue, out ArbValid);
        }

        public static void sFillPrintTrialName(bool AbAnonymize, CStudyInfo AStudy, out string ArText1)
        {
            string text1 = "";

            if (false == AbAnonymize && AStudy != null && AStudy._mTrial_IX > 0)
            {
                CTrialInfo trial = new CTrialInfo(AStudy.mGetSqlConnection());

                if (trial != null)
                {
                    trial.mbDoSqlSelectIndex(AStudy._mTrial_IX, trial.mGetValidMask(true));// CSqlDataTableRow.sGetMask((UInt16)DTrialInfoVars.Label));
                    text1 = trial._mLabel;// _mLabel;
                    if (text1 == null || text1.Length == 0)
                    {
                        text1 = trial._mLabel;
                    }
                }
            }
            ArText1 = text1;
        }
        public static void sFillPrintTrialName(bool AbAnonymize, CStudyInfo AStudy, Label ALabel1, Label ALabel2, bool AbAddP)
        {
            string text1;

            sFillPrintTrialName(AbAnonymize, AStudy, out text1);

            if (ALabel1 != null) ALabel1.Text = text1;
        }
        public static string sGetTrialLabel(UInt32 ATrial_IX, bool AbAddNr, string AEmptyText)
        {
            string s = "";

            if (ATrial_IX > 0)
            {
                if (AbAddNr)
                {
                    s += "T" + ATrial_IX.ToString() + "_";
                }
                CTrialInfo info = new CTrialInfo(CDvtmsData.sGetDBaseConnection());

                if (info != null)
                {
                    if (info.mbDoSqlSelectIndex(ATrial_IX, info.mMaskAll))
                    {
                        s += info._mLabel;
                    }
                    else
                    {
                        s += "?" + ATrial_IX.ToString();
                    }
                }
            }
            else
            {
                s = AEmptyText;
            }
            return s;
        }

    }
}
