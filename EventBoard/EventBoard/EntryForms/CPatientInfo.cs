﻿using Event_Base;
using Program_Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventboardEntryForms
{
    public enum DGender {  Unknown, Male, Female, NrOfGenders };

    public enum DPatientState {  Unknown, New, StudyStart, StudyEnd, NrOfStates };

    public enum DPatSocID {  PatPresent = 0, SocSec, PatID };

    public enum DPatientInfoVars
    {
        // primary keys

        SocSecNr = 0,
        // variables
        PatientID,
        PatientFirstName,
        PatientMiddleName,
        PatientLastName,
        PatientDateOfBirth,
        PatientBirthYear,
        PatientGender_IX,
        PatientRace_IX,
        PatientHeightM,
        PatientWeightKg,
        PatientAddress,
        PatientZip,
        PatientHouseNumber,
        PatientCity,
        PatientState,
        PatientCountryCode,
        PatientPhone1,
        PatientPhone2,
        PatientCell,
        PatientEmail,
        PatientSkype,
        HistoryStringSet,
        AllergiesStringSet,
        MedicationStringSet,
        SymptomsStringSet,
        RemarkLabel,
        RemarkText,
        State_IX,
        NrSqlVars       // keep as last
    };

    public class CPatientInfo:CSqlDataTableRow
    {
        private CEncryptedString _mPatientID;    // use mGetPatientSocIDValue() in display
        public CEncryptedString _mSocSecNr;
        public CEncryptedString _mPatientFirstName;
        public CEncryptedString _mPatientMiddleName;
        public CEncryptedString _mPatientLastName;
        public CEncryptedInt _mPatientDateOfBirth;
        public UInt16 _mPatientBirthYear;
        public UInt16 _mPatientGender_IX;
        public UInt16 _mPatientRace_IX;
        public float _mPatientHeightM;
        public float _mPatientWeightKg;
        public CEncryptedString _mPatientAddress;
        public CEncryptedString _mPatientZip;
        public CEncryptedString _mPatientHouseNumber;
        public string _mPatientCity;
        public string _mPatientState;
        public string _mPatientCountryCode;
        public CEncryptedString _mPatientPhone1;
        public CEncryptedString _mPatientPhone2;
        public CEncryptedString _mPatientCell;
        public CEncryptedString _mPatientEmail;
        public CEncryptedString _mPatientSkype;
        public CStringSet _mHistoryStringSet;
        public CStringSet _mAllergiesStringSet;
        public CStringSet _mMedicationStringSet;
        public CStringSet _mSymptomsStringSet;
        public string _mRemarkLabel;
        public string _mRemarkText; // not used at the moment!
        public UInt16 _mState_IX;

        private static bool _mbPatSocRead = false;
        private static DPatSocID _sPatSocMode = DPatSocID.PatPresent;
        private static string _sSocSecText;
        private static string _sPatIDText;

        public CPatientInfo(CSqlDBaseConnection ASqlConnection) : base(ASqlConnection, "d_PatientInfo")
        {
            UInt64 maskPrimary = CSqlDataTableRow.sGetMask((UInt16)DPatientInfoVars.SocSecNr);
            UInt64 maskValid = CSqlDataTableRow.sGetMaskRange((UInt16)0, (UInt16)DPatientInfoVars.NrSqlVars - 1);// mGetValidMask(false);

             mInitTableMasks(maskPrimary, maskValid);  // set primairyInsertFlags

            _mPatientID = new CEncryptedString("PatientID", DEncryptLevel.L1_Program, 32);
            _mSocSecNr = new CEncryptedString("SocSecNr", DEncryptLevel.L1_Program, 16);
            _mPatientFirstName = new CEncryptedString("PatientFirstName", DEncryptLevel.L1_Program, 32);
            _mPatientMiddleName = new CEncryptedString("PatientMiddleName", DEncryptLevel.L1_Program, 16);
            _mPatientLastName = new CEncryptedString("PatientLastName", DEncryptLevel.L1_Program, 32);
            _mPatientDateOfBirth = new CEncryptedInt("PatientDateOfBirth", DEncryptLevel.L1_Program);
            _mPatientAddress = new CEncryptedString("PatientAddress", DEncryptLevel.L1_Program, 128);
            _mPatientZip = new CEncryptedString("PatientZip", DEncryptLevel.L1_Program, 16);
            _mPatientHouseNumber = new CEncryptedString("mPatientHouseNumber", DEncryptLevel.L1_Program, 8);
            _mPatientPhone1 = new CEncryptedString("PatientPhone1", DEncryptLevel.L1_Program, 16);
            _mPatientPhone2 = new CEncryptedString("PatientPhone2", DEncryptLevel.L1_Program, 16);
            _mPatientCell = new CEncryptedString("PatientCell", DEncryptLevel.L1_Program, 16);
            _mPatientEmail = new CEncryptedString("PatientEmail", DEncryptLevel.L1_Program, 64);
            _mPatientSkype = new CEncryptedString("PatientSkype", DEncryptLevel.L1_Program, 64); 
            _mHistoryStringSet = new CStringSet();
            _mAllergiesStringSet = new CStringSet();
            _mMedicationStringSet = new CStringSet();
            _mSymptomsStringSet = new CStringSet();

            mClear();
        }

        public string mGetPatientIDValue()
        {
            return _mPatientID.mDecrypt();
        }
        public bool mbIsPatientIDEmpty()
        {
            return _mPatientID.mbIsEmpty();
        }
        public bool mbIsPatientIDNotEmpty()
        {
            return _mPatientID.mbNotEmpty();
        }
        public bool mbIsPatientIDEqual( string AValue)
        {
            return _mPatientID.mDecrypt() == AValue;
        }
        public bool mbSetPatientIDValue(CEncryptedString AEncryptedValue)
        {
            return _mPatientID.mbCopyFrom(AEncryptedValue);
        }
        public bool mbSetPatientIDValue(CPatientInfo APatient)
        {
            return _mPatientID.mbCopyFrom(APatient._mPatientID);
        }
        public bool mbSetPatientIDValue(String AValue)
        {
            return _mPatientID.mbEncrypt(AValue);
        }

        public override CSqlDataTableRow mCreateNewRow()
        {
            return new CPatientInfo(mGetSqlConnection());
        }
        public override bool mbCopyTo(CSqlDataTableRow ATo) // derived  public override void mbCopyTo() must call base.mbCopyTo(ATo)
        {
            bool bOk = false;

            if (ATo != null)
            {
                CPatientInfo to = ATo as CPatientInfo;

                if (to != null)
                {
                    bOk = base.mbCopyTo(ATo);
                    _mPatientID.mbCopyTo(ref to._mPatientID);
                    _mSocSecNr.mbCopyTo(ref to._mSocSecNr);
                    _mPatientFirstName.mbCopyTo(ref to._mPatientFirstName);
                    _mPatientMiddleName.mbCopyTo(ref to._mPatientMiddleName);
                    _mPatientLastName.mbCopyTo(ref to._mPatientLastName);
                    _mPatientDateOfBirth.mbCopyTo(ref to._mPatientDateOfBirth);
                    to._mPatientBirthYear = _mPatientBirthYear;
                    to._mPatientGender_IX = _mPatientGender_IX;
                    to._mPatientRace_IX = _mPatientRace_IX;
                    to._mPatientHeightM = _mPatientHeightM;
                    to._mPatientWeightKg = _mPatientWeightKg;
                    _mPatientAddress.mbCopyTo(ref to._mPatientAddress);
                    _mPatientZip.mbCopyTo(ref to._mPatientZip);
                    _mPatientHouseNumber.mbCopyTo(ref to._mPatientHouseNumber);
                    to._mPatientCity = _mPatientCity;
                    to._mPatientState = _mPatientState;
                    to._mPatientCountryCode = _mPatientCountryCode;
                    _mPatientPhone1.mbCopyTo(ref to._mPatientPhone1);
                    _mPatientPhone2.mbCopyTo(ref to._mPatientPhone2);
                    _mPatientCell.mbCopyTo(ref to._mPatientCell);
                    _mPatientEmail.mbCopyTo(ref to._mPatientEmail);
                    _mPatientSkype.mbCopyTo(ref to._mPatientSkype);
                    _mHistoryStringSet.mbCopyTo(ref to._mHistoryStringSet);
                    _mAllergiesStringSet.mbCopyTo(ref to._mAllergiesStringSet);
                    _mMedicationStringSet.mbCopyTo(ref to._mMedicationStringSet);
                    _mSymptomsStringSet.mbCopyTo(ref to._mSymptomsStringSet);
                    to._mRemarkLabel = _mRemarkLabel;
                    to._mRemarkText = _mRemarkText;
                    to._mState_IX = _mState_IX;
                }
            }
            return bOk;
        }
        public override void mClear()
        {
            base.mClear();
            _mPatientID.mClear();
            _mSocSecNr.mClear();
            _mPatientFirstName.mClear();
            _mPatientMiddleName.mClear();
            _mPatientLastName.mClear();
            _mPatientDateOfBirth.mClear();
            _mPatientBirthYear = 0;
            _mPatientGender_IX = 0;
            _mPatientRace_IX = 0;
            _mPatientHeightM = 0.0F;
            _mPatientWeightKg = 0.0F;
            _mPatientAddress.mClear();
            _mPatientZip.mClear();
            _mPatientHouseNumber.mClear();
            _mPatientCity = "";
            _mPatientState = "";
            _mPatientCountryCode = "";
            _mPatientPhone1.mClear();
            _mPatientPhone2.mClear();
            _mPatientCell.mClear();
            _mPatientEmail.mClear();
            _mPatientSkype.mClear();
            _mHistoryStringSet.mClear();
            _mAllergiesStringSet.mClear();
            _mMedicationStringSet.mClear();
            _mSymptomsStringSet.mClear();
            _mRemarkLabel = "";
            _mRemarkText = "";
            _mState_IX = 0;
        }

        public override DSqlDataType mVarGetSet(DSqlDataCmd ACmd, UInt16 AVarIndex, ref string AVarSqlName, ref string AStringValue, out bool ArbValid)
        {
            switch ((DPatientInfoVars)AVarIndex)
            {
                case DPatientInfoVars.PatientID:
                    return mSqlGetSetEncryptedString(ref _mPatientID, "PatientID_ES", ACmd, ref AVarSqlName, ref AStringValue, 16, out ArbValid);
                case DPatientInfoVars.SocSecNr:
                    return mSqlGetSetEncryptedString(ref _mSocSecNr, "SocSecNr_ES", ACmd, ref AVarSqlName, ref AStringValue, 16, out ArbValid);
                // variables
                case DPatientInfoVars.PatientFirstName:
                    return mSqlGetSetEncryptedString(ref _mPatientFirstName, "PatientFirstName_ES", ACmd, ref AVarSqlName, ref AStringValue, 32, out ArbValid);
                case DPatientInfoVars.PatientMiddleName:
                    return mSqlGetSetEncryptedString(ref _mPatientMiddleName, "PatientMiddleName_ES", ACmd, ref AVarSqlName, ref AStringValue, 32, out ArbValid);
                case DPatientInfoVars.PatientLastName:
                    return mSqlGetSetEncryptedString(ref _mPatientLastName, "PatientLastName_ES", ACmd, ref AVarSqlName, ref AStringValue, 64, out ArbValid);
                case DPatientInfoVars.PatientDateOfBirth:
                    return mSqlGetSetEncryptedInt(ref _mPatientDateOfBirth, "PatientDateOfBirth_EI", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DPatientInfoVars.PatientBirthYear:
                    return mSqlGetSetYear(ref _mPatientBirthYear, "PatientBirthYear", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DPatientInfoVars.PatientGender_IX:
                    return mSqlGetSetUInt16(ref _mPatientGender_IX, "PatientGender_IX", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DPatientInfoVars.PatientRace_IX:
                    return mSqlGetSetUInt16(ref _mPatientRace_IX, "PatientRace_IX", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DPatientInfoVars.PatientHeightM:
                    return mSqlGetSetFloat(ref _mPatientHeightM, "PatientHeightM", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DPatientInfoVars.PatientWeightKg:
                    return mSqlGetSetFloat(ref _mPatientWeightKg, "PatientWeightKg", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DPatientInfoVars.PatientAddress:
                    return mSqlGetSetEncryptedString(ref _mPatientAddress, "PatientAddress_ES", ACmd, ref AVarSqlName, ref AStringValue, 128, out ArbValid);
                case DPatientInfoVars.PatientZip:
                    return mSqlGetSetEncryptedString(ref _mPatientZip, "PatientZip_ES", ACmd, ref AVarSqlName, ref AStringValue, 16, out ArbValid);
                case DPatientInfoVars.PatientHouseNumber:
                    return mSqlGetSetEncryptedString(ref _mPatientHouseNumber, "PatientHouseNumber_ES", ACmd, ref AVarSqlName, ref AStringValue, 16, out ArbValid);
                case DPatientInfoVars.PatientCity:
                    return mSqlGetSetString(ref _mPatientCity, "PatientCity", ACmd, ref AVarSqlName, ref AStringValue, 32, out ArbValid);
                case DPatientInfoVars.PatientState:
                    return mSqlGetSetString(ref _mPatientState, "PatientState", ACmd, ref AVarSqlName, ref AStringValue, 32, out ArbValid);
                case DPatientInfoVars.PatientCountryCode:
                    return mSqlGetSetString(ref _mPatientCountryCode, "PatientCountryCode", ACmd, ref AVarSqlName, ref AStringValue, 4, out ArbValid);
                case DPatientInfoVars.PatientPhone1:
                    return mSqlGetSetEncryptedString(ref _mPatientPhone1, "PatientPhone1_ES", ACmd, ref AVarSqlName, ref AStringValue, 16, out ArbValid);
                case DPatientInfoVars.PatientPhone2:
                    return mSqlGetSetEncryptedString(ref _mPatientPhone2, "PatientPhone2_ES", ACmd, ref AVarSqlName, ref AStringValue, 16, out ArbValid);
                case DPatientInfoVars.PatientCell:
                    return mSqlGetSetEncryptedString(ref _mPatientCell, "PatientCell_ES", ACmd, ref AVarSqlName, ref AStringValue, 16, out ArbValid);
                case DPatientInfoVars.PatientEmail:
                    return mSqlGetSetEncryptedString(ref _mPatientEmail, "PatientEmail_ES", ACmd, ref AVarSqlName, ref AStringValue, 64, out ArbValid);
                case DPatientInfoVars.PatientSkype:
                    return mSqlGetSetEncryptedString(ref _mPatientSkype, "PatientSkype_ES", ACmd, ref AVarSqlName, ref AStringValue, 64, out ArbValid);
                case DPatientInfoVars.HistoryStringSet:
                    return mSqlGetSetStringSet(ref _mHistoryStringSet, "HistoryStringSet", ACmd, ref AVarSqlName, ref AStringValue, 64, out ArbValid);
                case DPatientInfoVars.AllergiesStringSet:
                    return mSqlGetSetStringSet(ref _mAllergiesStringSet, "AllergiesStringSet", ACmd, ref AVarSqlName, ref AStringValue, 64, out ArbValid);
                case DPatientInfoVars.MedicationStringSet:
                    return mSqlGetSetStringSet(ref _mMedicationStringSet, "MedicationStringSet", ACmd, ref AVarSqlName, ref AStringValue, 64, out ArbValid);
                case DPatientInfoVars.SymptomsStringSet:
                    return mSqlGetSetStringSet(ref _mSymptomsStringSet, "SymptomsStringSet", ACmd, ref AVarSqlName, ref AStringValue, 64, out ArbValid);
                case DPatientInfoVars.RemarkLabel:
                    return mSqlGetSetString(ref _mRemarkLabel, "RemarkLabel", ACmd, ref AVarSqlName, ref AStringValue, 32, out ArbValid);
                case DPatientInfoVars.RemarkText:
                    return mSqlGetSetString(ref _mRemarkText, "RemarkText", ACmd, ref AVarSqlName, ref AStringValue, 256, out ArbValid);
                case DPatientInfoVars.State_IX:
                    return mSqlGetSetUInt16(ref _mState_IX, "State_IX", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
            }
            return base.mVarGetSet(ACmd, AVarIndex, ref AVarSqlName, ref AStringValue, out ArbValid);
        }

        public string mGetFullName()
        {
            string s = _mPatientLastName.mDecrypt();
            string m = _mPatientMiddleName.mDecrypt();
            string f = _mPatientFirstName.mDecrypt();

            if (m != null && m.Length > 0) s += ", " + m;
            if (f != null && f.Length > 0) s += ", " + f;

            return s;
        }

        public int mGetAgeYears()
        {
            int years = -1;

            if( _mPatientDateOfBirth.mbNotZero())
            {
                years = CProgram.sCalcAgeYears((UInt32)_mPatientDateOfBirth.mDecrypt(), DateTime.Now);
                if( years > 116 )   // 117 at 1-1-1900 (default Sirona)
                {
                    years = -1;
                }
            }
            return years;
        }

        /*public DSqlDataType mSqlGetSetStringSet(ref CStringSet ArVar, string AName, DSqlDataCmd ACmd, ref string ArSqlName, ref string ArStringValue, UInt16 ASizeLimit)
        {
            DSqlDataType dataType = DSqlDataType.SqlError;

            if( ArVar != null )
            {
                string s = ArVar.mGetSet();
                dataType = mSqlGetSetString(ref s, AName, ACmd, ref ArSqlName, ref ArStringValue, ASizeLimit);
                ArVar.mSetSet( s );
            }
            return dataType;
        }
*/
        public static string sGetGenderString( DGender AGender)
        {
            switch( AGender )
            {
                case DGender.Unknown:    return "";
                case DGender.Male:       return "Male";
                case DGender.Female:     return "Female";
            }
            return "?Gender?";
        }
        public static bool sbParseGetGenderString( string AString, ref DGender ArGender)
        {
            bool bOk = false;

            for( DGender i = DGender.Unknown; i < DGender.NrOfGenders; ++i )
            {
                string s = sGetGenderString(i);

                if( s.CompareTo( AString) == 0)
                {
                    ArGender = i;
                    bOk = true;
                    break;
                }
            }
            return bOk;
        }
        public static void sFillCombobox( ComboBox ACombobox, UInt16 ASelected )
        {
            if( ACombobox != null )
            {
                string s, selected = "";
                ACombobox.Items.Clear();

                for (DGender i = DGender.Unknown; i < DGender.NrOfGenders; ++i)
                {
                    s = sGetGenderString(i);
                    ACombobox.Items.Add(s);
                    if( (UInt16)i == ASelected)
                    {
                        selected = s;
                    }
                }
                ACombobox.Text = selected;
            }
        }
        public string mGetNameAbrev()
        {
            string name = "";

            if( _mPatientFirstName != null && _mPatientFirstName.mbNotEmpty() )
            {
                name += Char.ToUpper(_mPatientFirstName.mDecrypt()[0]);
            }
            if (_mPatientMiddleName != null && _mPatientMiddleName.mbNotEmpty())
            {
                name += Char.ToUpper(_mPatientMiddleName.mDecrypt()[0]);
            }
            if (_mPatientLastName != null && _mPatientLastName.mbNotEmpty())
            {
                name += Char.ToUpper(_mPatientLastName.mDecrypt()[0]);
            }

            return name;
        }
        public static void sSetPatientSocID( int AMode, string APatientIdText, string ASocSecText)
        {
            _sPatSocMode = AMode <= (int)DPatSocID.PatPresent? DPatSocID.PatPresent : ( AMode > (int)DPatSocID.PatID ? DPatSocID.PatID : DPatSocID.SocSec);
            _sSocSecText = ASocSecText == null || ASocSecText.Length == 0 ? "Soc. Sec. ID" : ASocSecText;
            _sPatIDText = APatientIdText == null || APatientIdText.Length == 0 ? "Patient ID" : APatientIdText;
        }

        public static string _sGetPatientIDText()
        {
            return _sPatIDText;
        }
        public static string _sGetSocSecText()
        {
            return _sSocSecText;
        }
        public static DPatSocID _sGetPatSocMode()
        {
            return _sPatSocMode;
        }
        public static string sGetPatientSocIDText()
        {
            string s = "?";
            switch (_sPatSocMode)
            {
                default:
                case DPatSocID.PatPresent:
                    s = _sPatIDText;
                    break;
                case DPatSocID.SocSec:
                    s = _sSocSecText;
                    break;
                case DPatSocID.PatID:
                    s = _sPatIDText;
                    break;
            }
            return s;
        }
        public string mGetPatientSocIDText()
        {
            string s = "?";
            switch (_sPatSocMode)
            {
                default:
                case DPatSocID.PatPresent:
                    s = _mPatientID == null || _mPatientID.mbIsEmpty() ? _sSocSecText : _sPatIDText;
                    break;
                case DPatSocID.SocSec:
                    s = _sSocSecText;
                    break;
                case DPatSocID.PatID:
                    s = _sPatIDText;
                    break;
            }
            return s;
        }
        public string mGetPatientSocIDValue( bool AbShowSocUsed )
        {
            string s = "?";
            switch (_sPatSocMode)
            {
                default:
                case DPatSocID.PatPresent:
                    s = _mPatientID == null || _mPatientID.mbIsEmpty() ? (AbShowSocUsed ? "#": "" ) + _mSocSecNr.mDecrypt() : _mPatientID.mDecrypt();
                    break;
                case DPatSocID.SocSec:
                    s = (AbShowSocUsed ? "#" : "") + _mSocSecNr.mDecrypt();
                    break;
                case DPatSocID.PatID:
                    s = _mPatientID.mDecrypt();
                    break;
            }
            return s;
        }
        public static void sFillPrintPatientName(bool AbAnonymize, CPatientInfo APatient, CRecordMit ARecord, out string ArText1, out string ArText2)
        {
            string text1 = "";
            string text2 = "";

            if (false == AbAnonymize)
            {
                string fullName = APatient != null ? APatient.mGetFullName()
                    : (ARecord != null ? ARecord.mPatientTotalName.mDecrypt() : "");
                /*                int pos = fullName.IndexOf(',');
                                string firstName = "", lastName = fullName;

                                if (pos > 0)
                                {
                                    lastName = fullName.Substring(0, pos);
                                    firstName = fullName.Substring(pos + 1);
                                    if (firstName.StartsWith(", "))
                                    {
                                        firstName = firstName.Substring(2);
                                    }
                                }
                                text1 = lastName;
                                text2 = firstName;
                                */
                text1 = fullName;
            }
            ArText1 = text1;
            ArText2 = text2;
        }
        public static void sFillPrintPatientName(bool AbAnonymize, CPatientInfo APatient, CRecordMit ARecord, Label ALabel1, Label ALabel2)
        {
            string text1, text2;

            sFillPrintPatientName(AbAnonymize, APatient, ARecord, out text1, out text2);

            if (ALabel1 != null) ALabel1.Text = text1;
            if (ALabel2 != null)
            {
                ALabel2.Visible = false;
                ALabel2.Text = text2;
            }
        }
        public static void sFillPrintPatientDOB(bool AbAnonymize, CPatientInfo APatient, CRecordMit ARecord, out string ArText1, out string ArText2)
        {
            string text1 = "";
            string text2 = "";

            int ageYears = APatient != null ? APatient.mGetAgeYears()
                : (ARecord != null ? ARecord.mGetAgeYears() : -1);

            text2 = ageYears >= 0 ? ageYears.ToString() : "?";

            if (APatient != null && APatient._mPatientGender_IX > 0)
            {
                text2 += "  "/*", "*/ + CPatientInfo.sGetGenderString((DGender)APatient._mPatientGender_IX);
            }
            if (false == AbAnonymize && ageYears >= 0)
            {
                text1 = APatient != null ? CProgram.sDateToString(APatient._mPatientDateOfBirth.mDecryptToDate()) 
                        : (ARecord != null ? CProgram.sDateToString(ARecord.mGetBirthDate()): ""); //  "06/08/1974";
          }
            ArText1 = text1;
            ArText2 = text2;
        }
        public static void sFillPrintPatientDOB(bool AbAnonymize, CPatientInfo APatient, CRecordMit ARecord, Label ALabel1, Label ALabel2)
        {
            string text1, text2;
            sFillPrintPatientDOB(AbAnonymize, APatient, ARecord, out text1, out text2);

            if (ALabel1 != null) ALabel1.Text = text1;
            if (ALabel2 != null) ALabel2.Text = text2;
        }
        public static void sFillPrintPatientAddress(bool AbAnonymize, CPatientInfo APatient, out string ArText1, out string ArText2, out string ArText3)
        {
            string text1 = "";
            string text2 = "";
            string text3 = "";
            
            if (false == AbAnonymize && APatient != null)
            {
                bool bImperial = CProgram.sbGetImperial();

                if( bImperial)
                {
                    string stateCode = CDvtmsData.sGetStateCode(APatient._mPatientState);
                    bool bState = stateCode != null && stateCode.Length > 0;
                    if( bState )
                    {
                        stateCode += " ";                      
                    }
                    else
                    {
                        text3 = APatient._mPatientState;
                    }

                    text1 = APatient._mPatientHouseNumber.mDecrypt() + " " + APatient._mPatientAddress.mDecrypt();
                    text2 = APatient._mPatientCity + ", " + stateCode + APatient._mPatientZip.mDecrypt();

                    if (text3 != null && text3.Length > 0 && APatient._mPatientCountryCode != null && APatient._mPatientCountryCode.Length > 0 ) text3 += ", ";
//                    text3 += CDvtmsData.sGetCountryLabel(APatient._mPatientCountryCode);
                    text3 += APatient._mPatientCountryCode;
                }
                else
                {
                    text1 = APatient._mPatientAddress.mDecrypt() + " " + APatient._mPatientHouseNumber.mDecrypt();
                    text2 = APatient._mPatientZip.mDecrypt() + "  " + APatient._mPatientCity;
                    text3 = APatient._mPatientState;
                    if (text3 != null && text3.Length > 0 && APatient._mPatientCountryCode != null && APatient._mPatientCountryCode.Length > 0) text3 += ", ";
                    text3 += APatient._mPatientCountryCode;
                }
            }
            ArText1 = text1;
            ArText2 = text2;
            ArText3 = text3;
        }
        public static void sFillPrintPatientAddress(bool AbAnonymize, CPatientInfo APatient, Label ALabel1, Label ALabel2, Label ALabel3)
        {
            string text1, text2, text3;

            sFillPrintPatientAddress(AbAnonymize, APatient, out text1, out text2, out text3);

            if (ALabel1 != null) ALabel1.Text = text1;
            if (ALabel2 != null) ALabel2.Text = text2;
            if (ALabel3 != null) ALabel3.Text = text3;
        }

    }
}
