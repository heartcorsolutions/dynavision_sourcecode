﻿namespace EventboardEntryForms
{
    partial class CicdCodeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelAll = new System.Windows.Forms.Panel();
            this.panelMain = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.checkedListBoxAII = new System.Windows.Forms.CheckedListBox();
            this.label8 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.checkedListBoxSC = new System.Windows.Forms.CheckedListBox();
            this.label9 = new System.Windows.Forms.Label();
            this.panel21 = new System.Windows.Forms.Panel();
            this.checkedListBoxMVD = new System.Windows.Forms.CheckedListBox();
            this.label10 = new System.Windows.Forms.Label();
            this.panel19 = new System.Windows.Forms.Panel();
            this.checkedListBoxNVD = new System.Windows.Forms.CheckedListBox();
            this.label7 = new System.Windows.Forms.Label();
            this.panel15 = new System.Windows.Forms.Panel();
            this.checkedListBoxHF = new System.Windows.Forms.CheckedListBox();
            this.label5 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.checkedListBoxHT = new System.Windows.Forms.CheckedListBox();
            this.label6 = new System.Windows.Forms.Label();
            this.panel20 = new System.Windows.Forms.Panel();
            this.checkedListBoxCP = new System.Windows.Forms.CheckedListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel17 = new System.Windows.Forms.Panel();
            this.checkedListBoxCA = new System.Windows.Forms.CheckedListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel16 = new System.Windows.Forms.Panel();
            this.checkedListBoxAFF = new System.Windows.Forms.CheckedListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.checkedListBoxAHR = new System.Windows.Forms.CheckedListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.checkedListBoxMCTr = new System.Windows.Forms.CheckedListBox();
            this.checkedListBoxMCT = new System.Windows.Forms.Label();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.labelMaxNr = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.buttonSelect = new System.Windows.Forms.Button();
            this.panelF = new System.Windows.Forms.Panel();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.panelAll.SuspendLayout();
            this.panelMain.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panelF.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelAll
            // 
            this.panelAll.AutoScroll = true;
            this.panelAll.AutoSize = true;
            this.panelAll.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelAll.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panelAll.Controls.Add(this.panelMain);
            this.panelAll.Controls.Add(this.panel8);
            this.panelAll.Controls.Add(this.panel13);
            this.panelAll.Controls.Add(this.panel3);
            this.panelAll.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelAll.Font = new System.Drawing.Font("Arial", 10F);
            this.panelAll.Location = new System.Drawing.Point(0, 0);
            this.panelAll.Margin = new System.Windows.Forms.Padding(4);
            this.panelAll.Name = "panelAll";
            this.panelAll.Size = new System.Drawing.Size(1650, 1008);
            this.panelAll.TabIndex = 130;
            // 
            // panelMain
            // 
            this.panelMain.AutoScroll = true;
            this.panelMain.AutoSize = true;
            this.panelMain.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelMain.Controls.Add(this.panel10);
            this.panelMain.Controls.Add(this.panel11);
            this.panelMain.Controls.Add(this.panel7);
            this.panelMain.Controls.Add(this.panel6);
            this.panelMain.Controls.Add(this.panel5);
            this.panelMain.Controls.Add(this.panel4);
            this.panelMain.Controls.Add(this.panel2);
            this.panelMain.Controls.Add(this.panel18);
            this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMain.Location = new System.Drawing.Point(0, 20);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(1650, 923);
            this.panelMain.TabIndex = 25;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.checkedListBoxAII);
            this.panel10.Controls.Add(this.label8);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel10.Location = new System.Drawing.Point(1211, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(425, 923);
            this.panel10.TabIndex = 6;
            // 
            // checkedListBoxAII
            // 
            this.checkedListBoxAII.CheckOnClick = true;
            this.checkedListBoxAII.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkedListBoxAII.FormattingEnabled = true;
            this.checkedListBoxAII.HorizontalScrollbar = true;
            this.checkedListBoxAII.Items.AddRange(new object[] {
            ">Mockup list",
            "I21.01 ST elevation (STEMI) myocardial infarction involving left main coronary ar" +
                "tery",
            "I21.02 ST elevation (STEMI) myocardial infarction involving left anterior descend" +
                "ing coronary artery",
            "I21.09 ST elevation (STEMI) myocardial infarction involving other coronary artery" +
                " of anterior wall\rI",
            "21.11 ST elevation (STEMI) myocardial infarction involving right coronary artery",
            "I21.19 ST elevation (STEMI) myocardial infarction involving other coronary artery" +
                " of inferior wall",
            "I21.21 ST elevation (STEMI) myocardial infarction involving left circumflex coron" +
                "ary artery",
            "I21.29 ST elevation (STEMI) myocardial infarction involving other sites",
            "I21.3 ST elevation (STEMI) myocardial infarction of unspecified site",
            "I21.4  Non-ST elevation (NSTEMI) myocardial infarction",
            "I22.0 Subsequent ST elevation (STEMI) myocardial infarction of anterior wall",
            "I22.1 Subsequent ST elevation (STEMI) myocardial infarction of inferior wall\rI",
            "22.2 Subsequent non-ST elevation (NSTEMI) myocardial infarction",
            "I22.8 Subsequent ST elevation (STEMI) myocardial infarction of other sites",
            "I22.9 Subsequent ST elevation (STEMI) myocardial infarction of unspecified site",
            "I23.0 Hemopericardium as current complication following acute myocardial infarcti" +
                "on",
            "I23.1 Atrial septal defect as current complication following acute myocardial inf" +
                "arction",
            "I23.2 Ventricular septal defect as current complication following acute myocardia" +
                "l infarction",
            "I23.3 Rupture of cardiac wall without hemopericardium as current complication fol" +
                "lowing acute myocardial infarction",
            "I23.4 Rupture of chordae tendineae as current complication following acute myocar" +
                "dial infarction",
            "I23.5 Rupture of papillary muscle as current complication following acute myocard" +
                "ial infarction",
            "I23.6 Thrombosis of atrium, auricular appendage, and ventricle as current complic" +
                "ations following acute myocardial infarction",
            "I23.7 Postinfarction angina\rI23.8    Other current complications following acute " +
                "myocardial infarction",
            "I24.0 Acute coronary thrombosis not resulting in myocardial infarction",
            "I24.1 Dressler\'s syndrome",
            "I24.8 Other forms of acute ischemic heart disease",
            "I24.9 Acute ischemic heart disease, unspecified",
            "I25.10 Atherosclerotic heart disease of native coronary artery without angina pec" +
                "toris",
            "I25.110 Atherosclerotic heart disease of native coronary artery with unstable ang" +
                "ina pectoris",
            "I25.111 Atherosclerotic heart disease of native coronary artery with angina pecto" +
                "ris with documented spasm",
            "I25.118 Atherosclerotic heart disease of native coronary artery with other forms " +
                "of angina pectoris",
            "I25.119 Atherosclerotic heart disease of native coronary artery with unspecified " +
                "angina pectoris",
            "I25.2 Old myocardial infarction",
            "I25.3 Aneurysm of heart",
            "I25.41 Coronary artery aneurysm",
            "I25.42 Coronary artery dissection",
            "I25.5 Ischemic cardiomyopathy",
            "I25.6 Silent myocardial ischemia",
            "I25.700  Atherosclerosis of coronary artery bypass graft(s), unspecified, with un" +
                "stable angina pectoris",
            "I25.701  Atherosclerosis of coronary artery bypass graft(s), unspecified, with an" +
                "gina pectoris with documented spasm",
            "I25.708  Atherosclerosis of coronary artery bypass graft(s), unspecified, with ot" +
                "her forms of angina pectoris",
            "I25.709  Atherosclerosis of coronary artery bypass graft(s), unspecified, with un" +
                "specified angina pectoris",
            "I25.710  Atherosclerosis of autologous vein coronary artery bypass graft(s) with " +
                "unstable angina pectoris",
            "I25.711  Atherosclerosis of autologous vein coronary artery bypass graft(s) with " +
                "angina pectoris with documented spasm",
            "I25.718  Atherosclerosis of autologous vein coronary artery bypass graft(s) with " +
                "other forms of angina pectoris",
            "I25.719  Atherosclerosis of autologous vein coronary artery bypass graft(s) with " +
                "unspecified angina pectoris",
            "I25.720  Atherosclerosis of autologous artery coronary artery bypass graft(s) wit" +
                "h unstable angina pectoris",
            "I25.721  Atherosclerosis of autologous artery coronary artery bypass graft(s) wit" +
                "h angina pectoris with documented spasm",
            "I25.728  Atherosclerosis of autologous artery coronary artery bypass graft(s) wit" +
                "h other forms of angina pectoris",
            "I25.729  Atherosclerosis of autologous artery coronary artery bypass graft(s) wit" +
                "h unspecified angina pectoris",
            "I25.730  Atherosclerosis of nonautologous biological coronary artery bypass graft" +
                "(s) with unstable angina pectoris",
            "I25.731  Atherosclerosis of nonautologous biological coronary artery bypass graft" +
                "(s) with angina pectoris with documented spasm",
            "I25.738  Atherosclerosis of nonautologous biological coronary artery bypass graft" +
                "(s) with other forms of angina pectoris",
            "I25.739  Atherosclerosis of nonautologous biological coronary artery bypass graft" +
                "(s) with unspecified angina pectoris",
            "I25.750  Atherosclerosis of native coronary artery of transplanted heart with uns" +
                "table angina",
            "I25.751  Atherosclerosis of native coronary artery of transplanted heart with ang" +
                "ina pectoris with documented spasm",
            "I25.758  Atherosclerosis of native coronary artery of transplanted heart with oth" +
                "er forms of angina pectoris",
            "I25.759  Atherosclerosis of native coronary artery of transplanted heart with uns" +
                "pecified angina pectoris",
            "I25.760  Atherosclerosis of bypass graft of coronary artery of transplanted heart" +
                " with unstable angina",
            "I25.761  Atherosclerosis of bypass graft of coronary artery of transplanted heart" +
                " with angina pectoris with documented spasm",
            "I25.768  Atherosclerosis of bypass graft of coronary artery of transplanted heart" +
                " with other forms of angina pectoris",
            "I25.769  Atherosclerosis of bypass graft of coronary artery of transplanted heart" +
                " with unspecified angina pectoris",
            "I25.790  Atherosclerosis of other coronary artery bypass graft(s) with unstable a" +
                "ngina pectoris",
            "I25.791  Atherosclerosis of other coronary artery bypass graft(s) with angina pec" +
                "toris with documented spasm",
            "I25.798  Atherosclerosis of other coronary artery bypass graft(s) with other form" +
                "s of angina pectoris",
            "I25.799  Atherosclerosis of other coronary artery bypass graft(s) with unspecifie" +
                "d angina pectoris",
            "I25.810  Atherosclerosis of coronary artery bypass graft(s) without angina pector" +
                "is",
            "I25.811  Atherosclerosis of native coronary artery of transplanted heart without " +
                "angina pectoris",
            "I25.812  Atherosclerosis of bypass graft of coronary artery of transplanted heart" +
                " without angina pectoris",
            "I25.82  Chronic total occlusion of coronary artery",
            "I25.83  Coronary atherosclerosis due to lipid rich plaque",
            "I25.84  Coronary atherosclerosis due to calcified coronary lesion",
            "I25.89  Other forms of chronic ischemic heart disease",
            "I25.9  Chronic ischemic heart disease, unspecified"});
            this.checkedListBoxAII.Location = new System.Drawing.Point(0, 19);
            this.checkedListBoxAII.Margin = new System.Windows.Forms.Padding(4);
            this.checkedListBoxAII.Name = "checkedListBoxAII";
            this.checkedListBoxAII.Size = new System.Drawing.Size(425, 904);
            this.checkedListBoxAII.TabIndex = 17;
            // 
            // label8
            // 
            this.label8.Dock = System.Windows.Forms.DockStyle.Top;
            this.label8.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(0, 0);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(425, 19);
            this.label8.TabIndex = 8;
            this.label8.Text = "Selected Atherosclerosis, Ischemia, and Infarction";
            // 
            // panel11
            // 
            this.panel11.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel11.Location = new System.Drawing.Point(1201, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(10, 923);
            this.panel11.TabIndex = 5;
            // 
            // panel7
            // 
            this.panel7.AutoScroll = true;
            this.panel7.Controls.Add(this.checkedListBoxSC);
            this.panel7.Controls.Add(this.label9);
            this.panel7.Controls.Add(this.panel21);
            this.panel7.Controls.Add(this.checkedListBoxMVD);
            this.panel7.Controls.Add(this.label10);
            this.panel7.Controls.Add(this.panel19);
            this.panel7.Controls.Add(this.checkedListBoxNVD);
            this.panel7.Controls.Add(this.label7);
            this.panel7.Controls.Add(this.panel15);
            this.panel7.Controls.Add(this.checkedListBoxHF);
            this.panel7.Controls.Add(this.label5);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel7.Location = new System.Drawing.Point(852, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(349, 923);
            this.panel7.TabIndex = 4;
            // 
            // checkedListBoxSC
            // 
            this.checkedListBoxSC.CheckOnClick = true;
            this.checkedListBoxSC.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkedListBoxSC.FormattingEnabled = true;
            this.checkedListBoxSC.HorizontalScrollbar = true;
            this.checkedListBoxSC.Items.AddRange(new object[] {
            ">Mockup list",
            "R55 Syncope and Collapse"});
            this.checkedListBoxSC.Location = new System.Drawing.Point(0, 855);
            this.checkedListBoxSC.Margin = new System.Windows.Forms.Padding(4);
            this.checkedListBoxSC.Name = "checkedListBoxSC";
            this.checkedListBoxSC.Size = new System.Drawing.Size(349, 58);
            this.checkedListBoxSC.TabIndex = 144;
            // 
            // label9
            // 
            this.label9.Dock = System.Windows.Forms.DockStyle.Top;
            this.label9.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(0, 837);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(349, 18);
            this.label9.TabIndex = 143;
            this.label9.Text = "Syncope and Collapse";
            // 
            // panel21
            // 
            this.panel21.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel21.Location = new System.Drawing.Point(0, 817);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(349, 20);
            this.panel21.TabIndex = 142;
            // 
            // checkedListBoxMVD
            // 
            this.checkedListBoxMVD.CheckOnClick = true;
            this.checkedListBoxMVD.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkedListBoxMVD.FormattingEnabled = true;
            this.checkedListBoxMVD.HorizontalScrollbar = true;
            this.checkedListBoxMVD.Items.AddRange(new object[] {
            ">Mockup list",
            "I34.0\tNonrheumatic mitral (valve) insufficiency",
            "I34.1\tNonrheumatic mitral (valve) prolapse",
            "I34.2\tNonrheumatic mitral (valve) stenosis",
            "I34.8\tOther nonrheumatic mitral valve disorders",
            "I34.9\tNonrheumatic mitral valve disorder, unspecified"});
            this.checkedListBoxMVD.Location = new System.Drawing.Point(0, 597);
            this.checkedListBoxMVD.Margin = new System.Windows.Forms.Padding(4);
            this.checkedListBoxMVD.Name = "checkedListBoxMVD";
            this.checkedListBoxMVD.Size = new System.Drawing.Size(349, 220);
            this.checkedListBoxMVD.TabIndex = 141;
            // 
            // label10
            // 
            this.label10.Dock = System.Windows.Forms.DockStyle.Top;
            this.label10.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(0, 579);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(349, 18);
            this.label10.TabIndex = 140;
            this.label10.Text = "Mitral Valve Disorders";
            // 
            // panel19
            // 
            this.panel19.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel19.Location = new System.Drawing.Point(0, 559);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(349, 20);
            this.panel19.TabIndex = 139;
            // 
            // checkedListBoxNVD
            // 
            this.checkedListBoxNVD.CheckOnClick = true;
            this.checkedListBoxNVD.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkedListBoxNVD.FormattingEnabled = true;
            this.checkedListBoxNVD.HorizontalScrollbar = true;
            this.checkedListBoxNVD.Items.AddRange(new object[] {
            ">Mockup list",
            "I35.0 Nonrheumatic aortic (valve) stenosis",
            "I35.1 Nonrheumatic aortic (valve) insufficiency",
            "I35.2 Nonrheumatic aortic (valve) stenosis with insufficiency",
            "I35.8 Other nonrheumatic aortic valve disorders",
            "I359 Nonrheumatic aortic valve disorder, unspecified",
            "I34.0 Nonrheumatic mitral (valve) insufficiency",
            "I34.1 Nonrheumatic mitral (valve) prolapse",
            "I34.2 Nonrheumatic mitral (valve) stenosis",
            "I34.8 Other nonrheumatic mitral valve disorders",
            "I34.9 Nonrheumatic mitral valve disorder, unspecified"});
            this.checkedListBoxNVD.Location = new System.Drawing.Point(0, 357);
            this.checkedListBoxNVD.Margin = new System.Windows.Forms.Padding(4);
            this.checkedListBoxNVD.Name = "checkedListBoxNVD";
            this.checkedListBoxNVD.Size = new System.Drawing.Size(349, 202);
            this.checkedListBoxNVD.TabIndex = 136;
            // 
            // label7
            // 
            this.label7.Dock = System.Windows.Forms.DockStyle.Top;
            this.label7.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(0, 338);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(349, 19);
            this.label7.TabIndex = 135;
            this.label7.Text = "Nonrheumatic Valve Disorders";
            // 
            // panel15
            // 
            this.panel15.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel15.Location = new System.Drawing.Point(0, 311);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(349, 27);
            this.panel15.TabIndex = 134;
            // 
            // checkedListBoxHF
            // 
            this.checkedListBoxHF.CheckOnClick = true;
            this.checkedListBoxHF.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkedListBoxHF.FormattingEnabled = true;
            this.checkedListBoxHF.HorizontalScrollbar = true;
            this.checkedListBoxHF.Items.AddRange(new object[] {
            ">Mockup list",
            "I50.1 Left ventricular failure",
            "I50.20 Unspecified systolic (congestive) heart failure",
            "I50.21 Acute systolic (congestive) heart failure",
            "I50.22 Chronic systolic (congestive) heart failure",
            "I50.23 Acute on chronic systolic (congestive) heart failure",
            "I50.30 Unspecified diastolic (congestive) heart failure",
            "I50.31 Acute diastolic (congestive) heart failure",
            "I50.32 Chronic diastolic (congestive) heart failure",
            "I50.33 Acute on chronic diastolic (congestive) heart failure",
            "I50.40 Unspecified combined systolic (congestive) and diastolic (congestive) hear" +
                "t failure",
            "I50.41 Acute combined systolic (congestive) and diastolic (congestive) heart fail" +
                "ure",
            "I50.42 Chronic combined systolic (congestive) and diastolic (congestive) heart fa" +
                "ilure",
            "I50.43 Acute on chronic combined systolic (congestive) and diastolic (congestive)" +
                " heart failure",
            "I50.9 Heart failure, unspecified"});
            this.checkedListBoxHF.Location = new System.Drawing.Point(0, 19);
            this.checkedListBoxHF.Margin = new System.Windows.Forms.Padding(4);
            this.checkedListBoxHF.Name = "checkedListBoxHF";
            this.checkedListBoxHF.Size = new System.Drawing.Size(349, 292);
            this.checkedListBoxHF.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(349, 19);
            this.label5.TabIndex = 5;
            this.label5.Text = "Heart Failure";
            // 
            // panel6
            // 
            this.panel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel6.Location = new System.Drawing.Point(842, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(10, 923);
            this.panel6.TabIndex = 3;
            // 
            // panel5
            // 
            this.panel5.AutoScroll = true;
            this.panel5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel5.Controls.Add(this.checkedListBoxHT);
            this.panel5.Controls.Add(this.label6);
            this.panel5.Controls.Add(this.panel20);
            this.panel5.Controls.Add(this.checkedListBoxCP);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.panel17);
            this.panel5.Controls.Add(this.checkedListBoxCA);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.panel16);
            this.panel5.Controls.Add(this.checkedListBoxAFF);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.panel12);
            this.panel5.Controls.Add(this.checkedListBoxAHR);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel5.Location = new System.Drawing.Point(476, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(366, 923);
            this.panel5.TabIndex = 2;
            // 
            // checkedListBoxHT
            // 
            this.checkedListBoxHT.CheckOnClick = true;
            this.checkedListBoxHT.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkedListBoxHT.FormattingEnabled = true;
            this.checkedListBoxHT.HorizontalScrollbar = true;
            this.checkedListBoxHT.Items.AddRange(new object[] {
            ">Mockup list",
            "I10 Essential (primary) hypertension"});
            this.checkedListBoxHT.Location = new System.Drawing.Point(0, 855);
            this.checkedListBoxHT.Margin = new System.Windows.Forms.Padding(4);
            this.checkedListBoxHT.Name = "checkedListBoxHT";
            this.checkedListBoxHT.Size = new System.Drawing.Size(366, 58);
            this.checkedListBoxHT.TabIndex = 145;
            // 
            // label6
            // 
            this.label6.Dock = System.Windows.Forms.DockStyle.Top;
            this.label6.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(0, 837);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(366, 18);
            this.label6.TabIndex = 144;
            this.label6.Text = "Hypertension";
            // 
            // panel20
            // 
            this.panel20.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel20.Location = new System.Drawing.Point(0, 817);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(366, 20);
            this.panel20.TabIndex = 143;
            // 
            // checkedListBoxCP
            // 
            this.checkedListBoxCP.CheckOnClick = true;
            this.checkedListBoxCP.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkedListBoxCP.FormattingEnabled = true;
            this.checkedListBoxCP.HorizontalScrollbar = true;
            this.checkedListBoxCP.Items.AddRange(new object[] {
            ">Mockup list",
            "I20.0 Unstable angina",
            "I20.1 Angina pectoris with documented spasm",
            "I20.8 Other forms of angina pectoris",
            "I20.9 Angina pectoris, unspecified",
            "R07.1 Chest pain on breathing",
            "R07.2 Precordial pain",
            "R07.8 Other chest pain",
            "R07.81 Pleurodynia",
            "R07.82 Intercostal pain",
            "R07.89 Other chest pain",
            "R07.9 Chest pain, unspecified"});
            this.checkedListBoxCP.Location = new System.Drawing.Point(0, 597);
            this.checkedListBoxCP.Margin = new System.Windows.Forms.Padding(4);
            this.checkedListBoxCP.Name = "checkedListBoxCP";
            this.checkedListBoxCP.Size = new System.Drawing.Size(366, 220);
            this.checkedListBoxCP.TabIndex = 142;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(0, 579);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(366, 18);
            this.label4.TabIndex = 141;
            this.label4.Text = "Chest Pain";
            // 
            // panel17
            // 
            this.panel17.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel17.Location = new System.Drawing.Point(0, 559);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(366, 20);
            this.panel17.TabIndex = 140;
            // 
            // checkedListBoxCA
            // 
            this.checkedListBoxCA.CheckOnClick = true;
            this.checkedListBoxCA.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkedListBoxCA.FormattingEnabled = true;
            this.checkedListBoxCA.HorizontalScrollbar = true;
            this.checkedListBoxCA.Items.AddRange(new object[] {
            ">Mockup list",
            "I49.01 Ventricular fibrillation",
            "I49.02 Ventricular flutter",
            "I49.1 Atrial premature depolarization",
            "I49.2 Junctional premature depolarization",
            "I49.3 Ventricular premature depolarization",
            "I49.40 Unspecified premature depolarization",
            "I49.49 Other premature depolarization",
            "I49.5 Sick sinus syndrome",
            "I49.8 Other specified cardiac arrhythmias",
            "I49.9 Cardiac arrhythmia, unspecified"});
            this.checkedListBoxCA.Location = new System.Drawing.Point(0, 357);
            this.checkedListBoxCA.Margin = new System.Windows.Forms.Padding(4);
            this.checkedListBoxCA.Name = "checkedListBoxCA";
            this.checkedListBoxCA.Size = new System.Drawing.Size(366, 202);
            this.checkedListBoxCA.TabIndex = 139;
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(0, 338);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(366, 19);
            this.label3.TabIndex = 137;
            this.label3.Text = "Cardiac Arrhythmias (Other)";
            // 
            // panel16
            // 
            this.panel16.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel16.Location = new System.Drawing.Point(0, 318);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(366, 20);
            this.panel16.TabIndex = 138;
            // 
            // checkedListBoxAFF
            // 
            this.checkedListBoxAFF.CheckOnClick = true;
            this.checkedListBoxAFF.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkedListBoxAFF.FormattingEnabled = true;
            this.checkedListBoxAFF.HorizontalScrollbar = true;
            this.checkedListBoxAFF.Items.AddRange(new object[] {
            ">Mockup list",
            "I48.0 Paroxysmal atrial fibrillation",
            "I48.1 Persistent atrial fibrillation",
            "I48.2 Chronic atrial fibrillation",
            "I48.3 Typical atrial flutter",
            "I48.4 Atypical atrial flutter",
            "I48.91 Unspecified atrial fibrillation",
            "I48.92 Unspecified atrial flutter"});
            this.checkedListBoxAFF.Location = new System.Drawing.Point(0, 170);
            this.checkedListBoxAFF.Margin = new System.Windows.Forms.Padding(4);
            this.checkedListBoxAFF.Name = "checkedListBoxAFF";
            this.checkedListBoxAFF.Size = new System.Drawing.Size(366, 148);
            this.checkedListBoxAFF.TabIndex = 136;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(0, 151);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(366, 19);
            this.label2.TabIndex = 135;
            this.label2.Text = "Atrial Fibrillation and Flutter";
            // 
            // panel12
            // 
            this.panel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel12.Location = new System.Drawing.Point(0, 131);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(366, 20);
            this.panel12.TabIndex = 134;
            // 
            // checkedListBoxAHR
            // 
            this.checkedListBoxAHR.CheckOnClick = true;
            this.checkedListBoxAHR.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkedListBoxAHR.FormattingEnabled = true;
            this.checkedListBoxAHR.HorizontalScrollbar = true;
            this.checkedListBoxAHR.Items.AddRange(new object[] {
            ">Mockup list",
            "R00.0 Tachycardia, unspecified",
            "R00.1 Bradycardia, unspecified",
            "R00.2 Palpitations",
            "R00.8 Other abnormalities of heart beat",
            "R00.9 Unspecified abnormalities of heart beat"});
            this.checkedListBoxAHR.Location = new System.Drawing.Point(0, 19);
            this.checkedListBoxAHR.Margin = new System.Windows.Forms.Padding(4);
            this.checkedListBoxAHR.Name = "checkedListBoxAHR";
            this.checkedListBoxAHR.Size = new System.Drawing.Size(366, 112);
            this.checkedListBoxAHR.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(366, 19);
            this.label1.TabIndex = 1;
            this.label1.Text = "Abnormalities of Heart Rhythm";
            // 
            // panel4
            // 
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(466, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(10, 923);
            this.panel4.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.checkedListBoxMCTr);
            this.panel2.Controls.Add(this.checkedListBoxMCT);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(11, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(455, 923);
            this.panel2.TabIndex = 0;
            // 
            // checkedListBoxMCTr
            // 
            this.checkedListBoxMCTr.CheckOnClick = true;
            this.checkedListBoxMCTr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkedListBoxMCTr.FormattingEnabled = true;
            this.checkedListBoxMCTr.HorizontalScrollbar = true;
            this.checkedListBoxMCTr.Items.AddRange(new object[] {
            "G45.9\tTransient cerebral ischemic attack, unspecified",
            "I44.1\tAtrioventricular block, second degree",
            "I44.2\tAtrioventricular block, complete",
            "I44.30\tUnspecified atrioventricular block",
            "I45.5\tOther specified heart block",
            "I45.6\tPre-excitation syndrome",
            "I45.89\tOther specified conduction disorders",
            "I46.2\tCardiac arrest due to underlying cardiac condition",
            "I46.8\tCardiac arrest due to other underlying condition",
            "I46.9\tCardiac arrest, cause unspecified",
            "I47.0\tRe-entry ventricular arrhythmia",
            "I47.1\tSupraventricular tachycardia",
            "I47.2\tVentricular tachycardia",
            "I47.9\tParoxysmal tachycardia, unspecified",
            "I48.0\tParoxysmal atrial fibrillation",
            "I48.1\tPersistent atrial fibrillation",
            "I48.2\tChronic atrial fibrillation",
            "I48.3\tTypical atrial flutter",
            "I48.4\tAtypical atrial flutter",
            "I48.91\tUnspecified atrial fibrillation",
            "I48.92\tUnspecified atrial flutter",
            "I49.01\tVentricular fibrillation",
            "I49.02\tVentricular flutter",
            "I49.1\tAtrial premature depolarization",
            "I49.2\tJunctional premature depolarization",
            "I49.3\tVentricular premature depolarization",
            "I49.40\tUnspecified premature depolarization",
            "I49.49\tOther premature depolarization",
            "I49.5\tSick sinus syndrome",
            "I49.8\tOther specified cardiac arrhythmias",
            "I67.841\tReversible cerebrovascular vasoconstriction syndrome",
            "I67.848\tOther cerebrovascular vasospasm and vasoconstriction",
            "R06.00\tDyspnea, unspecified",
            "R06.01\tOrthopnea",
            "R06.09\tOther forms of dyspnea",
            "R06.1\tStridor",
            "R06.2\tWheezing",
            "R06.4\tHyperventilation",
            "R42\tDizziness and giddiness",
            "R55\tSyncope and collapse",
            "T46.0X5A\tAdverse effect of cardiac-stimulant glycosides and drugs of similar acti" +
                "on, initial encounter",
            "T46.0X5S\tAdverse effect of cardiac-stimulant glycosides and drugs of similar acti" +
                "on, sequela",
            "T46.1X5A\tAdverse effect of calcium-channel blockers, initial encounter",
            "T46.1X5S\tAdverse effect of calcium-channel blockers, sequela",
            "T46.2X5A\tAdverse effect of other antidysrhythmic drugs, initial encounter",
            "T46.2X5S\tAdverse effect of other antidysrhythmic drugs, sequela",
            "T46.905A\tAdverse effect of unspecified agents primarily affecting the cardiovascu" +
                "lar system, initial encounter",
            "T46.905S\tAdverse effect of unspecified agents primarily affecting the cardiovascu" +
                "lar system, sequela",
            "T46.995A\tAdverse effect of other agents primarily affecting the cardiovascular sy" +
                "stem, initial encounter",
            "T46.995S\tAdverse effect of other agents primarily affecting the cardiovascular sy" +
                "stem, sequela"});
            this.checkedListBoxMCTr.Location = new System.Drawing.Point(0, 19);
            this.checkedListBoxMCTr.Margin = new System.Windows.Forms.Padding(4);
            this.checkedListBoxMCTr.Name = "checkedListBoxMCTr";
            this.checkedListBoxMCTr.Size = new System.Drawing.Size(455, 904);
            this.checkedListBoxMCTr.TabIndex = 23;
            // 
            // checkedListBoxMCT
            // 
            this.checkedListBoxMCT.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkedListBoxMCT.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.checkedListBoxMCT.Location = new System.Drawing.Point(0, 0);
            this.checkedListBoxMCT.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.checkedListBoxMCT.Name = "checkedListBoxMCT";
            this.checkedListBoxMCT.Size = new System.Drawing.Size(455, 19);
            this.checkedListBoxMCT.TabIndex = 22;
            this.checkedListBoxMCT.Text = "MCT reimbursable ICD codes";
            // 
            // panel18
            // 
            this.panel18.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel18.Location = new System.Drawing.Point(0, 0);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(11, 923);
            this.panel18.TabIndex = 7;
            // 
            // panel8
            // 
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(1650, 20);
            this.panel8.TabIndex = 133;
            // 
            // panel13
            // 
            this.panel13.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel13.Location = new System.Drawing.Point(0, 943);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(1650, 20);
            this.panel13.TabIndex = 24;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.panel3.Controls.Add(this.labelMaxNr);
            this.panel3.Controls.Add(this.panel14);
            this.panel3.Controls.Add(this.panelF);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 963);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1650, 45);
            this.panel3.TabIndex = 134;
            // 
            // labelMaxNr
            // 
            this.labelMaxNr.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelMaxNr.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMaxNr.ForeColor = System.Drawing.Color.White;
            this.labelMaxNr.Location = new System.Drawing.Point(986, 0);
            this.labelMaxNr.Name = "labelMaxNr";
            this.labelMaxNr.Size = new System.Drawing.Size(364, 45);
            this.labelMaxNr.TabIndex = 5;
            this.labelMaxNr.Text = "3 Max";
            this.labelMaxNr.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.buttonSelect);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel14.Location = new System.Drawing.Point(1350, 0);
            this.panel14.Margin = new System.Windows.Forms.Padding(4);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(200, 45);
            this.panel14.TabIndex = 3;
            // 
            // buttonSelect
            // 
            this.buttonSelect.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonSelect.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonSelect.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonSelect.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonSelect.FlatAppearance.BorderSize = 0;
            this.buttonSelect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSelect.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonSelect.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonSelect.Location = new System.Drawing.Point(0, 0);
            this.buttonSelect.Margin = new System.Windows.Forms.Padding(2);
            this.buttonSelect.Name = "buttonSelect";
            this.buttonSelect.Size = new System.Drawing.Size(200, 45);
            this.buttonSelect.TabIndex = 3;
            this.buttonSelect.Text = "Add code(s) to study";
            this.buttonSelect.UseVisualStyleBackColor = false;
            this.buttonSelect.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // panelF
            // 
            this.panelF.Controls.Add(this.buttonCancel);
            this.panelF.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelF.Location = new System.Drawing.Point(1550, 0);
            this.panelF.Margin = new System.Windows.Forms.Padding(4);
            this.panelF.Name = "panelF";
            this.panelF.Size = new System.Drawing.Size(100, 45);
            this.panelF.TabIndex = 1;
            // 
            // buttonCancel
            // 
            this.buttonCancel.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonCancel.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonCancel.FlatAppearance.BorderSize = 0;
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCancel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonCancel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonCancel.Location = new System.Drawing.Point(0, 0);
            this.buttonCancel.Margin = new System.Windows.Forms.Padding(2);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(100, 45);
            this.buttonCancel.TabIndex = 3;
            this.buttonCancel.Text = "Close";
            this.buttonCancel.UseVisualStyleBackColor = false;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // CicdCodeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(1650, 1008);
            this.Controls.Add(this.panelAll);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "CicdCodeForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select ICD10 Codes";
            this.panelAll.ResumeLayout(false);
            this.panelAll.PerformLayout();
            this.panelMain.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panelF.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelAll;
        private System.Windows.Forms.Panel panelMain;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.CheckedListBox checkedListBoxAII;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.CheckedListBox checkedListBoxSC;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.CheckedListBox checkedListBoxMVD;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.CheckedListBox checkedListBoxNVD;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.CheckedListBox checkedListBoxHF;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.CheckedListBox checkedListBoxHT;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.CheckedListBox checkedListBoxCP;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.CheckedListBox checkedListBoxCA;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.CheckedListBox checkedListBoxAFF;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.CheckedListBox checkedListBoxAHR;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.CheckedListBox checkedListBoxMCTr;
        private System.Windows.Forms.Label checkedListBoxMCT;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label labelMaxNr;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Button buttonSelect;
        private System.Windows.Forms.Panel panelF;
        private System.Windows.Forms.Button buttonCancel;
    }
}