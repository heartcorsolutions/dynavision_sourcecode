﻿namespace EventBoard.EntryForms
{
    partial class CRhythmForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.checkedListBoxJR = new System.Windows.Forms.CheckedListBox();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.checkedListBoxAR = new System.Windows.Forms.CheckedListBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.checkedListBoxSR = new System.Windows.Forms.CheckedListBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel22 = new System.Windows.Forms.Panel();
            this.panel24 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.panel25 = new System.Windows.Forms.Panel();
            this.panel26 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.panel27 = new System.Windows.Forms.Panel();
            this.panel28 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.panel29 = new System.Windows.Forms.Panel();
            this.panel30 = new System.Windows.Forms.Panel();
            this.panel32 = new System.Windows.Forms.Panel();
            this.checkedListBoxPR = new System.Windows.Forms.CheckedListBox();
            this.panel33 = new System.Windows.Forms.Panel();
            this.panel34 = new System.Windows.Forms.Panel();
            this.checkedListBoxAVR = new System.Windows.Forms.CheckedListBox();
            this.panel35 = new System.Windows.Forms.Panel();
            this.panel36 = new System.Windows.Forms.Panel();
            this.checkedListBoxVR = new System.Windows.Forms.CheckedListBox();
            this.panel37 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.panel16 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.panel19 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel38 = new System.Windows.Forms.Panel();
            this.panel39 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.panel40 = new System.Windows.Forms.Panel();
            this.buttonSelect = new System.Windows.Forms.Button();
            this.panel42 = new System.Windows.Forms.Panel();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.panel43 = new System.Windows.Forms.Panel();
            this.panel5.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel26.SuspendLayout();
            this.panel28.SuspendLayout();
            this.panel30.SuspendLayout();
            this.panel32.SuspendLayout();
            this.panel34.SuspendLayout();
            this.panel36.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel39.SuspendLayout();
            this.panel40.SuspendLayout();
            this.panel42.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(905, 19);
            this.panel3.TabIndex = 2;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panel5.Controls.Add(this.panel13);
            this.panel5.Controls.Add(this.panel12);
            this.panel5.Controls.Add(this.panel11);
            this.panel5.Controls.Add(this.panel10);
            this.panel5.Controls.Add(this.panel9);
            this.panel5.Controls.Add(this.panel8);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 52);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(905, 142);
            this.panel5.TabIndex = 4;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.checkedListBoxJR);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel13.Location = new System.Drawing.Point(628, 0);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(266, 142);
            this.panel13.TabIndex = 12;
            // 
            // checkedListBoxJR
            // 
            this.checkedListBoxJR.CheckOnClick = true;
            this.checkedListBoxJR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkedListBoxJR.FormattingEnabled = true;
            this.checkedListBoxJR.Items.AddRange(new object[] {
            "Accelerated Junctional Rhythm",
            "Junctional Escape Rhythm",
            "Junctional Tachycardia",
            "Premature Junctional Complex"});
            this.checkedListBoxJR.Location = new System.Drawing.Point(0, 0);
            this.checkedListBoxJR.Name = "checkedListBoxJR";
            this.checkedListBoxJR.Size = new System.Drawing.Size(266, 142);
            this.checkedListBoxJR.TabIndex = 1;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panel12.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel12.Location = new System.Drawing.Point(616, 0);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(12, 142);
            this.panel12.TabIndex = 11;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.checkedListBoxAR);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel11.Location = new System.Drawing.Point(352, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(264, 142);
            this.panel11.TabIndex = 10;
            // 
            // checkedListBoxAR
            // 
            this.checkedListBoxAR.CheckOnClick = true;
            this.checkedListBoxAR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkedListBoxAR.FormattingEnabled = true;
            this.checkedListBoxAR.Items.AddRange(new object[] {
            "Atrial Fibrillation (afib)",
            "Atrial Flutter",
            "Multifocal Atrial Tachycardia",
            "Premature Atrial Complex",
            "Supraventricular Tachycardia",
            "Wandering Atrial Pacemaker",
            "Wolff-Parkinson-White Syndrome"});
            this.checkedListBoxAR.Location = new System.Drawing.Point(0, 0);
            this.checkedListBoxAR.Name = "checkedListBoxAR";
            this.checkedListBoxAR.Size = new System.Drawing.Size(264, 142);
            this.checkedListBoxAR.TabIndex = 1;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panel10.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel10.Location = new System.Drawing.Point(342, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(10, 142);
            this.panel10.TabIndex = 9;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.checkedListBoxSR);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel9.Location = new System.Drawing.Point(10, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(332, 142);
            this.panel9.TabIndex = 8;
            // 
            // checkedListBoxSR
            // 
            this.checkedListBoxSR.CheckOnClick = true;
            this.checkedListBoxSR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkedListBoxSR.FormattingEnabled = true;
            this.checkedListBoxSR.Items.AddRange(new object[] {
            "Normal Sinus Rhythm",
            "Sinoatrial Block",
            "Sinus Pause",
            "Sinus Arrhythmia",
            "Sinus Bradycardia",
            "Sinus Tachycardia"});
            this.checkedListBoxSR.Location = new System.Drawing.Point(0, 0);
            this.checkedListBoxSR.Name = "checkedListBoxSR";
            this.checkedListBoxSR.Size = new System.Drawing.Size(332, 142);
            this.checkedListBoxSR.TabIndex = 0;
            this.checkedListBoxSR.SelectedIndexChanged += new System.EventHandler(this.checkedListBox1_SelectedIndexChanged);
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panel8.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(10, 142);
            this.panel8.TabIndex = 7;
            // 
            // panel22
            // 
            this.panel22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panel22.Controls.Add(this.panel24);
            this.panel22.Controls.Add(this.panel25);
            this.panel22.Controls.Add(this.panel26);
            this.panel22.Controls.Add(this.panel27);
            this.panel22.Controls.Add(this.panel28);
            this.panel22.Controls.Add(this.panel29);
            this.panel22.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel22.Location = new System.Drawing.Point(0, 194);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(905, 33);
            this.panel22.TabIndex = 5;
            // 
            // panel24
            // 
            this.panel24.Controls.Add(this.label5);
            this.panel24.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel24.Location = new System.Drawing.Point(628, 0);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(245, 33);
            this.panel24.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(245, 33);
            this.label5.TabIndex = 2;
            this.label5.Text = "Pacemaker Rhythm";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel25
            // 
            this.panel25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panel25.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel25.Location = new System.Drawing.Point(616, 0);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(12, 33);
            this.panel25.TabIndex = 4;
            // 
            // panel26
            // 
            this.panel26.Controls.Add(this.label6);
            this.panel26.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel26.Location = new System.Drawing.Point(352, 0);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(264, 33);
            this.panel26.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(264, 33);
            this.label6.TabIndex = 1;
            this.label6.Text = "Atrioventricular Rhythm";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel27
            // 
            this.panel27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panel27.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel27.Location = new System.Drawing.Point(342, 0);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(10, 33);
            this.panel27.TabIndex = 2;
            // 
            // panel28
            // 
            this.panel28.Controls.Add(this.label7);
            this.panel28.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel28.Location = new System.Drawing.Point(10, 0);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(332, 33);
            this.panel28.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label7.Location = new System.Drawing.Point(0, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(332, 33);
            this.label7.TabIndex = 0;
            this.label7.Text = "Ventricular Rhythm";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel29
            // 
            this.panel29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panel29.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel29.Location = new System.Drawing.Point(0, 0);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(10, 33);
            this.panel29.TabIndex = 0;
            // 
            // panel30
            // 
            this.panel30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panel30.Controls.Add(this.panel32);
            this.panel30.Controls.Add(this.panel33);
            this.panel30.Controls.Add(this.panel34);
            this.panel30.Controls.Add(this.panel35);
            this.panel30.Controls.Add(this.panel36);
            this.panel30.Controls.Add(this.panel37);
            this.panel30.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel30.Location = new System.Drawing.Point(0, 227);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(905, 229);
            this.panel30.TabIndex = 6;
            // 
            // panel32
            // 
            this.panel32.Controls.Add(this.checkedListBoxPR);
            this.panel32.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel32.Location = new System.Drawing.Point(628, 0);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(266, 229);
            this.panel32.TabIndex = 12;
            // 
            // checkedListBoxPR
            // 
            this.checkedListBoxPR.CheckOnClick = true;
            this.checkedListBoxPR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkedListBoxPR.FormattingEnabled = true;
            this.checkedListBoxPR.Items.AddRange(new object[] {
            "Normal Single Chamber Pacemaker",
            "Normal Dual Chamber Pacemaker",
            "Failure to Capture",
            "Failure to Pace",
            "Failure to Sense"});
            this.checkedListBoxPR.Location = new System.Drawing.Point(0, 0);
            this.checkedListBoxPR.Name = "checkedListBoxPR";
            this.checkedListBoxPR.Size = new System.Drawing.Size(266, 229);
            this.checkedListBoxPR.TabIndex = 1;
            // 
            // panel33
            // 
            this.panel33.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panel33.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel33.Location = new System.Drawing.Point(616, 0);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(12, 229);
            this.panel33.TabIndex = 11;
            // 
            // panel34
            // 
            this.panel34.Controls.Add(this.checkedListBoxAVR);
            this.panel34.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel34.Location = new System.Drawing.Point(352, 0);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(264, 229);
            this.panel34.TabIndex = 10;
            // 
            // checkedListBoxAVR
            // 
            this.checkedListBoxAVR.CheckOnClick = true;
            this.checkedListBoxAVR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkedListBoxAVR.FormattingEnabled = true;
            this.checkedListBoxAVR.Items.AddRange(new object[] {
            "Bundle Branch Block",
            "First Degree Heart Block",
            "Second Degree Heart Block Type I",
            "Second Degree Heart Block Type II",
            "Third Degree Heart Block"});
            this.checkedListBoxAVR.Location = new System.Drawing.Point(0, 0);
            this.checkedListBoxAVR.Name = "checkedListBoxAVR";
            this.checkedListBoxAVR.Size = new System.Drawing.Size(264, 229);
            this.checkedListBoxAVR.TabIndex = 1;
            // 
            // panel35
            // 
            this.panel35.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panel35.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel35.Location = new System.Drawing.Point(342, 0);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(10, 229);
            this.panel35.TabIndex = 9;
            // 
            // panel36
            // 
            this.panel36.Controls.Add(this.checkedListBoxVR);
            this.panel36.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel36.Location = new System.Drawing.Point(10, 0);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(332, 229);
            this.panel36.TabIndex = 8;
            // 
            // checkedListBoxVR
            // 
            this.checkedListBoxVR.CheckOnClick = true;
            this.checkedListBoxVR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkedListBoxVR.FormattingEnabled = true;
            this.checkedListBoxVR.Items.AddRange(new object[] {
            "Accelerated Idioventricular Rhythm",
            "Asystole",
            "Idioventricular Rhythm",
            "Premature Ventricular Complex",
            "Premature Ventricular Complex - Bigeminy",
            "Premature Ventricular Complex - Trigeminy",
            "Premature Ventricular Complex - Quadrigeminy",
            "Ventricular Fibrillation",
            "Ventricular Tachycardia",
            "Ventricular Tachycardia Monomorphic",
            "Ventricular Tachycardia Polymorphic",
            "Torsade de Pointes"});
            this.checkedListBoxVR.Location = new System.Drawing.Point(0, 0);
            this.checkedListBoxVR.Name = "checkedListBoxVR";
            this.checkedListBoxVR.Size = new System.Drawing.Size(332, 229);
            this.checkedListBoxVR.TabIndex = 0;
            // 
            // panel37
            // 
            this.panel37.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panel37.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel37.Location = new System.Drawing.Point(0, 0);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(10, 229);
            this.panel37.TabIndex = 7;
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panel15.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel15.Location = new System.Drawing.Point(0, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(10, 33);
            this.panel15.TabIndex = 0;
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.label2);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel16.Location = new System.Drawing.Point(10, 0);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(332, 33);
            this.panel16.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(332, 33);
            this.label2.TabIndex = 0;
            this.label2.Text = "Sinus Rhythm";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panel17.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel17.Location = new System.Drawing.Point(342, 0);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(10, 33);
            this.panel17.TabIndex = 2;
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.label3);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel18.Location = new System.Drawing.Point(352, 0);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(264, 33);
            this.panel18.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(264, 33);
            this.label3.TabIndex = 1;
            this.label3.Text = "Atrial Rhythm";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panel19.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel19.Location = new System.Drawing.Point(616, 0);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(12, 33);
            this.panel19.TabIndex = 4;
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.label4);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel20.Location = new System.Drawing.Point(628, 0);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(245, 33);
            this.panel20.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(245, 33);
            this.label4.TabIndex = 2;
            this.label4.Text = "Junctional Rhythm";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panel4.Controls.Add(this.panel20);
            this.panel4.Controls.Add(this.panel19);
            this.panel4.Controls.Add(this.panel18);
            this.panel4.Controls.Add(this.panel17);
            this.panel4.Controls.Add(this.panel16);
            this.panel4.Controls.Add(this.panel15);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 19);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(905, 33);
            this.panel4.TabIndex = 3;
            // 
            // panel38
            // 
            this.panel38.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel38.Location = new System.Drawing.Point(0, 456);
            this.panel38.Name = "panel38";
            this.panel38.Size = new System.Drawing.Size(905, 22);
            this.panel38.TabIndex = 7;
            // 
            // panel39
            // 
            this.panel39.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.panel39.Controls.Add(this.button1);
            this.panel39.Controls.Add(this.panel40);
            this.panel39.Controls.Add(this.panel42);
            this.panel39.Controls.Add(this.panel43);
            this.panel39.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel39.Location = new System.Drawing.Point(0, 475);
            this.panel39.Margin = new System.Windows.Forms.Padding(5);
            this.panel39.Name = "panel39";
            this.panel39.Size = new System.Drawing.Size(905, 55);
            this.panel39.TabIndex = 132;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button1.Dock = System.Windows.Forms.DockStyle.Right;
            this.button1.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(320, 0);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(125, 55);
            this.button1.TabIndex = 5;
            this.button1.Text = "Clear Form";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel40
            // 
            this.panel40.Controls.Add(this.buttonSelect);
            this.panel40.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel40.Location = new System.Drawing.Point(445, 0);
            this.panel40.Margin = new System.Windows.Forms.Padding(5);
            this.panel40.Name = "panel40";
            this.panel40.Size = new System.Drawing.Size(272, 55);
            this.panel40.TabIndex = 3;
            // 
            // buttonSelect
            // 
            this.buttonSelect.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonSelect.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonSelect.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonSelect.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonSelect.FlatAppearance.BorderSize = 0;
            this.buttonSelect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSelect.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonSelect.ForeColor = System.Drawing.Color.White;
            this.buttonSelect.Location = new System.Drawing.Point(0, 0);
            this.buttonSelect.Margin = new System.Windows.Forms.Padding(2);
            this.buttonSelect.Name = "buttonSelect";
            this.buttonSelect.Size = new System.Drawing.Size(272, 55);
            this.buttonSelect.TabIndex = 3;
            this.buttonSelect.Text = "Add selected rhythms to report";
            this.buttonSelect.UseVisualStyleBackColor = false;
            this.buttonSelect.Click += new System.EventHandler(this.buttonSelect_Click);
            // 
            // panel42
            // 
            this.panel42.Controls.Add(this.buttonCancel);
            this.panel42.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel42.Location = new System.Drawing.Point(717, 0);
            this.panel42.Margin = new System.Windows.Forms.Padding(5);
            this.panel42.Name = "panel42";
            this.panel42.Size = new System.Drawing.Size(178, 55);
            this.panel42.TabIndex = 1;
            // 
            // buttonCancel
            // 
            this.buttonCancel.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonCancel.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonCancel.FlatAppearance.BorderSize = 0;
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCancel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonCancel.ForeColor = System.Drawing.Color.White;
            this.buttonCancel.Location = new System.Drawing.Point(0, 0);
            this.buttonCancel.Margin = new System.Windows.Forms.Padding(2);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(178, 55);
            this.buttonCancel.TabIndex = 3;
            this.buttonCancel.Text = "Close without saving";
            this.buttonCancel.UseVisualStyleBackColor = false;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // panel43
            // 
            this.panel43.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel43.Location = new System.Drawing.Point(895, 0);
            this.panel43.Margin = new System.Windows.Forms.Padding(5);
            this.panel43.Name = "panel43";
            this.panel43.Size = new System.Drawing.Size(10, 55);
            this.panel43.TabIndex = 0;
            // 
            // CRhythmForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(905, 530);
            this.Controls.Add(this.panel39);
            this.Controls.Add(this.panel38);
            this.Controls.Add(this.panel30);
            this.Controls.Add(this.panel22);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Font = new System.Drawing.Font("Arial", 10F);
            this.Name = "CRhythmForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Select Rythm";
            this.panel5.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel22.ResumeLayout(false);
            this.panel24.ResumeLayout(false);
            this.panel26.ResumeLayout(false);
            this.panel28.ResumeLayout(false);
            this.panel30.ResumeLayout(false);
            this.panel32.ResumeLayout(false);
            this.panel34.ResumeLayout(false);
            this.panel36.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel18.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel39.ResumeLayout(false);
            this.panel40.ResumeLayout(false);
            this.panel42.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.CheckedListBox checkedListBoxSR;
        private System.Windows.Forms.CheckedListBox checkedListBoxAR;
        private System.Windows.Forms.CheckedListBox checkedListBoxJR;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.CheckedListBox checkedListBoxPR;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.Panel panel34;
        private System.Windows.Forms.CheckedListBox checkedListBoxAVR;
        private System.Windows.Forms.Panel panel35;
        private System.Windows.Forms.Panel panel36;
        private System.Windows.Forms.CheckedListBox checkedListBoxVR;
        private System.Windows.Forms.Panel panel37;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel38;
        private System.Windows.Forms.Panel panel39;
        private System.Windows.Forms.Panel panel40;
        private System.Windows.Forms.Button buttonSelect;
        private System.Windows.Forms.Panel panel42;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Panel panel43;
        private System.Windows.Forms.Button button1;
    }
}