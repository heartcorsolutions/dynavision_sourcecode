﻿namespace EventBoard
{
    partial class CPrintMctForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CPrintMctForm));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea12 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series14 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series15 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series16 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint53 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(125D, 175D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint54 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(150D, 180D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint55 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(175D, 225D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint56 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(190D, 140D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint57 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(210D, 150D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint58 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(225D, 135D);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea13 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series17 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint59 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint60 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint61 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint62 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 0D);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea14 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series18 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint63 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint64 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint65 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint66 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 0D);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea15 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series19 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint67 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint68 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint69 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint70 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 0D);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea16 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series20 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint71 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint72 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint73 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint74 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 0D);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea17 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series21 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint75 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint76 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint77 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint78 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 0D);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea18 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series22 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint79 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint80 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint81 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint82 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 0D);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea19 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series23 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint83 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint84 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint85 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint86 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 0D);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea20 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series24 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint87 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint88 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint89 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint90 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 0D);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea21 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series25 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint91 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 30D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint92 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 60D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint93 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 14D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint94 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 30D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint95 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(5D, 6D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint96 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(6D, 20D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint97 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(7D, 7D);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea22 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series26 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint98 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 20D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint99 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 20D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint100 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 20D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint101 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 20D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint102 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 20D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint103 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 15D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint104 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 18D);
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripGenPages = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonPrint = new System.Windows.Forms.ToolStripButton();
            this.toolStripClipboard1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripClipboard2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripEnterData = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panelPrintHeader = new System.Windows.Forms.Panel();
            this.labelPage1 = new System.Windows.Forms.Label();
            this.panel56 = new System.Windows.Forms.Panel();
            this.panel57 = new System.Windows.Forms.Panel();
            this.panel58 = new System.Windows.Forms.Panel();
            this.labelCenter2 = new System.Windows.Forms.Label();
            this.panel59 = new System.Windows.Forms.Panel();
            this.labelCenter1 = new System.Windows.Forms.Label();
            this.panel60 = new System.Windows.Forms.Panel();
            this.panel61 = new System.Windows.Forms.Panel();
            this.pictureBoxCenter = new System.Windows.Forms.PictureBox();
            this.panelCardiacEVentReportNumber = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.labelCardiacEventReportNr = new System.Windows.Forms.Label();
            this.labelReportNr = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panelDateTimeofEventStrip = new System.Windows.Forms.Panel();
            this.panel52 = new System.Windows.Forms.Panel();
            this.panel53 = new System.Windows.Forms.Panel();
            this.panel66 = new System.Windows.Forms.Panel();
            this.panel65 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.labelSingleEventReportDate = new System.Windows.Forms.Label();
            this.panel54 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel49 = new System.Windows.Forms.Panel();
            this.panel42 = new System.Windows.Forms.Panel();
            this.panel36 = new System.Windows.Forms.Panel();
            this.panel32 = new System.Windows.Forms.Panel();
            this.labelPatID = new System.Windows.Forms.Label();
            this.panel25 = new System.Windows.Forms.Panel();
            this.labelPatIDHeader = new System.Windows.Forms.Label();
            this.panel41 = new System.Windows.Forms.Panel();
            this.panelVert4 = new System.Windows.Forms.Panel();
            this.panel29 = new System.Windows.Forms.Panel();
            this.panel31 = new System.Windows.Forms.Panel();
            this.labelPhone2 = new System.Windows.Forms.Label();
            this.panel30 = new System.Windows.Forms.Panel();
            this.labelPhone1 = new System.Windows.Forms.Label();
            this.panel24 = new System.Windows.Forms.Panel();
            this.labelPhoneHeader = new System.Windows.Forms.Label();
            this.panel34 = new System.Windows.Forms.Panel();
            this.panelVert3 = new System.Windows.Forms.Panel();
            this.panel22 = new System.Windows.Forms.Panel();
            this.panel47 = new System.Windows.Forms.Panel();
            this.labelCountry = new System.Windows.Forms.Label();
            this.panel23 = new System.Windows.Forms.Panel();
            this.labelZipCity = new System.Windows.Forms.Label();
            this.panel19 = new System.Windows.Forms.Panel();
            this.labelAddress = new System.Windows.Forms.Label();
            this.panel18 = new System.Windows.Forms.Panel();
            this.labelAddressHeader = new System.Windows.Forms.Label();
            this.panel27 = new System.Windows.Forms.Panel();
            this.panelVert2 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.labelAgeGender = new System.Windows.Forms.Label();
            this.panel16 = new System.Windows.Forms.Panel();
            this.labelDateOfBirth = new System.Windows.Forms.Label();
            this.panel15 = new System.Windows.Forms.Panel();
            this.labelDOBheader = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panelVert1 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.labelPatientLastName = new System.Windows.Forms.Label();
            this.labelPatientFirstName = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.labelPatientNameHeader = new System.Windows.Forms.Label();
            this.panelHorSepPatDet = new System.Windows.Forms.Panel();
            this.panelSecPatBar = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel67 = new System.Windows.Forms.Panel();
            this.panel259 = new System.Windows.Forms.Panel();
            this.panel258 = new System.Windows.Forms.Panel();
            this.panel256 = new System.Windows.Forms.Panel();
            this.labelClientName = new System.Windows.Forms.Label();
            this.labelClientTel = new System.Windows.Forms.Label();
            this.panel264 = new System.Windows.Forms.Panel();
            this.labelClientHeader = new System.Windows.Forms.Label();
            this.panel254 = new System.Windows.Forms.Panel();
            this.panelVert9 = new System.Windows.Forms.Panel();
            this.panel252 = new System.Windows.Forms.Panel();
            this.labelRefPhysicianName = new System.Windows.Forms.Label();
            this.labelRefPhysicianTel = new System.Windows.Forms.Label();
            this.panel262 = new System.Windows.Forms.Panel();
            this.labelRefPhysHeader = new System.Windows.Forms.Label();
            this.panel250 = new System.Windows.Forms.Panel();
            this.panelVert8 = new System.Windows.Forms.Panel();
            this.panel244 = new System.Windows.Forms.Panel();
            this.labelPhysicianName = new System.Windows.Forms.Label();
            this.labelPhysicianTel = new System.Windows.Forms.Label();
            this.panel260 = new System.Windows.Forms.Panel();
            this.labelPhysHeader = new System.Windows.Forms.Label();
            this.panel242 = new System.Windows.Forms.Panel();
            this.panelVert7 = new System.Windows.Forms.Panel();
            this.panel240 = new System.Windows.Forms.Panel();
            this.labelSerialNr = new System.Windows.Forms.Label();
            this.labelSnrError = new System.Windows.Forms.Label();
            this.panel247 = new System.Windows.Forms.Panel();
            this.labelSerialNrHeader = new System.Windows.Forms.Label();
            this.panel239 = new System.Windows.Forms.Panel();
            this.panelVert6 = new System.Windows.Forms.Panel();
            this.panel237 = new System.Windows.Forms.Panel();
            this.panel236 = new System.Windows.Forms.Panel();
            this.panel246 = new System.Windows.Forms.Panel();
            this.labelStartDate = new System.Windows.Forms.Label();
            this.panel245 = new System.Windows.Forms.Panel();
            this.labelStartDateHeader = new System.Windows.Forms.Label();
            this.panel235 = new System.Windows.Forms.Panel();
            this.panel231 = new System.Windows.Forms.Panel();
            this.panelStudyProcedures = new System.Windows.Forms.Panel();
            this.labelDeviceError = new System.Windows.Forms.Label();
            this.panel158 = new System.Windows.Forms.Panel();
            this.panel28 = new System.Windows.Forms.Panel();
            this.panel46 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.labelNote2 = new System.Windows.Forms.Label();
            this.labelNote1 = new System.Windows.Forms.Label();
            this.panelLineUnderSampl1 = new System.Windows.Forms.Panel();
            this.panelSignatures = new System.Windows.Forms.Panel();
            this.panel110 = new System.Windows.Forms.Panel();
            this.label30 = new System.Windows.Forms.Label();
            this.panel109 = new System.Windows.Forms.Panel();
            this.labelDateReportSign = new System.Windows.Forms.Label();
            this.panel108 = new System.Windows.Forms.Panel();
            this.labelPhysSignLine = new System.Windows.Forms.Label();
            this.panel107 = new System.Windows.Forms.Panel();
            this.labelPhysSign = new System.Windows.Forms.Label();
            this.panel106 = new System.Windows.Forms.Panel();
            this.labelPhysPrintName = new System.Windows.Forms.Label();
            this.panel105 = new System.Windows.Forms.Panel();
            this.labelPhysNameReportPrint = new System.Windows.Forms.Label();
            this.panel104 = new System.Windows.Forms.Panel();
            this.panel103 = new System.Windows.Forms.Panel();
            this.panel51 = new System.Windows.Forms.Panel();
            this.label44 = new System.Windows.Forms.Label();
            this.labelPrintDate1Bottom = new System.Windows.Forms.Label();
            this.labelStudyNr = new System.Windows.Forms.Label();
            this.labelPage = new System.Windows.Forms.Label();
            this.labelPage1of = new System.Windows.Forms.Label();
            this.labelPageOf = new System.Windows.Forms.Label();
            this.labelPage1xOfX = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panelPrintArea1 = new System.Windows.Forms.Panel();
            this.panelEventsStat = new System.Windows.Forms.Panel();
            this.panel126 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel101 = new System.Windows.Forms.Panel();
            this.panel40 = new System.Windows.Forms.Panel();
            this.label31 = new System.Windows.Forms.Label();
            this.labelFirstDate = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.labelDaysHours = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.labelTotalBeats = new System.Windows.Forms.Label();
            this.panel39 = new System.Windows.Forms.Panel();
            this.panel45 = new System.Windows.Forms.Panel();
            this.panel55 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.panel62 = new System.Windows.Forms.Panel();
            this.chartHR = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel64 = new System.Windows.Forms.Panel();
            this.panel68 = new System.Windows.Forms.Panel();
            this.panel82 = new System.Windows.Forms.Panel();
            this.panel80 = new System.Windows.Forms.Panel();
            this.panel81 = new System.Windows.Forms.Panel();
            this.labelGraphTachy = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.chartTachy = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel78 = new System.Windows.Forms.Panel();
            this.panel79 = new System.Windows.Forms.Panel();
            this.labelGraphBrady = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.chartBrady = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel76 = new System.Windows.Forms.Panel();
            this.panel77 = new System.Windows.Forms.Panel();
            this.labelGraphPause = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.chartPause = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel74 = new System.Windows.Forms.Panel();
            this.panel75 = new System.Windows.Forms.Panel();
            this.labelGraphAfib = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.chartAfib = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel69 = new System.Windows.Forms.Panel();
            this.panel70 = new System.Windows.Forms.Panel();
            this.labelGraphPace = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.chartPace = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel72 = new System.Windows.Forms.Panel();
            this.panel73 = new System.Windows.Forms.Panel();
            this.labelGraphManual = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.chartManual = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel83 = new System.Windows.Forms.Panel();
            this.panel84 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.panel85 = new System.Windows.Forms.Panel();
            this.label28 = new System.Windows.Forms.Label();
            this.panel86 = new System.Windows.Forms.Panel();
            this.panel88 = new System.Windows.Forms.Panel();
            this.panel92 = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel91 = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.labelEpTachy = new System.Windows.Forms.Label();
            this.labelEpBrady = new System.Windows.Forms.Label();
            this.labelEpPause = new System.Windows.Forms.Label();
            this.labelEpAfib = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.labelEpTotal = new System.Windows.Forms.Label();
            this.labelEpMan = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.labelUser = new System.Windows.Forms.Label();
            this.panel90 = new System.Windows.Forms.Panel();
            this.label22 = new System.Windows.Forms.Label();
            this.labelDurTachy = new System.Windows.Forms.Label();
            this.labelDurBrady = new System.Windows.Forms.Label();
            this.labelDurPause = new System.Windows.Forms.Label();
            this.labelDurAfib = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.labelDurTotal = new System.Windows.Forms.Label();
            this.labelDurMan = new System.Windows.Forms.Label();
            this.labelDurPeriod = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.panel89 = new System.Windows.Forms.Panel();
            this.label27 = new System.Windows.Forms.Label();
            this.labelPerTachy = new System.Windows.Forms.Label();
            this.labelPerBrady = new System.Windows.Forms.Label();
            this.labelPerPause = new System.Windows.Forms.Label();
            this.labelPerAfib = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.labelPerTotal = new System.Windows.Forms.Label();
            this.labelPerMan = new System.Windows.Forms.Label();
            this.panel87 = new System.Windows.Forms.Panel();
            this.chartSummaryBar = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chartSummeryPie = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel35 = new System.Windows.Forms.Panel();
            this.panel63 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.toolStrip1.SuspendLayout();
            this.panelPrintHeader.SuspendLayout();
            this.panel56.SuspendLayout();
            this.panel58.SuspendLayout();
            this.panel59.SuspendLayout();
            this.panel61.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCenter)).BeginInit();
            this.panelCardiacEVentReportNumber.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panelDateTimeofEventStrip.SuspendLayout();
            this.panel53.SuspendLayout();
            this.panel65.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel36.SuspendLayout();
            this.panel32.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel29.SuspendLayout();
            this.panel31.SuspendLayout();
            this.panel30.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel47.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panelSecPatBar.SuspendLayout();
            this.panel67.SuspendLayout();
            this.panel256.SuspendLayout();
            this.panel264.SuspendLayout();
            this.panel252.SuspendLayout();
            this.panel262.SuspendLayout();
            this.panel244.SuspendLayout();
            this.panel260.SuspendLayout();
            this.panel240.SuspendLayout();
            this.panel247.SuspendLayout();
            this.panel236.SuspendLayout();
            this.panel246.SuspendLayout();
            this.panel245.SuspendLayout();
            this.panelStudyProcedures.SuspendLayout();
            this.panel158.SuspendLayout();
            this.panel28.SuspendLayout();
            this.panelSignatures.SuspendLayout();
            this.panel110.SuspendLayout();
            this.panel109.SuspendLayout();
            this.panel108.SuspendLayout();
            this.panel107.SuspendLayout();
            this.panel106.SuspendLayout();
            this.panel105.SuspendLayout();
            this.panel51.SuspendLayout();
            this.panelPrintArea1.SuspendLayout();
            this.panelEventsStat.SuspendLayout();
            this.panel40.SuspendLayout();
            this.panel45.SuspendLayout();
            this.panel55.SuspendLayout();
            this.panel62.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartHR)).BeginInit();
            this.panel68.SuspendLayout();
            this.panel80.SuspendLayout();
            this.panel81.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartTachy)).BeginInit();
            this.panel78.SuspendLayout();
            this.panel79.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartBrady)).BeginInit();
            this.panel76.SuspendLayout();
            this.panel77.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartPause)).BeginInit();
            this.panel74.SuspendLayout();
            this.panel75.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartAfib)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.panel13.SuspendLayout();
            this.panel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            this.panel69.SuspendLayout();
            this.panel70.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartPace)).BeginInit();
            this.panel72.SuspendLayout();
            this.panel73.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartManual)).BeginInit();
            this.panel84.SuspendLayout();
            this.panel85.SuspendLayout();
            this.panel86.SuspendLayout();
            this.panel88.SuspendLayout();
            this.panel92.SuspendLayout();
            this.panel91.SuspendLayout();
            this.panel90.SuspendLayout();
            this.panel89.SuspendLayout();
            this.panel87.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartSummaryBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartSummeryPie)).BeginInit();
            this.panel35.SuspendLayout();
            this.panel63.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripGenPages,
            this.toolStripButtonPrint,
            this.toolStripClipboard1,
            this.toolStripClipboard2,
            this.toolStripButton1,
            this.toolStripEnterData,
            this.toolStripButton2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(2373, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripGenPages
            // 
            this.toolStripGenPages.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripGenPages.Image = ((System.Drawing.Image)(resources.GetObject("toolStripGenPages.Image")));
            this.toolStripGenPages.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripGenPages.Name = "toolStripGenPages";
            this.toolStripGenPages.Size = new System.Drawing.Size(112, 22);
            this.toolStripGenPages.Text = "Generating Pages...";
            // 
            // toolStripButtonPrint
            // 
            this.toolStripButtonPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonPrint.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonPrint.Image")));
            this.toolStripButtonPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonPrint.Name = "toolStripButtonPrint";
            this.toolStripButtonPrint.Size = new System.Drawing.Size(36, 22);
            this.toolStripButtonPrint.Text = "Print";
            this.toolStripButtonPrint.Click += new System.EventHandler(this.toolStripButton1_Click_1);
            // 
            // toolStripClipboard1
            // 
            this.toolStripClipboard1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripClipboard1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripClipboard1.Image")));
            this.toolStripClipboard1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripClipboard1.Name = "toolStripClipboard1";
            this.toolStripClipboard1.Size = new System.Drawing.Size(72, 22);
            this.toolStripClipboard1.Text = "Clipboard 1";
            this.toolStripClipboard1.Click += new System.EventHandler(this.toolStripClipboard_Click);
            // 
            // toolStripClipboard2
            // 
            this.toolStripClipboard2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripClipboard2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripClipboard2.Image")));
            this.toolStripClipboard2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripClipboard2.Name = "toolStripClipboard2";
            this.toolStripClipboard2.Size = new System.Drawing.Size(72, 22);
            this.toolStripClipboard2.Text = "Clipboard 2";
            this.toolStripClipboard2.Click += new System.EventHandler(this.toolStripClipboard2_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(72, 22);
            this.toolStripButton1.Text = "Clipboard 3";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click_2);
            // 
            // toolStripEnterData
            // 
            this.toolStripEnterData.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripEnterData.Image = ((System.Drawing.Image)(resources.GetObject("toolStripEnterData.Image")));
            this.toolStripEnterData.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripEnterData.Name = "toolStripEnterData";
            this.toolStripEnterData.Size = new System.Drawing.Size(106, 22);
            this.toolStripEnterData.Text = "Enter Header Data";
            this.toolStripEnterData.Visible = false;
            this.toolStripEnterData.Click += new System.EventHandler(this.toolStripEnterData_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(25, 22);
            this.toolStripButton2.Text = "C4";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // panelPrintHeader
            // 
            this.panelPrintHeader.Controls.Add(this.labelPage1);
            this.panelPrintHeader.Controls.Add(this.panel56);
            this.panelPrintHeader.Controls.Add(this.panel61);
            this.panelPrintHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelPrintHeader.Location = new System.Drawing.Point(0, 0);
            this.panelPrintHeader.Name = "panelPrintHeader";
            this.panelPrintHeader.Size = new System.Drawing.Size(2338, 175);
            this.panelPrintHeader.TabIndex = 4;
            this.panelPrintHeader.Paint += new System.Windows.Forms.PaintEventHandler(this.panelPrintHeader_Paint);
            // 
            // labelPage1
            // 
            this.labelPage1.AutoSize = true;
            this.labelPage1.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPage1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPage1.Location = new System.Drawing.Point(683, 0);
            this.labelPage1.Name = "labelPage1";
            this.labelPage1.Size = new System.Drawing.Size(177, 55);
            this.labelPage1.TabIndex = 9;
            this.labelPage1.Text = "Page 1";
            // 
            // panel56
            // 
            this.panel56.Controls.Add(this.panel57);
            this.panel56.Controls.Add(this.panel58);
            this.panel56.Controls.Add(this.panel59);
            this.panel56.Controls.Add(this.panel60);
            this.panel56.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel56.Location = new System.Drawing.Point(1607, 0);
            this.panel56.Name = "panel56";
            this.panel56.Size = new System.Drawing.Size(731, 175);
            this.panel56.TabIndex = 8;
            // 
            // panel57
            // 
            this.panel57.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel57.Location = new System.Drawing.Point(0, 129);
            this.panel57.Name = "panel57";
            this.panel57.Size = new System.Drawing.Size(731, 27);
            this.panel57.TabIndex = 16;
            // 
            // panel58
            // 
            this.panel58.Controls.Add(this.labelCenter2);
            this.panel58.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel58.Location = new System.Drawing.Point(0, 82);
            this.panel58.Name = "panel58";
            this.panel58.Size = new System.Drawing.Size(731, 47);
            this.panel58.TabIndex = 15;
            // 
            // labelCenter2
            // 
            this.labelCenter2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCenter2.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.labelCenter2.Location = new System.Drawing.Point(0, 0);
            this.labelCenter2.Name = "labelCenter2";
            this.labelCenter2.Size = new System.Drawing.Size(731, 47);
            this.labelCenter2.TabIndex = 12;
            this.labelCenter2.Text = "c2";
            this.labelCenter2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel59
            // 
            this.panel59.Controls.Add(this.labelCenter1);
            this.panel59.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel59.Location = new System.Drawing.Point(0, 27);
            this.panel59.Name = "panel59";
            this.panel59.Size = new System.Drawing.Size(731, 55);
            this.panel59.TabIndex = 14;
            // 
            // labelCenter1
            // 
            this.labelCenter1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCenter1.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.labelCenter1.Location = new System.Drawing.Point(0, 0);
            this.labelCenter1.Name = "labelCenter1";
            this.labelCenter1.Size = new System.Drawing.Size(731, 55);
            this.labelCenter1.TabIndex = 11;
            this.labelCenter1.Text = "c1";
            this.labelCenter1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel60
            // 
            this.panel60.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel60.Location = new System.Drawing.Point(0, 0);
            this.panel60.Name = "panel60";
            this.panel60.Size = new System.Drawing.Size(731, 27);
            this.panel60.TabIndex = 13;
            // 
            // panel61
            // 
            this.panel61.Controls.Add(this.pictureBoxCenter);
            this.panel61.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel61.Location = new System.Drawing.Point(0, 0);
            this.panel61.Name = "panel61";
            this.panel61.Size = new System.Drawing.Size(683, 175);
            this.panel61.TabIndex = 6;
            // 
            // pictureBoxCenter
            // 
            this.pictureBoxCenter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBoxCenter.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBoxCenter.Location = new System.Drawing.Point(27, 0);
            this.pictureBoxCenter.Name = "pictureBoxCenter";
            this.pictureBoxCenter.Size = new System.Drawing.Size(656, 175);
            this.pictureBoxCenter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxCenter.TabIndex = 0;
            this.pictureBoxCenter.TabStop = false;
            // 
            // panelCardiacEVentReportNumber
            // 
            this.panelCardiacEVentReportNumber.Controls.Add(this.panel4);
            this.panelCardiacEVentReportNumber.Controls.Add(this.panel2);
            this.panelCardiacEVentReportNumber.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCardiacEVentReportNumber.Location = new System.Drawing.Point(0, 175);
            this.panelCardiacEVentReportNumber.Name = "panelCardiacEVentReportNumber";
            this.panelCardiacEVentReportNumber.Size = new System.Drawing.Size(2338, 103);
            this.panelCardiacEVentReportNumber.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.labelCardiacEventReportNr);
            this.panel4.Controls.Add(this.labelReportNr);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(450, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1888, 100);
            this.panel4.TabIndex = 2;
            // 
            // labelCardiacEventReportNr
            // 
            this.labelCardiacEventReportNr.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelCardiacEventReportNr.Font = new System.Drawing.Font("Verdana", 45.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCardiacEventReportNr.Location = new System.Drawing.Point(0, 0);
            this.labelCardiacEventReportNr.Name = "labelCardiacEventReportNr";
            this.labelCardiacEventReportNr.Size = new System.Drawing.Size(1430, 100);
            this.labelCardiacEventReportNr.TabIndex = 3;
            this.labelCardiacEventReportNr.Text = "Mobile Cardiac Telemetry Report ";
            this.labelCardiacEventReportNr.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelReportNr
            // 
            this.labelReportNr.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelReportNr.Font = new System.Drawing.Font("Verdana", 45.75F, System.Drawing.FontStyle.Bold);
            this.labelReportNr.Location = new System.Drawing.Point(1588, 0);
            this.labelReportNr.Name = "labelReportNr";
            this.labelReportNr.Size = new System.Drawing.Size(300, 100);
            this.labelReportNr.TabIndex = 2;
            this.labelReportNr.Text = "??";
            this.labelReportNr.Visible = false;
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(450, 103);
            this.panel2.TabIndex = 0;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // panelDateTimeofEventStrip
            // 
            this.panelDateTimeofEventStrip.Controls.Add(this.panel52);
            this.panelDateTimeofEventStrip.Controls.Add(this.panel53);
            this.panelDateTimeofEventStrip.Controls.Add(this.panel54);
            this.panelDateTimeofEventStrip.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelDateTimeofEventStrip.Location = new System.Drawing.Point(0, 278);
            this.panelDateTimeofEventStrip.Name = "panelDateTimeofEventStrip";
            this.panelDateTimeofEventStrip.Size = new System.Drawing.Size(2338, 107);
            this.panelDateTimeofEventStrip.TabIndex = 1;
            // 
            // panel52
            // 
            this.panel52.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel52.Location = new System.Drawing.Point(2161, 0);
            this.panel52.Name = "panel52";
            this.panel52.Size = new System.Drawing.Size(177, 107);
            this.panel52.TabIndex = 5;
            this.panel52.Paint += new System.Windows.Forms.PaintEventHandler(this.panel52_Paint);
            // 
            // panel53
            // 
            this.panel53.Controls.Add(this.panel66);
            this.panel53.Controls.Add(this.panel65);
            this.panel53.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel53.Location = new System.Drawing.Point(300, 0);
            this.panel53.Name = "panel53";
            this.panel53.Size = new System.Drawing.Size(2038, 107);
            this.panel53.TabIndex = 4;
            // 
            // panel66
            // 
            this.panel66.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel66.Location = new System.Drawing.Point(1366, 0);
            this.panel66.Name = "panel66";
            this.panel66.Size = new System.Drawing.Size(672, 107);
            this.panel66.TabIndex = 5;
            // 
            // panel65
            // 
            this.panel65.Controls.Add(this.label11);
            this.panel65.Controls.Add(this.labelSingleEventReportDate);
            this.panel65.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel65.Location = new System.Drawing.Point(0, 0);
            this.panel65.Name = "panel65";
            this.panel65.Size = new System.Drawing.Size(1337, 107);
            this.panel65.TabIndex = 4;
            // 
            // label11
            // 
            this.label11.Dock = System.Windows.Forms.DockStyle.Right;
            this.label11.Font = new System.Drawing.Font("Verdana", 38.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(346, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(304, 107);
            this.label11.TabIndex = 9;
            this.label11.Text = "Printed:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelSingleEventReportDate
            // 
            this.labelSingleEventReportDate.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSingleEventReportDate.Font = new System.Drawing.Font("Verdana", 38.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSingleEventReportDate.Location = new System.Drawing.Point(650, 0);
            this.labelSingleEventReportDate.Name = "labelSingleEventReportDate";
            this.labelSingleEventReportDate.Size = new System.Drawing.Size(687, 107);
            this.labelSingleEventReportDate.TabIndex = 8;
            this.labelSingleEventReportDate.Text = "11/12/2016 22:22 AM";
            this.labelSingleEventReportDate.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel54
            // 
            this.panel54.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel54.Location = new System.Drawing.Point(0, 0);
            this.panel54.Name = "panel54";
            this.panel54.Size = new System.Drawing.Size(300, 107);
            this.panel54.TabIndex = 3;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.panel49);
            this.panel5.Controls.Add(this.panel42);
            this.panel5.Controls.Add(this.panel36);
            this.panel5.Controls.Add(this.panel41);
            this.panel5.Controls.Add(this.panelVert4);
            this.panel5.Controls.Add(this.panel29);
            this.panel5.Controls.Add(this.panel34);
            this.panel5.Controls.Add(this.panelVert3);
            this.panel5.Controls.Add(this.panel22);
            this.panel5.Controls.Add(this.panel27);
            this.panel5.Controls.Add(this.panelVert2);
            this.panel5.Controls.Add(this.panel10);
            this.panel5.Controls.Add(this.panel11);
            this.panel5.Controls.Add(this.panelVert1);
            this.panel5.Controls.Add(this.panel8);
            this.panel5.Controls.Add(this.panel7);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 385);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(2338, 208);
            this.panel5.TabIndex = 2;
            // 
            // panel49
            // 
            this.panel49.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel49.Location = new System.Drawing.Point(2307, 0);
            this.panel49.Name = "panel49";
            this.panel49.Size = new System.Drawing.Size(13, 208);
            this.panel49.TabIndex = 20;
            // 
            // panel42
            // 
            this.panel42.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel42.Location = new System.Drawing.Point(2320, 0);
            this.panel42.Name = "panel42";
            this.panel42.Size = new System.Drawing.Size(18, 208);
            this.panel42.TabIndex = 19;
            // 
            // panel36
            // 
            this.panel36.Controls.Add(this.panel32);
            this.panel36.Controls.Add(this.panel25);
            this.panel36.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel36.Location = new System.Drawing.Point(1967, 0);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(338, 208);
            this.panel36.TabIndex = 15;
            // 
            // panel32
            // 
            this.panel32.Controls.Add(this.labelPatID);
            this.panel32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel32.Location = new System.Drawing.Point(0, 50);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(338, 158);
            this.panel32.TabIndex = 5;
            // 
            // labelPatID
            // 
            this.labelPatID.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPatID.Font = new System.Drawing.Font("Verdana", 27F, System.Drawing.FontStyle.Bold);
            this.labelPatID.Location = new System.Drawing.Point(0, 0);
            this.labelPatID.Name = "labelPatID";
            this.labelPatID.Size = new System.Drawing.Size(338, 158);
            this.labelPatID.TabIndex = 40;
            this.labelPatID.Text = "12341234567";
            this.labelPatID.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel25
            // 
            this.panel25.Controls.Add(this.labelPatIDHeader);
            this.panel25.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel25.Location = new System.Drawing.Point(0, 0);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(338, 50);
            this.panel25.TabIndex = 3;
            // 
            // labelPatIDHeader
            // 
            this.labelPatIDHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPatIDHeader.Font = new System.Drawing.Font("Verdana", 27F);
            this.labelPatIDHeader.Location = new System.Drawing.Point(0, 0);
            this.labelPatIDHeader.Name = "labelPatIDHeader";
            this.labelPatIDHeader.Size = new System.Drawing.Size(338, 50);
            this.labelPatIDHeader.TabIndex = 35;
            this.labelPatIDHeader.Text = "PATIENT ID:";
            this.labelPatIDHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel41
            // 
            this.panel41.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel41.Location = new System.Drawing.Point(1952, 0);
            this.panel41.Name = "panel41";
            this.panel41.Size = new System.Drawing.Size(15, 208);
            this.panel41.TabIndex = 14;
            // 
            // panelVert4
            // 
            this.panelVert4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelVert4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelVert4.Location = new System.Drawing.Point(1950, 0);
            this.panelVert4.Name = "panelVert4";
            this.panelVert4.Size = new System.Drawing.Size(2, 208);
            this.panelVert4.TabIndex = 13;
            // 
            // panel29
            // 
            this.panel29.Controls.Add(this.panel31);
            this.panel29.Controls.Add(this.panel30);
            this.panel29.Controls.Add(this.panel24);
            this.panel29.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel29.Location = new System.Drawing.Point(1600, 0);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(350, 208);
            this.panel29.TabIndex = 12;
            // 
            // panel31
            // 
            this.panel31.Controls.Add(this.labelPhone2);
            this.panel31.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel31.Location = new System.Drawing.Point(0, 100);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(350, 50);
            this.panel31.TabIndex = 6;
            // 
            // labelPhone2
            // 
            this.labelPhone2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPhone2.Font = new System.Drawing.Font("Verdana", 27F, System.Drawing.FontStyle.Bold);
            this.labelPhone2.Location = new System.Drawing.Point(0, 0);
            this.labelPhone2.Name = "labelPhone2";
            this.labelPhone2.Size = new System.Drawing.Size(350, 50);
            this.labelPhone2.TabIndex = 40;
            this.labelPhone2.Text = "800-217-0520";
            this.labelPhone2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel30
            // 
            this.panel30.Controls.Add(this.labelPhone1);
            this.panel30.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel30.Location = new System.Drawing.Point(0, 50);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(350, 50);
            this.panel30.TabIndex = 5;
            // 
            // labelPhone1
            // 
            this.labelPhone1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPhone1.Font = new System.Drawing.Font("Verdana", 27F, System.Drawing.FontStyle.Bold);
            this.labelPhone1.Location = new System.Drawing.Point(0, 0);
            this.labelPhone1.Name = "labelPhone1";
            this.labelPhone1.Size = new System.Drawing.Size(350, 50);
            this.labelPhone1.TabIndex = 40;
            this.labelPhone1.Text = "800-217-0520";
            this.labelPhone1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel24
            // 
            this.panel24.Controls.Add(this.labelPhoneHeader);
            this.panel24.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel24.Location = new System.Drawing.Point(0, 0);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(350, 50);
            this.panel24.TabIndex = 3;
            // 
            // labelPhoneHeader
            // 
            this.labelPhoneHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPhoneHeader.Font = new System.Drawing.Font("Verdana", 27F);
            this.labelPhoneHeader.Location = new System.Drawing.Point(0, 0);
            this.labelPhoneHeader.Name = "labelPhoneHeader";
            this.labelPhoneHeader.Size = new System.Drawing.Size(350, 50);
            this.labelPhoneHeader.TabIndex = 35;
            this.labelPhoneHeader.Text = "PHONE:";
            this.labelPhoneHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel34
            // 
            this.panel34.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel34.Location = new System.Drawing.Point(1585, 0);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(15, 208);
            this.panel34.TabIndex = 11;
            // 
            // panelVert3
            // 
            this.panelVert3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelVert3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelVert3.Location = new System.Drawing.Point(1583, 0);
            this.panelVert3.Name = "panelVert3";
            this.panelVert3.Size = new System.Drawing.Size(2, 208);
            this.panelVert3.TabIndex = 10;
            // 
            // panel22
            // 
            this.panel22.Controls.Add(this.panel47);
            this.panel22.Controls.Add(this.panel23);
            this.panel22.Controls.Add(this.panel19);
            this.panel22.Controls.Add(this.panel18);
            this.panel22.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel22.Location = new System.Drawing.Point(923, 0);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(660, 208);
            this.panel22.TabIndex = 9;
            // 
            // panel47
            // 
            this.panel47.Controls.Add(this.labelCountry);
            this.panel47.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel47.Location = new System.Drawing.Point(0, 150);
            this.panel47.Name = "panel47";
            this.panel47.Size = new System.Drawing.Size(660, 50);
            this.panel47.TabIndex = 6;
            // 
            // labelCountry
            // 
            this.labelCountry.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCountry.Font = new System.Drawing.Font("Verdana", 27F, System.Drawing.FontStyle.Bold);
            this.labelCountry.Location = new System.Drawing.Point(0, 0);
            this.labelCountry.Name = "labelCountry";
            this.labelCountry.Size = new System.Drawing.Size(660, 50);
            this.labelCountry.TabIndex = 0;
            this.labelCountry.Text = "State, Country";
            this.labelCountry.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelCountry.Click += new System.EventHandler(this.labelCountry_Click);
            // 
            // panel23
            // 
            this.panel23.Controls.Add(this.labelZipCity);
            this.panel23.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel23.Location = new System.Drawing.Point(0, 100);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(660, 50);
            this.panel23.TabIndex = 5;
            // 
            // labelZipCity
            // 
            this.labelZipCity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelZipCity.Font = new System.Drawing.Font("Verdana", 27F, System.Drawing.FontStyle.Bold);
            this.labelZipCity.Location = new System.Drawing.Point(0, 0);
            this.labelZipCity.Name = "labelZipCity";
            this.labelZipCity.Size = new System.Drawing.Size(660, 50);
            this.labelZipCity.TabIndex = 40;
            this.labelZipCity.Text = "Texas TX 200111";
            this.labelZipCity.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelZipCity.Click += new System.EventHandler(this.labelZipCity_Click);
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.labelAddress);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel19.Location = new System.Drawing.Point(0, 50);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(660, 50);
            this.panel19.TabIndex = 4;
            // 
            // labelAddress
            // 
            this.labelAddress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelAddress.Font = new System.Drawing.Font("Verdana", 27F, System.Drawing.FontStyle.Bold);
            this.labelAddress.Location = new System.Drawing.Point(0, 0);
            this.labelAddress.Name = "labelAddress";
            this.labelAddress.Size = new System.Drawing.Size(660, 50);
            this.labelAddress.TabIndex = 40;
            this.labelAddress.Text = "Alpha Street 112, Houston";
            this.labelAddress.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelAddress.Click += new System.EventHandler(this.labelAddress_Click);
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.labelAddressHeader);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel18.Location = new System.Drawing.Point(0, 0);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(660, 50);
            this.panel18.TabIndex = 3;
            // 
            // labelAddressHeader
            // 
            this.labelAddressHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelAddressHeader.Font = new System.Drawing.Font("Verdana", 27F);
            this.labelAddressHeader.Location = new System.Drawing.Point(0, 0);
            this.labelAddressHeader.Name = "labelAddressHeader";
            this.labelAddressHeader.Size = new System.Drawing.Size(660, 50);
            this.labelAddressHeader.TabIndex = 35;
            this.labelAddressHeader.Text = "ADDRESS:";
            this.labelAddressHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel27
            // 
            this.panel27.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel27.Location = new System.Drawing.Point(911, 0);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(12, 208);
            this.panel27.TabIndex = 8;
            // 
            // panelVert2
            // 
            this.panelVert2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelVert2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelVert2.Location = new System.Drawing.Point(909, 0);
            this.panelVert2.Name = "panelVert2";
            this.panelVert2.Size = new System.Drawing.Size(2, 208);
            this.panelVert2.TabIndex = 7;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.panel17);
            this.panel10.Controls.Add(this.panel16);
            this.panel10.Controls.Add(this.panel15);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel10.Location = new System.Drawing.Point(549, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(360, 208);
            this.panel10.TabIndex = 6;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.labelAgeGender);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel17.Location = new System.Drawing.Point(0, 100);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(360, 50);
            this.panel17.TabIndex = 4;
            // 
            // labelAgeGender
            // 
            this.labelAgeGender.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelAgeGender.Font = new System.Drawing.Font("Verdana", 27F, System.Drawing.FontStyle.Bold);
            this.labelAgeGender.Location = new System.Drawing.Point(0, 0);
            this.labelAgeGender.Name = "labelAgeGender";
            this.labelAgeGender.Size = new System.Drawing.Size(360, 50);
            this.labelAgeGender.TabIndex = 40;
            this.labelAgeGender.Text = "41, male";
            this.labelAgeGender.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.labelDateOfBirth);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel16.Location = new System.Drawing.Point(0, 50);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(360, 50);
            this.panel16.TabIndex = 3;
            // 
            // labelDateOfBirth
            // 
            this.labelDateOfBirth.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDateOfBirth.Font = new System.Drawing.Font("Verdana", 27F, System.Drawing.FontStyle.Bold);
            this.labelDateOfBirth.Location = new System.Drawing.Point(0, 0);
            this.labelDateOfBirth.Name = "labelDateOfBirth";
            this.labelDateOfBirth.Size = new System.Drawing.Size(360, 50);
            this.labelDateOfBirth.TabIndex = 40;
            this.labelDateOfBirth.Text = "06/08/1974";
            this.labelDateOfBirth.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.labelDOBheader);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel15.Location = new System.Drawing.Point(0, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(360, 50);
            this.panel15.TabIndex = 2;
            // 
            // labelDOBheader
            // 
            this.labelDOBheader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDOBheader.Font = new System.Drawing.Font("Verdana", 27F);
            this.labelDOBheader.Location = new System.Drawing.Point(0, 0);
            this.labelDOBheader.Name = "labelDOBheader";
            this.labelDOBheader.Size = new System.Drawing.Size(360, 50);
            this.labelDOBheader.TabIndex = 35;
            this.labelDOBheader.Text = "DATE OF BIRTH:";
            this.labelDOBheader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel11
            // 
            this.panel11.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel11.Location = new System.Drawing.Point(544, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(5, 208);
            this.panel11.TabIndex = 4;
            // 
            // panelVert1
            // 
            this.panelVert1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelVert1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelVert1.Location = new System.Drawing.Point(542, 0);
            this.panelVert1.Name = "panelVert1";
            this.panelVert1.Size = new System.Drawing.Size(2, 208);
            this.panelVert1.TabIndex = 3;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.labelPatientLastName);
            this.panel8.Controls.Add(this.labelPatientFirstName);
            this.panel8.Controls.Add(this.panel12);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel8.Location = new System.Drawing.Point(22, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(520, 208);
            this.panel8.TabIndex = 1;
            // 
            // labelPatientLastName
            // 
            this.labelPatientLastName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPatientLastName.Font = new System.Drawing.Font("Verdana", 27F, System.Drawing.FontStyle.Bold);
            this.labelPatientLastName.Location = new System.Drawing.Point(0, 50);
            this.labelPatientLastName.Name = "labelPatientLastName";
            this.labelPatientLastName.Size = new System.Drawing.Size(520, 108);
            this.labelPatientLastName.TabIndex = 41;
            this.labelPatientLastName.Text = "Brest van Kempen";
            this.labelPatientLastName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelPatientFirstName
            // 
            this.labelPatientFirstName.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelPatientFirstName.Font = new System.Drawing.Font("Verdana", 27F, System.Drawing.FontStyle.Bold);
            this.labelPatientFirstName.Location = new System.Drawing.Point(0, 158);
            this.labelPatientFirstName.Name = "labelPatientFirstName";
            this.labelPatientFirstName.Size = new System.Drawing.Size(520, 50);
            this.labelPatientFirstName.TabIndex = 40;
            this.labelPatientFirstName.Text = "Rutger Alexander";
            this.labelPatientFirstName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.labelPatientNameHeader);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel12.Location = new System.Drawing.Point(0, 0);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(520, 50);
            this.panel12.TabIndex = 1;
            // 
            // labelPatientNameHeader
            // 
            this.labelPatientNameHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPatientNameHeader.Font = new System.Drawing.Font("Verdana", 27F);
            this.labelPatientNameHeader.Location = new System.Drawing.Point(0, 0);
            this.labelPatientNameHeader.Name = "labelPatientNameHeader";
            this.labelPatientNameHeader.Size = new System.Drawing.Size(520, 50);
            this.labelPatientNameHeader.TabIndex = 34;
            this.labelPatientNameHeader.Text = "PATIENT NAME:";
            this.labelPatientNameHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelPatientNameHeader.Click += new System.EventHandler(this.labelPatientNameHeader_Click);
            // 
            // panelHorSepPatDet
            // 
            this.panelHorSepPatDet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelHorSepPatDet.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHorSepPatDet.Location = new System.Drawing.Point(0, 593);
            this.panelHorSepPatDet.Name = "panelHorSepPatDet";
            this.panelHorSepPatDet.Size = new System.Drawing.Size(2338, 1);
            this.panelHorSepPatDet.TabIndex = 3;
            // 
            // panelSecPatBar
            // 
            this.panelSecPatBar.Controls.Add(this.panel1);
            this.panelSecPatBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSecPatBar.Location = new System.Drawing.Point(0, 594);
            this.panelSecPatBar.Name = "panelSecPatBar";
            this.panelSecPatBar.Size = new System.Drawing.Size(2338, 25);
            this.panelSecPatBar.TabIndex = 5;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(2338, 2);
            this.panel1.TabIndex = 0;
            // 
            // panel67
            // 
            this.panel67.Controls.Add(this.panel259);
            this.panel67.Controls.Add(this.panel258);
            this.panel67.Controls.Add(this.panel256);
            this.panel67.Controls.Add(this.panel254);
            this.panel67.Controls.Add(this.panelVert9);
            this.panel67.Controls.Add(this.panel252);
            this.panel67.Controls.Add(this.panel250);
            this.panel67.Controls.Add(this.panelVert8);
            this.panel67.Controls.Add(this.panel244);
            this.panel67.Controls.Add(this.panel242);
            this.panel67.Controls.Add(this.panelVert7);
            this.panel67.Controls.Add(this.panel240);
            this.panel67.Controls.Add(this.panel239);
            this.panel67.Controls.Add(this.panelVert6);
            this.panel67.Controls.Add(this.panel237);
            this.panel67.Controls.Add(this.panel236);
            this.panel67.Controls.Add(this.panel235);
            this.panel67.Controls.Add(this.panel231);
            this.panel67.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel67.Location = new System.Drawing.Point(0, 619);
            this.panel67.Name = "panel67";
            this.panel67.Size = new System.Drawing.Size(2338, 190);
            this.panel67.TabIndex = 6;
            // 
            // panel259
            // 
            this.panel259.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel259.Location = new System.Drawing.Point(2400, 2);
            this.panel259.Name = "panel259";
            this.panel259.Size = new System.Drawing.Size(23, 188);
            this.panel259.TabIndex = 21;
            // 
            // panel258
            // 
            this.panel258.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel258.Location = new System.Drawing.Point(2307, 2);
            this.panel258.Name = "panel258";
            this.panel258.Size = new System.Drawing.Size(93, 188);
            this.panel258.TabIndex = 20;
            // 
            // panel256
            // 
            this.panel256.Controls.Add(this.labelClientName);
            this.panel256.Controls.Add(this.labelClientTel);
            this.panel256.Controls.Add(this.panel264);
            this.panel256.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel256.Location = new System.Drawing.Point(1757, 2);
            this.panel256.Name = "panel256";
            this.panel256.Size = new System.Drawing.Size(550, 188);
            this.panel256.TabIndex = 18;
            // 
            // labelClientName
            // 
            this.labelClientName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelClientName.Font = new System.Drawing.Font("Verdana", 27F, System.Drawing.FontStyle.Bold);
            this.labelClientName.Location = new System.Drawing.Point(0, 50);
            this.labelClientName.Name = "labelClientName";
            this.labelClientName.Size = new System.Drawing.Size(550, 83);
            this.labelClientName.TabIndex = 31;
            this.labelClientName.Text = "New York Hospital";
            this.labelClientName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelClientTel
            // 
            this.labelClientTel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelClientTel.Font = new System.Drawing.Font("Verdana", 27F, System.Drawing.FontStyle.Bold);
            this.labelClientTel.Location = new System.Drawing.Point(0, 133);
            this.labelClientTel.Name = "labelClientTel";
            this.labelClientTel.Size = new System.Drawing.Size(550, 55);
            this.labelClientTel.TabIndex = 43;
            this.labelClientTel.Text = "800-217-0520";
            this.labelClientTel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel264
            // 
            this.panel264.Controls.Add(this.labelClientHeader);
            this.panel264.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel264.Location = new System.Drawing.Point(0, 0);
            this.panel264.Name = "panel264";
            this.panel264.Size = new System.Drawing.Size(550, 50);
            this.panel264.TabIndex = 3;
            // 
            // labelClientHeader
            // 
            this.labelClientHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelClientHeader.Font = new System.Drawing.Font("Verdana", 27F);
            this.labelClientHeader.Location = new System.Drawing.Point(0, 0);
            this.labelClientHeader.Name = "labelClientHeader";
            this.labelClientHeader.Size = new System.Drawing.Size(550, 50);
            this.labelClientHeader.TabIndex = 19;
            this.labelClientHeader.Text = "CLIENT:";
            this.labelClientHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel254
            // 
            this.panel254.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel254.Location = new System.Drawing.Point(1743, 2);
            this.panel254.Name = "panel254";
            this.panel254.Size = new System.Drawing.Size(14, 188);
            this.panel254.TabIndex = 16;
            // 
            // panelVert9
            // 
            this.panelVert9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelVert9.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelVert9.Location = new System.Drawing.Point(1741, 2);
            this.panelVert9.Name = "panelVert9";
            this.panelVert9.Size = new System.Drawing.Size(2, 188);
            this.panelVert9.TabIndex = 15;
            // 
            // panel252
            // 
            this.panel252.Controls.Add(this.labelRefPhysicianName);
            this.panel252.Controls.Add(this.labelRefPhysicianTel);
            this.panel252.Controls.Add(this.panel262);
            this.panel252.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel252.Location = new System.Drawing.Point(1241, 2);
            this.panel252.Name = "panel252";
            this.panel252.Size = new System.Drawing.Size(500, 188);
            this.panel252.TabIndex = 14;
            // 
            // labelRefPhysicianName
            // 
            this.labelRefPhysicianName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelRefPhysicianName.Font = new System.Drawing.Font("Verdana", 27F, System.Drawing.FontStyle.Bold);
            this.labelRefPhysicianName.Location = new System.Drawing.Point(0, 50);
            this.labelRefPhysicianName.Name = "labelRefPhysicianName";
            this.labelRefPhysicianName.Size = new System.Drawing.Size(500, 88);
            this.labelRefPhysicianName.TabIndex = 29;
            this.labelRefPhysicianName.Text = "Dr. First name\r\njkdfkglhsajk\r\n";
            this.labelRefPhysicianName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelRefPhysicianTel
            // 
            this.labelRefPhysicianTel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelRefPhysicianTel.Font = new System.Drawing.Font("Verdana", 27F, System.Drawing.FontStyle.Bold);
            this.labelRefPhysicianTel.Location = new System.Drawing.Point(0, 138);
            this.labelRefPhysicianTel.Name = "labelRefPhysicianTel";
            this.labelRefPhysicianTel.Size = new System.Drawing.Size(500, 50);
            this.labelRefPhysicianTel.TabIndex = 43;
            this.labelRefPhysicianTel.Text = "800-217-0520";
            this.labelRefPhysicianTel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel262
            // 
            this.panel262.Controls.Add(this.labelRefPhysHeader);
            this.panel262.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel262.Location = new System.Drawing.Point(0, 0);
            this.panel262.Name = "panel262";
            this.panel262.Size = new System.Drawing.Size(500, 50);
            this.panel262.TabIndex = 3;
            // 
            // labelRefPhysHeader
            // 
            this.labelRefPhysHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelRefPhysHeader.Font = new System.Drawing.Font("Verdana", 27F);
            this.labelRefPhysHeader.Location = new System.Drawing.Point(0, 0);
            this.labelRefPhysHeader.Name = "labelRefPhysHeader";
            this.labelRefPhysHeader.Size = new System.Drawing.Size(500, 50);
            this.labelRefPhysHeader.TabIndex = 18;
            this.labelRefPhysHeader.Text = "REF PHYSICIAN:";
            this.labelRefPhysHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel250
            // 
            this.panel250.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel250.Location = new System.Drawing.Point(1226, 2);
            this.panel250.Name = "panel250";
            this.panel250.Size = new System.Drawing.Size(15, 188);
            this.panel250.TabIndex = 12;
            // 
            // panelVert8
            // 
            this.panelVert8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelVert8.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelVert8.Location = new System.Drawing.Point(1224, 2);
            this.panelVert8.Name = "panelVert8";
            this.panelVert8.Size = new System.Drawing.Size(2, 188);
            this.panelVert8.TabIndex = 11;
            // 
            // panel244
            // 
            this.panel244.Controls.Add(this.labelPhysicianName);
            this.panel244.Controls.Add(this.labelPhysicianTel);
            this.panel244.Controls.Add(this.panel260);
            this.panel244.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel244.Location = new System.Drawing.Point(724, 2);
            this.panel244.Name = "panel244";
            this.panel244.Size = new System.Drawing.Size(500, 188);
            this.panel244.TabIndex = 10;
            // 
            // labelPhysicianName
            // 
            this.labelPhysicianName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPhysicianName.Font = new System.Drawing.Font("Verdana", 27F, System.Drawing.FontStyle.Bold);
            this.labelPhysicianName.Location = new System.Drawing.Point(0, 50);
            this.labelPhysicianName.Name = "labelPhysicianName";
            this.labelPhysicianName.Size = new System.Drawing.Size(500, 88);
            this.labelPhysicianName.TabIndex = 25;
            this.labelPhysicianName.Text = "Dr. First name\r\nhgfagdhhj\r\n";
            this.labelPhysicianName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelPhysicianTel
            // 
            this.labelPhysicianTel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelPhysicianTel.Font = new System.Drawing.Font("Verdana", 27F, System.Drawing.FontStyle.Bold);
            this.labelPhysicianTel.Location = new System.Drawing.Point(0, 138);
            this.labelPhysicianTel.Name = "labelPhysicianTel";
            this.labelPhysicianTel.Size = new System.Drawing.Size(500, 50);
            this.labelPhysicianTel.TabIndex = 43;
            this.labelPhysicianTel.Text = "!800-217-0520";
            this.labelPhysicianTel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel260
            // 
            this.panel260.Controls.Add(this.labelPhysHeader);
            this.panel260.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel260.Location = new System.Drawing.Point(0, 0);
            this.panel260.Name = "panel260";
            this.panel260.Size = new System.Drawing.Size(500, 50);
            this.panel260.TabIndex = 2;
            // 
            // labelPhysHeader
            // 
            this.labelPhysHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPhysHeader.Font = new System.Drawing.Font("Verdana", 27F);
            this.labelPhysHeader.Location = new System.Drawing.Point(0, 0);
            this.labelPhysHeader.Name = "labelPhysHeader";
            this.labelPhysHeader.Size = new System.Drawing.Size(500, 50);
            this.labelPhysHeader.TabIndex = 16;
            this.labelPhysHeader.Text = "PHYSICIAN:";
            this.labelPhysHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel242
            // 
            this.panel242.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel242.Location = new System.Drawing.Point(712, 2);
            this.panel242.Name = "panel242";
            this.panel242.Size = new System.Drawing.Size(12, 188);
            this.panel242.TabIndex = 8;
            // 
            // panelVert7
            // 
            this.panelVert7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelVert7.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelVert7.Location = new System.Drawing.Point(710, 2);
            this.panelVert7.Name = "panelVert7";
            this.panelVert7.Size = new System.Drawing.Size(2, 188);
            this.panelVert7.TabIndex = 7;
            // 
            // panel240
            // 
            this.panel240.Controls.Add(this.labelSerialNr);
            this.panel240.Controls.Add(this.labelSnrError);
            this.panel240.Controls.Add(this.panel247);
            this.panel240.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel240.Location = new System.Drawing.Point(385, 2);
            this.panel240.Name = "panel240";
            this.panel240.Size = new System.Drawing.Size(325, 188);
            this.panel240.TabIndex = 6;
            // 
            // labelSerialNr
            // 
            this.labelSerialNr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSerialNr.Font = new System.Drawing.Font("Verdana", 27F, System.Drawing.FontStyle.Bold);
            this.labelSerialNr.Location = new System.Drawing.Point(0, 50);
            this.labelSerialNr.Name = "labelSerialNr";
            this.labelSerialNr.Size = new System.Drawing.Size(325, 80);
            this.labelSerialNr.TabIndex = 43;
            this.labelSerialNr.Text = "AA102044403";
            this.labelSerialNr.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelSnrError
            // 
            this.labelSnrError.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelSnrError.Font = new System.Drawing.Font("Verdana", 27F, System.Drawing.FontStyle.Bold);
            this.labelSnrError.Location = new System.Drawing.Point(0, 130);
            this.labelSnrError.Name = "labelSnrError";
            this.labelSnrError.Size = new System.Drawing.Size(325, 58);
            this.labelSnrError.TabIndex = 44;
            this.labelSnrError.Text = "AA102044403";
            // 
            // panel247
            // 
            this.panel247.Controls.Add(this.labelSerialNrHeader);
            this.panel247.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel247.Location = new System.Drawing.Point(0, 0);
            this.panel247.Name = "panel247";
            this.panel247.Size = new System.Drawing.Size(325, 50);
            this.panel247.TabIndex = 1;
            // 
            // labelSerialNrHeader
            // 
            this.labelSerialNrHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSerialNrHeader.Font = new System.Drawing.Font("Verdana", 27F);
            this.labelSerialNrHeader.Location = new System.Drawing.Point(0, 0);
            this.labelSerialNrHeader.Name = "labelSerialNrHeader";
            this.labelSerialNrHeader.Size = new System.Drawing.Size(325, 50);
            this.labelSerialNrHeader.TabIndex = 35;
            this.labelSerialNrHeader.Text = "SERIAL#:";
            this.labelSerialNrHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelSerialNrHeader.Click += new System.EventHandler(this.labelSerialNrHeader_Click);
            // 
            // panel239
            // 
            this.panel239.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel239.Location = new System.Drawing.Point(369, 2);
            this.panel239.Name = "panel239";
            this.panel239.Size = new System.Drawing.Size(16, 188);
            this.panel239.TabIndex = 5;
            // 
            // panelVert6
            // 
            this.panelVert6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelVert6.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelVert6.Location = new System.Drawing.Point(367, 2);
            this.panelVert6.Name = "panelVert6";
            this.panelVert6.Size = new System.Drawing.Size(2, 188);
            this.panelVert6.TabIndex = 4;
            // 
            // panel237
            // 
            this.panel237.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel237.Location = new System.Drawing.Point(346, 2);
            this.panel237.Name = "panel237";
            this.panel237.Size = new System.Drawing.Size(21, 188);
            this.panel237.TabIndex = 3;
            // 
            // panel236
            // 
            this.panel236.Controls.Add(this.panel246);
            this.panel236.Controls.Add(this.panel245);
            this.panel236.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel236.Location = new System.Drawing.Point(21, 2);
            this.panel236.Name = "panel236";
            this.panel236.Size = new System.Drawing.Size(325, 188);
            this.panel236.TabIndex = 2;
            // 
            // panel246
            // 
            this.panel246.Controls.Add(this.labelStartDate);
            this.panel246.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel246.Location = new System.Drawing.Point(0, 50);
            this.panel246.Name = "panel246";
            this.panel246.Size = new System.Drawing.Size(325, 50);
            this.panel246.TabIndex = 1;
            // 
            // labelStartDate
            // 
            this.labelStartDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelStartDate.Font = new System.Drawing.Font("Verdana", 27F, System.Drawing.FontStyle.Bold);
            this.labelStartDate.Location = new System.Drawing.Point(0, 0);
            this.labelStartDate.Name = "labelStartDate";
            this.labelStartDate.Size = new System.Drawing.Size(325, 50);
            this.labelStartDate.TabIndex = 39;
            this.labelStartDate.Text = "10/23/2016";
            this.labelStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel245
            // 
            this.panel245.Controls.Add(this.labelStartDateHeader);
            this.panel245.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel245.Location = new System.Drawing.Point(0, 0);
            this.panel245.Name = "panel245";
            this.panel245.Size = new System.Drawing.Size(325, 50);
            this.panel245.TabIndex = 0;
            // 
            // labelStartDateHeader
            // 
            this.labelStartDateHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelStartDateHeader.Font = new System.Drawing.Font("Verdana", 27F);
            this.labelStartDateHeader.Location = new System.Drawing.Point(0, 0);
            this.labelStartDateHeader.Name = "labelStartDateHeader";
            this.labelStartDateHeader.Size = new System.Drawing.Size(325, 50);
            this.labelStartDateHeader.TabIndex = 34;
            this.labelStartDateHeader.Text = "START DATE:";
            this.labelStartDateHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel235
            // 
            this.panel235.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel235.Location = new System.Drawing.Point(0, 2);
            this.panel235.Name = "panel235";
            this.panel235.Size = new System.Drawing.Size(21, 188);
            this.panel235.TabIndex = 1;
            // 
            // panel231
            // 
            this.panel231.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel231.Location = new System.Drawing.Point(0, 0);
            this.panel231.Name = "panel231";
            this.panel231.Size = new System.Drawing.Size(2338, 2);
            this.panel231.TabIndex = 0;
            // 
            // panelStudyProcedures
            // 
            this.panelStudyProcedures.Controls.Add(this.labelDeviceError);
            this.panelStudyProcedures.Controls.Add(this.panel158);
            this.panelStudyProcedures.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelStudyProcedures.Location = new System.Drawing.Point(0, 809);
            this.panelStudyProcedures.Name = "panelStudyProcedures";
            this.panelStudyProcedures.Size = new System.Drawing.Size(2338, 67);
            this.panelStudyProcedures.TabIndex = 10;
            // 
            // labelDeviceError
            // 
            this.labelDeviceError.AutoSize = true;
            this.labelDeviceError.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelDeviceError.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDeviceError.Location = new System.Drawing.Point(2338, 43);
            this.labelDeviceError.Name = "labelDeviceError";
            this.labelDeviceError.Size = new System.Drawing.Size(0, 39);
            this.labelDeviceError.TabIndex = 3;
            // 
            // panel158
            // 
            this.panel158.Controls.Add(this.panel28);
            this.panel158.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel158.Location = new System.Drawing.Point(0, 0);
            this.panel158.Name = "panel158";
            this.panel158.Size = new System.Drawing.Size(2338, 43);
            this.panel158.TabIndex = 2;
            // 
            // panel28
            // 
            this.panel28.Controls.Add(this.panel46);
            this.panel28.Controls.Add(this.panel20);
            this.panel28.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel28.Location = new System.Drawing.Point(0, 0);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(2338, 26);
            this.panel28.TabIndex = 2;
            // 
            // panel46
            // 
            this.panel46.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel46.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel46.Location = new System.Drawing.Point(0, 1);
            this.panel46.Name = "panel46";
            this.panel46.Size = new System.Drawing.Size(2338, 2);
            this.panel46.TabIndex = 5;
            // 
            // panel20
            // 
            this.panel20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel20.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel20.Location = new System.Drawing.Point(0, 0);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(2338, 1);
            this.panel20.TabIndex = 4;
            // 
            // labelNote2
            // 
            this.labelNote2.AutoSize = true;
            this.labelNote2.Font = new System.Drawing.Font("Verdana", 21F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNote2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelNote2.Location = new System.Drawing.Point(3, 3049);
            this.labelNote2.Name = "labelNote2";
            this.labelNote2.Size = new System.Drawing.Size(543, 34);
            this.labelNote2.TabIndex = 23;
            this.labelNote2.Text = "Do not use for diagnostic purposes ! ";
            this.labelNote2.Visible = false;
            // 
            // labelNote1
            // 
            this.labelNote1.AutoSize = true;
            this.labelNote1.Font = new System.Drawing.Font("Verdana", 21F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNote1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelNote1.Location = new System.Drawing.Point(3, 3010);
            this.labelNote1.Name = "labelNote1";
            this.labelNote1.Size = new System.Drawing.Size(1767, 34);
            this.labelNote1.TabIndex = 0;
            this.labelNote1.Text = "Note: the event data in the trend graphs and episode tables are \"automatic trigge" +
    "red events\" without human verification. ";
            this.labelNote1.Visible = false;
            // 
            // panelLineUnderSampl1
            // 
            this.panelLineUnderSampl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLineUnderSampl1.Location = new System.Drawing.Point(0, 876);
            this.panelLineUnderSampl1.Name = "panelLineUnderSampl1";
            this.panelLineUnderSampl1.Size = new System.Drawing.Size(2338, 2);
            this.panelLineUnderSampl1.TabIndex = 20;
            // 
            // panelSignatures
            // 
            this.panelSignatures.Controls.Add(this.panel110);
            this.panelSignatures.Controls.Add(this.panel109);
            this.panelSignatures.Controls.Add(this.panel108);
            this.panelSignatures.Controls.Add(this.panel107);
            this.panelSignatures.Controls.Add(this.panel106);
            this.panelSignatures.Controls.Add(this.panel105);
            this.panelSignatures.Controls.Add(this.panel104);
            this.panelSignatures.Controls.Add(this.panel103);
            this.panelSignatures.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelSignatures.Location = new System.Drawing.Point(0, 3122);
            this.panelSignatures.Name = "panelSignatures";
            this.panelSignatures.Size = new System.Drawing.Size(2338, 85);
            this.panelSignatures.TabIndex = 33;
            // 
            // panel110
            // 
            this.panel110.Controls.Add(this.label30);
            this.panel110.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel110.Location = new System.Drawing.Point(1960, 0);
            this.panel110.Name = "panel110";
            this.panel110.Size = new System.Drawing.Size(325, 85);
            this.panel110.TabIndex = 7;
            // 
            // label30
            // 
            this.label30.Dock = System.Windows.Forms.DockStyle.Left;
            this.label30.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(0, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(270, 85);
            this.label30.TabIndex = 2;
            this.label30.Text = "________";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel109
            // 
            this.panel109.Controls.Add(this.labelDateReportSign);
            this.panel109.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel109.Location = new System.Drawing.Point(1783, 0);
            this.panel109.Name = "panel109";
            this.panel109.Size = new System.Drawing.Size(177, 85);
            this.panel109.TabIndex = 6;
            // 
            // labelDateReportSign
            // 
            this.labelDateReportSign.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelDateReportSign.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDateReportSign.Location = new System.Drawing.Point(0, 0);
            this.labelDateReportSign.Name = "labelDateReportSign";
            this.labelDateReportSign.Size = new System.Drawing.Size(164, 85);
            this.labelDateReportSign.TabIndex = 1;
            this.labelDateReportSign.Text = "Date:";
            this.labelDateReportSign.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel108
            // 
            this.panel108.Controls.Add(this.labelPhysSignLine);
            this.panel108.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel108.Location = new System.Drawing.Point(1471, 0);
            this.panel108.Name = "panel108";
            this.panel108.Size = new System.Drawing.Size(312, 85);
            this.panel108.TabIndex = 5;
            // 
            // labelPhysSignLine
            // 
            this.labelPhysSignLine.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPhysSignLine.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPhysSignLine.Location = new System.Drawing.Point(0, 0);
            this.labelPhysSignLine.Name = "labelPhysSignLine";
            this.labelPhysSignLine.Size = new System.Drawing.Size(270, 85);
            this.labelPhysSignLine.TabIndex = 1;
            this.labelPhysSignLine.Text = "________";
            this.labelPhysSignLine.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelPhysSignLine.Click += new System.EventHandler(this.labelPhysSignLine_Click);
            // 
            // panel107
            // 
            this.panel107.Controls.Add(this.labelPhysSign);
            this.panel107.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel107.Location = new System.Drawing.Point(920, 0);
            this.panel107.Name = "panel107";
            this.panel107.Size = new System.Drawing.Size(551, 85);
            this.panel107.TabIndex = 4;
            // 
            // labelPhysSign
            // 
            this.labelPhysSign.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPhysSign.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPhysSign.Location = new System.Drawing.Point(0, 0);
            this.labelPhysSign.Name = "labelPhysSign";
            this.labelPhysSign.Size = new System.Drawing.Size(490, 85);
            this.labelPhysSign.TabIndex = 1;
            this.labelPhysSign.Text = "Physician signature:";
            this.labelPhysSign.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel106
            // 
            this.panel106.Controls.Add(this.labelPhysPrintName);
            this.panel106.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel106.Location = new System.Drawing.Point(520, 0);
            this.panel106.Name = "panel106";
            this.panel106.Size = new System.Drawing.Size(400, 85);
            this.panel106.TabIndex = 3;
            // 
            // labelPhysPrintName
            // 
            this.labelPhysPrintName.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPhysPrintName.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPhysPrintName.Location = new System.Drawing.Point(0, 0);
            this.labelPhysPrintName.Name = "labelPhysPrintName";
            this.labelPhysPrintName.Size = new System.Drawing.Size(360, 85);
            this.labelPhysPrintName.TabIndex = 1;
            this.labelPhysPrintName.Text = "J. Smithsonian";
            this.labelPhysPrintName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelPhysPrintName.Click += new System.EventHandler(this.labelPhysPrintName_Click);
            // 
            // panel105
            // 
            this.panel105.Controls.Add(this.labelPhysNameReportPrint);
            this.panel105.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel105.Location = new System.Drawing.Point(36, 0);
            this.panel105.Name = "panel105";
            this.panel105.Size = new System.Drawing.Size(484, 85);
            this.panel105.TabIndex = 2;
            // 
            // labelPhysNameReportPrint
            // 
            this.labelPhysNameReportPrint.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPhysNameReportPrint.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPhysNameReportPrint.Location = new System.Drawing.Point(0, 0);
            this.labelPhysNameReportPrint.Name = "labelPhysNameReportPrint";
            this.labelPhysNameReportPrint.Size = new System.Drawing.Size(434, 85);
            this.labelPhysNameReportPrint.TabIndex = 2;
            this.labelPhysNameReportPrint.Text = "Physician name:";
            this.labelPhysNameReportPrint.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel104
            // 
            this.panel104.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel104.Location = new System.Drawing.Point(2316, 0);
            this.panel104.Name = "panel104";
            this.panel104.Size = new System.Drawing.Size(22, 85);
            this.panel104.TabIndex = 1;
            // 
            // panel103
            // 
            this.panel103.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel103.Location = new System.Drawing.Point(0, 0);
            this.panel103.Name = "panel103";
            this.panel103.Size = new System.Drawing.Size(36, 85);
            this.panel103.TabIndex = 0;
            // 
            // panel51
            // 
            this.panel51.Controls.Add(this.label44);
            this.panel51.Controls.Add(this.labelPrintDate1Bottom);
            this.panel51.Controls.Add(this.labelStudyNr);
            this.panel51.Controls.Add(this.labelPage);
            this.panel51.Controls.Add(this.labelPage1of);
            this.panel51.Controls.Add(this.labelPageOf);
            this.panel51.Controls.Add(this.labelPage1xOfX);
            this.panel51.Controls.Add(this.label8);
            this.panel51.Controls.Add(this.label7);
            this.panel51.Controls.Add(this.label3);
            this.panel51.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel51.Location = new System.Drawing.Point(0, 3266);
            this.panel51.Name = "panel51";
            this.panel51.Size = new System.Drawing.Size(2338, 37);
            this.panel51.TabIndex = 34;
            // 
            // label44
            // 
            this.label44.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label44.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(945, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(805, 37);
            this.label44.TabIndex = 17;
            this.label44.Text = "© Copyright 2017 Techmedic International B.V.";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPrintDate1Bottom
            // 
            this.labelPrintDate1Bottom.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPrintDate1Bottom.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPrintDate1Bottom.Location = new System.Drawing.Point(225, 0);
            this.labelPrintDate1Bottom.Name = "labelPrintDate1Bottom";
            this.labelPrintDate1Bottom.Size = new System.Drawing.Size(720, 37);
            this.labelPrintDate1Bottom.TabIndex = 7;
            this.labelPrintDate1Bottom.Text = "12/27/2016 22:22:22 AM+1200 v12345";
            this.labelPrintDate1Bottom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStudyNr
            // 
            this.labelStudyNr.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelStudyNr.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStudyNr.Location = new System.Drawing.Point(1750, 0);
            this.labelStudyNr.Name = "labelStudyNr";
            this.labelStudyNr.Size = new System.Drawing.Size(180, 37);
            this.labelStudyNr.TabIndex = 41;
            this.labelStudyNr.Text = "1212212";
            this.labelStudyNr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPage
            // 
            this.labelPage.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelPage.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPage.Location = new System.Drawing.Point(1930, 0);
            this.labelPage.Name = "labelPage";
            this.labelPage.Size = new System.Drawing.Size(117, 37);
            this.labelPage.TabIndex = 12;
            this.labelPage.Text = "Page";
            this.labelPage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPage1of
            // 
            this.labelPage1of.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelPage1of.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPage1of.Location = new System.Drawing.Point(2047, 0);
            this.labelPage1of.Name = "labelPage1of";
            this.labelPage1of.Size = new System.Drawing.Size(47, 37);
            this.labelPage1of.TabIndex = 11;
            this.labelPage1of.Text = "1";
            this.labelPage1of.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPageOf
            // 
            this.labelPageOf.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelPageOf.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPageOf.Location = new System.Drawing.Point(2094, 0);
            this.labelPageOf.Name = "labelPageOf";
            this.labelPageOf.Size = new System.Drawing.Size(70, 37);
            this.labelPageOf.TabIndex = 10;
            this.labelPageOf.Text = "of";
            this.labelPageOf.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPage1xOfX
            // 
            this.labelPage1xOfX.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelPage1xOfX.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPage1xOfX.Location = new System.Drawing.Point(2164, 0);
            this.labelPage1xOfX.Name = "labelPage1xOfX";
            this.labelPage1xOfX.Size = new System.Drawing.Size(128, 37);
            this.labelPage1xOfX.TabIndex = 9;
            this.labelPage1xOfX.Text = "20p1";
            this.labelPage1xOfX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.Dock = System.Windows.Forms.DockStyle.Left;
            this.label8.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(45, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(180, 37);
            this.label8.TabIndex = 6;
            this.label8.Text = "Print date:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label7.Dock = System.Windows.Forms.DockStyle.Right;
            this.label7.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label7.Location = new System.Drawing.Point(2292, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 37);
            this.label7.TabIndex = 1;
            this.label7.Text = "R1";
            this.label7.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label7.Visible = false;
            // 
            // label3
            // 
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Dock = System.Windows.Forms.DockStyle.Left;
            this.label3.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 37);
            this.label3.TabIndex = 0;
            this.label3.Text = "L1";
            this.label3.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label3.Visible = false;
            // 
            // panelPrintArea1
            // 
            this.panelPrintArea1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panelPrintArea1.Controls.Add(this.panelEventsStat);
            this.panelPrintArea1.Controls.Add(this.labelNote1);
            this.panelPrintArea1.Controls.Add(this.labelNote2);
            this.panelPrintArea1.Controls.Add(this.panelSignatures);
            this.panelPrintArea1.Controls.Add(this.panel126);
            this.panelPrintArea1.Controls.Add(this.panel51);
            this.panelPrintArea1.Controls.Add(this.panelLineUnderSampl1);
            this.panelPrintArea1.Controls.Add(this.panelStudyProcedures);
            this.panelPrintArea1.Controls.Add(this.panel67);
            this.panelPrintArea1.Controls.Add(this.panelSecPatBar);
            this.panelPrintArea1.Controls.Add(this.panelHorSepPatDet);
            this.panelPrintArea1.Controls.Add(this.panel5);
            this.panelPrintArea1.Controls.Add(this.panelDateTimeofEventStrip);
            this.panelPrintArea1.Controls.Add(this.panelCardiacEVentReportNumber);
            this.panelPrintArea1.Controls.Add(this.panelPrintHeader);
            this.panelPrintArea1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.panelPrintArea1.Location = new System.Drawing.Point(0, 100);
            this.panelPrintArea1.Name = "panelPrintArea1";
            this.panelPrintArea1.Size = new System.Drawing.Size(2338, 3303);
            this.panelPrintArea1.TabIndex = 0;
            this.panelPrintArea1.Paint += new System.Windows.Forms.PaintEventHandler(this.panelPrintArea1_Paint);
            // 
            // panelEventsStat
            // 
            this.panelEventsStat.Controls.Add(this.panel35);
            this.panelEventsStat.Location = new System.Drawing.Point(0, 962);
            this.panelEventsStat.Name = "panelEventsStat";
            this.panelEventsStat.Size = new System.Drawing.Size(2338, 2034);
            this.panelEventsStat.TabIndex = 13;
            // 
            // panel126
            // 
            this.panel126.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel126.Location = new System.Drawing.Point(0, 3207);
            this.panel126.Name = "panel126";
            this.panel126.Size = new System.Drawing.Size(2338, 59);
            this.panel126.TabIndex = 37;
            // 
            // panel7
            // 
            this.panel7.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(22, 208);
            this.panel7.TabIndex = 0;
            // 
            // panel101
            // 
            this.panel101.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel101.Location = new System.Drawing.Point(0, 0);
            this.panel101.Name = "panel101";
            this.panel101.Size = new System.Drawing.Size(2336, 10);
            this.panel101.TabIndex = 20;
            // 
            // panel40
            // 
            this.panel40.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel40.Controls.Add(this.labelTotalBeats);
            this.panel40.Controls.Add(this.label37);
            this.panel40.Controls.Add(this.labelDaysHours);
            this.panel40.Controls.Add(this.label33);
            this.panel40.Controls.Add(this.labelFirstDate);
            this.panel40.Controls.Add(this.label31);
            this.panel40.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel40.Location = new System.Drawing.Point(0, 10);
            this.panel40.Name = "panel40";
            this.panel40.Size = new System.Drawing.Size(2336, 59);
            this.panel40.TabIndex = 10;
            // 
            // label31
            // 
            this.label31.Dock = System.Windows.Forms.DockStyle.Left;
            this.label31.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(0, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(279, 57);
            this.label31.TabIndex = 1;
            this.label31.Text = "Trend from:";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelFirstDate
            // 
            this.labelFirstDate.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelFirstDate.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFirstDate.Location = new System.Drawing.Point(279, 0);
            this.labelFirstDate.Name = "labelFirstDate";
            this.labelFirstDate.Size = new System.Drawing.Size(748, 57);
            this.labelFirstDate.TabIndex = 2;
            this.labelFirstDate.Text = "10/12/018 - 10:00:00 AM +1200 ";
            this.labelFirstDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label33
            // 
            this.label33.Dock = System.Windows.Forms.DockStyle.Left;
            this.label33.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(1027, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(282, 57);
            this.label33.TabIndex = 3;
            this.label33.Text = "Trend time:";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelDaysHours
            // 
            this.labelDaysHours.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelDaysHours.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDaysHours.Location = new System.Drawing.Point(1309, 0);
            this.labelDaysHours.Name = "labelDaysHours";
            this.labelDaysHours.Size = new System.Drawing.Size(517, 57);
            this.labelDaysHours.TabIndex = 4;
            this.labelDaysHours.Text = "7d-3h-6m";
            this.labelDaysHours.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label37
            // 
            this.label37.Dock = System.Windows.Forms.DockStyle.Left;
            this.label37.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(1826, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(166, 57);
            this.label37.TabIndex = 7;
            this.label37.Text = "Beats:";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label37.Visible = false;
            // 
            // labelTotalBeats
            // 
            this.labelTotalBeats.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelTotalBeats.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalBeats.Location = new System.Drawing.Point(1992, 0);
            this.labelTotalBeats.Name = "labelTotalBeats";
            this.labelTotalBeats.Size = new System.Drawing.Size(222, 57);
            this.labelTotalBeats.TabIndex = 8;
            this.labelTotalBeats.Text = "112121";
            this.labelTotalBeats.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelTotalBeats.Visible = false;
            // 
            // panel39
            // 
            this.panel39.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel39.Location = new System.Drawing.Point(0, 69);
            this.panel39.Name = "panel39";
            this.panel39.Size = new System.Drawing.Size(2336, 25);
            this.panel39.TabIndex = 9;
            // 
            // panel45
            // 
            this.panel45.Controls.Add(this.panel55);
            this.panel45.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel45.Location = new System.Drawing.Point(0, 94);
            this.panel45.Name = "panel45";
            this.panel45.Size = new System.Drawing.Size(2336, 50);
            this.panel45.TabIndex = 11;
            // 
            // panel55
            // 
            this.panel55.Controls.Add(this.label9);
            this.panel55.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel55.Location = new System.Drawing.Point(0, 0);
            this.panel55.Name = "panel55";
            this.panel55.Size = new System.Drawing.Size(349, 50);
            this.panel55.TabIndex = 0;
            // 
            // label9
            // 
            this.label9.Dock = System.Windows.Forms.DockStyle.Left;
            this.label9.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(0, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(233, 50);
            this.label9.TabIndex = 0;
            this.label9.Text = "Trends";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel62
            // 
            this.panel62.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel62.Controls.Add(this.chartHR);
            this.panel62.Controls.Add(this.panel63);
            this.panel62.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel62.Location = new System.Drawing.Point(0, 144);
            this.panel62.Name = "panel62";
            this.panel62.Size = new System.Drawing.Size(2336, 465);
            this.panel62.TabIndex = 12;
            // 
            // chartHR
            // 
            this.chartHR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.chartHR.BorderlineColor = System.Drawing.Color.Black;
            this.chartHR.BorderlineWidth = 0;
            chartArea12.AxisX.IsLabelAutoFit = false;
            chartArea12.AxisX.LabelStyle.Font = new System.Drawing.Font("Verdana", 23F);
            chartArea12.AxisX.LineColor = System.Drawing.Color.DarkGray;
            chartArea12.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea12.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DarkGray;
            chartArea12.AxisX.Maximum = 250D;
            chartArea12.AxisX.Minimum = 100D;
            chartArea12.AxisX2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea12.AxisY.IsLabelAutoFit = false;
            chartArea12.AxisY.LabelStyle.Font = new System.Drawing.Font("Verdana", 23F);
            chartArea12.AxisY.LineColor = System.Drawing.Color.DarkGray;
            chartArea12.AxisY.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea12.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DarkGray;
            chartArea12.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea12.BorderColor = System.Drawing.Color.Transparent;
            chartArea12.BorderWidth = 0;
            chartArea12.Name = "ChartArea1";
            chartArea12.Position.Auto = false;
            chartArea12.Position.Height = 95F;
            chartArea12.Position.Width = 98F;
            chartArea12.Position.Y = 1F;
            this.chartHR.ChartAreas.Add(chartArea12);
            this.chartHR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartHR.Location = new System.Drawing.Point(127, 0);
            this.chartHR.Name = "chartHR";
            series14.BorderWidth = 2;
            series14.ChartArea = "ChartArea1";
            series14.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series14.Color = System.Drawing.Color.Silver;
            series14.Name = "HrMin";
            series14.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series15.BorderWidth = 2;
            series15.ChartArea = "ChartArea1";
            series15.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series15.Color = System.Drawing.Color.Gray;
            series15.Name = "HrMax";
            series15.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series16.BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.DiagonalLeft;
            series16.BackHatchStyle = System.Windows.Forms.DataVisualization.Charting.ChartHatchStyle.Cross;
            series16.BorderWidth = 3;
            series16.ChartArea = "ChartArea1";
            series16.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series16.Color = System.Drawing.Color.Black;
            series16.LegendText = "HR";
            series16.MarkerSize = 3;
            series16.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series16.Name = "Heart Rate";
            series16.Points.Add(dataPoint53);
            series16.Points.Add(dataPoint54);
            series16.Points.Add(dataPoint55);
            series16.Points.Add(dataPoint56);
            series16.Points.Add(dataPoint57);
            series16.Points.Add(dataPoint58);
            series16.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            this.chartHR.Series.Add(series14);
            this.chartHR.Series.Add(series15);
            this.chartHR.Series.Add(series16);
            this.chartHR.Size = new System.Drawing.Size(2207, 463);
            this.chartHR.TabIndex = 0;
            this.chartHR.Text = "chart1";
            // 
            // panel64
            // 
            this.panel64.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel64.Location = new System.Drawing.Point(0, 609);
            this.panel64.Name = "panel64";
            this.panel64.Size = new System.Drawing.Size(2336, 10);
            this.panel64.TabIndex = 13;
            // 
            // panel68
            // 
            this.panel68.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel68.Controls.Add(this.panel72);
            this.panel68.Controls.Add(this.panel69);
            this.panel68.Controls.Add(this.panel13);
            this.panel68.Controls.Add(this.panel3);
            this.panel68.Controls.Add(this.panel74);
            this.panel68.Controls.Add(this.panel76);
            this.panel68.Controls.Add(this.panel78);
            this.panel68.Controls.Add(this.panel80);
            this.panel68.Controls.Add(this.panel82);
            this.panel68.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel68.Location = new System.Drawing.Point(0, 619);
            this.panel68.Name = "panel68";
            this.panel68.Size = new System.Drawing.Size(2336, 734);
            this.panel68.TabIndex = 14;
            // 
            // panel82
            // 
            this.panel82.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel82.Location = new System.Drawing.Point(0, 0);
            this.panel82.Name = "panel82";
            this.panel82.Size = new System.Drawing.Size(2334, 10);
            this.panel82.TabIndex = 0;
            // 
            // panel80
            // 
            this.panel80.Controls.Add(this.chartTachy);
            this.panel80.Controls.Add(this.panel81);
            this.panel80.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel80.Location = new System.Drawing.Point(0, 10);
            this.panel80.Name = "panel80";
            this.panel80.Size = new System.Drawing.Size(2334, 90);
            this.panel80.TabIndex = 3;
            // 
            // panel81
            // 
            this.panel81.Controls.Add(this.labelGraphTachy);
            this.panel81.Controls.Add(this.label53);
            this.panel81.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel81.Location = new System.Drawing.Point(0, 0);
            this.panel81.Name = "panel81";
            this.panel81.Size = new System.Drawing.Size(178, 90);
            this.panel81.TabIndex = 0;
            // 
            // labelGraphTachy
            // 
            this.labelGraphTachy.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelGraphTachy.Font = new System.Drawing.Font("Verdana", 16F, System.Drawing.FontStyle.Bold);
            this.labelGraphTachy.Location = new System.Drawing.Point(0, 45);
            this.labelGraphTachy.Name = "labelGraphTachy";
            this.labelGraphTachy.Size = new System.Drawing.Size(178, 30);
            this.labelGraphTachy.TabIndex = 1;
            this.labelGraphTachy.Text = "999";
            this.labelGraphTachy.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label53
            // 
            this.label53.Dock = System.Windows.Forms.DockStyle.Top;
            this.label53.Font = new System.Drawing.Font("Verdana", 28F, System.Drawing.FontStyle.Bold);
            this.label53.Location = new System.Drawing.Point(0, 0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(178, 45);
            this.label53.TabIndex = 0;
            this.label53.Text = "Tachy";
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // chartTachy
            // 
            chartArea13.AxisX.IsLabelAutoFit = false;
            chartArea13.AxisX.LabelStyle.Enabled = false;
            chartArea13.AxisX.LineColor = System.Drawing.Color.DarkGray;
            chartArea13.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea13.AxisX.ScaleBreakStyle.BreakLineStyle = System.Windows.Forms.DataVisualization.Charting.BreakLineStyle.None;
            chartArea13.AxisX.ScaleBreakStyle.LineColor = System.Drawing.Color.White;
            chartArea13.AxisX.ScaleBreakStyle.Spacing = 1D;
            chartArea13.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea13.AxisY.IsLabelAutoFit = false;
            chartArea13.AxisY.LabelStyle.Enabled = false;
            chartArea13.AxisY.LineColor = System.Drawing.Color.DarkGray;
            chartArea13.AxisY.MajorGrid.Enabled = false;
            chartArea13.AxisY.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea13.AxisY.MajorTickMark.Enabled = false;
            chartArea13.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea13.BorderColor = System.Drawing.Color.DarkGray;
            chartArea13.InnerPlotPosition.Auto = false;
            chartArea13.InnerPlotPosition.Height = 80F;
            chartArea13.InnerPlotPosition.Width = 95F;
            chartArea13.InnerPlotPosition.X = 1F;
            chartArea13.InnerPlotPosition.Y = 1F;
            chartArea13.Name = "TachyEvents";
            this.chartTachy.ChartAreas.Add(chartArea13);
            this.chartTachy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartTachy.Location = new System.Drawing.Point(178, 0);
            this.chartTachy.Name = "chartTachy";
            series17.BorderWidth = 2;
            series17.ChartArea = "TachyEvents";
            series17.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series17.Color = System.Drawing.Color.Black;
            series17.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series17.IsVisibleInLegend = false;
            series17.MarkerSize = 3;
            series17.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series17.Name = "Series1";
            series17.Points.Add(dataPoint59);
            series17.Points.Add(dataPoint60);
            series17.Points.Add(dataPoint61);
            series17.Points.Add(dataPoint62);
            series17.SmartLabelStyle.Enabled = false;
            series17.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            this.chartTachy.Series.Add(series17);
            this.chartTachy.Size = new System.Drawing.Size(2156, 90);
            this.chartTachy.TabIndex = 1;
            this.chartTachy.Text = "chart2";
            // 
            // panel78
            // 
            this.panel78.Controls.Add(this.chartBrady);
            this.panel78.Controls.Add(this.panel79);
            this.panel78.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel78.Location = new System.Drawing.Point(0, 100);
            this.panel78.Name = "panel78";
            this.panel78.Size = new System.Drawing.Size(2334, 90);
            this.panel78.TabIndex = 4;
            // 
            // panel79
            // 
            this.panel79.Controls.Add(this.labelGraphBrady);
            this.panel79.Controls.Add(this.label32);
            this.panel79.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel79.Location = new System.Drawing.Point(0, 0);
            this.panel79.Name = "panel79";
            this.panel79.Size = new System.Drawing.Size(178, 90);
            this.panel79.TabIndex = 0;
            // 
            // labelGraphBrady
            // 
            this.labelGraphBrady.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelGraphBrady.Font = new System.Drawing.Font("Verdana", 16F, System.Drawing.FontStyle.Bold);
            this.labelGraphBrady.Location = new System.Drawing.Point(0, 45);
            this.labelGraphBrady.Name = "labelGraphBrady";
            this.labelGraphBrady.Size = new System.Drawing.Size(178, 30);
            this.labelGraphBrady.TabIndex = 2;
            this.labelGraphBrady.Text = "999";
            this.labelGraphBrady.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label32
            // 
            this.label32.Dock = System.Windows.Forms.DockStyle.Top;
            this.label32.Font = new System.Drawing.Font("Verdana", 28F, System.Drawing.FontStyle.Bold);
            this.label32.Location = new System.Drawing.Point(0, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(178, 45);
            this.label32.TabIndex = 0;
            this.label32.Text = "Brady";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // chartBrady
            // 
            chartArea14.AxisX.IsLabelAutoFit = false;
            chartArea14.AxisX.LabelStyle.Enabled = false;
            chartArea14.AxisX.LineColor = System.Drawing.Color.DarkGray;
            chartArea14.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea14.AxisX.ScaleBreakStyle.BreakLineStyle = System.Windows.Forms.DataVisualization.Charting.BreakLineStyle.None;
            chartArea14.AxisX.ScaleBreakStyle.LineColor = System.Drawing.Color.White;
            chartArea14.AxisX.ScaleBreakStyle.Spacing = 1D;
            chartArea14.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea14.AxisY.IsLabelAutoFit = false;
            chartArea14.AxisY.LabelStyle.Enabled = false;
            chartArea14.AxisY.LineColor = System.Drawing.Color.DarkGray;
            chartArea14.AxisY.MajorGrid.Enabled = false;
            chartArea14.AxisY.MajorTickMark.Enabled = false;
            chartArea14.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea14.BorderColor = System.Drawing.Color.DarkGray;
            chartArea14.InnerPlotPosition.Auto = false;
            chartArea14.InnerPlotPosition.Height = 80F;
            chartArea14.InnerPlotPosition.Width = 95F;
            chartArea14.InnerPlotPosition.X = 1F;
            chartArea14.InnerPlotPosition.Y = 1F;
            chartArea14.Name = "TachyEvents";
            this.chartBrady.ChartAreas.Add(chartArea14);
            this.chartBrady.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartBrady.Location = new System.Drawing.Point(178, 0);
            this.chartBrady.Name = "chartBrady";
            series18.ChartArea = "TachyEvents";
            series18.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series18.Color = System.Drawing.Color.Black;
            series18.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series18.IsVisibleInLegend = false;
            series18.MarkerSize = 3;
            series18.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series18.Name = "Series1";
            series18.Points.Add(dataPoint63);
            series18.Points.Add(dataPoint64);
            series18.Points.Add(dataPoint65);
            series18.Points.Add(dataPoint66);
            series18.SmartLabelStyle.Enabled = false;
            series18.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            this.chartBrady.Series.Add(series18);
            this.chartBrady.Size = new System.Drawing.Size(2156, 90);
            this.chartBrady.TabIndex = 1;
            this.chartBrady.Text = "chart1";
            // 
            // panel76
            // 
            this.panel76.Controls.Add(this.chartPause);
            this.panel76.Controls.Add(this.panel77);
            this.panel76.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel76.Location = new System.Drawing.Point(0, 190);
            this.panel76.Name = "panel76";
            this.panel76.Size = new System.Drawing.Size(2334, 90);
            this.panel76.TabIndex = 5;
            // 
            // panel77
            // 
            this.panel77.Controls.Add(this.labelGraphPause);
            this.panel77.Controls.Add(this.label34);
            this.panel77.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel77.Location = new System.Drawing.Point(0, 0);
            this.panel77.Name = "panel77";
            this.panel77.Size = new System.Drawing.Size(178, 90);
            this.panel77.TabIndex = 0;
            // 
            // labelGraphPause
            // 
            this.labelGraphPause.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelGraphPause.Font = new System.Drawing.Font("Verdana", 16F, System.Drawing.FontStyle.Bold);
            this.labelGraphPause.Location = new System.Drawing.Point(0, 45);
            this.labelGraphPause.Name = "labelGraphPause";
            this.labelGraphPause.Size = new System.Drawing.Size(178, 30);
            this.labelGraphPause.TabIndex = 2;
            this.labelGraphPause.Text = "999";
            this.labelGraphPause.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label34
            // 
            this.label34.Dock = System.Windows.Forms.DockStyle.Top;
            this.label34.Font = new System.Drawing.Font("Verdana", 28F, System.Drawing.FontStyle.Bold);
            this.label34.Location = new System.Drawing.Point(0, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(178, 45);
            this.label34.TabIndex = 0;
            this.label34.Text = "Pause";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // chartPause
            // 
            chartArea15.AxisX.IsLabelAutoFit = false;
            chartArea15.AxisX.LabelStyle.Enabled = false;
            chartArea15.AxisX.LineColor = System.Drawing.Color.DarkGray;
            chartArea15.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea15.AxisX.ScaleBreakStyle.BreakLineStyle = System.Windows.Forms.DataVisualization.Charting.BreakLineStyle.None;
            chartArea15.AxisX.ScaleBreakStyle.LineColor = System.Drawing.Color.White;
            chartArea15.AxisX.ScaleBreakStyle.Spacing = 1D;
            chartArea15.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea15.AxisY.IsLabelAutoFit = false;
            chartArea15.AxisY.LabelStyle.Enabled = false;
            chartArea15.AxisY.LineColor = System.Drawing.Color.DarkGray;
            chartArea15.AxisY.MajorGrid.Enabled = false;
            chartArea15.AxisY.MajorTickMark.Enabled = false;
            chartArea15.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea15.InnerPlotPosition.Auto = false;
            chartArea15.InnerPlotPosition.Height = 80F;
            chartArea15.InnerPlotPosition.Width = 95F;
            chartArea15.InnerPlotPosition.X = 1F;
            chartArea15.InnerPlotPosition.Y = 1F;
            chartArea15.Name = "TachyEvents";
            this.chartPause.ChartAreas.Add(chartArea15);
            this.chartPause.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartPause.Location = new System.Drawing.Point(178, 0);
            this.chartPause.Name = "chartPause";
            series19.ChartArea = "TachyEvents";
            series19.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series19.Color = System.Drawing.Color.Black;
            series19.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series19.IsVisibleInLegend = false;
            series19.MarkerSize = 3;
            series19.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series19.Name = "Series1";
            series19.Points.Add(dataPoint67);
            series19.Points.Add(dataPoint68);
            series19.Points.Add(dataPoint69);
            series19.Points.Add(dataPoint70);
            series19.SmartLabelStyle.Enabled = false;
            series19.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            this.chartPause.Series.Add(series19);
            this.chartPause.Size = new System.Drawing.Size(2156, 90);
            this.chartPause.TabIndex = 1;
            this.chartPause.Text = "chart1";
            // 
            // panel74
            // 
            this.panel74.Controls.Add(this.chartAfib);
            this.panel74.Controls.Add(this.panel75);
            this.panel74.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel74.Location = new System.Drawing.Point(0, 280);
            this.panel74.Name = "panel74";
            this.panel74.Size = new System.Drawing.Size(2334, 90);
            this.panel74.TabIndex = 6;
            // 
            // panel75
            // 
            this.panel75.Controls.Add(this.labelGraphAfib);
            this.panel75.Controls.Add(this.label36);
            this.panel75.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel75.Location = new System.Drawing.Point(0, 0);
            this.panel75.Name = "panel75";
            this.panel75.Size = new System.Drawing.Size(178, 90);
            this.panel75.TabIndex = 0;
            // 
            // labelGraphAfib
            // 
            this.labelGraphAfib.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelGraphAfib.Font = new System.Drawing.Font("Verdana", 16F, System.Drawing.FontStyle.Bold);
            this.labelGraphAfib.Location = new System.Drawing.Point(0, 45);
            this.labelGraphAfib.Name = "labelGraphAfib";
            this.labelGraphAfib.Size = new System.Drawing.Size(178, 30);
            this.labelGraphAfib.TabIndex = 2;
            this.labelGraphAfib.Text = "999";
            this.labelGraphAfib.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label36
            // 
            this.label36.Dock = System.Windows.Forms.DockStyle.Top;
            this.label36.Font = new System.Drawing.Font("Verdana", 28F, System.Drawing.FontStyle.Bold);
            this.label36.Location = new System.Drawing.Point(0, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(178, 45);
            this.label36.TabIndex = 0;
            this.label36.Text = "Afib";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // chartAfib
            // 
            chartArea16.AxisX.IsLabelAutoFit = false;
            chartArea16.AxisX.LabelStyle.Enabled = false;
            chartArea16.AxisX.LineColor = System.Drawing.Color.DarkGray;
            chartArea16.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea16.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DarkGray;
            chartArea16.AxisX.ScaleBreakStyle.BreakLineStyle = System.Windows.Forms.DataVisualization.Charting.BreakLineStyle.None;
            chartArea16.AxisX.ScaleBreakStyle.LineColor = System.Drawing.Color.White;
            chartArea16.AxisX.ScaleBreakStyle.Spacing = 1D;
            chartArea16.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea16.AxisY.IsLabelAutoFit = false;
            chartArea16.AxisY.LabelStyle.Enabled = false;
            chartArea16.AxisY.LineColor = System.Drawing.Color.DarkGray;
            chartArea16.AxisY.MajorGrid.Enabled = false;
            chartArea16.AxisY.MajorTickMark.Enabled = false;
            chartArea16.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea16.BorderColor = System.Drawing.Color.DarkGray;
            chartArea16.InnerPlotPosition.Auto = false;
            chartArea16.InnerPlotPosition.Height = 80F;
            chartArea16.InnerPlotPosition.Width = 95F;
            chartArea16.InnerPlotPosition.X = 1F;
            chartArea16.InnerPlotPosition.Y = 1F;
            chartArea16.Name = "TachyEvents";
            this.chartAfib.ChartAreas.Add(chartArea16);
            this.chartAfib.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartAfib.Location = new System.Drawing.Point(178, 0);
            this.chartAfib.Name = "chartAfib";
            series20.BorderWidth = 2;
            series20.ChartArea = "TachyEvents";
            series20.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series20.Color = System.Drawing.Color.Black;
            series20.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series20.IsVisibleInLegend = false;
            series20.MarkerSize = 3;
            series20.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series20.Name = "Series1";
            series20.Points.Add(dataPoint71);
            series20.Points.Add(dataPoint72);
            series20.Points.Add(dataPoint73);
            series20.Points.Add(dataPoint74);
            series20.SmartLabelStyle.Enabled = false;
            series20.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            this.chartAfib.Series.Add(series20);
            this.chartAfib.Size = new System.Drawing.Size(2156, 90);
            this.chartAfib.TabIndex = 1;
            this.chartAfib.Text = "chart1";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.chart1);
            this.panel3.Controls.Add(this.panel9);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 370);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(2334, 90);
            this.panel3.TabIndex = 9;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.label4);
            this.panel9.Controls.Add(this.label1);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(178, 90);
            this.panel9.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Font = new System.Drawing.Font("Verdana", 16F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(0, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(178, 30);
            this.label4.TabIndex = 2;
            this.label4.Text = "999";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Verdana", 28F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(178, 45);
            this.label1.TabIndex = 0;
            this.label1.Text = "PAC";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // chart1
            // 
            chartArea17.AxisX.IsLabelAutoFit = false;
            chartArea17.AxisX.LabelStyle.Enabled = false;
            chartArea17.AxisX.LineColor = System.Drawing.Color.DarkGray;
            chartArea17.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea17.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DarkGray;
            chartArea17.AxisX.ScaleBreakStyle.BreakLineStyle = System.Windows.Forms.DataVisualization.Charting.BreakLineStyle.None;
            chartArea17.AxisX.ScaleBreakStyle.LineColor = System.Drawing.Color.White;
            chartArea17.AxisX.ScaleBreakStyle.Spacing = 1D;
            chartArea17.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea17.AxisY.IsLabelAutoFit = false;
            chartArea17.AxisY.LabelStyle.Enabled = false;
            chartArea17.AxisY.LineColor = System.Drawing.Color.DarkGray;
            chartArea17.AxisY.MajorGrid.Enabled = false;
            chartArea17.AxisY.MajorTickMark.Enabled = false;
            chartArea17.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea17.BorderColor = System.Drawing.Color.DarkGray;
            chartArea17.InnerPlotPosition.Auto = false;
            chartArea17.InnerPlotPosition.Height = 80F;
            chartArea17.InnerPlotPosition.Width = 95F;
            chartArea17.InnerPlotPosition.X = 1F;
            chartArea17.InnerPlotPosition.Y = 1F;
            chartArea17.Name = "TachyEvents";
            this.chart1.ChartAreas.Add(chartArea17);
            this.chart1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chart1.Location = new System.Drawing.Point(178, 0);
            this.chart1.Name = "chart1";
            series21.BorderWidth = 2;
            series21.ChartArea = "TachyEvents";
            series21.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series21.Color = System.Drawing.Color.Black;
            series21.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series21.IsVisibleInLegend = false;
            series21.MarkerSize = 3;
            series21.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series21.Name = "Series1";
            series21.Points.Add(dataPoint75);
            series21.Points.Add(dataPoint76);
            series21.Points.Add(dataPoint77);
            series21.Points.Add(dataPoint78);
            series21.SmartLabelStyle.Enabled = false;
            series21.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            this.chart1.Series.Add(series21);
            this.chart1.Size = new System.Drawing.Size(2156, 90);
            this.chart1.TabIndex = 1;
            this.chart1.Text = "chart1";
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.chart2);
            this.panel13.Controls.Add(this.panel14);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel13.Location = new System.Drawing.Point(0, 460);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(2334, 90);
            this.panel13.TabIndex = 10;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.label14);
            this.panel14.Controls.Add(this.label10);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel14.Location = new System.Drawing.Point(0, 0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(178, 90);
            this.panel14.TabIndex = 0;
            // 
            // label14
            // 
            this.label14.Dock = System.Windows.Forms.DockStyle.Top;
            this.label14.Font = new System.Drawing.Font("Verdana", 16F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(0, 45);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(178, 30);
            this.label14.TabIndex = 2;
            this.label14.Text = "999";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.Dock = System.Windows.Forms.DockStyle.Top;
            this.label10.Font = new System.Drawing.Font("Verdana", 28F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(0, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(178, 45);
            this.label10.TabIndex = 0;
            this.label10.Text = "PVC";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // chart2
            // 
            chartArea18.AxisX.IsLabelAutoFit = false;
            chartArea18.AxisX.LabelStyle.Enabled = false;
            chartArea18.AxisX.LineColor = System.Drawing.Color.DarkGray;
            chartArea18.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea18.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DarkGray;
            chartArea18.AxisX.ScaleBreakStyle.BreakLineStyle = System.Windows.Forms.DataVisualization.Charting.BreakLineStyle.None;
            chartArea18.AxisX.ScaleBreakStyle.LineColor = System.Drawing.Color.White;
            chartArea18.AxisX.ScaleBreakStyle.Spacing = 1D;
            chartArea18.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea18.AxisY.IsLabelAutoFit = false;
            chartArea18.AxisY.LabelStyle.Enabled = false;
            chartArea18.AxisY.LineColor = System.Drawing.Color.DarkGray;
            chartArea18.AxisY.MajorGrid.Enabled = false;
            chartArea18.AxisY.MajorTickMark.Enabled = false;
            chartArea18.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea18.BorderColor = System.Drawing.Color.DarkGray;
            chartArea18.InnerPlotPosition.Auto = false;
            chartArea18.InnerPlotPosition.Height = 80F;
            chartArea18.InnerPlotPosition.Width = 95F;
            chartArea18.InnerPlotPosition.X = 1F;
            chartArea18.InnerPlotPosition.Y = 1F;
            chartArea18.Name = "TachyEvents";
            this.chart2.ChartAreas.Add(chartArea18);
            this.chart2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chart2.Location = new System.Drawing.Point(178, 0);
            this.chart2.Name = "chart2";
            series22.BorderWidth = 2;
            series22.ChartArea = "TachyEvents";
            series22.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series22.Color = System.Drawing.Color.Black;
            series22.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series22.IsVisibleInLegend = false;
            series22.MarkerSize = 3;
            series22.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series22.Name = "Series1";
            series22.Points.Add(dataPoint79);
            series22.Points.Add(dataPoint80);
            series22.Points.Add(dataPoint81);
            series22.Points.Add(dataPoint82);
            series22.SmartLabelStyle.Enabled = false;
            series22.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            this.chart2.Series.Add(series22);
            this.chart2.Size = new System.Drawing.Size(2156, 90);
            this.chart2.TabIndex = 1;
            this.chart2.Text = "chart2";
            // 
            // panel69
            // 
            this.panel69.Controls.Add(this.chartPace);
            this.panel69.Controls.Add(this.panel70);
            this.panel69.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel69.Location = new System.Drawing.Point(0, 550);
            this.panel69.Name = "panel69";
            this.panel69.Size = new System.Drawing.Size(2334, 90);
            this.panel69.TabIndex = 7;
            // 
            // panel70
            // 
            this.panel70.Controls.Add(this.labelGraphPace);
            this.panel70.Controls.Add(this.label38);
            this.panel70.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel70.Location = new System.Drawing.Point(0, 0);
            this.panel70.Name = "panel70";
            this.panel70.Size = new System.Drawing.Size(178, 90);
            this.panel70.TabIndex = 0;
            // 
            // labelGraphPace
            // 
            this.labelGraphPace.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelGraphPace.Font = new System.Drawing.Font("Verdana", 16F, System.Drawing.FontStyle.Bold);
            this.labelGraphPace.Location = new System.Drawing.Point(0, 45);
            this.labelGraphPace.Name = "labelGraphPace";
            this.labelGraphPace.Size = new System.Drawing.Size(178, 30);
            this.labelGraphPace.TabIndex = 2;
            this.labelGraphPace.Text = "999";
            this.labelGraphPace.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label38
            // 
            this.label38.Dock = System.Windows.Forms.DockStyle.Top;
            this.label38.Font = new System.Drawing.Font("Verdana", 28F, System.Drawing.FontStyle.Bold);
            this.label38.Location = new System.Drawing.Point(0, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(178, 45);
            this.label38.TabIndex = 0;
            this.label38.Text = "Pace";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // chartPace
            // 
            chartArea19.AxisX.IsLabelAutoFit = false;
            chartArea19.AxisX.LabelStyle.Enabled = false;
            chartArea19.AxisX.LineColor = System.Drawing.Color.DarkGray;
            chartArea19.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea19.AxisX.MinorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea19.AxisX.ScaleBreakStyle.BreakLineStyle = System.Windows.Forms.DataVisualization.Charting.BreakLineStyle.None;
            chartArea19.AxisX.ScaleBreakStyle.LineColor = System.Drawing.Color.White;
            chartArea19.AxisX.ScaleBreakStyle.Spacing = 1D;
            chartArea19.AxisX2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea19.AxisX2.MajorGrid.Enabled = false;
            chartArea19.AxisX2.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea19.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea19.AxisY.IsLabelAutoFit = false;
            chartArea19.AxisY.LabelStyle.Enabled = false;
            chartArea19.AxisY.LineColor = System.Drawing.Color.DarkGray;
            chartArea19.AxisY.MajorGrid.Enabled = false;
            chartArea19.AxisY.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea19.AxisY.MajorTickMark.Enabled = false;
            chartArea19.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea19.BorderColor = System.Drawing.Color.DarkGray;
            chartArea19.InnerPlotPosition.Auto = false;
            chartArea19.InnerPlotPosition.Height = 80F;
            chartArea19.InnerPlotPosition.Width = 95F;
            chartArea19.InnerPlotPosition.X = 1F;
            chartArea19.InnerPlotPosition.Y = 1F;
            chartArea19.Name = "TachyEvents";
            this.chartPace.ChartAreas.Add(chartArea19);
            this.chartPace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartPace.Location = new System.Drawing.Point(178, 0);
            this.chartPace.Name = "chartPace";
            series23.BorderWidth = 3;
            series23.ChartArea = "TachyEvents";
            series23.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series23.Color = System.Drawing.Color.Black;
            series23.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series23.IsVisibleInLegend = false;
            series23.MarkerSize = 3;
            series23.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series23.Name = "Series1";
            series23.Points.Add(dataPoint83);
            series23.Points.Add(dataPoint84);
            series23.Points.Add(dataPoint85);
            series23.Points.Add(dataPoint86);
            series23.SmartLabelStyle.Enabled = false;
            series23.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            this.chartPace.Series.Add(series23);
            this.chartPace.Size = new System.Drawing.Size(2156, 90);
            this.chartPace.TabIndex = 1;
            this.chartPace.Text = "chart1";
            // 
            // panel72
            // 
            this.panel72.Controls.Add(this.chartManual);
            this.panel72.Controls.Add(this.panel73);
            this.panel72.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel72.Location = new System.Drawing.Point(0, 640);
            this.panel72.Name = "panel72";
            this.panel72.Size = new System.Drawing.Size(2334, 90);
            this.panel72.TabIndex = 8;
            // 
            // panel73
            // 
            this.panel73.Controls.Add(this.labelGraphManual);
            this.panel73.Controls.Add(this.label54);
            this.panel73.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel73.Location = new System.Drawing.Point(0, 0);
            this.panel73.Name = "panel73";
            this.panel73.Size = new System.Drawing.Size(178, 90);
            this.panel73.TabIndex = 0;
            // 
            // labelGraphManual
            // 
            this.labelGraphManual.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelGraphManual.Font = new System.Drawing.Font("Verdana", 16F, System.Drawing.FontStyle.Bold);
            this.labelGraphManual.Location = new System.Drawing.Point(0, 45);
            this.labelGraphManual.Name = "labelGraphManual";
            this.labelGraphManual.Size = new System.Drawing.Size(178, 30);
            this.labelGraphManual.TabIndex = 2;
            this.labelGraphManual.Text = "999";
            this.labelGraphManual.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelGraphManual.Click += new System.EventHandler(this.labelGraphManual_Click);
            // 
            // label54
            // 
            this.label54.Dock = System.Windows.Forms.DockStyle.Top;
            this.label54.Font = new System.Drawing.Font("Verdana", 28F, System.Drawing.FontStyle.Bold);
            this.label54.Location = new System.Drawing.Point(0, 0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(178, 45);
            this.label54.TabIndex = 0;
            this.label54.Text = "Manual";
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // chartManual
            // 
            chartArea20.AxisX.IsLabelAutoFit = false;
            chartArea20.AxisX.LabelStyle.Enabled = false;
            chartArea20.AxisX.LineColor = System.Drawing.Color.DarkGray;
            chartArea20.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea20.AxisX.MajorTickMark.Enabled = false;
            chartArea20.AxisX.ScaleBreakStyle.BreakLineStyle = System.Windows.Forms.DataVisualization.Charting.BreakLineStyle.None;
            chartArea20.AxisX.ScaleBreakStyle.LineColor = System.Drawing.Color.White;
            chartArea20.AxisX.ScaleBreakStyle.Spacing = 1D;
            chartArea20.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea20.AxisY.IsMarksNextToAxis = false;
            chartArea20.AxisY.LabelStyle.Enabled = false;
            chartArea20.AxisY.LineColor = System.Drawing.Color.DarkGray;
            chartArea20.AxisY.MajorGrid.Enabled = false;
            chartArea20.AxisY.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea20.AxisY.MajorTickMark.Enabled = false;
            chartArea20.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea20.BorderColor = System.Drawing.Color.DarkGray;
            chartArea20.InnerPlotPosition.Auto = false;
            chartArea20.InnerPlotPosition.Height = 80F;
            chartArea20.InnerPlotPosition.Width = 95F;
            chartArea20.InnerPlotPosition.X = 1F;
            chartArea20.InnerPlotPosition.Y = 1F;
            chartArea20.Name = "TachyEvents";
            this.chartManual.ChartAreas.Add(chartArea20);
            this.chartManual.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartManual.Location = new System.Drawing.Point(178, 0);
            this.chartManual.Name = "chartManual";
            series24.BorderWidth = 3;
            series24.ChartArea = "TachyEvents";
            series24.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series24.Color = System.Drawing.Color.Black;
            series24.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series24.IsVisibleInLegend = false;
            series24.MarkerSize = 3;
            series24.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series24.Name = "Series1";
            series24.Points.Add(dataPoint87);
            series24.Points.Add(dataPoint88);
            series24.Points.Add(dataPoint89);
            series24.Points.Add(dataPoint90);
            series24.SmartLabelStyle.Enabled = false;
            series24.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            this.chartManual.Series.Add(series24);
            this.chartManual.Size = new System.Drawing.Size(2156, 90);
            this.chartManual.TabIndex = 1;
            this.chartManual.Text = "chart1";
            // 
            // panel83
            // 
            this.panel83.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel83.Location = new System.Drawing.Point(0, 1353);
            this.panel83.Name = "panel83";
            this.panel83.Size = new System.Drawing.Size(2336, 18);
            this.panel83.TabIndex = 15;
            // 
            // panel84
            // 
            this.panel84.Controls.Add(this.panel85);
            this.panel84.Controls.Add(this.label12);
            this.panel84.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel84.Location = new System.Drawing.Point(0, 1371);
            this.panel84.Name = "panel84";
            this.panel84.Size = new System.Drawing.Size(2336, 77);
            this.panel84.TabIndex = 16;
            // 
            // label12
            // 
            this.label12.Dock = System.Windows.Forms.DockStyle.Left;
            this.label12.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(0, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(709, 77);
            this.label12.TabIndex = 1;
            this.label12.Text = "Episodes Summary Table";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel85
            // 
            this.panel85.Controls.Add(this.label28);
            this.panel85.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel85.Location = new System.Drawing.Point(709, 0);
            this.panel85.Name = "panel85";
            this.panel85.Size = new System.Drawing.Size(809, 77);
            this.panel85.TabIndex = 2;
            // 
            // label28
            // 
            this.label28.Dock = System.Windows.Forms.DockStyle.Left;
            this.label28.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(0, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(686, 77);
            this.label28.TabIndex = 2;
            this.label28.Text = "Episodes Summary Graphs";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel86
            // 
            this.panel86.Controls.Add(this.panel87);
            this.panel86.Controls.Add(this.panel88);
            this.panel86.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel86.Location = new System.Drawing.Point(0, 1448);
            this.panel86.Name = "panel86";
            this.panel86.Size = new System.Drawing.Size(2336, 578);
            this.panel86.TabIndex = 17;
            // 
            // panel88
            // 
            this.panel88.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel88.Controls.Add(this.panel89);
            this.panel88.Controls.Add(this.panel90);
            this.panel88.Controls.Add(this.panel91);
            this.panel88.Controls.Add(this.panel92);
            this.panel88.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel88.Location = new System.Drawing.Point(0, 0);
            this.panel88.Name = "panel88";
            this.panel88.Size = new System.Drawing.Size(709, 578);
            this.panel88.TabIndex = 0;
            // 
            // panel92
            // 
            this.panel92.Controls.Add(this.label2);
            this.panel92.Controls.Add(this.label48);
            this.panel92.Controls.Add(this.label6);
            this.panel92.Controls.Add(this.label13);
            this.panel92.Controls.Add(this.label5);
            this.panel92.Controls.Add(this.label24);
            this.panel92.Controls.Add(this.label29);
            this.panel92.Controls.Add(this.label23);
            this.panel92.Controls.Add(this.label15);
            this.panel92.Controls.Add(this.label16);
            this.panel92.Controls.Add(this.label18);
            this.panel92.Controls.Add(this.label19);
            this.panel92.Controls.Add(this.label20);
            this.panel92.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel92.Location = new System.Drawing.Point(0, 0);
            this.panel92.Name = "panel92";
            this.panel92.Size = new System.Drawing.Size(157, 576);
            this.panel92.TabIndex = 0;
            // 
            // label20
            // 
            this.label20.Dock = System.Windows.Forms.DockStyle.Top;
            this.label20.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(0, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(157, 57);
            this.label20.TabIndex = 0;
            this.label20.Text = "Type";
            // 
            // label19
            // 
            this.label19.Dock = System.Windows.Forms.DockStyle.Top;
            this.label19.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(0, 57);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(157, 49);
            this.label19.TabIndex = 1;
            this.label19.Text = "Tachy";
            // 
            // label18
            // 
            this.label18.Dock = System.Windows.Forms.DockStyle.Top;
            this.label18.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(0, 106);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(157, 49);
            this.label18.TabIndex = 2;
            this.label18.Text = "Brady";
            // 
            // label16
            // 
            this.label16.Dock = System.Windows.Forms.DockStyle.Top;
            this.label16.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(0, 155);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(157, 49);
            this.label16.TabIndex = 3;
            this.label16.Text = "Pause";
            // 
            // label15
            // 
            this.label15.Dock = System.Windows.Forms.DockStyle.Top;
            this.label15.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(0, 204);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(157, 49);
            this.label15.TabIndex = 4;
            this.label15.Text = "Afib";
            // 
            // label23
            // 
            this.label23.Dock = System.Windows.Forms.DockStyle.Top;
            this.label23.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(0, 253);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(157, 49);
            this.label23.TabIndex = 10;
            this.label23.Text = "PAC";
            // 
            // label29
            // 
            this.label29.Dock = System.Windows.Forms.DockStyle.Top;
            this.label29.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(0, 302);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(157, 49);
            this.label29.TabIndex = 11;
            this.label29.Text = "PVC";
            // 
            // label24
            // 
            this.label24.Dock = System.Windows.Forms.DockStyle.Top;
            this.label24.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(0, 351);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(157, 12);
            this.label24.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(0, 363);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(157, 49);
            this.label5.TabIndex = 7;
            this.label5.Text = "Events";
            // 
            // label13
            // 
            this.label13.Dock = System.Windows.Forms.DockStyle.Top;
            this.label13.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(0, 412);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(157, 49);
            this.label13.TabIndex = 5;
            this.label13.Text = "Manual";
            // 
            // label6
            // 
            this.label6.Dock = System.Windows.Forms.DockStyle.Top;
            this.label6.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(0, 461);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(157, 49);
            this.label6.TabIndex = 8;
            this.label6.Text = "Period";
            // 
            // label48
            // 
            this.label48.Dock = System.Windows.Forms.DockStyle.Top;
            this.label48.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(0, 510);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(157, 12);
            this.label48.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(0, 522);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(157, 49);
            this.label2.TabIndex = 6;
            this.label2.Text = "Tech:";
            // 
            // panel91
            // 
            this.panel91.Controls.Add(this.labelUser);
            this.panel91.Controls.Add(this.label49);
            this.panel91.Controls.Add(this.label39);
            this.panel91.Controls.Add(this.labelEpMan);
            this.panel91.Controls.Add(this.labelEpTotal);
            this.panel91.Controls.Add(this.label25);
            this.panel91.Controls.Add(this.label42);
            this.panel91.Controls.Add(this.label41);
            this.panel91.Controls.Add(this.labelEpAfib);
            this.panel91.Controls.Add(this.labelEpPause);
            this.panel91.Controls.Add(this.labelEpBrady);
            this.panel91.Controls.Add(this.labelEpTachy);
            this.panel91.Controls.Add(this.label17);
            this.panel91.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel91.Location = new System.Drawing.Point(157, 0);
            this.panel91.Name = "panel91";
            this.panel91.Size = new System.Drawing.Size(213, 576);
            this.panel91.TabIndex = 1;
            // 
            // label17
            // 
            this.label17.Dock = System.Windows.Forms.DockStyle.Top;
            this.label17.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(0, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(213, 57);
            this.label17.TabIndex = 0;
            this.label17.Text = "Episodes";
            this.label17.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelEpTachy
            // 
            this.labelEpTachy.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpTachy.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEpTachy.Location = new System.Drawing.Point(0, 57);
            this.labelEpTachy.Name = "labelEpTachy";
            this.labelEpTachy.Size = new System.Drawing.Size(213, 49);
            this.labelEpTachy.TabIndex = 1;
            this.labelEpTachy.Text = "25";
            this.labelEpTachy.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelEpBrady
            // 
            this.labelEpBrady.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpBrady.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEpBrady.Location = new System.Drawing.Point(0, 106);
            this.labelEpBrady.Name = "labelEpBrady";
            this.labelEpBrady.Size = new System.Drawing.Size(213, 49);
            this.labelEpBrady.TabIndex = 2;
            this.labelEpBrady.Text = "35";
            this.labelEpBrady.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelEpPause
            // 
            this.labelEpPause.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpPause.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEpPause.Location = new System.Drawing.Point(0, 155);
            this.labelEpPause.Name = "labelEpPause";
            this.labelEpPause.Size = new System.Drawing.Size(213, 49);
            this.labelEpPause.TabIndex = 3;
            this.labelEpPause.Text = "10";
            this.labelEpPause.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelEpAfib
            // 
            this.labelEpAfib.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpAfib.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEpAfib.Location = new System.Drawing.Point(0, 204);
            this.labelEpAfib.Name = "labelEpAfib";
            this.labelEpAfib.Size = new System.Drawing.Size(213, 49);
            this.labelEpAfib.TabIndex = 4;
            this.labelEpAfib.Text = "15";
            this.labelEpAfib.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label41
            // 
            this.label41.Dock = System.Windows.Forms.DockStyle.Top;
            this.label41.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(0, 253);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(213, 49);
            this.label41.TabIndex = 12;
            this.label41.Text = "Pa";
            this.label41.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label42
            // 
            this.label42.Dock = System.Windows.Forms.DockStyle.Top;
            this.label42.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(0, 302);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(213, 49);
            this.label42.TabIndex = 13;
            this.label42.Text = "pv";
            this.label42.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label25
            // 
            this.label25.Dock = System.Windows.Forms.DockStyle.Top;
            this.label25.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(0, 351);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(213, 12);
            this.label25.TabIndex = 10;
            // 
            // labelEpTotal
            // 
            this.labelEpTotal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpTotal.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEpTotal.Location = new System.Drawing.Point(0, 363);
            this.labelEpTotal.Name = "labelEpTotal";
            this.labelEpTotal.Size = new System.Drawing.Size(213, 49);
            this.labelEpTotal.TabIndex = 8;
            this.labelEpTotal.Text = "15";
            this.labelEpTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelEpMan
            // 
            this.labelEpMan.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEpMan.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEpMan.Location = new System.Drawing.Point(0, 412);
            this.labelEpMan.Name = "labelEpMan";
            this.labelEpMan.Size = new System.Drawing.Size(213, 49);
            this.labelEpMan.TabIndex = 5;
            this.labelEpMan.Text = "15";
            this.labelEpMan.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label39
            // 
            this.label39.Dock = System.Windows.Forms.DockStyle.Top;
            this.label39.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(0, 461);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(213, 49);
            this.label39.TabIndex = 11;
            this.label39.Text = "15";
            this.label39.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label49
            // 
            this.label49.Dock = System.Windows.Forms.DockStyle.Top;
            this.label49.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(0, 510);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(213, 12);
            this.label49.TabIndex = 14;
            // 
            // labelUser
            // 
            this.labelUser.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelUser.Font = new System.Drawing.Font("Verdana", 27.75F);
            this.labelUser.Location = new System.Drawing.Point(0, 522);
            this.labelUser.Name = "labelUser";
            this.labelUser.Size = new System.Drawing.Size(213, 49);
            this.labelUser.TabIndex = 7;
            this.labelUser.Text = "XX";
            this.labelUser.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel90
            // 
            this.panel90.Controls.Add(this.label50);
            this.panel90.Controls.Add(this.labelDurPeriod);
            this.panel90.Controls.Add(this.labelDurMan);
            this.panel90.Controls.Add(this.labelDurTotal);
            this.panel90.Controls.Add(this.label26);
            this.panel90.Controls.Add(this.label45);
            this.panel90.Controls.Add(this.label43);
            this.panel90.Controls.Add(this.labelDurAfib);
            this.panel90.Controls.Add(this.labelDurPause);
            this.panel90.Controls.Add(this.labelDurBrady);
            this.panel90.Controls.Add(this.labelDurTachy);
            this.panel90.Controls.Add(this.label22);
            this.panel90.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel90.Location = new System.Drawing.Point(370, 0);
            this.panel90.Name = "panel90";
            this.panel90.Size = new System.Drawing.Size(212, 576);
            this.panel90.TabIndex = 2;
            // 
            // label22
            // 
            this.label22.Dock = System.Windows.Forms.DockStyle.Top;
            this.label22.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(0, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(212, 57);
            this.label22.TabIndex = 0;
            this.label22.Text = "Duration";
            this.label22.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDurTachy
            // 
            this.labelDurTachy.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurTachy.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDurTachy.Location = new System.Drawing.Point(0, 57);
            this.labelDurTachy.Name = "labelDurTachy";
            this.labelDurTachy.Size = new System.Drawing.Size(212, 49);
            this.labelDurTachy.TabIndex = 1;
            this.labelDurTachy.Text = "01:00:04";
            this.labelDurTachy.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDurBrady
            // 
            this.labelDurBrady.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurBrady.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDurBrady.Location = new System.Drawing.Point(0, 106);
            this.labelDurBrady.Name = "labelDurBrady";
            this.labelDurBrady.Size = new System.Drawing.Size(212, 49);
            this.labelDurBrady.TabIndex = 2;
            this.labelDurBrady.Text = "00:05:00";
            this.labelDurBrady.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDurPause
            // 
            this.labelDurPause.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurPause.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDurPause.Location = new System.Drawing.Point(0, 155);
            this.labelDurPause.Name = "labelDurPause";
            this.labelDurPause.Size = new System.Drawing.Size(212, 49);
            this.labelDurPause.TabIndex = 3;
            this.labelDurPause.Text = "00:07:05";
            this.labelDurPause.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDurAfib
            // 
            this.labelDurAfib.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurAfib.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDurAfib.Location = new System.Drawing.Point(0, 204);
            this.labelDurAfib.Name = "labelDurAfib";
            this.labelDurAfib.Size = new System.Drawing.Size(212, 49);
            this.labelDurAfib.TabIndex = 4;
            this.labelDurAfib.Text = "02:44:56";
            this.labelDurAfib.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label43
            // 
            this.label43.Dock = System.Windows.Forms.DockStyle.Top;
            this.label43.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(0, 253);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(212, 49);
            this.label43.TabIndex = 11;
            this.label43.Text = "Pa";
            // 
            // label45
            // 
            this.label45.Dock = System.Windows.Forms.DockStyle.Top;
            this.label45.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(0, 302);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(212, 49);
            this.label45.TabIndex = 12;
            this.label45.Text = "pv";
            // 
            // label26
            // 
            this.label26.Dock = System.Windows.Forms.DockStyle.Top;
            this.label26.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(0, 351);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(212, 12);
            this.label26.TabIndex = 10;
            // 
            // labelDurTotal
            // 
            this.labelDurTotal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurTotal.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDurTotal.Location = new System.Drawing.Point(0, 363);
            this.labelDurTotal.Name = "labelDurTotal";
            this.labelDurTotal.Size = new System.Drawing.Size(212, 49);
            this.labelDurTotal.TabIndex = 8;
            this.labelDurTotal.Text = "02:44:56";
            this.labelDurTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDurMan
            // 
            this.labelDurMan.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurMan.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDurMan.Location = new System.Drawing.Point(0, 412);
            this.labelDurMan.Name = "labelDurMan";
            this.labelDurMan.Size = new System.Drawing.Size(212, 49);
            this.labelDurMan.TabIndex = 5;
            this.labelDurMan.Text = "02:44:56";
            this.labelDurMan.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDurPeriod
            // 
            this.labelDurPeriod.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDurPeriod.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDurPeriod.Location = new System.Drawing.Point(0, 461);
            this.labelDurPeriod.Name = "labelDurPeriod";
            this.labelDurPeriod.Size = new System.Drawing.Size(212, 49);
            this.labelDurPeriod.TabIndex = 9;
            this.labelDurPeriod.Text = "02:44:56";
            this.labelDurPeriod.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label50
            // 
            this.label50.Dock = System.Windows.Forms.DockStyle.Top;
            this.label50.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(0, 510);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(212, 12);
            this.label50.TabIndex = 13;
            // 
            // panel89
            // 
            this.panel89.Controls.Add(this.labelPerMan);
            this.panel89.Controls.Add(this.labelPerTotal);
            this.panel89.Controls.Add(this.label40);
            this.panel89.Controls.Add(this.label47);
            this.panel89.Controls.Add(this.label46);
            this.panel89.Controls.Add(this.labelPerAfib);
            this.panel89.Controls.Add(this.labelPerPause);
            this.panel89.Controls.Add(this.labelPerBrady);
            this.panel89.Controls.Add(this.labelPerTachy);
            this.panel89.Controls.Add(this.label27);
            this.panel89.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel89.Location = new System.Drawing.Point(582, 0);
            this.panel89.Name = "panel89";
            this.panel89.Size = new System.Drawing.Size(110, 576);
            this.panel89.TabIndex = 3;
            // 
            // label27
            // 
            this.label27.Dock = System.Windows.Forms.DockStyle.Top;
            this.label27.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(0, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(110, 57);
            this.label27.TabIndex = 0;
            this.label27.Text = "%";
            this.label27.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPerTachy
            // 
            this.labelPerTachy.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerTachy.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPerTachy.Location = new System.Drawing.Point(0, 57);
            this.labelPerTachy.Name = "labelPerTachy";
            this.labelPerTachy.Size = new System.Drawing.Size(110, 49);
            this.labelPerTachy.TabIndex = 1;
            this.labelPerTachy.Text = "10";
            this.labelPerTachy.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPerBrady
            // 
            this.labelPerBrady.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerBrady.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPerBrady.Location = new System.Drawing.Point(0, 106);
            this.labelPerBrady.Name = "labelPerBrady";
            this.labelPerBrady.Size = new System.Drawing.Size(110, 49);
            this.labelPerBrady.TabIndex = 2;
            this.labelPerBrady.Text = "8";
            this.labelPerBrady.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPerPause
            // 
            this.labelPerPause.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerPause.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPerPause.Location = new System.Drawing.Point(0, 155);
            this.labelPerPause.Name = "labelPerPause";
            this.labelPerPause.Size = new System.Drawing.Size(110, 49);
            this.labelPerPause.TabIndex = 3;
            this.labelPerPause.Text = "0,01";
            this.labelPerPause.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPerAfib
            // 
            this.labelPerAfib.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerAfib.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPerAfib.Location = new System.Drawing.Point(0, 204);
            this.labelPerAfib.Name = "labelPerAfib";
            this.labelPerAfib.Size = new System.Drawing.Size(110, 49);
            this.labelPerAfib.TabIndex = 4;
            this.labelPerAfib.Text = "1.5";
            this.labelPerAfib.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label46
            // 
            this.label46.Dock = System.Windows.Forms.DockStyle.Top;
            this.label46.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(0, 253);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(110, 49);
            this.label46.TabIndex = 11;
            this.label46.Text = "pa";
            // 
            // label47
            // 
            this.label47.Dock = System.Windows.Forms.DockStyle.Top;
            this.label47.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(0, 302);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(110, 49);
            this.label47.TabIndex = 12;
            this.label47.Text = "pv";
            // 
            // label40
            // 
            this.label40.Dock = System.Windows.Forms.DockStyle.Top;
            this.label40.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(0, 351);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(110, 12);
            this.label40.TabIndex = 10;
            // 
            // labelPerTotal
            // 
            this.labelPerTotal.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerTotal.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPerTotal.Location = new System.Drawing.Point(0, 363);
            this.labelPerTotal.Name = "labelPerTotal";
            this.labelPerTotal.Size = new System.Drawing.Size(110, 49);
            this.labelPerTotal.TabIndex = 7;
            this.labelPerTotal.Text = "1.5";
            this.labelPerTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPerMan
            // 
            this.labelPerMan.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPerMan.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPerMan.Location = new System.Drawing.Point(0, 412);
            this.labelPerMan.Name = "labelPerMan";
            this.labelPerMan.Size = new System.Drawing.Size(110, 49);
            this.labelPerMan.TabIndex = 5;
            this.labelPerMan.Text = "1.5";
            this.labelPerMan.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel87
            // 
            this.panel87.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel87.Controls.Add(this.chartSummeryPie);
            this.panel87.Controls.Add(this.chartSummaryBar);
            this.panel87.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel87.Location = new System.Drawing.Point(709, 0);
            this.panel87.Name = "panel87";
            this.panel87.Size = new System.Drawing.Size(1627, 578);
            this.panel87.TabIndex = 1;
            // 
            // chartSummaryBar
            // 
            chartArea21.AxisX.IsLabelAutoFit = false;
            chartArea21.AxisX.LabelStyle.Enabled = false;
            chartArea21.AxisX.LabelStyle.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea21.AxisX.LineColor = System.Drawing.Color.DarkGray;
            chartArea21.AxisX.MajorGrid.Enabled = false;
            chartArea21.AxisX2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea21.AxisY.IsLabelAutoFit = false;
            chartArea21.AxisY.LabelStyle.Font = new System.Drawing.Font("Verdana", 27F);
            chartArea21.AxisY.LabelStyle.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea21.AxisY.LineColor = System.Drawing.Color.DarkGray;
            chartArea21.AxisY.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea21.AxisY.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea21.AxisY2.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea21.BorderWidth = 0;
            chartArea21.Name = "ChartArea1";
            this.chartSummaryBar.ChartAreas.Add(chartArea21);
            this.chartSummaryBar.Dock = System.Windows.Forms.DockStyle.Left;
            this.chartSummaryBar.Location = new System.Drawing.Point(0, 0);
            this.chartSummaryBar.Name = "chartSummaryBar";
            series25.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            series25.BorderWidth = 2;
            series25.ChartArea = "ChartArea1";
            series25.Color = System.Drawing.Color.Transparent;
            series25.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series25.IsVisibleInLegend = false;
            series25.Name = "Series1";
            dataPoint91.Color = System.Drawing.Color.Gainsboro;
            dataPoint91.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataPoint91.IsValueShownAsLabel = false;
            dataPoint91.IsVisibleInLegend = false;
            dataPoint91.Label = "Tachy";
            dataPoint91.LabelForeColor = System.Drawing.Color.Black;
            dataPoint92.Color = System.Drawing.Color.DarkGray;
            dataPoint92.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataPoint92.Label = "Brady";
            dataPoint93.Color = System.Drawing.Color.Gray;
            dataPoint93.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataPoint93.Label = "Pause";
            dataPoint94.Color = System.Drawing.Color.Silver;
            dataPoint94.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataPoint94.Label = "Afib";
            dataPoint95.Color = System.Drawing.Color.DimGray;
            dataPoint95.Font = new System.Drawing.Font("Verdana", 27.75F);
            dataPoint95.Label = "PAC";
            dataPoint96.Color = System.Drawing.Color.Silver;
            dataPoint96.Font = new System.Drawing.Font("Verdana", 27.75F);
            dataPoint96.Label = "PVC";
            dataPoint97.Color = System.Drawing.Color.Black;
            dataPoint97.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataPoint97.Label = "Manual";
            series25.Points.Add(dataPoint91);
            series25.Points.Add(dataPoint92);
            series25.Points.Add(dataPoint93);
            series25.Points.Add(dataPoint94);
            series25.Points.Add(dataPoint95);
            series25.Points.Add(dataPoint96);
            series25.Points.Add(dataPoint97);
            series25.SmartLabelStyle.MovingDirection = System.Windows.Forms.DataVisualization.Charting.LabelAlignmentStyles.Top;
            this.chartSummaryBar.Series.Add(series25);
            this.chartSummaryBar.Size = new System.Drawing.Size(1039, 576);
            this.chartSummaryBar.TabIndex = 0;
            this.chartSummaryBar.Text = "EventPie";
            this.chartSummaryBar.Click += new System.EventHandler(this.chartSummaryBar_Click);
            // 
            // chartSummeryPie
            // 
            chartArea22.Name = "ChartArea1";
            this.chartSummeryPie.ChartAreas.Add(chartArea22);
            this.chartSummeryPie.Dock = System.Windows.Forms.DockStyle.Left;
            this.chartSummeryPie.Location = new System.Drawing.Point(1039, 0);
            this.chartSummeryPie.Name = "chartSummeryPie";
            series26.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            series26.BorderWidth = 2;
            series26.ChartArea = "ChartArea1";
            series26.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Doughnut;
            series26.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series26.IsVisibleInLegend = false;
            series26.LabelForeColor = System.Drawing.Color.White;
            series26.Name = "Series1";
            dataPoint98.Color = System.Drawing.Color.Gainsboro;
            dataPoint98.Font = new System.Drawing.Font("Verdana", 28F, System.Drawing.FontStyle.Bold);
            dataPoint98.Label = "Tachy";
            dataPoint99.Color = System.Drawing.Color.DarkGray;
            dataPoint99.Font = new System.Drawing.Font("Verdana", 28F, System.Drawing.FontStyle.Bold);
            dataPoint99.Label = "Brady";
            dataPoint100.Color = System.Drawing.Color.Gray;
            dataPoint100.Font = new System.Drawing.Font("Verdana", 28F, System.Drawing.FontStyle.Bold);
            dataPoint100.Label = "Pause";
            dataPoint101.Color = System.Drawing.Color.Silver;
            dataPoint101.Font = new System.Drawing.Font("Verdana", 28F, System.Drawing.FontStyle.Bold);
            dataPoint101.IsValueShownAsLabel = true;
            dataPoint101.IsVisibleInLegend = false;
            dataPoint101.Label = "Afib";
            dataPoint101.LabelForeColor = System.Drawing.Color.White;
            dataPoint102.Color = System.Drawing.Color.Black;
            dataPoint102.Font = new System.Drawing.Font("Verdana", 28F, System.Drawing.FontStyle.Bold);
            dataPoint102.Label = "Manual";
            dataPoint103.Color = System.Drawing.Color.DimGray;
            dataPoint103.Font = new System.Drawing.Font("Verdana", 28F, System.Drawing.FontStyle.Bold);
            dataPoint103.Label = "PAC";
            dataPoint104.Color = System.Drawing.Color.DarkGray;
            dataPoint104.Font = new System.Drawing.Font("Verdana", 28F, System.Drawing.FontStyle.Bold);
            dataPoint104.Label = "PVC";
            series26.Points.Add(dataPoint98);
            series26.Points.Add(dataPoint99);
            series26.Points.Add(dataPoint100);
            series26.Points.Add(dataPoint101);
            series26.Points.Add(dataPoint102);
            series26.Points.Add(dataPoint103);
            series26.Points.Add(dataPoint104);
            series26.SmartLabelStyle.AllowOutsidePlotArea = System.Windows.Forms.DataVisualization.Charting.LabelOutsidePlotAreaStyle.Yes;
            this.chartSummeryPie.Series.Add(series26);
            this.chartSummeryPie.Size = new System.Drawing.Size(602, 576);
            this.chartSummeryPie.TabIndex = 3;
            this.chartSummeryPie.Text = "EventPie";
            // 
            // panel35
            // 
            this.panel35.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel35.Controls.Add(this.panel86);
            this.panel35.Controls.Add(this.panel84);
            this.panel35.Controls.Add(this.panel83);
            this.panel35.Controls.Add(this.panel68);
            this.panel35.Controls.Add(this.panel64);
            this.panel35.Controls.Add(this.panel62);
            this.panel35.Controls.Add(this.panel45);
            this.panel35.Controls.Add(this.panel39);
            this.panel35.Controls.Add(this.panel40);
            this.panel35.Controls.Add(this.panel101);
            this.panel35.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel35.Location = new System.Drawing.Point(0, 0);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(2338, 2031);
            this.panel35.TabIndex = 5;
            // 
            // panel63
            // 
            this.panel63.Controls.Add(this.label21);
            this.panel63.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel63.Location = new System.Drawing.Point(0, 0);
            this.panel63.Name = "panel63";
            this.panel63.Size = new System.Drawing.Size(127, 463);
            this.panel63.TabIndex = 1;
            // 
            // label21
            // 
            this.label21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label21.Font = new System.Drawing.Font("Verdana", 28F, System.Drawing.FontStyle.Bold);
            this.label21.Location = new System.Drawing.Point(0, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(127, 463);
            this.label21.TabIndex = 1;
            this.label21.Text = "HR";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label21.Click += new System.EventHandler(this.label21_Click);
            // 
            // CPrintMctForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(2390, 1253);
            this.Controls.Add(this.panelPrintArea1);
            this.Controls.Add(this.toolStrip1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "CPrintMctForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Print Study Events";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CPrintMctForm_FormClosing);
            this.Load += new System.EventHandler(this.CPrintMctForm_Load);
            this.Shown += new System.EventHandler(this.CPrintMctForm_Shown);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panelPrintHeader.ResumeLayout(false);
            this.panelPrintHeader.PerformLayout();
            this.panel56.ResumeLayout(false);
            this.panel58.ResumeLayout(false);
            this.panel59.ResumeLayout(false);
            this.panel61.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCenter)).EndInit();
            this.panelCardiacEVentReportNumber.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panelDateTimeofEventStrip.ResumeLayout(false);
            this.panel53.ResumeLayout(false);
            this.panel65.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel36.ResumeLayout(false);
            this.panel32.ResumeLayout(false);
            this.panel25.ResumeLayout(false);
            this.panel29.ResumeLayout(false);
            this.panel31.ResumeLayout(false);
            this.panel30.ResumeLayout(false);
            this.panel24.ResumeLayout(false);
            this.panel22.ResumeLayout(false);
            this.panel47.ResumeLayout(false);
            this.panel23.ResumeLayout(false);
            this.panel19.ResumeLayout(false);
            this.panel18.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panelSecPatBar.ResumeLayout(false);
            this.panel67.ResumeLayout(false);
            this.panel256.ResumeLayout(false);
            this.panel264.ResumeLayout(false);
            this.panel252.ResumeLayout(false);
            this.panel262.ResumeLayout(false);
            this.panel244.ResumeLayout(false);
            this.panel260.ResumeLayout(false);
            this.panel240.ResumeLayout(false);
            this.panel247.ResumeLayout(false);
            this.panel236.ResumeLayout(false);
            this.panel246.ResumeLayout(false);
            this.panel245.ResumeLayout(false);
            this.panelStudyProcedures.ResumeLayout(false);
            this.panelStudyProcedures.PerformLayout();
            this.panel158.ResumeLayout(false);
            this.panel28.ResumeLayout(false);
            this.panelSignatures.ResumeLayout(false);
            this.panel110.ResumeLayout(false);
            this.panel109.ResumeLayout(false);
            this.panel108.ResumeLayout(false);
            this.panel107.ResumeLayout(false);
            this.panel106.ResumeLayout(false);
            this.panel105.ResumeLayout(false);
            this.panel51.ResumeLayout(false);
            this.panelPrintArea1.ResumeLayout(false);
            this.panelPrintArea1.PerformLayout();
            this.panelEventsStat.ResumeLayout(false);
            this.panel40.ResumeLayout(false);
            this.panel45.ResumeLayout(false);
            this.panel55.ResumeLayout(false);
            this.panel62.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartHR)).EndInit();
            this.panel68.ResumeLayout(false);
            this.panel80.ResumeLayout(false);
            this.panel81.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartTachy)).EndInit();
            this.panel78.ResumeLayout(false);
            this.panel79.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartBrady)).EndInit();
            this.panel76.ResumeLayout(false);
            this.panel77.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartPause)).EndInit();
            this.panel74.ResumeLayout(false);
            this.panel75.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartAfib)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.panel13.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            this.panel69.ResumeLayout(false);
            this.panel70.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartPace)).EndInit();
            this.panel72.ResumeLayout(false);
            this.panel73.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartManual)).EndInit();
            this.panel84.ResumeLayout(false);
            this.panel85.ResumeLayout(false);
            this.panel86.ResumeLayout(false);
            this.panel88.ResumeLayout(false);
            this.panel92.ResumeLayout(false);
            this.panel91.ResumeLayout(false);
            this.panel90.ResumeLayout(false);
            this.panel89.ResumeLayout(false);
            this.panel87.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartSummaryBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartSummeryPie)).EndInit();
            this.panel35.ResumeLayout(false);
            this.panel63.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButtonPrint;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripButton toolStripClipboard1;
        private System.Windows.Forms.ToolStripButton toolStripEnterData;
        private System.Windows.Forms.ToolStripButton toolStripClipboard2;
        private System.Windows.Forms.ToolStripButton toolStripGenPages;
        private System.Windows.Forms.Panel panelPrintHeader;
        private System.Windows.Forms.Label labelPage1;
        private System.Windows.Forms.Panel panel56;
        private System.Windows.Forms.Panel panel57;
        private System.Windows.Forms.Panel panel58;
        private System.Windows.Forms.Label labelCenter2;
        private System.Windows.Forms.Panel panel59;
        private System.Windows.Forms.Label labelCenter1;
        private System.Windows.Forms.Panel panel60;
        private System.Windows.Forms.Panel panel61;
        private System.Windows.Forms.PictureBox pictureBoxCenter;
        private System.Windows.Forms.Panel panelCardiacEVentReportNumber;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panelDateTimeofEventStrip;
        private System.Windows.Forms.Panel panel52;
        private System.Windows.Forms.Panel panel53;
        private System.Windows.Forms.Panel panel66;
        private System.Windows.Forms.Panel panel65;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label labelSingleEventReportDate;
        private System.Windows.Forms.Panel panel54;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel49;
        private System.Windows.Forms.Panel panel42;
        private System.Windows.Forms.Panel panel36;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.Label labelPatID;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Label labelPatIDHeader;
        private System.Windows.Forms.Panel panel41;
        private System.Windows.Forms.Panel panelVert4;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.Label labelPhone2;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Label labelPhone1;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Label labelPhoneHeader;
        private System.Windows.Forms.Panel panel34;
        private System.Windows.Forms.Panel panelVert3;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Label labelZipCity;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Label labelAddress;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Label labelAddressHeader;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Panel panelVert2;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Label labelAgeGender;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Label labelDateOfBirth;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Label labelDOBheader;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panelVert1;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label labelPatientNameHeader;
        private System.Windows.Forms.Panel panelHorSepPatDet;
        private System.Windows.Forms.Panel panelSecPatBar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel67;
        private System.Windows.Forms.Panel panel259;
        private System.Windows.Forms.Panel panel258;
        private System.Windows.Forms.Panel panel256;
        private System.Windows.Forms.Panel panel264;
        private System.Windows.Forms.Label labelClientHeader;
        private System.Windows.Forms.Panel panel254;
        private System.Windows.Forms.Panel panelVert9;
        private System.Windows.Forms.Panel panel252;
        private System.Windows.Forms.Panel panel262;
        private System.Windows.Forms.Label labelRefPhysHeader;
        private System.Windows.Forms.Panel panel250;
        private System.Windows.Forms.Panel panelVert8;
        private System.Windows.Forms.Panel panel244;
        private System.Windows.Forms.Panel panel260;
        private System.Windows.Forms.Label labelPhysHeader;
        private System.Windows.Forms.Panel panel242;
        private System.Windows.Forms.Panel panelVert7;
        private System.Windows.Forms.Panel panel240;
        private System.Windows.Forms.Panel panel247;
        private System.Windows.Forms.Label labelSerialNrHeader;
        private System.Windows.Forms.Panel panel239;
        private System.Windows.Forms.Panel panelVert6;
        private System.Windows.Forms.Panel panel237;
        private System.Windows.Forms.Panel panel236;
        private System.Windows.Forms.Panel panel246;
        private System.Windows.Forms.Label labelStartDate;
        private System.Windows.Forms.Panel panel245;
        private System.Windows.Forms.Label labelStartDateHeader;
        private System.Windows.Forms.Panel panel235;
        private System.Windows.Forms.Panel panel231;
        private System.Windows.Forms.Panel panelStudyProcedures;
        private System.Windows.Forms.Panel panelLineUnderSampl1;
        private System.Windows.Forms.Panel panelSignatures;
        private System.Windows.Forms.Panel panel110;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Panel panel109;
        private System.Windows.Forms.Label labelDateReportSign;
        private System.Windows.Forms.Panel panel108;
        private System.Windows.Forms.Label labelPhysSignLine;
        private System.Windows.Forms.Panel panel107;
        private System.Windows.Forms.Label labelPhysSign;
        private System.Windows.Forms.Panel panel106;
        private System.Windows.Forms.Label labelPhysPrintName;
        private System.Windows.Forms.Panel panel105;
        private System.Windows.Forms.Label labelPhysNameReportPrint;
        private System.Windows.Forms.Panel panel104;
        private System.Windows.Forms.Panel panel103;
        private System.Windows.Forms.Panel panel51;
        private System.Windows.Forms.Label labelPage;
        private System.Windows.Forms.Label labelPage1of;
        private System.Windows.Forms.Label labelPageOf;
        private System.Windows.Forms.Label labelPage1xOfX;
        private System.Windows.Forms.Label labelPrintDate1Bottom;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panelPrintArea1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.Panel panel158;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.Panel panel46;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Panel panel126;
        private System.Windows.Forms.Label labelNote1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label labelReportNr;
        private System.Windows.Forms.Label labelDeviceError;
        private System.Windows.Forms.Label labelNote2;
        private System.Windows.Forms.Label labelCardiacEventReportNr;
        private System.Windows.Forms.Panel panel47;
        private System.Windows.Forms.Label labelCountry;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Panel panelEventsStat;
        private System.Windows.Forms.Label labelPhysicianName;
        private System.Windows.Forms.Label labelRefPhysicianName;
        private System.Windows.Forms.Label labelClientName;
        private System.Windows.Forms.Label labelSerialNr;
        private System.Windows.Forms.Label labelPatientLastName;
        private System.Windows.Forms.Label labelPatientFirstName;
        private System.Windows.Forms.Label labelStudyNr;
        private System.Windows.Forms.Label labelPhysicianTel;
        private System.Windows.Forms.Label labelRefPhysicianTel;
        private System.Windows.Forms.Label labelClientTel;
        private System.Windows.Forms.Label labelSnrError;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel35;
        private System.Windows.Forms.Panel panel86;
        private System.Windows.Forms.Panel panel87;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartSummeryPie;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartSummaryBar;
        private System.Windows.Forms.Panel panel88;
        private System.Windows.Forms.Panel panel89;
        private System.Windows.Forms.Label labelPerMan;
        private System.Windows.Forms.Label labelPerTotal;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label labelPerAfib;
        private System.Windows.Forms.Label labelPerPause;
        private System.Windows.Forms.Label labelPerBrady;
        private System.Windows.Forms.Label labelPerTachy;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Panel panel90;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label labelDurPeriod;
        private System.Windows.Forms.Label labelDurMan;
        private System.Windows.Forms.Label labelDurTotal;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label labelDurAfib;
        private System.Windows.Forms.Label labelDurPause;
        private System.Windows.Forms.Label labelDurBrady;
        private System.Windows.Forms.Label labelDurTachy;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Panel panel91;
        private System.Windows.Forms.Label labelUser;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label labelEpMan;
        private System.Windows.Forms.Label labelEpTotal;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label labelEpAfib;
        private System.Windows.Forms.Label labelEpPause;
        private System.Windows.Forms.Label labelEpBrady;
        private System.Windows.Forms.Label labelEpTachy;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel panel92;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Panel panel84;
        private System.Windows.Forms.Panel panel85;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel83;
        private System.Windows.Forms.Panel panel68;
        private System.Windows.Forms.Panel panel72;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartManual;
        private System.Windows.Forms.Panel panel73;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label labelGraphManual;
        private System.Windows.Forms.Panel panel69;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartPace;
        private System.Windows.Forms.Panel panel70;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label labelGraphPace;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart2;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel74;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartAfib;
        private System.Windows.Forms.Panel panel75;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label labelGraphAfib;
        private System.Windows.Forms.Panel panel76;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartPause;
        private System.Windows.Forms.Panel panel77;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label labelGraphPause;
        private System.Windows.Forms.Panel panel78;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartBrady;
        private System.Windows.Forms.Panel panel79;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label labelGraphBrady;
        private System.Windows.Forms.Panel panel80;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartTachy;
        private System.Windows.Forms.Panel panel81;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label labelGraphTachy;
        private System.Windows.Forms.Panel panel82;
        private System.Windows.Forms.Panel panel64;
        private System.Windows.Forms.Panel panel62;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartHR;
        private System.Windows.Forms.Panel panel45;
        private System.Windows.Forms.Panel panel55;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel39;
        private System.Windows.Forms.Panel panel40;
        private System.Windows.Forms.Label labelTotalBeats;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label labelDaysHours;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label labelFirstDate;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Panel panel101;
        private System.Windows.Forms.Panel panel63;
        private System.Windows.Forms.Label label21;
    }
}