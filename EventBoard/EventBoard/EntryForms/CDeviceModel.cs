﻿using Program_Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventboardEntryForms
{
    public enum DDeviceModelVars
    {
        // primary keys
        ModelLabel = 0,
        // variables
        Manufacturer,
        ModelType,
        DeviceColor,
        ServiceInterval,
        ManufacturerAddress,
        ManufacturerZipCode,
        ManufacturerStreetNr,
        ManufacturersState,
        ManufacturerCity,
        ManufacturerCountryCode,
        ManufacturerPhone1,
        ManufacturerPhone2,
        ManufacturerCell,
        ManufacturerSkype,
        ManufacturerEmail,
        ManufacturerWebSite,
        OperatingModesSet,
        DefaultOperatingMode,
        DefaultChannelsLabels,
        DefaultChannelUnits,
        DefaultInvertChannelsMask,

        NrSqlVars       // keep as last
    };


    public class CDeviceModel:CSqlDataTableRow
    {
        public string _mModelLabel;
        public string _mManufacturer;
        public string _mModelType;
        public string _mDeviceColor;
        public UInt16 _mServiceIntervalDays;
        public string _mManufacturerAddress;
        public string _mManufacturerZipCode;
        public string _mManufacturerStreetNr;
        public string _mManufacturerState;
        public string _mManufacturerCity;
        public string _mManufacturerCountryCode;
        public string _mManufacturerPhone1;
        public string _mManufacturerPhone2;
        public string _mManufacturerCell;
        public string _mManufacturerSkype;
        public string _mManufacturerEmail;
        public string _mManufacturerWebSite;

        public CStringSet _mOperatingModesSet;
        public string _mDefaultOperatingMode;
        public string _mDefaultChannelsLabels;
        public string _mDefaultChannelsUnits;
        public UInt32 _mDefaultInvertChannelsMask;

        public CDeviceModel(CSqlDBaseConnection ASqlConnection) : base(ASqlConnection, "d_devicemodel")
        {
            try
            {
                UInt64 maskPrimary = CSqlDataTableRow.sGetMask((UInt16)DDeviceModelVars.ModelLabel);
                UInt64 maskValid = CSqlDataTableRow.sGetMaskRange((UInt16)0, (UInt16)DDeviceModelVars.NrSqlVars - 1);// mGetValidMask(false);

                _mOperatingModesSet = new CStringSet();

                mInitTableMasks(maskPrimary, maskValid);  // set primairyInsertFlags

                mClear();
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Init ", ex);
            }
        }
        public override CSqlDataTableRow mCreateNewRow()
        {
            return new CDeviceModel(mGetSqlConnection());
        }
        public override bool mbCopyTo(CSqlDataTableRow ATo) // derived  public override void mbCopyTo() must call base.mbCopyTo(ATo)
        {
            bool bOk = false;

            if (ATo != null)
            {
                CDeviceModel to = ATo as CDeviceModel;

                if (to != null)
                {
                    bOk = base.mbCopyTo(ATo);
                    to._mModelLabel = _mModelLabel;
                    to._mManufacturer = _mManufacturer;
                    to._mModelType = _mModelType;
                    to._mDeviceColor = _mDeviceColor;
                    to._mServiceIntervalDays = _mServiceIntervalDays;
                    to._mManufacturerAddress = _mManufacturerAddress;
                    to._mManufacturerZipCode = _mManufacturerZipCode;
                    to._mManufacturerStreetNr = _mManufacturerStreetNr;
                    to._mManufacturerState = _mManufacturerState;
                    to._mManufacturerCity = _mManufacturerCity;
                    to._mManufacturerCountryCode = _mManufacturerCountryCode;
                    to._mManufacturerPhone1 = _mManufacturerPhone1;
                    to._mManufacturerPhone2 = _mManufacturerPhone2;
                    to._mManufacturerCell = _mManufacturerCell;
                    to._mManufacturerSkype = _mManufacturerSkype;
                    to._mManufacturerEmail = _mManufacturerEmail;
                    to._mManufacturerWebSite = _mManufacturerWebSite;

                    to._mOperatingModesSet.mSetSet(_mOperatingModesSet);
                    to._mDefaultOperatingMode = _mDefaultOperatingMode;
                    to._mDefaultChannelsLabels = _mDefaultChannelsLabels;
                    to._mDefaultChannelsUnits = _mDefaultChannelsUnits;
                    to._mDefaultInvertChannelsMask = _mDefaultInvertChannelsMask;
                }
            }
            return bOk;
        }

        public override void mClear()
        {
            base.mClear();
            _mModelLabel = "";
            _mManufacturer = "";
            _mModelType = "";
            _mDeviceColor = "";
            _mServiceIntervalDays = 0;
            _mManufacturerAddress = "";
            _mManufacturerZipCode = "";
            _mManufacturerStreetNr = "";
            _mManufacturerState = "";
            _mManufacturerCity = "";
            _mManufacturerCountryCode = "";
            _mManufacturerPhone1 = "";
            _mManufacturerPhone2 = "";
            _mManufacturerCell = "";
            _mManufacturerSkype = "";
            _mManufacturerEmail = "";
            _mManufacturerWebSite = "";

            _mOperatingModesSet.mClear();
            _mDefaultOperatingMode = "";
            _mDefaultChannelsLabels = "";
            _mDefaultChannelsUnits = "";
            _mDefaultInvertChannelsMask = 0;

    }
        public override DSqlDataType mVarGetSet(DSqlDataCmd ACmd, UInt16 AVarIndex, ref string AVarSqlName, ref string AStringValue, out bool ArbValid)
        {
                switch ((DDeviceModelVars)AVarIndex)
            {
                case DDeviceModelVars.ModelLabel:
                    return mSqlGetSetString(ref _mModelLabel, "ModelLabel", ACmd, ref AVarSqlName, ref AStringValue, 32, out ArbValid);
                case DDeviceModelVars.Manufacturer:
                    return mSqlGetSetString(ref _mManufacturer, "Manufacturer", ACmd, ref AVarSqlName, ref AStringValue, 64, out ArbValid);
                case DDeviceModelVars.ModelType:
                    return mSqlGetSetString(ref _mModelType, "ModelType", ACmd, ref AVarSqlName, ref AStringValue, 32, out ArbValid);
                case DDeviceModelVars.DeviceColor:
                    return mSqlGetSetString(ref _mDeviceColor, "DeviceColor", ACmd, ref AVarSqlName, ref AStringValue, 32, out ArbValid);
                case DDeviceModelVars.ServiceInterval:
                    return mSqlGetSetUInt16(ref _mServiceIntervalDays, "ServiceInterval", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DDeviceModelVars.ManufacturerAddress:
                    return mSqlGetSetString(ref _mManufacturerAddress, "ManufacturerAddress", ACmd, ref AVarSqlName, ref AStringValue, 250, out ArbValid);
                case DDeviceModelVars.ManufacturerZipCode:
                    return mSqlGetSetString(ref _mManufacturerZipCode, "ManufacturerZipCode", ACmd, ref AVarSqlName, ref AStringValue, 16, out ArbValid);
                case DDeviceModelVars.ManufacturerStreetNr:
                    return mSqlGetSetString(ref _mManufacturerStreetNr, "ManufacturerStreetNr", ACmd, ref AVarSqlName, ref AStringValue, 8, out ArbValid);
                case DDeviceModelVars.ManufacturersState:
                    return mSqlGetSetString(ref _mManufacturerState, "ManufacturersState", ACmd, ref AVarSqlName, ref AStringValue, 48, out ArbValid);
                case DDeviceModelVars.ManufacturerCity:
                    return mSqlGetSetString(ref _mManufacturerCity, "ManufacturerCity", ACmd, ref AVarSqlName, ref AStringValue, 48, out ArbValid);
                case DDeviceModelVars.ManufacturerCountryCode:
                    return mSqlGetSetString(ref _mManufacturerCountryCode, "ManufacturerCountryCode", ACmd, ref AVarSqlName, ref AStringValue, 4, out ArbValid);
                case DDeviceModelVars.ManufacturerPhone1:
                    return mSqlGetSetString(ref _mManufacturerPhone1, "ManufacturerPhone1", ACmd, ref AVarSqlName, ref AStringValue, 32, out ArbValid);
                case DDeviceModelVars.ManufacturerPhone2:
                    return mSqlGetSetString(ref _mManufacturerPhone2, "ManufacturerPhone2", ACmd, ref AVarSqlName, ref AStringValue, 32, out ArbValid);
                case DDeviceModelVars.ManufacturerCell:
                    return mSqlGetSetString(ref _mManufacturerCell, "ManufacturerCell", ACmd, ref AVarSqlName, ref AStringValue, 32, out ArbValid);
                case DDeviceModelVars.ManufacturerSkype:
                    return mSqlGetSetString(ref _mManufacturerSkype, "ManufacturerSkype", ACmd, ref AVarSqlName, ref AStringValue, 64, out ArbValid);
                case DDeviceModelVars.ManufacturerEmail:
                    return mSqlGetSetString(ref _mManufacturerEmail, "ManufacturerEmail", ACmd, ref AVarSqlName, ref AStringValue, 128, out ArbValid);
                case DDeviceModelVars.ManufacturerWebSite:
                    return mSqlGetSetString(ref _mManufacturerWebSite, "ManufacturerWebSite", ACmd, ref AVarSqlName, ref AStringValue, 128, out ArbValid);
                case DDeviceModelVars.OperatingModesSet:
                    return mSqlGetSetStringSet(ref _mOperatingModesSet, "OperatingModesSet", ACmd, ref AVarSqlName, ref AStringValue, 128, out ArbValid);
                case DDeviceModelVars.DefaultOperatingMode:
                    return mSqlGetSetString(ref _mDefaultOperatingMode, "DefaultOperatingMode", ACmd, ref AVarSqlName, ref AStringValue, 16, out ArbValid);
                case DDeviceModelVars.DefaultChannelsLabels:
                    return mSqlGetSetString(ref _mDefaultChannelsLabels, "DefaultChannelsLabels", ACmd, ref AVarSqlName, ref AStringValue, 128, out ArbValid);
                case DDeviceModelVars.DefaultChannelUnits:
                    return mSqlGetSetString(ref _mDefaultChannelsUnits, "DefaultChannelsUnits", ACmd, ref AVarSqlName, ref AStringValue, 128, out ArbValid);
                case DDeviceModelVars.DefaultInvertChannelsMask:
                    return mSqlGetSetUInt32(ref _mDefaultInvertChannelsMask, "DefaultInvertChannelsMask", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
            }
            return base.mVarGetSet(ACmd, AVarIndex, ref AVarSqlName, ref AStringValue, out ArbValid);
        }

        static public string sGetDeviceModelLabel( UInt32 AModelIX)
        {
            string modelStr = "";

            try
            {
                CSqlDBaseConnection dbConn = Event_Base.CDvtmsData.sGetDBaseConnection();

                CDeviceModel model = new CDeviceModel(dbConn);

                if (AModelIX > 0)
                {
                    //                    UInt64 mLoadMask = _mInfo.mMaskValid;
                    UInt64 loadMask = model.mMaskIndexKey
                            | CSqlDataTableRow.sGetMask((UInt16)DDeviceModelVars.ModelLabel)
                            | CSqlDataTableRow.sGetMask((UInt16)DDeviceModelVars.ServiceInterval);

                    if (model.mbDoSqlSelectIndex(AModelIX, loadMask))
                    {
                        modelStr = model._mModelLabel;
                    }

                }
            }

            catch (Exception ex)
            {
                CProgram.sLogException("Failed load device model " + AModelIX, ex);
            }
            return modelStr;
        }

    }
}
