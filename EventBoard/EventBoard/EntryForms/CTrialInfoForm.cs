﻿using Event_Base;
using EventBoard;
using Program_Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventboardEntryForms
{
    public partial class CTrialInfoForm : Form
    {
        public CTrialInfo _mInfo;
        private List<CSqlDataTableRow> _mList;
        private CTrialInfo _mSearchInfo = null;
        private bool _mbLoadListError = false;
        private bool _mbEnableSelect;
        public UInt32 _mSelectedIndex = 0;
        public bool _mbSelected = false;

        public CClientInfo _mClient;
        public List<CSqlDataTableRow> _mClientList;


        public CTrialInfoForm(CTrialInfo ATrial, UInt32 APreSelectClientIX, bool AbEnableSelect)
        {
            try
            {
                bool bProgrammer = CLicKeyDev.sbDeviceIsProgrammer();

                CDvtmsData.sbInitData();

                _mList = new List<CSqlDataTableRow>();              
                _mInfo = new CTrialInfo(CDvtmsData.sGetDBaseConnection());
                _mbEnableSelect = AbEnableSelect;

                if (_mInfo == null || _mList == null)
                {
                    CProgram.sLogLine(" CLientInfoForm() failed to init data");
                    Close();
                }
                else
                {
                    _mClient = new CClientInfo(CDvtmsData.sGetDBaseConnection());
                    _mClientList = new List<CSqlDataTableRow>();

                    bool bError;
                    _mClient.mbDoSqlSelectList(out bError, out _mClientList, CSqlDataTableRow.sGetMask((UInt16)DClientInfoVars.Label),
                                0, true, "", DSqlSort.Ascending, (UInt16)DClientInfoVars.Label);

                    _mInfo.mbActive = true;
                    if (ATrial != null)
                    {
                        _mInfo.mbCopyFrom(ATrial);
                        _mSelectedIndex = ATrial.mIndex_KEY;
                    }
                    else if(APreSelectClientIX > 0)
                    {
                        _mInfo.mIndex_KEY = APreSelectClientIX;
                        _mSelectedIndex = APreSelectClientIX;

                    }
                    InitializeComponent();

                    //Width = 1600;   // Nicer size then minimum 1240
                    buttonSelect.Visible = AbEnableSelect;
                    buttonRandom.Visible = CLicKeyDev.sbDeviceIsProgrammer();
                    mLoadList();
                    mSetValues();
                    mbCheckForm(ATrial == null ? "New" : "Edit");
                    labelStudyPermissions.Visible = bProgrammer;
                    textBoxStudyPermissions.Visible = bProgrammer;
                    int nList = CDvtmsData.sFillStudyPermissionsComboBox(comboBoxLocation, "---", _mInfo._mStudyPermissions, CDvtmsData._sStudyPermSiteGroup);


                    Text = CProgram.sMakeProgTitle("Trial", false, true);

                    labelSite.Text = CDvtmsData._sStudyPermSiteText + ":";
                    ColumnPermissions.HeaderText = CDvtmsData._sStudyPermSiteText;

                }
                labelSelected.Visible = CLicKeyDev.sbDeviceIsProgrammer();
            }
            catch ( Exception ex )
            {
                CProgram.sLogException(" CLientInfoForm() failed to init", ex);
            }
         }

        public void mUpdateButtons()
        {
            if( _mInfo != null )
            {
                bool bNew = _mInfo.mIndex_KEY == 0;
                textBoxNr.Text = bNew ? "" : _mInfo.mIndex_KEY.ToString();
                buttonAddUpdate.Text = bNew ? "Add" : "Update";
                buttonDisable.Text = _mInfo.mbActive ? "Disable" : "Enable";
                textBoxLabel.ReadOnly = false == bNew;
                textBoxNr.ReadOnly = false == bNew;
                mGetCursorSelected();
            }
        }
        private UInt32 mGetCursorSelected()
        {
            UInt32 index = 0;

            try
            {
                if (dataGridViewList.SelectedRows != null && dataGridViewList.SelectedRows.Count > 0)
                {
                    string s = dataGridViewList.SelectedRows[0].Cells[0].Value == null ? ""
                        : dataGridViewList.SelectedRows[0].Cells[0].Value.ToString();

                    if (false == UInt32.TryParse(s, out index))
                    {
                        index = 0;
                    }
                }
                labelSelected.Text = index > 0 ? index.ToString() : "";
                _mSelectedIndex = index;
                buttonSelect.Enabled = _mSelectedIndex > 0;
             }
            catch (Exception ex)
            {
                CProgram.sLogException("CLientInfoForm() get cursor failed", ex);
            }
            return index;
        }
        private bool mbLoadSelected()
        {
            bool bLoaded = false;
            try
            {
                _mSelectedIndex = mGetCursorSelected();

                if (_mSelectedIndex > 0)
                {
                    foreach (CTrialInfo row in _mList)
                    {
                        if (_mSelectedIndex > 0 && row.mIndex_KEY == _mSelectedIndex)
                        {
                            row.mbCopyTo(_mInfo);
                            bLoaded = true;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("CLientInfoForm() load Selected cursor failed: " + _mSelectedIndex.ToString(), ex);
            }
            return bLoaded;
        }

        private void mFillStudyPermissions()
        {
            string text = CDvtmsData.sGetStudyPermissionsBitLabels(_mInfo._mStudyPermissions, "\r\n");

            textBoxStudyPermissions.Text = text;
            CDvtmsData.sbSetStudyPermissionsComboBox(comboBoxLocation, _mInfo._mStudyPermissions, CDvtmsData._sStudyPermSiteGroup);


        }

        public void mSetValues()
        {
            // set the values from info on the form

            textBoxNr.Text = _mInfo.mIndex_KEY == 0 ? "" : _mInfo.mIndex_KEY.ToString();
            checkBoxActive.Checked = _mInfo.mbActive;
            textBoxLabel.Text = _mInfo._mLabel;

            if (_mClient != null)
            {
                _mClient.mbListFillComboBox(comboBoxStudyHospitalNameList, _mClientList, (UInt16)DClientInfoVars.Label, _mInfo._mSponsorClient_IX, "-- Select --");
            }
            dateTimePickerStartDate.Value = _mInfo._mTrialStartDate == null || _mInfo._mTrialStartDate == DateTime.MinValue
                                            ? DateTime.Now.AddHours(-1) : _mInfo._mTrialStartDate;
            dateTimePickerStopDate.Value = _mInfo._mTrialStopDate == null || _mInfo._mTrialStopDate == DateTime.MinValue
                                            ? DateTime.Now.AddHours(-1) : _mInfo._mTrialStopDate;

            comboBoxStudyMonitoringDaysList.Text = _mInfo._mStudyMonitoringDays.ToString();

            if (CDvtmsData._sEnumListStudyType != null)
            {
                CDvtmsData._sEnumListStudyType.mFillComboBox(comboBoxStudyTypeList, false, 0, false, ": ", _mInfo._mStudyTypeCode, "--Select--");
            }
            if (CDvtmsData._sEnumListReportInterval != null)
            {
                CDvtmsData._sEnumListReportInterval.mFillCheckedListBox(checkedListBoxReportInterval, false, 0, false, ": ", _mInfo._mReportIntervalSet, false);
            }
            if (CDvtmsData._sEnumListMctInterval != null)
            {
                CDvtmsData._sEnumListMctInterval.mFillCheckedListBox(checkedListBoxMctInterval, false, 0, false, ": ", _mInfo._mMctIntervalSet, false);
            }
            textBoxStudyFreeText.Text = _mInfo._mStudyFreeText;
            textBoxStudyNote.Text = _mInfo._mStudyRemarkLabel;
            if (CDvtmsData._sEnumListStudyProcedures != null)
            {
//                listViewStudyProcedures.Clear();
                CDvtmsData._sEnumListStudyProcedures.mFillListView(listViewStudyProcedures, false, 0, true, _mInfo._mStudyProcCodeSet, true); // list only set
            }
            mFillStudyPermissions();
        }

        private UInt32 mReadIndex( string AString )
        {
            UInt32 index = 0;

            if (AString != null && AString.Length > 0)
            {
                if (false == UInt32.TryParse(AString, out index))
                {
                    index = 0;
                }
            }
            return index;
        }

        public void mGetValues( out UInt32 ArIndex )
        {
            // get the values from the form  and store in info

            ArIndex = mReadIndex(textBoxNr.Text);
            _mInfo.mbActive = checkBoxActive.Checked;
            _mInfo._mLabel = textBoxLabel.Text;

            if (_mClient != null)
            {
                _mClient.mbListParseComboBox(comboBoxStudyHospitalNameList, _mClientList, (UInt16)DClientInfoVars.Label, ref _mInfo._mSponsorClient_IX);
            }
            _mInfo._mTrialStartDate = dateTimePickerStartDate.Value;
            _mInfo._mTrialStopDate = dateTimePickerStopDate.Value;

            UInt16.TryParse(comboBoxStudyMonitoringDaysList.Text, out _mInfo._mStudyMonitoringDays);

            if (CDvtmsData._sEnumListStudyType != null)
            {
                _mInfo._mStudyTypeCode = CDvtmsData._sEnumListStudyType.mReadComboBoxCode(comboBoxStudyTypeList);
            }
            if (CDvtmsData._sEnumListReportInterval != null)
            {
                CDvtmsData._sEnumListReportInterval.mReadCheckedListBoxSet(checkedListBoxReportInterval, false, ": ", _mInfo._mReportIntervalSet);
            }
            if (CDvtmsData._sEnumListMctInterval != null)
            {
                CDvtmsData._sEnumListMctInterval.mReadCheckedListBoxSet(checkedListBoxMctInterval, false, ": ", _mInfo._mMctIntervalSet);
            }
            if (CDvtmsData._sEnumListStudyProcedures != null)
            {
                CDvtmsData._sEnumListStudyProcedures.mReadListViewSet(listViewStudyProcedures, true, _mInfo._mStudyProcCodeSet); // list only set
            }
            _mInfo._mStudyPermissions = CDvtmsData.sGetStudyPermissionsComboBox(comboBoxLocation, CDvtmsData._sStudyPermSiteGroup);

            _mInfo._mStudyFreeText = textBoxStudyFreeText.Text;
            _mInfo._mStudyRemarkLabel = textBoxStudyNote.Text;
        }

        private void textBoxClientName_TextChanged(object sender, EventArgs e)
        {

        }

        private void dataGridViewClientList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void CAddClientsForm_Load(object sender, EventArgs e)
        {

        }

        private void buttonAddClient_Click(object sender, EventArgs e)
        {

        }
 
        public void mUpdateList()
        {
            if (_mList != null && _mInfo != null)
            {
                int nList, nResult = 0;
                int cursor = 0;

                dataGridViewList.Rows.Clear();
 
                if (_mList == null || (nList = _mList.Count()) == 0)
                {
                    labelListN.Text = "0" + (_mbLoadListError ? "!" : "");
                    labelResultN.Text = "-";
                }
                else
                {
                    bool bAll = checkBoxShowDeactivated.Checked;
                    bool bSearch = checkBoxShowSearchResult.Checked && _mSearchInfo != null;
                    bool bAdd;
                    bool bSearchDateTime = bSearch && _mSearchInfo._mTrialStartDate != null && _mSearchInfo._mTrialStopDate != DateTime.Now;

                    DateTime searchStartDate = DateTime.MinValue, searchStopDate = DateTime.MaxValue;

                    if(bSearchDateTime)
                    {
                        if (_mSearchInfo._mTrialStartDate >= CProgram.sMakeBeginDay(DateTime.Now))
                        {
                            bSearchDateTime = false; // searchStartDate from today show all 
                        }
                        else
                        {
                            searchStartDate = CProgram.sMakeBeginDay(_mSearchInfo._mTrialStartDate);
                            searchStopDate = CProgram.sMakeEndDay(_mSearchInfo._mTrialStopDate);
//                            bSearchDateTime = searchStopDate > searchStartDate;
                        }
                    }

                    foreach (CTrialInfo row in _mList)
                    {
                        if (bAll || row.mbActive)
                        {
                            bAdd = true;
                            if (bSearch)
                            {
                                if (bAdd && _mSearchInfo.mIndex_KEY != 0) bAdd = row.mIndex_KEY == _mSearchInfo.mIndex_KEY;


                                if (bAdd) bAdd = CProgram.sbCmpSearchString(row._mLabel, _mInfo._mLabel);
                                if (bAdd && _mSearchInfo._mSponsorClient_IX > 0) bAdd = row._mSponsorClient_IX == _mSearchInfo._mSponsorClient_IX;
                                if (bAdd && bSearchDateTime) bAdd = row._mTrialStartDate >= searchStartDate && row._mTrialStopDate <= searchStopDate;
                               
                                if (bAdd) bAdd = CProgram.sbCmpSearchString(row._mStudyTypeCode, _mInfo._mStudyTypeCode);
                            }
                            if (bAdd)
                            {
                                string clientName = mGetClientName(row._mSponsorClient_IX);
                                string startDate = row._mTrialStartDate == DateTime.MinValue ? "" : row._mTrialStartDate.ToString("yyyy-MM-dd");
                                string stopDate = row._mTrialStopDate == DateTime.MinValue ? "" : row._mTrialStopDate.ToString("yyyy-MM-dd");
                                string idc = row._mStudyProcCodeSet.mGetShowSet("+" );
                                string permissions = CDvtmsData.sGetStudyPermissionsBitLabels(row._mStudyPermissions, "+");

                                dataGridViewList.Rows.Add(row.mIndex_KEY, row.mbActive.ToString(), row._mLabel, clientName, startDate, stopDate,
                                    row._mStudyMonitoringDays.ToString(), idc, permissions);
                                if( row.mIndex_KEY == _mInfo.mIndex_KEY)
                                {
                                    cursor = nResult;
                                }
                                ++nResult;
                            }
                        }

                    }
                    if( nResult > 0 )
                    {
                        dataGridViewList.CurrentCell = dataGridViewList.Rows[cursor].Cells[0];
                    }
                    labelListN.Text = nList.ToString() + (_mbLoadListError ? "!" : "");
                    labelResultN.Text = nResult.ToString();
                }
            }
        }

        public string mGetClientName(UInt32 AClientIX)
        {
            foreach( CClientInfo client in _mClientList )
            {
                if( client.mIndex_KEY == AClientIX)
                {
                    return client._mLabel;
                }
            }
            return "?C" + AClientIX.ToString();
        }

        public void mLoadList()
        {
            if (_mInfo != null)
            {
                _mInfo.mbDoSqlSelectList(out _mbLoadListError, out _mList, _mInfo.mMaskValid, 0, false, "", DSqlSort.Desending, (UInt16)DSqlDataTableColum.Index_KEY);

                mUpdateList();
            }
        }

        private void buttonAddUpdate_Click(object sender, EventArgs e)
        {
            UInt32 index;
            mGetValues( out index);

            // check values

            if( false == mbCheckForm(_mInfo.mIndex_KEY == 0 ? "Add" : "Update" ))
            {
                return;
            }

            if( _mInfo.mIndex_KEY == 0)
            {
                // new Client

                // check if Laber is not in list

                if( _mInfo.mbDoSqlInsert())
                {
                    textBoxNr.Text = _mInfo.mIndex_KEY.ToString();
                    labelTrialError.Text = "Inserted";
                }
                else
                {
                    labelTrialError.Text = "Failed Insert";
                }
            }
            else if( index > 0 && _mInfo.mIndex_KEY == index )
             {
                // update client

                // check if label is in list (not this)
                if (_mInfo.mbDoSqlUpdate(_mInfo.mMaskValid, _mInfo.mbActive))
                {
                    textBoxNr.Text = "+" + _mInfo.mIndex_KEY.ToString();
                    labelTrialError.Text = "Updated";
                }
                else
                {
                    labelTrialError.Text = "Failed Update";
                }
            }
            mLoadList();
            mUpdateButtons();
        }

        private void buttonDup_Click(object sender, EventArgs e)
        {
            if( _mInfo != null )
            {
                _mInfo.mIndex_KEY = 0;
                _mInfo.mbActive = true;
                mSetValues();
                mUpdateButtons();
                mbCheckForm("Duplicated" );
            }
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            if (_mInfo != null)
            {
                _mInfo.mClear();
                _mInfo.mbActive = true;

                mSetValues();
                mUpdateButtons();
                mbCheckForm("Clear");
            }
        }

        private void buttonRandom_Click(object sender, EventArgs e)
        {
            if (_mInfo != null)
            {
                DateTime dt = DateTime.Now;
                string label = "T" + dt.ToString("yyyyMMddHHmmss");

                _mInfo.mClear();
                _mInfo.mbActive = true;
                _mInfo._mLabel = label;

                _mInfo._mSponsorClient_IX = 0;      // main client
                _mInfo._mTrialStartDate = DateTime.Now.AddDays( -10 );
                _mInfo._mTrialStopDate = DateTime.Now.AddDays(10);
                _mInfo._mStudyMonitoringDays = 10;
                _mInfo._mStudyTypeCode = "";
                _mInfo._mStudyProcCodeSet.mClear();

                _mInfo._mReportIntervalSet.mClear();
                _mInfo._mMctIntervalSet.mClear();
                _mInfo._mStudyRemarkLabel = "Study note " + label;
                _mInfo._mStudyFreeText = "Trial instructions for " + label;
                _mInfo._mStudyPermissions.mClear();
                _mInfo._mTrialState = 0;

                mSetValues();

                mUpdateButtons();
                mbCheckForm("Random");
            }
        }

        bool mbCheckTrial(out string AErrorText)
        {
            string errorText = "init form failed";

            if (_mInfo != null)
            {
                errorText = "";

                if( _mInfo._mLabel == null || _mInfo._mLabel.Length == 0 )
                {
                      errorText = " No Name";
                }
                if(_mInfo._mTrialStartDate == DateTime.MinValue || _mInfo._mTrialStartDate > _mInfo._mTrialStopDate)
                {
                    errorText += " Date not ok";
                }
                if( _mInfo._mSponsorClient_IX == 0)
                {
                    errorText += " Missing client";
                }
            }
            AErrorText = errorText;

            return errorText.Length == 0;
        }

        private bool mbCheckForm(string AState)
        {
            bool bOk = true;
            string errorText = "";

            if (mbCheckTrial(out errorText))
            {
                labelTrialError.Text = "";
            }
            else
            {
                labelTrialError.Text = AState + ": " + errorText;
                bOk = false;
            }
            return bOk;
        }


        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void checkBoxShowDeactivated_CheckedChanged(object sender, EventArgs e)
        {
            mUpdateList();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            mLoadList();
            mUpdateButtons();
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            UInt32 index;

            mGetValues(out index);

            if( _mInfo != null )
            {
                _mSearchInfo = _mInfo.mCreateCopy() as CTrialInfo;
                checkBoxShowSearchResult.Checked = _mSearchInfo != null;
                if (_mSearchInfo != null) _mSearchInfo.mIndex_KEY = index;

            }
            mUpdateList();
            mUpdateButtons();
            mbCheckForm("List");

        }

        private void checkBoxShowSearchResult_CheckedChanged(object sender, EventArgs e)
        {
            mUpdateList();
            mUpdateButtons();
            mbCheckForm("List");
        }

        public bool mbCopySelected( bool AbNewEntry )
        {
            bool bCopied = false;

            if (dataGridViewList.Rows != null && dataGridViewList.Rows.Count > 0)
            {
                if (dataGridViewList.SelectedRows != null && dataGridViewList.SelectedRows.Count > 0 && dataGridViewList.SelectedRows[0] != null)
                {
                    string indexStr = dataGridViewList.SelectedRows[0].Cells[0].Value == null ? ""
                       : dataGridViewList.SelectedRows[0].Cells[0].Value.ToString();
                    int index;

                    if (int.TryParse(indexStr, out index) && index > 0)
                    {
                        foreach (CTrialInfo row in _mList)
                        {
                            if (index == row.mIndex_KEY)
                            {
                                // found selected
                                CTrialInfo copy = row.mCreateCopy() as CTrialInfo;

                                if (copy != null)
                                {
                                    _mInfo = copy;
                                    bCopied = true;
                                    if (AbNewEntry)
                                    {
                                        _mInfo.mIndex_KEY = 0;
                                    }
                                    mSetValues();
                                    mUpdateButtons();
                                    mbCheckForm("Copy");
                                }
                                break;
                            }
                        }
                    }
                }
            }
            return bCopied;
        }
        private void buttonEdit_Click(object sender, EventArgs e)
        {
            mbCopySelected(false);
        }

        private void buttonDuplicate_Click(object sender, EventArgs e)
        {
            mbCopySelected(true);
        }

        private void dataGridClientList_DoubleClick(object sender, EventArgs e)
        {
            mbCopySelected(false);
        }

        private void buttonDisable_Click(object sender, EventArgs e)
        {
            if(mbCopySelected(false))
            {
                UInt32 index;
                mGetValues(out index);

                // check values

                if (_mInfo.mIndex_KEY == 0)
                {
                    labelTrialError.Text = "Not stored";
                }
                else if (index > 0 && _mInfo.mIndex_KEY == index)
                {
                    // update client
                    bool b = _mInfo.mbActive;
                    _mInfo.mbActive = !b;
                    // check if label is in list (not this)
                    if (_mInfo.mbDoSqlUpdate(_mInfo.mMaskValid, _mInfo.mbActive))
                    {
                        textBoxNr.Text = "+" + _mInfo.mIndex_KEY.ToString();
                        labelTrialError.Text = b ? "Disabled" : "Enabled";
                    }
                    else
                    {
                        labelTrialError.Text = b ? "Failed to Disable" : "Failed to Enable";
                    }
                }
            }
            mSetValues();
            mLoadList();
            mUpdateButtons();
        }

        private void dataGridViewList_Click(object sender, EventArgs e)
        {
            mUpdateButtons();
        }

        private void buttonSelect_Click(object sender, EventArgs e)
        {
            if( mbLoadSelected())
            {
                _mbSelected = true;
                Close();
            }
        }

        private void labelSelected_Click(object sender, EventArgs e)
        {

        }

        private void textBoxStudyPermissions_Click(object sender, EventArgs e)
        {
            CDvtmsData.sbEditStudyPermissionsBits("required permissions", ref _mInfo._mStudyPermissions, 0);

            mFillStudyPermissions();
        }

        private void buttonAddICD_Click(object sender, EventArgs e)
        {
            if (_mInfo != null)
            {
                CicdCodeForm form = new CicdCodeForm(_mInfo._mStudyProcCodeSet);

                if (form != null)
                {
                    form.ShowDialog();
                    if (form._mSelectedSet != null)
                    {
                        form._mSelectedSet.mbCopyTo(ref _mInfo._mStudyProcCodeSet);
                        if (CDvtmsData._sEnumListStudyProcedures != null)
                        {
                            CDvtmsData._sEnumListStudyProcedures.mFillListView(listViewStudyProcedures, false, 0, true, _mInfo._mStudyProcCodeSet, true); // list only set
                        }
                        //mFillFormStudy();
                        mUpdateButtons();
                    }
                }
            }
        }

        private void mCalcNrDays()
        {

        }

        private void buttonCalcDays_Click(object sender, EventArgs e)
        {
            DateTime dateStart = dateTimePickerStartDate.Value;
            DateTime dateStop = dateTimePickerStopDate.Value;

            try
            {
                int nDays = (int)((dateStop - dateStart).TotalDays + 0.5);

                if (nDays < 0) nDays = 0;
                comboBoxStudyMonitoringDaysList.Text = nDays.ToString();
            }
            catch( Exception)
            {

            }

        }
    }
}
