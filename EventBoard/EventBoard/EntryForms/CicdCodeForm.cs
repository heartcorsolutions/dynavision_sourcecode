﻿using Event_Base;
using Program_Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventboardEntryForms
{
    public partial class CicdCodeForm : Form
    {
        public CStringSet _mSelectedSet;

        public const UInt16 _cMaxNrCodes = 3;

        public CicdCodeForm( CStringSet ASet )
        {
            try
            {
                CDvtmsData.sbInitData();

                _mSelectedSet = ASet == null ? new CStringSet() : ASet.mCreateCopy();
 
                InitializeComponent();
                //labelMaxNr.Visible = CLicKeyDev.sbDeviceIsProgrammer();
                mFillForm();
                Text = CProgram.sMakeProgTitle("ICD10 codes", false, true);

            }
            catch (Exception ex)
            {
                CProgram.sLogException("Form creation failed", ex);
                Close();
            }
        }

        private void mFillForm()
        {
            /*
              $AHR Abnormalities of Heart Rhythm
              $AFF Atrial Fibrillation and Flutter
              $CA Cardiac Arrhythmias(Other)
              $CP Chest Pain
              $HF Heart Failure
              $HT Hypertension
              $NVD    Nonrheumatic Valve Disorders / AorticValve Disorders
              $MVD    Mitral Valve Disorders
              $AII    Selected Atherosclerosis, Ischemia, and Infarction
              $SC Syncope and Collapse
              */

            UInt16 n = _mSelectedSet.mCount();

            labelMaxNr.Text = n <= _cMaxNrCodes ? n.ToString() + " <= " + _cMaxNrCodes.ToString() + " selected codes"
                                    : n.ToString() + " > " + _cMaxNrCodes.ToString() + "!" + " to many codes selected";
//            labelMaxNr.Text = n <= _cMaxNrCodes ? n.ToString() + " <= " + _cMaxNrCodes.ToString()
//                                                : n.ToString() + " > " + _cMaxNrCodes.ToString() + "!";
            if ( CDvtmsData._sEnumListStudyProcedures != null && _mSelectedSet != null )
            {
                UInt16 group = (UInt16)CDvtmsData._sEnumListStudyProcedures.mGetIntValue("$AHR");

                CDvtmsData._sEnumListStudyProcedures.mFillCheckedListBox(checkedListBoxAHR, true, group, true, " ", _mSelectedSet, false);

                group = (UInt16)CDvtmsData._sEnumListStudyProcedures.mGetIntValue("$AFF");

                CDvtmsData._sEnumListStudyProcedures.mFillCheckedListBox(checkedListBoxAFF, true, group, true, " ", _mSelectedSet, false);

                group = (UInt16)CDvtmsData._sEnumListStudyProcedures.mGetIntValue("$CA");

                CDvtmsData._sEnumListStudyProcedures.mFillCheckedListBox(checkedListBoxCA, true, group, true, " ", _mSelectedSet, false);

                group = (UInt16)CDvtmsData._sEnumListStudyProcedures.mGetIntValue("$CP");

                CDvtmsData._sEnumListStudyProcedures.mFillCheckedListBox(checkedListBoxCP, true, group, true, " ", _mSelectedSet, false);

                group = (UInt16)CDvtmsData._sEnumListStudyProcedures.mGetIntValue("$HF");

                CDvtmsData._sEnumListStudyProcedures.mFillCheckedListBox(checkedListBoxHF, true, group, true, " ", _mSelectedSet, false);

                group = (UInt16)CDvtmsData._sEnumListStudyProcedures.mGetIntValue("$HT");

                CDvtmsData._sEnumListStudyProcedures.mFillCheckedListBox(checkedListBoxHT, true, group, true, " ", _mSelectedSet, false);

                group = (UInt16)CDvtmsData._sEnumListStudyProcedures.mGetIntValue("$NVD");

                CDvtmsData._sEnumListStudyProcedures.mFillCheckedListBox(checkedListBoxNVD, true, group, true, " ", _mSelectedSet, false);

                group = (UInt16)CDvtmsData._sEnumListStudyProcedures.mGetIntValue("$SC");

                CDvtmsData._sEnumListStudyProcedures.mFillCheckedListBox(checkedListBoxSC, true, group, true, " ", _mSelectedSet, false);

                group = (UInt16)CDvtmsData._sEnumListStudyProcedures.mGetIntValue("$AII");

                CDvtmsData._sEnumListStudyProcedures.mFillCheckedListBox(checkedListBoxAII, true, group, true, " ", _mSelectedSet, false);

                group = (UInt16)CDvtmsData._sEnumListStudyProcedures.mGetIntValue("$MVD");

                CDvtmsData._sEnumListStudyProcedures.mFillCheckedListBox(checkedListBoxMVD, true, group, true, " ", _mSelectedSet, false);

                group = (UInt16)CDvtmsData._sEnumListStudyProcedures.mGetIntValue("$MCTr");

                CDvtmsData._sEnumListStudyProcedures.mFillCheckedListBox(checkedListBoxMCTr, true, group, true, " ", _mSelectedSet, false);
                
            }
        }

        private void mReadForm()
        {
            if (CDvtmsData._sEnumListStudyProcedures != null && _mSelectedSet != null)
            {
                CStringSet set = new CStringSet();
                CStringSet newSet = new CStringSet();

                CDvtmsData._sEnumListStudyProcedures.mReadCheckedListBoxSet(checkedListBoxAHR, true, " ", set);

                newSet.mAddSet( set );

                CDvtmsData._sEnumListStudyProcedures.mReadCheckedListBoxSet(checkedListBoxAFF, true, " ", set);

                newSet.mAddSet(set);

                CDvtmsData._sEnumListStudyProcedures.mReadCheckedListBoxSet(checkedListBoxCA, true, " ", set);

                newSet.mAddSet(set);

                CDvtmsData._sEnumListStudyProcedures.mReadCheckedListBoxSet(checkedListBoxCP, true, " ", set);

                newSet.mAddSet(set);

                CDvtmsData._sEnumListStudyProcedures.mReadCheckedListBoxSet(checkedListBoxHF, true, " ", set);

                newSet.mAddSet(set);

                CDvtmsData._sEnumListStudyProcedures.mReadCheckedListBoxSet(checkedListBoxHT, true, " ", set);

                newSet.mAddSet(set);

                CDvtmsData._sEnumListStudyProcedures.mReadCheckedListBoxSet(checkedListBoxNVD, true, " ", set);

                newSet.mAddSet(set);

                CDvtmsData._sEnumListStudyProcedures.mReadCheckedListBoxSet(checkedListBoxSC, true, " ", set);

                newSet.mAddSet(set);

                CDvtmsData._sEnumListStudyProcedures.mReadCheckedListBoxSet(checkedListBoxAII, true, " ", set);

                newSet.mAddSet(set);

                CDvtmsData._sEnumListStudyProcedures.mReadCheckedListBoxSet(checkedListBoxMVD, true, " ", set);

                newSet.mAddSet(set);

                CDvtmsData._sEnumListStudyProcedures.mReadCheckedListBoxSet(checkedListBoxMCTr, true, " ", set);

                newSet.mAddSet(set);

                if ( newSet.mbIsEmpty())
                {
                    _mSelectedSet = null;
                }
                else
                {
                    newSet.mSortSet(_mSelectedSet);
                    _mSelectedSet = newSet;
                }
            }

        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void checkedListBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void panel9_Paint(object sender, PaintEventArgs e)
        {

        }

        private void checkedListBox7_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            _mSelectedSet = null;
            Close();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            mReadForm();

            UInt16 n = _mSelectedSet.mCount();

            labelMaxNr.Text = n <= _cMaxNrCodes ? n.ToString() + " <= " + _cMaxNrCodes.ToString() + " selected codes"
                                    : n.ToString() + " > " + _cMaxNrCodes.ToString() + "!" + " to many codes selected";
            if ( n <= _cMaxNrCodes )
            {
                Close();
                DialogResult = DialogResult.OK;
            }
            else
            {
                labelMaxNr.Visible = true;  // show to many used
            }
        }
    }
}
