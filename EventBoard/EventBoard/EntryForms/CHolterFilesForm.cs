﻿using Event_Base;
using EventBoard;
using EventBoard.EntryForms;
using EventBoard.Event_Base;
using Program_Base;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace EventboardEntryForms
{
    public enum DHolterTableVars
    {
        IncludeTable, FileName, FileDate, FileSize, FileStart, FileDuration, GapNote,
        RecStart, RecDuration, NrChannels, SampleFreq, nrSamples,
        Remark,
        NR_HolterTableVars
    }

    public enum DHolterChannelFormat
    {
        Selected,
        I_II_III
    }

    public enum DHolterUseChannel
    {
        CH0 = 0,
        CHxx = 99,
        Clear = 100,
        III
    }

    public partial class CHolterFilesForm : Form
    {
        public const UInt16 _cMaxNrChannnels = 10;
        public const UInt16 _cSizeChannnelsArray = 32;

        public static string _sHolterProgRun;
        public static string _sHolterProgParams;
        public static bool _sbHolterProgAddStudy = true;
        public static string _sHolterProgValidNrChannels;
        public static string _sHolterProgChannelFormat;
        public static string _sHolterProgSnrFormat;


        public static bool _sbPlot2Ch = false;
        public static bool _sbBlackWhite = false;

        public CStudyInfo _mStudyInfo = null;
        public CPatientInfo _mPatientInfo = null;
        public List<CHolterFile> _mHolterFileList = null;
        public List<CSqlDataTableRow> _mRecordList = null;  // cash all records collected for stat to use to cal start and end time
                                                            //        private bool _mbEditReport;
        UInt32 _mStudyIndex;

        //public string _mCreadedByInitials;

        public UInt16 _mSelectedTableN, _mTotalN;

        public CStringSet _mSelectedTableSet;

        DateTime _mStartPeriodDT;   // needed for print
        DateTime _mEndPeriodDT;
        string _mRecordsInfo;
        bool _mbUseExcluded = false;

        string _mStudyRecodingPath;
        string _mFileScanPattern = "*.dat";
        string _mDeviceType = "Sirona DAT";

        UInt32 _mHolterLengthSec = 0;

        DateTime _mRecordsEndUTC = DateTime.MinValue;
        Int16 _mRecordsEndTZ = 0;
        DateTime _mRecordsStartUTC = DateTime.MaxValue;
        Int16 _mRecordsStartTZ = 0;

        string _mExecMethod = "Merge";
        string _mExecDeviceSNr = "";
        string _mExecDeviceType = "Sirona";

        string _mExecFileName = "";
        string _mExecFilePath = "";
        UInt64 _mExecFileSize = 0;
        DateTime _mExecBeginUTC = DateTime.MinValue;
        Int16 _mExecTimeZoneMin = 0;
        CRecordMit _mExecRecord = null;
        string _mExecResult = "merging";
        UInt16 _mExecChannelMask = 0;
        UInt16 _mExecShiftValue = 0;
        UInt32 _mExecSamplesPerSec = 0;
        DateTime _mExecEndUTC = DateTime.MinValue;

        // (first) input file
        UInt16 _mExecInNrChannels = 0;              
        float _mExecInAdcGain = 0.0F;                  // number of units / mV
        ushort _mExecInAdcBits = 0;                 // number of bits in sample 

        // output file
        float _mExecOutAdcGain = 0.0F;                  // number of units / mV
        ushort _mExecOutAdcBits = 0;                 // number of bits in sample 
        UInt32 _mExecNrSamples = 0;
        UInt32 _mExecOutRangeMin = 0;
        UInt32 _mExecOutRangeMax = 0;
        UInt16 _mExecOutNrChannels = 0;

        UInt16[] _mExecOutUseChannel;

        Int32[] _mExecHolterLastValue;  // holds the last value for filling in the gap
        Int32 _mExecHolterToggleValue;

        float _mExecHolterTogglemV = 0.1F;  // fill in gaps with a +/- 0.1 mV squere at sample freq
        bool _mbTimeFrameUseAM = false;



        public CHolterFilesForm(UInt32 AStudyIndex, bool AbEditReport, UInt32 AStudyReportIndex, UInt32 AAnalysisIndex)
        {
            bool bOk = false;
            bool bError = false;

            try
            {
                Width = 1400;   // Nicer size then minimum 1240
                CDvtmsData.sbInitData();

                _mStudyIndex = AStudyIndex;
                //              _mbEditReport = AbEditReport;
                _mbUseExcluded = FormEventBoard._sbStudyUseExcluded;

                _mStudyInfo = new CStudyInfo(CDvtmsData.sGetDBaseConnection());
                _mPatientInfo = new CPatientInfo(CDvtmsData.sGetDBaseConnection());
                _mRecordList = new List<CSqlDataTableRow>();
                _mHolterFileList = new List<CHolterFile>();
                _mSelectedTableSet = new CStringSet();
                _mExecHolterLastValue = new Int32[_cSizeChannnelsArray];  // holds the last value for filling in the gap
                _mExecOutUseChannel = new UInt16[_cSizeChannnelsArray];
                

                if (_mStudyInfo != null && _mPatientInfo != null
                    && _mSelectedTableSet != null && _mRecordList != null
                    && _mHolterFileList != null && _mExecOutUseChannel != null)
                {
                    if (_mStudyInfo.mbDoSqlSelectIndex(AStudyIndex, _mStudyInfo.mGetValidMask(true)))
                    {
                        if (_mPatientInfo.mbDoSqlSelectIndex(_mStudyInfo._mPatient_IX, _mPatientInfo.mGetValidMask(true)))
                        {

                            bOk = CDvtmsData.sbGetStudyRecorderDir(out _mStudyRecodingPath, AStudyIndex, true);

                            if (bOk == false)
                            {
                                CProgram.sPromptWarning(true, "Holter Study " + AStudyIndex.ToString(), "Recoding folder missing (no files)");
                            }

                            for( int i = _cSizeChannnelsArray; --i >= 0;  )
                            {
                                _mExecOutUseChannel[i] = (UInt16)i;
                                _mExecHolterLastValue[i] = 0;
                            }
                        }
                    }
                }

                if (bOk && CProgram.sbEnterProgUserArea(true))
                {
                    InitializeComponent();

                    if (FormEventBoard._sbForceAnonymize)
                    {
                        checkBoxAnonymize.Checked = true;
                        checkBoxAnonymize.Enabled = false;
                    }

                    Width = 1400;   // Nicer size then minimum 1240

                    mSetTitle("...");

                    panelStudyPeriod.Visible = true;
                    //nelStudyReport1.Visible = AbEditReport;
                    //panelStudyReport1.Enabled = AbEditReport;
                    //panelStudyButtons.Visible = AbEditReport;

//                    checkBoxReducePdf.Visible = CLicKeyDev.sbDeviceIsProgrammer();

                    _mHolterLengthSec = CSironaStateSettings.sGetHolterReqLengthSec();

                    labelHolterSize.Text = "def. Holter length = " + CProgram.sPrintTimeSpan_dhmSec(_mHolterLengthSec, "");

                    mbLoadRecords();
                    mbLoadFileList();

                    mSetStudy();
                    mSetList("");
                    mSetReport();
                    labelRecordInfo.Text = _mRecordsInfo;
                    try
                    {
                        //                        dataGridView.Sort(dataGridView.Columns[(int)DHolterTableVars.RecordET], ListSortDirection.Ascending);
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException("Sort table", ex);
                    }
                    labelPdfNr.Text = "";

                    checkBoxShowResult.Checked = _sbBlackWhite;
                    labelExecResults.Text = ".";

                    bool b = true; // CLicKeyDev.sbDeviceIsProgrammer();
                    panelPeriod.Visible = b;
                    panelSetTime.Visible = b;
                    checkBoxShowResult.Checked = b;
                    mSetTitle("");
                    string timeFormat = CProgram.sGetShowTimeFormat().ToUpper();

                    _mbTimeFrameUseAM = timeFormat.Contains("T");

                    comboBoxFromAM.Visible = _mbTimeFrameUseAM;
                    comboBoxToAM.Visible = _mbTimeFrameUseAM;

                    buttonHolterResult.Visible = _sHolterProgRun != null && _sHolterProgRun.Length > 4 && File.Exists(_sHolterProgRun);

                    labelAllowedChannels.Text = _sHolterProgValidNrChannels;

                    int index = 0;
                    if(_sHolterProgChannelFormat != null && _sHolterProgChannelFormat.Length > 0 )
                    {
                        string search = _sHolterProgChannelFormat.ToLower().Trim();
                        int n = comboBoxChannelFormat.Items == null ? 0 : comboBoxChannelFormat.Items.Count;

                        while( --n >= 0 )
                        {
                            if( search == comboBoxChannelFormat.Items[n].ToString().ToLower())
                            {
                                index = n;
                                break;
                            }
                        }
                    }
                    comboBoxChannelFormat.SelectedIndex = index;
                }
                else
                {
                    //                    Close();
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("PreviousFindingsForm() failed to init", ex);
            }
        }

        private void mSetTitle( string AProcessText)
        {
            string title = "Holter S" + _mStudyIndex.ToString() + " " + _mDeviceType + ": " + AProcessText;
            
            Text = CProgram.sMakeProgTitle(title, false, true);
        }


        private CHolterFile mAddFileToList(FileInfo AFileInfo)
        {
            CHolterFile holterFile = null;


            if (AFileInfo != null && _mHolterFileList != null)
            {
                string fileName = Path.GetFileNameWithoutExtension(AFileInfo.Name);

                foreach (CHolterFile node in _mHolterFileList)
                {
                    if (node._mFileName == fileName)
                    {
                        holterFile = node;
                        break;
                    }
                }
                if (holterFile == null)
                {
                    holterFile = new CHolterFile();
                    if (holterFile != null)
                    {
                        bool b = holterFile.mbSetFileName(fileName);

                        if (b)
                        {
                            int i = 0;
                            foreach (CHolterFile node in _mHolterFileList)
                            {
                                if (holterFile._mFileStartDT <= node._mFileStartDT)
                                {
                                    break;
                                }
                                ++i;
                            }
                            if (i >= _mHolterFileList.Count)
                            {
                                _mHolterFileList.Add(holterFile);
                            }
                            else
                            {
                                _mHolterFileList.Insert(i, holterFile);
                            }
                        }
                        else
                        {
                            _mHolterFileList.Add(holterFile);
                        }
                    }
                }
                if (holterFile != null)
                {
                    holterFile._mFileUtc = AFileInfo.LastWriteTimeUtc;
                    holterFile._mFileSize = (UInt32)AFileInfo.Length;
                    holterFile._mbSelected = holterFile._mbHolter;
                }
            }
            return holterFile;
        }
        private CHolterFile mGetFileFromList(string AFileName)
        {
            CHolterFile holterFile = null;

            if (AFileName != null && _mHolterFileList != null)
            {
                string fileName = Path.GetFileNameWithoutExtension(AFileName);

                foreach (CHolterFile node in _mHolterFileList)
                {
                    if (node._mFileName == fileName)
                    {
                        holterFile = node;
                        break;
                    }
                }
            }
            return holterFile;
        }

        private void mDisposeECGData()
        {
            if (_mHolterFileList != null)
            {
                foreach (CHolterFile node in _mHolterFileList)
                {
                    if (node._mRecord != null)
                    {
                        node._mRecord.mDisposeSignals();
                    }
                }
            }
        }

        private void mDisposeData()
        {
            if (_mHolterFileList != null)
            {
                foreach (CHolterFile node in _mHolterFileList)
                {
                    if (node._mRecord != null)
                    {
                        node._mRecord.mDisposeSignals();
                        node._mRecord = null;
                    }
                }
                _mHolterFileList.Clear();
            }
            if (_mRecordList != null)
            {
                _mRecordList.Clear();
            }
        }

        private bool mbLoadFileList()
        {
            bool bOk = false;
            UInt32 nTotal = 0;
            string scanTxt = "Holer scan " + _mStudyRecodingPath + "\r\n";

            if (_mHolterFileList != null && _mStudyRecodingPath != null && _mStudyRecodingPath.Length > 0)
            {
                try
                {
                    DirectoryInfo dirInfo = new DirectoryInfo(_mStudyRecodingPath);

                    // disable all
                    foreach (CHolterFile node in _mHolterFileList)
                    {
                        node._mbActive = false;

                    }
                    scanTxt += "old list contains " + _mHolterFileList.Count + "\r\n";

                    if (dirInfo != null)
                    {
                        FileInfo[] files = dirInfo.GetFiles(_mFileScanPattern);

                        if (files != null)
                        {
                            // go through all the files
                            scanTxt += "scan " + _mFileScanPattern + " gives " + files.Length.ToString() + "\r\n";


                            foreach (FileInfo fileInfo in files)
                            {
                                CHolterFile node = mAddFileToList(fileInfo);

                                scanTxt += fileInfo.Name + " " + CProgram.sDateTimeToString(fileInfo.LastWriteTime) + " " + fileInfo.Length.ToString() + ":";
                                if (node != null)
                                {
                                    ++nTotal;
                                    node._mbActive = true;

                                    if (false == fileInfo.Name.StartsWith(node._mFileName)) scanTxt += " bad Name " + node._mFileName;
                                    if (fileInfo.Length != node._mFileSize) scanTxt += " bad size " + node._mFileSize;

                                    scanTxt += " " + CProgram.sDateTimeToString(node._mFileStartDT);
                                    scanTxt += " " + node._mFileDurationSec.ToString();
                                    scanTxt += node._mbHolter ? " Holter" : " -h";

                                }
                                else
                                {
                                    scanTxt += "no node";
                                }
                                scanTxt += "\r\n";
                            }
                        }
                    }
                    scanTxt += "new list contains " + _mHolterFileList.Count + " nTotaal= " + nTotal.ToString() + "\r\n";
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("LoadFileList failed", ex);
                }
            }
            labelHolterType.Text = _mDeviceType;
            labelNrXXX.Text = nTotal.ToString();

            return bOk;
        }


        private void buttonReloadList_Click(object sender, EventArgs e)
        {
            try
            {
                mSetTitle("Reload list");

                //                mbLoadRecords();
                mbLoadFileList();
                labelRecordInfo.Text = _mRecordsInfo;

                mSetList("");

                //dataGridView.Sort(dataGridView.Columns[(int)DHolterTableVars.RecordET], ListSortDirection.Ascending);
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Sort table", ex);
            }
            labelPdfNr.Text = "";
        }

        private void mCheckMinMax(UInt16 AN, float AMin, float AMax, ref float ArMin, ref float ArMax)
        {
            if (AN > 0 && AMin <= AMax)
            {
                if (ArMin > AMin) ArMin = AMin;
                if (ArMax < AMax) ArMax = AMax;
            }
        }
        /*      private void mCheckMinMax(CRecAnalysis AAnalysis, ref float ArMin, ref float ArMax)
              {
                  if (AAnalysis != null && AAnalysis._mMeasuredHrN > 0)
                  {
                      if (ArMin > AAnalysis._mMeasuredHrMin) ArMin = AAnalysis._mMeasuredHrMin;
                      if (ArMax < AAnalysis._mMeasuredHrMax) ArMax = AAnalysis._mMeasuredHrMax;
                  }
              }
      */

        public string mMakeEventNrString(UInt16 AEventNr)
        {
            string s = "";

            if (AEventNr > 0)
            {
                s = AEventNr.ToString();

                int len = s.Length;

                if (len < 5)
                {
                    string t = new string(' ', 5 - len);
                    s = t + s;
                }
            }
            return s;
        }

        private void mSetList(string ACursorFileName)
        {
            string cursorFileName = ACursorFileName;
            bool bFindCursor = ACursorFileName != null && ACursorFileName.Length > 0;

            if (false == bFindCursor && dataGridView.Rows.Count > 0 && dataGridView.CurrentRow != null )
            {
                if(dataGridView.CurrentRow.Cells.Count >= 4)
                {
                    cursorFileName = dataGridView.CurrentRow.Cells[1].Value.ToString();
                    bFindCursor = true;
                }
            }

            dataGridView.Rows.Clear();

            bool bHolterOnly = checkBoxHolterOnly.Checked;

            if (_mHolterFileList != null && _mStudyRecodingPath != null && _mStudyRecodingPath.Length > 0)
            {
                try
                {
                    UInt32 nNodes = 0;
                    UInt32 nActive = 0;
                    Int32 nList = 0;
                    UInt32 nSelected = 0;
                    bool bShown;
                    CHolterFile prevHolter = null;
                    Int32 iCursor = 0;

                    foreach (CHolterFile node in _mHolterFileList)
                    {
                        bShown = false;
                        ++nNodes;
                        if (node._mbActive)
                        {
                            ++nActive;
                            if (false == bHolterOnly || node._mbHolter)
                            {
                                String strFileDate = CProgram.sUtcToLocalString(node._mFileUtc);
                                String strFileStart = CProgram.sDateTimeToString(node._mFileStartDT);
                                String strFileDuration = CProgram.sPrintTimeSpan_dhmSec(node._mFileDurationSec, "");
                                String strRecStart = CProgram.sUtcTimeZoneToString(node._mRecStartUTC, node._mRecTimeZoneMin, true);
                                String strRecDuration = CProgram.sPrintTimeSpan_dhmSec(node._mRecDurationSec, "");
                                String strNrChannels = node._mRecNrChannels == 0 ? "" : node._mRecNrChannels.ToString();
                                String strNrSamples = node._mRecNrSamples == 0 ? "" : node._mRecNrSamples.ToString();
                                String strSampleFreq = node._mRecSamplesPerSec == 0 ? "" : node._mRecSamplesPerSec.ToString(); ;
                                String strRemark = "";
                                string gapNote = "";

                                if (prevHolter != null)
                                {
                                    UInt32 stripSec = prevHolter._mFileDurationSec == 0 ? _mHolterLengthSec : prevHolter._mFileDurationSec;
                                    DateTime dt = prevHolter._mFileStartDT.AddSeconds(stripSec);
                                    Int32 gapSec = (Int32)(node._mFileStartDT - dt).TotalSeconds;

                                    if (gapSec < -2)
                                    {
                                        gapNote = "f-" + CProgram.sPrintTimeSpan_dhmSec(-gapSec, "") + " O";

                                    }
                                    else if (gapSec > 2)
                                    {
                                        gapNote = "f" + CProgram.sPrintTimeSpan_dhmSec(gapSec, "") + " G";
                                    }
                                    if (prevHolter._mFileDurationSec == 0)
                                    {
                                        gapNote += "?";
                                    }

                                    if (node._mRecNrSamples > 0 && prevHolter._mRecStartUTC != DateTime.MinValue)
                                    {
                                        stripSec = (UInt32)(prevHolter._mRecDurationSec == 0 ? _mHolterLengthSec : prevHolter._mRecDurationSec);
                                        dt = prevHolter._mRecStartUTC.AddSeconds(stripSec);
                                        gapSec = (Int32)(node._mRecStartUTC - dt).TotalSeconds;

                                        if (gapSec < -2)
                                        {
                                            strRemark = "R-" + CProgram.sPrintTimeSpan_dhmSec(-gapSec, "") + " O";

                                        }
                                        else if (gapSec > 2)
                                        {
                                            strRemark = "R" + CProgram.sPrintTimeSpan_dhmSec(gapSec, "") + " G";
                                        }
                                        if (prevHolter._mRecDurationSec == 0)
                                        {
                                            strRemark += "?";
                                        }
                                    }
                                }
                                if (bFindCursor)
                                {
                                    if (node._mFileName == cursorFileName)
                                    {
                                        iCursor = nList;
                                    }
                                }


                                bShown = true;
                                ++nList;
                                if (node._mbSelected) ++nSelected;

                                dataGridView.Rows.Add(node._mbSelected, node._mFileName, strFileDate, node._mFileSize.ToString(), strFileStart, strFileDuration,
                                    gapNote, strRecStart, strRecDuration, strNrChannels, strSampleFreq, strNrSamples, strRemark);

                                prevHolter = node;
                            }
                        }
                        node._mbShown = bShown;
                    }
                    if (iCursor < nList)
                    {
                        dataGridView.CurrentCell = dataGridView.Rows[iCursor].Cells[0];
                            }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("SetList failed", ex);
                }

            }
            mUpdateStats();
        }
        private void mUpdateStats()
        {
            try
            {
                string recTableSet = "";
                UInt32 nrHolters = 0;
                string chText = "";

                UInt64 sumFileSize = 0;
                UInt32 maxFileSizeMB = 1 << 10; // nax file size is 1 GB -> loaded 2 Byte => 4 byte memory
                DateTime fileStartDT = DateTime.MaxValue;
                DateTime fileEndDT = DateTime.MinValue;

                DateTime fileRunDT = DateTime.MinValue;
                double gapFileTotalSec = 0;
                double overlapFileTotalSec = 0;
                string fileStats = "";
                DateTime recStartUtc = DateTime.MinValue;
                DateTime recRunUTC = DateTime.MinValue;
                double gapRecTotalSec = 0;
                double overlapRecTotalSec = 0;
                string recordStats = "";
                UInt32 nRecCount = 0;
                UInt32 recNrSamples = 0;
                UInt16 channelsMin = 1000;
                UInt16 channelsMax = 0;
                Int16 timeZoneMin = 0;
                UInt32 samplesPerSec = 0;

                _mSelectedTableN = _mTotalN = 0;

                _mSelectedTableSet.mClear();

                _mStartPeriodDT = DateTime.SpecifyKind(DateTime.MaxValue, DateTimeKind.Local);

                _mEndPeriodDT = DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Local);

                if (dataGridView.Rows != null && dataGridView.Rows.Count > 0)
                {
                    bool bTable;
                    int nRow = dataGridView.Rows.Count;

                    //CRecAnalysis selectedAnalysis;
                    DataGridViewRow gridRow;
                    string fileName;
                    CHolterFile holterFile;

                    foreach (CHolterFile node in _mHolterFileList)
                    {
                        node._mbSelected = false;
                    }
                    for (int i = 0; i < nRow; ++i)
                    {
                        //selectedAnalysis = null;

                        gridRow = dataGridView.Rows[i];
                        if (gridRow != null && gridRow.Cells.Count >= (int)DHolterTableVars.NR_HolterTableVars)
                        {
                            ++_mTotalN;
                            bTable = gridRow.Cells[0].Value != null && (bool)gridRow.Cells[0].Value;
                            if (bTable)
                            {
                                ++_mSelectedTableN;

                                fileName = gridRow.Cells[(int)DHolterTableVars.FileName].Value.ToString();

                                holterFile = mGetFileFromList(fileName);


                                if (holterFile != null && holterFile._mFileStartDT > DateTime.MinValue)
                                {
                                    try
                                    {
                                        if (_mSelectedTableN == 1)
                                        {
                                            fileRunDT = holterFile._mFileStartDT;
                                        }
                                        sumFileSize += holterFile._mFileSize;

                                        if (holterFile._mFileStartDT < fileStartDT) fileStartDT = holterFile._mFileStartDT;

                                        double gapSec = (holterFile._mFileStartDT - fileRunDT).TotalSeconds;

                                        if (gapSec < 0)
                                        {
                                            overlapFileTotalSec -= gapSec;
                                        }
                                        else
                                        {
                                            gapFileTotalSec += gapSec;
                                        }
                                        uint stripSec = holterFile._mFileDurationSec == 0 ? _mHolterLengthSec : holterFile._mFileDurationSec;

                                        fileRunDT = fileRunDT.AddSeconds(stripSec);

                                        if (fileRunDT > fileEndDT) fileEndDT = holterFile._mFileStartDT;
                                        if (holterFile._mbHolter)
                                        {
                                            ++nrHolters;
                                        }
                                        holterFile._mbSelected = true;


                                        if( holterFile._mRecNrSamples > 0)
                                        {
                                            if(recRunUTC == DateTime.MinValue)
                                            {
                                                recRunUTC = holterFile._mRecStartUTC;
                                                recStartUtc = recRunUTC;
                                                timeZoneMin = holterFile._mRecTimeZoneMin;
                                                samplesPerSec = holterFile._mRecSamplesPerSec;
                                            }
                                            if (channelsMin > holterFile._mRecNrChannels) channelsMin = holterFile._mRecNrChannels;
                                            if (channelsMax < holterFile._mRecNrChannels) channelsMax = holterFile._mRecNrChannels;

                                            ++nRecCount;
                                            recNrSamples += holterFile._mRecNrSamples;

                                            double gapRecSec = (holterFile._mRecStartUTC - recRunUTC).TotalSeconds;

                                            if (gapSec < 0)
                                            {
                                                overlapRecTotalSec -= gapSec;
                                            }
                                            else
                                            {
                                                gapRecTotalSec += gapSec;
                                            }
                                            double stripRecSec = holterFile._mRecSamplesPerSec == 0 ? 0 : holterFile._mRecDurationSec / (double)(holterFile._mRecSamplesPerSec);

                                            DateTime recUTC = recRunUTC.AddSeconds(stripRecSec);

                                            if (recUTC > recRunUTC) recRunUTC = recUTC;

                                        }

                                    }
                                    catch (Exception)
                                    {
                                        int zxyx = 3;
                                    }
                                }

                            }
                        }
                        if (_mTotalN > 0)
                        {
                            double nrSec = (fileEndDT - fileStartDT).TotalSeconds;
                            UInt32 MB = 1 << 20;
                            UInt64 sizeMB = (sumFileSize + MB - 1) / MB;
                            fileStats = "File stats: start " + CProgram.sDateTimeToString(fileStartDT)
                                + " End " + CProgram.sDateTimeToString(fileEndDT)
                                + " = " + CProgram.sPrintTimeSpan_dhmSec(nrSec, "")
                                + " gap= " + CProgram.sPrintTimeSpan_dhmSec(gapFileTotalSec, "")
                                + " overlap= " + CProgram.sPrintTimeSpan_dhmSec(overlapFileTotalSec, "")
                                + " total size=" + sumFileSize.ToString() + "(" + sizeMB.ToString()
                                + (sizeMB >= maxFileSizeMB ? ">=" + maxFileSizeMB.ToString() : "") + " MB)";
                        }
                        if (nRecCount > 0)
                        {
                            double nrSec = (recRunUTC - recStartUtc).TotalSeconds;
                            UInt32 MB = 1 << 20;
                            UInt64 totalSize = recNrSamples * 2  * channelsMax;
                            UInt64 sizeMB = (totalSize + MB - 1) / MB;
                            double sampleSec = samplesPerSec == 0 ? 0 : recNrSamples / (double)(samplesPerSec);
                            double deltaSec = (sampleSec - nrSec);
                            recordStats = "Record stats: start " + CProgram.sUtcTimeZoneToString (recStartUtc, timeZoneMin, true)
                                + " End " + CProgram.sUtcTimeZoneToString(recRunUTC, timeZoneMin, false)
                                + " = " + CProgram.sPrintTimeSpan_dhmSec(nrSec, "")
                                + " Δ" + (deltaSec < 0 ? "-" + CProgram.sPrintTimeSpan_dhmSec(deltaSec, "") : CProgram.sPrintTimeSpan_dhmSec(deltaSec, ""))
                                + " gap= " + CProgram.sPrintTimeSpan_dhmSec(gapRecTotalSec, "")
                                + " overlap= " + CProgram.sPrintTimeSpan_dhmSec(overlapRecTotalSec, "")
                                + " total size=" + totalSize.ToString() + "(" + sizeMB.ToString()
                                + (sizeMB >= maxFileSizeMB ? ">=" + maxFileSizeMB.ToString() : "") + " MB) " 
                                + channelsMax.ToString() + "ch*" + recNrSamples.ToString();
                        }
                    }
                }
                string tableText = "-";
                if (_mTotalN > 0)
                {
                    bool a = true;
                    tableText = (a ? _mSelectedTableN.ToString() + "/ " : "") + _mTotalN.ToString();
                }

                labelListTable.Text = tableText;
                labelNrHolter.Text = nrHolters.ToString();

                labelFileStats.Text = fileStats;
                labelRecordStats.Text = recordStats;

                mUpdateMemInfo();

            }
            catch (Exception ex)
            {
                CProgram.sLogException("PreviousFindingsForm() failed to calc stat", ex);
            }
        }
        private void mUpdateMemInfo()
        {
            string info;
            CProgram.sbCheckChangedMemSize(out info, true);
            labelMemInfo.Text = info;
        }

        private void mSelectAll(bool AbSelected)
        {
            try
            {
                if (dataGridView.Rows != null && dataGridView.Rows.Count > 0)
                {
                    foreach (CHolterFile node in _mHolterFileList)
                    {
                        node._mbSelected = AbSelected && node._mbActive;
                    }
                    mSetList("");
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("SetSelected", ex);
            }
        }
        private void mSelectPeriod()
        {
            try
            {
                if (dataGridView.Rows != null && dataGridView.Rows.Count > 0)
                {
                    DateTime fromDT, toDT;
                    bool bRange = mGetRange(out fromDT, out toDT, true);
                    string firstName = "";
                    bool bFindFirst = true;

                    foreach (CHolterFile node in _mHolterFileList)
                    {
                        DateTime endDT = node._mFileStartDT.AddSeconds(node._mFileDurationSec == 0 ? _mHolterLengthSec : node._mFileDurationSec);
                        node._mbSelected = bRange && node._mbActive 
                            && (endDT >= fromDT && node._mFileStartDT <= toDT);
                        if(bFindFirst && node._mbSelected)
                        {
                            bFindFirst = false;
                            firstName = node._mFileName;
                        }
                    }
                    mSetList(firstName);
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("SetPeriod", ex);
            }
        }

        private string mPrintNTotal(uint AN, UInt32 ATotal)
        {
            string s = "-";

            if (ATotal > 0)
            {
                s = AN.ToString() + " / " + ATotal.ToString();
            }
            return s;
        }
        private void mInitPatient()
        {
            int age = _mPatientInfo != null ? _mPatientInfo.mGetAgeYears() : -1;

            bool bHidePatient = checkBoxAnonymize.Checked;

            labelFullName.Text = bHidePatient ? "--Anonymized--" : (_mPatientInfo != null ? _mPatientInfo.mGetFullName() : "");
            labelDoB.Text = bHidePatient ? ""
                : (_mPatientInfo != null && age >= 0 ? CProgram.sDateToString(_mPatientInfo._mPatientDateOfBirth.mDecryptToDate()) : "");

            labelAge.Text = _mPatientInfo != null && age > 0 ? age.ToString() : "";
            labelGender.Text = _mPatientInfo != null ? CPatientInfo.sGetGenderString((DGender)_mPatientInfo._mPatientGender_IX) : "";

        }

        private void mSetStudy()
        {
            string studyRange = "";
            if (_mStudyInfo != null)
            {
                labelStudyIndex.Text = _mStudyInfo.mIndex_KEY.ToString();

                string s = _mStudyInfo.mGetLabelNoteMark();

                labelStudyNote.Text = s;

                if (s.Length > 0)
                {
                    s = _mStudyInfo.mGetRemarkAndNote();

                    new ToolTip().SetToolTip(labelStudyNote, s);
                }

                mInitPatient();

                // study nr reports is noe used for all pdf doc reports

                int n = _mRecordList == null ? 0 : _mRecordList.Count;
                CSqlDBaseConnection db = CDvtmsData.sGetDBaseConnection();

                UInt32 dbLimit = db == null ? 1 : db.mGetSqlRowLimit();
                if (n >= dbLimit)
                {
                    labelRecordInfo.BackColor = Color.Orange;
                }

                studyRange = "Study start " + CProgram.sDateTimeToString(_mStudyInfo._mStudyStartDate);
                if (_mStudyInfo._mRecorderStartUTC < _mStudyInfo._mRecorderEndUTC)
                {
                    double nrSec = (_mStudyInfo._mRecorderEndUTC - _mStudyInfo._mRecorderStartUTC).TotalSeconds;
                    studyRange += " recorder start " + CProgram.sDateTimeToString(CProgram.sUtcToLocal(_mStudyInfo._mRecorderStartUTC))
                    + " end " + CProgram.sDateTimeToString(CProgram.sUtcToLocal(_mStudyInfo._mRecorderEndUTC))
                    + " = " + CProgram.sPrintTimeSpan_dhmSec((Int32)nrSec, "");
                }
                else
                {
                    studyRange += " no recording ";
                }
                if (_mStudyInfo._mStudyRecorder_IX == 0)
                {
                    studyRange += " no Recorder";
                }
                else
                {
                    CDeviceInfo device = new CDeviceInfo(CDvtmsData.sGetDBaseConnection());

                    if (device != null && device.mbDoSqlSelectIndex(_mStudyInfo._mStudyRecorder_IX, CSqlEnumRow.sGetMask((UInt16)DDeviceInfoVars.DeviceSerialNr)))
                    {
                        studyRange += " " + device._mDeviceSerialNr;
                    }
                }
                mUpdateFromTo(_mStudyInfo._mStudyStartDate, DateTime.Now);
            }
            labelStudyRange.Text = studyRange;
        }

        private bool mbIsDay(DateTime ADateTime)
        {
            bool bIsDay = false;

            if (ADateTime.Hour >= 6) // >= 6:00 && <= 23:59
            {
                if (ADateTime.Hour <= 23)
                {
                    bIsDay = true;
                }
                else if (ADateTime.Hour == 23)
                {
                    bIsDay = ADateTime.Minute <= 59;
                }
            }
            return bIsDay;
        }

        public string mMaCodeString(bool AbManual)
        {
            return AbManual ? "M" : "A";
        }

        public bool mbLoadRecords()
        {
            bool bOk = false;
            string recRange = "";

            UInt32 dbLimit = 0;

            _mRecordsInfo = "? records";

            CSqlDBaseConnection db = CDvtmsData.sGetDBaseConnection();

            if (db != null)
            {
                dbLimit = db.mGetSqlRowLimit();
            }

            CRecordMit recBase = new CRecordMit(db);

            if (recBase != null && _mRecordList != null)
            {
                bool bError;

                recBase.mStudy_IX = _mStudyIndex;


                bOk = recBase.mbDoSqlSelectList(out bError, out _mRecordList, recBase.mGetValidMask(true), CRecordMit.sGetMask((UInt16)DRecordMitVars.Study_IX), true, "");

                if (bOk)
                {
                    int nStates = (int)DRecState.NrRecStates;
                    UInt32[] countStates = new UInt32[nStates];
                    Int32 nTotal = _mRecordList.Count;
                    Int32 nToDo = 0;
                    Int32 nAnalyzed = 0;
                    Int32 nOther = 0;
                    UInt16 state;
                    DateTime utc;

                    _mRecordsEndUTC = DateTime.MinValue;
                    _mRecordsStartUTC = DateTime.MaxValue;

                    for (int i = 0; i < nStates; ++i)
                    {
                        countStates[i] = 0;
                    }

                    foreach (CRecordMit rec in _mRecordList)
                    {
                        state = rec.mRecState;
                        if (state >= nStates)
                        {
                            state = (UInt16)DRecState.Unknown;
                        }
                        ++countStates[state];

                        if (CRecordMit.sbIsStateAnalyzed(state, _mbUseExcluded))
                        {
                            ++nAnalyzed;
                        }
                        else if (CRecordMit.sbIsStateToDo(state))
                        {
                            ++nToDo;
                        }
                        else
                        {
                            ++nOther;
                        }
                        if (rec.mBaseUTC < _mRecordsStartUTC)
                        {
                            _mRecordsStartUTC = rec.mBaseUTC;
                            _mRecordsStartTZ = (Int16)rec.mTimeZoneOffsetMin;

                        }
                        utc = rec.mBaseUTC.AddSeconds(rec.mRecDurationSec);
                        if (utc > _mRecordsEndUTC)
                        {
                            _mRecordsEndUTC = utc;
                            _mRecordsEndTZ = (Int16)rec.mTimeZoneOffsetMin;
                        }
                    }
                    _mRecordsInfo = nTotal.ToString();

                    if (nTotal >= dbLimit)
                    {
                        _mRecordsInfo += ">=" + dbLimit.ToString();
                    }
                    _mRecordsInfo += " records: " + nToDo.ToString() + " ToDo, " + nAnalyzed.ToString()
                        + (_mbUseExcluded ? " Analyzed+excl (" : " Analyzed (");

                    bool bShowAll = CLicKeyDev.sbDeviceIsProgrammer();
                    bool bAddKomma = false;

                    for (UInt16 i = 0; i < nStates; ++i)
                    {
                        if (bShowAll || countStates[i] > 0)
                        {
                            if (bAddKomma)
                            {
                                _mRecordsInfo += ", ";
                            }
                            _mRecordsInfo += countStates[i].ToString() + " " + CRecordMit.sGetRecStateUserString(i);
                            bAddKomma = true;
                        }
                    }
                    _mRecordsInfo += ")";
                    if (_mRecordsStartUTC < _mRecordsEndUTC)
                    {
                        double nrSec = (_mRecordsEndUTC - _mRecordsStartUTC).TotalSeconds;

                        recRange = "First Event strip " + CProgram.sUtcTimeZoneToString(_mRecordsStartUTC, _mRecordsStartTZ, true)
                            + ", End Event strip " + CProgram.sUtcTimeZoneToString(_mRecordsEndUTC, _mRecordsEndTZ, true)
                            + " duration = " + CProgram.sPrintTimeSpan_dhmSec((Int32)nrSec, "");
                    }
                }
            }
            labelRecordRange.Text = recRange;
            labelRecordInfo.Text = _mRecordsInfo;
            return bOk;
        }

        public void mDumpRecInfo()
        {
            if (_mRecordList != null)
            {
                string cr = "\r\n";
                string tab = "\t";
                string text = _mRecordList.Count.ToString() + " Records for Study " + _mStudyIndex.ToString() + " at " + CProgram.sDateTimeToString(DateTime.Now) + cr;
                string line;
                string remark;
                DateTime dtReceived, dtEvent, dtChanged, dtStart;

                text += labelRecordInfo.Text + cr + cr;
                text += "Record" + tab + "Received" + tab + "Device" + tab + "DevID" + tab
                        + "Import" + tab + "S.subNr" + tab + "State"
                        + tab + "Event" + tab + "Zone(min)" + tab + "Type"
                        + tab + "Start" + tab + "Duration(sec)" + tab + "Channels"
                        + tab + "remark" + tab + "changed" + tab + "by"
                        + tab + "Nann" + tab + "Analysis" + tab + "Classification" + tab + "Rhythm"
                        + tab + "Changed" + tab + "By"
                        + cr;
                int nUsedAnn = 0;
                foreach (CRecordMit rec in _mRecordList)
                {
                    dtReceived = CProgram.sDateTimeToLocal(rec.mReceivedUTC);

                    dtEvent = rec.mEventUTC.AddMinutes(rec.mTimeZoneOffsetMin);
                    dtChanged = CProgram.sDateTimeToLocal(rec.mChangedUTC);
                    dtStart = rec.mGetDeviceTime(rec.mBaseUTC);
                    remark = rec.mRecRemark.mDecrypt();
                    if (remark != null)
                    {
                        int pos = remark.IndexOf('\n');
                        if (pos > 0)
                        {
                            remark = remark.Substring(0, pos);
                        }
                    }
                    line = rec.mIndex_KEY.ToString() + tab + CProgram.sDateTimeToString(dtReceived) + tab + rec.mDeviceID + tab + rec.mDevice_IX.ToString()
                        + tab + ((DImportMethod)rec.mImportMethod).ToString() + tab + rec.mSeqNrInStudy.ToString() + tab + CRecordMit.sGetRecStateUserString(rec.mRecState)
                        + tab + CProgram.sDateTimeToString(dtEvent) + tab + rec.mTimeZoneOffsetMin.ToString() + tab + rec.mEventTypeString
                        + tab + CProgram.sDateTimeToString(dtStart) + tab + rec.mRecDurationSec.ToString("0.000") + tab + rec.mNrSignals.ToString()
                        + tab + "\"" + remark + "\"" + tab + CProgram.sDateTimeToString(dtChanged) + tab + CDvtmsData.sFindUserInitial(rec.mChangedBy_IX);
                    text += line + "\r\n";
                }
                try
                {
                    string studyPath;

                    if (CDvtmsData.sbCreateStudyRecorderDir(out studyPath, _mStudyIndex))
                    {
                        string fileName = "Rholt_S" + _mStudyIndex.ToString("00000000") + "_" + CProgram.sDateTimeToYMDHMS(DateTime.Now) + ".txt";
                        string filePath = Path.Combine(studyPath, fileName);

                        using (StreamWriter sw = new StreamWriter(filePath))
                        {
                            if (sw != null)
                            {
                                sw.Write(text);
                                sw.Close();
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    CProgram.sLogException("failed write RecHolt dump S" + _mStudyIndex.ToString(), ex);
                }

                if (CProgram.sbAskOkCancel("Dump Study " + _mStudyIndex.ToString() + " records", "dump to clipboard?"))
                {
                    Clipboard.SetText(text);
                }
            }
        }

        public CRecordMit mFindRecord(UInt32 ARecordIx)
        {
            CRecordMit recFound = null;

            foreach (CRecordMit rec in _mRecordList)
            {
                if (rec.mIndex_KEY == ARecordIx)
                {
                    recFound = rec;
                    break;
                }
            }

            return recFound;
        }
        private void mSetReport()
        {
        }

        public bool mbSaveReport(string AExtraText)
        {
            bool bOk = false;
            string resultText = "";

            mUpdateStats();    // calculates sets

            return bOk;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            mbSaveReport("");
        }

        private void buttonSaveReport_Click(object sender, EventArgs e)
        {
            mbSaveReport("");
        }

        private void label13_Click_1(object sender, EventArgs e)
        {

        }

        private void textBoxSummary_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttonMCT_Click(object sender, EventArgs e)
        {
            if (_mStudyInfo != null && _mStudyInfo.mIndex_KEY > 0)
            {
                bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;
                bool bSelectFiles = (Control.ModifierKeys & Keys.Alt) == Keys.Alt;

                if (bCtrl) bCtrl = CProgram.sbAskYesNo("MCT Full debug", "Do full debug logging?");

                DTriageTimerMode prevMode = FormEventBoard.sSetTriageTimerEntering(DTriageTimerMode.DialogSecond, "MCT");

                CMCTTrendDisplay form = new CMCTTrendDisplay(_mStudyInfo.mIndex_KEY, DDeviceType.Unknown, bCtrl, bSelectFiles);

                if (form != null)//&& form.bStatsLoaded)
                {
                    form.Show();
                }
                FormEventBoard.sSetTriageTimerLeaving(prevMode, "MCT");

            }
        }

        private void buttonViewPdf_Click(object sender, EventArgs e)
        {
            if (_mStudyInfo != null && _mStudyInfo.mIndex_KEY > 0)
            {
                try
                {
                    CStudyReportListForm form = new CStudyReportListForm(_mStudyInfo.mIndex_KEY);

                    if (form != null)
                    {
                        form.ShowDialog();
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Call pdf study list", ex);
                }
            }
        }

        private void mOpenStudyDir()
        {
            if (_mStudyIndex != 0)
            {
                string studyPath = null;

                if (CDvtmsData.sbGetStudyCaseDir(out studyPath, _mStudyIndex, true))
                {
                    // open explorer wit file at cursor
                    try
                    {
                        ProcessStartInfo startInfo = new ProcessStartInfo();
                        startInfo.FileName = @"explorer";
                        startInfo.Arguments = studyPath;
                        Process.Start(startInfo);
                    }
                    catch (Exception e2)
                    {
                        CProgram.sLogException("Failed open explorer", e2);
                    }
                }
            }
        }
        private void labelStudyIndex_DoubleClick(object sender, EventArgs e)
        {

            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl) //&& CLicKeyDev.sbDeviceIsProgrammer())
            {
                mOpenStudyDir();
            }

        }

        private void buttonEndStudy_Click(object sender, EventArgs e)
        {
            if (_mStudyInfo != null)
            {
                try
                {
                    string deviceText = "";

                    CDeviceInfo deviceInfo = new CDeviceInfo(CDvtmsData.sGetDBaseConnection());
                    if (_mStudyInfo._mStudyRecorder_IX > 0 && deviceInfo != null)
                    {
                        if (deviceInfo.mbDoSqlSelectIndex(_mStudyInfo._mStudyRecorder_IX, deviceInfo.mGetValidMask(true)))
                        {
                            if (deviceInfo._mActiveStudy_IX == _mStudyInfo.mIndex_KEY)
                            {
                                if (deviceInfo._mRecorderEndUTC > DateTime.UtcNow)
                                {
                                    deviceInfo._mRecorderEndUTC = DateTime.UtcNow;
                                    _mStudyInfo._mRecorderEndUTC = DateTime.UtcNow;
                                    deviceText = deviceInfo._mDeviceSerialNr + " End Recording";

                                    deviceInfo.mbDoSqlUpdate(CSqlDataTableRow.sGetMask((UInt16)DStudyInfoVars.RecorderEndUTC), true);
                                }
                            }
                        }
                    }
                    mbSaveReport(deviceText);  // update study;
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("End study", ex);
                }
            }
        }


        private void mDoSelection(bool AbSet)
        {

            try
            {
                /*                if (_mAnalysisList != null && _mAnalysisList.Count > 0 && dataGridView.Rows != null && dataGridView.Rows.Count > 0 && CDvtmsData._sEnumListFindingsClass != null)
                                {
                                    bool bTable, bStrip, bHR0, bDo, bQC, bMan;
                                    int nRow = dataGridView.Rows.Count;
                                    string afib2 = "Afib";
                                    string classification;
                                    DataGridViewRow gridRow;
                                    UInt16 nTotal = 0;
                                    UInt16 nSet = 0;

                                    dataGridView.SuspendLayout();

                                    for (int i = 0; i < nRow; ++i)
                                    {
                                        bDo = false;
                                        gridRow = dataGridView.Rows[i];
                                        classification = "";
                                        if (gridRow != null && gridRow.Cells.Count > (int)DHolterTableVars.FindingsClass)//AnalysisIndex)
                                        {
                                            bTable = bStrip = false;
                                            ++nTotal;

                                            gridRow.Cells[0].Value = bTable;
                                            gridRow.Cells[1].Value = bStrip;
                                        }
                                    }
                                }
                */
            }
            catch (Exception ex)
            {
                CProgram.sLogException("End study", ex);
            }
            finally
            {
                dataGridView.ResumeLayout();
                mUpdateStats();
            }
        }
        private void buttonSetSelection_Click(object sender, EventArgs e)
        {
            mDoSelection(true);
        }

        private void buttonResetSelection_Click(object sender, EventArgs e)
        {
            mDoSelection(false);
        }

        private void buttonAmplRange_Click(object sender, EventArgs e)
        {
        }

        private void labelRecordInfo_Click(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl)
            {
                mDumpRecInfo();
            }

        }

        private void buttonSetSelection_Click_1(object sender, EventArgs e)
        {
            mDoSelection(true);
        }

        private void checkBoxAnonymize_CheckedChanged(object sender, EventArgs e)
        {
            mInitPatient();
        }

        private void buttonUncheckAll_Click_1(object sender, EventArgs e)
        {
            mDoSelection(false);
        }


        private void buttonUncheckAll_Click(object sender, EventArgs e)
        {
            mDoSelection(false);
        }

        private void mOpenRowAnalysis()
        {
            if (dataGridView.CurrentRow != null)
            {
                string selectedFileName = "";

                try
                {
                    if (dataGridView.CurrentRow.Cells.Count > (int)DHolterTableVars.FileName
                                        && dataGridView.CurrentRow.Cells[(int)DHolterTableVars.FileName].Value != null)
                    {
                        selectedFileName = dataGridView.CurrentRow.Cells[(int)DHolterTableVars.FileName].Value.ToString();

                        mbLoadOpenAnalysis("Part", _mStudyRecodingPath, selectedFileName);
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Failed to run Analysis Form", ex);
                }
            }

        }


        private void buttonOpenAnalysis_Click(object sender, EventArgs e)
        {
            mOpenRowAnalysis();
        }
            private void labelHR_Click(object sender, EventArgs e)
        {
            mUpdateStats();
        }

        private void buttonSortET_Click(object sender, EventArgs e)
        {
            try
            {
                //                dataGridView.Sort(dataGridView.Columns[(int)DHolterTableVars.RecordET], ListSortDirection.Ascending);
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Sort table", ex);
            }

        }

        private void button7_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void dataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < dataGridView.Rows.Count)
            {
                DataGridViewRow row = dataGridView.Rows[e.RowIndex];

                if (row != null)
                {
                    if (e.ColumnIndex == 0)
                    {
                        if (row.Cells[0].Value != null)
                        {
                            bool b = (bool)row.Cells[0].Value;
                            b = !b;

                            row.Cells[0].Value = b;
                            if (b == false)
                            {
                            }
                            mUpdateStats();
                        }

                    }
                }
            }
        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void panel40_Paint(object sender, PaintEventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void buttonPrintPreviousFindings_Click(object sender, EventArgs e)
        {
        }
        private bool mbLoadOpenAnalysis(string AMethod, string ADirPath, string AFileName)
        {
            FormAnalyze formAnalyze = null;
            bool bOk = false;

            try
            {
                CSqlDBaseConnection dBase = CDvtmsData.sGetDBaseConnection();
                CRecordMit record = new CRecordMit(dBase);
                string recorderSnr = "";

                if (record != null)
                {
                    string fileHea = Path.ChangeExtension(AFileName, ".hea");
                    string pathHea = Path.Combine(ADirPath, fileHea);
                    Int16 defTimeZoneMin = CProgram.sGetLocalTimeZoneOffsetMin();
                    UInt32 invChannels = 0;

                    mSetTitle("Load file " + AMethod + " " + AFileName);

                    bOk = record.mbReadFile(pathHea, invChannels, DateTime.UtcNow, defTimeZoneMin, DMoveTimeZone.None,
                        true, false, DImportConverter.IntriconSironaMit, DImportMethod.Unknown, "");

                    if (bOk)
                    {
                        if (_mExecRecord != null)
                        {
                            _mExecRecord.mDisposeSignals();
                            mUpdateMemInfo();
                        }
                        _mExecRecord = record;

                        _mExecMethod = AMethod;
                        _mExecFilePath = pathHea;
                        _mExecFileName = Path.GetFileNameWithoutExtension(AFileName);

                        mbDeterminChannels(record, DateTime.MinValue);
                        _mExecInAdcBits = _mExecOutAdcBits = record.mAdcBits;
                        _mExecNrSamples = record.mNrSamples;
                        _mExecInNrChannels = record.mNrSignals;
                        _mExecSamplesPerSec = record.mSampleFrequency;

                        double t2Sec = record.mGetSamplesTotalTime();

                        _mExecResult = "Open " + _mExecMethod + " " + _mDeviceType + " S" + _mStudyIndex.ToString() + " " + _mExecFileName + " "
                             + _mExecInNrChannels.ToString() + " channels @ " + _mExecSamplesPerSec.ToString() + " Sps: " + _mExecNrSamples.ToString()
                                + "= " + CProgram.sPrintTimeSpan_dhmSec(t2Sec, "");

                        record.mStudy_IX = _mStudyIndex;
                        record.mRecState = 0;

                        if( record.mDeviceID == null || record.mDeviceID.Length == 0 )  // need a deviceId
                        {
                            string[] parts = AFileName.Split('_');
                            int nParts = parts == null ? 0 : parts.Length;

                            if( nParts > 0)
                            {
                                if( parts[0] == "H_")
                                {
                                    if( nParts >=2 )
                                    {
                                        recorderSnr = parts[1];
                                    }
                                }
                                else
                                {
                                    recorderSnr = parts[0];
                                }
                            }
                        }

                        labelExecResults.Text = _mExecResult;
                        CProgram.sLogLine(_mExecResult);

                        FormEventBoard eventBoardForm = FormEventBoard.sGetMainForm();
                        formAnalyze = new FormAnalyze(eventBoardForm);

                        if (eventBoardForm != null && formAnalyze != null)
                        {
                            if (formAnalyze.mbShowRecord(false, record, dBase, ADirPath, fileHea))
                            {
                                record.mDeviceID = recorderSnr;
                                string studyPath;

                                if (CDvtmsData.sbGetStudyCaseDir(out studyPath, _mStudyIndex, false))
                                {
                                    bool bFromStudy = ADirPath.StartsWith(studyPath);

                                    if (bFromStudy)
                                    {
                                        if (formAnalyze._mStudyInfo == null)
                                        {
                                            formAnalyze._mStudyInfo = (CStudyInfo)_mStudyInfo.mCreateCopy();
                                        }
                                        if (formAnalyze._mPatientInfo == null)
                                        {
                                            formAnalyze._mPatientInfo = (CPatientInfo)_mPatientInfo.mCreateCopy();
                                        }
                                    }
                                }
                                formAnalyze.mInitScreens();

                                if (formAnalyze != null)
                                {
                                    mSetTitle("View file " + AMethod + " " + AFileName + " Failed!");
                                    try
                                    {
                                        formAnalyze.Show();
                                        formAnalyze = null;  // running so no cleanup needed
                                                             //                                        formAnalyze.ShowDialog();
                                    }
                                    catch (Exception ex)
                                    {
                                        CProgram.sLogException("Analysis Form run", ex);
                                    }
                                }
                            }
                            else
                            {
                                mSetTitle("Show file " + AMethod + " " + AFileName + " Failed!");
                            }
                        }
                    }
                    else
                    {
                        UInt64 memBytes = record.mNrSignals * record.mNrSamples * 4;
                        UInt32 mem1MB = 1 << 20;
                        UInt32 memMB = (UInt32)((memBytes + mem1MB)/mem1MB);
                        labelExecResults.Text = "Failed Open " + AMethod + " " + _mDeviceType + " S" + _mStudyIndex.ToString() + " " + AFileName
                            + " ("+ record.mNrSignals.ToString() + "ch* "+ record.mNrSamples.ToString() + "=  " + memMB.ToString() + " MB)";
                        string title = "Load file " + AMethod + " " + AFileName + " Failed!";
                        mSetTitle(title);
                    }
                }

            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed to run Load Record and Analysis Form", ex);
            }
            mUpdateMemInfo();
            if (formAnalyze != null)
            {
                // Created but not in use => cleanup
                formAnalyze.mReleaseLock();
            }
            return bOk;
        }

        private bool mbReplaceParam( ref string ArLine, string AParam, string AValue)
        {
            bool bReplaced = false;
            if( ArLine != null && ArLine.Length > 0 && AParam != null && AParam.Length > 0)
            {
                int pos = ArLine.IndexOf(AParam);

                if ( pos >= 0)
                {
                    bReplaced = true;

                    string newLine = ArLine.Substring(0, pos) + AValue + ArLine.Substring(pos + AParam.Length);

                    ArLine = newLine;
                }
            }
            return bReplaced;
        }
        private bool mbRunHolterProg(string AMethod, string ADirPath, string AFileName)
        {
            bool bOk = false;

            try
            {
                bOk = _sHolterProgRun != null && _sHolterProgRun.Length > 4 && File.Exists(_sHolterProgRun);
                //            _sHolterProgRun;
                //_sHolterProgParams;

                if( bOk)
                {
                    bool bAn = checkBoxAnonymize.Checked;
                    UInt32 studyIX = _mStudyInfo == null ? 0 : _mStudyInfo.mIndex_KEY;
                    string studyStr = "S" + studyIX.ToString();
                    string patID = studyStr;
                    string firstName = "";
                    string lastName = "";
                    string dob = "";
                    string gender = "";

                    string ext = "hea";
                    string fileHea = Path.ChangeExtension(AFileName, ".hea");
                    string pathHea = Path.Combine(ADirPath, fileHea);
                    string fileName = Path.GetFileNameWithoutExtension(AFileName);

                    if (_mPatientInfo != null )
                    {
                        DateTime dt = _mPatientInfo._mPatientDateOfBirth.mDecryptToDate();

                        if( bAn )
                        {
                            if (dt != DateTime.MinValue) dob = ",\"dob\":\"" + dt.Year.ToString("0000") + "/01/01\"";
                        }
                        else
                        {
                            patID = ",\"pid\":\"";
                            if (_sbHolterProgAddStudy) patID += studyStr + "-";
                            patID += _mPatientInfo.mGetPatientSocIDValue(false) + "\"";
                            firstName = _mPatientInfo._mPatientFirstName.mDecrypt();
                            lastName = _mPatientInfo._mPatientLastName.mDecrypt();
                            if (dt != DateTime.MinValue) dob = ",\"dob\":\"" + dt.Year.ToString("0000") + "/" + dt.Month.ToString("00") + "/" + dt.Day.ToString("00") + "\"";
                        }
                        switch (_mPatientInfo._mPatientGender_IX )
                        {
                            case (UInt16)DGender.Male:
                                gender = ",\"gender\":\"M\"";
                                break;
                            case (UInt16)DGender.Female:
                                gender = ",\"gender\":\"F\"";
                                break;
                        }
                    }
                    string json = "{"
                        + "\"datapath\":\""  + pathHea +"\""        // mandatory full path to hea file
                        + patID                                     // optional Patient Identifier
                        + ",\"lastname\":\"" + lastName + "\""      // mandatory Last Name
                        + ",\"firstname\":\"" + firstName + "\""    // mandatory First Name
                        + gender                                    // optional gender [M|F]
                        + dob                                       // optional date of birth
                        + "}";
                    // DVXholter.exe --params={ "datapath":"d:/ecg/201711020823/1.hea", "pid":"392398","lastname":"david","firstname":"jone","gender":"M","dob":"1965/03/12"}
                    mSetTitle("run holter file " + AMethod + " " + AFileName);

                    string runParams = _sHolterProgParams;
                    UInt32 nParams = 0;
                    if (mbReplaceParam(ref runParams, "%p", ADirPath)) ++nParams;   // %p = path
                    if (mbReplaceParam(ref runParams, "%f", fileHea)) ++nParams;   // %f = file
                    if (mbReplaceParam(ref runParams, "%n", fileName)) ++nParams;   // %n = name
                    if (mbReplaceParam(ref runParams, "%e", ext)) ++nParams;   // %e = ext
                    if (mbReplaceParam(ref runParams, "%j", json)) ++nParams;   // %j = json parms
                    if (mbReplaceParam(ref runParams, "%d", _mExecDeviceSNr)) ++nParams;   // %d = device snr
                    if (mbReplaceParam(ref runParams, "%t", _mExecDeviceType)) ++nParams;   // %t = type
                    if (mbReplaceParam(ref runParams, "%c", _mExecOutNrChannels.ToString())) ++nParams;   // %c = channels
                    if (mbReplaceParam(ref runParams, "%s", _mExecSamplesPerSec.ToString())) ++nParams;   // %s = sample frequention
                    if (mbReplaceParam(ref runParams, "%x", _mExecMethod)) ++nParams;   // %x = exec method {Raw | Holter | open}
                    if (mbReplaceParam(ref runParams, "%i", studyIX.ToString())) ++nParams;   // %x = exec method {Raw | Holter | open}

                    CProgram.sLogLine("Run Holter>>> " + Path.GetFileName(_sHolterProgRun) + " " + runParams);
                    ProcessStartInfo startInfo = new ProcessStartInfo();
                    startInfo.FileName = _sHolterProgRun;
                    startInfo.Arguments = runParams;
                    Process.Start(startInfo);
                }

            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed to open holterLoad Record and Analysis Form", ex);
            }
            mUpdateMemInfo();
            return bOk;
        }

        private void dataGridView_DoubleClick(object sender, EventArgs e)
        {
            mOpenRowAnalysis();
        }

        private void dataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void CHolterFilesForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            mDisposeData();
        }

        private void panel26_Paint(object sender, PaintEventArgs e)
        {

        }

        private void buttonResetAll_Click(object sender, EventArgs e)
        {
            mSelectAll(false);
        }

        private void buttonSetAll_Click(object sender, EventArgs e)
        {
            mSelectAll(true);
        }

        private void checkBoxHolterOnly_CheckedChanged(object sender, EventArgs e)
        {
            mSetList("");
        }

        private void buttonMegeFiles_Click(object sender, EventArgs e)
        {
            mDoMergeFiles();


        }

        private void mDoMergeFiles()
        {
            bool bOk = false;
            UInt32 nToDo = 0;
            UInt32 nFiles = 0;
            UInt32 iFile = 0;
            string firstFileName = "";
            FileStream fs = null;
            List<string> fileResultList = new List<string>();
            string fileResult;
            double secPerSample = 0;
            DateTime startMergeDT = DateTime.Now;
            string nameMethod = "Raw";
            UInt32 invChannels = mReadInvertMask();


            string stepTitle;
            CProgram.sSetBusy(true);

            try
            {
                mSetTitle("Raw merge files");
                
                if (_mExecRecord != null)
                {
                    _mExecRecord.mDisposeSignals();
                }

                _mExecMethod = "Raw merge"; // may not contain a space, used for file
                _mExecFileName = "";
                _mExecFilePath = "";
                _mExecFileSize = 0;
                _mExecBeginUTC = DateTime.MinValue;
                _mExecRecord = null;
                _mExecResult = "Raw merging";
                _mExecNrSamples = 0;
                _mExecInNrChannels = 0;
                _mExecChannelMask = 0;
                _mExecShiftValue = 0;
                _mExecEndUTC = DateTime.MinValue;

                _mExecOutAdcGain = 0.0F;                  // number of units / mV
                _mExecOutAdcBits = 0;                 // number of bits in sample 
                _mExecNrSamples = 0;
                _mExecOutRangeMin = 0;
                _mExecOutRangeMax = 0;
                _mExecOutNrChannels = 0;
                _mExecHolterToggleValue = 10000;

                mUpdateStats(); // check selected files

                foreach (CHolterFile node in _mHolterFileList)
                {
                    if (node._mbActive && node._mbSelected && node._mFileSize > 10)
                    {
                        ++nToDo;
                    }
                }
                bOk = true;

                mSetTitle("Merge "+ nToDo.ToString() + " files");
                labelExecResults.Text = _mExecResult + "...";

                fileResult = _mExecMethod + " " + _mDeviceType + " files: " + nToDo.ToString();
                CProgram.sLogLine(fileResult);
                //            if (fileResultList != null) fileResultList.Add(fileResult);

                foreach (CHolterFile node in _mHolterFileList)
                {
                    if (node._mbActive && node._mbSelected && node._mFileSize > 10)
                    {
                        ++iFile;

                        stepTitle = _mExecMethod + " " + iFile.ToString() + " / " + nToDo.ToString();

                        mSetTitle(stepTitle + " Loading " + node._mFileName);

                        if (false == node.mbLoadFile(_mStudyRecodingPath, invChannels))        // load record from file
                        {
                            _mExecResult = " failed load " + node._mFileName;
                            bOk = false;
                            break;
                        }
                        if (iFile == 1)
                        {
                            // first file
                            firstFileName = node._mFileName;

                            if (false == mbDeterminChannels(node._mRecord, DateTime.MinValue))
                            {
                                _mExecResult = " failed determin channnels";
                                bOk = false;
                                break;

                            }

                            secPerSample = _mExecSamplesPerSec == 0 ? 0.0 : 1.0 / _mExecSamplesPerSec;
                            if (false == mbDoMakeFileName(node._mDeviceSnr, _mStudyIndex, nameMethod, node._mFileStartDT))
                            {
                                _mExecResult = " failed holter path";
                                bOk = false;
                                break;
                            }
                            _mExecResult = _mExecMethod + " " + _mDeviceType + " " + nToDo.ToString() + " files >> " + _mExecFileName + " "
                                        + _mExecOutNrChannels.ToString() + " channels @ " + _mExecSamplesPerSec.ToString() + " Sps: writing...";
                            labelExecResults.Text = _mExecResult;
                            fileResult = _mExecResult + " " + _mExecChannelMask.ToString("X");
                            CProgram.sLogLine(fileResult);
                            if (fileResultList != null)
                            {
                                fileResultList.Add(fileResult);

                                string outChannels = "out channels = {";

                                for( int i = 0; i < _mExecOutNrChannels; ++ i)
                                {
                                    if( i > 0 ) outChannels += ", ";
                                    outChannels += _mExecOutUseChannel[i].ToString();
                                }
                                fileResultList.Add(outChannels + "}");
                            }

                            if (false == mbCreateDataFile(out fs))
                            {
                                _mExecResult = " failed load " + node._mFileName;
                                bOk = false;
                                break;
                            }
                        }
                        else
                        {
                            bOk = mbCheckChannels(node._mRecord);
                            if (false == bOk)
                            {
                                break;
                            }
                        }

                        double curSec = _mExecNrSamples * secPerSample;
                        double addSec = node._mRecNrSamples * secPerSample;
                        DateTime curDT = _mExecBeginUTC.AddSeconds(curSec + _mExecTimeZoneMin * 60);

                        mSetTitle(stepTitle + " adding " + node._mFileName + ": sample "
                            + _mExecNrSamples.ToString() + " + " + node._mRecNrSamples.ToString()
                            + "  (" + CProgram.sPrintTimeSpan_dhmSec(curSec, "")
                            + " + " + CProgram.sPrintTimeSpan_dhmSec(addSec, "") + ")");

                        fileResult = _mExecMethod + " " + _mDeviceType + " " + iFile.ToString() 
                            +"[" + _mExecNrSamples.ToString("0000000000") + " " + CProgram.sDateTimeToYMDHMS( curDT) + " "+ CProgram.sPrintTimeSpan_dhmSec(curSec, "") 
                            + "]: " +
                            node._mFileName + " " + node._mRecNrChannels.ToString() + " channels @ " + node._mRecSamplesPerSec.ToString() + " Sps "
                            + node._mRecNrSamples.ToString() + "= " + CProgram.sPrintTimeSpan_dhmSec(node._mRecDurationSec, "");
                        CProgram.sLogLine(fileResult);


                        bOk = mbWriteSamplesArray(fs, node._mRecord, 0, node._mRecord.mNrSamples);

                        _mExecEndUTC = _mExecEndUTC.AddSeconds(node._mRecord.mNrSamples / (double)(node._mRecord.mSampleFrequency));



                        if (bOk)
                        {
                            ++nFiles;
                        }
                        else
                        {
                            fileResult += " failed!";
                            _mExecResult = stepTitle + " failed write part from " + node._mFileName;
                            break;
                        }
                        if (fileResultList != null) fileResultList.Add(fileResult);
                        node.mbDisposeData();
                        mUpdateMemInfo();
                        Application.DoEvents();
                    }
                }
                double dT = (DateTime.Now-startMergeDT).TotalSeconds;
                mSetTitle( _mExecMethod + " " + nFiles.ToString() + " / " + nToDo.ToString() 
                    + (bOk ? " done ok" : " failed!") + " in " + CProgram.sPrintTimeSpan_dhmSec(dT, ""));

                if (bOk)
                {
                    double t1Sec = (_mExecEndUTC - _mExecBeginUTC).TotalSeconds;
                    double t2Sec = _mExecNrSamples / (double)(_mExecSamplesPerSec);
                    _mExecResult = _mExecMethod + " " + _mDeviceType + " " + nFiles.ToString() + " S" + _mStudyIndex.ToString() + " " + _mExecFileName + " "
                        + _mExecOutNrChannels.ToString() + " channels @ " + _mExecSamplesPerSec.ToString() + " Sps: " + _mExecNrSamples.ToString()
                        + "= " + CProgram.sPrintTimeSpan_dhmSec(t2Sec, "");

                    if (Math.Abs(t1Sec - t2Sec) > iFile)
                    {
                        _mExecResult += "!= " + CProgram.sPrintTimeSpan_dhmSec(t1Sec, "");
                    }

                    labelExecResults.Text = _mExecResult + " save header...";
                    // write hea 
                    bOk = mbSaveHeader();

                    if (checkBoxCopyFirstJson.Checked && firstFileName != null && firstFileName.Length > 0)
                    {
                        // copy first json file to be used by merged
                        string fromJson = Path.Combine(_mStudyRecodingPath, Path.ChangeExtension(firstFileName, ".json"));
                        try
                        {
                            if (File.Exists(fromJson))
                            {
                                string toJson = Path.ChangeExtension(_mExecFilePath, "json");
                                File.Copy(fromJson, toJson, true);
                            }
                        }
                        catch (Exception ex)
                        {
                            CProgram.sLogException("Failed copy json " + fromJson, ex);
                        }

                    }
                }
                if (bOk)
                {

                }
                fileResult = _mExecResult + " " + (_mExecFileSize/ 1000000) + "MB " + (bOk ? " done ok " : " failed! ");
                CProgram.sLogLine(fileResult + " in " + CProgram.sPrintTimeSpan_dhmSec(dT, ""));
                if (fileResultList != null) fileResultList.Add(fileResult + " in " + CProgram.sPrintTimeSpan_dhmSec(dT, ""));

                labelExecResults.Text = fileResult;
                // close file
                if (fs != null)
                {
                    fs.Close();
                    if (fileResultList != null)
                    {
                        string resultFile = _mExecFilePath + ".MergeLog";
                        try
                        {
                            File.WriteAllLines(resultFile, fileResultList);
                        }
                        catch (Exception ex)
                        {
                            CProgram.sLogException("Failed write " + _mExecMethod + " " + resultFile, ex);
                        }
                    }

                    if (bOk)
                    {
                        // load  from file the result
                    }
                }

                mSetList(""); // update list with record info

            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed Raw merge " + _mExecMethod + " " + _mDeviceType, ex);
            }
            CProgram.sSetBusy(false);
            if (checkBoxShowResult.Checked)
            {
                string text = _mExecMethod + " " + _mDeviceType + " result:\r\n";

                foreach( string s in fileResultList)
                {
                    text += s + "\r\n";
                }
                ReqTextForm.sbShowText(_mExecMethod + " " + _mDeviceType + " S" + _mStudyIndex.ToString(), ( bOk ? "result OK" : "failed result"), text);
            }
        }

        private bool mbDoMakeFileName(string ADeviceSnr, UInt32 AStudyIndex, string AMethod, DateTime AStartDT, UInt32 ALengthSec = 0)
        {
            bool bOk = false;
            try
            {
                string deviceSnr = ADeviceSnr;
                if (_sHolterProgSnrFormat != null && _sHolterProgSnrFormat.Length > 0)
                {
                    string name = _sHolterProgSnrFormat.Trim();
                    int totalLength = name.Length;
                    int deviceLength = deviceSnr.Length;
                    int nReplace = 0;

                    for (int i = totalLength; nReplace < deviceLength && --i >= 0;)
                    {
                        char c = name[i];

                        if (c != '0')
                        {
                            break;  // replace last '0'by last digits of device snr
                        }
                        ++nReplace;
                    }
                    if (nReplace == 0)
                    {
                        deviceSnr = name;
                    }
                    else
                    {
                        deviceSnr = name.Substring(0, totalLength - nReplace) + deviceSnr.Substring(deviceLength - nReplace);
                    }
                }

                _mExecFileName = deviceSnr + "_S" + AStudyIndex.ToString("00000000") + "_"
                    + AMethod + "_C" + _mExecOutNrChannels.ToString("X");

                int firstChannel = 0;

                if (_mExecChannelMask > 0)
                {
                    while (firstChannel < 16)
                    {
                        if ((_mExecChannelMask & (1 << firstChannel)) != 0)
                        {
                            break;
                        }
                        ++firstChannel;
                    }
                    ++firstChannel; // bit 0 shown as 1
                }
                _mExecFileName += firstChannel.ToString("X");

                _mExecFileName += "_" + CProgram.sDateTimeToYMDHMS(AStartDT);

                if (ALengthSec > 0)
                {
                    _mExecFileName += "_L" + CProgram.sSecToDHMS(ALengthSec);
                }
                if (checkBoxaddDtFile.Checked)
                {
                    _mExecFileName += "_W" + CProgram.sDateTimeToYMDHMS(DateTime.Now);
                }
                string path;
                bOk = CDvtmsData.sbCreateStudyHolterDir(out path, _mStudyIndex);

                if (bOk)
                {
                    _mExecFilePath = Path.Combine(path, _mExecFileName + ".dat");
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed create file name for S" + AStudyIndex + " " + ADeviceSnr, ex);
            }

            return bOk;
        }

        private UInt16 mReadChannelCheckmarks(bool AbReadIndividualChannels)
        {
            UInt16 mask = 0;

            bool bAllChannels = checkBoxChAll.Checked;

            if (false == bAllChannels || AbReadIndividualChannels)
            {
                if (checkBoxCh1.Checked) mask |= 1 << 0;
                if (checkBoxCh2.Checked) mask |= 1 << 1;
                if (checkBoxCh3.Checked) mask |= 1 << 2;
                if (checkBoxCh4.Checked) mask |= 1 << 3;
                if (checkBoxCh5.Checked) mask |= 1 << 4;
                if (checkBoxCh6.Checked) mask |= 1 << 5;
                if (checkBoxCh7.Checked) mask |= 1 << 6;
                if (checkBoxCh8.Checked) mask |= 1 << 7;
                if (checkBoxCh9.Checked) mask |= 1 << 8;
                if (checkBoxCh10.Checked) mask |= 1 << 9;
            }
            return mask;
        }

        private bool mbDeterminChannels(CRecordMit ARecord, DateTime AStartUTC)
        {
            try
            {
                _mExecChannelMask = mReadChannelCheckmarks(false);

                _mExecNrSamples = 0;
                _mExecShiftValue = 0;
                _mExecBeginUTC = AStartUTC;
                _mExecEndUTC = _mExecBeginUTC;
                _mExecTimeZoneMin = 0;
                string _mExecDeviceSNr = "";


                _mExecSamplesPerSec = 0;
                _mExecInNrChannels = 0;
                _mExecInAdcGain = 0.0F;                  // number of units / mV
                _mExecInAdcBits = 0;                 // number of bits in sample 
                _mExecOutRangeMin = 0;
                _mExecOutRangeMax = 0;

                _mExecOutAdcGain = 0.0F;                  // number of units / mV
                _mExecOutAdcBits = 0;                 // number of bits in sample 
                _mExecNrSamples = 0;
                _mExecOutRangeMin = 0;
                _mExecOutRangeMax = 0;
                _mExecOutNrChannels = 0;
                _mExecHolterToggleValue = 10000;
                _mExecDeviceSNr = "";

                if (ARecord != null)
                {
                    int channelFormat = comboBoxChannelFormat.SelectedIndex;

                    _mExecDeviceSNr = ARecord.mDeviceID;

                    if(_mExecDeviceSNr == null || _mExecDeviceSNr.Length == 0)
                    {
                        // assume first part of file is snr
                        string s = ARecord.mFileName;

                        if( s.StartsWith( "H_"))
                        {
                            s = s.Substring(2);
                        }
                        int pos = s.IndexOf('_');
                            
                            if( pos > 0)
                        {
                            _mExecDeviceSNr = s.Substring(0, pos);
                        }
                    }
                    _mExecInNrChannels = ARecord.mNrSignals;
                    _mExecSamplesPerSec = ARecord.mSampleFrequency;
                    _mExecInAdcGain = ARecord.mAdcGain;                  // number of units / mV
                    _mExecInAdcBits = ARecord.mAdcBits;                 // number of bits in sample 


                    _mExecOutRangeMin = 0;
                    _mExecOutRangeMax = 0;

                    if (_mExecChannelMask == 0)
                    {
                        _mExecChannelMask = (UInt16)((1 << _mExecInNrChannels) - 1);    // all channels
                    }

                    for (int i = _cSizeChannnelsArray; --i >= 0;)
                    {
                        _mExecOutUseChannel[i] = (UInt16)DHolterUseChannel.Clear;
                        _mExecHolterLastValue[i] = 0;
                    }

                    for (int i = 0; i < _mExecInNrChannels; ++i)        // first file determins maximum nr channels
                    {
                        if ((_mExecChannelMask & (1 << i)) != 0)
                        {
                            _mExecOutUseChannel[_mExecOutNrChannels] = (UInt16)i;
                            ++_mExecOutNrChannels;
                        }
                        if (channelFormat == (int)DHolterChannelFormat.I_II_III && i == 1)
                        {
                            _mExecOutUseChannel[_mExecOutNrChannels] = (UInt16)DHolterUseChannel.III;   // add channel III = II - I
                            ++_mExecOutNrChannels;
                        }
                    }
                    if (0 == _mExecOutNrChannels)
                    {
                        CProgram.sLogError("Create Holter file out nr channels = 0");
                        return false;
                    }
                    if (_sHolterProgValidNrChannels != null && _sHolterProgValidNrChannels.Length > 0)
                    {
                        // check valid nr channels = "3;12;8"
                        int nUse = 0;

                        string[] parts = _sHolterProgValidNrChannels.Split(new char[] { ';', ',' });
                        if (parts != null && parts.Length > 0)
                        {
                            for (int i = 0; i < parts.Length; ++i)
                            {
                                int x = 0;
                                if (int.TryParse(parts[i], out x))
                                {
                                    if (x == _mExecOutNrChannels)
                                    {
                                        nUse = _mExecOutNrChannels; // ok
                                        break;
                                    }

                                    if (x > _mExecOutNrChannels)
                                    {
                                        if (x > nUse && x < _cSizeChannnelsArray)
                                        {
                                            nUse = x;
                                        }
                                    }
                                }
                            }
                        }
                        if (nUse == 0)
                        {
                            if (CProgram.sbPromptOkCancel(true, "Create holter file", "Nr channels(" + _mExecOutNrChannels.ToString() + ") not allowed, Continue"))
                            {
                                CProgram.sLogWarning("Create holter file Nr channels(" + _mExecOutNrChannels.ToString() + ") not allowed, continueing...");
                            }
                            else
                            {
                                CProgram.sLogError("Create holter file Nr channels(" + _mExecOutNrChannels.ToString() + ") not allowed");
                                return false;
                            }
                        }
                        else
                        {
                            _mExecOutNrChannels = (UInt16)nUse;
                        }

                    }

                    _mExecOutAdcGain = _mExecInAdcGain;                  // number of units / mV
                    _mExecOutAdcBits = 16;                 // number of bits in sample 

                    _mExecShiftValue = 0;
                    if (_mExecInAdcBits > _mExecOutAdcBits)
                    {
                        _mExecShiftValue = (UInt16)(_mExecInAdcBits - _mExecOutAdcBits);

                        Int32 factor = 1 << _mExecShiftValue;

                        _mExecOutAdcGain /= factor;
                    }
                    _mExecHolterToggleValue = (Int32)(_mExecOutAdcGain * _mExecHolterTogglemV + 0.5);  // fill in gaps with a +/- 0.1 mV squere at sample freq 
                    if (ARecord.mBaseUTC > _mExecEndUTC)
                    {
                        _mExecBeginUTC = ARecord.mBaseUTC;
                        _mExecEndUTC = _mExecBeginUTC;
                    }
                    _mExecTimeZoneMin = (Int16)ARecord.mTimeZoneOffsetMin;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed determin nr channels", ex);
            }
            return _mExecOutNrChannels > 0 && _mExecSamplesPerSec > 0;
        }

        private bool mbCheckChannels(CRecordMit ARecord)
        {
            bool bOk = true;
            string error = "";
            string fileName = "?";

            try
            {
                if (ARecord == null)
                {
                    CProgram.sAddText(ref error, "null", ", ");
                }
                else if(_mExecSamplesPerSec != ARecord.mSampleFrequency)
                {
                    CProgram.sAddText(ref error, ARecord.mSampleFrequency.ToString() + "!=" + _mExecSamplesPerSec.ToString() + " Sps", ", ");
                }
                else if (Math.Abs(_mExecInAdcGain - ARecord.mAdcGain) > 1 )
                {
                    CProgram.sAddText(ref error, ARecord.mAdcGain.ToString("0.0") + "!=" + _mExecInAdcGain.ToString("0.0") + " ", ", ");
                }
                else if (_mExecInAdcBits != ARecord.mAdcBits)
                {
                    CProgram.sAddText(ref error, ARecord.mAdcBits.ToString() + "!=" + _mExecInAdcBits.ToString() + " ", ", ");
                }
            }
            catch ( Exception ex )
            {
                CProgram.sAddText(ref error, "exception", ", ");
            }
            if (error != "")
            {
                bOk = false;
                CProgram.sPromptError(true, "Merge file error: " + fileName, error);
            }

            return bOk;
        }

        private bool mbCreateDataFile(out FileStream ArFileStream)
        {
            bool bOk = false;
            FileStream fs = null;

            try
            {
                if (false == File.Exists(_mExecFilePath)
                    || CProgram.sbAskOkCancel(_mExecMethod + " S" + _mStudyIndex.ToString(), "File " + _mExecFileName + " exists, overwrite?"))
                {
                    fs = new FileStream( _mExecFilePath,FileMode.Create, FileAccess.Write);
                    bOk = fs != null;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed to create file " + _mExecFileName, ex);
                bOk = false;

            }
            ArFileStream = fs;
            return bOk;
        }

/*        private bool mbMergeWritePart(FileStream AFileStream, CRecordMit ARecord)
        {
            bool bOk = false;

            if (AFileStream != null && ARecord != null)
            {
                int minLimit = Int16.MinValue + 1;
                int maxLimit = Int16.MaxValue - 1;
                int defMask = (1 << _mExecInNrChannels) - 1;
                int channelMask = _mExecChannelMask == 0 ? defMask : _mExecChannelMask;
                int i, j;

                try
                {
                    for (i = 0; i < ARecord.mNrSamples; ++i)
                    {
                        for (j = 0; j < 16; ++j)
                        {
                            if ((channelMask & (1 << j)) != 0)  // check output channels
                            {
                                Int32 v = j < ARecord.mNrSignals ? ARecord.mSignals[j].mValues[i] : 0;

                                if (_mExecShiftValue > 0)
                                {
                                    v = v >> _mExecShiftValue;
                                }
                                if (v < minLimit)
                                {
                                    v = minLimit;
                                    ++_mExecOutRangeMin;
                                }
                                else if (v > maxLimit)
                                {
                                    v = maxLimit;
                                    ++_mExecOutRangeMax;
                                }
                                AFileStream.WriteByte((byte)(v & 0x00FF));              // 16 bit
                                AFileStream.WriteByte((byte)((v >> 8) & 0x00FF));
                                _mExecFileSize += 2;
                            }
                        }
                        ++_mExecNrSamples;
                    }
                    _mExecEndUTC = _mExecEndUTC.AddSeconds(ARecord.mNrSamples / (double)(ARecord.mSampleFrequency));
                    bOk = true;
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Failed to write to file " + _mExecFileName, ex);
                    bOk = false;
                }
            }

            return bOk;
        }
*/
        private void buttonViewResult_Click(object sender, EventArgs e)
        {
            if (_mExecFilePath != null && _mExecFilePath.Length > 4 && _mExecFileName != null && _mExecFileName.Length > 1)
            {
                mbLoadOpenAnalysis(_mExecMethod, Path.GetDirectoryName(_mExecFilePath), _mExecFileName);
            }
        }

        private void buttonOpenFile_Click(object sender, EventArgs e)
        {
            mOpenFile();
        }
        private void mOpenFile()
        {
            string path;

            bool bOk = CDvtmsData.sbCreateStudyHolterDir(out path, _mStudyIndex);

            if (bOk)
            {
                openFileDialog.InitialDirectory = path;
                openFileDialog.Title = "Select Holter file to open";
                openFileDialog.FileName = path;

                if( openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    path = openFileDialog.FileName;

                    try
                    {
                        if (path != null && path.Length > 0 && File.Exists(path))
                        {
                            mbLoadOpenAnalysis("File", Path.GetDirectoryName(path), Path.GetFileNameWithoutExtension(path));
                        }
                    }
                    catch(Exception ex)
                    {
                        CProgram.sLogException("Open file", ex);
                    }
                }
            }
        }

        private void buttonHolterFiles_Click(object sender, EventArgs e)
        {
            mDoHolterFiles();
        }

        private void mUpdateDateTime(DateTimePicker APicker, TextBox AHours, TextBox AMinutes, ComboBox AAM, DateTime ADateTime, bool AbEnable)
        {
            try
            {
                if (ADateTime > DateTime.MinValue && ADateTime < DateTime.MaxValue)
                {
                    DateTime date = new DateTime(ADateTime.Year, ADateTime.Month, ADateTime.Day);
                    int hour = ADateTime.Hour;
                    int min = ADateTime.Minute;

                    APicker.Value = date;

                    if (_mbTimeFrameUseAM)
                    {
                        if (hour >= 12)
                        {
                            AAM.SelectedIndex = 1; // PM
                            if (hour > 12) hour -= 12;
                        }
                        else
                        {
                            AAM.SelectedIndex = 0; // AM
                            if (hour == 0) hour = 12;
                        }
                    }
                    AAM.Visible = _mbTimeFrameUseAM;

                    AHours.Text = hour.ToString();
                    AMinutes.Text = min.ToString();

                    APicker.Enabled = AbEnable;
                    AAM.Enabled = AbEnable;
                    AHours.Enabled = AbEnable;
                    AMinutes.Enabled = AbEnable;
                }
                else
                {
                    AHours.Text = "-";
                    AMinutes.Text = "-";
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("update time", ex);
            }
        }

        private DateTime mReadDateTime(DateTimePicker APicker, TextBox AHours, TextBox AMinutes, ComboBox AAM)
        {
            DateTime dt = DateTime.MinValue;
            int hour, min;

            try
            {
                if (int.TryParse(AHours.Text, out hour))
                {
                    if (_mbTimeFrameUseAM)
                    {
                        if (AAM.SelectedIndex == 1)
                        {
                            if( hour < 12 ) hour += 12;
                        }
                        else
                        {
                            if (hour == 12) hour = 0;
                        }
                    }
                }
                else
                {
                    hour = 0;
                }

                if (false == int.TryParse(AMinutes.Text, out min))
                {
                    min = 0;
                }

                if (hour < 0)
                {
                    hour = 0;
                    min = 0;
                }
                else if (hour >= 24)
                {
                    hour = 23;
                    min = 59;
                }
                dt = new DateTime(APicker.Value.Year, APicker.Value.Month, APicker.Value.Day, hour, min, 0);
            }
            catch (Exception ex)
            {
                CProgram.sLogException("read time", ex);
            }

            return dt;
        }

        private void mUpdateFromTo( DateTime AFromDT, DateTime AToDT)
        {
            DateTime fromDT = AFromDT;
            DateTime toDT = AToDT;
            bool bEnableFrom = true;
            bool bEnableTo = false;
            string reqDur = "?";

            try
            {
                if( radioButtonAll.Checked )
                {
                    bEnableFrom = false;
                    reqDur = "All";
                }
                else if (radioButtonPeriod.Checked)
                {
                    bEnableTo = true;
                }
                else if (radioButtonHour.Checked)
                {

                }
                else if (radioButton6Hour.Checked)
                {

                }
                else if (radioButton12Hour.Checked)
                {

                }
                else if (radioButtonDay.Checked)
                {

                }
                else if (radioButtonWeek.Checked)
                {

                }
                else if (radioButtonMonth.Checked)
                {

                }
                if (bEnableFrom && AFromDT != DateTime.MinValue && AToDT != DateTime.MinValue)
                {
                    double min = (AToDT - AFromDT).TotalMinutes;
                    reqDur = CProgram.sPrintTimeSpan_dhMin(min + 0.1, '\0');
                }
                mUpdateDateTime(dateTimePickerFrom, textBoxFromHour, textBoxFromMin, comboBoxFromAM, fromDT, bEnableFrom);
                mUpdateDateTime(dateTimePickerTo, textBoxToHour, textBoxToMin, comboBoxToAM, toDT, bEnableTo);
                labelReqDur.Text = " "+ reqDur;

            }
            catch (Exception ex)
            {
                CProgram.sLogException("update time", ex);
            }
        }

        private bool mGetRange(out DateTime ArReqStartDT, out DateTime ArReqEndDT, bool AbWarn)
        {
            bool bOk = true;
            DateTime reqStartDT = mReadDateTime(dateTimePickerFrom, textBoxFromHour, textBoxFromMin, comboBoxFromAM);
            DateTime reqEndDT = mReadDateTime(dateTimePickerTo, textBoxToHour, textBoxFromMin, comboBoxToAM);

            if (radioButtonAll.Checked)
            {
                reqStartDT = DateTime.MinValue;
                reqEndDT = DateTime.MaxValue;
            }
            else if (radioButtonPeriod.Checked)
            {

            }
            else if (radioButtonHour.Checked)
            {
                reqEndDT = reqStartDT.AddHours(1);
            }
            else if (radioButton6Hour.Checked)
            {
                reqEndDT = reqStartDT.AddHours(6);

            }
            else if (radioButton12Hour.Checked)
            {
                reqEndDT = reqStartDT.AddHours(12);
            }
            else if (radioButtonDay.Checked)
            {
                reqEndDT = reqStartDT.AddDays(1);

            }
            else if (radioButtonWeek.Checked)
            {
                reqEndDT = reqStartDT.AddDays(7);

            }
            else if (radioButtonMonth.Checked)
            {
                reqEndDT = reqStartDT.AddMonths(1);
            }

            mUpdateFromTo(reqStartDT, reqEndDT);

            ArReqStartDT = reqStartDT;
            ArReqEndDT = reqEndDT;

            if( bOk )
            {
                if( reqEndDT <= reqStartDT && AbWarn)
                {
                    CProgram.sPromptWarning(true, "Holter Files", "Time Range empty");
                }
            }

            return bOk;
        }

        private void mReadUpdateTime()
        {
            DateTime reqStartDT = DateTime.MinValue;
            DateTime reqEndDT = DateTime.MinValue;
            string rangeText = "";

            mGetRange(out reqStartDT, out reqEndDT, false);
        }

        private void mDoHolterFiles()
        {
            DateTime reqStartDT = DateTime.MinValue;
            DateTime reqEndDT = DateTime.MinValue;
            string rangeText = "";

            if (false == mGetRange(out reqStartDT, out reqEndDT, true))
            {
                return;
            }
            mUpdateStats(); // check selected files

            if (radioButtonAll.Checked)
            {
                rangeText = "All";
            }
            else
            {
                double nrMin = (reqEndDT - reqStartDT).TotalMinutes;
                rangeText = CProgram.sDateTimeToString(reqStartDT) + " " +CProgram.sPrintTimeSpan_dhMin(nrMin + 0.1,'\0');
            }
            string firstName = mDoHolterFilesRange(rangeText, reqStartDT, reqEndDT);

            mSetList(firstName); // update list with record info
        }


        private bool mbWriteSamplesGap(FileStream AFileStream, UInt32 ANrSamples)
        {
            bool bOk = false;

            if (AFileStream != null )
            {
                try
                {
                    int minLimit = Int16.MinValue + 1;
                    int maxLimit = Int16.MaxValue - 1;

                    UInt32 i = 0;
                    UInt32 iEnd = ANrSamples;
                    Int32 v;

                    for (; i < iEnd; ++i)
                    {
                        for (UInt32 j = 0; j < _mExecOutNrChannels; ++j)
                        {
                            v = _mExecHolterLastValue[j]
                                + ((_mExecNrSamples & 2) != 0 ? _mExecHolterToggleValue : -_mExecHolterToggleValue);

                            if (_mExecShiftValue > 0)
                            {
                                v = v >> _mExecShiftValue;
                            }
                            if (v < minLimit)
                            {
                                v = minLimit;
                                ++_mExecOutRangeMin;
                            }
                            else if (v > maxLimit)
                            {
                                v = maxLimit;
                                ++_mExecOutRangeMax;
                            }
                            AFileStream.WriteByte((byte)(v & 0x00FF));              // 16 bit
                            AFileStream.WriteByte((byte)((v >> 8) & 0x00FF));
                            _mExecFileSize += 2;
                        }
                        ++_mExecNrSamples;
                    }
                    bOk = true;
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Failed to write to file " + _mExecFileName, ex);
                    bOk = false;
                }
            }
            return bOk;
        }
        private bool mbWriteSamplesArray(FileStream AFileStream, CRecordMit ARecord, UInt32 AStartIndex, UInt32 ANrSamples)
        {
            bool bOk = false;

            if (AFileStream != null && ARecord != null)
            {
                try
                {
                    int minLimit = Int16.MinValue + 1;
                    int maxLimit = Int16.MaxValue - 1;

                    UInt32 i = AStartIndex;
                    UInt32 n = ANrSamples;
                    UInt32 iEnd = i + n;
                    UInt16 nChannels = ARecord.mNrSignals;
                    UInt16 channel;
                    Int32 v;

                    if (iEnd > ARecord.mNrSamples)
                    {
                        n = ARecord.mNrSamples - i;
                    }

                    for (; i < iEnd; ++i)
                    {
                        for (UInt32 j = 0; j < _mExecOutNrChannels; ++j)
                        {
                            channel = _mExecOutUseChannel[j];
                            v = 0;  // clear

                            if (channel <= (UInt16)DHolterUseChannel.CHxx)
                            {
                                v = j < nChannels ? ARecord.mSignals[j].mValues[i] : 0;     // use given channel
                            }
                            else if (channel == (UInt16)DHolterUseChannel.III && nChannels >= 2)
                            {
//                                if (j < nChannels)
                                {
                                    v = ARecord.mSignals[1].mValues[i] - ARecord.mSignals[0].mValues[i];    // III = II - I
                                }
                            }
                            _mExecHolterLastValue[j] = v;   // set last value

                            if (_mExecShiftValue > 0)
                            {
                                v = v >> _mExecShiftValue;
                            }
                            if (v < minLimit)
                            {
                                v = minLimit;
                                ++_mExecOutRangeMin;
                            }
                            else if (v > maxLimit)
                            {
                                v = maxLimit;
                                ++_mExecOutRangeMax;
                            }
                            AFileStream.WriteByte((byte)(v & 0x00FF));              // 16 bit
                            AFileStream.WriteByte((byte)((v >> 8) & 0x00FF));
                            _mExecFileSize += 2;
                        }
                        ++_mExecNrSamples;
                    }
                    bOk = true;
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Failed to write to file " + _mExecFileName, ex);
                    bOk = false;
                }
            }
            return bOk;
        }


        public bool mbCheckDoFillGap(double AGapSec, UInt32 AIndex, UInt16 AMaxHalfBlockSize = 10, float ABlockDiffSec = 0.5F)
        {
            return CRecordMit.sbCheckDoFillGap(AGapSec, AIndex);

        }


        private bool mbMergeWriteSamples(out string ArAddResult, FileStream AFileStream, DateTime AReqStartUTC, DateTime AReqEndUTC, UInt32 AFileIndex, CRecordMit ARecord)
        {
            bool bOk = false;
            string addResult = "!";

            if (AFileStream != null && ARecord != null)
            {

                int defMask = (1 << _mExecInNrChannels) - 1;
                int channelMask = _mExecChannelMask == 0 ? defMask : _mExecChannelMask;
                UInt32 i, j, iStart, iEnd, nSamples=ARecord.mNrSamples;


                try
                {
                    double relStartSec = ( ARecord.mBaseUTC - _mExecEndUTC).TotalSeconds;
                    DateTime startUTC = ARecord.mBaseUTC;
                    float stripSec = ARecord.mGetSamplesTotalTime();
                    DateTime endUTC = startUTC.AddSeconds(stripSec);

                    bOk = true;
                    if (startUTC >= AReqEndUTC)
                    {
                        addResult = "strip after period";
                    }
                    else if( endUTC < AReqStartUTC)
                    {
                        addResult = "strip before period";
                    }
                    else if( stripSec < 0.01)
                    {
                        addResult = "strip empty";
                    }
                    else
                    {
                        iStart = 0;
                        iEnd = nSamples;

                        if (relStartSec < 0)
                        {
                            // overlap: current time past start of strip
                            iStart = (UInt32)(-relStartSec * _mExecSamplesPerSec);

                            addResult = "overlap " + iStart.ToString() + " " + CProgram.sPrintTimeSpan_dhmSec(-relStartSec, "");
                        }
                        else if (false == mbCheckDoFillGap(relStartSec, AFileIndex))
                        {
                            // assume gap is small and ECG is continueing in next file
                            addResult = "disregard gap " + relStartSec.ToString("0.000") + " Sec";
                            startUTC = _mExecEndUTC;
                        }
                        else
                        {
                            // fill in gap: start of strip in future
                            UInt32 nGap = (UInt32)(relStartSec * _mExecSamplesPerSec);

                            addResult = "fill gap " + nGap.ToString() + " " + CProgram.sPrintTimeSpan_dhmSec(relStartSec, "");

                             bOk = mbWriteSamplesGap(AFileStream, nGap);

                            _mExecEndUTC = _mExecEndUTC.AddSeconds(relStartSec);
                        }
                        if (AReqEndUTC < DateTime.MaxValue)
                        {
                            if (startUTC >= AReqEndUTC)
                            {
                                iEnd = 0;
                                addResult += " past end";

                            }
                            else
                            {
                                iEnd = (UInt32)((AReqEndUTC - startUTC).TotalSeconds * _mExecSamplesPerSec + 0.5);

                                if (iEnd > nSamples)
                                {
                                    iEnd = nSamples;
                                    addResult += "*";
                                }
                                else if (iEnd < nSamples)
                                {
                                    addResult += "%";

                                }
                                else
                                {
                                    addResult += "$";
                                }
                            }
                        }
                        if (iEnd <= iStart)
                        {
                            addResult += " no add";
                        }
                        else
                        {
                            // add range
                            nSamples = iEnd - iStart;
                            double addSec = nSamples / (double)_mExecSamplesPerSec;
                            addResult += " add " + nSamples.ToString() + " " + CProgram.sPrintTimeSpan_dhmSec(addSec, "");

                            bOk = mbWriteSamplesArray(AFileStream, ARecord, iStart, nSamples);

                            _mExecEndUTC = _mExecEndUTC.AddSeconds(addSec);
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Failed to write to file " + _mExecFileName, ex);
                    bOk = false;
                }
            }
            ArAddResult = addResult;
            return bOk;
        }

        private string mDoHolterFilesRange( string ARangeText, DateTime AStartDT, DateTime AEndDT)
        {
            bool bOk = false;
            UInt32 nToDo = 0;
            UInt32 nFiles = 0;
            UInt32 iFile = 0;
            string firstFileName = "";
            FileStream fs = null;
            List<string> fileResultList = new List<string>();
            string fileResult;
            double secPerSample = 0;
            DateTime startMergeDT = DateTime.Now;
            DateTime reqStartUTC = AStartDT;
            DateTime reqEndUTC = AEndDT;
            string nameMethod = "Holter";
            UInt32 invChannels = mReadInvertMask();

            string firstName = "";
            bool bFindFirst = true;

            string stepTitle;
            CProgram.sSetBusy(true);

            try
            {
                mSetTitle("Holter files");

                if (_mExecRecord != null)
                {
                    _mExecRecord.mDisposeSignals();
                }

                _mExecMethod = "Holter"; //+ "[" + ARangeText + "] merge";
                _mExecFileName = "";
                _mExecFilePath = "";
                _mExecFileSize = 0;
                _mExecBeginUTC = DateTime.MinValue;
                _mExecRecord = null;
                _mExecResult = "holter merging";
                _mExecNrSamples = 0;
                _mExecInNrChannels = 0;
                _mExecChannelMask = 0;
                _mExecShiftValue = 0;
                _mExecEndUTC = DateTime.MinValue;

                mbDeterminChannels(null, DateTime.MinValue);   // reset

                foreach (CHolterFile node in _mHolterFileList)
                {
                    if (node._mbActive && node._mbSelected && node._mFileSize > 10)
                    {
                        ++nToDo;
                    }
                }
                bOk = true;

                mSetTitle("Holter merge " + nToDo.ToString() + " files[" + ARangeText + "]");
                labelExecResults.Text = _mExecResult + "...";

                fileResult = _mExecMethod + "[" + ARangeText + "] " + _mDeviceType + " files: " + nToDo.ToString() ;
                CProgram.sLogLine(fileResult);
                            if (fileResultList != null) fileResultList.Add(fileResult);

                foreach (CHolterFile node in _mHolterFileList)
                {
                    if (node._mbActive && node._mbSelected && node._mFileSize > 10)
                    {
                        ++iFile;

                        stepTitle = _mExecMethod + " " + iFile.ToString() + " / " + nToDo.ToString();

                        mSetTitle(stepTitle + " Loading " + node._mFileName);

                        if (false == node.mbLoadFile(_mStudyRecodingPath, invChannels))        // load record from file
                        {
                            _mExecResult = " failed load " + node._mFileName;
                            bOk = false;
                            break;
                        }
                        if (iFile == 1)
                        {
                            UInt32 lengthSec = 0;
                            DateTime startDT = _mExecBeginUTC.AddMinutes(_mExecTimeZoneMin);
                            // first file
                            firstFileName = node._mFileName;
                            _mExecTimeZoneMin = (Int16)node._mRecord.mTimeZoneOffsetMin;

                            if (reqStartUTC != DateTime.MinValue) reqStartUTC = AStartDT.AddMinutes(-_mExecTimeZoneMin);
                            if (reqEndUTC != DateTime.MaxValue) reqEndUTC = AEndDT.AddMinutes(-_mExecTimeZoneMin);

                            if (false == mbDeterminChannels(node._mRecord, reqStartUTC))
                            {
                                _mExecResult = " failed determin channnels";
                                bOk = false;
                                break;
                            }
                            if(radioButtonAll.Checked )
                            {
                                lengthSec = 0;
                            }
                            else
                            {
                                lengthSec = (UInt32)((AEndDT - AStartDT).TotalSeconds + 0.5);

                                startDT = AStartDT;

                            }


                            secPerSample = _mExecSamplesPerSec == 0 ? 0.0 : 1.0 / _mExecSamplesPerSec;
                            if (false == mbDoMakeFileName(node._mDeviceSnr,  _mStudyIndex, nameMethod, startDT, lengthSec))
                            {
                                _mExecResult = " failed holter path";
                                bOk = false;
                                break;
                            }
                            _mExecResult = _mExecMethod + " " + _mDeviceType + " " + nToDo.ToString() + " files >> " + _mExecFileName + " "
                                        + _mExecOutNrChannels.ToString() + " channels @ " + _mExecSamplesPerSec.ToString() + " Sps: writing...";
                            labelExecResults.Text = _mExecResult;
                            fileResult = _mExecResult + " " + _mExecChannelMask.ToString("X");
                            CProgram.sLogLine(fileResult);
                            if (fileResultList != null) fileResultList.Add(fileResult);


                            if (false == mbCreateDataFile(out fs))
                            {
                                _mExecResult = " failed load " + node._mFileName;
                                bOk = false;
                                break;
                            }
                        }
                        else
                        {
                            bOk = mbCheckChannels(node._mRecord);
                            if( false == bOk )
                            {
                                break;
                            }
                        }
                        double curSec = _mExecNrSamples * secPerSample;
                        double addSec = node._mRecNrSamples * secPerSample;
                        DateTime curDT = _mExecBeginUTC.AddSeconds(curSec + _mExecTimeZoneMin * 60);
                        string addResult = "";

                        mSetTitle(stepTitle + " adding " + node._mFileName + ": sample "
                            + _mExecNrSamples.ToString() + " + " + node._mRecNrSamples.ToString()
                            + "  (" + CProgram.sPrintTimeSpan_dhmSec(curSec, "")
                            + " + " + CProgram.sPrintTimeSpan_dhmSec(addSec, "") + ")");

                        fileResult = _mExecMethod + " " + _mDeviceType + " " + iFile.ToString("D4")
                            +"[" + _mExecNrSamples.ToString("0000000000") + " " + CProgram.sDateTimeToYMDHMS(curDT) + " " + CProgram.sPrintTimeSpan_dhmSec(curSec, "")
                            + " @" + _mExecFileSize.ToString("00000000000")
                            + "]: " 
                            +
                            node._mFileName + " " + node._mRecNrChannels.ToString() + " channels @ " + node._mRecSamplesPerSec.ToString() + " Sps "
                            + node._mRecNrSamples.ToString() + "= " + CProgram.sPrintTimeSpan_dhmSec(node._mRecDurationSec, "");
                        CProgram.sLogLine(fileResult);

                        bOk = mbMergeWriteSamples(out addResult, fs, reqStartUTC, reqEndUTC, nFiles, node._mRecord );
                        if (bOk)
                        {
                            ++nFiles;
                            fileResult += " " + addResult;
                        }
                        else
                        {
                            fileResult += " failed! " + addResult;

                            _mExecResult = stepTitle + " failed write part from " + node._mFileName;
                            break;
                        }
                        if( bFindFirst && _mExecNrSamples > 0 )
                        {
                            bFindFirst = false; // file with points added to the output file
                            firstName = node._mFileName;
                        }

                        fs.Flush();
                        UInt32 fsSize = (UInt32)fs.Length;

                        if (fsSize != _mExecFileSize)
                        {
                            fileResult += " stream = " + fsSize.ToString();
                        }

                        if (fileResultList != null) fileResultList.Add(fileResult);
                        node.mbDisposeData();
                        mUpdateMemInfo();
                        Application.DoEvents();


                    }
                }
                double dT = (DateTime.Now - startMergeDT).TotalSeconds;
                mSetTitle(_mExecMethod + " " + nFiles.ToString() + " / " + nToDo.ToString()
                    + (bOk ? " done ok" : " failed!") + " in " + CProgram.sPrintTimeSpan_dhmSec(dT, ""));

                if (bOk)
                {
                    double t1Sec = (_mExecEndUTC - _mExecBeginUTC).TotalSeconds;
                    double t2Sec = _mExecNrSamples / (double)(_mExecSamplesPerSec);
                    _mExecResult = _mExecMethod + " " + _mDeviceType + " " + _mExecFileName + " "
                        + _mExecOutNrChannels.ToString() + " ch@ " + _mExecSamplesPerSec.ToString() + " Sps: " + _mExecNrSamples.ToString()
                        + "= " + CProgram.sPrintTimeSpan_dhmSec(t2Sec, "")
                        + " " + (_mExecFileSize/1000000).ToString()+"MB ";

                    if (Math.Abs(t1Sec - t2Sec) > iFile)
                    {
                        _mExecResult += "!= " + CProgram.sPrintTimeSpan_dhmSec(t1Sec, "");
                    }

                    labelExecResults.Text = _mExecResult + " save header...";
                    // write hea 
                    bOk = mbSaveHeader();

                    if (checkBoxCopyFirstJson.Checked && firstFileName != null && firstFileName.Length > 0)
                    {
                        // copy first json file to be used by merged
                        string fromJson = Path.Combine(_mStudyRecodingPath, Path.ChangeExtension(firstFileName, ".json"));
                        try
                        {
                            if (File.Exists(fromJson))
                            {
                                string toJson = Path.ChangeExtension(_mExecFilePath, "json");
                                File.Copy(fromJson, toJson, true);
                            }
                        }
                        catch (Exception ex)
                        {
                            CProgram.sLogException("Failed copy json " + fromJson, ex);
                        }

                    }
                }
                if (bOk)
                {

                }
                fileResult = _mExecResult + (bOk ? " done ok " : " failed! ");
                CProgram.sLogLine(fileResult + " in " + CProgram.sPrintTimeSpan_dhmSec(dT, ""));
                if (fileResultList != null) fileResultList.Add(fileResult + " in " + CProgram.sPrintTimeSpan_dhmSec(dT, ""));

                labelExecResults.Text = fileResult;
                // close file
                if (fs != null)
                {
                    fs.Flush();

                    UInt32 fsSize = (UInt32)fs.Length;

                    fs.Close();

                    string strSize = _mExecFileName + " " + _mExecFileSize.ToString() + "B";

                    if( fsSize != _mExecFileSize)
                    {
                        strSize += " stream = " + fsSize.ToString();
                    }
                    try
                    {
                        FileInfo fi = new FileInfo(_mExecFilePath);

                        if ( fi != null )
                        {
                            if((UInt32)fi.Length != _mExecFileSize)
                            {
                                strSize += " file = " + fi.Length.ToString();
                            }
                        }
                    }
                    catch ( Exception ex)
                    {
                        CProgram.sLogException("Failed file info " + _mExecFileName, ex);
                        strSize += " file info failed";
                    }


                    if (fileResultList != null)
                    {
                        fileResultList.Add(strSize);

                        string resultFile = _mExecFilePath + ".MergeLog";
                        try
                        {
                            File.WriteAllLines(resultFile, fileResultList);
                        }
                        catch (Exception ex)
                        {
                            CProgram.sLogException("Failed write " + _mExecMethod + " " + resultFile, ex);
                        }
                    }
                    if (bOk)
                    {
                        // load  from file the result
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed " + _mExecMethod + " " + _mDeviceType, ex);
            }
            CProgram.sSetBusy(false);
            if (checkBoxShowResult.Checked)
            {
                string text = _mExecMethod + " " + _mDeviceType + " result:\r\n";

                foreach (string s in fileResultList)
                {
                    text += s + "\r\n";
                }
                ReqTextForm.sbShowText(_mExecMethod + " " + _mDeviceType + " S" + _mStudyIndex.ToString(), 
                    ( bOk ? "result OK" : "Failed result" ), text);
            }
            return firstName;
        }

        private void buttonM24_Click(object sender, EventArgs e)
        {
            mAddFromTo(-24);
        }

        private void mAddFromTo( Int32 ANrHours)
        {
            DateTime reqStartDT = DateTime.MinValue;
            DateTime reqEndDT = DateTime.MinValue;

            if (false == mGetRange(out reqStartDT, out reqEndDT, true))
            {
                return;
            }
            bool bShft = (Control.ModifierKeys & Keys.ShiftKey) == Keys.ShiftKey;
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl)
            {
                if (reqStartDT != DateTime.MinValue) reqStartDT = reqStartDT.AddHours(ANrHours);
            }
            else if (bShft)
            {
                if (reqEndDT != DateTime.MaxValue) reqEndDT = reqEndDT.AddHours(ANrHours);
            }
            else
            {
                if (reqStartDT != DateTime.MinValue) reqStartDT = reqStartDT.AddHours(ANrHours);
                if (reqEndDT != DateTime.MaxValue) reqEndDT = reqEndDT.AddHours(ANrHours);
            }
            mUpdateFromTo(reqStartDT, reqEndDT);
        }

        private void mResetDT( ref DateTime ADateTime, bool AbResetHours)
        {
            if (ADateTime != DateTime.MinValue && ADateTime != DateTime.MaxValue)
            {
                ADateTime = new DateTime(ADateTime.Year, ADateTime.Month, ADateTime.Day, (AbResetHours ? 0 : ADateTime.Hour), 0, 0);
            }
        }
        private void mResetFromToMin(bool AbResetHours)
        {
            DateTime reqStartDT = DateTime.MinValue;
            DateTime reqEndDT = DateTime.MinValue;

            if (false == mGetRange(out reqStartDT, out reqEndDT, true))
            {
                return;
            }
            bool bShft = (Control.ModifierKeys & Keys.ShiftKey) == Keys.ShiftKey;
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl)
            {
                mResetDT(ref reqStartDT, AbResetHours);
            }
            else if (bShft)
            {
                mResetDT(ref reqEndDT, AbResetHours);
            }
            else
            {
                mResetDT(ref reqStartDT, AbResetHours);
                mResetDT(ref reqEndDT, AbResetHours);
            }
            mUpdateFromTo(reqStartDT, reqEndDT);
        }

        private void buttonM6_Click(object sender, EventArgs e)
        {
            mAddFromTo(-6);
        }

        private void buttonM1_Click(object sender, EventArgs e)
        {
            mAddFromTo(-1);
        }

        private void buttonP1_Click(object sender, EventArgs e)
        {
            mAddFromTo(1);
        }

        private void buttonP6_Click(object sender, EventArgs e)
        {
            mAddFromTo(6);
        }

        private void buttonP24_Click(object sender, EventArgs e)
        {
            mAddFromTo(24);
        }

        private void buttonX00_Click(object sender, EventArgs e)
        {
            mResetFromToMin(true);
        }

        private void button0h00_Click(object sender, EventArgs e)
        {
            mResetFromToMin(false);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            mSelectPeriod();
        }

        private void radioButtonAll_Validated(object sender, EventArgs e)
        {
            mUpdatePeriod();
        }
        private void mUpdatePeriod()
        {
            mAddFromTo(0);  // update period time (enable from to)
        }

        private void radioButtonPeriod_Validated(object sender, EventArgs e)
        {
            mUpdatePeriod();
        }

        private void radioButtonHour_Validated(object sender, EventArgs e)
        {
            mUpdatePeriod();
        }

        private void radioButton6Hour_Validated(object sender, EventArgs e)
        {
            mUpdatePeriod();
        }

        private void radioButton12Hour_Validated(object sender, EventArgs e)
        {
            mUpdatePeriod();
        }

        private void radioButtonDay_Validated(object sender, EventArgs e)
        {
            mUpdatePeriod();
        }

        private void radioButtonWeek_Validated(object sender, EventArgs e)
        {
            mUpdatePeriod();
        }

        private void radioButtonMonth_Validated(object sender, EventArgs e)
        {
            mUpdatePeriod();
        }

        private void radioButtonPeriod_CheckedChanged(object sender, EventArgs e)
        {
            mUpdatePeriod();
        }

        private void radioButtonAll_CheckedChanged(object sender, EventArgs e)
        {
            mUpdatePeriod();
        }

        private void radioButtonHour_CheckedChanged(object sender, EventArgs e)
        {
            mUpdatePeriod();
        }

        private void radioButton6Hour_CheckedChanged(object sender, EventArgs e)
        {
            mUpdatePeriod();
        }

        private void radioButton12Hour_CheckedChanged(object sender, EventArgs e)
        {
            mUpdatePeriod();
        }

        private void radioButtonDay_CheckedChanged(object sender, EventArgs e)
        {
            mUpdatePeriod();
        }

        private void radioButtonWeek_CheckedChanged(object sender, EventArgs e)
        {
            mUpdatePeriod();
        }

        private void radioButtonMonth_CheckedChanged(object sender, EventArgs e)
        {
            mUpdatePeriod();
        }

        private void labelFrom_Click(object sender, EventArgs e)
        {
            CHolterFile node = mGetCursorNode();

            if( node != null)
            {
                DateTime reqFromDT, reqEndDT;

                if( mGetRange(out reqFromDT, out reqEndDT, true))
                {
                    reqFromDT = node._mFileStartDT;
                    mUpdateFromTo(reqFromDT, reqEndDT);
                }
            }

        }

        private CHolterFile mGetCursorNode()
        {
            CHolterFile node = null;

            if (dataGridView.CurrentRow != null)
            {
                string selectedFileName = "";

                try
                {
                    if (dataGridView.CurrentRow.Cells.Count > (int)DHolterTableVars.FileName
                                        && dataGridView.CurrentRow.Cells[(int)DHolterTableVars.FileName].Value != null)
                    {
                        selectedFileName = dataGridView.CurrentRow.Cells[(int)DHolterTableVars.FileName].Value.ToString();

                        node = mGetFileFromList( selectedFileName);
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Failed to run Analysis Form", ex);
                }
            }
            return node;
        }

        private void labelTo_Click(object sender, EventArgs e)
        {
            CHolterFile node = mGetCursorNode();

            if (node != null)
            {
                DateTime reqFromDT, reqEndDT;

                if (mGetRange(out reqFromDT, out reqEndDT, true))
                {
                    double durSec = node._mFileDurationSec;
                    if (durSec <= 1) durSec = _mHolterLengthSec;

                    reqEndDT = node._mFileStartDT.AddSeconds(durSec);
                    mUpdateFromTo(reqFromDT, reqEndDT);
                }
            }

        }

        private void checkBoxCh1_CheckStateChanged(object sender, EventArgs e)
        {
            mCheckSetAll();
        }

        private void mCheckSetAll()
        {
  //          if (checkBoxChAll.Checked)
            {
                UInt32 mask = mReadChannelCheckmarks(true);

                    checkBoxChAll.Checked = mask == 0;
            }
        }

        private void checkBoxCh2_CheckStateChanged(object sender, EventArgs e)
        {
            mCheckSetAll();
        }

        private void checkBoxCh3_CheckStateChanged(object sender, EventArgs e)
        {
            mCheckSetAll();
        }

        private void checkBoxCh4_CheckStateChanged(object sender, EventArgs e)
        {
            mCheckSetAll();
        }

        private void checkBoxCh6_CheckStateChanged(object sender, EventArgs e)
        {
            mCheckSetAll();
        }

        private void checkBoxCh7_CheckStateChanged(object sender, EventArgs e)
        {
            mCheckSetAll();
        }

        private void checkBoxCh8_CheckedChanged(object sender, EventArgs e)
        {
            mCheckSetAll();
        }

        private void checkBoxCh9_CheckedChanged(object sender, EventArgs e)
        {
            mCheckSetAll();
        }

        private void checkBoxCh10_CheckStateChanged(object sender, EventArgs e)
        {
            mCheckSetAll();
        }

        private void checkBoxCh4_CheckedChanged(object sender, EventArgs e)
        {
            mCheckSetAll();
        }

        private void checkBoxCh5_CheckStateChanged(object sender, EventArgs e)
        {
            mCheckSetAll();
        }

        private void checkBoxCh8_CheckStateChanged(object sender, EventArgs e)
        {
            mCheckSetAll();
        }

        private void checkBoxCh9_CheckStateChanged(object sender, EventArgs e)
        {
            mCheckSetAll();
        }

        private void buttonHolterResult_Click(object sender, EventArgs e)
        {
            if (_mExecFilePath != null && _mExecFilePath.Length > 4 && _mExecFileName != null && _mExecFileName.Length > 1)
            {
                mbRunHolterProg(_mExecMethod, Path.GetDirectoryName(_mExecFilePath), _mExecFileName);
            }
        }

        private void panelPeriod_Paint(object sender, PaintEventArgs e)
        {

        }

        private bool mbSaveHeader()
        {
            bool bOk = false;

            _mExecRecord = new CRecordMit(CDvtmsData.sGetDBaseConnection());
            if (_mExecRecord != null)
            {
                _mExecRecord.mAdcBits = _mExecOutAdcBits;
                _mExecRecord.mAdcGain = _mExecOutAdcGain;
                _mExecRecord.mNrSignals = _mExecOutNrChannels;
                _mExecRecord.mTimeZoneOffsetMin = _mExecTimeZoneMin;
                _mExecRecord.mFileName = _mExecFileName + ".dat";
                _mExecRecord.mBaseUTC = _mExecBeginUTC;
                _mExecRecord.mSampleFrequency = _mExecSamplesPerSec;
                bOk = _mExecRecord.mbSaveMITHeader(_mExecFilePath, 16);
            }

            return bOk;
        }

        private void labelReqDur_Click(object sender, EventArgs e)
        {
            mReadUpdateTime();
        }

        private void label36_Click(object sender, EventArgs e)
        {
            mReadUpdateTime();
        }

        private void buttonOpenFile2_Click(object sender, EventArgs e)
        {
            mOpenFile();
        }

        private UInt32 mReadInvertMask()
        {
            UInt32 invChannels = 0;

            if (checkBoxInv1.Checked) invChannels |= (1 << 0);
            if (checkBoxInv2.Checked) invChannels |= (1 << 1);
            if (checkBoxInv3.Checked) invChannels |= (1 << 2);
            if (checkBoxInv4.Checked) invChannels |= (1 << 3);
            if (checkBoxInv5.Checked) invChannels |= (1 << 4);
            if (checkBoxInv6.Checked) invChannels |= (1 << 5);
            if (checkBoxInv7.Checked) invChannels |= (1 << 6);
            if (checkBoxInv8.Checked) invChannels |= (1 << 7);
            if (checkBoxInv8.Checked) invChannels |= (1 << 8);
            if (checkBoxInv10.Checked) invChannels |= (1 << 9);

            return invChannels;
        }


        //* holter
    }
    public class CHolterFile
    {
        public bool _mbActive;
        public bool _mbShown;
        public bool _mbSelected;
        public bool _mbHolter;  // "H_"

        public string _mFileName;
        public DateTime _mFileUtc;
        public UInt32 _mFileSize;
        public DateTime _mFileStartDT;
        public UInt32 _mFileDurationSec;

        public string _mDeviceSnr;
        public DateTime _mRecStartUTC;
        public Int16 _mRecTimeZoneMin;
        public float _mRecDurationSec;
        public UInt16 _mRecNrChannels;
        public UInt32 _mRecNrSamples;
        public UInt32 _mRecSamplesPerSec;

        public CRecordMit _mRecord;

        public CHolterFile()
        {
            _mbActive = false;
            _mbShown = false;
            _mbSelected = false;
            _mbHolter = false;
            _mFileName = "";
            _mFileUtc = DateTime.MinValue;
            _mFileSize = 0;
            _mFileStartDT = DateTime.MinValue;
            _mFileDurationSec = 0;
            _mDeviceSnr = "";

            _mRecStartUTC = DateTime.MinValue;
            _mRecTimeZoneMin = 0;
            _mRecDurationSec = 0;
            _mRecNrChannels = 0;
            _mRecNrSamples = 0;
            _mRecSamplesPerSec = 0;

            _mRecord = null;
        }
        public bool mbSetFileName(String AFileName)
        {
            bool bOk = false;

            try
            {
                if (AFileName != null && AFileName.Length > 2)
                {
                    DateTime startDT = DateTime.MinValue;
                    bool bHolter = false;
                    string sNr = "";
                    string strip = "";
                    UInt32 durationSec = 0;

                    string[] parts = AFileName.Split('_');
                    int nParts = parts == null ? 0 : parts.Length;

                    _mFileName = AFileName;

                    if (nParts > 2)
                    {
                        if (AFileName.StartsWith("H_"))
                        {
                            // H_<snr>_ID_yyyMMddHHmmssH1234
                            bHolter = true;
                            sNr = parts[1];
                            strip = parts[nParts - 1];
                        }
                        else
                        {
                            sNr = parts[0];
                            strip = parts[nParts - 1];
                            // <snr>_ID_yyyMMddHHmmss
                        }
                        if (strip != null && strip.Length > 0)
                        {
                            int pos = strip.IndexOf('H');
                            if (pos >= 0)
                            {
                                string durationStr = strip.Substring(pos + 1);

                                UInt32.TryParse(durationStr, out durationSec);
                                strip = strip.Substring(0, pos);
                            }
                            bOk = CProgram.sbParseYMDHMS(strip, out startDT, false);
                        }
                    }
                    _mbHolter = bHolter;
                    _mFileStartDT = startDT;
                    _mFileDurationSec = durationSec;
                    _mDeviceSnr = sNr;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed to set file name " + AFileName, ex);
            }

            return bOk;

        }

        
        public bool mbLoadFile( string ADirPath, UInt32 AInvertChannels)
        {
            bool bOk = false;

            if (_mRecord != null && _mRecord.mSignals != null
                && _mRecord.mNrSignals > 0 && _mRecord.mSignals.Length == _mRecord.mNrSignals)
            {
                // already loaded 
                bOk = true;
            }
            else
            {
                CRecordMit record = new CRecordMit(CDvtmsData.sGetDBaseConnection());

                if (record != null)
                {
                    string fileHea = Path.ChangeExtension(_mFileName, ".hea");
                    string pathHea = Path.Combine(ADirPath, fileHea);

                    bOk = record.mbReadFile(pathHea, AInvertChannels, DateTime.UtcNow, CProgram.sGetLocalTimeZoneOffsetMin(), DMoveTimeZone.None,
                        true, false, DImportConverter.IntriconSironaMit, DImportMethod.Unknown, "");

                    _mRecord = record;

                    _mRecStartUTC = record.mBaseUTC;
                    _mRecTimeZoneMin = (Int16)record.mTimeZoneOffsetMin;
                    _mRecDurationSec = record.mGetSamplesTotalTime();
                    _mRecNrChannels = record.mNrSignals;
                    _mRecNrSamples = record.mNrSamples;
                    _mRecSamplesPerSec = record.mSampleFrequency;

                }
            }
            return bOk;
        }

        public bool mbDisposeData()
        {
            bool bOk = false;

            if( _mRecord != null )
            {
                _mRecord.mDisposeSignals();
                bOk = true;
            }
            return bOk;
        }
    }
}
