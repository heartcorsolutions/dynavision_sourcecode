﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Program_Base;
using EventboardEntryForms;
using System.Windows.Forms;

namespace EventBoard.EntryForms
{
    public enum DPhysicianInfoVars
    {
        // primary keys
        Label = 0,
        // variables
        GenderTitle,
        AcademicTitle,
        FirstName,
        MiddleName,
        LastName,
        DateOfBirth,
        Gender_IX,
        Hospital_IX,
        Address,
        ZipCode,
        HouseNr,
        City,
        State,
        CountryCode,
        Phone1,
        Phone2,
        Cell,
        Skype,
        Email,
        Instruction,

        NrSqlVars       // keep as last
    };

    public class CPhysicianInfo : CSqlDataTableRow
    {

        public string _mLabel;
        public string _mGenderTitle;
        public string _mAcademicTitle;
        public CEncryptedString _mFirstName;
        public CEncryptedString _mMiddleName;
        public CEncryptedString _mLastName;
        public CEncryptedInt _mDateOfBirth; 
        public UInt16 _mGender_IX;
        public UInt32 _mHospital_IX;
        public CEncryptedString _mAddress;
        public CEncryptedString _mZipCode;
        public CEncryptedString _mHouseNr;
        public string _mState;
        public string _mCity;
        public string _mCountryCode;
        public CEncryptedString _mPhone1;
        public CEncryptedString _mPhone2;
        public CEncryptedString _mCell;
        public CEncryptedString _mSkype;
        public CEncryptedString _mEmail;
        public CEncryptedString _mInstruction;

        public CPhysicianInfo(CSqlDBaseConnection ASqlConnection) : base(ASqlConnection, "d_PhysicianInfo")
        {
            try
            {
                UInt64 maskPrimary = CSqlDataTableRow.sGetMask((UInt16)DPhysicianInfoVars.Label);
                UInt64 maskValid = CSqlDataTableRow.sGetMaskRange((UInt16)0, (UInt16)DPhysicianInfoVars.NrSqlVars - 1);// mGetValidMask(false);

                mInitTableMasks(maskPrimary, maskValid);  // set primairyInsertFlags

                _mFirstName = new CEncryptedString("FirstName", DEncryptLevel.L1_Program, 32);
                _mMiddleName = new CEncryptedString("MiddleName", DEncryptLevel.L1_Program, 16);
                _mLastName = new CEncryptedString("LastName", DEncryptLevel.L1_Program, 32);
                _mDateOfBirth = new CEncryptedInt("DateOfBirth", DEncryptLevel.L1_Program);
                _mAddress = new CEncryptedString("Address", DEncryptLevel.L1_Program, 128);
                _mZipCode = new CEncryptedString("Zip", DEncryptLevel.L1_Program, 16);
                _mHouseNr = new CEncryptedString("mHouseNumber", DEncryptLevel.L1_Program, 8);
                _mPhone1 = new CEncryptedString("Phone1", DEncryptLevel.L1_Program, 16);
                _mPhone2 = new CEncryptedString("Phone2", DEncryptLevel.L1_Program, 16);
                _mCell = new CEncryptedString("Cell", DEncryptLevel.L1_Program, 16);
                _mEmail = new CEncryptedString("Email", DEncryptLevel.L1_Program, 128);
                _mSkype = new CEncryptedString("Skype", DEncryptLevel.L1_Program, 128);
                _mInstruction = new CEncryptedString("Instruction", DEncryptLevel.L1_Program, 128);

                mClear();
            }
            catch ( Exception ex )
            {
                CProgram.sLogException("Create CPhysicianInfo", ex);
            }
        }
        public override CSqlDataTableRow mCreateNewRow()
        {
            return new CPhysicianInfo(mGetSqlConnection());
        }
        public override bool mbCopyTo(CSqlDataTableRow ATo) // derived  public override void mbCopyTo() must call base.mbCopyTo(ATo)
        {
            bool bOk = false;

            if (ATo != null)
            {
                CPhysicianInfo to = ATo as CPhysicianInfo;

                if (to != null)
                {
                    bOk = base.mbCopyTo(ATo);

                    to._mLabel = _mLabel;
                    to._mGenderTitle = _mGenderTitle;
                    to._mAcademicTitle = _mAcademicTitle;
                    _mFirstName.mbCopyTo(ref to._mFirstName);
                    _mMiddleName.mbCopyTo(ref to._mMiddleName); 
                    _mLastName.mbCopyTo(ref to._mLastName);
                    _mDateOfBirth.mbCopyTo(ref to._mDateOfBirth);
                    to._mGender_IX = _mGender_IX;
                    to._mHospital_IX = _mHospital_IX;
                    _mAddress.mbCopyTo(ref to._mAddress);
                    _mZipCode.mbCopyTo(ref to._mZipCode); ;
                    _mHouseNr.mbCopyTo(ref to._mHouseNr); ;
                    to._mCity = _mCity;
                    to._mState = _mState;
                    _mCountryCode = to._mCountryCode;
                    _mPhone1.mbCopyTo(ref to._mPhone1);
                    _mPhone2.mbCopyTo(ref to._mPhone2);
                    _mCell.mbCopyTo(ref to._mCell);
                    _mEmail.mbCopyTo(ref to._mEmail);
                    _mSkype.mbCopyTo(ref to._mSkype);
                    to._mLabel = _mLabel;
                    to._mInstruction.mbCopyFrom(_mInstruction);

                }
            }
            return bOk;
        }
        public override void mClear()
        {
            base.mClear();
            _mLabel = "";
            _mGenderTitle = "";
            _mAcademicTitle = "";
            _mFirstName.mClear();
            _mMiddleName.mClear();
            _mLastName.mClear();
            _mDateOfBirth.mClear();
            _mGender_IX = 0;
            _mHospital_IX = 0;
            _mAddress.mClear();
            _mZipCode.mClear();
            _mHouseNr.mClear();
            _mCity = "";
            _mState = "";
            _mCountryCode = "";
            _mPhone1.mClear();
            _mPhone2.mClear();
            _mCell.mClear();
            _mEmail.mClear();
            _mSkype.mClear();
            _mInstruction.mClear();
        }
        public override DSqlDataType mVarGetSet(DSqlDataCmd ACmd, UInt16 AVarIndex, ref string AVarSqlName, ref string AStringValue, out bool ArbValid)
        {
            switch ((DPhysicianInfoVars)AVarIndex)
            {
                case DPhysicianInfoVars.Label:
                    return mSqlGetSetString(ref _mLabel, "Label", ACmd, ref AVarSqlName, ref AStringValue, 32, out ArbValid);
                case DPhysicianInfoVars.GenderTitle:
                    return mSqlGetSetString(ref _mGenderTitle, "GenderTitle", ACmd, ref AVarSqlName, ref AStringValue, 32, out ArbValid);
                case DPhysicianInfoVars.AcademicTitle:
                    return mSqlGetSetString(ref _mAcademicTitle, "AcademicTitle", ACmd, ref AVarSqlName, ref AStringValue, 32, out ArbValid);
                case DPhysicianInfoVars.FirstName:
                    return mSqlGetSetEncryptedString(ref _mFirstName, "FirstName_ES", ACmd, ref AVarSqlName, ref AStringValue, 64, out ArbValid);
                case DPhysicianInfoVars.MiddleName:
                    return mSqlGetSetEncryptedString(ref _mMiddleName, "MiddleName_ES", ACmd, ref AVarSqlName, ref AStringValue, 32, out ArbValid);
                case DPhysicianInfoVars.LastName:
                    return mSqlGetSetEncryptedString(ref _mLastName, "LastName_ES", ACmd, ref AVarSqlName, ref AStringValue, 64, out ArbValid);
                case DPhysicianInfoVars.DateOfBirth:
                    return mSqlGetSetEncryptedInt(ref _mDateOfBirth, "DateOfBirth_EI", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DPhysicianInfoVars.Gender_IX:
                    return mSqlGetSetUInt16(ref _mGender_IX, "Gender_IX", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DPhysicianInfoVars.Hospital_IX:
                    return mSqlGetSetUInt32(ref _mHospital_IX, "Hospital_IX", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DPhysicianInfoVars.Address:
                    return mSqlGetSetEncryptedString(ref _mAddress, "Address_ES", ACmd, ref AVarSqlName, ref AStringValue, 250, out ArbValid);
                case DPhysicianInfoVars.ZipCode:
                    return mSqlGetSetEncryptedString(ref _mZipCode, "Zip_ES", ACmd, ref AVarSqlName, ref AStringValue, 16, out ArbValid);
                case DPhysicianInfoVars.HouseNr:
                    return mSqlGetSetEncryptedString(ref _mHouseNr, "HouseNr_ES", ACmd, ref AVarSqlName, ref AStringValue, 16, out ArbValid);
                case DPhysicianInfoVars.City:
                    return mSqlGetSetString(ref _mCity, "City", ACmd, ref AVarSqlName, ref AStringValue, 32, out ArbValid);
                case DPhysicianInfoVars.State:
                    return mSqlGetSetString(ref _mState, "State", ACmd, ref AVarSqlName, ref AStringValue, 32, out ArbValid);
                case DPhysicianInfoVars.CountryCode:
                    return mSqlGetSetString(ref _mCountryCode, "CountryCode", ACmd, ref AVarSqlName, ref AStringValue, 4, out ArbValid);
                case DPhysicianInfoVars.Phone1:
                    return mSqlGetSetEncryptedString(ref _mPhone1, "Phone1_ES", ACmd, ref AVarSqlName, ref AStringValue, 24, out ArbValid);
                case DPhysicianInfoVars.Phone2:
                    return mSqlGetSetEncryptedString(ref _mPhone2, "Phone2_ES", ACmd, ref AVarSqlName, ref AStringValue, 24, out ArbValid);
                case DPhysicianInfoVars.Cell:
                    return mSqlGetSetEncryptedString(ref _mCell, "Cell_ES", ACmd, ref AVarSqlName, ref AStringValue, 24, out ArbValid);
                case DPhysicianInfoVars.Email:
                    return mSqlGetSetEncryptedString(ref _mEmail, "Email_ES", ACmd, ref AVarSqlName, ref AStringValue, 128, out ArbValid);
                case DPhysicianInfoVars.Skype:
                    return mSqlGetSetEncryptedString(ref _mSkype, "Skype_ES", ACmd, ref AVarSqlName, ref AStringValue, 128, out ArbValid);
                case DPhysicianInfoVars.Instruction:
                    return mSqlGetSetEncryptedString(ref _mInstruction, "Instruction_ES", ACmd, ref AVarSqlName, ref AStringValue, 256, out ArbValid);
            }
            return base.mVarGetSet(ACmd, AVarIndex, ref AVarSqlName, ref AStringValue, out ArbValid);
        }
        public static void sFillPrintPhysicianName(bool AbAnonymize, CStudyInfo AStudy, bool AbRefPhysician,  out string ArText1, out string ArText2, bool AbAddP)
        {
            string text1 = "";
            string text2 = "";

            if (false == AbAnonymize && AStudy != null )
            {
                UInt32 index = AbRefPhysician ? AStudy._mRefPhysisian_IX : AStudy._mPhysisian_IX;

                if (index > 0)
                {
                    CPhysicianInfo physician = new CPhysicianInfo(AStudy.mGetSqlConnection());

                    if (physician != null)
                    {
                        physician.mbDoSqlSelectIndex(index, physician.mGetValidMask(true));// CSqlDataTableRow.sGetMask((UInt16)DClientInfoVars.Label));
                                                                                                        //                    text1 = physician.mGetFull  _mFullName;// _mLabel;
                                                                                                        //                  if (text1 == null || text1.Length == 0)
                                                                                                        //                {
                        text1 = physician._mLabel;

                        //              }
                        text2 = physician._mPhone1.mDecrypt();
                        if( text2 != null && text2.Length > 0 && AbAddP)
                        {
                            text2 = "P:  " + text2;
                        }
                    }
                }
            }
            ArText1 = text1;
            ArText2 = text2;
        }
        public static void sFillPrintPhysicianName(bool AbAnonymize, CStudyInfo AStudy, bool AbRefPhysician, Label ALabel1, Label ALabel2, bool AbAddP)
        {
            string text1, text2;

            sFillPrintPhysicianName(AbAnonymize, AStudy, AbRefPhysician, out text1, out text2, AbAddP);

            if (ALabel1 != null) ALabel1.Text = text1;
            if (ALabel2 != null) ALabel2.Text = text2;
        }

    }
}
