﻿using Event_Base;
using EventBoard.EntryForms;
using EventBoard.Event_Base;
using EventboardEntryForms;
using Program_Base;
using SironaDevice;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace EventBoard
{

    public partial class CMCTTrendDisplay : Form
    {
        public static bool _sbBlackWhite = false;
        public static UInt16 _sMctMinBlockMin = 1;
        public static UInt16 _sMctMaxBlockMin = 15;

        public CStudyInfo _mStudyInfo = null;
        public CPatientInfo _mPatientInfo = null;
        public CDeviceInfo _mDeviceInfo = null;
        private CRecAnalysis _mAnalysisInfo = null;
        public List<CSqlDataTableRow> _mAnalysisList = null;
        public List<CSqlDataTableRow> _mRecordList = null;  // cash all records collected for stat to use to cal start and end time

        public CTzReport _mReport = null;
        public Int16 _mTimeZoneMin = Int16.MinValue;

        public DateTime _mTimeFrameFrom;
        public DateTime _mTimeFrameTo;
        public bool _bTimeFrameUseAM;

        public bool _mbQueryActive = false;
        public string _mDevicePath;
        public DateTime _mTimeQuery;
        public UInt16 _mQueryLengthMin;
        public CTzDeviceSettings _mTzSettings = null;

        public DateTime _mShowDT;
        private CRecordMit _mShowRecord = null;
        private CRecAnalysis _mShowAnalysis = null;
        private float _mNearStripSec = 30;

        bool _sbLogRecords = true;

        UInt32 _mStudyIndex;
        string _mDeviceID;
        string _mMctFilesPath;
        DDeviceType _mMctSource;
        string _mMctSourceLabel;

        UInt32 mDvxStudyIX = 0;
        Int16 _mTimeZoneOffsetMin = 0;


        public bool bStatsLoaded = false;

        public string _mCalcValidateLog;
        public string _mUpdValidateLog;

        private double _mTimeLoadSec = -1;

        public bool _mbUseTrend = true;
        public bool _mbUseAnalysis = true;

        public bool _mbFullDebug = false;
        public bool _mbSelectFiles = false;
        public bool _mbLogError = false;

        public DateTime _mCreateFormStartDT;
        public DateTime _mLoadFormStartDT;
        public DateTime _mPlotFormStartDT;

        public double _mCreatedSec;
        public double _mLoadedSec;
        public double _mPlotedSec;

        public UInt16 _mHrBlockMinutes = 0;

        public UInt32 _mAskLimit = 10000;
        public bool _mbAskFirstTime = true;
        public bool _mbValidatePacSveb = true;
        public bool _mbValidatePvcVeb = true;
        public bool _mbValidatePause = true;
        public bool _mbValidateAFib = true;

        public bool _mbDrawOncePacSveb = true;
        public bool _mbDrawOncePvcVeb = true;
        public bool _mbDrawOncePause = true;
        public bool _mbDrawOnceAFib = true;

        public bool _mbValidateTimeLineTo = false;   // validate time line on calculated from from

        bool _mbUseExcluded = false;

        //string _mCreadedByInitials;

        public CMCTTrendDisplay(UInt32 AStudyIndex, DDeviceType ASource, bool AbFullDebug, bool AbSelectFiles)
        {
            bool bOk = false;
            bool bError = false;

            try
            {
                bStatsLoaded = false;
                _mbFullDebug = AbFullDebug;
                _mbSelectFiles = AbSelectFiles;
                _mbUseExcluded = FormEventBoard._sbMctUseExcluded;

                //                _mCreadedByInitials = "";

                _mCreateFormStartDT = DateTime.Now;
                _mLoadFormStartDT = _mPlotFormStartDT = _mCreateFormStartDT;

                _mCreatedSec = _mLoadedSec = _mPlotedSec = 0.0;

                CDvtmsData.sbInitData();
                InitializeComponent();
  
                _mStudyIndex = AStudyIndex;
                _mMctSource = ASource;
                CProgram.sbLogMemSize("opening MCT S" + _mStudyIndex.ToString());

                _mMctSourceLabel = "XX";

                _mStudyInfo = new CStudyInfo(CDvtmsData.sGetDBaseConnection());
                _mPatientInfo = new CPatientInfo(CDvtmsData.sGetDBaseConnection());
                _mDeviceInfo = new CDeviceInfo(CDvtmsData.sGetDBaseConnection());
                _mAnalysisInfo = new CRecAnalysis(CDvtmsData.sGetDBaseConnection());
                _mAnalysisList = new List<CSqlDataTableRow>();
                _mRecordList = new List<CSqlDataTableRow>();
                CRecordMit rec = new CRecordMit(CDvtmsData.sGetDBaseConnection());

                List<CSqlDataTableRow> loadAnalysisList = new List<CSqlDataTableRow>();


                _mReport = new CTzReport();

                _mTimeFrameFrom = DateTime.MaxValue;
                _mTimeFrameTo = DateTime.MinValue;
                string timeFormat = CProgram.sGetShowTimeFormat().ToUpper();

                _bTimeFrameUseAM = timeFormat.Contains("T");

                _mTimeQuery = DateTime.MinValue;
                _mQueryLengthMin = 4;

                radioButtonHour.Checked = false;
                radioButtonAll.Checked = true;
                checkBoxQCD.Enabled = FormEventBoard._sbQCdEnabled;
                labelUseExluded.Text = _mbUseExcluded ? "+Excl" : "";

                Text = CProgram.sMakeProgTitle(_mMctSourceLabel + " MCT trends and statistics: Init screen", true, true);

                if (AStudyIndex > 0 && _mStudyInfo != null && _mPatientInfo != null && _mDeviceInfo != null && _mReport != null
                    && _mAnalysisInfo != null && _mAnalysisList != null && _mRecordList != null && rec != null)
                {
                    if (_mStudyInfo.mbDoSqlSelectIndex(AStudyIndex, _mStudyInfo.mGetValidMask(true)))
                    {
                        if (_mPatientInfo.mbDoSqlSelectIndex(_mStudyInfo._mPatient_IX, _mPatientInfo.mGetValidMask(true)))
                        {
                            if (_mDeviceInfo.mbDoSqlSelectIndex(_mStudyInfo._mStudyRecorder_IX, _mDeviceInfo.mGetValidMask(true)))// just load all device info
                            {
                                _mDeviceID = _mDeviceInfo._mDeviceSerialNr;

                                switch (_mMctSource)
                                {
                                    case DDeviceType.Unknown:
                                        _mMctSource = CDvtmsData.sFindDeviceUnitType(out _mDevicePath, _mDeviceID);
                                        break;
                                    case DDeviceType.TZ:
                                        CDvtmsData.sbGetDeviceUnitDir(out _mDevicePath, DDeviceType.TZ, _mDeviceID, true);
                                        break;

                                    case DDeviceType.Sirona:
                                        CDvtmsData.sbGetDeviceUnitDir(out _mDevicePath, DDeviceType.Sirona, _mDeviceID, true);
                                        break;
                                    case DDeviceType.DV2:
                                        CDvtmsData.sbGetDeviceUnitDir(out _mDevicePath, DDeviceType.DV2, _mDeviceID, true);
                                        break;
                                    case DDeviceType.DVX:
                                        CDvtmsData.sbGetDeviceUnitDir(out _mDevicePath, DDeviceType.DVX, _mDeviceID, true);
                                        break;
                                }

                                if (_mMctSource != DDeviceType.Unknown)
                                {
                                    _mMctSourceLabel = CDvtmsData.sGetDeviceTypeName(_mMctSource);
                                }
                                UInt64 loadMask = _mAnalysisInfo.mMaskValid;
                                UInt64 whereMask = CSqlDataTableRow.sGetMask((UInt16)DRecAnalysisVars.Study_IX);

                                _mAnalysisInfo._mStudy_IX = _mStudyInfo.mIndex_KEY;

                                bOk = _mAnalysisInfo.mbDoSqlSelectList(out bError, out loadAnalysisList, loadMask, whereMask, true, "");   // just load all  belonging to study

                                loadMask = rec.mMaskValid;
                                whereMask = CSqlDataTableRow.sGetMask((UInt16)DRecordMitVars.Study_IX);
                                rec.mStudy_IX = _mStudyInfo.mIndex_KEY;

                                bOk = rec.mbDoSqlSelectList(out bError, out _mRecordList, loadMask, whereMask, true, "", DSqlSort.Ascending, (UInt16)DRecordMitVars.StartRecUTC);   // just load all belonging to study

                                _mAnalysisList = mMakeAnalysisList(loadAnalysisList);
                                bOk = _mAnalysisList != null;
                            }
                            else
                            {
                                CProgram.sPromptError(true, _mMctSourceLabel + " Mct statistics", "Failed to load device" + _mStudyInfo._mStudyRecorder_IX.ToString());
                            }
                        }
                        else
                        {
                            CProgram.sPromptError(true, _mMctSourceLabel + " Mct statistics", "Failed to load patient " + _mStudyInfo._mPatient_IX.ToString());
                        }
                    }
                    else
                    {
                        CProgram.sPromptError(true, _mMctSourceLabel + " Mct statistics", "Failed to load study " + AStudyIndex.ToString());
                    }
                }

                if (bOk && CProgram.sbEnterProgUserArea(true))
                {
                    if (FormEventBoard._sbForceAnonymize)
                    {
                        checkBoxAnonymize.Checked = true;
                        checkBoxAnonymize.Enabled = false;
                    }
                    checkBoxPulseOnOnly.Checked = Properties.Settings.Default.MctPulsOnOnly;
                    checkBoxTogleAfOnly.Checked = Properties.Settings.Default.MctToggleAfOnOnly;
                    checkBoxUseTrend.Checked = Properties.Settings.Default.MctUseTrend;
                    checkBoxUseAnalysis.Checked = Properties.Settings.Default.MctUseAnalysis;
                    checkBoxCountAnOnly.Checked = Properties.Settings.Default.MctCountAnalysis;
                    checkBoxPrintStat.Checked = Properties.Settings.Default.MctPrintStats;
                    checkBoxPrintDur.Checked = Properties.Settings.Default.MctPrintDuration;
                    _mNearStripSec = Properties.Settings.Default.MctNearSec;

                    Text = CProgram.sMakeProgTitle(_mMctSourceLabel + " MCT trends and statistics: Study " + _mStudyInfo.mIndex_KEY.ToString() + ", ...", true, true);
                    labelFoundRec.Text = "";
                    mSetStudy();
                    if (CLicKeyDev.sbDeviceIsProgrammer())
                    {
                        labelMouseInfo.Visible = true;
                        labelFoundRec.Visible = true;
                        checkBoxLogCalc.Visible = true;
                        _mbLogError |= false;
                    }

                    mCalValidated(_mbLogError, true, true);
                    // set max range

                    _mTimeFrameFrom = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Local);
                    _mTimeFrameTo = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Local);

                    mUpdateTimeFrame();
                    mInitQuery();
                    mUpdateQueryTime();
                    mUpdateStats();
                    checkBoxBlackWhite.Checked = _sbBlackWhite;

                }
                else
                {
                    Close();
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("MCTTrendForm() failed to init", ex);
                bStatsLoaded = false;
            }
        }
        private List<CSqlDataTableRow> mMakeAnalysisList(List<CSqlDataTableRow> ArloadAnalysisList)
        {
            List<CSqlDataTableRow> analysisList = null;

            try
            {
                analysisList = new List<CSqlDataTableRow>();

                int nLoaded = 0;
                int nListed = 0;
                bool bAddAll = false;

                if (analysisList != null && _mRecordList != null && ArloadAnalysisList != null)
                {
                    foreach (CRecAnalysis node in ArloadAnalysisList)
                    {
                        ++nLoaded;
                        CRecordMit rec = mFindRecordByIndex(node._mRecording_IX);
                        if (rec != null && (CRecordMit.sbIsStateAnalyzed(rec.mRecState, _mbUseExcluded) || bAddAll))
                        {
                            if (nListed == 0)
                            {
                                _mTimeZoneMin = (Int16)rec.mTimeZoneOffsetMin;  // set time zone to first analyzed records time zone
                            }
                            ++nListed;
                            node._mrRecordMit = rec;
                            analysisList.Add(node);

                        }
                        else
                        {
                            node._mrRecordMit = null;
                        }
                    }
                }
                if (nListed == 0 && _mRecordList != null && _mRecordList.Count > 0)
                {
                    CRecordMit rec = (CRecordMit)_mRecordList[0];
                    _mTimeZoneMin = (Int16)rec.mTimeZoneOffsetMin;  // set time zone to first record time zone  if no analysis exists
                }
                CProgram.sLogLine(nListed.ToString() + " Analysis with records in Analysis, " + nLoaded.ToString() + " loaded");
            }
            catch (Exception ex)
            {
                CProgram.sLogException("mMakeAnalysisList() failed", ex);
            }
            return analysisList;
        }

        private void mReducePointsHr(UInt16 ABlockMin)
        {
            if (ABlockMin > 0)
            {
                int nOldHrAvg = _mReport._mItemHrAvg._mList.Count;
                int nOldHrMin = _mReport._mItemHrMin._mList.Count;
                int nOldHrMax = _mReport._mItemHrMax._mList.Count;

                List<CMctReportPoint> oldList = _mReport._mItemHrAvg._mList;
                List<CMctReportPoint> newList = new List<CMctReportPoint>();
                DateTime blockStartDT = DateTime.MinValue;
                int nBlock = 0;
                int hrSum = 0;
                double relMin;
                double halfBlockMin = ABlockMin * 0.5;
                UInt32 scpNr = 0;
                // HR average
                try
                {
                    foreach (CMctReportPoint point in oldList)
                    {
                        if (nBlock == 0)
                        {
                            blockStartDT = point._mDateTime;
                            scpNr = point._mScpNumber;
                            hrSum = point._mValue;
                            nBlock = 1;
                        }
                        else
                        {
                            relMin = (point._mDateTime - blockStartDT).TotalMinutes;

                            if (relMin > -0.1 && relMin <= ABlockMin)
                            {
                                hrSum += point._mValue;   // within same time block
                                ++nBlock;

                            }
                            else
                            {
                                if (nBlock > 0)
                                {
                                    newList.Add(new CMctReportPoint(blockStartDT.AddMinutes(halfBlockMin), scpNr, (UInt16)((hrSum + nBlock / 2) / nBlock), false));
                                }
                                scpNr = point._mScpNumber;
                                blockStartDT = point._mDateTime;
                                hrSum = point._mValue;
                                nBlock = 1;
                            }
                        }
                    }
                    if (nBlock > 0)
                    {
                        newList.Add(new CMctReportPoint(blockStartDT.AddMinutes(halfBlockMin), scpNr, (UInt16)((hrSum + nBlock / 2) / nBlock), false));
                    }

                    _mReport._mItemHrAvg._mList = newList;
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("MCT reduce HrAvg(" + nOldHrAvg.ToString() + ") " + ABlockMin.ToString() + "min", ex);
                }

                // HR Min


                try
                {
                    oldList = _mReport._mItemHrMin._mList;
                    newList = new List<CMctReportPoint>();
                    blockStartDT = DateTime.MinValue;
                    nBlock = 0;
                    int hrMin = int.MaxValue;
                    scpNr = 0;

                    foreach (CMctReportPoint point in oldList)
                    {
                        if (nBlock == 0)
                        {
                            blockStartDT = point._mDateTime;
                            scpNr = point._mScpNumber;
                            hrMin = point._mValue;
                            nBlock = 1;
                        }
                        else
                        {
                            relMin = (point._mDateTime - blockStartDT).TotalMinutes;

                            if (relMin > -0.1 && relMin <= ABlockMin)
                            {
                                if (point._mValue < hrMin) hrMin = point._mValue;   // within same time block
                                ++nBlock;
                            }
                            else
                            {
                                if (nBlock > 0)
                                {
                                    newList.Add(new CMctReportPoint(blockStartDT.AddMinutes(halfBlockMin), scpNr, (UInt16)(hrMin), false));
                                }
                                scpNr = point._mScpNumber;
                                blockStartDT = point._mDateTime;
                                hrMin = point._mValue;
                                nBlock = 1;
                            }
                        }
                    }
                    if (nBlock > 0)
                    {
                        newList.Add(new CMctReportPoint(blockStartDT.AddMinutes(halfBlockMin), scpNr, (UInt16)(hrMin), false));
                    }

                    _mReport._mItemHrMin._mList = newList;


                }
                catch (Exception ex)
                {
                    CProgram.sLogException("MCT reduce HrMin(" + nOldHrMin.ToString() + ") " + ABlockMin.ToString() + "min", ex);
                }

                // HR Max
                try
                {

                    oldList = _mReport._mItemHrMax._mList;
                    newList = new List<CMctReportPoint>();
                    blockStartDT = DateTime.MinValue;
                    nBlock = 0;
                    int hrMax = int.MinValue;
                    scpNr = 0;

                    foreach (CMctReportPoint point in oldList)
                    {
                        if (nBlock == 0)
                        {
                            blockStartDT = point._mDateTime;
                            scpNr = point._mScpNumber;
                            hrMax = point._mValue;
                            nBlock = 1;
                        }
                        else
                        {
                            relMin = (point._mDateTime - blockStartDT).TotalMinutes;

                            if (relMin > -0.1 && relMin <= ABlockMin)
                            {
                                if (point._mValue > hrMax) hrMax = point._mValue;   // within same time block
                                ++nBlock;
                            }
                            else
                            {
                                if (nBlock > 0)
                                {
                                    newList.Add(new CMctReportPoint(blockStartDT.AddMinutes(halfBlockMin), scpNr, (UInt16)(hrMax), false));
                                }
                                scpNr = point._mScpNumber;
                                blockStartDT = point._mDateTime;
                                hrMax = point._mValue;
                                nBlock = 1;
                            }
                        }
                    }
                    if (nBlock > 0)
                    {
                        newList.Add(new CMctReportPoint(blockStartDT.AddMinutes(halfBlockMin), scpNr, (UInt16)(hrMax), false));
                    }

                    _mReport._mItemHrMax._mList = newList;
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("MCT reduce HrMax(" + nOldHrMax.ToString() + ") " + ABlockMin.ToString() + "min", ex);
                }
                int nNewHrAvg = _mReport._mItemHrAvg._mList.Count;
                int nNewHrMin = _mReport._mItemHrMin._mList.Count;
                int nNewHrMax = _mReport._mItemHrMax._mList.Count;

                CProgram.sLogLine("MCT reduce HrAvg(" + nNewHrAvg.ToString() + "/" + nOldHrAvg.ToString()
                    + ") HrMin(" + nNewHrMin.ToString() + "/" + nOldHrMin.ToString()
                    + ") HrMax(" + nNewHrMax.ToString() + "/" + nOldHrMax.ToString() + ") using block " + ABlockMin.ToString() + " minutes");

            }
        }
        private void mFillForm()
        {
            try
            {
                Text = CProgram.sMakeProgTitle(_mMctSourceLabel + " MCT trends and statistics: Study " + _mStudyInfo.mIndex_KEY.ToString() + ", load statistics...", true, true);
                labelFoundRec.Text = "";
                mSetStudy();
                if (mbLoadStat())
                {
                    UInt16 nrFiles = _mReport._mNrFiles;

                    _mHrBlockMinutes = (UInt16)(nrFiles <= 1 ? 0 : 1 + nrFiles / 100);

                    if(_mHrBlockMinutes > 60)
                    {
                        _mHrBlockMinutes = 60;
                    }
                    if (_mHrBlockMinutes < _sMctMinBlockMin) _mHrBlockMinutes = _sMctMinBlockMin;
                    if (_mHrBlockMinutes > _sMctMaxBlockMin) _mHrBlockMinutes = _sMctMaxBlockMin;
                    if( _mbFullDebug || _mbSelectFiles)
                    {
                        CProgram.sbReqUInt16("MCT HR point reduce (0=off)", "Block length", ref _mHrBlockMinutes, "min", 0, 60);
                    }
                    mReducePointsHr(_mHrBlockMinutes);

                    mCalValidated(_mbLogError, true, _mbValidateTimeLineTo);

                    _mLoadedSec = (DateTime.Now - _mLoadFormStartDT).TotalSeconds;
                    string s =  CProgram.sMakeProgTitle(_mMctSourceLabel + " MCT trends and statistics: Study " + _mStudyInfo.mIndex_KEY.ToString()
      + ", " + _mReport._mNrFiles.ToString() + " files (" + (_mCreatedSec + _mLoadedSec).ToString("0.0") + "sec)", true, true);
                    if(_mHrBlockMinutes > 0 )
                    {
                        s += " HR block " + _mHrBlockMinutes.ToString() + " min.";
                    }
                    if(_mReport._mNrMissingFiles > 0 )
                    {
                        s += "! missing files >= " + _mReport._mNrMissingFiles.ToString();
                    }
                    Text = s;
                    _mPlotFormStartDT = DateTime.Now;
                    // set max range
                    _mTimeFrameFrom = DateTime.SpecifyKind(_mReport._mStartTimeDT, DateTimeKind.Local);
                    _mTimeFrameTo = DateTime.SpecifyKind(_mReport._mEndTimeDT, DateTimeKind.Local);

                    mUpdateTimeFrame();
                    mInitQuery();
                    mUpdateQueryTime();
                    mUpdateStats();
                    bStatsLoaded = true;
//                    _mPlotFormStartDT = DateTime.Now;
                    _mPlotedSec = (DateTime.Now - _mPlotFormStartDT).TotalSeconds;
                    labelPlotSec.Text = _mPlotedSec.ToString("0.00") + "sec";
                    CProgram.sLogLine(s);
                    CProgram.sLogLine( "MCT " + " create= " + _mCreatedSec.ToString("0.00") + "sec, load= " + _mLoadedSec.ToString("0.00") 
                        +" sec plot = " + _mPlotedSec.ToString("0.00") + " sec");
//                    if (_mMctSource != DDeviceType.DVX)
                    {
                        CProgram.sbLogMemSize("loaded MCT S" + _mStudyIndex.ToString());
                    }
                }
                else
                {
                    CProgram.sLogWarning("MCT Trend display S" + _mStudyIndex.ToString() + " Failed load stat");
                    //                    CProgram.sAskWarning("MCT Trend display S" + _mStudyIndex.ToString(), "Failed load stat");
                    Close();
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("MCTTrendForm() failed to fill form", ex);
                bStatsLoaded = false;
            }
        }
        private void mUpdateTimeFrame()
        {
            try
            {
                bool bFromVisible = false;
                bool bToVisible = false;
//                int hour, min;

                if (_mStudyInfo != null)
                {
                    DateTime dt = new DateTime(_mStudyInfo._mStudyStartDate.Year, _mStudyInfo._mStudyStartDate.Month, _mStudyInfo._mStudyStartDate.Day);

                    if (_mTimeFrameFrom < dt) _mTimeFrameFrom = dt;
                    if (_mTimeFrameTo < dt) _mTimeFrameTo = dt;
                }

                if (radioButtonHour.Checked) { bFromVisible = true; }
                else if (radioButton6hours.Checked) { bFromVisible = true; }
                else if (radioButton12hours.Checked) { bFromVisible = true; }
                else if (radioButtonDay.Checked) { bFromVisible = true; }
                else if (radioButtonWeek.Checked) { bFromVisible = true; }
                else if (radioButtonMonth.Checked) { bFromVisible = true; }
                else if (radioButtonPeriod.Checked) { bFromVisible = true; bToVisible = true; }

                mSetFrameTime("from", bFromVisible, _mTimeFrameFrom, dateTimePickerFrom, textBoxFromHour, textBoxFromMin, comboBoxFromAM);
                mSetFrameTime("to", bToVisible, _mTimeFrameTo, dateTimePickerTo, textBoxToHour, textBoxToMin, comboBoxToAM);

/*
                comboBoxFromAM.Visible = _bTimeFrameUseAM;
                comboBoxToAM.Visible = _bTimeFrameUseAM;

                dateTimePickerFrom.Enabled = bFromVisible;
                textBoxFromHour.Enabled = bFromVisible;
                textBoxFromMin.Enabled = bFromVisible;
                comboBoxFromAM.Enabled = bFromVisible;

                dateTimePickerTo.Enabled = bToVisible;
                textBoxToHour.Enabled = bToVisible;
                textBoxToMin.Enabled = bToVisible;
                comboBoxToAM.Enabled = bToVisible;

                dateTimePickerFrom.Value = _mTimeFrameFrom;
                hour = _mTimeFrameFrom.Hour;
                if (_bTimeFrameUseAM)
                {
                    if (hour >= 12)
                    {
                        comboBoxFromAM.SelectedIndex = 1; // PM
                        if (hour > 12) hour -= 12;
                    }
                    else
                    {
                        comboBoxFromAM.SelectedIndex = 0; // AM
                        if (hour == 0) hour = 12;
                    }
                }
                textBoxFromHour.Text = hour.ToString();
                textBoxFromMin.Text = _mTimeFrameFrom.Minute.ToString();

                dateTimePickerTo.Value = _mTimeFrameTo;
                hour = _mTimeFrameTo.Hour;
                if (_bTimeFrameUseAM)
                {
                    if (hour >= 12)
                    {
                        comboBoxToAM.SelectedIndex = 1; // PM
                        if (hour > 12) hour -= 12;
                    }
                    else
                    {
                        comboBoxToAM.SelectedIndex = 0; // AM
                        if (hour == 0) hour = 12;
                    }
                }
                textBoxToHour.Text = hour.ToString();
                textBoxToMin.Text = _mTimeFrameTo.Minute.ToString();
*/            }
            catch (Exception ex)
            {
                CProgram.sLogException("MCTTrendForm() failed to update Time Frame", ex);
            }
        }

        private void mSetFrameTime(string ATimeName, bool AbEnabled, DateTime ADateTime, DateTimePicker ADateTimePicker, TextBox ATextBoxHour, TextBox ATextBoxMin, ComboBox AComboBoxAM)
        {
            try
            {
                int hour,min;

                AComboBoxAM.Visible = _bTimeFrameUseAM;

                ADateTimePicker.Enabled = AbEnabled;
                ATextBoxHour.Enabled = AbEnabled;
                ATextBoxMin.Enabled = AbEnabled;
                AComboBoxAM.Enabled = AbEnabled;

                ADateTimePicker.Value = ADateTime;
                hour = ADateTime.Hour;
                if (_bTimeFrameUseAM)
                {
                    if (hour >= 12)
                    {
                        AComboBoxAM.SelectedIndex = 1; // PM
                        if (hour > 12) hour -= 12;
                    }
                    else
                    {
                        AComboBoxAM.SelectedIndex = 0; // AM
                        if (hour == 0) hour = 12;
                    }
                }
                ATextBoxHour.Text = hour.ToString();
                ATextBoxMin.Text = ADateTime.Minute.ToString();
            }
            catch (Exception ex)
            {
                CProgram.sLogException("MCTTrendForm() failed set " + ATimeName + " frame time", ex);

            }
        }


        private DateTime mReadFrameTime(string ATimeName, DateTimePicker ADateTimePicker, TextBox ATextBoxHour, TextBox ATextBoxMin, ComboBox AComboBoxAM)
        {
            DateTime dt = ADateTimePicker.Value;

            try
            {
                int hour, min;

                if (int.TryParse(ATextBoxHour.Text, out hour))
                {
                    if (_bTimeFrameUseAM)
                    {
                        if (AComboBoxAM.SelectedIndex == 1)
                        {
                            if (hour < 12) hour += 12;
                        }
                        else
                        {
                            if (hour == 12) hour = 0;
                        }
                    }
                }
                else
                {
                    hour = 0;
                }

                if (false == int.TryParse(ATextBoxMin.Text, out min))
                {
                    min = 0;
                }

                if (hour < 0)
                {
                    hour = 0;
                    min = 0;
                }
                else if (hour >= 24)
                {
                    hour = 23;
                    min = 59;
                }
                dt = new DateTime(dt.Year, dt.Month, dt.Day, hour, min, 0);

            }
            catch (Exception ex)
            {
                CProgram.sLogException("MCTTrendForm() failed read "+ ATimeName + " frame time", ex);

            }
            return dt;
        }


        private DateTime mReadFromTime()
        {
            _mTimeFrameFrom = mReadFrameTime("from", dateTimePickerFrom, textBoxFromHour, textBoxFromMin, comboBoxFromAM);
            return _mTimeFrameFrom;

            /*            DateTime dt = dateTimePickerFrom.Value;

                        try
                        {

                            int hour, min;

                            if (int.TryParse(textBoxFromHour.Text, out hour))
                            {
                                if (_bTimeFrameUseAM)
                                {
                                    if (comboBoxFromAM.SelectedIndex == 1)
                                    {
                                        if( hour < 12 ) hour += 12;
                                    }
                                    else
                                    {
                                        if (hour == 12) hour = 0;
                                    }
                                }
                            }
                            else
                            {
                                hour = 0;
                            }

                            if (false == int.TryParse(textBoxFromMin.Text, out min))
                            {
                                min = 0;
                            }

                            if (hour < 0)
                            {
                                hour = 0;
                                min = 0;
                            }
                            else if (hour >= 24)
                            {
                                hour = 23;
                                min = 59;
                            }
                            dt = new DateTime(dt.Year, dt.Month, dt.Day, hour, min, 0);
                            _mTimeFrameFrom = dt;
                        }
                        catch (Exception ex)
                        {
                            CProgram.sLogException("MCTTrendForm() failed read from time", ex);

                        }
                        return dt;
                        */
        }
        private DateTime mReadToTime()
        {
            _mTimeFrameTo = mReadFrameTime("to", dateTimePickerTo, textBoxToHour, textBoxToMin, comboBoxToAM);
            return _mTimeFrameFrom;
            /*
            DateTime dt = dateTimePickerTo.Value;

            try
            {
                int hour, min;

                if (int.TryParse(textBoxToHour.Text, out hour))
                {
                    if (_bTimeFrameUseAM)
                    {
                        if (comboBoxToAM.SelectedIndex == 1)
                        {
                            if (hour < 12) hour += 12;
                        }
                        else
                        {
                            if (hour == 12) hour = 0;
                        }
                    }
                }
                else
                {
                    hour = 0;
                }

                if (false == int.TryParse(textBoxToMin.Text, out min))
                {
                    min = 0;
                }

                if (hour < 0)
                {
                    hour = 0;
                    min = 0;
                }
                else if (hour >= 24)
                {
                    hour = 23;
                    min = 59;
                }
                dt = new DateTime(dt.Year, dt.Month, dt.Day, hour, min, 0);
                _mTimeFrameTo = dt;
            }
            catch (Exception ex)
            {
                CProgram.sLogException("MCTTrendForm() failed read from time", ex);
            }
            return dt;
            */
        }
        private void mGetTimeFrame()
        {
            try
            {
                //                bool bFromRead = false;
                //                bool bToRead = false;
                //                int hour, min;

                if (radioButtonHour.Checked) { _mTimeFrameTo = mReadFromTime().AddHours(1); }
                else if (radioButton6hours.Checked) { _mTimeFrameTo = mReadFromTime().AddHours(6); }
                else if (radioButton12hours.Checked) { _mTimeFrameTo = mReadFromTime().AddHours(12); }
                else if (radioButtonDay.Checked) { _mTimeFrameTo = mReadFromTime().AddDays(1); }
                else if (radioButtonWeek.Checked) { _mTimeFrameTo = mReadFromTime().AddDays(7); }
                else if (radioButtonMonth.Checked) { _mTimeFrameTo = mReadFromTime().AddMonths(1); }
                else if (radioButtonPeriod.Checked) { mReadFromTime(); mReadToTime(); }
                else
                {
                    // all
                    _mTimeFrameFrom = DateTime.SpecifyKind(_mReport._mStartTimeDT, DateTimeKind.Local);
                    _mTimeFrameTo = DateTime.SpecifyKind(_mReport._mEndTimeDT, DateTimeKind.Local);
                }
                if (_mTimeFrameFrom > _mTimeFrameTo)
                {
                    DateTime dt = _mTimeFrameTo;
                    _mTimeFrameTo = _mTimeFrameFrom;
                    _mTimeFrameFrom = dt;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("MCTTrendForm() failed get time frame", ex);
            }
        }

        private void mUpdateQueryTime()
        {
            if (_mbQueryActive)
            {
                int hour, min;

                comboBoxStart.Visible = _bTimeFrameUseAM;

                if (_mTimeQuery == DateTime.MinValue)
                {
                    _mTimeQuery = _mTimeFrameFrom;
                }
                dateTimeStart.Value = _mTimeQuery;
                hour = _mTimeQuery.Hour;
                if (_bTimeFrameUseAM)
                {
                    if (hour >= 12)
                    {
                        comboBoxStart.SelectedIndex = 1; // PM
                        if (hour > 12) hour -= 12;
                    }
                    else
                    {
                        comboBoxStart.SelectedIndex = 0; // AM
                        if (hour == 0) hour = 12;
                    }
                }
                textBoxStartHour.Text = hour.ToString();
                textBoxStartMinute.Text = _mTimeQuery.Minute.ToString();
                textBoxLength.Text = _mQueryLengthMin.ToString();
            }
        }

        private UInt16 mReadQuiryStartTime()
        {
            DateTime dt = dateTimeStart.Value;
            int hour, min;
            UInt16 length;

            if (int.TryParse(textBoxStartHour.Text, out hour))
            {
                if (_bTimeFrameUseAM)
                {
                    if (comboBoxStart.SelectedIndex == 1)
                    {
                        if (hour > 12) hour += 12;
                    }
                    else
                    {
                        if (hour == 12) hour = 0;
                    }
                }
            }
            else
            {
                hour = 0;
            }

            if (false == int.TryParse(textBoxStartMinute.Text, out min))
            {
                min = 0;
            }

            if (hour < 0)
            {
                hour = 0;
                min = 0;
            }
            else if (hour >= 24)
            {
                hour = 23;
                min = 59;
            }
            if (false == UInt16.TryParse(textBoxLength.Text, out length) || length < 0)
            {
                length = 0;
            }
            _mTimeQuery = new DateTime(dt.Year, dt.Month, dt.Day, hour, min, 0);
            _mQueryLengthMin = length;
            return length;
        }

        private void mReadUpdateTimeFrame()
        {
            try
            {
                mGetTimeFrame();

                mUpdateTimeFrame();
                mUpdateStats();
            }
            catch (Exception ex)
            {
                CProgram.sLogException("MCTTrendForm() failed update time frame", ex);
            }

        }

        private void mShiftTimeFrame(int AShiftHours)
        {
            try
            {
                mGetTimeFrame();
                _mTimeFrameFrom = _mTimeFrameFrom.AddHours(AShiftHours);
                _mTimeFrameTo = _mTimeFrameTo.AddHours(AShiftHours);
                mUpdateTimeFrame();
                mUpdateStats();
            }
            catch (Exception ex)
            {
                CProgram.sLogException("MCTTrendForm() failed shift time frame", ex);
            }

        }

        private void mInitPatient()
        {
            bool bHidePatient = checkBoxAnonymize.Checked;
            int age = _mPatientInfo.mGetAgeYears();

            labelPatIDHeader.Text = _mPatientInfo.mGetPatientSocIDText() + ":";
            labelPatID.Text = bHidePatient ? "--An--" : _mPatientInfo.mGetPatientSocIDValue(false)/* _mPatientID.mDecrypt()*/;
            labelPatName.Text = bHidePatient ? "--Anonymized--" : _mPatientInfo.mGetFullName(); 
            labelDoB.Text = age >= 0 && bHidePatient ? "" : CProgram.sDateToString(_mPatientInfo._mPatientDateOfBirth.mDecryptToDate());

            string s = " " + CPatientInfo.sGetGenderString((DGender)_mPatientInfo._mPatientGender_IX) + " " + (age >= 0 ? age.ToString() + "y" : "") ;
            labelAgeGender.Text = s;
        }

        private void mSetStudy()
        {
            labelUser.Text = CProgram.sGetProgUserInitials();
            //labelUser2.Text = CProgram.sGetProgUserInitials();
            if (_mStudyIndex > 0 && _mStudyInfo != null && _mPatientInfo != null && _mDeviceInfo != null)
            {
                labelStudy.Text = _mStudyInfo.mIndex_KEY.ToString();
                labelRecorderID.Text = _mDeviceID;

                string s = _mStudyInfo.mGetLabelNoteMark();

                labelStudyNote.Text = s;

                if (s.Length > 0)
                {
                    s = _mStudyInfo.mGetRemarkAndNote();

                    new ToolTip().SetToolTip(labelStudyNote, s);
                }

                mInitPatient();

                labelStartDate.Text = CProgram.sDateToString(_mStudyInfo._mStudyStartDate);
                TimeSpan ts = DateTime.UtcNow - _mStudyInfo._mStudyStartDate;
                /*
                                double d = ts.TotalDays;
                                int days = (int)d;
                                int hours = (int)((d - days) * 24);
                                labelDaysHours.Text = days.ToString() + " days + " + hours.ToString() + " hours";
                */
                labelDaysHours.Text = CProgram.sPrintTimeSpan_dhMin(ts.TotalMinutes, ' ');
                labelNow.Text = CProgram.sDateTimeToString(DateTime.Now);

                //labelDaysHours.Text = "";
                Int32 nRec = _mRecordList == null ? 0 : _mRecordList.Count;
                Int32 nAnalyzed = _mAnalysisList == null ? 0 : _mAnalysisList.Count;

                labelRecAnalysis.Text = nRec.ToString() + " Recorded events, " + nAnalyzed.ToString() + " analyzed.";

                CSqlDBaseConnection db = CDvtmsData.sGetDBaseConnection();

                UInt32 dbLimit = db == null ? 1 : db.mGetSqlRowLimit();
                if (nRec >= dbLimit)
                {
                    labelRecAnalysis.BackColor = Color.Orange;
                }
            }
        }
         string[] mTzSortByFileDateTime(string[] AFileList)
        {
            string[] sortedList = null;

            try
            {
                if (AFileList != null && AFileList.Length > 0)
                {
                    sortedList = AFileList;
                    Array.Sort(sortedList);

                    string fileName = Path.GetFileName(sortedList[sortedList.Length - 1]);

                    // I200010_20180408_19_20180408200905.tzr
                    UInt16 nrChars = CDvtmsData.sFindNthChar(fileName, '_', 3);

                    if (nrChars != 0 && 0 == CDvtmsData.sSortWipeDoubleFiles(ref sortedList, ++nrChars))
                    {
                        sortedList = null;  // empty
                    }
                  }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed to sort TZ files", ex);
            }
            return sortedList;
        }


        string[] mSironaSortByFileDateTime(string[] AFileList)
        {
            string[] sortedList = null;

            try
            {
                if (AFileList != null)
                {
                    string fileName, sortName, storeName;
                    List<string> list = new List<string>();
                    int len;
                    int n = AFileList.Length;
                    // convert fileName to sortable string
                    // in <dir>\snr_patid_yyyymmdd.atr
                    // sort yyyymmdd_snr

                    foreach (string fullName in AFileList)
                    {
                        fileName = Path.GetFileNameWithoutExtension(fullName);

                        len = fileName.Length;
                        if (fileName.Contains('_') && len > 8)
                        {
                            int l = len - 8;
                            sortName = fileName.Substring(l) + fileName.Substring(0, l);
                        }
                        else
                        {
                            sortName = fileName;
                        }
                        // store as
                        storeName = sortName + "$" + fullName;
                        list.Add(storeName);

                    }
                    list.Sort();

                    sortedList = new string[list.Count];

                    int i = 0, pos;
                    foreach (string s in list)
                    {
                        pos = s.IndexOf('$');
                        if (pos < 0)
                        {
                            storeName = s;
                        }
                        else
                        {
                            storeName = s.Substring(pos + 1);
                        }
                        sortedList[i] = storeName;
                        ++i;
                    }
                    if (i != n || n != sortedList.Length)
                    {
                        CProgram.sLogError("Failed to sort Sirona files, size array not equal");
                    }
                    //                    sortedList = AFileList;
                }

            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed to sort Sirona files", ex);
            }
            return sortedList;
        }
        public bool mbReadSelectedFiles(string ATitle, string[] AFileList, float AUseTimeSec = 0.0F, bool AbChangeDebug = false)
        {
            bool bOk = false;

            try
            {
                string fileExt = "." + CDvtmsData.sGetDeviceTypeMctExt(_mMctSource);

                int nFiles = AFileList == null ? 0 : AFileList.Length;
                string[] files = AFileList;

                bool bLogLine = _mbFullDebug;
                bool bLogLineDT = _mbFullDebug;
                bool bLogInfo = _mbFullDebug;
                bool bLogLineDetail = _mbFullDebug;
                bool bIsProgrammer = CLicKeyDev.sbDeviceIsProgrammer();

                if (AbChangeDebug && false == _mbFullDebug)
                {
                    bool bAll = false;
                    if (bIsProgrammer)
                    {
                        bAll |= false;
                    }
                    switch (_mMctSource)
                    {
                        case DDeviceType.TZ:
                            bLogLine = nFiles == 1 && bIsProgrammer || bAll;
                            bLogLineDT = bAll;
                            bLogInfo = bAll;
                            bLogLineDetail = bAll;
                            break;
                        case DDeviceType.Sirona:
                            bLogLine = nFiles == 1 && bIsProgrammer || bAll;
                            bLogLineDT = bAll;
                            bLogInfo = bAll;
                            bLogLineDetail = bAll;
                            break;
                        case DDeviceType.DV2:
                            break;
                        case DDeviceType.DVX:
                            bLogLine = nFiles == 1 && bIsProgrammer || bAll;
                            bLogLineDT = bAll;
                            bLogInfo = bAll;
                            bLogLineDetail = bAll;
                            break;
                    }
                }
                _mReport.mSetDebug(bLogLine, bLogLineDT, bLogInfo, bLogLineDetail);
                if (nFiles > 0)
                {
                    switch (_mMctSource)
                    {
                        case DDeviceType.TZ:
                            files = mTzSortByFileDateTime(files);
                            break;
                        case DDeviceType.Sirona:
                            files = mSironaSortByFileDateTime(files);

                           break;
                        case DDeviceType.DV2:
                            Array.Sort(files, StringComparer.InvariantCulture);
                            break;
                        case DDeviceType.DVX:
                            Array.Sort(files, StringComparer.InvariantCulture);  // <snr>_yyyyMMddHHmmss.tnd can be sorted by name

                            break;
                    }
                    nFiles = files == null ? 0 : files.Length;
                 }

                if (nFiles > 0 && _mReport.mbStart(_mMctSource))
                {
                    DateTime nowDT = DateTime.Now;
                    bool bIgnore = true;
                    int nOk = 0;

                    CProgram.sLogLine("MctReport read " + nFiles.ToString() + " files in list: start(" + CDvtmsData.sGetDeviceTypeName(_mMctSource) + ") " + AFileList[0]);

                    bOk = true;
                    for (int i = 0; i < nFiles;)
                    {

                        string fileName = files[i++];
                        if (fileName != null && fileName.Length > 1 && fileName.EndsWith(fileExt))
                        {
                            CProgram.sLogLine("MctReport read file: " + Path.GetFileName(fileName));
                            Text = CProgram.sMakeProgTitle(ATitle + ", loading " + i.ToString() + " /  " + nFiles.ToString(), true, true);
                            Application.DoEvents();

                            if (AUseTimeSec > 0.01F)
                            {
                                double dt = (i-1) / (float)nFiles * AUseTimeSec - (DateTime.Now - nowDT).TotalSeconds;
                                if (dt > 0.01)
                                {
                                    Thread.Sleep((int)(dt * 1000));
                                    Application.DoEvents();
                                }
                            }
                            if(bIsProgrammer && i < 2)
                            {
                                int k = i;
                            }
                            bool b = _mReport.mbReadOneFile(fileName, _mMctSource, _mTimeZoneOffsetMin);

                            if (b)
                            {
                                ++nOk;
                            }
                            else
                            {
                                if (bIgnore)
                                {
                                    CProgram.sLogLine("Read " + CDvtmsData.sGetDeviceTypeName(_mMctSource) + " MCT file " + i.ToString() + " / " + nFiles.ToString() + " Failed and ignored " + Path.GetFileName(fileName));
                                }
                                else if (CProgram.sbPromptAbortRetryIgnore(true, out bIgnore, "Read " + CDvtmsData.sGetDeviceTypeName(_mMctSource) + " MCT " + i.ToString() + " / " + nFiles.ToString(),
                                    "Failed read file " + Path.GetFileName(fileName)))
                                {
                                    CProgram.sLogLine("Read " + CDvtmsData.sGetDeviceTypeName(_mMctSource) + " MCT file " + i.ToString() + " / " + nFiles.ToString() + " Failed and continue " + Path.GetFileName(fileName));
                                }
                                else
                                {
                                    CProgram.sLogLine("Read " + CDvtmsData.sGetDeviceTypeName(_mMctSource) + " MCT file " + i.ToString() + " / " + nFiles.ToString() + " Failed and abort loop " + Path.GetFileName(fileName));
                                    bOk = false;
                                    break;
                                }
                            }
                            if (bOk)
                            {
                                DateTime startDT = _mReport._mStartTimeDT;
                                DateTime endDT = _mReport._mEndTimeDT;
                                if ((endDT - startDT).TotalDays > 100)
                                {
                                    bOk = true;  // show any way;
                                }
                            }
                        }
                    }
                    string errStr = ", failed";
                    if (bOk)
                    {
                        errStr = "";

                        if (nOk != nFiles)
                        {
                            errStr = ", " + (nFiles - nOk).ToString() + "error files";
                        }
                        CProgram.sLogLine("MctReport stop(" + CDvtmsData.sGetDeviceTypeName(_mMctSource) + ") processing... " + (DateTime.Now - nowDT).TotalSeconds.ToString("0.0") + errStr);

                        Text = CProgram.sMakeProgTitle(ATitle + ", processing " + nFiles.ToString() + errStr, true, true);
                        Application.DoEvents();

                        _mReport.mbStop(_mMctSource);

                        CProgram.sLogLine("MctReport stop(" + CDvtmsData.sGetDeviceTypeName(_mMctSource) + ") " + _mReport._mNrFiles.ToString() + "  files processed in " + (DateTime.Now - nowDT).TotalSeconds.ToString("0.0") + " sec" + errStr);

                        Text = CProgram.sMakeProgTitle(ATitle + ", " + nFiles.ToString() + " files processed" + errStr, true, true);
                        Application.DoEvents();

                        bOk = _mReport._mNrFiles > 0;
                    }
                    _mReport.mLogInfo();
                    _mTimeLoadSec = (DateTime.Now - nowDT).TotalSeconds;
                    CProgram.sLogLine("MctReport stop(" + CDvtmsData.sGetDeviceTypeName(_mMctSource) + ") " + _mReport._mNrFiles.ToString() + "  files loaded and processed in " + _mTimeLoadSec.ToString("0.0") + " sec" + errStr);

                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed import  " + _mMctSourceLabel + " files from path " + AFileList, ex);
                bOk = false;
            }
            return bOk;
        }

        private bool mbManualSelectFiles(out string[] ArFiles)
        {
            string[] files = null;
            bool bOk = false;

            try
            {
                if (_mMctFilesPath == null || _mMctFilesPath.Length < 3)
                {
                    string dataDir = CDvtmsData.sGetDataDir();
                    _mMctFilesPath = Path.Combine(dataDir, "HttpTZ", _mDeviceID, "uploads");
                }
                if (false == Directory.Exists(_mMctFilesPath))
                {
                    CProgram.sLogLine("MCT files path missing:  " + _mMctFilesPath);
                    CProgram.sPromptWarning(true, _mStudyIndex.ToString() + " Load " + _mMctSourceLabel + " MCT trend files", "Path to trend files missing!");
                }
                else
                {
                    //openFileDialog.Reset();
                    string ext = CDvtmsData.sGetDeviceTypeMctExt(_mMctSource);
                    ext = ext == null || ext.Length == 0 ? "*.*" : "*." + ext;

                    openFileDialog.Reset();
                    openFileDialog.FileName = String.Empty;
                    openFileDialog.InitialDirectory = _mMctFilesPath;
                    openFileDialog.Multiselect = true;

                    openFileDialog.Filter = _mMctSourceLabel + " trend files|" + ext + "|All Files|*.*";

                    openFileDialog.Title = _mStudyIndex.ToString() + " Select one or more of the " + _mMctSourceLabel + " trend files(recorder " + _mDeviceID + ") (use <ctrl>A to select all)";
                    //openFileDialog.Title = "Se"

                    if (_mReport != null && DialogResult.OK == openFileDialog.ShowDialog())
                    {
                        string fileName = openFileDialog.FileName;

                        if (openFileDialog.FileNames == null || openFileDialog.FileNames.Length <= 0)
                        {
                            Close();
                        }
                        else
                        {
                            bOk = true;
                            files = openFileDialog.FileNames;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed select  " + _mMctSourceLabel + " files from path " + _mMctFilesPath, ex);
                bOk = false;
            }
            ArFiles = files;
            return bOk;
        }

        private bool mbGetStudyRecordingFolder( out string ArStudyRecFolder, UInt32 AStudyIndex )
        {
            bool bOk = false;
            string studyRecFolder = "";

            switch (_mMctSource)
            {
                case DDeviceType.TZ:
                    bOk = CDvtmsData.sbGetStudyRecorderDir(out studyRecFolder, AStudyIndex, true);
                    break;
                case DDeviceType.Sirona:
                    bOk = CDvtmsData.sbGetStudyRecorderDir(out studyRecFolder, AStudyIndex, true);

                    break;
                case DDeviceType.DV2:
                    bOk = CDvtmsData.sbGetStudyRecorderDir(out studyRecFolder, AStudyIndex, true);
                    break;
                case DDeviceType.DVX:
                    {
                        string dvxStudyDir = "";
                        mDvxStudyIX = 0;

                        foreach (CRecordMit rec in _mRecordList)
                        {
                            string rem = rec.mRecRemark.mDecrypt();

                            if (CDvxEvtRec.sbGetStudyDeviceFromRemark(out dvxStudyDir, out mDvxStudyIX, rem, rec.mDeviceID))
                            {
                                if (mDvxStudyIX > 0 && dvxStudyDir != null && dvxStudyDir.Length > 3)
                                {
                                    studyRecFolder = dvxStudyDir;
                                    Int16 _mTimeZoneOffsetMin = (Int16)rec.mTimeZoneOffsetMin;  // use in DVX trend file read to move time to Device time
                                     bOk = true;
                                    break;
                                }
                            }
                        }
                        if(mDvxStudyIX == 0)
                        {
                            CProgram.sLogLine( "No DVX study from " + _mRecordList.Count + " records");
                        }
                    }
                    break;
            }
            ArStudyRecFolder = studyRecFolder;
            return bOk && studyRecFolder != null && studyRecFolder.Length > 3;
        }

        private bool mbLoadStat()
        {
            bool bOk = false;

            try
            {
                _mMctFilesPath = null;

                if (false == mbGetStudyRecordingFolder(out _mMctFilesPath, _mStudyIndex))
                {
                    CProgram.sPromptWarning(true, "Load " + _mMctSourceLabel + " MCT trend file for study " + _mStudyIndex.ToString(), "Missing recorder folder and files!");
                }
                else
                {
                    string title = _mMctSourceLabel + " MCT trends and statistics: Study " + _mStudyIndex.ToString();
                    string fileExt = "." + CDvtmsData.sGetDeviceTypeMctExt(_mMctSource);
                    string[] files = null;


                    if (false == CLicKeyDev.sbDeviceIsProgrammer() && false == _mbSelectFiles)
                    {
                        //string dirName = Path.GetDirectoryName(AFilePath);
                        files = Directory.GetFiles(_mMctFilesPath, "*" + fileExt);
                        bOk = true;
                        _mLoadedSec = (DateTime.Now - _mCreateFormStartDT).TotalSeconds;
                    }
                    else
                    {
                        _mCreatedSec = (DateTime.Now - _mCreateFormStartDT).TotalSeconds;
                        bOk = mbManualSelectFiles(out files);
                    }
                    _mLoadFormStartDT = DateTime.Now;
                    int nFiles = files == null ? 0 : files.Length;

                    if( bOk )
                    {
                        if( nFiles == 0 )
                        {
                            CProgram.sPromptWarning(true, "Load  " + _mMctSourceLabel + " MCT file", "No files to load! (Check study for *" + fileExt + ")");
                            bOk = false;
                        }
                    }
                    if (bOk)
                    {
                        bOk = mbReadSelectedFiles(title, files, 3.0F, true);

                        if (false == bOk)
                        {
                            double hours = _mReport._mEndTimeDT > _mReport._mStartTimeDT ? (_mReport._mEndTimeDT - _mReport._mStartTimeDT).TotalHours : 0.0;
                            string info = "(" + _mReport._mNrFiles.ToString() + "files, "+ hours.ToString("0.0") + " hours)";
                            CProgram.sLogLine("Failed to load  " + _mMctSourceLabel + " MCT files from " + _mMctFilesPath);
                            CProgram.sPromptWarning(true, "Load  " + _mMctSourceLabel + " MCT file", "Failed to load one or more files" + info);
                            bOk = _mReport._mNrFiles > 0;
                        }
                        if( bOk )
                        {
                            // filter HR points
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed import  " + _mMctSourceLabel + " files from path " + _mMctFilesPath, ex);
                bOk = false;
            }
            return bOk;
        }

        private string mTimeStr(double AT)
        {
            int i = (int)AT;
            int sec = i % 60;
            i /= 60;
            int min = i % 60;
            i /= 60;
            int hour = i;
            //int days = i / 24;

            string s = hour.ToString("D2") + ":" + min.ToString("D02") + ":" + sec.ToString("D02");
            return s;
        }

        private string mTimeDayStr(double AT)
        {
            int i = (int)AT;
            int sec = i % 60;
            i /= 60;
            int min = i % 60;
            i /= 60;
            int hour = i % 24;
            int days = i / 24;

            string s = days.ToString() + "d " + hour.ToString("D2") + ":" + min.ToString("D02") + ":" + sec.ToString("D02");
            return s;
        }

        private string mPerStr(double AT, double ATotal)
        {
            double d = ATotal <= 0.001 ? 0.0 : 100.0 * AT / ATotal;
            return d.ToString("0.00");
        }

        private void mSaveLogStats()
        {
            if (_mUpdValidateLog != null && _mUpdValidateLog.Length > 0 && _mMctFilesPath != null && _mMctFilesPath.Length > 0)
            {
                string logFile = "";
                try
                {
                    logFile = Path.Combine(_mMctFilesPath, _mMctSourceLabel + "MctLogStats" + CProgram.sDateTimeToYMDHMS(DateTime.Now) + ".log");

                    using (StreamWriter sw = new StreamWriter(logFile))
                    {
                        if (sw != null)
                        {
                            sw.WriteLine(_mUpdValidateLog);
                            sw.Close();
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException(_mMctSourceLabel + "MctLogStats write error " + logFile, ex);
                }
            }
        }
        private void mUpdateStats()
        {
            DateTime startDT = DateTime.Now;
            double t1, t2, t3;
            string s4 = "";

            DateTime firstDT = DateTime.MaxValue;
            DateTime lastDT = DateTime.MinValue;

            _mUpdValidateLog = "Update " + _mMctSourceLabel + " MCT stat @" + CProgram.sDateTimeToYMDHMS(DateTime.Now) + "\r\n";
            _mUpdValidateLog += _mCalcValidateLog;

            if (CLicKeyDev.sbDeviceIsProgrammer() && _mReport._mNrFiles > 0)
            {
                s4 = "";
            }

            labelNearSec.Text = "Near " + _mNearStripSec.ToString("0.0") + "sec";

            mUpdateStatsCharts(out firstDT, out lastDT);
            t1 = (DateTime.Now - startDT).TotalSeconds;
            mUpdateStatsEventsChart(firstDT, lastDT);
            t2 = (DateTime.Now - startDT).TotalSeconds;
            mUpdateStatsValidatedTable(firstDT, lastDT);
            mUpdateStatsDeviceTable(firstDT, lastDT);
            t3 = (DateTime.Now - startDT).TotalSeconds;
            if (checkBoxLogCalc.Checked)
            {
                mSaveLogStats();

                s4 = ", save= " + ((DateTime.Now - startDT).TotalSeconds - t3).ToString("0.00");
            }
            double tUpdate = (DateTime.Now - startDT).TotalSeconds;

            string s = "upd " + tUpdate.ToString("0.00") + " sec, charts=" + t1.ToString("0.00") + ", events=" + (t2 - t1).ToString("0.00")
                + ", tables=" + (t3 - t2).ToString("0.00") + s4 + ", load=" + _mTimeLoadSec.ToString("0.00");

            if (_mReport._mNrFiles > 0 && CLicKeyDev.sbDeviceIsProgrammer())
            {
                labelUpodateTime.Text = s;
            }
            else
            {
                labelUpodateTime.Text = "";
            }
             labelPlotSec.Text = tUpdate.ToString("0.00") + "sec";

            if(checkBoxPulseOnOnly.Checked || checkBoxTogleAfOnly.Checked)
            {
                if (checkBoxPulseOnOnly.Enabled) checkBoxPulseOnOnly.Enabled = false;
                if (checkBoxTogleAfOnly.Enabled) checkBoxTogleAfOnly.Enabled = false;
            }
        }

        public void mFillOneChartCount(Chart AChart, UInt16 AChartIndex, CMctReportItem ASerie, float ADrawFactor, Double AAxisMin, Double AAxisMax, string ALabelFormat)
        {
            string serieNames = "";
            try
            {
                // 
                AChart.Series[AChartIndex].Points.Clear();
                AChart.ChartAreas[0].AxisX.Minimum = AAxisMin;
                AChart.ChartAreas[0].AxisX.Maximum = AAxisMax <= AAxisMin ? AAxisMin + 0.001 : AAxisMax;
                AChart.ChartAreas[0].AxisX.LabelStyle.Format = ALabelFormat;
                //slecht werkt niet
                if (ASerie != null)
                {
                    UInt32 value = 0;
                    int onCount = 0;
                    int lastCount = 0;
                    bool bFirst = true;
                    bool bDo = true; // false;
                    serieNames = ASerie._mLabel;

                    foreach (CMctReportPoint point in ASerie._mList)
                    {
                        if (point._mValue > 0)
                        {
                            ++onCount;
                        }
                        else if (onCount > 0)
                        {
                            --onCount;
                        }
                        if (point._mDateTime >= _mTimeFrameFrom && point._mDateTime <= _mTimeFrameTo)
                        {

                            if (bFirst)
                            {
                                AChart.Series[AChartIndex].Points.AddXY(AAxisMin, onCount > 0 ? ADrawFactor : 0);
                                bFirst = false;
                            }
                            if (bDo) AChart.Series[AChartIndex].Points.AddXY(point._mDateTime.ToOADate(), onCount > 0 ? ADrawFactor : 0);
                            value = point._mValue;
                            bDo = true;
                            AChart.Series[AChartIndex].Points.AddXY(point._mDateTime.ToOADate(), onCount > 0 ? ADrawFactor : 0);
                            lastCount = onCount;
                        }
                        else
                        {
                            value = point._mValue;
                        }
                    }
                    if (false == bFirst)
                    {
                        AChart.Series[AChartIndex].Points.AddXY(AAxisMax, lastCount > 0 ? ADrawFactor : 0);
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException(_mMctSourceLabel + "MCTTrendForm() failed to show stats chart " + serieNames + "[" + AChartIndex + "]", ex);
            }
        }

        public void mFillOneChart/*MonoValue*/(Chart AChart, UInt16 AChartIndex, CMctReportItem ASerie, float ADrawFactor, Double AAxisMin, Double AAxisMax, string ALabelFormat)
        {
            string serieNames = "";
            try
            {
                // 
                AChart.Series[AChartIndex].Points.Clear();
                AChart.ChartAreas[0].AxisX.Minimum = AAxisMin;
                AChart.ChartAreas[0].AxisX.Maximum =  AAxisMax <= AAxisMin ? AAxisMin + 0.001 : AAxisMax;
                AChart.ChartAreas[0].AxisX.LabelStyle.Format = ALabelFormat;

                AChart.Series[AChartIndex].Points.AddXY(AAxisMin, 0);   // make sure that the chart is filled from begin to end

                if (ASerie != null)
                {
                    UInt32 value = 0;
                    UInt32 lastValue = 0;
                    bool bFirst = true;
                    bool bDo = true; // false;
                    serieNames = ASerie._mLabel;

                    foreach (CMctReportPoint point in ASerie._mList)
                    {
                        if (point._mDateTime >= _mTimeFrameFrom && point._mDateTime <= _mTimeFrameTo)
                        {
                            if (bFirst)
                            {
                                //                                AChart.Series[AChartIndex].Points.AddXY(AAxisMin, 0);  // efectively set range
                                //                                AChart.Series[AChartIndex].Points.AddXY(AAxisMin, 1);   // efectively set range
                                AChart.Series[AChartIndex].Points.AddXY(AAxisMin, value > 0 ? ADrawFactor : 0);
                                bFirst = false;
                            }
                            if (bDo) AChart.Series[AChartIndex].Points.AddXY(point._mDateTime.ToOADate(), value > 0 ? ADrawFactor : 0);
                            value = point._mValue;
                            bDo = true;
                            AChart.Series[AChartIndex].Points.AddXY(point._mDateTime.ToOADate(), point._mValue > 0 ? ADrawFactor : 0);
                            lastValue = value;
                        }
                        else
                        {
                            value = point._mValue;
                        }
                    }
                    if (false == bFirst)
                    {
                        AChart.Series[AChartIndex].Points.AddXY(AAxisMax, lastValue > 0 ? ADrawFactor : 0);
                    }
                }
                AChart.Series[AChartIndex].Points.AddXY(AAxisMax, 0);   // make sure that the chart is filled from begin to end
            }
            catch (Exception ex)
            {
                CProgram.sLogException(_mMctSourceLabel + "MCTTrendForm() failed to show stats chart " + serieNames + "[" + AChartIndex + "]", ex);
            }
        }

        public void mFillChart(Chart AChart, CMctReportItem ASerie0, CMctReportItem ASerie1, float ADrawFactor1, Double AAxisMin, Double AAxisMax, string ALabelFormat)
        {
            mFillOneChart(AChart, 0, ASerie0, 1.0F, AAxisMin, AAxisMax, ALabelFormat);
            mFillOneChart(AChart, 1, ASerie1, ADrawFactor1, AAxisMin, AAxisMax, ALabelFormat);
        }
        public void mClearChart(Chart AChart, Double AAxisMin, Double AAxisMax, string ALabelFormat)
        { 
            mFillOneChart(AChart, 0, null, 0.0F, AAxisMin, AAxisMax, ALabelFormat);
            mFillOneChart(AChart, 1, null, 0.0F, AAxisMin, AAxisMax, ALabelFormat);
        }
        public void mFillChartOldMonoValue(Chart AChart, CMctReportItem ASerie0, CMctReportItem ASerie1, float ADrawFactor1, Double AAxisMin, Double AAxisMax, string ALabelFormat)
        {
            string serieNames = "";
            try
            {
                // 
                AChart.Series[0].Points.Clear();
                AChart.ChartAreas[0].AxisX.Minimum = AAxisMin;
                AChart.ChartAreas[0].AxisX.Maximum =  AAxisMax <= AAxisMin ? AAxisMin + 0.001 : AAxisMax;
                AChart.ChartAreas[0].AxisX.LabelStyle.Format = ALabelFormat;

                if (ASerie0 != null)
                {
                    UInt32 value = 0;
                    UInt32 lastValue = 0;
                    bool bFirst = true;
                    bool bDo = true; // false;
                    serieNames = ASerie0._mLabel;

                    foreach (CMctReportPoint point in ASerie0._mList)
                    {
                        if (point._mDateTime >= _mTimeFrameFrom && point._mDateTime <= _mTimeFrameTo)
                        {
                            if (bFirst)
                            {
                                AChart.Series[0].Points.AddXY(AAxisMin, value);
                                bFirst = false;
                            }
                            if (bDo) AChart.Series[0].Points.AddXY(point._mDateTime.ToOADate(), value);
                            value = point._mValue;
                            bDo = true;
                            AChart.Series[0].Points.AddXY(point._mDateTime.ToOADate(), point._mValue);
                            lastValue = value;
                        }
                        else
                        {
                            value = point._mValue;
                        }
                    }
                    if (false == bFirst)
                    {
                        AChart.Series[0].Points.AddXY(AAxisMax, lastValue);
                    }

                }
                AChart.Series[1].Points.Clear();

                if (ASerie1 != null)
                {
                    float fValue = 0;
                    float lastValue = 0;
                    bool bFirst = true;
                    bool bDo = true; // false;

                    //                    chartTachy.Series[1].Points.AddXY(AAxisMin, 0);

                    foreach (CMctReportPoint point in ASerie1._mList)
                    {
                        if (point._mDateTime >= _mTimeFrameFrom && point._mDateTime <= _mTimeFrameTo)
                        {
                            if (bFirst)
                            {
                                AChart.Series[1].Points.AddXY(AAxisMin, fValue);
                                bFirst = false;
                            }
                            if (bDo) AChart.Series[1].Points.AddXY(point._mDateTime.ToOADate(), fValue);
                            fValue = ADrawFactor1 * point._mValue;
                            bDo = true;
                            AChart.Series[1].Points.AddXY(point._mDateTime.ToOADate(), fValue);
                            lastValue = fValue;
                        }
                        else
                        {
                            fValue = ADrawFactor1 * point._mValue;
                        }
                    }
                    if (false == bFirst)
                    {
                        AChart.Series[1].Points.AddXY(AAxisMax, lastValue);
                    }
                    serieNames += "+" + ASerie1._mLabel;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException(_mMctSourceLabel + "MCTTrendForm() failed to show stats chart " + serieNames, ex);

            }
        }

        private void mUpdateStatsCharts(out DateTime AFirstDT, out DateTime ALastDT)
        {
            DateTime firstDT = DateTime.MaxValue;
            DateTime lastDT = DateTime.MinValue;
            float drawFactor = 0.4F;

            try
            {
                if (_mReport != null)
                {
                    if (_mDeviceID != _mReport._mDeviceID)
                    {
                        string altName = CDvtmsData.sGetTzAltSnrName(_mDeviceID);

                        if (altName == null || altName != _mReport._mDeviceID)
                        {
                            labelRecorderID.Text = _mDeviceID + " != " + _mReport._mDeviceID;
                        }
                    }

                    //UInt32 nPace = _mReport.mGetPace( false)._mCountOff2On;
                    //UInt32 nPvc = _mReport._mItemPvc._mCountOff2On;

                    DateTime startPeriodDT = DateTime.SpecifyKind(_mTimeFrameFrom/*_mReport._mStartTimeDT*/, DateTimeKind.Local);
                    DateTime endPeriodDT = DateTime.SpecifyKind(_mTimeFrameTo/*_mReport._mEndTimeDT*/, DateTimeKind.Local);
                    TimeSpan ts = endPeriodDT - startPeriodDT;
                    /*int hours = (int)(ts.TotalHours + 0.1);
                    int days = hours / 24;
                    hours = hours % 24;
                    labelTrendTime.Text = days.ToString() + " days " + hours.ToString() + " hours";
                    */
                    labelTrendStart.Text = CProgram.sDateTimeToString(startPeriodDT) + CProgram.sTimeZoneOffsetStringLong( _mTimeZoneOffsetMin );
                    labelTrendEnd.Text = CProgram.sDateTimeToString(endPeriodDT);

                    labelTrendTime.Text = CProgram.sPrintTimeSpan_dhMin(ts.TotalMinutes, ' ');


                    // chart HR
                    double xAxisMin = startPeriodDT.ToOADate();
                    double xAxisMax = endPeriodDT.ToOADate();
                    string labelFormat = CProgram.sGetShowTimeFormat() + "\n" + CProgram.sGetShowDateFormat();

                    chartHR.Series[0].Points.Clear();
                    chartHR.Series[1].Points.Clear();
                    chartHR.Series[2].Points.Clear();
                    chartHR.ChartAreas[0].AxisX.Minimum = xAxisMin;
                    chartHR.ChartAreas[0].AxisX.Maximum = xAxisMax <= xAxisMin ? xAxisMin + 0.001 : xAxisMax;
                    chartHR.ChartAreas[0].AxisX.LabelStyle.Format = labelFormat;


                    if (CLicKeyDev.sbDeviceIsProgrammer() && _mReport._mNrFiles > 0)
                    {
                        int tempI = 333;

                    }

                    double gapTotalSec = 0;
                    double minGapSec = _mHrBlockMinutes * 60 * 2;
                    bool bTestGap = false;
                    UInt16 pointMin = UInt16.MaxValue;
                    UInt16 pointMax = 0;
                    double pointSum = 0.0;
                    UInt32 pointN = 0;
                    UInt16 minTestHR = 5;

                    // HR average
                    foreach (CMctReportPoint point in _mReport._mItemHrAvg._mList)
                    {
                        if (point._mDateTime >= _mTimeFrameFrom && point._mDateTime <= _mTimeFrameTo)
                        {
                            chartHR.Series[2].Points.AddXY(point._mDateTime.ToOADate(), point._mValue);
                            if( bTestGap )
                            {
                                double sec = (point._mDateTime - lastDT).TotalSeconds;

                                if( sec > minGapSec )
                                {
                                    gapTotalSec += sec;
                                }
                            }
                            bTestGap = true;
                            if (point._mDateTime < firstDT) firstDT = point._mDateTime;
                            if (point._mDateTime > lastDT) lastDT = point._mDateTime;
                            if (point._mValue >= minTestHR)
                            {
                                if (point._mValue < pointMin) pointMin = point._mValue;
                                if (point._mValue > pointMax) pointMax = point._mValue;
                                pointSum += point._mValue;
                                ++pointN;
                            }
                        }
                    }

                    // HR Min
                    foreach (CMctReportPoint point in _mReport._mItemHrMin._mList)
                    {
                        if (point._mDateTime >= _mTimeFrameFrom && point._mDateTime <= _mTimeFrameTo)
                        {
                            chartHR.Series[0].Points.AddXY(point._mDateTime.ToOADate(), point._mValue);
                            if (point._mValue < pointMin && point._mValue >= minTestHR) pointMin = point._mValue;
                        }
                    }

                    // HR Max

                    foreach (CMctReportPoint point in _mReport._mItemHrMax._mList)
                    {
                        if (point._mDateTime >= _mTimeFrameFrom && point._mDateTime <= _mTimeFrameTo)
                        {
                            chartHR.Series[1].Points.AddXY(point._mDateTime.ToOADate(), point._mValue);
                            if (point._mValue > pointMax && point._mValue >= minTestHR) pointMax = point._mValue;
                        }
                    }

                    string gapHR = "";
                    if( lastDT > firstDT)
                    {
                        double timeSec = (lastDT - firstDT).TotalSeconds;
                        if (timeSec >= 1 && pointN > 0)
                        {
                            double perGap = gapTotalSec * 100.0 / timeSec;
                            UInt16 pointMean = (UInt16)(pointSum / pointN + 0.5);
                            gapHR = ">= " + pointMin.ToString()
                                + "\n~ " + pointMean.ToString()
                                + "\n<= " + pointMax.ToString()
                                + "\ngap:\n" + perGap.ToString("0.0")
                                + "%\n=" + CProgram.sPrintTimeSpan_dhmSec(gapTotalSec, "", 2)
                                +"\n/" + CProgram.sPrintTimeSpan_dhmSec(timeSec, "", 2);
                            if( perGap >= 1.0 )
                            {
                                string gapLog = ">= " + pointMin.ToString()
                                    + "~ " + pointMean.ToString()
                                    + "<= " + pointMax.ToString()
                                    + "gap:" + perGap.ToString("0.0")
                                    + "%=" + CProgram.sPrintTimeSpan_dhmSec(gapTotalSec, "", 2)
                                    + "/" + CProgram.sPrintTimeSpan_dhmSec(timeSec, "", 2)
                                    + " @" + CProgram.sPrintDateTimeVersion( firstDT );
                                CProgram.sLogLine(gapLog);
                            }
                        }
                    }

                    labelHrGap.Text = gapHR;


                    // chart Tachy

                    mFillChart(chartTachy, _mReport.mGetTachy(true), _mReport.mGetTachy(false), drawFactor, xAxisMin, xAxisMax, labelFormat);
                    //                UInt32 value = 0;
                    //             float fValue = 0;
                    //            bool bDo = false;
                    // chart Brady

                    mFillChart(chartBrady, _mReport.mGetBrady(true), _mReport.mGetBrady(false), drawFactor, xAxisMin, xAxisMax, labelFormat);
                    if (_mbValidatePause)
                    {
                        chartPause.ChartAreas[0].BackColor = Color.White;
                        mFillChart(chartPause, _mReport.mGetPause(true), _mReport.mGetPause(false), drawFactor, xAxisMin, xAxisMax, labelFormat);
                    }
                    else
                    {
                        chartPause.ChartAreas[0].BackColor = Color.Yellow;
                        if (_mbDrawOncePause && CProgram.sbAskYesNo("MCT",
                            "MCT draw once " + (_mReport.mGetPause(false)._mList.Count / 3).ToString() + " Pause points?"))
                        {
                            _mbDrawOncePause = false;
                            mFillChart(chartPause, null, _mReport.mGetPause(false), drawFactor, xAxisMin, xAxisMax, labelFormat);
                        }
                        else
                        {
                            mClearChart(chartPause, xAxisMin, xAxisMax, labelFormat);
                        }
                    }

                    // chart Afib
                    if (_mbValidateAFib)
                    {
                        chartAfib.ChartAreas[0].BackColor = Color.White;
                        mFillChart(chartAfib, _mReport.mGetAfib(true), _mReport.mGetAfib(false), drawFactor, xAxisMin, xAxisMax, labelFormat);
                    }
                    else
                    {
                        chartAfib.ChartAreas[0].BackColor = Color.Yellow;
                        if (_mbDrawOnceAFib && CProgram.sbAskYesNo("MCT",
                            "MCT draw once " + (_mReport.mGetAfib(false)._mList.Count / 3).ToString() + " AFib points?"))
                        {
                            _mbDrawOnceAFib = false;
                            mFillChart(chartAfib, null, _mReport.mGetAfib(false), drawFactor, xAxisMin, xAxisMax, labelFormat);
                        }
                        else
                        {
                            mClearChart(chartAfib, xAxisMin, xAxisMax, labelFormat);
                        }
                    }
                    mFillChart(chartAflut, _mReport.mGetAflut(true), _mReport.mGetAflut(false), drawFactor, xAxisMin, xAxisMax, labelFormat);

                    mFillChart(chartOther, _mReport.mGetOther(true), _mReport.mGetOther(false), drawFactor, xAxisMin, xAxisMax, labelFormat);

                    mFillChart(chartVtach, _mReport.mGetVtach(true), _mReport.mGetVtach(false), drawFactor, xAxisMin, xAxisMax, labelFormat);

                    mFillChart(chartAbN, _mReport.mGetAbNorm(true), _mReport.mGetAbNorm(false), drawFactor, xAxisMin, xAxisMax, labelFormat);
                    // chart Manual
                    mFillChart(chartManual, _mReport.mGetManual(true), _mReport.mGetManual(false), drawFactor, xAxisMin, xAxisMax, labelFormat);
                    // chart Pace
                    mFillChart(chartPace, _mReport.mGetPace(true), _mReport.mGetPace(false), drawFactor, xAxisMin, xAxisMax, labelFormat);
                    // chart PAC
                    if (_mbValidatePacSveb)
                    {
                        chartPAC.ChartAreas[0].BackColor = Color.White;
                        mFillChart(chartPAC, _mReport.mGetPAC(true), _mReport.mGetPAC(false), drawFactor, xAxisMin, xAxisMax, labelFormat);
                    }
                    else
                    {
                        chartPAC.ChartAreas[0].BackColor = Color.Yellow;
                        if (_mbDrawOncePacSveb && CProgram.sbAskYesNo("MCT",
                            "MCT draw once " + (_mReport.mGetPAC(false)._mList.Count / 3).ToString() + " PAC-SVEB points?"))
                        {
                            _mbDrawOncePacSveb = false;
                            mFillChart(chartPAC, null, _mReport.mGetPAC(false), drawFactor, xAxisMin, xAxisMax, labelFormat);
                        }
                        else
                        {
                            mClearChart(chartPAC, xAxisMin, xAxisMax, labelFormat);
                        }
                    }
                    // chart PVC
                    if (_mbValidatePvcVeb)
                    {
                        chartPVC.ChartAreas[0].BackColor = Color.White;
                        mFillChart(chartPVC, _mReport.mGetPVC(true), _mReport.mGetPVC(false), drawFactor, xAxisMin, xAxisMax, labelFormat);
                    }
                    else
                    {
                        chartPVC.ChartAreas[0].BackColor = Color.Yellow;
                        if (_mbDrawOncePvcVeb && CProgram.sbAskYesNo("MCT",
                             "MCT draw once " + (_mReport.mGetPVC(false)._mList.Count / 3).ToString() + " PVC-VEB points?"))
                        {
                            _mbDrawOncePvcVeb = false;
                            mFillChart(chartPVC, null, _mReport.mGetPVC(false), drawFactor, xAxisMin, xAxisMax, labelFormat);
                        }
                        else
                        {
                            mClearChart(chartPVC, xAxisMin, xAxisMax, labelFormat);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException(_mMctSourceLabel + "MCTTrendForm() failed to show stats charts", ex);

            }
            AFirstDT = firstDT;
            ALastDT = lastDT;
        }

        private CRecAnalysis mFindAnalysisRecord(UInt32 ARecordIX)
        {
            CRecAnalysis analysis = null;

            if (_mAnalysisList != null)
            {
                foreach (CRecAnalysis node in _mAnalysisList)
                {
                    if (node._mRecording_IX == ARecordIX)
                    {
                        analysis = node;    // find last
                    }
                }
            }
            return analysis;
        }
        private void mUpdateStatsEventsChart(DateTime AFirstDT, DateTime ALastDT)
        {
            DateTime firstDT = AFirstDT;
            DateTime lastDT = ALastDT;
            float drawRec = 0.4F;
            float drawEvent = 0.7F;

            try
            {
                DateTime startPeriodDT = DateTime.SpecifyKind(_mTimeFrameFrom/*_mReport._mStartTimeDT*/, DateTimeKind.Local);
                DateTime endPeriodDT = DateTime.SpecifyKind(_mTimeFrameTo/*_mReport._mEndTimeDT*/, DateTimeKind.Local);

                DateTime startDT, endDT, eventDT;
                CRecAnalysis analysis;
                string logLines = "study " + _mStudyIndex.ToString() + "\r\n";

                logLines += "Start Period " + CProgram.sDateTimeToStringDTF(startPeriodDT) + "\r\n";
                logLines += "End Period " + CProgram.sDateTimeToStringDTF(endPeriodDT) + "\r\n";

                double xAxisMin = startPeriodDT.ToOADate();
                double xAxisMax = endPeriodDT.ToOADate();

                chartAllEvents.Series[0].Points.Clear();
                chartAllEvents.Series[1].Points.Clear();
                chartAllEvents.ChartAreas[0].AxisX.Minimum = xAxisMin;
                chartAllEvents.ChartAreas[0].AxisX.Maximum = xAxisMax;
                //                chartAfib.ChartAreas[0].AxisX.LabelStyle.Format = labelFormat;
                bool bFoundAnalysis;

                int nRecTot = 0, nRec = 0, nRecAn = 0, nRecList = _mRecordList.Count;
                int nAn = 0, nAnList = _mAnalysisList.Count;

                int nRecState = (int)DRecState.NrRecStates;
                int[] recStateCount = new int[nRecState];
                int nAnFind = CDvtmsData._sEnumListFindingsClass == null ? 0 : CDvtmsData._sEnumListFindingsClass.mCount();
                int[] anFindCount = nAnFind == 0 ? null : new int[nAnFind];
                string[] anFindLabel = nAnFind == 0 ? null : new string[nAnFind];
                int i;
                DateTime recMinDT = DateTime.MaxValue;
                DateTime recMaxDT = DateTime.MinValue;

                if (CLicKeyDev.sbDeviceIsProgrammer() && _mReport._mNrFiles > 0)
                {
                    i = 88;
                }

                for (i = 0; i < nRecState; ++i)
                {
                    recStateCount[i] = 0;
                }
                for (i = 0; i < nAnFind; ++i)
                {
                    anFindCount[i] = 0;
                    anFindLabel[i] = CDvtmsData._sEnumListFindingsClass.mGetEnumNode((UInt16)i)._mCode;
                }

                bool bFirst = true;


                foreach (CRecordMit rec in _mRecordList)
                {
                    ++nRec;
                    startDT = rec.mGetDeviceTime(rec.mBaseUTC); // all in device time
                    endDT = startDT.AddSeconds(rec.mRecDurationSec);
                    eventDT = rec.mGetDeviceTime(rec.mEventUTC);

                    analysis = mFindAnalysisRecord(rec.mIndex_KEY);
                    bFoundAnalysis = analysis != null;

                    if (_sbLogRecords || checkBoxLogCalc.Checked)
                    {
                        //                        string stateStr = CRecordMit.sGetStateUserString(ARecord.mRecState);
                        //AAnalysis._mAnalysisClassSet.mGetShowSet("+");

                        string line = mCreateShowRecString(false, rec, analysis);
                        logLines += line + "\r\n";
                        //                        CProgram.sLogLine(line);
                    }
                    if (endDT >= _mTimeFrameFrom && startDT <= _mTimeFrameTo)
                    {
                        if (bFirst)
                        {
                            chartAllEvents.Series[0].Points.AddXY(xAxisMin, 0);
                            chartAllEvents.Series[1].Points.AddXY(xAxisMin, 0);
                            bFirst = false;
                        }

                        ++nRecTot;
                        if (rec.mRecState < nRecState)
                        {
                            ++recStateCount[rec.mRecState];
                        }
                        if (bFoundAnalysis)
                        {
                            ++nRecAn;
                            if (nAnFind > 0)
                            {
                                string anLabel = analysis._mAnalysisClassSet.mGetValue(0);

                                for (i = 0; i < nAnFind; ++i)
                                {
                                    if (analysis._mAnalysisClassSet.mbContainsValue(anFindLabel[i]))
                                    {
                                        ++anFindCount[i];
                                    }
                                }
                            }
                        }

                        if (endDT > _mTimeFrameTo) endDT = _mTimeFrameTo;
                        if (startDT < _mTimeFrameFrom) startDT = _mTimeFrameFrom;

                        if (startDT < recMinDT) recMinDT = startDT;
                        if (endDT > recMaxDT) recMaxDT = endDT;

                        chartAllEvents.Series[1].Points.AddXY(startDT.ToOADate(), 0);
                        chartAllEvents.Series[1].Points.AddXY(startDT.ToOADate(), drawRec);
                        if (bFoundAnalysis)
                        {
                            chartAllEvents.Series[0].Points.AddXY(startDT.ToOADate(), 0);
                            chartAllEvents.Series[0].Points.AddXY(startDT.ToOADate(), 1);
                        }
                        if (eventDT >= startDT && eventDT <= endDT)
                        {
                            chartAllEvents.Series[1].Points.AddXY(eventDT.ToOADate(), drawRec);
                            chartAllEvents.Series[1].Points.AddXY(eventDT.ToOADate(), drawEvent);   // add spike at event time
                            chartAllEvents.Series[1].Points.AddXY(eventDT.ToOADate(), drawRec);
                        }
                        chartAllEvents.Series[1].Points.AddXY(endDT.ToOADate(), drawRec);
                        chartAllEvents.Series[1].Points.AddXY(endDT.ToOADate(), 0);
                        if (bFoundAnalysis)
                        {
                            chartAllEvents.Series[0].Points.AddXY(endDT.ToOADate(), 1);
                            chartAllEvents.Series[0].Points.AddXY(endDT.ToOADate(), 0);
                        }
                    }
                }
                if (false == bFirst)
                {
                    chartAllEvents.Series[0].Points.AddXY(xAxisMax, 0);
                    chartAllEvents.Series[1].Points.AddXY(xAxisMax, 0);
                }

                if (nRec != nRecList)
                {
                    logLines += "Wrong number of records\r\n";
                }
                if (_sbLogRecords || checkBoxLogCalc.Checked)
                {
                    int iLine = 0;
                    CRecordMit rec2;
                    int duration;

                    foreach (CRecAnalysis node in _mAnalysisList)
                    {
                        ++nAn;
                        string line = "analysys " + iLine + " " + node.mGetAnalysisClassString() + ": record ";

                        rec2 = node._mrRecordMit; // mFindRecordIx(node._mRecording_IX);

                        if (rec2 != null)
                        {
                            startDT = rec2.mGetDeviceTime(rec2.mBaseUTC);
                            duration = (int)(rec2.mRecDurationSec + 0.5);
                            line += rec2.mStudy_IX.ToString() + "." + rec2.mSeqNrInStudy.ToString()
                                + " (" + CProgram.sDateTimeToString(startDT) + " " + duration.ToString() + " sec " + CRecordMit.sGetStateUserString(rec2.mRecState) + ") ";
                        }
                        else
                        {
                            line += "? ix=" + node._mRecording_IX.ToString();
                        }
                        ++iLine;
                        logLines += line + "\r\n";
                    }
                    logLines += "";
                    if (nAn != nAnList)
                    {
                        logLines += "wrong number of analysis\r\n";
                    }
                }

                string s = nRecTot.ToString() + " / " + nRecList.ToString() + " Records {";
                bool bKomma = false;

                for (i = 0; i < nRecState; ++i)
                {
                    if (recStateCount[i] > 0)
                    {
                        if (bKomma) s += ", ";

                        s += recStateCount[i].ToString() + " " + ((DRecState)i).ToString();
                        bKomma = true;
                    }
                }

                s += "} with " + nRecAn.ToString() + " / " + nAnList.ToString() + " analysis {";
                bKomma = false;
                for (i = 0; i < nAnFind; ++i)
                {
                    if (anFindCount[i] > 0)
                    {
                        if (bKomma) s += ", ";
                        s += anFindCount[i].ToString() + " " + anFindLabel[i];
                        bKomma = true;
                    }
                }
                s += "}";
                labelRecAnalysis.Text = s;
                logLines += ">>" + s + "\r\n";
                _mUpdValidateLog += logLines;
            }
            catch (Exception ex)
            {
                CProgram.sLogException("TzMCTTrendForm() failed to show stats tables", ex);
            }
            _sbLogRecords = false;
        }

        public UInt32 mCountAnalyzed(out double ArDuration, string AFindingsClass, bool AbLog)
        {
            uint n = 0;
            double duration = 0.0;

            foreach (CRecAnalysis node in _mAnalysisList)
            {
                if (node._mAnalysisClassSet.mbContainsValue(AFindingsClass))
                {
                    CRecordMit rec = node._mrRecordMit; // mFindRecordIx(node._mRecording_IX);

                    if (rec != null)
                    {
                        DateTime eventDT = rec.mGetDeviceTime(rec.mEventUTC);
                        bool bEvent = eventDT >= _mTimeFrameFrom && eventDT <= _mTimeFrameTo;

                        if (bEvent)
                        {
                            ++n;
                            duration += rec.mRecDurationSec;

                            int i = _mAnalysisList.IndexOf(node);
                            string s = "counting " + AFindingsClass + " Analysis " + i.ToString()
                                                 + " " + node._mAnalysisClassSet.mGetShowSet("+");

                            DateTime startDT = rec.mGetDeviceTime(rec.mBaseUTC);

                            s += " rec " + rec.mStudy_IX.ToString() + "." + rec.mSeqNrInStudy.ToString()
                                 + (bEvent ? " +" : " -") + CProgram.sDateTimeToString(eventDT)
                                 + " (" + CProgram.sDateTimeToString(startDT) + " " + rec.mRecDurationSec.ToString("0.0") + " sec "
                                 + CRecordMit.sGetStateUserString(rec.mRecState) + ") ";

                            if (AbLog)
                            {
                                CProgram.sLogLine(s);
                            }
                            _mCalcValidateLog += s + "\r\n";
                        }
                    }
                }
            }

            ArDuration = duration;
            return n;
        }
        public UInt32 mCountManualAnalyzed(out double ArDuration, bool AbLog)
        {
            uint n = 0;
            double duration = 0.0;

            foreach (CRecAnalysis node in _mAnalysisList)
            {
                if (node._mbManualEvent)
                {
                    CRecordMit rec = node._mrRecordMit; // mFindRecordIx(node._mRecording_IX);

                    if (rec != null)
                    {
                        DateTime eventDT = rec.mGetDeviceTime(rec.mEventUTC);
                        bool bEvent = eventDT >= _mTimeFrameFrom && eventDT <= _mTimeFrameTo;

                        if (bEvent)
                        {
                            ++n;
                            duration += rec.mRecDurationSec;

                            int i = _mAnalysisList.IndexOf(node);
                            string s = "counting manual Analysis " + i.ToString()
                                                 + " " + node._mAnalysisClassSet.mGetShowSet("+");

                            DateTime startDT = rec.mGetDeviceTime(rec.mBaseUTC);

                            s += " rec " + rec.mStudy_IX.ToString() + "." + rec.mSeqNrInStudy.ToString()
                                 + (bEvent ? " +" : " -") + CProgram.sDateTimeToString(eventDT)
                                 + " (" + CProgram.sDateTimeToString(startDT) + " " + rec.mRecDurationSec.ToString("0.0") + " sec "
                                 + CRecordMit.sGetStateUserString(rec.mRecState) + ") ";

                            if (AbLog)
                            {
                                CProgram.sLogLine(s);
                            }
                            _mCalcValidateLog += s + "\r\n";
                        }
                    }
                }
            }

            ArDuration = duration;
            return n;
        }

        private void mFillChartSummary(int AIndex, UInt32 AValue)
        {
            try
            {
                chartSummaryBar.Series[0].Points[AIndex].SetValueY(AValue);
            }
            catch (Exception ex)
            {
                CProgram.sLogException("FillChartPoint " + AIndex.ToString(), ex);
            }
        }

        private void mUpdateStatsValidatedTable(DateTime AFirstDT, DateTime ALastDT)
        {
            //            DateTime firstDT = AFirstDT;
            //            DateTime lastDT = ALastDT;
            try
            {
                DateTime startPeriodDT = DateTime.SpecifyKind(_mTimeFrameFrom/*_mReport._mStartTimeDT*/, DateTimeKind.Local);
                DateTime endPeriodDT = DateTime.SpecifyKind(_mTimeFrameTo/*_mReport._mEndTimeDT*/, DateTimeKind.Local);
                TimeSpan ts = endPeriodDT - startPeriodDT;
                double tPeriod = ts.TotalSeconds;

                UInt32 nTachy = 0; // _mReport.mGetTachy(false)._mCountOff2On;
                double tTachy = 0; // _mReport.mGetTachy(false)._mDurationOnSec;

                UInt32 nBrady = 0; // _mReport.mGetBrady(false)._mCountOff2On;
                double tBrady = 0; // _mReport.mGetBrady(false)._mDurationOnSec;

                UInt32 nPause = 0; // _mReport.mGetPause(false)._mCountOff2On;
                double tPause = 0; // _mReport.mGetPause(false)._mDurationOnSec;

                UInt32 nAfib = 0; // _mReport.mGetAfib(false)._mCountOff2On;
                double tAfib = 0;// _mReport.mGetAfib(false)._mDurationOnSec;

                double tManual = 0; // not used
                UInt32 nManual = 0; //  _mReport.mGetManual(false)._mCountOff2On;
                                    //                double tManual = _mReport.mGetManual(false)._mDurationOnSec;
                                    //                double perManual = tManual / tTotal;

                UInt32 nPAC = 0;
                double tPAC = 0;

                UInt32 nPVC = 0;
                double tPVC = 0;

                UInt32 nAflut = 0;
                double tAflut = 0;

                UInt32 nPace = 0;
                double tPace = 0;

                UInt32 nVtach = 0;
                double tVtach = 0;

                UInt32 nOther = 0;
                double tOther = 0;

                UInt32 nNorm = 0;
                double tNorm = 0;

                UInt32 nAbNorm = 0;
                double tAbNorm = 0;

                // update validated tabe 
                //double tPeriod = firstDT < DateTime.MaxValue ? (lastDT - firstDT).TotalSeconds + 0.01 : 0.001F; //_mReport._mItemRecording._mDurationOnSec + 0.001;
                bool bLogAll = _mbFullDebug;

                bool bCountAnOnly = checkBoxCountAnOnly.Checked;

                if (CLicKeyDev.sbDeviceIsProgrammer() && _mReport._mNrFiles > 0)
                {
                    bLogAll |= false;
                    int iii = 4;
                }
                /*
         old20180918                       nTachy = bCountAnOnly ? mCountAnalyzed(out tTachy, CDvtmsData._cFindings_Tachy, bLogAll)
                                                    : _mReport.mGetTachy(true).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tTachy, bLogAll);

                                nBrady = bCountAnOnly ? mCountAnalyzed(out tBrady, CDvtmsData._cFindings_Brady, bLogAll)
                                                    : _mReport.mGetBrady(true).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tBrady, bLogAll);

                                nPause = bCountAnOnly ? mCountAnalyzed(out tPause, CDvtmsData._cFindings_Pause, bLogAll)
                                                    : _mReport.mGetPause(true).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tPause, bLogAll);

                                nAfib = bCountAnOnly ? mCountAnalyzed(out tAfib, CDvtmsData._cFindings_AF, bLogAll)
                                                    : _mReport.mGetAfib(true).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tAfib, bLogAll);

                                nManual = bCountAnOnly ? mCountManualAnalyzed(out tManual, bLogAll)
                                                    : _mReport.mGetManual(true).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tManual, bLogAll);

                                nPAC = bCountAnOnly ? mCountAnalyzed(out tPAC, CDvtmsData._cFindings_PAC, bLogAll)
                                                    : _mReport.mGetPAC(true).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tPAC, bLogAll);

                                nPVC = bCountAnOnly ? mCountAnalyzed(out tPVC, CDvtmsData._cFindings_PVC, bLogAll)
                                                    : _mReport.mGetPVC(true).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tPVC, bLogAll);

                                nPace = bCountAnOnly ? mCountAnalyzed(out tPace, CDvtmsData._cFindings_Pace, bLogAll)
                                                    : _mReport.mGetPace(true).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tPace, bLogAll);

                                nVtach = bCountAnOnly ? mCountAnalyzed(out tVtach, CDvtmsData._cFindings_VT, bLogAll)
                                                    : _mReport.mGetVtach(true).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tVtach, bLogAll);

                                nAflut = bCountAnOnly ? mCountAnalyzed(out tAflut, CDvtmsData._cFindings_AFL, bLogAll)
                                                    : _mReport.mGetAflut(true).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tAflut, bLogAll);

                                nOther = bCountAnOnly ? mCountAnalyzed(out tOther, CDvtmsData._cFindings_Other, bLogAll)
                                                    : _mReport.mGetOther(true).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tOther, bLogAll);
                */




                nTachy = _mReport.mGetTachy(true).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tTachy, bLogAll);

                nBrady = _mReport.mGetBrady(true).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tBrady, bLogAll);

                nPause = _mReport.mGetPause(true).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tPause, bLogAll);

                nAfib = _mReport.mGetAfib(true).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tAfib, bLogAll);

                nManual = _mReport.mGetManual(true).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tManual, bLogAll);

                nPAC =  _mReport.mGetPAC(true).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tPAC, bLogAll);

                nPVC =  _mReport.mGetPVC(true).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tPVC, bLogAll);

                nPace = _mReport.mGetPace(true).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tPace, bLogAll);

                nVtach = _mReport.mGetVtach(true).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tVtach, bLogAll);

                nAbNorm = _mReport.mGetAbNorm(true).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tAbNorm, bLogAll);

                nAflut = _mReport.mGetAflut(true).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tAflut, bLogAll);

                nOther = _mReport.mGetOther(true).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tOther, bLogAll);


                if( bCountAnOnly )  // 20180918 show count only, duration from graph
                   {
                    double tCountAnOnly;
                    nTachy = mCountAnalyzed(out tCountAnOnly, CDvtmsData._cFindings_Tachy, bLogAll);

                    nBrady = mCountAnalyzed(out tCountAnOnly, CDvtmsData._cFindings_Brady, bLogAll);

                    nPause = mCountAnalyzed(out tCountAnOnly, CDvtmsData._cFindings_Pause, bLogAll);

                    nAfib = mCountAnalyzed(out tCountAnOnly, CDvtmsData._cFindings_AF, bLogAll);

                    nManual = mCountManualAnalyzed(out tCountAnOnly, bLogAll);

                    nPAC = mCountAnalyzed(out tCountAnOnly, CDvtmsData._cFindings_PAC, bLogAll);

                    nPVC = mCountAnalyzed(out tCountAnOnly, CDvtmsData._cFindings_PVC, bLogAll);

                    nPace = mCountAnalyzed(out tCountAnOnly, CDvtmsData._cFindings_Pace, bLogAll);

                    nVtach = mCountAnalyzed(out tCountAnOnly, CDvtmsData._cFindings_VT, bLogAll);

                    nAflut = mCountAnalyzed(out tCountAnOnly, CDvtmsData._cFindings_AFL, bLogAll);

                    nOther = mCountAnalyzed(out tCountAnOnly, CDvtmsData._cFindings_Other, bLogAll);
                }

                nNorm = 0;
                tNorm = 0;
                if (bCountAnOnly )
                {
                    nNorm = mCountAnalyzed(out tNorm, CDvtmsData._cFindings_Normal, bLogAll);

                    nAbNorm = mCountAnalyzed(out tAbNorm, CDvtmsData._cFindings_AbNormal, bLogAll);
                }


                double tTotal = tTachy + tBrady + tPause + tAfib + /*tPAC + tPVC + tPace */+tAflut + tVtach;// + tManual;
                uint nTotal = nTachy + nBrady + nPause + nAfib + nPAC + nPVC + nPace + nAflut + nVtach + nOther + nNorm + nAbNorm;// + nManual;

                uint nAll = nTachy + nBrady + nPause + nAfib + nPAC + nPVC + nPace + nAflut + nVtach + nOther + nNorm + nAbNorm + nManual;

                uint nMax = nTachy;
                if (nBrady > nMax) nMax = nBrady;
                if (nPause > nMax) nMax = nPause;
                if (nAfib > nMax) nMax = nAfib;
                if (nPAC > nMax) nMax = nPAC;
                if (nPVC > nMax) nMax = nPVC;
                if (nPace > nMax) nMax = nPace;
                if (nAflut > nMax) nMax = nAflut;
                if (nVtach > nMax) nMax = nVtach;
                if (nOther > nMax) nMax = nOther;
                if (nNorm > nMax) nMax = nNorm;
                if (nAbNorm > nMax) nMax = nAbNorm;
                if (nManual > nMax) nMax = nManual;

                /*                    double perTachy = tTachy / tPeriod;
                                    double perBrady = tBrady / tPeriod;
                                    double perPause = tPause / tPeriod;
                                    double perAfib = tAfib / tPeriod;
                                    double perTotal = tTotal / tPeriod;
                */

                // Episodes Summery graph

                if (nAll == 0)
                {
                    chartSummaryBar.Visible = false;
                }
                else
                {
                    int iChart = 0;
                    chartSummaryBar.Visible = true;

                    chartSummaryBar.ChartAreas[0].AxisY.LabelStyle.Interval = nMax < 10 ? 1 : 0;
                    chartSummaryBar.ChartAreas[0].AxisY.MajorGrid.Interval = nMax < 10 ? 1 : 0;
                    chartSummaryBar.ChartAreas[0].AxisY.MajorTickMark.Interval = nMax < 10 ? 1 : 0;

                    mFillChartSummary(iChart++, nTachy);
                    mFillChartSummary(iChart++, nBrady);
                    mFillChartSummary(iChart++, nPause);
                    mFillChartSummary(iChart++, nAfib);
                    mFillChartSummary(iChart++, nAflut);

                    mFillChartSummary(iChart++, nPAC);
                    mFillChartSummary(iChart++, nPVC);
                    mFillChartSummary(iChart++, nPace);
                    mFillChartSummary(iChart++, nVtach);
                    mFillChartSummary(iChart++, nOther);
                    mFillChartSummary(iChart++, nNorm);
                    mFillChartSummary(iChart++, nAbNorm);

                    mFillChartSummary(iChart++, nManual);
                    //
                    //                        AxisY.Maximum = Double.NaN; // sets the Maximum to NaN
                    //                        AxisY.Minimum = Double.NaN; // sets the Minimum to NaN
                    chartSummaryBar.ChartAreas[0].RecalculateAxesScale();
                    chartSummaryBar.Refresh();
                }

                labelEpTachyVal.Text = nTachy.ToString();
                labelEpBradyVal.Text = nBrady.ToString();
                labelEpPauseVal.Text = nPause.ToString();
                labelEpAfibVal.Text = nAfib.ToString();
                labelEpAflutVal.Text = nAflut.ToString();
                labelEpPACVal.Text = nPAC.ToString();
                labelEpPVCVal.Text = nPVC.ToString();
                labelEpPaceVal.Text = nPace.ToString();
                labelEpVtachVal.Text = nVtach.ToString();
                labelEpOtherVal.Text = nOther.ToString();
                labelEpNormVal.Text = nNorm.ToString();
                labelEpAbnVal.Text = nAbNorm.ToString();


                labelEpManVal.Text = nManual.ToString();
                labelEpTotalVal.Text = nTotal.ToString();

                labelDurTachyVal.Text = mTimeStr(tTachy);
                labelDurBradyVal.Text = mTimeStr(tBrady);
                labelDurPauseVal.Text = mTimeStr(tPause);
                labelDurAfibVal.Text = mTimeStr(tAfib);
                labelDurAflutVal.Text = mTimeStr(tAflut);

                labelDurPACVal.Text = "";
                labelDurPVCVal.Text = "";
                labelDurPaceVal.Text = "";
                labelDurVtachVal.Text = mTimeStr(tVtach);
                labelDurAbnVal.Text = mTimeStr(tAbNorm);
                labelDurOtherVal.Text = "";
                labelDurNormVal.Text = "";

                labelDurManVal.Text = "";
                labelDurTotalVal.Text = mTimeStr(tTotal);
                //labelDurPeriodVal.Text = mTimeStr(tPeriod);

                if (tPeriod < 0.01) tPeriod = _mReport.mGetDurationSec();

                labelPerTachyVal.Text = mPerStr(tTachy, tPeriod);
                labelPerBradyVal.Text = mPerStr(tBrady, tPeriod);
                labelPerPauseVal.Text = mPerStr(tPause, tPeriod);
                labelPerAfibVal.Text = mPerStr(tAfib, tPeriod);
                labelPerAflutVal.Text = mPerStr(tAflut, tPeriod);
                labelPerPACVal.Text = "";
                labelPerPVCVal.Text = "";
                labelPerPaceVal.Text = "";
                labelPerVtachVal.Text = mPerStr(tVtach, tPeriod);
                labelPerAbnVal.Text = mPerStr(tAbNorm, tPeriod);
                labelPerOtherVal.Text = "";
                labelPerNormVal.Text = "";

                labelPerTotalVal.Text = mPerStr(tTotal, tPeriod);


            }
            catch (Exception ex)
            {
                CProgram.sLogException(_mMctSourceLabel + "MCTTrendForm() failed to show stats validated table", ex);

            }
        }
        private void mUpdateStatsDeviceTable(DateTime AFirstDT, DateTime ALastDT)
        {
            //DateTime firstDT = AFirstDT;
            //DateTime lastDT = ALastDT;
            try
            {
                DateTime startPeriodDT = DateTime.SpecifyKind(_mTimeFrameFrom/*_mReport._mStartTimeDT*/, DateTimeKind.Local);
                DateTime endPeriodDT = DateTime.SpecifyKind(_mTimeFrameTo/*_mReport._mEndTimeDT*/, DateTimeKind.Local);
                TimeSpan ts = endPeriodDT - startPeriodDT;
                double tPeriod = ts.TotalSeconds;

                UInt32 nTachy = 0; // _mReport.mGetTachy(false)._mCountOff2On;
                double tTachy = 0; // _mReport.mGetTachy(false)._mDurationOnSec;

                UInt32 nBrady = 0; // _mReport.mGetBrady(false)._mCountOff2On;
                double tBrady = 0; // _mReport.mGetBrady(false)._mDurationOnSec;

                UInt32 nPause = 0; // _mReport.mGetPause(false)._mCountOff2On;
                double tPause = 0; // _mReport.mGetPause(false)._mDurationOnSec;

                UInt32 nAfib = 0; // _mReport.mGetAfib(false)._mCountOff2On;
                double tAfib = 0;// _mReport.mGetAfib(false)._mDurationOnSec;

                double tManual = 0; // not used
                UInt32 nManual = 0; //  _mReport.mGetManual(false)._mCountOff2On;
                                    //                double tManual = _mReport.mGetManual(false)._mDurationOnSec;
                                    //                double perManual = tManual / tTotal;

                UInt32 nPAC = 0;
                double tPAC = 0;

                UInt32 nPVC = 0;
                double tPVC = 0;
                UInt32 nPace = 0;
                double tPace = 0;

                bool bLogAll = _mbFullDebug;

                bool bCountAnOnly = checkBoxCountAnOnly.Checked;

                if (CLicKeyDev.sbDeviceIsProgrammer() && _mReport._mNrFiles > 0)
                {
                    bLogAll |= false;
                    int iii = 4;
                }


                //
                // update device table 
                //
                nTachy = _mReport.mGetTachy(false).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tTachy, bLogAll);

                nBrady = _mReport.mGetBrady(false).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tBrady, bLogAll);

                nPause = _mReport.mGetPause(false).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tPause, bLogAll);

                nAfib = _mReport.mGetAfib(false).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tAfib, bLogAll);

                nManual = _mReport.mGetManual(false).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tManual, bLogAll);

                nPAC = _mReport.mGetPAC(false).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tPAC, bLogAll);
                nPVC = _mReport.mGetPVC(false).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tPVC, bLogAll);

                nPace = _mReport.mGetPace(false).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tPace, bLogAll);


                double tTotal = tTachy + tBrady + tPause + tAfib + tPAC + tPVC;// + tManual;
                UInt32 nTotal = nTachy + nBrady + nPause + nAfib + nPAC + nPVC + nPace; // + nManual;

                labelEpTachy.Text = nTachy.ToString();
                labelEpBrady.Text = nBrady.ToString();
                labelEpPause.Text = nPause.ToString();
                labelEpAfib.Text = nAfib.ToString();
                labelEpPAC.Text = nPAC.ToString();
                labelEpPVC.Text = nPVC.ToString();
                labelEpPace.Text = nPace.ToString();
                labelEpMan.Text = nManual.ToString();
                labelEpTotal.Text = nTotal.ToString();

                labelDurTachy.Text = mTimeStr(tTachy);
                labelDurBrady.Text = mTimeStr(tBrady);
                labelDurPause.Text = mTimeStr(tPause);
                labelDurAfib.Text = mTimeStr(tAfib);
                labelDurPAC.Text = "";
                labelDurPVC.Text = "";
                labelDurPace.Text = "";
                labelDurMan.Text = "";
                labelDurTotal.Text = mTimeStr(tTotal);
                labelDurDevice.Text = mTimeStr(tPeriod);

                if (tPeriod < 0.01) tPeriod = _mReport.mGetDurationSec();

                labelPerTachy.Text = mPerStr(tTachy, tPeriod);
                labelPerBrady.Text = mPerStr(tBrady, tPeriod);
                labelPerPause.Text = mPerStr(tPause, tPeriod);
                labelPerAfib.Text = mPerStr(tAfib, tPeriod);
                labelPerPAC.Text = "";
                labelPerPVC.Text = "";
                labelPerPVC.Text = "";

                labelPerMan.Text = "";
                labelPerTotal.Text = mPerStr(tTotal, tPeriod);

                double tDevice = AFirstDT < DateTime.MaxValue ? (ALastDT - AFirstDT).TotalSeconds + 0.01 : 0.001F; //_mReport._mItemRecording._mDurationOnSec + 0.001;

                labelDurDevice.Text = mTimeStr(tDevice);

                /*                labelTimeMonitored.Text = mTimeDayStr(tPeriod);
                                labelTimeAfib.Text = mTimeStr(tAfib);
                                labelAfibHrMax.Text = "";
                                labelLongAfib.Text = "";

                                labelAfibHrAvg.Text = "";
                                labelNoAfibHrAvg.Text = "";
                */
            }
            catch (Exception ex)
            {
                CProgram.sLogException(_mMctSourceLabel + "MCTTrendForm() failed to show stats tables", ex);

            }
        }

        private UInt32 mCalcValidatedCountAnalysis(DateTime AStartDT, DateTime AStopDT, string AFindingsClass, double AIncOverlapSec, bool AbLogAll = false, string ALogHeader = "")
        {
            UInt32 nAnalysis = 0;
            bool bClassSet = AFindingsClass != null && AFindingsClass.Length > 0;
            int i = 0;
            foreach (CRecAnalysis analysis in _mAnalysisList)
            {
                if (false == bClassSet || analysis._mAnalysisClassSet.mbContainsValue(AFindingsClass))
                {
                    CRecordMit rec = analysis._mrRecordMit; // mFindRecordIx(analysis._mRecording_IX);

                    if (rec != null)
                    {
                        DateTime beginDT = rec.mGetDeviceTime(rec.mBaseUTC);
                        DateTime endDT = beginDT.AddSeconds(rec.mRecDurationSec);

                        double dBeginStop = (AStopDT - beginDT).TotalSeconds;
                        double dStartEnd = (endDT - AStartDT).TotalSeconds;

                        //                      bool bOverlap = beginDT <= AStopDT && endDT >= AStartDT;
                        bool bOverLap = dBeginStop > -AIncOverlapSec
                            && dStartEnd > -AIncOverlapSec;

                        if (bOverLap)
                        {
                            analysis.mbActive = true;
                            ++nAnalysis;

                            string s2 = nAnalysis.ToString() + " Analysis #" + i.ToString()
                                  + " " + analysis._mAnalysisClassSet.mGetShowSet("+")
                                  + " rec " + rec.mStudy_IX.ToString() + "." + rec.mSeqNrInStudy.ToString()
                                 + " (" + CProgram.sDateTimeToString(beginDT) + " " + rec.mRecDurationSec.ToString("0.0") + " sec "
                                 + CRecordMit.sGetStateUserString(rec.mRecState) + ") ";

                            string logOverlap = "";

                            if (dStartEnd < 0)
                            {
                                logOverlap = " " + (-dStartEnd).ToString("0.0") + " after";
                            }
                            else if (dBeginStop < 0)
                            {
                                logOverlap = " " + (-dStartEnd).ToString("0.0") + " after";
                            }
                            else if (beginDT <= AStartDT && endDT >= AStopDT)
                            {
                                // included
                                logOverlap = " within ";
                            }
                            else
                            {
                                if (AStartDT > beginDT)
                                {
                                    // included
                                    logOverlap = " starts in ";
                                }
                                else
                                {
                                    logOverlap = " ends in ";
                                }
                            }
                            string s = ALogHeader + logOverlap + s2;
                            _mCalcValidateLog += s + "\r\n";
                            if (AbLogAll)
                            {
                                CProgram.sLogLine(s);
                            }
                        }
                    }
                }
                ++i;
            }

            return nAnalysis;
        }

        private UInt32 mCheckAddValidated(CMctReportItem ArTo, string ALabel, UInt32 ASequence, DateTime AStartDT, DateTime AStopDT, 
            float ADeltaSec, string AFindingsClass,  bool AbLogAll)
        {
            string header = ALabel + "{" + ASequence.ToString("000") + " " + AStartDT.ToString() + " " + AStopDT.ToString() + " " + AFindingsClass + "}";

            uint n = mCalcValidatedCountAnalysis(AStartDT, AStopDT, AFindingsClass, ADeltaSec, AbLogAll, header);
            string s;

            if (n > 0)
            {
                ArTo.mAddTailValue(AStartDT, 0, 0, false);
                ArTo.mAddTailValue(AStartDT, 0, 1, true);
                ArTo.mAddTailValue(AStopDT, 0, 1, true);
                ArTo.mAddTailValue(AStopDT, 0, 0, false);
                s = ArTo._mLabel + " added " + header + " " + n.ToString() + " analysis";
            }
            else
            {
                s = ArTo._mLabel + " not validated period " + header;
            }
            if (AbLogAll)
            {
                CProgram.sLogLine(s);
            }

            _mCalcValidateLog += s + "\r\n";
            return n;
        }

        private void mCalcValidatedFromList(bool AbLogError, CMctReportItem ArTo, bool AbCheckTimeTo, CMctReportItem ArFrom, bool AbCheckTimeFrom, float ADeltaSec, string AFindingsClass, bool AbLogAll = false)
        {
            uint n = 0;
            string logExtra = "";
            DateTime nowDT = DateTime.Now;

            ArTo._mList.Clear();
            if (ArFrom._mList.Count > 0)
            {

               if (AbLogAll)
                {
                    int k = 0;
                }

                if (AbCheckTimeFrom)
                {
                    ArFrom.mbCheckTimeline(AbLogError, AbLogAll);
                }
                double checkTimeSec = (DateTime.Now - nowDT).TotalSeconds;

                UInt32 iOn = ArFrom._mCountPointsOn;
                UInt32 iOff = ArFrom._mCountPointsOff;

                if (iOn > 0 && iOff == 0)
                {
                    logExtra = " Only " + iOn.ToString() + " ON trend points";

                    if( checkBoxTogleAfOnly.Checked && ArFrom._mLabel.Contains( "AF"))
                    {
                        List<CMctReportPoint> oldList = ArFrom._mList;
                        bool bToggleState = false;

                        ArFrom._mList = new List<CMctReportPoint>();

                        ArFrom.mStart(false);

                        foreach (CMctReportPoint point in oldList)
                        {
                            ArFrom.mAddTailValue(point._mDateTime, point._mScpNumber, (UInt16)(bToggleState ? 1 : 0), bToggleState);
                            bToggleState = !bToggleState;
                            ArFrom.mAddTailValue(point._mDateTime, point._mScpNumber, (UInt16)(bToggleState ? 1 : 0), bToggleState);
                        }
                        logExtra += " ! toggle converted";

                    }
                    else if (checkBoxPulseOnOnly.Checked)
                    {
                        List<CMctReportPoint> oldList = ArFrom._mList;

                        ArFrom._mList = new List<CMctReportPoint>();

                        ArFrom.mStart(false);

                        foreach (CMctReportPoint point in oldList)
                        {
                            ArFrom.mAddPulse(point._mDateTime, point._mScpNumber, point._mValue);
                        }
                        logExtra += " ! converted to pulse";
                    }
                }

                bool bState = false;
                DateTime onDT = DateTime.MaxValue;
                double dT;
                bool bFirst = true;
                string strLog = "";

                if (AbLogAll)
                {
                    dT = 0.0;
                }

                foreach (CMctReportPoint point in ArFrom._mList)
                {
                    if (AbLogAll)
                    {
                        strLog = "validating " + ArFrom._mLabel + " point " + CProgram.sDateTimeToString(point._mDateTime) + " val=" + point._mValue.ToString()
                            + (point._mbIsOn ? " ON " : " OFF ");
                    }
                        if (bFirst)
                        {
                            bFirst = false;
                            if (bState == true)
                            {
                                onDT = _mTimeFrameFrom;   // on period already started
                                ++n;
                            }
                        }
                        if (point._mbIsOn)
                        {
                            if (bState == false)
                            {
                                // from off to on => count
                                onDT = point._mDateTime;
                                ++n;
                            }
                        }
                        else if (bState == true)
                        {
                            // from on to off =>  calc on duration
                            dT = (point._mDateTime - onDT).TotalSeconds;
                            uint nAnalyzed = 0;
                            if (dT > 0)
                            {
                            nAnalyzed = mCheckAddValidated(ArTo, ArFrom._mLabel, n, onDT, point._mDateTime,
                                ADeltaSec, AFindingsClass, AbLogAll);
                            }
                            if (AbLogAll)
                            {
                                strLog += "dT= " + CProgram.sPrintTimeSpan_dhmSec(dT, " ") + "nA = "+ nAnalyzed;
                            }
                        }
                    
                    bState = point._mbIsOn;
                    if (AbLogAll)
                    {
                        CProgram.sLogLine(strLog);
                    }
                }
                // end period
                if (bFirst == false && bState == true)
                {

                    dT = (_mTimeFrameTo - onDT).TotalSeconds; // period ends as true
                    uint nAnalyzed = 0;
                    if (dT > 0)
                    {
                        nAnalyzed = mCheckAddValidated(ArTo, ArFrom._mLabel, n, onDT, _mTimeFrameTo,
                            ADeltaSec, AFindingsClass, AbLogAll);
                    }
                    if (AbLogAll)
                    {
                        CProgram.sLogLine("validating " + ArFrom._mLabel + " last point ON end " + CProgram.sDateTimeToString(_mTimeFrameTo) + "dT= " + CProgram.sPrintTimeSpan_dhmSec(dT, " "));
                    }
                }
                if (AbCheckTimeTo)
                {
                    ArTo.mbCheckTimeline(AbLogError, AbLogAll);
                }
                double totalSec = (DateTime.Now - nowDT).TotalSeconds;
                logExtra += " in " + totalSec.ToString("0.00") + "(" + checkTimeSec.ToString("0.00") + ")";
                    }
            
            //if( AbLogAll )
            {
                CProgram.sLogLine("Validated " + ArTo._mLabel + " " + ArTo._mList.Count.ToString() + " from " + ArFrom._mLabel
                    + " " + ArFrom._mList.Count.ToString() + " counted " + n.ToString() + logExtra
                    );
            }
        }
    
    
    private void mCalcValidatedFromListTestON(bool AbLogError, CMctReportItem ArTo, CMctReportItem ArFrom, float ADeltaSec, string AFindingsClass, bool AbLogAll = false)
        {

            if (ArFrom._mList.Count > 0)
            {
                CRecAnalysis analysis;
                int nState = 0;
                UInt16 lastValue = 0;
                int j = 0, count = 0;
                string logStr = "";
                string logExtra = "";

                if (AbLogAll)
                {
                    int k = 0;
                }

                ArFrom.mbCheckTimeline(AbLogError, AbLogAll);

                UInt32 iOn = ArFrom._mCountPointsOn;
                UInt32 iOff = ArFrom._mCountPointsOff;

                if (iOn > 0 && iOff == 0)
                {
                    logExtra = " Only " + iOn.ToString() + " ON trend points";

                    if (checkBoxTogleAfOnly.Checked && ArFrom._mLabel.Contains("AF"))
                    {
                        List<CMctReportPoint> oldList = ArFrom._mList;
                        bool bToggleState = false;

                        ArFrom._mList = new List<CMctReportPoint>();

                        ArFrom.mStart(false);

                        foreach (CMctReportPoint point in oldList)
                        {
                            bToggleState = !bToggleState;
                            ArFrom.mAddPulse(point._mDateTime, point._mScpNumber, (UInt16)(bToggleState ? 1 : 0));
                        }
                        logExtra += " ! toggle converted";

                    }
                    else if (checkBoxPulseOnOnly.Checked)
                    {
                        List<CMctReportPoint> oldList = ArFrom._mList;

                        ArFrom._mList = new List<CMctReportPoint>();

                        ArFrom.mStart(false);

                        foreach (CMctReportPoint point in oldList)
                        {
                            ArFrom.mAddPulse(point._mDateTime, point._mScpNumber, point._mValue);
                        }
                        logExtra += " ! converted to pulse";
                    }
                }

                ArTo._mList.Clear();

                foreach (CMctReportPoint point in ArFrom._mList)
                {
                    if (AbLogAll)
                    {
                        logStr = nState.ToString("D2") + " Validate point " + ArTo._mLabel + "[" + j.ToString("D4") + "] point " + CProgram.sDateTimeToStringDTF(point._mDateTime) + " value = " + point._mValue.ToString() + ": ";
                    }

                    if (point._mbIsOn)
                    {
                        analysis = mFindAnalysisDT(point._mDateTime, ADeltaSec, AFindingsClass);// CDvtmsData._cFindings_Tachy);

                        if (analysis != null)
                        {
                            analysis.mbActive = true;

                            int i = _mAnalysisList.IndexOf(analysis);

                            string s = "cv: to ON Analysis #" + i.ToString()
                                // not valid + " " + CProgram.sDateTimeToString(CProgram.sDateTimeToLocal(analysis._mStudyReportUTC))
                                + " " + analysis._mAnalysisClassSet.mGetShowSet("+");

                            CRecordMit rec = analysis._mrRecordMit; // mFindRecordIx(analysis._mRecording_IX);

                            string recText = "";
                            if (rec != null)
                            {
                                DateTime startDT = rec.mGetDeviceTime(rec.mBaseUTC);
                                int duration = (int)(rec.mRecDurationSec + 0.5);
                                DateTime eventDT = rec.mGetDeviceTime(rec.mEventUTC);
                                bool bEvent = eventDT >= _mTimeFrameFrom && eventDT <= _mTimeFrameTo;

                                recText = " rec " + rec.mStudy_IX.ToString() + "." + rec.mSeqNrInStudy.ToString()
                                 + (bEvent ? " +" : " -") + CProgram.sDateTimeToString(eventDT)
                                 + " (" + CProgram.sDateTimeToString(startDT) + " " + duration.ToString() + " sec "
                                 + CRecordMit.sGetStateUserString(rec.mRecState) + ") ";
                                s += recText;
                            }
                            else
                            {
                                recText = "rec R" + analysis._mRecording_IX.ToString() + "?";
                                s += recText;
                            }
                            _mCalcValidateLog += s + "\r\n";
                            if (AbLogAll)
                            {
                                logStr += s;
                            }
                            if (0 == nState)
                            {
                                // add off point for flank to ON
                                ArTo.mAddTailValue(point._mDateTime, point._mScpNumber, 0, false);
                            }
                            ArTo.mAddTailValue(point._mDateTime, point._mScpNumber, point._mValue, true);
                            ++nState;
                            //                            lastValue = point._mValue;
                            ++count;
                        }
                        else
                        {
                            if (AbLogAll)
                            {
                                logStr += " no analysis";
                                // just continue
                            }
                            ArTo.mAddTailValue(point._mDateTime, point._mScpNumber, 0, false);
                        }
                    }
                    else
                    {
                        // point off
                        if (nState > 0)
                        {
                            --nState;
                            if (0 == nState)
                            {
                                // on to off
                                ArTo.mAddTailValue(point._mDateTime, point._mScpNumber, lastValue, true);
                                //                           ArTo.mAddTailValue(point._mDateTime, point._mScpNumber, 0, false);
                                string s = CProgram.sDateTimeToStringDTF(point._mDateTime) + " last value = " + lastValue.ToString() + " to OFF";
                                _mCalcValidateLog += s + "\r\n";
                                if (AbLogAll)
                                {
                                    logStr += s;
                                }
                            }
                        }
                        ArTo.mAddTailValue(point._mDateTime, point._mScpNumber, 0, false);
                    }
                    if (AbLogAll)
                    {
                        CProgram.sLogLine(logStr);
                    }
                    lastValue = point._mValue;
                    j++;
                }
                //if( AbLogAll )
                {
                    CProgram.sLogLine("Validated " + ArTo._mLabel + " " + ArTo._mList.Count.ToString() + " from " + ArFrom._mLabel
                        + " " + ArFrom._mList.Count.ToString() + " counted " + count.ToString() + logExtra);
                }
                ArTo.mbCheckTimeline(AbLogError, AbLogAll);
            }
        }

        private bool mbAddValidatedToList(CMctReportItem ArTo, CRecAnalysis AAnalysis, bool AbPulse, string AFindingsClass, bool AbLogAdded)
        {
            bool bAdded = false;
            
            if( AAnalysis != null )
            {
                bool bClassSet = AFindingsClass != null && AFindingsClass.Length > 0;

                if (false == bClassSet || AAnalysis._mAnalysisClassSet.mbContainsValue(AFindingsClass))
                {
                    int i = _mAnalysisList.IndexOf(AAnalysis);

                    string s = "av: Analysis index " + i.ToString()
                        // not valid + " "+ CProgram.sDateTimeToString(CProgram.sDateTimeToLocal(AAnalysis._mStudyReportUTC))
                        + " " + AAnalysis._mAnalysisClassSet.mGetShowSet("+");


                    CRecordMit rec = AAnalysis._mrRecordMit; // mFindRecordIx(AAnalysis._mRecording_IX);

                    if (rec != null)
                    {
                        DateTime startDT = rec.mGetDeviceTime(rec.mBaseUTC); // all in device time
                        DateTime endDT = startDT.AddSeconds(rec.mRecDurationSec);
                        DateTime  eventDT = rec.mGetDeviceTime(rec.mEventUTC);
                        bool bEvent = eventDT >= _mTimeFrameFrom && eventDT <= _mTimeFrameTo;

                        if (_mReport._mStartTimeDT > startDT) _mReport._mStartTimeDT = startDT; // make sure the analisis record is in the report range
                        if (_mReport._mEndTimeDT < endDT) _mReport._mEndTimeDT = endDT;

                        if (AbPulse)
                        {
                            DateTime pulseDT = eventDT;

                            if (pulseDT < startDT) pulseDT = startDT;
                            if (pulseDT > endDT) pulseDT = endDT;

                            /*
                            ArTo.mAddAfterwardsBool(pulseDT.AddMilliseconds(-1), 0, false);
                            ArTo.mAddAfterwardsBool(pulseDT, 0, true); //sorted!
                            ArTo.mAddAfterwardsBool(pulseDT.AddMilliseconds(1), 0, false);
                            */
                            ArTo.mAddPeriodOn(pulseDT, pulseDT);
                        }
                        else
                        {
                            /*
                            ArTo.mAddAfterwardsBool(startDT.AddMilliseconds(-1), 0, false);
                            ArTo.mAddAfterwardsBool(startDT, 0, true); //sorted!
                            ArTo.mAddAfterwardsBool(endDT, 0, true);
                            ArTo.mAddAfterwardsBool(endDT.AddMilliseconds(1), 0, false);
                            */
                            ArTo.mAddPeriodOn(startDT, endDT);
                        }
                        int duration = (int)(rec.mRecDurationSec + 0.5);

                        s += " rec " + rec.mStudy_IX.ToString() + "." + rec.mSeqNrInStudy.ToString()
                            + ( bEvent ? " +" : " -" ) + CProgram.sDateTimeToString(eventDT)
                         + " (" + CProgram.sDateTimeToString(startDT) + " " + duration.ToString() + " sec "
                         + CProgram.sDateTimeToString(endDT) + " "
                         + CRecordMit.sGetStateUserString(rec.mRecState) + ") 0->1->1->0";

                        bAdded = true;
                    }
                    else
                    {
                        s += " No REC";
                    }
                    if( AbLogAdded)
                    {
                        CProgram.sLogLine(s);
                    }
                    _mCalcValidateLog += s + "\r\n";

                }
            }
            return bAdded;
        }

        private void mCalValidated(bool AbLogError, bool AbCheckTimeFrom, bool AbCheckTimeTo)
        {
            try
            {
                CMctReportPoint item;
                float tDeltaSec = _mNearStripSec;
                bool bLogAll = _mbFullDebug;
                bool bCountAnOnly = checkBoxCountAnOnly.Checked;



                if ( CLicKeyDev.sbDeviceIsProgrammer() && _mReport._mNrFiles > 0)
                {
                    bLogAll = false;
                    item = null;
                }

                _mCalcValidateLog = "Calc TZ MCT Validate " + CProgram.sDateTimeToString(DateTime.Now) + "\r\n";

                CProgram.sLogLine("Calc validated before " + _mReport._mNrFiles.ToString() + ": MCT start = " + CProgram.sDateTimeToYMDHMS(_mReport._mStartTimeDT)
+ " MCT end= " + CProgram.sDateTimeToYMDHMS(_mReport._mEndTimeDT));


                foreach ( CRecAnalysis node in _mAnalysisList)
                {
                    node.mbActive = false;
                }
                int nTachy0 = _mReport.mGetTachy(false)._mList.Count;
                int nBrady0 = _mReport.mGetBrady(false)._mList.Count;
                int nPause0 = _mReport.mGetPause(false)._mList.Count;
                int nAfib0 = _mReport.mGetAfib(false)._mList.Count;
                int nAflut0 = _mReport.mGetAflut(false)._mList.Count;
                int nPAC0 = _mReport.mGetPAC(false)._mList.Count;
                int nPVC0 = _mReport.mGetPVC(false)._mList.Count;
                int nPace0 = _mReport.mGetPace(false)._mList.Count;
                int nVtach0 = _mReport.mGetVtach(false)._mList.Count;
                int nAbNorm0 = _mReport.mGetAbNorm(false)._mList.Count;
                int nOther0 = _mReport.mGetOther(false)._mList.Count;

                // reset validated 
                _mReport.mGetTachy(true)._mList.Clear();
                _mReport.mGetBrady(true)._mList.Clear();
                _mReport.mGetPause(true)._mList.Clear();
                _mReport.mGetAfib(true)._mList.Clear();
                _mReport.mGetPAC(true)._mList.Clear();
                _mReport.mGetPVC(true)._mList.Clear();
                _mReport.mGetPace(true)._mList.Clear();
                _mReport.mGetManual(true)._mList.Clear();

                if (nPause0 > _mAskLimit)
                {
                    CProgram.sbReqBool("MCT speed ", "Validate " + (nPause0 / 3).ToString() + " Pause points (Slow!)", ref _mbValidatePause);
                }
                if (nAfib0 > _mAskLimit)
                {
                    CProgram.sbReqBool("MCT speed ", "Validate " + (nAfib0 / 3).ToString() + " AFib points (Slow!)", ref _mbValidateAFib);
                }
                if (nPAC0 > _mAskLimit)
                {
                    CProgram.sbReqBool("MCT speed ", "Validate " + (nPAC0/3).ToString() + " PAC-SVEB points (Slow!)", ref _mbValidatePacSveb);
                }
                if (nPVC0 > _mAskLimit)
                {
                    CProgram.sbReqBool("MCT speed ", "Validate " + (nPVC0/3).ToString() + " PVC-VEB points (Slow!)", ref _mbValidatePvcVeb);
                }
                // Tachy
                _mCalcValidateLog += "Validate Tachy: using trend\r\n";
                mCalcValidatedFromList(AbLogError, _mReport.mGetTachy(true), AbCheckTimeTo, _mReport.mGetTachy(false), AbCheckTimeFrom, tDeltaSec, CDvtmsData._cFindings_Tachy, bLogAll);
                // Brady
                _mCalcValidateLog += "Validate Brady\r\n";
                mCalcValidatedFromList(AbLogError, _mReport.mGetBrady(true), AbCheckTimeTo, _mReport.mGetBrady(false), AbCheckTimeFrom, tDeltaSec, CDvtmsData._cFindings_Brady, bLogAll);
                // Pause
                if (_mbValidatePause)
                {
                    _mCalcValidateLog += "Validate Pause:\r\n";
                    mCalcValidatedFromList(AbLogError, _mReport.mGetPause(true), AbCheckTimeTo, _mReport.mGetPause(false), AbCheckTimeFrom, tDeltaSec, CDvtmsData._cFindings_Pause, bLogAll);
                }
                else
                {
                    _mCalcValidateLog += "skip Validate Pause:\r\n";
                }
                // Afib
                if (_mbValidateAFib)
                {
                    _mCalcValidateLog += "Validate AFib:\r\n";
                    mCalcValidatedFromList(AbLogError, _mReport.mGetAfib(true), AbCheckTimeTo, _mReport.mGetAfib(false), AbCheckTimeFrom, tDeltaSec, CDvtmsData._cFindings_AF, bLogAll);
                }
                else
                {
                    _mCalcValidateLog += "skip Validate AFib:\r\n";
                }
                // Aflut
                _mCalcValidateLog += "Validate AFlut:\r\n";
                mCalcValidatedFromList(AbLogError, _mReport.mGetAflut(true), AbCheckTimeTo, _mReport.mGetAfib(false), AbCheckTimeFrom, tDeltaSec, CDvtmsData._cFindings_AFL, bLogAll);

                // PAC
                if (_mbValidatePacSveb)
                {
                    _mCalcValidateLog += "Validate PAC:\r\n";
                    mCalcValidatedFromList(AbLogError, _mReport.mGetPAC(true), AbCheckTimeTo, _mReport.mGetPAC(false), AbCheckTimeFrom, tDeltaSec, CDvtmsData._cFindings_PAC);
                }
                else
                {
                    _mCalcValidateLog += "skip Validate PAC:\r\n";
                }
                // PVC
                if (_mbValidatePvcVeb)
                {
                    _mCalcValidateLog += "Validate PVC:\r\n";
                    mCalcValidatedFromList(AbLogError, _mReport.mGetPVC(true), AbCheckTimeTo, _mReport.mGetPVC(false), AbCheckTimeFrom, tDeltaSec, CDvtmsData._cFindings_PVC);
                }
                else
                {
                    _mCalcValidateLog += "skip Validate PVC:\r\n";
                }
                // Pace
                _mCalcValidateLog += "Validate Pace:\r\n";
                mCalcValidatedFromList(AbLogError, _mReport.mGetPace(true), AbCheckTimeTo, _mReport.mGetPace(false), AbCheckTimeFrom, tDeltaSec, CDvtmsData._cFindings_Pace);
                // Vtach
                _mCalcValidateLog += "Validate Vtach:\r\n";
                mCalcValidatedFromList(AbLogError, _mReport.mGetVtach(true), AbCheckTimeTo, _mReport.mGetVtach(false), AbCheckTimeFrom, tDeltaSec, CDvtmsData._cFindings_VT, bLogAll);

                // AbNorm
                _mCalcValidateLog += "Validate AbNorm:\r\n";
                mCalcValidatedFromList(AbLogError, _mReport.mGetAbNorm(true), AbCheckTimeTo, _mReport.mGetAbNorm(false), AbCheckTimeFrom, tDeltaSec, CDvtmsData._cFindings_AbNormal, bLogAll);

                CProgram.sLogLine("Calc validated @1 " + _mReport._mNrFiles.ToString() + ": MCT start = " + CProgram.sDateTimeToYMDHMS(_mReport._mStartTimeDT)
+ " MCT end= " + CProgram.sDateTimeToYMDHMS(_mReport._mEndTimeDT));

                //                mCalcValidatedFromList(ref _mReport.mGetTachy( true ), _mReport.mGetTachy(false), tDeltaSec, CDvtmsData._cFindings_Tachy);
                // Manual

                if ( false == _mbUseTrend || bCountAnOnly)
                {
                    // only show validated manual events
                    foreach( CRecAnalysis analysis in _mAnalysisList)
                    {
                        if( analysis._mbManualEvent)
                        {
                            CRecordMit rec = analysis._mrRecordMit; // mFindRecordIx(analysis._mRecording_IX);
                            if ( rec != null)
                            {
                                DateTime eventDT = rec.mGetDeviceTime(rec.mEventUTC);
                                _mReport.mGetManual(true).mAddPulse(eventDT, 0, 1);
                            }
                        }
                    }
                }
                else
                {
                    foreach (CMctReportPoint point in _mReport.mGetManual(false)._mList)
                    {
                        item = new CMctReportPoint(point);
                        _mReport.mGetManual(true)._mList.Add(item);
                    }

                }
                /* now a classification test                // Pace
                                foreach (CMctReportPoint point in _mReport.mGetPace( false)._mList)
                                {
                                    item = new CMctReportPoint(point);
                                    _mReport.mGetPace( true )._mList.Add(item);
                                }
                */

                string txtAnalysisUsed = "";
                CProgram.sLogLine("Calc validated @2 " + _mReport._mNrFiles.ToString() + ": MCT start = " + CProgram.sDateTimeToYMDHMS(_mReport._mStartTimeDT)
                        + " MCT end= " + CProgram.sDateTimeToYMDHMS(_mReport._mEndTimeDT));
                if (_mbUseAnalysis)
                {
                    bool bLogAdded = bLogAll;
                    int n = 0, nAdded = 0, nNotUsed = 0;

                    _mCalcValidateLog += "Add unused Analysis:\r\n";

//                    _mCalcValidateLog += "Validate Tachy: using Analysis\r\n";

                    
                    int nTachy1 = _mReport.mGetTachy(true)._mList.Count;
                    int nBrady1 = _mReport.mGetBrady(true)._mList.Count;
                    int nPause1 = _mReport.mGetPause(true)._mList.Count;
                    int nAfib1 = _mReport.mGetAfib(true)._mList.Count;
                    int nAflut1 = _mReport.mGetAflut(true)._mList.Count;
                    int nPAC1 = _mReport.mGetPAC(true)._mList.Count;
                    int nPVC1 = _mReport.mGetPVC(true)._mList.Count;
                    int nPace1 = _mReport.mGetPace(true)._mList.Count;
                    int nVtach1 = _mReport.mGetVtach(true)._mList.Count;
                    int nAbNorm1 = _mReport.mGetAbNorm(true)._mList.Count;
                    int nOther1 = _mReport.mGetOther(true)._mList.Count;

                    foreach (CRecAnalysis node in _mAnalysisList)
                    {
                        ++n;
                        if (node.mbActive == false)
                        {
                            // add to strip
                            bool bAdded = false;
                            bAdded |= mbAddValidatedToList(_mReport.mGetTachy(true), node, false, CDvtmsData._cFindings_Tachy, bLogAdded);
                            bAdded |= mbAddValidatedToList(_mReport.mGetBrady(true), node, false, CDvtmsData._cFindings_Brady, bLogAdded);
                            bAdded |= mbAddValidatedToList(_mReport.mGetPause(true), node, false, CDvtmsData._cFindings_Pause, bLogAdded);
                            bAdded |= mbAddValidatedToList(_mReport.mGetAfib(true), node, false, CDvtmsData._cFindings_AF, bLogAdded);
                            bAdded |= mbAddValidatedToList(_mReport.mGetAflut(true), node, false, CDvtmsData._cFindings_AFL, bLogAdded);
                            bAdded |= mbAddValidatedToList(_mReport.mGetPAC(true), node, true, CDvtmsData._cFindings_PAC, bLogAdded);
                            bAdded |= mbAddValidatedToList(_mReport.mGetPVC(true), node, true, CDvtmsData._cFindings_PVC, bLogAdded);
                            bAdded |= mbAddValidatedToList(_mReport.mGetPace(true), node, true, CDvtmsData._cFindings_Pace, bLogAdded);
                            bAdded |= mbAddValidatedToList(_mReport.mGetVtach(true), node, false, CDvtmsData._cFindings_VT, bLogAdded);
                            bAdded |= mbAddValidatedToList(_mReport.mGetAbNorm(true), node, false, CDvtmsData._cFindings_AbNormal, bLogAdded);
                            bAdded |= mbAddValidatedToList(_mReport.mGetOther(true), node, false, CDvtmsData._cFindings_Other, bLogAdded);
                            if ( bAdded == false )
                            {
                                int i = _mAnalysisList.IndexOf(node);
                                string s = "av: Not added  Analysis index " + i.ToString() 
                                    // not valid + " " + CProgram.sDateTimeToString(CProgram.sDateTimeToLocal(node._mStudyReportUTC))
                                       + " " + node._mAnalysisClassSet.mGetShowSet("+");

                                CRecordMit rec = node._mrRecordMit; // mFindRecordIx(node._mRecording_IX);

                                string recText = "";
                                if (rec != null)
                                {
                                    DateTime startDT = rec.mGetDeviceTime(rec.mBaseUTC);
                                    int duration = (int)(rec.mRecDurationSec + 0.5);
                                    DateTime eventDT = rec.mGetDeviceTime(rec.mEventUTC);
                                    bool bEvent = eventDT >= _mTimeFrameFrom && eventDT <= _mTimeFrameTo;

                                    recText = " rec " + rec.mStudy_IX.ToString() + "." + rec.mSeqNrInStudy.ToString()
                                     + (bEvent ? " +" : " -") + CProgram.sDateTimeToString(eventDT)
                                     + " (" + CProgram.sDateTimeToString(startDT) + " " + duration.ToString() + " sec "
                                     + CRecordMit.sGetStateUserString(rec.mRecState) + ") ";
                                    s += recText;
                                }

                                _mCalcValidateLog += s + "\r\n";
                                if (bLogAdded)
                                {
                                    CProgram.sLogLine(s);
                                }
                                ++nNotUsed;
                            }
                            else
                            {
                                ++nAdded;
                                node.mbActive = true;
                            }
                        }
                    }
                    int nUsed = n - nAdded - nNotUsed;
                    txtAnalysisUsed = "Analysis: " + n.ToString() + ": "+ nUsed.ToString() + " used";
                    if (nAdded > 0 || nNotUsed > 0)
                    {
                        if (nAdded > 0)
                        {
                            txtAnalysisUsed += " " + nAdded.ToString() + " added";
                        }
                        if (nNotUsed > 0)
                        {
                            txtAnalysisUsed += " " + nNotUsed.ToString() +  " not used";
                        }
                    }
                    CProgram.sLogLine("Calc validated @added unused " + _mReport._mNrFiles.ToString() + ": MCT start = " + CProgram.sDateTimeToYMDHMS(_mReport._mStartTimeDT)
    + " MCT end= " + CProgram.sDateTimeToYMDHMS(_mReport._mEndTimeDT));

                    int nTachy2 = _mReport.mGetTachy(true)._mList.Count;
                    int nBrady2 = _mReport.mGetBrady(true)._mList.Count;
                    int nPause2 = _mReport.mGetPause(true)._mList.Count;
                    int nAfib2 = _mReport.mGetAfib(true)._mList.Count;
                    int nAflut2 = _mReport.mGetAflut(true)._mList.Count;
                    int nPAC2 = _mReport.mGetPAC(true)._mList.Count;
                    int nPVC2 = _mReport.mGetPVC(true)._mList.Count;
                    int nPace2 = _mReport.mGetPace(true)._mList.Count;
                    int nVtach2 = _mReport.mGetVtach(true)._mList.Count;
                    int nAbNorm2 = _mReport.mGetAbNorm(true)._mList.Count;
                    int nOther2 = _mReport.mGetOther(true)._mList.Count;

                    CProgram.sLogLine("Analysis " + txtAnalysisUsed);
                    CProgram.sLogLine("Added " + ((nTachy2 - nTachy1) / 4  ).ToString() + " analysis to validated Tachy (" + nTachy2.ToString() + ", " + nTachy1.ToString() + ", " + nTachy0.ToString() + ")");
                    CProgram.sLogLine("Added " + ((nBrady2 - nBrady1) / 4).ToString() + " analysis to validated Brady (" + nBrady2.ToString() + ", " + nBrady1.ToString() + ", " + nBrady0.ToString() + ")");
                    CProgram.sLogLine("Added " + ((nPause2 - nPause1) / 4).ToString() + " analysis to validated Pause (" + nPause2.ToString() + ", " + nPause1.ToString() + ", " + nPause0.ToString() + ")");
                    CProgram.sLogLine("Added " + ((nAfib2 - nAfib1) / 4).ToString() + " analysis to validated Afib (" + nAfib2.ToString() + ", " + nAfib1.ToString() + ", " + nAfib0.ToString() + ")");
                    CProgram.sLogLine("Added " + ((nAflut2 - nAflut1) / 4).ToString() + " analysis to validated Aflut (" + nAflut2.ToString() + ", " + nAflut1.ToString() + ", " + nAflut0.ToString() + ")");
                    CProgram.sLogLine("Added " + ((nPAC2 - nPAC1) / 4).ToString() + " analysis to validated PAC (" + nPAC2.ToString() + ", " + nPAC1.ToString() + ", " + nPAC0.ToString() + ")");
                    CProgram.sLogLine("Added " + ((nPVC2 - nPVC1) / 4).ToString() + " analysis to validated  (" + nPVC2.ToString() + ", " + nPVC1.ToString() + ", " + nPVC0.ToString() + ")");
                    CProgram.sLogLine("Added " + ((nPace2 - nPace1) / 4).ToString() + " analysis to validated Pace (" + nPace2.ToString() + ", " + nPace1.ToString() + ", " + nPace0.ToString() + ")");
                    CProgram.sLogLine("Added " + ((nVtach2 - nVtach1) / 4).ToString() + " analysis to validated Vtach (" + nVtach2.ToString() + ", " + nVtach1.ToString() + ", " + nVtach0.ToString() + ")");
                    CProgram.sLogLine("Added " + ((nAbNorm2 - nAbNorm1) / 4).ToString() + " analysis to validated AbNorm (" + nAbNorm2.ToString() + ", " + nAbNorm1.ToString() + ", " + nAbNorm0.ToString() + ")");
                    CProgram.sLogLine("Added " + ((nOther2 - nOther1) / 4).ToString() + " analysis to validated Other (" + nOther2.ToString() + ", " + nOther1.ToString() + ", " + nOther0.ToString() + ")");
                    // check time line because points were added
                    _mReport.mGetTachy(true).mbCheckTimeline(AbLogError, bLogAll);
                    _mReport.mGetBrady(true).mbCheckTimeline(AbLogError, bLogAll);
                    _mReport.mGetPause(true).mbCheckTimeline(AbLogError, bLogAll);
                    _mReport.mGetAfib(true).mbCheckTimeline(AbLogError, bLogAll);
                    _mReport.mGetAflut(true).mbCheckTimeline(AbLogError, bLogAll);
                    _mReport.mGetPAC(true).mbCheckTimeline(AbLogError, bLogAll);
                    _mReport.mGetPVC(true).mbCheckTimeline(AbLogError, bLogAll);
                    _mReport.mGetPace(true).mbCheckTimeline(AbLogError, bLogAll);
                    _mReport.mGetVtach(true).mbCheckTimeline(AbLogError, bLogAll);
                    _mReport.mGetAbNorm(true).mbCheckTimeline(AbLogError, bLogAll);
                    _mReport.mGetOther(true).mbCheckTimeline(AbLogError, bLogAll);

                    _mCalcValidateLog += "End Add unused Analysis:\r\n";
                }
                else
                {
                    txtAnalysisUsed = _mbUseTrend ? "Trend only, no Analysis" : "! No Trend & Analysis !";
                }
                labelAnalysisUse.Text = txtAnalysisUsed;
                _mCalcValidateLog += "End Calc Validated\r\n";
            }
            catch (Exception ex)
            {
                CProgram.sLogException(_mMctSourceLabel + "MCTTrendForm() failed to calc validated", ex);
            }
        }
        private CRecordMit mFindRecordDT(out bool ArbInStrip, out CRecAnalysis ArRecAnalysis, bool AbFindAnalysis, DateTime ADateTime, bool AbInStrip, string AClassification, bool AbManual)
        {
            CRecordMit foundRec = null;
            bool bFoundInStrip = false;
            bool bCheckClassification = AClassification != null && AClassification.Length > 0;
            CRecAnalysis foundAnalysis = null;

            try
            {
                DateTime startDT, endDT, midDT;
                double sec, foundSec = 1e6;
                bool bInStrip, bTest;
                CRecAnalysis recAnalysis;

                foreach (CRecordMit rec in _mRecordList)
                {
                    bTest = true;
                    recAnalysis = null;

                    if (bCheckClassification)
                    {
                        recAnalysis = mFindAnalysisRecord(rec.mIndex_KEY);

                        bTest = recAnalysis != null
                            && recAnalysis._mAnalysisClassSet.mbContainsValue(AClassification);

                        if (bTest && AbManual)
                        {
                            bTest = recAnalysis._mbManualEvent;
                        }
                    }
                    else if (AbManual)
                    {
                        bTest = rec.mbIsManualEventType(); //mEventTypeString.Contains("Manual");
                    }

                    if (bTest)
                    {
                        startDT = rec.mGetDeviceTime(rec.mBaseUTC); // all in device time
                        endDT = startDT.AddSeconds(rec.mRecDurationSec);
                        //eventDT = rec.mGetDeviceTime(rec.mEventUTC);
                        midDT = startDT.AddSeconds(rec.mRecDurationSec * 0.5F);

                        bInStrip = ADateTime >= startDT && ADateTime <= endDT;
                        if (false == AbInStrip || bInStrip)
                        {
                            sec = (midDT - ADateTime).TotalSeconds;
                            sec = Math.Abs(sec);

                            if (sec < foundSec)
                            {
                                foundRec = rec;
                                foundSec = sec;
                                foundAnalysis = recAnalysis;
                                bFoundInStrip = bInStrip;
                            }
                        }
                    }
                }
                if( foundRec != null && AbFindAnalysis && foundAnalysis == null )
                {
                    recAnalysis = mFindAnalysisRecord(foundRec.mIndex_KEY);
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("mFindRecord() failed to find record", ex);
            }
            ArbInStrip = bFoundInStrip;
            ArRecAnalysis = foundAnalysis;
            return foundRec;
        }
        /*
                private CRecordMit mFindRecordInStudy(UInt16 ARecInStudy)
                {
                    CRecordMit foundRec = null;

                    try
                    {
                        foreach (CRecordMit rec in _mRecordList)
                        {
                            if (rec.mSeqNrInStudy == ARecInStudy)
                            {
                                foundRec = rec;
                                break;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException("mFindRecord() failed to find record", ex);

                    }
                    return foundRec;
                }
        */
        private CRecordMit mFindRecordByIndex( UInt32 ARecIx)
        {
            CRecordMit foundRec = null;

            try
            {
                foreach (CRecordMit rec in _mRecordList)
                {
                    if (rec.mIndex_KEY == ARecIx)
                    {
                        foundRec = rec;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("mFindRecord() failed to find record", ex);

            }
            return foundRec;
        }
        private CRecAnalysis mFindAnalysisDT(DateTime ADateTime, float ATimeDeltaSec, String AFindingsClass )
        {
            CRecordMit foundRec = null;
            CRecAnalysis  foundAnalysis = null;
            CRecAnalysis analysis = null;
            bool bFoundInStrip = false;
            bool bClassSet = AFindingsClass != null && AFindingsClass.Length > 0;

            try
            {
                DateTime startDT, endDT, midDT;
                double sec, foundSec = 1e6;
                bool bInStrip;

                foreach (CRecordMit rec in _mRecordList)
                {
                    startDT = rec.mGetDeviceTime(rec.mBaseUTC); // all in device time
                    endDT = startDT.AddSeconds(rec.mRecDurationSec + ATimeDeltaSec); ;
                    //eventDT = rec.mGetDeviceTime(rec.mEventUTC);
                    midDT = startDT.AddSeconds(rec.mRecDurationSec * 0.5F);
                    startDT = startDT.AddSeconds(-ATimeDeltaSec);

                    bInStrip = ADateTime >= startDT && ADateTime <= endDT;
                    if (bInStrip)
                    {
                        analysis = mFindAnalysisRecord(rec.mIndex_KEY);

                        if( analysis != null 
                            && ( false == bClassSet || analysis._mAnalysisClassSet.mbContainsValue(AFindingsClass)))
                        {
                            sec = (midDT - ADateTime).TotalSeconds;
                            sec = Math.Abs(sec);

                            if (sec < foundSec)
                            {
                                foundRec = rec;
                                foundAnalysis = analysis; 
                                foundSec = sec;
                                bFoundInStrip = bInStrip;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("mFindAnalysis() failed to find record", ex);

            }
            return foundAnalysis;
        }

        private string mDeltaTimeStr( double ADeltaSec, UInt16 ARangeSec )
        {
            string s = "";

            if ( ADeltaSec < ARangeSec)
            {
                s = ADeltaSec.ToString( "0.0") + " sec";
            }
            else
            {
                s = CProgram.sPrintTimeSpan_dhmSec(ADeltaSec, " ");
            }
            return s;
        }

        private string mCreateShowRecString( bool AbShowTime, CRecordMit ARecord, CRecAnalysis AAnalysis)
        {
            string line = "";

            if (AbShowTime)
            {
                line = CProgram.sDateTimeToString(_mShowDT);
            }

            if (ARecord != null)
            {
                DateTime startDT = ARecord.mGetDeviceTime(ARecord.mBaseUTC);
                float duration = ARecord.mRecDurationSec;
                int iDuration = (int)(duration + 0.5);
                DateTime endDT = startDT.AddSeconds(duration);
                DateTime eventDT = ARecord.mGetDeviceTime(ARecord.mEventUTC);
                bool bEvent = eventDT >= _mTimeFrameFrom && eventDT <= _mTimeFrameTo;

                double deltaSec = (_mShowDT - startDT).TotalSeconds;
                string stateStr = CRecordMit.sGetStateUserString(ARecord.mRecState);
                string deltaStr = "";
                double tDeltaSec = _mNearStripSec;

                if( deltaSec < -100000)
                {
                    deltaStr = "?dT?";
                }
                else if (deltaSec < -tDeltaSec)
                {
                    deltaStr = " before " + mDeltaTimeStr(-deltaSec, 60);
                }
                else if (deltaSec < 0.0)
                {
                    deltaStr = " near " + mDeltaTimeStr(-deltaSec, 300); 

                }
                else if (deltaSec > duration + tDeltaSec)
                {
                    deltaStr = " after " + mDeltaTimeStr(deltaSec - duration, 300);// ((int)(deltaSec - duration)).ToString();

                }
                else if (deltaSec > duration)
                {
                    deltaStr = " follows " + mDeltaTimeStr(deltaSec - duration, 60);// // ((int)(deltaSec - duration)).ToString();

                }
                else
                {
                    deltaStr = " in " +  mDeltaTimeStr(deltaSec, 300 );
                }
                if(AbShowTime )
                {
                    line += deltaStr;
                }
                line += ": record " + ARecord.mStudy_IX.ToString()
                    + "." + ARecord.mSeqNrInStudy.ToString() + " (" + CProgram.sDateTimeToString(startDT) + " " + ARecord.mEventTypeString + " "
                    + mDeltaTimeStr(duration, 300) + " "
                    + stateStr + " " + CProgram.sTimeToString(endDT) + ") ";


                if (AAnalysis != null)
                {
                    int i = AAnalysis._mStudyReportNr;

                    i = _mAnalysisList.IndexOf(AAnalysis);

                    line += "Analysis index " + i.ToString()
                         + (bEvent ? " +" : " -") + CProgram.sDateTimeToString(eventDT)

                        // not valid + " " + CProgram.sDateTimeToString(CProgram.sDateTimeToLocal(AAnalysis._mStudyReportUTC))
                        + " " + AAnalysis._mAnalysisClassSet.mGetShowSet("+");
                    if (AAnalysis._mRecording_IX != ARecord.mIndex_KEY)
                    {
                        line += "!Rec not " + AAnalysis._mRecording_IX.ToString() +"!!";
                    }

                }
            }
            return line;
        }

        private void mUpdateShowRec(string AClassification)
        {
            string header = AClassification == null || AClassification.Length == 0 ? "*" : "?" + AClassification + "?";

            string line = mCreateShowRecString(true, _mShowRecord, _mShowAnalysis);

            labelFoundRec.Text = header + "» " + line;
        }
        private void mUpdateFoundRec(DateTime ADateTime, string AClassification, bool AbManual)
        {
            bool bInStrip = false;

            _mShowDT = ADateTime;
            _mShowAnalysis = null;
            _mShowRecord = mFindRecordDT(out bInStrip, out _mShowAnalysis, true, ADateTime, false, AClassification, AbManual);
/*            if (_mShowRecord != null)
            {
                 _mShowAnalysis = mFindAnalysisRecord(_mShowRecord.mIndex_KEY);
            }
*/            mUpdateShowRec(AClassification);
        }

        private void CMCTTrendDisplay_Load(object sender, EventArgs e)
        {

        }

        private void panel5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel10_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void chart2_Click_1(object sender, EventArgs e)
        {

        }

        private void chart6_Click(object sender, EventArgs e)
        {

        }

        private void chart9_Click(object sender, EventArgs e)
        {

        }

        private void labelPrint_Click(object sender, EventArgs e)
        {
            mDoPrint();
        }

        private bool mbCheckDeviceOnReport()
        {
            bool bOk = false;

            if (_mStudyInfo != null && _mDeviceInfo != null && _mReport != null)
            {
                bOk = _mDeviceInfo._mDeviceSerialNr == _mReport._mDeviceID;

                if( false == bOk )
                {
                    string altName = CDvtmsData.sGetTzAltSnrName(_mDeviceInfo._mDeviceSerialNr);

                    if( altName != null && altName.Length > 0)
                    {
                        bOk = altName == _mReport._mDeviceID;
                    }
                }
            }
            return bOk;
        }

        private void mDoPrint()
        { 
            if (_mStudyInfo != null && _mPatientInfo != null && _mDeviceInfo != null && _mReport != null)
            {
                /*                bool bInitials = _mCreadedByInitials != null && _mCreadedByInitials.Length > 0;

                                if (false == bInitials)
                                {
                                    if (CProgram.sbReqLabel("Print pdf", "User Initials", ref _mCreadedByInitials, ""))
                                    {
                                        bInitials = _mCreadedByInitials != null && _mCreadedByInitials.Length > 0;
                                    }
                                }
                                labelUser.Text = _mCreadedByInitials;
                */

                if ( mbCheckDeviceOnReport()
                    || CProgram.sbAskOkCancel( "Print MCT Trend for study " + _mStudyInfo.mIndex_KEY.ToString(),
                        "Serial number Not equal, print any way?" )
                    )
                {


                    bool bAnonymize = checkBoxAnonymize.Checked;
                    bool bBlackWhite = checkBoxBlackWhite.Checked;

                    string printInitials;

                    if (CProgram.sbEnterProgUserArea(true)
                    && FormEventBoard.sbGetPrintUserInitials("MCT", out printInitials, false, ""))
                    {
                        bool bValidated = checkBoxValidated.Checked;
                        bool bCountAnOnly = checkBoxCountAnOnly.Checked;
                        bool bPrintDur = checkBoxPrintDur.Checked;
                        bool bPrintStat = checkBoxPrintStat.Checked;
                        bool bQCd = checkBoxQCD.Checked;

                        DTriageTimerMode prevMode = FormEventBoard.sSetTriageTimerEntering(DTriageTimerMode.Print, "Print MCT by " + printInitials);

                        using (CPrintMctForm form = new CPrintMctForm(this, chartAllEvents, _mReport, bValidated, _mStudyInfo, _mPatientInfo, _mDeviceInfo,
                            _mTimeFrameFrom, _mTimeFrameTo, true, printInitials, bAnonymize, bBlackWhite, bCountAnOnly,
                            bPrintStat, bPrintDur, bQCd))
                        {
                            if (form != null)
                            {
                                form.ShowDialog();

                                if (form._mStudyReportNr == 0)
                                {
                                    labelPrintResult.Text = "Report not filed";
                                }
                                else
                                {
                                    labelPrintResult.Text = "Report " + form._mStudyReportNr.ToString() + " filed";
                                }
                            }
                        }
                        FormEventBoard.sSetTriageTimerLeaving(prevMode, "Print MCT");

                    }
                }
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private bool mbCheckTzQueue(ref string ArResult)
        {
            bool bOk = true;

            UInt32 nQueue = 0;
            DateTime oldestDT;
            string storeSnr = CDvtmsData.sGetTzStoreSnrName(_mDeviceID);

            if (CTzDeviceSettings._sbUseQueue)
            {
                if (CTzDeviceSettings.sbCheckActionQueue(out nQueue, out oldestDT, _mDevicePath, storeSnr))
                {
                    if (ArResult != null && ArResult.Length > 0)
                    {
                        ArResult += ", ";
                    }
                    ArResult += storeSnr + " " + nQueue.ToString() + " in queue ";
                    if (nQueue > 0)
                    {
                        ArResult += CProgram.sDateTimeToYMDHMS(oldestDT);
                    }
                    else
                    {
                        bOk = false;
                    }
                }
                else
                {
                    ArResult += "queue?";
                    bOk = false;
                }
            }
            return bOk;
        }

        private bool mbCheckSironaQueue(ref string ArResult)
        {
            bool bOk = true;

            if (CSironaStateSettings._sbUseQueue)
            {
                string result2;
                DateTime oldDT;
                UInt32 nrReqQueue = 0;

                if (ArResult != null && ArResult.Length > 0)
                {
                    ArResult += ", ";
                }
                if (CSironaStateSettings.sbCheckSettingsFileQueue(out result2, out nrReqQueue, out oldDT, _mDeviceID))
                {
                    ArResult += nrReqQueue.ToString() + " in queue ";
                    if (nrReqQueue == 0)
                    {
                        //result += "!";
                        bOk = false;
                    }
                    else
                    {
                        ArResult += CProgram.sDateTimeToYMDHMS(oldDT);
                    }
                }
                else
                {
                    ArResult += "queue?";
                    bOk = false;
                }
            }
            return bOk;
        }



        private void mInitQuery()
        {
            string result = "";
            UInt32 actionNumber = 0;
            string altDeviceID = "";
            string tzone = ".";

            _mbQueryActive = false;

            if (_mDeviceInfo == null || _mDeviceID == null || _mDeviceID == "")
            {
                result = "No device present";
            }
            else if (_mStudyInfo == null || _mDeviceInfo._mActiveStudy_IX != _mStudyInfo.mIndex_KEY)
            {
                result = "Device " + _mDeviceID + " is not assigned to study " + _mStudyInfo.mIndex_KEY.ToString();
            }
            else if (_mDeviceInfo._mState_IX != (UInt16)DDeviceState.Assigned || _mDeviceInfo._mActiveStudy_IX != _mStudyIndex)
            {
                result = "Device state " + _mDeviceID + " is not assigned";
            }
            else
            {
                switch (_mMctSource)
                {
                    case DDeviceType.TZ:
                        altDeviceID = CDvtmsData.sGetTzAltSnrName(_mDeviceID);
                        _mbQueryActive = _mDevicePath != null
                        && CTzDeviceSettings.sbGetActionSeqNr(out actionNumber, _mDevicePath, _mDeviceID);
                        //&& actionNumber > 0;

                        mbCheckTzQueue(ref result);

                        break;
                    case DDeviceType.Sirona:

                        _mbQueryActive = _mDevicePath != null;  // just enable for now, need to check if status file is present
                                                                //                         && CTzDeviceSettings.sbGetActionSeqNr(out actionNumber, _mDevicePath, _mDeviceID);
                                                                //&& actionNumber > 0;
                                                                // todo check last settings
                        mbCheckSironaQueue(ref result);

                        break;
                    case DDeviceType.DV2:
                        break;
                    case DDeviceType.DVX:
                        // no quiry                                        
                        break;
                }
                if (false == _mbQueryActive)
                {
                    result = "Device " + _mDeviceID + " not setup for query";

                    if( CLicKeyDev.sbDeviceIsProgrammer())
                    {
                        int j = 3;
                    }
                }
            }
            if (_mReport != null && _mReport._mDeviceID != null && _mReport._mDeviceID.Length > 0 && _mbQueryActive)
            {
                if (_mDeviceID != _mReport._mDeviceID 
                    && ( altDeviceID == null || altDeviceID.Length == 0 || altDeviceID != _mReport._mDeviceID))
                {
                    
                    if ( result != null && result.Length > 0)
                    {
                        result += ", ";
                    }
                    result += "Snr "+ _mDeviceID + " != " + _mReport._mDeviceID;
                }
            }
            if (_mReport != null )
            {
                tzone = "-";

                if(_mTimeZoneMin == Int16.MinValue && _mReport._mNrFiles > 0)
                {
                    _mTimeZoneMin = _mReport._mTimeZoneOffsetMin;   // no time zone in analysis or record then use from loaded files (tz only)
                }
                if(_mTimeZoneMin > Int16.MinValue)
                {
                    tzone = CProgram.sTimeZoneOffsetStringLong(_mTimeZoneMin);
                }
            }

            labelTimeZone.Text = tzone;
            labelStart.Visible = _mbQueryActive;
            dateTimeStart.Visible = _mbQueryActive;
            textBoxStartHour.Visible = _mbQueryActive;
            textBoxStartMinute.Visible = _mbQueryActive;
            comboBoxStart.Visible = _mbQueryActive;
            buttonQueryECG.Visible = _mbQueryActive;
            buttonQueryTrend.Visible = _mbQueryActive;
            labelLength.Visible = _mbQueryActive;
            textBoxLength.Visible = _mbQueryActive;
            labelMinutes.Visible = _mbQueryActive;
            labelColon.Visible = _mbQueryActive;

            buttonCopyFrom.Visible = _mbQueryActive;

            mUpdateQueryTime();

            labelQueryInfo.Text = result;
        }

        public bool mbDeterminTzQuery( out string ArRangeText, out UInt32 ArScpStartNumber, out UInt16 ArScpCount, string ASerialNr)
        {
            bool bOk = false;
            string s = "";
            UInt32 scpStartNumber = 0;
            UInt16 scpCount = 0;

            if (_mQueryLengthMin > 0 && _mReport != null && _mTimeQuery > DateTime.MinValue)
            {
                if (_mTzSettings == null)
                {
                    _mTzSettings = new CTzDeviceSettings();
                }
                if (_mTzSettings != null)
                {
                    UInt32 scpStart = 0;
                    UInt32 scpStop = 0;
                    bool bEstStart = false;
                    bool bEstStop = false;

                    _mTzSettings.serialNumber = ASerialNr;

                    if (_mReport.mbGetScpNumber(_mTimeQuery, out scpStart, out bEstStart, false))
                    {
                        DateTime endDT = _mTimeQuery.AddMinutes(_mQueryLengthMin);

                        if (_mReport.mbGetScpNumber(endDT, out scpStop, out bEstStop, true))
                        {
                            UInt32 n = scpStop - scpStart + 1;

                            if (n > UInt16.MaxValue)
                            {
                                s = "Length " + _mQueryLengthMin.ToString() + "min. to long";
                            }
                            else
                            {
                                scpStartNumber = scpStart;
                                scpCount = (UInt16)n;
                                bOk = true;

                                s = "File " + (bEstStart ? "~" : "") + scpStart.ToString() + ", n= " + (bEstStop ? "~" : "") + n.ToString();
                            }
                        }
                        else
                        {
                            s = "Invalid start time";
                        }
                    }
                    else
                    {
                        s = "Invalid start time";
                    }

                }


            }
            if (false == bOk && s == "")
            {
                s = "Invalid range";
            }
            ArRangeText = s;
            ArScpStartNumber = scpStartNumber;
            ArScpCount = scpCount;
            return bOk;
        }

        private bool mbTzSendQueryEcgAction(out string AResult, UInt32 AScpStartNumber, UInt16 AScpCount, string AMessage)
        {
            string result = "Failed";
            UInt32 actionNumber = 0;

            string storeSnr = CDvtmsData.sGetTzStoreSnrName(_mDeviceID);
            bool bOk = CTzDeviceSettings.sbIncActionSeqNr(out actionNumber, _mDevicePath, storeSnr);

            if (bOk)
            {
                string fileName = "";

                UInt32 index;
                Byte[] buffer = null;
                bool bStartStudy = false;

                try
                {
                    if (CTzDeviceSettings.sbMakeActionFileName(out fileName, storeSnr))
                    {
                        fileName = Path.Combine(_mDevicePath, fileName);
                        buffer = _mTzSettings.mActionCreateBuffer(out index, storeSnr, (UInt16)actionNumber);

                        if (buffer != null)
                        {
                            if (bStartStudy)
                            {
                                bOk &= _mTzSettings.mbActionStartStudy(buffer, ref index);
                            }
                            if (AMessage != null && AMessage != "")
                            {
                                bOk &= _mTzSettings.mbActionDisplayMessage(buffer, ref index, AMessage);
                            }
                            if (AScpCount > 0)
                            {
                                bOk &= _mTzSettings.mbActionRequestScpBlock(buffer, ref index, AScpStartNumber, AScpCount);
                            }
                            if (false == bOk)
                            {
                                result = "create failed";
                            }
                            else
                            {
                                result = "query SCP(" + AScpStartNumber.ToString() + "n" + AScpCount.ToString() + ")";

                                bool bCopyOk;
                                bOk = _mTzSettings.mbWriteAction(fileName, buffer, ref index, true, out bCopyOk, (UInt16)actionNumber);

                                if (bOk)
                                {
                                    result += bCopyOk ? " send to server." : " failed send!";                                   
                                }
                                else
                                {
                                    result += " failed write!";
                                }
                            }
                        }
                    }
                    UInt32 nQueue = 0;
                    DateTime oldestDT;

                    if (CTzDeviceSettings.sbCheckActionQueue(out nQueue, out oldestDT, _mDevicePath, storeSnr))
                    {
                        result += ", " + storeSnr + " " + nQueue.ToString() + " in queue ";
                        if( nQueue > 0 )
                        {
                            result += CProgram.sDateTimeToYMDHMS(oldestDT);
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Generate SCP request Action", ex);
                }
            }
            AResult = result;

            return bOk;
        }

        private bool mbTzSendQueryTrendAction(out string AResult, DateTime ADateTime, UInt16 ANrHours, string AMessage)
        {
            string result = "Failed";
            UInt32 actionNumber = 0;
            string storeSnr = CDvtmsData.sGetTzStoreSnrName(_mDeviceID);

            bool bOk = CTzDeviceSettings.sbIncActionSeqNr(out actionNumber, _mDevicePath, storeSnr);

            if (bOk)
            {
                string fileName = "";

                UInt32 index;
                Byte[] buffer = null;
                bool bStartStudy = false;

                try
                {
                    if (CTzDeviceSettings.sbMakeActionFileName(out fileName, storeSnr))
                    {
                        fileName = Path.Combine(_mDevicePath, fileName);
                        buffer = _mTzSettings.mActionCreateBuffer(out index, storeSnr, (UInt16)actionNumber);

                        if (buffer != null)
                        {
                            result = "Query Trend " + ADateTime.Year.ToString() + "\\" + ADateTime.Month.ToString()
                  + "\\" + ADateTime.Day.ToString() + " " + ADateTime.Hour.ToString() + " for " + ANrHours.ToString() + " hours";

                            if (bStartStudy)
                            {
                                bOk &= _mTzSettings.mbActionStartStudy(buffer, ref index);
                            }
                            if (AMessage != null && AMessage != "")
                            {
                                bOk &= _mTzSettings.mbActionDisplayMessage(buffer, ref index, AMessage);
                            }
                            if (ANrHours > 0)
                            {
                                DateTime dt = ADateTime;

                                for (int i = 0; i < ANrHours; ++i)
                                {
                                    bOk &= _mTzSettings.mbActionRequestTzrFile(buffer, ref index,
                                        (UInt16)dt.Year, (UInt16)dt.Month, (UInt16)dt.Day, (UInt16)dt.Hour);
                                    dt = dt.AddHours(1);
                                }

                            }
                            if (false == bOk)
                            {
                                result += " create failed";
                            }
                            else
                            {
                                bool bCopyOk;
                                 bOk = _mTzSettings.mbWriteAction(fileName, buffer, ref index, true, out bCopyOk, (UInt16)actionNumber);

                                if (bOk)
                                {
                                    result += bCopyOk ? " send to server." : " failed send!";
                                }
                                else
                                {
                                    result += " Failed write";
                                }
                            }
                        }
                    }
                    UInt32 nQueue = 0;
                    DateTime oldestDT;

                    if (CTzDeviceSettings.sbCheckActionQueue(out nQueue, out oldestDT, _mDevicePath, storeSnr))
                    {
                        result += ", " + storeSnr + " " + nQueue.ToString() + " in queue ";
                        if (nQueue > 0)
                        {
                            result += CProgram.sDateTimeToYMDHMS(oldestDT);
                        }
                    }

                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Generate Trend Request Action", ex);
                }
            }
            AResult = result;

            return bOk;
        }

        private void mDoQueryECG()
        {
            switch (_mMctSource)
                {
                    case DDeviceType.TZ:
                        mDoTzQueryECG();
                        break;
                    case DDeviceType.Sirona:
                        mDoSironaQueryECG();
                        break;
                case DDeviceType.DV2:
                    break;
                case DDeviceType.DVX:
                    break;

            }
        }
        private void mDoTzQueryECG()
        {
            string result = "";

            try
            {
                if (_mbQueryActive)
                {
                    bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;
                    UInt32 scpStartNumber = 0;
                    UInt16 scpCount = 0;
                    string storeSnr = CDvtmsData.sGetTzStoreSnrName(_mDeviceID);

                    if (mbDeterminTzQuery(out result, out scpStartNumber, out scpCount, storeSnr))
                    {
                        string actionResult;
                        string message = "";


                        if (false == bCtrl
                            || CProgram.sbReqLabel("TZ query ECG: Send to device " + _mDeviceID, "message", ref message, "", false))
                        {
                            mbTzSendQueryEcgAction(out actionResult, scpStartNumber, scpCount, message);
                            mbCheckTzQueue(ref result);
                            result += ", " + actionResult;
                        }
                    }

                }
                labelQueryInfo.Text = result;
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Query failed for device " + _mDeviceID, ex);
            }

        }

        private void mDoSironaQueryECG()
        {
            string result = "";

            try
            {
                if (_mbQueryActive)
                {
                    if (_mQueryLengthMin > 0 && _mReport != null && _mTimeQuery > DateTime.MinValue)
                    {
                        CSironaStateSettings.sbRequestSironaData(false, out result, _mDeviceID, _mTimeQuery, (UInt32)(_mQueryLengthMin * 60), true);
                        mbCheckSironaQueue(ref result);
                    }
                }
                labelQueryInfo.Text = result;
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Sirona Query failed for device " + _mDeviceID, ex);
            }

        }
        private void mDoQueryTrend()
        {
            switch (_mMctSource)
            {
                case DDeviceType.TZ:
                    mDoTzQueryTrend();
                    break;
                case DDeviceType.Sirona:
                    mDoSironaQueryTrend();
                    break;
                case DDeviceType.DV2:
                    break;
                case DDeviceType.DVX:
                    break;

            }
        }
        private void mDoTzQueryTrend()
        {
            string result = "";

            try
            {
                if (_mbQueryActive)
                {

                    bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

                    UInt16 nrHours = (UInt16)((_mQueryLengthMin + 59) / 60);
                    string actionResult;
                    string message = "";

                    if (false == bCtrl
                            || CProgram.sbReqLabel("TZ query Trend: Send to device " + _mDeviceID, "message", ref message, "", false))
                    {
                        mbTzSendQueryTrendAction(out actionResult, _mTimeQuery, nrHours, message);
                        result += ", " + actionResult;

                        mbCheckTzQueue(ref result);
                    }


                }
                labelQueryInfo.Text = result;
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Query failed for device " + _mDeviceID, ex);
            }

        }

        private void mDoSironaQueryTrend()
        {
            string result = "";

            try
            {
                if (_mbQueryActive)
                {
                    if (_mQueryLengthMin > 0 && _mReport != null && _mTimeQuery > DateTime.MinValue)
                    {

                        CSironaStateSettings.sbRequestSironaData(false, out result, _mDeviceID, _mTimeQuery, (UInt32)(_mQueryLengthMin * 60), false);

                        mbCheckSironaQueue(ref result);
                     }
                }
                labelQueryInfo.Text = result;
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Sirona Query failed for device " + _mDeviceID, ex);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (mReadQuiryStartTime() > 0)
            {
                mDoQueryECG();
            }
        }

        private void radioButtonHour_CheckedChanged(object sender, EventArgs e)
        {
            mChangeRangeTime();
        }

        private void buttonSetTimeFrame_Click(object sender, EventArgs e)
        {
            mReadUpdateTimeFrame();
            mInitQuery();
        }

        private void radioButtonWeek_CheckedChanged(object sender, EventArgs e)
        {
            mChangeRangeTime();
        }

        private void radioButtonDay_CheckedChanged(object sender, EventArgs e)
        {
            mChangeRangeTime();
        }

        private void radioButtonMonth_CheckedChanged(object sender, EventArgs e)
        {
            mChangeRangeTime();
        }

        private void radioButtonPeriod_CheckedChanged(object sender, EventArgs e)
        {
            mChangeRangeTime();
        }

        private void mChangeRangeTime()
        {
            mGetTimeFrame();
            mUpdateTimeFrame();
        }

        private void radioButtonAll_CheckedChanged(object sender, EventArgs e)
        {
            mChangeRangeTime();
        }

        private void buttonM24_Click(object sender, EventArgs e)
        {
            mShiftTimeFrame(-24);
        }

        private void buttonM6_Click(object sender, EventArgs e)
        {
            mShiftTimeFrame(-6);
        }

        private void buttonM1_Click(object sender, EventArgs e)
        {
            mShiftTimeFrame(-1);
        }

        private void buttonP1_Click(object sender, EventArgs e)
        {
            mShiftTimeFrame(+1);
        }

        private void buttonP6_Click(object sender, EventArgs e)
        {
            mShiftTimeFrame(+6);
        }

        private void buttonP24_Click(object sender, EventArgs e)
        {
            mShiftTimeFrame(+24);
        }

        private void button0h_Click(object sender, EventArgs e)
        {
            _mTimeFrameFrom = new DateTime(_mTimeFrameFrom.Year, _mTimeFrameFrom.Month, _mTimeFrameFrom.Day, 0, 0, 0 );
            _mTimeFrameTo = new DateTime(_mTimeFrameTo.Year, _mTimeFrameTo.Month, _mTimeFrameTo.Day, 0, 0, 0);
            mUpdateTimeFrame();
        }

        private void buttonCopyFrom_Click(object sender, EventArgs e)
        {
            _mTimeQuery = mReadFromTime().AddMinutes( -1);
            DateTime endTime = mReadToTime().AddMinutes(1);
            _mQueryLengthMin = (UInt16)(0.5 + (_mTimeQuery - endTime).TotalMinutes);
            if(_mQueryLengthMin > 24 * 60 )
            {
                _mQueryLengthMin = 24 * 60;
            }
            mUpdateQueryTime();
        }

        private void chartHR_Click(object sender, EventArgs e)
        {

        }

        private void mChartMouseDown( MouseEventArgs e, Chart AChart, string AClassification, bool AbManual)
        {
            try
            {
                double t = 0.0;
                int x = e.X;
                int y = e.Y;
                HitTestResult hitResult;


                hitResult = AChart.HitTest(x, y, Chart​Element​Type.PlottingArea);

                if (hitResult.Object == null)
                {
                    int i = hitResult.PointIndex;
                }
                else
                {
                    ChartArea chartArea = hitResult.Object as ChartArea;

                    if (chartArea != null && chartArea.AxisX != null)
                    {
                        t = chartArea.AxisX.PixelPositionToValue(x);
                    }
                }
                if (t < 1.0)
                {
                    hitResult = AChart.HitTest(x, y, Chart​Element​Type.Axis);

                       Axis chartAxis = hitResult.Object as Axis;

                        if (chartAxis != null)
                        {
                            t = chartAxis.PixelPositionToValue(x);
                        }
                }
                /*                HitTestResult hitResult = AChart.HitTest(x, y, true);       // Axis
                                if (hitResult != null)
                                {
                                    if (hitResult.Object == null)
                                    {
                                        int i = hitResult.PointIndex;
                                    }
                                    else
                                    {
                                        DataPoint dataPoint = hitResult.Object as DataPoint;

                                        if (dataPoint != null)
                                        {
                                            t = dataPoint.XValue;
                                        }
                                        else
                                        {
                                            ChartArea chartArea = hitResult.Object as ChartArea;

                                            if (chartArea != null && chartArea.AxisX != null)
                                            {
                                                t = chartArea.AxisX.PixelPositionToValue(x);
                                            }
                                        }
                                    }
                                }
                                if (t < 1.0)

                                  {
                                    hitResult = AChart.HitTest(x, y, Chart​Element​Type.PlottingArea);

                                    if (hitResult.Object == null)
                                    {
                                        int i = hitResult.PointIndex;
                                    }
                                    else
                                    {
                                        DataPoint dataPoint = hitResult.Object as DataPoint;

                                        if (dataPoint != null)
                                        {
                                            t = dataPoint.XValue;
                                        }
                                        else
                                        {
                                            ChartArea chartArea = hitResult.Object as ChartArea;

                                            if (chartArea != null && chartArea.AxisX != null)
                                            {
                                                t = chartArea.AxisX.PixelPositionToValue(x);
                                            }
                                        }
                                    }
                                }
                */
                if (t > 1.0)
                {
                    DateTime dt = DateTime.FromOADate(t);

                    if (dt >= _mReport._mStartTimeDT && dt <= _mReport._mEndTimeDT)
                    {
                        bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;
                        bool bAlt = (Control.ModifierKeys & Keys.Alt) == Keys.Alt;
                        bool bShift = (Control.ModifierKeys & Keys.Shift) == Keys.Shift;

                        if (bCtrl)
                        {
                            // set from time
                            _mTimeFrameFrom = dt;
                            if (_mTimeFrameFrom > _mTimeFrameTo)
                            {
                                dt = _mTimeFrameFrom;
                                _mTimeFrameFrom = _mTimeFrameTo;
                                _mTimeFrameTo = dt;
                            }
                            mUpdateTimeFrame();
                            mUpdateFoundRec(_mTimeFrameFrom, AClassification, AbManual);
                        }
                        else if (bAlt)
                        {
                            // set to time
                            _mTimeFrameTo = dt;

                            if (_mTimeFrameFrom > _mTimeFrameTo)
                            {
                                dt = _mTimeFrameFrom;
                                _mTimeFrameFrom = _mTimeFrameTo;
                                _mTimeFrameTo = dt;
                            }
                            mUpdateTimeFrame();
                            mUpdateFoundRec(_mTimeFrameTo, AClassification, AbManual);
                        }
                        else if (bShift) 
                        {
                            // set quiry time
                            double min = (dt - _mTimeQuery ).TotalMinutes;
                            if (min < 0)
                            {
                                // before start =>start is clicked position
                                _mTimeQuery = dt;
                                min = -min;
                            }

                            int i = (int)(min + 0.99);
                            if (i == 0) ++i;
                            if (i < 32000)  // 22 dagen
                            {
                                _mQueryLengthMin = (UInt16)(i);

                                mUpdateQueryTime();
                                mUpdateFoundRec(dt, AClassification, AbManual);
                            }
                        }
                        else
                        {
                            // set quiry time
                            _mTimeQuery = dt;
                            mUpdateQueryTime();
                            mUpdateFoundRec(_mTimeQuery, AClassification, AbManual);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                CProgram.sLogException("Hit test failed for device " + _mDeviceID, ex);
            }
        }

        private void chartHR_MouseDown(object sender, MouseEventArgs e)
        {
            mChartMouseDown(e, chartHR, "", false);
        }

        private void chartTachy_MouseDown(object sender, MouseEventArgs e)
        {
            mChartMouseDown(e, chartTachy, CDvtmsData._cFindings_Tachy, false);
        }

        private void chartBrady_MouseDown(object sender, MouseEventArgs e)
        {
            mChartMouseDown(e, chartBrady, CDvtmsData._cFindings_Brady, false);
        }

        private void chartPause_MouseDown(object sender, MouseEventArgs e)
        {
            mChartMouseDown(e, chartPause, CDvtmsData._cFindings_Pause, false);
        }

        private void chartAfib_MouseDown(object sender, MouseEventArgs e)
        {
            mChartMouseDown(e, chartAfib, CDvtmsData._cFindings_AF, false);
        }

        private void chartManual_MouseDown(object sender, MouseEventArgs e)
        {
            mChartMouseDown(e, chartManual, "", true);
        }

        private void chartPace_MouseDown(object sender, MouseEventArgs e)
        {
            mChartMouseDown(e, chartPace, CDvtmsData._cFindings_Pace, false);
        }

        private void label76_Click(object sender, EventArgs e)
        {

        }

        private void label75_Click(object sender, EventArgs e)
        {

        }

        private void label63_Click(object sender, EventArgs e)
        {

        }

        private void chartAllEvents_MouseDown(object sender, MouseEventArgs e)
        {
            mChartMouseDown(e, chartAllEvents, "", false);
        }

/*        private void labelFoundRec_Click(object sender, EventArgs e)
        {
            if( _mShowRecord != null )
            {
                bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;
                bool bAlt = (Control.ModifierKeys & Keys.Alt) == Keys.Alt;

                UInt16 indexInStudy = _mShowRecord.mSeqNrInStudy;

                if (bCtrl)
                {
                    if (indexInStudy >= 1) --indexInStudy;
                }
                else if (bAlt)
                {
                }
                else
                {
                    indexInStudy++;
                }
                _mShowRecord = mFindRecordInStudy(indexInStudy);
                _mShowAnalysis = null;
                if (_mShowRecord != null)
                {
                    _mShowAnalysis = mFindAnalysisRecord(_mShowRecord.mIndex_KEY);
                }
                mUpdateShowRec();
            }
        }
        */
        private void checkBoxLogCalc_CheckedChanged(object sender, EventArgs e)
        {
            mUpdateStats();
            mSaveLogStats();
        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void labelRecAnalysis_Click(object sender, EventArgs e)
        {

        }

        private void CMCTTrendDisplay_Shown(object sender, EventArgs e)
        {
            mFillForm();
        }

        private void radioButton6hours_CheckedChanged(object sender, EventArgs e)
        {
            mChangeRangeTime();
        }

        private void radioButton12hours_CheckedChanged(object sender, EventArgs e)
        {
            mChangeRangeTime();
        }

        private void chartPAC_MouseDown(object sender, MouseEventArgs e)
        {
            mChartMouseDown(e, chartPAC, CDvtmsData._cFindings_PAC, false);
        }

        private void chartPVC_Click(object sender, EventArgs e)
        {

        }

        private void chartPVC_MouseDown(object sender, MouseEventArgs e)
        {
            mChartMouseDown(e, chartPVC, CDvtmsData._cFindings_PVC, false);
        }

        private void label29_Click(object sender, EventArgs e)
        {
            mDoPrint();
        }

        private void checkBoxUseTrend_CheckStateChanged(object sender, EventArgs e)
        {
            _mbUseTrend = checkBoxUseTrend.Checked;
            mCalValidated(_mbLogError, false, _mbValidateTimeLineTo);
            mUpdateStats();
        }

        private void checkBoxUseAnalysis_CheckedChanged(object sender, EventArgs e)
        {
            _mbUseAnalysis = checkBoxUseAnalysis.Checked;
            mCalValidated(_mbLogError, false, _mbValidateTimeLineTo);
            mUpdateStats();
        }

        private void checkBoxPulseOffOnly_CheckedChanged(object sender, EventArgs e)
        {
            mCalValidated(_mbLogError, false, _mbValidateTimeLineTo);
            mUpdateStats();
        }

        private void labelStudy_DoubleClick(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl && _mStudyIndex > 0) //&& CLicKeyDev.sbDeviceIsProgrammer())
            {
                CDvtmsData.sOpenStudyDir( _mStudyIndex, CDvtmsData._cStudyFolder_Recorder);
            }

        }

        private void labelFDL_Click(object sender, EventArgs e)
        {
            if( CProgram.sbReqBool( "MCT Debug", "Full Debug Log", ref _mbFullDebug ))
            {
                mCalValidated(_mbLogError, false, _mbValidateTimeLineTo);
                mUpdateStats();

                if( false == CLicKeyDev.sbDeviceIsProgrammer())
                {
                    _mbFullDebug = false;
                }
            }

        }

        private void labelNearSec_Click(object sender, EventArgs e)
        {
            if (CProgram.sbReqFloat("MCT Debug", "Near strip", ref _mNearStripSec, "0.0", "Sec", 0.0F, 99999.9F))
            {
                mCalValidated(_mbLogError, false, _mbValidateTimeLineTo);
                mUpdateStats();

                if (false == CLicKeyDev.sbDeviceIsProgrammer())
                {
                    _mbFullDebug = false;
                }
            }
        }

        private void checkBoxCountAnOnly_CheckStateChanged(object sender, EventArgs e)
        {
            mCalValidated(_mbLogError, false, _mbValidateTimeLineTo);
            mUpdateStats();
        }

        private void chartAflut_MouseDown(object sender, MouseEventArgs e)
        {
            mChartMouseDown(e, chartAflut, CDvtmsData._cFindings_AFL, false);
        }

        private void chartVtach_MouseDown(object sender, MouseEventArgs e)
        {
            mChartMouseDown(e, chartVtach, CDvtmsData._cFindings_VT, false);

        }

        private void chartOther_MouseDown(object sender, MouseEventArgs e)
        {
            mChartMouseDown(e, chartOther, CDvtmsData._cFindings_Other, false);

        }

        private void buttonxh00_Click(object sender, EventArgs e)
        {
            _mTimeFrameFrom = new DateTime(_mTimeFrameFrom.Year, _mTimeFrameFrom.Month, _mTimeFrameFrom.Day, _mTimeFrameFrom.Hour, 0, 0);
            _mTimeFrameTo = new DateTime(_mTimeFrameTo.Year, _mTimeFrameTo.Month, _mTimeFrameTo.Day, _mTimeFrameTo.Hour, 0, 0);
            mUpdateTimeFrame();

        }

        private void buttonQuery1h_Click(object sender, EventArgs e)
        {
           _mQueryLengthMin = 60;
            mUpdateQueryTime();

        }

        private void buttonQueryh00_Click(object sender, EventArgs e)
        {
            mReadQuiryStartTime();

            _mTimeQuery = new DateTime(_mTimeQuery.Year, _mTimeQuery.Month, _mTimeQuery.Day, _mTimeQuery.Hour, 0, 0);
            mUpdateQueryTime();
        }

        private void buttonQueryTrend_Click(object sender, EventArgs e)
        {
            if (mReadQuiryStartTime() > 0)
            {
                mDoQueryTrend();
            }

        }

        private void checkBoxAnonymize_CheckedChanged(object sender, EventArgs e)
        {
            mInitPatient();
        }

        private void checkBoxTogleAfOnly_CheckedChanged(object sender, EventArgs e)
        {
            mCalValidated(_mbLogError, false, _mbValidateTimeLineTo);
            mUpdateStats();

        }

        private void label2_Click(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl)
            {
                labelFoundRec.Visible = !labelFoundRec.Visible;
            }
        }

        private void labelMouseInfo_Click(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl)
            {
                labelFoundRec.Visible = !labelFoundRec.Visible;
            }
        }
        private void panel10_DoubleClick(object sender, EventArgs e)
        {

         }

        private void labelRecAnalysis_DoubleClick(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl) //&& CLicKeyDev.sbDeviceIsProgrammer())
            {
                if (CProgram.sbAskOkCancel("Dump Validation S" + _mStudyIndex.ToString() + " lines", "dump to clipboard?"))
                {
                    Clipboard.SetText(_mUpdValidateLog);
                }
            }
        }

        private void panel27_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel10_Paint_1(object sender, PaintEventArgs e)
        {

        }

        private void panel59_Paint(object sender, PaintEventArgs e)
        {

        }

        private void chartBrady_Click(object sender, EventArgs e)
        {

        }

        private void chartAbN_MouseDown(object sender, MouseEventArgs e)
        {
            mChartMouseDown(e, chartAbN, CDvtmsData._cFindings_AbNormal, false);

        }
    }
}
