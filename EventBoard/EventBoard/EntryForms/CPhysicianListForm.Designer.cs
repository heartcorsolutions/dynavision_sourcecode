﻿namespace EventBoard.EntryForms
{
    partial class CPhysicianListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel16 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.panel14 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.panel12 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.dataGridViewClientList = new System.Windows.Forms.DataGridView();
            this.Column1ClientName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2ClientStreet = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3ClientZip = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4ClientHouseNr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5ClientState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6ClientCity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7ClientCountry = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8ClientCentralNr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9ClientDirectNr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10ClientFax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11ClientEmail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel4.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewClientList)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(952, 19);
            this.panel1.TabIndex = 1;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.panel4.Controls.Add(this.panel16);
            this.panel4.Controls.Add(this.panel14);
            this.panel4.Controls.Add(this.panel12);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(0, 635);
            this.panel4.Margin = new System.Windows.Forms.Padding(5);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(952, 55);
            this.panel4.TabIndex = 6;
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.button3);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel16.Location = new System.Drawing.Point(553, 0);
            this.panel16.Margin = new System.Windows.Forms.Padding(5);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(133, 55);
            this.panel16.TabIndex = 8;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button3.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.button3.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button3.Location = new System.Drawing.Point(0, 0);
            this.button3.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(133, 55);
            this.button3.TabIndex = 3;
            this.button3.Text = "Edit";
            this.button3.UseVisualStyleBackColor = false;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.button2);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel14.Location = new System.Drawing.Point(686, 0);
            this.panel14.Margin = new System.Windows.Forms.Padding(5);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(133, 55);
            this.panel14.TabIndex = 6;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button2.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.button2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button2.Location = new System.Drawing.Point(0, 0);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(133, 55);
            this.button2.TabIndex = 3;
            this.button2.Text = "Delete";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.button1);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel12.Location = new System.Drawing.Point(819, 0);
            this.panel12.Margin = new System.Windows.Forms.Padding(5);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(133, 55);
            this.panel12.TabIndex = 4;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button1.Location = new System.Drawing.Point(0, 0);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(133, 55);
            this.button1.TabIndex = 3;
            this.button1.Text = "Close";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 625);
            this.panel3.Margin = new System.Windows.Forms.Padding(5);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(952, 10);
            this.panel3.TabIndex = 5;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel10);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 19);
            this.panel2.Margin = new System.Windows.Forms.Padding(5);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(952, 606);
            this.panel2.TabIndex = 4;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.dataGridViewClientList);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Margin = new System.Windows.Forms.Padding(5);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(952, 606);
            this.panel10.TabIndex = 3;
            // 
            // dataGridViewClientList
            // 
            this.dataGridViewClientList.AllowUserToOrderColumns = true;
            this.dataGridViewClientList.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.dataGridViewClientList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewClientList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewClientList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1ClientName,
            this.Column2ClientStreet,
            this.Column3ClientZip,
            this.Column4ClientHouseNr,
            this.Column5ClientState,
            this.Column6ClientCity,
            this.Column7ClientCountry,
            this.Column8ClientCentralNr,
            this.Column9ClientDirectNr,
            this.Column10ClientFax,
            this.Column11ClientEmail});
            this.dataGridViewClientList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewClientList.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewClientList.Margin = new System.Windows.Forms.Padding(5);
            this.dataGridViewClientList.Name = "dataGridViewClientList";
            this.dataGridViewClientList.RowTemplate.Height = 33;
            this.dataGridViewClientList.Size = new System.Drawing.Size(952, 606);
            this.dataGridViewClientList.TabIndex = 33;
            // 
            // Column1ClientName
            // 
            this.Column1ClientName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column1ClientName.HeaderText = "Name";
            this.Column1ClientName.MaxInputLength = 30;
            this.Column1ClientName.Name = "Column1ClientName";
            this.Column1ClientName.Width = 70;
            // 
            // Column2ClientStreet
            // 
            this.Column2ClientStreet.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column2ClientStreet.HeaderText = "Street";
            this.Column2ClientStreet.MaxInputLength = 30;
            this.Column2ClientStreet.Name = "Column2ClientStreet";
            this.Column2ClientStreet.Width = 71;
            // 
            // Column3ClientZip
            // 
            this.Column3ClientZip.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column3ClientZip.HeaderText = "Zip";
            this.Column3ClientZip.MaxInputLength = 10;
            this.Column3ClientZip.Name = "Column3ClientZip";
            this.Column3ClientZip.Width = 53;
            // 
            // Column4ClientHouseNr
            // 
            this.Column4ClientHouseNr.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column4ClientHouseNr.HeaderText = "Nr";
            this.Column4ClientHouseNr.MaxInputLength = 8;
            this.Column4ClientHouseNr.Name = "Column4ClientHouseNr";
            this.Column4ClientHouseNr.Width = 48;
            // 
            // Column5ClientState
            // 
            this.Column5ClientState.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column5ClientState.HeaderText = "State";
            this.Column5ClientState.MaxInputLength = 30;
            this.Column5ClientState.Name = "Column5ClientState";
            this.Column5ClientState.Width = 66;
            // 
            // Column6ClientCity
            // 
            this.Column6ClientCity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column6ClientCity.HeaderText = "City";
            this.Column6ClientCity.MaxInputLength = 30;
            this.Column6ClientCity.Name = "Column6ClientCity";
            this.Column6ClientCity.Width = 56;
            // 
            // Column7ClientCountry
            // 
            this.Column7ClientCountry.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column7ClientCountry.HeaderText = "Country";
            this.Column7ClientCountry.MaxInputLength = 32;
            this.Column7ClientCountry.Name = "Column7ClientCountry";
            this.Column7ClientCountry.Width = 82;
            // 
            // Column8ClientCentralNr
            // 
            this.Column8ClientCentralNr.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column8ClientCentralNr.HeaderText = "Central Nr";
            this.Column8ClientCentralNr.MaxInputLength = 15;
            this.Column8ClientCentralNr.Name = "Column8ClientCentralNr";
            this.Column8ClientCentralNr.Width = 97;
            // 
            // Column9ClientDirectNr
            // 
            this.Column9ClientDirectNr.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column9ClientDirectNr.HeaderText = "Direct Nr";
            this.Column9ClientDirectNr.MaxInputLength = 15;
            this.Column9ClientDirectNr.Name = "Column9ClientDirectNr";
            this.Column9ClientDirectNr.Width = 89;
            // 
            // Column10ClientFax
            // 
            this.Column10ClientFax.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column10ClientFax.HeaderText = "Fax";
            this.Column10ClientFax.MaxInputLength = 15;
            this.Column10ClientFax.Name = "Column10ClientFax";
            this.Column10ClientFax.Width = 55;
            // 
            // Column11ClientEmail
            // 
            this.Column11ClientEmail.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column11ClientEmail.HeaderText = "Email";
            this.Column11ClientEmail.MaxInputLength = 25;
            this.Column11ClientEmail.Name = "Column11ClientEmail";
            this.Column11ClientEmail.Width = 67;
            // 
            // CPhysicianListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(952, 690);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "CPhysicianListForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Physician List";
            this.panel4.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewClientList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.DataGridView dataGridViewClientList;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1ClientName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2ClientStreet;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3ClientZip;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4ClientHouseNr;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5ClientState;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6ClientCity;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7ClientCountry;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8ClientCentralNr;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9ClientDirectNr;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10ClientFax;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11ClientEmail;
    }
}