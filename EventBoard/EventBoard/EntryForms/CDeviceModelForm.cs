﻿using Event_Base;
using Program_Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventboardEntryForms
{
    public partial class CDeviceModelForm : Form
    {
        CDeviceModel _mInfo = null;

        public CDeviceModelForm(CDeviceModel ADeviceModel)
        {
            try
            {
                CDvtmsData.sbInitData();

                CSqlDBaseConnection dbConn = Event_Base.CDvtmsData.sGetDBaseConnection();

                _mInfo = new CDeviceModel(dbConn);

                if (_mInfo == null )
                {
                    CProgram.sLogLine(" CDeviceModelForm() failed to init Client data");
                    Close();
                }
                else
                {
                    _mInfo.mbActive = true;

                    _mInfo.mbCopyFrom(ADeviceModel);

                    InitializeComponent();
//                    mLoadList();
                    mSetValues();
                    mUpdateButtons();
                    mbCheckForm(ADeviceModel == null ? "New" : "Edit");

                    Text = CProgram.sMakeProgTitle("Device Model", false, true);

                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException(" CDeviceModelForm() failed to init", ex);
            }
        }
        public void mUpdateButtons()
        {
            if( _mInfo != null )
            {
                bool bNew = _mInfo.mIndex_KEY == 0;
                labelIndex.Text = bNew ? "" : _mInfo.mIndex_KEY.ToString();
                buttonAddUpdate.Text = bNew ? "Add" : "Update";
                textBoxLabel.ReadOnly = false == bNew;
//                textBoxManufacturer.ReadOnly = false == bNew;

            }
        }

        public void mSetValues()
        {

            textBoxLabel.Text = _mInfo._mModelLabel;
            textBoxManufacturer.Text = _mInfo._mManufacturer;
            textBoxDeviceType.Text = _mInfo._mModelType;
            textBoxDeviceColor.Text = _mInfo._mDeviceColor;
            textBoxServiceInterval.Text = _mInfo._mServiceIntervalDays.ToString();
            textBoxManufacturerAddress.Text = _mInfo._mManufacturerAddress;
            textBoxManufacturerZip.Text = _mInfo._mManufacturerZipCode;
            textBoxManufacturerStreetNr.Text = _mInfo._mManufacturerStreetNr;
            comboManufacturerStateList.Text = _mInfo._mManufacturerState;
            textBoxManufacturerCity.Text = _mInfo._mManufacturerCity;

            if (CDvtmsData._sEnumListCountries != null)
            {
                CDvtmsData._sEnumListCountries.mFillComboBox(comboBoxCountryList, false, 0, false, "", _mInfo._mManufacturerCountryCode, "-- Select--");
            }

            comboBoxCountryList.Text = _mInfo._mManufacturerCity;
            textBoxManufacturerPhone1.Text = _mInfo._mManufacturerPhone1;
            textBoxManufacturerPhone2.Text = _mInfo._mManufacturerPhone2;
            textBoxManufacturerCell.Text = _mInfo._mManufacturerCell;
            textBoxManufacturerSkype.Text = _mInfo._mManufacturerSkype;
            textBoxManufacturerEmail.Text = _mInfo._mManufacturerEmail;

        }

        public void mGetValues()
        {
            _mInfo._mModelLabel = textBoxLabel.Text;
            _mInfo._mManufacturer = textBoxManufacturer.Text;
            _mInfo._mModelType = textBoxDeviceType.Text;
             _mInfo._mDeviceColor = textBoxDeviceColor.Text;
            UInt16.TryParse(textBoxServiceInterval.Text, out _mInfo._mServiceIntervalDays);
            _mInfo._mManufacturerAddress = textBoxManufacturerAddress.Text;
            _mInfo._mManufacturerZipCode = textBoxManufacturerZip.Text;
            _mInfo._mManufacturerStreetNr = textBoxManufacturerStreetNr.Text;
            _mInfo._mManufacturerState = comboManufacturerStateList.Text;
            _mInfo._mManufacturerCity = textBoxManufacturerCity.Text;
            if (CDvtmsData._sEnumListCountries != null)
            {
                _mInfo._mManufacturerCountryCode = CDvtmsData._sEnumListCountries.mReadComboBoxCode(comboBoxCountryList, false, "");
            }

            _mInfo._mManufacturerPhone1 = textBoxManufacturerPhone1.Text;
            _mInfo._mManufacturerPhone2 = textBoxManufacturerPhone2.Text;
            _mInfo._mManufacturerCell = textBoxManufacturerCell.Text;
            _mInfo._mManufacturerSkype = textBoxManufacturerSkype.Text;
            _mInfo._mManufacturerEmail = textBoxManufacturerEmail.Text;
        }
    
        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label39_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            if (_mInfo != null)
            {
                _mInfo.mClear();
                _mInfo.mbActive = true;
                mSetValues();
                mUpdateButtons();
                mbCheckForm("Cleared");
            }
        }

        bool mbCheckModel(out string AErrorText)
        {
            string errorText = "init form failed";

            if (_mInfo != null)
            {
                errorText = "";

                if (_mInfo._mModelLabel == null || _mInfo._mModelLabel.Length == 0)
                {
                    errorText = " No Name";
                }
                if (_mInfo._mManufacturer == null || _mInfo._mManufacturer.Length == 0)
                {
                    errorText += " No Manufacturer";
                }

            }
            AErrorText = errorText;

            return errorText.Length == 0;
        }



        private bool mbCheckForm(string AState)
        {
            bool bOk = true;
            string errorText = "";

            if (mbCheckModel(out errorText))
            {
                labelError.Text = "";
            }
            else
            {
                labelError.Text = AState + ": " + errorText;
                bOk = false;
            }
            return bOk;
        }
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonAddUpdate_Click(object sender, EventArgs e)
        {
            mGetValues();

            // check values
            if (false == mbCheckForm(_mInfo.mIndex_KEY == 0 ? "Add" : "Update"))
            {
                return;
            }


            if (_mInfo.mIndex_KEY == 0)
            {
                // new Client

                // check if Laber is not in list

                if (_mInfo.mbDoSqlInsert())
                {
                    labelIndex.Text = _mInfo.mIndex_KEY.ToString();
                    labelError.Text = "Inserted";
                }
                else
                {
                    labelError.Text = "Failed Insert";
                }
            }
            else if (_mInfo.mIndex_KEY > 0)
            {
                // update client

                // check if label is in list (not this)
                if (_mInfo.mbDoSqlUpdate(_mInfo.mMaskValid, _mInfo.mbActive))
                {
                    labelIndex.Text = "+" + _mInfo.mIndex_KEY.ToString();
                    labelError.Text = "Updated";
                }
                else
                {
                    labelError.Text = "Failed Update";
                }
            }
            mUpdateButtons();
        }
    }
}
