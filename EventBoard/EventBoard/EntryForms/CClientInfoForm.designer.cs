﻿namespace EventboardEntryForms
{
    partial class CClientInfoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CClientInfoForm));
            this.labelClientDetails = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.textBoxDepartmentPhone = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxDepartmentHead = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxDepartmentName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.buttonRandom = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.checkBoxActive = new System.Windows.Forms.CheckBox();
            this.textBoxClientNr = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxClientLabel = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxHouseNumber = new System.Windows.Forms.TextBox();
            this.labelPatientHouseNumber = new System.Windows.Forms.Label();
            this.textBoxClientEmail = new System.Windows.Forms.TextBox();
            this.labelClientEmail = new System.Windows.Forms.Label();
            this.textBoxClientFullName = new System.Windows.Forms.TextBox();
            this.labelClientName = new System.Windows.Forms.Label();
            this.comboBoxClientCountryList = new System.Windows.Forms.ComboBox();
            this.textBoxClientFax = new System.Windows.Forms.TextBox();
            this.labelClientFax = new System.Windows.Forms.Label();
            this.textBoxClientDirectNr = new System.Windows.Forms.TextBox();
            this.labelClientDirectNr = new System.Windows.Forms.Label();
            this.textBoxClientAddress = new System.Windows.Forms.TextBox();
            this.labelClientAddress = new System.Windows.Forms.Label();
            this.comboClientStateList = new System.Windows.Forms.ComboBox();
            this.textBoxClientCentralNr = new System.Windows.Forms.TextBox();
            this.labelClientCentralNr = new System.Windows.Forms.Label();
            this.labelClientCountry = new System.Windows.Forms.Label();
            this.textBoxClientCity = new System.Windows.Forms.TextBox();
            this.labelClientCity = new System.Windows.Forms.Label();
            this.labelClientState = new System.Windows.Forms.Label();
            this.textBoxClientZip = new System.Windows.Forms.TextBox();
            this.labelClientZip = new System.Windows.Forms.Label();
            this.dataGridViewList = new System.Windows.Forms.DataGridView();
            this.ClientNr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnActive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1ClientName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnFullName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6ClientCity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7ClientCountry = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnDepartment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnHeadDept = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnPhoneHeadDept = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2ClientStreet = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3ClientZip = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4ClientHouseNr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5ClientState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8ClientCentralNr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9ClientDirectNr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10ClientFax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11ClientEmail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.labelClientError = new System.Windows.Forms.Label();
            this.panel38 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.buttonAddUpdate = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.buttonDup = new System.Windows.Forms.Button();
            this.buttonClear = new System.Windows.Forms.Button();
            this.panel21 = new System.Windows.Forms.Panel();
            this.panel25 = new System.Windows.Forms.Panel();
            this.panel29 = new System.Windows.Forms.Panel();
            this.panel26 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelSelected = new System.Windows.Forms.Label();
            this.buttonSelect = new System.Windows.Forms.Button();
            this.labelListN = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelResultN = new System.Windows.Forms.Label();
            this.checkBoxShowSearchResult = new System.Windows.Forms.CheckBox();
            this.checkBoxShowDeactivated = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonDuplicate = new System.Windows.Forms.Button();
            this.buttonEdit = new System.Windows.Forms.Button();
            this.buttonDisable = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewList)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel29.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelClientDetails
            // 
            this.labelClientDetails.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelClientDetails.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelClientDetails.Location = new System.Drawing.Point(0, 0);
            this.labelClientDetails.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelClientDetails.Name = "labelClientDetails";
            this.labelClientDetails.Size = new System.Drawing.Size(120, 20);
            this.labelClientDetails.TabIndex = 72;
            this.labelClientDetails.Text = "Client details";
            this.labelClientDetails.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel5
            // 
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Margin = new System.Windows.Forms.Padding(4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1025, 10);
            this.panel5.TabIndex = 71;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panel7.Controls.Add(this.textBoxDepartmentPhone);
            this.panel7.Controls.Add(this.label10);
            this.panel7.Controls.Add(this.textBoxDepartmentHead);
            this.panel7.Controls.Add(this.label9);
            this.panel7.Controls.Add(this.textBoxDepartmentName);
            this.panel7.Controls.Add(this.label5);
            this.panel7.Controls.Add(this.buttonRandom);
            this.panel7.Controls.Add(this.label7);
            this.panel7.Controls.Add(this.checkBoxActive);
            this.panel7.Controls.Add(this.textBoxClientNr);
            this.panel7.Controls.Add(this.label3);
            this.panel7.Controls.Add(this.textBoxClientLabel);
            this.panel7.Controls.Add(this.label1);
            this.panel7.Controls.Add(this.textBoxHouseNumber);
            this.panel7.Controls.Add(this.labelPatientHouseNumber);
            this.panel7.Controls.Add(this.textBoxClientEmail);
            this.panel7.Controls.Add(this.labelClientEmail);
            this.panel7.Controls.Add(this.textBoxClientFullName);
            this.panel7.Controls.Add(this.labelClientName);
            this.panel7.Controls.Add(this.comboBoxClientCountryList);
            this.panel7.Controls.Add(this.textBoxClientFax);
            this.panel7.Controls.Add(this.labelClientFax);
            this.panel7.Controls.Add(this.textBoxClientDirectNr);
            this.panel7.Controls.Add(this.labelClientDirectNr);
            this.panel7.Controls.Add(this.textBoxClientAddress);
            this.panel7.Controls.Add(this.labelClientAddress);
            this.panel7.Controls.Add(this.comboClientStateList);
            this.panel7.Controls.Add(this.textBoxClientCentralNr);
            this.panel7.Controls.Add(this.labelClientCentralNr);
            this.panel7.Controls.Add(this.labelClientCountry);
            this.panel7.Controls.Add(this.textBoxClientCity);
            this.panel7.Controls.Add(this.labelClientCity);
            this.panel7.Controls.Add(this.labelClientState);
            this.panel7.Controls.Add(this.textBoxClientZip);
            this.panel7.Controls.Add(this.labelClientZip);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Font = new System.Drawing.Font("Arial", 10F);
            this.panel7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Margin = new System.Windows.Forms.Padding(4);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1025, 181);
            this.panel7.TabIndex = 128;
            // 
            // textBoxDepartmentPhone
            // 
            this.textBoxDepartmentPhone.Location = new System.Drawing.Point(844, 110);
            this.textBoxDepartmentPhone.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxDepartmentPhone.MaxLength = 32;
            this.textBoxDepartmentPhone.Name = "textBoxDepartmentPhone";
            this.textBoxDepartmentPhone.Size = new System.Drawing.Size(161, 23);
            this.textBoxDepartmentPhone.TabIndex = 79;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(753, 113);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(87, 16);
            this.label10.TabIndex = 78;
            this.label10.Text = "Dept Phone:";
            // 
            // textBoxDepartmentHead
            // 
            this.textBoxDepartmentHead.Location = new System.Drawing.Point(844, 80);
            this.textBoxDepartmentHead.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxDepartmentHead.MaxLength = 64;
            this.textBoxDepartmentHead.Name = "textBoxDepartmentHead";
            this.textBoxDepartmentHead.Size = new System.Drawing.Size(161, 23);
            this.textBoxDepartmentHead.TabIndex = 77;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(753, 83);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 16);
            this.label9.TabIndex = 76;
            this.label9.Text = "Dept Head:";
            // 
            // textBoxDepartmentName
            // 
            this.textBoxDepartmentName.Location = new System.Drawing.Point(844, 50);
            this.textBoxDepartmentName.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxDepartmentName.MaxLength = 64;
            this.textBoxDepartmentName.Name = "textBoxDepartmentName";
            this.textBoxDepartmentName.Size = new System.Drawing.Size(161, 23);
            this.textBoxDepartmentName.TabIndex = 75;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(753, 53);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 16);
            this.label5.TabIndex = 74;
            this.label5.Text = "Dept:";
            // 
            // buttonRandom
            // 
            this.buttonRandom.Location = new System.Drawing.Point(886, 10);
            this.buttonRandom.Name = "buttonRandom";
            this.buttonRandom.Size = new System.Drawing.Size(21, 25);
            this.buttonRandom.TabIndex = 0;
            this.buttonRandom.Text = "R";
            this.buttonRandom.UseVisualStyleBackColor = true;
            this.buttonRandom.Click += new System.EventHandler(this.buttonRandom_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(211, 12);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 16);
            this.label7.TabIndex = 73;
            this.label7.Text = "max 16";
            // 
            // checkBoxActive
            // 
            this.checkBoxActive.AutoSize = true;
            this.checkBoxActive.Location = new System.Drawing.Point(479, 10);
            this.checkBoxActive.Name = "checkBoxActive";
            this.checkBoxActive.Size = new System.Drawing.Size(65, 20);
            this.checkBoxActive.TabIndex = 68;
            this.checkBoxActive.Text = "Active";
            this.checkBoxActive.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBoxActive.UseVisualStyleBackColor = true;
            // 
            // textBoxClientNr
            // 
            this.textBoxClientNr.Location = new System.Drawing.Point(374, 9);
            this.textBoxClientNr.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxClientNr.Name = "textBoxClientNr";
            this.textBoxClientNr.Size = new System.Drawing.Size(96, 23);
            this.textBoxClientNr.TabIndex = 67;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(284, 12);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 16);
            this.label3.TabIndex = 66;
            this.label3.Text = "Client nr:";
            // 
            // textBoxClientLabel
            // 
            this.textBoxClientLabel.Location = new System.Drawing.Point(94, 9);
            this.textBoxClientLabel.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxClientLabel.MaxLength = 18;
            this.textBoxClientLabel.Name = "textBoxClientLabel";
            this.textBoxClientLabel.Size = new System.Drawing.Size(113, 23);
            this.textBoxClientLabel.TabIndex = 65;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.label1.Location = new System.Drawing.Point(5, 12);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 16);
            this.label1.TabIndex = 64;
            this.label1.Text = "Name: *";
            // 
            // textBoxHouseNumber
            // 
            this.textBoxHouseNumber.Location = new System.Drawing.Point(214, 141);
            this.textBoxHouseNumber.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxHouseNumber.MaxLength = 16;
            this.textBoxHouseNumber.Name = "textBoxHouseNumber";
            this.textBoxHouseNumber.Size = new System.Drawing.Size(51, 23);
            this.textBoxHouseNumber.TabIndex = 63;
            // 
            // labelPatientHouseNumber
            // 
            this.labelPatientHouseNumber.AutoSize = true;
            this.labelPatientHouseNumber.Location = new System.Drawing.Point(150, 144);
            this.labelPatientHouseNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPatientHouseNumber.Name = "labelPatientHouseNumber";
            this.labelPatientHouseNumber.Size = new System.Drawing.Size(61, 16);
            this.labelPatientHouseNumber.TabIndex = 62;
            this.labelPatientHouseNumber.Text = "Apartm.:";
            // 
            // textBoxClientEmail
            // 
            this.textBoxClientEmail.Location = new System.Drawing.Point(631, 141);
            this.textBoxClientEmail.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxClientEmail.MaxLength = 128;
            this.textBoxClientEmail.Name = "textBoxClientEmail";
            this.textBoxClientEmail.Size = new System.Drawing.Size(106, 23);
            this.textBoxClientEmail.TabIndex = 22;
            // 
            // labelClientEmail
            // 
            this.labelClientEmail.AutoSize = true;
            this.labelClientEmail.Location = new System.Drawing.Point(562, 144);
            this.labelClientEmail.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelClientEmail.Name = "labelClientEmail";
            this.labelClientEmail.Size = new System.Drawing.Size(46, 16);
            this.labelClientEmail.TabIndex = 21;
            this.labelClientEmail.Text = "Email:";
            this.labelClientEmail.Click += new System.EventHandler(this.label3_Click);
            // 
            // textBoxClientFullName
            // 
            this.textBoxClientFullName.Location = new System.Drawing.Point(95, 50);
            this.textBoxClientFullName.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxClientFullName.MaxLength = 64;
            this.textBoxClientFullName.Name = "textBoxClientFullName";
            this.textBoxClientFullName.Size = new System.Drawing.Size(170, 23);
            this.textBoxClientFullName.TabIndex = 2;
            this.textBoxClientFullName.TextChanged += new System.EventHandler(this.textBoxClientName_TextChanged);
            // 
            // labelClientName
            // 
            this.labelClientName.AutoSize = true;
            this.labelClientName.Location = new System.Drawing.Point(5, 53);
            this.labelClientName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelClientName.Name = "labelClientName";
            this.labelClientName.Size = new System.Drawing.Size(75, 16);
            this.labelClientName.TabIndex = 1;
            this.labelClientName.Text = "Full Name:";
            // 
            // comboBoxClientCountryList
            // 
            this.comboBoxClientCountryList.DropDownWidth = 200;
            this.comboBoxClientCountryList.FormattingEnabled = true;
            this.comboBoxClientCountryList.Items.AddRange(new object[] {
            "USA",
            "Mexico"});
            this.comboBoxClientCountryList.Location = new System.Drawing.Point(374, 109);
            this.comboBoxClientCountryList.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxClientCountryList.Name = "comboBoxClientCountryList";
            this.comboBoxClientCountryList.Size = new System.Drawing.Size(170, 24);
            this.comboBoxClientCountryList.TabIndex = 14;
            // 
            // textBoxClientFax
            // 
            this.textBoxClientFax.Location = new System.Drawing.Point(631, 110);
            this.textBoxClientFax.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxClientFax.MaxLength = 32;
            this.textBoxClientFax.Name = "textBoxClientFax";
            this.textBoxClientFax.Size = new System.Drawing.Size(106, 23);
            this.textBoxClientFax.TabIndex = 20;
            // 
            // labelClientFax
            // 
            this.labelClientFax.AutoSize = true;
            this.labelClientFax.Location = new System.Drawing.Point(562, 113);
            this.labelClientFax.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelClientFax.Name = "labelClientFax";
            this.labelClientFax.Size = new System.Drawing.Size(35, 16);
            this.labelClientFax.TabIndex = 19;
            this.labelClientFax.Text = "Fax:";
            // 
            // textBoxClientDirectNr
            // 
            this.textBoxClientDirectNr.Location = new System.Drawing.Point(631, 50);
            this.textBoxClientDirectNr.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxClientDirectNr.MaxLength = 32;
            this.textBoxClientDirectNr.Name = "textBoxClientDirectNr";
            this.textBoxClientDirectNr.Size = new System.Drawing.Size(106, 23);
            this.textBoxClientDirectNr.TabIndex = 18;
            // 
            // labelClientDirectNr
            // 
            this.labelClientDirectNr.AutoSize = true;
            this.labelClientDirectNr.Location = new System.Drawing.Point(562, 53);
            this.labelClientDirectNr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelClientDirectNr.Name = "labelClientDirectNr";
            this.labelClientDirectNr.Size = new System.Drawing.Size(49, 16);
            this.labelClientDirectNr.TabIndex = 17;
            this.labelClientDirectNr.Text = "Direct:";
            // 
            // textBoxClientAddress
            // 
            this.textBoxClientAddress.Location = new System.Drawing.Point(95, 83);
            this.textBoxClientAddress.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxClientAddress.MaxLength = 1022;
            this.textBoxClientAddress.Multiline = true;
            this.textBoxClientAddress.Name = "textBoxClientAddress";
            this.textBoxClientAddress.Size = new System.Drawing.Size(170, 50);
            this.textBoxClientAddress.TabIndex = 4;
            this.textBoxClientAddress.TextChanged += new System.EventHandler(this.textBoxManufacturerAddress_TextChanged);
            // 
            // labelClientAddress
            // 
            this.labelClientAddress.AutoSize = true;
            this.labelClientAddress.Location = new System.Drawing.Point(5, 90);
            this.labelClientAddress.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelClientAddress.Name = "labelClientAddress";
            this.labelClientAddress.Size = new System.Drawing.Size(64, 16);
            this.labelClientAddress.TabIndex = 3;
            this.labelClientAddress.Text = "Address:";
            // 
            // comboClientStateList
            // 
            this.comboClientStateList.DropDownWidth = 200;
            this.comboClientStateList.FormattingEnabled = true;
            this.comboClientStateList.Items.AddRange(new object[] {
            "NA",
            "Alabama",
            "Alaska",
            "Arizona",
            "Arkansas",
            "California",
            "Colorado",
            "Connecticut",
            "Delaware",
            "District of Columbia",
            "Florida",
            "Georgia",
            "Hawaii",
            "Idaho",
            "Illinois",
            "Indiana",
            "Iowa",
            "Kansas",
            "Kentucky",
            "Louisiana",
            "Maine",
            "Maryland",
            "Massachusetts",
            "Michigan",
            "Minnesota",
            "Mississippi",
            "Missouri",
            "Montana",
            "Nebraska",
            "Nevada",
            "New Hampshire",
            "New Jersey",
            "New Mexico",
            "New York",
            "North Carolina",
            "North Dakota",
            "Ohio",
            "Oklahoma",
            "Oregon",
            "Pennsylvania",
            "Rhode Island",
            "South Carolina",
            "South Dakota",
            "Tennessee",
            "Texas",
            "Utah",
            "Vermont",
            "Virginia",
            "Washington",
            "West Virginia",
            "Wisconsin"});
            this.comboClientStateList.Location = new System.Drawing.Point(374, 49);
            this.comboClientStateList.Margin = new System.Windows.Forms.Padding(4);
            this.comboClientStateList.Name = "comboClientStateList";
            this.comboClientStateList.Size = new System.Drawing.Size(170, 24);
            this.comboClientStateList.TabIndex = 10;
            // 
            // textBoxClientCentralNr
            // 
            this.textBoxClientCentralNr.Location = new System.Drawing.Point(374, 141);
            this.textBoxClientCentralNr.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxClientCentralNr.MaxLength = 32;
            this.textBoxClientCentralNr.Name = "textBoxClientCentralNr";
            this.textBoxClientCentralNr.Size = new System.Drawing.Size(170, 23);
            this.textBoxClientCentralNr.TabIndex = 16;
            // 
            // labelClientCentralNr
            // 
            this.labelClientCentralNr.AutoSize = true;
            this.labelClientCentralNr.Location = new System.Drawing.Point(284, 144);
            this.labelClientCentralNr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelClientCentralNr.Name = "labelClientCentralNr";
            this.labelClientCentralNr.Size = new System.Drawing.Size(87, 16);
            this.labelClientCentralNr.TabIndex = 15;
            this.labelClientCentralNr.Text = "Main Phone:";
            // 
            // labelClientCountry
            // 
            this.labelClientCountry.AutoSize = true;
            this.labelClientCountry.Location = new System.Drawing.Point(284, 113);
            this.labelClientCountry.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelClientCountry.Name = "labelClientCountry";
            this.labelClientCountry.Size = new System.Drawing.Size(62, 16);
            this.labelClientCountry.TabIndex = 13;
            this.labelClientCountry.Text = "Country:";
            // 
            // textBoxClientCity
            // 
            this.textBoxClientCity.Location = new System.Drawing.Point(374, 80);
            this.textBoxClientCity.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxClientCity.MaxLength = 48;
            this.textBoxClientCity.Name = "textBoxClientCity";
            this.textBoxClientCity.Size = new System.Drawing.Size(170, 23);
            this.textBoxClientCity.TabIndex = 12;
            // 
            // labelClientCity
            // 
            this.labelClientCity.AutoSize = true;
            this.labelClientCity.Location = new System.Drawing.Point(284, 83);
            this.labelClientCity.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelClientCity.Name = "labelClientCity";
            this.labelClientCity.Size = new System.Drawing.Size(36, 16);
            this.labelClientCity.TabIndex = 11;
            this.labelClientCity.Text = "City:";
            // 
            // labelClientState
            // 
            this.labelClientState.AutoSize = true;
            this.labelClientState.Location = new System.Drawing.Point(284, 53);
            this.labelClientState.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelClientState.Name = "labelClientState";
            this.labelClientState.Size = new System.Drawing.Size(45, 16);
            this.labelClientState.TabIndex = 9;
            this.labelClientState.Text = "State:";
            // 
            // textBoxClientZip
            // 
            this.textBoxClientZip.Location = new System.Drawing.Point(95, 141);
            this.textBoxClientZip.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxClientZip.MaxLength = 16;
            this.textBoxClientZip.Name = "textBoxClientZip";
            this.textBoxClientZip.Size = new System.Drawing.Size(51, 23);
            this.textBoxClientZip.TabIndex = 6;
            // 
            // labelClientZip
            // 
            this.labelClientZip.AutoSize = true;
            this.labelClientZip.Location = new System.Drawing.Point(5, 144);
            this.labelClientZip.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelClientZip.Name = "labelClientZip";
            this.labelClientZip.Size = new System.Drawing.Size(31, 16);
            this.labelClientZip.TabIndex = 5;
            this.labelClientZip.Text = "Zip:";
            // 
            // dataGridViewList
            // 
            this.dataGridViewList.AllowDrop = true;
            this.dataGridViewList.AllowUserToAddRows = false;
            this.dataGridViewList.AllowUserToDeleteRows = false;
            this.dataGridViewList.AllowUserToOrderColumns = true;
            this.dataGridViewList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewList.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.dataGridViewList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ClientNr,
            this.ColumnActive,
            this.Column1ClientName,
            this.ColumnFullName,
            this.Column6ClientCity,
            this.Column7ClientCountry,
            this.ColumnDepartment,
            this.ColumnHeadDept,
            this.ColumnPhoneHeadDept,
            this.Column2ClientStreet,
            this.Column3ClientZip,
            this.Column4ClientHouseNr,
            this.Column5ClientState,
            this.Column8ClientCentralNr,
            this.Column9ClientDirectNr,
            this.Column10ClientFax,
            this.Column11ClientEmail});
            this.dataGridViewList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewList.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewList.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridViewList.Name = "dataGridViewList";
            this.dataGridViewList.ReadOnly = true;
            this.dataGridViewList.RowHeadersVisible = false;
            this.dataGridViewList.RowTemplate.Height = 33;
            this.dataGridViewList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewList.ShowEditingIcon = false;
            this.dataGridViewList.Size = new System.Drawing.Size(1025, 273);
            this.dataGridViewList.TabIndex = 31;
            this.dataGridViewList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dataGridViewList.Click += new System.EventHandler(this.dataGridViewList_Click);
            this.dataGridViewList.DoubleClick += new System.EventHandler(this.dataGridClientList_DoubleClick);
            // 
            // ClientNr
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.ClientNr.DefaultCellStyle = dataGridViewCellStyle1;
            this.ClientNr.HeaderText = "Client Nr";
            this.ClientNr.Name = "ClientNr";
            this.ClientNr.ReadOnly = true;
            this.ClientNr.Width = 87;
            // 
            // ColumnActive
            // 
            this.ColumnActive.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ColumnActive.HeaderText = "Active";
            this.ColumnActive.Name = "ColumnActive";
            this.ColumnActive.ReadOnly = true;
            this.ColumnActive.Width = 70;
            // 
            // Column1ClientName
            // 
            this.Column1ClientName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column1ClientName.HeaderText = "Name";
            this.Column1ClientName.MaxInputLength = 30;
            this.Column1ClientName.Name = "Column1ClientName";
            this.Column1ClientName.ReadOnly = true;
            this.Column1ClientName.Width = 70;
            // 
            // ColumnFullName
            // 
            this.ColumnFullName.HeaderText = "Full Name";
            this.ColumnFullName.Name = "ColumnFullName";
            this.ColumnFullName.ReadOnly = true;
            this.ColumnFullName.Width = 96;
            // 
            // Column6ClientCity
            // 
            this.Column6ClientCity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column6ClientCity.HeaderText = "City";
            this.Column6ClientCity.MaxInputLength = 30;
            this.Column6ClientCity.Name = "Column6ClientCity";
            this.Column6ClientCity.ReadOnly = true;
            this.Column6ClientCity.Width = 56;
            // 
            // Column7ClientCountry
            // 
            this.Column7ClientCountry.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column7ClientCountry.HeaderText = "Country";
            this.Column7ClientCountry.MaxInputLength = 32;
            this.Column7ClientCountry.Name = "Column7ClientCountry";
            this.Column7ClientCountry.ReadOnly = true;
            this.Column7ClientCountry.Width = 82;
            // 
            // ColumnDepartment
            // 
            this.ColumnDepartment.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColumnDepartment.HeaderText = "Department";
            this.ColumnDepartment.MaxInputLength = 25;
            this.ColumnDepartment.Name = "ColumnDepartment";
            this.ColumnDepartment.ReadOnly = true;
            this.ColumnDepartment.Width = 107;
            // 
            // ColumnHeadDept
            // 
            this.ColumnHeadDept.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColumnHeadDept.HeaderText = "Dept. Head";
            this.ColumnHeadDept.MaxInputLength = 20;
            this.ColumnHeadDept.Name = "ColumnHeadDept";
            this.ColumnHeadDept.ReadOnly = true;
            this.ColumnHeadDept.Width = 105;
            // 
            // ColumnPhoneHeadDept
            // 
            this.ColumnPhoneHeadDept.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColumnPhoneHeadDept.HeaderText = "Dept. Phone";
            this.ColumnPhoneHeadDept.MaxInputLength = 15;
            this.ColumnPhoneHeadDept.Name = "ColumnPhoneHeadDept";
            this.ColumnPhoneHeadDept.ReadOnly = true;
            this.ColumnPhoneHeadDept.Width = 112;
            // 
            // Column2ClientStreet
            // 
            this.Column2ClientStreet.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column2ClientStreet.HeaderText = "Street";
            this.Column2ClientStreet.MaxInputLength = 30;
            this.Column2ClientStreet.Name = "Column2ClientStreet";
            this.Column2ClientStreet.ReadOnly = true;
            this.Column2ClientStreet.Width = 71;
            // 
            // Column3ClientZip
            // 
            this.Column3ClientZip.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column3ClientZip.HeaderText = "Zip";
            this.Column3ClientZip.MaxInputLength = 10;
            this.Column3ClientZip.Name = "Column3ClientZip";
            this.Column3ClientZip.ReadOnly = true;
            this.Column3ClientZip.Width = 53;
            // 
            // Column4ClientHouseNr
            // 
            this.Column4ClientHouseNr.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column4ClientHouseNr.HeaderText = "Nr";
            this.Column4ClientHouseNr.MaxInputLength = 8;
            this.Column4ClientHouseNr.Name = "Column4ClientHouseNr";
            this.Column4ClientHouseNr.ReadOnly = true;
            this.Column4ClientHouseNr.Width = 48;
            // 
            // Column5ClientState
            // 
            this.Column5ClientState.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column5ClientState.HeaderText = "State";
            this.Column5ClientState.MaxInputLength = 30;
            this.Column5ClientState.Name = "Column5ClientState";
            this.Column5ClientState.ReadOnly = true;
            this.Column5ClientState.Width = 66;
            // 
            // Column8ClientCentralNr
            // 
            this.Column8ClientCentralNr.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column8ClientCentralNr.HeaderText = "Central Nr";
            this.Column8ClientCentralNr.MaxInputLength = 15;
            this.Column8ClientCentralNr.Name = "Column8ClientCentralNr";
            this.Column8ClientCentralNr.ReadOnly = true;
            this.Column8ClientCentralNr.Width = 97;
            // 
            // Column9ClientDirectNr
            // 
            this.Column9ClientDirectNr.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column9ClientDirectNr.HeaderText = "Direct Nr";
            this.Column9ClientDirectNr.MaxInputLength = 15;
            this.Column9ClientDirectNr.Name = "Column9ClientDirectNr";
            this.Column9ClientDirectNr.ReadOnly = true;
            this.Column9ClientDirectNr.Width = 89;
            // 
            // Column10ClientFax
            // 
            this.Column10ClientFax.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column10ClientFax.HeaderText = "Fax";
            this.Column10ClientFax.MaxInputLength = 15;
            this.Column10ClientFax.Name = "Column10ClientFax";
            this.Column10ClientFax.ReadOnly = true;
            this.Column10ClientFax.Width = 55;
            // 
            // Column11ClientEmail
            // 
            this.Column11ClientEmail.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column11ClientEmail.HeaderText = "Email";
            this.Column11ClientEmail.MaxInputLength = 25;
            this.Column11ClientEmail.Name = "Column11ClientEmail";
            this.Column11ClientEmail.ReadOnly = true;
            this.Column11ClientEmail.Width = 67;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel8);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 10);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1025, 20);
            this.panel3.TabIndex = 135;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.labelClientError);
            this.panel8.Controls.Add(this.panel38);
            this.panel8.Controls.Add(this.labelClientDetails);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(1025, 20);
            this.panel8.TabIndex = 75;
            // 
            // labelClientError
            // 
            this.labelClientError.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelClientError.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelClientError.Location = new System.Drawing.Point(190, 0);
            this.labelClientError.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelClientError.Name = "labelClientError";
            this.labelClientError.Size = new System.Drawing.Size(263, 20);
            this.labelClientError.TabIndex = 75;
            this.labelClientError.Text = "...";
            this.labelClientError.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel38
            // 
            this.panel38.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel38.Location = new System.Drawing.Point(120, 0);
            this.panel38.Name = "panel38";
            this.panel38.Size = new System.Drawing.Size(70, 20);
            this.panel38.TabIndex = 73;
            // 
            // panel9
            // 
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 30);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(1025, 15);
            this.panel9.TabIndex = 136;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.panel14);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(0, 45);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(1025, 181);
            this.panel10.TabIndex = 137;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.panel7);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel14.Location = new System.Drawing.Point(0, 0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(1025, 181);
            this.panel14.TabIndex = 2;
            // 
            // panel11
            // 
            this.panel11.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel11.Location = new System.Drawing.Point(0, 226);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(1025, 10);
            this.panel11.TabIndex = 138;
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.panel15.Controls.Add(this.buttonAddUpdate);
            this.panel15.Controls.Add(this.buttonCancel);
            this.panel15.Controls.Add(this.buttonSearch);
            this.panel15.Controls.Add(this.buttonDup);
            this.panel15.Controls.Add(this.buttonClear);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel15.Location = new System.Drawing.Point(0, 236);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(1025, 26);
            this.panel15.TabIndex = 139;
            // 
            // buttonAddUpdate
            // 
            this.buttonAddUpdate.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonAddUpdate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonAddUpdate.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonAddUpdate.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonAddUpdate.FlatAppearance.BorderSize = 0;
            this.buttonAddUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddUpdate.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonAddUpdate.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonAddUpdate.Location = new System.Drawing.Point(829, 0);
            this.buttonAddUpdate.Margin = new System.Windows.Forms.Padding(2);
            this.buttonAddUpdate.Name = "buttonAddUpdate";
            this.buttonAddUpdate.Size = new System.Drawing.Size(123, 26);
            this.buttonAddUpdate.TabIndex = 1;
            this.buttonAddUpdate.Text = "Add";
            this.buttonAddUpdate.UseVisualStyleBackColor = false;
            this.buttonAddUpdate.Click += new System.EventHandler(this.buttonAddUpdate_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonCancel.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonCancel.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonCancel.FlatAppearance.BorderSize = 0;
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCancel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonCancel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonCancel.Location = new System.Drawing.Point(952, 0);
            this.buttonCancel.Margin = new System.Windows.Forms.Padding(2);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(73, 26);
            this.buttonCancel.TabIndex = 1;
            this.buttonCancel.Text = "Close";
            this.buttonCancel.UseVisualStyleBackColor = false;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonSearch
            // 
            this.buttonSearch.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonSearch.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonSearch.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonSearch.FlatAppearance.BorderSize = 0;
            this.buttonSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSearch.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonSearch.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonSearch.Location = new System.Drawing.Point(161, 0);
            this.buttonSearch.Margin = new System.Windows.Forms.Padding(2);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(78, 26);
            this.buttonSearch.TabIndex = 6;
            this.buttonSearch.Text = "Search";
            this.buttonSearch.UseVisualStyleBackColor = false;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // buttonDup
            // 
            this.buttonDup.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonDup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonDup.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonDup.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonDup.FlatAppearance.BorderSize = 0;
            this.buttonDup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDup.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonDup.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonDup.Location = new System.Drawing.Point(65, 0);
            this.buttonDup.Margin = new System.Windows.Forms.Padding(2);
            this.buttonDup.Name = "buttonDup";
            this.buttonDup.Size = new System.Drawing.Size(96, 26);
            this.buttonDup.TabIndex = 5;
            this.buttonDup.Text = "Duplicate";
            this.buttonDup.UseVisualStyleBackColor = false;
            this.buttonDup.Click += new System.EventHandler(this.buttonDup_Click);
            // 
            // buttonClear
            // 
            this.buttonClear.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonClear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonClear.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonClear.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonClear.FlatAppearance.BorderSize = 0;
            this.buttonClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonClear.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonClear.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonClear.Location = new System.Drawing.Point(0, 0);
            this.buttonClear.Margin = new System.Windows.Forms.Padding(2);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(65, 26);
            this.buttonClear.TabIndex = 4;
            this.buttonClear.Text = "Clear";
            this.buttonClear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonClear.UseVisualStyleBackColor = false;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // panel21
            // 
            this.panel21.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel21.Location = new System.Drawing.Point(0, 262);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(1025, 10);
            this.panel21.TabIndex = 141;
            // 
            // panel25
            // 
            this.panel25.Controls.Add(this.panel29);
            this.panel25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel25.Location = new System.Drawing.Point(0, 272);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(1025, 273);
            this.panel25.TabIndex = 142;
            // 
            // panel29
            // 
            this.panel29.Controls.Add(this.dataGridViewList);
            this.panel29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel29.Location = new System.Drawing.Point(0, 0);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(1025, 273);
            this.panel29.TabIndex = 3;
            // 
            // panel26
            // 
            this.panel26.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel26.Location = new System.Drawing.Point(0, 545);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(1025, 18);
            this.panel26.TabIndex = 143;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.panel1.Controls.Add(this.labelSelected);
            this.panel1.Controls.Add(this.buttonSelect);
            this.panel1.Controls.Add(this.labelListN);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.labelResultN);
            this.panel1.Controls.Add(this.checkBoxShowSearchResult);
            this.panel1.Controls.Add(this.checkBoxShowDeactivated);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.buttonDuplicate);
            this.panel1.Controls.Add(this.buttonEdit);
            this.panel1.Controls.Add(this.buttonDisable);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 563);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1025, 26);
            this.panel1.TabIndex = 144;
            // 
            // labelSelected
            // 
            this.labelSelected.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSelected.Font = new System.Drawing.Font("Arial", 10F);
            this.labelSelected.ForeColor = System.Drawing.Color.White;
            this.labelSelected.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.labelSelected.Location = new System.Drawing.Point(488, 0);
            this.labelSelected.Name = "labelSelected";
            this.labelSelected.Size = new System.Drawing.Size(56, 26);
            this.labelSelected.TabIndex = 16;
            this.labelSelected.Text = "0";
            this.labelSelected.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelSelected.Click += new System.EventHandler(this.labelSelected_Click);
            // 
            // buttonSelect
            // 
            this.buttonSelect.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonSelect.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonSelect.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonSelect.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonSelect.FlatAppearance.BorderSize = 0;
            this.buttonSelect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSelect.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonSelect.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonSelect.Location = new System.Drawing.Point(392, 0);
            this.buttonSelect.Margin = new System.Windows.Forms.Padding(2);
            this.buttonSelect.Name = "buttonSelect";
            this.buttonSelect.Size = new System.Drawing.Size(96, 26);
            this.buttonSelect.TabIndex = 15;
            this.buttonSelect.Text = "Select";
            this.buttonSelect.UseVisualStyleBackColor = false;
            this.buttonSelect.Click += new System.EventHandler(this.buttonSelect_Click);
            // 
            // labelListN
            // 
            this.labelListN.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelListN.Font = new System.Drawing.Font("Arial", 10F);
            this.labelListN.ForeColor = System.Drawing.Color.White;
            this.labelListN.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.labelListN.Location = new System.Drawing.Point(342, 0);
            this.labelListN.Name = "labelListN";
            this.labelListN.Size = new System.Drawing.Size(50, 26);
            this.labelListN.TabIndex = 13;
            this.labelListN.Text = "0";
            this.labelListN.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Left;
            this.label4.Font = new System.Drawing.Font("Arial", 10F);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label4.Location = new System.Drawing.Point(326, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(16, 26);
            this.label4.TabIndex = 12;
            this.label4.Text = " / ";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelResultN
            // 
            this.labelResultN.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelResultN.Font = new System.Drawing.Font("Arial", 10F);
            this.labelResultN.ForeColor = System.Drawing.Color.White;
            this.labelResultN.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.labelResultN.Location = new System.Drawing.Point(284, 0);
            this.labelResultN.Name = "labelResultN";
            this.labelResultN.Size = new System.Drawing.Size(42, 26);
            this.labelResultN.TabIndex = 11;
            this.labelResultN.Text = "0";
            this.labelResultN.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // checkBoxShowSearchResult
            // 
            this.checkBoxShowSearchResult.AutoSize = true;
            this.checkBoxShowSearchResult.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxShowSearchResult.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxShowSearchResult.ForeColor = System.Drawing.Color.White;
            this.checkBoxShowSearchResult.Location = new System.Drawing.Point(130, 0);
            this.checkBoxShowSearchResult.Name = "checkBoxShowSearchResult";
            this.checkBoxShowSearchResult.Size = new System.Drawing.Size(154, 26);
            this.checkBoxShowSearchResult.TabIndex = 10;
            this.checkBoxShowSearchResult.Text = "Show Search Result";
            this.checkBoxShowSearchResult.UseVisualStyleBackColor = true;
            this.checkBoxShowSearchResult.CheckedChanged += new System.EventHandler(this.checkBoxShowSearchResult_CheckedChanged);
            // 
            // checkBoxShowDeactivated
            // 
            this.checkBoxShowDeactivated.AutoSize = true;
            this.checkBoxShowDeactivated.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxShowDeactivated.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxShowDeactivated.ForeColor = System.Drawing.Color.White;
            this.checkBoxShowDeactivated.Location = new System.Drawing.Point(10, 0);
            this.checkBoxShowDeactivated.Name = "checkBoxShowDeactivated";
            this.checkBoxShowDeactivated.Size = new System.Drawing.Size(120, 26);
            this.checkBoxShowDeactivated.TabIndex = 9;
            this.checkBoxShowDeactivated.Text = "Show Disabled";
            this.checkBoxShowDeactivated.UseVisualStyleBackColor = true;
            this.checkBoxShowDeactivated.CheckedChanged += new System.EventHandler(this.checkBoxShowDeactivated_CheckedChanged);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button1.Dock = System.Windows.Forms.DockStyle.Right;
            this.button1.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button1.Location = new System.Drawing.Point(567, 0);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(96, 26);
            this.button1.TabIndex = 11;
            this.button1.Text = "Refresh";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonDuplicate
            // 
            this.buttonDuplicate.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonDuplicate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonDuplicate.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonDuplicate.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonDuplicate.FlatAppearance.BorderSize = 0;
            this.buttonDuplicate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDuplicate.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonDuplicate.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonDuplicate.Location = new System.Drawing.Point(663, 0);
            this.buttonDuplicate.Margin = new System.Windows.Forms.Padding(2);
            this.buttonDuplicate.Name = "buttonDuplicate";
            this.buttonDuplicate.Size = new System.Drawing.Size(138, 26);
            this.buttonDuplicate.TabIndex = 1;
            this.buttonDuplicate.Text = "Duplicate Client";
            this.buttonDuplicate.UseVisualStyleBackColor = false;
            this.buttonDuplicate.Click += new System.EventHandler(this.buttonDuplicate_Click);
            // 
            // buttonEdit
            // 
            this.buttonEdit.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonEdit.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonEdit.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonEdit.FlatAppearance.BorderSize = 0;
            this.buttonEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEdit.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonEdit.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonEdit.Location = new System.Drawing.Point(801, 0);
            this.buttonEdit.Margin = new System.Windows.Forms.Padding(2);
            this.buttonEdit.Name = "buttonEdit";
            this.buttonEdit.Size = new System.Drawing.Size(102, 26);
            this.buttonEdit.TabIndex = 1;
            this.buttonEdit.Text = "Edit Client";
            this.buttonEdit.UseVisualStyleBackColor = false;
            this.buttonEdit.Click += new System.EventHandler(this.buttonEdit_Click);
            // 
            // buttonDisable
            // 
            this.buttonDisable.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonDisable.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonDisable.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonDisable.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonDisable.FlatAppearance.BorderSize = 0;
            this.buttonDisable.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDisable.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonDisable.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonDisable.Location = new System.Drawing.Point(903, 0);
            this.buttonDisable.Margin = new System.Windows.Forms.Padding(2);
            this.buttonDisable.Name = "buttonDisable";
            this.buttonDisable.Size = new System.Drawing.Size(122, 26);
            this.buttonDisable.TabIndex = 1;
            this.buttonDisable.Text = "Disable Client";
            this.buttonDisable.UseVisualStyleBackColor = false;
            this.buttonDisable.Click += new System.EventHandler(this.buttonDisable_Click);
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(10, 26);
            this.panel2.TabIndex = 14;
            // 
            // CClientInfoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(1025, 589);
            this.Controls.Add(this.panel25);
            this.Controls.Add(this.panel26);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel21);
            this.Controls.Add(this.panel15);
            this.Controls.Add(this.panel11);
            this.Controls.Add(this.panel10);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel5);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "CClientInfoForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Clients";
            this.Load += new System.EventHandler(this.CAddClientsForm_Load);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewList)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel25.ResumeLayout(false);
            this.panel29.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label labelClientDetails;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TextBox textBoxClientFullName;
        private System.Windows.Forms.Label labelClientName;
        private System.Windows.Forms.ComboBox comboBoxClientCountryList;
        private System.Windows.Forms.TextBox textBoxClientDirectNr;
        private System.Windows.Forms.Label labelClientDirectNr;
        private System.Windows.Forms.TextBox textBoxClientAddress;
        private System.Windows.Forms.Label labelClientAddress;
        private System.Windows.Forms.ComboBox comboClientStateList;
        private System.Windows.Forms.TextBox textBoxClientCentralNr;
        private System.Windows.Forms.Label labelClientCentralNr;
        private System.Windows.Forms.Label labelClientCountry;
        private System.Windows.Forms.TextBox textBoxClientCity;
        private System.Windows.Forms.Label labelClientCity;
        private System.Windows.Forms.Label labelClientState;
        private System.Windows.Forms.TextBox textBoxClientZip;
        private System.Windows.Forms.Label labelClientZip;
        private System.Windows.Forms.TextBox textBoxClientEmail;
        private System.Windows.Forms.Label labelClientEmail;
        private System.Windows.Forms.TextBox textBoxClientFax;
        private System.Windows.Forms.Label labelClientFax;
        private System.Windows.Forms.DataGridView dataGridViewList;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonDuplicate;
        private System.Windows.Forms.Button buttonEdit;
        private System.Windows.Forms.Button buttonDisable;
        private System.Windows.Forms.TextBox textBoxHouseNumber;
        private System.Windows.Forms.Label labelPatientHouseNumber;
        private System.Windows.Forms.TextBox textBoxClientLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonDup;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Button buttonRandom;
        private System.Windows.Forms.TextBox textBoxClientNr;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonSearch;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox checkBoxActive;
        private System.Windows.Forms.Label labelClientError;
        private System.Windows.Forms.Panel panel38;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label labelListN;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelResultN;
        private System.Windows.Forms.CheckBox checkBoxShowSearchResult;
        private System.Windows.Forms.CheckBox checkBoxShowDeactivated;
        private System.Windows.Forms.Button buttonAddUpdate;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox textBoxDepartmentPhone;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxDepartmentHead;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxDepartmentName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelSelected;
        private System.Windows.Forms.Button buttonSelect;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClientNr;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnActive;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1ClientName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnFullName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6ClientCity;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7ClientCountry;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDepartment;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnHeadDept;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnPhoneHeadDept;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2ClientStreet;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3ClientZip;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4ClientHouseNr;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5ClientState;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8ClientCentralNr;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9ClientDirectNr;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10ClientFax;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11ClientEmail;
    }
}