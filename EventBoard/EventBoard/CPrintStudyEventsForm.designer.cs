﻿namespace EventBoard
{
    partial class CPrintStudyEventsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CPrintStudyEventsForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint1 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1D, 30D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint2 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 60D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint3 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 14D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint4 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 30D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint5 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(5D, 22D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint6 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(6D, 8D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint7 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(7D, 10D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint8 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(8D, 15D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint9 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(9D, 24D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint10 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(10D, 7D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint11 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(11D, 7D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint12 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(12D, 5D);
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripGenPages = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonPrint = new System.Windows.Forms.ToolStripButton();
            this.toolStripMemUsage = new System.Windows.Forms.ToolStripLabel();
            this.toolStripClipboard1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripClipboard2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripEnterData = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panelPrintArea3 = new System.Windows.Forms.Panel();
            this.panelEventSample5 = new System.Windows.Forms.Panel();
            this.panel96 = new System.Windows.Forms.Panel();
            this.panel98 = new System.Windows.Forms.Panel();
            this.textBoxActivities5 = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.panel117 = new System.Windows.Forms.Panel();
            this.textBoxSymptoms5 = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.panel92 = new System.Windows.Forms.Panel();
            this.textBoxFindings5 = new System.Windows.Forms.TextBox();
            this.panel137 = new System.Windows.Forms.Panel();
            this.labelRhythms5 = new System.Windows.Forms.Label();
            this.labelFindingsText5 = new System.Windows.Forms.Label();
            this.panel97 = new System.Windows.Forms.Panel();
            this.panel113 = new System.Windows.Forms.Panel();
            this.labelQtUnit5 = new System.Windows.Forms.Label();
            this.labelQrsUnit5 = new System.Windows.Forms.Label();
            this.panel112 = new System.Windows.Forms.Panel();
            this.labelQtValue5 = new System.Windows.Forms.Label();
            this.labelQrsValue5 = new System.Windows.Forms.Label();
            this.panel111 = new System.Windows.Forms.Panel();
            this.labelQtText5 = new System.Windows.Forms.Label();
            this.labelQrsText5 = new System.Windows.Forms.Label();
            this.panel102 = new System.Windows.Forms.Panel();
            this.labelPrUnit5 = new System.Windows.Forms.Label();
            this.labelMeanHrUnit5 = new System.Windows.Forms.Label();
            this.panel93 = new System.Windows.Forms.Panel();
            this.labelPrValue5 = new System.Windows.Forms.Label();
            this.labelMeanHrValue5 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.labelPrText5 = new System.Windows.Forms.Label();
            this.labelMeanHrText5 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.panel99 = new System.Windows.Forms.Panel();
            this.panel100 = new System.Windows.Forms.Panel();
            this.panel153 = new System.Windows.Forms.Panel();
            this.labelSample5AmplFull = new System.Windows.Forms.Label();
            this.labelSample5SweepFull = new System.Windows.Forms.Label();
            this.labelSample5EndFull = new System.Windows.Forms.Label();
            this.labelSample5MidFull = new System.Windows.Forms.Label();
            this.labelSample5StartFull = new System.Windows.Forms.Label();
            this.panel290 = new System.Windows.Forms.Panel();
            this.panel291 = new System.Windows.Forms.Panel();
            this.labelSample5LeadFull = new System.Windows.Forms.Label();
            this.pictureBoxSample5Full = new System.Windows.Forms.PictureBox();
            this.panel298 = new System.Windows.Forms.Panel();
            this.panel378 = new System.Windows.Forms.Panel();
            this.labelSample5Ampl = new System.Windows.Forms.Label();
            this.labelSample5Sweep = new System.Windows.Forms.Label();
            this.labelSample5End = new System.Windows.Forms.Label();
            this.labelSample5Mid = new System.Windows.Forms.Label();
            this.labelSample5Start = new System.Windows.Forms.Label();
            this.panel421 = new System.Windows.Forms.Panel();
            this.panel422 = new System.Windows.Forms.Panel();
            this.labelSample5Lead = new System.Windows.Forms.Label();
            this.pictureBoxSample5 = new System.Windows.Forms.PictureBox();
            this.panel453 = new System.Windows.Forms.Panel();
            this.label144 = new System.Windows.Forms.Label();
            this.label147 = new System.Windows.Forms.Label();
            this.labelSample5EventNr = new System.Windows.Forms.Label();
            this.labelEvent5 = new System.Windows.Forms.Label();
            this.labelSample5Date = new System.Windows.Forms.Label();
            this.label142 = new System.Windows.Forms.Label();
            this.labelSample5Time = new System.Windows.Forms.Label();
            this.labelSampleType5 = new System.Windows.Forms.Label();
            this.labelClassification5 = new System.Windows.Forms.Label();
            this.labelSample5Nr = new System.Windows.Forms.Label();
            this.panel468 = new System.Windows.Forms.Panel();
            this.panel64 = new System.Windows.Forms.Panel();
            this.panelEventSample4 = new System.Windows.Forms.Panel();
            this.panel114 = new System.Windows.Forms.Panel();
            this.panel94 = new System.Windows.Forms.Panel();
            this.textBoxActivities4 = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.panel63 = new System.Windows.Forms.Panel();
            this.textBoxSymptoms4 = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.panel19 = new System.Windows.Forms.Panel();
            this.textBoxFindings4 = new System.Windows.Forms.TextBox();
            this.panel119 = new System.Windows.Forms.Panel();
            this.labelRhythms4 = new System.Windows.Forms.Label();
            this.labelFindingsText4 = new System.Windows.Forms.Label();
            this.panel95 = new System.Windows.Forms.Panel();
            this.panel49 = new System.Windows.Forms.Panel();
            this.labelQtUnit4 = new System.Windows.Forms.Label();
            this.labelQrsUnit4 = new System.Windows.Forms.Label();
            this.panel48 = new System.Windows.Forms.Panel();
            this.labelQtValue4 = new System.Windows.Forms.Label();
            this.labelQrsValue4 = new System.Windows.Forms.Label();
            this.panel43 = new System.Windows.Forms.Panel();
            this.labelQtText4 = new System.Windows.Forms.Label();
            this.labelQrsText4 = new System.Windows.Forms.Label();
            this.panel33 = new System.Windows.Forms.Panel();
            this.labelPrUnit4 = new System.Windows.Forms.Label();
            this.labelMeanHrUnit4 = new System.Windows.Forms.Label();
            this.panel26 = new System.Windows.Forms.Panel();
            this.labelPrValue4 = new System.Windows.Forms.Label();
            this.labelMeanHrValue4 = new System.Windows.Forms.Label();
            this.panel23 = new System.Windows.Forms.Panel();
            this.labelPrText4 = new System.Windows.Forms.Label();
            this.labelMeanHrText4 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.panel115 = new System.Windows.Forms.Panel();
            this.panel116 = new System.Windows.Forms.Panel();
            this.panel266 = new System.Windows.Forms.Panel();
            this.labelSample4AmplFull = new System.Windows.Forms.Label();
            this.labelSample4SweepFull = new System.Windows.Forms.Label();
            this.labelSample4MidFull = new System.Windows.Forms.Label();
            this.labelSample4StartFull = new System.Windows.Forms.Label();
            this.labelSample4EndFull = new System.Windows.Forms.Label();
            this.panel275 = new System.Windows.Forms.Panel();
            this.pictureBoxSample4Full = new System.Windows.Forms.PictureBox();
            this.labelSample4LeadFull = new System.Windows.Forms.Label();
            this.panel305 = new System.Windows.Forms.Panel();
            this.panel306 = new System.Windows.Forms.Panel();
            this.labelSample4Ampl = new System.Windows.Forms.Label();
            this.labelSample4Sweep = new System.Windows.Forms.Label();
            this.labelSample4End = new System.Windows.Forms.Label();
            this.labelSample4Mid = new System.Windows.Forms.Label();
            this.labelSample4Start = new System.Windows.Forms.Label();
            this.panel328 = new System.Windows.Forms.Panel();
            this.panel329 = new System.Windows.Forms.Panel();
            this.labelSample4Lead = new System.Windows.Forms.Label();
            this.QtUnit4 = new System.Windows.Forms.Label();
            this.QtText4 = new System.Windows.Forms.Label();
            this.QrsUnit4 = new System.Windows.Forms.Label();
            this.QrsText4 = new System.Windows.Forms.Label();
            this.lPrUnit4 = new System.Windows.Forms.Label();
            this.PrText4 = new System.Windows.Forms.Label();
            this.MeanHrUnit4 = new System.Windows.Forms.Label();
            this.MeanHrText4 = new System.Windows.Forms.Label();
            this.pictureBoxSample4 = new System.Windows.Forms.PictureBox();
            this.panel357 = new System.Windows.Forms.Panel();
            this.label121 = new System.Windows.Forms.Label();
            this.label124 = new System.Windows.Forms.Label();
            this.labelSample4EventNr = new System.Windows.Forms.Label();
            this.labelEvent4 = new System.Windows.Forms.Label();
            this.labelSample4Date = new System.Windows.Forms.Label();
            this.label119 = new System.Windows.Forms.Label();
            this.labelSample4Time = new System.Windows.Forms.Label();
            this.labelSampleType4 = new System.Windows.Forms.Label();
            this.labelClassification4 = new System.Windows.Forms.Label();
            this.labelSample4Nr = new System.Windows.Forms.Label();
            this.panel35 = new System.Windows.Forms.Panel();
            this.panelEventSample3 = new System.Windows.Forms.Panel();
            this.panel255 = new System.Windows.Forms.Panel();
            this.panelActivities3 = new System.Windows.Forms.Panel();
            this.textBoxActivities3 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.panelSymptoms3 = new System.Windows.Forms.Panel();
            this.textBoxSymptoms3 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.panelfindings3 = new System.Windows.Forms.Panel();
            this.textBoxFindings3 = new System.Windows.Forms.TextBox();
            this.panel118 = new System.Windows.Forms.Panel();
            this.labelRhythms3 = new System.Windows.Forms.Label();
            this.labelFindingsText3 = new System.Windows.Forms.Label();
            this.panel270 = new System.Windows.Forms.Panel();
            this.panel273 = new System.Windows.Forms.Panel();
            this.panelQRSmeasurements3 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.labelQtUnit3 = new System.Windows.Forms.Label();
            this.labelQrsUnit3 = new System.Windows.Forms.Label();
            this.panel16 = new System.Windows.Forms.Panel();
            this.labelQtValue3 = new System.Windows.Forms.Label();
            this.labelQrsValue3 = new System.Windows.Forms.Label();
            this.panel15 = new System.Windows.Forms.Panel();
            this.labelQtText3 = new System.Windows.Forms.Label();
            this.labelQrsText3 = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.labelPrUnit3 = new System.Windows.Forms.Label();
            this.labelMeanHrUnit3 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.labelPrValue3 = new System.Windows.Forms.Label();
            this.labelMeanHrValue3 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.labelPrText3 = new System.Windows.Forms.Label();
            this.labelMeanHrText3 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.panel277 = new System.Windows.Forms.Panel();
            this.labelSample3AmplFull = new System.Windows.Forms.Label();
            this.labelSample3SweepFull = new System.Windows.Forms.Label();
            this.labelSample3MidFull = new System.Windows.Forms.Label();
            this.labelSample3StartFull = new System.Windows.Forms.Label();
            this.labelSample3EndFull = new System.Windows.Forms.Label();
            this.panel283 = new System.Windows.Forms.Panel();
            this.pictureBoxSample3Full = new System.Windows.Forms.PictureBox();
            this.panel300 = new System.Windows.Forms.Panel();
            this.panel384 = new System.Windows.Forms.Panel();
            this.labelSample3Mid = new System.Windows.Forms.Label();
            this.labelSample3Start = new System.Windows.Forms.Label();
            this.labelSample3Ampl = new System.Windows.Forms.Label();
            this.labelSample3Sweep = new System.Windows.Forms.Label();
            this.labelSample3End = new System.Windows.Forms.Label();
            this.panel387 = new System.Windows.Forms.Panel();
            this.panel388 = new System.Windows.Forms.Panel();
            this.pictureBoxSample3 = new System.Windows.Forms.PictureBox();
            this.panel416 = new System.Windows.Forms.Panel();
            this.label73 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.labelSample3EventNr = new System.Windows.Forms.Label();
            this.labelEvent3 = new System.Windows.Forms.Label();
            this.labelSampleType3 = new System.Windows.Forms.Label();
            this.labelSample3Date = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.labelSample3Time = new System.Windows.Forms.Label();
            this.labelClassification3 = new System.Windows.Forms.Label();
            this.labelSample3Nr = new System.Windows.Forms.Label();
            this.panel251 = new System.Windows.Forms.Panel();
            this.panelPage2Footer = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.labelPage3StudyNumber = new System.Windows.Forms.Label();
            this.labelPrintDate3Bottom = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.labelPage3Of = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.labelPage3xOfX = new System.Windows.Forms.Label();
            this.labelPrintDate = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel101 = new System.Windows.Forms.Panel();
            this.panelPage2Header = new System.Windows.Forms.Panel();
            this.panel122 = new System.Windows.Forms.Panel();
            this.panel125 = new System.Windows.Forms.Panel();
            this.panel123 = new System.Windows.Forms.Panel();
            this.labelPage3DateOfBirth = new System.Windows.Forms.Label();
            this.labelDateOfBirthPage2 = new System.Windows.Forms.Label();
            this.panel527 = new System.Windows.Forms.Panel();
            this.panel525 = new System.Windows.Forms.Panel();
            this.labelPage3PatLastName = new System.Windows.Forms.Label();
            this.labelPatName = new System.Windows.Forms.Label();
            this.panel522 = new System.Windows.Forms.Panel();
            this.panel520 = new System.Windows.Forms.Panel();
            this.labelPage3PatientID = new System.Windows.Forms.Label();
            this.labelPatientID = new System.Windows.Forms.Label();
            this.labelReportText3 = new System.Windows.Forms.Label();
            this.labelPage3ReportNr = new System.Windows.Forms.Label();
            this.panel500 = new System.Windows.Forms.Panel();
            this.labelPage3 = new System.Windows.Forms.Label();
            this.pictureBoxCenter3 = new System.Windows.Forms.PictureBox();
            this.panel501 = new System.Windows.Forms.Panel();
            this.panel503 = new System.Windows.Forms.Panel();
            this.labelCenter2p3 = new System.Windows.Forms.Label();
            this.panel504 = new System.Windows.Forms.Panel();
            this.labelCenter1p3 = new System.Windows.Forms.Label();
            this.panel505 = new System.Windows.Forms.Panel();
            this.panelPrintArea2 = new System.Windows.Forms.Panel();
            this.panelGridTableFill = new System.Windows.Forms.Panel();
            this.panelGridTable = new System.Windows.Forms.Panel();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnEventDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnSampleNr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnFindings = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnRemarks = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel78 = new System.Windows.Forms.Panel();
            this.panelHeaderPage2 = new System.Windows.Forms.Panel();
            this.panel37 = new System.Windows.Forms.Panel();
            this.panel40 = new System.Windows.Forms.Panel();
            this.labelNrStripsTable = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.panel508 = new System.Windows.Forms.Panel();
            this.panel745 = new System.Windows.Forms.Panel();
            this.panel799 = new System.Windows.Forms.Panel();
            this.panel800 = new System.Windows.Forms.Panel();
            this.panel801 = new System.Windows.Forms.Panel();
            this.panel802 = new System.Windows.Forms.Panel();
            this.panel803 = new System.Windows.Forms.Panel();
            this.panel804 = new System.Windows.Forms.Panel();
            this.panelPage2Header2 = new System.Windows.Forms.Panel();
            this.panel533 = new System.Windows.Forms.Panel();
            this.panel531 = new System.Windows.Forms.Panel();
            this.labelPage2DateOfBirth = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.panel819 = new System.Windows.Forms.Panel();
            this.panel821 = new System.Windows.Forms.Panel();
            this.labelPage2PatLastName = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.panel120 = new System.Windows.Forms.Panel();
            this.panel124 = new System.Windows.Forms.Panel();
            this.panel121 = new System.Windows.Forms.Panel();
            this.labelPage2PatientID = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.labelReportText2 = new System.Windows.Forms.Label();
            this.labelPage2ReportNr = new System.Windows.Forms.Label();
            this.panelPage2Header1 = new System.Windows.Forms.Panel();
            this.labelPage2 = new System.Windows.Forms.Label();
            this.pictureBoxCenter2 = new System.Windows.Forms.PictureBox();
            this.panel826 = new System.Windows.Forms.Panel();
            this.panel828 = new System.Windows.Forms.Panel();
            this.labelCenter2p2 = new System.Windows.Forms.Label();
            this.panel829 = new System.Windows.Forms.Panel();
            this.labelCenter1p2 = new System.Windows.Forms.Label();
            this.panel830 = new System.Windows.Forms.Panel();
            this.panel632 = new System.Windows.Forms.Panel();
            this.panel706 = new System.Windows.Forms.Panel();
            this.panel785 = new System.Windows.Forms.Panel();
            this.label61 = new System.Windows.Forms.Label();
            this.labelPage2StudyNumber = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.labelPage2Of = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.labelPage2xOfX = new System.Windows.Forms.Label();
            this.labelPrintDate2Bottom = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.panelPrintHeader = new System.Windows.Forms.Panel();
            this.labelPage1 = new System.Windows.Forms.Label();
            this.pictureBoxCenter = new System.Windows.Forms.PictureBox();
            this.panel56 = new System.Windows.Forms.Panel();
            this.panel58 = new System.Windows.Forms.Panel();
            this.labelCenter2 = new System.Windows.Forms.Label();
            this.panel59 = new System.Windows.Forms.Panel();
            this.labelCenter1 = new System.Windows.Forms.Label();
            this.panelCardiacEVentReportNumber = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel50 = new System.Windows.Forms.Panel();
            this.labelCardiacEventReportNr = new System.Windows.Forms.Label();
            this.labelReportNr = new System.Windows.Forms.Label();
            this.panel28 = new System.Windows.Forms.Panel();
            this.panelDateTimeofEventStrip = new System.Windows.Forms.Panel();
            this.panel53 = new System.Windows.Forms.Panel();
            this.labelSingleEventReportDate = new System.Windows.Forms.Label();
            this.labelPrintDateHeader = new System.Windows.Forms.Label();
            this.panelHorSepPatDet = new System.Windows.Forms.Panel();
            this.panelStudyProcedures = new System.Windows.Forms.Panel();
            this.panelDiagnosis = new System.Windows.Forms.Panel();
            this.panelHartRate = new System.Windows.Forms.Panel();
            this.panel54 = new System.Windows.Forms.Panel();
            this.labelHrMaxPage = new System.Windows.Forms.Label();
            this.labelHrMinPage = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.panel47 = new System.Windows.Forms.Panel();
            this.panel52 = new System.Windows.Forms.Panel();
            this.labelHrMaxIX = new System.Windows.Forms.Label();
            this.labelHrMinIX = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.panel46 = new System.Windows.Forms.Panel();
            this.labelHrMean = new System.Windows.Forms.Label();
            this.labelHrMax = new System.Windows.Forms.Label();
            this.labelHrMin = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label51 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel69 = new System.Windows.Forms.Panel();
            this.labelDiagnosis = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel158 = new System.Windows.Forms.Panel();
            this.panelEventsStat = new System.Windows.Forms.Panel();
            this.panel753 = new System.Windows.Forms.Panel();
            this.panel756ST = new System.Windows.Forms.Panel();
            this.panel771 = new System.Windows.Forms.Panel();
            this.label25 = new System.Windows.Forms.Label();
            this.labelAutoManual = new System.Windows.Forms.Label();
            this.label127 = new System.Windows.Forms.Label();
            this.panel74 = new System.Windows.Forms.Panel();
            this.labelUser = new System.Windows.Forms.Label();
            this.labelTechText = new System.Windows.Forms.Label();
            this.panel45 = new System.Windows.Forms.Panel();
            this.labelDayNight = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.panel770 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.labelAbnMax = new System.Windows.Forms.Label();
            this.labelAbnMin = new System.Windows.Forms.Label();
            this.labelAbnN = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.panel39 = new System.Windows.Forms.Panel();
            this.labelNormMax = new System.Windows.Forms.Label();
            this.labelNormMin = new System.Windows.Forms.Label();
            this.labelNormN = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.panel769 = new System.Windows.Forms.Panel();
            this.labelOtherMax = new System.Windows.Forms.Label();
            this.labelOtherMin = new System.Windows.Forms.Label();
            this.labelOtherN = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.panel146 = new System.Windows.Forms.Panel();
            this.labelVtMax = new System.Windows.Forms.Label();
            this.labelVtMin = new System.Windows.Forms.Label();
            this.labelVtN = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.panel85 = new System.Windows.Forms.Panel();
            this.labelPaceMax = new System.Windows.Forms.Label();
            this.labelPaceMin = new System.Windows.Forms.Label();
            this.labelPaceN = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.panel80 = new System.Windows.Forms.Panel();
            this.labelPvcMax = new System.Windows.Forms.Label();
            this.labelPvcMin = new System.Windows.Forms.Label();
            this.labelPvcN = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.panel79 = new System.Windows.Forms.Panel();
            this.labelPacMax = new System.Windows.Forms.Label();
            this.labelPacMin = new System.Windows.Forms.Label();
            this.labelPacN = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.panel38 = new System.Windows.Forms.Panel();
            this.labelAFlutMax = new System.Windows.Forms.Label();
            this.labelAFlutMin = new System.Windows.Forms.Label();
            this.labelAFlutN = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.panel768 = new System.Windows.Forms.Panel();
            this.labelAfibMax = new System.Windows.Forms.Label();
            this.labelAfibMin = new System.Windows.Forms.Label();
            this.labelAfibN = new System.Windows.Forms.Label();
            this.label123 = new System.Windows.Forms.Label();
            this.panel767 = new System.Windows.Forms.Panel();
            this.labelPauseMax = new System.Windows.Forms.Label();
            this.labelPauseMin = new System.Windows.Forms.Label();
            this.labelPauseN = new System.Windows.Forms.Label();
            this.label122 = new System.Windows.Forms.Label();
            this.panel766 = new System.Windows.Forms.Panel();
            this.labelBradyMax = new System.Windows.Forms.Label();
            this.labelBradyMin = new System.Windows.Forms.Label();
            this.labelBradyN = new System.Windows.Forms.Label();
            this.label120 = new System.Windows.Forms.Label();
            this.panel765 = new System.Windows.Forms.Panel();
            this.labelTachyMax = new System.Windows.Forms.Label();
            this.labelTachyMin = new System.Windows.Forms.Label();
            this.labelTachyN = new System.Windows.Forms.Label();
            this.label118 = new System.Windows.Forms.Label();
            this.panel764 = new System.Windows.Forms.Panel();
            this.label59 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.panel86 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.label89 = new System.Windows.Forms.Label();
            this.panel755 = new System.Windows.Forms.Panel();
            this.panel754 = new System.Windows.Forms.Panel();
            this.panel759 = new System.Windows.Forms.Panel();
            this.chartSummaryBar = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel758 = new System.Windows.Forms.Panel();
            this.label79 = new System.Windows.Forms.Label();
            this.panelLineUnderSampl1 = new System.Windows.Forms.Panel();
            this.panelSignatures = new System.Windows.Forms.Panel();
            this.panel110 = new System.Windows.Forms.Panel();
            this.labelReportSignDate = new System.Windows.Forms.Label();
            this.panel109 = new System.Windows.Forms.Panel();
            this.labelDateReportSign = new System.Windows.Forms.Label();
            this.panel108 = new System.Windows.Forms.Panel();
            this.labelPhysSignLine = new System.Windows.Forms.Label();
            this.panel107 = new System.Windows.Forms.Panel();
            this.labelPhysSign = new System.Windows.Forms.Label();
            this.panel106 = new System.Windows.Forms.Panel();
            this.labelPhysPrintName = new System.Windows.Forms.Label();
            this.panel105 = new System.Windows.Forms.Panel();
            this.labelPhysNameReportPrint = new System.Windows.Forms.Label();
            this.panel51 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.labelStudyNr = new System.Windows.Forms.Label();
            this.labelPage = new System.Windows.Forms.Label();
            this.labelPage1of = new System.Windows.Forms.Label();
            this.labelPageOf = new System.Windows.Forms.Label();
            this.labelPage1xOfX = new System.Windows.Forms.Label();
            this.labelPrintDate1Bottom = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panelPrintArea1 = new System.Windows.Forms.Panel();
            this.panel126 = new System.Windows.Forms.Panel();
            this.panelPhysicianConclusion = new System.Windows.Forms.Panel();
            this.panel72 = new System.Windows.Forms.Panel();
            this.labelConclusionText = new System.Windows.Forms.Label();
            this.panel75 = new System.Windows.Forms.Panel();
            this.panel82 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.panelRowInterpretation = new System.Windows.Forms.Panel();
            this.panel89 = new System.Windows.Forms.Panel();
            this.labelSummaryText = new System.Windows.Forms.Label();
            this.panel91 = new System.Windows.Forms.Panel();
            this.panel90 = new System.Windows.Forms.Panel();
            this.labelReportFindings = new System.Windows.Forms.Label();
            this.panelPhysInfo = new System.Windows.Forms.Panel();
            this.panel67 = new System.Windows.Forms.Panel();
            this.panel258 = new System.Windows.Forms.Panel();
            this.labelClientName = new System.Windows.Forms.Label();
            this.labelClientTel = new System.Windows.Forms.Label();
            this.panel256 = new System.Windows.Forms.Panel();
            this.label41 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.labelClientHeader = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel31 = new System.Windows.Forms.Panel();
            this.panelVert9 = new System.Windows.Forms.Panel();
            this.panel252 = new System.Windows.Forms.Panel();
            this.labelRefPhysicianName = new System.Windows.Forms.Label();
            this.labelRefPhysicianTel = new System.Windows.Forms.Label();
            this.panel250 = new System.Windows.Forms.Panel();
            this.label42 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.labelRefPhysHeader = new System.Windows.Forms.Label();
            this.panelVert8 = new System.Windows.Forms.Panel();
            this.panel244 = new System.Windows.Forms.Panel();
            this.labelPhysicianName = new System.Windows.Forms.Label();
            this.labelPhysicianTel = new System.Windows.Forms.Label();
            this.panel242 = new System.Windows.Forms.Panel();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.labelPhysHeader = new System.Windows.Forms.Label();
            this.panelVert6 = new System.Windows.Forms.Panel();
            this.panel32 = new System.Windows.Forms.Panel();
            this.panel36 = new System.Windows.Forms.Panel();
            this.panel41 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.labelEndStudyDate = new System.Windows.Forms.Label();
            this.labelRoom = new System.Windows.Forms.Label();
            this.labelPatID = new System.Windows.Forms.Label();
            this.labelSerialNr = new System.Windows.Forms.Label();
            this.labelStartDate = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.labelEndStudyText = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelPatIDHeader = new System.Windows.Forms.Label();
            this.labelSerialNrHeader = new System.Windows.Forms.Label();
            this.labelStartDateHeader = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel24 = new System.Windows.Forms.Panel();
            this.panel34 = new System.Windows.Forms.Panel();
            this.labelPhone2 = new System.Windows.Forms.Label();
            this.labelPhone1 = new System.Windows.Forms.Label();
            this.labelCountry = new System.Windows.Forms.Label();
            this.labelZipCity = new System.Windows.Forms.Label();
            this.labelAddress = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.labelAddressHeader = new System.Windows.Forms.Label();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel27 = new System.Windows.Forms.Panel();
            this.labelPatientLastName = new System.Windows.Forms.Label();
            this.labelPatientFirstName = new System.Windows.Forms.Label();
            this.labelDateOfBirth = new System.Windows.Forms.Label();
            this.labelAgeGender = new System.Windows.Forms.Label();
            this.panel21 = new System.Windows.Forms.Panel();
            this.panel22 = new System.Windows.Forms.Panel();
            this.label32 = new System.Windows.Forms.Label();
            this.labelDOBheader = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.panel25 = new System.Windows.Forms.Panel();
            this.panel29 = new System.Windows.Forms.Panel();
            this.panel30 = new System.Windows.Forms.Panel();
            this.toolStrip1.SuspendLayout();
            this.panelPrintArea3.SuspendLayout();
            this.panelEventSample5.SuspendLayout();
            this.panel96.SuspendLayout();
            this.panel98.SuspendLayout();
            this.panel117.SuspendLayout();
            this.panel92.SuspendLayout();
            this.panel137.SuspendLayout();
            this.panel97.SuspendLayout();
            this.panel113.SuspendLayout();
            this.panel112.SuspendLayout();
            this.panel111.SuspendLayout();
            this.panel102.SuspendLayout();
            this.panel93.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel153.SuspendLayout();
            this.panel290.SuspendLayout();
            this.panel291.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample5Full)).BeginInit();
            this.panel378.SuspendLayout();
            this.panel421.SuspendLayout();
            this.panel422.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample5)).BeginInit();
            this.panel453.SuspendLayout();
            this.panelEventSample4.SuspendLayout();
            this.panel114.SuspendLayout();
            this.panel94.SuspendLayout();
            this.panel63.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel119.SuspendLayout();
            this.panel95.SuspendLayout();
            this.panel49.SuspendLayout();
            this.panel48.SuspendLayout();
            this.panel43.SuspendLayout();
            this.panel33.SuspendLayout();
            this.panel26.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel266.SuspendLayout();
            this.panel275.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample4Full)).BeginInit();
            this.panel306.SuspendLayout();
            this.panel328.SuspendLayout();
            this.panel329.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample4)).BeginInit();
            this.panel357.SuspendLayout();
            this.panelEventSample3.SuspendLayout();
            this.panel255.SuspendLayout();
            this.panelActivities3.SuspendLayout();
            this.panelSymptoms3.SuspendLayout();
            this.panelfindings3.SuspendLayout();
            this.panel118.SuspendLayout();
            this.panelQRSmeasurements3.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel277.SuspendLayout();
            this.panel283.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample3Full)).BeginInit();
            this.panel384.SuspendLayout();
            this.panel387.SuspendLayout();
            this.panel388.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample3)).BeginInit();
            this.panel416.SuspendLayout();
            this.panelPage2Footer.SuspendLayout();
            this.panel101.SuspendLayout();
            this.panelPage2Header.SuspendLayout();
            this.panel122.SuspendLayout();
            this.panel500.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCenter3)).BeginInit();
            this.panel501.SuspendLayout();
            this.panel503.SuspendLayout();
            this.panel504.SuspendLayout();
            this.panelPrintArea2.SuspendLayout();
            this.panelGridTableFill.SuspendLayout();
            this.panelGridTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.panelHeaderPage2.SuspendLayout();
            this.panel37.SuspendLayout();
            this.panel40.SuspendLayout();
            this.panel800.SuspendLayout();
            this.panel802.SuspendLayout();
            this.panelPage2Header2.SuspendLayout();
            this.panel120.SuspendLayout();
            this.panelPage2Header1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCenter2)).BeginInit();
            this.panel826.SuspendLayout();
            this.panel828.SuspendLayout();
            this.panel829.SuspendLayout();
            this.panel632.SuspendLayout();
            this.panel785.SuspendLayout();
            this.panelPrintHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCenter)).BeginInit();
            this.panel56.SuspendLayout();
            this.panel58.SuspendLayout();
            this.panel59.SuspendLayout();
            this.panelCardiacEVentReportNumber.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel50.SuspendLayout();
            this.panelDateTimeofEventStrip.SuspendLayout();
            this.panel53.SuspendLayout();
            this.panelStudyProcedures.SuspendLayout();
            this.panelDiagnosis.SuspendLayout();
            this.panelHartRate.SuspendLayout();
            this.panel54.SuspendLayout();
            this.panel52.SuspendLayout();
            this.panel46.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel69.SuspendLayout();
            this.panelEventsStat.SuspendLayout();
            this.panel753.SuspendLayout();
            this.panel756ST.SuspendLayout();
            this.panel771.SuspendLayout();
            this.panel74.SuspendLayout();
            this.panel770.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel39.SuspendLayout();
            this.panel769.SuspendLayout();
            this.panel146.SuspendLayout();
            this.panel85.SuspendLayout();
            this.panel80.SuspendLayout();
            this.panel79.SuspendLayout();
            this.panel38.SuspendLayout();
            this.panel768.SuspendLayout();
            this.panel767.SuspendLayout();
            this.panel766.SuspendLayout();
            this.panel765.SuspendLayout();
            this.panel764.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel754.SuspendLayout();
            this.panel759.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartSummaryBar)).BeginInit();
            this.panel758.SuspendLayout();
            this.panelSignatures.SuspendLayout();
            this.panel110.SuspendLayout();
            this.panel109.SuspendLayout();
            this.panel108.SuspendLayout();
            this.panel107.SuspendLayout();
            this.panel106.SuspendLayout();
            this.panel105.SuspendLayout();
            this.panel51.SuspendLayout();
            this.panelPrintArea1.SuspendLayout();
            this.panelPhysicianConclusion.SuspendLayout();
            this.panel72.SuspendLayout();
            this.panel82.SuspendLayout();
            this.panelRowInterpretation.SuspendLayout();
            this.panel89.SuspendLayout();
            this.panel90.SuspendLayout();
            this.panelPhysInfo.SuspendLayout();
            this.panel67.SuspendLayout();
            this.panel258.SuspendLayout();
            this.panel256.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel252.SuspendLayout();
            this.panel250.SuspendLayout();
            this.panel244.SuspendLayout();
            this.panel242.SuspendLayout();
            this.panel32.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel34.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel25.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripGenPages,
            this.toolStripButtonPrint,
            this.toolStripMemUsage,
            this.toolStripClipboard1,
            this.toolStripClipboard2,
            this.toolStripButton1,
            this.toolStripEnterData,
            this.toolStripButton2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1855, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripGenPages
            // 
            this.toolStripGenPages.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripGenPages.Image = ((System.Drawing.Image)(resources.GetObject("toolStripGenPages.Image")));
            this.toolStripGenPages.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripGenPages.Name = "toolStripGenPages";
            this.toolStripGenPages.Size = new System.Drawing.Size(112, 22);
            this.toolStripGenPages.Text = "Generating Pages...";
            // 
            // toolStripButtonPrint
            // 
            this.toolStripButtonPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonPrint.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonPrint.Image")));
            this.toolStripButtonPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonPrint.Name = "toolStripButtonPrint";
            this.toolStripButtonPrint.Size = new System.Drawing.Size(36, 22);
            this.toolStripButtonPrint.Text = "Print";
            this.toolStripButtonPrint.Click += new System.EventHandler(this.toolStripButton1_Click_1);
            // 
            // toolStripMemUsage
            // 
            this.toolStripMemUsage.Name = "toolStripMemUsage";
            this.toolStripMemUsage.Size = new System.Drawing.Size(125, 22);
            this.toolStripMemUsage.Text = "^p1234 \\r\\nf 12345MB";
            // 
            // toolStripClipboard1
            // 
            this.toolStripClipboard1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripClipboard1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripClipboard1.Image")));
            this.toolStripClipboard1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripClipboard1.Name = "toolStripClipboard1";
            this.toolStripClipboard1.Size = new System.Drawing.Size(72, 22);
            this.toolStripClipboard1.Text = "Clipboard 1";
            this.toolStripClipboard1.Click += new System.EventHandler(this.toolStripClipboard_Click);
            // 
            // toolStripClipboard2
            // 
            this.toolStripClipboard2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripClipboard2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripClipboard2.Image")));
            this.toolStripClipboard2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripClipboard2.Name = "toolStripClipboard2";
            this.toolStripClipboard2.Size = new System.Drawing.Size(72, 22);
            this.toolStripClipboard2.Text = "Clipboard 2";
            this.toolStripClipboard2.Click += new System.EventHandler(this.toolStripClipboard2_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(72, 22);
            this.toolStripButton1.Text = "Clipboard 3";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click_2);
            // 
            // toolStripEnterData
            // 
            this.toolStripEnterData.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripEnterData.Image = ((System.Drawing.Image)(resources.GetObject("toolStripEnterData.Image")));
            this.toolStripEnterData.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripEnterData.Name = "toolStripEnterData";
            this.toolStripEnterData.Size = new System.Drawing.Size(106, 22);
            this.toolStripEnterData.Text = "Enter Header Data";
            this.toolStripEnterData.Visible = false;
            this.toolStripEnterData.Click += new System.EventHandler(this.toolStripEnterData_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(25, 22);
            this.toolStripButton2.Text = "C4";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // panelPrintArea3
            // 
            this.panelPrintArea3.BackColor = System.Drawing.Color.White;
            this.panelPrintArea3.Controls.Add(this.panelEventSample5);
            this.panelPrintArea3.Controls.Add(this.panel64);
            this.panelPrintArea3.Controls.Add(this.panelEventSample4);
            this.panelPrintArea3.Controls.Add(this.panel35);
            this.panelPrintArea3.Controls.Add(this.panelEventSample3);
            this.panelPrintArea3.Controls.Add(this.panel251);
            this.panelPrintArea3.Controls.Add(this.panelPage2Footer);
            this.panelPrintArea3.Controls.Add(this.panel101);
            this.panelPrintArea3.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelPrintArea3.Location = new System.Drawing.Point(99, 6130);
            this.panelPrintArea3.Margin = new System.Windows.Forms.Padding(0);
            this.panelPrintArea3.Name = "panelPrintArea3";
            this.panelPrintArea3.Size = new System.Drawing.Size(1654, 2339);
            this.panelPrintArea3.TabIndex = 3;
            // 
            // panelEventSample5
            // 
            this.panelEventSample5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panelEventSample5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelEventSample5.Controls.Add(this.panel96);
            this.panelEventSample5.Controls.Add(this.panel153);
            this.panelEventSample5.Controls.Add(this.panel290);
            this.panelEventSample5.Controls.Add(this.panel298);
            this.panelEventSample5.Controls.Add(this.panel378);
            this.panelEventSample5.Controls.Add(this.panel421);
            this.panelEventSample5.Controls.Add(this.panel453);
            this.panelEventSample5.Controls.Add(this.panel468);
            this.panelEventSample5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelEventSample5.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.panelEventSample5.Location = new System.Drawing.Point(0, 1574);
            this.panelEventSample5.Name = "panelEventSample5";
            this.panelEventSample5.Size = new System.Drawing.Size(1654, 732);
            this.panelEventSample5.TabIndex = 48;
            // 
            // panel96
            // 
            this.panel96.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel96.Controls.Add(this.panel98);
            this.panel96.Controls.Add(this.panel117);
            this.panel96.Controls.Add(this.panel92);
            this.panel96.Controls.Add(this.panel97);
            this.panel96.Controls.Add(this.panel99);
            this.panel96.Controls.Add(this.panel100);
            this.panel96.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel96.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel96.Location = new System.Drawing.Point(0, 567);
            this.panel96.Name = "panel96";
            this.panel96.Size = new System.Drawing.Size(1652, 120);
            this.panel96.TabIndex = 30;
            // 
            // panel98
            // 
            this.panel98.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel98.Controls.Add(this.textBoxActivities5);
            this.panel98.Controls.Add(this.label38);
            this.panel98.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel98.Location = new System.Drawing.Point(1412, 0);
            this.panel98.Name = "panel98";
            this.panel98.Size = new System.Drawing.Size(238, 118);
            this.panel98.TabIndex = 9;
            // 
            // textBoxActivities5
            // 
            this.textBoxActivities5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxActivities5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxActivities5.Font = new System.Drawing.Font("Verdana", 18F);
            this.textBoxActivities5.Location = new System.Drawing.Point(0, 38);
            this.textBoxActivities5.Multiline = true;
            this.textBoxActivities5.Name = "textBoxActivities5";
            this.textBoxActivities5.Size = new System.Drawing.Size(236, 78);
            this.textBoxActivities5.TabIndex = 5;
            // 
            // label38
            // 
            this.label38.Dock = System.Windows.Forms.DockStyle.Top;
            this.label38.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.label38.Location = new System.Drawing.Point(0, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(236, 38);
            this.label38.TabIndex = 4;
            this.label38.Text = "Activities:";
            // 
            // panel117
            // 
            this.panel117.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel117.Controls.Add(this.textBoxSymptoms5);
            this.panel117.Controls.Add(this.label39);
            this.panel117.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel117.Location = new System.Drawing.Point(1096, 0);
            this.panel117.Name = "panel117";
            this.panel117.Size = new System.Drawing.Size(316, 118);
            this.panel117.TabIndex = 12;
            // 
            // textBoxSymptoms5
            // 
            this.textBoxSymptoms5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxSymptoms5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxSymptoms5.Font = new System.Drawing.Font("Verdana", 18F);
            this.textBoxSymptoms5.Location = new System.Drawing.Point(0, 38);
            this.textBoxSymptoms5.Multiline = true;
            this.textBoxSymptoms5.Name = "textBoxSymptoms5";
            this.textBoxSymptoms5.Size = new System.Drawing.Size(314, 78);
            this.textBoxSymptoms5.TabIndex = 4;
            // 
            // label39
            // 
            this.label39.Dock = System.Windows.Forms.DockStyle.Top;
            this.label39.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.label39.Location = new System.Drawing.Point(0, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(314, 38);
            this.label39.TabIndex = 3;
            this.label39.Text = "Symptoms:";
            // 
            // panel92
            // 
            this.panel92.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel92.Controls.Add(this.textBoxFindings5);
            this.panel92.Controls.Add(this.panel137);
            this.panel92.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel92.Location = new System.Drawing.Point(434, 0);
            this.panel92.Name = "panel92";
            this.panel92.Size = new System.Drawing.Size(662, 118);
            this.panel92.TabIndex = 11;
            // 
            // textBoxFindings5
            // 
            this.textBoxFindings5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxFindings5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxFindings5.Font = new System.Drawing.Font("Verdana", 18F);
            this.textBoxFindings5.Location = new System.Drawing.Point(0, 38);
            this.textBoxFindings5.Multiline = true;
            this.textBoxFindings5.Name = "textBoxFindings5";
            this.textBoxFindings5.Size = new System.Drawing.Size(660, 78);
            this.textBoxFindings5.TabIndex = 1;
            this.textBoxFindings5.Text = "^regel 1\r\nregel 2\r\n";
            // 
            // panel137
            // 
            this.panel137.Controls.Add(this.labelRhythms5);
            this.panel137.Controls.Add(this.labelFindingsText5);
            this.panel137.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel137.Location = new System.Drawing.Point(0, 0);
            this.panel137.Name = "panel137";
            this.panel137.Size = new System.Drawing.Size(660, 38);
            this.panel137.TabIndex = 3;
            // 
            // labelRhythms5
            // 
            this.labelRhythms5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelRhythms5.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelRhythms5.Location = new System.Drawing.Point(212, 0);
            this.labelRhythms5.Name = "labelRhythms5";
            this.labelRhythms5.Size = new System.Drawing.Size(448, 38);
            this.labelRhythms5.TabIndex = 70;
            this.labelRhythms5.Text = "^Rhythms";
            this.labelRhythms5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelFindingsText5
            // 
            this.labelFindingsText5.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelFindingsText5.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.labelFindingsText5.Location = new System.Drawing.Point(0, 0);
            this.labelFindingsText5.Name = "labelFindingsText5";
            this.labelFindingsText5.Size = new System.Drawing.Size(212, 38);
            this.labelFindingsText5.TabIndex = 2;
            this.labelFindingsText5.Text = "^QC Findings:";
            // 
            // panel97
            // 
            this.panel97.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel97.Controls.Add(this.panel113);
            this.panel97.Controls.Add(this.panel112);
            this.panel97.Controls.Add(this.panel111);
            this.panel97.Controls.Add(this.panel102);
            this.panel97.Controls.Add(this.panel93);
            this.panel97.Controls.Add(this.panel3);
            this.panel97.Controls.Add(this.label29);
            this.panel97.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel97.Location = new System.Drawing.Point(0, 0);
            this.panel97.Name = "panel97";
            this.panel97.Size = new System.Drawing.Size(434, 118);
            this.panel97.TabIndex = 10;
            // 
            // panel113
            // 
            this.panel113.Controls.Add(this.labelQtUnit5);
            this.panel113.Controls.Add(this.labelQrsUnit5);
            this.panel113.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel113.Location = new System.Drawing.Point(381, 38);
            this.panel113.Name = "panel113";
            this.panel113.Size = new System.Drawing.Size(53, 78);
            this.panel113.TabIndex = 13;
            // 
            // labelQtUnit5
            // 
            this.labelQtUnit5.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelQtUnit5.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelQtUnit5.Location = new System.Drawing.Point(0, 40);
            this.labelQtUnit5.Name = "labelQtUnit5";
            this.labelQtUnit5.Size = new System.Drawing.Size(53, 40);
            this.labelQtUnit5.TabIndex = 70;
            this.labelQtUnit5.Text = "sec";
            this.labelQtUnit5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelQrsUnit5
            // 
            this.labelQrsUnit5.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelQrsUnit5.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelQrsUnit5.Location = new System.Drawing.Point(0, 0);
            this.labelQrsUnit5.Name = "labelQrsUnit5";
            this.labelQrsUnit5.Size = new System.Drawing.Size(53, 40);
            this.labelQrsUnit5.TabIndex = 69;
            this.labelQrsUnit5.Text = "sec";
            this.labelQrsUnit5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel112
            // 
            this.panel112.Controls.Add(this.labelQtValue5);
            this.panel112.Controls.Add(this.labelQrsValue5);
            this.panel112.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel112.Location = new System.Drawing.Point(293, 38);
            this.panel112.Name = "panel112";
            this.panel112.Size = new System.Drawing.Size(88, 78);
            this.panel112.TabIndex = 12;
            // 
            // labelQtValue5
            // 
            this.labelQtValue5.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelQtValue5.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelQtValue5.Location = new System.Drawing.Point(0, 40);
            this.labelQtValue5.Name = "labelQtValue5";
            this.labelQtValue5.Size = new System.Drawing.Size(88, 40);
            this.labelQtValue5.TabIndex = 71;
            this.labelQtValue5.Text = "^,202";
            this.labelQtValue5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelQrsValue5
            // 
            this.labelQrsValue5.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelQrsValue5.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelQrsValue5.Location = new System.Drawing.Point(0, 0);
            this.labelQrsValue5.Name = "labelQrsValue5";
            this.labelQrsValue5.Size = new System.Drawing.Size(88, 40);
            this.labelQrsValue5.TabIndex = 68;
            this.labelQrsValue5.Text = "^,202";
            this.labelQrsValue5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel111
            // 
            this.panel111.Controls.Add(this.labelQtText5);
            this.panel111.Controls.Add(this.labelQrsText5);
            this.panel111.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel111.Location = new System.Drawing.Point(214, 38);
            this.panel111.Name = "panel111";
            this.panel111.Size = new System.Drawing.Size(79, 78);
            this.panel111.TabIndex = 11;
            // 
            // labelQtText5
            // 
            this.labelQtText5.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelQtText5.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelQtText5.Location = new System.Drawing.Point(0, 40);
            this.labelQtText5.Name = "labelQtText5";
            this.labelQtText5.Size = new System.Drawing.Size(79, 40);
            this.labelQtText5.TabIndex = 72;
            this.labelQtText5.Text = "QT:";
            this.labelQtText5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelQrsText5
            // 
            this.labelQrsText5.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelQrsText5.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelQrsText5.Location = new System.Drawing.Point(0, 0);
            this.labelQrsText5.Name = "labelQrsText5";
            this.labelQrsText5.Size = new System.Drawing.Size(79, 40);
            this.labelQrsText5.TabIndex = 67;
            this.labelQrsText5.Text = "QRS:";
            this.labelQrsText5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel102
            // 
            this.panel102.Controls.Add(this.labelPrUnit5);
            this.panel102.Controls.Add(this.labelMeanHrUnit5);
            this.panel102.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel102.Location = new System.Drawing.Point(146, 38);
            this.panel102.Name = "panel102";
            this.panel102.Size = new System.Drawing.Size(68, 78);
            this.panel102.TabIndex = 10;
            // 
            // labelPrUnit5
            // 
            this.labelPrUnit5.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPrUnit5.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelPrUnit5.Location = new System.Drawing.Point(0, 40);
            this.labelPrUnit5.Name = "labelPrUnit5";
            this.labelPrUnit5.Size = new System.Drawing.Size(68, 40);
            this.labelPrUnit5.TabIndex = 63;
            this.labelPrUnit5.Text = "sec";
            this.labelPrUnit5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelMeanHrUnit5
            // 
            this.labelMeanHrUnit5.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelMeanHrUnit5.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelMeanHrUnit5.Location = new System.Drawing.Point(0, 0);
            this.labelMeanHrUnit5.Name = "labelMeanHrUnit5";
            this.labelMeanHrUnit5.Size = new System.Drawing.Size(68, 40);
            this.labelMeanHrUnit5.TabIndex = 61;
            this.labelMeanHrUnit5.Text = "bpm";
            this.labelMeanHrUnit5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel93
            // 
            this.panel93.Controls.Add(this.labelPrValue5);
            this.panel93.Controls.Add(this.labelMeanHrValue5);
            this.panel93.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel93.Location = new System.Drawing.Point(59, 38);
            this.panel93.Name = "panel93";
            this.panel93.Size = new System.Drawing.Size(87, 78);
            this.panel93.TabIndex = 9;
            // 
            // labelPrValue5
            // 
            this.labelPrValue5.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPrValue5.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelPrValue5.Location = new System.Drawing.Point(0, 40);
            this.labelPrValue5.Name = "labelPrValue5";
            this.labelPrValue5.Size = new System.Drawing.Size(87, 40);
            this.labelPrValue5.TabIndex = 64;
            this.labelPrValue5.Text = "^,202";
            this.labelPrValue5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelMeanHrValue5
            // 
            this.labelMeanHrValue5.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelMeanHrValue5.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelMeanHrValue5.Location = new System.Drawing.Point(0, 0);
            this.labelMeanHrValue5.Name = "labelMeanHrValue5";
            this.labelMeanHrValue5.Size = new System.Drawing.Size(87, 40);
            this.labelMeanHrValue5.TabIndex = 59;
            this.labelMeanHrValue5.Text = "120";
            this.labelMeanHrValue5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.labelPrText5);
            this.panel3.Controls.Add(this.labelMeanHrText5);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 38);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(59, 78);
            this.panel3.TabIndex = 8;
            // 
            // labelPrText5
            // 
            this.labelPrText5.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPrText5.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelPrText5.Location = new System.Drawing.Point(0, 40);
            this.labelPrText5.Name = "labelPrText5";
            this.labelPrText5.Size = new System.Drawing.Size(59, 40);
            this.labelPrText5.TabIndex = 62;
            this.labelPrText5.Text = "PR:";
            this.labelPrText5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelMeanHrText5
            // 
            this.labelMeanHrText5.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelMeanHrText5.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelMeanHrText5.Location = new System.Drawing.Point(0, 0);
            this.labelMeanHrText5.Name = "labelMeanHrText5";
            this.labelMeanHrText5.Size = new System.Drawing.Size(59, 40);
            this.labelMeanHrText5.TabIndex = 58;
            this.labelMeanHrText5.Text = "HR:";
            this.labelMeanHrText5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label29
            // 
            this.label29.Dock = System.Windows.Forms.DockStyle.Top;
            this.label29.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.label29.Location = new System.Drawing.Point(0, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(432, 38);
            this.label29.TabIndex = 7;
            this.label29.Text = "QRS Measurements:";
            // 
            // panel99
            // 
            this.panel99.AutoSize = true;
            this.panel99.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel99.Location = new System.Drawing.Point(0, 0);
            this.panel99.Name = "panel99";
            this.panel99.Size = new System.Drawing.Size(0, 118);
            this.panel99.TabIndex = 6;
            // 
            // panel100
            // 
            this.panel100.AutoSize = true;
            this.panel100.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel100.Location = new System.Drawing.Point(0, 0);
            this.panel100.Name = "panel100";
            this.panel100.Size = new System.Drawing.Size(0, 118);
            this.panel100.TabIndex = 3;
            // 
            // panel153
            // 
            this.panel153.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel153.Controls.Add(this.labelSample5AmplFull);
            this.panel153.Controls.Add(this.labelSample5SweepFull);
            this.panel153.Controls.Add(this.labelSample5EndFull);
            this.panel153.Controls.Add(this.labelSample5MidFull);
            this.panel153.Controls.Add(this.labelSample5StartFull);
            this.panel153.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel153.Location = new System.Drawing.Point(0, 518);
            this.panel153.Name = "panel153";
            this.panel153.Size = new System.Drawing.Size(1652, 49);
            this.panel153.TabIndex = 26;
            // 
            // labelSample5AmplFull
            // 
            this.labelSample5AmplFull.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample5AmplFull.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample5AmplFull.Location = new System.Drawing.Point(847, 0);
            this.labelSample5AmplFull.Name = "labelSample5AmplFull";
            this.labelSample5AmplFull.Size = new System.Drawing.Size(310, 47);
            this.labelSample5AmplFull.TabIndex = 12;
            this.labelSample5AmplFull.Text = "10 mm/mV (0.50 mV)";
            this.labelSample5AmplFull.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample5SweepFull
            // 
            this.labelSample5SweepFull.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample5SweepFull.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample5SweepFull.Location = new System.Drawing.Point(1157, 0);
            this.labelSample5SweepFull.Name = "labelSample5SweepFull";
            this.labelSample5SweepFull.Size = new System.Drawing.Size(302, 47);
            this.labelSample5SweepFull.TabIndex = 11;
            this.labelSample5SweepFull.Text = "25 mm/sec (0.20 Sec)";
            this.labelSample5SweepFull.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample5EndFull
            // 
            this.labelSample5EndFull.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample5EndFull.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample5EndFull.Location = new System.Drawing.Point(1459, 0);
            this.labelSample5EndFull.Name = "labelSample5EndFull";
            this.labelSample5EndFull.Size = new System.Drawing.Size(191, 47);
            this.labelSample5EndFull.TabIndex = 10;
            this.labelSample5EndFull.Text = "10:15:30 AM";
            this.labelSample5EndFull.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelSample5MidFull
            // 
            this.labelSample5MidFull.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSample5MidFull.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample5MidFull.Location = new System.Drawing.Point(475, 0);
            this.labelSample5MidFull.Name = "labelSample5MidFull";
            this.labelSample5MidFull.Size = new System.Drawing.Size(191, 47);
            this.labelSample5MidFull.TabIndex = 9;
            this.labelSample5MidFull.Text = "10:15:00 AM";
            this.labelSample5MidFull.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelSample5MidFull.Visible = false;
            // 
            // labelSample5StartFull
            // 
            this.labelSample5StartFull.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSample5StartFull.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample5StartFull.Location = new System.Drawing.Point(0, 0);
            this.labelSample5StartFull.Name = "labelSample5StartFull";
            this.labelSample5StartFull.Size = new System.Drawing.Size(475, 47);
            this.labelSample5StartFull.TabIndex = 8;
            this.labelSample5StartFull.Text = "^10:14:30 AM 99/99/9999 9999 sec";
            this.labelSample5StartFull.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel290
            // 
            this.panel290.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel290.Controls.Add(this.panel291);
            this.panel290.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel290.Location = new System.Drawing.Point(0, 298);
            this.panel290.Name = "panel290";
            this.panel290.Size = new System.Drawing.Size(1652, 220);
            this.panel290.TabIndex = 25;
            // 
            // panel291
            // 
            this.panel291.Controls.Add(this.labelSample5LeadFull);
            this.panel291.Controls.Add(this.pictureBoxSample5Full);
            this.panel291.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel291.Location = new System.Drawing.Point(0, 0);
            this.panel291.Name = "panel291";
            this.panel291.Size = new System.Drawing.Size(1650, 218);
            this.panel291.TabIndex = 2;
            // 
            // labelSample5LeadFull
            // 
            this.labelSample5LeadFull.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSample5LeadFull.Location = new System.Drawing.Point(59, 34);
            this.labelSample5LeadFull.Name = "labelSample5LeadFull";
            this.labelSample5LeadFull.Size = new System.Drawing.Size(180, 45);
            this.labelSample5LeadFull.TabIndex = 47;
            this.labelSample5LeadFull.Text = "LEAD III";
            this.labelSample5LeadFull.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelSample5LeadFull.Visible = false;
            // 
            // pictureBoxSample5Full
            // 
            this.pictureBoxSample5Full.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxSample5Full.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxSample5Full.Name = "pictureBoxSample5Full";
            this.pictureBoxSample5Full.Size = new System.Drawing.Size(1650, 218);
            this.pictureBoxSample5Full.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxSample5Full.TabIndex = 0;
            this.pictureBoxSample5Full.TabStop = false;
            // 
            // panel298
            // 
            this.panel298.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel298.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel298.Location = new System.Drawing.Point(0, 296);
            this.panel298.Name = "panel298";
            this.panel298.Size = new System.Drawing.Size(1652, 2);
            this.panel298.TabIndex = 20;
            // 
            // panel378
            // 
            this.panel378.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel378.Controls.Add(this.labelSample5Ampl);
            this.panel378.Controls.Add(this.labelSample5Sweep);
            this.panel378.Controls.Add(this.labelSample5End);
            this.panel378.Controls.Add(this.labelSample5Mid);
            this.panel378.Controls.Add(this.labelSample5Start);
            this.panel378.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel378.Location = new System.Drawing.Point(0, 261);
            this.panel378.Name = "panel378";
            this.panel378.Size = new System.Drawing.Size(1652, 35);
            this.panel378.TabIndex = 11;
            // 
            // labelSample5Ampl
            // 
            this.labelSample5Ampl.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample5Ampl.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample5Ampl.Location = new System.Drawing.Point(848, 0);
            this.labelSample5Ampl.Name = "labelSample5Ampl";
            this.labelSample5Ampl.Size = new System.Drawing.Size(309, 33);
            this.labelSample5Ampl.TabIndex = 9;
            this.labelSample5Ampl.Text = "10 mm/mV (0.50 mV)";
            this.labelSample5Ampl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample5Sweep
            // 
            this.labelSample5Sweep.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample5Sweep.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample5Sweep.Location = new System.Drawing.Point(1157, 0);
            this.labelSample5Sweep.Name = "labelSample5Sweep";
            this.labelSample5Sweep.Size = new System.Drawing.Size(303, 33);
            this.labelSample5Sweep.TabIndex = 10;
            this.labelSample5Sweep.Text = "25 mm/sec (0.20 Sec)";
            this.labelSample5Sweep.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample5End
            // 
            this.labelSample5End.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample5End.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample5End.Location = new System.Drawing.Point(1460, 0);
            this.labelSample5End.Name = "labelSample5End";
            this.labelSample5End.Size = new System.Drawing.Size(190, 33);
            this.labelSample5End.TabIndex = 8;
            this.labelSample5End.Text = "10:15:20 AM";
            this.labelSample5End.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelSample5Mid
            // 
            this.labelSample5Mid.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSample5Mid.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample5Mid.Location = new System.Drawing.Point(470, 0);
            this.labelSample5Mid.Name = "labelSample5Mid";
            this.labelSample5Mid.Size = new System.Drawing.Size(197, 33);
            this.labelSample5Mid.TabIndex = 7;
            this.labelSample5Mid.Text = "10:15:15 AM";
            this.labelSample5Mid.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelSample5Mid.Visible = false;
            // 
            // labelSample5Start
            // 
            this.labelSample5Start.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSample5Start.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample5Start.Location = new System.Drawing.Point(0, 0);
            this.labelSample5Start.Name = "labelSample5Start";
            this.labelSample5Start.Size = new System.Drawing.Size(470, 33);
            this.labelSample5Start.TabIndex = 5;
            this.labelSample5Start.Text = "^10:14:30 AM 99/99/9999 9999 sec";
            this.labelSample5Start.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel421
            // 
            this.panel421.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel421.Controls.Add(this.panel422);
            this.panel421.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel421.Location = new System.Drawing.Point(0, 41);
            this.panel421.Name = "panel421";
            this.panel421.Size = new System.Drawing.Size(1652, 220);
            this.panel421.TabIndex = 10;
            // 
            // panel422
            // 
            this.panel422.Controls.Add(this.labelSample5Lead);
            this.panel422.Controls.Add(this.pictureBoxSample5);
            this.panel422.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel422.Location = new System.Drawing.Point(0, 0);
            this.panel422.Name = "panel422";
            this.panel422.Size = new System.Drawing.Size(1650, 218);
            this.panel422.TabIndex = 2;
            // 
            // labelSample5Lead
            // 
            this.labelSample5Lead.Font = new System.Drawing.Font("Verdana", 28F);
            this.labelSample5Lead.Location = new System.Drawing.Point(108, 61);
            this.labelSample5Lead.Name = "labelSample5Lead";
            this.labelSample5Lead.Size = new System.Drawing.Size(186, 49);
            this.labelSample5Lead.TabIndex = 46;
            this.labelSample5Lead.Text = "LEAD III";
            this.labelSample5Lead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelSample5Lead.Visible = false;
            // 
            // pictureBoxSample5
            // 
            this.pictureBoxSample5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxSample5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxSample5.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxSample5.InitialImage")));
            this.pictureBoxSample5.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxSample5.Name = "pictureBoxSample5";
            this.pictureBoxSample5.Size = new System.Drawing.Size(1650, 218);
            this.pictureBoxSample5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxSample5.TabIndex = 0;
            this.pictureBoxSample5.TabStop = false;
            // 
            // panel453
            // 
            this.panel453.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel453.Controls.Add(this.label144);
            this.panel453.Controls.Add(this.label147);
            this.panel453.Controls.Add(this.labelSample5EventNr);
            this.panel453.Controls.Add(this.labelEvent5);
            this.panel453.Controls.Add(this.labelSample5Date);
            this.panel453.Controls.Add(this.label142);
            this.panel453.Controls.Add(this.labelSample5Time);
            this.panel453.Controls.Add(this.labelSampleType5);
            this.panel453.Controls.Add(this.labelClassification5);
            this.panel453.Controls.Add(this.labelSample5Nr);
            this.panel453.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel453.Location = new System.Drawing.Point(0, 1);
            this.panel453.Name = "panel453";
            this.panel453.Size = new System.Drawing.Size(1652, 40);
            this.panel453.TabIndex = 7;
            // 
            // label144
            // 
            this.label144.Dock = System.Windows.Forms.DockStyle.Left;
            this.label144.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold);
            this.label144.Location = new System.Drawing.Point(1128, 0);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(20, 38);
            this.label144.TabIndex = 21;
            this.label144.Text = "-";
            this.label144.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label144.Visible = false;
            // 
            // label147
            // 
            this.label147.Dock = System.Windows.Forms.DockStyle.Left;
            this.label147.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold);
            this.label147.Location = new System.Drawing.Point(1108, 0);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(20, 38);
            this.label147.TabIndex = 19;
            this.label147.Text = "-";
            this.label147.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label147.Visible = false;
            // 
            // labelSample5EventNr
            // 
            this.labelSample5EventNr.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSample5EventNr.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold);
            this.labelSample5EventNr.Location = new System.Drawing.Point(953, 0);
            this.labelSample5EventNr.Name = "labelSample5EventNr";
            this.labelSample5EventNr.Size = new System.Drawing.Size(155, 38);
            this.labelSample5EventNr.TabIndex = 18;
            this.labelSample5EventNr.Text = "^9999";
            this.labelSample5EventNr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelEvent5
            // 
            this.labelEvent5.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelEvent5.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold);
            this.labelEvent5.Location = new System.Drawing.Point(817, 0);
            this.labelEvent5.Name = "labelEvent5";
            this.labelEvent5.Size = new System.Drawing.Size(136, 38);
            this.labelEvent5.TabIndex = 17;
            this.labelEvent5.Text = "Event";
            this.labelEvent5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelSample5Date
            // 
            this.labelSample5Date.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample5Date.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelSample5Date.Location = new System.Drawing.Point(1237, 0);
            this.labelSample5Date.Name = "labelSample5Date";
            this.labelSample5Date.Size = new System.Drawing.Size(190, 38);
            this.labelSample5Date.TabIndex = 24;
            this.labelSample5Date.Text = "^9/99/9999";
            this.labelSample5Date.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label142
            // 
            this.label142.Dock = System.Windows.Forms.DockStyle.Right;
            this.label142.Font = new System.Drawing.Font("Verdana", 20F);
            this.label142.Location = new System.Drawing.Point(1427, 0);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(20, 38);
            this.label142.TabIndex = 23;
            this.label142.Text = "-";
            this.label142.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelSample5Time
            // 
            this.labelSample5Time.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample5Time.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelSample5Time.Location = new System.Drawing.Point(1447, 0);
            this.labelSample5Time.Name = "labelSample5Time";
            this.labelSample5Time.Size = new System.Drawing.Size(203, 38);
            this.labelSample5Time.TabIndex = 22;
            this.labelSample5Time.Text = "10:15:10 AM";
            this.labelSample5Time.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSampleType5
            // 
            this.labelSampleType5.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSampleType5.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold);
            this.labelSampleType5.Location = new System.Drawing.Point(577, 0);
            this.labelSampleType5.Name = "labelSampleType5";
            this.labelSampleType5.Size = new System.Drawing.Size(240, 38);
            this.labelSampleType5.TabIndex = 20;
            this.labelSampleType5.Text = "^Automatic";
            this.labelSampleType5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelClassification5
            // 
            this.labelClassification5.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelClassification5.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold);
            this.labelClassification5.Location = new System.Drawing.Point(100, 0);
            this.labelClassification5.Name = "labelClassification5";
            this.labelClassification5.Size = new System.Drawing.Size(477, 38);
            this.labelClassification5.TabIndex = 16;
            this.labelClassification5.Text = "Classification";
            this.labelClassification5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample5Nr
            // 
            this.labelSample5Nr.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSample5Nr.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold);
            this.labelSample5Nr.Location = new System.Drawing.Point(0, 0);
            this.labelSample5Nr.Name = "labelSample5Nr";
            this.labelSample5Nr.Size = new System.Drawing.Size(100, 38);
            this.labelSample5Nr.TabIndex = 15;
            this.labelSample5Nr.Text = "05";
            this.labelSample5Nr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel468
            // 
            this.panel468.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel468.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel468.Location = new System.Drawing.Point(0, 0);
            this.panel468.Name = "panel468";
            this.panel468.Size = new System.Drawing.Size(1652, 1);
            this.panel468.TabIndex = 3;
            // 
            // panel64
            // 
            this.panel64.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel64.Location = new System.Drawing.Point(0, 1564);
            this.panel64.Name = "panel64";
            this.panel64.Size = new System.Drawing.Size(1654, 10);
            this.panel64.TabIndex = 47;
            // 
            // panelEventSample4
            // 
            this.panelEventSample4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panelEventSample4.Controls.Add(this.panel114);
            this.panelEventSample4.Controls.Add(this.panel266);
            this.panelEventSample4.Controls.Add(this.panel275);
            this.panelEventSample4.Controls.Add(this.panel305);
            this.panelEventSample4.Controls.Add(this.panel306);
            this.panelEventSample4.Controls.Add(this.panel328);
            this.panelEventSample4.Controls.Add(this.panel357);
            this.panelEventSample4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelEventSample4.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.panelEventSample4.Location = new System.Drawing.Point(0, 889);
            this.panelEventSample4.Name = "panelEventSample4";
            this.panelEventSample4.Size = new System.Drawing.Size(1654, 675);
            this.panelEventSample4.TabIndex = 46;
            // 
            // panel114
            // 
            this.panel114.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel114.Controls.Add(this.panel94);
            this.panel114.Controls.Add(this.panel63);
            this.panel114.Controls.Add(this.panel19);
            this.panel114.Controls.Add(this.panel95);
            this.panel114.Controls.Add(this.panel115);
            this.panel114.Controls.Add(this.panel116);
            this.panel114.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel114.Location = new System.Drawing.Point(0, 552);
            this.panel114.Name = "panel114";
            this.panel114.Size = new System.Drawing.Size(1654, 123);
            this.panel114.TabIndex = 28;
            // 
            // panel94
            // 
            this.panel94.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel94.Controls.Add(this.textBoxActivities4);
            this.panel94.Controls.Add(this.label33);
            this.panel94.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel94.Location = new System.Drawing.Point(1413, 0);
            this.panel94.Name = "panel94";
            this.panel94.Size = new System.Drawing.Size(239, 121);
            this.panel94.TabIndex = 9;
            // 
            // textBoxActivities4
            // 
            this.textBoxActivities4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxActivities4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxActivities4.Font = new System.Drawing.Font("Verdana", 18F);
            this.textBoxActivities4.Location = new System.Drawing.Point(0, 38);
            this.textBoxActivities4.Multiline = true;
            this.textBoxActivities4.Name = "textBoxActivities4";
            this.textBoxActivities4.Size = new System.Drawing.Size(237, 81);
            this.textBoxActivities4.TabIndex = 5;
            // 
            // label33
            // 
            this.label33.Dock = System.Windows.Forms.DockStyle.Top;
            this.label33.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.label33.Location = new System.Drawing.Point(0, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(237, 38);
            this.label33.TabIndex = 4;
            this.label33.Text = "Activities:";
            // 
            // panel63
            // 
            this.panel63.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel63.Controls.Add(this.textBoxSymptoms4);
            this.panel63.Controls.Add(this.label35);
            this.panel63.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel63.Location = new System.Drawing.Point(1096, 0);
            this.panel63.Name = "panel63";
            this.panel63.Size = new System.Drawing.Size(317, 121);
            this.panel63.TabIndex = 12;
            // 
            // textBoxSymptoms4
            // 
            this.textBoxSymptoms4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxSymptoms4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxSymptoms4.Font = new System.Drawing.Font("Verdana", 18F);
            this.textBoxSymptoms4.Location = new System.Drawing.Point(0, 38);
            this.textBoxSymptoms4.Multiline = true;
            this.textBoxSymptoms4.Name = "textBoxSymptoms4";
            this.textBoxSymptoms4.Size = new System.Drawing.Size(315, 81);
            this.textBoxSymptoms4.TabIndex = 4;
            // 
            // label35
            // 
            this.label35.Dock = System.Windows.Forms.DockStyle.Top;
            this.label35.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.label35.Location = new System.Drawing.Point(0, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(315, 38);
            this.label35.TabIndex = 3;
            this.label35.Text = "Symptoms:";
            // 
            // panel19
            // 
            this.panel19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel19.Controls.Add(this.textBoxFindings4);
            this.panel19.Controls.Add(this.panel119);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel19.Location = new System.Drawing.Point(435, 0);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(661, 121);
            this.panel19.TabIndex = 11;
            // 
            // textBoxFindings4
            // 
            this.textBoxFindings4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxFindings4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxFindings4.Font = new System.Drawing.Font("Verdana", 18F);
            this.textBoxFindings4.Location = new System.Drawing.Point(0, 38);
            this.textBoxFindings4.Multiline = true;
            this.textBoxFindings4.Name = "textBoxFindings4";
            this.textBoxFindings4.Size = new System.Drawing.Size(659, 81);
            this.textBoxFindings4.TabIndex = 1;
            this.textBoxFindings4.Text = "^regel 1\r\nregel 2\r\n";
            // 
            // panel119
            // 
            this.panel119.Controls.Add(this.labelRhythms4);
            this.panel119.Controls.Add(this.labelFindingsText4);
            this.panel119.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel119.Location = new System.Drawing.Point(0, 0);
            this.panel119.Name = "panel119";
            this.panel119.Size = new System.Drawing.Size(659, 38);
            this.panel119.TabIndex = 3;
            // 
            // labelRhythms4
            // 
            this.labelRhythms4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelRhythms4.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelRhythms4.Location = new System.Drawing.Point(212, 0);
            this.labelRhythms4.Name = "labelRhythms4";
            this.labelRhythms4.Size = new System.Drawing.Size(447, 38);
            this.labelRhythms4.TabIndex = 70;
            this.labelRhythms4.Text = "^Rhythms";
            this.labelRhythms4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelFindingsText4
            // 
            this.labelFindingsText4.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelFindingsText4.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.labelFindingsText4.Location = new System.Drawing.Point(0, 0);
            this.labelFindingsText4.Name = "labelFindingsText4";
            this.labelFindingsText4.Size = new System.Drawing.Size(212, 38);
            this.labelFindingsText4.TabIndex = 2;
            this.labelFindingsText4.Text = "^QC Findings:";
            // 
            // panel95
            // 
            this.panel95.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel95.Controls.Add(this.panel49);
            this.panel95.Controls.Add(this.panel48);
            this.panel95.Controls.Add(this.panel43);
            this.panel95.Controls.Add(this.panel33);
            this.panel95.Controls.Add(this.panel26);
            this.panel95.Controls.Add(this.panel23);
            this.panel95.Controls.Add(this.label24);
            this.panel95.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel95.Location = new System.Drawing.Point(0, 0);
            this.panel95.Name = "panel95";
            this.panel95.Size = new System.Drawing.Size(435, 121);
            this.panel95.TabIndex = 10;
            // 
            // panel49
            // 
            this.panel49.Controls.Add(this.labelQtUnit4);
            this.panel49.Controls.Add(this.labelQrsUnit4);
            this.panel49.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel49.Location = new System.Drawing.Point(376, 38);
            this.panel49.Name = "panel49";
            this.panel49.Size = new System.Drawing.Size(58, 81);
            this.panel49.TabIndex = 12;
            // 
            // labelQtUnit4
            // 
            this.labelQtUnit4.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelQtUnit4.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelQtUnit4.Location = new System.Drawing.Point(0, 40);
            this.labelQtUnit4.Name = "labelQtUnit4";
            this.labelQtUnit4.Size = new System.Drawing.Size(58, 40);
            this.labelQtUnit4.TabIndex = 70;
            this.labelQtUnit4.Text = "sec";
            this.labelQtUnit4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelQrsUnit4
            // 
            this.labelQrsUnit4.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelQrsUnit4.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelQrsUnit4.Location = new System.Drawing.Point(0, 0);
            this.labelQrsUnit4.Name = "labelQrsUnit4";
            this.labelQrsUnit4.Size = new System.Drawing.Size(58, 40);
            this.labelQrsUnit4.TabIndex = 69;
            this.labelQrsUnit4.Text = "sec";
            this.labelQrsUnit4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelQrsUnit4.Click += new System.EventHandler(this.label53_Click_1);
            // 
            // panel48
            // 
            this.panel48.Controls.Add(this.labelQtValue4);
            this.panel48.Controls.Add(this.labelQrsValue4);
            this.panel48.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel48.Location = new System.Drawing.Point(288, 38);
            this.panel48.Name = "panel48";
            this.panel48.Size = new System.Drawing.Size(88, 81);
            this.panel48.TabIndex = 11;
            // 
            // labelQtValue4
            // 
            this.labelQtValue4.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelQtValue4.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelQtValue4.Location = new System.Drawing.Point(0, 40);
            this.labelQtValue4.Name = "labelQtValue4";
            this.labelQtValue4.Size = new System.Drawing.Size(88, 40);
            this.labelQtValue4.TabIndex = 71;
            this.labelQtValue4.Text = "^,202";
            this.labelQtValue4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelQrsValue4
            // 
            this.labelQrsValue4.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelQrsValue4.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelQrsValue4.Location = new System.Drawing.Point(0, 0);
            this.labelQrsValue4.Name = "labelQrsValue4";
            this.labelQrsValue4.Size = new System.Drawing.Size(88, 40);
            this.labelQrsValue4.TabIndex = 68;
            this.labelQrsValue4.Text = "^,202";
            this.labelQrsValue4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel43
            // 
            this.panel43.Controls.Add(this.labelQtText4);
            this.panel43.Controls.Add(this.labelQrsText4);
            this.panel43.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel43.Location = new System.Drawing.Point(212, 38);
            this.panel43.Name = "panel43";
            this.panel43.Size = new System.Drawing.Size(76, 81);
            this.panel43.TabIndex = 10;
            // 
            // labelQtText4
            // 
            this.labelQtText4.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelQtText4.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelQtText4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelQtText4.Location = new System.Drawing.Point(0, 40);
            this.labelQtText4.Name = "labelQtText4";
            this.labelQtText4.Size = new System.Drawing.Size(76, 40);
            this.labelQtText4.TabIndex = 72;
            this.labelQtText4.Text = "QT:";
            this.labelQtText4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelQrsText4
            // 
            this.labelQrsText4.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelQrsText4.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelQrsText4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelQrsText4.Location = new System.Drawing.Point(0, 0);
            this.labelQrsText4.Name = "labelQrsText4";
            this.labelQrsText4.Size = new System.Drawing.Size(76, 40);
            this.labelQrsText4.TabIndex = 67;
            this.labelQrsText4.Text = "QRS:";
            this.labelQrsText4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel33
            // 
            this.panel33.Controls.Add(this.labelPrUnit4);
            this.panel33.Controls.Add(this.labelMeanHrUnit4);
            this.panel33.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel33.Location = new System.Drawing.Point(146, 38);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(66, 81);
            this.panel33.TabIndex = 9;
            // 
            // labelPrUnit4
            // 
            this.labelPrUnit4.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPrUnit4.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelPrUnit4.Location = new System.Drawing.Point(0, 40);
            this.labelPrUnit4.Name = "labelPrUnit4";
            this.labelPrUnit4.Size = new System.Drawing.Size(66, 40);
            this.labelPrUnit4.TabIndex = 63;
            this.labelPrUnit4.Text = "sec";
            this.labelPrUnit4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelMeanHrUnit4
            // 
            this.labelMeanHrUnit4.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelMeanHrUnit4.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelMeanHrUnit4.Location = new System.Drawing.Point(0, 0);
            this.labelMeanHrUnit4.Name = "labelMeanHrUnit4";
            this.labelMeanHrUnit4.Size = new System.Drawing.Size(66, 40);
            this.labelMeanHrUnit4.TabIndex = 61;
            this.labelMeanHrUnit4.Text = "bpm";
            this.labelMeanHrUnit4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel26
            // 
            this.panel26.Controls.Add(this.labelPrValue4);
            this.panel26.Controls.Add(this.labelMeanHrValue4);
            this.panel26.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel26.Location = new System.Drawing.Point(59, 38);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(87, 81);
            this.panel26.TabIndex = 8;
            // 
            // labelPrValue4
            // 
            this.labelPrValue4.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPrValue4.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelPrValue4.Location = new System.Drawing.Point(0, 40);
            this.labelPrValue4.Name = "labelPrValue4";
            this.labelPrValue4.Size = new System.Drawing.Size(87, 40);
            this.labelPrValue4.TabIndex = 64;
            this.labelPrValue4.Text = "^,202";
            this.labelPrValue4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelMeanHrValue4
            // 
            this.labelMeanHrValue4.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelMeanHrValue4.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelMeanHrValue4.Location = new System.Drawing.Point(0, 0);
            this.labelMeanHrValue4.Name = "labelMeanHrValue4";
            this.labelMeanHrValue4.Size = new System.Drawing.Size(87, 40);
            this.labelMeanHrValue4.TabIndex = 59;
            this.labelMeanHrValue4.Text = "120";
            this.labelMeanHrValue4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel23
            // 
            this.panel23.Controls.Add(this.labelPrText4);
            this.panel23.Controls.Add(this.labelMeanHrText4);
            this.panel23.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel23.Location = new System.Drawing.Point(0, 38);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(59, 81);
            this.panel23.TabIndex = 7;
            // 
            // labelPrText4
            // 
            this.labelPrText4.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPrText4.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelPrText4.Location = new System.Drawing.Point(0, 40);
            this.labelPrText4.Name = "labelPrText4";
            this.labelPrText4.Size = new System.Drawing.Size(59, 40);
            this.labelPrText4.TabIndex = 62;
            this.labelPrText4.Text = "PR:";
            this.labelPrText4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelMeanHrText4
            // 
            this.labelMeanHrText4.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelMeanHrText4.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelMeanHrText4.Location = new System.Drawing.Point(0, 0);
            this.labelMeanHrText4.Name = "labelMeanHrText4";
            this.labelMeanHrText4.Size = new System.Drawing.Size(59, 40);
            this.labelMeanHrText4.TabIndex = 58;
            this.labelMeanHrText4.Text = "HR:";
            this.labelMeanHrText4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label24
            // 
            this.label24.Dock = System.Windows.Forms.DockStyle.Top;
            this.label24.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.label24.Location = new System.Drawing.Point(0, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(433, 38);
            this.label24.TabIndex = 6;
            this.label24.Text = "QRS Measurements:";
            // 
            // panel115
            // 
            this.panel115.AutoSize = true;
            this.panel115.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel115.Location = new System.Drawing.Point(0, 0);
            this.panel115.Name = "panel115";
            this.panel115.Size = new System.Drawing.Size(0, 121);
            this.panel115.TabIndex = 6;
            // 
            // panel116
            // 
            this.panel116.AutoSize = true;
            this.panel116.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel116.Location = new System.Drawing.Point(0, 0);
            this.panel116.Name = "panel116";
            this.panel116.Size = new System.Drawing.Size(0, 121);
            this.panel116.TabIndex = 3;
            // 
            // panel266
            // 
            this.panel266.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel266.Controls.Add(this.labelSample4AmplFull);
            this.panel266.Controls.Add(this.labelSample4SweepFull);
            this.panel266.Controls.Add(this.labelSample4MidFull);
            this.panel266.Controls.Add(this.labelSample4StartFull);
            this.panel266.Controls.Add(this.labelSample4EndFull);
            this.panel266.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel266.Location = new System.Drawing.Point(0, 517);
            this.panel266.Name = "panel266";
            this.panel266.Size = new System.Drawing.Size(1654, 35);
            this.panel266.TabIndex = 26;
            // 
            // labelSample4AmplFull
            // 
            this.labelSample4AmplFull.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample4AmplFull.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample4AmplFull.Location = new System.Drawing.Point(844, 0);
            this.labelSample4AmplFull.Name = "labelSample4AmplFull";
            this.labelSample4AmplFull.Size = new System.Drawing.Size(310, 33);
            this.labelSample4AmplFull.TabIndex = 11;
            this.labelSample4AmplFull.Text = "10 mm/mV (0.50 mV)";
            this.labelSample4AmplFull.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample4SweepFull
            // 
            this.labelSample4SweepFull.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample4SweepFull.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample4SweepFull.Location = new System.Drawing.Point(1154, 0);
            this.labelSample4SweepFull.Name = "labelSample4SweepFull";
            this.labelSample4SweepFull.Size = new System.Drawing.Size(302, 33);
            this.labelSample4SweepFull.TabIndex = 12;
            this.labelSample4SweepFull.Text = "25 mm/sec (0.20 Sec)";
            this.labelSample4SweepFull.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample4MidFull
            // 
            this.labelSample4MidFull.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSample4MidFull.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample4MidFull.Location = new System.Drawing.Point(470, 0);
            this.labelSample4MidFull.Name = "labelSample4MidFull";
            this.labelSample4MidFull.Size = new System.Drawing.Size(197, 33);
            this.labelSample4MidFull.TabIndex = 10;
            this.labelSample4MidFull.Text = "10:15:00 AM";
            this.labelSample4MidFull.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelSample4MidFull.Visible = false;
            // 
            // labelSample4StartFull
            // 
            this.labelSample4StartFull.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSample4StartFull.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample4StartFull.Location = new System.Drawing.Point(0, 0);
            this.labelSample4StartFull.Name = "labelSample4StartFull";
            this.labelSample4StartFull.Size = new System.Drawing.Size(470, 33);
            this.labelSample4StartFull.TabIndex = 9;
            this.labelSample4StartFull.Text = "^10:14:30 AM 99/99/9999 9999 sec";
            this.labelSample4StartFull.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample4EndFull
            // 
            this.labelSample4EndFull.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample4EndFull.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample4EndFull.Location = new System.Drawing.Point(1456, 0);
            this.labelSample4EndFull.Name = "labelSample4EndFull";
            this.labelSample4EndFull.Size = new System.Drawing.Size(196, 33);
            this.labelSample4EndFull.TabIndex = 8;
            this.labelSample4EndFull.Text = "10:15:30 AM";
            this.labelSample4EndFull.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel275
            // 
            this.panel275.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel275.Controls.Add(this.pictureBoxSample4Full);
            this.panel275.Controls.Add(this.labelSample4LeadFull);
            this.panel275.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel275.Location = new System.Drawing.Point(0, 297);
            this.panel275.Name = "panel275";
            this.panel275.Size = new System.Drawing.Size(1654, 220);
            this.panel275.TabIndex = 25;
            // 
            // pictureBoxSample4Full
            // 
            this.pictureBoxSample4Full.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxSample4Full.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxSample4Full.Name = "pictureBoxSample4Full";
            this.pictureBoxSample4Full.Size = new System.Drawing.Size(1652, 218);
            this.pictureBoxSample4Full.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxSample4Full.TabIndex = 48;
            this.pictureBoxSample4Full.TabStop = false;
            // 
            // labelSample4LeadFull
            // 
            this.labelSample4LeadFull.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSample4LeadFull.Location = new System.Drawing.Point(128, 51);
            this.labelSample4LeadFull.Name = "labelSample4LeadFull";
            this.labelSample4LeadFull.Size = new System.Drawing.Size(186, 45);
            this.labelSample4LeadFull.TabIndex = 47;
            this.labelSample4LeadFull.Text = "LEAD III";
            this.labelSample4LeadFull.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelSample4LeadFull.Visible = false;
            // 
            // panel305
            // 
            this.panel305.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel305.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel305.Location = new System.Drawing.Point(0, 295);
            this.panel305.Name = "panel305";
            this.panel305.Size = new System.Drawing.Size(1654, 2);
            this.panel305.TabIndex = 20;
            // 
            // panel306
            // 
            this.panel306.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel306.Controls.Add(this.labelSample4Ampl);
            this.panel306.Controls.Add(this.labelSample4Sweep);
            this.panel306.Controls.Add(this.labelSample4End);
            this.panel306.Controls.Add(this.labelSample4Mid);
            this.panel306.Controls.Add(this.labelSample4Start);
            this.panel306.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel306.Location = new System.Drawing.Point(0, 260);
            this.panel306.Name = "panel306";
            this.panel306.Size = new System.Drawing.Size(1654, 35);
            this.panel306.TabIndex = 11;
            // 
            // labelSample4Ampl
            // 
            this.labelSample4Ampl.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample4Ampl.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample4Ampl.Location = new System.Drawing.Point(844, 0);
            this.labelSample4Ampl.Name = "labelSample4Ampl";
            this.labelSample4Ampl.Size = new System.Drawing.Size(310, 33);
            this.labelSample4Ampl.TabIndex = 11;
            this.labelSample4Ampl.Text = "10 mm/mV (0.50 mV)";
            this.labelSample4Ampl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample4Sweep
            // 
            this.labelSample4Sweep.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample4Sweep.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample4Sweep.Location = new System.Drawing.Point(1154, 0);
            this.labelSample4Sweep.Name = "labelSample4Sweep";
            this.labelSample4Sweep.Size = new System.Drawing.Size(302, 33);
            this.labelSample4Sweep.TabIndex = 10;
            this.labelSample4Sweep.Text = "25 mm/sec (0.20 Sec)";
            this.labelSample4Sweep.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample4End
            // 
            this.labelSample4End.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample4End.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample4End.Location = new System.Drawing.Point(1456, 0);
            this.labelSample4End.Name = "labelSample4End";
            this.labelSample4End.Size = new System.Drawing.Size(196, 33);
            this.labelSample4End.TabIndex = 9;
            this.labelSample4End.Text = "10:15:20 AM";
            this.labelSample4End.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelSample4Mid
            // 
            this.labelSample4Mid.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSample4Mid.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample4Mid.Location = new System.Drawing.Point(470, 0);
            this.labelSample4Mid.Name = "labelSample4Mid";
            this.labelSample4Mid.Size = new System.Drawing.Size(197, 33);
            this.labelSample4Mid.TabIndex = 7;
            this.labelSample4Mid.Text = "10:15:15 AM";
            this.labelSample4Mid.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelSample4Mid.Visible = false;
            // 
            // labelSample4Start
            // 
            this.labelSample4Start.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSample4Start.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample4Start.Location = new System.Drawing.Point(0, 0);
            this.labelSample4Start.Name = "labelSample4Start";
            this.labelSample4Start.Size = new System.Drawing.Size(470, 33);
            this.labelSample4Start.TabIndex = 8;
            this.labelSample4Start.Text = "^10:14:30 AM 99/99/9999 9999 sec";
            this.labelSample4Start.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel328
            // 
            this.panel328.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel328.Controls.Add(this.panel329);
            this.panel328.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel328.Location = new System.Drawing.Point(0, 40);
            this.panel328.Name = "panel328";
            this.panel328.Size = new System.Drawing.Size(1654, 220);
            this.panel328.TabIndex = 10;
            // 
            // panel329
            // 
            this.panel329.Controls.Add(this.labelSample4Lead);
            this.panel329.Controls.Add(this.QtUnit4);
            this.panel329.Controls.Add(this.QtText4);
            this.panel329.Controls.Add(this.QrsUnit4);
            this.panel329.Controls.Add(this.QrsText4);
            this.panel329.Controls.Add(this.lPrUnit4);
            this.panel329.Controls.Add(this.PrText4);
            this.panel329.Controls.Add(this.MeanHrUnit4);
            this.panel329.Controls.Add(this.MeanHrText4);
            this.panel329.Controls.Add(this.pictureBoxSample4);
            this.panel329.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel329.Location = new System.Drawing.Point(0, 0);
            this.panel329.Name = "panel329";
            this.panel329.Size = new System.Drawing.Size(1652, 218);
            this.panel329.TabIndex = 2;
            // 
            // labelSample4Lead
            // 
            this.labelSample4Lead.Font = new System.Drawing.Font("Verdana", 28F);
            this.labelSample4Lead.Location = new System.Drawing.Point(1017, 38);
            this.labelSample4Lead.Name = "labelSample4Lead";
            this.labelSample4Lead.Size = new System.Drawing.Size(186, 49);
            this.labelSample4Lead.TabIndex = 80;
            this.labelSample4Lead.Text = "LEAD III";
            this.labelSample4Lead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelSample4Lead.Visible = false;
            // 
            // QtUnit4
            // 
            this.QtUnit4.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.QtUnit4.Location = new System.Drawing.Point(1730, 65);
            this.QtUnit4.Name = "QtUnit4";
            this.QtUnit4.Size = new System.Drawing.Size(78, 49);
            this.QtUnit4.TabIndex = 78;
            this.QtUnit4.Text = "sec";
            this.QtUnit4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.QtUnit4.Visible = false;
            // 
            // QtText4
            // 
            this.QtText4.Font = new System.Drawing.Font("Verdana", 24F);
            this.QtText4.Location = new System.Drawing.Point(1531, 65);
            this.QtText4.Name = "QtText4";
            this.QtText4.Size = new System.Drawing.Size(77, 49);
            this.QtText4.TabIndex = 79;
            this.QtText4.Text = "QT:";
            this.QtText4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.QtText4.Visible = false;
            // 
            // QrsUnit4
            // 
            this.QrsUnit4.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.QrsUnit4.Location = new System.Drawing.Point(1455, 65);
            this.QrsUnit4.Name = "QrsUnit4";
            this.QrsUnit4.Size = new System.Drawing.Size(76, 49);
            this.QrsUnit4.TabIndex = 77;
            this.QrsUnit4.Text = "sec";
            this.QrsUnit4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.QrsUnit4.Visible = false;
            // 
            // QrsText4
            // 
            this.QrsText4.Font = new System.Drawing.Font("Verdana", 24F);
            this.QrsText4.Location = new System.Drawing.Point(1224, 65);
            this.QrsText4.Name = "QrsText4";
            this.QrsText4.Size = new System.Drawing.Size(110, 49);
            this.QrsText4.TabIndex = 76;
            this.QrsText4.Text = "QRS:";
            this.QrsText4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.QrsText4.Visible = false;
            // 
            // lPrUnit4
            // 
            this.lPrUnit4.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lPrUnit4.Location = new System.Drawing.Point(1146, 65);
            this.lPrUnit4.Name = "lPrUnit4";
            this.lPrUnit4.Size = new System.Drawing.Size(78, 49);
            this.lPrUnit4.TabIndex = 75;
            this.lPrUnit4.Text = "sec";
            this.lPrUnit4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lPrUnit4.Visible = false;
            // 
            // PrText4
            // 
            this.PrText4.Font = new System.Drawing.Font("Verdana", 24F);
            this.PrText4.Location = new System.Drawing.Point(931, 65);
            this.PrText4.Name = "PrText4";
            this.PrText4.Size = new System.Drawing.Size(73, 49);
            this.PrText4.TabIndex = 74;
            this.PrText4.Text = "PR:";
            this.PrText4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.PrText4.Visible = false;
            // 
            // MeanHrUnit4
            // 
            this.MeanHrUnit4.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeanHrUnit4.Location = new System.Drawing.Point(834, 65);
            this.MeanHrUnit4.Name = "MeanHrUnit4";
            this.MeanHrUnit4.Size = new System.Drawing.Size(97, 49);
            this.MeanHrUnit4.TabIndex = 73;
            this.MeanHrUnit4.Text = "bpm";
            this.MeanHrUnit4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.MeanHrUnit4.Visible = false;
            // 
            // MeanHrText4
            // 
            this.MeanHrText4.Font = new System.Drawing.Font("Verdana", 24F);
            this.MeanHrText4.Location = new System.Drawing.Point(526, 65);
            this.MeanHrText4.Name = "MeanHrText4";
            this.MeanHrText4.Size = new System.Drawing.Size(217, 49);
            this.MeanHrText4.TabIndex = 72;
            this.MeanHrText4.Text = "Heart Rate:";
            this.MeanHrText4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.MeanHrText4.Visible = false;
            // 
            // pictureBoxSample4
            // 
            this.pictureBoxSample4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxSample4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxSample4.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxSample4.InitialImage")));
            this.pictureBoxSample4.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxSample4.Name = "pictureBoxSample4";
            this.pictureBoxSample4.Size = new System.Drawing.Size(1652, 218);
            this.pictureBoxSample4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxSample4.TabIndex = 0;
            this.pictureBoxSample4.TabStop = false;
            // 
            // panel357
            // 
            this.panel357.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel357.Controls.Add(this.label121);
            this.panel357.Controls.Add(this.label124);
            this.panel357.Controls.Add(this.labelSample4EventNr);
            this.panel357.Controls.Add(this.labelEvent4);
            this.panel357.Controls.Add(this.labelSample4Date);
            this.panel357.Controls.Add(this.label119);
            this.panel357.Controls.Add(this.labelSample4Time);
            this.panel357.Controls.Add(this.labelSampleType4);
            this.panel357.Controls.Add(this.labelClassification4);
            this.panel357.Controls.Add(this.labelSample4Nr);
            this.panel357.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel357.Location = new System.Drawing.Point(0, 0);
            this.panel357.Name = "panel357";
            this.panel357.Size = new System.Drawing.Size(1654, 40);
            this.panel357.TabIndex = 7;
            // 
            // label121
            // 
            this.label121.Dock = System.Windows.Forms.DockStyle.Left;
            this.label121.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold);
            this.label121.Location = new System.Drawing.Point(1119, 0);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(20, 38);
            this.label121.TabIndex = 20;
            this.label121.Text = "-";
            this.label121.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label121.Visible = false;
            // 
            // label124
            // 
            this.label124.Dock = System.Windows.Forms.DockStyle.Left;
            this.label124.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold);
            this.label124.Location = new System.Drawing.Point(1099, 0);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(20, 38);
            this.label124.TabIndex = 17;
            this.label124.Text = "-";
            this.label124.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label124.Visible = false;
            // 
            // labelSample4EventNr
            // 
            this.labelSample4EventNr.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSample4EventNr.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold);
            this.labelSample4EventNr.Location = new System.Drawing.Point(944, 0);
            this.labelSample4EventNr.Name = "labelSample4EventNr";
            this.labelSample4EventNr.Size = new System.Drawing.Size(155, 38);
            this.labelSample4EventNr.TabIndex = 16;
            this.labelSample4EventNr.Text = "^9999";
            this.labelSample4EventNr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelEvent4
            // 
            this.labelEvent4.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelEvent4.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold);
            this.labelEvent4.Location = new System.Drawing.Point(808, 0);
            this.labelEvent4.Name = "labelEvent4";
            this.labelEvent4.Size = new System.Drawing.Size(136, 38);
            this.labelEvent4.TabIndex = 15;
            this.labelEvent4.Text = "Event";
            this.labelEvent4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelSample4Date
            // 
            this.labelSample4Date.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample4Date.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelSample4Date.Location = new System.Drawing.Point(1239, 0);
            this.labelSample4Date.Name = "labelSample4Date";
            this.labelSample4Date.Size = new System.Drawing.Size(190, 38);
            this.labelSample4Date.TabIndex = 24;
            this.labelSample4Date.Text = "^9/99/9999";
            this.labelSample4Date.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label119
            // 
            this.label119.Dock = System.Windows.Forms.DockStyle.Right;
            this.label119.Font = new System.Drawing.Font("Verdana", 20F);
            this.label119.Location = new System.Drawing.Point(1429, 0);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(20, 38);
            this.label119.TabIndex = 23;
            this.label119.Text = "-";
            this.label119.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelSample4Time
            // 
            this.labelSample4Time.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample4Time.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelSample4Time.Location = new System.Drawing.Point(1449, 0);
            this.labelSample4Time.Name = "labelSample4Time";
            this.labelSample4Time.Size = new System.Drawing.Size(203, 38);
            this.labelSample4Time.TabIndex = 25;
            this.labelSample4Time.Text = "10:15:10 AM";
            this.labelSample4Time.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSampleType4
            // 
            this.labelSampleType4.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSampleType4.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold);
            this.labelSampleType4.Location = new System.Drawing.Point(577, 0);
            this.labelSampleType4.Name = "labelSampleType4";
            this.labelSampleType4.Size = new System.Drawing.Size(231, 38);
            this.labelSampleType4.TabIndex = 18;
            this.labelSampleType4.Text = "^Automatic";
            this.labelSampleType4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelClassification4
            // 
            this.labelClassification4.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelClassification4.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold);
            this.labelClassification4.Location = new System.Drawing.Point(100, 0);
            this.labelClassification4.Name = "labelClassification4";
            this.labelClassification4.Size = new System.Drawing.Size(477, 38);
            this.labelClassification4.TabIndex = 21;
            this.labelClassification4.Text = "Classification";
            this.labelClassification4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample4Nr
            // 
            this.labelSample4Nr.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSample4Nr.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold);
            this.labelSample4Nr.Location = new System.Drawing.Point(0, 0);
            this.labelSample4Nr.Name = "labelSample4Nr";
            this.labelSample4Nr.Size = new System.Drawing.Size(100, 38);
            this.labelSample4Nr.TabIndex = 22;
            this.labelSample4Nr.Text = "04";
            this.labelSample4Nr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel35
            // 
            this.panel35.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel35.Location = new System.Drawing.Point(0, 879);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(1654, 10);
            this.panel35.TabIndex = 45;
            // 
            // panelEventSample3
            // 
            this.panelEventSample3.AutoSize = true;
            this.panelEventSample3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panelEventSample3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelEventSample3.Controls.Add(this.panel255);
            this.panelEventSample3.Controls.Add(this.panel277);
            this.panelEventSample3.Controls.Add(this.panel283);
            this.panelEventSample3.Controls.Add(this.panel300);
            this.panelEventSample3.Controls.Add(this.panel384);
            this.panelEventSample3.Controls.Add(this.panel387);
            this.panelEventSample3.Controls.Add(this.panel416);
            this.panelEventSample3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelEventSample3.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.panelEventSample3.Location = new System.Drawing.Point(0, 225);
            this.panelEventSample3.Name = "panelEventSample3";
            this.panelEventSample3.Size = new System.Drawing.Size(1654, 654);
            this.panelEventSample3.TabIndex = 44;
            // 
            // panel255
            // 
            this.panel255.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel255.Controls.Add(this.panelActivities3);
            this.panel255.Controls.Add(this.panelSymptoms3);
            this.panel255.Controls.Add(this.panelfindings3);
            this.panel255.Controls.Add(this.panel270);
            this.panel255.Controls.Add(this.panel273);
            this.panel255.Controls.Add(this.panelQRSmeasurements3);
            this.panel255.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel255.Location = new System.Drawing.Point(0, 532);
            this.panel255.Name = "panel255";
            this.panel255.Size = new System.Drawing.Size(1652, 120);
            this.panel255.TabIndex = 28;
            // 
            // panelActivities3
            // 
            this.panelActivities3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelActivities3.Controls.Add(this.textBoxActivities3);
            this.panelActivities3.Controls.Add(this.label22);
            this.panelActivities3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelActivities3.Location = new System.Drawing.Point(1411, 0);
            this.panelActivities3.Name = "panelActivities3";
            this.panelActivities3.Size = new System.Drawing.Size(239, 118);
            this.panelActivities3.TabIndex = 8;
            // 
            // textBoxActivities3
            // 
            this.textBoxActivities3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxActivities3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxActivities3.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxActivities3.Location = new System.Drawing.Point(0, 39);
            this.textBoxActivities3.Multiline = true;
            this.textBoxActivities3.Name = "textBoxActivities3";
            this.textBoxActivities3.Size = new System.Drawing.Size(237, 77);
            this.textBoxActivities3.TabIndex = 5;
            // 
            // label22
            // 
            this.label22.Dock = System.Windows.Forms.DockStyle.Top;
            this.label22.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(0, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(237, 39);
            this.label22.TabIndex = 4;
            this.label22.Text = "Activities:";
            // 
            // panelSymptoms3
            // 
            this.panelSymptoms3.Controls.Add(this.textBoxSymptoms3);
            this.panelSymptoms3.Controls.Add(this.label31);
            this.panelSymptoms3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelSymptoms3.Location = new System.Drawing.Point(1096, 0);
            this.panelSymptoms3.Name = "panelSymptoms3";
            this.panelSymptoms3.Size = new System.Drawing.Size(315, 118);
            this.panelSymptoms3.TabIndex = 3;
            // 
            // textBoxSymptoms3
            // 
            this.textBoxSymptoms3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxSymptoms3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxSymptoms3.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSymptoms3.Location = new System.Drawing.Point(0, 39);
            this.textBoxSymptoms3.Multiline = true;
            this.textBoxSymptoms3.Name = "textBoxSymptoms3";
            this.textBoxSymptoms3.Size = new System.Drawing.Size(315, 79);
            this.textBoxSymptoms3.TabIndex = 4;
            // 
            // label31
            // 
            this.label31.Dock = System.Windows.Forms.DockStyle.Top;
            this.label31.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(0, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(315, 39);
            this.label31.TabIndex = 3;
            this.label31.Text = "Symptoms:";
            // 
            // panelfindings3
            // 
            this.panelfindings3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelfindings3.Controls.Add(this.textBoxFindings3);
            this.panelfindings3.Controls.Add(this.panel118);
            this.panelfindings3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelfindings3.Location = new System.Drawing.Point(433, 0);
            this.panelfindings3.Name = "panelfindings3";
            this.panelfindings3.Size = new System.Drawing.Size(663, 118);
            this.panelfindings3.TabIndex = 7;
            // 
            // textBoxFindings3
            // 
            this.textBoxFindings3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxFindings3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxFindings3.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxFindings3.Location = new System.Drawing.Point(0, 38);
            this.textBoxFindings3.Multiline = true;
            this.textBoxFindings3.Name = "textBoxFindings3";
            this.textBoxFindings3.Size = new System.Drawing.Size(661, 78);
            this.textBoxFindings3.TabIndex = 1;
            this.textBoxFindings3.Text = "^regel 1\r\nregel 2\r\n";
            // 
            // panel118
            // 
            this.panel118.Controls.Add(this.labelRhythms3);
            this.panel118.Controls.Add(this.labelFindingsText3);
            this.panel118.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel118.Location = new System.Drawing.Point(0, 0);
            this.panel118.Name = "panel118";
            this.panel118.Size = new System.Drawing.Size(661, 38);
            this.panel118.TabIndex = 2;
            // 
            // labelRhythms3
            // 
            this.labelRhythms3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelRhythms3.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRhythms3.Location = new System.Drawing.Point(212, 0);
            this.labelRhythms3.Name = "labelRhythms3";
            this.labelRhythms3.Size = new System.Drawing.Size(449, 38);
            this.labelRhythms3.TabIndex = 70;
            this.labelRhythms3.Text = "^Rhythms";
            this.labelRhythms3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelFindingsText3
            // 
            this.labelFindingsText3.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelFindingsText3.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFindingsText3.Location = new System.Drawing.Point(0, 0);
            this.labelFindingsText3.Name = "labelFindingsText3";
            this.labelFindingsText3.Size = new System.Drawing.Size(212, 38);
            this.labelFindingsText3.TabIndex = 2;
            this.labelFindingsText3.Text = "^QC Findings:";
            // 
            // panel270
            // 
            this.panel270.AutoSize = true;
            this.panel270.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel270.Location = new System.Drawing.Point(433, 0);
            this.panel270.Name = "panel270";
            this.panel270.Size = new System.Drawing.Size(0, 118);
            this.panel270.TabIndex = 6;
            // 
            // panel273
            // 
            this.panel273.AutoSize = true;
            this.panel273.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel273.Location = new System.Drawing.Point(433, 0);
            this.panel273.Name = "panel273";
            this.panel273.Size = new System.Drawing.Size(0, 118);
            this.panel273.TabIndex = 3;
            // 
            // panelQRSmeasurements3
            // 
            this.panelQRSmeasurements3.Controls.Add(this.panel17);
            this.panelQRSmeasurements3.Controls.Add(this.panel16);
            this.panelQRSmeasurements3.Controls.Add(this.panel15);
            this.panelQRSmeasurements3.Controls.Add(this.panel14);
            this.panelQRSmeasurements3.Controls.Add(this.panel13);
            this.panelQRSmeasurements3.Controls.Add(this.panel9);
            this.panelQRSmeasurements3.Controls.Add(this.label23);
            this.panelQRSmeasurements3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelQRSmeasurements3.Location = new System.Drawing.Point(0, 0);
            this.panelQRSmeasurements3.Name = "panelQRSmeasurements3";
            this.panelQRSmeasurements3.Size = new System.Drawing.Size(433, 118);
            this.panelQRSmeasurements3.TabIndex = 9;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.labelQtUnit3);
            this.panel17.Controls.Add(this.labelQrsUnit3);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel17.Location = new System.Drawing.Point(380, 38);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(54, 80);
            this.panel17.TabIndex = 11;
            // 
            // labelQtUnit3
            // 
            this.labelQtUnit3.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelQtUnit3.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQtUnit3.Location = new System.Drawing.Point(0, 40);
            this.labelQtUnit3.Name = "labelQtUnit3";
            this.labelQtUnit3.Size = new System.Drawing.Size(54, 40);
            this.labelQtUnit3.TabIndex = 70;
            this.labelQtUnit3.Text = "sec";
            this.labelQtUnit3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelQrsUnit3
            // 
            this.labelQrsUnit3.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelQrsUnit3.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQrsUnit3.Location = new System.Drawing.Point(0, 0);
            this.labelQrsUnit3.Name = "labelQrsUnit3";
            this.labelQrsUnit3.Size = new System.Drawing.Size(54, 40);
            this.labelQrsUnit3.TabIndex = 69;
            this.labelQrsUnit3.Text = "sec";
            this.labelQrsUnit3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.labelQtValue3);
            this.panel16.Controls.Add(this.labelQrsValue3);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel16.Location = new System.Drawing.Point(288, 38);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(92, 80);
            this.panel16.TabIndex = 10;
            // 
            // labelQtValue3
            // 
            this.labelQtValue3.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelQtValue3.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQtValue3.Location = new System.Drawing.Point(0, 40);
            this.labelQtValue3.Name = "labelQtValue3";
            this.labelQtValue3.Size = new System.Drawing.Size(92, 40);
            this.labelQtValue3.TabIndex = 70;
            this.labelQtValue3.Text = "^,202";
            this.labelQtValue3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelQrsValue3
            // 
            this.labelQrsValue3.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelQrsValue3.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQrsValue3.Location = new System.Drawing.Point(0, 0);
            this.labelQrsValue3.Name = "labelQrsValue3";
            this.labelQrsValue3.Size = new System.Drawing.Size(92, 40);
            this.labelQrsValue3.TabIndex = 67;
            this.labelQrsValue3.Text = "^,202";
            this.labelQrsValue3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.labelQtText3);
            this.panel15.Controls.Add(this.labelQrsText3);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel15.Location = new System.Drawing.Point(212, 38);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(76, 80);
            this.panel15.TabIndex = 9;
            // 
            // labelQtText3
            // 
            this.labelQtText3.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelQtText3.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQtText3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelQtText3.Location = new System.Drawing.Point(0, 40);
            this.labelQtText3.Name = "labelQtText3";
            this.labelQtText3.Size = new System.Drawing.Size(76, 40);
            this.labelQtText3.TabIndex = 72;
            this.labelQtText3.Text = "QT:";
            this.labelQtText3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelQrsText3
            // 
            this.labelQrsText3.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelQrsText3.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQrsText3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelQrsText3.Location = new System.Drawing.Point(0, 0);
            this.labelQrsText3.Name = "labelQrsText3";
            this.labelQrsText3.Size = new System.Drawing.Size(76, 40);
            this.labelQrsText3.TabIndex = 67;
            this.labelQrsText3.Text = "QRS:";
            this.labelQrsText3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.labelPrUnit3);
            this.panel14.Controls.Add(this.labelMeanHrUnit3);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel14.Location = new System.Drawing.Point(146, 38);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(66, 80);
            this.panel14.TabIndex = 8;
            // 
            // labelPrUnit3
            // 
            this.labelPrUnit3.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPrUnit3.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPrUnit3.Location = new System.Drawing.Point(0, 40);
            this.labelPrUnit3.Name = "labelPrUnit3";
            this.labelPrUnit3.Size = new System.Drawing.Size(66, 40);
            this.labelPrUnit3.TabIndex = 63;
            this.labelPrUnit3.Text = "sec";
            this.labelPrUnit3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelMeanHrUnit3
            // 
            this.labelMeanHrUnit3.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelMeanHrUnit3.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMeanHrUnit3.Location = new System.Drawing.Point(0, 0);
            this.labelMeanHrUnit3.Name = "labelMeanHrUnit3";
            this.labelMeanHrUnit3.Size = new System.Drawing.Size(66, 40);
            this.labelMeanHrUnit3.TabIndex = 61;
            this.labelMeanHrUnit3.Text = "bpm";
            this.labelMeanHrUnit3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.labelPrValue3);
            this.panel13.Controls.Add(this.labelMeanHrValue3);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel13.Location = new System.Drawing.Point(59, 38);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(87, 80);
            this.panel13.TabIndex = 7;
            // 
            // labelPrValue3
            // 
            this.labelPrValue3.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPrValue3.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPrValue3.Location = new System.Drawing.Point(0, 40);
            this.labelPrValue3.Name = "labelPrValue3";
            this.labelPrValue3.Size = new System.Drawing.Size(87, 40);
            this.labelPrValue3.TabIndex = 63;
            this.labelPrValue3.Text = "^,202";
            this.labelPrValue3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelMeanHrValue3
            // 
            this.labelMeanHrValue3.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelMeanHrValue3.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMeanHrValue3.Location = new System.Drawing.Point(0, 0);
            this.labelMeanHrValue3.Name = "labelMeanHrValue3";
            this.labelMeanHrValue3.Size = new System.Drawing.Size(87, 40);
            this.labelMeanHrValue3.TabIndex = 58;
            this.labelMeanHrValue3.Text = "120";
            this.labelMeanHrValue3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.labelPrText3);
            this.panel9.Controls.Add(this.labelMeanHrText3);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel9.Location = new System.Drawing.Point(0, 38);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(59, 80);
            this.panel9.TabIndex = 6;
            // 
            // labelPrText3
            // 
            this.labelPrText3.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPrText3.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPrText3.Location = new System.Drawing.Point(0, 40);
            this.labelPrText3.Name = "labelPrText3";
            this.labelPrText3.Size = new System.Drawing.Size(59, 40);
            this.labelPrText3.TabIndex = 62;
            this.labelPrText3.Text = "PR:";
            this.labelPrText3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelMeanHrText3
            // 
            this.labelMeanHrText3.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelMeanHrText3.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMeanHrText3.Location = new System.Drawing.Point(0, 0);
            this.labelMeanHrText3.Name = "labelMeanHrText3";
            this.labelMeanHrText3.Size = new System.Drawing.Size(59, 40);
            this.labelMeanHrText3.TabIndex = 58;
            this.labelMeanHrText3.Text = "HR:";
            this.labelMeanHrText3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label23
            // 
            this.label23.Dock = System.Windows.Forms.DockStyle.Top;
            this.label23.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(0, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(433, 38);
            this.label23.TabIndex = 5;
            this.label23.Text = "QRS Measurements:";
            // 
            // panel277
            // 
            this.panel277.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel277.Controls.Add(this.labelSample3AmplFull);
            this.panel277.Controls.Add(this.labelSample3SweepFull);
            this.panel277.Controls.Add(this.labelSample3MidFull);
            this.panel277.Controls.Add(this.labelSample3StartFull);
            this.panel277.Controls.Add(this.labelSample3EndFull);
            this.panel277.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel277.Location = new System.Drawing.Point(0, 497);
            this.panel277.Name = "panel277";
            this.panel277.Size = new System.Drawing.Size(1652, 35);
            this.panel277.TabIndex = 26;
            // 
            // labelSample3AmplFull
            // 
            this.labelSample3AmplFull.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample3AmplFull.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample3AmplFull.Location = new System.Drawing.Point(842, 0);
            this.labelSample3AmplFull.Name = "labelSample3AmplFull";
            this.labelSample3AmplFull.Size = new System.Drawing.Size(310, 33);
            this.labelSample3AmplFull.TabIndex = 11;
            this.labelSample3AmplFull.Text = "10 mm/mV (0.50 mV)";
            this.labelSample3AmplFull.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample3SweepFull
            // 
            this.labelSample3SweepFull.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample3SweepFull.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample3SweepFull.Location = new System.Drawing.Point(1152, 0);
            this.labelSample3SweepFull.Name = "labelSample3SweepFull";
            this.labelSample3SweepFull.Size = new System.Drawing.Size(302, 33);
            this.labelSample3SweepFull.TabIndex = 10;
            this.labelSample3SweepFull.Text = "25 mm/sec (0.20 Sec)";
            this.labelSample3SweepFull.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample3MidFull
            // 
            this.labelSample3MidFull.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSample3MidFull.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample3MidFull.Location = new System.Drawing.Point(470, 0);
            this.labelSample3MidFull.Name = "labelSample3MidFull";
            this.labelSample3MidFull.Size = new System.Drawing.Size(194, 33);
            this.labelSample3MidFull.TabIndex = 9;
            this.labelSample3MidFull.Text = "10:15:00 AM";
            this.labelSample3MidFull.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelSample3MidFull.Visible = false;
            // 
            // labelSample3StartFull
            // 
            this.labelSample3StartFull.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSample3StartFull.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample3StartFull.Location = new System.Drawing.Point(0, 0);
            this.labelSample3StartFull.Name = "labelSample3StartFull";
            this.labelSample3StartFull.Size = new System.Drawing.Size(470, 33);
            this.labelSample3StartFull.TabIndex = 8;
            this.labelSample3StartFull.Text = "^10:14:30 AM 99/99/9999 9999 sec";
            this.labelSample3StartFull.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample3EndFull
            // 
            this.labelSample3EndFull.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample3EndFull.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample3EndFull.Location = new System.Drawing.Point(1454, 0);
            this.labelSample3EndFull.Name = "labelSample3EndFull";
            this.labelSample3EndFull.Size = new System.Drawing.Size(196, 33);
            this.labelSample3EndFull.TabIndex = 12;
            this.labelSample3EndFull.Text = "10:15:30 AM";
            this.labelSample3EndFull.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel283
            // 
            this.panel283.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel283.Controls.Add(this.pictureBoxSample3Full);
            this.panel283.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel283.Location = new System.Drawing.Point(0, 287);
            this.panel283.Name = "panel283";
            this.panel283.Size = new System.Drawing.Size(1652, 210);
            this.panel283.TabIndex = 25;
            // 
            // pictureBoxSample3Full
            // 
            this.pictureBoxSample3Full.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxSample3Full.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxSample3Full.Name = "pictureBoxSample3Full";
            this.pictureBoxSample3Full.Size = new System.Drawing.Size(1650, 208);
            this.pictureBoxSample3Full.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxSample3Full.TabIndex = 1;
            this.pictureBoxSample3Full.TabStop = false;
            // 
            // panel300
            // 
            this.panel300.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel300.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel300.Location = new System.Drawing.Point(0, 285);
            this.panel300.Name = "panel300";
            this.panel300.Size = new System.Drawing.Size(1652, 2);
            this.panel300.TabIndex = 20;
            // 
            // panel384
            // 
            this.panel384.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel384.Controls.Add(this.labelSample3Mid);
            this.panel384.Controls.Add(this.labelSample3Start);
            this.panel384.Controls.Add(this.labelSample3Ampl);
            this.panel384.Controls.Add(this.labelSample3Sweep);
            this.panel384.Controls.Add(this.labelSample3End);
            this.panel384.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel384.Location = new System.Drawing.Point(0, 250);
            this.panel384.Name = "panel384";
            this.panel384.Size = new System.Drawing.Size(1652, 35);
            this.panel384.TabIndex = 11;
            // 
            // labelSample3Mid
            // 
            this.labelSample3Mid.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSample3Mid.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample3Mid.Location = new System.Drawing.Point(470, 0);
            this.labelSample3Mid.Name = "labelSample3Mid";
            this.labelSample3Mid.Size = new System.Drawing.Size(218, 33);
            this.labelSample3Mid.TabIndex = 12;
            this.labelSample3Mid.Text = "10:15:15 AM";
            this.labelSample3Mid.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelSample3Mid.Visible = false;
            // 
            // labelSample3Start
            // 
            this.labelSample3Start.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSample3Start.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample3Start.Location = new System.Drawing.Point(0, 0);
            this.labelSample3Start.Name = "labelSample3Start";
            this.labelSample3Start.Size = new System.Drawing.Size(470, 33);
            this.labelSample3Start.TabIndex = 11;
            this.labelSample3Start.Text = "^10:14:30 AM 99/99/9999 9999 sec";
            this.labelSample3Start.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample3Ampl
            // 
            this.labelSample3Ampl.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample3Ampl.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample3Ampl.Location = new System.Drawing.Point(842, 0);
            this.labelSample3Ampl.Name = "labelSample3Ampl";
            this.labelSample3Ampl.Size = new System.Drawing.Size(310, 33);
            this.labelSample3Ampl.TabIndex = 7;
            this.labelSample3Ampl.Text = "10 mm/mV (0.50 mV)";
            this.labelSample3Ampl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample3Sweep
            // 
            this.labelSample3Sweep.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample3Sweep.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample3Sweep.Location = new System.Drawing.Point(1152, 0);
            this.labelSample3Sweep.Name = "labelSample3Sweep";
            this.labelSample3Sweep.Size = new System.Drawing.Size(302, 33);
            this.labelSample3Sweep.TabIndex = 8;
            this.labelSample3Sweep.Text = "25 mm/sec (0.20 Sec)";
            this.labelSample3Sweep.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample3End
            // 
            this.labelSample3End.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample3End.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample3End.Location = new System.Drawing.Point(1454, 0);
            this.labelSample3End.Name = "labelSample3End";
            this.labelSample3End.Size = new System.Drawing.Size(196, 33);
            this.labelSample3End.TabIndex = 10;
            this.labelSample3End.Text = "10:15:20 AM";
            this.labelSample3End.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel387
            // 
            this.panel387.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel387.Controls.Add(this.panel388);
            this.panel387.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel387.Location = new System.Drawing.Point(0, 40);
            this.panel387.Name = "panel387";
            this.panel387.Size = new System.Drawing.Size(1652, 210);
            this.panel387.TabIndex = 10;
            // 
            // panel388
            // 
            this.panel388.Controls.Add(this.pictureBoxSample3);
            this.panel388.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel388.Location = new System.Drawing.Point(0, 0);
            this.panel388.Name = "panel388";
            this.panel388.Size = new System.Drawing.Size(1650, 208);
            this.panel388.TabIndex = 2;
            // 
            // pictureBoxSample3
            // 
            this.pictureBoxSample3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxSample3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxSample3.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxSample3.InitialImage")));
            this.pictureBoxSample3.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxSample3.Name = "pictureBoxSample3";
            this.pictureBoxSample3.Size = new System.Drawing.Size(1650, 208);
            this.pictureBoxSample3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxSample3.TabIndex = 0;
            this.pictureBoxSample3.TabStop = false;
            // 
            // panel416
            // 
            this.panel416.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel416.Controls.Add(this.label73);
            this.panel416.Controls.Add(this.label70);
            this.panel416.Controls.Add(this.labelSample3EventNr);
            this.panel416.Controls.Add(this.labelEvent3);
            this.panel416.Controls.Add(this.labelSampleType3);
            this.panel416.Controls.Add(this.labelSample3Date);
            this.panel416.Controls.Add(this.label75);
            this.panel416.Controls.Add(this.labelSample3Time);
            this.panel416.Controls.Add(this.labelClassification3);
            this.panel416.Controls.Add(this.labelSample3Nr);
            this.panel416.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel416.Location = new System.Drawing.Point(0, 0);
            this.panel416.Name = "panel416";
            this.panel416.Size = new System.Drawing.Size(1652, 40);
            this.panel416.TabIndex = 7;
            // 
            // label73
            // 
            this.label73.Dock = System.Windows.Forms.DockStyle.Left;
            this.label73.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold);
            this.label73.Location = new System.Drawing.Point(1119, 0);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(20, 38);
            this.label73.TabIndex = 16;
            this.label73.Text = "-";
            this.label73.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label73.Visible = false;
            // 
            // label70
            // 
            this.label70.Dock = System.Windows.Forms.DockStyle.Left;
            this.label70.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold);
            this.label70.Location = new System.Drawing.Point(1099, 0);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(20, 38);
            this.label70.TabIndex = 15;
            this.label70.Text = "-";
            this.label70.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label70.Visible = false;
            // 
            // labelSample3EventNr
            // 
            this.labelSample3EventNr.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSample3EventNr.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold);
            this.labelSample3EventNr.Location = new System.Drawing.Point(944, 0);
            this.labelSample3EventNr.Name = "labelSample3EventNr";
            this.labelSample3EventNr.Size = new System.Drawing.Size(155, 38);
            this.labelSample3EventNr.TabIndex = 3;
            this.labelSample3EventNr.Text = "^9999";
            this.labelSample3EventNr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelEvent3
            // 
            this.labelEvent3.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelEvent3.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold);
            this.labelEvent3.Location = new System.Drawing.Point(808, 0);
            this.labelEvent3.Name = "labelEvent3";
            this.labelEvent3.Size = new System.Drawing.Size(136, 38);
            this.labelEvent3.TabIndex = 2;
            this.labelEvent3.Text = "Event";
            this.labelEvent3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelSampleType3
            // 
            this.labelSampleType3.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSampleType3.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold);
            this.labelSampleType3.Location = new System.Drawing.Point(577, 0);
            this.labelSampleType3.Name = "labelSampleType3";
            this.labelSampleType3.Size = new System.Drawing.Size(231, 38);
            this.labelSampleType3.TabIndex = 4;
            this.labelSampleType3.Text = "^Automatic";
            this.labelSampleType3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelSample3Date
            // 
            this.labelSample3Date.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample3Date.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelSample3Date.Location = new System.Drawing.Point(1237, 0);
            this.labelSample3Date.Name = "labelSample3Date";
            this.labelSample3Date.Size = new System.Drawing.Size(190, 38);
            this.labelSample3Date.TabIndex = 13;
            this.labelSample3Date.Text = "^9/99/9999";
            this.labelSample3Date.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label75
            // 
            this.label75.Dock = System.Windows.Forms.DockStyle.Right;
            this.label75.Font = new System.Drawing.Font("Verdana", 20F);
            this.label75.Location = new System.Drawing.Point(1427, 0);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(20, 38);
            this.label75.TabIndex = 11;
            this.label75.Text = "-";
            this.label75.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelSample3Time
            // 
            this.labelSample3Time.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample3Time.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelSample3Time.Location = new System.Drawing.Point(1447, 0);
            this.labelSample3Time.Name = "labelSample3Time";
            this.labelSample3Time.Size = new System.Drawing.Size(203, 38);
            this.labelSample3Time.TabIndex = 12;
            this.labelSample3Time.Text = "10:15:10 AM";
            this.labelSample3Time.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelClassification3
            // 
            this.labelClassification3.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelClassification3.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold);
            this.labelClassification3.Location = new System.Drawing.Point(100, 0);
            this.labelClassification3.Name = "labelClassification3";
            this.labelClassification3.Size = new System.Drawing.Size(477, 38);
            this.labelClassification3.TabIndex = 10;
            this.labelClassification3.Text = "Classification";
            this.labelClassification3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample3Nr
            // 
            this.labelSample3Nr.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSample3Nr.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold);
            this.labelSample3Nr.Location = new System.Drawing.Point(0, 0);
            this.labelSample3Nr.Name = "labelSample3Nr";
            this.labelSample3Nr.Size = new System.Drawing.Size(100, 38);
            this.labelSample3Nr.TabIndex = 14;
            this.labelSample3Nr.Text = "03";
            this.labelSample3Nr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel251
            // 
            this.panel251.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel251.Location = new System.Drawing.Point(0, 211);
            this.panel251.Name = "panel251";
            this.panel251.Size = new System.Drawing.Size(1654, 14);
            this.panel251.TabIndex = 43;
            // 
            // panelPage2Footer
            // 
            this.panelPage2Footer.Controls.Add(this.label2);
            this.panelPage2Footer.Controls.Add(this.labelPage3StudyNumber);
            this.panelPage2Footer.Controls.Add(this.labelPrintDate3Bottom);
            this.panelPage2Footer.Controls.Add(this.label57);
            this.panelPage2Footer.Controls.Add(this.labelPage3Of);
            this.panelPage2Footer.Controls.Add(this.label58);
            this.panelPage2Footer.Controls.Add(this.labelPage3xOfX);
            this.panelPage2Footer.Controls.Add(this.labelPrintDate);
            this.panelPage2Footer.Controls.Add(this.label5);
            this.panelPage2Footer.Controls.Add(this.label6);
            this.panelPage2Footer.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelPage2Footer.Location = new System.Drawing.Point(0, 2302);
            this.panelPage2Footer.Name = "panelPage2Footer";
            this.panelPage2Footer.Size = new System.Drawing.Size(1654, 37);
            this.panelPage2Footer.TabIndex = 37;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Left;
            this.label2.Font = new System.Drawing.Font("Verdana", 18F);
            this.label2.Location = new System.Drawing.Point(706, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(460, 37);
            this.label2.TabIndex = 44;
            this.label2.Text = "2018 © Techmedic International B.V.";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPage3StudyNumber
            // 
            this.labelPage3StudyNumber.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelPage3StudyNumber.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.labelPage3StudyNumber.Location = new System.Drawing.Point(1172, 0);
            this.labelPage3StudyNumber.Name = "labelPage3StudyNumber";
            this.labelPage3StudyNumber.Size = new System.Drawing.Size(167, 37);
            this.labelPage3StudyNumber.TabIndex = 45;
            this.labelPage3StudyNumber.Text = "1212212";
            this.labelPage3StudyNumber.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelPrintDate3Bottom
            // 
            this.labelPrintDate3Bottom.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPrintDate3Bottom.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.labelPrintDate3Bottom.Location = new System.Drawing.Point(138, 0);
            this.labelPrintDate3Bottom.Name = "labelPrintDate3Bottom";
            this.labelPrintDate3Bottom.Size = new System.Drawing.Size(568, 37);
            this.labelPrintDate3Bottom.TabIndex = 43;
            this.labelPrintDate3Bottom.Text = "12/27/2016 22:22:22 AM+1200 v12345";
            this.labelPrintDate3Bottom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label57
            // 
            this.label57.Dock = System.Windows.Forms.DockStyle.Right;
            this.label57.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(1339, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(81, 37);
            this.label57.TabIndex = 23;
            this.label57.Text = "Page";
            this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelPage3Of
            // 
            this.labelPage3Of.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelPage3Of.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.labelPage3Of.Location = new System.Drawing.Point(1420, 0);
            this.labelPage3Of.Name = "labelPage3Of";
            this.labelPage3Of.Size = new System.Drawing.Size(80, 37);
            this.labelPage3Of.TabIndex = 22;
            this.labelPage3Of.Text = "3";
            this.labelPage3Of.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label58
            // 
            this.label58.Dock = System.Windows.Forms.DockStyle.Right;
            this.label58.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(1500, 0);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(36, 37);
            this.label58.TabIndex = 21;
            this.label58.Text = "of";
            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPage3xOfX
            // 
            this.labelPage3xOfX.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelPage3xOfX.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.labelPage3xOfX.Location = new System.Drawing.Point(1536, 0);
            this.labelPage3xOfX.Name = "labelPage3xOfX";
            this.labelPage3xOfX.Size = new System.Drawing.Size(97, 37);
            this.labelPage3xOfX.TabIndex = 20;
            this.labelPage3xOfX.Text = "20p3";
            this.labelPage3xOfX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPrintDate
            // 
            this.labelPrintDate.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPrintDate.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPrintDate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelPrintDate.Location = new System.Drawing.Point(30, 0);
            this.labelPrintDate.Name = "labelPrintDate";
            this.labelPrintDate.Size = new System.Drawing.Size(108, 37);
            this.labelPrintDate.TabIndex = 17;
            this.labelPrintDate.Text = "Printed:";
            this.labelPrintDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label5.Dock = System.Windows.Forms.DockStyle.Right;
            this.label5.Font = new System.Drawing.Font("Verdana", 10F);
            this.label5.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label5.Location = new System.Drawing.Point(1633, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(21, 37);
            this.label5.TabIndex = 1;
            this.label5.Text = "R2";
            this.label5.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label5.Visible = false;
            // 
            // label6
            // 
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label6.Dock = System.Windows.Forms.DockStyle.Left;
            this.label6.Font = new System.Drawing.Font("Verdana", 10F);
            this.label6.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 37);
            this.label6.TabIndex = 0;
            this.label6.Text = "L2";
            this.label6.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label6.Visible = false;
            // 
            // panel101
            // 
            this.panel101.Controls.Add(this.panelPage2Header);
            this.panel101.Controls.Add(this.panel500);
            this.panel101.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel101.Location = new System.Drawing.Point(0, 0);
            this.panel101.Name = "panel101";
            this.panel101.Size = new System.Drawing.Size(1654, 211);
            this.panel101.TabIndex = 42;
            // 
            // panelPage2Header
            // 
            this.panelPage2Header.Controls.Add(this.panel122);
            this.panelPage2Header.Controls.Add(this.panel123);
            this.panelPage2Header.Controls.Add(this.labelPage3DateOfBirth);
            this.panelPage2Header.Controls.Add(this.labelDateOfBirthPage2);
            this.panelPage2Header.Controls.Add(this.panel527);
            this.panelPage2Header.Controls.Add(this.panel525);
            this.panelPage2Header.Controls.Add(this.labelPage3PatLastName);
            this.panelPage2Header.Controls.Add(this.labelPatName);
            this.panelPage2Header.Controls.Add(this.panel522);
            this.panelPage2Header.Controls.Add(this.panel520);
            this.panelPage2Header.Controls.Add(this.labelPage3PatientID);
            this.panelPage2Header.Controls.Add(this.labelPatientID);
            this.panelPage2Header.Controls.Add(this.labelReportText3);
            this.panelPage2Header.Controls.Add(this.labelPage3ReportNr);
            this.panelPage2Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelPage2Header.Location = new System.Drawing.Point(0, 84);
            this.panelPage2Header.Name = "panelPage2Header";
            this.panelPage2Header.Size = new System.Drawing.Size(1654, 109);
            this.panelPage2Header.TabIndex = 6;
            // 
            // panel122
            // 
            this.panel122.Controls.Add(this.panel125);
            this.panel122.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel122.Location = new System.Drawing.Point(1418, 0);
            this.panel122.Name = "panel122";
            this.panel122.Size = new System.Drawing.Size(17, 109);
            this.panel122.TabIndex = 60;
            // 
            // panel125
            // 
            this.panel125.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel125.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel125.Location = new System.Drawing.Point(0, 0);
            this.panel125.Name = "panel125";
            this.panel125.Size = new System.Drawing.Size(2, 109);
            this.panel125.TabIndex = 16;
            // 
            // panel123
            // 
            this.panel123.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel123.Location = new System.Drawing.Point(1401, 0);
            this.panel123.Name = "panel123";
            this.panel123.Size = new System.Drawing.Size(17, 109);
            this.panel123.TabIndex = 59;
            // 
            // labelPage3DateOfBirth
            // 
            this.labelPage3DateOfBirth.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPage3DateOfBirth.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelPage3DateOfBirth.Location = new System.Drawing.Point(1182, 0);
            this.labelPage3DateOfBirth.Name = "labelPage3DateOfBirth";
            this.labelPage3DateOfBirth.Size = new System.Drawing.Size(219, 109);
            this.labelPage3DateOfBirth.TabIndex = 58;
            this.labelPage3DateOfBirth.Text = "99/99/9999";
            this.labelPage3DateOfBirth.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelDateOfBirthPage2
            // 
            this.labelDateOfBirthPage2.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelDateOfBirthPage2.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDateOfBirthPage2.Location = new System.Drawing.Point(1105, 0);
            this.labelDateOfBirthPage2.Name = "labelDateOfBirthPage2";
            this.labelDateOfBirthPage2.Size = new System.Drawing.Size(77, 109);
            this.labelDateOfBirthPage2.TabIndex = 57;
            this.labelDateOfBirthPage2.Text = "DOB:";
            this.labelDateOfBirthPage2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel527
            // 
            this.panel527.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel527.Location = new System.Drawing.Point(1090, 0);
            this.panel527.Name = "panel527";
            this.panel527.Size = new System.Drawing.Size(15, 109);
            this.panel527.TabIndex = 56;
            // 
            // panel525
            // 
            this.panel525.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel525.Location = new System.Drawing.Point(1076, 0);
            this.panel525.Name = "panel525";
            this.panel525.Size = new System.Drawing.Size(14, 109);
            this.panel525.TabIndex = 55;
            // 
            // labelPage3PatLastName
            // 
            this.labelPage3PatLastName.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPage3PatLastName.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelPage3PatLastName.Location = new System.Drawing.Point(489, 0);
            this.labelPage3PatLastName.Name = "labelPage3PatLastName";
            this.labelPage3PatLastName.Size = new System.Drawing.Size(587, 109);
            this.labelPage3PatLastName.TabIndex = 54;
            this.labelPage3PatLastName.Text = "^Brest van Kempen, hjghdgfhdsghj, jkdhjfhjas0dk";
            this.labelPage3PatLastName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelPatName
            // 
            this.labelPatName.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPatName.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPatName.Location = new System.Drawing.Point(398, 0);
            this.labelPatName.Name = "labelPatName";
            this.labelPatName.Size = new System.Drawing.Size(91, 109);
            this.labelPatName.TabIndex = 53;
            this.labelPatName.Text = "Name:";
            this.labelPatName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel522
            // 
            this.panel522.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel522.Location = new System.Drawing.Point(381, 0);
            this.panel522.Name = "panel522";
            this.panel522.Size = new System.Drawing.Size(17, 109);
            this.panel522.TabIndex = 52;
            // 
            // panel520
            // 
            this.panel520.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel520.Location = new System.Drawing.Point(364, 0);
            this.panel520.Name = "panel520";
            this.panel520.Size = new System.Drawing.Size(17, 109);
            this.panel520.TabIndex = 51;
            // 
            // labelPage3PatientID
            // 
            this.labelPage3PatientID.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPage3PatientID.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelPage3PatientID.Location = new System.Drawing.Point(135, 0);
            this.labelPage3PatientID.Name = "labelPage3PatientID";
            this.labelPage3PatientID.Size = new System.Drawing.Size(229, 109);
            this.labelPage3PatientID.TabIndex = 50;
            this.labelPage3PatientID.Text = "12341234567";
            this.labelPage3PatientID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelPatientID
            // 
            this.labelPatientID.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPatientID.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPatientID.Location = new System.Drawing.Point(0, 0);
            this.labelPatientID.Name = "labelPatientID";
            this.labelPatientID.Size = new System.Drawing.Size(135, 109);
            this.labelPatientID.TabIndex = 49;
            this.labelPatientID.Text = "Patient ID:";
            this.labelPatientID.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelReportText3
            // 
            this.labelReportText3.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelReportText3.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelReportText3.Location = new System.Drawing.Point(1445, 0);
            this.labelReportText3.Name = "labelReportText3";
            this.labelReportText3.Size = new System.Drawing.Size(94, 109);
            this.labelReportText3.TabIndex = 43;
            this.labelReportText3.Text = "Report:";
            this.labelReportText3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPage3ReportNr
            // 
            this.labelPage3ReportNr.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelPage3ReportNr.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelPage3ReportNr.Location = new System.Drawing.Point(1539, 0);
            this.labelPage3ReportNr.Name = "labelPage3ReportNr";
            this.labelPage3ReportNr.Size = new System.Drawing.Size(115, 109);
            this.labelPage3ReportNr.TabIndex = 44;
            this.labelPage3ReportNr.Text = "^9999";
            this.labelPage3ReportNr.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel500
            // 
            this.panel500.Controls.Add(this.labelPage3);
            this.panel500.Controls.Add(this.pictureBoxCenter3);
            this.panel500.Controls.Add(this.panel501);
            this.panel500.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel500.Location = new System.Drawing.Point(0, 0);
            this.panel500.Name = "panel500";
            this.panel500.Size = new System.Drawing.Size(1654, 84);
            this.panel500.TabIndex = 5;
            // 
            // labelPage3
            // 
            this.labelPage3.AutoSize = true;
            this.labelPage3.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPage3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Italic);
            this.labelPage3.Location = new System.Drawing.Point(400, 0);
            this.labelPage3.Name = "labelPage3";
            this.labelPage3.Size = new System.Drawing.Size(99, 31);
            this.labelPage3.TabIndex = 9;
            this.labelPage3.Text = "Page 3";
            // 
            // pictureBoxCenter3
            // 
            this.pictureBoxCenter3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBoxCenter3.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBoxCenter3.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxCenter3.Name = "pictureBoxCenter3";
            this.pictureBoxCenter3.Size = new System.Drawing.Size(400, 84);
            this.pictureBoxCenter3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxCenter3.TabIndex = 0;
            this.pictureBoxCenter3.TabStop = false;
            // 
            // panel501
            // 
            this.panel501.Controls.Add(this.panel503);
            this.panel501.Controls.Add(this.panel504);
            this.panel501.Controls.Add(this.panel505);
            this.panel501.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel501.Location = new System.Drawing.Point(1254, 0);
            this.panel501.Name = "panel501";
            this.panel501.Size = new System.Drawing.Size(400, 84);
            this.panel501.TabIndex = 8;
            // 
            // panel503
            // 
            this.panel503.Controls.Add(this.labelCenter2p3);
            this.panel503.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel503.Font = new System.Drawing.Font("Verdana", 18F);
            this.panel503.Location = new System.Drawing.Point(0, 50);
            this.panel503.Name = "panel503";
            this.panel503.Size = new System.Drawing.Size(400, 34);
            this.panel503.TabIndex = 15;
            // 
            // labelCenter2p3
            // 
            this.labelCenter2p3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCenter2p3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.labelCenter2p3.Location = new System.Drawing.Point(0, 0);
            this.labelCenter2p3.Name = "labelCenter2p3";
            this.labelCenter2p3.Size = new System.Drawing.Size(400, 34);
            this.labelCenter2p3.TabIndex = 12;
            this.labelCenter2p3.Text = "c2";
            this.labelCenter2p3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel504
            // 
            this.panel504.Controls.Add(this.labelCenter1p3);
            this.panel504.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel504.Font = new System.Drawing.Font("Verdana", 18F);
            this.panel504.Location = new System.Drawing.Point(0, 10);
            this.panel504.Name = "panel504";
            this.panel504.Size = new System.Drawing.Size(400, 34);
            this.panel504.TabIndex = 14;
            // 
            // labelCenter1p3
            // 
            this.labelCenter1p3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCenter1p3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.labelCenter1p3.Location = new System.Drawing.Point(0, 0);
            this.labelCenter1p3.Name = "labelCenter1p3";
            this.labelCenter1p3.Size = new System.Drawing.Size(400, 34);
            this.labelCenter1p3.TabIndex = 11;
            this.labelCenter1p3.Text = "c1";
            this.labelCenter1p3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel505
            // 
            this.panel505.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel505.Location = new System.Drawing.Point(0, 0);
            this.panel505.Name = "panel505";
            this.panel505.Size = new System.Drawing.Size(400, 10);
            this.panel505.TabIndex = 13;
            // 
            // panelPrintArea2
            // 
            this.panelPrintArea2.BackColor = System.Drawing.Color.White;
            this.panelPrintArea2.Controls.Add(this.panelGridTableFill);
            this.panelPrintArea2.Controls.Add(this.panel78);
            this.panelPrintArea2.Controls.Add(this.panelHeaderPage2);
            this.panelPrintArea2.Controls.Add(this.panel632);
            this.panelPrintArea2.Controls.Add(this.panel785);
            this.panelPrintArea2.Location = new System.Drawing.Point(0, 3430);
            this.panelPrintArea2.Margin = new System.Windows.Forms.Padding(0);
            this.panelPrintArea2.Name = "panelPrintArea2";
            this.panelPrintArea2.Size = new System.Drawing.Size(1654, 2339);
            this.panelPrintArea2.TabIndex = 4;
            // 
            // panelGridTableFill
            // 
            this.panelGridTableFill.AutoSize = true;
            this.panelGridTableFill.Controls.Add(this.panelGridTable);
            this.panelGridTableFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelGridTableFill.Font = new System.Drawing.Font("Verdana", 15F);
            this.panelGridTableFill.Location = new System.Drawing.Point(0, 250);
            this.panelGridTableFill.Name = "panelGridTableFill";
            this.panelGridTableFill.Size = new System.Drawing.Size(1654, 2042);
            this.panelGridTableFill.TabIndex = 43;
            // 
            // panelGridTable
            // 
            this.panelGridTable.AutoSize = true;
            this.panelGridTable.Controls.Add(this.dataGridView);
            this.panelGridTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelGridTable.Font = new System.Drawing.Font("Verdana", 18F);
            this.panelGridTable.Location = new System.Drawing.Point(0, 0);
            this.panelGridTable.Name = "panelGridTable";
            this.panelGridTable.Size = new System.Drawing.Size(1654, 2042);
            this.panelGridTable.TabIndex = 3;
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGridView.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column7,
            this.Column4,
            this.ColumnEventDate,
            this.ColumnSampleNr,
            this.Column5,
            this.Column3,
            this.ColumnFindings,
            this.Column6,
            this.ColumnRemarks,
            this.Column9,
            this.Column11,
            this.Column10,
            this.Column2,
            this.Column1});
            this.dataGridView.Cursor = System.Windows.Forms.Cursors.PanWest;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Verdana", 18F);
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView.GridColor = System.Drawing.SystemColors.Control;
            this.dataGridView.Location = new System.Drawing.Point(0, 0);
            this.dataGridView.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataGridView.MinimumSize = new System.Drawing.Size(0, 60);
            this.dataGridView.MultiSelect = false;
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            this.dataGridView.RowHeadersVisible = false;
            this.dataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView.RowsDefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridView.RowTemplate.Height = 45;
            this.dataGridView.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView.ShowCellErrors = false;
            this.dataGridView.ShowEditingIcon = false;
            this.dataGridView.Size = new System.Drawing.Size(1654, 2042);
            this.dataGridView.TabIndex = 4;
            this.dataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellContentClick);
            // 
            // Column7
            // 
            this.Column7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column7.DefaultCellStyle = dataGridViewCellStyle2;
            this.Column7.HeaderText = "# ";
            this.Column7.MaxInputLength = 7;
            this.Column7.MinimumWidth = 60;
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 60;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.Column4.HeaderText = "Event #";
            this.Column4.MaxInputLength = 5;
            this.Column4.MinimumWidth = 125;
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Visible = false;
            // 
            // ColumnEventDate
            // 
            this.ColumnEventDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ColumnEventDate.HeaderText = "Event Date Time";
            this.ColumnEventDate.MaxInputLength = 25;
            this.ColumnEventDate.MinimumWidth = 360;
            this.ColumnEventDate.Name = "ColumnEventDate";
            this.ColumnEventDate.ReadOnly = true;
            this.ColumnEventDate.Width = 360;
            // 
            // ColumnSampleNr
            // 
            this.ColumnSampleNr.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.ColumnSampleNr.HeaderText = "A #";
            this.ColumnSampleNr.MaxInputLength = 8;
            this.ColumnSampleNr.MinimumWidth = 50;
            this.ColumnSampleNr.Name = "ColumnSampleNr";
            this.ColumnSampleNr.ReadOnly = true;
            this.ColumnSampleNr.Visible = false;
            // 
            // Column5
            // 
            this.Column5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column5.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column5.HeaderText = "M A";
            this.Column5.MaxInputLength = 7;
            this.Column5.MinimumWidth = 50;
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 50;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Column3.HeaderText = "Class";
            this.Column3.MaxInputLength = 9;
            this.Column3.MinimumWidth = 120;
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 120;
            // 
            // ColumnFindings
            // 
            this.ColumnFindings.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.ColumnFindings.HeaderText = "Rhythm";
            this.ColumnFindings.MaxInputLength = 9;
            this.ColumnFindings.MinimumWidth = 200;
            this.ColumnFindings.Name = "ColumnFindings";
            this.ColumnFindings.ReadOnly = true;
            this.ColumnFindings.Visible = false;
            // 
            // Column6
            // 
            this.Column6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column6.FillWeight = 10F;
            this.Column6.HeaderText = "Findings";
            this.Column6.MinimumWidth = 100;
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // ColumnRemarks
            // 
            this.ColumnRemarks.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ColumnRemarks.HeaderText = "Symptoms";
            this.ColumnRemarks.MaxInputLength = 32;
            this.ColumnRemarks.MinimumWidth = 280;
            this.ColumnRemarks.Name = "ColumnRemarks";
            this.ColumnRemarks.ReadOnly = true;
            this.ColumnRemarks.Width = 280;
            // 
            // Column9
            // 
            this.Column9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.Column9.DefaultCellStyle = dataGridViewCellStyle4;
            this.Column9.FillWeight = 60F;
            this.Column9.HeaderText = "HR Min";
            this.Column9.MaxInputLength = 4;
            this.Column9.MinimumWidth = 80;
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 80;
            // 
            // Column11
            // 
            this.Column11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.Column11.DefaultCellStyle = dataGridViewCellStyle5;
            this.Column11.HeaderText = "HR Mean";
            this.Column11.MaxInputLength = 4;
            this.Column11.MinimumWidth = 80;
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.Width = 80;
            // 
            // Column10
            // 
            this.Column10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.Column10.DefaultCellStyle = dataGridViewCellStyle6;
            this.Column10.HeaderText = "HR Max";
            this.Column10.MaxInputLength = 4;
            this.Column10.MinimumWidth = 80;
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Width = 80;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.Column2.HeaderText = "Tech";
            this.Column2.MaxInputLength = 4;
            this.Column2.MinimumWidth = 100;
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Visible = false;
            // 
            // Column1
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Column1.DefaultCellStyle = dataGridViewCellStyle7;
            this.Column1.HeaderText = "Page";
            this.Column1.MaxInputLength = 8;
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 80;
            // 
            // panel78
            // 
            this.panel78.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel78.Location = new System.Drawing.Point(0, 2292);
            this.panel78.Name = "panel78";
            this.panel78.Size = new System.Drawing.Size(1654, 10);
            this.panel78.TabIndex = 45;
            // 
            // panelHeaderPage2
            // 
            this.panelHeaderPage2.AutoSize = true;
            this.panelHeaderPage2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panelHeaderPage2.Controls.Add(this.panel37);
            this.panelHeaderPage2.Controls.Add(this.panel508);
            this.panelHeaderPage2.Controls.Add(this.panel745);
            this.panelHeaderPage2.Controls.Add(this.panel799);
            this.panelHeaderPage2.Controls.Add(this.panel800);
            this.panelHeaderPage2.Controls.Add(this.panelPage2Header2);
            this.panelHeaderPage2.Controls.Add(this.panelPage2Header1);
            this.panelHeaderPage2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeaderPage2.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.panelHeaderPage2.Location = new System.Drawing.Point(0, 1);
            this.panelHeaderPage2.Name = "panelHeaderPage2";
            this.panelHeaderPage2.Size = new System.Drawing.Size(1654, 249);
            this.panelHeaderPage2.TabIndex = 42;
            // 
            // panel37
            // 
            this.panel37.Controls.Add(this.panel40);
            this.panel37.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel37.Location = new System.Drawing.Point(0, 199);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(1654, 50);
            this.panel37.TabIndex = 35;
            // 
            // panel40
            // 
            this.panel40.Controls.Add(this.labelNrStripsTable);
            this.panel40.Controls.Add(this.label10);
            this.panel40.Controls.Add(this.label34);
            this.panel40.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel40.Location = new System.Drawing.Point(0, 0);
            this.panel40.Name = "panel40";
            this.panel40.Size = new System.Drawing.Size(1654, 50);
            this.panel40.TabIndex = 2;
            // 
            // labelNrStripsTable
            // 
            this.labelNrStripsTable.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelNrStripsTable.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelNrStripsTable.Location = new System.Drawing.Point(1433, 0);
            this.labelNrStripsTable.Name = "labelNrStripsTable";
            this.labelNrStripsTable.Size = new System.Drawing.Size(107, 50);
            this.labelNrStripsTable.TabIndex = 4;
            this.labelNrStripsTable.Text = "^999";
            this.labelNrStripsTable.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.Dock = System.Windows.Forms.DockStyle.Right;
            this.label10.Font = new System.Drawing.Font("Verdana", 20F);
            this.label10.Location = new System.Drawing.Point(1540, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(114, 50);
            this.label10.TabIndex = 3;
            this.label10.Text = "Strips";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label34
            // 
            this.label34.Dock = System.Windows.Forms.DockStyle.Left;
            this.label34.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.label34.Location = new System.Drawing.Point(0, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(342, 50);
            this.label34.TabIndex = 2;
            this.label34.Text = "Event Table";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel508
            // 
            this.panel508.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel508.Location = new System.Drawing.Point(0, 185);
            this.panel508.Name = "panel508";
            this.panel508.Size = new System.Drawing.Size(1654, 14);
            this.panel508.TabIndex = 32;
            // 
            // panel745
            // 
            this.panel745.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel745.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel745.Location = new System.Drawing.Point(0, 183);
            this.panel745.Name = "panel745";
            this.panel745.Size = new System.Drawing.Size(1654, 2);
            this.panel745.TabIndex = 20;
            // 
            // panel799
            // 
            this.panel799.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel799.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel799.Location = new System.Drawing.Point(0, 182);
            this.panel799.Name = "panel799";
            this.panel799.Size = new System.Drawing.Size(1654, 1);
            this.panel799.TabIndex = 3;
            // 
            // panel800
            // 
            this.panel800.Controls.Add(this.panel801);
            this.panel800.Controls.Add(this.panel802);
            this.panel800.Controls.Add(this.panel804);
            this.panel800.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel800.Location = new System.Drawing.Point(0, 172);
            this.panel800.Name = "panel800";
            this.panel800.Size = new System.Drawing.Size(1654, 10);
            this.panel800.TabIndex = 1;
            // 
            // panel801
            // 
            this.panel801.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel801.Location = new System.Drawing.Point(1477, 0);
            this.panel801.Name = "panel801";
            this.panel801.Size = new System.Drawing.Size(177, 10);
            this.panel801.TabIndex = 5;
            // 
            // panel802
            // 
            this.panel802.Controls.Add(this.panel803);
            this.panel802.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel802.Location = new System.Drawing.Point(234, 0);
            this.panel802.Name = "panel802";
            this.panel802.Size = new System.Drawing.Size(1420, 10);
            this.panel802.TabIndex = 4;
            // 
            // panel803
            // 
            this.panel803.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel803.Location = new System.Drawing.Point(0, 0);
            this.panel803.Name = "panel803";
            this.panel803.Size = new System.Drawing.Size(971, 10);
            this.panel803.TabIndex = 4;
            // 
            // panel804
            // 
            this.panel804.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel804.Location = new System.Drawing.Point(0, 0);
            this.panel804.Name = "panel804";
            this.panel804.Size = new System.Drawing.Size(234, 10);
            this.panel804.TabIndex = 3;
            // 
            // panelPage2Header2
            // 
            this.panelPage2Header2.Controls.Add(this.panel533);
            this.panelPage2Header2.Controls.Add(this.panel531);
            this.panelPage2Header2.Controls.Add(this.labelPage2DateOfBirth);
            this.panelPage2Header2.Controls.Add(this.label94);
            this.panelPage2Header2.Controls.Add(this.panel819);
            this.panelPage2Header2.Controls.Add(this.panel821);
            this.panelPage2Header2.Controls.Add(this.labelPage2PatLastName);
            this.panelPage2Header2.Controls.Add(this.label96);
            this.panelPage2Header2.Controls.Add(this.panel120);
            this.panelPage2Header2.Controls.Add(this.panel121);
            this.panelPage2Header2.Controls.Add(this.labelPage2PatientID);
            this.panelPage2Header2.Controls.Add(this.label99);
            this.panelPage2Header2.Controls.Add(this.labelReportText2);
            this.panelPage2Header2.Controls.Add(this.labelPage2ReportNr);
            this.panelPage2Header2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelPage2Header2.Location = new System.Drawing.Point(0, 84);
            this.panelPage2Header2.Name = "panelPage2Header2";
            this.panelPage2Header2.Size = new System.Drawing.Size(1654, 88);
            this.panelPage2Header2.TabIndex = 0;
            // 
            // panel533
            // 
            this.panel533.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel533.Location = new System.Drawing.Point(1424, 0);
            this.panel533.Name = "panel533";
            this.panel533.Size = new System.Drawing.Size(10, 88);
            this.panel533.TabIndex = 61;
            // 
            // panel531
            // 
            this.panel531.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel531.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel531.Location = new System.Drawing.Point(1422, 0);
            this.panel531.Name = "panel531";
            this.panel531.Size = new System.Drawing.Size(2, 88);
            this.panel531.TabIndex = 60;
            // 
            // labelPage2DateOfBirth
            // 
            this.labelPage2DateOfBirth.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPage2DateOfBirth.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelPage2DateOfBirth.Location = new System.Drawing.Point(1207, 0);
            this.labelPage2DateOfBirth.Name = "labelPage2DateOfBirth";
            this.labelPage2DateOfBirth.Size = new System.Drawing.Size(215, 88);
            this.labelPage2DateOfBirth.TabIndex = 54;
            this.labelPage2DateOfBirth.Text = "99/99/9999";
            this.labelPage2DateOfBirth.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label94
            // 
            this.label94.Dock = System.Windows.Forms.DockStyle.Left;
            this.label94.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label94.Location = new System.Drawing.Point(1131, 0);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(76, 88);
            this.label94.TabIndex = 53;
            this.label94.Text = "DOB:";
            this.label94.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel819
            // 
            this.panel819.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel819.Location = new System.Drawing.Point(1114, 0);
            this.panel819.Name = "panel819";
            this.panel819.Size = new System.Drawing.Size(17, 88);
            this.panel819.TabIndex = 52;
            // 
            // panel821
            // 
            this.panel821.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel821.Location = new System.Drawing.Point(1097, 0);
            this.panel821.Name = "panel821";
            this.panel821.Size = new System.Drawing.Size(17, 88);
            this.panel821.TabIndex = 51;
            // 
            // labelPage2PatLastName
            // 
            this.labelPage2PatLastName.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPage2PatLastName.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelPage2PatLastName.Location = new System.Drawing.Point(511, 0);
            this.labelPage2PatLastName.Name = "labelPage2PatLastName";
            this.labelPage2PatLastName.Size = new System.Drawing.Size(586, 88);
            this.labelPage2PatLastName.TabIndex = 50;
            this.labelPage2PatLastName.Text = "^Brest van Kempen, Rutger, gfhjkfhjfhkffghkf, hjfgjhghj";
            this.labelPage2PatLastName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label96
            // 
            this.label96.Dock = System.Windows.Forms.DockStyle.Left;
            this.label96.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label96.Location = new System.Drawing.Point(421, 0);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(90, 88);
            this.label96.TabIndex = 47;
            this.label96.Text = "Name:";
            this.label96.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel120
            // 
            this.panel120.Controls.Add(this.panel124);
            this.panel120.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel120.Location = new System.Drawing.Point(404, 0);
            this.panel120.Name = "panel120";
            this.panel120.Size = new System.Drawing.Size(17, 88);
            this.panel120.TabIndex = 44;
            // 
            // panel124
            // 
            this.panel124.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel124.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel124.Location = new System.Drawing.Point(0, 0);
            this.panel124.Name = "panel124";
            this.panel124.Size = new System.Drawing.Size(2, 88);
            this.panel124.TabIndex = 16;
            // 
            // panel121
            // 
            this.panel121.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel121.Location = new System.Drawing.Point(387, 0);
            this.panel121.Name = "panel121";
            this.panel121.Size = new System.Drawing.Size(17, 88);
            this.panel121.TabIndex = 43;
            // 
            // labelPage2PatientID
            // 
            this.labelPage2PatientID.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPage2PatientID.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelPage2PatientID.Location = new System.Drawing.Point(150, 0);
            this.labelPage2PatientID.Name = "labelPage2PatientID";
            this.labelPage2PatientID.Size = new System.Drawing.Size(237, 88);
            this.labelPage2PatientID.TabIndex = 46;
            this.labelPage2PatientID.Text = "12341234567";
            this.labelPage2PatientID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label99
            // 
            this.label99.Dock = System.Windows.Forms.DockStyle.Left;
            this.label99.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label99.Location = new System.Drawing.Point(0, 0);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(150, 88);
            this.label99.TabIndex = 45;
            this.label99.Text = "Patient ID:";
            this.label99.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelReportText2
            // 
            this.labelReportText2.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelReportText2.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelReportText2.Location = new System.Drawing.Point(1439, 0);
            this.labelReportText2.Name = "labelReportText2";
            this.labelReportText2.Size = new System.Drawing.Size(95, 88);
            this.labelReportText2.TabIndex = 37;
            this.labelReportText2.Text = "Report:";
            this.labelReportText2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPage2ReportNr
            // 
            this.labelPage2ReportNr.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelPage2ReportNr.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelPage2ReportNr.Location = new System.Drawing.Point(1534, 0);
            this.labelPage2ReportNr.Name = "labelPage2ReportNr";
            this.labelPage2ReportNr.Size = new System.Drawing.Size(120, 88);
            this.labelPage2ReportNr.TabIndex = 42;
            this.labelPage2ReportNr.Text = "^9999";
            this.labelPage2ReportNr.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panelPage2Header1
            // 
            this.panelPage2Header1.Controls.Add(this.labelPage2);
            this.panelPage2Header1.Controls.Add(this.pictureBoxCenter2);
            this.panelPage2Header1.Controls.Add(this.panel826);
            this.panelPage2Header1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelPage2Header1.Location = new System.Drawing.Point(0, 0);
            this.panelPage2Header1.Name = "panelPage2Header1";
            this.panelPage2Header1.Size = new System.Drawing.Size(1654, 84);
            this.panelPage2Header1.TabIndex = 4;
            // 
            // labelPage2
            // 
            this.labelPage2.AutoSize = true;
            this.labelPage2.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPage2.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPage2.Location = new System.Drawing.Point(400, 0);
            this.labelPage2.Name = "labelPage2";
            this.labelPage2.Size = new System.Drawing.Size(177, 55);
            this.labelPage2.TabIndex = 9;
            this.labelPage2.Text = "Page 2";
            // 
            // pictureBoxCenter2
            // 
            this.pictureBoxCenter2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBoxCenter2.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBoxCenter2.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxCenter2.Name = "pictureBoxCenter2";
            this.pictureBoxCenter2.Size = new System.Drawing.Size(400, 84);
            this.pictureBoxCenter2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxCenter2.TabIndex = 0;
            this.pictureBoxCenter2.TabStop = false;
            // 
            // panel826
            // 
            this.panel826.Controls.Add(this.panel828);
            this.panel826.Controls.Add(this.panel829);
            this.panel826.Controls.Add(this.panel830);
            this.panel826.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel826.Location = new System.Drawing.Point(1254, 0);
            this.panel826.Name = "panel826";
            this.panel826.Size = new System.Drawing.Size(400, 84);
            this.panel826.TabIndex = 8;
            // 
            // panel828
            // 
            this.panel828.Controls.Add(this.labelCenter2p2);
            this.panel828.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel828.Location = new System.Drawing.Point(0, 49);
            this.panel828.Name = "panel828";
            this.panel828.Size = new System.Drawing.Size(400, 34);
            this.panel828.TabIndex = 15;
            // 
            // labelCenter2p2
            // 
            this.labelCenter2p2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCenter2p2.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelCenter2p2.Location = new System.Drawing.Point(0, 0);
            this.labelCenter2p2.Name = "labelCenter2p2";
            this.labelCenter2p2.Size = new System.Drawing.Size(400, 34);
            this.labelCenter2p2.TabIndex = 12;
            this.labelCenter2p2.Text = "c2";
            this.labelCenter2p2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel829
            // 
            this.panel829.Controls.Add(this.labelCenter1p2);
            this.panel829.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel829.Location = new System.Drawing.Point(0, 15);
            this.panel829.Name = "panel829";
            this.panel829.Size = new System.Drawing.Size(400, 34);
            this.panel829.TabIndex = 14;
            // 
            // labelCenter1p2
            // 
            this.labelCenter1p2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCenter1p2.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelCenter1p2.Location = new System.Drawing.Point(0, 0);
            this.labelCenter1p2.Name = "labelCenter1p2";
            this.labelCenter1p2.Size = new System.Drawing.Size(400, 34);
            this.labelCenter1p2.TabIndex = 11;
            this.labelCenter1p2.Text = "c1";
            this.labelCenter1p2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel830
            // 
            this.panel830.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel830.Location = new System.Drawing.Point(0, 0);
            this.panel830.Name = "panel830";
            this.panel830.Size = new System.Drawing.Size(400, 15);
            this.panel830.TabIndex = 13;
            // 
            // panel632
            // 
            this.panel632.AutoSize = true;
            this.panel632.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel632.Controls.Add(this.panel706);
            this.panel632.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel632.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.panel632.Location = new System.Drawing.Point(0, 0);
            this.panel632.Name = "panel632";
            this.panel632.Size = new System.Drawing.Size(1654, 1);
            this.panel632.TabIndex = 41;
            // 
            // panel706
            // 
            this.panel706.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel706.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel706.Location = new System.Drawing.Point(0, 0);
            this.panel706.Name = "panel706";
            this.panel706.Size = new System.Drawing.Size(1654, 1);
            this.panel706.TabIndex = 3;
            // 
            // panel785
            // 
            this.panel785.Controls.Add(this.label61);
            this.panel785.Controls.Add(this.labelPage2StudyNumber);
            this.panel785.Controls.Add(this.label80);
            this.panel785.Controls.Add(this.labelPage2Of);
            this.panel785.Controls.Add(this.label82);
            this.panel785.Controls.Add(this.labelPage2xOfX);
            this.panel785.Controls.Add(this.labelPrintDate2Bottom);
            this.panel785.Controls.Add(this.label86);
            this.panel785.Controls.Add(this.label87);
            this.panel785.Controls.Add(this.label88);
            this.panel785.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel785.Location = new System.Drawing.Point(0, 2302);
            this.panel785.Name = "panel785";
            this.panel785.Size = new System.Drawing.Size(1654, 37);
            this.panel785.TabIndex = 37;
            // 
            // label61
            // 
            this.label61.Dock = System.Windows.Forms.DockStyle.Left;
            this.label61.Font = new System.Drawing.Font("Verdana", 18F);
            this.label61.Location = new System.Drawing.Point(697, 0);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(460, 37);
            this.label61.TabIndex = 43;
            this.label61.Text = "2018 © Techmedic International B.V.";
            this.label61.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPage2StudyNumber
            // 
            this.labelPage2StudyNumber.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelPage2StudyNumber.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.labelPage2StudyNumber.Location = new System.Drawing.Point(1169, 0);
            this.labelPage2StudyNumber.Name = "labelPage2StudyNumber";
            this.labelPage2StudyNumber.Size = new System.Drawing.Size(164, 37);
            this.labelPage2StudyNumber.TabIndex = 44;
            this.labelPage2StudyNumber.Text = "1212212";
            this.labelPage2StudyNumber.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label80
            // 
            this.label80.Dock = System.Windows.Forms.DockStyle.Right;
            this.label80.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.Location = new System.Drawing.Point(1333, 0);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(81, 37);
            this.label80.TabIndex = 23;
            this.label80.Text = "Page";
            this.label80.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelPage2Of
            // 
            this.labelPage2Of.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelPage2Of.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.labelPage2Of.Location = new System.Drawing.Point(1414, 0);
            this.labelPage2Of.Name = "labelPage2Of";
            this.labelPage2Of.Size = new System.Drawing.Size(80, 37);
            this.labelPage2Of.TabIndex = 22;
            this.labelPage2Of.Text = "2 A";
            this.labelPage2Of.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label82
            // 
            this.label82.Dock = System.Windows.Forms.DockStyle.Right;
            this.label82.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.Location = new System.Drawing.Point(1494, 0);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(36, 37);
            this.label82.TabIndex = 21;
            this.label82.Text = "of";
            this.label82.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPage2xOfX
            // 
            this.labelPage2xOfX.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelPage2xOfX.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.labelPage2xOfX.Location = new System.Drawing.Point(1530, 0);
            this.labelPage2xOfX.Name = "labelPage2xOfX";
            this.labelPage2xOfX.Size = new System.Drawing.Size(97, 37);
            this.labelPage2xOfX.TabIndex = 20;
            this.labelPage2xOfX.Text = "20p2";
            this.labelPage2xOfX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPrintDate2Bottom
            // 
            this.labelPrintDate2Bottom.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPrintDate2Bottom.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.labelPrintDate2Bottom.Location = new System.Drawing.Point(129, 0);
            this.labelPrintDate2Bottom.Name = "labelPrintDate2Bottom";
            this.labelPrintDate2Bottom.Size = new System.Drawing.Size(568, 37);
            this.labelPrintDate2Bottom.TabIndex = 18;
            this.labelPrintDate2Bottom.Text = "12/27/2016 22:22:22 AM+1200 v12345";
            this.labelPrintDate2Bottom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label86
            // 
            this.label86.Dock = System.Windows.Forms.DockStyle.Left;
            this.label86.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.Location = new System.Drawing.Point(27, 0);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(102, 37);
            this.label86.TabIndex = 17;
            this.label86.Text = "Printed:";
            this.label86.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label87
            // 
            this.label87.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label87.Dock = System.Windows.Forms.DockStyle.Right;
            this.label87.Font = new System.Drawing.Font("Verdana", 10F);
            this.label87.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label87.Location = new System.Drawing.Point(1627, 0);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(27, 37);
            this.label87.TabIndex = 1;
            this.label87.Text = "R2";
            this.label87.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label87.Visible = false;
            // 
            // label88
            // 
            this.label88.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label88.Dock = System.Windows.Forms.DockStyle.Left;
            this.label88.Font = new System.Drawing.Font("Verdana", 10F);
            this.label88.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label88.Location = new System.Drawing.Point(0, 0);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(27, 37);
            this.label88.TabIndex = 0;
            this.label88.Text = "L2";
            this.label88.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label88.Visible = false;
            // 
            // panelPrintHeader
            // 
            this.panelPrintHeader.Controls.Add(this.labelPage1);
            this.panelPrintHeader.Controls.Add(this.pictureBoxCenter);
            this.panelPrintHeader.Controls.Add(this.panel56);
            this.panelPrintHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelPrintHeader.Location = new System.Drawing.Point(0, 0);
            this.panelPrintHeader.Name = "panelPrintHeader";
            this.panelPrintHeader.Size = new System.Drawing.Size(1654, 84);
            this.panelPrintHeader.TabIndex = 4;
            // 
            // labelPage1
            // 
            this.labelPage1.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPage1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPage1.Location = new System.Drawing.Point(300, 0);
            this.labelPage1.Name = "labelPage1";
            this.labelPage1.Size = new System.Drawing.Size(177, 84);
            this.labelPage1.TabIndex = 9;
            this.labelPage1.Text = "Page 1";
            // 
            // pictureBoxCenter
            // 
            this.pictureBoxCenter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBoxCenter.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBoxCenter.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxCenter.Name = "pictureBoxCenter";
            this.pictureBoxCenter.Size = new System.Drawing.Size(300, 84);
            this.pictureBoxCenter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxCenter.TabIndex = 0;
            this.pictureBoxCenter.TabStop = false;
            // 
            // panel56
            // 
            this.panel56.Controls.Add(this.panel58);
            this.panel56.Controls.Add(this.panel59);
            this.panel56.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel56.Location = new System.Drawing.Point(1254, 0);
            this.panel56.Name = "panel56";
            this.panel56.Size = new System.Drawing.Size(400, 84);
            this.panel56.TabIndex = 8;
            // 
            // panel58
            // 
            this.panel58.Controls.Add(this.labelCenter2);
            this.panel58.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel58.Location = new System.Drawing.Point(0, 50);
            this.panel58.Name = "panel58";
            this.panel58.Size = new System.Drawing.Size(400, 34);
            this.panel58.TabIndex = 15;
            // 
            // labelCenter2
            // 
            this.labelCenter2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCenter2.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelCenter2.Location = new System.Drawing.Point(0, 0);
            this.labelCenter2.Name = "labelCenter2";
            this.labelCenter2.Size = new System.Drawing.Size(400, 34);
            this.labelCenter2.TabIndex = 12;
            this.labelCenter2.Text = "c2";
            this.labelCenter2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel59
            // 
            this.panel59.Controls.Add(this.labelCenter1);
            this.panel59.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel59.Location = new System.Drawing.Point(0, 0);
            this.panel59.Name = "panel59";
            this.panel59.Size = new System.Drawing.Size(400, 34);
            this.panel59.TabIndex = 14;
            // 
            // labelCenter1
            // 
            this.labelCenter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelCenter1.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelCenter1.Location = new System.Drawing.Point(0, 0);
            this.labelCenter1.Name = "labelCenter1";
            this.labelCenter1.Size = new System.Drawing.Size(400, 34);
            this.labelCenter1.TabIndex = 11;
            this.labelCenter1.Text = "c1";
            this.labelCenter1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelCardiacEVentReportNumber
            // 
            this.panelCardiacEVentReportNumber.Controls.Add(this.panel4);
            this.panelCardiacEVentReportNumber.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCardiacEVentReportNumber.Location = new System.Drawing.Point(0, 84);
            this.panelCardiacEVentReportNumber.Name = "panelCardiacEVentReportNumber";
            this.panelCardiacEVentReportNumber.Size = new System.Drawing.Size(1654, 103);
            this.panelCardiacEVentReportNumber.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.panel50);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1654, 100);
            this.panel4.TabIndex = 2;
            // 
            // panel50
            // 
            this.panel50.Controls.Add(this.labelCardiacEventReportNr);
            this.panel50.Controls.Add(this.labelReportNr);
            this.panel50.Controls.Add(this.panel28);
            this.panel50.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel50.Location = new System.Drawing.Point(0, 0);
            this.panel50.Name = "panel50";
            this.panel50.Size = new System.Drawing.Size(1654, 100);
            this.panel50.TabIndex = 1;
            // 
            // labelCardiacEventReportNr
            // 
            this.labelCardiacEventReportNr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCardiacEventReportNr.Font = new System.Drawing.Font("Verdana", 38F, System.Drawing.FontStyle.Bold);
            this.labelCardiacEventReportNr.Location = new System.Drawing.Point(215, 0);
            this.labelCardiacEventReportNr.Name = "labelCardiacEventReportNr";
            this.labelCardiacEventReportNr.Size = new System.Drawing.Size(1224, 100);
            this.labelCardiacEventReportNr.TabIndex = 1;
            this.labelCardiacEventReportNr.Text = "^End of Study Report  Summary";
            this.labelCardiacEventReportNr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelReportNr
            // 
            this.labelReportNr.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelReportNr.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelReportNr.Location = new System.Drawing.Point(1439, 0);
            this.labelReportNr.Name = "labelReportNr";
            this.labelReportNr.Size = new System.Drawing.Size(215, 100);
            this.labelReportNr.TabIndex = 14;
            this.labelReportNr.Text = "^Not";
            this.labelReportNr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelReportNr.Visible = false;
            // 
            // panel28
            // 
            this.panel28.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel28.Location = new System.Drawing.Point(0, 0);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(215, 100);
            this.panel28.TabIndex = 13;
            // 
            // panelDateTimeofEventStrip
            // 
            this.panelDateTimeofEventStrip.Controls.Add(this.panel53);
            this.panelDateTimeofEventStrip.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelDateTimeofEventStrip.Location = new System.Drawing.Point(0, 187);
            this.panelDateTimeofEventStrip.Name = "panelDateTimeofEventStrip";
            this.panelDateTimeofEventStrip.Size = new System.Drawing.Size(1654, 66);
            this.panelDateTimeofEventStrip.TabIndex = 1;
            // 
            // panel53
            // 
            this.panel53.Controls.Add(this.labelSingleEventReportDate);
            this.panel53.Controls.Add(this.labelPrintDateHeader);
            this.panel53.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel53.Location = new System.Drawing.Point(0, 0);
            this.panel53.Name = "panel53";
            this.panel53.Size = new System.Drawing.Size(1654, 66);
            this.panel53.TabIndex = 4;
            // 
            // labelSingleEventReportDate
            // 
            this.labelSingleEventReportDate.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSingleEventReportDate.Font = new System.Drawing.Font("Verdana", 28F);
            this.labelSingleEventReportDate.Location = new System.Drawing.Point(668, 0);
            this.labelSingleEventReportDate.Name = "labelSingleEventReportDate";
            this.labelSingleEventReportDate.Size = new System.Drawing.Size(585, 66);
            this.labelSingleEventReportDate.TabIndex = 11;
            this.labelSingleEventReportDate.Text = "^11/12/2016 22:22:99 AM";
            // 
            // labelPrintDateHeader
            // 
            this.labelPrintDateHeader.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPrintDateHeader.Font = new System.Drawing.Font("Verdana", 28F);
            this.labelPrintDateHeader.Location = new System.Drawing.Point(0, 0);
            this.labelPrintDateHeader.Name = "labelPrintDateHeader";
            this.labelPrintDateHeader.Size = new System.Drawing.Size(668, 66);
            this.labelPrintDateHeader.TabIndex = 10;
            this.labelPrintDateHeader.Text = "Printed:";
            this.labelPrintDateHeader.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panelHorSepPatDet
            // 
            this.panelHorSepPatDet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelHorSepPatDet.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHorSepPatDet.Location = new System.Drawing.Point(0, 654);
            this.panelHorSepPatDet.Name = "panelHorSepPatDet";
            this.panelHorSepPatDet.Size = new System.Drawing.Size(1654, 1);
            this.panelHorSepPatDet.TabIndex = 3;
            // 
            // panelStudyProcedures
            // 
            this.panelStudyProcedures.Controls.Add(this.panelDiagnosis);
            this.panelStudyProcedures.Controls.Add(this.panel158);
            this.panelStudyProcedures.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelStudyProcedures.Location = new System.Drawing.Point(0, 655);
            this.panelStudyProcedures.Name = "panelStudyProcedures";
            this.panelStudyProcedures.Size = new System.Drawing.Size(1654, 178);
            this.panelStudyProcedures.TabIndex = 10;
            // 
            // panelDiagnosis
            // 
            this.panelDiagnosis.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelDiagnosis.Controls.Add(this.panelHartRate);
            this.panelDiagnosis.Controls.Add(this.panel5);
            this.panelDiagnosis.Controls.Add(this.panel69);
            this.panelDiagnosis.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelDiagnosis.Location = new System.Drawing.Point(0, 10);
            this.panelDiagnosis.Name = "panelDiagnosis";
            this.panelDiagnosis.Size = new System.Drawing.Size(1654, 156);
            this.panelDiagnosis.TabIndex = 36;
            // 
            // panelHartRate
            // 
            this.panelHartRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelHartRate.Controls.Add(this.panel54);
            this.panelHartRate.Controls.Add(this.panel47);
            this.panelHartRate.Controls.Add(this.panel52);
            this.panelHartRate.Controls.Add(this.panel46);
            this.panelHartRate.Controls.Add(this.panel2);
            this.panelHartRate.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelHartRate.Location = new System.Drawing.Point(1204, 0);
            this.panelHartRate.Name = "panelHartRate";
            this.panelHartRate.Size = new System.Drawing.Size(445, 154);
            this.panelHartRate.TabIndex = 5;
            // 
            // panel54
            // 
            this.panel54.Controls.Add(this.labelHrMaxPage);
            this.panel54.Controls.Add(this.labelHrMinPage);
            this.panel54.Controls.Add(this.label21);
            this.panel54.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel54.Location = new System.Drawing.Point(338, 0);
            this.panel54.Name = "panel54";
            this.panel54.Size = new System.Drawing.Size(94, 152);
            this.panel54.TabIndex = 16;
            // 
            // labelHrMaxPage
            // 
            this.labelHrMaxPage.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelHrMaxPage.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelHrMaxPage.Location = new System.Drawing.Point(0, 74);
            this.labelHrMaxPage.Name = "labelHrMaxPage";
            this.labelHrMaxPage.Size = new System.Drawing.Size(94, 38);
            this.labelHrMaxPage.TabIndex = 16;
            this.labelHrMaxPage.Text = "p41";
            this.labelHrMaxPage.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelHrMinPage
            // 
            this.labelHrMinPage.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelHrMinPage.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelHrMinPage.Location = new System.Drawing.Point(0, 36);
            this.labelHrMinPage.Name = "labelHrMinPage";
            this.labelHrMinPage.Size = new System.Drawing.Size(94, 38);
            this.labelHrMinPage.TabIndex = 3;
            this.labelHrMinPage.Text = "p2";
            this.labelHrMinPage.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label21
            // 
            this.label21.Dock = System.Windows.Forms.DockStyle.Top;
            this.label21.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.label21.Location = new System.Drawing.Point(0, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(94, 36);
            this.label21.TabIndex = 2;
            this.label21.Text = "Page";
            // 
            // panel47
            // 
            this.panel47.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel47.Location = new System.Drawing.Point(316, 0);
            this.panel47.Name = "panel47";
            this.panel47.Size = new System.Drawing.Size(22, 152);
            this.panel47.TabIndex = 14;
            // 
            // panel52
            // 
            this.panel52.Controls.Add(this.labelHrMaxIX);
            this.panel52.Controls.Add(this.labelHrMinIX);
            this.panel52.Controls.Add(this.label20);
            this.panel52.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel52.Location = new System.Drawing.Point(225, 0);
            this.panel52.Name = "panel52";
            this.panel52.Size = new System.Drawing.Size(91, 152);
            this.panel52.TabIndex = 15;
            // 
            // labelHrMaxIX
            // 
            this.labelHrMaxIX.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelHrMaxIX.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelHrMaxIX.Location = new System.Drawing.Point(0, 74);
            this.labelHrMaxIX.Name = "labelHrMaxIX";
            this.labelHrMaxIX.Size = new System.Drawing.Size(91, 38);
            this.labelHrMaxIX.TabIndex = 14;
            this.labelHrMaxIX.Text = "#210";
            this.labelHrMaxIX.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelHrMinIX
            // 
            this.labelHrMinIX.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelHrMinIX.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelHrMinIX.Location = new System.Drawing.Point(0, 36);
            this.labelHrMinIX.Name = "labelHrMinIX";
            this.labelHrMinIX.Size = new System.Drawing.Size(91, 38);
            this.labelHrMinIX.TabIndex = 2;
            this.labelHrMinIX.Text = "#14";
            this.labelHrMinIX.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label20
            // 
            this.label20.Dock = System.Windows.Forms.DockStyle.Top;
            this.label20.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.label20.Location = new System.Drawing.Point(0, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(91, 36);
            this.label20.TabIndex = 1;
            this.label20.Text = "Strip";
            // 
            // panel46
            // 
            this.panel46.Controls.Add(this.labelHrMean);
            this.panel46.Controls.Add(this.labelHrMax);
            this.panel46.Controls.Add(this.labelHrMin);
            this.panel46.Controls.Add(this.label17);
            this.panel46.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel46.Location = new System.Drawing.Point(141, 0);
            this.panel46.Name = "panel46";
            this.panel46.Size = new System.Drawing.Size(84, 152);
            this.panel46.TabIndex = 13;
            // 
            // labelHrMean
            // 
            this.labelHrMean.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelHrMean.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelHrMean.Location = new System.Drawing.Point(0, 112);
            this.labelHrMean.Name = "labelHrMean";
            this.labelHrMean.Size = new System.Drawing.Size(84, 38);
            this.labelHrMean.TabIndex = 5;
            this.labelHrMean.Text = "999";
            this.labelHrMean.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelHrMax
            // 
            this.labelHrMax.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelHrMax.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelHrMax.Location = new System.Drawing.Point(0, 74);
            this.labelHrMax.Name = "labelHrMax";
            this.labelHrMax.Size = new System.Drawing.Size(84, 38);
            this.labelHrMax.TabIndex = 4;
            this.labelHrMax.Text = "999";
            this.labelHrMax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelHrMin
            // 
            this.labelHrMin.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelHrMin.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelHrMin.Location = new System.Drawing.Point(0, 36);
            this.labelHrMin.Name = "labelHrMin";
            this.labelHrMin.Size = new System.Drawing.Size(84, 38);
            this.labelHrMin.TabIndex = 3;
            this.labelHrMin.Text = "999";
            this.labelHrMin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.Dock = System.Windows.Forms.DockStyle.Top;
            this.label17.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(0, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(84, 36);
            this.label17.TabIndex = 2;
            this.label17.Text = "BPM";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label51);
            this.panel2.Controls.Add(this.label50);
            this.panel2.Controls.Add(this.label49);
            this.panel2.Controls.Add(this.label48);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(141, 152);
            this.panel2.TabIndex = 12;
            // 
            // label51
            // 
            this.label51.Dock = System.Windows.Forms.DockStyle.Top;
            this.label51.Font = new System.Drawing.Font("Verdana", 20F);
            this.label51.Location = new System.Drawing.Point(0, 112);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(141, 38);
            this.label51.TabIndex = 4;
            this.label51.Text = "Average:";
            // 
            // label50
            // 
            this.label50.Dock = System.Windows.Forms.DockStyle.Top;
            this.label50.Font = new System.Drawing.Font("Verdana", 20F);
            this.label50.Location = new System.Drawing.Point(0, 74);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(141, 38);
            this.label50.TabIndex = 3;
            this.label50.Text = "Max:";
            // 
            // label49
            // 
            this.label49.Dock = System.Windows.Forms.DockStyle.Top;
            this.label49.Font = new System.Drawing.Font("Verdana", 20F);
            this.label49.Location = new System.Drawing.Point(0, 36);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(141, 38);
            this.label49.TabIndex = 2;
            this.label49.Text = "Min:";
            // 
            // label48
            // 
            this.label48.Dock = System.Windows.Forms.DockStyle.Top;
            this.label48.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.label48.Location = new System.Drawing.Point(0, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(141, 36);
            this.label48.TabIndex = 1;
            this.label48.Text = "Rate";
            // 
            // panel5
            // 
            this.panel5.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel5.Location = new System.Drawing.Point(1168, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(36, 154);
            this.panel5.TabIndex = 6;
            // 
            // panel69
            // 
            this.panel69.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel69.Controls.Add(this.labelDiagnosis);
            this.panel69.Controls.Add(this.label1);
            this.panel69.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel69.Location = new System.Drawing.Point(0, 0);
            this.panel69.Name = "panel69";
            this.panel69.Size = new System.Drawing.Size(1168, 154);
            this.panel69.TabIndex = 2;
            // 
            // labelDiagnosis
            // 
            this.labelDiagnosis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDiagnosis.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelDiagnosis.Location = new System.Drawing.Point(0, 36);
            this.labelDiagnosis.Name = "labelDiagnosis";
            this.labelDiagnosis.Size = new System.Drawing.Size(1166, 116);
            this.labelDiagnosis.TabIndex = 4;
            this.labelDiagnosis.Text = "^I50.41   Acute combined systolic (congestive) and diastolic (congestive) heart f" +
    "ailure";
            this.labelDiagnosis.Click += new System.EventHandler(this.labelDiagnosis_Click);
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1166, 36);
            this.label1.TabIndex = 2;
            this.label1.Text = "Indication:";
            // 
            // panel158
            // 
            this.panel158.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel158.Location = new System.Drawing.Point(0, 0);
            this.panel158.Name = "panel158";
            this.panel158.Size = new System.Drawing.Size(1654, 10);
            this.panel158.TabIndex = 2;
            // 
            // panelEventsStat
            // 
            this.panelEventsStat.Controls.Add(this.panel753);
            this.panelEventsStat.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelEventsStat.Location = new System.Drawing.Point(0, 833);
            this.panelEventsStat.Name = "panelEventsStat";
            this.panelEventsStat.Size = new System.Drawing.Size(1654, 632);
            this.panelEventsStat.TabIndex = 13;
            // 
            // panel753
            // 
            this.panel753.Controls.Add(this.panel756ST);
            this.panel753.Controls.Add(this.panel755);
            this.panel753.Controls.Add(this.panel754);
            this.panel753.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel753.Location = new System.Drawing.Point(0, 0);
            this.panel753.Name = "panel753";
            this.panel753.Size = new System.Drawing.Size(1654, 632);
            this.panel753.TabIndex = 4;
            // 
            // panel756ST
            // 
            this.panel756ST.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel756ST.Controls.Add(this.panel771);
            this.panel756ST.Controls.Add(this.panel74);
            this.panel756ST.Controls.Add(this.panel770);
            this.panel756ST.Controls.Add(this.panel769);
            this.panel756ST.Controls.Add(this.panel146);
            this.panel756ST.Controls.Add(this.panel85);
            this.panel756ST.Controls.Add(this.panel80);
            this.panel756ST.Controls.Add(this.panel79);
            this.panel756ST.Controls.Add(this.panel38);
            this.panel756ST.Controls.Add(this.panel768);
            this.panel756ST.Controls.Add(this.panel767);
            this.panel756ST.Controls.Add(this.panel766);
            this.panel756ST.Controls.Add(this.panel765);
            this.panel756ST.Controls.Add(this.panel764);
            this.panel756ST.Controls.Add(this.panel20);
            this.panel756ST.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel756ST.Location = new System.Drawing.Point(978, 0);
            this.panel756ST.Name = "panel756ST";
            this.panel756ST.Size = new System.Drawing.Size(676, 632);
            this.panel756ST.TabIndex = 2;
            // 
            // panel771
            // 
            this.panel771.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel771.Controls.Add(this.label25);
            this.panel771.Controls.Add(this.labelAutoManual);
            this.panel771.Controls.Add(this.label127);
            this.panel771.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel771.Location = new System.Drawing.Point(0, 548);
            this.panel771.Name = "panel771";
            this.panel771.Size = new System.Drawing.Size(674, 37);
            this.panel771.TabIndex = 31;
            // 
            // label25
            // 
            this.label25.Dock = System.Windows.Forms.DockStyle.Left;
            this.label25.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(383, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(200, 35);
            this.label25.TabIndex = 8;
            // 
            // labelAutoManual
            // 
            this.labelAutoManual.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelAutoManual.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelAutoManual.Location = new System.Drawing.Point(206, 0);
            this.labelAutoManual.Name = "labelAutoManual";
            this.labelAutoManual.Size = new System.Drawing.Size(177, 35);
            this.labelAutoManual.TabIndex = 4;
            this.labelAutoManual.Text = "^120/22";
            this.labelAutoManual.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label127
            // 
            this.label127.Dock = System.Windows.Forms.DockStyle.Left;
            this.label127.Font = new System.Drawing.Font("Verdana", 20F);
            this.label127.Location = new System.Drawing.Point(0, 0);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(206, 35);
            this.label127.TabIndex = 3;
            this.label127.Text = "Auto / Manual";
            // 
            // panel74
            // 
            this.panel74.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel74.Controls.Add(this.labelUser);
            this.panel74.Controls.Add(this.labelTechText);
            this.panel74.Controls.Add(this.panel45);
            this.panel74.Controls.Add(this.labelDayNight);
            this.panel74.Controls.Add(this.label15);
            this.panel74.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel74.Location = new System.Drawing.Point(0, 585);
            this.panel74.Name = "panel74";
            this.panel74.Size = new System.Drawing.Size(674, 45);
            this.panel74.TabIndex = 32;
            // 
            // labelUser
            // 
            this.labelUser.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelUser.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelUser.Location = new System.Drawing.Point(540, 0);
            this.labelUser.Name = "labelUser";
            this.labelUser.Size = new System.Drawing.Size(153, 43);
            this.labelUser.TabIndex = 7;
            this.labelUser.Text = "xx";
            this.labelUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelTechText
            // 
            this.labelTechText.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelTechText.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelTechText.Location = new System.Drawing.Point(420, 0);
            this.labelTechText.Name = "labelTechText";
            this.labelTechText.Size = new System.Drawing.Size(120, 43);
            this.labelTechText.TabIndex = 6;
            this.labelTechText.Text = "Tech:";
            this.labelTechText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel45
            // 
            this.panel45.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel45.Location = new System.Drawing.Point(383, 0);
            this.panel45.Name = "panel45";
            this.panel45.Size = new System.Drawing.Size(37, 43);
            this.panel45.TabIndex = 5;
            // 
            // labelDayNight
            // 
            this.labelDayNight.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelDayNight.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelDayNight.Location = new System.Drawing.Point(206, 0);
            this.labelDayNight.Name = "labelDayNight";
            this.labelDayNight.Size = new System.Drawing.Size(177, 43);
            this.labelDayNight.TabIndex = 4;
            this.labelDayNight.Text = "^120/100";
            this.labelDayNight.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label15
            // 
            this.label15.Dock = System.Windows.Forms.DockStyle.Left;
            this.label15.Font = new System.Drawing.Font("Verdana", 20F);
            this.label15.Location = new System.Drawing.Point(0, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(206, 43);
            this.label15.TabIndex = 3;
            this.label15.Text = "Day / Night";
            // 
            // panel770
            // 
            this.panel770.Controls.Add(this.panel6);
            this.panel770.Controls.Add(this.panel39);
            this.panel770.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel770.Location = new System.Drawing.Point(0, 474);
            this.panel770.Name = "panel770";
            this.panel770.Size = new System.Drawing.Size(674, 72);
            this.panel770.TabIndex = 30;
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.labelAbnMax);
            this.panel6.Controls.Add(this.labelAbnMin);
            this.panel6.Controls.Add(this.labelAbnN);
            this.panel6.Controls.Add(this.label30);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 40);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(674, 40);
            this.panel6.TabIndex = 26;
            // 
            // labelAbnMax
            // 
            this.labelAbnMax.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelAbnMax.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelAbnMax.Location = new System.Drawing.Point(410, 0);
            this.labelAbnMax.Name = "labelAbnMax";
            this.labelAbnMax.Size = new System.Drawing.Size(179, 38);
            this.labelAbnMax.TabIndex = 10;
            this.labelAbnMax.Text = "--Mx-";
            this.labelAbnMax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelAbnMin
            // 
            this.labelAbnMin.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelAbnMin.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelAbnMin.Location = new System.Drawing.Point(268, 0);
            this.labelAbnMin.Name = "labelAbnMin";
            this.labelAbnMin.Size = new System.Drawing.Size(142, 38);
            this.labelAbnMin.TabIndex = 11;
            this.labelAbnMin.Text = "-Min--";
            this.labelAbnMin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelAbnN
            // 
            this.labelAbnN.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelAbnN.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelAbnN.Location = new System.Drawing.Point(181, 0);
            this.labelAbnN.Name = "labelAbnN";
            this.labelAbnN.Size = new System.Drawing.Size(87, 38);
            this.labelAbnN.TabIndex = 5;
            this.labelAbnN.Text = "^99";
            this.labelAbnN.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label30
            // 
            this.label30.Dock = System.Windows.Forms.DockStyle.Left;
            this.label30.Font = new System.Drawing.Font("Verdana", 20F);
            this.label30.Location = new System.Drawing.Point(0, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(181, 38);
            this.label30.TabIndex = 4;
            this.label30.Text = "AbNormal";
            // 
            // panel39
            // 
            this.panel39.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel39.Controls.Add(this.labelNormMax);
            this.panel39.Controls.Add(this.labelNormMin);
            this.panel39.Controls.Add(this.labelNormN);
            this.panel39.Controls.Add(this.label63);
            this.panel39.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel39.Location = new System.Drawing.Point(0, 0);
            this.panel39.Name = "panel39";
            this.panel39.Size = new System.Drawing.Size(674, 40);
            this.panel39.TabIndex = 25;
            // 
            // labelNormMax
            // 
            this.labelNormMax.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelNormMax.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelNormMax.Location = new System.Drawing.Point(410, 0);
            this.labelNormMax.Name = "labelNormMax";
            this.labelNormMax.Size = new System.Drawing.Size(179, 38);
            this.labelNormMax.TabIndex = 10;
            this.labelNormMax.Text = "--Mx-";
            this.labelNormMax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelNormMin
            // 
            this.labelNormMin.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelNormMin.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelNormMin.Location = new System.Drawing.Point(268, 0);
            this.labelNormMin.Name = "labelNormMin";
            this.labelNormMin.Size = new System.Drawing.Size(142, 38);
            this.labelNormMin.TabIndex = 11;
            this.labelNormMin.Text = "-Min--";
            this.labelNormMin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelNormN
            // 
            this.labelNormN.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelNormN.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelNormN.Location = new System.Drawing.Point(181, 0);
            this.labelNormN.Name = "labelNormN";
            this.labelNormN.Size = new System.Drawing.Size(87, 38);
            this.labelNormN.TabIndex = 5;
            this.labelNormN.Text = "^99";
            this.labelNormN.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label63
            // 
            this.label63.Dock = System.Windows.Forms.DockStyle.Left;
            this.label63.Font = new System.Drawing.Font("Verdana", 20F);
            this.label63.Location = new System.Drawing.Point(0, 0);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(181, 38);
            this.label63.TabIndex = 4;
            this.label63.Text = "Normal";
            // 
            // panel769
            // 
            this.panel769.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel769.Controls.Add(this.labelOtherMax);
            this.panel769.Controls.Add(this.labelOtherMin);
            this.panel769.Controls.Add(this.labelOtherN);
            this.panel769.Controls.Add(this.label18);
            this.panel769.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel769.Location = new System.Drawing.Point(0, 434);
            this.panel769.Name = "panel769";
            this.panel769.Size = new System.Drawing.Size(674, 40);
            this.panel769.TabIndex = 24;
            // 
            // labelOtherMax
            // 
            this.labelOtherMax.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelOtherMax.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelOtherMax.Location = new System.Drawing.Point(410, 0);
            this.labelOtherMax.Name = "labelOtherMax";
            this.labelOtherMax.Size = new System.Drawing.Size(179, 38);
            this.labelOtherMax.TabIndex = 10;
            this.labelOtherMax.Text = "--Mx-";
            this.labelOtherMax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelOtherMin
            // 
            this.labelOtherMin.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelOtherMin.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelOtherMin.Location = new System.Drawing.Point(268, 0);
            this.labelOtherMin.Name = "labelOtherMin";
            this.labelOtherMin.Size = new System.Drawing.Size(142, 38);
            this.labelOtherMin.TabIndex = 11;
            this.labelOtherMin.Text = "-Min--";
            this.labelOtherMin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelOtherN
            // 
            this.labelOtherN.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelOtherN.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelOtherN.Location = new System.Drawing.Point(181, 0);
            this.labelOtherN.Name = "labelOtherN";
            this.labelOtherN.Size = new System.Drawing.Size(87, 38);
            this.labelOtherN.TabIndex = 5;
            this.labelOtherN.Text = "^99";
            this.labelOtherN.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.Dock = System.Windows.Forms.DockStyle.Left;
            this.label18.Font = new System.Drawing.Font("Verdana", 20F);
            this.label18.Location = new System.Drawing.Point(0, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(181, 38);
            this.label18.TabIndex = 4;
            this.label18.Text = "Other";
            // 
            // panel146
            // 
            this.panel146.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel146.Controls.Add(this.labelVtMax);
            this.panel146.Controls.Add(this.labelVtMin);
            this.panel146.Controls.Add(this.labelVtN);
            this.panel146.Controls.Add(this.label55);
            this.panel146.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel146.Location = new System.Drawing.Point(0, 394);
            this.panel146.Name = "panel146";
            this.panel146.Size = new System.Drawing.Size(674, 40);
            this.panel146.TabIndex = 29;
            // 
            // labelVtMax
            // 
            this.labelVtMax.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelVtMax.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelVtMax.Location = new System.Drawing.Point(409, 0);
            this.labelVtMax.Name = "labelVtMax";
            this.labelVtMax.Size = new System.Drawing.Size(179, 38);
            this.labelVtMax.TabIndex = 9;
            this.labelVtMax.Text = "^99";
            this.labelVtMax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelVtMin
            // 
            this.labelVtMin.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelVtMin.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelVtMin.Location = new System.Drawing.Point(268, 0);
            this.labelVtMin.Name = "labelVtMin";
            this.labelVtMin.Size = new System.Drawing.Size(141, 38);
            this.labelVtMin.TabIndex = 10;
            this.labelVtMin.Text = "^99";
            this.labelVtMin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelVtN
            // 
            this.labelVtN.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelVtN.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelVtN.Location = new System.Drawing.Point(181, 0);
            this.labelVtN.Name = "labelVtN";
            this.labelVtN.Size = new System.Drawing.Size(87, 38);
            this.labelVtN.TabIndex = 8;
            this.labelVtN.Text = "^99";
            this.labelVtN.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label55
            // 
            this.label55.Dock = System.Windows.Forms.DockStyle.Left;
            this.label55.Font = new System.Drawing.Font("Verdana", 20F);
            this.label55.Location = new System.Drawing.Point(0, 0);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(181, 38);
            this.label55.TabIndex = 7;
            this.label55.Text = "VTach";
            // 
            // panel85
            // 
            this.panel85.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel85.Controls.Add(this.labelPaceMax);
            this.panel85.Controls.Add(this.labelPaceMin);
            this.panel85.Controls.Add(this.labelPaceN);
            this.panel85.Controls.Add(this.label47);
            this.panel85.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel85.Location = new System.Drawing.Point(0, 354);
            this.panel85.Name = "panel85";
            this.panel85.Size = new System.Drawing.Size(674, 40);
            this.panel85.TabIndex = 28;
            // 
            // labelPaceMax
            // 
            this.labelPaceMax.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPaceMax.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelPaceMax.Location = new System.Drawing.Point(409, 0);
            this.labelPaceMax.Name = "labelPaceMax";
            this.labelPaceMax.Size = new System.Drawing.Size(179, 38);
            this.labelPaceMax.TabIndex = 9;
            this.labelPaceMax.Text = "^99";
            this.labelPaceMax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelPaceMin
            // 
            this.labelPaceMin.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPaceMin.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelPaceMin.Location = new System.Drawing.Point(268, 0);
            this.labelPaceMin.Name = "labelPaceMin";
            this.labelPaceMin.Size = new System.Drawing.Size(141, 38);
            this.labelPaceMin.TabIndex = 10;
            this.labelPaceMin.Text = "^99";
            this.labelPaceMin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelPaceN
            // 
            this.labelPaceN.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPaceN.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelPaceN.Location = new System.Drawing.Point(181, 0);
            this.labelPaceN.Name = "labelPaceN";
            this.labelPaceN.Size = new System.Drawing.Size(87, 38);
            this.labelPaceN.TabIndex = 8;
            this.labelPaceN.Text = "^99";
            this.labelPaceN.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label47
            // 
            this.label47.Dock = System.Windows.Forms.DockStyle.Left;
            this.label47.Font = new System.Drawing.Font("Verdana", 20F);
            this.label47.Location = new System.Drawing.Point(0, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(181, 38);
            this.label47.TabIndex = 7;
            this.label47.Text = "Pace";
            // 
            // panel80
            // 
            this.panel80.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel80.Controls.Add(this.labelPvcMax);
            this.panel80.Controls.Add(this.labelPvcMin);
            this.panel80.Controls.Add(this.labelPvcN);
            this.panel80.Controls.Add(this.label43);
            this.panel80.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel80.Location = new System.Drawing.Point(0, 314);
            this.panel80.Name = "panel80";
            this.panel80.Size = new System.Drawing.Size(674, 40);
            this.panel80.TabIndex = 27;
            // 
            // labelPvcMax
            // 
            this.labelPvcMax.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPvcMax.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelPvcMax.Location = new System.Drawing.Point(409, 0);
            this.labelPvcMax.Name = "labelPvcMax";
            this.labelPvcMax.Size = new System.Drawing.Size(179, 38);
            this.labelPvcMax.TabIndex = 9;
            this.labelPvcMax.Text = "^99";
            this.labelPvcMax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelPvcMin
            // 
            this.labelPvcMin.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPvcMin.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelPvcMin.Location = new System.Drawing.Point(268, 0);
            this.labelPvcMin.Name = "labelPvcMin";
            this.labelPvcMin.Size = new System.Drawing.Size(141, 38);
            this.labelPvcMin.TabIndex = 10;
            this.labelPvcMin.Text = "^99";
            this.labelPvcMin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelPvcN
            // 
            this.labelPvcN.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPvcN.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelPvcN.Location = new System.Drawing.Point(181, 0);
            this.labelPvcN.Name = "labelPvcN";
            this.labelPvcN.Size = new System.Drawing.Size(87, 38);
            this.labelPvcN.TabIndex = 8;
            this.labelPvcN.Text = "^99";
            this.labelPvcN.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label43
            // 
            this.label43.Dock = System.Windows.Forms.DockStyle.Left;
            this.label43.Font = new System.Drawing.Font("Verdana", 20F);
            this.label43.Location = new System.Drawing.Point(0, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(181, 38);
            this.label43.TabIndex = 7;
            this.label43.Text = "PVC-VEB";
            // 
            // panel79
            // 
            this.panel79.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel79.Controls.Add(this.labelPacMax);
            this.panel79.Controls.Add(this.labelPacMin);
            this.panel79.Controls.Add(this.labelPacN);
            this.panel79.Controls.Add(this.label36);
            this.panel79.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel79.Location = new System.Drawing.Point(0, 274);
            this.panel79.Name = "panel79";
            this.panel79.Size = new System.Drawing.Size(674, 40);
            this.panel79.TabIndex = 26;
            // 
            // labelPacMax
            // 
            this.labelPacMax.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPacMax.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelPacMax.Location = new System.Drawing.Point(409, 0);
            this.labelPacMax.Name = "labelPacMax";
            this.labelPacMax.Size = new System.Drawing.Size(179, 38);
            this.labelPacMax.TabIndex = 9;
            this.labelPacMax.Text = "^99";
            this.labelPacMax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelPacMin
            // 
            this.labelPacMin.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPacMin.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelPacMin.Location = new System.Drawing.Point(268, 0);
            this.labelPacMin.Name = "labelPacMin";
            this.labelPacMin.Size = new System.Drawing.Size(141, 38);
            this.labelPacMin.TabIndex = 10;
            this.labelPacMin.Text = "^99";
            this.labelPacMin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelPacN
            // 
            this.labelPacN.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPacN.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelPacN.Location = new System.Drawing.Point(181, 0);
            this.labelPacN.Name = "labelPacN";
            this.labelPacN.Size = new System.Drawing.Size(87, 38);
            this.labelPacN.TabIndex = 8;
            this.labelPacN.Text = "^99";
            this.labelPacN.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label36
            // 
            this.label36.Dock = System.Windows.Forms.DockStyle.Left;
            this.label36.Font = new System.Drawing.Font("Verdana", 20F);
            this.label36.Location = new System.Drawing.Point(0, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(181, 38);
            this.label36.TabIndex = 7;
            this.label36.Text = "PAC-SVEB";
            // 
            // panel38
            // 
            this.panel38.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel38.Controls.Add(this.labelAFlutMax);
            this.panel38.Controls.Add(this.labelAFlutMin);
            this.panel38.Controls.Add(this.labelAFlutN);
            this.panel38.Controls.Add(this.label26);
            this.panel38.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel38.Location = new System.Drawing.Point(0, 234);
            this.panel38.Name = "panel38";
            this.panel38.Size = new System.Drawing.Size(674, 40);
            this.panel38.TabIndex = 25;
            // 
            // labelAFlutMax
            // 
            this.labelAFlutMax.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelAFlutMax.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelAFlutMax.Location = new System.Drawing.Point(409, 0);
            this.labelAFlutMax.Name = "labelAFlutMax";
            this.labelAFlutMax.Size = new System.Drawing.Size(179, 38);
            this.labelAFlutMax.TabIndex = 9;
            this.labelAFlutMax.Text = "^99";
            this.labelAFlutMax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelAFlutMin
            // 
            this.labelAFlutMin.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelAFlutMin.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelAFlutMin.Location = new System.Drawing.Point(268, 0);
            this.labelAFlutMin.Name = "labelAFlutMin";
            this.labelAFlutMin.Size = new System.Drawing.Size(141, 38);
            this.labelAFlutMin.TabIndex = 10;
            this.labelAFlutMin.Text = "^99";
            this.labelAFlutMin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelAFlutN
            // 
            this.labelAFlutN.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelAFlutN.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelAFlutN.Location = new System.Drawing.Point(181, 0);
            this.labelAFlutN.Name = "labelAFlutN";
            this.labelAFlutN.Size = new System.Drawing.Size(87, 38);
            this.labelAFlutN.TabIndex = 8;
            this.labelAFlutN.Text = "^99";
            this.labelAFlutN.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label26
            // 
            this.label26.Dock = System.Windows.Forms.DockStyle.Left;
            this.label26.Font = new System.Drawing.Font("Verdana", 20F);
            this.label26.Location = new System.Drawing.Point(0, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(181, 38);
            this.label26.TabIndex = 7;
            this.label26.Text = "AFlut";
            // 
            // panel768
            // 
            this.panel768.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel768.Controls.Add(this.labelAfibMax);
            this.panel768.Controls.Add(this.labelAfibMin);
            this.panel768.Controls.Add(this.labelAfibN);
            this.panel768.Controls.Add(this.label123);
            this.panel768.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel768.Location = new System.Drawing.Point(0, 194);
            this.panel768.Name = "panel768";
            this.panel768.Size = new System.Drawing.Size(674, 40);
            this.panel768.TabIndex = 23;
            // 
            // labelAfibMax
            // 
            this.labelAfibMax.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelAfibMax.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelAfibMax.Location = new System.Drawing.Point(409, 0);
            this.labelAfibMax.Name = "labelAfibMax";
            this.labelAfibMax.Size = new System.Drawing.Size(179, 38);
            this.labelAfibMax.TabIndex = 5;
            this.labelAfibMax.Text = "^99";
            this.labelAfibMax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelAfibMin
            // 
            this.labelAfibMin.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelAfibMin.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelAfibMin.Location = new System.Drawing.Point(268, 0);
            this.labelAfibMin.Name = "labelAfibMin";
            this.labelAfibMin.Size = new System.Drawing.Size(141, 38);
            this.labelAfibMin.TabIndex = 6;
            this.labelAfibMin.Text = "--";
            this.labelAfibMin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelAfibN
            // 
            this.labelAfibN.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelAfibN.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelAfibN.Location = new System.Drawing.Point(181, 0);
            this.labelAfibN.Name = "labelAfibN";
            this.labelAfibN.Size = new System.Drawing.Size(87, 38);
            this.labelAfibN.TabIndex = 4;
            this.labelAfibN.Text = "^99";
            this.labelAfibN.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label123
            // 
            this.label123.Dock = System.Windows.Forms.DockStyle.Left;
            this.label123.Font = new System.Drawing.Font("Verdana", 20F);
            this.label123.Location = new System.Drawing.Point(0, 0);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(181, 38);
            this.label123.TabIndex = 3;
            this.label123.Text = "AFib";
            // 
            // panel767
            // 
            this.panel767.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel767.Controls.Add(this.labelPauseMax);
            this.panel767.Controls.Add(this.labelPauseMin);
            this.panel767.Controls.Add(this.labelPauseN);
            this.panel767.Controls.Add(this.label122);
            this.panel767.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel767.Location = new System.Drawing.Point(0, 154);
            this.panel767.Name = "panel767";
            this.panel767.Size = new System.Drawing.Size(674, 40);
            this.panel767.TabIndex = 22;
            // 
            // labelPauseMax
            // 
            this.labelPauseMax.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPauseMax.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelPauseMax.Location = new System.Drawing.Point(409, 0);
            this.labelPauseMax.Name = "labelPauseMax";
            this.labelPauseMax.Size = new System.Drawing.Size(179, 38);
            this.labelPauseMax.TabIndex = 5;
            this.labelPauseMax.Text = "^99";
            this.labelPauseMax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelPauseMin
            // 
            this.labelPauseMin.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPauseMin.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelPauseMin.Location = new System.Drawing.Point(268, 0);
            this.labelPauseMin.Name = "labelPauseMin";
            this.labelPauseMin.Size = new System.Drawing.Size(141, 38);
            this.labelPauseMin.TabIndex = 6;
            this.labelPauseMin.Text = "^99";
            this.labelPauseMin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelPauseN
            // 
            this.labelPauseN.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPauseN.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelPauseN.Location = new System.Drawing.Point(181, 0);
            this.labelPauseN.Name = "labelPauseN";
            this.labelPauseN.Size = new System.Drawing.Size(87, 38);
            this.labelPauseN.TabIndex = 4;
            this.labelPauseN.Text = "^99";
            this.labelPauseN.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label122
            // 
            this.label122.Dock = System.Windows.Forms.DockStyle.Left;
            this.label122.Font = new System.Drawing.Font("Verdana", 20F);
            this.label122.Location = new System.Drawing.Point(0, 0);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(181, 38);
            this.label122.TabIndex = 3;
            this.label122.Text = "Pause";
            // 
            // panel766
            // 
            this.panel766.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel766.Controls.Add(this.labelBradyMax);
            this.panel766.Controls.Add(this.labelBradyMin);
            this.panel766.Controls.Add(this.labelBradyN);
            this.panel766.Controls.Add(this.label120);
            this.panel766.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel766.Location = new System.Drawing.Point(0, 114);
            this.panel766.Name = "panel766";
            this.panel766.Size = new System.Drawing.Size(674, 40);
            this.panel766.TabIndex = 21;
            // 
            // labelBradyMax
            // 
            this.labelBradyMax.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelBradyMax.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelBradyMax.Location = new System.Drawing.Point(409, 0);
            this.labelBradyMax.Name = "labelBradyMax";
            this.labelBradyMax.Size = new System.Drawing.Size(179, 38);
            this.labelBradyMax.TabIndex = 5;
            this.labelBradyMax.Text = "^99";
            this.labelBradyMax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelBradyMin
            // 
            this.labelBradyMin.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelBradyMin.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelBradyMin.Location = new System.Drawing.Point(268, 0);
            this.labelBradyMin.Name = "labelBradyMin";
            this.labelBradyMin.Size = new System.Drawing.Size(141, 38);
            this.labelBradyMin.TabIndex = 6;
            this.labelBradyMin.Text = "^99";
            this.labelBradyMin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelBradyN
            // 
            this.labelBradyN.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelBradyN.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelBradyN.Location = new System.Drawing.Point(181, 0);
            this.labelBradyN.Name = "labelBradyN";
            this.labelBradyN.Size = new System.Drawing.Size(87, 38);
            this.labelBradyN.TabIndex = 4;
            this.labelBradyN.Text = "^99";
            this.labelBradyN.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label120
            // 
            this.label120.Dock = System.Windows.Forms.DockStyle.Left;
            this.label120.Font = new System.Drawing.Font("Verdana", 20F);
            this.label120.Location = new System.Drawing.Point(0, 0);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(181, 38);
            this.label120.TabIndex = 3;
            this.label120.Text = "Bradycardia";
            // 
            // panel765
            // 
            this.panel765.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel765.Controls.Add(this.labelTachyMax);
            this.panel765.Controls.Add(this.labelTachyMin);
            this.panel765.Controls.Add(this.labelTachyN);
            this.panel765.Controls.Add(this.label118);
            this.panel765.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel765.Location = new System.Drawing.Point(0, 74);
            this.panel765.Name = "panel765";
            this.panel765.Size = new System.Drawing.Size(674, 40);
            this.panel765.TabIndex = 20;
            // 
            // labelTachyMax
            // 
            this.labelTachyMax.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelTachyMax.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelTachyMax.Location = new System.Drawing.Point(409, 0);
            this.labelTachyMax.Name = "labelTachyMax";
            this.labelTachyMax.Size = new System.Drawing.Size(179, 38);
            this.labelTachyMax.TabIndex = 5;
            this.labelTachyMax.Text = "^99";
            this.labelTachyMax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTachyMin
            // 
            this.labelTachyMin.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelTachyMin.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelTachyMin.Location = new System.Drawing.Point(268, 0);
            this.labelTachyMin.Name = "labelTachyMin";
            this.labelTachyMin.Size = new System.Drawing.Size(141, 38);
            this.labelTachyMin.TabIndex = 6;
            this.labelTachyMin.Text = "^99";
            this.labelTachyMin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTachyN
            // 
            this.labelTachyN.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelTachyN.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelTachyN.Location = new System.Drawing.Point(181, 0);
            this.labelTachyN.Name = "labelTachyN";
            this.labelTachyN.Size = new System.Drawing.Size(87, 38);
            this.labelTachyN.TabIndex = 4;
            this.labelTachyN.Text = "^99";
            this.labelTachyN.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label118
            // 
            this.label118.Dock = System.Windows.Forms.DockStyle.Left;
            this.label118.Font = new System.Drawing.Font("Verdana", 20F);
            this.label118.Location = new System.Drawing.Point(0, 0);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(181, 38);
            this.label118.TabIndex = 3;
            this.label118.Text = "Tachycardia";
            // 
            // panel764
            // 
            this.panel764.BackColor = System.Drawing.Color.Transparent;
            this.panel764.Controls.Add(this.label59);
            this.panel764.Controls.Add(this.label90);
            this.panel764.Controls.Add(this.label56);
            this.panel764.Controls.Add(this.label105);
            this.panel764.Controls.Add(this.label13);
            this.panel764.Controls.Add(this.panel86);
            this.panel764.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel764.Location = new System.Drawing.Point(0, 39);
            this.panel764.Margin = new System.Windows.Forms.Padding(0);
            this.panel764.Name = "panel764";
            this.panel764.Size = new System.Drawing.Size(674, 35);
            this.panel764.TabIndex = 19;
            // 
            // label59
            // 
            this.label59.Dock = System.Windows.Forms.DockStyle.Left;
            this.label59.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold);
            this.label59.Location = new System.Drawing.Point(601, 0);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(56, 35);
            this.label59.TabIndex = 13;
            this.label59.Text = "BPM";
            this.label59.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label90
            // 
            this.label90.Dock = System.Windows.Forms.DockStyle.Left;
            this.label90.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.label90.Location = new System.Drawing.Point(464, 0);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(137, 35);
            this.label90.TabIndex = 9;
            this.label90.Text = "Highest";
            this.label90.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label56
            // 
            this.label56.Dock = System.Windows.Forms.DockStyle.Left;
            this.label56.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold);
            this.label56.Location = new System.Drawing.Point(410, 0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(54, 35);
            this.label56.TabIndex = 12;
            this.label56.Text = "BPM";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label105
            // 
            this.label105.Dock = System.Windows.Forms.DockStyle.Left;
            this.label105.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.label105.Location = new System.Drawing.Point(284, 0);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(126, 35);
            this.label105.TabIndex = 10;
            this.label105.Text = "Lowest";
            this.label105.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label13
            // 
            this.label13.Dock = System.Windows.Forms.DockStyle.Left;
            this.label13.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(167, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(117, 35);
            this.label13.TabIndex = 11;
            this.label13.Text = "Count";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel86
            // 
            this.panel86.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel86.Location = new System.Drawing.Point(0, 0);
            this.panel86.Name = "panel86";
            this.panel86.Size = new System.Drawing.Size(167, 35);
            this.panel86.TabIndex = 5;
            // 
            // panel20
            // 
            this.panel20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel20.Controls.Add(this.label89);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel20.Location = new System.Drawing.Point(0, 0);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(674, 39);
            this.panel20.TabIndex = 17;
            // 
            // label89
            // 
            this.label89.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label89.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.label89.Location = new System.Drawing.Point(0, 0);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(672, 37);
            this.label89.TabIndex = 2;
            this.label89.Text = "Heart  Rate per Classification";
            // 
            // panel755
            // 
            this.panel755.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel755.Location = new System.Drawing.Point(950, 0);
            this.panel755.Name = "panel755";
            this.panel755.Size = new System.Drawing.Size(28, 632);
            this.panel755.TabIndex = 1;
            // 
            // panel754
            // 
            this.panel754.Controls.Add(this.panel759);
            this.panel754.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel754.Location = new System.Drawing.Point(0, 0);
            this.panel754.Name = "panel754";
            this.panel754.Size = new System.Drawing.Size(950, 632);
            this.panel754.TabIndex = 0;
            // 
            // panel759
            // 
            this.panel759.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel759.Controls.Add(this.chartSummaryBar);
            this.panel759.Controls.Add(this.panel758);
            this.panel759.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel759.Location = new System.Drawing.Point(0, 0);
            this.panel759.Name = "panel759";
            this.panel759.Size = new System.Drawing.Size(950, 632);
            this.panel759.TabIndex = 2;
            // 
            // chartSummaryBar
            // 
            chartArea1.AxisX.LabelStyle.Enabled = false;
            chartArea1.AxisX.LineColor = System.Drawing.Color.DarkGray;
            chartArea1.AxisX.MajorGrid.Enabled = false;
            chartArea1.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea1.AxisX2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea1.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea1.AxisY.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea1.AxisY.IsLabelAutoFit = false;
            chartArea1.AxisY.LabelStyle.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea1.AxisY.LabelStyle.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea1.AxisY.LineColor = System.Drawing.Color.DarkGray;
            chartArea1.AxisY.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea1.AxisY.TitleFont = new System.Drawing.Font("Verdana", 20F);
            chartArea1.AxisY2.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea1.BorderWidth = 0;
            chartArea1.Name = "ChartArea1";
            this.chartSummaryBar.ChartAreas.Add(chartArea1);
            this.chartSummaryBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartSummaryBar.Location = new System.Drawing.Point(0, 39);
            this.chartSummaryBar.Name = "chartSummaryBar";
            series1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            series1.BorderWidth = 2;
            series1.ChartArea = "ChartArea1";
            series1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series1.IsVisibleInLegend = false;
            series1.Name = "Series1";
            dataPoint1.BackHatchStyle = System.Windows.Forms.DataVisualization.Charting.ChartHatchStyle.None;
            dataPoint1.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            dataPoint1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            dataPoint1.Font = new System.Drawing.Font("Verdana", 16F);
            dataPoint1.IsValueShownAsLabel = false;
            dataPoint1.IsVisibleInLegend = false;
            dataPoint1.Label = "Tachy";
            dataPoint1.LabelForeColor = System.Drawing.Color.Black;
            dataPoint2.Color = System.Drawing.Color.Green;
            dataPoint2.Font = new System.Drawing.Font("Verdana", 16F);
            dataPoint2.Label = "Brady";
            dataPoint3.Color = System.Drawing.Color.Gray;
            dataPoint3.Font = new System.Drawing.Font("Verdana", 16F);
            dataPoint3.Label = "Pause";
            dataPoint4.Color = System.Drawing.Color.Blue;
            dataPoint4.Font = new System.Drawing.Font("Verdana", 16F);
            dataPoint4.Label = "AFib";
            dataPoint5.Font = new System.Drawing.Font("Verdana", 16F);
            dataPoint5.Label = "AFlut";
            dataPoint6.Color = System.Drawing.Color.DimGray;
            dataPoint6.Font = new System.Drawing.Font("Verdana", 16F);
            dataPoint6.Label = "PAC\\nSVEB";
            dataPoint7.Color = System.Drawing.Color.Gray;
            dataPoint7.Font = new System.Drawing.Font("Verdana", 16F);
            dataPoint7.Label = "PVC\\nVEB";
            dataPoint8.Color = System.Drawing.Color.Silver;
            dataPoint8.Font = new System.Drawing.Font("Verdana", 16F);
            dataPoint8.Label = "Pace";
            dataPoint9.Color = System.Drawing.Color.Gainsboro;
            dataPoint9.Font = new System.Drawing.Font("Verdana", 16F);
            dataPoint9.Label = "VTach";
            dataPoint10.Color = System.Drawing.Color.RosyBrown;
            dataPoint10.Font = new System.Drawing.Font("Verdana", 16F);
            dataPoint10.Label = "Other";
            dataPoint11.Font = new System.Drawing.Font("Verdana", 16F);
            dataPoint11.Label = "Norm";
            dataPoint12.Color = System.Drawing.Color.Aqua;
            dataPoint12.Font = new System.Drawing.Font("Verdana", 16F);
            dataPoint12.Label = "AbN";
            series1.Points.Add(dataPoint1);
            series1.Points.Add(dataPoint2);
            series1.Points.Add(dataPoint3);
            series1.Points.Add(dataPoint4);
            series1.Points.Add(dataPoint5);
            series1.Points.Add(dataPoint6);
            series1.Points.Add(dataPoint7);
            series1.Points.Add(dataPoint8);
            series1.Points.Add(dataPoint9);
            series1.Points.Add(dataPoint10);
            series1.Points.Add(dataPoint11);
            series1.Points.Add(dataPoint12);
            series1.SmartLabelStyle.MovingDirection = System.Windows.Forms.DataVisualization.Charting.LabelAlignmentStyles.Top;
            this.chartSummaryBar.Series.Add(series1);
            this.chartSummaryBar.Size = new System.Drawing.Size(948, 591);
            this.chartSummaryBar.TabIndex = 4;
            this.chartSummaryBar.Text = "EventPie";
            // 
            // panel758
            // 
            this.panel758.Controls.Add(this.label79);
            this.panel758.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel758.Location = new System.Drawing.Point(0, 0);
            this.panel758.Name = "panel758";
            this.panel758.Size = new System.Drawing.Size(948, 39);
            this.panel758.TabIndex = 3;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Dock = System.Windows.Forms.DockStyle.Left;
            this.label79.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.label79.Location = new System.Drawing.Point(0, 0);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(385, 32);
            this.label79.TabIndex = 0;
            this.label79.Text = "Events per Classification";
            // 
            // panelLineUnderSampl1
            // 
            this.panelLineUnderSampl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLineUnderSampl1.Location = new System.Drawing.Point(0, 1465);
            this.panelLineUnderSampl1.Name = "panelLineUnderSampl1";
            this.panelLineUnderSampl1.Size = new System.Drawing.Size(1654, 2);
            this.panelLineUnderSampl1.TabIndex = 20;
            // 
            // panelSignatures
            // 
            this.panelSignatures.Controls.Add(this.panel110);
            this.panelSignatures.Controls.Add(this.panel109);
            this.panelSignatures.Controls.Add(this.panel108);
            this.panelSignatures.Controls.Add(this.panel107);
            this.panelSignatures.Controls.Add(this.panel106);
            this.panelSignatures.Controls.Add(this.panel105);
            this.panelSignatures.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelSignatures.Location = new System.Drawing.Point(0, 2202);
            this.panelSignatures.Name = "panelSignatures";
            this.panelSignatures.Size = new System.Drawing.Size(1654, 80);
            this.panelSignatures.TabIndex = 33;
            // 
            // panel110
            // 
            this.panel110.Controls.Add(this.labelReportSignDate);
            this.panel110.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel110.Location = new System.Drawing.Point(1414, 0);
            this.panel110.Name = "panel110";
            this.panel110.Size = new System.Drawing.Size(240, 80);
            this.panel110.TabIndex = 7;
            // 
            // labelReportSignDate
            // 
            this.labelReportSignDate.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelReportSignDate.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelReportSignDate.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.labelReportSignDate.Location = new System.Drawing.Point(0, 0);
            this.labelReportSignDate.Name = "labelReportSignDate";
            this.labelReportSignDate.Size = new System.Drawing.Size(240, 80);
            this.labelReportSignDate.TabIndex = 2;
            this.labelReportSignDate.Text = "^99/99/2019";
            this.labelReportSignDate.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.labelReportSignDate.Click += new System.EventHandler(this.labelReportSignDate_Click);
            // 
            // panel109
            // 
            this.panel109.Controls.Add(this.labelDateReportSign);
            this.panel109.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel109.Location = new System.Drawing.Point(1311, 0);
            this.panel109.Name = "panel109";
            this.panel109.Size = new System.Drawing.Size(103, 80);
            this.panel109.TabIndex = 6;
            // 
            // labelDateReportSign
            // 
            this.labelDateReportSign.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDateReportSign.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelDateReportSign.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.labelDateReportSign.Location = new System.Drawing.Point(0, 0);
            this.labelDateReportSign.Name = "labelDateReportSign";
            this.labelDateReportSign.Size = new System.Drawing.Size(103, 80);
            this.labelDateReportSign.TabIndex = 1;
            this.labelDateReportSign.Text = "Date:";
            this.labelDateReportSign.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // panel108
            // 
            this.panel108.Controls.Add(this.labelPhysSignLine);
            this.panel108.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel108.Location = new System.Drawing.Point(946, 0);
            this.panel108.Name = "panel108";
            this.panel108.Size = new System.Drawing.Size(365, 80);
            this.panel108.TabIndex = 5;
            // 
            // labelPhysSignLine
            // 
            this.labelPhysSignLine.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPhysSignLine.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelPhysSignLine.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.labelPhysSignLine.Location = new System.Drawing.Point(0, 0);
            this.labelPhysSignLine.Name = "labelPhysSignLine";
            this.labelPhysSignLine.Size = new System.Drawing.Size(365, 80);
            this.labelPhysSignLine.TabIndex = 1;
            this.labelPhysSignLine.Text = "^Simon\r\n---xxx--";
            this.labelPhysSignLine.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.labelPhysSignLine.Click += new System.EventHandler(this.labelPhysSignLine_Click);
            // 
            // panel107
            // 
            this.panel107.Controls.Add(this.labelPhysSign);
            this.panel107.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel107.Location = new System.Drawing.Point(611, 0);
            this.panel107.Name = "panel107";
            this.panel107.Size = new System.Drawing.Size(335, 80);
            this.panel107.TabIndex = 4;
            // 
            // labelPhysSign
            // 
            this.labelPhysSign.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPhysSign.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelPhysSign.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.labelPhysSign.Location = new System.Drawing.Point(0, 0);
            this.labelPhysSign.Name = "labelPhysSign";
            this.labelPhysSign.Size = new System.Drawing.Size(335, 80);
            this.labelPhysSign.TabIndex = 1;
            this.labelPhysSign.Text = "^QC\r\nPhysician signature:";
            this.labelPhysSign.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // panel106
            // 
            this.panel106.Controls.Add(this.labelPhysPrintName);
            this.panel106.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel106.Location = new System.Drawing.Point(264, 0);
            this.panel106.Name = "panel106";
            this.panel106.Size = new System.Drawing.Size(347, 80);
            this.panel106.TabIndex = 3;
            // 
            // labelPhysPrintName
            // 
            this.labelPhysPrintName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPhysPrintName.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelPhysPrintName.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.labelPhysPrintName.Location = new System.Drawing.Point(0, 0);
            this.labelPhysPrintName.Name = "labelPhysPrintName";
            this.labelPhysPrintName.Size = new System.Drawing.Size(347, 80);
            this.labelPhysPrintName.TabIndex = 1;
            this.labelPhysPrintName.Text = "^J. Smithsonian\r\nfunction";
            this.labelPhysPrintName.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.labelPhysPrintName.Click += new System.EventHandler(this.labelPhysPrintName_Click);
            // 
            // panel105
            // 
            this.panel105.Controls.Add(this.labelPhysNameReportPrint);
            this.panel105.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel105.Location = new System.Drawing.Point(0, 0);
            this.panel105.Name = "panel105";
            this.panel105.Size = new System.Drawing.Size(264, 80);
            this.panel105.TabIndex = 2;
            // 
            // labelPhysNameReportPrint
            // 
            this.labelPhysNameReportPrint.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPhysNameReportPrint.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelPhysNameReportPrint.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.labelPhysNameReportPrint.Location = new System.Drawing.Point(0, 0);
            this.labelPhysNameReportPrint.Name = "labelPhysNameReportPrint";
            this.labelPhysNameReportPrint.Size = new System.Drawing.Size(264, 80);
            this.labelPhysNameReportPrint.TabIndex = 2;
            this.labelPhysNameReportPrint.Text = "^QC\r\nPhysician name:";
            this.labelPhysNameReportPrint.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // panel51
            // 
            this.panel51.Controls.Add(this.label14);
            this.panel51.Controls.Add(this.labelStudyNr);
            this.panel51.Controls.Add(this.labelPage);
            this.panel51.Controls.Add(this.labelPage1of);
            this.panel51.Controls.Add(this.labelPageOf);
            this.panel51.Controls.Add(this.labelPage1xOfX);
            this.panel51.Controls.Add(this.labelPrintDate1Bottom);
            this.panel51.Controls.Add(this.label8);
            this.panel51.Controls.Add(this.label7);
            this.panel51.Controls.Add(this.label3);
            this.panel51.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel51.Location = new System.Drawing.Point(0, 2292);
            this.panel51.Name = "panel51";
            this.panel51.Size = new System.Drawing.Size(1654, 47);
            this.panel51.TabIndex = 34;
            // 
            // label14
            // 
            this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label14.Font = new System.Drawing.Font("Verdana", 18F);
            this.label14.Location = new System.Drawing.Point(681, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(498, 47);
            this.label14.TabIndex = 42;
            this.label14.Text = "2018 © Techmedic International B.V.";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelStudyNr
            // 
            this.labelStudyNr.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelStudyNr.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.labelStudyNr.Location = new System.Drawing.Point(1179, 0);
            this.labelStudyNr.Name = "labelStudyNr";
            this.labelStudyNr.Size = new System.Drawing.Size(155, 47);
            this.labelStudyNr.TabIndex = 43;
            this.labelStudyNr.Text = "1212212";
            this.labelStudyNr.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelPage
            // 
            this.labelPage.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelPage.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPage.Location = new System.Drawing.Point(1334, 0);
            this.labelPage.Name = "labelPage";
            this.labelPage.Size = new System.Drawing.Size(75, 47);
            this.labelPage.TabIndex = 12;
            this.labelPage.Text = "Page";
            this.labelPage.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelPage1of
            // 
            this.labelPage1of.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelPage1of.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.labelPage1of.Location = new System.Drawing.Point(1409, 0);
            this.labelPage1of.Name = "labelPage1of";
            this.labelPage1of.Size = new System.Drawing.Size(71, 47);
            this.labelPage1of.TabIndex = 11;
            this.labelPage1of.Text = "1";
            this.labelPage1of.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPageOf
            // 
            this.labelPageOf.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelPageOf.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPageOf.Location = new System.Drawing.Point(1480, 0);
            this.labelPageOf.Name = "labelPageOf";
            this.labelPageOf.Size = new System.Drawing.Size(47, 47);
            this.labelPageOf.TabIndex = 10;
            this.labelPageOf.Text = "of";
            this.labelPageOf.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPage1xOfX
            // 
            this.labelPage1xOfX.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelPage1xOfX.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.labelPage1xOfX.Location = new System.Drawing.Point(1527, 0);
            this.labelPage1xOfX.Name = "labelPage1xOfX";
            this.labelPage1xOfX.Size = new System.Drawing.Size(100, 47);
            this.labelPage1xOfX.TabIndex = 9;
            this.labelPage1xOfX.Text = "20p1";
            this.labelPage1xOfX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPrintDate1Bottom
            // 
            this.labelPrintDate1Bottom.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPrintDate1Bottom.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.labelPrintDate1Bottom.Location = new System.Drawing.Point(135, 0);
            this.labelPrintDate1Bottom.Name = "labelPrintDate1Bottom";
            this.labelPrintDate1Bottom.Size = new System.Drawing.Size(546, 47);
            this.labelPrintDate1Bottom.TabIndex = 7;
            this.labelPrintDate1Bottom.Text = "12/27/2016 22:22:22 AM+1200 v1234";
            this.labelPrintDate1Bottom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.Dock = System.Windows.Forms.DockStyle.Left;
            this.label8.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(27, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(108, 47);
            this.label8.TabIndex = 6;
            this.label8.Text = "Printed:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label7.Dock = System.Windows.Forms.DockStyle.Right;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label7.Location = new System.Drawing.Point(1627, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(27, 47);
            this.label7.TabIndex = 1;
            this.label7.Text = "R1";
            this.label7.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label7.Visible = false;
            // 
            // label3
            // 
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Dock = System.Windows.Forms.DockStyle.Left;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 47);
            this.label3.TabIndex = 0;
            this.label3.Text = "L1";
            this.label3.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label3.Visible = false;
            // 
            // panelPrintArea1
            // 
            this.panelPrintArea1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panelPrintArea1.Controls.Add(this.panelSignatures);
            this.panelPrintArea1.Controls.Add(this.panel126);
            this.panelPrintArea1.Controls.Add(this.panelPhysicianConclusion);
            this.panelPrintArea1.Controls.Add(this.panel51);
            this.panelPrintArea1.Controls.Add(this.panelRowInterpretation);
            this.panelPrintArea1.Controls.Add(this.panelLineUnderSampl1);
            this.panelPrintArea1.Controls.Add(this.panelEventsStat);
            this.panelPrintArea1.Controls.Add(this.panelStudyProcedures);
            this.panelPrintArea1.Controls.Add(this.panelHorSepPatDet);
            this.panelPrintArea1.Controls.Add(this.panelPhysInfo);
            this.panelPrintArea1.Controls.Add(this.panel7);
            this.panelPrintArea1.Controls.Add(this.panelDateTimeofEventStrip);
            this.panelPrintArea1.Controls.Add(this.panelCardiacEVentReportNumber);
            this.panelPrintArea1.Controls.Add(this.panelPrintHeader);
            this.panelPrintArea1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.panelPrintArea1.Location = new System.Drawing.Point(0, 100);
            this.panelPrintArea1.Name = "panelPrintArea1";
            this.panelPrintArea1.Size = new System.Drawing.Size(1654, 2339);
            this.panelPrintArea1.TabIndex = 0;
            // 
            // panel126
            // 
            this.panel126.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel126.Location = new System.Drawing.Point(0, 2282);
            this.panel126.Name = "panel126";
            this.panel126.Size = new System.Drawing.Size(1654, 10);
            this.panel126.TabIndex = 37;
            this.panel126.Paint += new System.Windows.Forms.PaintEventHandler(this.panel126_Paint);
            // 
            // panelPhysicianConclusion
            // 
            this.panelPhysicianConclusion.Controls.Add(this.panel72);
            this.panelPhysicianConclusion.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelPhysicianConclusion.Location = new System.Drawing.Point(0, 1797);
            this.panelPhysicianConclusion.Name = "panelPhysicianConclusion";
            this.panelPhysicianConclusion.Size = new System.Drawing.Size(1654, 382);
            this.panelPhysicianConclusion.TabIndex = 36;
            // 
            // panel72
            // 
            this.panel72.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel72.Controls.Add(this.labelConclusionText);
            this.panel72.Controls.Add(this.panel75);
            this.panel72.Controls.Add(this.panel82);
            this.panel72.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel72.Location = new System.Drawing.Point(0, 0);
            this.panel72.Name = "panel72";
            this.panel72.Size = new System.Drawing.Size(1654, 382);
            this.panel72.TabIndex = 10;
            // 
            // labelConclusionText
            // 
            this.labelConclusionText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelConclusionText.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelConclusionText.Location = new System.Drawing.Point(0, 47);
            this.labelConclusionText.Name = "labelConclusionText";
            this.labelConclusionText.Size = new System.Drawing.Size(1652, 333);
            this.labelConclusionText.TabIndex = 20;
            this.labelConclusionText.Text = "^Conclusion text";
            // 
            // panel75
            // 
            this.panel75.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel75.Location = new System.Drawing.Point(0, 37);
            this.panel75.Name = "panel75";
            this.panel75.Size = new System.Drawing.Size(1652, 10);
            this.panel75.TabIndex = 19;
            // 
            // panel82
            // 
            this.panel82.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel82.Controls.Add(this.label9);
            this.panel82.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel82.Location = new System.Drawing.Point(0, 0);
            this.panel82.Name = "panel82";
            this.panel82.Size = new System.Drawing.Size(1652, 37);
            this.panel82.TabIndex = 4;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Left;
            this.label9.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(0, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(329, 32);
            this.label9.TabIndex = 1;
            this.label9.Text = "Physician conclusion";
            // 
            // panelRowInterpretation
            // 
            this.panelRowInterpretation.Controls.Add(this.panel89);
            this.panelRowInterpretation.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelRowInterpretation.Location = new System.Drawing.Point(0, 1467);
            this.panelRowInterpretation.Name = "panelRowInterpretation";
            this.panelRowInterpretation.Size = new System.Drawing.Size(1654, 330);
            this.panelRowInterpretation.TabIndex = 31;
            // 
            // panel89
            // 
            this.panel89.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel89.Controls.Add(this.labelSummaryText);
            this.panel89.Controls.Add(this.panel91);
            this.panel89.Controls.Add(this.panel90);
            this.panel89.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel89.Location = new System.Drawing.Point(0, 0);
            this.panel89.Name = "panel89";
            this.panel89.Size = new System.Drawing.Size(1653, 330);
            this.panel89.TabIndex = 10;
            // 
            // labelSummaryText
            // 
            this.labelSummaryText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSummaryText.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSummaryText.Location = new System.Drawing.Point(0, 47);
            this.labelSummaryText.Name = "labelSummaryText";
            this.labelSummaryText.Size = new System.Drawing.Size(1651, 281);
            this.labelSummaryText.TabIndex = 20;
            this.labelSummaryText.Text = resources.GetString("labelSummaryText.Text");
            // 
            // panel91
            // 
            this.panel91.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel91.Location = new System.Drawing.Point(0, 37);
            this.panel91.Name = "panel91";
            this.panel91.Size = new System.Drawing.Size(1651, 10);
            this.panel91.TabIndex = 19;
            // 
            // panel90
            // 
            this.panel90.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel90.Controls.Add(this.labelReportFindings);
            this.panel90.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel90.Location = new System.Drawing.Point(0, 0);
            this.panel90.Name = "panel90";
            this.panel90.Size = new System.Drawing.Size(1651, 37);
            this.panel90.TabIndex = 4;
            // 
            // labelReportFindings
            // 
            this.labelReportFindings.AutoSize = true;
            this.labelReportFindings.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelReportFindings.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelReportFindings.Location = new System.Drawing.Point(0, 0);
            this.labelReportFindings.Name = "labelReportFindings";
            this.labelReportFindings.Size = new System.Drawing.Size(247, 32);
            this.labelReportFindings.TabIndex = 1;
            this.labelReportFindings.Text = "Report findings";
            // 
            // panelPhysInfo
            // 
            this.panelPhysInfo.Controls.Add(this.panel67);
            this.panelPhysInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelPhysInfo.Location = new System.Drawing.Point(0, 507);
            this.panelPhysInfo.Name = "panelPhysInfo";
            this.panelPhysInfo.Size = new System.Drawing.Size(1654, 147);
            this.panelPhysInfo.TabIndex = 39;
            // 
            // panel67
            // 
            this.panel67.Controls.Add(this.panel258);
            this.panel67.Controls.Add(this.panel256);
            this.panel67.Controls.Add(this.panel1);
            this.panel67.Controls.Add(this.panelVert9);
            this.panel67.Controls.Add(this.panel252);
            this.panel67.Controls.Add(this.panel250);
            this.panel67.Controls.Add(this.panelVert8);
            this.panel67.Controls.Add(this.panel244);
            this.panel67.Controls.Add(this.panel242);
            this.panel67.Controls.Add(this.panelVert6);
            this.panel67.Controls.Add(this.panel32);
            this.panel67.Controls.Add(this.panel41);
            this.panel67.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel67.Location = new System.Drawing.Point(0, 0);
            this.panel67.Name = "panel67";
            this.panel67.Size = new System.Drawing.Size(1654, 146);
            this.panel67.TabIndex = 8;
            // 
            // panel258
            // 
            this.panel258.Controls.Add(this.labelClientName);
            this.panel258.Controls.Add(this.labelClientTel);
            this.panel258.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel258.Location = new System.Drawing.Point(1280, 1);
            this.panel258.Name = "panel258";
            this.panel258.Size = new System.Drawing.Size(373, 145);
            this.panel258.TabIndex = 20;
            // 
            // labelClientName
            // 
            this.labelClientName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelClientName.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelClientName.Location = new System.Drawing.Point(0, 0);
            this.labelClientName.Name = "labelClientName";
            this.labelClientName.Size = new System.Drawing.Size(373, 98);
            this.labelClientName.TabIndex = 32;
            this.labelClientName.Text = "^New York Hospital";
            // 
            // labelClientTel
            // 
            this.labelClientTel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelClientTel.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelClientTel.Location = new System.Drawing.Point(0, 98);
            this.labelClientTel.Name = "labelClientTel";
            this.labelClientTel.Size = new System.Drawing.Size(373, 47);
            this.labelClientTel.TabIndex = 42;
            this.labelClientTel.Text = "^800-217-0520";
            this.labelClientTel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel256
            // 
            this.panel256.Controls.Add(this.label41);
            this.panel256.Controls.Add(this.label44);
            this.panel256.Controls.Add(this.labelClientHeader);
            this.panel256.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel256.Location = new System.Drawing.Point(1184, 1);
            this.panel256.Name = "panel256";
            this.panel256.Size = new System.Drawing.Size(96, 145);
            this.panel256.TabIndex = 18;
            // 
            // label41
            // 
            this.label41.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label41.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(0, 98);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(96, 47);
            this.label41.TabIndex = 42;
            this.label41.Text = "Phone:";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label44
            // 
            this.label44.Dock = System.Windows.Forms.DockStyle.Top;
            this.label44.Font = new System.Drawing.Font("Verdana", 27F);
            this.label44.Location = new System.Drawing.Point(0, 47);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(96, 47);
            this.label44.TabIndex = 40;
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelClientHeader
            // 
            this.labelClientHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelClientHeader.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelClientHeader.Location = new System.Drawing.Point(0, 0);
            this.labelClientHeader.Name = "labelClientHeader";
            this.labelClientHeader.Size = new System.Drawing.Size(96, 47);
            this.labelClientHeader.TabIndex = 20;
            this.labelClientHeader.Text = "Name:";
            this.labelClientHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel1
            // 
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.panel31);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(1118, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(66, 145);
            this.panel1.TabIndex = 26;
            // 
            // panel31
            // 
            this.panel31.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel31.BackgroundImage")));
            this.panel31.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel31.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel31.Location = new System.Drawing.Point(0, 0);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(66, 120);
            this.panel31.TabIndex = 27;
            // 
            // panelVert9
            // 
            this.panelVert9.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelVert9.Location = new System.Drawing.Point(1116, 1);
            this.panelVert9.Name = "panelVert9";
            this.panelVert9.Size = new System.Drawing.Size(2, 145);
            this.panelVert9.TabIndex = 15;
            // 
            // panel252
            // 
            this.panel252.Controls.Add(this.labelRefPhysicianName);
            this.panel252.Controls.Add(this.labelRefPhysicianTel);
            this.panel252.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel252.Location = new System.Drawing.Point(668, 1);
            this.panel252.Name = "panel252";
            this.panel252.Size = new System.Drawing.Size(448, 145);
            this.panel252.TabIndex = 14;
            // 
            // labelRefPhysicianName
            // 
            this.labelRefPhysicianName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelRefPhysicianName.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelRefPhysicianName.Location = new System.Drawing.Point(0, 0);
            this.labelRefPhysicianName.Name = "labelRefPhysicianName";
            this.labelRefPhysicianName.Size = new System.Drawing.Size(448, 90);
            this.labelRefPhysicianName.TabIndex = 29;
            this.labelRefPhysicianName.Text = "^Dr. John Smithsonian";
            // 
            // labelRefPhysicianTel
            // 
            this.labelRefPhysicianTel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelRefPhysicianTel.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelRefPhysicianTel.Location = new System.Drawing.Point(0, 90);
            this.labelRefPhysicianTel.Name = "labelRefPhysicianTel";
            this.labelRefPhysicianTel.Size = new System.Drawing.Size(448, 55);
            this.labelRefPhysicianTel.TabIndex = 41;
            this.labelRefPhysicianTel.Text = "^800-217-0520";
            this.labelRefPhysicianTel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel250
            // 
            this.panel250.Controls.Add(this.label42);
            this.panel250.Controls.Add(this.label52);
            this.panel250.Controls.Add(this.labelRefPhysHeader);
            this.panel250.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel250.Location = new System.Drawing.Point(547, 1);
            this.panel250.Name = "panel250";
            this.panel250.Size = new System.Drawing.Size(121, 145);
            this.panel250.TabIndex = 12;
            // 
            // label42
            // 
            this.label42.Dock = System.Windows.Forms.DockStyle.Top;
            this.label42.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(0, 94);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(121, 47);
            this.label42.TabIndex = 41;
            this.label42.Text = "Phone:";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label52
            // 
            this.label52.Dock = System.Windows.Forms.DockStyle.Top;
            this.label52.Font = new System.Drawing.Font("Verdana", 27F);
            this.label52.Location = new System.Drawing.Point(0, 47);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(121, 47);
            this.label52.TabIndex = 39;
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelRefPhysHeader
            // 
            this.labelRefPhysHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelRefPhysHeader.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRefPhysHeader.Location = new System.Drawing.Point(0, 0);
            this.labelRefPhysHeader.Name = "labelRefPhysHeader";
            this.labelRefPhysHeader.Size = new System.Drawing.Size(121, 47);
            this.labelRefPhysHeader.TabIndex = 19;
            this.labelRefPhysHeader.Text = "Ref Phys.:";
            this.labelRefPhysHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panelVert8
            // 
            this.panelVert8.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelVert8.Location = new System.Drawing.Point(545, 1);
            this.panelVert8.Name = "panelVert8";
            this.panelVert8.Size = new System.Drawing.Size(2, 145);
            this.panelVert8.TabIndex = 11;
            // 
            // panel244
            // 
            this.panel244.Controls.Add(this.labelPhysicianName);
            this.panel244.Controls.Add(this.labelPhysicianTel);
            this.panel244.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel244.Location = new System.Drawing.Point(160, 1);
            this.panel244.Name = "panel244";
            this.panel244.Size = new System.Drawing.Size(385, 145);
            this.panel244.TabIndex = 10;
            // 
            // labelPhysicianName
            // 
            this.labelPhysicianName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPhysicianName.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelPhysicianName.Location = new System.Drawing.Point(0, 0);
            this.labelPhysicianName.Name = "labelPhysicianName";
            this.labelPhysicianName.Size = new System.Drawing.Size(385, 98);
            this.labelPhysicianName.TabIndex = 25;
            this.labelPhysicianName.Text = "^Dr. John Smithsonian hjsgdhfhag";
            // 
            // labelPhysicianTel
            // 
            this.labelPhysicianTel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelPhysicianTel.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelPhysicianTel.Location = new System.Drawing.Point(0, 98);
            this.labelPhysicianTel.Name = "labelPhysicianTel";
            this.labelPhysicianTel.Size = new System.Drawing.Size(385, 47);
            this.labelPhysicianTel.TabIndex = 40;
            this.labelPhysicianTel.Text = "!800-217-0520";
            this.labelPhysicianTel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel242
            // 
            this.panel242.Controls.Add(this.label53);
            this.panel242.Controls.Add(this.label54);
            this.panel242.Controls.Add(this.labelPhysHeader);
            this.panel242.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel242.Location = new System.Drawing.Point(68, 1);
            this.panel242.Name = "panel242";
            this.panel242.Size = new System.Drawing.Size(92, 145);
            this.panel242.TabIndex = 8;
            // 
            // label53
            // 
            this.label53.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label53.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(0, 98);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(92, 47);
            this.label53.TabIndex = 40;
            this.label53.Text = "Phone:";
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label54
            // 
            this.label54.Dock = System.Windows.Forms.DockStyle.Top;
            this.label54.Font = new System.Drawing.Font("Verdana", 27F);
            this.label54.Location = new System.Drawing.Point(0, 47);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(92, 47);
            this.label54.TabIndex = 38;
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPhysHeader
            // 
            this.labelPhysHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPhysHeader.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPhysHeader.Location = new System.Drawing.Point(0, 0);
            this.labelPhysHeader.Name = "labelPhysHeader";
            this.labelPhysHeader.Size = new System.Drawing.Size(92, 47);
            this.labelPhysHeader.TabIndex = 17;
            this.labelPhysHeader.Text = "Phys.:";
            this.labelPhysHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panelVert6
            // 
            this.panelVert6.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelVert6.Location = new System.Drawing.Point(66, 1);
            this.panelVert6.Name = "panelVert6";
            this.panelVert6.Size = new System.Drawing.Size(2, 145);
            this.panelVert6.TabIndex = 4;
            // 
            // panel32
            // 
            this.panel32.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel32.Controls.Add(this.panel36);
            this.panel32.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel32.Location = new System.Drawing.Point(0, 1);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(66, 145);
            this.panel32.TabIndex = 25;
            // 
            // panel36
            // 
            this.panel36.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel36.BackgroundImage")));
            this.panel36.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel36.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel36.Location = new System.Drawing.Point(0, 0);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(66, 120);
            this.panel36.TabIndex = 26;
            // 
            // panel41
            // 
            this.panel41.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel41.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel41.Location = new System.Drawing.Point(0, 0);
            this.panel41.Name = "panel41";
            this.panel41.Size = new System.Drawing.Size(1654, 1);
            this.panel41.TabIndex = 27;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.panel8);
            this.panel7.Controls.Add(this.panel10);
            this.panel7.Controls.Add(this.panel11);
            this.panel7.Controls.Add(this.panel34);
            this.panel7.Controls.Add(this.panel12);
            this.panel7.Controls.Add(this.panel18);
            this.panel7.Controls.Add(this.panel30);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 253);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1654, 254);
            this.panel7.TabIndex = 38;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.labelEndStudyDate);
            this.panel8.Controls.Add(this.labelRoom);
            this.panel8.Controls.Add(this.labelPatID);
            this.panel8.Controls.Add(this.labelSerialNr);
            this.panel8.Controls.Add(this.labelStartDate);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(1390, 2);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(264, 252);
            this.panel8.TabIndex = 6;
            // 
            // labelEndStudyDate
            // 
            this.labelEndStudyDate.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEndStudyDate.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelEndStudyDate.Location = new System.Drawing.Point(0, 201);
            this.labelEndStudyDate.Name = "labelEndStudyDate";
            this.labelEndStudyDate.Size = new System.Drawing.Size(264, 47);
            this.labelEndStudyDate.TabIndex = 47;
            this.labelEndStudyDate.Text = "^10/23/2016";
            this.labelEndStudyDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelEndStudyDate.Visible = false;
            // 
            // labelRoom
            // 
            this.labelRoom.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelRoom.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelRoom.Location = new System.Drawing.Point(0, 166);
            this.labelRoom.Name = "labelRoom";
            this.labelRoom.Size = new System.Drawing.Size(264, 35);
            this.labelRoom.TabIndex = 49;
            this.labelRoom.Text = "210 - bed 4";
            // 
            // labelPatID
            // 
            this.labelPatID.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPatID.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelPatID.Location = new System.Drawing.Point(0, 94);
            this.labelPatID.Name = "labelPatID";
            this.labelPatID.Size = new System.Drawing.Size(264, 72);
            this.labelPatID.TabIndex = 46;
            this.labelPatID.Text = "^12341234567";
            // 
            // labelSerialNr
            // 
            this.labelSerialNr.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelSerialNr.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelSerialNr.Location = new System.Drawing.Point(0, 47);
            this.labelSerialNr.Name = "labelSerialNr";
            this.labelSerialNr.Size = new System.Drawing.Size(264, 47);
            this.labelSerialNr.TabIndex = 41;
            this.labelSerialNr.Text = "^AA102044403";
            this.labelSerialNr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStartDate
            // 
            this.labelStartDate.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStartDate.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelStartDate.Location = new System.Drawing.Point(0, 0);
            this.labelStartDate.Name = "labelStartDate";
            this.labelStartDate.Size = new System.Drawing.Size(264, 47);
            this.labelStartDate.TabIndex = 40;
            this.labelStartDate.Text = "^10/23/2016";
            this.labelStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelStartDate.Click += new System.EventHandler(this.labelStartDate_Click);
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.labelEndStudyText);
            this.panel10.Controls.Add(this.label4);
            this.panel10.Controls.Add(this.labelPatIDHeader);
            this.panel10.Controls.Add(this.labelSerialNrHeader);
            this.panel10.Controls.Add(this.labelStartDateHeader);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel10.Location = new System.Drawing.Point(1279, 2);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(111, 252);
            this.panel10.TabIndex = 5;
            // 
            // labelEndStudyText
            // 
            this.labelEndStudyText.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelEndStudyText.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEndStudyText.Location = new System.Drawing.Point(0, 201);
            this.labelEndStudyText.Name = "labelEndStudyText";
            this.labelEndStudyText.Size = new System.Drawing.Size(111, 47);
            this.labelEndStudyText.TabIndex = 42;
            this.labelEndStudyText.Text = "Ended:";
            this.labelEndStudyText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelEndStudyText.Visible = false;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(0, 166);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 35);
            this.label4.TabIndex = 44;
            this.label4.Text = "Room:";
            // 
            // labelPatIDHeader
            // 
            this.labelPatIDHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPatIDHeader.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPatIDHeader.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.labelPatIDHeader.Location = new System.Drawing.Point(0, 94);
            this.labelPatIDHeader.Name = "labelPatIDHeader";
            this.labelPatIDHeader.Size = new System.Drawing.Size(111, 72);
            this.labelPatIDHeader.TabIndex = 41;
            this.labelPatIDHeader.Text = "Patient ID:";
            // 
            // labelSerialNrHeader
            // 
            this.labelSerialNrHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelSerialNrHeader.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSerialNrHeader.Location = new System.Drawing.Point(0, 47);
            this.labelSerialNrHeader.Name = "labelSerialNrHeader";
            this.labelSerialNrHeader.Size = new System.Drawing.Size(111, 47);
            this.labelSerialNrHeader.TabIndex = 36;
            this.labelSerialNrHeader.Text = "Serial nr:";
            this.labelSerialNrHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStartDateHeader
            // 
            this.labelStartDateHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStartDateHeader.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStartDateHeader.Location = new System.Drawing.Point(0, 0);
            this.labelStartDateHeader.Name = "labelStartDateHeader";
            this.labelStartDateHeader.Size = new System.Drawing.Size(111, 47);
            this.labelStartDateHeader.TabIndex = 35;
            this.labelStartDateHeader.Text = "Enrolled:";
            this.labelStartDateHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel11
            // 
            this.panel11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel11.Controls.Add(this.panel24);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel11.Location = new System.Drawing.Point(1213, 2);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(66, 252);
            this.panel11.TabIndex = 4;
            // 
            // panel24
            // 
            this.panel24.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel24.BackgroundImage")));
            this.panel24.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel24.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel24.Location = new System.Drawing.Point(0, 0);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(66, 120);
            this.panel24.TabIndex = 5;
            // 
            // panel34
            // 
            this.panel34.Controls.Add(this.labelPhone2);
            this.panel34.Controls.Add(this.labelPhone1);
            this.panel34.Controls.Add(this.labelCountry);
            this.panel34.Controls.Add(this.labelZipCity);
            this.panel34.Controls.Add(this.labelAddress);
            this.panel34.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel34.Location = new System.Drawing.Point(668, 2);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(545, 252);
            this.panel34.TabIndex = 3;
            // 
            // labelPhone2
            // 
            this.labelPhone2.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPhone2.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelPhone2.Location = new System.Drawing.Point(0, 188);
            this.labelPhone2.Name = "labelPhone2";
            this.labelPhone2.Size = new System.Drawing.Size(545, 47);
            this.labelPhone2.TabIndex = 48;
            this.labelPhone2.Text = "^800-217-0520";
            this.labelPhone2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelPhone1
            // 
            this.labelPhone1.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPhone1.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelPhone1.Location = new System.Drawing.Point(0, 141);
            this.labelPhone1.Name = "labelPhone1";
            this.labelPhone1.Size = new System.Drawing.Size(545, 47);
            this.labelPhone1.TabIndex = 47;
            this.labelPhone1.Text = "^800-217-0520";
            this.labelPhone1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelCountry
            // 
            this.labelCountry.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelCountry.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelCountry.Location = new System.Drawing.Point(0, 94);
            this.labelCountry.Name = "labelCountry";
            this.labelCountry.Size = new System.Drawing.Size(545, 47);
            this.labelCountry.TabIndex = 46;
            this.labelCountry.Text = "^Country";
            this.labelCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelZipCity
            // 
            this.labelZipCity.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelZipCity.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelZipCity.Location = new System.Drawing.Point(0, 47);
            this.labelZipCity.Name = "labelZipCity";
            this.labelZipCity.Size = new System.Drawing.Size(545, 47);
            this.labelZipCity.TabIndex = 45;
            this.labelZipCity.Text = "^Texas TX 200111";
            this.labelZipCity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelAddress
            // 
            this.labelAddress.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelAddress.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelAddress.Location = new System.Drawing.Point(0, 0);
            this.labelAddress.Name = "labelAddress";
            this.labelAddress.Size = new System.Drawing.Size(545, 47);
            this.labelAddress.TabIndex = 44;
            this.labelAddress.Text = "^112 Alpha Street App 1000 abcde";
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.label16);
            this.panel12.Controls.Add(this.label27);
            this.panel12.Controls.Add(this.label28);
            this.panel12.Controls.Add(this.labelAddressHeader);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel12.Location = new System.Drawing.Point(545, 2);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(123, 252);
            this.panel12.TabIndex = 2;
            // 
            // label16
            // 
            this.label16.Dock = System.Windows.Forms.DockStyle.Top;
            this.label16.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(0, 141);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(123, 47);
            this.label16.TabIndex = 39;
            this.label16.Text = "Phone:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label27
            // 
            this.label27.Dock = System.Windows.Forms.DockStyle.Top;
            this.label27.Font = new System.Drawing.Font("Verdana", 27F);
            this.label27.Location = new System.Drawing.Point(0, 94);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(123, 47);
            this.label27.TabIndex = 38;
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label28
            // 
            this.label28.Dock = System.Windows.Forms.DockStyle.Top;
            this.label28.Font = new System.Drawing.Font("Verdana", 27F);
            this.label28.Location = new System.Drawing.Point(0, 47);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(123, 47);
            this.label28.TabIndex = 37;
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelAddressHeader
            // 
            this.labelAddressHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelAddressHeader.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAddressHeader.Location = new System.Drawing.Point(0, 0);
            this.labelAddressHeader.Name = "labelAddressHeader";
            this.labelAddressHeader.Size = new System.Drawing.Size(123, 47);
            this.labelAddressHeader.TabIndex = 36;
            this.labelAddressHeader.Text = "Address:";
            this.labelAddressHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.panel27);
            this.panel18.Controls.Add(this.panel22);
            this.panel18.Controls.Add(this.panel25);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel18.Location = new System.Drawing.Point(0, 2);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(545, 252);
            this.panel18.TabIndex = 1;
            // 
            // panel27
            // 
            this.panel27.Controls.Add(this.labelPatientLastName);
            this.panel27.Controls.Add(this.labelPatientFirstName);
            this.panel27.Controls.Add(this.labelDateOfBirth);
            this.panel27.Controls.Add(this.labelAgeGender);
            this.panel27.Controls.Add(this.panel21);
            this.panel27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel27.Location = new System.Drawing.Point(160, 0);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(385, 252);
            this.panel27.TabIndex = 37;
            // 
            // labelPatientLastName
            // 
            this.labelPatientLastName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPatientLastName.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelPatientLastName.Location = new System.Drawing.Point(0, 0);
            this.labelPatientLastName.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.labelPatientLastName.Name = "labelPatientLastName";
            this.labelPatientLastName.Size = new System.Drawing.Size(385, 91);
            this.labelPatientLastName.TabIndex = 41;
            this.labelPatientLastName.Text = "^Brest van Kempen\r\nhsgkh\r\n";
            // 
            // labelPatientFirstName
            // 
            this.labelPatientFirstName.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelPatientFirstName.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelPatientFirstName.Location = new System.Drawing.Point(0, 91);
            this.labelPatientFirstName.Name = "labelPatientFirstName";
            this.labelPatientFirstName.Size = new System.Drawing.Size(385, 47);
            this.labelPatientFirstName.TabIndex = 42;
            this.labelPatientFirstName.Text = "^Rutger Alexander";
            this.labelPatientFirstName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelDateOfBirth
            // 
            this.labelDateOfBirth.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelDateOfBirth.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelDateOfBirth.Location = new System.Drawing.Point(0, 138);
            this.labelDateOfBirth.Name = "labelDateOfBirth";
            this.labelDateOfBirth.Size = new System.Drawing.Size(385, 47);
            this.labelDateOfBirth.TabIndex = 43;
            this.labelDateOfBirth.Text = "^06/08/1974";
            this.labelDateOfBirth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelAgeGender
            // 
            this.labelAgeGender.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelAgeGender.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelAgeGender.Location = new System.Drawing.Point(0, 185);
            this.labelAgeGender.Name = "labelAgeGender";
            this.labelAgeGender.Size = new System.Drawing.Size(385, 47);
            this.labelAgeGender.TabIndex = 44;
            this.labelAgeGender.Text = "^41, male";
            this.labelAgeGender.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel21
            // 
            this.panel21.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel21.Location = new System.Drawing.Point(0, 232);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(385, 20);
            this.panel21.TabIndex = 45;
            // 
            // panel22
            // 
            this.panel22.Controls.Add(this.label32);
            this.panel22.Controls.Add(this.labelDOBheader);
            this.panel22.Controls.Add(this.label37);
            this.panel22.Controls.Add(this.label40);
            this.panel22.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel22.Location = new System.Drawing.Point(66, 0);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(94, 252);
            this.panel22.TabIndex = 36;
            // 
            // label32
            // 
            this.label32.Dock = System.Windows.Forms.DockStyle.Top;
            this.label32.Font = new System.Drawing.Font("Verdana", 27F);
            this.label32.Location = new System.Drawing.Point(0, 47);
            this.label32.Margin = new System.Windows.Forms.Padding(0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(94, 47);
            this.label32.TabIndex = 39;
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelDOBheader
            // 
            this.labelDOBheader.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelDOBheader.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDOBheader.Location = new System.Drawing.Point(0, 141);
            this.labelDOBheader.Margin = new System.Windows.Forms.Padding(0);
            this.labelDOBheader.Name = "labelDOBheader";
            this.labelDOBheader.Size = new System.Drawing.Size(94, 47);
            this.labelDOBheader.TabIndex = 37;
            this.labelDOBheader.Text = "D.o.b.:";
            this.labelDOBheader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label37
            // 
            this.label37.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label37.Font = new System.Drawing.Font("Verdana", 27F);
            this.label37.Location = new System.Drawing.Point(0, 188);
            this.label37.Margin = new System.Windows.Forms.Padding(0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(94, 64);
            this.label37.TabIndex = 38;
            this.label37.Text = " ";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label40
            // 
            this.label40.Dock = System.Windows.Forms.DockStyle.Top;
            this.label40.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(0, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(94, 47);
            this.label40.TabIndex = 36;
            this.label40.Text = "Name:";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel25
            // 
            this.panel25.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel25.Controls.Add(this.panel29);
            this.panel25.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel25.Location = new System.Drawing.Point(0, 0);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(66, 252);
            this.panel25.TabIndex = 38;
            // 
            // panel29
            // 
            this.panel29.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel29.BackgroundImage")));
            this.panel29.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel29.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel29.Location = new System.Drawing.Point(0, 0);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(66, 120);
            this.panel29.TabIndex = 39;
            // 
            // panel30
            // 
            this.panel30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel30.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel30.Location = new System.Drawing.Point(0, 0);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(1654, 2);
            this.panel30.TabIndex = 0;
            // 
            // CPrintStudyEventsForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1872, 1061);
            this.Controls.Add(this.panelPrintArea2);
            this.Controls.Add(this.panelPrintArea3);
            this.Controls.Add(this.panelPrintArea1);
            this.Controls.Add(this.toolStrip1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "CPrintStudyEventsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "^,202";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CPrintStudyEventsForm_FormClosing);
            this.Shown += new System.EventHandler(this.CPrintStudyEventsForm_Shown);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panelPrintArea3.ResumeLayout(false);
            this.panelPrintArea3.PerformLayout();
            this.panelEventSample5.ResumeLayout(false);
            this.panel96.ResumeLayout(false);
            this.panel96.PerformLayout();
            this.panel98.ResumeLayout(false);
            this.panel98.PerformLayout();
            this.panel117.ResumeLayout(false);
            this.panel117.PerformLayout();
            this.panel92.ResumeLayout(false);
            this.panel92.PerformLayout();
            this.panel137.ResumeLayout(false);
            this.panel97.ResumeLayout(false);
            this.panel113.ResumeLayout(false);
            this.panel112.ResumeLayout(false);
            this.panel111.ResumeLayout(false);
            this.panel102.ResumeLayout(false);
            this.panel93.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel153.ResumeLayout(false);
            this.panel290.ResumeLayout(false);
            this.panel291.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample5Full)).EndInit();
            this.panel378.ResumeLayout(false);
            this.panel421.ResumeLayout(false);
            this.panel422.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample5)).EndInit();
            this.panel453.ResumeLayout(false);
            this.panelEventSample4.ResumeLayout(false);
            this.panel114.ResumeLayout(false);
            this.panel114.PerformLayout();
            this.panel94.ResumeLayout(false);
            this.panel94.PerformLayout();
            this.panel63.ResumeLayout(false);
            this.panel63.PerformLayout();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            this.panel119.ResumeLayout(false);
            this.panel95.ResumeLayout(false);
            this.panel49.ResumeLayout(false);
            this.panel48.ResumeLayout(false);
            this.panel43.ResumeLayout(false);
            this.panel33.ResumeLayout(false);
            this.panel26.ResumeLayout(false);
            this.panel23.ResumeLayout(false);
            this.panel266.ResumeLayout(false);
            this.panel275.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample4Full)).EndInit();
            this.panel306.ResumeLayout(false);
            this.panel328.ResumeLayout(false);
            this.panel329.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample4)).EndInit();
            this.panel357.ResumeLayout(false);
            this.panelEventSample3.ResumeLayout(false);
            this.panel255.ResumeLayout(false);
            this.panel255.PerformLayout();
            this.panelActivities3.ResumeLayout(false);
            this.panelActivities3.PerformLayout();
            this.panelSymptoms3.ResumeLayout(false);
            this.panelSymptoms3.PerformLayout();
            this.panelfindings3.ResumeLayout(false);
            this.panelfindings3.PerformLayout();
            this.panel118.ResumeLayout(false);
            this.panelQRSmeasurements3.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel277.ResumeLayout(false);
            this.panel283.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample3Full)).EndInit();
            this.panel384.ResumeLayout(false);
            this.panel387.ResumeLayout(false);
            this.panel388.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample3)).EndInit();
            this.panel416.ResumeLayout(false);
            this.panelPage2Footer.ResumeLayout(false);
            this.panel101.ResumeLayout(false);
            this.panelPage2Header.ResumeLayout(false);
            this.panel122.ResumeLayout(false);
            this.panel500.ResumeLayout(false);
            this.panel500.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCenter3)).EndInit();
            this.panel501.ResumeLayout(false);
            this.panel503.ResumeLayout(false);
            this.panel504.ResumeLayout(false);
            this.panelPrintArea2.ResumeLayout(false);
            this.panelPrintArea2.PerformLayout();
            this.panelGridTableFill.ResumeLayout(false);
            this.panelGridTableFill.PerformLayout();
            this.panelGridTable.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.panelHeaderPage2.ResumeLayout(false);
            this.panel37.ResumeLayout(false);
            this.panel40.ResumeLayout(false);
            this.panel800.ResumeLayout(false);
            this.panel802.ResumeLayout(false);
            this.panelPage2Header2.ResumeLayout(false);
            this.panel120.ResumeLayout(false);
            this.panelPage2Header1.ResumeLayout(false);
            this.panelPage2Header1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCenter2)).EndInit();
            this.panel826.ResumeLayout(false);
            this.panel828.ResumeLayout(false);
            this.panel829.ResumeLayout(false);
            this.panel632.ResumeLayout(false);
            this.panel785.ResumeLayout(false);
            this.panelPrintHeader.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCenter)).EndInit();
            this.panel56.ResumeLayout(false);
            this.panel58.ResumeLayout(false);
            this.panel59.ResumeLayout(false);
            this.panelCardiacEVentReportNumber.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel50.ResumeLayout(false);
            this.panelDateTimeofEventStrip.ResumeLayout(false);
            this.panel53.ResumeLayout(false);
            this.panelStudyProcedures.ResumeLayout(false);
            this.panelDiagnosis.ResumeLayout(false);
            this.panelHartRate.ResumeLayout(false);
            this.panel54.ResumeLayout(false);
            this.panel52.ResumeLayout(false);
            this.panel46.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel69.ResumeLayout(false);
            this.panelEventsStat.ResumeLayout(false);
            this.panel753.ResumeLayout(false);
            this.panel756ST.ResumeLayout(false);
            this.panel771.ResumeLayout(false);
            this.panel74.ResumeLayout(false);
            this.panel770.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel39.ResumeLayout(false);
            this.panel769.ResumeLayout(false);
            this.panel146.ResumeLayout(false);
            this.panel85.ResumeLayout(false);
            this.panel80.ResumeLayout(false);
            this.panel79.ResumeLayout(false);
            this.panel38.ResumeLayout(false);
            this.panel768.ResumeLayout(false);
            this.panel767.ResumeLayout(false);
            this.panel766.ResumeLayout(false);
            this.panel765.ResumeLayout(false);
            this.panel764.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.panel754.ResumeLayout(false);
            this.panel759.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartSummaryBar)).EndInit();
            this.panel758.ResumeLayout(false);
            this.panel758.PerformLayout();
            this.panelSignatures.ResumeLayout(false);
            this.panel110.ResumeLayout(false);
            this.panel109.ResumeLayout(false);
            this.panel108.ResumeLayout(false);
            this.panel107.ResumeLayout(false);
            this.panel106.ResumeLayout(false);
            this.panel105.ResumeLayout(false);
            this.panel51.ResumeLayout(false);
            this.panelPrintArea1.ResumeLayout(false);
            this.panelPhysicianConclusion.ResumeLayout(false);
            this.panel72.ResumeLayout(false);
            this.panel82.ResumeLayout(false);
            this.panel82.PerformLayout();
            this.panelRowInterpretation.ResumeLayout(false);
            this.panel89.ResumeLayout(false);
            this.panel90.ResumeLayout(false);
            this.panel90.PerformLayout();
            this.panelPhysInfo.ResumeLayout(false);
            this.panel67.ResumeLayout(false);
            this.panel258.ResumeLayout(false);
            this.panel256.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel252.ResumeLayout(false);
            this.panel250.ResumeLayout(false);
            this.panel244.ResumeLayout(false);
            this.panel242.ResumeLayout(false);
            this.panel32.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel34.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel18.ResumeLayout(false);
            this.panel27.ResumeLayout(false);
            this.panel22.ResumeLayout(false);
            this.panel25.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButtonPrint;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripButton toolStripClipboard1;
        private System.Windows.Forms.ToolStripButton toolStripEnterData;
        private System.Windows.Forms.Panel panelPrintArea3;
        private System.Windows.Forms.Panel panelPage2Footer;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ToolStripButton toolStripClipboard2;
        private System.Windows.Forms.ToolStripButton toolStripGenPages;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label labelPage3Of;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label labelPage3xOfX;
        private System.Windows.Forms.Label labelPrintDate;
        private System.Windows.Forms.Panel panelPrintArea2;
        private System.Windows.Forms.Panel panel785;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label labelPage2Of;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label labelPage2xOfX;
        private System.Windows.Forms.Label labelPrintDate2Bottom;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Panel panel632;
        private System.Windows.Forms.Panel panel706;
        private System.Windows.Forms.Panel panelHeaderPage2;
        private System.Windows.Forms.Panel panel508;
        private System.Windows.Forms.Panel panel745;
        private System.Windows.Forms.Panel panel799;
        private System.Windows.Forms.Panel panel800;
        private System.Windows.Forms.Panel panel801;
        private System.Windows.Forms.Panel panel802;
        private System.Windows.Forms.Panel panel803;
        private System.Windows.Forms.Panel panel804;
        private System.Windows.Forms.Panel panelPage2Header2;
        private System.Windows.Forms.Panel panelPage2Header1;
        private System.Windows.Forms.Label labelPage2;
        private System.Windows.Forms.Panel panelPrintHeader;
        private System.Windows.Forms.Label labelPage1;
        private System.Windows.Forms.Panel panel56;
        private System.Windows.Forms.Panel panel58;
        private System.Windows.Forms.Label labelCenter2;
        private System.Windows.Forms.Panel panel59;
        private System.Windows.Forms.Label labelCenter1;
        private System.Windows.Forms.PictureBox pictureBoxCenter;
        private System.Windows.Forms.Panel panelCardiacEVentReportNumber;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel50;
        private System.Windows.Forms.Panel panelDateTimeofEventStrip;
        private System.Windows.Forms.Panel panel53;
        private System.Windows.Forms.Panel panelHorSepPatDet;
        private System.Windows.Forms.Panel panelStudyProcedures;
        private System.Windows.Forms.Panel panelEventsStat;
        private System.Windows.Forms.Panel panelLineUnderSampl1;
        private System.Windows.Forms.Panel panelSignatures;
        private System.Windows.Forms.Panel panel110;
        private System.Windows.Forms.Label labelReportSignDate;
        private System.Windows.Forms.Panel panel109;
        private System.Windows.Forms.Label labelDateReportSign;
        private System.Windows.Forms.Panel panel108;
        private System.Windows.Forms.Label labelPhysSignLine;
        private System.Windows.Forms.Panel panel107;
        private System.Windows.Forms.Label labelPhysSign;
        private System.Windows.Forms.Panel panel106;
        private System.Windows.Forms.Label labelPhysPrintName;
        private System.Windows.Forms.Panel panel105;
        private System.Windows.Forms.Label labelPhysNameReportPrint;
        private System.Windows.Forms.Panel panel51;
        private System.Windows.Forms.Label labelPage;
        private System.Windows.Forms.Label labelPage1of;
        private System.Windows.Forms.Label labelPageOf;
        private System.Windows.Forms.Label labelPage1xOfX;
        private System.Windows.Forms.Label labelPrintDate1Bottom;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panelPrintArea1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.Panel panel101;
        private System.Windows.Forms.Panel panelPage2Header;
        private System.Windows.Forms.Panel panel500;
        private System.Windows.Forms.Label labelPage3;
        private System.Windows.Forms.Panel panel501;
        private System.Windows.Forms.Panel panel503;
        private System.Windows.Forms.Label labelCenter2p3;
        private System.Windows.Forms.Panel panel504;
        private System.Windows.Forms.Label labelCenter1p3;
        private System.Windows.Forms.Panel panel505;
        private System.Windows.Forms.PictureBox pictureBoxCenter3;
        private System.Windows.Forms.Panel panelGridTableFill;
        private System.Windows.Forms.Panel panelGridTable;
        private System.Windows.Forms.Panel panel37;
        private System.Windows.Forms.Panel panel40;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Panel panelDiagnosis;
        private System.Windows.Forms.Panel panel69;
        private System.Windows.Forms.Panel panel158;
        private System.Windows.Forms.Label labelDiagnosis;
        private System.Windows.Forms.Panel panelPhysicianConclusion;
        private System.Windows.Forms.Panel panel72;
        private System.Windows.Forms.Label labelConclusionText;
        private System.Windows.Forms.Panel panel75;
        private System.Windows.Forms.Panel panel82;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Label labelPage3ReportNr;
        private System.Windows.Forms.Label labelReportText3;
        private System.Windows.Forms.Label labelPage2ReportNr;
        private System.Windows.Forms.Label labelReportText2;
        private System.Windows.Forms.Panel panel120;
        private System.Windows.Forms.Panel panel121;
        private System.Windows.Forms.Panel panel124;
        private System.Windows.Forms.Panel panel126;
        private System.Windows.Forms.PictureBox pictureBoxCenter2;
        private System.Windows.Forms.Label labelNrStripsTable;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label labelStudyNr;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label labelPage2StudyNumber;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelPage3StudyNumber;
        private System.Windows.Forms.Label labelPrintDate3Bottom;
        private System.Windows.Forms.Panel panelEventSample5;
        private System.Windows.Forms.Panel panel96;
        private System.Windows.Forms.Panel panel98;
        private System.Windows.Forms.TextBox textBoxActivities5;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Panel panel117;
        private System.Windows.Forms.TextBox textBoxSymptoms5;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Panel panel92;
        private System.Windows.Forms.TextBox textBoxFindings5;
        private System.Windows.Forms.Panel panel97;
        private System.Windows.Forms.Panel panel113;
        private System.Windows.Forms.Label labelQtUnit5;
        private System.Windows.Forms.Label labelQrsUnit5;
        private System.Windows.Forms.Panel panel112;
        private System.Windows.Forms.Label labelQtValue5;
        private System.Windows.Forms.Label labelQrsValue5;
        private System.Windows.Forms.Panel panel111;
        private System.Windows.Forms.Label labelQtText5;
        private System.Windows.Forms.Label labelQrsText5;
        private System.Windows.Forms.Panel panel102;
        private System.Windows.Forms.Label labelPrUnit5;
        private System.Windows.Forms.Label labelMeanHrUnit5;
        private System.Windows.Forms.Panel panel93;
        private System.Windows.Forms.Label labelPrValue5;
        private System.Windows.Forms.Label labelMeanHrValue5;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label labelPrText5;
        private System.Windows.Forms.Label labelMeanHrText5;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Panel panel99;
        private System.Windows.Forms.Panel panel100;
        private System.Windows.Forms.Panel panel153;
        private System.Windows.Forms.Panel panel290;
        private System.Windows.Forms.Panel panel291;
        private System.Windows.Forms.Label labelSample5LeadFull;
        private System.Windows.Forms.PictureBox pictureBoxSample5Full;
        private System.Windows.Forms.Panel panel298;
        private System.Windows.Forms.Panel panel378;
        private System.Windows.Forms.Panel panel421;
        private System.Windows.Forms.Panel panel422;
        private System.Windows.Forms.Label labelSample5Lead;
        private System.Windows.Forms.PictureBox pictureBoxSample5;
        private System.Windows.Forms.Panel panel453;
        private System.Windows.Forms.Label labelSample5Date;
        private System.Windows.Forms.Label label142;
        private System.Windows.Forms.Label labelSample5Time;
        private System.Windows.Forms.Label label144;
        private System.Windows.Forms.Label labelSampleType5;
        private System.Windows.Forms.Label label147;
        private System.Windows.Forms.Label labelSample5EventNr;
        private System.Windows.Forms.Label labelEvent5;
        private System.Windows.Forms.Label labelClassification5;
        private System.Windows.Forms.Label labelSample5Nr;
        private System.Windows.Forms.Panel panel468;
        private System.Windows.Forms.Panel panel64;
        private System.Windows.Forms.Panel panelEventSample4;
        private System.Windows.Forms.Panel panel114;
        private System.Windows.Forms.Panel panel94;
        private System.Windows.Forms.TextBox textBoxActivities4;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Panel panel63;
        private System.Windows.Forms.TextBox textBoxSymptoms4;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.TextBox textBoxFindings4;
        private System.Windows.Forms.Panel panel95;
        private System.Windows.Forms.Panel panel49;
        private System.Windows.Forms.Label labelQtUnit4;
        private System.Windows.Forms.Label labelQrsUnit4;
        private System.Windows.Forms.Panel panel48;
        private System.Windows.Forms.Label labelQtValue4;
        private System.Windows.Forms.Label labelQrsValue4;
        private System.Windows.Forms.Panel panel43;
        private System.Windows.Forms.Label labelQtText4;
        private System.Windows.Forms.Label labelQrsText4;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.Label labelPrUnit4;
        private System.Windows.Forms.Label labelMeanHrUnit4;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Label labelPrValue4;
        private System.Windows.Forms.Label labelMeanHrValue4;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Label labelPrText4;
        private System.Windows.Forms.Label labelMeanHrText4;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Panel panel115;
        private System.Windows.Forms.Panel panel116;
        private System.Windows.Forms.Panel panel266;
        private System.Windows.Forms.Panel panel275;
        private System.Windows.Forms.PictureBox pictureBoxSample4Full;
        private System.Windows.Forms.Label labelSample4LeadFull;
        private System.Windows.Forms.Panel panel305;
        private System.Windows.Forms.Panel panel306;
        private System.Windows.Forms.Panel panel328;
        private System.Windows.Forms.Panel panel329;
        private System.Windows.Forms.Label labelSample4Lead;
        private System.Windows.Forms.Label QtUnit4;
        private System.Windows.Forms.Label QtText4;
        private System.Windows.Forms.Label QrsUnit4;
        private System.Windows.Forms.Label QrsText4;
        private System.Windows.Forms.Label lPrUnit4;
        private System.Windows.Forms.Label PrText4;
        private System.Windows.Forms.Label MeanHrUnit4;
        private System.Windows.Forms.Label MeanHrText4;
        private System.Windows.Forms.PictureBox pictureBoxSample4;
        private System.Windows.Forms.Panel panel357;
        private System.Windows.Forms.Label labelSample4Date;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.Label labelSample4Time;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.Label labelSampleType4;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.Label labelSample4EventNr;
        private System.Windows.Forms.Label labelEvent4;
        private System.Windows.Forms.Label labelClassification4;
        private System.Windows.Forms.Label labelSample4Nr;
        private System.Windows.Forms.Panel panel35;
        private System.Windows.Forms.Panel panelEventSample3;
        private System.Windows.Forms.Panel panel255;
        private System.Windows.Forms.Panel panelActivities3;
        private System.Windows.Forms.TextBox textBoxActivities3;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Panel panelSymptoms3;
        private System.Windows.Forms.TextBox textBoxSymptoms3;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Panel panelfindings3;
        private System.Windows.Forms.TextBox textBoxFindings3;
        private System.Windows.Forms.Panel panel270;
        private System.Windows.Forms.Panel panel273;
        private System.Windows.Forms.Panel panelQRSmeasurements3;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Label labelQtUnit3;
        private System.Windows.Forms.Label labelQrsUnit3;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Label labelQtValue3;
        private System.Windows.Forms.Label labelQrsValue3;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Label labelQtText3;
        private System.Windows.Forms.Label labelQrsText3;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Label labelPrUnit3;
        private System.Windows.Forms.Label labelMeanHrUnit3;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label labelPrValue3;
        private System.Windows.Forms.Label labelMeanHrValue3;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label labelPrText3;
        private System.Windows.Forms.Label labelMeanHrText3;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Panel panel277;
        private System.Windows.Forms.Panel panel283;
        private System.Windows.Forms.PictureBox pictureBoxSample3Full;
        private System.Windows.Forms.Panel panel300;
        private System.Windows.Forms.Panel panel384;
        private System.Windows.Forms.Panel panel387;
        private System.Windows.Forms.Panel panel388;
        private System.Windows.Forms.PictureBox pictureBoxSample3;
        private System.Windows.Forms.Panel panel416;
        private System.Windows.Forms.Label labelSample3Date;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label labelSampleType3;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label labelSample3EventNr;
        private System.Windows.Forms.Label labelSample3Time;
        private System.Windows.Forms.Label labelEvent3;
        private System.Windows.Forms.Label labelClassification3;
        private System.Windows.Forms.Label labelSample3Nr;
        private System.Windows.Forms.Panel panel251;
        private System.Windows.Forms.Panel panel137;
        private System.Windows.Forms.Label labelRhythms5;
        private System.Windows.Forms.Label labelFindingsText5;
        private System.Windows.Forms.Panel panel119;
        private System.Windows.Forms.Label labelRhythms4;
        private System.Windows.Forms.Label labelFindingsText4;
        private System.Windows.Forms.Panel panel118;
        private System.Windows.Forms.Label labelRhythms3;
        private System.Windows.Forms.Label labelFindingsText3;
        private System.Windows.Forms.Panel panel78;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label labelPatID;
        private System.Windows.Forms.Label labelSerialNr;
        private System.Windows.Forms.Label labelStartDate;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label labelPatIDHeader;
        private System.Windows.Forms.Label labelSerialNrHeader;
        private System.Windows.Forms.Label labelStartDateHeader;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Panel panel34;
        private System.Windows.Forms.Label labelPhone2;
        private System.Windows.Forms.Label labelPhone1;
        private System.Windows.Forms.Label labelCountry;
        private System.Windows.Forms.Label labelZipCity;
        private System.Windows.Forms.Label labelAddress;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label labelAddressHeader;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Label labelPatientLastName;
        private System.Windows.Forms.Label labelPatientFirstName;
        private System.Windows.Forms.Label labelDateOfBirth;
        private System.Windows.Forms.Label labelAgeGender;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label labelDOBheader;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Panel panelHartRate;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel753;
        private System.Windows.Forms.Panel panel756ST;
        private System.Windows.Forms.Panel panel755;
        private System.Windows.Forms.Panel panel754;
        private System.Windows.Forms.Panel panel759;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartSummaryBar;
        private System.Windows.Forms.Panel panel758;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Panel panelPhysInfo;
        private System.Windows.Forms.Panel panel67;
        private System.Windows.Forms.Panel panel258;
        private System.Windows.Forms.Label labelClientName;
        private System.Windows.Forms.Label labelClientTel;
        private System.Windows.Forms.Panel panel256;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label labelClientHeader;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.Panel panelVert9;
        private System.Windows.Forms.Panel panel252;
        private System.Windows.Forms.Label labelRefPhysicianName;
        private System.Windows.Forms.Label labelRefPhysicianTel;
        private System.Windows.Forms.Panel panel250;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label labelRefPhysHeader;
        private System.Windows.Forms.Panel panelVert8;
        private System.Windows.Forms.Panel panel244;
        private System.Windows.Forms.Label labelPhysicianName;
        private System.Windows.Forms.Label labelPhysicianTel;
        private System.Windows.Forms.Panel panel242;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label labelPhysHeader;
        private System.Windows.Forms.Panel panelVert6;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.Panel panel36;
        private System.Windows.Forms.Panel panel74;
        private System.Windows.Forms.Label labelUser;
        private System.Windows.Forms.Label labelTechText;
        private System.Windows.Forms.Panel panel45;
        private System.Windows.Forms.Label labelDayNight;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel771;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label labelAutoManual;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.Panel panel770;
        private System.Windows.Forms.Panel panel769;
        private System.Windows.Forms.Label labelOtherMax;
        private System.Windows.Forms.Label labelOtherMin;
        private System.Windows.Forms.Label labelOtherN;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Panel panel146;
        private System.Windows.Forms.Label labelVtMax;
        private System.Windows.Forms.Label labelVtMin;
        private System.Windows.Forms.Label labelVtN;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Panel panel85;
        private System.Windows.Forms.Label labelPaceMax;
        private System.Windows.Forms.Label labelPaceMin;
        private System.Windows.Forms.Label labelPaceN;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Panel panel80;
        private System.Windows.Forms.Label labelPvcMax;
        private System.Windows.Forms.Label labelPvcMin;
        private System.Windows.Forms.Label labelPvcN;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Panel panel79;
        private System.Windows.Forms.Label labelPacMax;
        private System.Windows.Forms.Label labelPacMin;
        private System.Windows.Forms.Label labelPacN;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Panel panel38;
        private System.Windows.Forms.Label labelAFlutMax;
        private System.Windows.Forms.Label labelAFlutMin;
        private System.Windows.Forms.Label labelAFlutN;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Panel panel768;
        private System.Windows.Forms.Label labelAfibMax;
        private System.Windows.Forms.Label labelAfibMin;
        private System.Windows.Forms.Label labelAfibN;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.Panel panel767;
        private System.Windows.Forms.Label labelPauseMax;
        private System.Windows.Forms.Label labelPauseMin;
        private System.Windows.Forms.Label labelPauseN;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.Panel panel766;
        private System.Windows.Forms.Label labelBradyMax;
        private System.Windows.Forms.Label labelBradyMin;
        private System.Windows.Forms.Label labelBradyN;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.Panel panel765;
        private System.Windows.Forms.Label labelTachyMax;
        private System.Windows.Forms.Label labelTachyMin;
        private System.Windows.Forms.Label labelTachyN;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.Panel panel764;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel86;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Panel panel41;
        private System.Windows.Forms.Label labelEndStudyDate;
        private System.Windows.Forms.Label labelEndStudyText;
        private System.Windows.Forms.Label labelCardiacEventReportNr;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Panel panel39;
        private System.Windows.Forms.Label labelNormMax;
        private System.Windows.Forms.Label labelNormMin;
        private System.Windows.Forms.Label labelNormN;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.ToolStripLabel toolStripMemUsage;
        private System.Windows.Forms.Label labelSingleEventReportDate;
        private System.Windows.Forms.Label labelPrintDateHeader;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Panel panel54;
        private System.Windows.Forms.Label labelHrMaxPage;
        private System.Windows.Forms.Label labelHrMinPage;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Panel panel52;
        private System.Windows.Forms.Label labelHrMaxIX;
        private System.Windows.Forms.Label labelHrMinIX;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Panel panel47;
        private System.Windows.Forms.Panel panel46;
        private System.Windows.Forms.Label labelHrMean;
        private System.Windows.Forms.Label labelHrMax;
        private System.Windows.Forms.Label labelHrMin;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Panel panelRowInterpretation;
        private System.Windows.Forms.Panel panel89;
        private System.Windows.Forms.Label labelSummaryText;
        private System.Windows.Forms.Panel panel91;
        private System.Windows.Forms.Panel panel90;
        private System.Windows.Forms.Label labelReportFindings;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Label labelPage2PatientID;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label labelPage2DateOfBirth;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Panel panel819;
        private System.Windows.Forms.Panel panel821;
        private System.Windows.Forms.Label labelPage2PatLastName;
        private System.Windows.Forms.Panel panel826;
        private System.Windows.Forms.Panel panel828;
        private System.Windows.Forms.Label labelCenter2p2;
        private System.Windows.Forms.Panel panel829;
        private System.Windows.Forms.Label labelCenter1p2;
        private System.Windows.Forms.Panel panel830;
        private System.Windows.Forms.Panel panel533;
        private System.Windows.Forms.Panel panel531;
        private System.Windows.Forms.Label labelPage3PatientID;
        private System.Windows.Forms.Label labelPatientID;
        private System.Windows.Forms.Panel panel522;
        private System.Windows.Forms.Panel panel520;
        private System.Windows.Forms.Label labelPage3PatLastName;
        private System.Windows.Forms.Label labelPatName;
        private System.Windows.Forms.Label labelDateOfBirthPage2;
        private System.Windows.Forms.Panel panel527;
        private System.Windows.Forms.Panel panel525;
        private System.Windows.Forms.Panel panel122;
        private System.Windows.Forms.Panel panel125;
        private System.Windows.Forms.Panel panel123;
        private System.Windows.Forms.Label labelPage3DateOfBirth;
        private System.Windows.Forms.Label labelSample5Mid;
        private System.Windows.Forms.Label labelSample3Ampl;
        private System.Windows.Forms.Label labelSample3Sweep;
        private System.Windows.Forms.Label labelSample3End;
        private System.Windows.Forms.Label labelSample3MidFull;
        private System.Windows.Forms.Label labelSample3StartFull;
        private System.Windows.Forms.Label labelSample3Mid;
        private System.Windows.Forms.Label labelSample3Start;
        private System.Windows.Forms.Label labelSample3AmplFull;
        private System.Windows.Forms.Label labelSample3SweepFull;
        private System.Windows.Forms.Label labelSample3EndFull;
        private System.Windows.Forms.Label labelSample4Ampl;
        private System.Windows.Forms.Label labelSample4Sweep;
        private System.Windows.Forms.Label labelSample4End;
        private System.Windows.Forms.Label labelSample4Mid;
        private System.Windows.Forms.Label labelSample4Start;
        private System.Windows.Forms.Label labelSample4EndFull;
        private System.Windows.Forms.Label labelSample5Start;
        private System.Windows.Forms.Label labelSample4AmplFull;
        private System.Windows.Forms.Label labelSample4SweepFull;
        private System.Windows.Forms.Label labelSample4MidFull;
        private System.Windows.Forms.Label labelSample4StartFull;
        private System.Windows.Forms.Label labelSample5AmplFull;
        private System.Windows.Forms.Label labelSample5SweepFull;
        private System.Windows.Forms.Label labelSample5EndFull;
        private System.Windows.Forms.Label labelSample5MidFull;
        private System.Windows.Forms.Label labelSample5StartFull;
        private System.Windows.Forms.Label labelSample5Ampl;
        private System.Windows.Forms.Label labelSample5Sweep;
        private System.Windows.Forms.Label labelSample5End;
        private System.Windows.Forms.Label labelRoom;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnEventDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnSampleNr;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnFindings;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnRemarks;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label labelAbnMax;
        private System.Windows.Forms.Label labelAbnMin;
        private System.Windows.Forms.Label labelAbnN;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label labelReportNr;
        private System.Windows.Forms.Panel panel28;
    }
}