﻿#define PLOT_SAMPLES
using Event_Base;
using EventBoard.EntryForms;
using EventBoard.Event_Base;
using EventboardEntryForms;
using PdfSharp;
using PdfSharp.Pdf;
using Program_Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace EventBoard
{
 
    public partial class CPrintStudyEventsForm : Form
    {
        private enum DPrintStudyTableVars
        {
            ReportNr, EventNr, EventDateTime, AnalysisNr, EventMA,
            Classification, Rhythm, Findings, Symptoms, HrMin, HrMean, HrMax,
            Technician, StripOnPage
        };

        CPreviousFindingsForm mFindingsForm = null;
        DataGridView _mFindingsDatagridView = null;
        public CStudyReport _mStudyReport = null;
        public CStudyInfo _mStudyInfo = null;
        public CPatientInfo _mPatientInfo = null;
        public CDeviceInfo _mDeviceInfo = null;

        public string _mCreadedByInitials;
        bool _mbAnonymize;

#if PLOT_SAMPLES
        UInt32 mRecordIndex = 0;
        UInt16 mRecordEventNr = 0;
        UInt32 mAnalysisIndex = 0;
        UInt16 mAnalysisNr = 0;
        CRecordMit mRecordDb = null;
        CRecordMit mRecordFile = null;
        CRecAnalysis mRecAnalysis = null;
       CRecAnalysisFile mRecAnalysisFile = null;
#endif

        List<Bitmap> mPrintBitmapList = null;

        UInt16 mNrPages = 0;
        UInt16 mNrSamples = 0;
        UInt16 mCurrentSample = 0;
        UInt16 mCurrentPrintPage = 0;
        const UInt16 _cNrSamplesFirstPage = 0;
        const UInt16 _cNrSamplesPerPage = 3;
        private bool mbFirstPage = true;
        private bool mbPrintTable = true;
        private int mPrintTableN = 1;


        bool mbPrintEventStat = true;
        bool mbPrintEventHR = true;

        DateTime _mStartPeriodDT;
        DateTime _mEndPeriodDT;
        bool _mbReduceImage = false;

        public UInt16 _mStudyReportNr = 0;
        public UInt32 _mReportIX = 0;

        private bool _mbHideReportNr = false;
        private bool _mbHideReportDate = false;
        private bool _mbPlot2Ch = false;
        private bool _mbBlackWhite = false;
        private UInt32 _mTestOffset = 0;
        private bool _mbQCd = false;

        private bool _mbWarnEmptyBmp = true;

        //        CRecordMit _mBaseLineRecordDb;
        //        CRecordMit _mBaseLineRecordFile;
        //        CRecAnalysis _mBaseLineRecAnalysis;
        //        CRecAnalysisFile _mBaseLineRecAnalysisFile;
        //CMeasureStrip _mBaseLineStrip;

        /*        public static bool sStoreBaseLine(CRecordMit ARecordDb, CRecordMit ARecordFile, CRecAnalysis ARecAnalysis, CRecAnalysisFile ARecAnalysisFile, CMeasureStrip AStrip)
                {
                    bool bOk = false;

                    if(ARecordDb != null && ARecordFile != null && ARecAnalysis != null && ARecAnalysisFile != null && AStrip != null)
                    {
                        _mBaseLineRecordDb = ARecordDb;
                        _mBaseLineRecordFile = ARecordFile;
                        _mBaseLineRecAnalysis = ARecAnalysis;
                        _mBaseLineRecAnalysisFile = ARecAnalysisFile;
                        _mBaseLineStrip = AStrip;
                        bOk = true;
                    }
                    return bOk;
                }
        */
        public CPrintStudyEventsForm(CPreviousFindingsForm AFindingsForm, DataGridView ADatagridView, bool AbSmall, string ACreadedByInitials, 
            DateTime AStartPeriodDT, DateTime AEndPeriodDT, bool AbReduceImage, bool AbPlot2Ch, bool AbAnonymize, bool AbPrintBlackWhite, bool AbQCd)
        {
            bool bOk = false;

            try
            {
                mFindingsForm = AFindingsForm;
                _mCreadedByInitials = ACreadedByInitials;
                _mStartPeriodDT = AStartPeriodDT;
                _mEndPeriodDT = AEndPeriodDT;
                _mbReduceImage = AbReduceImage;
                _mbPlot2Ch = AbPlot2Ch;
                _mbHideReportNr = Properties.Settings.Default.HideReportNr;
                _mbHideReportDate = Properties.Settings.Default.HideReportDate;

                mPrintBitmapList = new List<Bitmap>();
                _mbAnonymize = AbAnonymize;
                _mbBlackWhite = AbPrintBlackWhite;
                _mTestOffset = 0;
                _mbQCd = AbQCd;

                if (_mStartPeriodDT == DateTime.MaxValue || _mStartPeriodDT == DateTime.MinValue) _mStartPeriodDT = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Local);
                if (_mEndPeriodDT == DateTime.MaxValue || _mEndPeriodDT == DateTime.MinValue) _mEndPeriodDT = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Local);

                if (mFindingsForm != null && ADatagridView != null && mPrintBitmapList != null)
                {
                    _mFindingsDatagridView = ADatagridView;
                    _mStudyReport = mFindingsForm._mStudyReport;
                    _mStudyInfo = mFindingsForm._mStudyInfo;
                    _mPatientInfo = mFindingsForm._mPatientInfo;
                    _mDeviceInfo = new CDeviceInfo(CDvtmsData.sGetDBaseConnection());
#if PLOT_SAMPLES
                    mRecordDb = new CRecordMit(CDvtmsData.sGetDBaseConnection());
                     mRecordFile = new CRecordMit(CDvtmsData.sGetDBaseConnection());
                    mRecAnalysis = null;
                     mRecAnalysisFile = new CRecAnalysisFile();
                    if(mRecordDb == null || mRecordFile == null  /*|| mRecAnalysis == null*/ || mRecAnalysisFile == null)
                    {
                        bOk = false;
                    }
                    else
#endif
                    if (_mStudyReport != null && _mStudyInfo != null && _mPatientInfo != null && _mDeviceInfo != null  )
                    {
                        bOk = true;
                    }
                }
                //mRecordIndex = AFindingsForm.mRecordIndex;
                //            mAnalysisIndex = AAnalyzeForm.mAna;
                //mAnalysisNr = AFindingsForm.mAnalysisSeqNr;
                // mRecordDb = AFindingsForm.mRecordDb;
                //mRecordFile = AFindingsForm.mRecordFile;
                //mRecAnalysis = AFindingsForm.mRecAnalysis;
                //mRecAnalysisFile = AFindingsForm.mRecAnalysisFile;
                //            _mBaseLineRecordDb = AAnalyzeForm._mBaseLineRecordDb;
                //            _mBaseLineRecordFile = AAnalyzeForm._mBaseLineRecordFile;
                //            _mBaseLineRecAnalysis = AAnalyzeForm._mBaseLineRecAnalysis;
                //            _mBaseLineRecAnalysisFile = AAnalyzeForm._mBaseLineRecAnalysisFile;
                //            _mBaseLineStrip = AAnalyzeForm.;


                if (bOk)
                {
                    InitializeComponent();

                    mbPrintEventStat = Properties.Settings.Default.PrintEventStat;
                    mbPrintEventHR = Properties.Settings.Default.PrintEventHR;


                    panelHartRate.Visible = mbPrintEventHR;
                    panelEventsStat.Visible = mbPrintEventStat;

//                    labelTextQC1.Text = _mbQCd ? "QC" : "";
  //                  labelTextQC2.Text = labelTextQC1.Text;

                    string strFindings = _mbQCd ? "QC Findings:" : "Findings:";

                    labelFindingsText3.Text = strFindings;
                    labelFindingsText4.Text = strFindings;
                    labelFindingsText5.Text = strFindings;

                    _mDeviceInfo.mbDoSqlSelectIndex(_mStudyInfo._mStudyRecorder_IX, _mDeviceInfo.mMaskValid);

                    if (AbSmall)
                    {
                        Width = 333;
                        Height = 85;
                    }

                    toolStrip1.Focus();
                    //            toolStripButtonPrint.

                    string reportStr = "";

                    _mStudyReportNr = 0;

                    if (false == _mbAnonymize && _mStudyInfo != null)
                    {
                        if (_mStudyInfo.mbGetNextReportNr(out _mStudyReportNr))
                        {
                            if (false == _mbHideReportNr)
                            {
                                reportStr = _mStudyReportNr.ToString();
                            }
                        }
                    }
                    CProgram.sbLogMemSize("opening Study print S" + _mStudyInfo.mIndex_KEY.ToString() + " report "+ _mStudyReportNr.ToString());

                    if (_mStudyReportNr == 0)
                    {
                        labelReportText2.Text = "";
                        labelReportText3.Text = "";
                    }
                    labelReportNr.Text = reportStr;
                    labelPage2ReportNr.Text = reportStr;
                    labelPage3ReportNr.Text = reportStr;

                    mNrSamples = mFindingsForm._mSelectedStripN; // mRecAnalysisFile.mCountActiveStrips();
                    mNrPages = (UInt16)(mNrSamples == 0 ? 2 : 3 + (mNrSamples - 1) / _cNrSamplesPerPage);

                    int nEstimate = mNrPages + 1;
                    string sEstimate = "Print Study Report ~" + nEstimate.ToString() + " pages";
                    Text = sEstimate;

                    if (nEstimate > 15)
                    {
                        if (false == CProgram.sbAskOkCancel("Print Study Report", "Print " + nEstimate.ToString() + " pages, continue?"))
                        {
                            Close();
                            return;
                        }
                    }

                    mSetFormValues();
                    mUpdateMemUse();

                    BringToFront();
                }
                else
                {
                    CProgram.sPromptError(false, "PrintStudyEventForm", "Form not initialized");
                    Close();
                }

            }
            catch (Exception ex)
            {
                CProgram.sLogException("CPrintStudyEventsForm() failed to init", ex);
                Close();
            }

        }

        ~CPrintStudyEventsForm()
        {
            if(mPrintBitmapList != null)
            {
                while (mPrintBitmapList.Count > 0)
                {
                    Bitmap bitmap = mPrintBitmapList[0];

                    mPrintBitmapList.Remove(bitmap);
                    bitmap.Dispose();
                    bitmap = null;
                }
            }
        }

        public void mSetFormValues()
        {
            try
            {
                labelPage1.Text = "";
                labelPage2.Text = "";
                labelPage3.Text = "";

                Image img = CDvtmsData.sGetPrintCenterImage(_mbBlackWhite);
                pictureBoxCenter.Image = img;
                pictureBoxCenter2.Image = img;
                pictureBoxCenter3.Image = img;
                string s = CDvtmsData.sGetPrintCenterLine1();

                labelCenter1.Text = s;
                labelCenter1p2.Text = s;
                labelCenter1p3.Text = s;

                s = CDvtmsData.sGetPrintCenterLine2();
                labelCenter2.Text = s;
                labelCenter2p2.Text = s;
                labelCenter2p3.Text = s;
                
 /*               if (mRecordDb == null || mRecordFile == null || mRecAnalysis == null || mRecAnalysisFile == null)
                {
                    CProgram.sPromptError(false, "PrintAnalysis", "Form not initialized");
                    Close();
                }
                else
*/                {

                    mNrSamples = mFindingsForm._mSelectedStripN; // mRecAnalysisFile.mCountActiveStrips();
                    mNrPages = (UInt16)( mNrSamples == 0 ? 2 : 3 + ( mNrSamples - 1 ) / _cNrSamplesPerPage);
                    mCurrentPrintPage = (UInt16)(mNrSamples == 0 ? 2 : 3 );
                    //test mNrPages = 2;

                 
                    labelPage1xOfX.Text = mNrPages.ToString();
                    labelPage2xOfX.Text = mNrPages.ToString();
                    labelPage3xOfX.Text = mNrPages.ToString();

                    mInitHeader();
                    mInitTable();
                    mInitStats();   // needs table values

                    //                    mBaseLine();
                    //                    mInitPrevious();
                    //                    mInitCurrent();
                    //                    mInitSample1();
                    //                    mInitStrip1();
                    //                    mInitSample2();
                    mInitFooter();
                    mInitSample3();
                    mInitSample4();
                    mInitSample5();
                    //mInitSample6(3);
                    // to do multi sample pages
                }

                toolStrip1.Focus();
            }
            catch ( Exception ex )
            {
                CProgram.sLogException("Set Study Print Form  error", ex);
            }
        }

        void mInitHeader()
        {
            try
            {
                DateTime dt;
                string s;
                bool bStudy = false == _mbAnonymize && _mStudyInfo != null && _mStudyInfo.mIndex_KEY > 0;
                bool bPatient = false == _mbAnonymize && _mPatientInfo != null && _mPatientInfo.mIndex_KEY > 0;

 
                string reportTitle = "Study Report";

                if ((_mStudyReport._mReportSectionMask & (1 << (int)DReportSection.Weekly)) != 0)
                {
                    reportTitle = "Weekly Event Report";

                    string userTitle = Properties.Settings.Default.PrintReportWeek;
                    if (userTitle != null && userTitle.Length > 1)
                    {
                        reportTitle = userTitle;
                    }
                }
                else if ((_mStudyReport._mReportSectionMask & (1 << (int)DReportSection.EndOfStudy)) != 0)
                {
                    reportTitle = "End of Study Report";

                    string userTitle = Properties.Settings.Default.PrintReportEoS;
                    if (userTitle != null && userTitle.Length > 1)
                    {
                        reportTitle = userTitle;
                    }
                }
                else if ((_mStudyReport._mReportSectionMask & (1 << (int)DReportSection.Daily)) != 0)
                {
                    reportTitle = "Daily Event Report";

                    string userTitle = Properties.Settings.Default.PrintReportDay;
                    if (userTitle != null && userTitle.Length > 1)
                    {
                        reportTitle = userTitle;
                    }
                }
                else if ((_mStudyReport._mReportSectionMask & (1 << (int)DReportSection.Hour)) != 0)
                {
                    reportTitle = "Hourly Event Report";

                    string userTitle = Properties.Settings.Default.PrintReportHour;
                    if (userTitle != null && userTitle.Length > 1)
                    {
                        reportTitle = userTitle;
                    }
                }
                else if ((_mStudyReport._mReportSectionMask & (1 << (int)DReportSection.Period)) != 0)
                {
                    reportTitle = "Period Event Report";

                    string userTitle = Properties.Settings.Default.PrintReportPeriod;
                    if (userTitle != null && userTitle.Length > 1)
                    {
                        reportTitle = userTitle;
                    }
                }

                string reportNr = "";

                if( bStudy && false == _mbHideReportNr)
                {
                    reportNr = _mStudyReportNr.ToString();
                }

                labelCardiacEventReportNr.Text = reportTitle + " "+ reportNr; 

                if(_mbHideReportNr)
                {
                    labelReportText2.Text = "";
                    labelReportText3.Text = "";
                }

                // labelReportNr.Text = _mStudyReport._mReportNr == 0 ? "" : _mStudyReport._mReportNr.ToString();

                /*s = "";
                if (bStudy && _mStudyInfo._mClient_IX > 0)
                {
                    try
                    {
                        CClientInfo client = new EventboardEntryForms.CClientInfo(_mStudyInfo.mGetSqlConnection());

                        if (client != null)
                        {
                            client.mbDoSqlSelectIndex(_mStudyInfo._mClient_IX, CSqlDataTableRow.sGetMask((UInt16)DClientInfoVars.Label));
                            s = client._mLabel;
                        }

                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException("Init header Study Form error", ex);
                    }
                }
                labelClientName.Text = s; // "New York Hospital";
                                      //            labelClientHeader.Text = "CLIENT:";
                                      */
                CClientInfo.sFillPrintClientName(_mbAnonymize, _mStudyInfo, labelClientName, labelClientTel, true);

                /*
                s = "";
                if (bStudy && _mStudyInfo._mRefPhysisian_IX > 0)
                {
                    try
                    {
                        CPhysicianInfo physician = new CPhysicianInfo(_mStudyInfo.mGetSqlConnection());

                        if (physician != null)
                        {
                            physician.mbDoSqlSelectIndex(_mStudyInfo._mRefPhysisian_IX, CSqlDataTableRow.sGetMask((UInt16)DPhysicianInfoVars.Label));
                            s = physician._mLabel;
                        }
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException("Init header Study Form error", ex);
                    }
                }
                labelRefPhysicianName.Text = s; // "Dr. John Smithsonian";
                                            //labelRefPhysHeader.Text = "REFERRING PHYSICIAN:";
                                            */
                CPhysicianInfo.sFillPrintPhysicianName(_mbAnonymize, _mStudyInfo, true, labelRefPhysicianName, labelRefPhysicianTel, true);
                /*
                                s = "";
                                if (bStudy && _mStudyInfo._mPhysisian_IX > 0)
                                {
                                    try
                                    {
                                        CPhysicianInfo physician = new CPhysicianInfo(_mStudyInfo.mGetSqlConnection());

                                        if (physician != null)
                                        {
                                            physician.mbDoSqlSelectIndex(_mStudyInfo._mPhysisian_IX, CSqlDataTableRow.sGetMask((UInt16)DPhysicianInfoVars.Label));
                                            s = physician._mLabel;
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        CProgram.sLogException("Init header Study Form error", ex);
                                    }
                                }
                                labelPhysicianName.Text = s; // "Dr. John Smithsonian";
                                                         //labelPhysHeader.Text = "PHYSICIAN:";
                */
                CPhysicianInfo.sFillPrintPhysicianName(_mbAnonymize, _mStudyInfo, false, labelPhysicianName, labelPhysicianTel, true);


                s = "";
                if (_mDeviceInfo != null && _mStudyInfo._mStudyRecorder_IX > 0)
                {
                    try
                    {
                        if (_mDeviceInfo.mbDoSqlSelectIndex(_mStudyInfo._mStudyRecorder_IX, _mDeviceInfo.mMaskValid))
                        {
                            s = _mDeviceInfo._mDeviceSerialNr;
                        }
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException("Init header Study Form error", ex);
                    }
                }

                labelSerialNr.Text = s;
                //            labelSerialNrHeader.Text = "RECORDER SERIAL#:";

                labelStartDate.Text = bStudy ? CProgram.sDateToString(_mStudyInfo._mStudyStartDate) : ""; // "10/23/2016"; study start
                                                                                                          //            labelStartDateHeader.Text = "START DATE:";
                if( bStudy && (_mStudyReport._mReportSectionMask & (1 << (int)DReportSection.EndOfStudy)) != 0)
                {
                    DateTime date = _mStudyInfo._mStudyStartDate.AddDays(_mStudyInfo._mStudyMonitoringDays);
                    labelEndStudyDate.Text = CProgram.sDateToString(date);
                    labelEndStudyDate.Visible = true;
                    labelEndStudyText.Visible = true;
                }

                labelStudyNr.Text = bStudy ? CDvtmsData.sGetPrintStudyCode() + _mStudyInfo.mIndex_KEY.ToString() : "-"; // "1212212";
                labelPage2StudyNumber.Text = labelStudyNr.Text;
                labelPage3StudyNumber.Text = labelPage2StudyNumber.Text;


                string room = "";
                if (bStudy)
                {
                    room = _mStudyInfo._mHospitalRoom;
                    if (room != null && room.Length > 0 && _mStudyInfo._mHospitalBed != null && _mStudyInfo._mHospitalBed.Length > 0)
                    {
                        room += " - " + _mStudyInfo._mHospitalBed;
                    }
                }
                labelRoom.Text = room;


                labelPage2ReportNr.Text = labelReportNr.Text;
                labelPage3ReportNr.Text = labelPage2ReportNr.Text;
                //labelStudyNrHeader.Text = "STUDY#:";
                labelPatIDHeader.Text = bPatient ? _mPatientInfo.mGetPatientSocIDText()/*.ToUpper()*/ + ":" : "";
                labelPatID.Text = bPatient ? _mPatientInfo.mGetPatientSocIDValue(false) : ""/* _mPatientID.mDecrypt()*/;
                labelPage2PatientID.Text = labelPatID.Text;
                labelPage3PatientID.Text = labelPatID.Text;
                //labelPatientIDResult.Text = labelPatID.Text;

                labelPhone2.Text = bPatient ? _mPatientInfo._mPatientPhone2.mDecrypt() : ""; // "800-217-0520";
                labelPhone1.Text = bPatient ? _mPatientInfo._mPatientPhone1.mDecrypt() : ""; // "800-217-0520";
                                                                                             //labelPhoneHeader.Text = "PHONE:";
/*                labelZipCity.Text = bPatient ? (CProgram.sbGetImperial() ? _mPatientInfo._mPatientCity + ", " + _mPatientInfo._mPatientZip.mDecrypt()
                                            : _mPatientInfo._mPatientZip.mDecrypt() + "  " + _mPatientInfo._mPatientCity ) : ""; // "Texas TX 200111";
                labelAddress.Text = bPatient ? _mPatientInfo._mPatientAddress.mDecrypt() : ""; // "Alpha Street 112, Houston";
*/                                                                                               //labelAddressHeader.Text = "ADDRESS:";
                CPatientInfo.sFillPrintPatientAddress(_mbAnonymize, _mPatientInfo, labelAddress, labelZipCity, labelCountry);

  /*              int age = _mPatientInfo.mGetAgeYears();
                string ageGender = bPatient && age >= 0 ? age.ToString() : "";
                if (bPatient && _mPatientInfo._mPatientGender_IX > 0)
                {
                    ageGender += " " + CPatientInfo.sGetGenderString((DGender)_mPatientInfo._mPatientGender_IX);
                }
                labelAgeGender.Text = ageGender;  //"41, male";

                labelDateOfBirth.Text = bPatient && age >= 0 ? CProgram.sDateToString(_mPatientInfo._mPatientDateOfBirth.mDecryptToDate()) : ""; //  "06/08/1974";
 */
                CPatientInfo.sFillPrintPatientDOB(_mbAnonymize, _mPatientInfo, null, labelDateOfBirth, labelAgeGender);

                labelPage2DateOfBirth.Text = labelDateOfBirth.Text;
                labelPage3DateOfBirth.Text = labelDateOfBirth.Text;

                //            labelDOBheader.Text = "DATE OF BIRTH:";
                CPatientInfo.sFillPrintPatientName(_mbAnonymize, _mPatientInfo, null, labelPatientLastName, labelPatientFirstName);

                string fullName = bPatient ? _mPatientInfo.mGetFullName() : "";
/*
                int pos = fullName.IndexOf(',');
                string firstName = "", lastName = fullName; ;
                if (pos > 0)
                {
                    lastName = fullName.Substring(0, pos);
                    firstName = fullName.Substring(pos + 1);
                    if (firstName.StartsWith(", "))
                    {
                        firstName = firstName.Substring(2);
                    }
                }
                labelPatientFirstName.Text = firstName;  //"Rutger Alexander";
                labelPatientLastName.Text = lastName;  //"Brest van Kempen";
                                                       //            labelPatLastName.Text = fullName;
                                                       */
                labelPage2PatLastName.Text = fullName;
                labelPage3PatLastName.Text = fullName;
                //labelPatientNameHeader.Text = "PATIENT NAME:";
                dt = DateTime.Now;
                //labelSingleEventReportTime.Text = CProgram.sTimeToString(dt);// "10:15 AM";

                if (_mbHideReportDate)
                {
                    labelSingleEventReportDate.Text = "";
                    labelPrintDateHeader.Text = "";
                }
                else
                {
                    labelSingleEventReportDate.Text = CProgram.sDateTimeToString(dt);// "11/12/2016";
                }
                s = "";
                if (bStudy)
                {
                    if (CDvtmsData._sEnumListStudyProcedures != null)
                    {
                        s = CDvtmsData._sEnumListStudyProcedures.mFillString(_mStudyInfo._mStudyProcCodeSet, true, " ", true, "\r\n");
                    }
                }
                labelDiagnosis.Text = s;

                labelSummaryText.Text = _mStudyReport._mSummary.mDecrypt();

                string conclusion = _mStudyReport._mConclusion.mDecrypt();
                labelConclusionText.Text = conclusion;

                if ((conclusion == null || conclusion.Length == 0) && false == Properties.Settings.Default.PrintStudyConclusion)
                {
                    panelPhysicianConclusion.Visible = false;
                    labelReportFindings.Text = "Report Findings and Conclusion";
                }

            }
            catch (Exception ex)
            {
                CProgram.sLogException("Init header Study Form error", ex);
            }

        }

        private void mSetChartValueColor(Chart AChartBar, UInt16 APointIndex, UInt32 AValue, string AClassification)
        {
            try
            {
                DataPoint pointBar = AChartBar.Series[0].Points[APointIndex];
                Color color = CDvtmsData.sPrintClassificationColor(AClassification, _mbBlackWhite);

                pointBar.SetValueY(AValue + _mTestOffset );
                pointBar.Color = color;

            }
            catch (Exception ex)
            {
                CProgram.sLogException("PrintStudyForm() failed to set chart point " + APointIndex.ToString() + " " + AClassification, ex);

            }
        }

        private void mInitStats()
        {
            if(mFindingsForm != null )
            {
                try
                {
                    int i;
                    string eventNr, page;
                    bool b = mFindingsForm._mSelectedHrCount > 0 && mFindingsForm._mSelectedHrMin <= mFindingsForm._mSelectedHrMax;

                    labelHrMin.Text = b ? mFindingsForm._mSelectedHrMin.ToString("0") : "";
                    eventNr = page = "";
                    i = mFindingsForm._mSelectedHrMinIX;
                    if (b && i < dataGridView.Rows.Count)
                    {
                        // eventNr = dataGridView.Rows[i].Cells[(int)DPrintStudyTableVars.EventNr].Value.ToString() + " - " + dataGridView.Rows[i].Cells[(int)DPrintStudyTableVars.AnalysisNr].Value.ToString();
                        eventNr = dataGridView.Rows[i].Cells[(int)DPrintStudyTableVars.ReportNr].Value.ToString();
                        page = dataGridView.Rows[i].Cells[(int)DPrintStudyTableVars.StripOnPage].Value.ToString();
                        if( page != null && page.Length > 0 )
                        {
  //                          page = "page " + page;
                        }
                    }
                    labelHrMinIX.Text = /*i.ToString(); //*/ eventNr;
                    labelHrMinPage.Text = page;

                    labelHrMax.Text = b ? mFindingsForm._mSelectedHrMax.ToString("0") : "";
                    eventNr = page = "";
                    i = mFindingsForm._mSelectedHrMaxIX;
                    if (b && i < dataGridView.Rows.Count)
                    {
                        //                        eventNr = dataGridView.Rows[i].Cells[(int)DPrintStudyTableVars.EventNr].Value.ToString() + " - " + dataGridView.Rows[i].Cells[(int)DPrintStudyTableVars.AnalysisNr].Value.ToString();
                        eventNr = dataGridView.Rows[i].Cells[(int)DPrintStudyTableVars.ReportNr].Value.ToString();
                        page = dataGridView.Rows[i].Cells[(int)DPrintStudyTableVars.StripOnPage].Value.ToString();
                        if (page != null && page.Length > 0)
                        {
//                            page = "page " + page;
                        }
                    }
                    labelHrMaxIX.Text = /*i.ToString(); //*/ eventNr;
                    labelHrMaxPage.Text = page;

                    labelHrMean.Text = b ? mFindingsForm._mSelectedHrMean.ToString("0") : "";


                    b = mFindingsForm._mSelectedTachy > 0;
                    labelTachyN.Text = b ? mFindingsForm._mSelectedTachy.ToString() : "";
                    b &= mFindingsForm._mTachyHrMax >= mFindingsForm._mTachyHrMin;
                    labelTachyMax.Text = b ? mFindingsForm._mTachyHrMax.ToString("0") : "";
                    labelTachyMin.Text = b ? mFindingsForm._mTachyHrMin.ToString("0") : "";


                    b = mFindingsForm._mSelectedBrady > 0;
                    labelBradyN.Text = b ? mFindingsForm._mSelectedBrady.ToString() : "";
                    b &= mFindingsForm._mBradyHrMax >= mFindingsForm._mBradyHrMin;
                    labelBradyMax.Text = b ? mFindingsForm._mBradyHrMax.ToString("0") : "";
                    labelBradyMin.Text = b ? mFindingsForm._mBradyHrMin.ToString("0") : "";


                    b = mFindingsForm._mSelectedPause > 0;
                    labelPauseN.Text = b ? mFindingsForm._mSelectedPause.ToString() : "";
                    b &= mFindingsForm._mPauseHrMax >= mFindingsForm._mPauseHrMin;
                    labelPauseMax.Text = b ? mFindingsForm._mPauseHrMax.ToString("0") : "";
                    labelPauseMin.Text = b ? mFindingsForm._mPauseHrMin.ToString("0") : "";


                    b = mFindingsForm._mSelectedAfib > 0;
                    labelAfibN.Text = b ? mFindingsForm._mSelectedAfib.ToString() : "";
                    b &= mFindingsForm._mAfibHrMax >= mFindingsForm._mAfibHrMin;
                    labelAfibMax.Text = b ? mFindingsForm._mAfibHrMax.ToString("0") : "";
                    labelAfibMin.Text = b ? mFindingsForm._mAfibHrMin.ToString("0") : "";

                    b = mFindingsForm._mSelectedAFl > 0;
                    labelAFlutN.Text = b ? mFindingsForm._mSelectedAFl.ToString() : "";
                    b &= mFindingsForm._mAflHrMax >= mFindingsForm._mAflHrMin;
                    labelAFlutMax.Text = b ? mFindingsForm._mAflHrMax.ToString("0") : "";
                    labelAFlutMin.Text = b ? mFindingsForm._mAflHrMin.ToString("0") : "";

                    b = mFindingsForm._mSelectedPAC > 0;
                    labelPacN.Text = b ? mFindingsForm._mSelectedPAC.ToString() : "";
                    b &= mFindingsForm._mPacHrMax >= mFindingsForm._mPacHrMin;
                    labelPacMax.Text = b ? mFindingsForm._mPacHrMax.ToString("0") : "";
                    labelPacMin.Text = b ? mFindingsForm._mPacHrMin.ToString("0") : "";


                    b = mFindingsForm._mSelectedPVC > 0;
                    labelPvcN.Text = b ? mFindingsForm._mSelectedPVC.ToString() : "";
                    b &= mFindingsForm._mPvcHrMax >= mFindingsForm._mPvcHrMin;
                    labelPvcMax.Text = b ? mFindingsForm._mPvcHrMax.ToString("0") : "";
                    labelPvcMin.Text = b ? mFindingsForm._mPvcHrMin.ToString("0") : "";


                    b = mFindingsForm._mSelectedPace > 0;
                    labelPaceN.Text = b ? mFindingsForm._mSelectedPace.ToString() : "";
                    b &= mFindingsForm._mPaceHrMax >= mFindingsForm._mPaceHrMin;
                    labelPaceMax.Text = b ? mFindingsForm._mPaceHrMax.ToString("0") : "";
                    labelPaceMin.Text = b ? mFindingsForm._mPaceHrMin.ToString("0") : "";


                    b = mFindingsForm._mSelectedVT > 0;
                    labelVtN.Text = b ? mFindingsForm._mSelectedVT.ToString() : "";
                    b &= mFindingsForm._mVtHrMax >= mFindingsForm._mVtHrMin;
                    labelVtMax.Text = b ? mFindingsForm._mVtHrMax.ToString("0") : "";
                    labelVtMin.Text = b ? mFindingsForm._mVtHrMin.ToString("0") : "";



                    b = mFindingsForm._mSelectedOther > 0;
                    labelOtherN.Text = b ? mFindingsForm._mSelectedOther.ToString() : "";
                    b &= mFindingsForm._mOtherHrMax >= mFindingsForm._mOtherHrMin;
                    labelOtherMax.Text = b ? mFindingsForm._mOtherHrMax.ToString("0") : "";
                    labelOtherMin.Text = b ? mFindingsForm._mOtherHrMin.ToString("0") : "";

                    b = mFindingsForm._mSelectedNorm > 0;
                    labelNormN.Text = b ? mFindingsForm._mSelectedNorm.ToString() : "";
                    b &= mFindingsForm._mNormHrMax >= mFindingsForm._mNormHrMin;
                    labelNormMax.Text = b ? mFindingsForm._mNormHrMax.ToString("0") : "";
                    labelNormMin.Text = b ? mFindingsForm._mNormHrMin.ToString("0") : "";

                    b = mFindingsForm._mSelectedAbN > 0;
                    labelAbnN.Text = b ? mFindingsForm._mSelectedAbN.ToString() : "";
                    b &= mFindingsForm._mAbNHrMax >= mFindingsForm._mAbNHrMin;
                    labelAbnMax.Text = b ? mFindingsForm._mAbNHrMax.ToString("0") : "";
                    labelAbnMin.Text = b ? mFindingsForm._mAbNHrMin.ToString("0") : "";

                    labelAutoManual.Text = mFindingsForm._mAutoActivation.ToString() + " / " + mFindingsForm._mManualActivation;
                    labelDayNight.Text = mFindingsForm._mDayActivation.ToString() + " / " + mFindingsForm._mNightActivation;


                    /*chartSummaryBar.Series[0].Points[0].SetValueY(mFindingsForm._mSelectedTachy);
                    chartSummaryBar.Series[0].Points[1].SetValueY(mFindingsForm._mSelectedBrady);
                    chartSummaryBar.Series[0].Points[2].SetValueY(mFindingsForm._mSelectedPause);
                    chartSummaryBar.Series[0].Points[3].SetValueY(mFindingsForm._mSelectedAfib);
                    chartSummaryBar.Series[0].Points[4].SetValueY(mFindingsForm._mSelectedAFl);
                    chartSummaryBar.Series[0].Points[5].SetValueY(mFindingsForm._mSelectedPAC);
                    chartSummaryBar.Series[0].Points[6].SetValueY(mFindingsForm._mSelectedPVC);
                    chartSummaryBar.Series[0].Points[7].SetValueY(mFindingsForm._mSelectedPace);
                    chartSummaryBar.Series[0].Points[8].SetValueY(mFindingsForm._mSelectedVT);

                    chartSummaryBar.Series[0].Points[9].SetValueY(mFindingsForm._mSelectedOther);
                    */
                    uint nMax = mFindingsForm._mSelectedTachy;
                    if (mFindingsForm._mSelectedBrady > nMax) nMax = mFindingsForm._mSelectedBrady;
                    if (mFindingsForm._mSelectedPause > nMax) nMax = mFindingsForm._mSelectedPause;
                    if (mFindingsForm._mSelectedAfib > nMax) nMax = mFindingsForm._mSelectedAfib;
                    if (mFindingsForm._mSelectedPAC > nMax) nMax = mFindingsForm._mSelectedPAC;
                    if (mFindingsForm._mSelectedPVC > nMax) nMax = mFindingsForm._mSelectedPVC;
                    if (mFindingsForm._mSelectedPace > nMax) nMax = mFindingsForm._mSelectedPace;
                    if (mFindingsForm._mSelectedAFl > nMax) nMax = mFindingsForm._mSelectedAFl;
                    if (mFindingsForm._mSelectedVT > nMax) nMax = mFindingsForm._mSelectedVT;
                    if (mFindingsForm._mSelectedOther > nMax) nMax = mFindingsForm._mSelectedOther;
                    if (mFindingsForm._mSelectedNorm > nMax) nMax = mFindingsForm._mSelectedNorm;
                    if (mFindingsForm._mSelectedAbN > nMax) nMax = mFindingsForm._mSelectedAbN;

                    chartSummaryBar.ChartAreas[0].AxisY.LabelStyle.Interval = nMax < 10 ? 1 : 0;
                    chartSummaryBar.ChartAreas[0].AxisY.MajorGrid.Interval = nMax < 10 ? 1 : 0;
                    chartSummaryBar.ChartAreas[0].AxisY.MajorTickMark.Interval = nMax < 10 ? 1 : 0;

  
                    mSetChartValueColor(chartSummaryBar, 0, mFindingsForm._mSelectedTachy, CDvtmsData._cFindings_Tachy);
                    mSetChartValueColor(chartSummaryBar, 1, mFindingsForm._mSelectedBrady, CDvtmsData._cFindings_Brady);
                    mSetChartValueColor(chartSummaryBar, 2, mFindingsForm._mSelectedPause, CDvtmsData._cFindings_Pause);
                    mSetChartValueColor(chartSummaryBar, 3, mFindingsForm._mSelectedAfib, CDvtmsData._cFindings_AF);
                    mSetChartValueColor(chartSummaryBar, 4, mFindingsForm._mSelectedAFl, CDvtmsData._cFindings_AFL);
                    mSetChartValueColor(chartSummaryBar, 5, mFindingsForm._mSelectedPAC, CDvtmsData._cFindings_PAC);
                    mSetChartValueColor(chartSummaryBar, 6, mFindingsForm._mSelectedPVC, CDvtmsData._cFindings_PVC);
                    mSetChartValueColor(chartSummaryBar, 7, mFindingsForm._mSelectedPace, CDvtmsData._cFindings_Pace);
                    mSetChartValueColor(chartSummaryBar, 8, mFindingsForm._mSelectedVT, CDvtmsData._cFindings_VT );
                    mSetChartValueColor(chartSummaryBar, 9, mFindingsForm._mSelectedOther, CDvtmsData._cFindings_Other);
                    mSetChartValueColor(chartSummaryBar, 10, mFindingsForm._mSelectedNorm, CDvtmsData._cFindings_Normal);
                    mSetChartValueColor(chartSummaryBar, 11, mFindingsForm._mSelectedAbN, CDvtmsData._cFindings_AbNormal);

                    chartSummaryBar.ChartAreas[0].RecalculateAxesScale();
                    chartSummaryBar.Refresh();




                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Set Analysis Print Form stats error", ex);
                }
            }
        }

        private void mInitTable()
        {
            try
            {
                if (_mFindingsDatagridView != null && _mFindingsDatagridView.Rows != null && _mFindingsDatagridView.Rows.Count > 0 )
                {
                    bool bTable, bStrip;
                    int nRow = _mFindingsDatagridView.Rows.Count;
                    DataGridViewRow gridRow;
                    UInt16 iStrip = 0;

                    labelUser.Text = FormEventBoard._sbPrintHideTechName ? "" : _mCreadedByInitials;// "SHJ";_mCreadedByInitials;
                    if (FormEventBoard._sbPrintHideTechName) labelTechText.Text = "";

                    dataGridView.DefaultCellStyle.SelectionBackColor = dataGridView.DefaultCellStyle.BackColor;
                    dataGridView.DefaultCellStyle.SelectionForeColor = dataGridView.DefaultCellStyle.ForeColor;                  

                    //IncludeTable, IncludeStrip, 
                    string reportDate, reportNr, eventNr, maStr, eventDateTime, analysisNr, analysisDate, technician, classification, findings, rhythm, symptoms, hrMin, hrMean, hrMax, iPage;

                    for (int i = 0; i < nRow; ++i)
                    {
                        gridRow = _mFindingsDatagridView.Rows[i];
                        if (gridRow != null && gridRow.Cells.Count > (int)DFindingsTableVars.AnalysisIndex)
                        {
                            bTable = gridRow.Cells[(int)DFindingsTableVars.IncludeTable].Value != null && (bool)gridRow.Cells[0].Value;
                            bStrip = gridRow.Cells[(int)DFindingsTableVars.IncludeStrip].Value != null && (bool)gridRow.Cells[1].Value;
                            if (bStrip) bTable = true;

                            if (bTable)
                            {
                                if (bStrip) ++iStrip;
                                reportDate = (string)gridRow.Cells[(int)DFindingsTableVars.ReportDate].Value;
                                reportNr = bStrip ? iStrip.ToString() : ""; // gridRow.Cells[(int)DFindingsTableVars.ReportNr].Value.ToString();
                                eventNr = (string)gridRow.Cells[(int)DFindingsTableVars.EventNr].Value;
                                maStr = (string)gridRow.Cells[(int)DFindingsTableVars.EventMA].Value;
                                eventDateTime = (string)gridRow.Cells[(int)DFindingsTableVars.EventDateTime].Value;
                                analysisNr = (string)gridRow.Cells[(int)DFindingsTableVars.AnalysisNr].Value;
                                analysisDate = (string)gridRow.Cells[(int)DFindingsTableVars.AnalysisDate].Value;
                                technician = (string)gridRow.Cells[(int)DFindingsTableVars.Technician].Value;
                                classification = (string)gridRow.Cells[(int)DFindingsTableVars.Classification].Value;
                                findings = (string)gridRow.Cells[(int)DFindingsTableVars.Findings].Value;
                                rhythm = (string)gridRow.Cells[(int)DFindingsTableVars.Rhythm].Value;
                                symptoms = (string)gridRow.Cells[(int)DFindingsTableVars.Symptoms].Value;//.ToString();
                                hrMin = (string)gridRow.Cells[(int)DFindingsTableVars.HrMin].Value;
                                hrMean = (string)gridRow.Cells[(int)DFindingsTableVars.HrMean].Value;
                                hrMax = (string)gridRow.Cells[(int)DFindingsTableVars.HrMax].Value;
                                iPage = bStrip ? (3 + iStrip / _cNrSamplesPerPage).ToString() : ""; 

                                dataGridView.Rows.Add( reportNr, eventNr, eventDateTime, analysisNr, maStr, classification,
                                    rhythm, findings, symptoms, hrMin, hrMean, hrMax,technician, iPage);
                            }
                        }
                    }
                }
                dataGridView.CurrentCell = null;
                dataGridView.ClearSelection();
                toolStrip1.Focus();

                int nRows = dataGridView.RowCount;
                int nDisplaidRows = dataGridView.DisplayedRowCount( false );
                int firstRow = dataGridView.FirstDisplayedScrollingRowIndex;

                labelNrStripsTable.Text = nRows.ToString();

                if ( firstRow + nDisplaidRows < nRows)
                {
                    // multiple pages;
                    labelPage2Of.Text = "2 A";
                }
                else
                {
                    labelPage2Of.Text = "2";
                }
            }

            catch (Exception ex)
            {
                CProgram.sLogException("Fill Print Study table error", ex);
            }
        }
    
        private bool mbLoadNextSample()  // load next strip (datgridview[mCurrentSample ]
        {
            bool bOk = false;
            string analysisStr = "";

#if PLOT_SAMPLES
            try
            {
               if (mRecordFile == null || mRecAnalysisFile == null  )
                {
                    bOk = false;
                }
                else if (mCurrentSample < mNrSamples ) //&& mCurrentSample < dataGridView.Rows.Count)
                {
                    analysisStr = _mStudyReport._mReportStripSet.mGetValue(mCurrentSample);
                    UInt32 analysisIX = 0;

                    if (analysisStr != null && analysisStr.Length > 0 && UInt32.TryParse(analysisStr, out analysisIX) && analysisIX > 0)
                    {
                        if(mRecAnalysis == null || mRecAnalysis.mIndex_KEY != analysisIX)
                        {
                            mRecAnalysis = mFindingsForm.mGetAnalysis(analysisIX);
                        }
                        if (mRecAnalysis != null && mRecAnalysis.mIndex_KEY == analysisIX)
                        {
                            mAnalysisIndex = analysisIX;
                            mAnalysisNr = mRecAnalysis._mSeqInRecording;

                            if(mRecAnalysis._mRecording_IX == 0)
                            {
                                if (mRecordFile != null)
                                {
                                    mRecordFile.mDisposeSignals();
                                }
                                mRecordDb = null;
                                mRecordFile = null;
                                if (mRecAnalysisFile != null)
                                {
                                    mRecAnalysisFile.mClearAll();
                                }
                                mRecAnalysisFile = null;
                            }
                            else if( mRecordDb == null || mRecordDb.mIndex_KEY != mRecAnalysis._mRecording_IX)
                            {
                                //mRecordDb.mbDoSqlSelectIndex(mRecAnalysis._mRecording_IX, mRecordDb.mMaskValid);
                                mRecordDb = mFindingsForm.mFindRecord(mRecAnalysis._mRecording_IX);
                            }
                            if (mRecordDb != null && mRecordDb.mIndex_KEY == mRecAnalysis._mRecording_IX)
                            {
                                mRecordIndex = mRecAnalysis._mRecording_IX;
                                mRecordEventNr = mRecordDb.mSeqNrInStudy;

                                if (mRecordFile.mIndex_KEY != mRecAnalysis._mRecording_IX)
                                {
                                    //mRecordFile // load file
                                    string casePath;
                                    string filePath;
                                    if (mRecordDb.mbGetCasePath(out casePath))
                                    {
                                        string recordDir = Path.Combine(CDvtmsData.sGetRecordingDir(), casePath);
                                        filePath = Path.Combine(recordDir, mRecordDb.mFileName);

                                        if( mRecordFile != null )
                                        {
                                            mRecordFile.mDisposeSignals();
                                        }
                                        mRecordFile = FormAnalyze.sImportDataFiles(CDvtmsData.sGetDBaseConnection(), filePath, mRecordDb.mInvertChannelsMask,
                                            mRecordDb.mReceivedUTC, mRecordDb.mTimeZoneOffsetMin, DMoveTimeZone.Move, // use record time zone to load from file 
                                            (DImportConverter)mRecordDb.mImportConverter, (DImportMethod)mRecordDb.mImportMethod);

                                        string fullName = Path.Combine(recordDir, "Analysis-R" + mRecordIndex.ToString() + "-A" + mAnalysisNr.ToString() + ".ini");

                                        if (mRecordDb.mTimeZoneOffsetMin != mRecordFile.mTimeZoneOffsetMin)
                                        {
                                            CProgram.sPromptError(true, "PrintFindings", "TimeZone not equal");                                          
                                        }
                                        if( mRecAnalysisFile != null )
                                        {
                                            mRecAnalysisFile.mClearAll();
                                        }
                                        if (false == mRecAnalysisFile.mbLoadMeasureFile(fullName, mRecordIndex, mAnalysisNr, mRecordDb.mInvertChannelsMask, ref mRecordFile))    // load last analysis of baseline
                                        {
                                            fullName = Path.Combine(recordDir, "Analysis-R" + mRecordIndex.ToString() + "-A0" + ".ini"); // old used 0
                                            mRecAnalysisFile.mbLoadMeasureFile(fullName, mRecordIndex, mAnalysisNr, mRecordDb.mInvertChannelsMask, ref mRecordFile);
                                        }
                                        bOk = mRecordFile != null;
                                    }
                                }
                            }
                            else
                            {
                                CProgram.sLogLine("Failed to find record #" + mRecAnalysis._mRecording_IX.ToString() + " for analysis #" + analysisStr);
                            }
                        }
                        else
                        {
                            CProgram.sLogLine("Failed to find analysis #" + analysisStr);
                        }
                    }
                }
                ++mCurrentSample;
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Load next sample strip [" + mCurrentSample.ToString() + "] A#" + analysisStr + " failed", ex);
            }           
#endif
            return bOk;
        }



        /*       bool mbGetBaseLine()
               {
                   bool bOk = false;

                   if (_mBaseLineRecordDb != null && _mBaseLineRecordFile != null && _mBaseLineRecAnalysis != null && _mBaseLineRecAnalysisFile != null ) //&& _mBaseLineStrip != null
                        //&& String.Compare(_mBaseLineRecordDb.mPatientID.mDecrypt(), mRecordDb.mPatientID.mDecrypt(), true) == 0)
                   {
                       bOk = true; // already have it
                   }
                   else
                   {
                       // lets try and find it
                       CRecordMit baseLineRecordDb = null;
                       CRecordMit baseLineRecordFile = null;
                       CRecAnalysis baseLineRecAnalysis = null;
                       CRecAnalysisFile baseLineRecAnalysisFile = null;
                       CMeasureStrip baseLineStrip = null;



                       if (baseLineRecordDb != null && baseLineRecordFile != null && baseLineRecAnalysis != null && baseLineRecAnalysisFile != null && baseLineStrip != null) ;
       //                     && String.Compare(_mBaseLineRecordDb.mPatientID.mDecrypt(), mRecordDb.mPatientID.mDecrypt(), true) == 0)
                       {

                           _mBaseLineRecordDb = baseLineRecordDb;
                           _mBaseLineRecordFile = baseLineRecordFile;
                           _mBaseLineRecAnalysis = baseLineRecAnalysis;
                           _mBaseLineRecAnalysisFile = baseLineRecAnalysisFile;
                           _mBaseLineStrip = baseLineStrip;
                           bOk = true; // found it
                       }
                   }

                   return bOk;
               }
       */
        void mBaseLine()
        {
/*            if (_mBaseLineRecordDb != null && _mBaseLineRecordFile != null && _mBaseLineRecAnalysis != null && _mBaseLineRecAnalysisFile != null ) //&& _mBaseLineStrip != null
//                && String.Compare( _mBaseLineRecordDb.mPatientID.mDecrypt(), mRecordDb.mPatientID.mDecrypt(), true ) == 0 )
                // test patient 
            {

                DateTime dtEvent = _mBaseLineRecordDb.mGetDeviceTime(mRecordDb.mEventUTC);

                bool bAutoSizeA = false;
                float cursorStart = -1.0F;
                float cursorDuration = 0.0F;
                float unitT = 0.2F;
                float unitA = 0.5F;
                int width = pictureBoxBaselineReferenceECGStrip.Width;
                int height = pictureBoxBaselineReferenceECGStrip.Height;

                CMeasureStrip mStrip = _mBaseLineRecAnalysisFile.mGetStrip(0); 


                UInt16 channel = mStrip == null ? (UInt16)0 :  mStrip._mChannel;
                float startT = mStrip == null ? 0 : mStrip._mStartTimeSec;
                float durationT = mStrip == null ? _mBaseLineRecordFile.mGetSamplesTotalTime() : mStrip._mDurationSec;
                float startA = mStrip == null ? -10 : mStrip._mStartA;
                float endA = mStrip == null ? 10 : mStrip._mEndA;

                labelLeadID.Text = mStrip == null ? "" : _mBaseLineRecordDb.mGetChannelName(channel); ;

                DateTime dtStart = _mBaseLineRecordDb.mStartRecUTC == DateTime.MinValue ? DateTime.MinValue : _mBaseLineRecordDb.mGetDeviceTime(_mBaseLineRecordDb.mStartRecUTC.AddSeconds(startT));
                string stripLen = "     " + ((int)(durationT + 0.499)).ToString() + "  sec";


                Image img = mMakeBaseChartImage(width, height, channel, bAutoSizeA,
                    startT, durationT, startA, endA, cursorStart, cursorDuration, unitT, unitA);

                pictureBoxBaselineReferenceECGStrip.Image = img;


                labelBaseSweetSpeed.Text = _mBaseLineRecordFile.mGetShowSpeedString(unitT);
                labelBaseAmplitudeSet.Text = _mBaseLineRecordFile.mGetShowAmplitudeString(unitA);
                labelEventMiddle.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT * 0.5F)); // "10:15:15";
                labelEventEnd.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT)); // "10:15:20";
                labelEventStart.Text = CProgram.sTimeToString(dtStart) + stripLen;//  "10:15:10";

                CMeasureStat ms;
                string min, mean, max;
                labelTechnicianMeasurement.Text = "";// "SHJ";
                                                          //labelTechMeasure.Text = "Technician:";
                                                          //labelQTmeasureSI.Text = "(s)";
                min = mean = max = "";
                ms = _mBaseLineRecAnalysisFile.mFindStat(true, (UInt16)DMeasure2Tag.RRtime);
                if (ms != null && ms._mCount > 0)
                {
                    mean = mBpmString(ms._mMean);
                    if (ms._mCount > 1)
                    {
                        min = mBpmString(ms._mMax);
                        max = mBpmString(ms._mMin);
                    }
                }
                labelMaxRate.Text = max; // "120";
                labelMeanRate.Text = mean; // "110";
                labelMinRate.Text = min; // "100";

                if( mean == "" )
                {
                    labelMeanRateSI.Text = "";
                    labelMeanRate.Text = ""; // "110";
                }
                if ( min == "" )
                {
                    labelMaxRateSI.Text = "";
                    labelMaxRate.Text = ""; // "120";
                    labelMaxRateHeader.Text = "";
                    labelMinRateSI.Text = "";
                    labelMinRate.Text = ""; // "100";
                    labelMinRateHeader.Text = "";
                }

                min = mean = max = "";
                ms = _mBaseLineRecAnalysisFile.mFindStat(true, (UInt16)DMeasure2Tag.QTtime);
                if (ms != null && ms._mCount > 0)
                {
                    mean = mTimeString(ms._mMean);
                    if (ms._mCount > 1)
                    {
                        min = mTimeString(ms._mMin);
                        max = mTimeString(ms._mMax);
                    }
                }

                labelQT.Text = mean;
                if (mean == "")
                {
                    labelQTSI.Text = "";
                    labelQTHeader.Text = ""; //  "0,465";
                }

                min = mean = max = "";
                ms = _mBaseLineRecAnalysisFile.mFindStat(true, (UInt16)DMeasure2Tag.QRStime);
                if (ms != null && ms._mCount > 0)
                {
                    mean = mTimeString(ms._mMean);
                    if (ms._mCount > 1)
                    {
                        min = mTimeString(ms._mMin);
                        max = mTimeString(ms._mMax);
                    }
                }
                labelQRS.Text = mean;
                if (mean == "")
                {
                    labelQRSSI.Text = "";
                    labelQRSHeader.Text = "";
                }
                min = mean = max = "";
                ms = _mBaseLineRecAnalysisFile.mFindStat(true, (UInt16)DMeasure2Tag.PRtime);
                if (ms != null && ms._mCount > 0)
                {
                    mean = mTimeString(ms._mMean);
                    if (ms._mCount > 1)
                    {
                        min = mTimeString(ms._mMin);
                        max = mTimeString(ms._mMax);
                    }
                }
                labelPR.Text = mean;
                if (mean == "")
                {
                    labelPRSI.Text = "";
                    labelPRHeader.Text = "";
                }
            }
            else
            {
                panelECGBaselineSTrip.Visible = false;
                panelTimeBarBaselineECG.Visible = false;
                panelTimeHeadingsPerStrip.Visible = false;
                panelBaseLineHeader.Visible = false;
                panelECGSweepAmpIndicator.Visible = false;
/ *
                labelBaseSweetSpeed.Text = "";
                labelBaseAmplitudeSet.Text = "";
                labelEventMiddle.Text = ""; // "10:15:15";
                labelEventEnd.Text = ""; // "10:15:20";
                labelEventStart.Text = "";//  "10:15:10";
                labelQTSI.Text = "";
                labelQT.Text = ""; //  "0,465";
                labelQTHeader.Text = "";
                labelQRSSI.Text = "";
                labelQRS.Text = ""; // "0,113";
                labelQRSHeader.Text = "";
                labelPRSI.Text = "";
                labelPR.Text = ""; // "0,20";
                labelPRHeader.Text = "";
                labelMaxRateSI.Text = "";
                labelMaxRate.Text = ""; // "120";
                labelMaxRateHeader.Text = "";
                labelMeanRateSI.Text = "";
                labelMeanRate.Text = ""; // "110";
                labelMeanRateHeader.Text = "";
                labelMinRateSI.Text = "";
                labelMinRate.Text = ""; // "100";
                labelMinRateHeader.Text  = "";
                labelLeadID.Text = ""; // "LEAD I";
                //labelBaseRefHeader.Text = "Baseline Reference";
  * /         }
*/        }
        void mInitPrevious()
        {

        }
        public string mFirstWord(string ALine)
        {
            string s = "";
            int n = ALine == null ? 0 : ALine.Length;
            int l = 0;

            for (int i = 0; i < n; ++i)
            {
                char c = ALine[i];
                if (char.IsLetterOrDigit(c))
                {
                    s += c;
                    ++l;
                }
                else if (l > 0)
                {
                    break;
                }
            }
            return s;
        }
        public string mOneLine(string ALine)
        {
            string s = "";
            int n = ALine == null ? 0 : ALine.Length;
            int l = 0;

            for (int i = 0; i < n; ++i)
            {
                char c = ALine[i];
                if (char.IsLetterOrDigit(c))
                {
                    s += c;
                    ++l;
                }
                else if (c == '\r' || c == '\n')
                {
                    break;
                }
                else if (c >= ' ')
                {
                    s += c;
                    ++l;
                }
                else if (l > 0)
                {
                    s += " ";
                }
            }
            return s;
        }


        string mBpmString( float ATimeSec  )
        {
            float f = Math.Abs(ATimeSec);
            float bpm = f < 0.001 ? 0.0F : 1 / f;
            int i = (int)(bpm * 60 + 0.5F);
            return i.ToString(); // bpm.ToString("0.0");
        }
        string mTimeString(float ATimeSec)
        {
            return ATimeSec.ToString("0.000");
        }
        
#if PLOT_SAMPLES

        Image mMakeChartImage( int AWidth, int AHeight, UInt16 AChannel, bool AbAutoSizeA,
            float AStartT, float ADurationT, float AStartA, float AEndA, 
            float ACursorStart, float ACursorDuration,
            float AUnitT, float AUnitA, float AMaxRange)
        {
            CStripChart chart = new CStripChart();
            float startA = AStartA;
            float endA = AEndA;

            mRecordFile.mInitChart(AChannel, chart, AStartT, ADurationT, AUnitA, AUnitT, AMaxRange);

            if( false == AbAutoSizeA)
            {
                startA = chart.mGetStartA();
                endA = chart.mGetEndA();
            }

            Image img = mRecordFile.mCreateChartImage2( AChannel, chart, AWidth, AHeight, ACursorStart, ACursorDuration, 
                _mbBlackWhite ? DChartDrawTo.Print_BW : DChartDrawTo.Print_color);

            return img;
        }
#endif
        /*        Image mMakeBaseChartImage(int AWidth, int AHeight, UInt16 AChannel, bool AbAutoSizeA,
                    float AStartT, float ADurationT, float AStartA, float AEndA,
                    float ACursorStart, float ACursorDuration,
                    float AUnitT, float AUnitA)
                {
                    CStripChart chart = new CStripChart();
                    float startA = AStartA;
                    float endA = AEndA;

                    _mBaseLineRecordFile.mInitChart(AChannel, chart, AStartT, ADurationT, AUnitA, AUnitT);

                    if (false == AbAutoSizeA)
                    {
                        startA = chart.mGetStartA();
                        endA = chart.mGetEndA();
                    }

                    Image img = _mBaseLineRecordFile.mCreateChartImage2(AChannel, chart, AWidth, AHeight, ACursorStart, ACursorDuration);

                    return img;
                }
        */
        void mInitStripHr(CRecAnalysisFile AAnalysisFile, CMeasureStrip AMs, 
                                            Label ALabelMeanText, Label ALabelMeanValue, Label ALabelMeanUnit )
        {
            bool bMean = false;
            string mean = "";

            try
            {
                CMeasureStat pStat = AAnalysisFile == null || AMs == null ? null : AAnalysisFile.mCalcStripStat(AMs, DMeasure2Tag.RRtime);

                if (pStat != null && pStat._mCount > 0)
                {
                    mean = mBpmString(pStat._mMean);
                    bMean = true;
                }

            }
            catch (Exception ex)
            {
                CProgram.sLogException("mInitStripHr", ex);
            }
            ALabelMeanValue.Text = mean;
            ALabelMeanValue.Visible = bMean;
            ALabelMeanText.Visible = bMean;
            ALabelMeanUnit.Visible = bMean;
        }

        void mInitStripTimeTag(CRecAnalysisFile AAnalysisFile, CMeasureStrip AMs, 
            DMeasure2Tag ATagID, Label ALabelMeanText, Label ALabelMeanValue, Label ALabelMeanUnit)
        {
            bool bMean = false;
            string mean = "";

            try
            {
                CMeasureStat pStat = AAnalysisFile == null|| AMs == null ? null : AAnalysisFile.mCalcStripStat(AMs, ATagID);

                if (pStat != null && pStat._mCount > 0)
                {
                    mean = mTimeString(pStat._mMean);
                    bMean = true;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("mInitStripTimeTag", ex);
            }
            ALabelMeanValue.Text = mean;
            ALabelMeanValue.Visible = bMean;
            ALabelMeanText.Visible = bMean;
            ALabelMeanUnit.Visible = bMean;
        }

        void mInitSample1()
        {
            // zomm 1
/*            DateTime dtEvent = mRecordDb.mGetDeviceTime(mRecordDb.mEventUTC);
            CMeasureStrip ms = mRecAnalysisFile.mFindActiveStrip(0);
            int stripIndex = mRecAnalysisFile.mGetStripIndex(ms);
            UInt16 channel;
            float startT, durationT, startA, endA;

            bool bAutoSizeA = false;
            float cursorStart = -1.0F;
            float cursorDuration = 0.0F;
            float unitT = 0.2F;
            float unitA = 0.5F;
            int width = pictureBoxZoom1.Width;
            int height = pictureBoxZoom1.Height;

            if ( ms == null )
            {
                channel = 0;
                startT = mRecordDb.mEventTimeSec - 6.0F;
                durationT = 12.0F;
                bAutoSizeA = true;
                startA = 10.0F;
                endA = -10.0F;
            }
            else
            {
                channel = ms._mChannel;
                startT = ms._mStartTimeSec;
                durationT = ms._mDurationSec;
                startA = ms._mStartA;
                endA = ms._mEndA;
                ++stripIndex;
            }
            DateTime dtStart = mRecordDb.mStartRecUTC == DateTime.MinValue ? DateTime.MinValue : mRecordDb.mGetDeviceTime(mRecordDb.mStartRecUTC.AddSeconds(startT));
            string stripLen = "     " + ((int)(durationT+0.499)).ToString() + "  sec";


            Image img = mMakeChartImage(width, height, channel, bAutoSizeA,
                startT, durationT, startA, endA, cursorStart, cursorDuration, unitT, unitA);

            pictureBoxZoom1.Image = img;

            labelSweepSample1.Text = mRecordFile.mGetShowSpeedString(unitT); ;// "25 mm/sec";
            labelZoomSample1.Text = mRecordFile.mGetShowAmplitudeString(unitA); //"1 mm/mV";
            labelEndSample1.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT)); //"10:15:20";
            labelMiddleSample1.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT * 0.5F)); //"10:15:15";
            labelStartSample1.Text = CProgram.sTimeToString(dtStart) + stripLen; //"10:15:10";
            labelLeadZoomStrip1.Text = mRecordDb.mGetChannelName(channel); //"LEAD I";
            //labelEVent1DayorNight1.Text = ""; // "am";
            labelEvent1Time.Text = CProgram.sTimeToString(dtEvent); //"10:15:10";
            //label104.Text = "-";
            labelEvent1Date.Text = CProgram.sDateToString(dtEvent); //"11/23/2016";
            //label101.Text = "-";
            labelEvent1NrSample.Text = stripIndex < 0 ? "" : stripIndex.ToString(); // record study nr  "1";
            //labelSample1Nr.Text = "Sample";
            //label98.Text = "-";
            labelEvent1NrRes.Text = "R#" + mRecordIndex.ToString(); // "175";
            labelEvent1Nr.Text = "Event #";
*/
        }
        void mInitStrip1()
        {
/*            DateTime dtEvent = mRecordDb.mGetDeviceTime(mRecordDb.mEventUTC);
            CMeasureStrip ms = mRecAnalysisFile.mFindActiveStrip(0);
            UInt16 channel;
            float startT, durationT, startA, endA;

            bool bAutoSizeA = true;
            float cursorStart = -1.0F;
            float cursorDuration = 0.0F;
            float unitT = 1.0F;
            float unitA = 0.5F;
            int width = pictureBoxZoom1.Width;
            int height = pictureBoxZoom1.Height;

            startT = 0.0F;
            durationT = mRecordFile.mGetSamplesTotalTime();
            startA = -10.0F;
            endA = 10.0F;

            if ( ms == null )
            {
                channel = 0;
                cursorStart = -1; //  mRecordDb.mEventTimeSec - 6.0F;
                cursorDuration = -1; // 12.0F;
            }
            else
            {
                channel = ms._mChannel;
                cursorStart = ms._mStartTimeSec;
                cursorDuration = ms._mDurationSec;
            }
            DateTime dtStart = mRecordDb.mStartRecUTC == DateTime.MinValue ? DateTime.MinValue : mRecordDb.mGetDeviceTime(mRecordDb.mStartRecUTC.AddSeconds(startT));


            Image img = mMakeChartImage(width, height, channel, bAutoSizeA,
                startT, durationT, startA, endA, cursorStart, cursorDuration, unitT, unitA);

            pictureBoxStrip1.Image = img;
            string stripLen = "     " + ((int)(durationT+0.499)).ToString() + "  sec";

            labelSweepFullSample1.Text = mRecordFile.mGetShowSpeedString(unitT); ;// "25 mm/sec";
            labelZoomFullSample1.Text = mRecordFile.mGetShowAmplitudeString(unitA); //"1 mm/mV";
            labelEndFullSample1.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT)); //"10:15:20";
            labelMiddleFullSample1.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT * 0.5F)); //"10:15:15";
            labelStartFullSample1.Text = CProgram.sTimeToString(dtStart) + stripLen; //"10:15:10";
            labelLeadFullStripSample1.Text = mRecordDb.mGetChannelName(channel); //"LEAD I";

//            labelEvent1Time.Text = CProgram.sTimeToString(dtEvent); //"10:15:10";
            //label104.Text = "-";
//            labelEvent1Date.Text = CProgram.sDateToString(dtEvent); //"11/23/2016";
            //label101.Text = "-";
//            labelEvent1NrID.Text = "A#" + mAnalysisNr.ToString(); // record study nr  "1";
            //labelSample1Nr.Text = "Sample";
            //label98.Text = "-";
//            labelEvent1NrRes.Text = "R#" + mRecordIndex.ToString();// "175";
//            labelEvent1Nr.Text = "Event #";
*/
        }
        void mInitSample2()
        {
            // zomm 1
/*            DateTime dtEvent = mRecordDb.mGetDeviceTime(mRecordDb.mEventUTC);
            CMeasureStrip ms = mRecAnalysisFile.mFindActiveStrip(1);
            int stripIndex = mRecAnalysisFile.mGetStripIndex(ms);
            UInt16 channel;
            float startT, durationT, startA, endA;

            bool bAutoSizeA = false;
            float cursorStart = -1.0F;
            float cursorDuration = 0.0F;
            float unitT = 0.2F;
            float unitA = 0.5F;
            int width = pictureBoxZoom1.Width;
            int height = pictureBoxZoom1.Height;

            if (ms == null)
            {
                panelSample2Header.Visible = false;
                panelSample2Lead.Visible = false;
                panelSeceondStrip.Visible = false;
                panelSample2Time.Visible = false;
/*
                labelMiddleSample2.Text = "";
                labelSweepSample2.Text = "";
                labelZoomSample2.Text = "";
                labelEndSample2.Text = "";
                labelStartSample2.Text = "";
                labelLeadSample2.Text = "";
                labelTimeSampleNr2.Text = "";
                //label36.Text = "-";
                labelDateSampleNr2.Text = "";
                //label34.Text = "-";
                labelEvent2NrSample.Text = "";
                //labelSample.Text = "Sample";
                //label30.Text = "-";
                labelEvent2NrRes.Text = "";
                //labelEvenNrSample2.Text = "Event #";
* /            }
            else
            {
                channel = ms._mChannel;
                startT = ms._mStartTimeSec;
                durationT = ms._mDurationSec;
                startA = ms._mStartA;
                endA = ms._mEndA;
                ++stripIndex;

                DateTime dtStart = mRecordDb.mStartRecUTC == DateTime.MinValue ? DateTime.MinValue : mRecordDb.mGetDeviceTime(mRecordDb.mStartRecUTC.AddSeconds(startT));
                string stripLen = "     " + ((int)(durationT+0.499)).ToString() + "  sec";


                Image img = mMakeChartImage(width, height, channel, bAutoSizeA,
                    startT, durationT, startA, endA, cursorStart, cursorDuration, unitT, unitA);

          }
*/ 
        }
        void mInitFooter()
        {
//            labelReportSignDate.Text = "_________"; //CProgram.sDateToString( DateTime.Now );// "08/23/2016";
            //'labelDateReportSign.Text = "Date:";
            //labelPhysSignLine.Text = "_______________";
            //labelPhysSign.Text = "Physician signature:";
            labelPhysPrintName.Text = "_______________"; // "J. Smithsonian";
                                                              //labelPhysNameReportPrint.Text = "Physician\'s name:";
                                                              //label7.Text = ">R";
                                                              //label3.Text = "L<";
            string date = CProgram.sPrintDateTimeVersion(DateTime.Now);

            labelPrintDate1Bottom.Text = date;
            labelPrintDate2Bottom.Text = date;
            labelPrintDate3Bottom.Text = date;

            CDvtmsData.sFillPrintPhysicianLabels(CProgram.sGetProgUserIX(), CProgram.sGetProgUserInitials(), "Analysis", _mbQCd,
                labelPhysNameReportPrint, labelPhysPrintName,
                labelPhysSign, labelPhysSignLine,
                labelDateReportSign, labelReportSignDate);

            labelPrintDate2Bottom.Text = labelPrintDate1Bottom.Text;

        }

        public string mGetEventNrString( UInt32 AEventNr, UInt16 AAnalysisNr, UInt32 ARecordIndex )
        {
            string s = AEventNr.ToString();

            if (AAnalysisNr > 1) s += " - " + AAnalysisNr.ToString();

            // s += " R#" + ARecordIndex.ToString(); // "175";
            return s;
        }

        void mInitSamplePanel( Panel APanel, PictureBox APictBox,
            Label ALabelEventNr, Label ALabelSampleNr, Label ALabelDate, Label ALabelTime, Label ALabelSampleType,
            Label ALabelLead, Label ALabelSweep, Label ALabelAmpl, Label ALabelStart, Label ALabelMid, Label ALabelEnd,
            Label ALabelClassification, Label ATextRhythm, TextBox ATextFindings, TextBox ATextActivities, TextBox ATextSymptoms,
            PictureBox APictBoxFull,
            Label ALabelLeadFull, Label ALabelSweepFull, Label ALabelAmplFull, Label ALabelStartFull, Label ALabelMidFull, Label ALabelEndFull,
            Label ALabelMeanHrText, Label ALabelMeanHrValue, Label ALabelMeanHrUnit,
            Label ALabelPrText, Label ALabelPrValue, Label ALabelPrUnit,
            Label ALabelQrsText, Label ALabelQrsValue, Label ALabelQrsUnit,
            Label ALabelQtText, Label ALabelQtValue, Label ALabelQtUnit
            )

        {
#if PLOT_SAMPLES
            CMeasureStrip ms = null;

            if (mbLoadNextSample() == false
            || mRecAnalysis == null || mRecordDb == null || mRecordFile == null || mRecAnalysisFile == null)
            {
                APanel.Visible = false;
                ms = null;
            }
            else
            {
                APanel.Visible = true;

                ms = mRecAnalysisFile.mFindActiveStrip(0);  // only show first active strip

                bool bMs = ms != null;
                bool bEmptyStrip = !bMs;

                int stripIndex = bMs ? mRecAnalysisFile.mGetStripIndex(ms)+1 : 0;

                float stripT = mRecordFile.mGetSamplesTotalTime();

                bool bAutoSizeA = false;
                float cursorStart = -1.0F;
                float cursorDuration = 0.0F;
                float unitT = 0.2F;
                float unitA = 0.5F;
                int width = APictBox.Width;
                int height = APictBox.Height;
                UInt16 channel = 0;
                float startT = mRecordDb.mEventTimeSec - 3.0F;   // no strip use event
                float durationT = 6.0F;
                float startA = -1.0F;
                float endA = 1.0F;

                float totalT = mRecordFile.mGetSamplesTotalTime();
                if (startT < 0)
                {
                    startT = 0.0F;
                }

                if (bMs)
                {
                    channel = ms._mChannel;
                    startT = ms._mStartTimeSec;
                    durationT = ms._mDurationSec;
                    startA = ms._mStartA;
                    endA = ms._mEndA;
                }
                else
                {
                    startA = mRecordFile.mGetSamplesMinValue(channel);
                    endA = mRecordFile.mGetSamplesMinValue(channel);

                    if (startT < totalT + 60.0F)
                    {
                        startT = totalT - durationT;
                        bEmptyStrip = false;
                    }
                }
                cursorStart = startT;
                cursorDuration = durationT;

                DateTime dtEvent = mRecordDb.mGetDeviceTime(mRecordDb.mEventUTC);

                float plotEventT = mRecordFile.mEventTimeSec;

                if (plotEventT <= 0)
                {
                    plotEventT = mRecordDb.mEventTimeSec;
                    mRecordFile.mEventTimeSec = plotEventT;
                }


                // plot sample strip
//                DateTime dtStart = mRecordDb.mStartRecUTC == DateTime.MinValue ? DateTime.MinValue : mRecordDb.mGetDeviceTime(mRecordDb.mStartRecUTC.AddSeconds(startT));
                DateTime dtStart = mRecordFile.mBaseUTC == DateTime.MinValue ? DateTime.MinValue : mRecordFile.mGetDeviceTime(mRecordFile.mBaseUTC.AddSeconds(startT));

                string stripLen = "     " + ((int)(durationT + 0.499)).ToString() + "  sec";

                Image oldImg = APictBox.Image;

                if( oldImg != null )
                {
                    APictBox.Image = null;

                    oldImg.Dispose(); // free old image memory
                    oldImg = null;
                }

                Image img = bEmptyStrip ? null : mMakeChartImage(width, height, channel, bAutoSizeA,
                    startT, durationT, startA, endA, -1.0F, 0.0F, unitT, unitA, CRecordMit.sGetMaxAmplitudeRange());


                APictBox.Image = img;

                ALabelSweep.Text = bEmptyStrip ? "" : mRecordFile.mGetShowSpeedString(unitT); ;// "25 mm/sec";
                ALabelAmpl.Text = bEmptyStrip ? "" : mRecordFile.mGetShowAmplitudeString(unitA); //"1 mm/mV";
                ALabelEnd.Text = bEmptyStrip || dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT)); //"10:15:20";
                ALabelMid.Text = bEmptyStrip || dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT * 0.5F)); //"10:15:15";
                ALabelStart.Text = bEmptyStrip ? "" : CProgram.sTimeToString(dtStart) + " " + CProgram.sDateToString(dtStart) + stripLen; //"10:15:10";
                if(ALabelLead != null ) ALabelLead.Text = bEmptyStrip ? "" : mRecordDb.mGetChannelName(channel); //"LEAD I";
                ALabelTime.Text = CProgram.sTimeToString(dtEvent); //"10:15:10";
                ALabelDate.Text = CProgram.sDateToString(dtEvent); //"11/23/2016";
                ALabelSampleNr.Text = mCurrentSample.ToString(); //stripIndex < 0 ? "" : stripIndex.ToString(); // record study nr  "1";
                ALabelEventNr.Text = mGetEventNrString(mRecordEventNr, mAnalysisNr, mRecordIndex); // "175";
                ALabelSampleType.Text = mRecAnalysis._mbManualEvent ? "Manual" : "Auto";

                string classification = mRecAnalysis._mAnalysisClassSet.mbIsEmpty() || CDvtmsData._sEnumListFindingsClass == null ? ""
                         : CDvtmsData._sEnumListFindingsClass.mGetLabel(mRecAnalysis._mAnalysisClassSet.mGetValue(0));

                string rhytems = "";
                if (CDvtmsData._sEnumListStudyRhythms != null)
                {
                    rhytems = CDvtmsData._sEnumListStudyRhythms.mFillString(mRecAnalysis._mAnalysisRhythmSet, true, " ", false/*true*/, ", "/*"\r\n"*/);
                }
                ATextRhythm.Text = rhytems;
                ALabelClassification.Text = classification;
                ATextFindings.Text = mRecAnalysis._mAnalysisRemark;
                ATextActivities.Text = mRecAnalysis._mActivitiesRemark;
                ATextSymptoms.Text = mRecAnalysis._mSymptomsRemark;

                bool bDoFull = true;

                // Plot full strip with cursor
                if ( _mbPlot2Ch )
                {
                    UInt16 firstChannel = channel;

/*  always show second channel, if second strip is needed it should be active selected                    CMeasureStrip ms2 = mRecAnalysisFile.mFindActiveStrip(1);  // show second active strip

                    if (ms2 != null)
                    {
                        // show second strip
                        bDoFull = false;
                        channel = ms2._mChannel;
                        startT = ms2._mStartTimeSec;
                        durationT = ms2._mDurationSec;
                        startA = ms2._mStartA;
                        endA = ms2._mEndA;
                        dtStart = mRecordDb.mStartRecUTC == DateTime.MinValue ? DateTime.MinValue : mRecordDb.mGetDeviceTime(mRecordDb.mStartRecUTC.AddSeconds(startT));
                    }
                    else 
                    */
                    if(mRecordFile.mNrSignals >= 2)
                    {
                        // show alternate channel in same strip time
                        bDoFull = false;
                        channel = (UInt16)(firstChannel == 0 ? 1 : 0);
                        bAutoSizeA = true;  // unknown amplitude
//                        ms2 = ms;    
                    }
                    oldImg = APictBoxFull.Image;

                    if (oldImg != null)
                    {
                        APictBoxFull.Image = null;

                        oldImg.Dispose();   // free old image memory
                        oldImg = null;
                    }


                    if ( bDoFull == false )
                    { 
                        img = img = bEmptyStrip ? null : mMakeChartImage(width, height, channel, bAutoSizeA,
                           startT, durationT, startA, endA, -1.0F, 0.0F, unitT, unitA, CRecordMit.sGetMaxAmplitudeRange());

                        APictBoxFull.Image = img;

                        string partStr = "   " + ((int)durationT).ToString();

                        if (durationT < stripT - 0.1F)
                        {
                            partStr += " / " + ((int)stripT).ToString();
                        }
                        partStr += " sec ";

                        ALabelSweepFull.Text = bEmptyStrip ? "" : mRecordFile.mGetShowSpeedString(unitT); ;// "25 mm/sec";
                        ALabelAmplFull.Text = bEmptyStrip ? "" : mRecordFile.mGetShowAmplitudeString(unitA); //"1 mm/mV";
                        ALabelEndFull.Text = bEmptyStrip || dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT)); //"10:15:20";
                        ALabelMidFull.Text = bEmptyStrip || dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT * 0.5F)); //"10:15:15";
                        ALabelStartFull.Text = bEmptyStrip ? "" : CProgram.sTimeToString(dtStart) + " " + CProgram.sDateToString(dtStart) + partStr; //"10:15:10";

                        ALabelTime.Text = CProgram.sTimeToString(dtEvent); //"10:15:10";
                        ALabelDate.Text = CProgram.sDateToString(dtEvent); //"11/23/2016";
                        ALabelSampleNr.Text = mCurrentSample.ToString(); //stripIndex < 0 ? "" : stripIndex.ToString(); // record study nr  "1";
                        ALabelEventNr.Text = mGetEventNrString(mRecordEventNr, mAnalysisNr, mRecordIndex); // "175";
                        if(ALabelLeadFull != null) ALabelLeadFull.Text = mRecordDb.mGetChannelName(channel); //"LEAD I";
                      

  /*                      ALabelMeanHrText.Visible = false;
                        ALabelMeanHrValue.Visible = false;
                        ALabelMeanHrUnit.Visible = false; 
                        ALabelPrText.Visible = false;
                        ALabelPrValue.Visible = false;
                        ALabelPrUnit.Visible = false; 
                        ALabelQrsText.Visible = false;
                        ALabelQrsValue.Visible = false;
                        ALabelQrsUnit.Visible = false; 
                        ALabelQtText.Visible = false;
                        ALabelQtValue.Visible = false;
                        ALabelQtUnit.Visible = false; 
  */
                    }
                }

                if (bDoFull)
                {
                    bAutoSizeA = true;
                    //                cursorStart = -1.0F;
                    //                cursorDuration = 0.0F;
                    unitT = 1.0F;
                    unitA = 0.5F;
                    width = APictBoxFull.Width;
                    height = APictBoxFull.Height;

                    startT = 0.0F;
                    durationT = mRecordFile.mGetSamplesTotalTime();
                    startA = -10.0F;
                    endA = 10.0F;

                    //                cursorStart = ms._mStartTimeSec;
                    //                cursorDuration = ms._mDurationSec;
//                    dtStart = mRecordDb.mGetDeviceTime(mRecordDb.mStartRecUTC.AddSeconds(startT));
                    dtStart = mRecordFile.mGetDeviceTime(mRecordFile.mBaseUTC.AddSeconds(startT));

                    img = mMakeChartImage(width, height, channel, bAutoSizeA,
                        startT, durationT, startA, endA, cursorStart, cursorDuration, unitT, unitA, CRecordMit.sGetMaxAmplitudeRange());

                    APictBoxFull.Image = img;
                    stripLen = "     " + ((int)(durationT + 0.499)).ToString() + "  sec";
                    
                    ALabelSweepFull.Text = mRecordFile.mGetShowSpeedString(unitT); ;// "25 mm/sec";
                    ALabelAmplFull.Text = mRecordFile.mGetShowAmplitudeString(unitA); //"1 mm/mV";
                    ALabelEndFull.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT)); //"10:15:20";
                    ALabelMidFull.Text = dtStart == DateTime.MinValue ? "" :  CProgram.sTimeToString(dtStart.AddSeconds(durationT * 0.5F)); //"10:15:15";
                    ALabelStartFull.Text = CProgram.sTimeToString(dtStart) + " " + CProgram.sDateToString(dtStart) + stripLen; //"10:15:10";
                    if (ALabelLeadFull != null) ALabelLeadFull.Text = mRecordDb.mGetChannelName(channel); //"LEAD I";
                }
                    mInitStripHr(mRecAnalysisFile, ms, ALabelMeanHrText, ALabelMeanHrValue, ALabelMeanHrUnit);
                    mInitStripTimeTag(mRecAnalysisFile, ms, DMeasure2Tag.PRtime, ALabelPrText, ALabelPrValue, ALabelPrUnit);
                    mInitStripTimeTag(mRecAnalysisFile, ms, DMeasure2Tag.QRStime, ALabelQrsText, ALabelQrsValue, ALabelQrsUnit);
                    mInitStripTimeTag(mRecAnalysisFile, ms, DMeasure2Tag.QTtime, ALabelQtText, ALabelQtValue, ALabelQtUnit);
                
            }
#endif
        }

        void mInitSample3()
        {
            mInitSamplePanel(panelEventSample3, pictureBoxSample3,
                labelSample3EventNr, labelSample3Nr, labelSample3Date, labelSample3Time, labelSampleType3,
                null/* labelSample3Lead*/, labelSample3Sweep, labelSample3Ampl, labelSample3Start, labelSample3Mid, labelSample3End,
                labelClassification3, labelRhythms3, textBoxFindings3, textBoxActivities3, textBoxSymptoms3,
                pictureBoxSample3Full,
                null /*labelSample3LeadFull*/, labelSample3SweepFull, labelSample3AmplFull, labelSample3StartFull, labelSample3MidFull, labelSample3EndFull,
                labelMeanHrText3, labelMeanHrValue3, labelMeanHrUnit3,
                labelPrText3, labelPrValue3, labelPrUnit3,
                labelQrsText3, labelQrsValue3, labelQrsUnit3,
                labelQtText3, labelQtValue3, labelQtUnit3
            );
        }

        void mInitSample4()
        {
            mInitSamplePanel(panelEventSample4, pictureBoxSample4,
                labelSample4EventNr, labelSample4Nr, labelSample4Date, labelSample4Time, labelSampleType4,
                labelSample4Lead, labelSample4Sweep, labelSample4Ampl, labelSample4Start, labelSample4Mid, labelSample4End,
                labelClassification4, labelRhythms4, textBoxFindings4, textBoxActivities4, textBoxSymptoms4,
                pictureBoxSample4Full,
                labelSample4LeadFull, labelSample4SweepFull, labelSample4AmplFull, labelSample4StartFull, labelSample4MidFull, labelSample4EndFull,
                MeanHrText4, labelMeanHrValue4, MeanHrUnit4,
                PrText4, labelPrValue4, lPrUnit4,
                QrsText4, labelQrsValue4, QrsUnit4,
                QtText4, labelQtValue4, QtUnit4
            );
        }

        void mInitSample5()
        {
            mInitSamplePanel(panelEventSample5, pictureBoxSample5,
                labelSample5EventNr, labelSample5Nr, labelSample5Date, labelSample5Time, labelSampleType5,
                labelSample5Lead, labelSample5Sweep, labelSample5Ampl, labelSample5Start, labelSample5Mid, labelSample5End,
                labelClassification5, labelRhythms5, textBoxFindings5, textBoxActivities5, textBoxSymptoms5,
                pictureBoxSample5Full,
                labelSample5LeadFull, labelSample5SweepFull, labelSample5AmplFull, labelSample5StartFull, labelSample5MidFull, labelSample5EndFull,
                labelMeanHrText5, labelMeanHrValue5, labelMeanHrUnit5,
                labelPrText5, labelPrValue5, labelPrUnit5,
                labelQrsText5, labelQrsValue5, labelQrsUnit5,
                labelQtText5, labelQtValue5, labelQtUnit5
            );
        }
#if oldSampleInit
        void mInitSample3Old()
        {
            
            // zomm 1
#if PLOT_SAMPLES

            CMeasureStrip ms = null;

            if( mbLoadNextSample() == false
            || mRecAnalysis == null || mRecordDb == null || mRecordFile == null || mRecAnalysisFile == null)
            {
                panelEventSample3.Visible = false;
                ms = null;
            }
            else
            {
                panelEventSample3.Visible = true;

                ms = mRecAnalysisFile.mFindActiveStrip(0);  // only show first active strip

                bool bMs = ms != null;
                bool bEmptyStrip = !bMs;
                PictureBox pictBox = pictureBoxSample3;
                int stripIndex = mRecAnalysisFile.mGetStripIndex(ms);
       
                bool bAutoSizeA = false;
                float cursorStart = -1.0F;
                float cursorDuration = 0.0F;
                float unitT = 0.2F;
                float unitA = 0.5F;
                int width = pictBox.Width;
                int height = pictBox.Height;
                UInt16 channel = 0;
                float startT = mRecordDb.mEventTimeSec - 3.0F;   // no strip use event
                float durationT = 6.0F;
                float startA = -1.0F;
                float endA = 1.0F;

                float totalT = mRecordFile.mGetSamplesTotalTime();
                if ( startT < 0 )
                {
                    startT = 0.0F;
                }
                ++stripIndex;

                if( bMs )
                {
                    channel = ms._mChannel;
                    startT = ms._mStartTimeSec;
                    durationT = ms._mDurationSec;
                    startA = ms._mStartA;
                    endA = ms._mEndA;
                }
                else
                {
                    startA = mRecordFile.mGetSamplesMinValue(channel);
                    endA = mRecordFile.mGetSamplesMinValue(channel);

                    if( startT < totalT + 60.0F)
                    {
                        startT = totalT - durationT;
                        bEmptyStrip = false;
                    }
                }

                panelEventSample3.Visible = true;
                DateTime dtEvent = mRecordDb.mGetDeviceTime(mRecordDb.mEventUTC);

                // plot sample

                DateTime dtStart = mRecordDb.mStartRecUTC == DateTime.MinValue ? DateTime.MinValue : mRecordDb.mGetDeviceTime(mRecordDb.mStartRecUTC.AddSeconds(startT));

                string stripLen = "     " + ((int)(durationT + 0.499)).ToString() + "  sec";

                Image img = bEmptyStrip ? null :  mMakeChartImage(width, height, channel, bAutoSizeA,
                    startT, durationT, startA, endA, cursorStart, cursorDuration, unitT, unitA);

                pictBox.Image = img;

                labelSample3Sweep.Text = bEmptyStrip ? "" : mRecordFile.mGetShowSpeedString(unitT); ;// "25 mm/sec";
                labelSample3Ampl.Text = bEmptyStrip ? "" : mRecordFile.mGetShowAmplitudeString(unitA); //"1 mm/mV";
                labelSample3End.Text = bEmptyStrip || dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT)); //"10:15:20";
                labelSample3Mid.Text = bEmptyStrip || dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT * 0.5F)); //"10:15:15";
                labelSample3Start.Text = bEmptyStrip ? "" : CProgram.sTimeToString(dtStart) + stripLen; //"10:15:10";
                labelSample3Lead.Text = bEmptyStrip ? "" : mRecordDb.mGetChannelName(channel); //"LEAD I";
                labelSample3Time.Text = CProgram.sTimeToString(dtEvent); //"10:15:10";
                labelSample3Date.Text = CProgram.sDateToString(dtEvent); //"11/23/2016";
                labelSample3Nr.Text = mCurrentSample.ToString(); //stripIndex < 0 ? "" : stripIndex.ToString(); // record study nr  "1";
                labelSample3EventNr.Text = mGetEventNrString( mRecordEventNr, mAnalysisNr, mRecordIndex ); // "175";

                string classification = mRecAnalysis._mAnalysisClassSet.mbIsEmpty() || CDvtmsData._sEnumListFindingsClass == null ? ""
                         : CDvtmsData._sEnumListFindingsClass.mGetLabel(mRecAnalysis._mAnalysisClassSet.mGetValue(0));


                string rhytems = "";
                if (CDvtmsData._sEnumListStudyRhythms != null)
                {
                    rhytems = CDvtmsData._sEnumListStudyRhythms.mFillString(mRecAnalysis._mAnalysisRhythmSet, true, " ", true, "\r\n");
                }

                labelClassification3.Text = classification;
                textBoxFindings3.Text = rhytems + "\r\n" + mRecAnalysis._mAnalysisRemark;
                textBoxActivities3.Text = mRecAnalysis._mActivitiesRemark;
                textBoxSymptoms3.Text = mRecAnalysis._mSymptomsRemark;
  
                // Plot full strip with cursor
                pictBox = pictureBoxSample3Full;

                bAutoSizeA = true;
//                cursorStart = -1.0F;
//                cursorDuration = 0.0F;
                unitT = 1.0F;
                unitA = 0.5F;
                width = pictBox.Width;
                height = pictBox.Height;

                startT = 0.0F;
                durationT = mRecordFile.mGetSamplesTotalTime();
                startA = -10.0F;
                endA = 10.0F;

//                cursorStart = ms._mStartTimeSec;
//                cursorDuration = ms._mDurationSec;
                dtStart = mRecordDb.mGetDeviceTime(mRecordDb.mStartRecUTC.AddSeconds(startT));

                img = mMakeChartImage(width, height, channel, bAutoSizeA,
                    startT, durationT, startA, endA, cursorStart, cursorDuration, unitT, unitA);

                pictBox.Image = img;
                stripLen = "     " + ((int)(durationT + 0.499)).ToString() + "  sec";

                labelSample3SweepFull.Text = mRecordFile.mGetShowSpeedString(unitT); ;// "25 mm/sec";
                labelSample3AmplFull.Text = mRecordFile.mGetShowAmplitudeString(unitA); //"1 mm/mV";
                labelSample3EndFull.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT)); //"10:15:20";
                labelSample3MidFull.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT * 0.5F)); //"10:15:15";
                labelSample3StartFull.Text = CProgram.sTimeToString(dtStart) + stripLen; //"10:15:10";
                labelSample3LeadFull.Text = mRecordDb.mGetChannelName(channel); //"LEAD I";

                mInitStripHr(mRecAnalysisFile, 0, labelMeanHrText3, labelMeanHrValue3, labelMeanHrUnit3);
                mInitStripTimeTag(mRecAnalysisFile, 0, DMeasure2Tag.PRtime, labelPrText3, labelPrValue3, labelPrUnit3);
                mInitStripTimeTag(mRecAnalysisFile, 0, DMeasure2Tag.QRStime, labelQrsText3, labelQrsValue3, labelQrsUnit3);
                mInitStripTimeTag(mRecAnalysisFile, 0, DMeasure2Tag.QTtime, labelQtText3, labelQtValue3, labelQtUnit3);

            }
#endif
        }

        void mInitSample4old()
        {
            // zomm 1
#if PLOT_SAMPLES
            CMeasureStrip ms = null;

            if (mbLoadNextSample() == false
            || mRecAnalysis == null || mRecordDb == null || mRecordFile == null || mRecAnalysisFile == null)
            {
                ms = null;
            }
            else
            {
                ms = mRecAnalysisFile.mFindActiveStrip(0);  // only show first active strip
            }
            if (ms == null)
            {
                panelEventSample4.Visible = false;
            }
            else
            {
                PictureBox pictBox = pictureBoxSample4;
                int stripIndex = mRecAnalysisFile.mGetStripIndex(ms);

                bool bAutoSizeA = false;
                float cursorStart = -1.0F;
                float cursorDuration = 0.0F;
                float unitT = 0.2F;
                float unitA = 0.5F;
                int width = pictBox.Width;
                int height = pictBox.Height;
                UInt16 channel = ms._mChannel;
                float startT = ms._mStartTimeSec;
                float durationT = ms._mDurationSec;
                float startA = ms._mStartA;
                float endA = ms._mEndA;
                ++stripIndex;

                panelEventSample4.Visible = true;

                DateTime dtEvent = mRecordDb.mGetDeviceTime(mRecordDb.mEventUTC);

                // plot sample

                DateTime dtStart = mRecordDb.mStartRecUTC == DateTime.MinValue ? DateTime.MinValue : mRecordDb.mGetDeviceTime(mRecordDb.mStartRecUTC.AddSeconds(startT));

                string stripLen = "     " + ((int)(durationT + 0.499)).ToString() + "  sec";

                Image img = mMakeChartImage(width, height, channel, bAutoSizeA,
                    startT, durationT, startA, endA, cursorStart, cursorDuration, unitT, unitA);

                pictBox.Image = img;

                labelSample4Sweep.Text = mRecordFile.mGetShowSpeedString(unitT); ;// "25 mm/sec";
                labelSample4Ampl.Text = mRecordFile.mGetShowAmplitudeString(unitA); //"1 mm/mV";
                labelSample4End.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT)); //"10:15:20";
                labelSample4Mid.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT * 0.5F)); //"10:15:15";
                labelSample4Start.Text = CProgram.sTimeToString(dtStart) + stripLen; //"10:15:10";
                labelSample4Lead.Text = mRecordDb.mGetChannelName(channel); //"LEAD I";
                labelSample4Time.Text = CProgram.sTimeToString(dtEvent); //"10:15:10";
                labelSample4Date.Text = CProgram.sDateToString(dtEvent); //"11/23/2016";
                labelSample4Nr.Text = mCurrentSample.ToString(); //stripIndex < 0 ? "" : stripIndex.ToString(); // record study nr  "1";
                labelSample4EventNr.Text = mGetEventNrString(mRecordEventNr, mAnalysisNr, mRecordIndex); // "175";

                string classification = mRecAnalysis._mAnalysisClassSet.mbIsEmpty() || CDvtmsData._sEnumListFindingsClass == null ? ""
                         : CDvtmsData._sEnumListFindingsClass.mGetLabel(mRecAnalysis._mAnalysisClassSet.mGetValue(0));


                string rhytems = "";
                if (CDvtmsData._sEnumListStudyRhythms != null)
                {
                    rhytems = CDvtmsData._sEnumListStudyRhythms.mFillString(mRecAnalysis._mAnalysisRhythmSet, true, " ", true, "\r\n");
                }

                labelClassification4.Text = classification;
                textBoxFindings4.Text = rhytems + "\r\n" + mRecAnalysis._mAnalysisRemark;
                textBoxActivities4.Text = mRecAnalysis._mActivitiesRemark;
                textBoxSymptoms4.Text = mRecAnalysis._mSymptomsRemark;
                // Plot full strip with cursor
                pictBox = pictureBoxSample4Full;

                bAutoSizeA = true;
                cursorStart = -1.0F;
                cursorDuration = 0.0F;
                unitT = 1.0F;
                unitA = 0.5F;
                width = pictBox.Width;
                height = pictBox.Height;

                startT = 0.0F;
                durationT = mRecordFile.mGetSamplesTotalTime();
                startA = -10.0F;
                endA = 10.0F;

                cursorStart = ms._mStartTimeSec;
                cursorDuration = ms._mDurationSec;
                dtStart = mRecordDb.mGetDeviceTime(mRecordDb.mStartRecUTC.AddSeconds(startT));

                img = mMakeChartImage(width, height, channel, bAutoSizeA,
                    startT, durationT, startA, endA, cursorStart, cursorDuration, unitT, unitA);

                pictBox.Image = img;
                stripLen = "     " + ((int)(durationT + 0.499)).ToString() + "  sec";

                labelSample4SweepFull.Text = mRecordFile.mGetShowSpeedString(unitT); ;// "25 mm/sec";
                labelSample4AmplFull.Text = mRecordFile.mGetShowAmplitudeString(unitA); //"1 mm/mV";
                labelSample4EndFull.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT)); //"10:15:20";
                labelSample4MidFull.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT * 0.5F)); //"10:15:15";
                labelSample4StartFull.Text = CProgram.sTimeToString(dtStart) + stripLen; //"10:15:10";
                labelSample4LeadFull.Text = mRecordDb.mGetChannelName(channel); //"LEAD I";

                mInitStripHr(mRecAnalysisFile, 0, labelMeanHrText4, labelMeanHrValue4, labelMeanHrUnit4);
                mInitStripTimeTag(mRecAnalysisFile, 0, DMeasure2Tag.PRtime, labelPrText4, labelPrValue4, labelPrUnit4);
                mInitStripTimeTag(mRecAnalysisFile, 0, DMeasure2Tag.QRStime, labelQrsText4, labelQrsValue4, labelQrsUnit4);
                mInitStripTimeTag(mRecAnalysisFile, 0, DMeasure2Tag.QTtime, labelQtText4, labelQtValue4, labelQtUnit4);
            }
#endif
        }

        void mInitSample5old()
        {
#if PLOT_SAMPLES
            // zomm 1
            CMeasureStrip ms = null;

            if (mbLoadNextSample() == false
            || mRecAnalysis == null || mRecordDb == null || mRecordFile == null || mRecAnalysisFile == null)
            {
                ms = null;
            }
            else
            {
                ms = mRecAnalysisFile.mFindActiveStrip(0);  // only show first active strip
            }
            if (ms == null)
            {
                panelEventSample5.Visible = false;
            }
            else
            {
                PictureBox pictBox = pictureBoxSample5;
                int stripIndex = mRecAnalysisFile.mGetStripIndex(ms);

                bool bAutoSizeA = false;
                float cursorStart = -1.0F;
                float cursorDuration = 0.0F;
                float unitT = 0.2F;
                float unitA = 0.5F;
                int width = pictBox.Width;
                int height = pictBox.Height;
                UInt16 channel = ms._mChannel;
                float startT = ms._mStartTimeSec;
                float durationT = ms._mDurationSec;
                float startA = ms._mStartA;
                float endA = ms._mEndA;
                ++stripIndex;

                DateTime dtEvent = mRecordDb.mGetDeviceTime(mRecordDb.mEventUTC);

                // plot sample

                DateTime dtStart = mRecordDb.mStartRecUTC == DateTime.MinValue ? DateTime.MinValue : mRecordDb.mGetDeviceTime(mRecordDb.mStartRecUTC.AddSeconds(startT));

                string stripLen = "     " + ((int)(durationT + 0.499)).ToString() + "  sec";

                Image img = mMakeChartImage(width, height, channel, bAutoSizeA,
                    startT, durationT, startA, endA, cursorStart, cursorDuration, unitT, unitA);

                pictBox.Image = img;

                labelSample5Sweep.Text = mRecordFile.mGetShowSpeedString(unitT); ;// "25 mm/sec";
                labelSample5Ampl.Text = mRecordFile.mGetShowAmplitudeString(unitA); //"1 mm/mV";
                labelSample5End.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT)); //"10:15:20";
                labelSample5Mid.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT * 0.5F)); //"10:15:15";
                labelSample5Start.Text = CProgram.sTimeToString(dtStart) + stripLen; //"10:15:10";
                labelSample5Lead.Text = mRecordDb.mGetChannelName(channel); //"LEAD I";
                labelSample5Time.Text = CProgram.sTimeToString(dtEvent); //"10:15:10";
                labelSample5Date.Text = CProgram.sDateToString(dtEvent); //"11/23/2016";
                labelSample5Nr.Text = mCurrentSample.ToString(); //stripIndex < 0 ? "" : stripIndex.ToString(); // record study nr  "1";
                labelSample5EventNr.Text = mGetEventNrString(mRecordEventNr, mAnalysisNr, mRecordIndex); // "175";

                string classification = mRecAnalysis._mAnalysisClassSet.mbIsEmpty() || CDvtmsData._sEnumListFindingsClass == null ? ""
                        : CDvtmsData._sEnumListFindingsClass.mGetLabel(mRecAnalysis._mAnalysisClassSet.mGetValue(0));


                string rhytems = "";
                if (CDvtmsData._sEnumListStudyRhythms != null)
                {
                    rhytems = CDvtmsData._sEnumListStudyRhythms.mFillString(mRecAnalysis._mAnalysisRhythmSet, true, " ", true, "\r\n");
                }

                labelClassification5.Text = classification;
                textBoxFindings5.Text = rhytems + "\r\n" + mRecAnalysis._mAnalysisRemark;
                textBoxActivities5.Text = mRecAnalysis._mActivitiesRemark;
                textBoxSymptoms5.Text = mRecAnalysis._mSymptomsRemark;
                // Plot full strip with cursor
                pictBox = pictureBoxSample5Full;

                bAutoSizeA = true;
                cursorStart = -1.0F;
                cursorDuration = 0.0F;
                unitT = 1.0F;
                unitA = 0.5F;
                width = pictBox.Width;
                height = pictBox.Height;

                startT = 0.0F;
                durationT = mRecordFile.mGetSamplesTotalTime();
                startA = -10.0F;
                endA = 10.0F;

                cursorStart = ms._mStartTimeSec;
                cursorDuration = ms._mDurationSec;
                dtStart = mRecordDb.mGetDeviceTime(mRecordDb.mStartRecUTC.AddSeconds(startT));

                img = mMakeChartImage(width, height, channel, bAutoSizeA,
                    startT, durationT, startA, endA, cursorStart, cursorDuration, unitT, unitA);

                pictBox.Image = img;
                stripLen = "     " + ((int)(durationT + 0.499)).ToString() + "  sec";

                labelSample5SweepFull.Text = mRecordFile.mGetShowSpeedString(unitT); ;// "25 mm/sec";
                labelSample5AmplFull.Text = mRecordFile.mGetShowAmplitudeString(unitA); //"1 mm/mV";
                labelSample5EndFull.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT)); //"10:15:20";
                labelSample5MidFull.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT * 0.5F)); //"10:15:15";
                labelSample5StartFull.Text = CProgram.sTimeToString(dtStart) + stripLen; //"10:15:10";
                labelSample5LeadFull.Text = mRecordDb.mGetChannelName(channel); //"LEAD I";

                mInitStripHr(mRecAnalysisFile, 0, labelMeanHrText5, labelMeanHrValue5, labelMeanHrUnit5);
                mInitStripTimeTag(mRecAnalysisFile, 0, DMeasure2Tag.PRtime, labelPrText5, labelPrValue5, labelPrUnit5);
                mInitStripTimeTag(mRecAnalysisFile, 0, DMeasure2Tag.QRStime, labelQrsText5, labelQrsValue5, labelQrsUnit5);
                mInitStripTimeTag(mRecAnalysisFile, 0, DMeasure2Tag.QTtime, labelQtText5, labelQtValue5, labelQtUnit5);
            }
#endif
        }

/*        void mInitSample6(UInt16 ASampleNr)
        {
            // zomm 1
            CMeasureStrip ms = mRecAnalysisFile.mFindActiveStrip(ASampleNr);

            if (ms == null)
            {
                panelEventSample6.Visible = false;
            }
            else
            {
                PictureBox pictBox = pictureBoxSample6;
                int stripIndex = mRecAnalysisFile.mGetStripIndex(ms);

                bool bAutoSizeA = false;
                float cursorStart = -1.0F;
                float cursorDuration = 0.0F;
                float unitT = 0.2F;
                float unitA = 0.5F;
                int width = pictBox.Width;
                int height = pictBox.Height;
                UInt16 channel = ms._mChannel;
                float startT = ms._mStartTimeSec;
                float durationT = ms._mDurationSec;
                float startA = ms._mStartA;
                float endA = ms._mEndA;
                ++stripIndex;

                DateTime dtEvent = mRecordDb.mGetDeviceTime(mRecordDb.mEventUTC);

                // plot sample

                DateTime dtStart = mRecordDb.mStartRecUTC == DateTime.MinValue ? DateTime.MinValue : mRecordDb.mGetDeviceTime(mRecordDb.mStartRecUTC.AddSeconds(startT));

                string stripLen = "     " + ((int)(durationT + 0.499)).ToString() + "  sec";

                Image img = mMakeChartImage(width, height, channel, bAutoSizeA,
                    startT, durationT, startA, endA, cursorStart, cursorDuration, unitT, unitA);

                pictBox.Image = img;

                labelSample6Sweep.Text = mRecordFile.mGetShowSpeedString(unitT); ;// "25 mm/sec";
                labelSample6Ampl.Text = mRecordFile.mGetShowAmplitudeString(unitA); //"1 mm/mV";
                labelSample6End.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT)); //"10:15:20";
                labelSample6Mid.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT * 0.5F)); //"10:15:15";
                labelSample6Start.Text = CProgram.sTimeToString(dtStart) + stripLen; //"10:15:10";
                labelSample6Lead.Text = mRecordDb.mGetChannelName(channel); //"LEAD I";
                labelSample6Time.Text = CProgram.sTimeToString(dtEvent); //"10:15:10";
                labelSample6Date.Text = CProgram.sDateToString(dtEvent); //"11/23/2016";
                labelSample6Nr.Text = stripIndex < 0 ? "" : stripIndex.ToString(); // record study nr  "1";
                labelSample6EventNr.Text = "R#" + mRecordIndex.ToString(); // "175";

                // Plot full strip with cursor
                pictBox = pictureBoxSample6Full;

                bAutoSizeA = true;
                cursorStart = -1.0F;
                cursorDuration = 0.0F;
                unitT = 1.0F;
                unitA = 0.5F;
                width = pictBox.Width;
                height = pictBox.Height;

                startT = 0.0F;
                durationT = mRecordFile.mGetSamplesTotalTime();
                startA = -10.0F;
                endA = 10.0F;

                cursorStart = ms._mStartTimeSec;
                cursorDuration = ms._mDurationSec;
                dtStart = mRecordDb.mGetDeviceTime(mRecordDb.mStartRecUTC.AddSeconds(startT));

                img = mMakeChartImage(width, height, channel, bAutoSizeA,
                    startT, durationT, startA, endA, cursorStart, cursorDuration, unitT, unitA);

                pictBox.Image = img;
                stripLen = "     " + ((int)(durationT + 0.499)).ToString() + "  sec";

                labelSample6SweepFull.Text = mRecordFile.mGetShowSpeedString(unitT); ;// "25 mm/sec";
                labelSample6AmplFull.Text = mRecordFile.mGetShowAmplitudeString(unitA); //"1 mm/mV";
                labelSample6EndFull.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT)); //"10:15:20";
                labelSample6MidFull.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT * 0.5F)); //"10:15:15";
                labelSample6StartFull.Text = CProgram.sTimeToString(dtStart) + stripLen; //"10:15:10";
                labelSample6LeadFull.Text = mRecordDb.mGetChannelName(channel); //"LEAD I";
            }
        }
*/
#endif


        [System.Runtime.InteropServices.DllImportAttribute("gdi32.dll")]
        private static extern int BitBlt(
     IntPtr hdcDest,     // handle to destination DC (device context)
     int nXDest,         // x-coord of destination upper-left corner
     int nYDest,         // y-coord of destination upper-left corner
     int nWidth,         // width of destination rectangle
     int nHeight,        // height of destination rectangle
     IntPtr hdcSrc,      // handle to source DC
     int nXSrc,          // x-coordinate of source upper-left corner
     int nYSrc,          // y-coordinate of source upper-left corner
     System.Int32 dwRop  // raster operation code
     );

        [System.Runtime.InteropServices.DllImportAttribute("gdi32.dll")]
        public static extern IntPtr SelectObject(IntPtr hdc, IntPtr obj);

        [System.Runtime.InteropServices.DllImportAttribute("gdi32.dll")]
        public static extern void DeleteObject(IntPtr obj);

        void mPanelToClipBoard1(Control APanel)
        {
            CProgram.sPromptError(true, "Print", "Do not use this function");
            try
            {
                Bitmap bmp = new Bitmap(APanel.Width, APanel.Height);

                using (Graphics G = Graphics.FromImage(bmp))
                {
                    G.Clear(Color.Cyan);
                }

                Graphics g1 = APanel.CreateGraphics();
                Image MyImage = new Bitmap(APanel.ClientRectangle.Width, APanel.ClientRectangle.Height, g1);
                Graphics g2 = Graphics.FromImage(bmp);
                IntPtr dc1 = g1.GetHdc();
                IntPtr dc2 = g2.GetHdc();
                BitBlt(dc2, 0, 0, ClientRectangle.Width, ClientRectangle.Height, dc1, 0, 0, 13369376);
                g1.ReleaseHdc(dc1);
                g2.ReleaseHdc(dc2);


                //                InvertZOrderOfControls(APanel.Controls);
                //                panel1.DrawToBitmap(bmp, new System.Drawing.Rectangle(0, 0, APanel.Width, APanel.Height));
                //                InvertZOrderOfControls(APanel.Controls);

                //                Graphics g = APanel.CreateGraphics();


                //g.FillRectangle(new SolidBrush(Color.FromArgb(0, Color.Black)), p.DisplayRectangle);
                //               Pen pen = new Pen(Color.Red);
                //                g.DrawLine(pen, 0, 0, APanel.Width, APanel.Height);



                Clipboard.SetImage(bmp);

            }
            catch (Exception /*ex*/)
            {
                //CProgram.sLogException("Panel to Clipboard error", ex);

            }
        }

/*        void mPanelToClipBoard(Control APanel)
        {
            CProgram.sPromptError(true, "Print", "Do not use this function");
            try
            {
                mPrint1Bitmap = new Bitmap(APanel.Width, APanel.Height);

                using (Graphics G = Graphics.FromImage(mPrint1Bitmap))
                {
                    G.Clear(Color.White);           // needed for init
                }

                Graphics g1 = APanel.CreateGraphics();
                Image MyImage = new Bitmap(APanel.ClientRectangle.Width, APanel.ClientRectangle.Height, g1);
                Graphics g2 = Graphics.FromImage(mPrint1Bitmap);
                IntPtr dc1 = g1.GetHdc();
                IntPtr dc2 = g2.GetHdc();
                BitBlt(dc2, 0, 0, ClientRectangle.Width, ClientRectangle.Height, dc1, 0, 0, 13369376);
                g1.ReleaseHdc(dc1);
                g2.ReleaseHdc(dc2);

                Clipboard.SetImage(mPrint1Bitmap);

            }
            catch (Exception /*ex* /)
            {
                //CProgram.sLogException("Image to Clipboard error", ex);
            }
        }
*/
/*        Bitmap mPanelToImage(Control APanel, bool AbInvert, bool AbToClipBoard)
        {
            Bitmap bitmap = null;
            try
            {
                bitmap = new Bitmap(APanel.Width, APanel.Height);

                using (Graphics G = Graphics.FromImage(bitmap))
                {
                    G.Clear(Color.White);           // needed for init
                }

                if (AbInvert) InvertZOrderOfControls(APanel.Controls);
                APanel.DrawToBitmap(bitmap, new System.Drawing.Rectangle(0, 0, APanel.Width, APanel.Height));
                if (AbInvert) InvertZOrderOfControls(APanel.Controls);

                if (AbToClipBoard)
                {
                    Clipboard.SetImage(bitmap);
                }
            }
            catch (Exception /*ex * /)
            {
                //                CProgram.sLogException("Image to Clipboard error", ex);
                bitmap = null;
            }
            return bitmap;
        }
        */
        private bool mbPrintImage()
        {
            bool bOk = false;
            string fullName = "";
            bool bTestRemove = false;

            if (mPrintBitmapList != null && mPrintBitmapList.Count > 0 && mNrPages > 0)
            {
                try
                {
                    toolStripGenPages.Text = "PDF generarating...";
                    CPdfDocRec pdfDocRec = new CPdfDocRec(CDvtmsData.sGetDBaseConnection());
                    UInt32 studyIX = _mStudyInfo == null ? 0 : _mStudyInfo.mIndex_KEY;
                    //UInt16 studySubNr = //_mStudyReport._mReportNr;
                    UInt32 patientIX = _mPatientInfo == null ? 0 : _mPatientInfo.mIndex_KEY;
                    string reportType = CStudyInfo.sMergeCodeLabelQC( _mStudyReport.mGetMaskReportCode(_mStudyReport._mReportSectionMask), _mbQCd );
                    UInt32 refID = _mStudyReport.mIndex_KEY;
                    string refLetter = "S";
                    PdfDocument pdfDocument;
                    PageOrientation pageOrientation = PageOrientation.Portrait;
                    string extraName = _mbQCd ? "QC" : "";

                    bool bInitials = _mCreadedByInitials != null && _mCreadedByInitials.Length > 0;

                    if (false == bInitials)
                    {
                        if (CProgram.sbReqLabel("Print pdf", "User Initials", ref _mCreadedByInitials, "", false))
                        {
                            bInitials = _mCreadedByInitials != null && _mCreadedByInitials.Length > 0;
                        }
                    }
                    CProgram.sbLogMemSize("start Study print S" + _mStudyInfo.mIndex_KEY.ToString() + " report "
    + _mStudyReportNr.ToString() + " " + mNrPages.ToString() + " pages ");

                    if (bInitials && pdfDocRec != null
                        && null != (pdfDocument = pdfDocRec.mbNewDocument(_mCreadedByInitials, studyIX, _mStudyReportNr, patientIX, 
                                    reportType, refLetter, refID, extraName, _mStartPeriodDT, _mEndPeriodDT)))
                    {
                        Bitmap bitmap;
                        string errorStr = "";
                        bOk = true;
                        int nPages = mPrintBitmapList.Count;
                        int i = 0;

                        while(mPrintBitmapList.Count > 0)
 //                       for ( int i =  0; i < nPages; ++i )
                        {
                            if (i++ > nPages)
                            {
                                CProgram.sLogError("printing to much pages");
                                break;
                            }

                            toolStripGenPages.Text = "PDF " + i.ToString() + "/" + nPages.ToString();
                            Application.DoEvents();
                            bitmap = mPrintBitmapList[0];
                            mPrintBitmapList.Remove(bitmap);
                            if ( bitmap == null)
                            {
                                bOk = false;
                                errorStr = "bitmap null";
                            }
                            else
                            {
                                if( false == pdfDocRec.mbAddImagePage(pageOrientation, bitmap))
                                {
                                    bOk = false;
                                    errorStr = "add failed";
                                    CProgram.sLogError("Failed add page " + i.ToString() + "/" + nPages.ToString());
                                }
                                bitmap.Dispose();   // free as we added a page;
                                bitmap = null;
                            }
                        }
                        CProgram.sMemoryCleanup(true, true);
                        if (bOk == false)
                        {
                            bOk = false;
                        }
                        else if(_mbAnonymize )
                        {
                            bOk = true;
                            _mStudyReportNr = 0;
                        }
                        else if (_mStudyInfo.mbSetNextReportNr(_mStudyReportNr))
                        {
                            bOk = true;
                        }
                        else
                        {
                            bOk = false;
                            errorStr = "Report nr failed";

                        }
                        if (bOk)
                        {
                            toolStripGenPages.Text = "PDF saving...";
                            bTestRemove = true;
                            bOk = pdfDocRec.mbSaveToStudyPdf(out fullName, false == _mbAnonymize);
                            if (bOk)
                            {
                                CProgram.sbLogMemSize("saved Study print S" + _mStudyInfo.mIndex_KEY.ToString() + " report "
    + _mStudyReportNr.ToString() + " " + mNrPages.ToString() + " pages " + Path.GetFileName(fullName));

                                toolStripGenPages.Text = "PDF opening...";
                                _mReportIX = pdfDocRec.mIndex_KEY;
                                Process.Start(fullName);
                                Close();
                            }
                            else
                            {
                                toolStripGenPages.Text = "Save Failed!";
                            }
                        }
                        else
                        {
                            toolStripGenPages.Text = "PDF Failed! " + errorStr;
                        }
                        pdfDocRec.mClear(); // disposes memory faster
                     }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("PdfPrint study report failed", ex);
                    toolStripGenPages.Text = "PDF Exception!";
                    bTestRemove = true;
                }
                if (bTestRemove && _mReportIX == 0)
                {
                    try
                    {
                        bTestRemove =  fullName.Length == 0 || false == File.Exists(fullName);
                    }
                    catch (Exception ex)
                    {
                        bTestRemove = false;
                    }
                    if (bTestRemove)
                    {
                        _mStudyInfo.mbReverseNextReportNr(_mStudyReportNr); // something failed revert to previousNr Reports
                    }
                }
            }
            return bOk;
        }
/*
        private bool mbPrintImageOld()
        {
            bool bOk = false;
            try
            {
                PrintDocument pd = new PrintDocument();

                Margins oldMargins = pd.DefaultPageSettings.Margins;
                Margins margins = new Margins(50, 50, 50, 50);
                pd.DefaultPageSettings.Margins = margins;

                mCurrentPrintPage = 1;
                pd.PrintPage += mPrintPage;

                PrintDialog printDialog1 = new PrintDialog();

                if (printDialog1 != null)
                {
                    printDialog1.Document = pd;
                    DialogResult result = printDialog1.ShowDialog(this);
                    if (result == DialogResult.OK)
                    {
                        PrintPreviewDialog printPreviewDialog = new PrintPreviewDialog();

                        if (printPreviewDialog != null)
                        {
                            printPreviewDialog.ClientSize = new System.Drawing.Size(600, 900);
                            printPreviewDialog.Location = new System.Drawing.Point(10, 10);
                            printPreviewDialog.Name = "PrintPreviewDialog1";

                            // Associate the event-handling method with the  
                            // document's PrintPage event. 
                            // Set the minimum size the dialog can be resized to. 
                            printPreviewDialog.MinimumSize = new System.Drawing.Size(375, 250);

                            // Set the UseAntiAlias property to true, which will allow the  
                            // operating system to smooth fonts. 
                            printPreviewDialog.UseAntiAlias = true;

                            printPreviewDialog.Document = pd;
                            printPreviewDialog.StartPosition = FormStartPosition.CenterParent;


                            // Call the ShowDialog method. This will trigger the document's
                            //  PrintPage event.
                            printPreviewDialog.ShowDialog( this );
                        }
                        bOk = true;
                    }
               
/*
                else
                {
                    //here to select the printer attached to user PC
//                    PrintDialog printDialog1 = new PrintDialog();

                    if (printDialog1 != null)
                    {
                        printDialog1.Document = pd;
                        DialogResult result = printDialog1.ShowDialog(this);
                        if (result == DialogResult.OK)
                        {
                            pd.Print();//this will trigger the Print Event handeler PrintPage
                            bOk = true;
                        }
                    }
* /
                }

                pd.Dispose();
            }
            catch (Exception)
            {

            }
            return bOk;
        }

        //The Print Event handeler
        private void mPrintPage(object o, PrintPageEventArgs e)
        {
            try
            {
              
                Bitmap bitmap = mCurrentPrintPage <= mNrPages && mPrintBitmapList.Count >= mCurrentPrintPage ? mPrintBitmapList[ mCurrentPrintPage ] : null;  

                if (bitmap != null && bitmap.Width > 0 && bitmap.Height > 0)
                {
                    //Adjust the size of the image to the page to print the full image without loosing any part of it
                    Rectangle m = e.MarginBounds;

                    if ((double)bitmap.Width / (double)bitmap.Height > (double)m.Width / (double)m.Height) // image is wider
                    {
                        m.Height = (int)((double)bitmap.Height / (double)bitmap.Width * (double)m.Width);
                    }
                    else
                    {
                        m.Width = (int)((double)bitmap.Width / (double)bitmap.Height * (double)m.Height);
                    }
                    e.Graphics.DrawImage(bitmap, m);
                }
                else
                {
                    int i = 1;
                }
                if( ++mCurrentPrintPage <= mNrPages)
                {
                    e.HasMorePages = true;
                }
                else
                {
                    e.HasMorePages = false;
                    mCurrentPrintPage = 1;  // set ready for the print by print preview
                }

            }
            catch (Exception)
            {

            }
        }
        public static void InvertZOrderOfControls(Control.ControlCollection ControlList)
        {
            // do not process empty control list
            if (ControlList.Count == 0)
                return;
            // only re-order if list is writable
            if (!ControlList.IsReadOnly)
            {
                SortedList<int, Control> sortedChildControls = new SortedList<int, Control>();
                // find all none docked controls and sort in to list
                foreach (Control ctrlChild in ControlList)
                {
                    if (ctrlChild.Dock == DockStyle.None)
                        sortedChildControls.Add(ControlList.GetChildIndex(ctrlChild), ctrlChild);
                }
                // re-order the controls in the parent by swapping z-order of first 
                // and last controls in the list and moving towards the center
                for (int i = 0; i < sortedChildControls.Count / 2; i++)
                {
                    Control ctrlChild1 = sortedChildControls.Values[i];
                    Control ctrlChild2 = sortedChildControls.Values[sortedChildControls.Count - 1 - i];
                    int zOrder1 = ControlList.GetChildIndex(ctrlChild1);
                    int zOrder2 = ControlList.GetChildIndex(ctrlChild2);
                    ControlList.SetChildIndex(ctrlChild1, zOrder2);
                    ControlList.SetChildIndex(ctrlChild2, zOrder1);
                }
            }
            // try to invert the z-order of child controls
            foreach (Control ctrlChild in ControlList)
            {
                try { InvertZOrderOfControls(ctrlChild.Controls); }
                catch { }
            }
        }
*/
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
        }

        private void mPanalToClipboard()
        {
            Bitmap bitmap = null;
            Panel APanel = panelPrintArea1;
            try
            {
                bitmap = new Bitmap(APanel.Width, APanel.Height);

                using (Graphics G = Graphics.FromImage(bitmap))
                {
                    G.Clear(Color.White);           // needed for init
                }

                APanel.DrawToBitmap(bitmap, new System.Drawing.Rectangle(0, 0, APanel.Width, APanel.Height));

                Clipboard.SetImage(bitmap);
            }
            catch (Exception /*ex*/)
            {
                bitmap = null;
            }
        }

        public Bitmap mScaleImg(Bitmap AImage, double scale)

        {
            Bitmap result = null;

            try
            {
                int newWidth = (int)(AImage.Width * scale);
                int newHeight = (int)(AImage.Height * scale);

                result = new Bitmap(newWidth, newHeight);

                result.SetResolution(AImage.HorizontalResolution, AImage.VerticalResolution);

                using (Graphics g = Graphics.FromImage(result))

                {
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    g.CompositingQuality = CompositingQuality.HighQuality;
                    g.SmoothingMode = SmoothingMode.HighQuality;
                    g.PixelOffsetMode = PixelOffsetMode.HighQuality;

                    g.DrawImage(AImage, 0, 0, result.Width, result.Height);
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("scale bitmap failed", ex);
                result = null;
            }
            return result;

        }
        private bool mbAddPanelToImageList( Panel APanel, bool AbWarnEmptyBmp )
        {
            bool bOk = false;
            try
            {
                Bitmap bitmap = CPdfDocRec.sPanelToImage(APanel, false, AbWarnEmptyBmp);
                bOk = bitmap != null;

                if (bOk)
                {
                    if (_mbReduceImage)
                    {
                        Bitmap smallImage = mScaleImg(bitmap, 0.5);

                        if (smallImage != null)
                        {
                            bitmap.Dispose();
                            bitmap = smallImage;
                        }
                    }
                    mPrintBitmapList.Add(bitmap);
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("create bitmap from panel " + (APanel == null ? "null": APanel.Name), ex);
                bOk = false;
            }

            return bOk;
        }

        private void mCapture3Pages()
        {
            if (mPrintBitmapList != null)
            {
                mbAddPanelToImageList(panelPrintArea1, true);
                mbAddPanelToImageList(panelPrintArea2, true);

                //mPrintBitmapList.Add( CPdfDocRec.sPanelToImage(panelPrintArea1, false));// header
                //mPrintBitmapList.Add( CPdfDocRec.sPanelToImage(panelPrintArea2, false));//table

                int nRows = dataGridView.RowCount;
                int nDisplaidRows = dataGridView.DisplayedRowCount(false);
                int firstRow = dataGridView.FirstDisplayedScrollingRowIndex;

                //                labelNrStripsTable.Text = nRows.ToString();
                for (int i = 0; i < nDisplaidRows; ++i)
                {
                    dataGridView.Rows.RemoveAt(0);  // remove the shown rows
                }

                firstRow += nDisplaidRows;
                mbPrintTable = firstRow < nRows && dataGridView.RowCount > 0;
                if( mbPrintTable )
                {
                    // multiple pages;
//                    dataGridView.FirstDisplayedScrollingRowIndex = firstRow;
                    labelPage2Of.Text = "2 " + (char)('A' + mPrintTableN);
                    ++mPrintTableN;
                }
                else
                {
//                    mPrintBitmapList.Add( CPdfDocRec.sPanelToImage(panelPrintArea3, false));
                }
            }
        }
 
        private void mCapturePageN( UInt16 APageNr)
        {
            if (mPrintBitmapList != null )
            {
                if( false == mbAddPanelToImageList(panelPrintArea3, _mbWarnEmptyBmp))
                {
                    _mbWarnEmptyBmp = APageNr < 10;
                }

                //mPrintBitmapList.Add( CPdfDocRec.sPanelToImage(panelPrintArea3, false));
                // todo multiple pages
            }
        }

        private void toolStripButton1_Click_1(object sender, EventArgs e)
        {
/*            timer1.Enabled = false;
            try
            {
                mCapture3Pages();
                mbPrintImage();
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Print Analysis", ex);
            }
*/        }

        private void panel64_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel52_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label39_Click(object sender, EventArgs e)
        {

        }

        private void label57_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label104_Click(object sender, EventArgs e)
        {

        }

        private void label102_Click(object sender, EventArgs e)
        {

        }

        private void panel113_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBoxBaselineReferenceECGStrip_Click(object sender, EventArgs e)
        {

        }

        private void panel62_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel51_Paint(object sender, PaintEventArgs e)
        {

        }
        private void mUpdateMemUse()
        {
            /*           UInt32 progMB = CProgram.sGetProgUsedMemoryMB();
                       UInt32 freeMB = CProgram.sGetFreeMemoryMB();
                       toolStripMemUsage.Text = "p" + progMB.ToString() + " f" + freeMB.ToString() + "MB";
           */
            string memInfo = "";
            toolStripMemUsage.BackColor = CProgram.sbCheckChangedMemSize(out memInfo) ? toolStripClipboard1.BackColor : Color.Orange;
            toolStripMemUsage.Text = memInfo;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;

            try
            {
                mUpdateMemUse();

                if (mbFirstPage)
                {
                    toolStripGenPages.Text = "First Page " + mCurrentPrintPage.ToString() + " / " + mNrPages.ToString();
                    mCapture3Pages();
                    mbFirstPage = false;
                    timer1.Interval = 1000;
                    timer1.Enabled = true;

                }
                else if (mbPrintTable)
                {
                    mbAddPanelToImageList(panelPrintArea2, true);

                    //mPrintBitmapList.Add(CPdfDocRec.sPanelToImage(panelPrintArea2, false));//table
                    toolStripGenPages.Text = "Table Page " + mCurrentPrintPage.ToString() + " / " + mNrPages.ToString();

                    int nRows = dataGridView.RowCount;
                    int nDisplaidRows = dataGridView.DisplayedRowCount(false);
                    int firstRow = dataGridView.FirstDisplayedScrollingRowIndex;

                    for( int i = 0; i < nDisplaidRows; ++i)
                    {
                        dataGridView.Rows.RemoveAt(0);  // remove the shown rows
                    }
                    firstRow += nDisplaidRows;
                    mbPrintTable = firstRow < nRows && dataGridView.RowCount > 0;
                    if (mbPrintTable)
                    {
                        // multiple pages;
                        // still must be at top because we have removed rows dataGridView.FirstDisplayedScrollingRowIndex = 0; // firstRow;
                        labelPage2Of.Text = "2 " + (char)('A' + mPrintTableN);
                        ++mPrintTableN;
                    }
                    else
                    {
                        //mPrintBitmapList.Add(CPdfDocRec.sPanelToImage(panelPrintArea3, false));
                    }
                    timer1.Interval = 1000;
                    timer1.Enabled = true;
                }
                else
                {
                    mCapturePageN(mCurrentPrintPage);

                    ++mCurrentPrintPage;
                    toolStripGenPages.Text = "Page " + mCurrentPrintPage.ToString() + " / " + mNrPages.ToString();

                    if (mCurrentPrintPage <= mNrPages)
                    {
                        labelPage3Of.Text = mCurrentPrintPage.ToString();
                        mInitSample3();
                        mInitSample4();
                        mInitSample5();
                        timer1.Interval = 1000;


                        if(mCurrentPrintPage % 5 == 0)
                        {
                            CProgram.sMemoryCleanup(true, true);
                        }
                        timer1.Enabled = true;
                    }
                    else
                    {
                        timer1.Enabled = false;
                        toolStripGenPages.Text = "Done.";
                        CProgram.sMemoryCleanup(true, true);
                        if (mbPrintImage())
                        {
                            CProgram.sMemoryCleanup(true, true);

                            while ( mPrintBitmapList.Count > 0)
                            {
                                Bitmap bitmap = mPrintBitmapList[0];

                                mPrintBitmapList.Remove(bitmap);
                                bitmap.Dispose();
                            }
                            CProgram.sMemoryCleanup(true, true);
                            Close();
                        }
                    }
                }
            }
            catch ( Exception ex)
            {
                CProgram.sLogException("Print Study Report", ex);
                toolStripGenPages.Text = "Error " + toolStripGenPages.Text;

            }
        }

        private void CPrintStudyEventsForm_Shown(object sender, EventArgs e)
        {
            BringToFront();
            timer1.Enabled = true;
            toolStrip1.Focus();
            //            mPanelToClipBoard1(panelPrintArea);
            Application.DoEvents();
        }

        private void labelRatemeasureSI_Click(object sender, EventArgs e)
        {

        }

        private void CPrintStudyEventsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            timer1.Enabled = false;
        }

        private void toolStripClipboard_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;

            if (mPrintBitmapList != null && mPrintBitmapList.Count > 0)
            {
                Clipboard.SetImage(mPrintBitmapList[0]);
            }
        }

        /*        private bool mbEnterLable( Label ArLabel, string AName )
                {
                    bool bOk = false;

                    if( ArLabel != null )
                    {
                        string s = ArLabel.Text;

                            if( CProgram.sbReqLabel( "Enter Analysis header", "Patient Last name", ref s, "" ))
                        {
                            ArLabel.Text = s;
                            bOk = true;

                        }
                    }
                    return bOk;
                }
        */
        private void toolStripEnterData_Click(object sender, EventArgs e)
        {

        }

        private void dataGridViewPrevThreeTX_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void labelPhysSignLine_Click(object sender, EventArgs e)
        {

        }

        private void labelSerialNrHeader_Click(object sender, EventArgs e)
        {

        }

        private void labelPhysPrintName_Click(object sender, EventArgs e)
        {

        }

        private void label53_Click(object sender, EventArgs e)
        {

        }

        private void label93_Click(object sender, EventArgs e)
        {

        }

        private void label78_Click(object sender, EventArgs e)
        {

        }

        private void label19_Click(object sender, EventArgs e)
        {

        }

        private void toolStripClipboard2_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;

            if (mPrintBitmapList != null && mPrintBitmapList.Count >= 2)
            {
                Clipboard.SetImage(mPrintBitmapList[1]);
            }
        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void labelPatientNameHeader_Click(object sender, EventArgs e)
        {

        }

        private void label46_Click(object sender, EventArgs e)
        {

        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void labelCardiacEventReportNr_Click(object sender, EventArgs e)
        {

        }

        private void panel68_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label72_Click(object sender, EventArgs e)
        {

        }

        private void panel122_Paint(object sender, PaintEventArgs e)
        {

        }

        private void toolStripButton1_Click_2(object sender, EventArgs e)
        {
            if (mPrintBitmapList != null && mPrintBitmapList.Count >= 3)
            {
                Clipboard.SetImage(mPrintBitmapList[2]);
            }
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            if (mPrintBitmapList != null && mPrintBitmapList.Count >= 4)
            {
                Clipboard.SetImage(mPrintBitmapList[3]);
            }

        }

        private void panel77_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panelTablePrevious3Trans_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label115_Click(object sender, EventArgs e)
        {

        }

        private void labelPage2PatLastName_Click(object sender, EventArgs e)
        {

        }

        private void labelDiagnosis_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void labelHrMinIX_Click(object sender, EventArgs e)
        {

        }

        private void labelSample3Lead_Click(object sender, EventArgs e)
        {

        }

        private void labelSample3LeadFull_Click(object sender, EventArgs e)
        {

        }

        private void labelSample4LeadFull_Click(object sender, EventArgs e)
        {

        }

        private void panel126_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel48_Paint(object sender, PaintEventArgs e)
        {

        }

        private void labelQrsUnit5_Click(object sender, EventArgs e)
        {

        }

        private void panel276_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label40_Click(object sender, EventArgs e)
        {

        }

        private void label53_Click_1(object sender, EventArgs e)
        {

        }

        private void labelStartDate_Click(object sender, EventArgs e)
        {

        }

        private void labelReportSignDate_Click(object sender, EventArgs e)
        {

        }
    }
}
