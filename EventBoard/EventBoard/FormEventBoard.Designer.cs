﻿namespace EventBoard
{
    partial class FormEventBoard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormEventBoard));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolStripTop = new System.Windows.Forms.ToolStrip();
            this.toolStripWorkNew = new System.Windows.Forms.ToolStripButton();
            this.toolStripWorld = new System.Windows.Forms.ToolStripButton();
            this.toolStripPatientList = new System.Windows.Forms.ToolStripButton();
            this.toolStripDepartments = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonTrial = new System.Windows.Forms.ToolStripButton();
            this.toolStripPhysician = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripDevices = new System.Windows.Forms.ToolStripButton();
            this.toolStripAddDevice = new System.Windows.Forms.ToolStripButton();
            this.toolStripAlarmSignal = new System.Windows.Forms.ToolStripButton();
            this.toolStripNewEvent = new System.Windows.Forms.ToolStripButton();
            this.toolStripLoad = new System.Windows.Forms.ToolStripButton();
            this.toolStripListReports = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripExit = new System.Windows.Forms.ToolStripButton();
            this.toolStripUserCancel = new System.Windows.Forms.ToolStripButton();
            this.toolStripUserLabel = new System.Windows.Forms.ToolStripLabel();
            this.toolStripUserOk = new System.Windows.Forms.ToolStripButton();
            this.toolStripUserLock = new System.Windows.Forms.ToolStripButton();
            this.toolStripSelectUser = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSettings = new System.Windows.Forms.ToolStripButton();
            this.toolStripHelpdesk = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonUpdate2 = new System.Windows.Forms.ToolStripButton();
            this.toolTriageUpdTime2 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripUsers = new System.Windows.Forms.ToolStripButton();
            this.toolStripCustomers = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonWarn = new System.Windows.Forms.ToolStripButton();
            this.toolStripReaderCheck = new System.Windows.Forms.ToolStripLabel();
            this.contextMenuSettings = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.globalSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.showDashBoardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panelStatsUser = new System.Windows.Forms.Panel();
            this.panel46 = new System.Windows.Forms.Panel();
            this.labelViewMode1 = new System.Windows.Forms.Label();
            this.panelLoadStats = new System.Windows.Forms.Panel();
            this.labelUpdateTS = new System.Windows.Forms.Label();
            this.labelUpdateTC = new System.Windows.Forms.Label();
            this.labelStudyIndex = new System.Windows.Forms.Label();
            this.labelRecIndex = new System.Windows.Forms.Label();
            this.panelUserInfo = new System.Windows.Forms.Panel();
            this.panel21 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.label2UserMsg = new System.Windows.Forms.Label();
            this.ValueUserMsg = new System.Windows.Forms.Label();
            this.label1UserMsg = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.buttonRefresh = new System.Windows.Forms.Button();
            this.panel43 = new System.Windows.Forms.Panel();
            this.button8 = new System.Windows.Forms.Button();
            this.labelTime = new System.Windows.Forms.Label();
            this.labelDate = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label2EventReport = new System.Windows.Forms.Label();
            this.valueEventReport = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label2EventAnalyze = new System.Windows.Forms.Label();
            this.valueEventAnalyze = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label2EventTriage = new System.Windows.Forms.Label();
            this.valueEventTriage = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label2EventTotalOpen = new System.Windows.Forms.Label();
            this.valueEventTotalOpen = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.label2EventDone24 = new System.Windows.Forms.Label();
            this.valueEventDone24 = new System.Windows.Forms.Label();
            this.label1EventDone24 = new System.Windows.Forms.Label();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel19 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label2ReportToDo = new System.Windows.Forms.Label();
            this.ValueReportToDo24 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label2ProcedureOpen = new System.Windows.Forms.Label();
            this.valueStudyOpen = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label2ProcedureDone24 = new System.Windows.Forms.Label();
            this.valueStudyDone24 = new System.Windows.Forms.Label();
            this.label1ProcedureDone24 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.panel16 = new System.Windows.Forms.Panel();
            this.timerTriage = new System.Windows.Forms.Timer(this.components);
            this.contextMenuState = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.panelViewButtons = new System.Windows.Forms.Panel();
            this.panel508 = new System.Windows.Forms.Panel();
            this.panel510 = new System.Windows.Forms.Panel();
            this.buttonShowQCd = new System.Windows.Forms.Button();
            this.buttonShowBaseLine = new System.Windows.Forms.Button();
            this.buttonShowExclude = new System.Windows.Forms.Button();
            this.buttonShowLeadOff = new System.Windows.Forms.Button();
            this.buttonShowNoise = new System.Windows.Forms.Button();
            this.buttonShowReported = new System.Windows.Forms.Button();
            this.buttonShowAnalyzed = new System.Windows.Forms.Button();
            this.labelCountedAnalyzed = new System.Windows.Forms.Label();
            this.buttonShowTriaged = new System.Windows.Forms.Button();
            this.labelCountedTriaged = new System.Windows.Forms.Label();
            this.buttonShowReceived = new System.Windows.Forms.Button();
            this.labelCountedReceived = new System.Windows.Forms.Label();
            this.buttonShowAll = new System.Windows.Forms.Button();
            this.labelCountedAll = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.labelTriageCountDays = new System.Windows.Forms.Label();
            this.panel693 = new System.Windows.Forms.Panel();
            this.labelCountRows = new System.Windows.Forms.Label();
            this.buttonShowReport = new System.Windows.Forms.Button();
            this.buttonShowAnalyze = new System.Windows.Forms.Button();
            this.buttonShowTriage = new System.Windows.Forms.Button();
            this.panelViewColorStrip = new System.Windows.Forms.Panel();
            this.panelDataGrid = new System.Windows.Forms.Panel();
            this.dataGridTriage = new System.Windows.Forms.DataGridView();
            this.Count = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Patient = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Event = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Read = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Remark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EventStrip = new System.Windows.Forms.DataGridViewImageColumn();
            this.Triage = new System.Windows.Forms.DataGridViewImageColumn();
            this.Index = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.State = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FilePath = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NrSignals = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Study = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Active = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ModelSnr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReadUTC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumTrailer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel24 = new System.Windows.Forms.Panel();
            this.panelViewMode = new System.Windows.Forms.Panel();
            this.listBoxEventMode = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.panel64 = new System.Windows.Forms.Panel();
            this.labelImageNk = new System.Windows.Forms.Label();
            this.labelLoadImages = new System.Windows.Forms.Label();
            this.panel38 = new System.Windows.Forms.Panel();
            this.labelMemUsage = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelUpdTimeStrip = new System.Windows.Forms.Label();
            this.labelUpdTimeTable = new System.Windows.Forms.Label();
            this.panelRight61 = new System.Windows.Forms.Panel();
            this.labelDrive = new System.Windows.Forms.Label();
            this.buttonTriageFields = new System.Windows.Forms.Button();
            this.panel39 = new System.Windows.Forms.Panel();
            this.checkBoxAllStudy = new System.Windows.Forms.CheckBox();
            this.panel47 = new System.Windows.Forms.Panel();
            this.panel62 = new System.Windows.Forms.Panel();
            this.panelViewDec = new System.Windows.Forms.Panel();
            this.panelViewInc = new System.Windows.Forms.Panel();
            this.textBoxViewTime = new System.Windows.Forms.TextBox();
            this.listBoxViewUnit = new System.Windows.Forms.ListBox();
            this.panel30 = new System.Windows.Forms.Panel();
            this.panel28 = new System.Windows.Forms.Panel();
            this.panel48 = new System.Windows.Forms.Panel();
            this.labelSelectPermisions = new System.Windows.Forms.Label();
            this.panel53 = new System.Windows.Forms.Panel();
            this.comboBoxLocation = new System.Windows.Forms.ComboBox();
            this.panel54 = new System.Windows.Forms.Panel();
            this.checkBoxStudyPermissions = new System.Windows.Forms.CheckBox();
            this.labelClient = new System.Windows.Forms.Label();
            this.checkBoxClient = new System.Windows.Forms.CheckBox();
            this.labelTrial = new System.Windows.Forms.Label();
            this.checkBoxTrial = new System.Windows.Forms.CheckBox();
            this.panel52 = new System.Windows.Forms.Panel();
            this.panelSelectView = new System.Windows.Forms.Panel();
            this.textBoxSelectStudy = new System.Windows.Forms.TextBox();
            this.labelSelectStudy = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxSelectDevice = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxSelectPatID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxSelectName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxSelectType = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxSelectRem = new System.Windows.Forms.TextBox();
            this.panel23 = new System.Windows.Forms.Panel();
            this.buttonClear = new System.Windows.Forms.Button();
            this.panelView = new System.Windows.Forms.Panel();
            this.panel32 = new System.Windows.Forms.Panel();
            this.checkBoxHideS0 = new System.Windows.Forms.CheckBox();
            this.checkBoxAnonymize = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.radioButtonViewAll = new System.Windows.Forms.RadioButton();
            this.radioButtonCurrentPatID = new System.Windows.Forms.RadioButton();
            this.radioButtonCurrentDevice = new System.Windows.Forms.RadioButton();
            this.radioButtonNoStudy = new System.Windows.Forms.RadioButton();
            this.panel14 = new System.Windows.Forms.Panel();
            this.comboBoxTriageSort = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBoxEventShowCh = new System.Windows.Forms.ComboBox();
            this.panel26 = new System.Windows.Forms.Panel();
            this.panel27 = new System.Windows.Forms.Panel();
            this.buttonSelectLast = new System.Windows.Forms.Button();
            this.panel59 = new System.Windows.Forms.Panel();
            this.panel31 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.checkBoxUseLimit = new System.Windows.Forms.CheckBox();
            this.panel44 = new System.Windows.Forms.Panel();
            this.labelTriageRow = new System.Windows.Forms.Label();
            this.panel40 = new System.Windows.Forms.Panel();
            this.labelRowLimit = new System.Windows.Forms.Label();
            this.panel22 = new System.Windows.Forms.Panel();
            this.panel60 = new System.Windows.Forms.Panel();
            this.buttonSelectFirst = new System.Windows.Forms.Button();
            this.panelEventInfo = new System.Windows.Forms.Panel();
            this.panel25 = new System.Windows.Forms.Panel();
            this.panelStripInfo = new System.Windows.Forms.Panel();
            this.textStudyFreeNote = new System.Windows.Forms.TextBox();
            this.panel29 = new System.Windows.Forms.Panel();
            this.textBoxRecordDir = new System.Windows.Forms.TextBox();
            this.labelStudyTrial = new System.Windows.Forms.Label();
            this.labelStudyClient = new System.Windows.Forms.Label();
            this.panel36 = new System.Windows.Forms.Panel();
            this.labelStudyNrEvents = new System.Windows.Forms.Label();
            this.buttonFindings = new System.Windows.Forms.Button();
            this.panel58 = new System.Windows.Forms.Panel();
            this.labelModelNr = new System.Windows.Forms.Label();
            this.panel34 = new System.Windows.Forms.Panel();
            this.textBoxStudyRemark = new System.Windows.Forms.TextBox();
            this.panel35 = new System.Windows.Forms.Panel();
            this.labelStudyPermissions = new System.Windows.Forms.Label();
            this.labelStudyReports = new System.Windows.Forms.Label();
            this.labelTriageStudy = new System.Windows.Forms.Label();
            this.labelStudyIdText = new System.Windows.Forms.Label();
            this.buttonPdfView = new System.Windows.Forms.Button();
            this.panel33 = new System.Windows.Forms.Panel();
            this.labelTriagePatient = new System.Windows.Forms.Label();
            this.panel57 = new System.Windows.Forms.Panel();
            this.labelPatientID = new System.Windows.Forms.Label();
            this.labelPatIdText = new System.Windows.Forms.Label();
            this.panelPriority = new System.Windows.Forms.Panel();
            this.panel50 = new System.Windows.Forms.Panel();
            this.buttonLow = new System.Windows.Forms.Button();
            this.panel51 = new System.Windows.Forms.Panel();
            this.buttonNormal2 = new System.Windows.Forms.Button();
            this.panel37 = new System.Windows.Forms.Panel();
            this.buttonHigh = new System.Windows.Forms.Button();
            this.panel49 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.panel41 = new System.Windows.Forms.Panel();
            this.labelEventStudyInfo = new System.Windows.Forms.Label();
            this.panel42 = new System.Windows.Forms.Panel();
            this.labelNrChannels = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.panel63 = new System.Windows.Forms.Panel();
            this.labelRevertS0 = new System.Windows.Forms.Label();
            this.labelRecNr = new System.Windows.Forms.Label();
            this.labelEventText = new System.Windows.Forms.Label();
            this.comboBoxTriageChannel = new System.Windows.Forms.ComboBox();
            this.panel66 = new System.Windows.Forms.Panel();
            this.buttonAnalyzeAdd = new System.Windows.Forms.Button();
            this.Anayze = new System.Windows.Forms.Button();
            this.buttonSetState = new System.Windows.Forms.Button();
            this.panel45 = new System.Windows.Forms.Panel();
            this.FilterList = new System.Windows.Forms.CheckedListBox();
            this.label5 = new System.Windows.Forms.Label();
            this.panelTriageStrip0 = new System.Windows.Forms.Panel();
            this.labelFullStrip0 = new System.Windows.Forms.Label();
            this.contextMenuQuickStat = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.quickStatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quickTachyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quickBradyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quickPauseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quickAFIBToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quickOtherToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quickPACToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quickPVCToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quickPaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quickVTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quickVFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quickAFlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quickASysToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quickNormalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quickAbNormalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuHelp = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.openManualToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openChangeHystoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openDownloadLinksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openLicenseInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openContentFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.changeUserPinToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem11 = new System.Windows.Forms.ToolStripSeparator();
            this.programInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.programInfoToClipboardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.openLogFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.addLogLineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newLogFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.openRecorderFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.heckDBReaderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkDBCollectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testFilesDurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createTestFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.countTestFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readTestFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteTestFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recordingCreateTestFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recordingCountTestFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recordingTestFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recordingDeleteTestFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.folderCreateTestFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.folderCountTestFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.folderReadTestFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.folderDeleteTestFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.makeLauncherVersionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updWindowSizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuHelpDesk = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.startTMIHelpdeskToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startStandardTeamViewerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panelTriageStrip1 = new System.Windows.Forms.Panel();
            this.labelFullStrip1 = new System.Windows.Forms.Label();
            this.panelTriageStrip2 = new System.Windows.Forms.Panel();
            this.labelFullStrip2 = new System.Windows.Forms.Label();
            this.folderBrowserDialogTest = new System.Windows.Forms.FolderBrowserDialog();
            this.toolStripTop.SuspendLayout();
            this.contextMenuSettings.SuspendLayout();
            this.panelStatsUser.SuspendLayout();
            this.panel46.SuspendLayout();
            this.panelLoadStats.SuspendLayout();
            this.panelUserInfo.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel43.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel7.SuspendLayout();
            this.contextMenuState.SuspendLayout();
            this.panelViewButtons.SuspendLayout();
            this.panel508.SuspendLayout();
            this.panel510.SuspendLayout();
            this.panel693.SuspendLayout();
            this.panelDataGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridTriage)).BeginInit();
            this.panel24.SuspendLayout();
            this.panelViewMode.SuspendLayout();
            this.panel64.SuspendLayout();
            this.panel38.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panelRight61.SuspendLayout();
            this.panel39.SuspendLayout();
            this.panel47.SuspendLayout();
            this.panel62.SuspendLayout();
            this.panel28.SuspendLayout();
            this.panel48.SuspendLayout();
            this.panel53.SuspendLayout();
            this.panelSelectView.SuspendLayout();
            this.panelView.SuspendLayout();
            this.panel26.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel59.SuspendLayout();
            this.panel31.SuspendLayout();
            this.panel44.SuspendLayout();
            this.panel40.SuspendLayout();
            this.panelEventInfo.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panelStripInfo.SuspendLayout();
            this.panel29.SuspendLayout();
            this.panel36.SuspendLayout();
            this.panel58.SuspendLayout();
            this.panel34.SuspendLayout();
            this.panel35.SuspendLayout();
            this.panel33.SuspendLayout();
            this.panel57.SuspendLayout();
            this.panelPriority.SuspendLayout();
            this.panel50.SuspendLayout();
            this.panel51.SuspendLayout();
            this.panel37.SuspendLayout();
            this.panel49.SuspendLayout();
            this.panel41.SuspendLayout();
            this.panel42.SuspendLayout();
            this.panel63.SuspendLayout();
            this.panel66.SuspendLayout();
            this.panel45.SuspendLayout();
            this.panelTriageStrip0.SuspendLayout();
            this.contextMenuQuickStat.SuspendLayout();
            this.contextMenuHelp.SuspendLayout();
            this.contextMenuHelpDesk.SuspendLayout();
            this.panelTriageStrip1.SuspendLayout();
            this.panelTriageStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripTop
            // 
            this.toolStripTop.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStripTop.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripWorkNew,
            this.toolStripWorld,
            this.toolStripPatientList,
            this.toolStripDepartments,
            this.toolStripButtonTrial,
            this.toolStripPhysician,
            this.toolStripButton1,
            this.toolStripDevices,
            this.toolStripAddDevice,
            this.toolStripAlarmSignal,
            this.toolStripNewEvent,
            this.toolStripLoad,
            this.toolStripListReports,
            this.toolStripSeparator1,
            this.toolStripSeparator2,
            this.toolStripExit,
            this.toolStripUserCancel,
            this.toolStripUserLabel,
            this.toolStripUserOk,
            this.toolStripUserLock,
            this.toolStripSelectUser,
            this.toolStripSettings,
            this.toolStripHelpdesk,
            this.toolStripButtonUpdate2,
            this.toolTriageUpdTime2,
            this.toolStripUsers,
            this.toolStripCustomers,
            this.toolStripButton2,
            this.toolStripButtonWarn,
            this.toolStripReaderCheck});
            this.toolStripTop.Location = new System.Drawing.Point(0, 0);
            this.toolStripTop.Name = "toolStripTop";
            this.toolStripTop.Size = new System.Drawing.Size(1691, 44);
            this.toolStripTop.TabIndex = 0;
            this.toolStripTop.Text = "toolStrip";
            this.toolStripTop.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStrip1_ItemClicked);
            // 
            // toolStripWorkNew
            // 
            this.toolStripWorkNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripWorkNew.Image = ((System.Drawing.Image)(resources.GetObject("toolStripWorkNew.Image")));
            this.toolStripWorkNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripWorkNew.Name = "toolStripWorkNew";
            this.toolStripWorkNew.Size = new System.Drawing.Size(36, 41);
            this.toolStripWorkNew.Text = "toolStripWorkNew";
            this.toolStripWorkNew.ToolTipText = "New Study";
            this.toolStripWorkNew.Click += new System.EventHandler(this.toolStripWorkNew_Click);
            // 
            // toolStripWorld
            // 
            this.toolStripWorld.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripWorld.Image = ((System.Drawing.Image)(resources.GetObject("toolStripWorld.Image")));
            this.toolStripWorld.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripWorld.Name = "toolStripWorld";
            this.toolStripWorld.Size = new System.Drawing.Size(36, 41);
            this.toolStripWorld.Text = "toolStripWork";
            this.toolStripWorld.ToolTipText = "Study List";
            this.toolStripWorld.Click += new System.EventHandler(this.toolStripWorld_Click);
            // 
            // toolStripPatientList
            // 
            this.toolStripPatientList.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripPatientList.Image = ((System.Drawing.Image)(resources.GetObject("toolStripPatientList.Image")));
            this.toolStripPatientList.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripPatientList.Name = "toolStripPatientList";
            this.toolStripPatientList.Size = new System.Drawing.Size(36, 41);
            this.toolStripPatientList.Text = "Patient List";
            this.toolStripPatientList.Click += new System.EventHandler(this.toolStripPatientList_Click);
            // 
            // toolStripDepartments
            // 
            this.toolStripDepartments.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripDepartments.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDepartments.Image")));
            this.toolStripDepartments.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDepartments.Name = "toolStripDepartments";
            this.toolStripDepartments.Size = new System.Drawing.Size(36, 41);
            this.toolStripDepartments.Text = "toolStripDepartments";
            this.toolStripDepartments.ToolTipText = "Client List";
            this.toolStripDepartments.Click += new System.EventHandler(this.toolStripDepartments_Click);
            // 
            // toolStripButtonTrial
            // 
            this.toolStripButtonTrial.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonTrial.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonTrial.Image")));
            this.toolStripButtonTrial.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonTrial.Name = "toolStripButtonTrial";
            this.toolStripButtonTrial.Size = new System.Drawing.Size(36, 41);
            this.toolStripButtonTrial.Text = "toolStripButton3";
            this.toolStripButtonTrial.ToolTipText = "Trial";
            this.toolStripButtonTrial.Click += new System.EventHandler(this.toolStripButtonTrial_Click);
            // 
            // toolStripPhysician
            // 
            this.toolStripPhysician.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripPhysician.Image = ((System.Drawing.Image)(resources.GetObject("toolStripPhysician.Image")));
            this.toolStripPhysician.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripPhysician.Name = "toolStripPhysician";
            this.toolStripPhysician.Size = new System.Drawing.Size(36, 41);
            this.toolStripPhysician.ToolTipText = "Physicians";
            this.toolStripPhysician.Click += new System.EventHandler(this.toolStripPhysician_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(36, 41);
            this.toolStripButton1.Text = "toolStripButton1";
            this.toolStripButton1.ToolTipText = "New Device model";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripDevices
            // 
            this.toolStripDevices.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripDevices.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDevices.Image")));
            this.toolStripDevices.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDevices.Name = "toolStripDevices";
            this.toolStripDevices.Size = new System.Drawing.Size(36, 41);
            this.toolStripDevices.Text = "toolStripDevices";
            this.toolStripDevices.ToolTipText = "Device List";
            this.toolStripDevices.Visible = false;
            this.toolStripDevices.Click += new System.EventHandler(this.toolStripDevices_Click);
            // 
            // toolStripAddDevice
            // 
            this.toolStripAddDevice.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripAddDevice.Image = ((System.Drawing.Image)(resources.GetObject("toolStripAddDevice.Image")));
            this.toolStripAddDevice.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripAddDevice.Name = "toolStripAddDevice";
            this.toolStripAddDevice.Size = new System.Drawing.Size(36, 41);
            this.toolStripAddDevice.Text = "View Device";
            this.toolStripAddDevice.Click += new System.EventHandler(this.toolStripAddDevice_Click);
            // 
            // toolStripAlarmSignal
            // 
            this.toolStripAlarmSignal.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripAlarmSignal.Enabled = false;
            this.toolStripAlarmSignal.Image = ((System.Drawing.Image)(resources.GetObject("toolStripAlarmSignal.Image")));
            this.toolStripAlarmSignal.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripAlarmSignal.Name = "toolStripAlarmSignal";
            this.toolStripAlarmSignal.Size = new System.Drawing.Size(36, 41);
            this.toolStripAlarmSignal.Text = "toolStripAlarmSignal";
            this.toolStripAlarmSignal.Visible = false;
            // 
            // toolStripNewEvent
            // 
            this.toolStripNewEvent.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripNewEvent.Enabled = false;
            this.toolStripNewEvent.Image = ((System.Drawing.Image)(resources.GetObject("toolStripNewEvent.Image")));
            this.toolStripNewEvent.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripNewEvent.Name = "toolStripNewEvent";
            this.toolStripNewEvent.Size = new System.Drawing.Size(36, 41);
            this.toolStripNewEvent.Text = "toolStripNewEvent";
            this.toolStripNewEvent.ToolTipText = "Add new Event";
            this.toolStripNewEvent.Visible = false;
            // 
            // toolStripLoad
            // 
            this.toolStripLoad.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripLoad.Enabled = false;
            this.toolStripLoad.Image = ((System.Drawing.Image)(resources.GetObject("toolStripLoad.Image")));
            this.toolStripLoad.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripLoad.Name = "toolStripLoad";
            this.toolStripLoad.Size = new System.Drawing.Size(36, 41);
            this.toolStripLoad.Text = "toolStripLoad";
            this.toolStripLoad.ToolTipText = "Load event from file";
            this.toolStripLoad.Visible = false;
            // 
            // toolStripListReports
            // 
            this.toolStripListReports.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripListReports.Enabled = false;
            this.toolStripListReports.Image = ((System.Drawing.Image)(resources.GetObject("toolStripListReports.Image")));
            this.toolStripListReports.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripListReports.Name = "toolStripListReports";
            this.toolStripListReports.Size = new System.Drawing.Size(36, 41);
            this.toolStripListReports.Text = "toolStripButton2";
            this.toolStripListReports.ToolTipText = "List Study Reports";
            this.toolStripListReports.Visible = false;
            this.toolStripListReports.Click += new System.EventHandler(this.toolStripListReports_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 44);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 44);
            // 
            // toolStripExit
            // 
            this.toolStripExit.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripExit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripExit.Image = ((System.Drawing.Image)(resources.GetObject("toolStripExit.Image")));
            this.toolStripExit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripExit.Name = "toolStripExit";
            this.toolStripExit.Size = new System.Drawing.Size(36, 41);
            this.toolStripExit.Text = "Exit";
            this.toolStripExit.ToolTipText = "Exit";
            this.toolStripExit.Click += new System.EventHandler(this.toolStripExit_Click);
            // 
            // toolStripUserCancel
            // 
            this.toolStripUserCancel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripUserCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripUserCancel.Image = ((System.Drawing.Image)(resources.GetObject("toolStripUserCancel.Image")));
            this.toolStripUserCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripUserCancel.Name = "toolStripUserCancel";
            this.toolStripUserCancel.Size = new System.Drawing.Size(36, 41);
            this.toolStripUserCancel.Text = "toolStripUserCancel";
            this.toolStripUserCancel.ToolTipText = "Clear User";
            this.toolStripUserCancel.Click += new System.EventHandler(this.toolStripUserCancel_Click);
            // 
            // toolStripUserLabel
            // 
            this.toolStripUserLabel.ActiveLinkColor = System.Drawing.Color.DarkSeaGreen;
            this.toolStripUserLabel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripUserLabel.AutoToolTip = true;
            this.toolStripUserLabel.BackColor = System.Drawing.Color.CornflowerBlue;
            this.toolStripUserLabel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.toolStripUserLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripUserLabel.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold);
            this.toolStripUserLabel.Name = "toolStripUserLabel";
            this.toolStripUserLabel.Size = new System.Drawing.Size(49, 41);
            this.toolStripUserLabel.Text = "SVlr";
            this.toolStripUserLabel.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.toolStripUserLabel.ToolTipText = "User Label";
            // 
            // toolStripUserOk
            // 
            this.toolStripUserOk.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripUserOk.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripUserOk.Image = ((System.Drawing.Image)(resources.GetObject("toolStripUserOk.Image")));
            this.toolStripUserOk.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripUserOk.Name = "toolStripUserOk";
            this.toolStripUserOk.Size = new System.Drawing.Size(36, 41);
            this.toolStripUserOk.ToolTipText = "Reactivate User";
            this.toolStripUserOk.Click += new System.EventHandler(this.toolStripUserOk_Click);
            // 
            // toolStripUserLock
            // 
            this.toolStripUserLock.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripUserLock.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripUserLock.Image = ((System.Drawing.Image)(resources.GetObject("toolStripUserLock.Image")));
            this.toolStripUserLock.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripUserLock.Name = "toolStripUserLock";
            this.toolStripUserLock.Size = new System.Drawing.Size(36, 41);
            this.toolStripUserLock.ToolTipText = "Lock User";
            this.toolStripUserLock.Click += new System.EventHandler(this.toolStripUserLock_Click);
            // 
            // toolStripSelectUser
            // 
            this.toolStripSelectUser.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSelectUser.Enabled = false;
            this.toolStripSelectUser.Name = "toolStripSelectUser";
            this.toolStripSelectUser.Size = new System.Drawing.Size(118, 44);
            this.toolStripSelectUser.Text = "Select User...";
            this.toolStripSelectUser.ToolTipText = "Select User";
            this.toolStripSelectUser.Click += new System.EventHandler(this.toolStripDropDownButton1_Click);
            this.toolStripSelectUser.TextChanged += new System.EventHandler(this.toolStripSelectUser_TextChanged);
            // 
            // toolStripSettings
            // 
            this.toolStripSettings.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSettings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripSettings.Enabled = false;
            this.toolStripSettings.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSettings.Image")));
            this.toolStripSettings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSettings.Name = "toolStripSettings";
            this.toolStripSettings.Size = new System.Drawing.Size(36, 41);
            this.toolStripSettings.Text = "toolStripSettings";
            this.toolStripSettings.ToolTipText = "Settings";
            // 
            // toolStripHelpdesk
            // 
            this.toolStripHelpdesk.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripHelpdesk.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripHelpdesk.Image = ((System.Drawing.Image)(resources.GetObject("toolStripHelpdesk.Image")));
            this.toolStripHelpdesk.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripHelpdesk.Name = "toolStripHelpdesk";
            this.toolStripHelpdesk.Size = new System.Drawing.Size(36, 41);
            this.toolStripHelpdesk.Text = "Helpdesk";
            this.toolStripHelpdesk.Click += new System.EventHandler(this.toolStripHelpdesk_Click);
            // 
            // toolStripButtonUpdate2
            // 
            this.toolStripButtonUpdate2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonUpdate2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonUpdate2.Image")));
            this.toolStripButtonUpdate2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonUpdate2.Name = "toolStripButtonUpdate2";
            this.toolStripButtonUpdate2.Size = new System.Drawing.Size(36, 41);
            this.toolStripButtonUpdate2.Text = "toolStripButton3";
            this.toolStripButtonUpdate2.ToolTipText = "Update List";
            this.toolStripButtonUpdate2.Click += new System.EventHandler(this.toolStripButtonUpdate2_Click);
            // 
            // toolTriageUpdTime2
            // 
            this.toolTriageUpdTime2.AutoSize = false;
            this.toolTriageUpdTime2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolTriageUpdTime2.Name = "toolTriageUpdTime2";
            this.toolTriageUpdTime2.Size = new System.Drawing.Size(51, 36);
            this.toolTriageUpdTime2.Text = "--:--";
            this.toolTriageUpdTime2.Click += new System.EventHandler(this.toolTriageUpdTime2_Click);
            // 
            // toolStripUsers
            // 
            this.toolStripUsers.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripUsers.Image = ((System.Drawing.Image)(resources.GetObject("toolStripUsers.Image")));
            this.toolStripUsers.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripUsers.Name = "toolStripUsers";
            this.toolStripUsers.Size = new System.Drawing.Size(36, 41);
            this.toolStripUsers.Text = "toolStripUsers";
            this.toolStripUsers.ToolTipText = "New User";
            this.toolStripUsers.Visible = false;
            this.toolStripUsers.Click += new System.EventHandler(this.toolStripUsers_Click);
            // 
            // toolStripCustomers
            // 
            this.toolStripCustomers.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripCustomers.Image = ((System.Drawing.Image)(resources.GetObject("toolStripCustomers.Image")));
            this.toolStripCustomers.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripCustomers.Name = "toolStripCustomers";
            this.toolStripCustomers.Size = new System.Drawing.Size(36, 41);
            this.toolStripCustomers.Text = "toolStripCustomers";
            this.toolStripCustomers.ToolTipText = "Hospital List not used";
            this.toolStripCustomers.Visible = false;
            this.toolStripCustomers.Click += new System.EventHandler(this.toolStripCustomers_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(36, 41);
            this.toolStripButton2.Text = "toolStripButton2";
            this.toolStripButton2.ToolTipText = "Help Menu";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripHelp_Click);
            // 
            // toolStripButtonWarn
            // 
            this.toolStripButtonWarn.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonWarn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonWarn.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripButtonWarn.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.toolStripButtonWarn.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonWarn.Image")));
            this.toolStripButtonWarn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonWarn.Name = "toolStripButtonWarn";
            this.toolStripButtonWarn.Size = new System.Drawing.Size(31, 41);
            this.toolStripButtonWarn.Text = "!";
            this.toolStripButtonWarn.Click += new System.EventHandler(this.toolStripButtonWarn_Click);
            // 
            // toolStripReaderCheck
            // 
            this.toolStripReaderCheck.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripReaderCheck.AutoToolTip = true;
            this.toolStripReaderCheck.BackColor = System.Drawing.Color.CornflowerBlue;
            this.toolStripReaderCheck.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.toolStripReaderCheck.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripReaderCheck.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripReaderCheck.ForeColor = System.Drawing.Color.MediumBlue;
            this.toolStripReaderCheck.Name = "toolStripReaderCheck";
            this.toolStripReaderCheck.Size = new System.Drawing.Size(160, 41);
            this.toolStripReaderCheck.Text = "?Reader svrSir! svrTZ";
            this.toolStripReaderCheck.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolStripReaderCheck.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.toolStripReaderCheck.ToolTipText = "Reader Status";
            // 
            // contextMenuSettings
            // 
            this.contextMenuSettings.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.settingsToolStripMenuItem,
            this.userSettingsToolStripMenuItem,
            this.globalSettingsToolStripMenuItem,
            this.settingsToolStripMenuItem1,
            this.showDashBoardToolStripMenuItem});
            this.contextMenuSettings.Name = "contextMenuSettings";
            this.contextMenuSettings.Size = new System.Drawing.Size(181, 136);
            this.contextMenuSettings.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuSettings_Opening);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.toolStripMenuItem1.Text = "toolStripMenuItem1";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.settingsToolStripMenuItem.Text = "Program Settings";
            // 
            // userSettingsToolStripMenuItem
            // 
            this.userSettingsToolStripMenuItem.Name = "userSettingsToolStripMenuItem";
            this.userSettingsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.userSettingsToolStripMenuItem.Text = "User Settings";
            // 
            // globalSettingsToolStripMenuItem
            // 
            this.globalSettingsToolStripMenuItem.Name = "globalSettingsToolStripMenuItem";
            this.globalSettingsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.globalSettingsToolStripMenuItem.Text = "Global Settings";
            // 
            // settingsToolStripMenuItem1
            // 
            this.settingsToolStripMenuItem1.Name = "settingsToolStripMenuItem1";
            this.settingsToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.settingsToolStripMenuItem1.Text = "Alarm Settings";
            // 
            // showDashBoardToolStripMenuItem
            // 
            this.showDashBoardToolStripMenuItem.Name = "showDashBoardToolStripMenuItem";
            this.showDashBoardToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.showDashBoardToolStripMenuItem.Text = "Show DashBoard";
            // 
            // panelStatsUser
            // 
            this.panelStatsUser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panelStatsUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelStatsUser.Controls.Add(this.panel46);
            this.panelStatsUser.Controls.Add(this.panelLoadStats);
            this.panelStatsUser.Controls.Add(this.panelUserInfo);
            this.panelStatsUser.Controls.Add(this.panel4);
            this.panelStatsUser.Controls.Add(this.panel3);
            this.panelStatsUser.Controls.Add(this.panel18);
            this.panelStatsUser.Controls.Add(this.panel2);
            this.panelStatsUser.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelStatsUser.Location = new System.Drawing.Point(0, 44);
            this.panelStatsUser.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panelStatsUser.Name = "panelStatsUser";
            this.panelStatsUser.Size = new System.Drawing.Size(1691, 79);
            this.panelStatsUser.TabIndex = 2;
            // 
            // panel46
            // 
            this.panel46.Controls.Add(this.labelViewMode1);
            this.panel46.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel46.Location = new System.Drawing.Point(764, 0);
            this.panel46.Name = "panel46";
            this.panel46.Size = new System.Drawing.Size(569, 77);
            this.panel46.TabIndex = 11;
            // 
            // labelViewMode1
            // 
            this.labelViewMode1.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelViewMode1.Font = new System.Drawing.Font("Verdana", 22F);
            this.labelViewMode1.ForeColor = System.Drawing.Color.Black;
            this.labelViewMode1.Location = new System.Drawing.Point(0, 0);
            this.labelViewMode1.Name = "labelViewMode1";
            this.labelViewMode1.Size = new System.Drawing.Size(58, 77);
            this.labelViewMode1.TabIndex = 0;
            this.labelViewMode1.Text = "XXXX";
            this.labelViewMode1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelViewMode1.Visible = false;
            // 
            // panelLoadStats
            // 
            this.panelLoadStats.Controls.Add(this.labelUpdateTS);
            this.panelLoadStats.Controls.Add(this.labelUpdateTC);
            this.panelLoadStats.Controls.Add(this.labelStudyIndex);
            this.panelLoadStats.Controls.Add(this.labelRecIndex);
            this.panelLoadStats.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelLoadStats.Location = new System.Drawing.Point(1333, 0);
            this.panelLoadStats.Name = "panelLoadStats";
            this.panelLoadStats.Size = new System.Drawing.Size(126, 77);
            this.panelLoadStats.TabIndex = 9;
            // 
            // labelUpdateTS
            // 
            this.labelUpdateTS.AutoSize = true;
            this.labelUpdateTS.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelUpdateTS.Location = new System.Drawing.Point(0, 45);
            this.labelUpdateTS.Name = "labelUpdateTS";
            this.labelUpdateTS.Size = new System.Drawing.Size(116, 16);
            this.labelUpdateTS.TabIndex = 3;
            this.labelUpdateTS.Text = "updTS=0.000sec";
            // 
            // labelUpdateTC
            // 
            this.labelUpdateTC.AutoSize = true;
            this.labelUpdateTC.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelUpdateTC.Location = new System.Drawing.Point(0, 61);
            this.labelUpdateTC.Name = "labelUpdateTC";
            this.labelUpdateTC.Size = new System.Drawing.Size(117, 16);
            this.labelUpdateTC.TabIndex = 2;
            this.labelUpdateTC.Text = "updTC=0.000sec";
            // 
            // labelStudyIndex
            // 
            this.labelStudyIndex.AutoSize = true;
            this.labelStudyIndex.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStudyIndex.Location = new System.Drawing.Point(0, 16);
            this.labelStudyIndex.Name = "labelStudyIndex";
            this.labelStudyIndex.Size = new System.Drawing.Size(25, 16);
            this.labelStudyIndex.TabIndex = 1;
            this.labelStudyIndex.Text = "S#";
            // 
            // labelRecIndex
            // 
            this.labelRecIndex.AutoSize = true;
            this.labelRecIndex.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelRecIndex.Location = new System.Drawing.Point(0, 0);
            this.labelRecIndex.Name = "labelRecIndex";
            this.labelRecIndex.Size = new System.Drawing.Size(26, 16);
            this.labelRecIndex.TabIndex = 0;
            this.labelRecIndex.Text = "R#";
            this.labelRecIndex.Click += new System.EventHandler(this.labelRecIndex_Click);
            // 
            // panelUserInfo
            // 
            this.panelUserInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelUserInfo.Controls.Add(this.panel21);
            this.panelUserInfo.Controls.Add(this.label1UserMsg);
            this.panelUserInfo.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelUserInfo.Location = new System.Drawing.Point(1459, 0);
            this.panelUserInfo.Name = "panelUserInfo";
            this.panelUserInfo.Size = new System.Drawing.Size(98, 77);
            this.panelUserInfo.TabIndex = 3;
            this.panelUserInfo.Visible = false;
            // 
            // panel21
            // 
            this.panel21.Controls.Add(this.panel15);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel21.Location = new System.Drawing.Point(0, 25);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(96, 50);
            this.panel21.TabIndex = 6;
            // 
            // panel15
            // 
            this.panel15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel15.Controls.Add(this.label2UserMsg);
            this.panel15.Controls.Add(this.ValueUserMsg);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel15.Location = new System.Drawing.Point(0, 0);
            this.panel15.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(96, 50);
            this.panel15.TabIndex = 7;
            // 
            // label2UserMsg
            // 
            this.label2UserMsg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2UserMsg.ForeColor = System.Drawing.Color.Black;
            this.label2UserMsg.Location = new System.Drawing.Point(0, 0);
            this.label2UserMsg.Name = "label2UserMsg";
            this.label2UserMsg.Size = new System.Drawing.Size(94, 21);
            this.label2UserMsg.TabIndex = 6;
            this.label2UserMsg.Text = "Messages:";
            this.label2UserMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ValueUserMsg
            // 
            this.ValueUserMsg.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ValueUserMsg.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ValueUserMsg.ForeColor = System.Drawing.Color.Black;
            this.ValueUserMsg.Location = new System.Drawing.Point(0, 21);
            this.ValueUserMsg.Name = "ValueUserMsg";
            this.ValueUserMsg.Size = new System.Drawing.Size(94, 27);
            this.ValueUserMsg.TabIndex = 5;
            this.ValueUserMsg.Text = "-";
            this.ValueUserMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1UserMsg
            // 
            this.label1UserMsg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1UserMsg.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1UserMsg.ForeColor = System.Drawing.Color.Black;
            this.label1UserMsg.Location = new System.Drawing.Point(0, 0);
            this.label1UserMsg.Name = "label1UserMsg";
            this.label1UserMsg.Size = new System.Drawing.Size(96, 25);
            this.label1UserMsg.TabIndex = 5;
            this.label1UserMsg.Text = "User";
            this.label1UserMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.panel8);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(1557, 0);
            this.panel4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(132, 77);
            this.panel4.TabIndex = 2;
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.panel17);
            this.panel8.Controls.Add(this.labelTime);
            this.panel8.Controls.Add(this.labelDate);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(130, 75);
            this.panel8.TabIndex = 0;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.buttonRefresh);
            this.panel17.Controls.Add(this.panel43);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel17.Location = new System.Drawing.Point(0, 38);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(128, 35);
            this.panel17.TabIndex = 4;
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonRefresh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonRefresh.FlatAppearance.BorderSize = 0;
            this.buttonRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRefresh.Image = ((System.Drawing.Image)(resources.GetObject("buttonRefresh.Image")));
            this.buttonRefresh.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonRefresh.Location = new System.Drawing.Point(0, 0);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(104, 35);
            this.buttonRefresh.TabIndex = 0;
            this.buttonRefresh.Text = "refresh";
            this.buttonRefresh.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonRefresh.UseVisualStyleBackColor = false;
            // 
            // panel43
            // 
            this.panel43.Controls.Add(this.button8);
            this.panel43.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel43.Location = new System.Drawing.Point(104, 0);
            this.panel43.Name = "panel43";
            this.panel43.Size = new System.Drawing.Size(24, 35);
            this.panel43.TabIndex = 2;
            // 
            // button8
            // 
            this.button8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.button8.FlatAppearance.BorderSize = 0;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.Font = new System.Drawing.Font("Webdings", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.button8.Location = new System.Drawing.Point(0, 12);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(24, 23);
            this.button8.TabIndex = 2;
            this.button8.Text = "5";
            this.button8.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button8.UseVisualStyleBackColor = true;
            // 
            // labelTime
            // 
            this.labelTime.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelTime.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTime.ForeColor = System.Drawing.Color.Black;
            this.labelTime.Location = new System.Drawing.Point(0, 19);
            this.labelTime.Name = "labelTime";
            this.labelTime.Size = new System.Drawing.Size(128, 19);
            this.labelTime.TabIndex = 3;
            this.labelTime.Text = "10:23 pm";
            this.labelTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelTime.UseMnemonic = false;
            // 
            // labelDate
            // 
            this.labelDate.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDate.ForeColor = System.Drawing.Color.Black;
            this.labelDate.Location = new System.Drawing.Point(0, 0);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new System.Drawing.Size(128, 19);
            this.labelDate.TabIndex = 2;
            this.labelDate.Text = "ma 24/6/2016";
            this.labelDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.panel20);
            this.panel3.Controls.Add(this.label1EventDone24);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(299, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(465, 77);
            this.panel3.TabIndex = 1;
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.panel11);
            this.panel20.Controls.Add(this.panel12);
            this.panel20.Controls.Add(this.panel10);
            this.panel20.Controls.Add(this.panel9);
            this.panel20.Controls.Add(this.panel13);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel20.Location = new System.Drawing.Point(0, 25);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(463, 50);
            this.panel20.TabIndex = 6;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.label2EventReport);
            this.panel11.Controls.Add(this.valueEventReport);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel11.Location = new System.Drawing.Point(397, 0);
            this.panel11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(66, 50);
            this.panel11.TabIndex = 11;
            // 
            // label2EventReport
            // 
            this.label2EventReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2EventReport.ForeColor = System.Drawing.Color.Black;
            this.label2EventReport.Location = new System.Drawing.Point(0, 0);
            this.label2EventReport.Name = "label2EventReport";
            this.label2EventReport.Size = new System.Drawing.Size(66, 23);
            this.label2EventReport.TabIndex = 6;
            this.label2EventReport.Text = "Report:";
            this.label2EventReport.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // valueEventReport
            // 
            this.valueEventReport.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.valueEventReport.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.valueEventReport.ForeColor = System.Drawing.Color.Black;
            this.valueEventReport.Location = new System.Drawing.Point(0, 23);
            this.valueEventReport.Name = "valueEventReport";
            this.valueEventReport.Size = new System.Drawing.Size(66, 27);
            this.valueEventReport.TabIndex = 5;
            this.valueEventReport.Text = "-";
            this.valueEventReport.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.label2EventAnalyze);
            this.panel12.Controls.Add(this.valueEventAnalyze);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel12.Location = new System.Drawing.Point(290, 0);
            this.panel12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(107, 50);
            this.panel12.TabIndex = 10;
            // 
            // label2EventAnalyze
            // 
            this.label2EventAnalyze.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2EventAnalyze.ForeColor = System.Drawing.Color.Black;
            this.label2EventAnalyze.Location = new System.Drawing.Point(0, 0);
            this.label2EventAnalyze.Name = "label2EventAnalyze";
            this.label2EventAnalyze.Size = new System.Drawing.Size(107, 23);
            this.label2EventAnalyze.TabIndex = 6;
            this.label2EventAnalyze.Text = "Analyze:";
            this.label2EventAnalyze.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // valueEventAnalyze
            // 
            this.valueEventAnalyze.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.valueEventAnalyze.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.valueEventAnalyze.ForeColor = System.Drawing.Color.Black;
            this.valueEventAnalyze.Location = new System.Drawing.Point(0, 23);
            this.valueEventAnalyze.Name = "valueEventAnalyze";
            this.valueEventAnalyze.Size = new System.Drawing.Size(107, 27);
            this.valueEventAnalyze.TabIndex = 5;
            this.valueEventAnalyze.Text = "-";
            this.valueEventAnalyze.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.label2EventTriage);
            this.panel10.Controls.Add(this.valueEventTriage);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel10.Location = new System.Drawing.Point(195, 0);
            this.panel10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(95, 50);
            this.panel10.TabIndex = 9;
            // 
            // label2EventTriage
            // 
            this.label2EventTriage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2EventTriage.ForeColor = System.Drawing.Color.Black;
            this.label2EventTriage.Location = new System.Drawing.Point(0, 0);
            this.label2EventTriage.Name = "label2EventTriage";
            this.label2EventTriage.Size = new System.Drawing.Size(95, 23);
            this.label2EventTriage.TabIndex = 6;
            this.label2EventTriage.Text = "Triage:";
            this.label2EventTriage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // valueEventTriage
            // 
            this.valueEventTriage.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.valueEventTriage.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.valueEventTriage.ForeColor = System.Drawing.Color.Black;
            this.valueEventTriage.Location = new System.Drawing.Point(0, 23);
            this.valueEventTriage.Name = "valueEventTriage";
            this.valueEventTriage.Size = new System.Drawing.Size(95, 27);
            this.valueEventTriage.TabIndex = 5;
            this.valueEventTriage.Text = "-";
            this.valueEventTriage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.label2EventTotalOpen);
            this.panel9.Controls.Add(this.valueEventTotalOpen);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel9.Location = new System.Drawing.Point(103, 0);
            this.panel9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(92, 50);
            this.panel9.TabIndex = 8;
            // 
            // label2EventTotalOpen
            // 
            this.label2EventTotalOpen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2EventTotalOpen.ForeColor = System.Drawing.Color.Black;
            this.label2EventTotalOpen.Location = new System.Drawing.Point(0, 0);
            this.label2EventTotalOpen.Name = "label2EventTotalOpen";
            this.label2EventTotalOpen.Size = new System.Drawing.Size(92, 23);
            this.label2EventTotalOpen.TabIndex = 6;
            this.label2EventTotalOpen.Text = "To Do:";
            this.label2EventTotalOpen.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // valueEventTotalOpen
            // 
            this.valueEventTotalOpen.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.valueEventTotalOpen.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.valueEventTotalOpen.ForeColor = System.Drawing.Color.Black;
            this.valueEventTotalOpen.Location = new System.Drawing.Point(0, 23);
            this.valueEventTotalOpen.Name = "valueEventTotalOpen";
            this.valueEventTotalOpen.Size = new System.Drawing.Size(92, 27);
            this.valueEventTotalOpen.TabIndex = 5;
            this.valueEventTotalOpen.Text = "-";
            this.valueEventTotalOpen.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.label2EventDone24);
            this.panel13.Controls.Add(this.valueEventDone24);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel13.Location = new System.Drawing.Point(0, 0);
            this.panel13.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(103, 50);
            this.panel13.TabIndex = 7;
            // 
            // label2EventDone24
            // 
            this.label2EventDone24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2EventDone24.ForeColor = System.Drawing.Color.Black;
            this.label2EventDone24.Location = new System.Drawing.Point(0, 0);
            this.label2EventDone24.Name = "label2EventDone24";
            this.label2EventDone24.Size = new System.Drawing.Size(103, 23);
            this.label2EventDone24.TabIndex = 6;
            this.label2EventDone24.Text = "Done<24hrs:";
            this.label2EventDone24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // valueEventDone24
            // 
            this.valueEventDone24.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.valueEventDone24.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.valueEventDone24.ForeColor = System.Drawing.Color.Black;
            this.valueEventDone24.Location = new System.Drawing.Point(0, 23);
            this.valueEventDone24.Name = "valueEventDone24";
            this.valueEventDone24.Size = new System.Drawing.Size(103, 27);
            this.valueEventDone24.TabIndex = 5;
            this.valueEventDone24.Text = "-";
            this.valueEventDone24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1EventDone24
            // 
            this.label1EventDone24.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1EventDone24.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1EventDone24.ForeColor = System.Drawing.Color.Black;
            this.label1EventDone24.Location = new System.Drawing.Point(0, 0);
            this.label1EventDone24.Name = "label1EventDone24";
            this.label1EventDone24.Size = new System.Drawing.Size(463, 25);
            this.label1EventDone24.TabIndex = 5;
            this.label1EventDone24.Text = "EVENTS";
            this.label1EventDone24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel18
            // 
            this.panel18.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel18.Location = new System.Drawing.Point(288, 0);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(11, 77);
            this.panel18.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.panel19);
            this.panel2.Controls.Add(this.label1ProcedureDone24);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.ForeColor = System.Drawing.Color.White;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(288, 77);
            this.panel2.TabIndex = 0;
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.panel6);
            this.panel19.Controls.Add(this.panel5);
            this.panel19.Controls.Add(this.panel7);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel19.Location = new System.Drawing.Point(0, 25);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(286, 50);
            this.panel19.TabIndex = 6;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.label2ReportToDo);
            this.panel6.Controls.Add(this.ValueReportToDo24);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(194, 0);
            this.panel6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(92, 50);
            this.panel6.TabIndex = 7;
            // 
            // label2ReportToDo
            // 
            this.label2ReportToDo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2ReportToDo.ForeColor = System.Drawing.Color.Black;
            this.label2ReportToDo.Location = new System.Drawing.Point(0, 0);
            this.label2ReportToDo.Name = "label2ReportToDo";
            this.label2ReportToDo.Size = new System.Drawing.Size(92, 23);
            this.label2ReportToDo.TabIndex = 6;
            this.label2ReportToDo.Text = "To Report:";
            this.label2ReportToDo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ValueReportToDo24
            // 
            this.ValueReportToDo24.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ValueReportToDo24.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ValueReportToDo24.ForeColor = System.Drawing.Color.Black;
            this.ValueReportToDo24.Location = new System.Drawing.Point(0, 23);
            this.ValueReportToDo24.Name = "ValueReportToDo24";
            this.ValueReportToDo24.Size = new System.Drawing.Size(92, 27);
            this.ValueReportToDo24.TabIndex = 5;
            this.ValueReportToDo24.Text = "-";
            this.ValueReportToDo24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.label2ProcedureOpen);
            this.panel5.Controls.Add(this.valueStudyOpen);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel5.Location = new System.Drawing.Point(97, 0);
            this.panel5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(97, 50);
            this.panel5.TabIndex = 5;
            // 
            // label2ProcedureOpen
            // 
            this.label2ProcedureOpen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2ProcedureOpen.ForeColor = System.Drawing.Color.Black;
            this.label2ProcedureOpen.Location = new System.Drawing.Point(0, 0);
            this.label2ProcedureOpen.Name = "label2ProcedureOpen";
            this.label2ProcedureOpen.Size = new System.Drawing.Size(97, 23);
            this.label2ProcedureOpen.TabIndex = 6;
            this.label2ProcedureOpen.Text = "Open:";
            this.label2ProcedureOpen.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // valueStudyOpen
            // 
            this.valueStudyOpen.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.valueStudyOpen.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.valueStudyOpen.ForeColor = System.Drawing.Color.Black;
            this.valueStudyOpen.Location = new System.Drawing.Point(0, 23);
            this.valueStudyOpen.Name = "valueStudyOpen";
            this.valueStudyOpen.Size = new System.Drawing.Size(97, 27);
            this.valueStudyOpen.TabIndex = 5;
            this.valueStudyOpen.Text = "-";
            this.valueStudyOpen.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.label2ProcedureDone24);
            this.panel7.Controls.Add(this.valueStudyDone24);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel7.ForeColor = System.Drawing.Color.White;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(97, 50);
            this.panel7.TabIndex = 6;
            // 
            // label2ProcedureDone24
            // 
            this.label2ProcedureDone24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2ProcedureDone24.ForeColor = System.Drawing.Color.Black;
            this.label2ProcedureDone24.Location = new System.Drawing.Point(0, 0);
            this.label2ProcedureDone24.Name = "label2ProcedureDone24";
            this.label2ProcedureDone24.Size = new System.Drawing.Size(97, 23);
            this.label2ProcedureDone24.TabIndex = 6;
            this.label2ProcedureDone24.Text = "Done<24hrs:";
            this.label2ProcedureDone24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // valueStudyDone24
            // 
            this.valueStudyDone24.BackColor = System.Drawing.Color.Transparent;
            this.valueStudyDone24.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.valueStudyDone24.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.valueStudyDone24.ForeColor = System.Drawing.Color.Black;
            this.valueStudyDone24.Location = new System.Drawing.Point(0, 23);
            this.valueStudyDone24.Name = "valueStudyDone24";
            this.valueStudyDone24.Size = new System.Drawing.Size(97, 27);
            this.valueStudyDone24.TabIndex = 5;
            this.valueStudyDone24.Text = "-";
            this.valueStudyDone24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1ProcedureDone24
            // 
            this.label1ProcedureDone24.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1ProcedureDone24.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1ProcedureDone24.ForeColor = System.Drawing.Color.Black;
            this.label1ProcedureDone24.Location = new System.Drawing.Point(0, 0);
            this.label1ProcedureDone24.Name = "label1ProcedureDone24";
            this.label1ProcedureDone24.Size = new System.Drawing.Size(286, 25);
            this.label1ProcedureDone24.TabIndex = 5;
            this.label1ProcedureDone24.Text = "STUDY";
            this.label1ProcedureDone24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label1ProcedureDone24.Click += new System.EventHandler(this.label1ProcedureDone24_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.statusStrip1.Location = new System.Drawing.Point(0, 717);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1691, 22);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.Gainsboro;
            this.panel16.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel16.Location = new System.Drawing.Point(0, 123);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(1691, 2);
            this.panel16.TabIndex = 4;
            // 
            // timerTriage
            // 
            this.timerTriage.Interval = 250;
            this.timerTriage.Tick += new System.EventHandler(this.timerTriage_Tick);
            // 
            // contextMenuState
            // 
            this.contextMenuState.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.toolStripMenuItem3,
            this.toolStripMenuItem4,
            this.toolStripMenuItem5,
            this.toolStripMenuItem6,
            this.toolStripMenuItem7,
            this.toolStripMenuItem8,
            this.toolStripMenuItem9});
            this.contextMenuState.Name = "contextMenuState";
            this.contextMenuState.Size = new System.Drawing.Size(123, 180);
            this.contextMenuState.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuState_Opening);
            this.contextMenuState.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenuState_ItemClicked);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(122, 22);
            this.toolStripMenuItem2.Text = "Received";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(122, 22);
            this.toolStripMenuItem3.Text = "Excluded";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(122, 22);
            this.toolStripMenuItem4.Text = "Noise";
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(122, 22);
            this.toolStripMenuItem5.Text = "LeadOff";
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(122, 22);
            this.toolStripMenuItem6.Text = "Triaged";
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(122, 22);
            this.toolStripMenuItem7.Text = "Analyzed";
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(122, 22);
            this.toolStripMenuItem8.Text = "BaseLine";
            // 
            // toolStripMenuItem9
            // 
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            this.toolStripMenuItem9.Size = new System.Drawing.Size(122, 22);
            this.toolStripMenuItem9.Text = "Reported";
            // 
            // panelViewButtons
            // 
            this.panelViewButtons.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.panelViewButtons.Controls.Add(this.panel508);
            this.panelViewButtons.Controls.Add(this.label45);
            this.panelViewButtons.Controls.Add(this.labelTriageCountDays);
            this.panelViewButtons.Controls.Add(this.panel693);
            this.panelViewButtons.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelViewButtons.Location = new System.Drawing.Point(0, 125);
            this.panelViewButtons.Name = "panelViewButtons";
            this.panelViewButtons.Size = new System.Drawing.Size(1691, 36);
            this.panelViewButtons.TabIndex = 43;
            // 
            // panel508
            // 
            this.panel508.Controls.Add(this.panel510);
            this.panel508.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel508.Location = new System.Drawing.Point(404, 0);
            this.panel508.Margin = new System.Windows.Forms.Padding(0);
            this.panel508.Name = "panel508";
            this.panel508.Size = new System.Drawing.Size(1131, 36);
            this.panel508.TabIndex = 18;
            // 
            // panel510
            // 
            this.panel510.Controls.Add(this.buttonShowQCd);
            this.panel510.Controls.Add(this.buttonShowBaseLine);
            this.panel510.Controls.Add(this.buttonShowExclude);
            this.panel510.Controls.Add(this.buttonShowLeadOff);
            this.panel510.Controls.Add(this.buttonShowNoise);
            this.panel510.Controls.Add(this.buttonShowReported);
            this.panel510.Controls.Add(this.buttonShowAnalyzed);
            this.panel510.Controls.Add(this.labelCountedAnalyzed);
            this.panel510.Controls.Add(this.buttonShowTriaged);
            this.panel510.Controls.Add(this.labelCountedTriaged);
            this.panel510.Controls.Add(this.buttonShowReceived);
            this.panel510.Controls.Add(this.labelCountedReceived);
            this.panel510.Controls.Add(this.buttonShowAll);
            this.panel510.Controls.Add(this.labelCountedAll);
            this.panel510.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel510.Location = new System.Drawing.Point(0, 0);
            this.panel510.Margin = new System.Windows.Forms.Padding(0);
            this.panel510.Name = "panel510";
            this.panel510.Size = new System.Drawing.Size(1131, 36);
            this.panel510.TabIndex = 0;
            // 
            // buttonShowQCd
            // 
            this.buttonShowQCd.BackColor = System.Drawing.Color.LightBlue;
            this.buttonShowQCd.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonShowQCd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonShowQCd.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonShowQCd.Location = new System.Drawing.Point(1052, 0);
            this.buttonShowQCd.Name = "buttonShowQCd";
            this.buttonShowQCd.Size = new System.Drawing.Size(72, 36);
            this.buttonShowQCd.TabIndex = 26;
            this.buttonShowQCd.Text = "QCd";
            this.buttonShowQCd.UseVisualStyleBackColor = false;
            this.buttonShowQCd.Click += new System.EventHandler(this.buttonShowQCd_Click);
            // 
            // buttonShowBaseLine
            // 
            this.buttonShowBaseLine.BackColor = System.Drawing.Color.Orange;
            this.buttonShowBaseLine.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonShowBaseLine.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonShowBaseLine.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonShowBaseLine.Location = new System.Drawing.Point(950, 0);
            this.buttonShowBaseLine.Name = "buttonShowBaseLine";
            this.buttonShowBaseLine.Size = new System.Drawing.Size(102, 36);
            this.buttonShowBaseLine.TabIndex = 16;
            this.buttonShowBaseLine.Text = "BaseLine";
            this.buttonShowBaseLine.UseVisualStyleBackColor = false;
            this.buttonShowBaseLine.Click += new System.EventHandler(this.buttonShowBaseLine_Click);
            // 
            // buttonShowExclude
            // 
            this.buttonShowExclude.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.buttonShowExclude.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonShowExclude.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonShowExclude.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonShowExclude.Location = new System.Drawing.Point(849, 0);
            this.buttonShowExclude.Name = "buttonShowExclude";
            this.buttonShowExclude.Size = new System.Drawing.Size(101, 36);
            this.buttonShowExclude.TabIndex = 15;
            this.buttonShowExclude.Text = "Excluded";
            this.buttonShowExclude.UseVisualStyleBackColor = false;
            this.buttonShowExclude.Click += new System.EventHandler(this.buttonShowNormal_Click);
            // 
            // buttonShowLeadOff
            // 
            this.buttonShowLeadOff.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.buttonShowLeadOff.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonShowLeadOff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonShowLeadOff.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonShowLeadOff.Location = new System.Drawing.Point(755, 0);
            this.buttonShowLeadOff.Name = "buttonShowLeadOff";
            this.buttonShowLeadOff.Size = new System.Drawing.Size(94, 36);
            this.buttonShowLeadOff.TabIndex = 14;
            this.buttonShowLeadOff.Text = "LeadOff";
            this.buttonShowLeadOff.UseVisualStyleBackColor = false;
            this.buttonShowLeadOff.Click += new System.EventHandler(this.buttonShowLeadOff_Click);
            // 
            // buttonShowNoise
            // 
            this.buttonShowNoise.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.buttonShowNoise.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonShowNoise.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonShowNoise.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonShowNoise.Location = new System.Drawing.Point(675, 0);
            this.buttonShowNoise.Name = "buttonShowNoise";
            this.buttonShowNoise.Size = new System.Drawing.Size(80, 36);
            this.buttonShowNoise.TabIndex = 13;
            this.buttonShowNoise.Text = "Noise";
            this.buttonShowNoise.UseVisualStyleBackColor = false;
            this.buttonShowNoise.Click += new System.EventHandler(this.buttonShowNoise_Click);
            // 
            // buttonShowReported
            // 
            this.buttonShowReported.BackColor = System.Drawing.Color.LightGray;
            this.buttonShowReported.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonShowReported.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonShowReported.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonShowReported.Location = new System.Drawing.Point(577, 0);
            this.buttonShowReported.Name = "buttonShowReported";
            this.buttonShowReported.Size = new System.Drawing.Size(98, 36);
            this.buttonShowReported.TabIndex = 12;
            this.buttonShowReported.Text = "Reported";
            this.buttonShowReported.UseVisualStyleBackColor = false;
            this.buttonShowReported.Click += new System.EventHandler(this.buttonShowReported_Click);
            // 
            // buttonShowAnalyzed
            // 
            this.buttonShowAnalyzed.BackColor = System.Drawing.Color.LightYellow;
            this.buttonShowAnalyzed.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonShowAnalyzed.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonShowAnalyzed.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonShowAnalyzed.Location = new System.Drawing.Point(468, 0);
            this.buttonShowAnalyzed.Name = "buttonShowAnalyzed";
            this.buttonShowAnalyzed.Size = new System.Drawing.Size(109, 36);
            this.buttonShowAnalyzed.TabIndex = 11;
            this.buttonShowAnalyzed.Text = "Analyzed";
            this.buttonShowAnalyzed.UseVisualStyleBackColor = false;
            this.buttonShowAnalyzed.Click += new System.EventHandler(this.buttonShowAnalyzed_Click);
            // 
            // labelCountedAnalyzed
            // 
            this.labelCountedAnalyzed.BackColor = System.Drawing.Color.LightYellow;
            this.labelCountedAnalyzed.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelCountedAnalyzed.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelCountedAnalyzed.ForeColor = System.Drawing.Color.Black;
            this.labelCountedAnalyzed.Location = new System.Drawing.Point(412, 0);
            this.labelCountedAnalyzed.Name = "labelCountedAnalyzed";
            this.labelCountedAnalyzed.Size = new System.Drawing.Size(56, 36);
            this.labelCountedAnalyzed.TabIndex = 25;
            this.labelCountedAnalyzed.Text = "-";
            this.labelCountedAnalyzed.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // buttonShowTriaged
            // 
            this.buttonShowTriaged.BackColor = System.Drawing.Color.LightGreen;
            this.buttonShowTriaged.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonShowTriaged.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonShowTriaged.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonShowTriaged.Location = new System.Drawing.Point(318, 0);
            this.buttonShowTriaged.Margin = new System.Windows.Forms.Padding(0);
            this.buttonShowTriaged.Name = "buttonShowTriaged";
            this.buttonShowTriaged.Size = new System.Drawing.Size(94, 36);
            this.buttonShowTriaged.TabIndex = 10;
            this.buttonShowTriaged.Text = "Triaged";
            this.buttonShowTriaged.UseVisualStyleBackColor = false;
            this.buttonShowTriaged.Click += new System.EventHandler(this.buttonShowTriaged_Click);
            // 
            // labelCountedTriaged
            // 
            this.labelCountedTriaged.BackColor = System.Drawing.Color.LightGreen;
            this.labelCountedTriaged.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelCountedTriaged.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelCountedTriaged.ForeColor = System.Drawing.Color.Black;
            this.labelCountedTriaged.Location = new System.Drawing.Point(262, 0);
            this.labelCountedTriaged.Name = "labelCountedTriaged";
            this.labelCountedTriaged.Size = new System.Drawing.Size(56, 36);
            this.labelCountedTriaged.TabIndex = 24;
            this.labelCountedTriaged.Text = "-";
            this.labelCountedTriaged.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // buttonShowReceived
            // 
            this.buttonShowReceived.BackColor = System.Drawing.Color.White;
            this.buttonShowReceived.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonShowReceived.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonShowReceived.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonShowReceived.Location = new System.Drawing.Point(161, 0);
            this.buttonShowReceived.Name = "buttonShowReceived";
            this.buttonShowReceived.Size = new System.Drawing.Size(101, 36);
            this.buttonShowReceived.TabIndex = 5;
            this.buttonShowReceived.Text = "Received";
            this.buttonShowReceived.UseVisualStyleBackColor = false;
            this.buttonShowReceived.Click += new System.EventHandler(this.buttonShowReceived_Click);
            // 
            // labelCountedReceived
            // 
            this.labelCountedReceived.BackColor = System.Drawing.Color.White;
            this.labelCountedReceived.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelCountedReceived.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelCountedReceived.ForeColor = System.Drawing.Color.Black;
            this.labelCountedReceived.Location = new System.Drawing.Point(105, 0);
            this.labelCountedReceived.Name = "labelCountedReceived";
            this.labelCountedReceived.Size = new System.Drawing.Size(56, 36);
            this.labelCountedReceived.TabIndex = 23;
            this.labelCountedReceived.Text = "-";
            this.labelCountedReceived.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // buttonShowAll
            // 
            this.buttonShowAll.BackColor = System.Drawing.Color.Yellow;
            this.buttonShowAll.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonShowAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonShowAll.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.buttonShowAll.Location = new System.Drawing.Point(56, 0);
            this.buttonShowAll.Margin = new System.Windows.Forms.Padding(0);
            this.buttonShowAll.Name = "buttonShowAll";
            this.buttonShowAll.Size = new System.Drawing.Size(49, 36);
            this.buttonShowAll.TabIndex = 3;
            this.buttonShowAll.Text = "All";
            this.buttonShowAll.UseVisualStyleBackColor = false;
            this.buttonShowAll.Click += new System.EventHandler(this.buttonShowAll_Click);
            // 
            // labelCountedAll
            // 
            this.labelCountedAll.BackColor = System.Drawing.Color.Yellow;
            this.labelCountedAll.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelCountedAll.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelCountedAll.ForeColor = System.Drawing.Color.Black;
            this.labelCountedAll.Location = new System.Drawing.Point(0, 0);
            this.labelCountedAll.Name = "labelCountedAll";
            this.labelCountedAll.Size = new System.Drawing.Size(56, 36);
            this.labelCountedAll.TabIndex = 22;
            this.labelCountedAll.Text = "-";
            this.labelCountedAll.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label45
            // 
            this.label45.Dock = System.Windows.Forms.DockStyle.Left;
            this.label45.Font = new System.Drawing.Font("Arial", 12F);
            this.label45.ForeColor = System.Drawing.Color.White;
            this.label45.Location = new System.Drawing.Point(334, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(70, 36);
            this.label45.TabIndex = 0;
            this.label45.Text = "Events:";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTriageCountDays
            // 
            this.labelTriageCountDays.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelTriageCountDays.Font = new System.Drawing.Font("Arial", 10F);
            this.labelTriageCountDays.ForeColor = System.Drawing.Color.White;
            this.labelTriageCountDays.Location = new System.Drawing.Point(300, 0);
            this.labelTriageCountDays.Name = "labelTriageCountDays";
            this.labelTriageCountDays.Size = new System.Drawing.Size(34, 36);
            this.labelTriageCountDays.TabIndex = 4;
            this.labelTriageCountDays.Text = "-d\r\n-h";
            this.labelTriageCountDays.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelTriageCountDays.Click += new System.EventHandler(this.labelTriageCountDays_Click);
            // 
            // panel693
            // 
            this.panel693.Controls.Add(this.labelCountRows);
            this.panel693.Controls.Add(this.buttonShowReport);
            this.panel693.Controls.Add(this.buttonShowAnalyze);
            this.panel693.Controls.Add(this.buttonShowTriage);
            this.panel693.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel693.Location = new System.Drawing.Point(0, 0);
            this.panel693.Name = "panel693";
            this.panel693.Size = new System.Drawing.Size(300, 36);
            this.panel693.TabIndex = 19;
            // 
            // labelCountRows
            // 
            this.labelCountRows.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelCountRows.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCountRows.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.labelCountRows.Location = new System.Drawing.Point(270, 0);
            this.labelCountRows.Name = "labelCountRows";
            this.labelCountRows.Size = new System.Drawing.Size(29, 36);
            this.labelCountRows.TabIndex = 19;
            this.labelCountRows.Text = "ψ";
            this.labelCountRows.Click += new System.EventHandler(this.labelCountRows_Click);
            // 
            // buttonShowReport
            // 
            this.buttonShowReport.BackColor = System.Drawing.Color.LightYellow;
            this.buttonShowReport.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonShowReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonShowReport.Font = new System.Drawing.Font("Arial", 14F);
            this.buttonShowReport.Location = new System.Drawing.Point(180, 0);
            this.buttonShowReport.Name = "buttonShowReport";
            this.buttonShowReport.Size = new System.Drawing.Size(90, 36);
            this.buttonShowReport.TabIndex = 18;
            this.buttonShowReport.Text = "Report";
            this.buttonShowReport.UseVisualStyleBackColor = false;
            this.buttonShowReport.Click += new System.EventHandler(this.buttonShowReport_Click);
            // 
            // buttonShowAnalyze
            // 
            this.buttonShowAnalyze.BackColor = System.Drawing.Color.LightGreen;
            this.buttonShowAnalyze.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonShowAnalyze.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonShowAnalyze.Font = new System.Drawing.Font("Arial", 14F);
            this.buttonShowAnalyze.Location = new System.Drawing.Point(90, 0);
            this.buttonShowAnalyze.Name = "buttonShowAnalyze";
            this.buttonShowAnalyze.Size = new System.Drawing.Size(90, 36);
            this.buttonShowAnalyze.TabIndex = 17;
            this.buttonShowAnalyze.Text = "Analyze";
            this.buttonShowAnalyze.UseVisualStyleBackColor = false;
            this.buttonShowAnalyze.Click += new System.EventHandler(this.buttonShowAnalyze_Click);
            // 
            // buttonShowTriage
            // 
            this.buttonShowTriage.BackColor = System.Drawing.Color.LightGray;
            this.buttonShowTriage.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonShowTriage.FlatAppearance.BorderSize = 3;
            this.buttonShowTriage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonShowTriage.Font = new System.Drawing.Font("Arial", 14F);
            this.buttonShowTriage.Location = new System.Drawing.Point(0, 0);
            this.buttonShowTriage.Name = "buttonShowTriage";
            this.buttonShowTriage.Size = new System.Drawing.Size(90, 36);
            this.buttonShowTriage.TabIndex = 16;
            this.buttonShowTriage.Text = "Triage";
            this.buttonShowTriage.UseVisualStyleBackColor = false;
            this.buttonShowTriage.Click += new System.EventHandler(this.buttonShowTriage_Click);
            // 
            // panelViewColorStrip
            // 
            this.panelViewColorStrip.BackColor = System.Drawing.Color.LightGreen;
            this.panelViewColorStrip.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelViewColorStrip.Location = new System.Drawing.Point(0, 161);
            this.panelViewColorStrip.Name = "panelViewColorStrip";
            this.panelViewColorStrip.Size = new System.Drawing.Size(1691, 6);
            this.panelViewColorStrip.TabIndex = 44;
            // 
            // panelDataGrid
            // 
            this.panelDataGrid.BackColor = System.Drawing.Color.White;
            this.panelDataGrid.Controls.Add(this.dataGridTriage);
            this.panelDataGrid.Controls.Add(this.panel24);
            this.panelDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDataGrid.Location = new System.Drawing.Point(0, 167);
            this.panelDataGrid.Name = "panelDataGrid";
            this.panelDataGrid.Size = new System.Drawing.Size(1691, 188);
            this.panelDataGrid.TabIndex = 45;
            // 
            // dataGridTriage
            // 
            this.dataGridTriage.AllowUserToAddRows = false;
            this.dataGridTriage.AllowUserToDeleteRows = false;
            this.dataGridTriage.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridTriage.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Count,
            this.Patient,
            this.Event,
            this.Read,
            this.Remark,
            this.EventStrip,
            this.Triage,
            this.Index,
            this.State,
            this.FilePath,
            this.FileName,
            this.NrSignals,
            this.Study,
            this.Active,
            this.ModelSnr,
            this.Column1,
            this.Column2,
            this.ReadUTC,
            this.ColumTrailer});
            this.dataGridTriage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridTriage.Location = new System.Drawing.Point(0, 87);
            this.dataGridTriage.MultiSelect = false;
            this.dataGridTriage.Name = "dataGridTriage";
            this.dataGridTriage.ReadOnly = true;
            this.dataGridTriage.RowHeadersVisible = false;
            this.dataGridTriage.RowHeadersWidth = 20;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridTriage.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridTriage.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            this.dataGridTriage.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Verdana", 10F);
            this.dataGridTriage.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridTriage.RowTemplate.Height = 70;
            this.dataGridTriage.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridTriage.ShowEditingIcon = false;
            this.dataGridTriage.Size = new System.Drawing.Size(1691, 101);
            this.dataGridTriage.TabIndex = 6;
            this.dataGridTriage.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridTriage_CellClick);
            this.dataGridTriage.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridTriage_CellContentClick);
            this.dataGridTriage.CellMouseMove += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridTriage_CellMouseMove);
            this.dataGridTriage.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dataGridTriage_CellPainting);
            this.dataGridTriage.SelectionChanged += new System.EventHandler(this.dataGridTriage_SelectionChanged);
            this.dataGridTriage.DoubleClick += new System.EventHandler(this.dataGridTriage_DoubleClick);
            this.dataGridTriage.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dataGridTriage_MouseUp);
            // 
            // Count
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Count.DefaultCellStyle = dataGridViewCellStyle1;
            this.Count.HeaderText = "Count";
            this.Count.Name = "Count";
            this.Count.ReadOnly = true;
            this.Count.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Count.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Count.ToolTipText = "Priority & State";
            this.Count.Width = 76;
            // 
            // Patient
            // 
            this.Patient.HeaderText = "Patient";
            this.Patient.Name = "Patient";
            this.Patient.ReadOnly = true;
            this.Patient.Width = 160;
            // 
            // Event
            // 
            this.Event.HeaderText = "Event";
            this.Event.Name = "Event";
            this.Event.ReadOnly = true;
            this.Event.Width = 130;
            // 
            // Read
            // 
            this.Read.HeaderText = "Received";
            this.Read.Name = "Read";
            this.Read.ReadOnly = true;
            this.Read.Width = 116;
            // 
            // Remark
            // 
            this.Remark.HeaderText = "Remark";
            this.Remark.Name = "Remark";
            this.Remark.ReadOnly = true;
            this.Remark.Width = 190;
            // 
            // EventStrip
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.NullValue = null;
            dataGridViewCellStyle2.Padding = new System.Windows.Forms.Padding(2, 2, 0, 0);
            this.EventStrip.DefaultCellStyle = dataGridViewCellStyle2;
            this.EventStrip.HeaderText = "Event Strip";
            this.EventStrip.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
            this.EventStrip.Name = "EventStrip";
            this.EventStrip.ReadOnly = true;
            this.EventStrip.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.EventStrip.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.EventStrip.Width = 780;
            // 
            // Triage
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Courier New", 10.2F);
            dataGridViewCellStyle3.NullValue = null;
            dataGridViewCellStyle3.Padding = new System.Windows.Forms.Padding(2, 2, 0, 0);
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Triage.DefaultCellStyle = dataGridViewCellStyle3;
            this.Triage.HeaderText = "Triage";
            this.Triage.Name = "Triage";
            this.Triage.ReadOnly = true;
            this.Triage.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Triage.ToolTipText = "Execute Action";
            this.Triage.Width = 167;
            // 
            // Index
            // 
            this.Index.HeaderText = "Index";
            this.Index.Name = "Index";
            this.Index.ReadOnly = true;
            this.Index.Visible = false;
            // 
            // State
            // 
            this.State.HeaderText = "State";
            this.State.Name = "State";
            this.State.ReadOnly = true;
            this.State.Visible = false;
            // 
            // FilePath
            // 
            this.FilePath.HeaderText = "FilePath";
            this.FilePath.Name = "FilePath";
            this.FilePath.ReadOnly = true;
            this.FilePath.Visible = false;
            // 
            // FileName
            // 
            this.FileName.HeaderText = "File Name";
            this.FileName.Name = "FileName";
            this.FileName.ReadOnly = true;
            this.FileName.Visible = false;
            // 
            // NrSignals
            // 
            this.NrSignals.HeaderText = "NrSignals";
            this.NrSignals.Name = "NrSignals";
            this.NrSignals.ReadOnly = true;
            this.NrSignals.Visible = false;
            // 
            // Study
            // 
            this.Study.HeaderText = "Study";
            this.Study.Name = "Study";
            this.Study.ReadOnly = true;
            this.Study.Visible = false;
            // 
            // Active
            // 
            this.Active.HeaderText = "Active";
            this.Active.Name = "Active";
            this.Active.ReadOnly = true;
            this.Active.Visible = false;
            // 
            // ModelSnr
            // 
            this.ModelSnr.HeaderText = "ModelSnr";
            this.ModelSnr.Name = "ModelSnr";
            this.ModelSnr.ReadOnly = true;
            this.ModelSnr.Visible = false;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "RecDir";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Visible = false;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "NrA";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Visible = false;
            // 
            // ReadUTC
            // 
            this.ReadUTC.HeaderText = "ReadUTC";
            this.ReadUTC.Name = "ReadUTC";
            this.ReadUTC.ReadOnly = true;
            this.ReadUTC.Visible = false;
            // 
            // ColumTrailer
            // 
            this.ColumTrailer.HeaderText = "C";
            this.ColumTrailer.Name = "ColumTrailer";
            this.ColumTrailer.ReadOnly = true;
            this.ColumTrailer.Visible = false;
            this.ColumTrailer.Width = 10;
            // 
            // panel24
            // 
            this.panel24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panel24.Controls.Add(this.panelViewMode);
            this.panel24.Controls.Add(this.panel64);
            this.panel24.Controls.Add(this.panel38);
            this.panel24.Controls.Add(this.panelRight61);
            this.panel24.Controls.Add(this.panel39);
            this.panel24.Controls.Add(this.panel30);
            this.panel24.Controls.Add(this.panel28);
            this.panel24.Controls.Add(this.panel26);
            this.panel24.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel24.Location = new System.Drawing.Point(0, 0);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(1691, 87);
            this.panel24.TabIndex = 5;
            // 
            // panelViewMode
            // 
            this.panelViewMode.Controls.Add(this.listBoxEventMode);
            this.panelViewMode.Controls.Add(this.label10);
            this.panelViewMode.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelViewMode.Location = new System.Drawing.Point(1483, 0);
            this.panelViewMode.Name = "panelViewMode";
            this.panelViewMode.Size = new System.Drawing.Size(98, 87);
            this.panelViewMode.TabIndex = 5;
            // 
            // listBoxEventMode
            // 
            this.listBoxEventMode.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.listBoxEventMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.listBoxEventMode.Font = new System.Drawing.Font("Arial", 12F);
            this.listBoxEventMode.FormattingEnabled = true;
            this.listBoxEventMode.Items.AddRange(new object[] {
            "Triage",
            "Analyze",
            "Report",
            "--------",
            "List All",
            "List Received",
            "List Normal",
            "List Noise",
            "List Lead Off",
            "List Base Line",
            "List Triaged",
            "List Analyzed",
            "List Reported"});
            this.listBoxEventMode.Location = new System.Drawing.Point(0, 61);
            this.listBoxEventMode.MaxDropDownItems = 20;
            this.listBoxEventMode.Name = "listBoxEventMode";
            this.listBoxEventMode.Size = new System.Drawing.Size(98, 26);
            this.listBoxEventMode.TabIndex = 9;
            // 
            // label10
            // 
            this.label10.Dock = System.Windows.Forms.DockStyle.Top;
            this.label10.Font = new System.Drawing.Font("Arial", 12F);
            this.label10.Location = new System.Drawing.Point(0, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(98, 26);
            this.label10.TabIndex = 0;
            this.label10.Text = "View Mode:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel64
            // 
            this.panel64.Controls.Add(this.labelImageNk);
            this.panel64.Controls.Add(this.labelLoadImages);
            this.panel64.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel64.Location = new System.Drawing.Point(1354, 0);
            this.panel64.Name = "panel64";
            this.panel64.Size = new System.Drawing.Size(129, 87);
            this.panel64.TabIndex = 9;
            // 
            // labelImageNk
            // 
            this.labelImageNk.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelImageNk.Font = new System.Drawing.Font("Arial", 12F);
            this.labelImageNk.Location = new System.Drawing.Point(0, 0);
            this.labelImageNk.Name = "labelImageNk";
            this.labelImageNk.Size = new System.Drawing.Size(129, 26);
            this.labelImageNk.TabIndex = 5;
            this.labelImageNk.Text = "^222 Images ?11";
            this.labelImageNk.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelImageNk.Click += new System.EventHandler(this.labelImageNk_Click);
            this.labelImageNk.DoubleClick += new System.EventHandler(this.labelImageNk_DoubleClick);
            // 
            // labelLoadImages
            // 
            this.labelLoadImages.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelLoadImages.Font = new System.Drawing.Font("Arial", 12F);
            this.labelLoadImages.Location = new System.Drawing.Point(0, 65);
            this.labelLoadImages.Name = "labelLoadImages";
            this.labelLoadImages.Size = new System.Drawing.Size(129, 22);
            this.labelLoadImages.TabIndex = 0;
            this.labelLoadImages.Text = "iC=^9999, nT= 2";
            this.labelLoadImages.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel38
            // 
            this.panel38.Controls.Add(this.labelMemUsage);
            this.panel38.Controls.Add(this.panel1);
            this.panel38.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel38.Location = new System.Drawing.Point(1166, 0);
            this.panel38.Name = "panel38";
            this.panel38.Size = new System.Drawing.Size(188, 87);
            this.panel38.TabIndex = 10;
            // 
            // labelMemUsage
            // 
            this.labelMemUsage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelMemUsage.Font = new System.Drawing.Font("Arial", 12F);
            this.labelMemUsage.Location = new System.Drawing.Point(0, 26);
            this.labelMemUsage.Name = "labelMemUsage";
            this.labelMemUsage.Size = new System.Drawing.Size(188, 61);
            this.labelMemUsage.TabIndex = 7;
            this.labelMemUsage.Text = "^AP: 1534>2000MB PC: 12345>2000MBMB";
            this.labelMemUsage.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.labelUpdTimeStrip);
            this.panel1.Controls.Add(this.labelUpdTimeTable);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(188, 26);
            this.panel1.TabIndex = 4;
            // 
            // labelUpdTimeStrip
            // 
            this.labelUpdTimeStrip.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelUpdTimeStrip.Font = new System.Drawing.Font("Arial", 12F);
            this.labelUpdTimeStrip.Location = new System.Drawing.Point(-11, 0);
            this.labelUpdTimeStrip.Name = "labelUpdTimeStrip";
            this.labelUpdTimeStrip.Size = new System.Drawing.Size(114, 26);
            this.labelUpdTimeStrip.TabIndex = 4;
            this.labelUpdTimeStrip.Text = "uS=90.0s";
            this.labelUpdTimeStrip.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelUpdTimeTable
            // 
            this.labelUpdTimeTable.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelUpdTimeTable.Font = new System.Drawing.Font("Arial", 12F);
            this.labelUpdTimeTable.Location = new System.Drawing.Point(103, 0);
            this.labelUpdTimeTable.Name = "labelUpdTimeTable";
            this.labelUpdTimeTable.Size = new System.Drawing.Size(85, 26);
            this.labelUpdTimeTable.TabIndex = 3;
            this.labelUpdTimeTable.Text = "-uT=90.0s";
            this.labelUpdTimeTable.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panelRight61
            // 
            this.panelRight61.Controls.Add(this.labelDrive);
            this.panelRight61.Controls.Add(this.buttonTriageFields);
            this.panelRight61.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight61.Location = new System.Drawing.Point(1666, 0);
            this.panelRight61.Name = "panelRight61";
            this.panelRight61.Size = new System.Drawing.Size(25, 87);
            this.panelRight61.TabIndex = 8;
            // 
            // labelDrive
            // 
            this.labelDrive.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDrive.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDrive.Location = new System.Drawing.Point(0, 0);
            this.labelDrive.Name = "labelDrive";
            this.labelDrive.Size = new System.Drawing.Size(25, 14);
            this.labelDrive.TabIndex = 1;
            this.labelDrive.Text = "-d:";
            this.labelDrive.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonTriageFields
            // 
            this.buttonTriageFields.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonTriageFields.Location = new System.Drawing.Point(0, 65);
            this.buttonTriageFields.Name = "buttonTriageFields";
            this.buttonTriageFields.Size = new System.Drawing.Size(25, 22);
            this.buttonTriageFields.TabIndex = 0;
            this.buttonTriageFields.Text = "tf";
            this.buttonTriageFields.UseVisualStyleBackColor = true;
            this.buttonTriageFields.Click += new System.EventHandler(this.buttonTriageFields_Click);
            // 
            // panel39
            // 
            this.panel39.Controls.Add(this.checkBoxAllStudy);
            this.panel39.Controls.Add(this.panel47);
            this.panel39.Controls.Add(this.listBoxViewUnit);
            this.panel39.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel39.Location = new System.Drawing.Point(1084, 0);
            this.panel39.Name = "panel39";
            this.panel39.Size = new System.Drawing.Size(82, 87);
            this.panel39.TabIndex = 4;
            // 
            // checkBoxAllStudy
            // 
            this.checkBoxAllStudy.AutoSize = true;
            this.checkBoxAllStudy.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkBoxAllStudy.Font = new System.Drawing.Font("Arial", 12F);
            this.checkBoxAllStudy.Location = new System.Drawing.Point(0, 0);
            this.checkBoxAllStudy.Name = "checkBoxAllStudy";
            this.checkBoxAllStudy.Size = new System.Drawing.Size(82, 22);
            this.checkBoxAllStudy.TabIndex = 28;
            this.checkBoxAllStudy.Text = "All S+T";
            this.checkBoxAllStudy.UseVisualStyleBackColor = true;
            // 
            // panel47
            // 
            this.panel47.Controls.Add(this.panel62);
            this.panel47.Controls.Add(this.textBoxViewTime);
            this.panel47.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel47.Location = new System.Drawing.Point(0, 37);
            this.panel47.Name = "panel47";
            this.panel47.Size = new System.Drawing.Size(82, 28);
            this.panel47.TabIndex = 5;
            // 
            // panel62
            // 
            this.panel62.Controls.Add(this.panelViewDec);
            this.panel62.Controls.Add(this.panelViewInc);
            this.panel62.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel62.Location = new System.Drawing.Point(64, 0);
            this.panel62.Name = "panel62";
            this.panel62.Size = new System.Drawing.Size(18, 28);
            this.panel62.TabIndex = 6;
            // 
            // panelViewDec
            // 
            this.panelViewDec.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelViewDec.BackgroundImage")));
            this.panelViewDec.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panelViewDec.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelViewDec.Location = new System.Drawing.Point(0, 14);
            this.panelViewDec.Name = "panelViewDec";
            this.panelViewDec.Size = new System.Drawing.Size(18, 12);
            this.panelViewDec.TabIndex = 1;
            this.panelViewDec.Click += new System.EventHandler(this.panelViewDec_Click);
            // 
            // panelViewInc
            // 
            this.panelViewInc.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelViewInc.BackgroundImage")));
            this.panelViewInc.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panelViewInc.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelViewInc.Location = new System.Drawing.Point(0, 0);
            this.panelViewInc.Name = "panelViewInc";
            this.panelViewInc.Size = new System.Drawing.Size(18, 14);
            this.panelViewInc.TabIndex = 0;
            this.panelViewInc.Click += new System.EventHandler(this.panelViewInc_Click);
            // 
            // textBoxViewTime
            // 
            this.textBoxViewTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxViewTime.Font = new System.Drawing.Font("Arial", 12F);
            this.textBoxViewTime.Location = new System.Drawing.Point(0, 0);
            this.textBoxViewTime.Name = "textBoxViewTime";
            this.textBoxViewTime.Size = new System.Drawing.Size(82, 26);
            this.textBoxViewTime.TabIndex = 5;
            this.textBoxViewTime.Text = "48";
            this.textBoxViewTime.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxViewTime_KeyPress);
            // 
            // listBoxViewUnit
            // 
            this.listBoxViewUnit.AllowDrop = true;
            this.listBoxViewUnit.CausesValidation = false;
            this.listBoxViewUnit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.listBoxViewUnit.Font = new System.Drawing.Font("Arial", 12F);
            this.listBoxViewUnit.ItemHeight = 18;
            this.listBoxViewUnit.Items.AddRange(new object[] {
            "Hours",
            "Days"});
            this.listBoxViewUnit.Location = new System.Drawing.Point(0, 65);
            this.listBoxViewUnit.Name = "listBoxViewUnit";
            this.listBoxViewUnit.ScrollAlwaysVisible = true;
            this.listBoxViewUnit.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.listBoxViewUnit.Size = new System.Drawing.Size(82, 22);
            this.listBoxViewUnit.TabIndex = 1;
            this.listBoxViewUnit.MouseClick += new System.Windows.Forms.MouseEventHandler(this.listBoxViewUnit_MouseClick);
            // 
            // panel30
            // 
            this.panel30.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel30.Location = new System.Drawing.Point(1080, 0);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(4, 87);
            this.panel30.TabIndex = 3;
            // 
            // panel28
            // 
            this.panel28.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel28.Controls.Add(this.panel48);
            this.panel28.Controls.Add(this.panelSelectView);
            this.panel28.Controls.Add(this.panelView);
            this.panel28.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel28.Location = new System.Drawing.Point(196, 0);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(884, 87);
            this.panel28.TabIndex = 2;
            // 
            // panel48
            // 
            this.panel48.Controls.Add(this.labelSelectPermisions);
            this.panel48.Controls.Add(this.panel53);
            this.panel48.Controls.Add(this.checkBoxStudyPermissions);
            this.panel48.Controls.Add(this.labelClient);
            this.panel48.Controls.Add(this.checkBoxClient);
            this.panel48.Controls.Add(this.labelTrial);
            this.panel48.Controls.Add(this.checkBoxTrial);
            this.panel48.Controls.Add(this.panel52);
            this.panel48.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel48.Location = new System.Drawing.Point(0, 26);
            this.panel48.Name = "panel48";
            this.panel48.Size = new System.Drawing.Size(884, 35);
            this.panel48.TabIndex = 3;
            // 
            // labelSelectPermisions
            // 
            this.labelSelectPermisions.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.labelSelectPermisions.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSelectPermisions.Font = new System.Drawing.Font("Arial", 8F);
            this.labelSelectPermisions.Location = new System.Drawing.Point(738, 0);
            this.labelSelectPermisions.Name = "labelSelectPermisions";
            this.labelSelectPermisions.Size = new System.Drawing.Size(130, 35);
            this.labelSelectPermisions.TabIndex = 29;
            this.labelSelectPermisions.Text = "^<Site label 1+Site label 2+MCT-master + Event thechnician";
            this.labelSelectPermisions.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelSelectPermisions.Click += new System.EventHandler(this.labelStudyPermisions_Click);
            // 
            // panel53
            // 
            this.panel53.Controls.Add(this.comboBoxLocation);
            this.panel53.Controls.Add(this.panel54);
            this.panel53.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel53.Location = new System.Drawing.Point(597, 0);
            this.panel53.Name = "panel53";
            this.panel53.Size = new System.Drawing.Size(141, 35);
            this.panel53.TabIndex = 35;
            // 
            // comboBoxLocation
            // 
            this.comboBoxLocation.Dock = System.Windows.Forms.DockStyle.Top;
            this.comboBoxLocation.FormattingEnabled = true;
            this.comboBoxLocation.Location = new System.Drawing.Point(0, 6);
            this.comboBoxLocation.Name = "comboBoxLocation";
            this.comboBoxLocation.Size = new System.Drawing.Size(141, 24);
            this.comboBoxLocation.TabIndex = 35;
            this.comboBoxLocation.Text = "-- All --";
            this.comboBoxLocation.SelectedIndexChanged += new System.EventHandler(this.comboBoxLocation_SelectedIndexChanged);
            // 
            // panel54
            // 
            this.panel54.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel54.Location = new System.Drawing.Point(0, 0);
            this.panel54.Name = "panel54";
            this.panel54.Size = new System.Drawing.Size(141, 6);
            this.panel54.TabIndex = 36;
            // 
            // checkBoxStudyPermissions
            // 
            this.checkBoxStudyPermissions.AutoSize = true;
            this.checkBoxStudyPermissions.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxStudyPermissions.Font = new System.Drawing.Font("Arial", 12F);
            this.checkBoxStudyPermissions.Location = new System.Drawing.Point(501, 0);
            this.checkBoxStudyPermissions.Name = "checkBoxStudyPermissions";
            this.checkBoxStudyPermissions.Size = new System.Drawing.Size(96, 35);
            this.checkBoxStudyPermissions.TabIndex = 27;
            this.checkBoxStudyPermissions.Text = "Location=";
            this.checkBoxStudyPermissions.UseVisualStyleBackColor = true;
            this.checkBoxStudyPermissions.CheckedChanged += new System.EventHandler(this.checkBoxStudyPermissions_CheckedChanged);
            // 
            // labelClient
            // 
            this.labelClient.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelClient.Font = new System.Drawing.Font("Arial", 10F);
            this.labelClient.Location = new System.Drawing.Point(322, 0);
            this.labelClient.Name = "labelClient";
            this.labelClient.Size = new System.Drawing.Size(179, 35);
            this.labelClient.TabIndex = 33;
            this.labelClient.Text = "^C23456789 123456789jhgjlhsakjg";
            this.labelClient.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelClient.Click += new System.EventHandler(this.labelClient_Click);
            // 
            // checkBoxClient
            // 
            this.checkBoxClient.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxClient.Font = new System.Drawing.Font("Arial", 12F);
            this.checkBoxClient.Location = new System.Drawing.Point(246, 0);
            this.checkBoxClient.Name = "checkBoxClient";
            this.checkBoxClient.Size = new System.Drawing.Size(76, 35);
            this.checkBoxClient.TabIndex = 32;
            this.checkBoxClient.Text = "Client=";
            this.checkBoxClient.UseVisualStyleBackColor = true;
            // 
            // labelTrial
            // 
            this.labelTrial.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelTrial.Font = new System.Drawing.Font("Arial", 10F);
            this.labelTrial.Location = new System.Drawing.Point(76, 0);
            this.labelTrial.Name = "labelTrial";
            this.labelTrial.Size = new System.Drawing.Size(170, 35);
            this.labelTrial.TabIndex = 31;
            this.labelTrial.Text = "^T12345678901234567 gm,fkhdsfsjkjkl;f";
            this.labelTrial.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelTrial.Click += new System.EventHandler(this.labelTrial_Click);
            // 
            // checkBoxTrial
            // 
            this.checkBoxTrial.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxTrial.Font = new System.Drawing.Font("Arial", 12F);
            this.checkBoxTrial.Location = new System.Drawing.Point(10, 0);
            this.checkBoxTrial.Name = "checkBoxTrial";
            this.checkBoxTrial.Size = new System.Drawing.Size(66, 35);
            this.checkBoxTrial.TabIndex = 30;
            this.checkBoxTrial.Text = "Trial=";
            this.checkBoxTrial.UseVisualStyleBackColor = true;
            // 
            // panel52
            // 
            this.panel52.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel52.Location = new System.Drawing.Point(0, 0);
            this.panel52.Name = "panel52";
            this.panel52.Size = new System.Drawing.Size(10, 35);
            this.panel52.TabIndex = 28;
            // 
            // panelSelectView
            // 
            this.panelSelectView.Controls.Add(this.textBoxSelectStudy);
            this.panelSelectView.Controls.Add(this.labelSelectStudy);
            this.panelSelectView.Controls.Add(this.label7);
            this.panelSelectView.Controls.Add(this.label4);
            this.panelSelectView.Controls.Add(this.textBoxSelectDevice);
            this.panelSelectView.Controls.Add(this.label3);
            this.panelSelectView.Controls.Add(this.textBoxSelectPatID);
            this.panelSelectView.Controls.Add(this.label2);
            this.panelSelectView.Controls.Add(this.textBoxSelectName);
            this.panelSelectView.Controls.Add(this.label6);
            this.panelSelectView.Controls.Add(this.textBoxSelectType);
            this.panelSelectView.Controls.Add(this.label1);
            this.panelSelectView.Controls.Add(this.textBoxSelectRem);
            this.panelSelectView.Controls.Add(this.panel23);
            this.panelSelectView.Controls.Add(this.buttonClear);
            this.panelSelectView.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSelectView.Location = new System.Drawing.Point(0, 0);
            this.panelSelectView.Name = "panelSelectView";
            this.panelSelectView.Size = new System.Drawing.Size(884, 26);
            this.panelSelectView.TabIndex = 2;
            // 
            // textBoxSelectStudy
            // 
            this.textBoxSelectStudy.Dock = System.Windows.Forms.DockStyle.Left;
            this.textBoxSelectStudy.Font = new System.Drawing.Font("Arial", 12F);
            this.textBoxSelectStudy.Location = new System.Drawing.Point(122, 0);
            this.textBoxSelectStudy.Name = "textBoxSelectStudy";
            this.textBoxSelectStudy.Size = new System.Drawing.Size(63, 26);
            this.textBoxSelectStudy.TabIndex = 12;
            this.textBoxSelectStudy.Text = "S12345";
            this.textBoxSelectStudy.DoubleClick += new System.EventHandler(this.textBoxSelectStudy_DoubleClick);
            // 
            // labelSelectStudy
            // 
            this.labelSelectStudy.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSelectStudy.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSelectStudy.Location = new System.Drawing.Point(70, 0);
            this.labelSelectStudy.Name = "labelSelectStudy";
            this.labelSelectStudy.Size = new System.Drawing.Size(52, 26);
            this.labelSelectStudy.TabIndex = 11;
            this.labelSelectStudy.Text = "Study:";
            this.labelSelectStudy.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelSelectStudy.Click += new System.EventHandler(this.labelSelectStudy_Click);
            // 
            // label7
            // 
            this.label7.Dock = System.Windows.Forms.DockStyle.Left;
            this.label7.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(0, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 26);
            this.label7.TabIndex = 20;
            this.label7.Text = "Search:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Right;
            this.label4.Font = new System.Drawing.Font("Arial", 12F);
            this.label4.Location = new System.Drawing.Point(184, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 26);
            this.label4.TabIndex = 10;
            this.label4.Text = "Device:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxSelectDevice
            // 
            this.textBoxSelectDevice.Dock = System.Windows.Forms.DockStyle.Right;
            this.textBoxSelectDevice.Font = new System.Drawing.Font("Arial", 12F);
            this.textBoxSelectDevice.Location = new System.Drawing.Point(246, 0);
            this.textBoxSelectDevice.Name = "textBoxSelectDevice";
            this.textBoxSelectDevice.Size = new System.Drawing.Size(91, 26);
            this.textBoxSelectDevice.TabIndex = 13;
            this.textBoxSelectDevice.Text = "AA980000401";
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Right;
            this.label3.Font = new System.Drawing.Font("Arial", 12F);
            this.label3.Location = new System.Drawing.Point(337, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 26);
            this.label3.TabIndex = 9;
            this.label3.Text = "Pat.ID:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxSelectPatID
            // 
            this.textBoxSelectPatID.Dock = System.Windows.Forms.DockStyle.Right;
            this.textBoxSelectPatID.Font = new System.Drawing.Font("Arial", 12F);
            this.textBoxSelectPatID.Location = new System.Drawing.Point(400, 0);
            this.textBoxSelectPatID.Name = "textBoxSelectPatID";
            this.textBoxSelectPatID.Size = new System.Drawing.Size(62, 26);
            this.textBoxSelectPatID.TabIndex = 14;
            this.textBoxSelectPatID.Text = "PatID";
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Right;
            this.label2.Font = new System.Drawing.Font("Arial", 12F);
            this.label2.Location = new System.Drawing.Point(462, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 26);
            this.label2.TabIndex = 8;
            this.label2.Text = "Name:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxSelectName
            // 
            this.textBoxSelectName.Dock = System.Windows.Forms.DockStyle.Right;
            this.textBoxSelectName.Font = new System.Drawing.Font("Arial", 12F);
            this.textBoxSelectName.Location = new System.Drawing.Point(521, 0);
            this.textBoxSelectName.Name = "textBoxSelectName";
            this.textBoxSelectName.Size = new System.Drawing.Size(62, 26);
            this.textBoxSelectName.TabIndex = 15;
            this.textBoxSelectName.Text = "Name";
            // 
            // label6
            // 
            this.label6.Dock = System.Windows.Forms.DockStyle.Right;
            this.label6.Font = new System.Drawing.Font("Arial", 12F);
            this.label6.Location = new System.Drawing.Point(583, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 26);
            this.label6.TabIndex = 18;
            this.label6.Text = "Type:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxSelectType
            // 
            this.textBoxSelectType.Dock = System.Windows.Forms.DockStyle.Right;
            this.textBoxSelectType.Font = new System.Drawing.Font("Arial", 12F);
            this.textBoxSelectType.Location = new System.Drawing.Point(630, 0);
            this.textBoxSelectType.Name = "textBoxSelectType";
            this.textBoxSelectType.Size = new System.Drawing.Size(62, 26);
            this.textBoxSelectType.TabIndex = 19;
            this.textBoxSelectType.Text = "type";
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Right;
            this.label1.Font = new System.Drawing.Font("Arial", 12F);
            this.label1.Location = new System.Drawing.Point(692, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 26);
            this.label1.TabIndex = 7;
            this.label1.Text = "Remark:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxSelectRem
            // 
            this.textBoxSelectRem.Dock = System.Windows.Forms.DockStyle.Right;
            this.textBoxSelectRem.Font = new System.Drawing.Font("Arial", 12F);
            this.textBoxSelectRem.Location = new System.Drawing.Point(762, 0);
            this.textBoxSelectRem.Name = "textBoxSelectRem";
            this.textBoxSelectRem.Size = new System.Drawing.Size(62, 26);
            this.textBoxSelectRem.TabIndex = 16;
            this.textBoxSelectRem.Text = "Rem";
            // 
            // panel23
            // 
            this.panel23.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel23.Location = new System.Drawing.Point(824, 0);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(5, 26);
            this.panel23.TabIndex = 21;
            // 
            // buttonClear
            // 
            this.buttonClear.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonClear.Font = new System.Drawing.Font("Arial", 12F);
            this.buttonClear.Location = new System.Drawing.Point(829, 0);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(55, 26);
            this.buttonClear.TabIndex = 17;
            this.buttonClear.Text = "Clear";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // panelView
            // 
            this.panelView.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelView.Controls.Add(this.panel32);
            this.panelView.Controls.Add(this.checkBoxHideS0);
            this.panelView.Controls.Add(this.checkBoxAnonymize);
            this.panelView.Controls.Add(this.label9);
            this.panelView.Controls.Add(this.radioButtonViewAll);
            this.panelView.Controls.Add(this.radioButtonCurrentPatID);
            this.panelView.Controls.Add(this.radioButtonCurrentDevice);
            this.panelView.Controls.Add(this.radioButtonNoStudy);
            this.panelView.Controls.Add(this.panel14);
            this.panelView.Controls.Add(this.comboBoxTriageSort);
            this.panelView.Controls.Add(this.label8);
            this.panelView.Controls.Add(this.comboBoxEventShowCh);
            this.panelView.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelView.Location = new System.Drawing.Point(0, 61);
            this.panelView.Name = "panelView";
            this.panelView.Size = new System.Drawing.Size(884, 26);
            this.panelView.TabIndex = 1;
            // 
            // panel32
            // 
            this.panel32.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel32.Location = new System.Drawing.Point(572, 0);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(10, 26);
            this.panel32.TabIndex = 28;
            // 
            // checkBoxHideS0
            // 
            this.checkBoxHideS0.AutoSize = true;
            this.checkBoxHideS0.Dock = System.Windows.Forms.DockStyle.Right;
            this.checkBoxHideS0.Font = new System.Drawing.Font("Arial", 12F);
            this.checkBoxHideS0.Location = new System.Drawing.Point(582, 0);
            this.checkBoxHideS0.Name = "checkBoxHideS0";
            this.checkBoxHideS0.Size = new System.Drawing.Size(84, 26);
            this.checkBoxHideS0.TabIndex = 27;
            this.checkBoxHideS0.Text = "Hide S0";
            this.checkBoxHideS0.UseVisualStyleBackColor = true;
            // 
            // checkBoxAnonymize
            // 
            this.checkBoxAnonymize.AutoSize = true;
            this.checkBoxAnonymize.Dock = System.Windows.Forms.DockStyle.Right;
            this.checkBoxAnonymize.Font = new System.Drawing.Font("Arial", 12F);
            this.checkBoxAnonymize.Location = new System.Drawing.Point(666, 0);
            this.checkBoxAnonymize.Name = "checkBoxAnonymize";
            this.checkBoxAnonymize.Size = new System.Drawing.Size(103, 26);
            this.checkBoxAnonymize.TabIndex = 26;
            this.checkBoxAnonymize.Text = "Anonymize";
            this.checkBoxAnonymize.UseVisualStyleBackColor = true;
            this.checkBoxAnonymize.CheckedChanged += new System.EventHandler(this.checkBoxAnonymize_CheckedChanged);
            // 
            // label9
            // 
            this.label9.Dock = System.Windows.Forms.DockStyle.Right;
            this.label9.Font = new System.Drawing.Font("Arial", 12F);
            this.label9.Location = new System.Drawing.Point(769, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 26);
            this.label9.TabIndex = 18;
            this.label9.Text = "Show ch:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // radioButtonViewAll
            // 
            this.radioButtonViewAll.Checked = true;
            this.radioButtonViewAll.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioButtonViewAll.Font = new System.Drawing.Font("Arial", 12F);
            this.radioButtonViewAll.Location = new System.Drawing.Point(532, 0);
            this.radioButtonViewAll.Name = "radioButtonViewAll";
            this.radioButtonViewAll.Size = new System.Drawing.Size(48, 26);
            this.radioButtonViewAll.TabIndex = 0;
            this.radioButtonViewAll.TabStop = true;
            this.radioButtonViewAll.Text = "All";
            this.radioButtonViewAll.UseVisualStyleBackColor = true;
            this.radioButtonViewAll.CheckedChanged += new System.EventHandler(this.radioButtonViewAll_CheckedChanged);
            // 
            // radioButtonCurrentPatID
            // 
            this.radioButtonCurrentPatID.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioButtonCurrentPatID.Font = new System.Drawing.Font("Arial", 12F);
            this.radioButtonCurrentPatID.Location = new System.Drawing.Point(405, 0);
            this.radioButtonCurrentPatID.Name = "radioButtonCurrentPatID";
            this.radioButtonCurrentPatID.Size = new System.Drawing.Size(127, 26);
            this.radioButtonCurrentPatID.TabIndex = 7;
            this.radioButtonCurrentPatID.TabStop = true;
            this.radioButtonCurrentPatID.Text = "Current Pat.ID";
            this.radioButtonCurrentPatID.UseVisualStyleBackColor = true;
            this.radioButtonCurrentPatID.CheckedChanged += new System.EventHandler(this.radioButtonCurrentPatID_CheckedChanged);
            // 
            // radioButtonCurrentDevice
            // 
            this.radioButtonCurrentDevice.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioButtonCurrentDevice.Font = new System.Drawing.Font("Arial", 12F);
            this.radioButtonCurrentDevice.Location = new System.Drawing.Point(275, 0);
            this.radioButtonCurrentDevice.Name = "radioButtonCurrentDevice";
            this.radioButtonCurrentDevice.Size = new System.Drawing.Size(130, 26);
            this.radioButtonCurrentDevice.TabIndex = 4;
            this.radioButtonCurrentDevice.TabStop = true;
            this.radioButtonCurrentDevice.Text = "Current Device";
            this.radioButtonCurrentDevice.UseVisualStyleBackColor = true;
            this.radioButtonCurrentDevice.CheckedChanged += new System.EventHandler(this.radioButtonCurrentDevice_CheckedChanged);
            // 
            // radioButtonNoStudy
            // 
            this.radioButtonNoStudy.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioButtonNoStudy.Font = new System.Drawing.Font("Arial", 12F);
            this.radioButtonNoStudy.Location = new System.Drawing.Point(188, 0);
            this.radioButtonNoStudy.Name = "radioButtonNoStudy";
            this.radioButtonNoStudy.Size = new System.Drawing.Size(87, 26);
            this.radioButtonNoStudy.TabIndex = 8;
            this.radioButtonNoStudy.Text = "No study";
            this.radioButtonNoStudy.UseVisualStyleBackColor = true;
            this.radioButtonNoStudy.CheckedChanged += new System.EventHandler(this.radioButtonNoStudy_CheckedChanged);
            // 
            // panel14
            // 
            this.panel14.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel14.Location = new System.Drawing.Point(184, 0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(4, 26);
            this.panel14.TabIndex = 14;
            // 
            // comboBoxTriageSort
            // 
            this.comboBoxTriageSort.Dock = System.Windows.Forms.DockStyle.Left;
            this.comboBoxTriageSort.Font = new System.Drawing.Font("Arial", 12F);
            this.comboBoxTriageSort.FormattingEnabled = true;
            this.comboBoxTriageSort.Items.AddRange(new object[] {
            "Record",
            "Device snr",
            "Received Time",
            "Event Time",
            "Event Type",
            "Patient ID",
            "Patient Name"});
            this.comboBoxTriageSort.Location = new System.Drawing.Point(54, 0);
            this.comboBoxTriageSort.Name = "comboBoxTriageSort";
            this.comboBoxTriageSort.Size = new System.Drawing.Size(130, 26);
            this.comboBoxTriageSort.TabIndex = 12;
            this.comboBoxTriageSort.Text = "Received Time";
            // 
            // label8
            // 
            this.label8.Dock = System.Windows.Forms.DockStyle.Left;
            this.label8.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(0, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 26);
            this.label8.TabIndex = 11;
            this.label8.Text = "Sort:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comboBoxEventShowCh
            // 
            this.comboBoxEventShowCh.Dock = System.Windows.Forms.DockStyle.Right;
            this.comboBoxEventShowCh.Font = new System.Drawing.Font("Arial", 12F);
            this.comboBoxEventShowCh.FormattingEnabled = true;
            this.comboBoxEventShowCh.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBoxEventShowCh.Location = new System.Drawing.Point(840, 0);
            this.comboBoxEventShowCh.Name = "comboBoxEventShowCh";
            this.comboBoxEventShowCh.Size = new System.Drawing.Size(44, 26);
            this.comboBoxEventShowCh.TabIndex = 19;
            this.comboBoxEventShowCh.Text = "^1";
            // 
            // panel26
            // 
            this.panel26.Controls.Add(this.panel27);
            this.panel26.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel26.Location = new System.Drawing.Point(0, 0);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(196, 87);
            this.panel26.TabIndex = 1;
            // 
            // panel27
            // 
            this.panel27.Controls.Add(this.buttonSelectLast);
            this.panel27.Controls.Add(this.panel59);
            this.panel27.Controls.Add(this.buttonSelectFirst);
            this.panel27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel27.Location = new System.Drawing.Point(0, 0);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(196, 87);
            this.panel27.TabIndex = 0;
            // 
            // buttonSelectLast
            // 
            this.buttonSelectLast.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonSelectLast.Font = new System.Drawing.Font("Webdings", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.buttonSelectLast.Location = new System.Drawing.Point(161, 0);
            this.buttonSelectLast.Name = "buttonSelectLast";
            this.buttonSelectLast.Size = new System.Drawing.Size(34, 87);
            this.buttonSelectLast.TabIndex = 7;
            this.buttonSelectLast.Text = ":";
            this.buttonSelectLast.UseVisualStyleBackColor = true;
            this.buttonSelectLast.Click += new System.EventHandler(this.buttonViewLast_Click);
            // 
            // panel59
            // 
            this.panel59.Controls.Add(this.panel31);
            this.panel59.Controls.Add(this.panel60);
            this.panel59.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel59.Location = new System.Drawing.Point(34, 0);
            this.panel59.Name = "panel59";
            this.panel59.Size = new System.Drawing.Size(127, 87);
            this.panel59.TabIndex = 12;
            // 
            // panel31
            // 
            this.panel31.Controls.Add(this.label12);
            this.panel31.Controls.Add(this.checkBoxUseLimit);
            this.panel31.Controls.Add(this.panel44);
            this.panel31.Controls.Add(this.panel40);
            this.panel31.Controls.Add(this.panel22);
            this.panel31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel31.Location = new System.Drawing.Point(0, 2);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(127, 85);
            this.panel31.TabIndex = 12;
            // 
            // label12
            // 
            this.label12.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label12.Font = new System.Drawing.Font("Arial", 12F);
            this.label12.Location = new System.Drawing.Point(4, 41);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(123, 22);
            this.label12.TabIndex = 15;
            this.label12.Text = "Events shown:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // checkBoxUseLimit
            // 
            this.checkBoxUseLimit.AutoSize = true;
            this.checkBoxUseLimit.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkBoxUseLimit.Font = new System.Drawing.Font("Arial", 12F);
            this.checkBoxUseLimit.Location = new System.Drawing.Point(4, 20);
            this.checkBoxUseLimit.Name = "checkBoxUseLimit";
            this.checkBoxUseLimit.Size = new System.Drawing.Size(123, 22);
            this.checkBoxUseLimit.TabIndex = 14;
            this.checkBoxUseLimit.Text = "Use Limits";
            this.checkBoxUseLimit.UseVisualStyleBackColor = true;
            // 
            // panel44
            // 
            this.panel44.Controls.Add(this.labelTriageRow);
            this.panel44.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel44.Location = new System.Drawing.Point(4, 63);
            this.panel44.Name = "panel44";
            this.panel44.Size = new System.Drawing.Size(123, 22);
            this.panel44.TabIndex = 13;
            // 
            // labelTriageRow
            // 
            this.labelTriageRow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelTriageRow.Font = new System.Drawing.Font("Arial", 12F);
            this.labelTriageRow.Location = new System.Drawing.Point(0, 0);
            this.labelTriageRow.Name = "labelTriageRow";
            this.labelTriageRow.Size = new System.Drawing.Size(123, 22);
            this.labelTriageRow.TabIndex = 13;
            this.labelTriageRow.Text = "^10000 / 10000!";
            this.labelTriageRow.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel40
            // 
            this.panel40.Controls.Add(this.labelRowLimit);
            this.panel40.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel40.Location = new System.Drawing.Point(4, 0);
            this.panel40.Name = "panel40";
            this.panel40.Size = new System.Drawing.Size(123, 20);
            this.panel40.TabIndex = 12;
            // 
            // labelRowLimit
            // 
            this.labelRowLimit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelRowLimit.Font = new System.Drawing.Font("Arial", 12F);
            this.labelRowLimit.Location = new System.Drawing.Point(0, 0);
            this.labelRowLimit.Name = "labelRowLimit";
            this.labelRowLimit.Size = new System.Drawing.Size(123, 20);
            this.labelRowLimit.TabIndex = 1;
            this.labelRowLimit.Text = "^10000/10000";
            this.labelRowLimit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelRowLimit.Click += new System.EventHandler(this.labelRowLimit_Click);
            // 
            // panel22
            // 
            this.panel22.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel22.Location = new System.Drawing.Point(0, 0);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(4, 85);
            this.panel22.TabIndex = 16;
            // 
            // panel60
            // 
            this.panel60.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel60.Location = new System.Drawing.Point(0, 0);
            this.panel60.Margin = new System.Windows.Forms.Padding(0);
            this.panel60.Name = "panel60";
            this.panel60.Size = new System.Drawing.Size(127, 2);
            this.panel60.TabIndex = 0;
            // 
            // buttonSelectFirst
            // 
            this.buttonSelectFirst.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonSelectFirst.Font = new System.Drawing.Font("Webdings", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.buttonSelectFirst.Location = new System.Drawing.Point(0, 0);
            this.buttonSelectFirst.Name = "buttonSelectFirst";
            this.buttonSelectFirst.Size = new System.Drawing.Size(34, 87);
            this.buttonSelectFirst.TabIndex = 2;
            this.buttonSelectFirst.Text = "9";
            this.buttonSelectFirst.UseVisualStyleBackColor = true;
            this.buttonSelectFirst.Click += new System.EventHandler(this.buttonViewFirst_Click);
            // 
            // panelEventInfo
            // 
            this.panelEventInfo.BackColor = System.Drawing.Color.Gainsboro;
            this.panelEventInfo.Controls.Add(this.panel25);
            this.panelEventInfo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelEventInfo.Location = new System.Drawing.Point(0, 355);
            this.panelEventInfo.Name = "panelEventInfo";
            this.panelEventInfo.Size = new System.Drawing.Size(1691, 104);
            this.panelEventInfo.TabIndex = 46;
            // 
            // panel25
            // 
            this.panel25.Controls.Add(this.panelStripInfo);
            this.panel25.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel25.Location = new System.Drawing.Point(0, 1);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(1691, 103);
            this.panel25.TabIndex = 6;
            // 
            // panelStripInfo
            // 
            this.panelStripInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.panelStripInfo.Controls.Add(this.textStudyFreeNote);
            this.panelStripInfo.Controls.Add(this.panel29);
            this.panelStripInfo.Controls.Add(this.panel58);
            this.panelStripInfo.Controls.Add(this.panel34);
            this.panelStripInfo.Controls.Add(this.panel33);
            this.panelStripInfo.Controls.Add(this.panelPriority);
            this.panelStripInfo.Controls.Add(this.panel41);
            this.panelStripInfo.Controls.Add(this.panel66);
            this.panelStripInfo.Controls.Add(this.panel45);
            this.panelStripInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelStripInfo.Location = new System.Drawing.Point(0, 0);
            this.panelStripInfo.Name = "panelStripInfo";
            this.panelStripInfo.Size = new System.Drawing.Size(1691, 103);
            this.panelStripInfo.TabIndex = 0;
            // 
            // textStudyFreeNote
            // 
            this.textStudyFreeNote.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.textStudyFreeNote.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textStudyFreeNote.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textStudyFreeNote.Location = new System.Drawing.Point(926, 0);
            this.textStudyFreeNote.Multiline = true;
            this.textStudyFreeNote.Name = "textStudyFreeNote";
            this.textStudyFreeNote.ReadOnly = true;
            this.textStudyFreeNote.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textStudyFreeNote.Size = new System.Drawing.Size(193, 103);
            this.textStudyFreeNote.TabIndex = 19;
            this.textStudyFreeNote.Text = "Study Free note xxxxxxxxxxxxxx\r\n2\r\n3\r\n4\r\n5\r\n6\r\n7\r\n8\r\n9\r\n0\r\n";
            // 
            // panel29
            // 
            this.panel29.Controls.Add(this.textBoxRecordDir);
            this.panel29.Controls.Add(this.labelStudyTrial);
            this.panel29.Controls.Add(this.labelStudyClient);
            this.panel29.Controls.Add(this.panel36);
            this.panel29.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel29.Location = new System.Drawing.Point(701, 0);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(225, 103);
            this.panel29.TabIndex = 15;
            // 
            // textBoxRecordDir
            // 
            this.textBoxRecordDir.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.textBoxRecordDir.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxRecordDir.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxRecordDir.Font = new System.Drawing.Font("Arial", 8F);
            this.textBoxRecordDir.ForeColor = System.Drawing.Color.Gray;
            this.textBoxRecordDir.Location = new System.Drawing.Point(0, 63);
            this.textBoxRecordDir.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.textBoxRecordDir.Multiline = true;
            this.textBoxRecordDir.Name = "textBoxRecordDir";
            this.textBoxRecordDir.ReadOnly = true;
            this.textBoxRecordDir.Size = new System.Drawing.Size(225, 40);
            this.textBoxRecordDir.TabIndex = 5;
            this.textBoxRecordDir.Text = "ym201701\\D12       storedAt\r\nhgdfhgash\r\nhdkjakhfkjj";
            this.textBoxRecordDir.TextChanged += new System.EventHandler(this.textBoxRecordDir_TextChanged_1);
            this.textBoxRecordDir.DoubleClick += new System.EventHandler(this.textBoxRecordDir_DoubleClick);
            this.textBoxRecordDir.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.textBoxRecordDir_MouseDoubleClick);
            // 
            // labelStudyTrial
            // 
            this.labelStudyTrial.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStudyTrial.Location = new System.Drawing.Point(0, 47);
            this.labelStudyTrial.Name = "labelStudyTrial";
            this.labelStudyTrial.Size = new System.Drawing.Size(225, 16);
            this.labelStudyTrial.TabIndex = 7;
            this.labelStudyTrial.Text = "Trial: XXXXXXXXXXXXjj";
            this.labelStudyTrial.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStudyClient
            // 
            this.labelStudyClient.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStudyClient.Location = new System.Drawing.Point(0, 31);
            this.labelStudyClient.Name = "labelStudyClient";
            this.labelStudyClient.Size = new System.Drawing.Size(225, 16);
            this.labelStudyClient.TabIndex = 6;
            this.labelStudyClient.Text = "Client: XXXXXXXXXXXXjj";
            this.labelStudyClient.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelStudyClient.Click += new System.EventHandler(this.labelStudyClient_Click);
            // 
            // panel36
            // 
            this.panel36.Controls.Add(this.labelStudyNrEvents);
            this.panel36.Controls.Add(this.buttonFindings);
            this.panel36.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel36.Location = new System.Drawing.Point(0, 0);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(225, 31);
            this.panel36.TabIndex = 2;
            // 
            // labelStudyNrEvents
            // 
            this.labelStudyNrEvents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelStudyNrEvents.Location = new System.Drawing.Point(95, 0);
            this.labelStudyNrEvents.Name = "labelStudyNrEvents";
            this.labelStudyNrEvents.Size = new System.Drawing.Size(130, 31);
            this.labelStudyNrEvents.TabIndex = 3;
            this.labelStudyNrEvents.Text = "Events: xx\r\nReports: yyy";
            this.labelStudyNrEvents.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // buttonFindings
            // 
            this.buttonFindings.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonFindings.Font = new System.Drawing.Font("Arial", 12F);
            this.buttonFindings.Image = ((System.Drawing.Image)(resources.GetObject("buttonFindings.Image")));
            this.buttonFindings.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonFindings.Location = new System.Drawing.Point(0, 0);
            this.buttonFindings.Name = "buttonFindings";
            this.buttonFindings.Size = new System.Drawing.Size(95, 31);
            this.buttonFindings.TabIndex = 4;
            this.buttonFindings.Text = "Findings";
            this.buttonFindings.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonFindings.UseVisualStyleBackColor = true;
            this.buttonFindings.Click += new System.EventHandler(this.buttonFindings_Click);
            // 
            // panel58
            // 
            this.panel58.Controls.Add(this.labelModelNr);
            this.panel58.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel58.Location = new System.Drawing.Point(1119, 0);
            this.panel58.Name = "panel58";
            this.panel58.Size = new System.Drawing.Size(139, 103);
            this.panel58.TabIndex = 13;
            // 
            // labelModelNr
            // 
            this.labelModelNr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelModelNr.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelModelNr.Location = new System.Drawing.Point(0, 0);
            this.labelModelNr.Name = "labelModelNr";
            this.labelModelNr.Size = new System.Drawing.Size(139, 103);
            this.labelModelNr.TabIndex = 0;
            this.labelModelNr.Text = ".";
            this.labelModelNr.Click += new System.EventHandler(this.labelModelNr_Click_1);
            this.labelModelNr.DoubleClick += new System.EventHandler(this.labelModelNr_DoubleClick);
            // 
            // panel34
            // 
            this.panel34.Controls.Add(this.textBoxStudyRemark);
            this.panel34.Controls.Add(this.panel35);
            this.panel34.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel34.Location = new System.Drawing.Point(287, 0);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(414, 103);
            this.panel34.TabIndex = 2;
            // 
            // textBoxStudyRemark
            // 
            this.textBoxStudyRemark.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(236)))), ((int)(((byte)(247)))));
            this.textBoxStudyRemark.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxStudyRemark.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxStudyRemark.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxStudyRemark.Location = new System.Drawing.Point(0, 31);
            this.textBoxStudyRemark.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.textBoxStudyRemark.Multiline = true;
            this.textBoxStudyRemark.Name = "textBoxStudyRemark";
            this.textBoxStudyRemark.ReadOnly = true;
            this.textBoxStudyRemark.Size = new System.Drawing.Size(414, 72);
            this.textBoxStudyRemark.TabIndex = 4;
            this.textBoxStudyRemark.Text = "<procedure code>\r\n2\r\n1234567890123456789012345678901234567890\r\n<remark>";
            // 
            // panel35
            // 
            this.panel35.Controls.Add(this.labelStudyPermissions);
            this.panel35.Controls.Add(this.labelStudyReports);
            this.panel35.Controls.Add(this.labelTriageStudy);
            this.panel35.Controls.Add(this.labelStudyIdText);
            this.panel35.Controls.Add(this.buttonPdfView);
            this.panel35.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel35.Location = new System.Drawing.Point(0, 0);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(414, 31);
            this.panel35.TabIndex = 2;
            // 
            // labelStudyPermissions
            // 
            this.labelStudyPermissions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelStudyPermissions.Font = new System.Drawing.Font("Arial", 8F);
            this.labelStudyPermissions.Location = new System.Drawing.Point(143, 0);
            this.labelStudyPermissions.Name = "labelStudyPermissions";
            this.labelStudyPermissions.Size = new System.Drawing.Size(194, 31);
            this.labelStudyPermissions.TabIndex = 5;
            this.labelStudyPermissions.Text = "^<site 01+ Site 02 + MCT physiscian+ Event technician >";
            this.labelStudyPermissions.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStudyReports
            // 
            this.labelStudyReports.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelStudyReports.Image = ((System.Drawing.Image)(resources.GetObject("labelStudyReports.Image")));
            this.labelStudyReports.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelStudyReports.Location = new System.Drawing.Point(337, 0);
            this.labelStudyReports.Name = "labelStudyReports";
            this.labelStudyReports.Size = new System.Drawing.Size(42, 31);
            this.labelStudyReports.TabIndex = 6;
            this.labelStudyReports.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelStudyReports.Visible = false;
            this.labelStudyReports.Click += new System.EventHandler(this.labelStudyReports_Click);
            // 
            // labelTriageStudy
            // 
            this.labelTriageStudy.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelTriageStudy.Location = new System.Drawing.Point(78, 0);
            this.labelTriageStudy.Margin = new System.Windows.Forms.Padding(0);
            this.labelTriageStudy.Name = "labelTriageStudy";
            this.labelTriageStudy.Size = new System.Drawing.Size(65, 31);
            this.labelTriageStudy.TabIndex = 3;
            this.labelTriageStudy.Text = "^012345";
            this.labelTriageStudy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelTriageStudy.DoubleClick += new System.EventHandler(this.labelTriageStudy_DoubleClick);
            // 
            // labelStudyIdText
            // 
            this.labelStudyIdText.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelStudyIdText.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStudyIdText.Image = ((System.Drawing.Image)(resources.GetObject("labelStudyIdText.Image")));
            this.labelStudyIdText.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelStudyIdText.Location = new System.Drawing.Point(0, 0);
            this.labelStudyIdText.Name = "labelStudyIdText";
            this.labelStudyIdText.Size = new System.Drawing.Size(78, 31);
            this.labelStudyIdText.TabIndex = 2;
            this.labelStudyIdText.Text = "Study:";
            this.labelStudyIdText.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelStudyIdText.Click += new System.EventHandler(this.labelStudyIdText_Click);
            // 
            // buttonPdfView
            // 
            this.buttonPdfView.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonPdfView.BackColor = System.Drawing.SystemColors.Control;
            this.buttonPdfView.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonPdfView.Image = ((System.Drawing.Image)(resources.GetObject("buttonPdfView.Image")));
            this.buttonPdfView.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonPdfView.Location = new System.Drawing.Point(379, 0);
            this.buttonPdfView.Name = "buttonPdfView";
            this.buttonPdfView.Size = new System.Drawing.Size(35, 31);
            this.buttonPdfView.TabIndex = 8;
            this.buttonPdfView.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonPdfView.UseVisualStyleBackColor = false;
            this.buttonPdfView.Click += new System.EventHandler(this.buttonPdfView_Click);
            // 
            // panel33
            // 
            this.panel33.Controls.Add(this.labelTriagePatient);
            this.panel33.Controls.Add(this.panel57);
            this.panel33.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel33.Location = new System.Drawing.Point(0, 0);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(287, 103);
            this.panel33.TabIndex = 1;
            // 
            // labelTriagePatient
            // 
            this.labelTriagePatient.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelTriagePatient.Font = new System.Drawing.Font("Arial", 10F);
            this.labelTriagePatient.Location = new System.Drawing.Point(0, 31);
            this.labelTriagePatient.Name = "labelTriagePatient";
            this.labelTriagePatient.Size = new System.Drawing.Size(287, 72);
            this.labelTriagePatient.TabIndex = 3;
            this.labelTriagePatient.Text = "John Philips";
            // 
            // panel57
            // 
            this.panel57.Controls.Add(this.labelPatientID);
            this.panel57.Controls.Add(this.labelPatIdText);
            this.panel57.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel57.Location = new System.Drawing.Point(0, 0);
            this.panel57.Name = "panel57";
            this.panel57.Size = new System.Drawing.Size(287, 31);
            this.panel57.TabIndex = 1;
            // 
            // labelPatientID
            // 
            this.labelPatientID.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPatientID.Location = new System.Drawing.Point(120, 0);
            this.labelPatientID.Name = "labelPatientID";
            this.labelPatientID.Size = new System.Drawing.Size(167, 31);
            this.labelPatientID.TabIndex = 2;
            this.labelPatientID.Text = "^-patID12345678";
            this.labelPatientID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelPatIdText
            // 
            this.labelPatIdText.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPatIdText.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPatIdText.Image = ((System.Drawing.Image)(resources.GetObject("labelPatIdText.Image")));
            this.labelPatIdText.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelPatIdText.Location = new System.Drawing.Point(0, 0);
            this.labelPatIdText.Name = "labelPatIdText";
            this.labelPatIdText.Size = new System.Drawing.Size(120, 31);
            this.labelPatIdText.TabIndex = 1;
            this.labelPatIdText.Text = "Patient ID:";
            this.labelPatIdText.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panelPriority
            // 
            this.panelPriority.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelPriority.Controls.Add(this.panel50);
            this.panelPriority.Controls.Add(this.panel51);
            this.panelPriority.Controls.Add(this.panel37);
            this.panelPriority.Controls.Add(this.panel49);
            this.panelPriority.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelPriority.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelPriority.Location = new System.Drawing.Point(1258, 0);
            this.panelPriority.Name = "panelPriority";
            this.panelPriority.Size = new System.Drawing.Size(80, 103);
            this.panelPriority.TabIndex = 4;
            this.panelPriority.Visible = false;
            // 
            // panel50
            // 
            this.panel50.Controls.Add(this.buttonLow);
            this.panel50.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel50.Location = new System.Drawing.Point(0, 68);
            this.panel50.Name = "panel50";
            this.panel50.Size = new System.Drawing.Size(78, 24);
            this.panel50.TabIndex = 3;
            // 
            // buttonLow
            // 
            this.buttonLow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonLow.Location = new System.Drawing.Point(0, 0);
            this.buttonLow.Name = "buttonLow";
            this.buttonLow.Size = new System.Drawing.Size(78, 24);
            this.buttonLow.TabIndex = 7;
            this.buttonLow.Text = "3 Low";
            this.buttonLow.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonLow.UseVisualStyleBackColor = true;
            // 
            // panel51
            // 
            this.panel51.Controls.Add(this.buttonNormal2);
            this.panel51.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel51.Location = new System.Drawing.Point(0, 44);
            this.panel51.Name = "panel51";
            this.panel51.Size = new System.Drawing.Size(78, 24);
            this.panel51.TabIndex = 4;
            // 
            // buttonNormal2
            // 
            this.buttonNormal2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonNormal2.Location = new System.Drawing.Point(0, 0);
            this.buttonNormal2.Name = "buttonNormal2";
            this.buttonNormal2.Size = new System.Drawing.Size(78, 24);
            this.buttonNormal2.TabIndex = 7;
            this.buttonNormal2.Text = "2 Normal";
            this.buttonNormal2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonNormal2.UseVisualStyleBackColor = true;
            // 
            // panel37
            // 
            this.panel37.Controls.Add(this.buttonHigh);
            this.panel37.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel37.Location = new System.Drawing.Point(0, 20);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(78, 24);
            this.panel37.TabIndex = 1;
            // 
            // buttonHigh
            // 
            this.buttonHigh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonHigh.Location = new System.Drawing.Point(0, 0);
            this.buttonHigh.Name = "buttonHigh";
            this.buttonHigh.Size = new System.Drawing.Size(78, 24);
            this.buttonHigh.TabIndex = 6;
            this.buttonHigh.Text = "1 High";
            this.buttonHigh.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonHigh.UseVisualStyleBackColor = true;
            // 
            // panel49
            // 
            this.panel49.Controls.Add(this.label14);
            this.panel49.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel49.Location = new System.Drawing.Point(0, 0);
            this.panel49.Name = "panel49";
            this.panel49.Size = new System.Drawing.Size(78, 20);
            this.panel49.TabIndex = 2;
            // 
            // label14
            // 
            this.label14.Dock = System.Windows.Forms.DockStyle.Left;
            this.label14.Font = new System.Drawing.Font("Arial", 10F);
            this.label14.Location = new System.Drawing.Point(0, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(100, 20);
            this.label14.TabIndex = 1;
            this.label14.Text = " Priority:   ";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel41
            // 
            this.panel41.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel41.Controls.Add(this.labelEventStudyInfo);
            this.panel41.Controls.Add(this.panel42);
            this.panel41.Controls.Add(this.panel63);
            this.panel41.Controls.Add(this.comboBoxTriageChannel);
            this.panel41.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel41.Location = new System.Drawing.Point(1338, 0);
            this.panel41.Name = "panel41";
            this.panel41.Size = new System.Drawing.Size(129, 103);
            this.panel41.TabIndex = 6;
            // 
            // labelEventStudyInfo
            // 
            this.labelEventStudyInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelEventStudyInfo.Font = new System.Drawing.Font("Arial", 8F);
            this.labelEventStudyInfo.Location = new System.Drawing.Point(0, 20);
            this.labelEventStudyInfo.Name = "labelEventStudyInfo";
            this.labelEventStudyInfo.Size = new System.Drawing.Size(127, 37);
            this.labelEventStudyInfo.TabIndex = 4;
            this.labelEventStudyInfo.Text = "A0 S12345.2456 P65352 C7858 T897 &&6\r\n4567278";
            this.labelEventStudyInfo.Click += new System.EventHandler(this.labelEventStudyInfo_Click);
            // 
            // panel42
            // 
            this.panel42.Controls.Add(this.labelNrChannels);
            this.panel42.Controls.Add(this.label15);
            this.panel42.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel42.Location = new System.Drawing.Point(0, 57);
            this.panel42.Name = "panel42";
            this.panel42.Size = new System.Drawing.Size(127, 20);
            this.panel42.TabIndex = 0;
            // 
            // labelNrChannels
            // 
            this.labelNrChannels.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelNrChannels.Location = new System.Drawing.Point(94, 0);
            this.labelNrChannels.Name = "labelNrChannels";
            this.labelNrChannels.Size = new System.Drawing.Size(25, 20);
            this.labelNrChannels.TabIndex = 2;
            this.labelNrChannels.Text = "3";
            this.labelNrChannels.Click += new System.EventHandler(this.labelNrChannels_Click);
            // 
            // label15
            // 
            this.label15.Dock = System.Windows.Forms.DockStyle.Left;
            this.label15.Location = new System.Drawing.Point(0, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(94, 20);
            this.label15.TabIndex = 1;
            this.label15.Text = "Channels:";
            this.label15.Click += new System.EventHandler(this.label15_Click);
            // 
            // panel63
            // 
            this.panel63.Controls.Add(this.labelRevertS0);
            this.panel63.Controls.Add(this.labelRecNr);
            this.panel63.Controls.Add(this.labelEventText);
            this.panel63.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel63.Location = new System.Drawing.Point(0, 0);
            this.panel63.Name = "panel63";
            this.panel63.Size = new System.Drawing.Size(127, 20);
            this.panel63.TabIndex = 2;
            // 
            // labelRevertS0
            // 
            this.labelRevertS0.AutoSize = true;
            this.labelRevertS0.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelRevertS0.Location = new System.Drawing.Point(113, 0);
            this.labelRevertS0.Name = "labelRevertS0";
            this.labelRevertS0.Size = new System.Drawing.Size(14, 16);
            this.labelRevertS0.TabIndex = 2;
            this.labelRevertS0.Text = "°";
            this.labelRevertS0.Click += new System.EventHandler(this.labelRevertS0_Click);
            this.labelRevertS0.DoubleClick += new System.EventHandler(this.labelRevertS0_DoubleClick);
            // 
            // labelRecNr
            // 
            this.labelRecNr.AutoSize = true;
            this.labelRecNr.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelRecNr.Location = new System.Drawing.Point(48, 0);
            this.labelRecNr.Name = "labelRecNr";
            this.labelRecNr.Size = new System.Drawing.Size(12, 16);
            this.labelRecNr.TabIndex = 1;
            this.labelRecNr.Text = ".";
            this.labelRecNr.Click += new System.EventHandler(this.labelRecNr_Click);
            this.labelRecNr.DoubleClick += new System.EventHandler(this.labelRecNr_DoubleClick);
            // 
            // labelEventText
            // 
            this.labelEventText.AutoSize = true;
            this.labelEventText.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelEventText.Location = new System.Drawing.Point(0, 0);
            this.labelEventText.Name = "labelEventText";
            this.labelEventText.Size = new System.Drawing.Size(48, 16);
            this.labelEventText.TabIndex = 0;
            this.labelEventText.Text = "Event:";
            this.labelEventText.DoubleClick += new System.EventHandler(this.labelEventText_DoubleClick);
            // 
            // comboBoxTriageChannel
            // 
            this.comboBoxTriageChannel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.comboBoxTriageChannel.FormattingEnabled = true;
            this.comboBoxTriageChannel.Items.AddRange(new object[] {
            "1. Lead I",
            "2. Lead II",
            "3. Lead III",
            "4. ECG 4",
            "5. ECG 5",
            "6. ECG 6",
            "7. ECG 7",
            "8. ECG 8",
            "9. ECG 9",
            "10. ECG 10",
            "11. ECG 11",
            "12. ECG 12"});
            this.comboBoxTriageChannel.Location = new System.Drawing.Point(0, 77);
            this.comboBoxTriageChannel.Name = "comboBoxTriageChannel";
            this.comboBoxTriageChannel.Size = new System.Drawing.Size(127, 24);
            this.comboBoxTriageChannel.TabIndex = 1;
            this.comboBoxTriageChannel.Text = "1. Lead I";
            this.comboBoxTriageChannel.SelectedIndexChanged += new System.EventHandler(this.comboBoxChannel_SelectedIndexChanged);
            // 
            // panel66
            // 
            this.panel66.Controls.Add(this.buttonAnalyzeAdd);
            this.panel66.Controls.Add(this.Anayze);
            this.panel66.Controls.Add(this.buttonSetState);
            this.panel66.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel66.Location = new System.Drawing.Point(1467, 0);
            this.panel66.Name = "panel66";
            this.panel66.Size = new System.Drawing.Size(83, 103);
            this.panel66.TabIndex = 14;
            // 
            // buttonAnalyzeAdd
            // 
            this.buttonAnalyzeAdd.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonAnalyzeAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAnalyzeAdd.Image = ((System.Drawing.Image)(resources.GetObject("buttonAnalyzeAdd.Image")));
            this.buttonAnalyzeAdd.Location = new System.Drawing.Point(0, 64);
            this.buttonAnalyzeAdd.Name = "buttonAnalyzeAdd";
            this.buttonAnalyzeAdd.Size = new System.Drawing.Size(83, 13);
            this.buttonAnalyzeAdd.TabIndex = 2;
            this.buttonAnalyzeAdd.Text = "0";
            this.buttonAnalyzeAdd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAnalyzeAdd.UseVisualStyleBackColor = true;
            this.buttonAnalyzeAdd.Visible = false;
            // 
            // Anayze
            // 
            this.Anayze.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Anayze.Dock = System.Windows.Forms.DockStyle.Top;
            this.Anayze.FlatAppearance.BorderSize = 0;
            this.Anayze.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Anayze.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Bold);
            this.Anayze.Image = ((System.Drawing.Image)(resources.GetObject("Anayze.Image")));
            this.Anayze.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Anayze.Location = new System.Drawing.Point(0, 0);
            this.Anayze.Name = "Anayze";
            this.Anayze.Size = new System.Drawing.Size(83, 58);
            this.Anayze.TabIndex = 1;
            this.Anayze.Text = "STAT";
            this.Anayze.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Anayze.UseVisualStyleBackColor = true;
            this.Anayze.Click += new System.EventHandler(this.Anayze_Click_1);
            // 
            // buttonSetState
            // 
            this.buttonSetState.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonSetState.FlatAppearance.BorderSize = 0;
            this.buttonSetState.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSetState.Font = new System.Drawing.Font("Arial", 10F);
            this.buttonSetState.Location = new System.Drawing.Point(0, 77);
            this.buttonSetState.Name = "buttonSetState";
            this.buttonSetState.Size = new System.Drawing.Size(83, 26);
            this.buttonSetState.TabIndex = 13;
            this.buttonSetState.Text = "Set State";
            this.buttonSetState.UseVisualStyleBackColor = true;
            this.buttonSetState.Click += new System.EventHandler(this.buttonSetState_Click_1);
            // 
            // panel45
            // 
            this.panel45.Controls.Add(this.FilterList);
            this.panel45.Controls.Add(this.label5);
            this.panel45.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel45.Location = new System.Drawing.Point(1550, 0);
            this.panel45.Name = "panel45";
            this.panel45.Size = new System.Drawing.Size(141, 103);
            this.panel45.TabIndex = 18;
            // 
            // FilterList
            // 
            this.FilterList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FilterList.FormattingEnabled = true;
            this.FilterList.Location = new System.Drawing.Point(0, 16);
            this.FilterList.Name = "FilterList";
            this.FilterList.Size = new System.Drawing.Size(141, 87);
            this.FilterList.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(143, 16);
            this.label5.TabIndex = 0;
            this.label5.Text = "TZ Load Quick filters:";
            // 
            // panelTriageStrip0
            // 
            this.panelTriageStrip0.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.panelTriageStrip0.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelTriageStrip0.Controls.Add(this.labelFullStrip0);
            this.panelTriageStrip0.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelTriageStrip0.Location = new System.Drawing.Point(0, 459);
            this.panelTriageStrip0.Name = "panelTriageStrip0";
            this.panelTriageStrip0.Size = new System.Drawing.Size(1691, 86);
            this.panelTriageStrip0.TabIndex = 47;
            // 
            // labelFullStrip0
            // 
            this.labelFullStrip0.BackColor = System.Drawing.Color.Transparent;
            this.labelFullStrip0.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelFullStrip0.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.labelFullStrip0.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelFullStrip0.Location = new System.Drawing.Point(1596, 0);
            this.labelFullStrip0.Name = "labelFullStrip0";
            this.labelFullStrip0.Size = new System.Drawing.Size(95, 86);
            this.labelFullStrip0.TabIndex = 0;
            this.labelFullStrip0.Text = "strip0";
            this.labelFullStrip0.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // contextMenuQuickStat
            // 
            this.contextMenuQuickStat.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.quickStatToolStripMenuItem,
            this.quickTachyToolStripMenuItem,
            this.quickBradyToolStripMenuItem,
            this.quickPauseToolStripMenuItem,
            this.quickAFIBToolStripMenuItem,
            this.quickOtherToolStripMenuItem,
            this.quickPACToolStripMenuItem,
            this.quickPVCToolStripMenuItem,
            this.quickPaceToolStripMenuItem,
            this.quickVTToolStripMenuItem,
            this.quickVFToolStripMenuItem,
            this.quickAFlToolStripMenuItem,
            this.quickASysToolStripMenuItem,
            this.quickNormalToolStripMenuItem,
            this.quickAbNormalToolStripMenuItem});
            this.contextMenuQuickStat.Name = "contextMenuQuickStat";
            this.contextMenuQuickStat.Size = new System.Drawing.Size(164, 334);
            this.contextMenuQuickStat.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuQuickStat_Opening);
            // 
            // quickStatToolStripMenuItem
            // 
            this.quickStatToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.quickStatToolStripMenuItem.Name = "quickStatToolStripMenuItem";
            this.quickStatToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.quickStatToolStripMenuItem.Text = "Quick Stat:";
            this.quickStatToolStripMenuItem.Click += new System.EventHandler(this.quickStatToolStripMenuItem_Click);
            // 
            // quickTachyToolStripMenuItem
            // 
            this.quickTachyToolStripMenuItem.Name = "quickTachyToolStripMenuItem";
            this.quickTachyToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.quickTachyToolStripMenuItem.Text = "Quick Tachy";
            this.quickTachyToolStripMenuItem.Click += new System.EventHandler(this.quickTachyToolStripMenuItem_Click);
            // 
            // quickBradyToolStripMenuItem
            // 
            this.quickBradyToolStripMenuItem.Name = "quickBradyToolStripMenuItem";
            this.quickBradyToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.quickBradyToolStripMenuItem.Text = "Quick Brady";
            this.quickBradyToolStripMenuItem.Click += new System.EventHandler(this.quickBradyToolStripMenuItem_Click);
            // 
            // quickPauseToolStripMenuItem
            // 
            this.quickPauseToolStripMenuItem.Name = "quickPauseToolStripMenuItem";
            this.quickPauseToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.quickPauseToolStripMenuItem.Text = "Quick Pause";
            this.quickPauseToolStripMenuItem.Click += new System.EventHandler(this.quickPauseToolStripMenuItem_Click);
            // 
            // quickAFIBToolStripMenuItem
            // 
            this.quickAFIBToolStripMenuItem.Name = "quickAFIBToolStripMenuItem";
            this.quickAFIBToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.quickAFIBToolStripMenuItem.Text = "Quick AFIB";
            this.quickAFIBToolStripMenuItem.Click += new System.EventHandler(this.quickAFIBToolStripMenuItem_Click);
            // 
            // quickOtherToolStripMenuItem
            // 
            this.quickOtherToolStripMenuItem.Name = "quickOtherToolStripMenuItem";
            this.quickOtherToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.quickOtherToolStripMenuItem.Text = "Quick Other";
            this.quickOtherToolStripMenuItem.Click += new System.EventHandler(this.quickOtherToolStripMenuItem_Click);
            // 
            // quickPACToolStripMenuItem
            // 
            this.quickPACToolStripMenuItem.Name = "quickPACToolStripMenuItem";
            this.quickPACToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.quickPACToolStripMenuItem.Text = "Quick PAC";
            this.quickPACToolStripMenuItem.Click += new System.EventHandler(this.quickPACToolStripMenuItem_Click);
            // 
            // quickPVCToolStripMenuItem
            // 
            this.quickPVCToolStripMenuItem.Name = "quickPVCToolStripMenuItem";
            this.quickPVCToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.quickPVCToolStripMenuItem.Text = "Quick PVC";
            this.quickPVCToolStripMenuItem.Click += new System.EventHandler(this.quickPVCToolStripMenuItem_Click);
            // 
            // quickPaceToolStripMenuItem
            // 
            this.quickPaceToolStripMenuItem.Name = "quickPaceToolStripMenuItem";
            this.quickPaceToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.quickPaceToolStripMenuItem.Text = "Quick Pace";
            this.quickPaceToolStripMenuItem.Click += new System.EventHandler(this.quickPaceToolStripMenuItem_Click);
            // 
            // quickVTToolStripMenuItem
            // 
            this.quickVTToolStripMenuItem.Name = "quickVTToolStripMenuItem";
            this.quickVTToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.quickVTToolStripMenuItem.Text = "Quick VT";
            this.quickVTToolStripMenuItem.Click += new System.EventHandler(this.quickVTToolStripMenuItem_Click);
            // 
            // quickVFToolStripMenuItem
            // 
            this.quickVFToolStripMenuItem.Name = "quickVFToolStripMenuItem";
            this.quickVFToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.quickVFToolStripMenuItem.Text = "Quick VF";
            this.quickVFToolStripMenuItem.Click += new System.EventHandler(this.quickVFToolStripMenuItem_Click);
            // 
            // quickAFlToolStripMenuItem
            // 
            this.quickAFlToolStripMenuItem.Name = "quickAFlToolStripMenuItem";
            this.quickAFlToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.quickAFlToolStripMenuItem.Text = "Quick AFl";
            this.quickAFlToolStripMenuItem.Click += new System.EventHandler(this.quickAFlToolStripMenuItem_Click);
            // 
            // quickASysToolStripMenuItem
            // 
            this.quickASysToolStripMenuItem.Name = "quickASysToolStripMenuItem";
            this.quickASysToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.quickASysToolStripMenuItem.Text = "Quick ASys";
            this.quickASysToolStripMenuItem.Click += new System.EventHandler(this.quickASysToolStripMenuItem_Click);
            // 
            // quickNormalToolStripMenuItem
            // 
            this.quickNormalToolStripMenuItem.Name = "quickNormalToolStripMenuItem";
            this.quickNormalToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.quickNormalToolStripMenuItem.Text = "Quick Normal";
            this.quickNormalToolStripMenuItem.Click += new System.EventHandler(this.quickNormalToolStripMenuItem_Click);
            // 
            // quickAbNormalToolStripMenuItem
            // 
            this.quickAbNormalToolStripMenuItem.Name = "quickAbNormalToolStripMenuItem";
            this.quickAbNormalToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.quickAbNormalToolStripMenuItem.Text = "Quick AbNormal";
            this.quickAbNormalToolStripMenuItem.Click += new System.EventHandler(this.quickAbNormalToolStripMenuItem_Click);
            // 
            // contextMenuHelp
            // 
            this.contextMenuHelp.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openManualToolStripMenuItem,
            this.openChangeHystoryToolStripMenuItem,
            this.openDownloadLinksToolStripMenuItem,
            this.openLicenseInfoToolStripMenuItem,
            this.openContentFolderToolStripMenuItem,
            this.toolStripSeparator3,
            this.changeUserPinToolStripMenuItem,
            this.toolStripMenuItem11,
            this.programInfoToolStripMenuItem,
            this.updWindowSizeToolStripMenuItem});
            this.contextMenuHelp.Name = "contextMenuHelp";
            this.contextMenuHelp.Size = new System.Drawing.Size(192, 192);
            this.contextMenuHelp.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuHelp_Opening);
            // 
            // openManualToolStripMenuItem
            // 
            this.openManualToolStripMenuItem.Name = "openManualToolStripMenuItem";
            this.openManualToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.openManualToolStripMenuItem.Text = "Open Manual";
            this.openManualToolStripMenuItem.Click += new System.EventHandler(this.openManualToolStripMenuItem_Click);
            // 
            // openChangeHystoryToolStripMenuItem
            // 
            this.openChangeHystoryToolStripMenuItem.Name = "openChangeHystoryToolStripMenuItem";
            this.openChangeHystoryToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.openChangeHystoryToolStripMenuItem.Text = "Open Change Hystory";
            this.openChangeHystoryToolStripMenuItem.Click += new System.EventHandler(this.openChangeHystoryToolStripMenuItem_Click);
            // 
            // openDownloadLinksToolStripMenuItem
            // 
            this.openDownloadLinksToolStripMenuItem.Name = "openDownloadLinksToolStripMenuItem";
            this.openDownloadLinksToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.openDownloadLinksToolStripMenuItem.Text = "Open Download links";
            this.openDownloadLinksToolStripMenuItem.Visible = false;
            this.openDownloadLinksToolStripMenuItem.Click += new System.EventHandler(this.openDownloadLinksToolStripMenuItem_Click);
            // 
            // openLicenseInfoToolStripMenuItem
            // 
            this.openLicenseInfoToolStripMenuItem.Name = "openLicenseInfoToolStripMenuItem";
            this.openLicenseInfoToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.openLicenseInfoToolStripMenuItem.Text = "Open License Info";
            this.openLicenseInfoToolStripMenuItem.Click += new System.EventHandler(this.openLicenseInfoToolStripMenuItem_Click);
            // 
            // openContentFolderToolStripMenuItem
            // 
            this.openContentFolderToolStripMenuItem.Name = "openContentFolderToolStripMenuItem";
            this.openContentFolderToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.openContentFolderToolStripMenuItem.Text = "Open Content folder";
            this.openContentFolderToolStripMenuItem.Click += new System.EventHandler(this.openContentFolderToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(188, 6);
            // 
            // changeUserPinToolStripMenuItem
            // 
            this.changeUserPinToolStripMenuItem.Name = "changeUserPinToolStripMenuItem";
            this.changeUserPinToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.changeUserPinToolStripMenuItem.Text = "Change User Pin";
            this.changeUserPinToolStripMenuItem.Click += new System.EventHandler(this.changeUserPinToolStripMenuItem_Click);
            // 
            // toolStripMenuItem11
            // 
            this.toolStripMenuItem11.Name = "toolStripMenuItem11";
            this.toolStripMenuItem11.Size = new System.Drawing.Size(188, 6);
            // 
            // programInfoToolStripMenuItem
            // 
            this.programInfoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.programInfoToClipboardToolStripMenuItem,
            this.toolStripSeparator5,
            this.openLogFileToolStripMenuItem,
            this.toolStripMenuItem10,
            this.addLogLineToolStripMenuItem,
            this.newLogFileToolStripMenuItem,
            this.toolStripSeparator4,
            this.openRecorderFolderToolStripMenuItem,
            this.toolStripSeparator6,
            this.heckDBReaderToolStripMenuItem,
            this.checkDBCollectToolStripMenuItem,
            this.testFilesDurationToolStripMenuItem,
            this.makeLauncherVersionToolStripMenuItem});
            this.programInfoToolStripMenuItem.Name = "programInfoToolStripMenuItem";
            this.programInfoToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.programInfoToolStripMenuItem.Text = "Program Info";
            // 
            // programInfoToClipboardToolStripMenuItem
            // 
            this.programInfoToClipboardToolStripMenuItem.Name = "programInfoToClipboardToolStripMenuItem";
            this.programInfoToClipboardToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.programInfoToClipboardToolStripMenuItem.Text = "Program Info to Clipboard";
            this.programInfoToClipboardToolStripMenuItem.Click += new System.EventHandler(this.programInfoToClipboardToolStripMenuItem_Click_1);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(222, 6);
            // 
            // openLogFileToolStripMenuItem
            // 
            this.openLogFileToolStripMenuItem.Name = "openLogFileToolStripMenuItem";
            this.openLogFileToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.openLogFileToolStripMenuItem.Text = "Open Log File";
            this.openLogFileToolStripMenuItem.Click += new System.EventHandler(this.openLogFileToolStripMenuItem_Click);
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(225, 22);
            this.toolStripMenuItem10.Text = "Open Log Folder";
            this.toolStripMenuItem10.Click += new System.EventHandler(this.toolStripMenuItem10_Click);
            // 
            // addLogLineToolStripMenuItem
            // 
            this.addLogLineToolStripMenuItem.Name = "addLogLineToolStripMenuItem";
            this.addLogLineToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.addLogLineToolStripMenuItem.Text = "Add Log Line";
            this.addLogLineToolStripMenuItem.Click += new System.EventHandler(this.addLogLineToolStripMenuItem_Click);
            // 
            // newLogFileToolStripMenuItem
            // 
            this.newLogFileToolStripMenuItem.Name = "newLogFileToolStripMenuItem";
            this.newLogFileToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.newLogFileToolStripMenuItem.Text = "New Log File";
            this.newLogFileToolStripMenuItem.Click += new System.EventHandler(this.newLogFileToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(222, 6);
            // 
            // openRecorderFolderToolStripMenuItem
            // 
            this.openRecorderFolderToolStripMenuItem.Name = "openRecorderFolderToolStripMenuItem";
            this.openRecorderFolderToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.openRecorderFolderToolStripMenuItem.Text = "Open Recording Folder";
            this.openRecorderFolderToolStripMenuItem.Click += new System.EventHandler(this.openRecorderFolderToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(222, 6);
            // 
            // heckDBReaderToolStripMenuItem
            // 
            this.heckDBReaderToolStripMenuItem.Name = "heckDBReaderToolStripMenuItem";
            this.heckDBReaderToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.heckDBReaderToolStripMenuItem.Text = "Check DB && Reader && Server";
            this.heckDBReaderToolStripMenuItem.Click += new System.EventHandler(this.heckDBReaderToolStripMenuItem_Click);
            // 
            // checkDBCollectToolStripMenuItem
            // 
            this.checkDBCollectToolStripMenuItem.Name = "checkDBCollectToolStripMenuItem";
            this.checkDBCollectToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.checkDBCollectToolStripMenuItem.Text = "Check DB Collect";
            this.checkDBCollectToolStripMenuItem.Click += new System.EventHandler(this.checkDBCollectToolStripMenuItem_Click);
            // 
            // testFilesDurationToolStripMenuItem
            // 
            this.testFilesDurationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createTestFilesToolStripMenuItem,
            this.countTestFilesToolStripMenuItem,
            this.readTestFilesToolStripMenuItem,
            this.deleteTestFilesToolStripMenuItem,
            this.recordingCreateTestFilesToolStripMenuItem,
            this.recordingCountTestFilesToolStripMenuItem,
            this.recordingTestFilesToolStripMenuItem,
            this.recordingDeleteTestFilesToolStripMenuItem,
            this.folderCreateTestFilesToolStripMenuItem,
            this.folderCountTestFilesToolStripMenuItem,
            this.folderReadTestFilesToolStripMenuItem,
            this.folderDeleteTestFilesToolStripMenuItem});
            this.testFilesDurationToolStripMenuItem.Name = "testFilesDurationToolStripMenuItem";
            this.testFilesDurationToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.testFilesDurationToolStripMenuItem.Text = "Test Files Duration";
            // 
            // createTestFilesToolStripMenuItem
            // 
            this.createTestFilesToolStripMenuItem.Name = "createTestFilesToolStripMenuItem";
            this.createTestFilesToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.createTestFilesToolStripMenuItem.Text = "Local - Create Test Files";
            this.createTestFilesToolStripMenuItem.Click += new System.EventHandler(this.createTestFilesToolStripMenuItem_Click);
            // 
            // countTestFilesToolStripMenuItem
            // 
            this.countTestFilesToolStripMenuItem.Name = "countTestFilesToolStripMenuItem";
            this.countTestFilesToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.countTestFilesToolStripMenuItem.Text = "Local - Count Test Files";
            this.countTestFilesToolStripMenuItem.Click += new System.EventHandler(this.countTestFilesToolStripMenuItem_Click);
            // 
            // readTestFilesToolStripMenuItem
            // 
            this.readTestFilesToolStripMenuItem.Name = "readTestFilesToolStripMenuItem";
            this.readTestFilesToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.readTestFilesToolStripMenuItem.Text = "Local - Read Test Files";
            this.readTestFilesToolStripMenuItem.Click += new System.EventHandler(this.readTestFilesToolStripMenuItem_Click);
            // 
            // deleteTestFilesToolStripMenuItem
            // 
            this.deleteTestFilesToolStripMenuItem.Name = "deleteTestFilesToolStripMenuItem";
            this.deleteTestFilesToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.deleteTestFilesToolStripMenuItem.Text = "Local - Delete Test Files";
            this.deleteTestFilesToolStripMenuItem.Click += new System.EventHandler(this.deleteTestFilesToolStripMenuItem_Click);
            // 
            // recordingCreateTestFilesToolStripMenuItem
            // 
            this.recordingCreateTestFilesToolStripMenuItem.Name = "recordingCreateTestFilesToolStripMenuItem";
            this.recordingCreateTestFilesToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.recordingCreateTestFilesToolStripMenuItem.Text = "Recording - Create Test Files";
            this.recordingCreateTestFilesToolStripMenuItem.Click += new System.EventHandler(this.recordingCreateTestFilesToolStripMenuItem_Click);
            // 
            // recordingCountTestFilesToolStripMenuItem
            // 
            this.recordingCountTestFilesToolStripMenuItem.Name = "recordingCountTestFilesToolStripMenuItem";
            this.recordingCountTestFilesToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.recordingCountTestFilesToolStripMenuItem.Text = "Recording - Count Test Files ";
            this.recordingCountTestFilesToolStripMenuItem.Click += new System.EventHandler(this.recordingCountTestFilesToolStripMenuItem_Click);
            // 
            // recordingTestFilesToolStripMenuItem
            // 
            this.recordingTestFilesToolStripMenuItem.Name = "recordingTestFilesToolStripMenuItem";
            this.recordingTestFilesToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.recordingTestFilesToolStripMenuItem.Text = "Recording - Read Test Files";
            this.recordingTestFilesToolStripMenuItem.Click += new System.EventHandler(this.recordingTestFilesToolStripMenuItem_Click);
            // 
            // recordingDeleteTestFilesToolStripMenuItem
            // 
            this.recordingDeleteTestFilesToolStripMenuItem.Name = "recordingDeleteTestFilesToolStripMenuItem";
            this.recordingDeleteTestFilesToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.recordingDeleteTestFilesToolStripMenuItem.Text = "Recording - Delete Test Files";
            this.recordingDeleteTestFilesToolStripMenuItem.Click += new System.EventHandler(this.recordingDeleteTestFilesToolStripMenuItem_Click);
            // 
            // folderCreateTestFilesToolStripMenuItem
            // 
            this.folderCreateTestFilesToolStripMenuItem.Name = "folderCreateTestFilesToolStripMenuItem";
            this.folderCreateTestFilesToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.folderCreateTestFilesToolStripMenuItem.Text = "Folder - Create Test Files";
            this.folderCreateTestFilesToolStripMenuItem.Click += new System.EventHandler(this.folderCreateTestFilesToolStripMenuItem_Click);
            // 
            // folderCountTestFilesToolStripMenuItem
            // 
            this.folderCountTestFilesToolStripMenuItem.Name = "folderCountTestFilesToolStripMenuItem";
            this.folderCountTestFilesToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.folderCountTestFilesToolStripMenuItem.Text = "Folder -  Count Test Files";
            this.folderCountTestFilesToolStripMenuItem.Click += new System.EventHandler(this.folderCountTestFilesToolStripMenuItem_Click);
            // 
            // folderReadTestFilesToolStripMenuItem
            // 
            this.folderReadTestFilesToolStripMenuItem.Name = "folderReadTestFilesToolStripMenuItem";
            this.folderReadTestFilesToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.folderReadTestFilesToolStripMenuItem.Text = "Folder -  Read Test Files";
            this.folderReadTestFilesToolStripMenuItem.Click += new System.EventHandler(this.folderReadTestFilesToolStripMenuItem_Click);
            // 
            // folderDeleteTestFilesToolStripMenuItem
            // 
            this.folderDeleteTestFilesToolStripMenuItem.Name = "folderDeleteTestFilesToolStripMenuItem";
            this.folderDeleteTestFilesToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.folderDeleteTestFilesToolStripMenuItem.Text = "Folder -  Delete Test Files";
            this.folderDeleteTestFilesToolStripMenuItem.Click += new System.EventHandler(this.folderDeleteTestFilesToolStripMenuItem_Click);
            // 
            // makeLauncherVersionToolStripMenuItem
            // 
            this.makeLauncherVersionToolStripMenuItem.Name = "makeLauncherVersionToolStripMenuItem";
            this.makeLauncherVersionToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.makeLauncherVersionToolStripMenuItem.Text = "Make Launcher Version";
            this.makeLauncherVersionToolStripMenuItem.Click += new System.EventHandler(this.makeLauncherVersionToolStripMenuItem_Click);
            // 
            // updWindowSizeToolStripMenuItem
            // 
            this.updWindowSizeToolStripMenuItem.Name = "updWindowSizeToolStripMenuItem";
            this.updWindowSizeToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.updWindowSizeToolStripMenuItem.Text = "Upd. Window Size";
            this.updWindowSizeToolStripMenuItem.Click += new System.EventHandler(this.updWindowSizeToolStripMenuItem_Click);
            // 
            // contextMenuHelpDesk
            // 
            this.contextMenuHelpDesk.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.startTMIHelpdeskToolStripMenuItem,
            this.startStandardTeamViewerToolStripMenuItem});
            this.contextMenuHelpDesk.Name = "contextMenuHelpDesk";
            this.contextMenuHelpDesk.Size = new System.Drawing.Size(217, 48);
            this.contextMenuHelpDesk.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuHelpDesk_Opening);
            // 
            // startTMIHelpdeskToolStripMenuItem
            // 
            this.startTMIHelpdeskToolStripMenuItem.Name = "startTMIHelpdeskToolStripMenuItem";
            this.startTMIHelpdeskToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.startTMIHelpdeskToolStripMenuItem.Text = "Start TMI Helpdesk";
            this.startTMIHelpdeskToolStripMenuItem.Click += new System.EventHandler(this.startTMIHelpdeskToolStripMenuItem_Click);
            // 
            // startStandardTeamViewerToolStripMenuItem
            // 
            this.startStandardTeamViewerToolStripMenuItem.Name = "startStandardTeamViewerToolStripMenuItem";
            this.startStandardTeamViewerToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.startStandardTeamViewerToolStripMenuItem.Text = "Start standard Team Viewer";
            this.startStandardTeamViewerToolStripMenuItem.Click += new System.EventHandler(this.startStandardTeamViewerToolStripMenuItem_Click);
            // 
            // panelTriageStrip1
            // 
            this.panelTriageStrip1.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.panelTriageStrip1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelTriageStrip1.Controls.Add(this.labelFullStrip1);
            this.panelTriageStrip1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelTriageStrip1.Location = new System.Drawing.Point(0, 545);
            this.panelTriageStrip1.Name = "panelTriageStrip1";
            this.panelTriageStrip1.Size = new System.Drawing.Size(1691, 86);
            this.panelTriageStrip1.TabIndex = 48;
            this.panelTriageStrip1.Visible = false;
            // 
            // labelFullStrip1
            // 
            this.labelFullStrip1.BackColor = System.Drawing.Color.Transparent;
            this.labelFullStrip1.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelFullStrip1.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.labelFullStrip1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelFullStrip1.Location = new System.Drawing.Point(1596, 0);
            this.labelFullStrip1.Name = "labelFullStrip1";
            this.labelFullStrip1.Size = new System.Drawing.Size(95, 86);
            this.labelFullStrip1.TabIndex = 1;
            this.labelFullStrip1.Text = "strip1";
            this.labelFullStrip1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panelTriageStrip2
            // 
            this.panelTriageStrip2.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.panelTriageStrip2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelTriageStrip2.Controls.Add(this.labelFullStrip2);
            this.panelTriageStrip2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelTriageStrip2.Location = new System.Drawing.Point(0, 631);
            this.panelTriageStrip2.Name = "panelTriageStrip2";
            this.panelTriageStrip2.Size = new System.Drawing.Size(1691, 86);
            this.panelTriageStrip2.TabIndex = 49;
            this.panelTriageStrip2.Visible = false;
            // 
            // labelFullStrip2
            // 
            this.labelFullStrip2.BackColor = System.Drawing.Color.Transparent;
            this.labelFullStrip2.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelFullStrip2.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.labelFullStrip2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelFullStrip2.Location = new System.Drawing.Point(1596, 0);
            this.labelFullStrip2.Name = "labelFullStrip2";
            this.labelFullStrip2.Size = new System.Drawing.Size(95, 86);
            this.labelFullStrip2.TabIndex = 1;
            this.labelFullStrip2.Text = "strip2";
            this.labelFullStrip2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // FormEventBoard
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.MidnightBlue;
            this.ClientSize = new System.Drawing.Size(1691, 739);
            this.Controls.Add(this.panelDataGrid);
            this.Controls.Add(this.panelViewColorStrip);
            this.Controls.Add(this.panelViewButtons);
            this.Controls.Add(this.panelEventInfo);
            this.Controls.Add(this.panel16);
            this.Controls.Add(this.panelStatsUser);
            this.Controls.Add(this.toolStripTop);
            this.Controls.Add(this.panelTriageStrip0);
            this.Controls.Add(this.panelTriageStrip1);
            this.Controls.Add(this.panelTriageStrip2);
            this.Controls.Add(this.statusStrip1);
            this.Font = new System.Drawing.Font("Arial", 10F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FormEventBoard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DVTMS  EventBoard - 2 v10.0.1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormEventBoard_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormEventBoard_FormClosed);
            this.Shown += new System.EventHandler(this.FormEventBoard_Shown);
            this.SizeChanged += new System.EventHandler(this.FormEventBoard_SizeChanged);
            this.toolStripTop.ResumeLayout(false);
            this.toolStripTop.PerformLayout();
            this.contextMenuSettings.ResumeLayout(false);
            this.panelStatsUser.ResumeLayout(false);
            this.panel46.ResumeLayout(false);
            this.panelLoadStats.ResumeLayout(false);
            this.panelLoadStats.PerformLayout();
            this.panelUserInfo.ResumeLayout(false);
            this.panel21.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.panel43.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel19.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.contextMenuState.ResumeLayout(false);
            this.panelViewButtons.ResumeLayout(false);
            this.panel508.ResumeLayout(false);
            this.panel510.ResumeLayout(false);
            this.panel693.ResumeLayout(false);
            this.panelDataGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridTriage)).EndInit();
            this.panel24.ResumeLayout(false);
            this.panelViewMode.ResumeLayout(false);
            this.panel64.ResumeLayout(false);
            this.panel38.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panelRight61.ResumeLayout(false);
            this.panel39.ResumeLayout(false);
            this.panel39.PerformLayout();
            this.panel47.ResumeLayout(false);
            this.panel47.PerformLayout();
            this.panel62.ResumeLayout(false);
            this.panel28.ResumeLayout(false);
            this.panel48.ResumeLayout(false);
            this.panel48.PerformLayout();
            this.panel53.ResumeLayout(false);
            this.panelSelectView.ResumeLayout(false);
            this.panelSelectView.PerformLayout();
            this.panelView.ResumeLayout(false);
            this.panelView.PerformLayout();
            this.panel26.ResumeLayout(false);
            this.panel27.ResumeLayout(false);
            this.panel59.ResumeLayout(false);
            this.panel31.ResumeLayout(false);
            this.panel31.PerformLayout();
            this.panel44.ResumeLayout(false);
            this.panel40.ResumeLayout(false);
            this.panelEventInfo.ResumeLayout(false);
            this.panel25.ResumeLayout(false);
            this.panelStripInfo.ResumeLayout(false);
            this.panelStripInfo.PerformLayout();
            this.panel29.ResumeLayout(false);
            this.panel29.PerformLayout();
            this.panel36.ResumeLayout(false);
            this.panel58.ResumeLayout(false);
            this.panel34.ResumeLayout(false);
            this.panel34.PerformLayout();
            this.panel35.ResumeLayout(false);
            this.panel33.ResumeLayout(false);
            this.panel57.ResumeLayout(false);
            this.panelPriority.ResumeLayout(false);
            this.panel50.ResumeLayout(false);
            this.panel51.ResumeLayout(false);
            this.panel37.ResumeLayout(false);
            this.panel49.ResumeLayout(false);
            this.panel41.ResumeLayout(false);
            this.panel42.ResumeLayout(false);
            this.panel63.ResumeLayout(false);
            this.panel63.PerformLayout();
            this.panel66.ResumeLayout(false);
            this.panel45.ResumeLayout(false);
            this.panel45.PerformLayout();
            this.panelTriageStrip0.ResumeLayout(false);
            this.contextMenuQuickStat.ResumeLayout(false);
            this.contextMenuHelp.ResumeLayout(false);
            this.contextMenuHelpDesk.ResumeLayout(false);
            this.panelTriageStrip1.ResumeLayout(false);
            this.panelTriageStrip2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStripTop;
        private System.Windows.Forms.ToolStripButton toolStripWorkNew;
        private System.Windows.Forms.ToolStripButton toolStripNewEvent;
        private System.Windows.Forms.ToolStripButton toolStripLoad;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripWorld;
        private System.Windows.Forms.ToolStripButton toolStripCustomers;
        private System.Windows.Forms.ToolStripButton toolStripUsers;
        private System.Windows.Forms.ToolStripButton toolStripDepartments;
        private System.Windows.Forms.ToolStripButton toolStripDevices;
        private System.Windows.Forms.ToolStripButton toolStripAlarmSignal;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripExit;
        private System.Windows.Forms.ToolStripLabel toolStripUserLabel;
        private System.Windows.Forms.ToolStripButton toolStripUserCancel;
        private System.Windows.Forms.ToolStripComboBox toolStripSelectUser;
        private System.Windows.Forms.ToolStripButton toolStripUserLock;
        private System.Windows.Forms.ToolStripButton toolStripSettings;
        private System.Windows.Forms.ContextMenuStrip contextMenuSettings;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem userSettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem globalSettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem1;
        private System.Windows.Forms.Panel panelStatsUser;
        private System.Windows.Forms.ToolStripButton toolStripUserOk;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStripButton toolStripPhysician;
        private System.Windows.Forms.Panel panelUserInfo;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label labelTime;
        private System.Windows.Forms.Label labelDate;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Button buttonRefresh;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Label label2UserMsg;
        private System.Windows.Forms.Label ValueUserMsg;
        private System.Windows.Forms.Label label1UserMsg;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label label2EventReport;
        private System.Windows.Forms.Label valueEventReport;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label label2EventAnalyze;
        private System.Windows.Forms.Label valueEventAnalyze;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label2EventTriage;
        private System.Windows.Forms.Label valueEventTriage;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label2EventTotalOpen;
        private System.Windows.Forms.Label valueEventTotalOpen;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label label2EventDone24;
        private System.Windows.Forms.Label valueEventDone24;
        private System.Windows.Forms.Label label1EventDone24;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label2ReportToDo;
        private System.Windows.Forms.Label ValueReportToDo24;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label2ProcedureOpen;
        private System.Windows.Forms.Label valueStudyOpen;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label2ProcedureDone24;
        private System.Windows.Forms.Label valueStudyDone24;
        private System.Windows.Forms.Label label1ProcedureDone24;
        private System.Windows.Forms.ToolStripMenuItem showDashBoardToolStripMenuItem;
        private System.Windows.Forms.Panel panel43;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Panel panelLoadStats;
        private System.Windows.Forms.Label labelStudyIndex;
        private System.Windows.Forms.Label labelRecIndex;
        private System.Windows.Forms.Panel panel46;
        private System.Windows.Forms.Label labelViewMode1;
        private System.Windows.Forms.Label labelUpdateTC;
        private System.Windows.Forms.Label labelUpdateTS;
        private System.Windows.Forms.Timer timerTriage;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripListReports;
        private System.Windows.Forms.ToolStripButton toolStripHelpdesk;
        private System.Windows.Forms.ContextMenuStrip contextMenuState;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem9;
        private System.Windows.Forms.ToolStripButton toolStripPatientList;
        private System.Windows.Forms.ToolStripButton toolStripAddDevice;
        private System.Windows.Forms.ToolStripButton toolStripButtonUpdate2;
        private System.Windows.Forms.Panel panelViewButtons;
        private System.Windows.Forms.Panel panel508;
        private System.Windows.Forms.Panel panel510;
        private System.Windows.Forms.Button buttonShowBaseLine;
        private System.Windows.Forms.Button buttonShowExclude;
        private System.Windows.Forms.Button buttonShowLeadOff;
        private System.Windows.Forms.Button buttonShowNoise;
        private System.Windows.Forms.Button buttonShowReported;
        private System.Windows.Forms.Button buttonShowAnalyzed;
        private System.Windows.Forms.Button buttonShowTriaged;
        private System.Windows.Forms.Button buttonShowReceived;
        private System.Windows.Forms.Button buttonShowAll;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Panel panel693;
        private System.Windows.Forms.Button buttonShowReport;
        private System.Windows.Forms.Button buttonShowAnalyze;
        private System.Windows.Forms.Button buttonShowTriage;
        private System.Windows.Forms.Panel panelViewColorStrip;
        private System.Windows.Forms.Panel panelDataGrid;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Panel panel64;
        private System.Windows.Forms.Label labelLoadImages;
        private System.Windows.Forms.Panel panelRight61;
        private System.Windows.Forms.Label labelDrive;
        private System.Windows.Forms.Button buttonTriageFields;
        private System.Windows.Forms.Panel panelViewMode;
        private System.Windows.Forms.ComboBox listBoxEventMode;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel39;
        private System.Windows.Forms.ListBox listBoxViewUnit;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.Panel panelView;
        private System.Windows.Forms.RadioButton radioButtonCurrentDevice;
        private System.Windows.Forms.RadioButton radioButtonViewAll;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Button buttonSelectLast;
        private System.Windows.Forms.Panel panel59;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.Panel panel60;
        private System.Windows.Forms.Button buttonSelectFirst;
        private System.Windows.Forms.Panel panelEventInfo;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Panel panelStripInfo;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.TextBox textBoxRecordDir;
        private System.Windows.Forms.Panel panel36;
        private System.Windows.Forms.Label labelStudyNrEvents;
        private System.Windows.Forms.Button buttonFindings;
        private System.Windows.Forms.Panel panel58;
        private System.Windows.Forms.Label labelModelNr;
        private System.Windows.Forms.Panel panelPriority;
        private System.Windows.Forms.Panel panel50;
        private System.Windows.Forms.Button buttonLow;
        private System.Windows.Forms.Panel panel51;
        private System.Windows.Forms.Button buttonNormal2;
        private System.Windows.Forms.Panel panel37;
        private System.Windows.Forms.Button buttonHigh;
        private System.Windows.Forms.Panel panel49;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel34;
        private System.Windows.Forms.TextBox textBoxStudyRemark;
        private System.Windows.Forms.Panel panel35;
        private System.Windows.Forms.Label labelStudyPermissions;
        private System.Windows.Forms.Label labelTriageStudy;
        private System.Windows.Forms.Label labelStudyIdText;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.Label labelTriagePatient;
        private System.Windows.Forms.Panel panel57;
        private System.Windows.Forms.Label labelPatientID;
        private System.Windows.Forms.Label labelPatIdText;
        private System.Windows.Forms.Panel panel41;
        private System.Windows.Forms.Panel panel42;
        private System.Windows.Forms.Label labelNrChannels;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel63;
        private System.Windows.Forms.Label labelRecNr;
        private System.Windows.Forms.Label labelEventText;
        private System.Windows.Forms.ComboBox comboBoxTriageChannel;
        private System.Windows.Forms.Panel panel66;
        private System.Windows.Forms.Button buttonAnalyzeAdd;
        private System.Windows.Forms.Button Anayze;
        private System.Windows.Forms.Panel panelTriageStrip0;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.Button buttonSetState;
        private System.Windows.Forms.Label labelStudyClient;
        private System.Windows.Forms.Label labelStudyReports;
        private System.Windows.Forms.RadioButton radioButtonCurrentPatID;
        private System.Windows.Forms.Button buttonPdfView;
        private System.Windows.Forms.Label labelEventStudyInfo;
        private System.Windows.Forms.ToolStripLabel toolStripReaderCheck;
        private System.Windows.Forms.ContextMenuStrip contextMenuQuickStat;
        private System.Windows.Forms.ToolStripMenuItem quickStatToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quickTachyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quickBradyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quickAFIBToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quickOtherToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quickPauseToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuHelp;
        private System.Windows.Forms.ToolStripMenuItem openManualToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openChangeHystoryToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuHelpDesk;
        private System.Windows.Forms.ToolStripMenuItem startTMIHelpdeskToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem startStandardTeamViewerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openLicenseInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quickPACToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quickPVCToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quickPaceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quickVTToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quickVFToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quickAFlToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quickASysToolStripMenuItem;
        private System.Windows.Forms.RadioButton radioButtonNoStudy;
        private System.Windows.Forms.Panel panelSelectView;
        private System.Windows.Forms.Label labelSelectStudy;
        private System.Windows.Forms.TextBox textBoxSelectStudy;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxSelectDevice;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxSelectPatID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxSelectName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxSelectRem;
        private System.Windows.Forms.Panel panelTriageStrip1;
        private System.Windows.Forms.Label labelFullStrip0;
        private System.Windows.Forms.Label labelFullStrip1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxSelectType;
        private System.Windows.Forms.Panel panelTriageStrip2;
        private System.Windows.Forms.Label labelFullStrip2;
        private System.Windows.Forms.ComboBox comboBoxTriageSort;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.ToolStripLabel toolTriageUpdTime2;
        private System.Windows.Forms.ToolStripMenuItem heckDBReaderToolStripMenuItem;
        private System.Windows.Forms.Label labelRevertS0;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBoxEventShowCh;
        private System.Windows.Forms.CheckBox checkBoxAnonymize;
        private System.Windows.Forms.ToolStripMenuItem quickNormalToolStripMenuItem;
        private System.Windows.Forms.DataGridView dataGridTriage;
        private System.Windows.Forms.DataGridViewTextBoxColumn Count;
        private System.Windows.Forms.DataGridViewTextBoxColumn Patient;
        private System.Windows.Forms.DataGridViewTextBoxColumn Event;
        private System.Windows.Forms.DataGridViewTextBoxColumn Read;
        private System.Windows.Forms.DataGridViewTextBoxColumn Remark;
        private System.Windows.Forms.DataGridViewImageColumn EventStrip;
        private System.Windows.Forms.DataGridViewImageColumn Triage;
        private System.Windows.Forms.DataGridViewTextBoxColumn Index;
        private System.Windows.Forms.DataGridViewTextBoxColumn State;
        private System.Windows.Forms.DataGridViewTextBoxColumn FilePath;
        private System.Windows.Forms.DataGridViewTextBoxColumn FileName;
        private System.Windows.Forms.DataGridViewTextBoxColumn NrSignals;
        private System.Windows.Forms.DataGridViewTextBoxColumn Study;
        private System.Windows.Forms.DataGridViewTextBoxColumn Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn ModelSnr;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReadUTC;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumTrailer;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.CheckBox checkBoxHideS0;
        private System.Windows.Forms.ToolStripMenuItem checkDBCollectToolStripMenuItem;
        private System.Windows.Forms.Panel panel44;
        private System.Windows.Forms.Label labelTriageRow;
        private System.Windows.Forms.Panel panel40;
        private System.Windows.Forms.Label labelRowLimit;
        private System.Windows.Forms.ToolStripMenuItem addLogLineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newLogFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testFilesDurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createTestFilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem countTestFilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem readTestFilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteTestFilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recordingCreateTestFilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recordingCountTestFilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recordingTestFilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recordingDeleteTestFilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem folderCreateTestFilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem folderCountTestFilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem folderReadTestFilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem folderDeleteTestFilesToolStripMenuItem;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialogTest;
        private System.Windows.Forms.ToolStripMenuItem openLogFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openRecorderFolderToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButtonWarn;
        private System.Windows.Forms.Label labelImageNk;
        private System.Windows.Forms.Label labelCountRows;
        private System.Windows.Forms.Label labelCountedAnalyzed;
        private System.Windows.Forms.Label labelCountedTriaged;
        private System.Windows.Forms.Label labelCountedReceived;
        private System.Windows.Forms.Label labelCountedAll;
        private System.Windows.Forms.Label labelTriageCountDays;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel38;
        private System.Windows.Forms.Label labelMemUsage;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelUpdTimeTable;
        private System.Windows.Forms.Label labelUpdTimeStrip;
        private System.Windows.Forms.Panel panel45;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckedListBox FilterList;
        private System.Windows.Forms.ToolStripMenuItem makeLauncherVersionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem programInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem programInfoToClipboardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem openDownloadLinksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openContentFolderToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.Panel panel47;
        private System.Windows.Forms.Panel panel62;
        private System.Windows.Forms.Panel panelViewDec;
        private System.Windows.Forms.Panel panelViewInc;
        private System.Windows.Forms.TextBox textBoxViewTime;
        private System.Windows.Forms.CheckBox checkBoxAllStudy;
        private System.Windows.Forms.Panel panel48;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Label labelTrial;
        private System.Windows.Forms.CheckBox checkBoxTrial;
        private System.Windows.Forms.Label labelSelectPermisions;
        private System.Windows.Forms.CheckBox checkBoxStudyPermissions;
        private System.Windows.Forms.Panel panel52;
        private System.Windows.Forms.CheckBox checkBoxUseLimit;
        private System.Windows.Forms.Label labelClient;
        private System.Windows.Forms.CheckBox checkBoxClient;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.ToolStripButton toolStripButtonTrial;
        private System.Windows.Forms.Panel panel53;
        private System.Windows.Forms.ComboBox comboBoxLocation;
        private System.Windows.Forms.Panel panel54;
        private System.Windows.Forms.Button buttonShowQCd;
        private System.Windows.Forms.Label labelStudyTrial;
        private System.Windows.Forms.ToolStripMenuItem changeUserPinToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem quickAbNormalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updWindowSizeToolStripMenuItem;
        private System.Windows.Forms.TextBox textStudyFreeNote;
    }
}

