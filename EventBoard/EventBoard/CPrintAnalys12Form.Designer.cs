﻿namespace EventBoard
{
    partial class CPrintAnalys12Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CPrintAnalys12Form));
            this.panelPrintArea1 = new System.Windows.Forms.Panel();
            this.panel150 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.labelPhysPrintName = new System.Windows.Forms.Label();
            this.labelPhysicianText = new System.Windows.Forms.Label();
            this.panel22 = new System.Windows.Forms.Panel();
            this.labelSignatureText = new System.Windows.Forms.Label();
            this.labelPhysPrintSign = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.labelFindingsRemark = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.labelFindingsText = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelActivity = new System.Windows.Forms.Label();
            this.labelSymptoms = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.labelActivityHeader = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.labelSymptomsHeader = new System.Windows.Forms.Label();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel149 = new System.Windows.Forms.Panel();
            this.panelECGSweepAmpIndicator = new System.Windows.Forms.Panel();
            this.labelBaseSweepSpeed = new System.Windows.Forms.Label();
            this.labelBaseAmplitudeSet = new System.Windows.Forms.Label();
            this.labelEventMiddle = new System.Windows.Forms.Label();
            this.labelEventStart = new System.Windows.Forms.Label();
            this.labelEventEnd = new System.Windows.Forms.Label();
            this.panelFullStrip = new System.Windows.Forms.Panel();
            this.panelFooterPage1 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.labelStudyNumberResPage1 = new System.Windows.Forms.Label();
            this.labelPrintDate1Bottom = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panel510 = new System.Windows.Forms.Panel();
            this.labelPage = new System.Windows.Forms.Label();
            this.panel509 = new System.Windows.Forms.Panel();
            this.labelPage1of = new System.Windows.Forms.Label();
            this.panel508 = new System.Windows.Forms.Panel();
            this.labelPageOf = new System.Windows.Forms.Label();
            this.panel507 = new System.Windows.Forms.Panel();
            this.labelPage1ofx = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panelLineUnderSampl1 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.labelTopStripFeed = new System.Windows.Forms.Label();
            this.labelTopStripAmpl = new System.Windows.Forms.Label();
            this.labelTopStripTime = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.labelSampleRate = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.labelHPF = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.labelNotch = new System.Windows.Forms.Label();
            this.panelTimeBarBaselineECG = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panelChannel6 = new System.Windows.Forms.Panel();
            this.panelChannel5 = new System.Windows.Forms.Panel();
            this.panelChannel4 = new System.Windows.Forms.Panel();
            this.panelChannel3 = new System.Windows.Forms.Panel();
            this.panelChannel2 = new System.Windows.Forms.Panel();
            this.panelChannel1 = new System.Windows.Forms.Panel();
            this.panelChannel12 = new System.Windows.Forms.Panel();
            this.panelChannel11 = new System.Windows.Forms.Panel();
            this.panelChannel10 = new System.Windows.Forms.Panel();
            this.panelChannel9 = new System.Windows.Forms.Panel();
            this.panelChannel8 = new System.Windows.Forms.Panel();
            this.panelChannel7 = new System.Windows.Forms.Panel();
            this.panelTimeHeadingsPerStrip = new System.Windows.Forms.Panel();
            this.panelHorSepPatDet = new System.Windows.Forms.Panel();
            this.panelPrintHeader = new System.Windows.Forms.Panel();
            this.panel179 = new System.Windows.Forms.Panel();
            this.labelRythm = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.label36 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.labelResultQTcFavg = new System.Windows.Forms.Label();
            this.labelResultQTcBavg = new System.Windows.Forms.Label();
            this.labelResultQTavg = new System.Windows.Forms.Label();
            this.labelResultQRSavg = new System.Windows.Forms.Label();
            this.labelResultPRavg = new System.Windows.Forms.Label();
            this.labelResultRRavg = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.labelResultQTcFmax = new System.Windows.Forms.Label();
            this.labelResultQTcBmax = new System.Windows.Forms.Label();
            this.labelResultQTmax = new System.Windows.Forms.Label();
            this.labelResultQRSmax = new System.Windows.Forms.Label();
            this.labelResultPRmax = new System.Windows.Forms.Label();
            this.labelResultRRmax = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.panel154 = new System.Windows.Forms.Panel();
            this.labelResultQTcFmin = new System.Windows.Forms.Label();
            this.labelResultQTcBmin = new System.Windows.Forms.Label();
            this.labelResultQTmin = new System.Windows.Forms.Label();
            this.labelResultQRSmin = new System.Windows.Forms.Label();
            this.labelResultPRmin = new System.Windows.Forms.Label();
            this.labelResultRRmin = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.panel94 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.panel93 = new System.Windows.Forms.Panel();
            this.labelPhysician = new System.Windows.Forms.Label();
            this.labelDateOfBirth = new System.Windows.Forms.Label();
            this.labelPatLastName = new System.Windows.Forms.Label();
            this.labelPatientIDResult = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel92 = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.labelDOBheader = new System.Windows.Forms.Label();
            this.labelPatientID = new System.Windows.Forms.Label();
            this.labelPatientNameHeader = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.panel61 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.labelClient = new System.Windows.Forms.Label();
            this.labelSerialNr = new System.Windows.Forms.Label();
            this.labelSingleEventReportDate = new System.Windows.Forms.Label();
            this.labelReportNr = new System.Windows.Forms.Label();
            this.labelStudyNr = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.labelClientHeader = new System.Windows.Forms.Label();
            this.labelSerialNrHeader = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.labelStudyNrHeader = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label37 = new System.Windows.Forms.Label();
            this.panel91 = new System.Windows.Forms.Panel();
            this.labelCenter2 = new System.Windows.Forms.Label();
            this.labelCenter1 = new System.Windows.Forms.Label();
            this.pictureBoxCenter1 = new System.Windows.Forms.PictureBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripGenPages = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonPrint = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripClipboard1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripClipboard2 = new System.Windows.Forms.ToolStripButton();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel21 = new System.Windows.Forms.Panel();
            this.panelPrintArea2 = new System.Windows.Forms.Panel();
            this.panelPage2Footer = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.labelPagex = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.labelPagexOfX = new System.Windows.Forms.Label();
            this.labelPrintDate2Bottom = new System.Windows.Forms.Label();
            this.labelPrintDate = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panelHeaderPage2 = new System.Windows.Forms.Panel();
            this.panel255 = new System.Windows.Forms.Panel();
            this.panel270 = new System.Windows.Forms.Panel();
            this.panel273 = new System.Windows.Forms.Panel();
            this.panel300 = new System.Windows.Forms.Panel();
            this.panel391 = new System.Windows.Forms.Panel();
            this.panel393 = new System.Windows.Forms.Panel();
            this.panel394 = new System.Windows.Forms.Panel();
            this.panel450 = new System.Windows.Forms.Panel();
            this.panel488 = new System.Windows.Forms.Panel();
            this.panel489 = new System.Windows.Forms.Panel();
            this.panel490 = new System.Windows.Forms.Panel();
            this.panel492 = new System.Windows.Forms.Panel();
            this.panel493 = new System.Windows.Forms.Panel();
            this.panelPage2Header = new System.Windows.Forms.Panel();
            this.labelPage2ReportNr = new System.Windows.Forms.Label();
            this.labelReportText2 = new System.Windows.Forms.Label();
            this.panel74 = new System.Windows.Forms.Panel();
            this.panel76 = new System.Windows.Forms.Panel();
            this.panel495 = new System.Windows.Forms.Panel();
            this.panel534 = new System.Windows.Forms.Panel();
            this.labelStudyNumberResPage2 = new System.Windows.Forms.Label();
            this.panel533 = new System.Windows.Forms.Panel();
            this.labelStudyNrPage2 = new System.Windows.Forms.Label();
            this.panel532 = new System.Windows.Forms.Panel();
            this.panel531 = new System.Windows.Forms.Panel();
            this.panel530 = new System.Windows.Forms.Panel();
            this.panel529 = new System.Windows.Forms.Panel();
            this.labelDateOfBirthResPage2 = new System.Windows.Forms.Label();
            this.panel528 = new System.Windows.Forms.Panel();
            this.labelDateOfBirthPage2 = new System.Windows.Forms.Label();
            this.panel527 = new System.Windows.Forms.Panel();
            this.panel526 = new System.Windows.Forms.Panel();
            this.panel525 = new System.Windows.Forms.Panel();
            this.panel524 = new System.Windows.Forms.Panel();
            this.panel523 = new System.Windows.Forms.Panel();
            this.panel522 = new System.Windows.Forms.Panel();
            this.panel521 = new System.Windows.Forms.Panel();
            this.panel520 = new System.Windows.Forms.Panel();
            this.panel519 = new System.Windows.Forms.Panel();
            this.panel497 = new System.Windows.Forms.Panel();
            this.panel499 = new System.Windows.Forms.Panel();
            this.panel500 = new System.Windows.Forms.Panel();
            this.panel501 = new System.Windows.Forms.Panel();
            this.panel503 = new System.Windows.Forms.Panel();
            this.labelCenter2p2 = new System.Windows.Forms.Label();
            this.panel504 = new System.Windows.Forms.Panel();
            this.labelCenter1p2 = new System.Windows.Forms.Label();
            this.panel506 = new System.Windows.Forms.Panel();
            this.pictureBoxCenter2 = new System.Windows.Forms.PictureBox();
            this.panelPrintArea1.SuspendLayout();
            this.panel150.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panelECGSweepAmpIndicator.SuspendLayout();
            this.panelFooterPage1.SuspendLayout();
            this.panel510.SuspendLayout();
            this.panel509.SuspendLayout();
            this.panel508.SuspendLayout();
            this.panel507.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panelTimeBarBaselineECG.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panelPrintHeader.SuspendLayout();
            this.panel179.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel154.SuspendLayout();
            this.panel94.SuspendLayout();
            this.panel93.SuspendLayout();
            this.panel92.SuspendLayout();
            this.panel61.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel91.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCenter1)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.panelPrintArea2.SuspendLayout();
            this.panelPage2Footer.SuspendLayout();
            this.panelHeaderPage2.SuspendLayout();
            this.panel255.SuspendLayout();
            this.panel391.SuspendLayout();
            this.panel488.SuspendLayout();
            this.panel490.SuspendLayout();
            this.panelPage2Header.SuspendLayout();
            this.panel495.SuspendLayout();
            this.panel534.SuspendLayout();
            this.panel533.SuspendLayout();
            this.panel529.SuspendLayout();
            this.panel528.SuspendLayout();
            this.panel500.SuspendLayout();
            this.panel501.SuspendLayout();
            this.panel503.SuspendLayout();
            this.panel504.SuspendLayout();
            this.panel506.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCenter2)).BeginInit();
            this.SuspendLayout();
            // 
            // panelPrintArea1
            // 
            this.panelPrintArea1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panelPrintArea1.Controls.Add(this.panel150);
            this.panelPrintArea1.Controls.Add(this.panel149);
            this.panelPrintArea1.Controls.Add(this.panelECGSweepAmpIndicator);
            this.panelPrintArea1.Controls.Add(this.panelFullStrip);
            this.panelPrintArea1.Controls.Add(this.panelFooterPage1);
            this.panelPrintArea1.Controls.Add(this.panelLineUnderSampl1);
            this.panelPrintArea1.Controls.Add(this.panel6);
            this.panelPrintArea1.Controls.Add(this.panelTimeBarBaselineECG);
            this.panelPrintArea1.Controls.Add(this.panelTimeHeadingsPerStrip);
            this.panelPrintArea1.Controls.Add(this.panelHorSepPatDet);
            this.panelPrintArea1.Controls.Add(this.panelPrintHeader);
            this.panelPrintArea1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelPrintArea1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.panelPrintArea1.Location = new System.Drawing.Point(0, 60);
            this.panelPrintArea1.Name = "panelPrintArea1";
            this.panelPrintArea1.Size = new System.Drawing.Size(3424, 2336);
            this.panelPrintArea1.TabIndex = 0;
            // 
            // panel150
            // 
            this.panel150.Controls.Add(this.panel20);
            this.panel150.Controls.Add(this.panel22);
            this.panel150.Controls.Add(this.panel3);
            this.panel150.Controls.Add(this.panel2);
            this.panel150.Controls.Add(this.panel1);
            this.panel150.Controls.Add(this.panel11);
            this.panel150.Controls.Add(this.panel16);
            this.panel150.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel150.Location = new System.Drawing.Point(0, 2051);
            this.panel150.Name = "panel150";
            this.panel150.Size = new System.Drawing.Size(3424, 230);
            this.panel150.TabIndex = 27;
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.labelPhysPrintName);
            this.panel20.Controls.Add(this.labelPhysicianText);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel20.Location = new System.Drawing.Point(2535, 0);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(436, 230);
            this.panel20.TabIndex = 4;
            // 
            // labelPhysPrintName
            // 
            this.labelPhysPrintName.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelPhysPrintName.Font = new System.Drawing.Font("Tahoma", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPhysPrintName.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.labelPhysPrintName.Location = new System.Drawing.Point(0, 114);
            this.labelPhysPrintName.Name = "labelPhysPrintName";
            this.labelPhysPrintName.Size = new System.Drawing.Size(436, 116);
            this.labelPhysPrintName.TabIndex = 2;
            this.labelPhysPrintName.Text = "^ J. Smithsonian\r\nexample";
            this.labelPhysPrintName.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // labelPhysicianText
            // 
            this.labelPhysicianText.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPhysicianText.Font = new System.Drawing.Font("Tahoma", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPhysicianText.Location = new System.Drawing.Point(0, 0);
            this.labelPhysicianText.Name = "labelPhysicianText";
            this.labelPhysicianText.Size = new System.Drawing.Size(436, 104);
            this.labelPhysicianText.TabIndex = 6;
            this.labelPhysicianText.Text = "^QC \r\nPhysician name:";
            // 
            // panel22
            // 
            this.panel22.Controls.Add(this.labelSignatureText);
            this.panel22.Controls.Add(this.labelPhysPrintSign);
            this.panel22.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel22.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel22.Location = new System.Drawing.Point(2971, 0);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(436, 230);
            this.panel22.TabIndex = 5;
            // 
            // labelSignatureText
            // 
            this.labelSignatureText.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelSignatureText.Font = new System.Drawing.Font("Tahoma", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSignatureText.Location = new System.Drawing.Point(0, 0);
            this.labelSignatureText.Name = "labelSignatureText";
            this.labelSignatureText.Size = new System.Drawing.Size(436, 104);
            this.labelSignatureText.TabIndex = 3;
            this.labelSignatureText.Text = "^QC \r\nPhysician Signature:";
            // 
            // labelPhysPrintSign
            // 
            this.labelPhysPrintSign.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelPhysPrintSign.Font = new System.Drawing.Font("Comic Sans MS", 27.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPhysPrintSign.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.labelPhysPrintSign.Location = new System.Drawing.Point(0, 114);
            this.labelPhysPrintSign.Name = "labelPhysPrintSign";
            this.labelPhysPrintSign.Size = new System.Drawing.Size(436, 116);
            this.labelPhysPrintSign.TabIndex = 4;
            this.labelPhysPrintSign.Text = "^ J. Smithsonian\r\n--xx--";
            this.labelPhysPrintSign.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // panel3
            // 
            this.panel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel3.Controls.Add(this.labelFindingsRemark);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(1004, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1417, 230);
            this.panel3.TabIndex = 3;
            // 
            // labelFindingsRemark
            // 
            this.labelFindingsRemark.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelFindingsRemark.Font = new System.Drawing.Font("Tahoma", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFindingsRemark.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.labelFindingsRemark.Location = new System.Drawing.Point(0, 0);
            this.labelFindingsRemark.Name = "labelFindingsRemark";
            this.labelFindingsRemark.Size = new System.Drawing.Size(1395, 230);
            this.labelFindingsRemark.TabIndex = 5;
            this.labelFindingsRemark.Text = resources.GetString("labelFindingsRemark.Text");
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.labelFindingsText);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(810, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(194, 230);
            this.panel2.TabIndex = 2;
            // 
            // labelFindingsText
            // 
            this.labelFindingsText.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelFindingsText.Font = new System.Drawing.Font("Tahoma", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFindingsText.Location = new System.Drawing.Point(0, 0);
            this.labelFindingsText.Name = "labelFindingsText";
            this.labelFindingsText.Size = new System.Drawing.Size(194, 104);
            this.labelFindingsText.TabIndex = 2;
            this.labelFindingsText.Text = "QC Findings:";
            this.labelFindingsText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.labelActivity);
            this.panel1.Controls.Add(this.labelSymptoms);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(228, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(582, 230);
            this.panel1.TabIndex = 1;
            // 
            // labelActivity
            // 
            this.labelActivity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelActivity.Font = new System.Drawing.Font("Tahoma", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelActivity.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.labelActivity.Location = new System.Drawing.Point(0, 104);
            this.labelActivity.Name = "labelActivity";
            this.labelActivity.Size = new System.Drawing.Size(582, 126);
            this.labelActivity.TabIndex = 3;
            this.labelActivity.Text = "Walking up the Stairs - these are coming from the Activity  in the STAT screen";
            // 
            // labelSymptoms
            // 
            this.labelSymptoms.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelSymptoms.Font = new System.Drawing.Font("Tahoma", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSymptoms.Location = new System.Drawing.Point(0, 0);
            this.labelSymptoms.Name = "labelSymptoms";
            this.labelSymptoms.Size = new System.Drawing.Size(582, 104);
            this.labelSymptoms.TabIndex = 1;
            this.labelSymptoms.Text = "Palpitations - these are coming from the Symptoms in the STAT screen";
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.labelActivityHeader);
            this.panel11.Controls.Add(this.panel12);
            this.panel11.Controls.Add(this.labelSymptomsHeader);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel11.Location = new System.Drawing.Point(0, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(228, 230);
            this.panel11.TabIndex = 6;
            // 
            // labelActivityHeader
            // 
            this.labelActivityHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelActivityHeader.Font = new System.Drawing.Font("Tahoma", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelActivityHeader.Location = new System.Drawing.Point(0, 104);
            this.labelActivityHeader.Name = "labelActivityHeader";
            this.labelActivityHeader.Size = new System.Drawing.Size(228, 52);
            this.labelActivityHeader.TabIndex = 3;
            this.labelActivityHeader.Text = "Activity:";
            this.labelActivityHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel12
            // 
            this.panel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel12.Location = new System.Drawing.Point(0, 52);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(228, 52);
            this.panel12.TabIndex = 2;
            // 
            // labelSymptomsHeader
            // 
            this.labelSymptomsHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelSymptomsHeader.Font = new System.Drawing.Font("Tahoma", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSymptomsHeader.Location = new System.Drawing.Point(0, 0);
            this.labelSymptomsHeader.Name = "labelSymptomsHeader";
            this.labelSymptomsHeader.Size = new System.Drawing.Size(228, 52);
            this.labelSymptomsHeader.TabIndex = 1;
            this.labelSymptomsHeader.Text = "Symptoms:";
            this.labelSymptomsHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel16
            // 
            this.panel16.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel16.Location = new System.Drawing.Point(3407, 0);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(17, 230);
            this.panel16.TabIndex = 7;
            // 
            // panel149
            // 
            this.panel149.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel149.Location = new System.Drawing.Point(0, 2281);
            this.panel149.Name = "panel149";
            this.panel149.Size = new System.Drawing.Size(3424, 4);
            this.panel149.TabIndex = 26;
            // 
            // panelECGSweepAmpIndicator
            // 
            this.panelECGSweepAmpIndicator.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelECGSweepAmpIndicator.Controls.Add(this.labelBaseSweepSpeed);
            this.panelECGSweepAmpIndicator.Controls.Add(this.labelBaseAmplitudeSet);
            this.panelECGSweepAmpIndicator.Controls.Add(this.labelEventMiddle);
            this.panelECGSweepAmpIndicator.Controls.Add(this.labelEventStart);
            this.panelECGSweepAmpIndicator.Controls.Add(this.labelEventEnd);
            this.panelECGSweepAmpIndicator.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelECGSweepAmpIndicator.Location = new System.Drawing.Point(0, 1979);
            this.panelECGSweepAmpIndicator.Name = "panelECGSweepAmpIndicator";
            this.panelECGSweepAmpIndicator.Size = new System.Drawing.Size(3424, 51);
            this.panelECGSweepAmpIndicator.TabIndex = 11;
            // 
            // labelBaseSweepSpeed
            // 
            this.labelBaseSweepSpeed.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelBaseSweepSpeed.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBaseSweepSpeed.Location = new System.Drawing.Point(2259, 0);
            this.labelBaseSweepSpeed.Name = "labelBaseSweepSpeed";
            this.labelBaseSweepSpeed.Size = new System.Drawing.Size(427, 49);
            this.labelBaseSweepSpeed.TabIndex = 16;
            this.labelBaseSweepSpeed.Text = "25 mm/sec";
            this.labelBaseSweepSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelBaseAmplitudeSet
            // 
            this.labelBaseAmplitudeSet.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelBaseAmplitudeSet.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBaseAmplitudeSet.Location = new System.Drawing.Point(2686, 0);
            this.labelBaseAmplitudeSet.Name = "labelBaseAmplitudeSet";
            this.labelBaseAmplitudeSet.Size = new System.Drawing.Size(496, 49);
            this.labelBaseAmplitudeSet.TabIndex = 17;
            this.labelBaseAmplitudeSet.Text = "1 mm/mV";
            this.labelBaseAmplitudeSet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelEventMiddle
            // 
            this.labelEventMiddle.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelEventMiddle.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEventMiddle.Location = new System.Drawing.Point(929, 0);
            this.labelEventMiddle.Name = "labelEventMiddle";
            this.labelEventMiddle.Size = new System.Drawing.Size(879, 49);
            this.labelEventMiddle.TabIndex = 6;
            this.labelEventMiddle.Text = "10:15:15 AM";
            this.labelEventMiddle.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelEventStart
            // 
            this.labelEventStart.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelEventStart.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEventStart.Location = new System.Drawing.Point(0, 0);
            this.labelEventStart.Name = "labelEventStart";
            this.labelEventStart.Size = new System.Drawing.Size(929, 49);
            this.labelEventStart.TabIndex = 4;
            this.labelEventStart.Text = "10:15:10 AM";
            this.labelEventStart.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelEventEnd
            // 
            this.labelEventEnd.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelEventEnd.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEventEnd.Location = new System.Drawing.Point(3182, 0);
            this.labelEventEnd.Name = "labelEventEnd";
            this.labelEventEnd.Size = new System.Drawing.Size(240, 49);
            this.labelEventEnd.TabIndex = 5;
            this.labelEventEnd.Text = "10:15:20 AM";
            this.labelEventEnd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelEventEnd.Click += new System.EventHandler(this.labelEventEnd_Click);
            // 
            // panelFullStrip
            // 
            this.panelFullStrip.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelFullStrip.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelFullStrip.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelFullStrip.Location = new System.Drawing.Point(0, 1812);
            this.panelFullStrip.Name = "panelFullStrip";
            this.panelFullStrip.Size = new System.Drawing.Size(3424, 167);
            this.panelFullStrip.TabIndex = 10;
            // 
            // panelFooterPage1
            // 
            this.panelFooterPage1.Controls.Add(this.label14);
            this.panelFooterPage1.Controls.Add(this.labelStudyNumberResPage1);
            this.panelFooterPage1.Controls.Add(this.labelPrintDate1Bottom);
            this.panelFooterPage1.Controls.Add(this.label8);
            this.panelFooterPage1.Controls.Add(this.panel510);
            this.panelFooterPage1.Controls.Add(this.panel509);
            this.panelFooterPage1.Controls.Add(this.panel508);
            this.panelFooterPage1.Controls.Add(this.panel507);
            this.panelFooterPage1.Controls.Add(this.label7);
            this.panelFooterPage1.Controls.Add(this.label3);
            this.panelFooterPage1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelFooterPage1.Location = new System.Drawing.Point(0, 2285);
            this.panelFooterPage1.Name = "panelFooterPage1";
            this.panelFooterPage1.Size = new System.Drawing.Size(3424, 51);
            this.panelFooterPage1.TabIndex = 34;
            // 
            // label14
            // 
            this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label14.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(1077, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(1750, 51);
            this.label14.TabIndex = 8;
            this.label14.Text = "Copyright 2017 © Techmedic International B.V.";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelStudyNumberResPage1
            // 
            this.labelStudyNumberResPage1.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelStudyNumberResPage1.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStudyNumberResPage1.Location = new System.Drawing.Point(2827, 0);
            this.labelStudyNumberResPage1.Name = "labelStudyNumberResPage1";
            this.labelStudyNumberResPage1.Size = new System.Drawing.Size(162, 51);
            this.labelStudyNumberResPage1.TabIndex = 44;
            this.labelStudyNumberResPage1.Text = "1212212";
            this.labelStudyNumberResPage1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelPrintDate1Bottom
            // 
            this.labelPrintDate1Bottom.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPrintDate1Bottom.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPrintDate1Bottom.Location = new System.Drawing.Point(260, 0);
            this.labelPrintDate1Bottom.Name = "labelPrintDate1Bottom";
            this.labelPrintDate1Bottom.Size = new System.Drawing.Size(817, 51);
            this.labelPrintDate1Bottom.TabIndex = 7;
            this.labelPrintDate1Bottom.Text = "12/27/2016 24:99:99 AM+1200 v12345";
            this.labelPrintDate1Bottom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.Dock = System.Windows.Forms.DockStyle.Left;
            this.label8.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(28, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(232, 51);
            this.label8.TabIndex = 6;
            this.label8.Text = "Print date:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel510
            // 
            this.panel510.Controls.Add(this.labelPage);
            this.panel510.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel510.Location = new System.Drawing.Point(2989, 0);
            this.panel510.Name = "panel510";
            this.panel510.Size = new System.Drawing.Size(123, 51);
            this.panel510.TabIndex = 5;
            // 
            // labelPage
            // 
            this.labelPage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPage.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPage.Location = new System.Drawing.Point(0, 0);
            this.labelPage.Name = "labelPage";
            this.labelPage.Size = new System.Drawing.Size(123, 51);
            this.labelPage.TabIndex = 0;
            this.labelPage.Text = "Page";
            this.labelPage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel509
            // 
            this.panel509.Controls.Add(this.labelPage1of);
            this.panel509.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel509.Location = new System.Drawing.Point(3112, 0);
            this.panel509.Name = "panel509";
            this.panel509.Size = new System.Drawing.Size(74, 51);
            this.panel509.TabIndex = 4;
            // 
            // labelPage1of
            // 
            this.labelPage1of.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPage1of.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPage1of.Location = new System.Drawing.Point(0, 0);
            this.labelPage1of.Name = "labelPage1of";
            this.labelPage1of.Size = new System.Drawing.Size(74, 51);
            this.labelPage1of.TabIndex = 0;
            this.labelPage1of.Text = "1";
            this.labelPage1of.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelPage1of.Click += new System.EventHandler(this.label53_Click);
            // 
            // panel508
            // 
            this.panel508.Controls.Add(this.labelPageOf);
            this.panel508.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel508.Location = new System.Drawing.Point(3186, 0);
            this.panel508.Name = "panel508";
            this.panel508.Size = new System.Drawing.Size(82, 51);
            this.panel508.TabIndex = 3;
            // 
            // labelPageOf
            // 
            this.labelPageOf.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelPageOf.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPageOf.Location = new System.Drawing.Point(14, 0);
            this.labelPageOf.Name = "labelPageOf";
            this.labelPageOf.Size = new System.Drawing.Size(68, 51);
            this.labelPageOf.TabIndex = 0;
            this.labelPageOf.Text = "of";
            this.labelPageOf.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel507
            // 
            this.panel507.Controls.Add(this.labelPage1ofx);
            this.panel507.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel507.Location = new System.Drawing.Point(3268, 0);
            this.panel507.Name = "panel507";
            this.panel507.Size = new System.Drawing.Size(130, 51);
            this.panel507.TabIndex = 2;
            // 
            // labelPage1ofx
            // 
            this.labelPage1ofx.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPage1ofx.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPage1ofx.Location = new System.Drawing.Point(0, 0);
            this.labelPage1ofx.Name = "labelPage1ofx";
            this.labelPage1ofx.Size = new System.Drawing.Size(130, 51);
            this.labelPage1ofx.TabIndex = 1;
            this.labelPage1ofx.Text = "20p1";
            this.labelPage1ofx.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label7.Dock = System.Windows.Forms.DockStyle.Right;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label7.Location = new System.Drawing.Point(3398, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(26, 51);
            this.label7.TabIndex = 1;
            this.label7.Text = "R1";
            this.label7.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label7.Visible = false;
            // 
            // label3
            // 
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Dock = System.Windows.Forms.DockStyle.Left;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 51);
            this.label3.TabIndex = 0;
            this.label3.Text = "L1";
            this.label3.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label3.Visible = false;
            // 
            // panelLineUnderSampl1
            // 
            this.panelLineUnderSampl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelLineUnderSampl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLineUnderSampl1.Location = new System.Drawing.Point(0, 1810);
            this.panelLineUnderSampl1.Name = "panelLineUnderSampl1";
            this.panelLineUnderSampl1.Size = new System.Drawing.Size(3424, 2);
            this.panelLineUnderSampl1.TabIndex = 20;
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.labelTopStripFeed);
            this.panel6.Controls.Add(this.labelTopStripAmpl);
            this.panel6.Controls.Add(this.labelTopStripTime);
            this.panel6.Controls.Add(this.label16);
            this.panel6.Controls.Add(this.label44);
            this.panel6.Controls.Add(this.labelSampleRate);
            this.panel6.Controls.Add(this.label42);
            this.panel6.Controls.Add(this.labelHPF);
            this.panel6.Controls.Add(this.label40);
            this.panel6.Controls.Add(this.labelNotch);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 1759);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(3424, 51);
            this.panel6.TabIndex = 15;
            // 
            // labelTopStripFeed
            // 
            this.labelTopStripFeed.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelTopStripFeed.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTopStripFeed.Location = new System.Drawing.Point(1383, 0);
            this.labelTopStripFeed.Name = "labelTopStripFeed";
            this.labelTopStripFeed.Size = new System.Drawing.Size(500, 49);
            this.labelTopStripFeed.TabIndex = 25;
            this.labelTopStripFeed.Text = "25 mm/sec";
            this.labelTopStripFeed.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelTopStripAmpl
            // 
            this.labelTopStripAmpl.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelTopStripAmpl.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTopStripAmpl.Location = new System.Drawing.Point(948, 0);
            this.labelTopStripAmpl.Name = "labelTopStripAmpl";
            this.labelTopStripAmpl.Size = new System.Drawing.Size(435, 49);
            this.labelTopStripAmpl.TabIndex = 26;
            this.labelTopStripAmpl.Text = "1 mm/mV";
            this.labelTopStripAmpl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelTopStripTime
            // 
            this.labelTopStripTime.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelTopStripTime.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTopStripTime.Location = new System.Drawing.Point(301, 0);
            this.labelTopStripTime.Name = "labelTopStripTime";
            this.labelTopStripTime.Size = new System.Drawing.Size(647, 49);
            this.labelTopStripTime.TabIndex = 24;
            this.labelTopStripTime.Text = "10:15:10 AM";
            this.labelTopStripTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label16
            // 
            this.label16.Dock = System.Windows.Forms.DockStyle.Left;
            this.label16.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(0, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(301, 49);
            this.label16.TabIndex = 23;
            this.label16.Text = "Channel Strips:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label44
            // 
            this.label44.Dock = System.Windows.Forms.DockStyle.Right;
            this.label44.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(2432, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(246, 49);
            this.label44.TabIndex = 22;
            this.label44.Text = "Sample rate:";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelSampleRate
            // 
            this.labelSampleRate.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSampleRate.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSampleRate.Location = new System.Drawing.Point(2678, 0);
            this.labelSampleRate.Name = "labelSampleRate";
            this.labelSampleRate.Size = new System.Drawing.Size(177, 49);
            this.labelSampleRate.TabIndex = 21;
            this.labelSampleRate.Text = "?1000 Sps";
            this.labelSampleRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label42
            // 
            this.label42.Dock = System.Windows.Forms.DockStyle.Right;
            this.label42.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(2855, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(123, 49);
            this.label42.TabIndex = 20;
            this.label42.Text = "HPF:";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label42.Visible = false;
            // 
            // labelHPF
            // 
            this.labelHPF.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelHPF.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHPF.Location = new System.Drawing.Point(2978, 0);
            this.labelHPF.Name = "labelHPF";
            this.labelHPF.Size = new System.Drawing.Size(161, 49);
            this.labelHPF.TabIndex = 19;
            this.labelHPF.Text = "0.05 Hz";
            this.labelHPF.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelHPF.Visible = false;
            // 
            // label40
            // 
            this.label40.Dock = System.Windows.Forms.DockStyle.Right;
            this.label40.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(3139, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(158, 49);
            this.label40.TabIndex = 18;
            this.label40.Text = "Notch:";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label40.Visible = false;
            // 
            // labelNotch
            // 
            this.labelNotch.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelNotch.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNotch.Location = new System.Drawing.Point(3297, 0);
            this.labelNotch.Name = "labelNotch";
            this.labelNotch.Size = new System.Drawing.Size(125, 49);
            this.labelNotch.TabIndex = 17;
            this.labelNotch.Text = "50 Hz";
            this.labelNotch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelNotch.Visible = false;
            // 
            // panelTimeBarBaselineECG
            // 
            this.panelTimeBarBaselineECG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelTimeBarBaselineECG.Controls.Add(this.splitContainer1);
            this.panelTimeBarBaselineECG.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTimeBarBaselineECG.Location = new System.Drawing.Point(0, 339);
            this.panelTimeBarBaselineECG.Name = "panelTimeBarBaselineECG";
            this.panelTimeBarBaselineECG.Size = new System.Drawing.Size(3424, 1420);
            this.panelTimeBarBaselineECG.TabIndex = 9;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panelChannel6);
            this.splitContainer1.Panel1.Controls.Add(this.panelChannel5);
            this.splitContainer1.Panel1.Controls.Add(this.panelChannel4);
            this.splitContainer1.Panel1.Controls.Add(this.panelChannel3);
            this.splitContainer1.Panel1.Controls.Add(this.panelChannel2);
            this.splitContainer1.Panel1.Controls.Add(this.panelChannel1);
            this.splitContainer1.Panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.splitContainer1_Panel1_Paint);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panelChannel12);
            this.splitContainer1.Panel2.Controls.Add(this.panelChannel11);
            this.splitContainer1.Panel2.Controls.Add(this.panelChannel10);
            this.splitContainer1.Panel2.Controls.Add(this.panelChannel9);
            this.splitContainer1.Panel2.Controls.Add(this.panelChannel8);
            this.splitContainer1.Panel2.Controls.Add(this.panelChannel7);
            this.splitContainer1.Size = new System.Drawing.Size(3422, 1418);
            this.splitContainer1.SplitterDistance = 1707;
            this.splitContainer1.TabIndex = 0;
            // 
            // panelChannel6
            // 
            this.panelChannel6.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelChannel6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelChannel6.BackgroundImage")));
            this.panelChannel6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelChannel6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelChannel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelChannel6.Location = new System.Drawing.Point(0, 1185);
            this.panelChannel6.Name = "panelChannel6";
            this.panelChannel6.Size = new System.Drawing.Size(1707, 237);
            this.panelChannel6.TabIndex = 29;
            // 
            // panelChannel5
            // 
            this.panelChannel5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelChannel5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelChannel5.BackgroundImage")));
            this.panelChannel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelChannel5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelChannel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelChannel5.Location = new System.Drawing.Point(0, 948);
            this.panelChannel5.Name = "panelChannel5";
            this.panelChannel5.Size = new System.Drawing.Size(1707, 237);
            this.panelChannel5.TabIndex = 28;
            // 
            // panelChannel4
            // 
            this.panelChannel4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelChannel4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelChannel4.BackgroundImage")));
            this.panelChannel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelChannel4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelChannel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelChannel4.Location = new System.Drawing.Point(0, 711);
            this.panelChannel4.Name = "panelChannel4";
            this.panelChannel4.Size = new System.Drawing.Size(1707, 237);
            this.panelChannel4.TabIndex = 27;
            // 
            // panelChannel3
            // 
            this.panelChannel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelChannel3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelChannel3.BackgroundImage")));
            this.panelChannel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelChannel3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelChannel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelChannel3.Location = new System.Drawing.Point(0, 474);
            this.panelChannel3.Name = "panelChannel3";
            this.panelChannel3.Size = new System.Drawing.Size(1707, 237);
            this.panelChannel3.TabIndex = 26;
            // 
            // panelChannel2
            // 
            this.panelChannel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelChannel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelChannel2.BackgroundImage")));
            this.panelChannel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelChannel2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelChannel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelChannel2.Location = new System.Drawing.Point(0, 237);
            this.panelChannel2.Name = "panelChannel2";
            this.panelChannel2.Size = new System.Drawing.Size(1707, 237);
            this.panelChannel2.TabIndex = 25;
            // 
            // panelChannel1
            // 
            this.panelChannel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelChannel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelChannel1.BackgroundImage")));
            this.panelChannel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelChannel1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelChannel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelChannel1.Location = new System.Drawing.Point(0, 0);
            this.panelChannel1.Name = "panelChannel1";
            this.panelChannel1.Size = new System.Drawing.Size(1707, 237);
            this.panelChannel1.TabIndex = 24;
            // 
            // panelChannel12
            // 
            this.panelChannel12.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelChannel12.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelChannel12.BackgroundImage")));
            this.panelChannel12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelChannel12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelChannel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelChannel12.Location = new System.Drawing.Point(0, 1185);
            this.panelChannel12.Name = "panelChannel12";
            this.panelChannel12.Size = new System.Drawing.Size(1711, 237);
            this.panelChannel12.TabIndex = 31;
            // 
            // panelChannel11
            // 
            this.panelChannel11.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelChannel11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelChannel11.BackgroundImage")));
            this.panelChannel11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelChannel11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelChannel11.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelChannel11.Location = new System.Drawing.Point(0, 948);
            this.panelChannel11.Name = "panelChannel11";
            this.panelChannel11.Size = new System.Drawing.Size(1711, 237);
            this.panelChannel11.TabIndex = 30;
            // 
            // panelChannel10
            // 
            this.panelChannel10.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelChannel10.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelChannel10.BackgroundImage")));
            this.panelChannel10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelChannel10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelChannel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelChannel10.Location = new System.Drawing.Point(0, 711);
            this.panelChannel10.Name = "panelChannel10";
            this.panelChannel10.Size = new System.Drawing.Size(1711, 237);
            this.panelChannel10.TabIndex = 29;
            // 
            // panelChannel9
            // 
            this.panelChannel9.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelChannel9.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelChannel9.BackgroundImage")));
            this.panelChannel9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelChannel9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelChannel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelChannel9.Location = new System.Drawing.Point(0, 474);
            this.panelChannel9.Name = "panelChannel9";
            this.panelChannel9.Size = new System.Drawing.Size(1711, 237);
            this.panelChannel9.TabIndex = 28;
            // 
            // panelChannel8
            // 
            this.panelChannel8.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelChannel8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelChannel8.BackgroundImage")));
            this.panelChannel8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelChannel8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelChannel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelChannel8.Location = new System.Drawing.Point(0, 237);
            this.panelChannel8.Name = "panelChannel8";
            this.panelChannel8.Size = new System.Drawing.Size(1711, 237);
            this.panelChannel8.TabIndex = 27;
            // 
            // panelChannel7
            // 
            this.panelChannel7.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelChannel7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelChannel7.BackgroundImage")));
            this.panelChannel7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelChannel7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelChannel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelChannel7.Location = new System.Drawing.Point(0, 0);
            this.panelChannel7.Name = "panelChannel7";
            this.panelChannel7.Size = new System.Drawing.Size(1711, 237);
            this.panelChannel7.TabIndex = 26;
            // 
            // panelTimeHeadingsPerStrip
            // 
            this.panelTimeHeadingsPerStrip.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTimeHeadingsPerStrip.Location = new System.Drawing.Point(0, 329);
            this.panelTimeHeadingsPerStrip.Name = "panelTimeHeadingsPerStrip";
            this.panelTimeHeadingsPerStrip.Size = new System.Drawing.Size(3424, 10);
            this.panelTimeHeadingsPerStrip.TabIndex = 8;
            this.panelTimeHeadingsPerStrip.Paint += new System.Windows.Forms.PaintEventHandler(this.panelTimeHeadingsPerStrip_Paint);
            // 
            // panelHorSepPatDet
            // 
            this.panelHorSepPatDet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelHorSepPatDet.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHorSepPatDet.Location = new System.Drawing.Point(0, 328);
            this.panelHorSepPatDet.Name = "panelHorSepPatDet";
            this.panelHorSepPatDet.Size = new System.Drawing.Size(3424, 1);
            this.panelHorSepPatDet.TabIndex = 3;
            // 
            // panelPrintHeader
            // 
            this.panelPrintHeader.Controls.Add(this.panel179);
            this.panelPrintHeader.Controls.Add(this.panel10);
            this.panelPrintHeader.Controls.Add(this.panel9);
            this.panelPrintHeader.Controls.Add(this.panel4);
            this.panelPrintHeader.Controls.Add(this.panel154);
            this.panelPrintHeader.Controls.Add(this.panel94);
            this.panelPrintHeader.Controls.Add(this.panel93);
            this.panelPrintHeader.Controls.Add(this.panel92);
            this.panelPrintHeader.Controls.Add(this.panel61);
            this.panelPrintHeader.Controls.Add(this.panel91);
            this.panelPrintHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelPrintHeader.Location = new System.Drawing.Point(0, 0);
            this.panelPrintHeader.Name = "panelPrintHeader";
            this.panelPrintHeader.Size = new System.Drawing.Size(3424, 328);
            this.panelPrintHeader.TabIndex = 4;
            // 
            // panel179
            // 
            this.panel179.Controls.Add(this.labelRythm);
            this.panel179.Controls.Add(this.label17);
            this.panel179.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel179.Location = new System.Drawing.Point(2326, 0);
            this.panel179.Name = "panel179";
            this.panel179.Size = new System.Drawing.Size(1098, 328);
            this.panel179.TabIndex = 16;
            // 
            // labelRythm
            // 
            this.labelRythm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelRythm.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRythm.Location = new System.Drawing.Point(0, 46);
            this.labelRythm.Name = "labelRythm";
            this.labelRythm.Size = new System.Drawing.Size(1098, 282);
            this.labelRythm.TabIndex = 5;
            this.labelRythm.Text = "DLL data - when no data comes from the DLL this is filled with the  info from STA" +
    "T";
            // 
            // label17
            // 
            this.label17.Dock = System.Windows.Forms.DockStyle.Top;
            this.label17.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(0, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(1098, 46);
            this.label17.TabIndex = 4;
            this.label17.Text = "Rhythm";
            // 
            // panel10
            // 
            this.panel10.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel10.Location = new System.Drawing.Point(2312, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(14, 328);
            this.panel10.TabIndex = 19;
            // 
            // panel9
            // 
            this.panel9.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel9.Location = new System.Drawing.Point(2298, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(14, 328);
            this.panel9.TabIndex = 18;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.panel15);
            this.panel4.Controls.Add(this.panel14);
            this.panel4.Controls.Add(this.panel13);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(1973, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(325, 328);
            this.panel4.TabIndex = 17;
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.label36);
            this.panel15.Controls.Add(this.label39);
            this.panel15.Controls.Add(this.label41);
            this.panel15.Controls.Add(this.label43);
            this.panel15.Controls.Add(this.label45);
            this.panel15.Controls.Add(this.label46);
            this.panel15.Controls.Add(this.label47);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel15.Location = new System.Drawing.Point(247, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(65, 328);
            this.panel15.TabIndex = 16;
            // 
            // label36
            // 
            this.label36.Dock = System.Windows.Forms.DockStyle.Top;
            this.label36.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(0, 279);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(65, 46);
            this.label36.TabIndex = 10;
            this.label36.Text = "ms";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label39
            // 
            this.label39.Dock = System.Windows.Forms.DockStyle.Top;
            this.label39.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(0, 233);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(65, 46);
            this.label39.TabIndex = 9;
            this.label39.Text = "ms";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label41
            // 
            this.label41.Dock = System.Windows.Forms.DockStyle.Top;
            this.label41.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(0, 187);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(65, 46);
            this.label41.TabIndex = 7;
            this.label41.Text = "ms";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label43
            // 
            this.label43.Dock = System.Windows.Forms.DockStyle.Top;
            this.label43.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(0, 141);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(65, 46);
            this.label43.TabIndex = 6;
            this.label43.Text = "ms";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label45
            // 
            this.label45.Dock = System.Windows.Forms.DockStyle.Top;
            this.label45.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(0, 95);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(65, 46);
            this.label45.TabIndex = 5;
            this.label45.Text = "ms";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label46
            // 
            this.label46.Dock = System.Windows.Forms.DockStyle.Top;
            this.label46.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(0, 49);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(65, 46);
            this.label46.TabIndex = 3;
            this.label46.Text = "ms";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label47
            // 
            this.label47.Dock = System.Windows.Forms.DockStyle.Top;
            this.label47.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(0, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(65, 49);
            this.label47.TabIndex = 8;
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.labelResultQTcFavg);
            this.panel14.Controls.Add(this.labelResultQTcBavg);
            this.panel14.Controls.Add(this.labelResultQTavg);
            this.panel14.Controls.Add(this.labelResultQRSavg);
            this.panel14.Controls.Add(this.labelResultPRavg);
            this.panel14.Controls.Add(this.labelResultRRavg);
            this.panel14.Controls.Add(this.label34);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel14.Location = new System.Drawing.Point(127, 0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(120, 328);
            this.panel14.TabIndex = 15;
            // 
            // labelResultQTcFavg
            // 
            this.labelResultQTcFavg.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelResultQTcFavg.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelResultQTcFavg.Location = new System.Drawing.Point(0, 279);
            this.labelResultQTcFavg.Name = "labelResultQTcFavg";
            this.labelResultQTcFavg.Size = new System.Drawing.Size(120, 46);
            this.labelResultQTcFavg.TabIndex = 10;
            this.labelResultQTcFavg.Text = "^123";
            this.labelResultQTcFavg.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelResultQTcBavg
            // 
            this.labelResultQTcBavg.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelResultQTcBavg.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelResultQTcBavg.Location = new System.Drawing.Point(0, 233);
            this.labelResultQTcBavg.Name = "labelResultQTcBavg";
            this.labelResultQTcBavg.Size = new System.Drawing.Size(120, 46);
            this.labelResultQTcBavg.TabIndex = 9;
            this.labelResultQTcBavg.Text = "^123";
            this.labelResultQTcBavg.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelResultQTavg
            // 
            this.labelResultQTavg.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelResultQTavg.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelResultQTavg.Location = new System.Drawing.Point(0, 187);
            this.labelResultQTavg.Name = "labelResultQTavg";
            this.labelResultQTavg.Size = new System.Drawing.Size(120, 46);
            this.labelResultQTavg.TabIndex = 7;
            this.labelResultQTavg.Text = "^123";
            this.labelResultQTavg.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelResultQRSavg
            // 
            this.labelResultQRSavg.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelResultQRSavg.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelResultQRSavg.Location = new System.Drawing.Point(0, 141);
            this.labelResultQRSavg.Name = "labelResultQRSavg";
            this.labelResultQRSavg.Size = new System.Drawing.Size(120, 46);
            this.labelResultQRSavg.TabIndex = 6;
            this.labelResultQRSavg.Text = "^123";
            this.labelResultQRSavg.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelResultPRavg
            // 
            this.labelResultPRavg.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelResultPRavg.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelResultPRavg.Location = new System.Drawing.Point(0, 95);
            this.labelResultPRavg.Name = "labelResultPRavg";
            this.labelResultPRavg.Size = new System.Drawing.Size(120, 46);
            this.labelResultPRavg.TabIndex = 5;
            this.labelResultPRavg.Text = "^123";
            this.labelResultPRavg.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelResultRRavg
            // 
            this.labelResultRRavg.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelResultRRavg.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelResultRRavg.Location = new System.Drawing.Point(0, 49);
            this.labelResultRRavg.Name = "labelResultRRavg";
            this.labelResultRRavg.Size = new System.Drawing.Size(120, 46);
            this.labelResultRRavg.TabIndex = 3;
            this.labelResultRRavg.Text = "^1234";
            this.labelResultRRavg.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label34
            // 
            this.label34.Dock = System.Windows.Forms.DockStyle.Top;
            this.label34.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(0, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(120, 49);
            this.label34.TabIndex = 8;
            this.label34.Text = "Avg";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.labelResultQTcFmax);
            this.panel13.Controls.Add(this.labelResultQTcBmax);
            this.panel13.Controls.Add(this.labelResultQTmax);
            this.panel13.Controls.Add(this.labelResultQRSmax);
            this.panel13.Controls.Add(this.labelResultPRmax);
            this.panel13.Controls.Add(this.labelResultRRmax);
            this.panel13.Controls.Add(this.label25);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel13.Location = new System.Drawing.Point(0, 0);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(127, 328);
            this.panel13.TabIndex = 14;
            // 
            // labelResultQTcFmax
            // 
            this.labelResultQTcFmax.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelResultQTcFmax.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelResultQTcFmax.Location = new System.Drawing.Point(0, 279);
            this.labelResultQTcFmax.Name = "labelResultQTcFmax";
            this.labelResultQTcFmax.Size = new System.Drawing.Size(127, 46);
            this.labelResultQTcFmax.TabIndex = 10;
            this.labelResultQTcFmax.Text = "^123";
            this.labelResultQTcFmax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelResultQTcBmax
            // 
            this.labelResultQTcBmax.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelResultQTcBmax.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelResultQTcBmax.Location = new System.Drawing.Point(0, 233);
            this.labelResultQTcBmax.Name = "labelResultQTcBmax";
            this.labelResultQTcBmax.Size = new System.Drawing.Size(127, 46);
            this.labelResultQTcBmax.TabIndex = 9;
            this.labelResultQTcBmax.Text = "^123";
            this.labelResultQTcBmax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelResultQTmax
            // 
            this.labelResultQTmax.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelResultQTmax.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelResultQTmax.Location = new System.Drawing.Point(0, 187);
            this.labelResultQTmax.Name = "labelResultQTmax";
            this.labelResultQTmax.Size = new System.Drawing.Size(127, 46);
            this.labelResultQTmax.TabIndex = 7;
            this.labelResultQTmax.Text = "^123";
            this.labelResultQTmax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelResultQRSmax
            // 
            this.labelResultQRSmax.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelResultQRSmax.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelResultQRSmax.Location = new System.Drawing.Point(0, 141);
            this.labelResultQRSmax.Name = "labelResultQRSmax";
            this.labelResultQRSmax.Size = new System.Drawing.Size(127, 46);
            this.labelResultQRSmax.TabIndex = 6;
            this.labelResultQRSmax.Text = "^123";
            this.labelResultQRSmax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelResultPRmax
            // 
            this.labelResultPRmax.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelResultPRmax.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelResultPRmax.Location = new System.Drawing.Point(0, 95);
            this.labelResultPRmax.Name = "labelResultPRmax";
            this.labelResultPRmax.Size = new System.Drawing.Size(127, 46);
            this.labelResultPRmax.TabIndex = 5;
            this.labelResultPRmax.Text = "^123";
            this.labelResultPRmax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelResultRRmax
            // 
            this.labelResultRRmax.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelResultRRmax.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelResultRRmax.Location = new System.Drawing.Point(0, 49);
            this.labelResultRRmax.Name = "labelResultRRmax";
            this.labelResultRRmax.Size = new System.Drawing.Size(127, 46);
            this.labelResultRRmax.TabIndex = 3;
            this.labelResultRRmax.Text = "^1234";
            this.labelResultRRmax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label25
            // 
            this.label25.Dock = System.Windows.Forms.DockStyle.Top;
            this.label25.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(0, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(127, 49);
            this.label25.TabIndex = 8;
            this.label25.Text = "Max";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel154
            // 
            this.panel154.Controls.Add(this.labelResultQTcFmin);
            this.panel154.Controls.Add(this.labelResultQTcBmin);
            this.panel154.Controls.Add(this.labelResultQTmin);
            this.panel154.Controls.Add(this.labelResultQRSmin);
            this.panel154.Controls.Add(this.labelResultPRmin);
            this.panel154.Controls.Add(this.labelResultRRmin);
            this.panel154.Controls.Add(this.label33);
            this.panel154.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel154.Location = new System.Drawing.Point(1850, 0);
            this.panel154.Name = "panel154";
            this.panel154.Size = new System.Drawing.Size(123, 328);
            this.panel154.TabIndex = 13;
            // 
            // labelResultQTcFmin
            // 
            this.labelResultQTcFmin.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelResultQTcFmin.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelResultQTcFmin.Location = new System.Drawing.Point(0, 279);
            this.labelResultQTcFmin.Name = "labelResultQTcFmin";
            this.labelResultQTcFmin.Size = new System.Drawing.Size(123, 46);
            this.labelResultQTcFmin.TabIndex = 10;
            this.labelResultQTcFmin.Text = "^123";
            this.labelResultQTcFmin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelResultQTcBmin
            // 
            this.labelResultQTcBmin.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelResultQTcBmin.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelResultQTcBmin.Location = new System.Drawing.Point(0, 233);
            this.labelResultQTcBmin.Name = "labelResultQTcBmin";
            this.labelResultQTcBmin.Size = new System.Drawing.Size(123, 46);
            this.labelResultQTcBmin.TabIndex = 9;
            this.labelResultQTcBmin.Text = "^123";
            this.labelResultQTcBmin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelResultQTmin
            // 
            this.labelResultQTmin.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelResultQTmin.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelResultQTmin.Location = new System.Drawing.Point(0, 187);
            this.labelResultQTmin.Name = "labelResultQTmin";
            this.labelResultQTmin.Size = new System.Drawing.Size(123, 46);
            this.labelResultQTmin.TabIndex = 7;
            this.labelResultQTmin.Text = "^123";
            this.labelResultQTmin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelResultQRSmin
            // 
            this.labelResultQRSmin.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelResultQRSmin.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelResultQRSmin.Location = new System.Drawing.Point(0, 141);
            this.labelResultQRSmin.Name = "labelResultQRSmin";
            this.labelResultQRSmin.Size = new System.Drawing.Size(123, 46);
            this.labelResultQRSmin.TabIndex = 6;
            this.labelResultQRSmin.Text = "^123";
            this.labelResultQRSmin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelResultPRmin
            // 
            this.labelResultPRmin.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelResultPRmin.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelResultPRmin.Location = new System.Drawing.Point(0, 95);
            this.labelResultPRmin.Name = "labelResultPRmin";
            this.labelResultPRmin.Size = new System.Drawing.Size(123, 46);
            this.labelResultPRmin.TabIndex = 5;
            this.labelResultPRmin.Text = "^123";
            this.labelResultPRmin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelResultRRmin
            // 
            this.labelResultRRmin.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelResultRRmin.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelResultRRmin.Location = new System.Drawing.Point(0, 49);
            this.labelResultRRmin.Name = "labelResultRRmin";
            this.labelResultRRmin.Size = new System.Drawing.Size(123, 46);
            this.labelResultRRmin.TabIndex = 3;
            this.labelResultRRmin.Text = "^1234";
            this.labelResultRRmin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label33
            // 
            this.label33.Dock = System.Windows.Forms.DockStyle.Top;
            this.label33.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(0, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(123, 49);
            this.label33.TabIndex = 8;
            this.label33.Text = "Min";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel94
            // 
            this.panel94.Controls.Add(this.label19);
            this.panel94.Controls.Add(this.label11);
            this.panel94.Controls.Add(this.label15);
            this.panel94.Controls.Add(this.label13);
            this.panel94.Controls.Add(this.label12);
            this.panel94.Controls.Add(this.label10);
            this.panel94.Controls.Add(this.label32);
            this.panel94.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel94.Location = new System.Drawing.Point(1713, 0);
            this.panel94.Name = "panel94";
            this.panel94.Size = new System.Drawing.Size(137, 328);
            this.panel94.TabIndex = 12;
            // 
            // label19
            // 
            this.label19.Dock = System.Windows.Forms.DockStyle.Top;
            this.label19.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(0, 276);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(137, 46);
            this.label19.TabIndex = 9;
            this.label19.Text = "QTcF:";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label11
            // 
            this.label11.Dock = System.Windows.Forms.DockStyle.Top;
            this.label11.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(0, 230);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(137, 46);
            this.label11.TabIndex = 8;
            this.label11.Text = "QTcB:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label15
            // 
            this.label15.Dock = System.Windows.Forms.DockStyle.Top;
            this.label15.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(0, 184);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(137, 46);
            this.label15.TabIndex = 6;
            this.label15.Text = "QT:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label13
            // 
            this.label13.Dock = System.Windows.Forms.DockStyle.Top;
            this.label13.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(0, 138);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(137, 46);
            this.label13.TabIndex = 5;
            this.label13.Text = "QRS:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label12
            // 
            this.label12.Dock = System.Windows.Forms.DockStyle.Top;
            this.label12.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(0, 92);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(137, 46);
            this.label12.TabIndex = 4;
            this.label12.Text = "PR:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label10
            // 
            this.label10.Dock = System.Windows.Forms.DockStyle.Top;
            this.label10.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(0, 46);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(137, 46);
            this.label10.TabIndex = 2;
            this.label10.Text = "RR:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label32
            // 
            this.label32.Dock = System.Windows.Forms.DockStyle.Top;
            this.label32.Font = new System.Drawing.Font("Tahoma", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(0, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(137, 46);
            this.label32.TabIndex = 7;
            this.label32.Text = "Results";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel93
            // 
            this.panel93.Controls.Add(this.labelPhysician);
            this.panel93.Controls.Add(this.labelDateOfBirth);
            this.panel93.Controls.Add(this.labelPatLastName);
            this.panel93.Controls.Add(this.labelPatientIDResult);
            this.panel93.Controls.Add(this.label4);
            this.panel93.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel93.Location = new System.Drawing.Point(1157, 0);
            this.panel93.Name = "panel93";
            this.panel93.Size = new System.Drawing.Size(556, 328);
            this.panel93.TabIndex = 11;
            // 
            // labelPhysician
            // 
            this.labelPhysician.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPhysician.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPhysician.Location = new System.Drawing.Point(0, 279);
            this.labelPhysician.Name = "labelPhysician";
            this.labelPhysician.Size = new System.Drawing.Size(556, 46);
            this.labelPhysician.TabIndex = 52;
            this.labelPhysician.Text = "Dr. John Smithsonian";
            // 
            // labelDateOfBirth
            // 
            this.labelDateOfBirth.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDateOfBirth.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDateOfBirth.Location = new System.Drawing.Point(0, 233);
            this.labelDateOfBirth.Name = "labelDateOfBirth";
            this.labelDateOfBirth.Size = new System.Drawing.Size(556, 46);
            this.labelDateOfBirth.TabIndex = 46;
            this.labelDateOfBirth.Text = "06/08/1974 (31y) F";
            this.labelDateOfBirth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelPatLastName
            // 
            this.labelPatLastName.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPatLastName.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPatLastName.Location = new System.Drawing.Point(0, 92);
            this.labelPatLastName.Name = "labelPatLastName";
            this.labelPatLastName.Size = new System.Drawing.Size(556, 141);
            this.labelPatLastName.TabIndex = 44;
            this.labelPatLastName.Text = "Brest van Kempen, Rutger\r\nhgfjkdhjah\r\nkhjfska";
            // 
            // labelPatientIDResult
            // 
            this.labelPatientIDResult.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPatientIDResult.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPatientIDResult.Location = new System.Drawing.Point(0, 46);
            this.labelPatientIDResult.Name = "labelPatientIDResult";
            this.labelPatientIDResult.Size = new System.Drawing.Size(556, 46);
            this.labelPatientIDResult.TabIndex = 43;
            this.labelPatientIDResult.Text = "12341234567";
            this.labelPatientIDResult.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(556, 46);
            this.label4.TabIndex = 51;
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel92
            // 
            this.panel92.Controls.Add(this.label20);
            this.panel92.Controls.Add(this.labelDOBheader);
            this.panel92.Controls.Add(this.labelPatientID);
            this.panel92.Controls.Add(this.labelPatientNameHeader);
            this.panel92.Controls.Add(this.label9);
            this.panel92.Controls.Add(this.label50);
            this.panel92.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel92.Location = new System.Drawing.Point(977, 0);
            this.panel92.Name = "panel92";
            this.panel92.Size = new System.Drawing.Size(180, 328);
            this.panel92.TabIndex = 10;
            // 
            // label20
            // 
            this.label20.Dock = System.Windows.Forms.DockStyle.Top;
            this.label20.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(0, 279);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(180, 46);
            this.label20.TabIndex = 44;
            this.label20.Text = "Physician:";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelDOBheader
            // 
            this.labelDOBheader.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDOBheader.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDOBheader.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelDOBheader.Location = new System.Drawing.Point(0, 233);
            this.labelDOBheader.Margin = new System.Windows.Forms.Padding(0);
            this.labelDOBheader.Name = "labelDOBheader";
            this.labelDOBheader.Size = new System.Drawing.Size(180, 46);
            this.labelDOBheader.TabIndex = 39;
            this.labelDOBheader.Text = "D.O.B.:";
            // 
            // labelPatientID
            // 
            this.labelPatientID.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPatientID.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPatientID.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.labelPatientID.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelPatientID.Location = new System.Drawing.Point(0, 190);
            this.labelPatientID.Name = "labelPatientID";
            this.labelPatientID.Size = new System.Drawing.Size(180, 43);
            this.labelPatientID.TabIndex = 37;
            this.labelPatientID.Text = "Patient ID:";
            this.labelPatientID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelPatientNameHeader
            // 
            this.labelPatientNameHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPatientNameHeader.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPatientNameHeader.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelPatientNameHeader.Location = new System.Drawing.Point(0, 92);
            this.labelPatientNameHeader.Name = "labelPatientNameHeader";
            this.labelPatientNameHeader.Size = new System.Drawing.Size(180, 98);
            this.labelPatientNameHeader.TabIndex = 38;
            this.labelPatientNameHeader.Text = "Name:";
            // 
            // label9
            // 
            this.label9.Dock = System.Windows.Forms.DockStyle.Top;
            this.label9.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label9.Location = new System.Drawing.Point(0, 46);
            this.label9.Margin = new System.Windows.Forms.Padding(0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(180, 46);
            this.label9.TabIndex = 43;
            this.label9.Text = "ID:";
            // 
            // label50
            // 
            this.label50.Dock = System.Windows.Forms.DockStyle.Top;
            this.label50.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(0, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(180, 46);
            this.label50.TabIndex = 42;
            this.label50.Text = "Patient";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel61
            // 
            this.panel61.Controls.Add(this.panel8);
            this.panel61.Controls.Add(this.panel7);
            this.panel61.Controls.Add(this.panel5);
            this.panel61.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel61.Location = new System.Drawing.Point(406, 0);
            this.panel61.Name = "panel61";
            this.panel61.Size = new System.Drawing.Size(571, 328);
            this.panel61.TabIndex = 6;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.labelClient);
            this.panel8.Controls.Add(this.labelSerialNr);
            this.panel8.Controls.Add(this.labelSingleEventReportDate);
            this.panel8.Controls.Add(this.labelReportNr);
            this.panel8.Controls.Add(this.labelStudyNr);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel8.Location = new System.Drawing.Point(170, 49);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(395, 279);
            this.panel8.TabIndex = 2;
            // 
            // labelClient
            // 
            this.labelClient.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelClient.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelClient.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.labelClient.Location = new System.Drawing.Point(0, 184);
            this.labelClient.Name = "labelClient";
            this.labelClient.Size = new System.Drawing.Size(395, 95);
            this.labelClient.TabIndex = 48;
            this.labelClient.Text = "New York Hospital";
            // 
            // labelSerialNr
            // 
            this.labelSerialNr.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelSerialNr.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSerialNr.Location = new System.Drawing.Point(0, 138);
            this.labelSerialNr.Name = "labelSerialNr";
            this.labelSerialNr.Size = new System.Drawing.Size(395, 46);
            this.labelSerialNr.TabIndex = 40;
            this.labelSerialNr.Text = "AA102044403";
            this.labelSerialNr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSingleEventReportDate
            // 
            this.labelSingleEventReportDate.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelSingleEventReportDate.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSingleEventReportDate.Location = new System.Drawing.Point(0, 92);
            this.labelSingleEventReportDate.Name = "labelSingleEventReportDate";
            this.labelSingleEventReportDate.Size = new System.Drawing.Size(395, 46);
            this.labelSingleEventReportDate.TabIndex = 8;
            this.labelSingleEventReportDate.Text = "11/12/2016";
            this.labelSingleEventReportDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelReportNr
            // 
            this.labelReportNr.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelReportNr.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelReportNr.Location = new System.Drawing.Point(0, 46);
            this.labelReportNr.Name = "labelReportNr";
            this.labelReportNr.Size = new System.Drawing.Size(395, 46);
            this.labelReportNr.TabIndex = 0;
            this.labelReportNr.Text = "991";
            this.labelReportNr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStudyNr
            // 
            this.labelStudyNr.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStudyNr.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStudyNr.Location = new System.Drawing.Point(0, 0);
            this.labelStudyNr.Name = "labelStudyNr";
            this.labelStudyNr.Size = new System.Drawing.Size(395, 46);
            this.labelStudyNr.TabIndex = 40;
            this.labelStudyNr.Text = "1212212";
            this.labelStudyNr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.labelClientHeader);
            this.panel7.Controls.Add(this.labelSerialNrHeader);
            this.panel7.Controls.Add(this.label38);
            this.panel7.Controls.Add(this.label35);
            this.panel7.Controls.Add(this.labelStudyNrHeader);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel7.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel7.Location = new System.Drawing.Point(0, 49);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(170, 279);
            this.panel7.TabIndex = 1;
            // 
            // labelClientHeader
            // 
            this.labelClientHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelClientHeader.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelClientHeader.Location = new System.Drawing.Point(0, 184);
            this.labelClientHeader.Name = "labelClientHeader";
            this.labelClientHeader.Size = new System.Drawing.Size(170, 46);
            this.labelClientHeader.TabIndex = 43;
            this.labelClientHeader.Text = "Hospital:";
            this.labelClientHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSerialNrHeader
            // 
            this.labelSerialNrHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelSerialNrHeader.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSerialNrHeader.Location = new System.Drawing.Point(0, 138);
            this.labelSerialNrHeader.Name = "labelSerialNrHeader";
            this.labelSerialNrHeader.Size = new System.Drawing.Size(170, 46);
            this.labelSerialNrHeader.TabIndex = 35;
            this.labelSerialNrHeader.Text = "Serial#:";
            this.labelSerialNrHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelSerialNrHeader.Click += new System.EventHandler(this.labelSerialNrHeader_Click);
            // 
            // label38
            // 
            this.label38.Dock = System.Windows.Forms.DockStyle.Top;
            this.label38.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(0, 92);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(170, 46);
            this.label38.TabIndex = 42;
            this.label38.Text = "Date:";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label38.Click += new System.EventHandler(this.label38_Click);
            // 
            // label35
            // 
            this.label35.Dock = System.Windows.Forms.DockStyle.Top;
            this.label35.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(0, 46);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(170, 46);
            this.label35.TabIndex = 7;
            this.label35.Text = "Report#:";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStudyNrHeader
            // 
            this.labelStudyNrHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStudyNrHeader.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStudyNrHeader.Location = new System.Drawing.Point(0, 0);
            this.labelStudyNrHeader.Name = "labelStudyNrHeader";
            this.labelStudyNrHeader.Size = new System.Drawing.Size(170, 46);
            this.labelStudyNrHeader.TabIndex = 35;
            this.labelStudyNrHeader.Text = "Study#:";
            this.labelStudyNrHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelStudyNrHeader.Click += new System.EventHandler(this.labelStudyNrHeader_Click);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.label37);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(571, 49);
            this.panel5.TabIndex = 0;
            // 
            // label37
            // 
            this.label37.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label37.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(0, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(571, 49);
            this.label37.TabIndex = 41;
            this.label37.Text = "Report information";
            this.label37.Click += new System.EventHandler(this.label37_Click);
            // 
            // panel91
            // 
            this.panel91.Controls.Add(this.labelCenter2);
            this.panel91.Controls.Add(this.labelCenter1);
            this.panel91.Controls.Add(this.pictureBoxCenter1);
            this.panel91.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel91.Location = new System.Drawing.Point(0, 0);
            this.panel91.Name = "panel91";
            this.panel91.Size = new System.Drawing.Size(406, 328);
            this.panel91.TabIndex = 9;
            // 
            // labelCenter2
            // 
            this.labelCenter2.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelCenter2.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCenter2.Location = new System.Drawing.Point(0, 196);
            this.labelCenter2.Name = "labelCenter2";
            this.labelCenter2.Size = new System.Drawing.Size(406, 49);
            this.labelCenter2.TabIndex = 12;
            this.labelCenter2.Text = "c2";
            this.labelCenter2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelCenter1
            // 
            this.labelCenter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelCenter1.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCenter1.Location = new System.Drawing.Point(0, 147);
            this.labelCenter1.Name = "labelCenter1";
            this.labelCenter1.Size = new System.Drawing.Size(406, 49);
            this.labelCenter1.TabIndex = 11;
            this.labelCenter1.Text = "c1";
            this.labelCenter1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBoxCenter1
            // 
            this.pictureBoxCenter1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBoxCenter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBoxCenter1.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxCenter1.Name = "pictureBoxCenter1";
            this.pictureBoxCenter1.Size = new System.Drawing.Size(406, 147);
            this.pictureBoxCenter1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxCenter1.TabIndex = 13;
            this.pictureBoxCenter1.TabStop = false;
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripGenPages,
            this.toolStripButtonPrint,
            this.toolStripButton1,
            this.toolStripClipboard1,
            this.toolStripClipboard2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(3424, 39);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            this.toolStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStrip1_ItemClicked);
            // 
            // toolStripGenPages
            // 
            this.toolStripGenPages.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripGenPages.Image = ((System.Drawing.Image)(resources.GetObject("toolStripGenPages.Image")));
            this.toolStripGenPages.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripGenPages.Name = "toolStripGenPages";
            this.toolStripGenPages.Size = new System.Drawing.Size(112, 36);
            this.toolStripGenPages.Text = "Generating Pages...";
            // 
            // toolStripButtonPrint
            // 
            this.toolStripButtonPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonPrint.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonPrint.Image")));
            this.toolStripButtonPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonPrint.Name = "toolStripButtonPrint";
            this.toolStripButtonPrint.Size = new System.Drawing.Size(36, 36);
            this.toolStripButtonPrint.Text = "Print";
            this.toolStripButtonPrint.Click += new System.EventHandler(this.toolStripButton1_Click_1);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton1.Text = "toolStripButton1";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click_2);
            // 
            // toolStripClipboard1
            // 
            this.toolStripClipboard1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripClipboard1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripClipboard1.Image")));
            this.toolStripClipboard1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripClipboard1.Name = "toolStripClipboard1";
            this.toolStripClipboard1.Size = new System.Drawing.Size(72, 36);
            this.toolStripClipboard1.Text = "Clipboard 1";
            this.toolStripClipboard1.Click += new System.EventHandler(this.toolStripClipboard_Click);
            // 
            // toolStripClipboard2
            // 
            this.toolStripClipboard2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripClipboard2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripClipboard2.Image")));
            this.toolStripClipboard2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripClipboard2.Name = "toolStripClipboard2";
            this.toolStripClipboard2.Size = new System.Drawing.Size(72, 36);
            this.toolStripClipboard2.Text = "Clipboard 2";
            this.toolStripClipboard2.Click += new System.EventHandler(this.toolStripClipboard2_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // panel21
            // 
            this.panel21.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel21.Location = new System.Drawing.Point(0, 39);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(3424, 21);
            this.panel21.TabIndex = 2;
            // 
            // panelPrintArea2
            // 
            this.panelPrintArea2.BackColor = System.Drawing.Color.White;
            this.panelPrintArea2.Controls.Add(this.panelPage2Footer);
            this.panelPrintArea2.Controls.Add(this.panelHeaderPage2);
            this.panelPrintArea2.Location = new System.Drawing.Point(0, 3420);
            this.panelPrintArea2.Margin = new System.Windows.Forms.Padding(0);
            this.panelPrintArea2.Name = "panelPrintArea2";
            this.panelPrintArea2.Size = new System.Drawing.Size(3303, 3303);
            this.panelPrintArea2.TabIndex = 3;
            // 
            // panelPage2Footer
            // 
            this.panelPage2Footer.Controls.Add(this.label2);
            this.panelPage2Footer.Controls.Add(this.label57);
            this.panelPage2Footer.Controls.Add(this.labelPagex);
            this.panelPage2Footer.Controls.Add(this.label58);
            this.panelPage2Footer.Controls.Add(this.labelPagexOfX);
            this.panelPage2Footer.Controls.Add(this.labelPrintDate2Bottom);
            this.panelPage2Footer.Controls.Add(this.labelPrintDate);
            this.panelPage2Footer.Controls.Add(this.label5);
            this.panelPage2Footer.Controls.Add(this.label6);
            this.panelPage2Footer.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelPage2Footer.Location = new System.Drawing.Point(0, 3219);
            this.panelPage2Footer.Name = "panelPage2Footer";
            this.panelPage2Footer.Size = new System.Drawing.Size(3303, 84);
            this.panelPage2Footer.TabIndex = 37;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(1171, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(1722, 84);
            this.label2.TabIndex = 19;
            this.label2.Text = "Copyright 2017 © Techmedic International B.V.";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label57
            // 
            this.label57.Dock = System.Windows.Forms.DockStyle.Right;
            this.label57.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(2893, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(101, 84);
            this.label57.TabIndex = 23;
            this.label57.Text = "Page";
            this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPagex
            // 
            this.labelPagex.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelPagex.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPagex.Location = new System.Drawing.Point(2994, 0);
            this.labelPagex.Name = "labelPagex";
            this.labelPagex.Size = new System.Drawing.Size(78, 84);
            this.labelPagex.TabIndex = 22;
            this.labelPagex.Text = "2";
            this.labelPagex.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label58
            // 
            this.label58.Dock = System.Windows.Forms.DockStyle.Right;
            this.label58.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(3072, 0);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(62, 84);
            this.label58.TabIndex = 21;
            this.label58.Text = "of";
            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPagexOfX
            // 
            this.labelPagexOfX.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelPagexOfX.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPagexOfX.Location = new System.Drawing.Point(3134, 0);
            this.labelPagexOfX.Name = "labelPagexOfX";
            this.labelPagexOfX.Size = new System.Drawing.Size(114, 84);
            this.labelPagexOfX.TabIndex = 20;
            this.labelPagexOfX.Text = "20p2";
            this.labelPagexOfX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPrintDate2Bottom
            // 
            this.labelPrintDate2Bottom.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPrintDate2Bottom.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPrintDate2Bottom.Location = new System.Drawing.Point(245, 0);
            this.labelPrintDate2Bottom.Name = "labelPrintDate2Bottom";
            this.labelPrintDate2Bottom.Size = new System.Drawing.Size(926, 84);
            this.labelPrintDate2Bottom.TabIndex = 18;
            this.labelPrintDate2Bottom.Text = "12/27/2016 22:22:22 AM+1200 v12345";
            this.labelPrintDate2Bottom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelPrintDate
            // 
            this.labelPrintDate.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPrintDate.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPrintDate.Location = new System.Drawing.Point(60, 0);
            this.labelPrintDate.Name = "labelPrintDate";
            this.labelPrintDate.Size = new System.Drawing.Size(185, 84);
            this.labelPrintDate.TabIndex = 17;
            this.labelPrintDate.Text = "Print date:";
            this.labelPrintDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label5.Dock = System.Windows.Forms.DockStyle.Right;
            this.label5.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label5.Location = new System.Drawing.Point(3248, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 84);
            this.label5.TabIndex = 1;
            this.label5.Text = "R2";
            this.label5.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label5.Visible = false;
            // 
            // label6
            // 
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label6.Dock = System.Windows.Forms.DockStyle.Left;
            this.label6.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 84);
            this.label6.TabIndex = 0;
            this.label6.Text = "L2";
            this.label6.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label6.Visible = false;
            // 
            // panelHeaderPage2
            // 
            this.panelHeaderPage2.AutoSize = true;
            this.panelHeaderPage2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panelHeaderPage2.Controls.Add(this.panel255);
            this.panelHeaderPage2.Controls.Add(this.panel300);
            this.panelHeaderPage2.Controls.Add(this.panel391);
            this.panelHeaderPage2.Controls.Add(this.panel450);
            this.panelHeaderPage2.Controls.Add(this.panel488);
            this.panelHeaderPage2.Controls.Add(this.panelPage2Header);
            this.panelHeaderPage2.Controls.Add(this.panel500);
            this.panelHeaderPage2.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.panelHeaderPage2.Location = new System.Drawing.Point(0, 8);
            this.panelHeaderPage2.Name = "panelHeaderPage2";
            this.panelHeaderPage2.Size = new System.Drawing.Size(2336, 339);
            this.panelHeaderPage2.TabIndex = 36;
            // 
            // panel255
            // 
            this.panel255.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel255.Controls.Add(this.panel270);
            this.panel255.Controls.Add(this.panel273);
            this.panel255.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel255.Location = new System.Drawing.Point(0, 291);
            this.panel255.Name = "panel255";
            this.panel255.Size = new System.Drawing.Size(2336, 14);
            this.panel255.TabIndex = 28;
            // 
            // panel270
            // 
            this.panel270.AutoSize = true;
            this.panel270.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel270.Location = new System.Drawing.Point(0, 0);
            this.panel270.Name = "panel270";
            this.panel270.Size = new System.Drawing.Size(0, 12);
            this.panel270.TabIndex = 6;
            // 
            // panel273
            // 
            this.panel273.AutoSize = true;
            this.panel273.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel273.Location = new System.Drawing.Point(0, 0);
            this.panel273.Name = "panel273";
            this.panel273.Size = new System.Drawing.Size(0, 12);
            this.panel273.TabIndex = 3;
            // 
            // panel300
            // 
            this.panel300.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel300.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel300.Location = new System.Drawing.Point(0, 289);
            this.panel300.Name = "panel300";
            this.panel300.Size = new System.Drawing.Size(2336, 2);
            this.panel300.TabIndex = 20;
            // 
            // panel391
            // 
            this.panel391.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel391.Controls.Add(this.panel393);
            this.panel391.Controls.Add(this.panel394);
            this.panel391.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel391.Location = new System.Drawing.Point(0, 276);
            this.panel391.Name = "panel391";
            this.panel391.Size = new System.Drawing.Size(2336, 13);
            this.panel391.TabIndex = 9;
            // 
            // panel393
            // 
            this.panel393.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel393.Location = new System.Drawing.Point(2033, 0);
            this.panel393.Name = "panel393";
            this.panel393.Size = new System.Drawing.Size(301, 11);
            this.panel393.TabIndex = 2;
            // 
            // panel394
            // 
            this.panel394.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel394.Location = new System.Drawing.Point(0, 0);
            this.panel394.Name = "panel394";
            this.panel394.Size = new System.Drawing.Size(502, 11);
            this.panel394.TabIndex = 0;
            // 
            // panel450
            // 
            this.panel450.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel450.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel450.Location = new System.Drawing.Point(0, 275);
            this.panel450.Name = "panel450";
            this.panel450.Size = new System.Drawing.Size(2336, 1);
            this.panel450.TabIndex = 3;
            // 
            // panel488
            // 
            this.panel488.Controls.Add(this.panel489);
            this.panel488.Controls.Add(this.panel490);
            this.panel488.Controls.Add(this.panel493);
            this.panel488.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel488.Location = new System.Drawing.Point(0, 265);
            this.panel488.Name = "panel488";
            this.panel488.Size = new System.Drawing.Size(2336, 10);
            this.panel488.TabIndex = 1;
            // 
            // panel489
            // 
            this.panel489.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel489.Location = new System.Drawing.Point(2159, 0);
            this.panel489.Name = "panel489";
            this.panel489.Size = new System.Drawing.Size(177, 10);
            this.panel489.TabIndex = 5;
            // 
            // panel490
            // 
            this.panel490.Controls.Add(this.panel492);
            this.panel490.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel490.Location = new System.Drawing.Point(234, 0);
            this.panel490.Name = "panel490";
            this.panel490.Size = new System.Drawing.Size(2102, 10);
            this.panel490.TabIndex = 4;
            // 
            // panel492
            // 
            this.panel492.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel492.Location = new System.Drawing.Point(0, 0);
            this.panel492.Name = "panel492";
            this.panel492.Size = new System.Drawing.Size(971, 10);
            this.panel492.TabIndex = 4;
            // 
            // panel493
            // 
            this.panel493.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel493.Location = new System.Drawing.Point(0, 0);
            this.panel493.Name = "panel493";
            this.panel493.Size = new System.Drawing.Size(234, 10);
            this.panel493.TabIndex = 3;
            // 
            // panelPage2Header
            // 
            this.panelPage2Header.Controls.Add(this.labelPage2ReportNr);
            this.panelPage2Header.Controls.Add(this.labelReportText2);
            this.panelPage2Header.Controls.Add(this.panel74);
            this.panelPage2Header.Controls.Add(this.panel76);
            this.panelPage2Header.Controls.Add(this.panel495);
            this.panelPage2Header.Controls.Add(this.panel499);
            this.panelPage2Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelPage2Header.Location = new System.Drawing.Point(0, 175);
            this.panelPage2Header.Name = "panelPage2Header";
            this.panelPage2Header.Size = new System.Drawing.Size(2336, 90);
            this.panelPage2Header.TabIndex = 0;
            // 
            // labelPage2ReportNr
            // 
            this.labelPage2ReportNr.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPage2ReportNr.Font = new System.Drawing.Font("Verdana", 24.25F, System.Drawing.FontStyle.Bold);
            this.labelPage2ReportNr.Location = new System.Drawing.Point(2257, 0);
            this.labelPage2ReportNr.Name = "labelPage2ReportNr";
            this.labelPage2ReportNr.Size = new System.Drawing.Size(107, 90);
            this.labelPage2ReportNr.TabIndex = 44;
            this.labelPage2ReportNr.Text = "12";
            this.labelPage2ReportNr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelReportText2
            // 
            this.labelReportText2.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelReportText2.Font = new System.Drawing.Font("Verdana", 24F);
            this.labelReportText2.Location = new System.Drawing.Point(2117, 0);
            this.labelReportText2.Name = "labelReportText2";
            this.labelReportText2.Size = new System.Drawing.Size(140, 90);
            this.labelReportText2.TabIndex = 43;
            this.labelReportText2.Text = "Report:";
            this.labelReportText2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel74
            // 
            this.panel74.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel74.Location = new System.Drawing.Point(2100, 0);
            this.panel74.Name = "panel74";
            this.panel74.Size = new System.Drawing.Size(17, 90);
            this.panel74.TabIndex = 18;
            // 
            // panel76
            // 
            this.panel76.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel76.Location = new System.Drawing.Point(2083, 0);
            this.panel76.Name = "panel76";
            this.panel76.Size = new System.Drawing.Size(17, 90);
            this.panel76.TabIndex = 17;
            // 
            // panel495
            // 
            this.panel495.Controls.Add(this.panel534);
            this.panel495.Controls.Add(this.panel533);
            this.panel495.Controls.Add(this.panel532);
            this.panel495.Controls.Add(this.panel531);
            this.panel495.Controls.Add(this.panel530);
            this.panel495.Controls.Add(this.panel529);
            this.panel495.Controls.Add(this.panel528);
            this.panel495.Controls.Add(this.panel527);
            this.panel495.Controls.Add(this.panel526);
            this.panel495.Controls.Add(this.panel525);
            this.panel495.Controls.Add(this.panel524);
            this.panel495.Controls.Add(this.panel523);
            this.panel495.Controls.Add(this.panel522);
            this.panel495.Controls.Add(this.panel521);
            this.panel495.Controls.Add(this.panel520);
            this.panel495.Controls.Add(this.panel519);
            this.panel495.Controls.Add(this.panel497);
            this.panel495.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel495.Location = new System.Drawing.Point(28, 0);
            this.panel495.Name = "panel495";
            this.panel495.Size = new System.Drawing.Size(2055, 90);
            this.panel495.TabIndex = 2;
            // 
            // panel534
            // 
            this.panel534.Controls.Add(this.labelStudyNumberResPage2);
            this.panel534.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel534.Location = new System.Drawing.Point(1860, 0);
            this.panel534.Name = "panel534";
            this.panel534.Size = new System.Drawing.Size(185, 90);
            this.panel534.TabIndex = 18;
            // 
            // labelStudyNumberResPage2
            // 
            this.labelStudyNumberResPage2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelStudyNumberResPage2.Font = new System.Drawing.Font("Verdana", 24.25F, System.Drawing.FontStyle.Bold);
            this.labelStudyNumberResPage2.Location = new System.Drawing.Point(0, 0);
            this.labelStudyNumberResPage2.Name = "labelStudyNumberResPage2";
            this.labelStudyNumberResPage2.Size = new System.Drawing.Size(185, 90);
            this.labelStudyNumberResPage2.TabIndex = 41;
            this.labelStudyNumberResPage2.Text = "1212212";
            this.labelStudyNumberResPage2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel533
            // 
            this.panel533.Controls.Add(this.labelStudyNrPage2);
            this.panel533.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel533.Location = new System.Drawing.Point(1721, 0);
            this.panel533.Name = "panel533";
            this.panel533.Size = new System.Drawing.Size(139, 90);
            this.panel533.TabIndex = 17;
            // 
            // labelStudyNrPage2
            // 
            this.labelStudyNrPage2.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelStudyNrPage2.Font = new System.Drawing.Font("Verdana", 24F);
            this.labelStudyNrPage2.Location = new System.Drawing.Point(0, 0);
            this.labelStudyNrPage2.Name = "labelStudyNrPage2";
            this.labelStudyNrPage2.Size = new System.Drawing.Size(139, 90);
            this.labelStudyNrPage2.TabIndex = 36;
            this.labelStudyNrPage2.Text = "Study:";
            this.labelStudyNrPage2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel532
            // 
            this.panel532.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel532.Location = new System.Drawing.Point(1704, 0);
            this.panel532.Name = "panel532";
            this.panel532.Size = new System.Drawing.Size(17, 90);
            this.panel532.TabIndex = 16;
            // 
            // panel531
            // 
            this.panel531.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel531.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel531.Location = new System.Drawing.Point(1702, 0);
            this.panel531.Name = "panel531";
            this.panel531.Size = new System.Drawing.Size(2, 90);
            this.panel531.TabIndex = 15;
            // 
            // panel530
            // 
            this.panel530.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel530.Location = new System.Drawing.Point(1685, 0);
            this.panel530.Name = "panel530";
            this.panel530.Size = new System.Drawing.Size(17, 90);
            this.panel530.TabIndex = 14;
            // 
            // panel529
            // 
            this.panel529.Controls.Add(this.labelDateOfBirthResPage2);
            this.panel529.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel529.Location = new System.Drawing.Point(1432, 0);
            this.panel529.Name = "panel529";
            this.panel529.Size = new System.Drawing.Size(253, 90);
            this.panel529.TabIndex = 13;
            // 
            // labelDateOfBirthResPage2
            // 
            this.labelDateOfBirthResPage2.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelDateOfBirthResPage2.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold);
            this.labelDateOfBirthResPage2.Location = new System.Drawing.Point(0, 0);
            this.labelDateOfBirthResPage2.Name = "labelDateOfBirthResPage2";
            this.labelDateOfBirthResPage2.Size = new System.Drawing.Size(253, 90);
            this.labelDateOfBirthResPage2.TabIndex = 41;
            this.labelDateOfBirthResPage2.Text = "06/08/1974";
            this.labelDateOfBirthResPage2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel528
            // 
            this.panel528.Controls.Add(this.labelDateOfBirthPage2);
            this.panel528.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel528.Location = new System.Drawing.Point(1322, 0);
            this.panel528.Name = "panel528";
            this.panel528.Size = new System.Drawing.Size(110, 90);
            this.panel528.TabIndex = 12;
            // 
            // labelDateOfBirthPage2
            // 
            this.labelDateOfBirthPage2.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelDateOfBirthPage2.Font = new System.Drawing.Font("Verdana", 24F);
            this.labelDateOfBirthPage2.Location = new System.Drawing.Point(0, 0);
            this.labelDateOfBirthPage2.Name = "labelDateOfBirthPage2";
            this.labelDateOfBirthPage2.Size = new System.Drawing.Size(110, 90);
            this.labelDateOfBirthPage2.TabIndex = 36;
            this.labelDateOfBirthPage2.Text = "DOB:";
            this.labelDateOfBirthPage2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel527
            // 
            this.panel527.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel527.Location = new System.Drawing.Point(1305, 0);
            this.panel527.Name = "panel527";
            this.panel527.Size = new System.Drawing.Size(17, 90);
            this.panel527.TabIndex = 11;
            // 
            // panel526
            // 
            this.panel526.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel526.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel526.Location = new System.Drawing.Point(1303, 0);
            this.panel526.Name = "panel526";
            this.panel526.Size = new System.Drawing.Size(2, 90);
            this.panel526.TabIndex = 10;
            // 
            // panel525
            // 
            this.panel525.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel525.Location = new System.Drawing.Point(1286, 0);
            this.panel525.Name = "panel525";
            this.panel525.Size = new System.Drawing.Size(17, 90);
            this.panel525.TabIndex = 9;
            // 
            // panel524
            // 
            this.panel524.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel524.Location = new System.Drawing.Point(831, 0);
            this.panel524.Name = "panel524";
            this.panel524.Size = new System.Drawing.Size(455, 90);
            this.panel524.TabIndex = 8;
            // 
            // panel523
            // 
            this.panel523.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel523.Location = new System.Drawing.Point(671, 0);
            this.panel523.Name = "panel523";
            this.panel523.Size = new System.Drawing.Size(160, 90);
            this.panel523.TabIndex = 7;
            // 
            // panel522
            // 
            this.panel522.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel522.Location = new System.Drawing.Point(654, 0);
            this.panel522.Name = "panel522";
            this.panel522.Size = new System.Drawing.Size(17, 90);
            this.panel522.TabIndex = 6;
            // 
            // panel521
            // 
            this.panel521.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel521.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel521.Location = new System.Drawing.Point(652, 0);
            this.panel521.Name = "panel521";
            this.panel521.Size = new System.Drawing.Size(2, 90);
            this.panel521.TabIndex = 5;
            // 
            // panel520
            // 
            this.panel520.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel520.Location = new System.Drawing.Point(635, 0);
            this.panel520.Name = "panel520";
            this.panel520.Size = new System.Drawing.Size(17, 90);
            this.panel520.TabIndex = 4;
            // 
            // panel519
            // 
            this.panel519.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel519.Location = new System.Drawing.Point(200, 0);
            this.panel519.Name = "panel519";
            this.panel519.Size = new System.Drawing.Size(435, 90);
            this.panel519.TabIndex = 3;
            // 
            // panel497
            // 
            this.panel497.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel497.Location = new System.Drawing.Point(0, 0);
            this.panel497.Name = "panel497";
            this.panel497.Size = new System.Drawing.Size(200, 90);
            this.panel497.TabIndex = 2;
            // 
            // panel499
            // 
            this.panel499.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel499.Location = new System.Drawing.Point(0, 0);
            this.panel499.Name = "panel499";
            this.panel499.Size = new System.Drawing.Size(28, 90);
            this.panel499.TabIndex = 0;
            // 
            // panel500
            // 
            this.panel500.Controls.Add(this.panel501);
            this.panel500.Controls.Add(this.panel506);
            this.panel500.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel500.Location = new System.Drawing.Point(0, 0);
            this.panel500.Name = "panel500";
            this.panel500.Size = new System.Drawing.Size(2336, 175);
            this.panel500.TabIndex = 4;
            // 
            // panel501
            // 
            this.panel501.Controls.Add(this.panel503);
            this.panel501.Controls.Add(this.panel504);
            this.panel501.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel501.Location = new System.Drawing.Point(1605, 0);
            this.panel501.Name = "panel501";
            this.panel501.Size = new System.Drawing.Size(731, 175);
            this.panel501.TabIndex = 8;
            // 
            // panel503
            // 
            this.panel503.Controls.Add(this.labelCenter2p2);
            this.panel503.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel503.Location = new System.Drawing.Point(0, 55);
            this.panel503.Name = "panel503";
            this.panel503.Size = new System.Drawing.Size(731, 60);
            this.panel503.TabIndex = 15;
            // 
            // labelCenter2p2
            // 
            this.labelCenter2p2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCenter2p2.Font = new System.Drawing.Font("Verdana", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCenter2p2.Location = new System.Drawing.Point(0, 0);
            this.labelCenter2p2.Name = "labelCenter2p2";
            this.labelCenter2p2.Size = new System.Drawing.Size(731, 60);
            this.labelCenter2p2.TabIndex = 12;
            this.labelCenter2p2.Text = "c2";
            this.labelCenter2p2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel504
            // 
            this.panel504.Controls.Add(this.labelCenter1p2);
            this.panel504.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel504.Location = new System.Drawing.Point(0, 0);
            this.panel504.Name = "panel504";
            this.panel504.Size = new System.Drawing.Size(731, 55);
            this.panel504.TabIndex = 14;
            // 
            // labelCenter1p2
            // 
            this.labelCenter1p2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCenter1p2.Font = new System.Drawing.Font("Verdana", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCenter1p2.Location = new System.Drawing.Point(0, 0);
            this.labelCenter1p2.Name = "labelCenter1p2";
            this.labelCenter1p2.Size = new System.Drawing.Size(731, 55);
            this.labelCenter1p2.TabIndex = 11;
            this.labelCenter1p2.Text = "c1";
            this.labelCenter1p2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel506
            // 
            this.panel506.Controls.Add(this.pictureBoxCenter2);
            this.panel506.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel506.Location = new System.Drawing.Point(0, 0);
            this.panel506.Name = "panel506";
            this.panel506.Size = new System.Drawing.Size(683, 175);
            this.panel506.TabIndex = 6;
            // 
            // pictureBoxCenter2
            // 
            this.pictureBoxCenter2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBoxCenter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBoxCenter2.Location = new System.Drawing.Point(383, 0);
            this.pictureBoxCenter2.Name = "pictureBoxCenter2";
            this.pictureBoxCenter2.Size = new System.Drawing.Size(300, 175);
            this.pictureBoxCenter2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxCenter2.TabIndex = 0;
            this.pictureBoxCenter2.TabStop = false;
            // 
            // CPrintAnalys12Form
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(3441, 1386);
            this.Controls.Add(this.panelPrintArea2);
            this.Controls.Add(this.panelPrintArea1);
            this.Controls.Add(this.panel21);
            this.Controls.Add(this.toolStrip1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "CPrintAnalys12Form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Print Analysis 12 Lead";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CPrintAnalys12Form_FormClosing);
            this.Shown += new System.EventHandler(this.CPrintAnalys12Form_Shown);
            this.panelPrintArea1.ResumeLayout(false);
            this.panel150.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.panel22.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panelECGSweepAmpIndicator.ResumeLayout(false);
            this.panelFooterPage1.ResumeLayout(false);
            this.panel510.ResumeLayout(false);
            this.panel509.ResumeLayout(false);
            this.panel508.ResumeLayout(false);
            this.panel507.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panelTimeBarBaselineECG.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panelPrintHeader.ResumeLayout(false);
            this.panel179.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel154.ResumeLayout(false);
            this.panel94.ResumeLayout(false);
            this.panel93.ResumeLayout(false);
            this.panel92.ResumeLayout(false);
            this.panel61.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel91.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCenter1)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panelPrintArea2.ResumeLayout(false);
            this.panelPrintArea2.PerformLayout();
            this.panelPage2Footer.ResumeLayout(false);
            this.panelHeaderPage2.ResumeLayout(false);
            this.panel255.ResumeLayout(false);
            this.panel255.PerformLayout();
            this.panel391.ResumeLayout(false);
            this.panel488.ResumeLayout(false);
            this.panel490.ResumeLayout(false);
            this.panelPage2Header.ResumeLayout(false);
            this.panel495.ResumeLayout(false);
            this.panel534.ResumeLayout(false);
            this.panel533.ResumeLayout(false);
            this.panel529.ResumeLayout(false);
            this.panel528.ResumeLayout(false);
            this.panel500.ResumeLayout(false);
            this.panel501.ResumeLayout(false);
            this.panel503.ResumeLayout(false);
            this.panel504.ResumeLayout(false);
            this.panel506.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCenter2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelPrintArea1;
        private System.Windows.Forms.Panel panelHorSepPatDet;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButtonPrint;
        private System.Windows.Forms.Panel panelPrintHeader;
        private System.Windows.Forms.Label labelCenter2;
        private System.Windows.Forms.Label labelCenter1;
        private System.Windows.Forms.Panel panel61;
        private System.Windows.Forms.Label labelSingleEventReportDate;
        private System.Windows.Forms.Panel panelECGSweepAmpIndicator;
        private System.Windows.Forms.Panel panelFullStrip;
        private System.Windows.Forms.Panel panelTimeBarBaselineECG;
        private System.Windows.Forms.Panel panelTimeHeadingsPerStrip;
        private System.Windows.Forms.Panel panel150;
        private System.Windows.Forms.Panel panel149;
        private System.Windows.Forms.Panel panelLineUnderSampl1;
        private System.Windows.Forms.Label labelSerialNr;
        private System.Windows.Forms.Label labelSerialNrHeader;
        private System.Windows.Forms.Label labelStudyNr;
        private System.Windows.Forms.Label labelReportNr;
        private System.Windows.Forms.Panel panelFooterPage1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripButton toolStripClipboard1;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Panel panelPrintArea2;
        private System.Windows.Forms.Panel panel509;
        private System.Windows.Forms.Label labelPage1of;
        private System.Windows.Forms.Panel panel508;
        private System.Windows.Forms.Panel panel507;
        private System.Windows.Forms.Panel panel510;
        private System.Windows.Forms.Label labelPage;
        private System.Windows.Forms.Label labelPageOf;
        private System.Windows.Forms.Label labelPage1ofx;
        private System.Windows.Forms.Panel panelPage2Footer;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ToolStripButton toolStripClipboard2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label labelPrintDate1Bottom;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ToolStripButton toolStripGenPages;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label labelPagex;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label labelPagexOfX;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelPrintDate2Bottom;
        private System.Windows.Forms.Label labelPrintDate;
        private System.Windows.Forms.Label labelEventMiddle;
        private System.Windows.Forms.Label labelEventStart;
        private System.Windows.Forms.Label labelEventEnd;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.Panel panel91;
        private System.Windows.Forms.Panel panel154;
        private System.Windows.Forms.Panel panel94;
        private System.Windows.Forms.Panel panel93;
        private System.Windows.Forms.Label labelPatientIDResult;
        private System.Windows.Forms.Panel panel92;
        private System.Windows.Forms.Label labelPatientID;
        private System.Windows.Forms.Label labelClient;
        private System.Windows.Forms.Label labelDateOfBirth;
        private System.Windows.Forms.Label labelPatLastName;
        private System.Windows.Forms.Label labelDOBheader;
        private System.Windows.Forms.Label labelPatientNameHeader;
        private System.Windows.Forms.Label labelStudyNrHeader;
        private System.Windows.Forms.Panel panel179;
        private System.Windows.Forms.Label labelRythm;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label labelResultQTmin;
        private System.Windows.Forms.Label labelResultQRSmin;
        private System.Windows.Forms.Label labelResultPRmin;
        private System.Windows.Forms.Label labelResultRRmin;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label labelClientHeader;
        private System.Windows.Forms.PictureBox pictureBoxCenter1;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Panel panelChannel6;
        private System.Windows.Forms.Panel panelChannel5;
        private System.Windows.Forms.Panel panelChannel4;
        private System.Windows.Forms.Panel panelChannel3;
        private System.Windows.Forms.Panel panelChannel2;
        private System.Windows.Forms.Panel panelChannel1;
        private System.Windows.Forms.Panel panelChannel12;
        private System.Windows.Forms.Panel panelChannel11;
        private System.Windows.Forms.Panel panelChannel10;
        private System.Windows.Forms.Panel panelChannel9;
        private System.Windows.Forms.Panel panelChannel8;
        private System.Windows.Forms.Panel panelChannel7;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Label labelSignatureText;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Label labelPhysPrintName;
        private System.Windows.Forms.Label labelPhysicianText;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label labelFindingsRemark;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelSymptoms;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label labelSampleRate;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label labelHPF;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label labelNotch;
        private System.Windows.Forms.Label labelTopStripFeed;
        private System.Windows.Forms.Label labelTopStripAmpl;
        private System.Windows.Forms.Label labelTopStripTime;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label labelBaseSweepSpeed;
        private System.Windows.Forms.Label labelBaseAmplitudeSet;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panelHeaderPage2;
        private System.Windows.Forms.Panel panel255;
        private System.Windows.Forms.Panel panel270;
        private System.Windows.Forms.Panel panel273;
        private System.Windows.Forms.Panel panel300;
        private System.Windows.Forms.Panel panel391;
        private System.Windows.Forms.Panel panel393;
        private System.Windows.Forms.Panel panel394;
        private System.Windows.Forms.Panel panel450;
        private System.Windows.Forms.Panel panel488;
        private System.Windows.Forms.Panel panel489;
        private System.Windows.Forms.Panel panel490;
        private System.Windows.Forms.Panel panel492;
        private System.Windows.Forms.Panel panel493;
        private System.Windows.Forms.Panel panelPage2Header;
        private System.Windows.Forms.Label labelPage2ReportNr;
        private System.Windows.Forms.Label labelReportText2;
        private System.Windows.Forms.Panel panel74;
        private System.Windows.Forms.Panel panel76;
        private System.Windows.Forms.Panel panel495;
        private System.Windows.Forms.Panel panel534;
        private System.Windows.Forms.Label labelStudyNumberResPage2;
        private System.Windows.Forms.Panel panel533;
        private System.Windows.Forms.Label labelStudyNrPage2;
        private System.Windows.Forms.Panel panel532;
        private System.Windows.Forms.Panel panel531;
        private System.Windows.Forms.Panel panel530;
        private System.Windows.Forms.Panel panel529;
        private System.Windows.Forms.Label labelDateOfBirthResPage2;
        private System.Windows.Forms.Panel panel528;
        private System.Windows.Forms.Label labelDateOfBirthPage2;
        private System.Windows.Forms.Panel panel527;
        private System.Windows.Forms.Panel panel526;
        private System.Windows.Forms.Panel panel525;
        private System.Windows.Forms.Panel panel524;
        private System.Windows.Forms.Panel panel523;
        private System.Windows.Forms.Panel panel522;
        private System.Windows.Forms.Panel panel521;
        private System.Windows.Forms.Panel panel520;
        private System.Windows.Forms.Panel panel519;
        private System.Windows.Forms.Panel panel497;
        private System.Windows.Forms.Panel panel499;
        private System.Windows.Forms.Panel panel500;
        private System.Windows.Forms.Panel panel501;
        private System.Windows.Forms.Panel panel503;
        private System.Windows.Forms.Label labelCenter2p2;
        private System.Windows.Forms.Panel panel504;
        private System.Windows.Forms.Label labelCenter1p2;
        private System.Windows.Forms.Panel panel506;
        private System.Windows.Forms.PictureBox pictureBoxCenter2;
        private System.Windows.Forms.Label labelFindingsText;
        private System.Windows.Forms.Label labelActivity;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label labelActivityHeader;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label labelSymptomsHeader;
        private System.Windows.Forms.Label labelStudyNumberResPage1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelResultQTcFmin;
        private System.Windows.Forms.Label labelResultQTcBmin;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label labelPhysician;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Label labelResultQTcFavg;
        private System.Windows.Forms.Label labelResultQTcBavg;
        private System.Windows.Forms.Label labelResultQTavg;
        private System.Windows.Forms.Label labelResultQRSavg;
        private System.Windows.Forms.Label labelResultPRavg;
        private System.Windows.Forms.Label labelResultRRavg;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label labelResultQTcFmax;
        private System.Windows.Forms.Label labelResultQTcBmax;
        private System.Windows.Forms.Label labelResultQTmax;
        private System.Windows.Forms.Label labelResultQRSmax;
        private System.Windows.Forms.Label labelResultPRmax;
        private System.Windows.Forms.Label labelResultRRmax;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label labelPhysPrintSign;
        private System.Windows.Forms.Panel panel16;
    }
}