﻿namespace EventBoard
{
    partial class CPrintAnalysisMultiForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CPrintAnalysisMultiForm));
            this.panelPrintArea1 = new System.Windows.Forms.Panel();
            this.panel51 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.labelPrintDate1Bottom = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panel510 = new System.Windows.Forms.Panel();
            this.labelPage = new System.Windows.Forms.Label();
            this.panel509 = new System.Windows.Forms.Panel();
            this.labelPage1of = new System.Windows.Forms.Label();
            this.panel508 = new System.Windows.Forms.Panel();
            this.labelPageOf = new System.Windows.Forms.Label();
            this.panel507 = new System.Windows.Forms.Panel();
            this.labelPage1ofx = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel78 = new System.Windows.Forms.Panel();
            this.panel110 = new System.Windows.Forms.Panel();
            this.labelReportSignDate = new System.Windows.Forms.Label();
            this.panel109 = new System.Windows.Forms.Panel();
            this.labelDateReportSign = new System.Windows.Forms.Label();
            this.panel108 = new System.Windows.Forms.Panel();
            this.labelPhysSignLine = new System.Windows.Forms.Label();
            this.panel107 = new System.Windows.Forms.Panel();
            this.labelPhysSign = new System.Windows.Forms.Label();
            this.panel106 = new System.Windows.Forms.Panel();
            this.labelPhysPrintName = new System.Windows.Forms.Label();
            this.panel105 = new System.Windows.Forms.Panel();
            this.labelPhysNameReportPrint = new System.Windows.Forms.Label();
            this.panel104 = new System.Windows.Forms.Panel();
            this.panel103 = new System.Windows.Forms.Panel();
            this.panel77 = new System.Windows.Forms.Panel();
            this.panelSample2Time = new System.Windows.Forms.Panel();
            this.panel102 = new System.Windows.Forms.Panel();
            this.labelMiddleSample2 = new System.Windows.Forms.Label();
            this.panel101 = new System.Windows.Forms.Panel();
            this.labelSweepSample2 = new System.Windows.Forms.Label();
            this.panel100 = new System.Windows.Forms.Panel();
            this.labelZoomSample2 = new System.Windows.Forms.Label();
            this.panel99 = new System.Windows.Forms.Panel();
            this.labelEndSample2 = new System.Windows.Forms.Label();
            this.panel98 = new System.Windows.Forms.Panel();
            this.labelStartSample2 = new System.Windows.Forms.Label();
            this.panelSeceondStrip = new System.Windows.Forms.Panel();
            this.pictureBoxZoom2 = new System.Windows.Forms.PictureBox();
            this.panel97 = new System.Windows.Forms.Panel();
            this.panel96 = new System.Windows.Forms.Panel();
            this.panelSample2Lead = new System.Windows.Forms.Panel();
            this.panel95 = new System.Windows.Forms.Panel();
            this.labelQtUnit2 = new System.Windows.Forms.Label();
            this.labelQtValue2 = new System.Windows.Forms.Label();
            this.labelQtText2 = new System.Windows.Forms.Label();
            this.labelQrsUnit2 = new System.Windows.Forms.Label();
            this.labelQrsValue2 = new System.Windows.Forms.Label();
            this.labelQrsText2 = new System.Windows.Forms.Label();
            this.labelPrUnit2 = new System.Windows.Forms.Label();
            this.labelPrValue2 = new System.Windows.Forms.Label();
            this.labelPrText2 = new System.Windows.Forms.Label();
            this.labelMaxHrUnit2 = new System.Windows.Forms.Label();
            this.labelMaxHrValue2 = new System.Windows.Forms.Label();
            this.labelMaxHrText2 = new System.Windows.Forms.Label();
            this.labelMeanHrUnit2 = new System.Windows.Forms.Label();
            this.labelMeanHrValue2 = new System.Windows.Forms.Label();
            this.labelMeanHrText2 = new System.Windows.Forms.Label();
            this.labelMinHrUnit2 = new System.Windows.Forms.Label();
            this.labelMinHrValue2 = new System.Windows.Forms.Label();
            this.labelMinHrText2 = new System.Windows.Forms.Label();
            this.panel57 = new System.Windows.Forms.Panel();
            this.labelLeadSample2 = new System.Windows.Forms.Label();
            this.panelSample2Header = new System.Windows.Forms.Panel();
            this.labelDateSampleNr2 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.labelTimeSampleNr2 = new System.Windows.Forms.Label();
            this.panel90 = new System.Windows.Forms.Panel();
            this.label34 = new System.Windows.Forms.Label();
            this.panel89 = new System.Windows.Forms.Panel();
            this.labelEvent2NrSample = new System.Windows.Forms.Label();
            this.panel88 = new System.Windows.Forms.Panel();
            this.labelSample = new System.Windows.Forms.Label();
            this.panel87 = new System.Windows.Forms.Panel();
            this.label30 = new System.Windows.Forms.Label();
            this.panel86 = new System.Windows.Forms.Panel();
            this.labelEvent2NrRes = new System.Windows.Forms.Label();
            this.panel85 = new System.Windows.Forms.Panel();
            this.labelEvenNrSample2 = new System.Windows.Forms.Label();
            this.panel84 = new System.Windows.Forms.Panel();
            this.panel150 = new System.Windows.Forms.Panel();
            this.panel149 = new System.Windows.Forms.Panel();
            this.panel83 = new System.Windows.Forms.Panel();
            this.labelSweepFullSample1 = new System.Windows.Forms.Label();
            this.panel82 = new System.Windows.Forms.Panel();
            this.labelZoomFullSample1 = new System.Windows.Forms.Label();
            this.panel81 = new System.Windows.Forms.Panel();
            this.labelEndFullSample1 = new System.Windows.Forms.Label();
            this.panel80 = new System.Windows.Forms.Panel();
            this.labelMiddleFullSample1 = new System.Windows.Forms.Label();
            this.panel79 = new System.Windows.Forms.Panel();
            this.labelStartFullSample1 = new System.Windows.Forms.Label();
            this.panelFullStrip = new System.Windows.Forms.Panel();
            this.panel44 = new System.Windows.Forms.Panel();
            this.pictureBoxStrip1 = new System.Windows.Forms.PictureBox();
            this.panel40 = new System.Windows.Forms.Panel();
            this.panel39 = new System.Windows.Forms.Panel();
            this.panel147 = new System.Windows.Forms.Panel();
            this.panel38 = new System.Windows.Forms.Panel();
            this.labelLeadFullStripSample1 = new System.Windows.Forms.Label();
            this.panel146 = new System.Windows.Forms.Panel();
            this.panel232 = new System.Windows.Forms.Panel();
            this.labelSweepSample1 = new System.Windows.Forms.Label();
            this.panel234 = new System.Windows.Forms.Panel();
            this.labelZoomSample1 = new System.Windows.Forms.Label();
            this.panel233 = new System.Windows.Forms.Panel();
            this.labelEndSample1 = new System.Windows.Forms.Label();
            this.panel230 = new System.Windows.Forms.Panel();
            this.labelMiddleSample1 = new System.Windows.Forms.Label();
            this.panel145 = new System.Windows.Forms.Panel();
            this.labelStartSample1 = new System.Windows.Forms.Label();
            this.panelSample1STrip = new System.Windows.Forms.Panel();
            this.panel37 = new System.Windows.Forms.Panel();
            this.pictureBoxZoom1 = new System.Windows.Forms.PictureBox();
            this.panel143 = new System.Windows.Forms.Panel();
            this.panel229 = new System.Windows.Forms.Panel();
            this.panel144 = new System.Windows.Forms.Panel();
            this.labelQtUnit1 = new System.Windows.Forms.Label();
            this.labelQtValue1 = new System.Windows.Forms.Label();
            this.labelQtText1 = new System.Windows.Forms.Label();
            this.labelQrsUnit1 = new System.Windows.Forms.Label();
            this.labelQrsValue1 = new System.Windows.Forms.Label();
            this.labelQrsText1 = new System.Windows.Forms.Label();
            this.labelPrUnit1 = new System.Windows.Forms.Label();
            this.labelPrValue1 = new System.Windows.Forms.Label();
            this.labelPrText1 = new System.Windows.Forms.Label();
            this.labelMaxHrUnit1 = new System.Windows.Forms.Label();
            this.labelMaxHrValue1 = new System.Windows.Forms.Label();
            this.labelMaxHrText1 = new System.Windows.Forms.Label();
            this.labelMeanHrUnit1 = new System.Windows.Forms.Label();
            this.labelMeanHrValue1 = new System.Windows.Forms.Label();
            this.labelMeanHrText1 = new System.Windows.Forms.Label();
            this.labelMinHrUnit1 = new System.Windows.Forms.Label();
            this.labelMinHrValue1 = new System.Windows.Forms.Label();
            this.labelMinHrText1 = new System.Windows.Forms.Label();
            this.panel55 = new System.Windows.Forms.Panel();
            this.labelLeadZoomStrip1 = new System.Windows.Forms.Label();
            this.panelLineUnderSampl1 = new System.Windows.Forms.Panel();
            this.panel142 = new System.Windows.Forms.Panel();
            this.labelEvent1Date = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.labelEvent1Time = new System.Windows.Forms.Label();
            this.panel222 = new System.Windows.Forms.Panel();
            this.labelEvent1NrSample = new System.Windows.Forms.Label();
            this.panel221 = new System.Windows.Forms.Panel();
            this.labelSample1Nr = new System.Windows.Forms.Label();
            this.panel220 = new System.Windows.Forms.Panel();
            this.label98 = new System.Windows.Forms.Label();
            this.panel219 = new System.Windows.Forms.Panel();
            this.labelEvent1NrRes = new System.Windows.Forms.Label();
            this.panel218 = new System.Windows.Forms.Panel();
            this.labelEvent1Nr = new System.Windows.Forms.Label();
            this.labelFindingsRythm = new System.Windows.Forms.Label();
            this.labelFindingsClasification = new System.Windows.Forms.Label();
            this.panel217 = new System.Windows.Forms.Panel();
            this.panelEventStats = new System.Windows.Forms.Panel();
            this.panel162 = new System.Windows.Forms.Panel();
            this.panel215 = new System.Windows.Forms.Panel();
            this.panel216 = new System.Windows.Forms.Panel();
            this.panel213 = new System.Windows.Forms.Panel();
            this.labelTechnicianMeasurement = new System.Windows.Forms.Label();
            this.panel214 = new System.Windows.Forms.Panel();
            this.labelTechMeasure = new System.Windows.Forms.Label();
            this.panel209 = new System.Windows.Forms.Panel();
            this.labelQTmeasureSI = new System.Windows.Forms.Label();
            this.panel205 = new System.Windows.Forms.Panel();
            this.labelQTAvg = new System.Windows.Forms.Label();
            this.panel206 = new System.Windows.Forms.Panel();
            this.labelQTMax = new System.Windows.Forms.Label();
            this.panel207 = new System.Windows.Forms.Panel();
            this.labelQTmin = new System.Windows.Forms.Label();
            this.panel208 = new System.Windows.Forms.Panel();
            this.labelQTMeasure = new System.Windows.Forms.Label();
            this.panel203 = new System.Windows.Forms.Panel();
            this.labelQRSmeasureSI = new System.Windows.Forms.Label();
            this.panel199 = new System.Windows.Forms.Panel();
            this.labelQRSAvg = new System.Windows.Forms.Label();
            this.panel200 = new System.Windows.Forms.Panel();
            this.labelQRSMax = new System.Windows.Forms.Label();
            this.panel201 = new System.Windows.Forms.Panel();
            this.labelQRSmin = new System.Windows.Forms.Label();
            this.panel202 = new System.Windows.Forms.Panel();
            this.labelQRSMeasure = new System.Windows.Forms.Label();
            this.panel197 = new System.Windows.Forms.Panel();
            this.labelPRmeasureSI = new System.Windows.Forms.Label();
            this.panel193 = new System.Windows.Forms.Panel();
            this.labelPRAvg = new System.Windows.Forms.Label();
            this.panel194 = new System.Windows.Forms.Panel();
            this.labelPRMax = new System.Windows.Forms.Label();
            this.panel195 = new System.Windows.Forms.Panel();
            this.labelPRmin = new System.Windows.Forms.Label();
            this.panel196 = new System.Windows.Forms.Panel();
            this.labelPRMeasure = new System.Windows.Forms.Label();
            this.panel191 = new System.Windows.Forms.Panel();
            this.labelRatemeasureSI = new System.Windows.Forms.Label();
            this.panel187 = new System.Windows.Forms.Panel();
            this.labelRateAVG = new System.Windows.Forms.Label();
            this.panel188 = new System.Windows.Forms.Panel();
            this.labelRateMax = new System.Windows.Forms.Label();
            this.panel189 = new System.Windows.Forms.Panel();
            this.labelRateMin = new System.Windows.Forms.Label();
            this.panel190 = new System.Windows.Forms.Panel();
            this.labelRateMeasure = new System.Windows.Forms.Label();
            this.panel122 = new System.Windows.Forms.Panel();
            this.panel186 = new System.Windows.Forms.Panel();
            this.panel185 = new System.Windows.Forms.Panel();
            this.labelAVGMeasurement = new System.Windows.Forms.Label();
            this.panel184 = new System.Windows.Forms.Panel();
            this.labelMaxMeasurement = new System.Windows.Forms.Label();
            this.panel183 = new System.Windows.Forms.Panel();
            this.labelMinMeasurement = new System.Windows.Forms.Label();
            this.panel182 = new System.Windows.Forms.Panel();
            this.panel161 = new System.Windows.Forms.Panel();
            this.panel160 = new System.Windows.Forms.Panel();
            this.panel28 = new System.Windows.Forms.Panel();
            this.labelFindingsRemark = new System.Windows.Forms.Label();
            this.panel45 = new System.Windows.Forms.Panel();
            this.panel46 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel178 = new System.Windows.Forms.Panel();
            this.panel176 = new System.Windows.Forms.Panel();
            this.labelActivationType = new System.Windows.Forms.Label();
            this.panel177 = new System.Windows.Forms.Panel();
            this.labelActivationTypeHeader = new System.Windows.Forms.Label();
            this.panel175 = new System.Windows.Forms.Panel();
            this.panel173 = new System.Windows.Forms.Panel();
            this.labelActivity = new System.Windows.Forms.Label();
            this.panel174 = new System.Windows.Forms.Panel();
            this.labelActivityHeader = new System.Windows.Forms.Label();
            this.panel172 = new System.Windows.Forms.Panel();
            this.panel170 = new System.Windows.Forms.Panel();
            this.labelSymptoms = new System.Windows.Forms.Label();
            this.panel171 = new System.Windows.Forms.Panel();
            this.labelSymptomsHeader = new System.Windows.Forms.Label();
            this.panel167 = new System.Windows.Forms.Panel();
            this.panel169 = new System.Windows.Forms.Panel();
            this.labelICDcode = new System.Windows.Forms.Label();
            this.panel168 = new System.Windows.Forms.Panel();
            this.labelDiagnosisHeader = new System.Windows.Forms.Label();
            this.panel121 = new System.Windows.Forms.Panel();
            this.panelCurrentEventStats = new System.Windows.Forms.Panel();
            this.labelRecordNr = new System.Windows.Forms.Label();
            this.labelRecordNrText = new System.Windows.Forms.Label();
            this.panel163 = new System.Windows.Forms.Panel();
            this.labelMeasurement = new System.Windows.Forms.Label();
            this.panel164 = new System.Windows.Forms.Panel();
            this.panel165 = new System.Windows.Forms.Panel();
            this.labelCurrentEvent = new System.Windows.Forms.Label();
            this.panel166 = new System.Windows.Forms.Panel();
            this.panel120 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panelPreviousTransm = new System.Windows.Forms.Panel();
            this.panelTablePrevious3Trans = new System.Windows.Forms.Panel();
            this.panel159 = new System.Windows.Forms.Panel();
            this.dataGridViewPrevThreeTX = new System.Windows.Forms.DataGridView();
            this.ColumnEventID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnEventDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnTimeOfEvent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnSymptoms = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnFindings = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel75 = new System.Windows.Forms.Panel();
            this.panel72 = new System.Windows.Forms.Panel();
            this.panel71 = new System.Windows.Forms.Panel();
            this.labelPreviousTransmissions = new System.Windows.Forms.Label();
            this.panel73 = new System.Windows.Forms.Panel();
            this.panelECGSweepAmpIndicator = new System.Windows.Forms.Panel();
            this.labelEventMiddle = new System.Windows.Forms.Label();
            this.labelEventStart = new System.Windows.Forms.Label();
            this.panelSweepSpeed = new System.Windows.Forms.Panel();
            this.labelBaseSweetSpeed = new System.Windows.Forms.Label();
            this.panelAmpPanel = new System.Windows.Forms.Panel();
            this.labelBaseAmplitudeSet = new System.Windows.Forms.Label();
            this.labelEventEnd = new System.Windows.Forms.Label();
            this.panelECGBaselineSTrip = new System.Windows.Forms.Panel();
            this.panel158 = new System.Windows.Forms.Panel();
            this.pictureBoxBaselineReferenceECGStrip = new System.Windows.Forms.PictureBox();
            this.panel157 = new System.Windows.Forms.Panel();
            this.panel70 = new System.Windows.Forms.Panel();
            this.panelTimeBarBaselineECG = new System.Windows.Forms.Panel();
            this.panelTimeHeadingsPerStrip = new System.Windows.Forms.Panel();
            this.labelQTSI = new System.Windows.Forms.Label();
            this.panel151 = new System.Windows.Forms.Panel();
            this.labelQT = new System.Windows.Forms.Label();
            this.panel141 = new System.Windows.Forms.Panel();
            this.labelQTHeader = new System.Windows.Forms.Label();
            this.panel140 = new System.Windows.Forms.Panel();
            this.labelQRSSI = new System.Windows.Forms.Label();
            this.panel139 = new System.Windows.Forms.Panel();
            this.labelQRS = new System.Windows.Forms.Label();
            this.panel138 = new System.Windows.Forms.Panel();
            this.labelQRSHeader = new System.Windows.Forms.Label();
            this.panel137 = new System.Windows.Forms.Panel();
            this.panel136 = new System.Windows.Forms.Panel();
            this.labelPRSI = new System.Windows.Forms.Label();
            this.panel135 = new System.Windows.Forms.Panel();
            this.labelPR = new System.Windows.Forms.Label();
            this.panel134 = new System.Windows.Forms.Panel();
            this.labelPRHeader = new System.Windows.Forms.Label();
            this.panel133 = new System.Windows.Forms.Panel();
            this.labelMaxRateSI = new System.Windows.Forms.Label();
            this.panel69 = new System.Windows.Forms.Panel();
            this.labelMaxRate = new System.Windows.Forms.Label();
            this.panel132 = new System.Windows.Forms.Panel();
            this.labelMaxRateHeader = new System.Windows.Forms.Label();
            this.panel131 = new System.Windows.Forms.Panel();
            this.labelMeanRateSI = new System.Windows.Forms.Label();
            this.panel130 = new System.Windows.Forms.Panel();
            this.labelMeanRate = new System.Windows.Forms.Label();
            this.panel129 = new System.Windows.Forms.Panel();
            this.labelMeanRateHeader = new System.Windows.Forms.Label();
            this.panel128 = new System.Windows.Forms.Panel();
            this.labelMinRateSI = new System.Windows.Forms.Label();
            this.panel127 = new System.Windows.Forms.Panel();
            this.labelMinRate = new System.Windows.Forms.Label();
            this.panel126 = new System.Windows.Forms.Panel();
            this.labelMinRateHeader = new System.Windows.Forms.Label();
            this.panel125 = new System.Windows.Forms.Panel();
            this.labelLeadID = new System.Windows.Forms.Label();
            this.panel124 = new System.Windows.Forms.Panel();
            this.panelBaseLineHeader = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.panel123 = new System.Windows.Forms.Panel();
            this.labelFindingsRythmBL = new System.Windows.Forms.Label();
            this.labelBaselineDate = new System.Windows.Forms.Label();
            this.label134 = new System.Windows.Forms.Label();
            this.labelBaselineTime = new System.Windows.Forms.Label();
            this.labelBaseRefHeader = new System.Windows.Forms.Label();
            this.panel67 = new System.Windows.Forms.Panel();
            this.panel259 = new System.Windows.Forms.Panel();
            this.panel258 = new System.Windows.Forms.Panel();
            this.panel256 = new System.Windows.Forms.Panel();
            this.labelClient = new System.Windows.Forms.Label();
            this.panel264 = new System.Windows.Forms.Panel();
            this.labelClientHeader = new System.Windows.Forms.Label();
            this.panel254 = new System.Windows.Forms.Panel();
            this.panelVert9 = new System.Windows.Forms.Panel();
            this.panel252 = new System.Windows.Forms.Panel();
            this.labelRefPhysician = new System.Windows.Forms.Label();
            this.panel262 = new System.Windows.Forms.Panel();
            this.labelRefPhysHeader = new System.Windows.Forms.Label();
            this.panel250 = new System.Windows.Forms.Panel();
            this.panelVert8 = new System.Windows.Forms.Panel();
            this.panel244 = new System.Windows.Forms.Panel();
            this.labelPhysician = new System.Windows.Forms.Label();
            this.panel260 = new System.Windows.Forms.Panel();
            this.labelPhysHeader = new System.Windows.Forms.Label();
            this.panel242 = new System.Windows.Forms.Panel();
            this.panelVert7 = new System.Windows.Forms.Panel();
            this.panel240 = new System.Windows.Forms.Panel();
            this.panel248 = new System.Windows.Forms.Panel();
            this.labelSerialNr = new System.Windows.Forms.Label();
            this.panel247 = new System.Windows.Forms.Panel();
            this.labelSerialNrHeader = new System.Windows.Forms.Label();
            this.panel239 = new System.Windows.Forms.Panel();
            this.panelVert6 = new System.Windows.Forms.Panel();
            this.panel237 = new System.Windows.Forms.Panel();
            this.panel236 = new System.Windows.Forms.Panel();
            this.panel246 = new System.Windows.Forms.Panel();
            this.labelStartDate = new System.Windows.Forms.Label();
            this.panel245 = new System.Windows.Forms.Panel();
            this.labelStartDateHeader = new System.Windows.Forms.Label();
            this.panel235 = new System.Windows.Forms.Panel();
            this.panel231 = new System.Windows.Forms.Panel();
            this.panelSecPatBar = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panelHorSepPatDet = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel49 = new System.Windows.Forms.Panel();
            this.panel42 = new System.Windows.Forms.Panel();
            this.panel43 = new System.Windows.Forms.Panel();
            this.panel33 = new System.Windows.Forms.Panel();
            this.labelStudyNr = new System.Windows.Forms.Label();
            this.panel26 = new System.Windows.Forms.Panel();
            this.labelStudyNrHeader = new System.Windows.Forms.Label();
            this.panel48 = new System.Windows.Forms.Panel();
            this.panelVert5 = new System.Windows.Forms.Panel();
            this.panel36 = new System.Windows.Forms.Panel();
            this.panel32 = new System.Windows.Forms.Panel();
            this.labelPatID = new System.Windows.Forms.Label();
            this.panel25 = new System.Windows.Forms.Panel();
            this.labelPatIDHeader = new System.Windows.Forms.Label();
            this.panel41 = new System.Windows.Forms.Panel();
            this.panelVert4 = new System.Windows.Forms.Panel();
            this.panel29 = new System.Windows.Forms.Panel();
            this.panel31 = new System.Windows.Forms.Panel();
            this.labelPhone2 = new System.Windows.Forms.Label();
            this.panel30 = new System.Windows.Forms.Panel();
            this.labelPhone1 = new System.Windows.Forms.Label();
            this.panel24 = new System.Windows.Forms.Panel();
            this.labelPhoneHeader = new System.Windows.Forms.Label();
            this.panel34 = new System.Windows.Forms.Panel();
            this.panelVert3 = new System.Windows.Forms.Panel();
            this.panel22 = new System.Windows.Forms.Panel();
            this.panel23 = new System.Windows.Forms.Panel();
            this.labelZipCity = new System.Windows.Forms.Label();
            this.panel19 = new System.Windows.Forms.Panel();
            this.labelAddress = new System.Windows.Forms.Label();
            this.panel18 = new System.Windows.Forms.Panel();
            this.labelAddressHeader = new System.Windows.Forms.Label();
            this.panel27 = new System.Windows.Forms.Panel();
            this.panelVert2 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.labelAgeGender = new System.Windows.Forms.Label();
            this.panel16 = new System.Windows.Forms.Panel();
            this.labelDateOfBirth = new System.Windows.Forms.Label();
            this.panel15 = new System.Windows.Forms.Panel();
            this.labelDOBheader = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panelVert1 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.labelPatientFirstName = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.labelPatientLastName = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.labelPatientNameHeader = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panelDateTimeofEventStrip = new System.Windows.Forms.Panel();
            this.panel52 = new System.Windows.Forms.Panel();
            this.panel53 = new System.Windows.Forms.Panel();
            this.panel66 = new System.Windows.Forms.Panel();
            this.labelSingleEventReportTime = new System.Windows.Forms.Label();
            this.panel65 = new System.Windows.Forms.Panel();
            this.labelSingleEventReportDate = new System.Windows.Forms.Label();
            this.panel54 = new System.Windows.Forms.Panel();
            this.panelCardiacEVentReportNumber = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel50 = new System.Windows.Forms.Panel();
            this.labelReportNr = new System.Windows.Forms.Label();
            this.panel47 = new System.Windows.Forms.Panel();
            this.labelCardiacEventReportNr = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panelPrintHeader = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.panel56 = new System.Windows.Forms.Panel();
            this.panel58 = new System.Windows.Forms.Panel();
            this.labelCenter2 = new System.Windows.Forms.Label();
            this.panel59 = new System.Windows.Forms.Panel();
            this.labelCenter1 = new System.Windows.Forms.Label();
            this.panel60 = new System.Windows.Forms.Panel();
            this.panel61 = new System.Windows.Forms.Panel();
            this.pictureBoxCenter = new System.Windows.Forms.PictureBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripGenPages = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonPrint = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripClipboard1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripClipboard2 = new System.Windows.Forms.ToolStripButton();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel21 = new System.Windows.Forms.Panel();
            this.panelPrintArea3 = new System.Windows.Forms.Panel();
            this.panelPage2Footer = new System.Windows.Forms.Panel();
            this.label57 = new System.Windows.Forms.Label();
            this.labelPage3 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.labelPagexOf3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelPrintDate3Bottom = new System.Windows.Forms.Label();
            this.labelPrintDate = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel500 = new System.Windows.Forms.Panel();
            this.panel506 = new System.Windows.Forms.Panel();
            this.pictureBoxCenter2 = new System.Windows.Forms.PictureBox();
            this.panel501 = new System.Windows.Forms.Panel();
            this.panelPage2Header = new System.Windows.Forms.Panel();
            this.panel499 = new System.Windows.Forms.Panel();
            this.panel495 = new System.Windows.Forms.Panel();
            this.panel497 = new System.Windows.Forms.Panel();
            this.labelPatientID3 = new System.Windows.Forms.Label();
            this.panel519 = new System.Windows.Forms.Panel();
            this.labelPatientIDResult3 = new System.Windows.Forms.Label();
            this.panel520 = new System.Windows.Forms.Panel();
            this.panel521 = new System.Windows.Forms.Panel();
            this.panel522 = new System.Windows.Forms.Panel();
            this.panel523 = new System.Windows.Forms.Panel();
            this.labelPatName3 = new System.Windows.Forms.Label();
            this.panel524 = new System.Windows.Forms.Panel();
            this.labelPatLastNam3 = new System.Windows.Forms.Label();
            this.panel525 = new System.Windows.Forms.Panel();
            this.panel526 = new System.Windows.Forms.Panel();
            this.panel527 = new System.Windows.Forms.Panel();
            this.panel528 = new System.Windows.Forms.Panel();
            this.labelDateOfBirthPage3 = new System.Windows.Forms.Label();
            this.panel529 = new System.Windows.Forms.Panel();
            this.labelDateOfBirthResPage3 = new System.Windows.Forms.Label();
            this.panel530 = new System.Windows.Forms.Panel();
            this.panel531 = new System.Windows.Forms.Panel();
            this.panel532 = new System.Windows.Forms.Panel();
            this.panel533 = new System.Windows.Forms.Panel();
            this.labelStudyNrPage3 = new System.Windows.Forms.Label();
            this.panel534 = new System.Windows.Forms.Panel();
            this.labelStudyNumberResPage3 = new System.Windows.Forms.Label();
            this.panel76 = new System.Windows.Forms.Panel();
            this.panel74 = new System.Windows.Forms.Panel();
            this.labelReportText3 = new System.Windows.Forms.Label();
            this.labelPage3ReportNr = new System.Windows.Forms.Label();
            this.panel450 = new System.Windows.Forms.Panel();
            this.panel448 = new System.Windows.Forms.Panel();
            this.panel449 = new System.Windows.Forms.Panel();
            this.panel416 = new System.Windows.Forms.Panel();
            this.panel418 = new System.Windows.Forms.Panel();
            this.panel315 = new System.Windows.Forms.Panel();
            this.panel316 = new System.Windows.Forms.Panel();
            this.panel417 = new System.Windows.Forms.Panel();
            this.panel391 = new System.Windows.Forms.Panel();
            this.panel387 = new System.Windows.Forms.Panel();
            this.panel390 = new System.Windows.Forms.Panel();
            this.panel389 = new System.Windows.Forms.Panel();
            this.panel388 = new System.Windows.Forms.Panel();
            this.pictureBoxSample3 = new System.Windows.Forms.PictureBox();
            this.panel384 = new System.Windows.Forms.Panel();
            this.panel300 = new System.Windows.Forms.Panel();
            this.panel283 = new System.Windows.Forms.Panel();
            this.panel286 = new System.Windows.Forms.Panel();
            this.panel285 = new System.Windows.Forms.Panel();
            this.panel284 = new System.Windows.Forms.Panel();
            this.pictureBoxSample3Full = new System.Windows.Forms.PictureBox();
            this.panel277 = new System.Windows.Forms.Panel();
            this.panel280 = new System.Windows.Forms.Panel();
            this.labelEndPageTime = new System.Windows.Forms.Label();
            this.panelEventSample3 = new System.Windows.Forms.Panel();
            this.labelSample3Lead = new System.Windows.Forms.Label();
            this.labelSample3StartFull = new System.Windows.Forms.Label();
            this.labelSample3End = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labelSample3SweepFull = new System.Windows.Forms.Label();
            this.panel35 = new System.Windows.Forms.Panel();
            this.panel62 = new System.Windows.Forms.Panel();
            this.panel63 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel64 = new System.Windows.Forms.Panel();
            this.panel68 = new System.Windows.Forms.Panel();
            this.panel91 = new System.Windows.Forms.Panel();
            this.panel92 = new System.Windows.Forms.Panel();
            this.panel93 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel94 = new System.Windows.Forms.Panel();
            this.panel111 = new System.Windows.Forms.Panel();
            this.panel112 = new System.Windows.Forms.Panel();
            this.panel113 = new System.Windows.Forms.Panel();
            this.panel114 = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.panel115 = new System.Windows.Forms.Panel();
            this.panel116 = new System.Windows.Forms.Panel();
            this.panel117 = new System.Windows.Forms.Panel();
            this.panel118 = new System.Windows.Forms.Panel();
            this.panel119 = new System.Windows.Forms.Panel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.panel148 = new System.Windows.Forms.Panel();
            this.panel152 = new System.Windows.Forms.Panel();
            this.panel153 = new System.Windows.Forms.Panel();
            this.panel154 = new System.Windows.Forms.Panel();
            this.panel155 = new System.Windows.Forms.Panel();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.panel156 = new System.Windows.Forms.Panel();
            this.panel179 = new System.Windows.Forms.Panel();
            this.panel180 = new System.Windows.Forms.Panel();
            this.panel181 = new System.Windows.Forms.Panel();
            this.panel192 = new System.Windows.Forms.Panel();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.panel198 = new System.Windows.Forms.Panel();
            this.panel204 = new System.Windows.Forms.Panel();
            this.panel210 = new System.Windows.Forms.Panel();
            this.panel211 = new System.Windows.Forms.Panel();
            this.panel212 = new System.Windows.Forms.Panel();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.panel223 = new System.Windows.Forms.Panel();
            this.panel224 = new System.Windows.Forms.Panel();
            this.panel225 = new System.Windows.Forms.Panel();
            this.panel226 = new System.Windows.Forms.Panel();
            this.panel227 = new System.Windows.Forms.Panel();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.panel228 = new System.Windows.Forms.Panel();
            this.panel238 = new System.Windows.Forms.Panel();
            this.panel241 = new System.Windows.Forms.Panel();
            this.panel243 = new System.Windows.Forms.Panel();
            this.panel249 = new System.Windows.Forms.Panel();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.panel251 = new System.Windows.Forms.Panel();
            this.panel253 = new System.Windows.Forms.Panel();
            this.panel255 = new System.Windows.Forms.Panel();
            this.panel257 = new System.Windows.Forms.Panel();
            this.panel261 = new System.Windows.Forms.Panel();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.panel263 = new System.Windows.Forms.Panel();
            this.panel265 = new System.Windows.Forms.Panel();
            this.panel266 = new System.Windows.Forms.Panel();
            this.panel267 = new System.Windows.Forms.Panel();
            this.panel268 = new System.Windows.Forms.Panel();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.panel269 = new System.Windows.Forms.Panel();
            this.panel270 = new System.Windows.Forms.Panel();
            this.panel271 = new System.Windows.Forms.Panel();
            this.panel272 = new System.Windows.Forms.Panel();
            this.panel273 = new System.Windows.Forms.Panel();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.panel274 = new System.Windows.Forms.Panel();
            this.panel275 = new System.Windows.Forms.Panel();
            this.panel276 = new System.Windows.Forms.Panel();
            this.panel281 = new System.Windows.Forms.Panel();
            this.panel282 = new System.Windows.Forms.Panel();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.panel287 = new System.Windows.Forms.Panel();
            this.panel288 = new System.Windows.Forms.Panel();
            this.panel289 = new System.Windows.Forms.Panel();
            this.panel290 = new System.Windows.Forms.Panel();
            this.panel291 = new System.Windows.Forms.Panel();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.panel292 = new System.Windows.Forms.Panel();
            this.panel293 = new System.Windows.Forms.Panel();
            this.panel294 = new System.Windows.Forms.Panel();
            this.panel295 = new System.Windows.Forms.Panel();
            this.panel296 = new System.Windows.Forms.Panel();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.panel297 = new System.Windows.Forms.Panel();
            this.panel298 = new System.Windows.Forms.Panel();
            this.panel299 = new System.Windows.Forms.Panel();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label116 = new System.Windows.Forms.Label();
            this.labelStartPageTime = new System.Windows.Forms.Label();
            this.panel301 = new System.Windows.Forms.Panel();
            this.labelStartPageDate = new System.Windows.Forms.Label();
            this.labelSample3AmplFull = new System.Windows.Forms.Label();
            this.panel278 = new System.Windows.Forms.Panel();
            this.panel279 = new System.Windows.Forms.Panel();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.panel302 = new System.Windows.Forms.Panel();
            this.panel303 = new System.Windows.Forms.Panel();
            this.label119 = new System.Windows.Forms.Label();
            this.label120 = new System.Windows.Forms.Label();
            this.label121 = new System.Windows.Forms.Label();
            this.label122 = new System.Windows.Forms.Label();
            this.label123 = new System.Windows.Forms.Label();
            this.label124 = new System.Windows.Forms.Label();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.pictureBox22 = new System.Windows.Forms.PictureBox();
            this.pictureBox23 = new System.Windows.Forms.PictureBox();
            this.pictureBox24 = new System.Windows.Forms.PictureBox();
            this.pictureBox25 = new System.Windows.Forms.PictureBox();
            this.pictureBox26 = new System.Windows.Forms.PictureBox();
            this.pictureBox27 = new System.Windows.Forms.PictureBox();
            this.pictureBox28 = new System.Windows.Forms.PictureBox();
            this.pictureBox29 = new System.Windows.Forms.PictureBox();
            this.pictureBox30 = new System.Windows.Forms.PictureBox();
            this.pictureBox31 = new System.Windows.Forms.PictureBox();
            this.pictureBox32 = new System.Windows.Forms.PictureBox();
            this.pictureBox33 = new System.Windows.Forms.PictureBox();
            this.panelPrintArea1.SuspendLayout();
            this.panel51.SuspendLayout();
            this.panel510.SuspendLayout();
            this.panel509.SuspendLayout();
            this.panel508.SuspendLayout();
            this.panel507.SuspendLayout();
            this.panel78.SuspendLayout();
            this.panel110.SuspendLayout();
            this.panel109.SuspendLayout();
            this.panel108.SuspendLayout();
            this.panel107.SuspendLayout();
            this.panel106.SuspendLayout();
            this.panel105.SuspendLayout();
            this.panelSample2Time.SuspendLayout();
            this.panel102.SuspendLayout();
            this.panel101.SuspendLayout();
            this.panel100.SuspendLayout();
            this.panel99.SuspendLayout();
            this.panel98.SuspendLayout();
            this.panelSeceondStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxZoom2)).BeginInit();
            this.panelSample2Lead.SuspendLayout();
            this.panel95.SuspendLayout();
            this.panelSample2Header.SuspendLayout();
            this.panel90.SuspendLayout();
            this.panel89.SuspendLayout();
            this.panel88.SuspendLayout();
            this.panel87.SuspendLayout();
            this.panel86.SuspendLayout();
            this.panel85.SuspendLayout();
            this.panel149.SuspendLayout();
            this.panel83.SuspendLayout();
            this.panel82.SuspendLayout();
            this.panel81.SuspendLayout();
            this.panel80.SuspendLayout();
            this.panel79.SuspendLayout();
            this.panelFullStrip.SuspendLayout();
            this.panel44.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip1)).BeginInit();
            this.panel147.SuspendLayout();
            this.panel38.SuspendLayout();
            this.panel146.SuspendLayout();
            this.panel232.SuspendLayout();
            this.panel234.SuspendLayout();
            this.panel233.SuspendLayout();
            this.panel230.SuspendLayout();
            this.panel145.SuspendLayout();
            this.panelSample1STrip.SuspendLayout();
            this.panel37.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxZoom1)).BeginInit();
            this.panel144.SuspendLayout();
            this.panel142.SuspendLayout();
            this.panel222.SuspendLayout();
            this.panel221.SuspendLayout();
            this.panel220.SuspendLayout();
            this.panel219.SuspendLayout();
            this.panel218.SuspendLayout();
            this.panelEventStats.SuspendLayout();
            this.panel162.SuspendLayout();
            this.panel215.SuspendLayout();
            this.panel213.SuspendLayout();
            this.panel214.SuspendLayout();
            this.panel209.SuspendLayout();
            this.panel205.SuspendLayout();
            this.panel206.SuspendLayout();
            this.panel207.SuspendLayout();
            this.panel208.SuspendLayout();
            this.panel203.SuspendLayout();
            this.panel199.SuspendLayout();
            this.panel200.SuspendLayout();
            this.panel201.SuspendLayout();
            this.panel202.SuspendLayout();
            this.panel197.SuspendLayout();
            this.panel193.SuspendLayout();
            this.panel194.SuspendLayout();
            this.panel195.SuspendLayout();
            this.panel196.SuspendLayout();
            this.panel191.SuspendLayout();
            this.panel187.SuspendLayout();
            this.panel188.SuspendLayout();
            this.panel189.SuspendLayout();
            this.panel190.SuspendLayout();
            this.panel122.SuspendLayout();
            this.panel185.SuspendLayout();
            this.panel184.SuspendLayout();
            this.panel183.SuspendLayout();
            this.panel160.SuspendLayout();
            this.panel28.SuspendLayout();
            this.panel45.SuspendLayout();
            this.panel46.SuspendLayout();
            this.panel178.SuspendLayout();
            this.panel176.SuspendLayout();
            this.panel177.SuspendLayout();
            this.panel175.SuspendLayout();
            this.panel173.SuspendLayout();
            this.panel174.SuspendLayout();
            this.panel172.SuspendLayout();
            this.panel170.SuspendLayout();
            this.panel171.SuspendLayout();
            this.panel167.SuspendLayout();
            this.panel169.SuspendLayout();
            this.panel168.SuspendLayout();
            this.panelCurrentEventStats.SuspendLayout();
            this.panel163.SuspendLayout();
            this.panel165.SuspendLayout();
            this.panelPreviousTransm.SuspendLayout();
            this.panelTablePrevious3Trans.SuspendLayout();
            this.panel159.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPrevThreeTX)).BeginInit();
            this.panel71.SuspendLayout();
            this.panelECGSweepAmpIndicator.SuspendLayout();
            this.panelSweepSpeed.SuspendLayout();
            this.panelAmpPanel.SuspendLayout();
            this.panelECGBaselineSTrip.SuspendLayout();
            this.panel158.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBaselineReferenceECGStrip)).BeginInit();
            this.panelTimeHeadingsPerStrip.SuspendLayout();
            this.panel151.SuspendLayout();
            this.panel141.SuspendLayout();
            this.panel140.SuspendLayout();
            this.panel139.SuspendLayout();
            this.panel138.SuspendLayout();
            this.panel136.SuspendLayout();
            this.panel135.SuspendLayout();
            this.panel134.SuspendLayout();
            this.panel133.SuspendLayout();
            this.panel69.SuspendLayout();
            this.panel132.SuspendLayout();
            this.panel131.SuspendLayout();
            this.panel130.SuspendLayout();
            this.panel129.SuspendLayout();
            this.panel128.SuspendLayout();
            this.panel127.SuspendLayout();
            this.panel126.SuspendLayout();
            this.panel125.SuspendLayout();
            this.panelBaseLineHeader.SuspendLayout();
            this.panel123.SuspendLayout();
            this.panel67.SuspendLayout();
            this.panel256.SuspendLayout();
            this.panel264.SuspendLayout();
            this.panel252.SuspendLayout();
            this.panel262.SuspendLayout();
            this.panel244.SuspendLayout();
            this.panel260.SuspendLayout();
            this.panel240.SuspendLayout();
            this.panel248.SuspendLayout();
            this.panel247.SuspendLayout();
            this.panel236.SuspendLayout();
            this.panel246.SuspendLayout();
            this.panel245.SuspendLayout();
            this.panelSecPatBar.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel43.SuspendLayout();
            this.panel33.SuspendLayout();
            this.panel26.SuspendLayout();
            this.panel36.SuspendLayout();
            this.panel32.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel29.SuspendLayout();
            this.panel31.SuspendLayout();
            this.panel30.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panelDateTimeofEventStrip.SuspendLayout();
            this.panel53.SuspendLayout();
            this.panel66.SuspendLayout();
            this.panel65.SuspendLayout();
            this.panelCardiacEVentReportNumber.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel50.SuspendLayout();
            this.panel47.SuspendLayout();
            this.panelPrintHeader.SuspendLayout();
            this.panel56.SuspendLayout();
            this.panel58.SuspendLayout();
            this.panel59.SuspendLayout();
            this.panel61.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCenter)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.panelPrintArea3.SuspendLayout();
            this.panelPage2Footer.SuspendLayout();
            this.panel500.SuspendLayout();
            this.panel506.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCenter2)).BeginInit();
            this.panelPage2Header.SuspendLayout();
            this.panel495.SuspendLayout();
            this.panel497.SuspendLayout();
            this.panel519.SuspendLayout();
            this.panel523.SuspendLayout();
            this.panel524.SuspendLayout();
            this.panel528.SuspendLayout();
            this.panel529.SuspendLayout();
            this.panel533.SuspendLayout();
            this.panel534.SuspendLayout();
            this.panel448.SuspendLayout();
            this.panel416.SuspendLayout();
            this.panel418.SuspendLayout();
            this.panel391.SuspendLayout();
            this.panel387.SuspendLayout();
            this.panel390.SuspendLayout();
            this.panel388.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample3)).BeginInit();
            this.panel283.SuspendLayout();
            this.panel286.SuspendLayout();
            this.panel284.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample3Full)).BeginInit();
            this.panel277.SuspendLayout();
            this.panel280.SuspendLayout();
            this.panelEventSample3.SuspendLayout();
            this.panel62.SuspendLayout();
            this.panel63.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel68.SuspendLayout();
            this.panel92.SuspendLayout();
            this.panel93.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel111.SuspendLayout();
            this.panel113.SuspendLayout();
            this.panel114.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel116.SuspendLayout();
            this.panel118.SuspendLayout();
            this.panel119.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.panel152.SuspendLayout();
            this.panel154.SuspendLayout();
            this.panel155.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.panel179.SuspendLayout();
            this.panel181.SuspendLayout();
            this.panel192.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.panel204.SuspendLayout();
            this.panel211.SuspendLayout();
            this.panel212.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.panel224.SuspendLayout();
            this.panel226.SuspendLayout();
            this.panel227.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.panel238.SuspendLayout();
            this.panel243.SuspendLayout();
            this.panel249.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            this.panel253.SuspendLayout();
            this.panel257.SuspendLayout();
            this.panel261.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            this.panel265.SuspendLayout();
            this.panel267.SuspendLayout();
            this.panel268.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            this.panel270.SuspendLayout();
            this.panel272.SuspendLayout();
            this.panel273.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            this.panel275.SuspendLayout();
            this.panel281.SuspendLayout();
            this.panel282.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            this.panel288.SuspendLayout();
            this.panel290.SuspendLayout();
            this.panel291.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            this.panel293.SuspendLayout();
            this.panel295.SuspendLayout();
            this.panel296.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            this.panel298.SuspendLayout();
            this.panel278.SuspendLayout();
            this.panel279.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            this.panel303.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox33)).BeginInit();
            this.SuspendLayout();
            // 
            // panelPrintArea1
            // 
            this.panelPrintArea1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panelPrintArea1.Controls.Add(this.panel51);
            this.panelPrintArea1.Controls.Add(this.panel78);
            this.panelPrintArea1.Controls.Add(this.panel77);
            this.panelPrintArea1.Controls.Add(this.panelSample2Time);
            this.panelPrintArea1.Controls.Add(this.panelSeceondStrip);
            this.panelPrintArea1.Controls.Add(this.panelSample2Lead);
            this.panelPrintArea1.Controls.Add(this.panelSample2Header);
            this.panelPrintArea1.Controls.Add(this.panel150);
            this.panelPrintArea1.Controls.Add(this.panel149);
            this.panelPrintArea1.Controls.Add(this.panelFullStrip);
            this.panelPrintArea1.Controls.Add(this.panel147);
            this.panelPrintArea1.Controls.Add(this.panel146);
            this.panelPrintArea1.Controls.Add(this.panelSample1STrip);
            this.panelPrintArea1.Controls.Add(this.panel144);
            this.panelPrintArea1.Controls.Add(this.panelLineUnderSampl1);
            this.panelPrintArea1.Controls.Add(this.panel142);
            this.panelPrintArea1.Controls.Add(this.panelEventStats);
            this.panelPrintArea1.Controls.Add(this.panelCurrentEventStats);
            this.panelPrintArea1.Controls.Add(this.panel120);
            this.panelPrintArea1.Controls.Add(this.panel6);
            this.panelPrintArea1.Controls.Add(this.panelPreviousTransm);
            this.panelPrintArea1.Controls.Add(this.panel73);
            this.panelPrintArea1.Controls.Add(this.panelECGSweepAmpIndicator);
            this.panelPrintArea1.Controls.Add(this.panelECGBaselineSTrip);
            this.panelPrintArea1.Controls.Add(this.panelTimeBarBaselineECG);
            this.panelPrintArea1.Controls.Add(this.panelTimeHeadingsPerStrip);
            this.panelPrintArea1.Controls.Add(this.panelBaseLineHeader);
            this.panelPrintArea1.Controls.Add(this.panel67);
            this.panelPrintArea1.Controls.Add(this.panelSecPatBar);
            this.panelPrintArea1.Controls.Add(this.panelHorSepPatDet);
            this.panelPrintArea1.Controls.Add(this.panel5);
            this.panelPrintArea1.Controls.Add(this.panelDateTimeofEventStrip);
            this.panelPrintArea1.Controls.Add(this.panelCardiacEVentReportNumber);
            this.panelPrintArea1.Controls.Add(this.panelPrintHeader);
            this.panelPrintArea1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.panelPrintArea1.Location = new System.Drawing.Point(0, 100);
            this.panelPrintArea1.Name = "panelPrintArea1";
            this.panelPrintArea1.Size = new System.Drawing.Size(2336, 3303);
            this.panelPrintArea1.TabIndex = 0;
            // 
            // panel51
            // 
            this.panel51.Controls.Add(this.label14);
            this.panel51.Controls.Add(this.labelPrintDate1Bottom);
            this.panel51.Controls.Add(this.label8);
            this.panel51.Controls.Add(this.panel510);
            this.panel51.Controls.Add(this.panel509);
            this.panel51.Controls.Add(this.panel508);
            this.panel51.Controls.Add(this.panel507);
            this.panel51.Controls.Add(this.label7);
            this.panel51.Controls.Add(this.label3);
            this.panel51.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel51.Location = new System.Drawing.Point(0, 3266);
            this.panel51.Name = "panel51";
            this.panel51.Size = new System.Drawing.Size(2336, 37);
            this.panel51.TabIndex = 34;
            // 
            // label14
            // 
            this.label14.Dock = System.Windows.Forms.DockStyle.Left;
            this.label14.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(747, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(823, 37);
            this.label14.TabIndex = 8;
            this.label14.Text = "Copyright 2017 © Techmedic International B.V.";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label14.Click += new System.EventHandler(this.label14_Click);
            // 
            // labelPrintDate1Bottom
            // 
            this.labelPrintDate1Bottom.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPrintDate1Bottom.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPrintDate1Bottom.Location = new System.Drawing.Point(236, 0);
            this.labelPrintDate1Bottom.Name = "labelPrintDate1Bottom";
            this.labelPrintDate1Bottom.Size = new System.Drawing.Size(511, 37);
            this.labelPrintDate1Bottom.TabIndex = 7;
            this.labelPrintDate1Bottom.Text = "12/27/2016";
            this.labelPrintDate1Bottom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.Dock = System.Windows.Forms.DockStyle.Left;
            this.label8.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(45, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(191, 37);
            this.label8.TabIndex = 6;
            this.label8.Text = "Print date:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel510
            // 
            this.panel510.Controls.Add(this.labelPage);
            this.panel510.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel510.Location = new System.Drawing.Point(1881, 0);
            this.panel510.Name = "panel510";
            this.panel510.Size = new System.Drawing.Size(123, 37);
            this.panel510.TabIndex = 5;
            // 
            // labelPage
            // 
            this.labelPage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPage.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPage.Location = new System.Drawing.Point(0, 0);
            this.labelPage.Name = "labelPage";
            this.labelPage.Size = new System.Drawing.Size(123, 37);
            this.labelPage.TabIndex = 0;
            this.labelPage.Text = "Page";
            this.labelPage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel509
            // 
            this.panel509.Controls.Add(this.labelPage1of);
            this.panel509.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel509.Location = new System.Drawing.Point(2004, 0);
            this.panel509.Name = "panel509";
            this.panel509.Size = new System.Drawing.Size(74, 37);
            this.panel509.TabIndex = 4;
            // 
            // labelPage1of
            // 
            this.labelPage1of.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPage1of.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPage1of.Location = new System.Drawing.Point(0, 0);
            this.labelPage1of.Name = "labelPage1of";
            this.labelPage1of.Size = new System.Drawing.Size(74, 37);
            this.labelPage1of.TabIndex = 0;
            this.labelPage1of.Text = "1";
            this.labelPage1of.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelPage1of.Click += new System.EventHandler(this.label53_Click);
            // 
            // panel508
            // 
            this.panel508.Controls.Add(this.labelPageOf);
            this.panel508.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel508.Location = new System.Drawing.Point(2078, 0);
            this.panel508.Name = "panel508";
            this.panel508.Size = new System.Drawing.Size(82, 37);
            this.panel508.TabIndex = 3;
            // 
            // labelPageOf
            // 
            this.labelPageOf.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelPageOf.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPageOf.Location = new System.Drawing.Point(14, 0);
            this.labelPageOf.Name = "labelPageOf";
            this.labelPageOf.Size = new System.Drawing.Size(68, 37);
            this.labelPageOf.TabIndex = 0;
            this.labelPageOf.Text = "of";
            this.labelPageOf.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel507
            // 
            this.panel507.Controls.Add(this.labelPage1ofx);
            this.panel507.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel507.Location = new System.Drawing.Point(2160, 0);
            this.panel507.Name = "panel507";
            this.panel507.Size = new System.Drawing.Size(130, 37);
            this.panel507.TabIndex = 2;
            // 
            // labelPage1ofx
            // 
            this.labelPage1ofx.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPage1ofx.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPage1ofx.Location = new System.Drawing.Point(0, 0);
            this.labelPage1ofx.Name = "labelPage1ofx";
            this.labelPage1ofx.Size = new System.Drawing.Size(130, 37);
            this.labelPage1ofx.TabIndex = 1;
            this.labelPage1ofx.Text = "20p1";
            this.labelPage1ofx.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label7.Dock = System.Windows.Forms.DockStyle.Right;
            this.label7.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label7.Location = new System.Drawing.Point(2290, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 37);
            this.label7.TabIndex = 1;
            this.label7.Text = "R1";
            this.label7.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label7.Visible = false;
            // 
            // label3
            // 
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Dock = System.Windows.Forms.DockStyle.Left;
            this.label3.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 37);
            this.label3.TabIndex = 0;
            this.label3.Text = "L1";
            this.label3.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label3.Visible = false;
            // 
            // panel78
            // 
            this.panel78.Controls.Add(this.panel110);
            this.panel78.Controls.Add(this.panel109);
            this.panel78.Controls.Add(this.panel108);
            this.panel78.Controls.Add(this.panel107);
            this.panel78.Controls.Add(this.panel106);
            this.panel78.Controls.Add(this.panel105);
            this.panel78.Controls.Add(this.panel104);
            this.panel78.Controls.Add(this.panel103);
            this.panel78.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel78.Location = new System.Drawing.Point(0, 3185);
            this.panel78.Name = "panel78";
            this.panel78.Size = new System.Drawing.Size(2336, 85);
            this.panel78.TabIndex = 33;
            // 
            // panel110
            // 
            this.panel110.Controls.Add(this.labelReportSignDate);
            this.panel110.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel110.Location = new System.Drawing.Point(1960, 0);
            this.panel110.Name = "panel110";
            this.panel110.Size = new System.Drawing.Size(325, 85);
            this.panel110.TabIndex = 7;
            // 
            // labelReportSignDate
            // 
            this.labelReportSignDate.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelReportSignDate.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelReportSignDate.Location = new System.Drawing.Point(0, 0);
            this.labelReportSignDate.Name = "labelReportSignDate";
            this.labelReportSignDate.Size = new System.Drawing.Size(309, 85);
            this.labelReportSignDate.TabIndex = 0;
            this.labelReportSignDate.Text = "08/23/2016";
            this.labelReportSignDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel109
            // 
            this.panel109.Controls.Add(this.labelDateReportSign);
            this.panel109.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel109.Location = new System.Drawing.Point(1783, 0);
            this.panel109.Name = "panel109";
            this.panel109.Size = new System.Drawing.Size(177, 85);
            this.panel109.TabIndex = 6;
            // 
            // labelDateReportSign
            // 
            this.labelDateReportSign.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelDateReportSign.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDateReportSign.Location = new System.Drawing.Point(0, 0);
            this.labelDateReportSign.Name = "labelDateReportSign";
            this.labelDateReportSign.Size = new System.Drawing.Size(164, 85);
            this.labelDateReportSign.TabIndex = 1;
            this.labelDateReportSign.Text = "Date:";
            this.labelDateReportSign.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel108
            // 
            this.panel108.Controls.Add(this.labelPhysSignLine);
            this.panel108.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel108.Location = new System.Drawing.Point(1471, 0);
            this.panel108.Name = "panel108";
            this.panel108.Size = new System.Drawing.Size(312, 85);
            this.panel108.TabIndex = 5;
            // 
            // labelPhysSignLine
            // 
            this.labelPhysSignLine.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPhysSignLine.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPhysSignLine.Location = new System.Drawing.Point(0, 0);
            this.labelPhysSignLine.Name = "labelPhysSignLine";
            this.labelPhysSignLine.Size = new System.Drawing.Size(270, 85);
            this.labelPhysSignLine.TabIndex = 1;
            this.labelPhysSignLine.Text = "________";
            this.labelPhysSignLine.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelPhysSignLine.Click += new System.EventHandler(this.labelPhysSignLine_Click);
            // 
            // panel107
            // 
            this.panel107.Controls.Add(this.labelPhysSign);
            this.panel107.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel107.Location = new System.Drawing.Point(920, 0);
            this.panel107.Name = "panel107";
            this.panel107.Size = new System.Drawing.Size(551, 85);
            this.panel107.TabIndex = 4;
            // 
            // labelPhysSign
            // 
            this.labelPhysSign.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPhysSign.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPhysSign.Location = new System.Drawing.Point(0, 0);
            this.labelPhysSign.Name = "labelPhysSign";
            this.labelPhysSign.Size = new System.Drawing.Size(490, 85);
            this.labelPhysSign.TabIndex = 1;
            this.labelPhysSign.Text = "Physician signature:";
            this.labelPhysSign.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel106
            // 
            this.panel106.Controls.Add(this.labelPhysPrintName);
            this.panel106.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel106.Location = new System.Drawing.Point(520, 0);
            this.panel106.Name = "panel106";
            this.panel106.Size = new System.Drawing.Size(400, 85);
            this.panel106.TabIndex = 3;
            // 
            // labelPhysPrintName
            // 
            this.labelPhysPrintName.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPhysPrintName.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPhysPrintName.Location = new System.Drawing.Point(0, 0);
            this.labelPhysPrintName.Name = "labelPhysPrintName";
            this.labelPhysPrintName.Size = new System.Drawing.Size(360, 85);
            this.labelPhysPrintName.TabIndex = 1;
            this.labelPhysPrintName.Text = "J. Smithsonian";
            this.labelPhysPrintName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelPhysPrintName.Click += new System.EventHandler(this.labelPhysPrintName_Click);
            // 
            // panel105
            // 
            this.panel105.Controls.Add(this.labelPhysNameReportPrint);
            this.panel105.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel105.Location = new System.Drawing.Point(36, 0);
            this.panel105.Name = "panel105";
            this.panel105.Size = new System.Drawing.Size(484, 85);
            this.panel105.TabIndex = 2;
            // 
            // labelPhysNameReportPrint
            // 
            this.labelPhysNameReportPrint.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPhysNameReportPrint.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPhysNameReportPrint.Location = new System.Drawing.Point(0, 0);
            this.labelPhysNameReportPrint.Name = "labelPhysNameReportPrint";
            this.labelPhysNameReportPrint.Size = new System.Drawing.Size(434, 85);
            this.labelPhysNameReportPrint.TabIndex = 2;
            this.labelPhysNameReportPrint.Text = "Physician\'s name:";
            this.labelPhysNameReportPrint.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel104
            // 
            this.panel104.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel104.Location = new System.Drawing.Point(2314, 0);
            this.panel104.Name = "panel104";
            this.panel104.Size = new System.Drawing.Size(22, 85);
            this.panel104.TabIndex = 1;
            // 
            // panel103
            // 
            this.panel103.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel103.Location = new System.Drawing.Point(0, 0);
            this.panel103.Name = "panel103";
            this.panel103.Size = new System.Drawing.Size(36, 85);
            this.panel103.TabIndex = 0;
            // 
            // panel77
            // 
            this.panel77.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel77.Location = new System.Drawing.Point(0, 3140);
            this.panel77.Name = "panel77";
            this.panel77.Size = new System.Drawing.Size(2336, 45);
            this.panel77.TabIndex = 32;
            // 
            // panelSample2Time
            // 
            this.panelSample2Time.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelSample2Time.Controls.Add(this.panel102);
            this.panelSample2Time.Controls.Add(this.panel101);
            this.panelSample2Time.Controls.Add(this.panel100);
            this.panelSample2Time.Controls.Add(this.panel99);
            this.panelSample2Time.Controls.Add(this.panel98);
            this.panelSample2Time.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSample2Time.Location = new System.Drawing.Point(0, 3085);
            this.panelSample2Time.Name = "panelSample2Time";
            this.panelSample2Time.Size = new System.Drawing.Size(2336, 55);
            this.panelSample2Time.TabIndex = 31;
            // 
            // panel102
            // 
            this.panel102.Controls.Add(this.labelMiddleSample2);
            this.panel102.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel102.Location = new System.Drawing.Point(1023, 0);
            this.panel102.Name = "panel102";
            this.panel102.Size = new System.Drawing.Size(253, 53);
            this.panel102.TabIndex = 8;
            // 
            // labelMiddleSample2
            // 
            this.labelMiddleSample2.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelMiddleSample2.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMiddleSample2.Location = new System.Drawing.Point(0, 0);
            this.labelMiddleSample2.Name = "labelMiddleSample2";
            this.labelMiddleSample2.Size = new System.Drawing.Size(250, 53);
            this.labelMiddleSample2.TabIndex = 1;
            this.labelMiddleSample2.Text = "10:15:05 AM";
            this.labelMiddleSample2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel101
            // 
            this.panel101.Controls.Add(this.labelSweepSample2);
            this.panel101.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel101.Location = new System.Drawing.Point(1290, 0);
            this.panel101.Name = "panel101";
            this.panel101.Size = new System.Drawing.Size(444, 53);
            this.panel101.TabIndex = 7;
            // 
            // labelSweepSample2
            // 
            this.labelSweepSample2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSweepSample2.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSweepSample2.Location = new System.Drawing.Point(0, 0);
            this.labelSweepSample2.Name = "labelSweepSample2";
            this.labelSweepSample2.Size = new System.Drawing.Size(444, 53);
            this.labelSweepSample2.TabIndex = 3;
            this.labelSweepSample2.Text = "25 mm/sec";
            this.labelSweepSample2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel100
            // 
            this.panel100.Controls.Add(this.labelZoomSample2);
            this.panel100.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel100.Location = new System.Drawing.Point(1734, 0);
            this.panel100.Name = "panel100";
            this.panel100.Size = new System.Drawing.Size(376, 53);
            this.panel100.TabIndex = 6;
            // 
            // labelZoomSample2
            // 
            this.labelZoomSample2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelZoomSample2.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelZoomSample2.Location = new System.Drawing.Point(0, 0);
            this.labelZoomSample2.Name = "labelZoomSample2";
            this.labelZoomSample2.Size = new System.Drawing.Size(376, 53);
            this.labelZoomSample2.TabIndex = 2;
            this.labelZoomSample2.Text = "1 mm/mV";
            this.labelZoomSample2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel99
            // 
            this.panel99.Controls.Add(this.labelEndSample2);
            this.panel99.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel99.Location = new System.Drawing.Point(2110, 0);
            this.panel99.Name = "panel99";
            this.panel99.Size = new System.Drawing.Size(224, 53);
            this.panel99.TabIndex = 5;
            // 
            // labelEndSample2
            // 
            this.labelEndSample2.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelEndSample2.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEndSample2.Location = new System.Drawing.Point(-22, 0);
            this.labelEndSample2.Name = "labelEndSample2";
            this.labelEndSample2.Size = new System.Drawing.Size(246, 53);
            this.labelEndSample2.TabIndex = 1;
            this.labelEndSample2.Text = "10:15:10 AM";
            this.labelEndSample2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel98
            // 
            this.panel98.Controls.Add(this.labelStartSample2);
            this.panel98.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel98.Location = new System.Drawing.Point(0, 0);
            this.panel98.Name = "panel98";
            this.panel98.Size = new System.Drawing.Size(1023, 53);
            this.panel98.TabIndex = 2;
            // 
            // labelStartSample2
            // 
            this.labelStartSample2.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelStartSample2.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStartSample2.Location = new System.Drawing.Point(0, 0);
            this.labelStartSample2.Name = "labelStartSample2";
            this.labelStartSample2.Size = new System.Drawing.Size(556, 53);
            this.labelStartSample2.TabIndex = 1;
            this.labelStartSample2.Text = "10:15:00";
            this.labelStartSample2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panelSeceondStrip
            // 
            this.panelSeceondStrip.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelSeceondStrip.Controls.Add(this.pictureBoxZoom2);
            this.panelSeceondStrip.Controls.Add(this.panel97);
            this.panelSeceondStrip.Controls.Add(this.panel96);
            this.panelSeceondStrip.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSeceondStrip.Location = new System.Drawing.Point(0, 2890);
            this.panelSeceondStrip.Name = "panelSeceondStrip";
            this.panelSeceondStrip.Size = new System.Drawing.Size(2336, 195);
            this.panelSeceondStrip.TabIndex = 30;
            // 
            // pictureBoxZoom2
            // 
            this.pictureBoxZoom2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxZoom2.Location = new System.Drawing.Point(36, 0);
            this.pictureBoxZoom2.Name = "pictureBoxZoom2";
            this.pictureBoxZoom2.Size = new System.Drawing.Size(2275, 193);
            this.pictureBoxZoom2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxZoom2.TabIndex = 6;
            this.pictureBoxZoom2.TabStop = false;
            // 
            // panel97
            // 
            this.panel97.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel97.Location = new System.Drawing.Point(2311, 0);
            this.panel97.Name = "panel97";
            this.panel97.Size = new System.Drawing.Size(23, 193);
            this.panel97.TabIndex = 5;
            // 
            // panel96
            // 
            this.panel96.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel96.Location = new System.Drawing.Point(0, 0);
            this.panel96.Name = "panel96";
            this.panel96.Size = new System.Drawing.Size(36, 193);
            this.panel96.TabIndex = 4;
            // 
            // panelSample2Lead
            // 
            this.panelSample2Lead.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelSample2Lead.Controls.Add(this.panel95);
            this.panelSample2Lead.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSample2Lead.Location = new System.Drawing.Point(0, 2835);
            this.panelSample2Lead.Name = "panelSample2Lead";
            this.panelSample2Lead.Size = new System.Drawing.Size(2336, 55);
            this.panelSample2Lead.TabIndex = 29;
            // 
            // panel95
            // 
            this.panel95.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel95.Controls.Add(this.labelQtUnit2);
            this.panel95.Controls.Add(this.labelQtValue2);
            this.panel95.Controls.Add(this.labelQtText2);
            this.panel95.Controls.Add(this.labelQrsUnit2);
            this.panel95.Controls.Add(this.labelQrsValue2);
            this.panel95.Controls.Add(this.labelQrsText2);
            this.panel95.Controls.Add(this.labelPrUnit2);
            this.panel95.Controls.Add(this.labelPrValue2);
            this.panel95.Controls.Add(this.labelPrText2);
            this.panel95.Controls.Add(this.labelMaxHrUnit2);
            this.panel95.Controls.Add(this.labelMaxHrValue2);
            this.panel95.Controls.Add(this.labelMaxHrText2);
            this.panel95.Controls.Add(this.labelMeanHrUnit2);
            this.panel95.Controls.Add(this.labelMeanHrValue2);
            this.panel95.Controls.Add(this.labelMeanHrText2);
            this.panel95.Controls.Add(this.labelMinHrUnit2);
            this.panel95.Controls.Add(this.labelMinHrValue2);
            this.panel95.Controls.Add(this.labelMinHrText2);
            this.panel95.Controls.Add(this.panel57);
            this.panel95.Controls.Add(this.labelLeadSample2);
            this.panel95.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel95.Location = new System.Drawing.Point(0, 0);
            this.panel95.Name = "panel95";
            this.panel95.Size = new System.Drawing.Size(2334, 53);
            this.panel95.TabIndex = 1;
            // 
            // labelQtUnit2
            // 
            this.labelQtUnit2.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelQtUnit2.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQtUnit2.Location = new System.Drawing.Point(2116, 0);
            this.labelQtUnit2.Name = "labelQtUnit2";
            this.labelQtUnit2.Size = new System.Drawing.Size(78, 51);
            this.labelQtUnit2.TabIndex = 82;
            this.labelQtUnit2.Text = "sec";
            this.labelQtUnit2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelQtUnit2.Click += new System.EventHandler(this.label52_Click);
            // 
            // labelQtValue2
            // 
            this.labelQtValue2.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelQtValue2.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQtValue2.Location = new System.Drawing.Point(1994, 0);
            this.labelQtValue2.Name = "labelQtValue2";
            this.labelQtValue2.Size = new System.Drawing.Size(122, 51);
            this.labelQtValue2.TabIndex = 83;
            this.labelQtValue2.Text = "0,465";
            this.labelQtValue2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelQtText2
            // 
            this.labelQtText2.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelQtText2.Font = new System.Drawing.Font("Verdana", 24F);
            this.labelQtText2.Location = new System.Drawing.Point(1917, 0);
            this.labelQtText2.Name = "labelQtText2";
            this.labelQtText2.Size = new System.Drawing.Size(77, 51);
            this.labelQtText2.TabIndex = 81;
            this.labelQtText2.Text = "QT:";
            this.labelQtText2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelQrsUnit2
            // 
            this.labelQrsUnit2.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelQrsUnit2.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQrsUnit2.Location = new System.Drawing.Point(1841, 0);
            this.labelQrsUnit2.Name = "labelQrsUnit2";
            this.labelQrsUnit2.Size = new System.Drawing.Size(76, 51);
            this.labelQrsUnit2.TabIndex = 80;
            this.labelQrsUnit2.Text = "sec";
            this.labelQrsUnit2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelQrsValue2
            // 
            this.labelQrsValue2.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelQrsValue2.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQrsValue2.Location = new System.Drawing.Point(1720, 0);
            this.labelQrsValue2.Name = "labelQrsValue2";
            this.labelQrsValue2.Size = new System.Drawing.Size(121, 51);
            this.labelQrsValue2.TabIndex = 79;
            this.labelQrsValue2.Text = "0,113";
            this.labelQrsValue2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelQrsText2
            // 
            this.labelQrsText2.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelQrsText2.Font = new System.Drawing.Font("Verdana", 24F);
            this.labelQrsText2.Location = new System.Drawing.Point(1610, 0);
            this.labelQrsText2.Name = "labelQrsText2";
            this.labelQrsText2.Size = new System.Drawing.Size(110, 51);
            this.labelQrsText2.TabIndex = 78;
            this.labelQrsText2.Text = "QRS:";
            this.labelQrsText2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPrUnit2
            // 
            this.labelPrUnit2.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPrUnit2.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPrUnit2.Location = new System.Drawing.Point(1532, 0);
            this.labelPrUnit2.Name = "labelPrUnit2";
            this.labelPrUnit2.Size = new System.Drawing.Size(78, 51);
            this.labelPrUnit2.TabIndex = 77;
            this.labelPrUnit2.Text = "sec";
            this.labelPrUnit2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelPrValue2
            // 
            this.labelPrValue2.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPrValue2.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPrValue2.Location = new System.Drawing.Point(1404, 0);
            this.labelPrValue2.Name = "labelPrValue2";
            this.labelPrValue2.Size = new System.Drawing.Size(128, 51);
            this.labelPrValue2.TabIndex = 76;
            this.labelPrValue2.Text = "0,202";
            this.labelPrValue2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPrText2
            // 
            this.labelPrText2.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPrText2.Font = new System.Drawing.Font("Verdana", 24F);
            this.labelPrText2.Location = new System.Drawing.Point(1331, 0);
            this.labelPrText2.Name = "labelPrText2";
            this.labelPrText2.Size = new System.Drawing.Size(73, 51);
            this.labelPrText2.TabIndex = 75;
            this.labelPrText2.Text = "PR:";
            this.labelPrText2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelMaxHrUnit2
            // 
            this.labelMaxHrUnit2.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelMaxHrUnit2.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMaxHrUnit2.Location = new System.Drawing.Point(1234, 0);
            this.labelMaxHrUnit2.Name = "labelMaxHrUnit2";
            this.labelMaxHrUnit2.Size = new System.Drawing.Size(97, 51);
            this.labelMaxHrUnit2.TabIndex = 69;
            this.labelMaxHrUnit2.Text = "bpm";
            this.labelMaxHrUnit2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelMaxHrValue2
            // 
            this.labelMaxHrValue2.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelMaxHrValue2.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMaxHrValue2.Location = new System.Drawing.Point(1143, 0);
            this.labelMaxHrValue2.Name = "labelMaxHrValue2";
            this.labelMaxHrValue2.Size = new System.Drawing.Size(91, 51);
            this.labelMaxHrValue2.TabIndex = 68;
            this.labelMaxHrValue2.Text = "120";
            this.labelMaxHrValue2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelMaxHrText2
            // 
            this.labelMaxHrText2.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelMaxHrText2.Font = new System.Drawing.Font("Verdana", 24F);
            this.labelMaxHrText2.Location = new System.Drawing.Point(978, 0);
            this.labelMaxHrText2.Name = "labelMaxHrText2";
            this.labelMaxHrText2.Size = new System.Drawing.Size(165, 51);
            this.labelMaxHrText2.TabIndex = 67;
            this.labelMaxHrText2.Text = "Max HR:";
            this.labelMaxHrText2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelMeanHrUnit2
            // 
            this.labelMeanHrUnit2.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelMeanHrUnit2.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMeanHrUnit2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelMeanHrUnit2.Location = new System.Drawing.Point(881, 0);
            this.labelMeanHrUnit2.Name = "labelMeanHrUnit2";
            this.labelMeanHrUnit2.Size = new System.Drawing.Size(97, 51);
            this.labelMeanHrUnit2.TabIndex = 84;
            this.labelMeanHrUnit2.Text = "bpm";
            this.labelMeanHrUnit2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelMeanHrValue2
            // 
            this.labelMeanHrValue2.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelMeanHrValue2.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMeanHrValue2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelMeanHrValue2.Location = new System.Drawing.Point(794, 0);
            this.labelMeanHrValue2.Name = "labelMeanHrValue2";
            this.labelMeanHrValue2.Size = new System.Drawing.Size(87, 51);
            this.labelMeanHrValue2.TabIndex = 66;
            this.labelMeanHrValue2.Text = "110";
            this.labelMeanHrValue2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelMeanHrText2
            // 
            this.labelMeanHrText2.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelMeanHrText2.Font = new System.Drawing.Font("Verdana", 24F);
            this.labelMeanHrText2.Location = new System.Drawing.Point(619, 0);
            this.labelMeanHrText2.Name = "labelMeanHrText2";
            this.labelMeanHrText2.Size = new System.Drawing.Size(175, 51);
            this.labelMeanHrText2.TabIndex = 65;
            this.labelMeanHrText2.Text = "Mean HR:";
            this.labelMeanHrText2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelMinHrUnit2
            // 
            this.labelMinHrUnit2.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelMinHrUnit2.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMinHrUnit2.Location = new System.Drawing.Point(520, 0);
            this.labelMinHrUnit2.Name = "labelMinHrUnit2";
            this.labelMinHrUnit2.Size = new System.Drawing.Size(99, 51);
            this.labelMinHrUnit2.TabIndex = 64;
            this.labelMinHrUnit2.Text = "bpm";
            this.labelMinHrUnit2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelMinHrValue2
            // 
            this.labelMinHrValue2.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelMinHrValue2.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMinHrValue2.Location = new System.Drawing.Point(425, 0);
            this.labelMinHrValue2.Name = "labelMinHrValue2";
            this.labelMinHrValue2.Size = new System.Drawing.Size(95, 51);
            this.labelMinHrValue2.TabIndex = 63;
            this.labelMinHrValue2.Text = "100";
            this.labelMinHrValue2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelMinHrText2
            // 
            this.labelMinHrText2.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelMinHrText2.Font = new System.Drawing.Font("Verdana", 24F);
            this.labelMinHrText2.Location = new System.Drawing.Point(278, 0);
            this.labelMinHrText2.Name = "labelMinHrText2";
            this.labelMinHrText2.Size = new System.Drawing.Size(147, 51);
            this.labelMinHrText2.TabIndex = 62;
            this.labelMinHrText2.Text = "Min HR:";
            this.labelMinHrText2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel57
            // 
            this.panel57.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel57.Location = new System.Drawing.Point(219, 0);
            this.panel57.Name = "panel57";
            this.panel57.Size = new System.Drawing.Size(59, 51);
            this.panel57.TabIndex = 48;
            // 
            // labelLeadSample2
            // 
            this.labelLeadSample2.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelLeadSample2.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLeadSample2.Location = new System.Drawing.Point(0, 0);
            this.labelLeadSample2.Name = "labelLeadSample2";
            this.labelLeadSample2.Size = new System.Drawing.Size(219, 51);
            this.labelLeadSample2.TabIndex = 47;
            this.labelLeadSample2.Text = "LEAD I";
            this.labelLeadSample2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panelSample2Header
            // 
            this.panelSample2Header.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelSample2Header.Controls.Add(this.labelDateSampleNr2);
            this.panelSample2Header.Controls.Add(this.label36);
            this.panelSample2Header.Controls.Add(this.labelTimeSampleNr2);
            this.panelSample2Header.Controls.Add(this.panel90);
            this.panelSample2Header.Controls.Add(this.panel89);
            this.panelSample2Header.Controls.Add(this.panel88);
            this.panelSample2Header.Controls.Add(this.panel87);
            this.panelSample2Header.Controls.Add(this.panel86);
            this.panelSample2Header.Controls.Add(this.panel85);
            this.panelSample2Header.Controls.Add(this.panel84);
            this.panelSample2Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSample2Header.Location = new System.Drawing.Point(0, 2772);
            this.panelSample2Header.Name = "panelSample2Header";
            this.panelSample2Header.Size = new System.Drawing.Size(2336, 63);
            this.panelSample2Header.TabIndex = 28;
            // 
            // labelDateSampleNr2
            // 
            this.labelDateSampleNr2.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelDateSampleNr2.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDateSampleNr2.Location = new System.Drawing.Point(1765, 0);
            this.labelDateSampleNr2.Name = "labelDateSampleNr2";
            this.labelDateSampleNr2.Size = new System.Drawing.Size(273, 61);
            this.labelDateSampleNr2.TabIndex = 14;
            this.labelDateSampleNr2.Text = "11/23/2016";
            this.labelDateSampleNr2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label36
            // 
            this.label36.Dock = System.Windows.Forms.DockStyle.Right;
            this.label36.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(2038, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(23, 61);
            this.label36.TabIndex = 13;
            this.label36.Text = "-";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelTimeSampleNr2
            // 
            this.labelTimeSampleNr2.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelTimeSampleNr2.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTimeSampleNr2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelTimeSampleNr2.Location = new System.Drawing.Point(2061, 0);
            this.labelTimeSampleNr2.Name = "labelTimeSampleNr2";
            this.labelTimeSampleNr2.Size = new System.Drawing.Size(273, 61);
            this.labelTimeSampleNr2.TabIndex = 12;
            this.labelTimeSampleNr2.Text = "11:15:10 AM";
            this.labelTimeSampleNr2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel90
            // 
            this.panel90.Controls.Add(this.label34);
            this.panel90.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel90.Location = new System.Drawing.Point(799, 0);
            this.panel90.Name = "panel90";
            this.panel90.Size = new System.Drawing.Size(29, 61);
            this.panel90.TabIndex = 7;
            this.panel90.Visible = false;
            // 
            // label34
            // 
            this.label34.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label34.Font = new System.Drawing.Font("Verdana", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(0, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(29, 61);
            this.label34.TabIndex = 4;
            this.label34.Text = "-";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel89
            // 
            this.panel89.AutoSize = true;
            this.panel89.Controls.Add(this.labelEvent2NrSample);
            this.panel89.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel89.Location = new System.Drawing.Point(733, 0);
            this.panel89.Name = "panel89";
            this.panel89.Size = new System.Drawing.Size(66, 61);
            this.panel89.TabIndex = 6;
            this.panel89.Visible = false;
            // 
            // labelEvent2NrSample
            // 
            this.labelEvent2NrSample.AutoSize = true;
            this.labelEvent2NrSample.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelEvent2NrSample.Font = new System.Drawing.Font("Verdana", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEvent2NrSample.Location = new System.Drawing.Point(0, 0);
            this.labelEvent2NrSample.Name = "labelEvent2NrSample";
            this.labelEvent2NrSample.Size = new System.Drawing.Size(66, 65);
            this.labelEvent2NrSample.TabIndex = 3;
            this.labelEvent2NrSample.Text = "2";
            this.labelEvent2NrSample.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel88
            // 
            this.panel88.Controls.Add(this.labelSample);
            this.panel88.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel88.Location = new System.Drawing.Point(468, 0);
            this.panel88.Name = "panel88";
            this.panel88.Size = new System.Drawing.Size(265, 61);
            this.panel88.TabIndex = 5;
            this.panel88.Visible = false;
            // 
            // labelSample
            // 
            this.labelSample.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSample.Font = new System.Drawing.Font("Verdana", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSample.Location = new System.Drawing.Point(0, 0);
            this.labelSample.Name = "labelSample";
            this.labelSample.Size = new System.Drawing.Size(265, 61);
            this.labelSample.TabIndex = 3;
            this.labelSample.Text = "Sample";
            this.labelSample.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel87
            // 
            this.panel87.Controls.Add(this.label30);
            this.panel87.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel87.Location = new System.Drawing.Point(439, 0);
            this.panel87.Name = "panel87";
            this.panel87.Size = new System.Drawing.Size(29, 61);
            this.panel87.TabIndex = 4;
            this.panel87.Visible = false;
            // 
            // label30
            // 
            this.label30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label30.Font = new System.Drawing.Font("Verdana", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(0, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(29, 61);
            this.label30.TabIndex = 3;
            this.label30.Text = "-";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel86
            // 
            this.panel86.AutoSize = true;
            this.panel86.Controls.Add(this.labelEvent2NrRes);
            this.panel86.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel86.Location = new System.Drawing.Point(297, 0);
            this.panel86.Name = "panel86";
            this.panel86.Size = new System.Drawing.Size(142, 61);
            this.panel86.TabIndex = 3;
            this.panel86.Visible = false;
            // 
            // labelEvent2NrRes
            // 
            this.labelEvent2NrRes.AutoSize = true;
            this.labelEvent2NrRes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelEvent2NrRes.Font = new System.Drawing.Font("Verdana", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEvent2NrRes.Location = new System.Drawing.Point(0, 0);
            this.labelEvent2NrRes.Name = "labelEvent2NrRes";
            this.labelEvent2NrRes.Size = new System.Drawing.Size(142, 65);
            this.labelEvent2NrRes.TabIndex = 2;
            this.labelEvent2NrRes.Text = "175";
            this.labelEvent2NrRes.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel85
            // 
            this.panel85.Controls.Add(this.labelEvenNrSample2);
            this.panel85.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel85.Location = new System.Drawing.Point(20, 0);
            this.panel85.Name = "panel85";
            this.panel85.Size = new System.Drawing.Size(277, 61);
            this.panel85.TabIndex = 2;
            this.panel85.Visible = false;
            // 
            // labelEvenNrSample2
            // 
            this.labelEvenNrSample2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelEvenNrSample2.Font = new System.Drawing.Font("Verdana", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEvenNrSample2.Location = new System.Drawing.Point(0, 0);
            this.labelEvenNrSample2.Name = "labelEvenNrSample2";
            this.labelEvenNrSample2.Size = new System.Drawing.Size(277, 61);
            this.labelEvenNrSample2.TabIndex = 1;
            this.labelEvenNrSample2.Text = "Event #";
            this.labelEvenNrSample2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel84
            // 
            this.panel84.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel84.Location = new System.Drawing.Point(0, 0);
            this.panel84.Name = "panel84";
            this.panel84.Size = new System.Drawing.Size(20, 61);
            this.panel84.TabIndex = 1;
            // 
            // panel150
            // 
            this.panel150.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel150.Location = new System.Drawing.Point(0, 2749);
            this.panel150.Name = "panel150";
            this.panel150.Size = new System.Drawing.Size(2336, 23);
            this.panel150.TabIndex = 27;
            // 
            // panel149
            // 
            this.panel149.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel149.Controls.Add(this.panel83);
            this.panel149.Controls.Add(this.panel82);
            this.panel149.Controls.Add(this.panel81);
            this.panel149.Controls.Add(this.panel80);
            this.panel149.Controls.Add(this.panel79);
            this.panel149.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel149.Location = new System.Drawing.Point(0, 2700);
            this.panel149.Name = "panel149";
            this.panel149.Size = new System.Drawing.Size(2336, 49);
            this.panel149.TabIndex = 26;
            // 
            // panel83
            // 
            this.panel83.Controls.Add(this.labelSweepFullSample1);
            this.panel83.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel83.Location = new System.Drawing.Point(1295, 0);
            this.panel83.Name = "panel83";
            this.panel83.Size = new System.Drawing.Size(436, 47);
            this.panel83.TabIndex = 7;
            // 
            // labelSweepFullSample1
            // 
            this.labelSweepFullSample1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSweepFullSample1.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSweepFullSample1.Location = new System.Drawing.Point(0, 0);
            this.labelSweepFullSample1.Name = "labelSweepFullSample1";
            this.labelSweepFullSample1.Size = new System.Drawing.Size(436, 47);
            this.labelSweepFullSample1.TabIndex = 3;
            this.labelSweepFullSample1.Text = "25 mm/sec";
            this.labelSweepFullSample1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel82
            // 
            this.panel82.Controls.Add(this.labelZoomFullSample1);
            this.panel82.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel82.Location = new System.Drawing.Point(1731, 0);
            this.panel82.Name = "panel82";
            this.panel82.Size = new System.Drawing.Size(378, 47);
            this.panel82.TabIndex = 6;
            // 
            // labelZoomFullSample1
            // 
            this.labelZoomFullSample1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelZoomFullSample1.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelZoomFullSample1.Location = new System.Drawing.Point(0, 0);
            this.labelZoomFullSample1.Name = "labelZoomFullSample1";
            this.labelZoomFullSample1.Size = new System.Drawing.Size(378, 47);
            this.labelZoomFullSample1.TabIndex = 2;
            this.labelZoomFullSample1.Text = "1 mm/mV";
            this.labelZoomFullSample1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel81
            // 
            this.panel81.Controls.Add(this.labelEndFullSample1);
            this.panel81.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel81.Location = new System.Drawing.Point(2109, 0);
            this.panel81.Name = "panel81";
            this.panel81.Size = new System.Drawing.Size(225, 47);
            this.panel81.TabIndex = 5;
            // 
            // labelEndFullSample1
            // 
            this.labelEndFullSample1.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelEndFullSample1.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEndFullSample1.Location = new System.Drawing.Point(-28, 0);
            this.labelEndFullSample1.Name = "labelEndFullSample1";
            this.labelEndFullSample1.Size = new System.Drawing.Size(253, 47);
            this.labelEndFullSample1.TabIndex = 1;
            this.labelEndFullSample1.Text = "10:15:30 AM";
            this.labelEndFullSample1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel80
            // 
            this.panel80.Controls.Add(this.labelMiddleFullSample1);
            this.panel80.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel80.Location = new System.Drawing.Point(1023, 0);
            this.panel80.Name = "panel80";
            this.panel80.Size = new System.Drawing.Size(253, 47);
            this.panel80.TabIndex = 2;
            // 
            // labelMiddleFullSample1
            // 
            this.labelMiddleFullSample1.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelMiddleFullSample1.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMiddleFullSample1.Location = new System.Drawing.Point(0, 0);
            this.labelMiddleFullSample1.Name = "labelMiddleFullSample1";
            this.labelMiddleFullSample1.Size = new System.Drawing.Size(237, 47);
            this.labelMiddleFullSample1.TabIndex = 1;
            this.labelMiddleFullSample1.Text = "10:15:00 AM";
            this.labelMiddleFullSample1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel79
            // 
            this.panel79.Controls.Add(this.labelStartFullSample1);
            this.panel79.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel79.Location = new System.Drawing.Point(0, 0);
            this.panel79.Name = "panel79";
            this.panel79.Size = new System.Drawing.Size(1023, 47);
            this.panel79.TabIndex = 1;
            // 
            // labelStartFullSample1
            // 
            this.labelStartFullSample1.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelStartFullSample1.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStartFullSample1.Location = new System.Drawing.Point(0, 0);
            this.labelStartFullSample1.Name = "labelStartFullSample1";
            this.labelStartFullSample1.Size = new System.Drawing.Size(489, 47);
            this.labelStartFullSample1.TabIndex = 1;
            this.labelStartFullSample1.Text = "10:14:30";
            this.labelStartFullSample1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panelFullStrip
            // 
            this.panelFullStrip.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelFullStrip.Controls.Add(this.panel44);
            this.panelFullStrip.Controls.Add(this.panel40);
            this.panelFullStrip.Controls.Add(this.panel39);
            this.panelFullStrip.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelFullStrip.Location = new System.Drawing.Point(0, 2505);
            this.panelFullStrip.Name = "panelFullStrip";
            this.panelFullStrip.Size = new System.Drawing.Size(2336, 195);
            this.panelFullStrip.TabIndex = 25;
            // 
            // panel44
            // 
            this.panel44.Controls.Add(this.pictureBoxStrip1);
            this.panel44.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel44.Location = new System.Drawing.Point(36, 0);
            this.panel44.Name = "panel44";
            this.panel44.Size = new System.Drawing.Size(2275, 193);
            this.panel44.TabIndex = 2;
            // 
            // pictureBoxStrip1
            // 
            this.pictureBoxStrip1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxStrip1.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxStrip1.Name = "pictureBoxStrip1";
            this.pictureBoxStrip1.Size = new System.Drawing.Size(2275, 193);
            this.pictureBoxStrip1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxStrip1.TabIndex = 0;
            this.pictureBoxStrip1.TabStop = false;
            // 
            // panel40
            // 
            this.panel40.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel40.Location = new System.Drawing.Point(2311, 0);
            this.panel40.Name = "panel40";
            this.panel40.Size = new System.Drawing.Size(23, 193);
            this.panel40.TabIndex = 1;
            // 
            // panel39
            // 
            this.panel39.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel39.Location = new System.Drawing.Point(0, 0);
            this.panel39.Name = "panel39";
            this.panel39.Size = new System.Drawing.Size(36, 193);
            this.panel39.TabIndex = 0;
            // 
            // panel147
            // 
            this.panel147.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel147.Controls.Add(this.panel38);
            this.panel147.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel147.Location = new System.Drawing.Point(0, 2458);
            this.panel147.Name = "panel147";
            this.panel147.Size = new System.Drawing.Size(2336, 47);
            this.panel147.TabIndex = 24;
            // 
            // panel38
            // 
            this.panel38.Controls.Add(this.labelLeadFullStripSample1);
            this.panel38.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel38.Location = new System.Drawing.Point(0, 0);
            this.panel38.Name = "panel38";
            this.panel38.Size = new System.Drawing.Size(220, 45);
            this.panel38.TabIndex = 0;
            // 
            // labelLeadFullStripSample1
            // 
            this.labelLeadFullStripSample1.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelLeadFullStripSample1.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLeadFullStripSample1.Location = new System.Drawing.Point(0, 0);
            this.labelLeadFullStripSample1.Name = "labelLeadFullStripSample1";
            this.labelLeadFullStripSample1.Size = new System.Drawing.Size(217, 45);
            this.labelLeadFullStripSample1.TabIndex = 47;
            this.labelLeadFullStripSample1.Text = "LEAD I";
            this.labelLeadFullStripSample1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel146
            // 
            this.panel146.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel146.Controls.Add(this.panel232);
            this.panel146.Controls.Add(this.panel234);
            this.panel146.Controls.Add(this.panel233);
            this.panel146.Controls.Add(this.panel230);
            this.panel146.Controls.Add(this.panel145);
            this.panel146.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel146.Location = new System.Drawing.Point(0, 2409);
            this.panel146.Name = "panel146";
            this.panel146.Size = new System.Drawing.Size(2336, 49);
            this.panel146.TabIndex = 23;
            // 
            // panel232
            // 
            this.panel232.Controls.Add(this.labelSweepSample1);
            this.panel232.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel232.Location = new System.Drawing.Point(1293, 0);
            this.panel232.Name = "panel232";
            this.panel232.Size = new System.Drawing.Size(434, 47);
            this.panel232.TabIndex = 6;
            // 
            // labelSweepSample1
            // 
            this.labelSweepSample1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSweepSample1.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSweepSample1.Location = new System.Drawing.Point(0, 0);
            this.labelSweepSample1.Name = "labelSweepSample1";
            this.labelSweepSample1.Size = new System.Drawing.Size(434, 47);
            this.labelSweepSample1.TabIndex = 3;
            this.labelSweepSample1.Text = "25 mm/sec";
            this.labelSweepSample1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel234
            // 
            this.panel234.Controls.Add(this.labelZoomSample1);
            this.panel234.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel234.Location = new System.Drawing.Point(1727, 0);
            this.panel234.Name = "panel234";
            this.panel234.Size = new System.Drawing.Size(379, 47);
            this.panel234.TabIndex = 5;
            // 
            // labelZoomSample1
            // 
            this.labelZoomSample1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelZoomSample1.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelZoomSample1.Location = new System.Drawing.Point(0, 0);
            this.labelZoomSample1.Name = "labelZoomSample1";
            this.labelZoomSample1.Size = new System.Drawing.Size(379, 47);
            this.labelZoomSample1.TabIndex = 2;
            this.labelZoomSample1.Text = "1 mm/mV";
            this.labelZoomSample1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel233
            // 
            this.panel233.Controls.Add(this.labelEndSample1);
            this.panel233.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel233.Location = new System.Drawing.Point(2106, 0);
            this.panel233.Name = "panel233";
            this.panel233.Size = new System.Drawing.Size(228, 47);
            this.panel233.TabIndex = 4;
            // 
            // labelEndSample1
            // 
            this.labelEndSample1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelEndSample1.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEndSample1.Location = new System.Drawing.Point(0, 0);
            this.labelEndSample1.Name = "labelEndSample1";
            this.labelEndSample1.Size = new System.Drawing.Size(228, 47);
            this.labelEndSample1.TabIndex = 1;
            this.labelEndSample1.Text = "10:15:20 AM";
            this.labelEndSample1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel230
            // 
            this.panel230.Controls.Add(this.labelMiddleSample1);
            this.panel230.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel230.Location = new System.Drawing.Point(1023, 0);
            this.panel230.Name = "panel230";
            this.panel230.Size = new System.Drawing.Size(253, 47);
            this.panel230.TabIndex = 1;
            // 
            // labelMiddleSample1
            // 
            this.labelMiddleSample1.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelMiddleSample1.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMiddleSample1.Location = new System.Drawing.Point(0, 0);
            this.labelMiddleSample1.Name = "labelMiddleSample1";
            this.labelMiddleSample1.Size = new System.Drawing.Size(237, 47);
            this.labelMiddleSample1.TabIndex = 1;
            this.labelMiddleSample1.Text = "10:15:15 AM";
            this.labelMiddleSample1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel145
            // 
            this.panel145.Controls.Add(this.labelStartSample1);
            this.panel145.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel145.Location = new System.Drawing.Point(0, 0);
            this.panel145.Name = "panel145";
            this.panel145.Size = new System.Drawing.Size(1023, 47);
            this.panel145.TabIndex = 0;
            // 
            // labelStartSample1
            // 
            this.labelStartSample1.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelStartSample1.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStartSample1.Location = new System.Drawing.Point(0, 0);
            this.labelStartSample1.Name = "labelStartSample1";
            this.labelStartSample1.Size = new System.Drawing.Size(532, 47);
            this.labelStartSample1.TabIndex = 1;
            this.labelStartSample1.Text = "10:15:10";
            this.labelStartSample1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panelSample1STrip
            // 
            this.panelSample1STrip.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelSample1STrip.Controls.Add(this.panel37);
            this.panelSample1STrip.Controls.Add(this.panel143);
            this.panelSample1STrip.Controls.Add(this.panel229);
            this.panelSample1STrip.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSample1STrip.Location = new System.Drawing.Point(0, 2214);
            this.panelSample1STrip.Name = "panelSample1STrip";
            this.panelSample1STrip.Size = new System.Drawing.Size(2336, 195);
            this.panelSample1STrip.TabIndex = 22;
            // 
            // panel37
            // 
            this.panel37.Controls.Add(this.pictureBoxZoom1);
            this.panel37.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel37.Location = new System.Drawing.Point(36, 0);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(2275, 193);
            this.panel37.TabIndex = 5;
            // 
            // pictureBoxZoom1
            // 
            this.pictureBoxZoom1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxZoom1.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxZoom1.Name = "pictureBoxZoom1";
            this.pictureBoxZoom1.Size = new System.Drawing.Size(2275, 193);
            this.pictureBoxZoom1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxZoom1.TabIndex = 0;
            this.pictureBoxZoom1.TabStop = false;
            // 
            // panel143
            // 
            this.panel143.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel143.Location = new System.Drawing.Point(2311, 0);
            this.panel143.Name = "panel143";
            this.panel143.Size = new System.Drawing.Size(23, 193);
            this.panel143.TabIndex = 4;
            // 
            // panel229
            // 
            this.panel229.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel229.Location = new System.Drawing.Point(0, 0);
            this.panel229.Name = "panel229";
            this.panel229.Size = new System.Drawing.Size(36, 193);
            this.panel229.TabIndex = 3;
            // 
            // panel144
            // 
            this.panel144.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel144.Controls.Add(this.labelQtUnit1);
            this.panel144.Controls.Add(this.labelQtValue1);
            this.panel144.Controls.Add(this.labelQtText1);
            this.panel144.Controls.Add(this.labelQrsUnit1);
            this.panel144.Controls.Add(this.labelQrsValue1);
            this.panel144.Controls.Add(this.labelQrsText1);
            this.panel144.Controls.Add(this.labelPrUnit1);
            this.panel144.Controls.Add(this.labelPrValue1);
            this.panel144.Controls.Add(this.labelPrText1);
            this.panel144.Controls.Add(this.labelMaxHrUnit1);
            this.panel144.Controls.Add(this.labelMaxHrValue1);
            this.panel144.Controls.Add(this.labelMaxHrText1);
            this.panel144.Controls.Add(this.labelMeanHrUnit1);
            this.panel144.Controls.Add(this.labelMeanHrValue1);
            this.panel144.Controls.Add(this.labelMeanHrText1);
            this.panel144.Controls.Add(this.labelMinHrUnit1);
            this.panel144.Controls.Add(this.labelMinHrValue1);
            this.panel144.Controls.Add(this.labelMinHrText1);
            this.panel144.Controls.Add(this.panel55);
            this.panel144.Controls.Add(this.labelLeadZoomStrip1);
            this.panel144.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel144.Location = new System.Drawing.Point(0, 2167);
            this.panel144.Name = "panel144";
            this.panel144.Size = new System.Drawing.Size(2336, 47);
            this.panel144.TabIndex = 21;
            // 
            // labelQtUnit1
            // 
            this.labelQtUnit1.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelQtUnit1.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQtUnit1.Location = new System.Drawing.Point(2117, 0);
            this.labelQtUnit1.Name = "labelQtUnit1";
            this.labelQtUnit1.Size = new System.Drawing.Size(78, 45);
            this.labelQtUnit1.TabIndex = 73;
            this.labelQtUnit1.Text = "sec";
            this.labelQtUnit1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelQtValue1
            // 
            this.labelQtValue1.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelQtValue1.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQtValue1.Location = new System.Drawing.Point(1995, 0);
            this.labelQtValue1.Name = "labelQtValue1";
            this.labelQtValue1.Size = new System.Drawing.Size(122, 45);
            this.labelQtValue1.TabIndex = 74;
            this.labelQtValue1.Text = "0,465";
            this.labelQtValue1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelQtText1
            // 
            this.labelQtText1.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelQtText1.Font = new System.Drawing.Font("Verdana", 24F);
            this.labelQtText1.Location = new System.Drawing.Point(1918, 0);
            this.labelQtText1.Name = "labelQtText1";
            this.labelQtText1.Size = new System.Drawing.Size(77, 45);
            this.labelQtText1.TabIndex = 72;
            this.labelQtText1.Text = "QT:";
            this.labelQtText1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelQrsUnit1
            // 
            this.labelQrsUnit1.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelQrsUnit1.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQrsUnit1.Location = new System.Drawing.Point(1842, 0);
            this.labelQrsUnit1.Name = "labelQrsUnit1";
            this.labelQrsUnit1.Size = new System.Drawing.Size(76, 45);
            this.labelQrsUnit1.TabIndex = 70;
            this.labelQrsUnit1.Text = "sec";
            this.labelQrsUnit1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelQrsValue1
            // 
            this.labelQrsValue1.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelQrsValue1.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQrsValue1.Location = new System.Drawing.Point(1721, 0);
            this.labelQrsValue1.Name = "labelQrsValue1";
            this.labelQrsValue1.Size = new System.Drawing.Size(121, 45);
            this.labelQrsValue1.TabIndex = 69;
            this.labelQrsValue1.Text = "0,113";
            this.labelQrsValue1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelQrsText1
            // 
            this.labelQrsText1.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelQrsText1.Font = new System.Drawing.Font("Verdana", 24F);
            this.labelQrsText1.Location = new System.Drawing.Point(1611, 0);
            this.labelQrsText1.Name = "labelQrsText1";
            this.labelQrsText1.Size = new System.Drawing.Size(110, 45);
            this.labelQrsText1.TabIndex = 67;
            this.labelQrsText1.Text = "QRS:";
            this.labelQrsText1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPrUnit1
            // 
            this.labelPrUnit1.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPrUnit1.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPrUnit1.Location = new System.Drawing.Point(1533, 0);
            this.labelPrUnit1.Name = "labelPrUnit1";
            this.labelPrUnit1.Size = new System.Drawing.Size(78, 45);
            this.labelPrUnit1.TabIndex = 65;
            this.labelPrUnit1.Text = "sec";
            this.labelPrUnit1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelPrValue1
            // 
            this.labelPrValue1.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPrValue1.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPrValue1.Location = new System.Drawing.Point(1405, 0);
            this.labelPrValue1.Name = "labelPrValue1";
            this.labelPrValue1.Size = new System.Drawing.Size(128, 45);
            this.labelPrValue1.TabIndex = 64;
            this.labelPrValue1.Text = "0,202";
            this.labelPrValue1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPrText1
            // 
            this.labelPrText1.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPrText1.Font = new System.Drawing.Font("Verdana", 24F);
            this.labelPrText1.Location = new System.Drawing.Point(1332, 0);
            this.labelPrText1.Name = "labelPrText1";
            this.labelPrText1.Size = new System.Drawing.Size(73, 45);
            this.labelPrText1.TabIndex = 62;
            this.labelPrText1.Text = "PR:";
            this.labelPrText1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelMaxHrUnit1
            // 
            this.labelMaxHrUnit1.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelMaxHrUnit1.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMaxHrUnit1.Location = new System.Drawing.Point(1235, 0);
            this.labelMaxHrUnit1.Name = "labelMaxHrUnit1";
            this.labelMaxHrUnit1.Size = new System.Drawing.Size(97, 45);
            this.labelMaxHrUnit1.TabIndex = 61;
            this.labelMaxHrUnit1.Text = "bpm";
            this.labelMaxHrUnit1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelMaxHrValue1
            // 
            this.labelMaxHrValue1.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelMaxHrValue1.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMaxHrValue1.Location = new System.Drawing.Point(1144, 0);
            this.labelMaxHrValue1.Name = "labelMaxHrValue1";
            this.labelMaxHrValue1.Size = new System.Drawing.Size(91, 45);
            this.labelMaxHrValue1.TabIndex = 59;
            this.labelMaxHrValue1.Text = "120";
            this.labelMaxHrValue1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelMaxHrText1
            // 
            this.labelMaxHrText1.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelMaxHrText1.Font = new System.Drawing.Font("Verdana", 24F);
            this.labelMaxHrText1.Location = new System.Drawing.Point(979, 0);
            this.labelMaxHrText1.Name = "labelMaxHrText1";
            this.labelMaxHrText1.Size = new System.Drawing.Size(165, 45);
            this.labelMaxHrText1.TabIndex = 58;
            this.labelMaxHrText1.Text = "Max HR:";
            this.labelMaxHrText1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelMeanHrUnit1
            // 
            this.labelMeanHrUnit1.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelMeanHrUnit1.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMeanHrUnit1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelMeanHrUnit1.Location = new System.Drawing.Point(882, 0);
            this.labelMeanHrUnit1.Name = "labelMeanHrUnit1";
            this.labelMeanHrUnit1.Size = new System.Drawing.Size(97, 45);
            this.labelMeanHrUnit1.TabIndex = 57;
            this.labelMeanHrUnit1.Text = "bpm";
            this.labelMeanHrUnit1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelMeanHrValue1
            // 
            this.labelMeanHrValue1.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelMeanHrValue1.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMeanHrValue1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelMeanHrValue1.Location = new System.Drawing.Point(795, 0);
            this.labelMeanHrValue1.Name = "labelMeanHrValue1";
            this.labelMeanHrValue1.Size = new System.Drawing.Size(87, 45);
            this.labelMeanHrValue1.TabIndex = 55;
            this.labelMeanHrValue1.Text = "110";
            this.labelMeanHrValue1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelMeanHrText1
            // 
            this.labelMeanHrText1.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelMeanHrText1.Font = new System.Drawing.Font("Verdana", 24F);
            this.labelMeanHrText1.Location = new System.Drawing.Point(620, 0);
            this.labelMeanHrText1.Name = "labelMeanHrText1";
            this.labelMeanHrText1.Size = new System.Drawing.Size(175, 45);
            this.labelMeanHrText1.TabIndex = 54;
            this.labelMeanHrText1.Text = "Mean HR:";
            this.labelMeanHrText1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelMinHrUnit1
            // 
            this.labelMinHrUnit1.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelMinHrUnit1.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMinHrUnit1.Location = new System.Drawing.Point(521, 0);
            this.labelMinHrUnit1.Name = "labelMinHrUnit1";
            this.labelMinHrUnit1.Size = new System.Drawing.Size(99, 45);
            this.labelMinHrUnit1.TabIndex = 53;
            this.labelMinHrUnit1.Text = "bpm";
            this.labelMinHrUnit1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelMinHrValue1
            // 
            this.labelMinHrValue1.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelMinHrValue1.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMinHrValue1.Location = new System.Drawing.Point(426, 0);
            this.labelMinHrValue1.Name = "labelMinHrValue1";
            this.labelMinHrValue1.Size = new System.Drawing.Size(95, 45);
            this.labelMinHrValue1.TabIndex = 51;
            this.labelMinHrValue1.Text = "100";
            this.labelMinHrValue1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelMinHrText1
            // 
            this.labelMinHrText1.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelMinHrText1.Font = new System.Drawing.Font("Verdana", 24F);
            this.labelMinHrText1.Location = new System.Drawing.Point(279, 0);
            this.labelMinHrText1.Name = "labelMinHrText1";
            this.labelMinHrText1.Size = new System.Drawing.Size(147, 45);
            this.labelMinHrText1.TabIndex = 49;
            this.labelMinHrText1.Text = "Min HR:";
            this.labelMinHrText1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel55
            // 
            this.panel55.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel55.Location = new System.Drawing.Point(220, 0);
            this.panel55.Name = "panel55";
            this.panel55.Size = new System.Drawing.Size(59, 45);
            this.panel55.TabIndex = 47;
            // 
            // labelLeadZoomStrip1
            // 
            this.labelLeadZoomStrip1.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelLeadZoomStrip1.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLeadZoomStrip1.Location = new System.Drawing.Point(0, 0);
            this.labelLeadZoomStrip1.Name = "labelLeadZoomStrip1";
            this.labelLeadZoomStrip1.Size = new System.Drawing.Size(220, 45);
            this.labelLeadZoomStrip1.TabIndex = 46;
            this.labelLeadZoomStrip1.Text = "LEAD I";
            this.labelLeadZoomStrip1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panelLineUnderSampl1
            // 
            this.panelLineUnderSampl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelLineUnderSampl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLineUnderSampl1.Location = new System.Drawing.Point(0, 2165);
            this.panelLineUnderSampl1.Name = "panelLineUnderSampl1";
            this.panelLineUnderSampl1.Size = new System.Drawing.Size(2336, 2);
            this.panelLineUnderSampl1.TabIndex = 20;
            // 
            // panel142
            // 
            this.panel142.Controls.Add(this.labelEvent1Date);
            this.panel142.Controls.Add(this.label104);
            this.panel142.Controls.Add(this.labelEvent1Time);
            this.panel142.Controls.Add(this.panel222);
            this.panel142.Controls.Add(this.panel221);
            this.panel142.Controls.Add(this.panel220);
            this.panel142.Controls.Add(this.panel219);
            this.panel142.Controls.Add(this.panel218);
            this.panel142.Controls.Add(this.labelFindingsRythm);
            this.panel142.Controls.Add(this.labelFindingsClasification);
            this.panel142.Controls.Add(this.panel217);
            this.panel142.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel142.Location = new System.Drawing.Point(0, 2104);
            this.panel142.Name = "panel142";
            this.panel142.Size = new System.Drawing.Size(2336, 61);
            this.panel142.TabIndex = 19;
            // 
            // labelEvent1Date
            // 
            this.labelEvent1Date.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelEvent1Date.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEvent1Date.Location = new System.Drawing.Point(1763, 0);
            this.labelEvent1Date.Name = "labelEvent1Date";
            this.labelEvent1Date.Size = new System.Drawing.Size(273, 61);
            this.labelEvent1Date.TabIndex = 14;
            this.labelEvent1Date.Text = "11/23/2016";
            this.labelEvent1Date.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label104
            // 
            this.label104.Dock = System.Windows.Forms.DockStyle.Right;
            this.label104.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label104.Location = new System.Drawing.Point(2036, 0);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(27, 61);
            this.label104.TabIndex = 13;
            this.label104.Text = "-";
            this.label104.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelEvent1Time
            // 
            this.labelEvent1Time.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelEvent1Time.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEvent1Time.Location = new System.Drawing.Point(2063, 0);
            this.labelEvent1Time.Name = "labelEvent1Time";
            this.labelEvent1Time.Size = new System.Drawing.Size(273, 61);
            this.labelEvent1Time.TabIndex = 11;
            this.labelEvent1Time.Text = "10:15:10 AM";
            this.labelEvent1Time.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel222
            // 
            this.panel222.AutoSize = true;
            this.panel222.Controls.Add(this.labelEvent1NrSample);
            this.panel222.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel222.Location = new System.Drawing.Point(1612, 0);
            this.panel222.Name = "panel222";
            this.panel222.Size = new System.Drawing.Size(66, 61);
            this.panel222.TabIndex = 5;
            this.panel222.Visible = false;
            // 
            // labelEvent1NrSample
            // 
            this.labelEvent1NrSample.AutoSize = true;
            this.labelEvent1NrSample.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelEvent1NrSample.Font = new System.Drawing.Font("Verdana", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEvent1NrSample.Location = new System.Drawing.Point(0, 0);
            this.labelEvent1NrSample.Name = "labelEvent1NrSample";
            this.labelEvent1NrSample.Size = new System.Drawing.Size(66, 65);
            this.labelEvent1NrSample.TabIndex = 3;
            this.labelEvent1NrSample.Text = "1";
            this.labelEvent1NrSample.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel221
            // 
            this.panel221.Controls.Add(this.labelSample1Nr);
            this.panel221.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel221.Location = new System.Drawing.Point(1550, 0);
            this.panel221.Name = "panel221";
            this.panel221.Size = new System.Drawing.Size(62, 61);
            this.panel221.TabIndex = 4;
            this.panel221.Visible = false;
            // 
            // labelSample1Nr
            // 
            this.labelSample1Nr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSample1Nr.Font = new System.Drawing.Font("Verdana", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSample1Nr.Location = new System.Drawing.Point(0, 0);
            this.labelSample1Nr.Name = "labelSample1Nr";
            this.labelSample1Nr.Size = new System.Drawing.Size(62, 61);
            this.labelSample1Nr.TabIndex = 3;
            this.labelSample1Nr.Text = "Sample";
            this.labelSample1Nr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel220
            // 
            this.panel220.Controls.Add(this.label98);
            this.panel220.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel220.Location = new System.Drawing.Point(1503, 0);
            this.panel220.Name = "panel220";
            this.panel220.Size = new System.Drawing.Size(47, 61);
            this.panel220.TabIndex = 3;
            this.panel220.Visible = false;
            // 
            // label98
            // 
            this.label98.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label98.Font = new System.Drawing.Font("Verdana", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label98.Location = new System.Drawing.Point(0, 0);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(47, 61);
            this.label98.TabIndex = 3;
            this.label98.Text = "-";
            this.label98.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel219
            // 
            this.panel219.AutoSize = true;
            this.panel219.Controls.Add(this.labelEvent1NrRes);
            this.panel219.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel219.Location = new System.Drawing.Point(1361, 0);
            this.panel219.Name = "panel219";
            this.panel219.Size = new System.Drawing.Size(142, 61);
            this.panel219.TabIndex = 2;
            this.panel219.Visible = false;
            // 
            // labelEvent1NrRes
            // 
            this.labelEvent1NrRes.AutoSize = true;
            this.labelEvent1NrRes.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelEvent1NrRes.Font = new System.Drawing.Font("Verdana", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEvent1NrRes.Location = new System.Drawing.Point(0, 0);
            this.labelEvent1NrRes.Name = "labelEvent1NrRes";
            this.labelEvent1NrRes.Size = new System.Drawing.Size(142, 65);
            this.labelEvent1NrRes.TabIndex = 2;
            this.labelEvent1NrRes.Text = "175";
            this.labelEvent1NrRes.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel218
            // 
            this.panel218.Controls.Add(this.labelEvent1Nr);
            this.panel218.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel218.Location = new System.Drawing.Point(1276, 0);
            this.panel218.Name = "panel218";
            this.panel218.Size = new System.Drawing.Size(85, 61);
            this.panel218.TabIndex = 1;
            this.panel218.Visible = false;
            // 
            // labelEvent1Nr
            // 
            this.labelEvent1Nr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelEvent1Nr.Font = new System.Drawing.Font("Verdana", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEvent1Nr.Location = new System.Drawing.Point(0, 0);
            this.labelEvent1Nr.Name = "labelEvent1Nr";
            this.labelEvent1Nr.Size = new System.Drawing.Size(85, 61);
            this.labelEvent1Nr.TabIndex = 1;
            this.labelEvent1Nr.Text = "Event #";
            this.labelEvent1Nr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelFindingsRythm
            // 
            this.labelFindingsRythm.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelFindingsRythm.Font = new System.Drawing.Font("Verdana", 27.75F);
            this.labelFindingsRythm.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.labelFindingsRythm.Location = new System.Drawing.Point(234, 0);
            this.labelFindingsRythm.Name = "labelFindingsRythm";
            this.labelFindingsRythm.Size = new System.Drawing.Size(1042, 61);
            this.labelFindingsRythm.TabIndex = 16;
            this.labelFindingsRythm.Text = "xxxxxxx";
            this.labelFindingsRythm.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // labelFindingsClasification
            // 
            this.labelFindingsClasification.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelFindingsClasification.Font = new System.Drawing.Font("Verdana", 39.75F, System.Drawing.FontStyle.Bold);
            this.labelFindingsClasification.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelFindingsClasification.Location = new System.Drawing.Point(22, 0);
            this.labelFindingsClasification.Name = "labelFindingsClasification";
            this.labelFindingsClasification.Size = new System.Drawing.Size(212, 61);
            this.labelFindingsClasification.TabIndex = 15;
            this.labelFindingsClasification.Text = "Other";
            this.labelFindingsClasification.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel217
            // 
            this.panel217.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel217.Location = new System.Drawing.Point(0, 0);
            this.panel217.Name = "panel217";
            this.panel217.Size = new System.Drawing.Size(22, 61);
            this.panel217.TabIndex = 0;
            // 
            // panelEventStats
            // 
            this.panelEventStats.Controls.Add(this.panel162);
            this.panelEventStats.Controls.Add(this.panel161);
            this.panelEventStats.Controls.Add(this.panel160);
            this.panelEventStats.Controls.Add(this.panel121);
            this.panelEventStats.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelEventStats.Location = new System.Drawing.Point(0, 1649);
            this.panelEventStats.Name = "panelEventStats";
            this.panelEventStats.Size = new System.Drawing.Size(2336, 455);
            this.panelEventStats.TabIndex = 18;
            // 
            // panel162
            // 
            this.panel162.Controls.Add(this.panel215);
            this.panel162.Controls.Add(this.panel209);
            this.panel162.Controls.Add(this.panel203);
            this.panel162.Controls.Add(this.panel197);
            this.panel162.Controls.Add(this.panel191);
            this.panel162.Controls.Add(this.panel122);
            this.panel162.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel162.Location = new System.Drawing.Point(1484, 0);
            this.panel162.Name = "panel162";
            this.panel162.Size = new System.Drawing.Size(822, 455);
            this.panel162.TabIndex = 4;
            // 
            // panel215
            // 
            this.panel215.Controls.Add(this.panel216);
            this.panel215.Controls.Add(this.panel213);
            this.panel215.Controls.Add(this.panel214);
            this.panel215.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel215.Location = new System.Drawing.Point(0, 260);
            this.panel215.Name = "panel215";
            this.panel215.Size = new System.Drawing.Size(822, 52);
            this.panel215.TabIndex = 5;
            // 
            // panel216
            // 
            this.panel216.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel216.Location = new System.Drawing.Point(731, 0);
            this.panel216.Name = "panel216";
            this.panel216.Size = new System.Drawing.Size(91, 52);
            this.panel216.TabIndex = 4;
            // 
            // panel213
            // 
            this.panel213.Controls.Add(this.labelTechnicianMeasurement);
            this.panel213.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel213.Location = new System.Drawing.Point(272, 0);
            this.panel213.Name = "panel213";
            this.panel213.Size = new System.Drawing.Size(285, 52);
            this.panel213.TabIndex = 1;
            // 
            // labelTechnicianMeasurement
            // 
            this.labelTechnicianMeasurement.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelTechnicianMeasurement.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTechnicianMeasurement.Location = new System.Drawing.Point(0, 0);
            this.labelTechnicianMeasurement.Name = "labelTechnicianMeasurement";
            this.labelTechnicianMeasurement.Size = new System.Drawing.Size(285, 52);
            this.labelTechnicianMeasurement.TabIndex = 7;
            this.labelTechnicianMeasurement.Text = "SHJ";
            this.labelTechnicianMeasurement.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel214
            // 
            this.panel214.Controls.Add(this.labelTechMeasure);
            this.panel214.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel214.Location = new System.Drawing.Point(0, 0);
            this.panel214.Name = "panel214";
            this.panel214.Size = new System.Drawing.Size(272, 52);
            this.panel214.TabIndex = 0;
            // 
            // labelTechMeasure
            // 
            this.labelTechMeasure.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelTechMeasure.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTechMeasure.Location = new System.Drawing.Point(0, 0);
            this.labelTechMeasure.Name = "labelTechMeasure";
            this.labelTechMeasure.Size = new System.Drawing.Size(266, 52);
            this.labelTechMeasure.TabIndex = 1;
            this.labelTechMeasure.Text = "Technician:";
            this.labelTechMeasure.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel209
            // 
            this.panel209.Controls.Add(this.labelQTmeasureSI);
            this.panel209.Controls.Add(this.panel205);
            this.panel209.Controls.Add(this.panel206);
            this.panel209.Controls.Add(this.panel207);
            this.panel209.Controls.Add(this.panel208);
            this.panel209.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel209.Location = new System.Drawing.Point(0, 208);
            this.panel209.Name = "panel209";
            this.panel209.Size = new System.Drawing.Size(822, 52);
            this.panel209.TabIndex = 4;
            // 
            // labelQTmeasureSI
            // 
            this.labelQTmeasureSI.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelQTmeasureSI.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQTmeasureSI.Location = new System.Drawing.Point(680, 0);
            this.labelQTmeasureSI.Name = "labelQTmeasureSI";
            this.labelQTmeasureSI.Size = new System.Drawing.Size(142, 52);
            this.labelQTmeasureSI.TabIndex = 6;
            this.labelQTmeasureSI.Text = "(s)";
            this.labelQTmeasureSI.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel205
            // 
            this.panel205.Controls.Add(this.labelQTAvg);
            this.panel205.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel205.Location = new System.Drawing.Point(544, 0);
            this.panel205.Name = "panel205";
            this.panel205.Size = new System.Drawing.Size(150, 52);
            this.panel205.TabIndex = 3;
            // 
            // labelQTAvg
            // 
            this.labelQTAvg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelQTAvg.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQTAvg.Location = new System.Drawing.Point(0, 0);
            this.labelQTAvg.Name = "labelQTAvg";
            this.labelQTAvg.Size = new System.Drawing.Size(150, 52);
            this.labelQTAvg.TabIndex = 7;
            this.labelQTAvg.Text = "76";
            this.labelQTAvg.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel206
            // 
            this.panel206.Controls.Add(this.labelQTMax);
            this.panel206.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel206.Location = new System.Drawing.Point(409, 0);
            this.panel206.Name = "panel206";
            this.panel206.Size = new System.Drawing.Size(135, 52);
            this.panel206.TabIndex = 2;
            // 
            // labelQTMax
            // 
            this.labelQTMax.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelQTMax.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQTMax.Location = new System.Drawing.Point(0, 0);
            this.labelQTMax.Name = "labelQTMax";
            this.labelQTMax.Size = new System.Drawing.Size(140, 52);
            this.labelQTMax.TabIndex = 7;
            this.labelQTMax.Text = "76";
            this.labelQTMax.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel207
            // 
            this.panel207.Controls.Add(this.labelQTmin);
            this.panel207.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel207.Location = new System.Drawing.Point(272, 0);
            this.panel207.Name = "panel207";
            this.panel207.Size = new System.Drawing.Size(137, 52);
            this.panel207.TabIndex = 1;
            // 
            // labelQTmin
            // 
            this.labelQTmin.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelQTmin.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQTmin.Location = new System.Drawing.Point(0, 0);
            this.labelQTmin.Name = "labelQTmin";
            this.labelQTmin.Size = new System.Drawing.Size(140, 52);
            this.labelQTmin.TabIndex = 7;
            this.labelQTmin.Text = "76";
            this.labelQTmin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel208
            // 
            this.panel208.Controls.Add(this.labelQTMeasure);
            this.panel208.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel208.Location = new System.Drawing.Point(0, 0);
            this.panel208.Name = "panel208";
            this.panel208.Size = new System.Drawing.Size(272, 52);
            this.panel208.TabIndex = 0;
            // 
            // labelQTMeasure
            // 
            this.labelQTMeasure.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelQTMeasure.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQTMeasure.Location = new System.Drawing.Point(0, 0);
            this.labelQTMeasure.Name = "labelQTMeasure";
            this.labelQTMeasure.Size = new System.Drawing.Size(115, 52);
            this.labelQTMeasure.TabIndex = 1;
            this.labelQTMeasure.Text = "QT:";
            this.labelQTMeasure.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel203
            // 
            this.panel203.Controls.Add(this.labelQRSmeasureSI);
            this.panel203.Controls.Add(this.panel199);
            this.panel203.Controls.Add(this.panel200);
            this.panel203.Controls.Add(this.panel201);
            this.panel203.Controls.Add(this.panel202);
            this.panel203.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel203.Location = new System.Drawing.Point(0, 156);
            this.panel203.Name = "panel203";
            this.panel203.Size = new System.Drawing.Size(822, 52);
            this.panel203.TabIndex = 3;
            // 
            // labelQRSmeasureSI
            // 
            this.labelQRSmeasureSI.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelQRSmeasureSI.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQRSmeasureSI.Location = new System.Drawing.Point(680, 0);
            this.labelQRSmeasureSI.Name = "labelQRSmeasureSI";
            this.labelQRSmeasureSI.Size = new System.Drawing.Size(142, 52);
            this.labelQRSmeasureSI.TabIndex = 6;
            this.labelQRSmeasureSI.Text = "(s)";
            this.labelQRSmeasureSI.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel199
            // 
            this.panel199.Controls.Add(this.labelQRSAvg);
            this.panel199.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel199.Location = new System.Drawing.Point(544, 0);
            this.panel199.Name = "panel199";
            this.panel199.Size = new System.Drawing.Size(147, 52);
            this.panel199.TabIndex = 3;
            // 
            // labelQRSAvg
            // 
            this.labelQRSAvg.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelQRSAvg.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQRSAvg.Location = new System.Drawing.Point(0, 0);
            this.labelQRSAvg.Name = "labelQRSAvg";
            this.labelQRSAvg.Size = new System.Drawing.Size(144, 52);
            this.labelQRSAvg.TabIndex = 7;
            this.labelQRSAvg.Text = "76";
            this.labelQRSAvg.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel200
            // 
            this.panel200.Controls.Add(this.labelQRSMax);
            this.panel200.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel200.Location = new System.Drawing.Point(409, 0);
            this.panel200.Name = "panel200";
            this.panel200.Size = new System.Drawing.Size(135, 52);
            this.panel200.TabIndex = 2;
            // 
            // labelQRSMax
            // 
            this.labelQRSMax.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelQRSMax.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQRSMax.Location = new System.Drawing.Point(0, 0);
            this.labelQRSMax.Name = "labelQRSMax";
            this.labelQRSMax.Size = new System.Drawing.Size(140, 52);
            this.labelQRSMax.TabIndex = 7;
            this.labelQRSMax.Text = "76";
            this.labelQRSMax.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel201
            // 
            this.panel201.Controls.Add(this.labelQRSmin);
            this.panel201.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel201.Location = new System.Drawing.Point(272, 0);
            this.panel201.Name = "panel201";
            this.panel201.Size = new System.Drawing.Size(137, 52);
            this.panel201.TabIndex = 1;
            // 
            // labelQRSmin
            // 
            this.labelQRSmin.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelQRSmin.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQRSmin.Location = new System.Drawing.Point(0, 0);
            this.labelQRSmin.Name = "labelQRSmin";
            this.labelQRSmin.Size = new System.Drawing.Size(140, 52);
            this.labelQRSmin.TabIndex = 7;
            this.labelQRSmin.Text = "76";
            this.labelQRSmin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel202
            // 
            this.panel202.Controls.Add(this.labelQRSMeasure);
            this.panel202.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel202.Location = new System.Drawing.Point(0, 0);
            this.panel202.Name = "panel202";
            this.panel202.Size = new System.Drawing.Size(272, 52);
            this.panel202.TabIndex = 0;
            // 
            // labelQRSMeasure
            // 
            this.labelQRSMeasure.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelQRSMeasure.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQRSMeasure.Location = new System.Drawing.Point(0, 0);
            this.labelQRSMeasure.Name = "labelQRSMeasure";
            this.labelQRSMeasure.Size = new System.Drawing.Size(143, 52);
            this.labelQRSMeasure.TabIndex = 1;
            this.labelQRSMeasure.Text = "QRS:";
            this.labelQRSMeasure.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel197
            // 
            this.panel197.Controls.Add(this.labelPRmeasureSI);
            this.panel197.Controls.Add(this.panel193);
            this.panel197.Controls.Add(this.panel194);
            this.panel197.Controls.Add(this.panel195);
            this.panel197.Controls.Add(this.panel196);
            this.panel197.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel197.Location = new System.Drawing.Point(0, 104);
            this.panel197.Name = "panel197";
            this.panel197.Size = new System.Drawing.Size(822, 52);
            this.panel197.TabIndex = 2;
            // 
            // labelPRmeasureSI
            // 
            this.labelPRmeasureSI.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelPRmeasureSI.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPRmeasureSI.Location = new System.Drawing.Point(680, 0);
            this.labelPRmeasureSI.Name = "labelPRmeasureSI";
            this.labelPRmeasureSI.Size = new System.Drawing.Size(142, 52);
            this.labelPRmeasureSI.TabIndex = 6;
            this.labelPRmeasureSI.Text = "(s)";
            this.labelPRmeasureSI.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel193
            // 
            this.panel193.Controls.Add(this.labelPRAvg);
            this.panel193.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel193.Location = new System.Drawing.Point(544, 0);
            this.panel193.Name = "panel193";
            this.panel193.Size = new System.Drawing.Size(147, 52);
            this.panel193.TabIndex = 3;
            // 
            // labelPRAvg
            // 
            this.labelPRAvg.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPRAvg.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPRAvg.Location = new System.Drawing.Point(0, 0);
            this.labelPRAvg.Name = "labelPRAvg";
            this.labelPRAvg.Size = new System.Drawing.Size(144, 52);
            this.labelPRAvg.TabIndex = 7;
            this.labelPRAvg.Text = "0,076";
            this.labelPRAvg.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel194
            // 
            this.panel194.Controls.Add(this.labelPRMax);
            this.panel194.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel194.Location = new System.Drawing.Point(409, 0);
            this.panel194.Name = "panel194";
            this.panel194.Size = new System.Drawing.Size(135, 52);
            this.panel194.TabIndex = 2;
            // 
            // labelPRMax
            // 
            this.labelPRMax.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPRMax.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPRMax.Location = new System.Drawing.Point(0, 0);
            this.labelPRMax.Name = "labelPRMax";
            this.labelPRMax.Size = new System.Drawing.Size(140, 52);
            this.labelPRMax.TabIndex = 7;
            this.labelPRMax.Text = "76";
            this.labelPRMax.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel195
            // 
            this.panel195.Controls.Add(this.labelPRmin);
            this.panel195.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel195.Location = new System.Drawing.Point(272, 0);
            this.panel195.Name = "panel195";
            this.panel195.Size = new System.Drawing.Size(137, 52);
            this.panel195.TabIndex = 1;
            // 
            // labelPRmin
            // 
            this.labelPRmin.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPRmin.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPRmin.Location = new System.Drawing.Point(0, 0);
            this.labelPRmin.Name = "labelPRmin";
            this.labelPRmin.Size = new System.Drawing.Size(140, 52);
            this.labelPRmin.TabIndex = 7;
            this.labelPRmin.Text = "0,076";
            this.labelPRmin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel196
            // 
            this.panel196.Controls.Add(this.labelPRMeasure);
            this.panel196.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel196.Location = new System.Drawing.Point(0, 0);
            this.panel196.Name = "panel196";
            this.panel196.Size = new System.Drawing.Size(272, 52);
            this.panel196.TabIndex = 0;
            // 
            // labelPRMeasure
            // 
            this.labelPRMeasure.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPRMeasure.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPRMeasure.Location = new System.Drawing.Point(0, 0);
            this.labelPRMeasure.Name = "labelPRMeasure";
            this.labelPRMeasure.Size = new System.Drawing.Size(95, 52);
            this.labelPRMeasure.TabIndex = 1;
            this.labelPRMeasure.Text = "PR:";
            this.labelPRMeasure.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel191
            // 
            this.panel191.Controls.Add(this.labelRatemeasureSI);
            this.panel191.Controls.Add(this.panel187);
            this.panel191.Controls.Add(this.panel188);
            this.panel191.Controls.Add(this.panel189);
            this.panel191.Controls.Add(this.panel190);
            this.panel191.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel191.Location = new System.Drawing.Point(0, 52);
            this.panel191.Name = "panel191";
            this.panel191.Size = new System.Drawing.Size(822, 52);
            this.panel191.TabIndex = 1;
            // 
            // labelRatemeasureSI
            // 
            this.labelRatemeasureSI.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelRatemeasureSI.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRatemeasureSI.Location = new System.Drawing.Point(682, 0);
            this.labelRatemeasureSI.Name = "labelRatemeasureSI";
            this.labelRatemeasureSI.Size = new System.Drawing.Size(140, 52);
            this.labelRatemeasureSI.TabIndex = 5;
            this.labelRatemeasureSI.Text = "(bpm)";
            this.labelRatemeasureSI.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelRatemeasureSI.Click += new System.EventHandler(this.labelRatemeasureSI_Click);
            // 
            // panel187
            // 
            this.panel187.Controls.Add(this.labelRateAVG);
            this.panel187.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel187.Location = new System.Drawing.Point(544, 0);
            this.panel187.Name = "panel187";
            this.panel187.Size = new System.Drawing.Size(140, 52);
            this.panel187.TabIndex = 3;
            // 
            // labelRateAVG
            // 
            this.labelRateAVG.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelRateAVG.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRateAVG.Location = new System.Drawing.Point(0, 0);
            this.labelRateAVG.Name = "labelRateAVG";
            this.labelRateAVG.Size = new System.Drawing.Size(140, 52);
            this.labelRateAVG.TabIndex = 7;
            this.labelRateAVG.Text = "76";
            this.labelRateAVG.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel188
            // 
            this.panel188.Controls.Add(this.labelRateMax);
            this.panel188.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel188.Location = new System.Drawing.Point(409, 0);
            this.panel188.Name = "panel188";
            this.panel188.Size = new System.Drawing.Size(135, 52);
            this.panel188.TabIndex = 2;
            // 
            // labelRateMax
            // 
            this.labelRateMax.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelRateMax.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRateMax.Location = new System.Drawing.Point(0, 0);
            this.labelRateMax.Name = "labelRateMax";
            this.labelRateMax.Size = new System.Drawing.Size(140, 52);
            this.labelRateMax.TabIndex = 7;
            this.labelRateMax.Text = "76";
            this.labelRateMax.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel189
            // 
            this.panel189.Controls.Add(this.labelRateMin);
            this.panel189.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel189.Location = new System.Drawing.Point(272, 0);
            this.panel189.Name = "panel189";
            this.panel189.Size = new System.Drawing.Size(137, 52);
            this.panel189.TabIndex = 1;
            // 
            // labelRateMin
            // 
            this.labelRateMin.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelRateMin.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRateMin.Location = new System.Drawing.Point(0, 0);
            this.labelRateMin.Name = "labelRateMin";
            this.labelRateMin.Size = new System.Drawing.Size(140, 52);
            this.labelRateMin.TabIndex = 6;
            this.labelRateMin.Text = "76";
            this.labelRateMin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel190
            // 
            this.panel190.Controls.Add(this.labelRateMeasure);
            this.panel190.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel190.Location = new System.Drawing.Point(0, 0);
            this.panel190.Name = "panel190";
            this.panel190.Size = new System.Drawing.Size(272, 52);
            this.panel190.TabIndex = 0;
            // 
            // labelRateMeasure
            // 
            this.labelRateMeasure.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelRateMeasure.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRateMeasure.Location = new System.Drawing.Point(0, 0);
            this.labelRateMeasure.Name = "labelRateMeasure";
            this.labelRateMeasure.Size = new System.Drawing.Size(145, 52);
            this.labelRateMeasure.TabIndex = 0;
            this.labelRateMeasure.Text = "Rate:";
            this.labelRateMeasure.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel122
            // 
            this.panel122.Controls.Add(this.panel186);
            this.panel122.Controls.Add(this.panel185);
            this.panel122.Controls.Add(this.panel184);
            this.panel122.Controls.Add(this.panel183);
            this.panel122.Controls.Add(this.panel182);
            this.panel122.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel122.Location = new System.Drawing.Point(0, 0);
            this.panel122.Name = "panel122";
            this.panel122.Size = new System.Drawing.Size(822, 52);
            this.panel122.TabIndex = 0;
            // 
            // panel186
            // 
            this.panel186.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel186.Location = new System.Drawing.Point(731, 0);
            this.panel186.Name = "panel186";
            this.panel186.Size = new System.Drawing.Size(91, 52);
            this.panel186.TabIndex = 4;
            // 
            // panel185
            // 
            this.panel185.Controls.Add(this.labelAVGMeasurement);
            this.panel185.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel185.Location = new System.Drawing.Point(544, 0);
            this.panel185.Name = "panel185";
            this.panel185.Size = new System.Drawing.Size(112, 52);
            this.panel185.TabIndex = 3;
            // 
            // labelAVGMeasurement
            // 
            this.labelAVGMeasurement.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelAVGMeasurement.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAVGMeasurement.Location = new System.Drawing.Point(0, 0);
            this.labelAVGMeasurement.Name = "labelAVGMeasurement";
            this.labelAVGMeasurement.Size = new System.Drawing.Size(140, 52);
            this.labelAVGMeasurement.TabIndex = 1;
            this.labelAVGMeasurement.Text = "Avg";
            this.labelAVGMeasurement.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel184
            // 
            this.panel184.Controls.Add(this.labelMaxMeasurement);
            this.panel184.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel184.Location = new System.Drawing.Point(409, 0);
            this.panel184.Name = "panel184";
            this.panel184.Size = new System.Drawing.Size(135, 52);
            this.panel184.TabIndex = 2;
            // 
            // labelMaxMeasurement
            // 
            this.labelMaxMeasurement.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelMaxMeasurement.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMaxMeasurement.Location = new System.Drawing.Point(0, 0);
            this.labelMaxMeasurement.Name = "labelMaxMeasurement";
            this.labelMaxMeasurement.Size = new System.Drawing.Size(140, 52);
            this.labelMaxMeasurement.TabIndex = 1;
            this.labelMaxMeasurement.Text = "Max";
            this.labelMaxMeasurement.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel183
            // 
            this.panel183.Controls.Add(this.labelMinMeasurement);
            this.panel183.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel183.Location = new System.Drawing.Point(272, 0);
            this.panel183.Name = "panel183";
            this.panel183.Size = new System.Drawing.Size(137, 52);
            this.panel183.TabIndex = 1;
            // 
            // labelMinMeasurement
            // 
            this.labelMinMeasurement.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelMinMeasurement.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMinMeasurement.Location = new System.Drawing.Point(0, 0);
            this.labelMinMeasurement.Name = "labelMinMeasurement";
            this.labelMinMeasurement.Size = new System.Drawing.Size(140, 52);
            this.labelMinMeasurement.TabIndex = 0;
            this.labelMinMeasurement.Text = "Min";
            this.labelMinMeasurement.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel182
            // 
            this.panel182.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel182.Location = new System.Drawing.Point(0, 0);
            this.panel182.Name = "panel182";
            this.panel182.Size = new System.Drawing.Size(272, 52);
            this.panel182.TabIndex = 0;
            // 
            // panel161
            // 
            this.panel161.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel161.Location = new System.Drawing.Point(2306, 0);
            this.panel161.Name = "panel161";
            this.panel161.Size = new System.Drawing.Size(30, 455);
            this.panel161.TabIndex = 3;
            // 
            // panel160
            // 
            this.panel160.Controls.Add(this.panel28);
            this.panel160.Controls.Add(this.panel178);
            this.panel160.Controls.Add(this.panel175);
            this.panel160.Controls.Add(this.panel172);
            this.panel160.Controls.Add(this.panel167);
            this.panel160.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel160.Location = new System.Drawing.Point(21, 0);
            this.panel160.Name = "panel160";
            this.panel160.Size = new System.Drawing.Size(1406, 455);
            this.panel160.TabIndex = 2;
            // 
            // panel28
            // 
            this.panel28.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel28.Controls.Add(this.labelFindingsRemark);
            this.panel28.Controls.Add(this.panel45);
            this.panel28.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel28.Location = new System.Drawing.Point(0, 254);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(1406, 200);
            this.panel28.TabIndex = 5;
            // 
            // labelFindingsRemark
            // 
            this.labelFindingsRemark.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelFindingsRemark.Font = new System.Drawing.Font("Arial", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFindingsRemark.Location = new System.Drawing.Point(429, 0);
            this.labelFindingsRemark.Name = "labelFindingsRemark";
            this.labelFindingsRemark.Size = new System.Drawing.Size(977, 200);
            this.labelFindingsRemark.TabIndex = 5;
            this.labelFindingsRemark.Text = "Atrial Fibrillation, with Occasional PVC\'s and many other findings that the Tech " +
    "can add a lot of text \r\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx \r\ny" +
    "yyyyyyyyyyyyyyyyyyyy\r\n\r\n";
            // 
            // panel45
            // 
            this.panel45.Controls.Add(this.panel46);
            this.panel45.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel45.Location = new System.Drawing.Point(0, 0);
            this.panel45.Name = "panel45";
            this.panel45.Size = new System.Drawing.Size(429, 200);
            this.panel45.TabIndex = 1;
            // 
            // panel46
            // 
            this.panel46.Controls.Add(this.label1);
            this.panel46.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel46.Location = new System.Drawing.Point(0, 0);
            this.panel46.Name = "panel46";
            this.panel46.Size = new System.Drawing.Size(429, 52);
            this.panel46.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(419, 52);
            this.label1.TabIndex = 1;
            this.label1.Text = "Findings:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel178
            // 
            this.panel178.Controls.Add(this.panel176);
            this.panel178.Controls.Add(this.panel177);
            this.panel178.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel178.Location = new System.Drawing.Point(0, 202);
            this.panel178.Name = "panel178";
            this.panel178.Size = new System.Drawing.Size(1406, 52);
            this.panel178.TabIndex = 3;
            // 
            // panel176
            // 
            this.panel176.Controls.Add(this.labelActivationType);
            this.panel176.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel176.Location = new System.Drawing.Point(429, 0);
            this.panel176.Name = "panel176";
            this.panel176.Size = new System.Drawing.Size(992, 52);
            this.panel176.TabIndex = 1;
            // 
            // labelActivationType
            // 
            this.labelActivationType.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelActivationType.Font = new System.Drawing.Font("Arial", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelActivationType.Location = new System.Drawing.Point(0, 0);
            this.labelActivationType.Name = "labelActivationType";
            this.labelActivationType.Size = new System.Drawing.Size(927, 52);
            this.labelActivationType.TabIndex = 3;
            this.labelActivationType.Text = "Patient / Recorder";
            this.labelActivationType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel177
            // 
            this.panel177.Controls.Add(this.labelActivationTypeHeader);
            this.panel177.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel177.Location = new System.Drawing.Point(0, 0);
            this.panel177.Name = "panel177";
            this.panel177.Size = new System.Drawing.Size(429, 52);
            this.panel177.TabIndex = 0;
            // 
            // labelActivationTypeHeader
            // 
            this.labelActivationTypeHeader.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelActivationTypeHeader.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelActivationTypeHeader.Location = new System.Drawing.Point(0, 0);
            this.labelActivationTypeHeader.Name = "labelActivationTypeHeader";
            this.labelActivationTypeHeader.Size = new System.Drawing.Size(423, 52);
            this.labelActivationTypeHeader.TabIndex = 1;
            this.labelActivationTypeHeader.Text = "Type of Activation:";
            this.labelActivationTypeHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel175
            // 
            this.panel175.Controls.Add(this.panel173);
            this.panel175.Controls.Add(this.panel174);
            this.panel175.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel175.Location = new System.Drawing.Point(0, 150);
            this.panel175.Name = "panel175";
            this.panel175.Size = new System.Drawing.Size(1406, 52);
            this.panel175.TabIndex = 2;
            // 
            // panel173
            // 
            this.panel173.Controls.Add(this.labelActivity);
            this.panel173.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel173.Location = new System.Drawing.Point(429, 0);
            this.panel173.Name = "panel173";
            this.panel173.Size = new System.Drawing.Size(992, 52);
            this.panel173.TabIndex = 1;
            // 
            // labelActivity
            // 
            this.labelActivity.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelActivity.Font = new System.Drawing.Font("Arial", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelActivity.Location = new System.Drawing.Point(0, 0);
            this.labelActivity.Name = "labelActivity";
            this.labelActivity.Size = new System.Drawing.Size(927, 52);
            this.labelActivity.TabIndex = 2;
            this.labelActivity.Text = "Walking up the Stairs";
            this.labelActivity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel174
            // 
            this.panel174.Controls.Add(this.labelActivityHeader);
            this.panel174.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel174.Location = new System.Drawing.Point(0, 0);
            this.panel174.Name = "panel174";
            this.panel174.Size = new System.Drawing.Size(429, 52);
            this.panel174.TabIndex = 0;
            // 
            // labelActivityHeader
            // 
            this.labelActivityHeader.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelActivityHeader.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelActivityHeader.Location = new System.Drawing.Point(0, 0);
            this.labelActivityHeader.Name = "labelActivityHeader";
            this.labelActivityHeader.Size = new System.Drawing.Size(213, 52);
            this.labelActivityHeader.TabIndex = 0;
            this.labelActivityHeader.Text = "Activity:";
            this.labelActivityHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel172
            // 
            this.panel172.Controls.Add(this.panel170);
            this.panel172.Controls.Add(this.panel171);
            this.panel172.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel172.Location = new System.Drawing.Point(0, 98);
            this.panel172.Name = "panel172";
            this.panel172.Size = new System.Drawing.Size(1406, 52);
            this.panel172.TabIndex = 1;
            // 
            // panel170
            // 
            this.panel170.Controls.Add(this.labelSymptoms);
            this.panel170.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel170.Location = new System.Drawing.Point(429, 0);
            this.panel170.Name = "panel170";
            this.panel170.Size = new System.Drawing.Size(992, 52);
            this.panel170.TabIndex = 1;
            // 
            // labelSymptoms
            // 
            this.labelSymptoms.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSymptoms.Font = new System.Drawing.Font("Arial", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSymptoms.Location = new System.Drawing.Point(0, 0);
            this.labelSymptoms.Name = "labelSymptoms";
            this.labelSymptoms.Size = new System.Drawing.Size(927, 52);
            this.labelSymptoms.TabIndex = 1;
            this.labelSymptoms.Text = "Palpitation";
            this.labelSymptoms.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel171
            // 
            this.panel171.Controls.Add(this.labelSymptomsHeader);
            this.panel171.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel171.Location = new System.Drawing.Point(0, 0);
            this.panel171.Name = "panel171";
            this.panel171.Size = new System.Drawing.Size(429, 52);
            this.panel171.TabIndex = 0;
            // 
            // labelSymptomsHeader
            // 
            this.labelSymptomsHeader.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSymptomsHeader.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSymptomsHeader.Location = new System.Drawing.Point(0, 0);
            this.labelSymptomsHeader.Name = "labelSymptomsHeader";
            this.labelSymptomsHeader.Size = new System.Drawing.Size(275, 52);
            this.labelSymptomsHeader.TabIndex = 0;
            this.labelSymptomsHeader.Text = "Symptoms:";
            this.labelSymptomsHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel167
            // 
            this.panel167.Controls.Add(this.panel169);
            this.panel167.Controls.Add(this.panel168);
            this.panel167.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel167.Location = new System.Drawing.Point(0, 0);
            this.panel167.Name = "panel167";
            this.panel167.Size = new System.Drawing.Size(1406, 98);
            this.panel167.TabIndex = 0;
            // 
            // panel169
            // 
            this.panel169.Controls.Add(this.labelICDcode);
            this.panel169.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel169.Location = new System.Drawing.Point(429, 0);
            this.panel169.Name = "panel169";
            this.panel169.Size = new System.Drawing.Size(992, 98);
            this.panel169.TabIndex = 1;
            // 
            // labelICDcode
            // 
            this.labelICDcode.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelICDcode.Font = new System.Drawing.Font("Arial", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelICDcode.Location = new System.Drawing.Point(0, 0);
            this.labelICDcode.Name = "labelICDcode";
            this.labelICDcode.Size = new System.Drawing.Size(927, 98);
            this.labelICDcode.TabIndex = 0;
            this.labelICDcode.Text = "ICD10";
            // 
            // panel168
            // 
            this.panel168.Controls.Add(this.labelDiagnosisHeader);
            this.panel168.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel168.Location = new System.Drawing.Point(0, 0);
            this.panel168.Name = "panel168";
            this.panel168.Size = new System.Drawing.Size(429, 98);
            this.panel168.TabIndex = 0;
            // 
            // labelDiagnosisHeader
            // 
            this.labelDiagnosisHeader.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelDiagnosisHeader.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDiagnosisHeader.Location = new System.Drawing.Point(0, 0);
            this.labelDiagnosisHeader.Name = "labelDiagnosisHeader";
            this.labelDiagnosisHeader.Size = new System.Drawing.Size(248, 98);
            this.labelDiagnosisHeader.TabIndex = 0;
            this.labelDiagnosisHeader.Text = "Diagnosis:";
            // 
            // panel121
            // 
            this.panel121.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel121.Location = new System.Drawing.Point(0, 0);
            this.panel121.Name = "panel121";
            this.panel121.Size = new System.Drawing.Size(21, 455);
            this.panel121.TabIndex = 1;
            // 
            // panelCurrentEventStats
            // 
            this.panelCurrentEventStats.Controls.Add(this.labelRecordNr);
            this.panelCurrentEventStats.Controls.Add(this.labelRecordNrText);
            this.panelCurrentEventStats.Controls.Add(this.panel163);
            this.panelCurrentEventStats.Controls.Add(this.panel164);
            this.panelCurrentEventStats.Controls.Add(this.panel165);
            this.panelCurrentEventStats.Controls.Add(this.panel166);
            this.panelCurrentEventStats.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCurrentEventStats.Location = new System.Drawing.Point(0, 1562);
            this.panelCurrentEventStats.Name = "panelCurrentEventStats";
            this.panelCurrentEventStats.Size = new System.Drawing.Size(2336, 87);
            this.panelCurrentEventStats.TabIndex = 17;
            // 
            // labelRecordNr
            // 
            this.labelRecordNr.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelRecordNr.Font = new System.Drawing.Font("Arial", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRecordNr.Location = new System.Drawing.Point(1009, 0);
            this.labelRecordNr.Name = "labelRecordNr";
            this.labelRecordNr.Size = new System.Drawing.Size(202, 87);
            this.labelRecordNr.TabIndex = 10;
            this.labelRecordNr.Text = "1234";
            this.labelRecordNr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelRecordNrText
            // 
            this.labelRecordNrText.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelRecordNrText.Font = new System.Drawing.Font("Arial", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRecordNrText.Location = new System.Drawing.Point(926, 0);
            this.labelRecordNrText.Name = "labelRecordNrText";
            this.labelRecordNrText.Size = new System.Drawing.Size(83, 87);
            this.labelRecordNrText.TabIndex = 9;
            this.labelRecordNrText.Text = "R# ";
            this.labelRecordNrText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel163
            // 
            this.panel163.Controls.Add(this.labelMeasurement);
            this.panel163.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel163.Location = new System.Drawing.Point(1484, 0);
            this.panel163.Name = "panel163";
            this.panel163.Size = new System.Drawing.Size(822, 87);
            this.panel163.TabIndex = 8;
            // 
            // labelMeasurement
            // 
            this.labelMeasurement.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelMeasurement.Font = new System.Drawing.Font("Verdana", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMeasurement.Location = new System.Drawing.Point(0, 0);
            this.labelMeasurement.Name = "labelMeasurement";
            this.labelMeasurement.Size = new System.Drawing.Size(601, 87);
            this.labelMeasurement.TabIndex = 1;
            this.labelMeasurement.Text = "Measurements";
            this.labelMeasurement.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel164
            // 
            this.panel164.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel164.Location = new System.Drawing.Point(2306, 0);
            this.panel164.Name = "panel164";
            this.panel164.Size = new System.Drawing.Size(30, 87);
            this.panel164.TabIndex = 7;
            // 
            // panel165
            // 
            this.panel165.Controls.Add(this.labelCurrentEvent);
            this.panel165.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel165.Location = new System.Drawing.Point(21, 0);
            this.panel165.Name = "panel165";
            this.panel165.Size = new System.Drawing.Size(905, 87);
            this.panel165.TabIndex = 6;
            // 
            // labelCurrentEvent
            // 
            this.labelCurrentEvent.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelCurrentEvent.Font = new System.Drawing.Font("Verdana", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCurrentEvent.Location = new System.Drawing.Point(0, 0);
            this.labelCurrentEvent.Name = "labelCurrentEvent";
            this.labelCurrentEvent.Size = new System.Drawing.Size(632, 87);
            this.labelCurrentEvent.TabIndex = 0;
            this.labelCurrentEvent.Text = "Current Event";
            this.labelCurrentEvent.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel166
            // 
            this.panel166.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel166.Location = new System.Drawing.Point(0, 0);
            this.panel166.Name = "panel166";
            this.panel166.Size = new System.Drawing.Size(21, 87);
            this.panel166.TabIndex = 5;
            // 
            // panel120
            // 
            this.panel120.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel120.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel120.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel120.Location = new System.Drawing.Point(0, 1521);
            this.panel120.Name = "panel120";
            this.panel120.Size = new System.Drawing.Size(2336, 41);
            this.panel120.TabIndex = 16;
            // 
            // panel6
            // 
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 1440);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(2336, 81);
            this.panel6.TabIndex = 15;
            // 
            // panelPreviousTransm
            // 
            this.panelPreviousTransm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelPreviousTransm.Controls.Add(this.panelTablePrevious3Trans);
            this.panelPreviousTransm.Controls.Add(this.panel71);
            this.panelPreviousTransm.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelPreviousTransm.Location = new System.Drawing.Point(0, 1262);
            this.panelPreviousTransm.Name = "panelPreviousTransm";
            this.panelPreviousTransm.Size = new System.Drawing.Size(2336, 178);
            this.panelPreviousTransm.TabIndex = 13;
            this.panelPreviousTransm.Visible = false;
            // 
            // panelTablePrevious3Trans
            // 
            this.panelTablePrevious3Trans.Controls.Add(this.panel159);
            this.panelTablePrevious3Trans.Controls.Add(this.panel75);
            this.panelTablePrevious3Trans.Controls.Add(this.panel72);
            this.panelTablePrevious3Trans.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTablePrevious3Trans.Location = new System.Drawing.Point(0, 81);
            this.panelTablePrevious3Trans.Name = "panelTablePrevious3Trans";
            this.panelTablePrevious3Trans.Size = new System.Drawing.Size(2334, 172);
            this.panelTablePrevious3Trans.TabIndex = 15;
            // 
            // panel159
            // 
            this.panel159.Controls.Add(this.dataGridViewPrevThreeTX);
            this.panel159.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel159.Location = new System.Drawing.Point(20, 0);
            this.panel159.Name = "panel159";
            this.panel159.Size = new System.Drawing.Size(2291, 172);
            this.panel159.TabIndex = 3;
            // 
            // dataGridViewPrevThreeTX
            // 
            this.dataGridViewPrevThreeTX.AllowUserToAddRows = false;
            this.dataGridViewPrevThreeTX.AllowUserToDeleteRows = false;
            this.dataGridViewPrevThreeTX.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewPrevThreeTX.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewPrevThreeTX.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridViewPrevThreeTX.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewPrevThreeTX.CausesValidation = false;
            this.dataGridViewPrevThreeTX.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridViewPrevThreeTX.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPrevThreeTX.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnEventID,
            this.ColumnEventDate,
            this.ColumnTimeOfEvent,
            this.ColumnSymptoms,
            this.ColumnFindings});
            this.dataGridViewPrevThreeTX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewPrevThreeTX.GridColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.dataGridViewPrevThreeTX.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewPrevThreeTX.MultiSelect = false;
            this.dataGridViewPrevThreeTX.Name = "dataGridViewPrevThreeTX";
            this.dataGridViewPrevThreeTX.ReadOnly = true;
            this.dataGridViewPrevThreeTX.RowTemplate.Height = 33;
            this.dataGridViewPrevThreeTX.Size = new System.Drawing.Size(2291, 172);
            this.dataGridViewPrevThreeTX.TabIndex = 2;
            // 
            // ColumnEventID
            // 
            this.ColumnEventID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColumnEventID.HeaderText = "#";
            this.ColumnEventID.MaxInputLength = 4;
            this.ColumnEventID.Name = "ColumnEventID";
            this.ColumnEventID.ReadOnly = true;
            this.ColumnEventID.Width = 51;
            // 
            // ColumnEventDate
            // 
            this.ColumnEventDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColumnEventDate.HeaderText = "Date";
            this.ColumnEventDate.MaxInputLength = 10;
            this.ColumnEventDate.Name = "ColumnEventDate";
            this.ColumnEventDate.ReadOnly = true;
            this.ColumnEventDate.Width = 88;
            // 
            // ColumnTimeOfEvent
            // 
            this.ColumnTimeOfEvent.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColumnTimeOfEvent.HeaderText = "Time";
            this.ColumnTimeOfEvent.MaxInputLength = 6;
            this.ColumnTimeOfEvent.Name = "ColumnTimeOfEvent";
            this.ColumnTimeOfEvent.ReadOnly = true;
            this.ColumnTimeOfEvent.Width = 94;
            // 
            // ColumnSymptoms
            // 
            this.ColumnSymptoms.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColumnSymptoms.HeaderText = "Symptoms";
            this.ColumnSymptoms.MaxInputLength = 30;
            this.ColumnSymptoms.Name = "ColumnSymptoms";
            this.ColumnSymptoms.ReadOnly = true;
            this.ColumnSymptoms.Width = 151;
            // 
            // ColumnFindings
            // 
            this.ColumnFindings.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColumnFindings.HeaderText = "Findings";
            this.ColumnFindings.MaxInputLength = 500;
            this.ColumnFindings.Name = "ColumnFindings";
            this.ColumnFindings.ReadOnly = true;
            this.ColumnFindings.Width = 131;
            // 
            // panel75
            // 
            this.panel75.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel75.Location = new System.Drawing.Point(2311, 0);
            this.panel75.Name = "panel75";
            this.panel75.Size = new System.Drawing.Size(23, 172);
            this.panel75.TabIndex = 2;
            // 
            // panel72
            // 
            this.panel72.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel72.Location = new System.Drawing.Point(0, 0);
            this.panel72.Name = "panel72";
            this.panel72.Size = new System.Drawing.Size(20, 172);
            this.panel72.TabIndex = 1;
            // 
            // panel71
            // 
            this.panel71.Controls.Add(this.labelPreviousTransmissions);
            this.panel71.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel71.Location = new System.Drawing.Point(0, 0);
            this.panel71.Name = "panel71";
            this.panel71.Size = new System.Drawing.Size(2334, 81);
            this.panel71.TabIndex = 0;
            // 
            // labelPreviousTransmissions
            // 
            this.labelPreviousTransmissions.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPreviousTransmissions.Font = new System.Drawing.Font("Verdana", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPreviousTransmissions.Location = new System.Drawing.Point(0, 0);
            this.labelPreviousTransmissions.Name = "labelPreviousTransmissions";
            this.labelPreviousTransmissions.Size = new System.Drawing.Size(1008, 81);
            this.labelPreviousTransmissions.TabIndex = 0;
            this.labelPreviousTransmissions.Text = "Previous Three Transmissions";
            this.labelPreviousTransmissions.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel73
            // 
            this.panel73.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel73.Location = new System.Drawing.Point(0, 1227);
            this.panel73.Name = "panel73";
            this.panel73.Size = new System.Drawing.Size(2336, 35);
            this.panel73.TabIndex = 12;
            // 
            // panelECGSweepAmpIndicator
            // 
            this.panelECGSweepAmpIndicator.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelECGSweepAmpIndicator.Controls.Add(this.labelEventMiddle);
            this.panelECGSweepAmpIndicator.Controls.Add(this.labelEventStart);
            this.panelECGSweepAmpIndicator.Controls.Add(this.panelSweepSpeed);
            this.panelECGSweepAmpIndicator.Controls.Add(this.panelAmpPanel);
            this.panelECGSweepAmpIndicator.Controls.Add(this.labelEventEnd);
            this.panelECGSweepAmpIndicator.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelECGSweepAmpIndicator.Location = new System.Drawing.Point(0, 1176);
            this.panelECGSweepAmpIndicator.Name = "panelECGSweepAmpIndicator";
            this.panelECGSweepAmpIndicator.Size = new System.Drawing.Size(2336, 51);
            this.panelECGSweepAmpIndicator.TabIndex = 11;
            // 
            // labelEventMiddle
            // 
            this.labelEventMiddle.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelEventMiddle.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEventMiddle.Location = new System.Drawing.Point(418, 0);
            this.labelEventMiddle.Name = "labelEventMiddle";
            this.labelEventMiddle.Size = new System.Drawing.Size(771, 49);
            this.labelEventMiddle.TabIndex = 6;
            this.labelEventMiddle.Text = "10:15:15 AM";
            this.labelEventMiddle.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelEventStart
            // 
            this.labelEventStart.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelEventStart.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEventStart.Location = new System.Drawing.Point(0, 0);
            this.labelEventStart.Name = "labelEventStart";
            this.labelEventStart.Size = new System.Drawing.Size(418, 49);
            this.labelEventStart.TabIndex = 4;
            this.labelEventStart.Text = "10:15:10";
            this.labelEventStart.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panelSweepSpeed
            // 
            this.panelSweepSpeed.Controls.Add(this.labelBaseSweetSpeed);
            this.panelSweepSpeed.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelSweepSpeed.Location = new System.Drawing.Point(1453, 0);
            this.panelSweepSpeed.Name = "panelSweepSpeed";
            this.panelSweepSpeed.Size = new System.Drawing.Size(436, 49);
            this.panelSweepSpeed.TabIndex = 3;
            // 
            // labelBaseSweetSpeed
            // 
            this.labelBaseSweetSpeed.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelBaseSweetSpeed.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBaseSweetSpeed.Location = new System.Drawing.Point(0, 0);
            this.labelBaseSweetSpeed.Name = "labelBaseSweetSpeed";
            this.labelBaseSweetSpeed.Size = new System.Drawing.Size(436, 49);
            this.labelBaseSweetSpeed.TabIndex = 0;
            this.labelBaseSweetSpeed.Text = "25 mm/sec";
            this.labelBaseSweetSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panelAmpPanel
            // 
            this.panelAmpPanel.Controls.Add(this.labelBaseAmplitudeSet);
            this.panelAmpPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelAmpPanel.Location = new System.Drawing.Point(1889, 0);
            this.panelAmpPanel.Name = "panelAmpPanel";
            this.panelAmpPanel.Size = new System.Drawing.Size(230, 49);
            this.panelAmpPanel.TabIndex = 2;
            // 
            // labelBaseAmplitudeSet
            // 
            this.labelBaseAmplitudeSet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelBaseAmplitudeSet.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBaseAmplitudeSet.Location = new System.Drawing.Point(0, 0);
            this.labelBaseAmplitudeSet.Name = "labelBaseAmplitudeSet";
            this.labelBaseAmplitudeSet.Size = new System.Drawing.Size(230, 49);
            this.labelBaseAmplitudeSet.TabIndex = 1;
            this.labelBaseAmplitudeSet.Text = "1 mm/mV";
            this.labelBaseAmplitudeSet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelEventEnd
            // 
            this.labelEventEnd.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelEventEnd.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEventEnd.Location = new System.Drawing.Point(2119, 0);
            this.labelEventEnd.Name = "labelEventEnd";
            this.labelEventEnd.Size = new System.Drawing.Size(215, 49);
            this.labelEventEnd.TabIndex = 5;
            this.labelEventEnd.Text = "10:15:20 AM";
            this.labelEventEnd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panelECGBaselineSTrip
            // 
            this.panelECGBaselineSTrip.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelECGBaselineSTrip.Controls.Add(this.panel158);
            this.panelECGBaselineSTrip.Controls.Add(this.panel157);
            this.panelECGBaselineSTrip.Controls.Add(this.panel70);
            this.panelECGBaselineSTrip.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelECGBaselineSTrip.Location = new System.Drawing.Point(0, 995);
            this.panelECGBaselineSTrip.Name = "panelECGBaselineSTrip";
            this.panelECGBaselineSTrip.Size = new System.Drawing.Size(2336, 181);
            this.panelECGBaselineSTrip.TabIndex = 10;
            // 
            // panel158
            // 
            this.panel158.Controls.Add(this.pictureBoxBaselineReferenceECGStrip);
            this.panel158.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel158.Location = new System.Drawing.Point(20, 0);
            this.panel158.Name = "panel158";
            this.panel158.Size = new System.Drawing.Size(2291, 179);
            this.panel158.TabIndex = 2;
            // 
            // pictureBoxBaselineReferenceECGStrip
            // 
            this.pictureBoxBaselineReferenceECGStrip.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxBaselineReferenceECGStrip.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxBaselineReferenceECGStrip.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxBaselineReferenceECGStrip.InitialImage")));
            this.pictureBoxBaselineReferenceECGStrip.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxBaselineReferenceECGStrip.Name = "pictureBoxBaselineReferenceECGStrip";
            this.pictureBoxBaselineReferenceECGStrip.Size = new System.Drawing.Size(2291, 179);
            this.pictureBoxBaselineReferenceECGStrip.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxBaselineReferenceECGStrip.TabIndex = 0;
            this.pictureBoxBaselineReferenceECGStrip.TabStop = false;
            this.pictureBoxBaselineReferenceECGStrip.Click += new System.EventHandler(this.pictureBoxBaselineReferenceECGStrip_Click);
            // 
            // panel157
            // 
            this.panel157.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel157.Location = new System.Drawing.Point(2311, 0);
            this.panel157.Name = "panel157";
            this.panel157.Size = new System.Drawing.Size(23, 179);
            this.panel157.TabIndex = 1;
            // 
            // panel70
            // 
            this.panel70.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel70.Location = new System.Drawing.Point(0, 0);
            this.panel70.Name = "panel70";
            this.panel70.Size = new System.Drawing.Size(20, 179);
            this.panel70.TabIndex = 0;
            // 
            // panelTimeBarBaselineECG
            // 
            this.panelTimeBarBaselineECG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelTimeBarBaselineECG.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTimeBarBaselineECG.Location = new System.Drawing.Point(0, 975);
            this.panelTimeBarBaselineECG.Name = "panelTimeBarBaselineECG";
            this.panelTimeBarBaselineECG.Size = new System.Drawing.Size(2336, 20);
            this.panelTimeBarBaselineECG.TabIndex = 9;
            // 
            // panelTimeHeadingsPerStrip
            // 
            this.panelTimeHeadingsPerStrip.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelTimeHeadingsPerStrip.Controls.Add(this.labelQTSI);
            this.panelTimeHeadingsPerStrip.Controls.Add(this.panel151);
            this.panelTimeHeadingsPerStrip.Controls.Add(this.panel141);
            this.panelTimeHeadingsPerStrip.Controls.Add(this.panel140);
            this.panelTimeHeadingsPerStrip.Controls.Add(this.panel139);
            this.panelTimeHeadingsPerStrip.Controls.Add(this.panel138);
            this.panelTimeHeadingsPerStrip.Controls.Add(this.panel137);
            this.panelTimeHeadingsPerStrip.Controls.Add(this.panel136);
            this.panelTimeHeadingsPerStrip.Controls.Add(this.panel135);
            this.panelTimeHeadingsPerStrip.Controls.Add(this.panel134);
            this.panelTimeHeadingsPerStrip.Controls.Add(this.panel133);
            this.panelTimeHeadingsPerStrip.Controls.Add(this.panel69);
            this.panelTimeHeadingsPerStrip.Controls.Add(this.panel132);
            this.panelTimeHeadingsPerStrip.Controls.Add(this.panel131);
            this.panelTimeHeadingsPerStrip.Controls.Add(this.panel130);
            this.panelTimeHeadingsPerStrip.Controls.Add(this.panel129);
            this.panelTimeHeadingsPerStrip.Controls.Add(this.panel128);
            this.panelTimeHeadingsPerStrip.Controls.Add(this.panel127);
            this.panelTimeHeadingsPerStrip.Controls.Add(this.panel126);
            this.panelTimeHeadingsPerStrip.Controls.Add(this.panel125);
            this.panelTimeHeadingsPerStrip.Controls.Add(this.panel124);
            this.panelTimeHeadingsPerStrip.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTimeHeadingsPerStrip.Location = new System.Drawing.Point(0, 926);
            this.panelTimeHeadingsPerStrip.Name = "panelTimeHeadingsPerStrip";
            this.panelTimeHeadingsPerStrip.Size = new System.Drawing.Size(2336, 49);
            this.panelTimeHeadingsPerStrip.TabIndex = 8;
            // 
            // labelQTSI
            // 
            this.labelQTSI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelQTSI.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQTSI.Location = new System.Drawing.Point(2259, 0);
            this.labelQTSI.Name = "labelQTSI";
            this.labelQTSI.Size = new System.Drawing.Size(53, 47);
            this.labelQTSI.TabIndex = 57;
            this.labelQTSI.Text = "sec";
            this.labelQTSI.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel151
            // 
            this.panel151.Controls.Add(this.labelQT);
            this.panel151.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel151.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.panel151.Location = new System.Drawing.Point(2132, 0);
            this.panel151.Name = "panel151";
            this.panel151.Size = new System.Drawing.Size(127, 47);
            this.panel151.TabIndex = 19;
            // 
            // labelQT
            // 
            this.labelQT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelQT.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQT.Location = new System.Drawing.Point(0, 0);
            this.labelQT.Name = "labelQT";
            this.labelQT.Size = new System.Drawing.Size(127, 47);
            this.labelQT.TabIndex = 58;
            this.labelQT.Text = "0,465";
            this.labelQT.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel141
            // 
            this.panel141.Controls.Add(this.labelQTHeader);
            this.panel141.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel141.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.panel141.Location = new System.Drawing.Point(2036, 0);
            this.panel141.Name = "panel141";
            this.panel141.Size = new System.Drawing.Size(96, 47);
            this.panel141.TabIndex = 18;
            // 
            // labelQTHeader
            // 
            this.labelQTHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelQTHeader.Font = new System.Drawing.Font("Verdana", 24F);
            this.labelQTHeader.Location = new System.Drawing.Point(0, 0);
            this.labelQTHeader.Name = "labelQTHeader";
            this.labelQTHeader.Size = new System.Drawing.Size(96, 47);
            this.labelQTHeader.TabIndex = 59;
            this.labelQTHeader.Text = "QT:";
            this.labelQTHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel140
            // 
            this.panel140.Controls.Add(this.labelQRSSI);
            this.panel140.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel140.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.panel140.Location = new System.Drawing.Point(1960, 0);
            this.panel140.Name = "panel140";
            this.panel140.Size = new System.Drawing.Size(76, 47);
            this.panel140.TabIndex = 17;
            // 
            // labelQRSSI
            // 
            this.labelQRSSI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelQRSSI.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQRSSI.Location = new System.Drawing.Point(0, 0);
            this.labelQRSSI.Name = "labelQRSSI";
            this.labelQRSSI.Size = new System.Drawing.Size(76, 47);
            this.labelQRSSI.TabIndex = 56;
            this.labelQRSSI.Text = "sec";
            this.labelQRSSI.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel139
            // 
            this.panel139.Controls.Add(this.labelQRS);
            this.panel139.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel139.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.panel139.Location = new System.Drawing.Point(1834, 0);
            this.panel139.Name = "panel139";
            this.panel139.Size = new System.Drawing.Size(126, 47);
            this.panel139.TabIndex = 16;
            // 
            // labelQRS
            // 
            this.labelQRS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelQRS.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQRS.Location = new System.Drawing.Point(0, 0);
            this.labelQRS.Name = "labelQRS";
            this.labelQRS.Size = new System.Drawing.Size(126, 47);
            this.labelQRS.TabIndex = 56;
            this.labelQRS.Text = "0,113";
            this.labelQRS.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel138
            // 
            this.panel138.Controls.Add(this.labelQRSHeader);
            this.panel138.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel138.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.panel138.Location = new System.Drawing.Point(1706, 0);
            this.panel138.Name = "panel138";
            this.panel138.Size = new System.Drawing.Size(128, 47);
            this.panel138.TabIndex = 15;
            // 
            // labelQRSHeader
            // 
            this.labelQRSHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelQRSHeader.Font = new System.Drawing.Font("Verdana", 24F);
            this.labelQRSHeader.Location = new System.Drawing.Point(0, 0);
            this.labelQRSHeader.Name = "labelQRSHeader";
            this.labelQRSHeader.Size = new System.Drawing.Size(128, 47);
            this.labelQRSHeader.TabIndex = 57;
            this.labelQRSHeader.Text = "QRS:";
            this.labelQRSHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel137
            // 
            this.panel137.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel137.Location = new System.Drawing.Point(2312, 0);
            this.panel137.Name = "panel137";
            this.panel137.Size = new System.Drawing.Size(22, 47);
            this.panel137.TabIndex = 14;
            // 
            // panel136
            // 
            this.panel136.Controls.Add(this.labelPRSI);
            this.panel136.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel136.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.panel136.Location = new System.Drawing.Point(1628, 0);
            this.panel136.Name = "panel136";
            this.panel136.Size = new System.Drawing.Size(78, 47);
            this.panel136.TabIndex = 13;
            // 
            // labelPRSI
            // 
            this.labelPRSI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPRSI.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPRSI.Location = new System.Drawing.Point(0, 0);
            this.labelPRSI.Name = "labelPRSI";
            this.labelPRSI.Size = new System.Drawing.Size(78, 47);
            this.labelPRSI.TabIndex = 54;
            this.labelPRSI.Text = "sec";
            this.labelPRSI.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel135
            // 
            this.panel135.Controls.Add(this.labelPR);
            this.panel135.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel135.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.panel135.Location = new System.Drawing.Point(1519, 0);
            this.panel135.Name = "panel135";
            this.panel135.Size = new System.Drawing.Size(109, 47);
            this.panel135.TabIndex = 12;
            // 
            // labelPR
            // 
            this.labelPR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPR.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPR.Location = new System.Drawing.Point(0, 0);
            this.labelPR.Name = "labelPR";
            this.labelPR.Size = new System.Drawing.Size(109, 47);
            this.labelPR.TabIndex = 54;
            this.labelPR.Text = "0,20";
            this.labelPR.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel134
            // 
            this.panel134.Controls.Add(this.labelPRHeader);
            this.panel134.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel134.Location = new System.Drawing.Point(1415, 0);
            this.panel134.Name = "panel134";
            this.panel134.Size = new System.Drawing.Size(104, 47);
            this.panel134.TabIndex = 11;
            // 
            // labelPRHeader
            // 
            this.labelPRHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPRHeader.Font = new System.Drawing.Font("Verdana", 24F);
            this.labelPRHeader.Location = new System.Drawing.Point(0, 0);
            this.labelPRHeader.Name = "labelPRHeader";
            this.labelPRHeader.Size = new System.Drawing.Size(104, 47);
            this.labelPRHeader.TabIndex = 55;
            this.labelPRHeader.Text = "PR:";
            this.labelPRHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel133
            // 
            this.panel133.Controls.Add(this.labelMaxRateSI);
            this.panel133.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel133.Location = new System.Drawing.Point(1318, 0);
            this.panel133.Name = "panel133";
            this.panel133.Size = new System.Drawing.Size(97, 47);
            this.panel133.TabIndex = 10;
            // 
            // labelMaxRateSI
            // 
            this.labelMaxRateSI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelMaxRateSI.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMaxRateSI.Location = new System.Drawing.Point(0, 0);
            this.labelMaxRateSI.Name = "labelMaxRateSI";
            this.labelMaxRateSI.Size = new System.Drawing.Size(97, 47);
            this.labelMaxRateSI.TabIndex = 53;
            this.labelMaxRateSI.Text = "bpm";
            this.labelMaxRateSI.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel69
            // 
            this.panel69.Controls.Add(this.labelMaxRate);
            this.panel69.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel69.Location = new System.Drawing.Point(1228, 0);
            this.panel69.Name = "panel69";
            this.panel69.Size = new System.Drawing.Size(90, 47);
            this.panel69.TabIndex = 9;
            // 
            // labelMaxRate
            // 
            this.labelMaxRate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelMaxRate.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMaxRate.Location = new System.Drawing.Point(0, 0);
            this.labelMaxRate.Name = "labelMaxRate";
            this.labelMaxRate.Size = new System.Drawing.Size(90, 47);
            this.labelMaxRate.TabIndex = 52;
            this.labelMaxRate.Text = "120";
            this.labelMaxRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel132
            // 
            this.panel132.Controls.Add(this.labelMaxRateHeader);
            this.panel132.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel132.Location = new System.Drawing.Point(1020, 0);
            this.panel132.Name = "panel132";
            this.panel132.Size = new System.Drawing.Size(208, 47);
            this.panel132.TabIndex = 8;
            // 
            // labelMaxRateHeader
            // 
            this.labelMaxRateHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelMaxRateHeader.Font = new System.Drawing.Font("Verdana", 24F);
            this.labelMaxRateHeader.Location = new System.Drawing.Point(0, 0);
            this.labelMaxRateHeader.Name = "labelMaxRateHeader";
            this.labelMaxRateHeader.Size = new System.Drawing.Size(208, 47);
            this.labelMaxRateHeader.TabIndex = 53;
            this.labelMaxRateHeader.Text = "Max Rate:";
            this.labelMaxRateHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel131
            // 
            this.panel131.Controls.Add(this.labelMeanRateSI);
            this.panel131.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel131.Location = new System.Drawing.Point(917, 0);
            this.panel131.Name = "panel131";
            this.panel131.Size = new System.Drawing.Size(103, 47);
            this.panel131.TabIndex = 7;
            // 
            // labelMeanRateSI
            // 
            this.labelMeanRateSI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelMeanRateSI.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMeanRateSI.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelMeanRateSI.Location = new System.Drawing.Point(0, 0);
            this.labelMeanRateSI.Name = "labelMeanRateSI";
            this.labelMeanRateSI.Size = new System.Drawing.Size(103, 47);
            this.labelMeanRateSI.TabIndex = 51;
            this.labelMeanRateSI.Text = "bpm";
            this.labelMeanRateSI.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel130
            // 
            this.panel130.Controls.Add(this.labelMeanRate);
            this.panel130.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel130.Location = new System.Drawing.Point(830, 0);
            this.panel130.Name = "panel130";
            this.panel130.Size = new System.Drawing.Size(87, 47);
            this.panel130.TabIndex = 6;
            // 
            // labelMeanRate
            // 
            this.labelMeanRate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelMeanRate.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMeanRate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelMeanRate.Location = new System.Drawing.Point(0, 0);
            this.labelMeanRate.Name = "labelMeanRate";
            this.labelMeanRate.Size = new System.Drawing.Size(87, 47);
            this.labelMeanRate.TabIndex = 50;
            this.labelMeanRate.Text = "110";
            this.labelMeanRate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel129
            // 
            this.panel129.Controls.Add(this.labelMeanRateHeader);
            this.panel129.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel129.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.panel129.Location = new System.Drawing.Point(618, 0);
            this.panel129.Name = "panel129";
            this.panel129.Size = new System.Drawing.Size(212, 47);
            this.panel129.TabIndex = 5;
            // 
            // labelMeanRateHeader
            // 
            this.labelMeanRateHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelMeanRateHeader.Font = new System.Drawing.Font("Verdana", 24F);
            this.labelMeanRateHeader.Location = new System.Drawing.Point(0, 0);
            this.labelMeanRateHeader.Name = "labelMeanRateHeader";
            this.labelMeanRateHeader.Size = new System.Drawing.Size(212, 47);
            this.labelMeanRateHeader.TabIndex = 50;
            this.labelMeanRateHeader.Text = "Mean Rate:";
            this.labelMeanRateHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel128
            // 
            this.panel128.Controls.Add(this.labelMinRateSI);
            this.panel128.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel128.Location = new System.Drawing.Point(513, 0);
            this.panel128.Name = "panel128";
            this.panel128.Size = new System.Drawing.Size(105, 47);
            this.panel128.TabIndex = 4;
            // 
            // labelMinRateSI
            // 
            this.labelMinRateSI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelMinRateSI.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMinRateSI.Location = new System.Drawing.Point(0, 0);
            this.labelMinRateSI.Name = "labelMinRateSI";
            this.labelMinRateSI.Size = new System.Drawing.Size(105, 47);
            this.labelMinRateSI.TabIndex = 49;
            this.labelMinRateSI.Text = "bpm";
            this.labelMinRateSI.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel127
            // 
            this.panel127.Controls.Add(this.labelMinRate);
            this.panel127.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel127.Location = new System.Drawing.Point(418, 0);
            this.panel127.Name = "panel127";
            this.panel127.Size = new System.Drawing.Size(95, 47);
            this.panel127.TabIndex = 3;
            // 
            // labelMinRate
            // 
            this.labelMinRate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelMinRate.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMinRate.Location = new System.Drawing.Point(0, 0);
            this.labelMinRate.Name = "labelMinRate";
            this.labelMinRate.Size = new System.Drawing.Size(95, 47);
            this.labelMinRate.TabIndex = 48;
            this.labelMinRate.Text = "100";
            this.labelMinRate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel126
            // 
            this.panel126.Controls.Add(this.labelMinRateHeader);
            this.panel126.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel126.Location = new System.Drawing.Point(244, 0);
            this.panel126.Name = "panel126";
            this.panel126.Size = new System.Drawing.Size(174, 47);
            this.panel126.TabIndex = 2;
            // 
            // labelMinRateHeader
            // 
            this.labelMinRateHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelMinRateHeader.Font = new System.Drawing.Font("Verdana", 24F);
            this.labelMinRateHeader.Location = new System.Drawing.Point(0, 0);
            this.labelMinRateHeader.Name = "labelMinRateHeader";
            this.labelMinRateHeader.Size = new System.Drawing.Size(174, 47);
            this.labelMinRateHeader.TabIndex = 47;
            this.labelMinRateHeader.Text = "Min Rate:";
            this.labelMinRateHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel125
            // 
            this.panel125.Controls.Add(this.labelLeadID);
            this.panel125.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel125.Location = new System.Drawing.Point(22, 0);
            this.panel125.Name = "panel125";
            this.panel125.Size = new System.Drawing.Size(222, 47);
            this.panel125.TabIndex = 1;
            // 
            // labelLeadID
            // 
            this.labelLeadID.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelLeadID.Font = new System.Drawing.Font("Verdana", 28F);
            this.labelLeadID.Location = new System.Drawing.Point(0, 0);
            this.labelLeadID.Name = "labelLeadID";
            this.labelLeadID.Size = new System.Drawing.Size(198, 47);
            this.labelLeadID.TabIndex = 46;
            this.labelLeadID.Text = "LEAD III";
            this.labelLeadID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel124
            // 
            this.panel124.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel124.Location = new System.Drawing.Point(0, 0);
            this.panel124.Name = "panel124";
            this.panel124.Size = new System.Drawing.Size(22, 47);
            this.panel124.TabIndex = 0;
            // 
            // panelBaseLineHeader
            // 
            this.panelBaseLineHeader.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelBaseLineHeader.Controls.Add(this.panel20);
            this.panelBaseLineHeader.Controls.Add(this.panel123);
            this.panelBaseLineHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelBaseLineHeader.Location = new System.Drawing.Point(0, 810);
            this.panelBaseLineHeader.Name = "panelBaseLineHeader";
            this.panelBaseLineHeader.Size = new System.Drawing.Size(2336, 116);
            this.panelBaseLineHeader.TabIndex = 7;
            // 
            // panel20
            // 
            this.panel20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel20.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel20.Location = new System.Drawing.Point(0, 0);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(2334, 2);
            this.panel20.TabIndex = 1;
            // 
            // panel123
            // 
            this.panel123.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel123.Controls.Add(this.labelFindingsRythmBL);
            this.panel123.Controls.Add(this.labelBaselineDate);
            this.panel123.Controls.Add(this.label134);
            this.panel123.Controls.Add(this.labelBaselineTime);
            this.panel123.Controls.Add(this.labelBaseRefHeader);
            this.panel123.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel123.Location = new System.Drawing.Point(0, 38);
            this.panel123.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.panel123.Name = "panel123";
            this.panel123.Size = new System.Drawing.Size(2334, 76);
            this.panel123.TabIndex = 0;
            // 
            // labelFindingsRythmBL
            // 
            this.labelFindingsRythmBL.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelFindingsRythmBL.Font = new System.Drawing.Font("Verdana", 27.75F);
            this.labelFindingsRythmBL.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.labelFindingsRythmBL.Location = new System.Drawing.Point(651, 0);
            this.labelFindingsRythmBL.Name = "labelFindingsRythmBL";
            this.labelFindingsRythmBL.Size = new System.Drawing.Size(1097, 74);
            this.labelFindingsRythmBL.TabIndex = 18;
            this.labelFindingsRythmBL.Text = "xxxxxxx";
            this.labelFindingsRythmBL.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelBaselineDate
            // 
            this.labelBaselineDate.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelBaselineDate.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBaselineDate.Location = new System.Drawing.Point(1759, 0);
            this.labelBaselineDate.Name = "labelBaselineDate";
            this.labelBaselineDate.Size = new System.Drawing.Size(273, 74);
            this.labelBaselineDate.TabIndex = 17;
            this.labelBaselineDate.Text = "11/23/2016";
            this.labelBaselineDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label134
            // 
            this.label134.Dock = System.Windows.Forms.DockStyle.Right;
            this.label134.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label134.Location = new System.Drawing.Point(2032, 0);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(27, 74);
            this.label134.TabIndex = 16;
            this.label134.Text = "-";
            this.label134.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelBaselineTime
            // 
            this.labelBaselineTime.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelBaselineTime.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBaselineTime.Location = new System.Drawing.Point(2059, 0);
            this.labelBaselineTime.Name = "labelBaselineTime";
            this.labelBaselineTime.Size = new System.Drawing.Size(273, 74);
            this.labelBaselineTime.TabIndex = 15;
            this.labelBaselineTime.Text = "10:15:10 AM";
            this.labelBaselineTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelBaseRefHeader
            // 
            this.labelBaseRefHeader.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelBaseRefHeader.Font = new System.Drawing.Font("Verdana", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBaseRefHeader.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.labelBaseRefHeader.Location = new System.Drawing.Point(0, 0);
            this.labelBaseRefHeader.Name = "labelBaseRefHeader";
            this.labelBaseRefHeader.Size = new System.Drawing.Size(651, 74);
            this.labelBaseRefHeader.TabIndex = 1;
            this.labelBaseRefHeader.Text = "Baseline Reference";
            this.labelBaseRefHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel67
            // 
            this.panel67.Controls.Add(this.panel259);
            this.panel67.Controls.Add(this.panel258);
            this.panel67.Controls.Add(this.panel256);
            this.panel67.Controls.Add(this.panel254);
            this.panel67.Controls.Add(this.panelVert9);
            this.panel67.Controls.Add(this.panel252);
            this.panel67.Controls.Add(this.panel250);
            this.panel67.Controls.Add(this.panelVert8);
            this.panel67.Controls.Add(this.panel244);
            this.panel67.Controls.Add(this.panel242);
            this.panel67.Controls.Add(this.panelVert7);
            this.panel67.Controls.Add(this.panel240);
            this.panel67.Controls.Add(this.panel239);
            this.panel67.Controls.Add(this.panelVert6);
            this.panel67.Controls.Add(this.panel237);
            this.panel67.Controls.Add(this.panel236);
            this.panel67.Controls.Add(this.panel235);
            this.panel67.Controls.Add(this.panel231);
            this.panel67.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel67.Location = new System.Drawing.Point(0, 630);
            this.panel67.Name = "panel67";
            this.panel67.Size = new System.Drawing.Size(2336, 180);
            this.panel67.TabIndex = 6;
            // 
            // panel259
            // 
            this.panel259.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel259.Location = new System.Drawing.Point(2429, 2);
            this.panel259.Name = "panel259";
            this.panel259.Size = new System.Drawing.Size(23, 178);
            this.panel259.TabIndex = 21;
            // 
            // panel258
            // 
            this.panel258.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel258.Location = new System.Drawing.Point(2336, 2);
            this.panel258.Name = "panel258";
            this.panel258.Size = new System.Drawing.Size(93, 178);
            this.panel258.TabIndex = 20;
            // 
            // panel256
            // 
            this.panel256.Controls.Add(this.labelClient);
            this.panel256.Controls.Add(this.panel264);
            this.panel256.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel256.Location = new System.Drawing.Point(1875, 2);
            this.panel256.Name = "panel256";
            this.panel256.Size = new System.Drawing.Size(461, 178);
            this.panel256.TabIndex = 18;
            // 
            // labelClient
            // 
            this.labelClient.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelClient.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelClient.Location = new System.Drawing.Point(0, 62);
            this.labelClient.Name = "labelClient";
            this.labelClient.Size = new System.Drawing.Size(461, 116);
            this.labelClient.TabIndex = 31;
            this.labelClient.Text = "New York Hospital";
            this.labelClient.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel264
            // 
            this.panel264.Controls.Add(this.labelClientHeader);
            this.panel264.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel264.Location = new System.Drawing.Point(0, 0);
            this.panel264.Name = "panel264";
            this.panel264.Size = new System.Drawing.Size(461, 62);
            this.panel264.TabIndex = 3;
            // 
            // labelClientHeader
            // 
            this.labelClientHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelClientHeader.Font = new System.Drawing.Font("Verdana", 32F);
            this.labelClientHeader.Location = new System.Drawing.Point(0, 0);
            this.labelClientHeader.Name = "labelClientHeader";
            this.labelClientHeader.Size = new System.Drawing.Size(461, 62);
            this.labelClientHeader.TabIndex = 19;
            this.labelClientHeader.Text = "CLIENT:";
            this.labelClientHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel254
            // 
            this.panel254.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel254.Location = new System.Drawing.Point(1861, 2);
            this.panel254.Name = "panel254";
            this.panel254.Size = new System.Drawing.Size(14, 178);
            this.panel254.TabIndex = 16;
            // 
            // panelVert9
            // 
            this.panelVert9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelVert9.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelVert9.Location = new System.Drawing.Point(1859, 2);
            this.panelVert9.Name = "panelVert9";
            this.panelVert9.Size = new System.Drawing.Size(2, 178);
            this.panelVert9.TabIndex = 15;
            // 
            // panel252
            // 
            this.panel252.Controls.Add(this.labelRefPhysician);
            this.panel252.Controls.Add(this.panel262);
            this.panel252.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel252.Location = new System.Drawing.Point(1322, 2);
            this.panel252.Name = "panel252";
            this.panel252.Size = new System.Drawing.Size(537, 178);
            this.panel252.TabIndex = 14;
            // 
            // labelRefPhysician
            // 
            this.labelRefPhysician.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelRefPhysician.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRefPhysician.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.labelRefPhysician.Location = new System.Drawing.Point(0, 62);
            this.labelRefPhysician.Name = "labelRefPhysician";
            this.labelRefPhysician.Size = new System.Drawing.Size(537, 116);
            this.labelRefPhysician.TabIndex = 29;
            this.labelRefPhysician.Text = "Dr. John Smithsonian";
            this.labelRefPhysician.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel262
            // 
            this.panel262.Controls.Add(this.labelRefPhysHeader);
            this.panel262.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel262.Location = new System.Drawing.Point(0, 0);
            this.panel262.Name = "panel262";
            this.panel262.Size = new System.Drawing.Size(537, 62);
            this.panel262.TabIndex = 3;
            // 
            // labelRefPhysHeader
            // 
            this.labelRefPhysHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelRefPhysHeader.Font = new System.Drawing.Font("Verdana", 32F);
            this.labelRefPhysHeader.Location = new System.Drawing.Point(0, 0);
            this.labelRefPhysHeader.Name = "labelRefPhysHeader";
            this.labelRefPhysHeader.Size = new System.Drawing.Size(537, 62);
            this.labelRefPhysHeader.TabIndex = 18;
            this.labelRefPhysHeader.Text = "REF PHYSICIAN:";
            this.labelRefPhysHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel250
            // 
            this.panel250.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel250.Location = new System.Drawing.Point(1307, 2);
            this.panel250.Name = "panel250";
            this.panel250.Size = new System.Drawing.Size(15, 178);
            this.panel250.TabIndex = 12;
            // 
            // panelVert8
            // 
            this.panelVert8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelVert8.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelVert8.Location = new System.Drawing.Point(1305, 2);
            this.panelVert8.Name = "panelVert8";
            this.panelVert8.Size = new System.Drawing.Size(2, 178);
            this.panelVert8.TabIndex = 11;
            // 
            // panel244
            // 
            this.panel244.Controls.Add(this.labelPhysician);
            this.panel244.Controls.Add(this.panel260);
            this.panel244.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel244.Location = new System.Drawing.Point(893, 2);
            this.panel244.Name = "panel244";
            this.panel244.Size = new System.Drawing.Size(412, 178);
            this.panel244.TabIndex = 10;
            // 
            // labelPhysician
            // 
            this.labelPhysician.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPhysician.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPhysician.Location = new System.Drawing.Point(0, 62);
            this.labelPhysician.Name = "labelPhysician";
            this.labelPhysician.Size = new System.Drawing.Size(412, 116);
            this.labelPhysician.TabIndex = 25;
            this.labelPhysician.Text = "Dr. John Smithsonian";
            this.labelPhysician.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel260
            // 
            this.panel260.Controls.Add(this.labelPhysHeader);
            this.panel260.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel260.Location = new System.Drawing.Point(0, 0);
            this.panel260.Name = "panel260";
            this.panel260.Size = new System.Drawing.Size(412, 62);
            this.panel260.TabIndex = 2;
            // 
            // labelPhysHeader
            // 
            this.labelPhysHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPhysHeader.Font = new System.Drawing.Font("Verdana", 32F);
            this.labelPhysHeader.Location = new System.Drawing.Point(0, 0);
            this.labelPhysHeader.Name = "labelPhysHeader";
            this.labelPhysHeader.Size = new System.Drawing.Size(412, 62);
            this.labelPhysHeader.TabIndex = 16;
            this.labelPhysHeader.Text = "PHYSICIAN:";
            this.labelPhysHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel242
            // 
            this.panel242.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel242.Location = new System.Drawing.Point(881, 2);
            this.panel242.Name = "panel242";
            this.panel242.Size = new System.Drawing.Size(12, 178);
            this.panel242.TabIndex = 8;
            // 
            // panelVert7
            // 
            this.panelVert7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelVert7.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelVert7.Location = new System.Drawing.Point(879, 2);
            this.panelVert7.Name = "panelVert7";
            this.panelVert7.Size = new System.Drawing.Size(2, 178);
            this.panelVert7.TabIndex = 7;
            // 
            // panel240
            // 
            this.panel240.Controls.Add(this.panel248);
            this.panel240.Controls.Add(this.panel247);
            this.panel240.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel240.Location = new System.Drawing.Point(497, 2);
            this.panel240.Name = "panel240";
            this.panel240.Size = new System.Drawing.Size(382, 178);
            this.panel240.TabIndex = 6;
            // 
            // panel248
            // 
            this.panel248.Controls.Add(this.labelSerialNr);
            this.panel248.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel248.Location = new System.Drawing.Point(0, 62);
            this.panel248.Name = "panel248";
            this.panel248.Size = new System.Drawing.Size(382, 62);
            this.panel248.TabIndex = 2;
            // 
            // labelSerialNr
            // 
            this.labelSerialNr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSerialNr.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSerialNr.Location = new System.Drawing.Point(0, 0);
            this.labelSerialNr.Name = "labelSerialNr";
            this.labelSerialNr.Size = new System.Drawing.Size(382, 62);
            this.labelSerialNr.TabIndex = 40;
            this.labelSerialNr.Text = "AA102044403";
            this.labelSerialNr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel247
            // 
            this.panel247.Controls.Add(this.labelSerialNrHeader);
            this.panel247.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel247.Location = new System.Drawing.Point(0, 0);
            this.panel247.Name = "panel247";
            this.panel247.Size = new System.Drawing.Size(382, 62);
            this.panel247.TabIndex = 1;
            // 
            // labelSerialNrHeader
            // 
            this.labelSerialNrHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSerialNrHeader.Font = new System.Drawing.Font("Verdana", 32F);
            this.labelSerialNrHeader.Location = new System.Drawing.Point(0, 0);
            this.labelSerialNrHeader.Name = "labelSerialNrHeader";
            this.labelSerialNrHeader.Size = new System.Drawing.Size(382, 62);
            this.labelSerialNrHeader.TabIndex = 35;
            this.labelSerialNrHeader.Text = "SERIAL #:";
            this.labelSerialNrHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelSerialNrHeader.Click += new System.EventHandler(this.labelSerialNrHeader_Click);
            // 
            // panel239
            // 
            this.panel239.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel239.Location = new System.Drawing.Point(481, 2);
            this.panel239.Name = "panel239";
            this.panel239.Size = new System.Drawing.Size(16, 178);
            this.panel239.TabIndex = 5;
            // 
            // panelVert6
            // 
            this.panelVert6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelVert6.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelVert6.Location = new System.Drawing.Point(479, 2);
            this.panelVert6.Name = "panelVert6";
            this.panelVert6.Size = new System.Drawing.Size(2, 178);
            this.panelVert6.TabIndex = 4;
            // 
            // panel237
            // 
            this.panel237.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel237.Location = new System.Drawing.Point(458, 2);
            this.panel237.Name = "panel237";
            this.panel237.Size = new System.Drawing.Size(21, 178);
            this.panel237.TabIndex = 3;
            // 
            // panel236
            // 
            this.panel236.Controls.Add(this.panel246);
            this.panel236.Controls.Add(this.panel245);
            this.panel236.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel236.Location = new System.Drawing.Point(21, 2);
            this.panel236.Name = "panel236";
            this.panel236.Size = new System.Drawing.Size(437, 178);
            this.panel236.TabIndex = 2;
            // 
            // panel246
            // 
            this.panel246.Controls.Add(this.labelStartDate);
            this.panel246.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel246.Location = new System.Drawing.Point(0, 62);
            this.panel246.Name = "panel246";
            this.panel246.Size = new System.Drawing.Size(437, 62);
            this.panel246.TabIndex = 1;
            // 
            // labelStartDate
            // 
            this.labelStartDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelStartDate.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStartDate.Location = new System.Drawing.Point(0, 0);
            this.labelStartDate.Name = "labelStartDate";
            this.labelStartDate.Size = new System.Drawing.Size(437, 62);
            this.labelStartDate.TabIndex = 39;
            this.labelStartDate.Text = "10/23/2016";
            this.labelStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel245
            // 
            this.panel245.Controls.Add(this.labelStartDateHeader);
            this.panel245.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel245.Location = new System.Drawing.Point(0, 0);
            this.panel245.Name = "panel245";
            this.panel245.Size = new System.Drawing.Size(437, 62);
            this.panel245.TabIndex = 0;
            // 
            // labelStartDateHeader
            // 
            this.labelStartDateHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelStartDateHeader.Font = new System.Drawing.Font("Verdana", 32F);
            this.labelStartDateHeader.Location = new System.Drawing.Point(0, 0);
            this.labelStartDateHeader.Name = "labelStartDateHeader";
            this.labelStartDateHeader.Size = new System.Drawing.Size(437, 62);
            this.labelStartDateHeader.TabIndex = 34;
            this.labelStartDateHeader.Text = "START DATE:";
            this.labelStartDateHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel235
            // 
            this.panel235.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel235.Location = new System.Drawing.Point(0, 2);
            this.panel235.Name = "panel235";
            this.panel235.Size = new System.Drawing.Size(21, 178);
            this.panel235.TabIndex = 1;
            // 
            // panel231
            // 
            this.panel231.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel231.Location = new System.Drawing.Point(0, 0);
            this.panel231.Name = "panel231";
            this.panel231.Size = new System.Drawing.Size(2336, 2);
            this.panel231.TabIndex = 0;
            // 
            // panelSecPatBar
            // 
            this.panelSecPatBar.Controls.Add(this.panel1);
            this.panelSecPatBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSecPatBar.Location = new System.Drawing.Point(0, 593);
            this.panelSecPatBar.Name = "panelSecPatBar";
            this.panelSecPatBar.Size = new System.Drawing.Size(2336, 37);
            this.panelSecPatBar.TabIndex = 5;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(2336, 2);
            this.panel1.TabIndex = 0;
            // 
            // panelHorSepPatDet
            // 
            this.panelHorSepPatDet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelHorSepPatDet.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHorSepPatDet.Location = new System.Drawing.Point(0, 592);
            this.panelHorSepPatDet.Name = "panelHorSepPatDet";
            this.panelHorSepPatDet.Size = new System.Drawing.Size(2336, 1);
            this.panelHorSepPatDet.TabIndex = 3;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.panel49);
            this.panel5.Controls.Add(this.panel42);
            this.panel5.Controls.Add(this.panel43);
            this.panel5.Controls.Add(this.panel48);
            this.panel5.Controls.Add(this.panelVert5);
            this.panel5.Controls.Add(this.panel36);
            this.panel5.Controls.Add(this.panel41);
            this.panel5.Controls.Add(this.panelVert4);
            this.panel5.Controls.Add(this.panel29);
            this.panel5.Controls.Add(this.panel34);
            this.panel5.Controls.Add(this.panelVert3);
            this.panel5.Controls.Add(this.panel22);
            this.panel5.Controls.Add(this.panel27);
            this.panel5.Controls.Add(this.panelVert2);
            this.panel5.Controls.Add(this.panel10);
            this.panel5.Controls.Add(this.panel11);
            this.panel5.Controls.Add(this.panelVert1);
            this.panel5.Controls.Add(this.panel9);
            this.panel5.Controls.Add(this.panel8);
            this.panel5.Controls.Add(this.panel7);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 384);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(2336, 208);
            this.panel5.TabIndex = 2;
            // 
            // panel49
            // 
            this.panel49.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel49.Location = new System.Drawing.Point(2305, 0);
            this.panel49.Name = "panel49";
            this.panel49.Size = new System.Drawing.Size(13, 208);
            this.panel49.TabIndex = 20;
            // 
            // panel42
            // 
            this.panel42.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel42.Location = new System.Drawing.Point(2318, 0);
            this.panel42.Name = "panel42";
            this.panel42.Size = new System.Drawing.Size(18, 208);
            this.panel42.TabIndex = 19;
            // 
            // panel43
            // 
            this.panel43.Controls.Add(this.panel33);
            this.panel43.Controls.Add(this.panel26);
            this.panel43.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel43.Location = new System.Drawing.Point(2044, 0);
            this.panel43.Name = "panel43";
            this.panel43.Size = new System.Drawing.Size(292, 208);
            this.panel43.TabIndex = 18;
            // 
            // panel33
            // 
            this.panel33.Controls.Add(this.labelStudyNr);
            this.panel33.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel33.Location = new System.Drawing.Point(0, 62);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(292, 62);
            this.panel33.TabIndex = 5;
            // 
            // labelStudyNr
            // 
            this.labelStudyNr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelStudyNr.Font = new System.Drawing.Font("Verdana", 32.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStudyNr.Location = new System.Drawing.Point(0, 0);
            this.labelStudyNr.Name = "labelStudyNr";
            this.labelStudyNr.Size = new System.Drawing.Size(292, 62);
            this.labelStudyNr.TabIndex = 40;
            this.labelStudyNr.Text = "1212212";
            this.labelStudyNr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel26
            // 
            this.panel26.Controls.Add(this.labelStudyNrHeader);
            this.panel26.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel26.Location = new System.Drawing.Point(0, 0);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(292, 62);
            this.panel26.TabIndex = 3;
            // 
            // labelStudyNrHeader
            // 
            this.labelStudyNrHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelStudyNrHeader.Font = new System.Drawing.Font("Verdana", 32F);
            this.labelStudyNrHeader.Location = new System.Drawing.Point(0, 0);
            this.labelStudyNrHeader.Name = "labelStudyNrHeader";
            this.labelStudyNrHeader.Size = new System.Drawing.Size(292, 62);
            this.labelStudyNrHeader.TabIndex = 35;
            this.labelStudyNrHeader.Text = "STUDY#:";
            this.labelStudyNrHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel48
            // 
            this.panel48.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel48.Location = new System.Drawing.Point(2032, 0);
            this.panel48.Name = "panel48";
            this.panel48.Size = new System.Drawing.Size(12, 208);
            this.panel48.TabIndex = 17;
            // 
            // panelVert5
            // 
            this.panelVert5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelVert5.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelVert5.Location = new System.Drawing.Point(2030, 0);
            this.panelVert5.Name = "panelVert5";
            this.panelVert5.Size = new System.Drawing.Size(2, 208);
            this.panelVert5.TabIndex = 16;
            // 
            // panel36
            // 
            this.panel36.Controls.Add(this.panel32);
            this.panel36.Controls.Add(this.panel25);
            this.panel36.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel36.Location = new System.Drawing.Point(1692, 0);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(338, 208);
            this.panel36.TabIndex = 15;
            // 
            // panel32
            // 
            this.panel32.Controls.Add(this.labelPatID);
            this.panel32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel32.Location = new System.Drawing.Point(0, 72);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(338, 136);
            this.panel32.TabIndex = 5;
            // 
            // labelPatID
            // 
            this.labelPatID.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPatID.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPatID.Location = new System.Drawing.Point(0, 0);
            this.labelPatID.Name = "labelPatID";
            this.labelPatID.Size = new System.Drawing.Size(338, 136);
            this.labelPatID.TabIndex = 40;
            this.labelPatID.Text = "12341234567";
            this.labelPatID.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel25
            // 
            this.panel25.Controls.Add(this.labelPatIDHeader);
            this.panel25.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel25.Location = new System.Drawing.Point(0, 0);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(338, 72);
            this.panel25.TabIndex = 3;
            // 
            // labelPatIDHeader
            // 
            this.labelPatIDHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPatIDHeader.Font = new System.Drawing.Font("Verdana", 32F);
            this.labelPatIDHeader.Location = new System.Drawing.Point(0, 0);
            this.labelPatIDHeader.Name = "labelPatIDHeader";
            this.labelPatIDHeader.Size = new System.Drawing.Size(338, 72);
            this.labelPatIDHeader.TabIndex = 35;
            this.labelPatIDHeader.Text = "PATIENT ID:";
            this.labelPatIDHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel41
            // 
            this.panel41.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel41.Location = new System.Drawing.Point(1677, 0);
            this.panel41.Name = "panel41";
            this.panel41.Size = new System.Drawing.Size(15, 208);
            this.panel41.TabIndex = 14;
            // 
            // panelVert4
            // 
            this.panelVert4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelVert4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelVert4.Location = new System.Drawing.Point(1675, 0);
            this.panelVert4.Name = "panelVert4";
            this.panelVert4.Size = new System.Drawing.Size(2, 208);
            this.panelVert4.TabIndex = 13;
            // 
            // panel29
            // 
            this.panel29.Controls.Add(this.panel31);
            this.panel29.Controls.Add(this.panel30);
            this.panel29.Controls.Add(this.panel24);
            this.panel29.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel29.Location = new System.Drawing.Point(1325, 0);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(350, 208);
            this.panel29.TabIndex = 12;
            // 
            // panel31
            // 
            this.panel31.Controls.Add(this.labelPhone2);
            this.panel31.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel31.Location = new System.Drawing.Point(0, 124);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(350, 62);
            this.panel31.TabIndex = 6;
            // 
            // labelPhone2
            // 
            this.labelPhone2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPhone2.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPhone2.Location = new System.Drawing.Point(0, 0);
            this.labelPhone2.Name = "labelPhone2";
            this.labelPhone2.Size = new System.Drawing.Size(350, 62);
            this.labelPhone2.TabIndex = 40;
            this.labelPhone2.Text = "800-217-0520";
            this.labelPhone2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel30
            // 
            this.panel30.Controls.Add(this.labelPhone1);
            this.panel30.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel30.Location = new System.Drawing.Point(0, 62);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(350, 62);
            this.panel30.TabIndex = 5;
            // 
            // labelPhone1
            // 
            this.labelPhone1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPhone1.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPhone1.Location = new System.Drawing.Point(0, 0);
            this.labelPhone1.Name = "labelPhone1";
            this.labelPhone1.Size = new System.Drawing.Size(350, 62);
            this.labelPhone1.TabIndex = 40;
            this.labelPhone1.Text = "800-217-0520";
            this.labelPhone1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel24
            // 
            this.panel24.Controls.Add(this.labelPhoneHeader);
            this.panel24.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel24.Location = new System.Drawing.Point(0, 0);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(350, 62);
            this.panel24.TabIndex = 3;
            // 
            // labelPhoneHeader
            // 
            this.labelPhoneHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPhoneHeader.Font = new System.Drawing.Font("Verdana", 32F);
            this.labelPhoneHeader.Location = new System.Drawing.Point(0, 0);
            this.labelPhoneHeader.Name = "labelPhoneHeader";
            this.labelPhoneHeader.Size = new System.Drawing.Size(350, 62);
            this.labelPhoneHeader.TabIndex = 35;
            this.labelPhoneHeader.Text = "PHONE:";
            this.labelPhoneHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel34
            // 
            this.panel34.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel34.Location = new System.Drawing.Point(1310, 0);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(15, 208);
            this.panel34.TabIndex = 11;
            // 
            // panelVert3
            // 
            this.panelVert3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelVert3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelVert3.Location = new System.Drawing.Point(1308, 0);
            this.panelVert3.Name = "panelVert3";
            this.panelVert3.Size = new System.Drawing.Size(2, 208);
            this.panelVert3.TabIndex = 10;
            // 
            // panel22
            // 
            this.panel22.Controls.Add(this.panel23);
            this.panel22.Controls.Add(this.panel19);
            this.panel22.Controls.Add(this.panel18);
            this.panel22.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel22.Location = new System.Drawing.Point(894, 0);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(414, 208);
            this.panel22.TabIndex = 9;
            // 
            // panel23
            // 
            this.panel23.Controls.Add(this.labelZipCity);
            this.panel23.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel23.Location = new System.Drawing.Point(0, 124);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(414, 62);
            this.panel23.TabIndex = 5;
            // 
            // labelZipCity
            // 
            this.labelZipCity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelZipCity.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelZipCity.Location = new System.Drawing.Point(0, 0);
            this.labelZipCity.Name = "labelZipCity";
            this.labelZipCity.Size = new System.Drawing.Size(414, 62);
            this.labelZipCity.TabIndex = 40;
            this.labelZipCity.Text = "Texas TX 200111";
            this.labelZipCity.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.labelAddress);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel19.Location = new System.Drawing.Point(0, 62);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(414, 62);
            this.panel19.TabIndex = 4;
            // 
            // labelAddress
            // 
            this.labelAddress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelAddress.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAddress.Location = new System.Drawing.Point(0, 0);
            this.labelAddress.Name = "labelAddress";
            this.labelAddress.Size = new System.Drawing.Size(414, 62);
            this.labelAddress.TabIndex = 40;
            this.labelAddress.Text = "Alpha Street 112, Houston";
            this.labelAddress.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.labelAddressHeader);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel18.Location = new System.Drawing.Point(0, 0);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(414, 62);
            this.panel18.TabIndex = 3;
            // 
            // labelAddressHeader
            // 
            this.labelAddressHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelAddressHeader.Font = new System.Drawing.Font("Verdana", 32F);
            this.labelAddressHeader.Location = new System.Drawing.Point(0, 0);
            this.labelAddressHeader.Name = "labelAddressHeader";
            this.labelAddressHeader.Size = new System.Drawing.Size(414, 62);
            this.labelAddressHeader.TabIndex = 35;
            this.labelAddressHeader.Text = "ADDRESS:";
            this.labelAddressHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel27
            // 
            this.panel27.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel27.Location = new System.Drawing.Point(882, 0);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(12, 208);
            this.panel27.TabIndex = 8;
            // 
            // panelVert2
            // 
            this.panelVert2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelVert2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelVert2.Location = new System.Drawing.Point(880, 0);
            this.panelVert2.Name = "panelVert2";
            this.panelVert2.Size = new System.Drawing.Size(2, 208);
            this.panelVert2.TabIndex = 7;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.panel17);
            this.panel10.Controls.Add(this.panel16);
            this.panel10.Controls.Add(this.panel15);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel10.Location = new System.Drawing.Point(484, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(396, 208);
            this.panel10.TabIndex = 6;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.labelAgeGender);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel17.Location = new System.Drawing.Point(0, 124);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(396, 62);
            this.panel17.TabIndex = 4;
            // 
            // labelAgeGender
            // 
            this.labelAgeGender.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelAgeGender.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAgeGender.Location = new System.Drawing.Point(0, 0);
            this.labelAgeGender.Name = "labelAgeGender";
            this.labelAgeGender.Size = new System.Drawing.Size(396, 62);
            this.labelAgeGender.TabIndex = 40;
            this.labelAgeGender.Text = "41, male";
            this.labelAgeGender.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.labelDateOfBirth);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel16.Location = new System.Drawing.Point(0, 62);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(396, 62);
            this.panel16.TabIndex = 3;
            // 
            // labelDateOfBirth
            // 
            this.labelDateOfBirth.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDateOfBirth.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDateOfBirth.Location = new System.Drawing.Point(0, 0);
            this.labelDateOfBirth.Name = "labelDateOfBirth";
            this.labelDateOfBirth.Size = new System.Drawing.Size(396, 62);
            this.labelDateOfBirth.TabIndex = 40;
            this.labelDateOfBirth.Text = "06/08/1974";
            this.labelDateOfBirth.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.labelDOBheader);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel15.Location = new System.Drawing.Point(0, 0);
            this.panel15.Margin = new System.Windows.Forms.Padding(0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(396, 62);
            this.panel15.TabIndex = 2;
            // 
            // labelDOBheader
            // 
            this.labelDOBheader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDOBheader.Font = new System.Drawing.Font("Verdana", 32F);
            this.labelDOBheader.Location = new System.Drawing.Point(0, 0);
            this.labelDOBheader.Margin = new System.Windows.Forms.Padding(0);
            this.labelDOBheader.Name = "labelDOBheader";
            this.labelDOBheader.Size = new System.Drawing.Size(396, 62);
            this.labelDOBheader.TabIndex = 35;
            this.labelDOBheader.Text = "DATE OF BIRTH:";
            this.labelDOBheader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel11
            // 
            this.panel11.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel11.Location = new System.Drawing.Point(479, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(5, 208);
            this.panel11.TabIndex = 4;
            // 
            // panelVert1
            // 
            this.panelVert1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelVert1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelVert1.Location = new System.Drawing.Point(477, 0);
            this.panelVert1.Name = "panelVert1";
            this.panelVert1.Size = new System.Drawing.Size(2, 208);
            this.panelVert1.TabIndex = 3;
            // 
            // panel9
            // 
            this.panel9.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel9.Location = new System.Drawing.Point(458, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(19, 208);
            this.panel9.TabIndex = 2;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.panel14);
            this.panel8.Controls.Add(this.panel13);
            this.panel8.Controls.Add(this.panel12);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel8.Location = new System.Drawing.Point(22, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(436, 208);
            this.panel8.TabIndex = 1;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.labelPatientFirstName);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel14.Location = new System.Drawing.Point(0, 124);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(436, 62);
            this.panel14.TabIndex = 3;
            // 
            // labelPatientFirstName
            // 
            this.labelPatientFirstName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPatientFirstName.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPatientFirstName.Location = new System.Drawing.Point(0, 0);
            this.labelPatientFirstName.Name = "labelPatientFirstName";
            this.labelPatientFirstName.Size = new System.Drawing.Size(436, 62);
            this.labelPatientFirstName.TabIndex = 39;
            this.labelPatientFirstName.Text = "Rutger Alexander";
            this.labelPatientFirstName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.labelPatientLastName);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel13.Location = new System.Drawing.Point(0, 62);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(436, 62);
            this.panel13.TabIndex = 2;
            // 
            // labelPatientLastName
            // 
            this.labelPatientLastName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPatientLastName.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPatientLastName.Location = new System.Drawing.Point(0, 0);
            this.labelPatientLastName.Name = "labelPatientLastName";
            this.labelPatientLastName.Size = new System.Drawing.Size(436, 62);
            this.labelPatientLastName.TabIndex = 39;
            this.labelPatientLastName.Text = "Brest van Kempen";
            this.labelPatientLastName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.labelPatientNameHeader);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel12.Location = new System.Drawing.Point(0, 0);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(436, 62);
            this.panel12.TabIndex = 1;
            // 
            // labelPatientNameHeader
            // 
            this.labelPatientNameHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPatientNameHeader.Font = new System.Drawing.Font("Verdana", 32F);
            this.labelPatientNameHeader.Location = new System.Drawing.Point(0, 0);
            this.labelPatientNameHeader.Name = "labelPatientNameHeader";
            this.labelPatientNameHeader.Size = new System.Drawing.Size(436, 62);
            this.labelPatientNameHeader.TabIndex = 34;
            this.labelPatientNameHeader.Text = "PATIENT NAME:";
            this.labelPatientNameHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel7
            // 
            this.panel7.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(22, 208);
            this.panel7.TabIndex = 0;
            // 
            // panelDateTimeofEventStrip
            // 
            this.panelDateTimeofEventStrip.Controls.Add(this.panel52);
            this.panelDateTimeofEventStrip.Controls.Add(this.panel53);
            this.panelDateTimeofEventStrip.Controls.Add(this.panel54);
            this.panelDateTimeofEventStrip.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelDateTimeofEventStrip.Location = new System.Drawing.Point(0, 278);
            this.panelDateTimeofEventStrip.Name = "panelDateTimeofEventStrip";
            this.panelDateTimeofEventStrip.Size = new System.Drawing.Size(2336, 106);
            this.panelDateTimeofEventStrip.TabIndex = 1;
            // 
            // panel52
            // 
            this.panel52.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel52.Location = new System.Drawing.Point(2159, 0);
            this.panel52.Name = "panel52";
            this.panel52.Size = new System.Drawing.Size(177, 106);
            this.panel52.TabIndex = 5;
            this.panel52.Paint += new System.Windows.Forms.PaintEventHandler(this.panel52_Paint);
            // 
            // panel53
            // 
            this.panel53.Controls.Add(this.panel66);
            this.panel53.Controls.Add(this.panel65);
            this.panel53.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel53.Location = new System.Drawing.Point(234, 0);
            this.panel53.Name = "panel53";
            this.panel53.Size = new System.Drawing.Size(2102, 106);
            this.panel53.TabIndex = 4;
            // 
            // panel66
            // 
            this.panel66.Controls.Add(this.labelSingleEventReportTime);
            this.panel66.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel66.Location = new System.Drawing.Point(977, 0);
            this.panel66.Name = "panel66";
            this.panel66.Size = new System.Drawing.Size(1125, 106);
            this.panel66.TabIndex = 5;
            // 
            // labelSingleEventReportTime
            // 
            this.labelSingleEventReportTime.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSingleEventReportTime.Font = new System.Drawing.Font("Verdana", 38.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSingleEventReportTime.Location = new System.Drawing.Point(0, 0);
            this.labelSingleEventReportTime.Name = "labelSingleEventReportTime";
            this.labelSingleEventReportTime.Size = new System.Drawing.Size(359, 106);
            this.labelSingleEventReportTime.TabIndex = 9;
            this.labelSingleEventReportTime.Text = "10:15 AM";
            // 
            // panel65
            // 
            this.panel65.Controls.Add(this.labelSingleEventReportDate);
            this.panel65.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel65.Location = new System.Drawing.Point(0, 0);
            this.panel65.Name = "panel65";
            this.panel65.Size = new System.Drawing.Size(971, 106);
            this.panel65.TabIndex = 4;
            // 
            // labelSingleEventReportDate
            // 
            this.labelSingleEventReportDate.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSingleEventReportDate.Font = new System.Drawing.Font("Verdana", 38.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSingleEventReportDate.Location = new System.Drawing.Point(485, 0);
            this.labelSingleEventReportDate.Name = "labelSingleEventReportDate";
            this.labelSingleEventReportDate.Size = new System.Drawing.Size(486, 106);
            this.labelSingleEventReportDate.TabIndex = 8;
            this.labelSingleEventReportDate.Text = "11/12/2016";
            this.labelSingleEventReportDate.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel54
            // 
            this.panel54.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel54.Location = new System.Drawing.Point(0, 0);
            this.panel54.Name = "panel54";
            this.panel54.Size = new System.Drawing.Size(234, 106);
            this.panel54.TabIndex = 3;
            // 
            // panelCardiacEVentReportNumber
            // 
            this.panelCardiacEVentReportNumber.Controls.Add(this.panel4);
            this.panelCardiacEVentReportNumber.Controls.Add(this.panel3);
            this.panelCardiacEVentReportNumber.Controls.Add(this.panel2);
            this.panelCardiacEVentReportNumber.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCardiacEVentReportNumber.Location = new System.Drawing.Point(0, 175);
            this.panelCardiacEVentReportNumber.Name = "panelCardiacEVentReportNumber";
            this.panelCardiacEVentReportNumber.Size = new System.Drawing.Size(2336, 103);
            this.panelCardiacEVentReportNumber.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.panel50);
            this.panel4.Controls.Add(this.panel47);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(221, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1957, 103);
            this.panel4.TabIndex = 2;
            // 
            // panel50
            // 
            this.panel50.Controls.Add(this.labelReportNr);
            this.panel50.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel50.Location = new System.Drawing.Point(1621, 0);
            this.panel50.Name = "panel50";
            this.panel50.Size = new System.Drawing.Size(336, 103);
            this.panel50.TabIndex = 1;
            // 
            // labelReportNr
            // 
            this.labelReportNr.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelReportNr.Font = new System.Drawing.Font("Verdana", 45.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelReportNr.Location = new System.Drawing.Point(14, 0);
            this.labelReportNr.Name = "labelReportNr";
            this.labelReportNr.Size = new System.Drawing.Size(322, 103);
            this.labelReportNr.TabIndex = 0;
            this.labelReportNr.Text = "175 (2)";
            this.labelReportNr.Visible = false;
            // 
            // panel47
            // 
            this.panel47.Controls.Add(this.labelCardiacEventReportNr);
            this.panel47.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel47.Location = new System.Drawing.Point(0, 0);
            this.panel47.Name = "panel47";
            this.panel47.Size = new System.Drawing.Size(1547, 103);
            this.panel47.TabIndex = 0;
            // 
            // labelCardiacEventReportNr
            // 
            this.labelCardiacEventReportNr.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelCardiacEventReportNr.Font = new System.Drawing.Font("Verdana", 45.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCardiacEventReportNr.Location = new System.Drawing.Point(362, 0);
            this.labelCardiacEventReportNr.Name = "labelCardiacEventReportNr";
            this.labelCardiacEventReportNr.Size = new System.Drawing.Size(1185, 103);
            this.labelCardiacEventReportNr.TabIndex = 0;
            this.labelCardiacEventReportNr.Text = "Cardiac Event Report 99";
            this.labelCardiacEventReportNr.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(2178, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(158, 103);
            this.panel3.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(221, 103);
            this.panel2.TabIndex = 0;
            // 
            // panelPrintHeader
            // 
            this.panelPrintHeader.Controls.Add(this.label4);
            this.panelPrintHeader.Controls.Add(this.panel56);
            this.panelPrintHeader.Controls.Add(this.panel61);
            this.panelPrintHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelPrintHeader.Location = new System.Drawing.Point(0, 0);
            this.panelPrintHeader.Name = "panelPrintHeader";
            this.panelPrintHeader.Size = new System.Drawing.Size(2336, 175);
            this.panelPrintHeader.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(768, 41);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(251, 29);
            this.label4.TabIndex = 9;
            this.label4.Text = "STAT Multi line format";
            // 
            // panel56
            // 
            this.panel56.Controls.Add(this.panel58);
            this.panel56.Controls.Add(this.panel59);
            this.panel56.Controls.Add(this.panel60);
            this.panel56.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel56.Location = new System.Drawing.Point(1605, 0);
            this.panel56.Name = "panel56";
            this.panel56.Size = new System.Drawing.Size(731, 175);
            this.panel56.TabIndex = 8;
            // 
            // panel58
            // 
            this.panel58.Controls.Add(this.labelCenter2);
            this.panel58.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel58.Location = new System.Drawing.Point(0, 82);
            this.panel58.Name = "panel58";
            this.panel58.Size = new System.Drawing.Size(731, 58);
            this.panel58.TabIndex = 15;
            // 
            // labelCenter2
            // 
            this.labelCenter2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCenter2.Font = new System.Drawing.Font("Verdana", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCenter2.Location = new System.Drawing.Point(0, 0);
            this.labelCenter2.Name = "labelCenter2";
            this.labelCenter2.Size = new System.Drawing.Size(731, 58);
            this.labelCenter2.TabIndex = 12;
            this.labelCenter2.Text = "c2";
            this.labelCenter2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel59
            // 
            this.panel59.Controls.Add(this.labelCenter1);
            this.panel59.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel59.Location = new System.Drawing.Point(0, 27);
            this.panel59.Name = "panel59";
            this.panel59.Size = new System.Drawing.Size(731, 55);
            this.panel59.TabIndex = 14;
            // 
            // labelCenter1
            // 
            this.labelCenter1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCenter1.Font = new System.Drawing.Font("Verdana", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCenter1.Location = new System.Drawing.Point(0, 0);
            this.labelCenter1.Name = "labelCenter1";
            this.labelCenter1.Size = new System.Drawing.Size(731, 55);
            this.labelCenter1.TabIndex = 11;
            this.labelCenter1.Text = "c1";
            this.labelCenter1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel60
            // 
            this.panel60.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel60.Location = new System.Drawing.Point(0, 0);
            this.panel60.Name = "panel60";
            this.panel60.Size = new System.Drawing.Size(731, 27);
            this.panel60.TabIndex = 13;
            // 
            // panel61
            // 
            this.panel61.Controls.Add(this.pictureBoxCenter);
            this.panel61.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel61.Location = new System.Drawing.Point(0, 0);
            this.panel61.Name = "panel61";
            this.panel61.Size = new System.Drawing.Size(683, 175);
            this.panel61.TabIndex = 6;
            // 
            // pictureBoxCenter
            // 
            this.pictureBoxCenter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBoxCenter.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBoxCenter.Location = new System.Drawing.Point(27, 0);
            this.pictureBoxCenter.Name = "pictureBoxCenter";
            this.pictureBoxCenter.Size = new System.Drawing.Size(656, 175);
            this.pictureBoxCenter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxCenter.TabIndex = 0;
            this.pictureBoxCenter.TabStop = false;
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripGenPages,
            this.toolStripButtonPrint,
            this.toolStripButton1,
            this.toolStripClipboard1,
            this.toolStripClipboard2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(2589, 39);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripGenPages
            // 
            this.toolStripGenPages.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripGenPages.Image = ((System.Drawing.Image)(resources.GetObject("toolStripGenPages.Image")));
            this.toolStripGenPages.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripGenPages.Name = "toolStripGenPages";
            this.toolStripGenPages.Size = new System.Drawing.Size(112, 36);
            this.toolStripGenPages.Text = "Generating Pages...";
            // 
            // toolStripButtonPrint
            // 
            this.toolStripButtonPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonPrint.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonPrint.Image")));
            this.toolStripButtonPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonPrint.Name = "toolStripButtonPrint";
            this.toolStripButtonPrint.Size = new System.Drawing.Size(36, 36);
            this.toolStripButtonPrint.Text = "Print";
            this.toolStripButtonPrint.Click += new System.EventHandler(this.toolStripButton1_Click_1);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton1.Text = "toolStripButton1";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click_2);
            // 
            // toolStripClipboard1
            // 
            this.toolStripClipboard1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripClipboard1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripClipboard1.Image")));
            this.toolStripClipboard1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripClipboard1.Name = "toolStripClipboard1";
            this.toolStripClipboard1.Size = new System.Drawing.Size(72, 36);
            this.toolStripClipboard1.Text = "Clipboard 1";
            this.toolStripClipboard1.Click += new System.EventHandler(this.toolStripClipboard_Click);
            // 
            // toolStripClipboard2
            // 
            this.toolStripClipboard2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripClipboard2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripClipboard2.Image")));
            this.toolStripClipboard2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripClipboard2.Name = "toolStripClipboard2";
            this.toolStripClipboard2.Size = new System.Drawing.Size(72, 36);
            this.toolStripClipboard2.Text = "Clipboard 2";
            this.toolStripClipboard2.Click += new System.EventHandler(this.toolStripClipboard2_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // panel21
            // 
            this.panel21.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel21.Location = new System.Drawing.Point(0, 39);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(2589, 21);
            this.panel21.TabIndex = 2;
            // 
            // panelPrintArea3
            // 
            this.panelPrintArea3.BackColor = System.Drawing.Color.White;
            this.panelPrintArea3.Controls.Add(this.panelPage2Footer);
            this.panelPrintArea3.Controls.Add(this.panelEventSample3);
            this.panelPrintArea3.Location = new System.Drawing.Point(0, 4000);
            this.panelPrintArea3.Margin = new System.Windows.Forms.Padding(0);
            this.panelPrintArea3.Name = "panelPrintArea3";
            this.panelPrintArea3.Size = new System.Drawing.Size(2336, 3303);
            this.panelPrintArea3.TabIndex = 3;
            // 
            // panelPage2Footer
            // 
            this.panelPage2Footer.Controls.Add(this.label57);
            this.panelPage2Footer.Controls.Add(this.labelPage3);
            this.panelPage2Footer.Controls.Add(this.label58);
            this.panelPage2Footer.Controls.Add(this.labelPagexOf3);
            this.panelPage2Footer.Controls.Add(this.label2);
            this.panelPage2Footer.Controls.Add(this.labelPrintDate3Bottom);
            this.panelPage2Footer.Controls.Add(this.labelPrintDate);
            this.panelPage2Footer.Controls.Add(this.label5);
            this.panelPage2Footer.Controls.Add(this.label6);
            this.panelPage2Footer.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelPage2Footer.Location = new System.Drawing.Point(0, 3219);
            this.panelPage2Footer.Name = "panelPage2Footer";
            this.panelPage2Footer.Size = new System.Drawing.Size(2336, 84);
            this.panelPage2Footer.TabIndex = 37;
            // 
            // label57
            // 
            this.label57.Dock = System.Windows.Forms.DockStyle.Right;
            this.label57.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(1885, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(101, 84);
            this.label57.TabIndex = 23;
            this.label57.Text = "Page";
            this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPage3
            // 
            this.labelPage3.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelPage3.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPage3.Location = new System.Drawing.Point(1986, 0);
            this.labelPage3.Name = "labelPage3";
            this.labelPage3.Size = new System.Drawing.Size(119, 84);
            this.labelPage3.TabIndex = 22;
            this.labelPage3.Text = "2";
            this.labelPage3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label58
            // 
            this.label58.Dock = System.Windows.Forms.DockStyle.Right;
            this.label58.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(2105, 0);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(62, 84);
            this.label58.TabIndex = 21;
            this.label58.Text = "of";
            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPagexOf3
            // 
            this.labelPagexOf3.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelPagexOf3.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPagexOf3.Location = new System.Drawing.Point(2167, 0);
            this.labelPagexOf3.Name = "labelPagexOf3";
            this.labelPagexOf3.Size = new System.Drawing.Size(114, 84);
            this.labelPagexOf3.TabIndex = 20;
            this.labelPagexOf3.Text = "20p2";
            this.labelPagexOf3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Left;
            this.label2.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(699, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(880, 84);
            this.label2.TabIndex = 19;
            this.label2.Text = "Copyright 2017 © Techmedic International B.V.";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelPrintDate3Bottom
            // 
            this.labelPrintDate3Bottom.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPrintDate3Bottom.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPrintDate3Bottom.Location = new System.Drawing.Point(245, 0);
            this.labelPrintDate3Bottom.Name = "labelPrintDate3Bottom";
            this.labelPrintDate3Bottom.Size = new System.Drawing.Size(454, 84);
            this.labelPrintDate3Bottom.TabIndex = 18;
            this.labelPrintDate3Bottom.Text = "12/27/2016 22:22:22 AM";
            this.labelPrintDate3Bottom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelPrintDate
            // 
            this.labelPrintDate.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPrintDate.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPrintDate.Location = new System.Drawing.Point(60, 0);
            this.labelPrintDate.Name = "labelPrintDate";
            this.labelPrintDate.Size = new System.Drawing.Size(185, 84);
            this.labelPrintDate.TabIndex = 17;
            this.labelPrintDate.Text = "Print date:";
            this.labelPrintDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label5.Dock = System.Windows.Forms.DockStyle.Right;
            this.label5.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label5.Location = new System.Drawing.Point(2281, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 84);
            this.label5.TabIndex = 1;
            this.label5.Text = "R2";
            this.label5.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label5.Visible = false;
            // 
            // label6
            // 
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label6.Dock = System.Windows.Forms.DockStyle.Left;
            this.label6.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 84);
            this.label6.TabIndex = 0;
            this.label6.Text = "L2";
            this.label6.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label6.Visible = false;
            // 
            // panel500
            // 
            this.panel500.Controls.Add(this.panel501);
            this.panel500.Controls.Add(this.panel506);
            this.panel500.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel500.Location = new System.Drawing.Point(0, 0);
            this.panel500.Name = "panel500";
            this.panel500.Size = new System.Drawing.Size(2336, 10);
            this.panel500.TabIndex = 4;
            // 
            // panel506
            // 
            this.panel506.Controls.Add(this.pictureBoxCenter2);
            this.panel506.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel506.Location = new System.Drawing.Point(0, 0);
            this.panel506.Name = "panel506";
            this.panel506.Size = new System.Drawing.Size(683, 10);
            this.panel506.TabIndex = 6;
            // 
            // pictureBoxCenter2
            // 
            this.pictureBoxCenter2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBoxCenter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBoxCenter2.Location = new System.Drawing.Point(27, 0);
            this.pictureBoxCenter2.Name = "pictureBoxCenter2";
            this.pictureBoxCenter2.Size = new System.Drawing.Size(656, 10);
            this.pictureBoxCenter2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxCenter2.TabIndex = 0;
            this.pictureBoxCenter2.TabStop = false;
            // 
            // panel501
            // 
            this.panel501.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel501.Location = new System.Drawing.Point(1605, 0);
            this.panel501.Name = "panel501";
            this.panel501.Size = new System.Drawing.Size(731, 10);
            this.panel501.TabIndex = 8;
            // 
            // panelPage2Header
            // 
            this.panelPage2Header.Controls.Add(this.labelPage3ReportNr);
            this.panelPage2Header.Controls.Add(this.labelReportText3);
            this.panelPage2Header.Controls.Add(this.panel74);
            this.panelPage2Header.Controls.Add(this.panel76);
            this.panelPage2Header.Controls.Add(this.panel495);
            this.panelPage2Header.Controls.Add(this.panel499);
            this.panelPage2Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelPage2Header.Location = new System.Drawing.Point(0, 10);
            this.panelPage2Header.Name = "panelPage2Header";
            this.panelPage2Header.Size = new System.Drawing.Size(2336, 90);
            this.panelPage2Header.TabIndex = 0;
            // 
            // panel499
            // 
            this.panel499.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel499.Location = new System.Drawing.Point(0, 0);
            this.panel499.Name = "panel499";
            this.panel499.Size = new System.Drawing.Size(28, 90);
            this.panel499.TabIndex = 0;
            // 
            // panel495
            // 
            this.panel495.Controls.Add(this.panel534);
            this.panel495.Controls.Add(this.panel533);
            this.panel495.Controls.Add(this.panel532);
            this.panel495.Controls.Add(this.panel531);
            this.panel495.Controls.Add(this.panel530);
            this.panel495.Controls.Add(this.panel529);
            this.panel495.Controls.Add(this.panel528);
            this.panel495.Controls.Add(this.panel527);
            this.panel495.Controls.Add(this.panel526);
            this.panel495.Controls.Add(this.panel525);
            this.panel495.Controls.Add(this.panel524);
            this.panel495.Controls.Add(this.panel523);
            this.panel495.Controls.Add(this.panel522);
            this.panel495.Controls.Add(this.panel521);
            this.panel495.Controls.Add(this.panel520);
            this.panel495.Controls.Add(this.panel519);
            this.panel495.Controls.Add(this.panel497);
            this.panel495.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel495.Location = new System.Drawing.Point(28, 0);
            this.panel495.Name = "panel495";
            this.panel495.Size = new System.Drawing.Size(2055, 90);
            this.panel495.TabIndex = 2;
            // 
            // panel497
            // 
            this.panel497.Controls.Add(this.labelPatientID3);
            this.panel497.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel497.Location = new System.Drawing.Point(0, 0);
            this.panel497.Name = "panel497";
            this.panel497.Size = new System.Drawing.Size(200, 90);
            this.panel497.TabIndex = 2;
            // 
            // labelPatientID3
            // 
            this.labelPatientID3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPatientID3.Font = new System.Drawing.Font("Verdana", 24F);
            this.labelPatientID3.Location = new System.Drawing.Point(0, 0);
            this.labelPatientID3.Name = "labelPatientID3";
            this.labelPatientID3.Size = new System.Drawing.Size(200, 90);
            this.labelPatientID3.TabIndex = 36;
            this.labelPatientID3.Text = "Patient ID:";
            this.labelPatientID3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel519
            // 
            this.panel519.Controls.Add(this.labelPatientIDResult3);
            this.panel519.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel519.Location = new System.Drawing.Point(200, 0);
            this.panel519.Name = "panel519";
            this.panel519.Size = new System.Drawing.Size(435, 90);
            this.panel519.TabIndex = 3;
            // 
            // labelPatientIDResult3
            // 
            this.labelPatientIDResult3.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPatientIDResult3.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPatientIDResult3.Location = new System.Drawing.Point(0, 0);
            this.labelPatientIDResult3.Name = "labelPatientIDResult3";
            this.labelPatientIDResult3.Size = new System.Drawing.Size(455, 90);
            this.labelPatientIDResult3.TabIndex = 41;
            this.labelPatientIDResult3.Text = "12341234567";
            this.labelPatientIDResult3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel520
            // 
            this.panel520.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel520.Location = new System.Drawing.Point(635, 0);
            this.panel520.Name = "panel520";
            this.panel520.Size = new System.Drawing.Size(17, 90);
            this.panel520.TabIndex = 4;
            // 
            // panel521
            // 
            this.panel521.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel521.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel521.Location = new System.Drawing.Point(652, 0);
            this.panel521.Name = "panel521";
            this.panel521.Size = new System.Drawing.Size(2, 90);
            this.panel521.TabIndex = 5;
            // 
            // panel522
            // 
            this.panel522.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel522.Location = new System.Drawing.Point(654, 0);
            this.panel522.Name = "panel522";
            this.panel522.Size = new System.Drawing.Size(17, 90);
            this.panel522.TabIndex = 6;
            // 
            // panel523
            // 
            this.panel523.Controls.Add(this.labelPatName3);
            this.panel523.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel523.Location = new System.Drawing.Point(671, 0);
            this.panel523.Name = "panel523";
            this.panel523.Size = new System.Drawing.Size(160, 90);
            this.panel523.TabIndex = 7;
            // 
            // labelPatName3
            // 
            this.labelPatName3.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPatName3.Font = new System.Drawing.Font("Verdana", 24F);
            this.labelPatName3.Location = new System.Drawing.Point(0, 0);
            this.labelPatName3.Name = "labelPatName3";
            this.labelPatName3.Size = new System.Drawing.Size(160, 90);
            this.labelPatName3.TabIndex = 35;
            this.labelPatName3.Text = "Patient:";
            this.labelPatName3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel524
            // 
            this.panel524.Controls.Add(this.labelPatLastNam3);
            this.panel524.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel524.Location = new System.Drawing.Point(831, 0);
            this.panel524.Name = "panel524";
            this.panel524.Size = new System.Drawing.Size(455, 90);
            this.panel524.TabIndex = 8;
            // 
            // labelPatLastNam3
            // 
            this.labelPatLastNam3.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPatLastNam3.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPatLastNam3.Location = new System.Drawing.Point(0, 0);
            this.labelPatLastNam3.Name = "labelPatLastNam3";
            this.labelPatLastNam3.Size = new System.Drawing.Size(500, 90);
            this.labelPatLastNam3.TabIndex = 40;
            this.labelPatLastNam3.Text = "Brest van Kempen";
            this.labelPatLastNam3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel525
            // 
            this.panel525.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel525.Location = new System.Drawing.Point(1286, 0);
            this.panel525.Name = "panel525";
            this.panel525.Size = new System.Drawing.Size(17, 90);
            this.panel525.TabIndex = 9;
            // 
            // panel526
            // 
            this.panel526.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel526.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel526.Location = new System.Drawing.Point(1303, 0);
            this.panel526.Name = "panel526";
            this.panel526.Size = new System.Drawing.Size(2, 90);
            this.panel526.TabIndex = 10;
            // 
            // panel527
            // 
            this.panel527.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel527.Location = new System.Drawing.Point(1305, 0);
            this.panel527.Name = "panel527";
            this.panel527.Size = new System.Drawing.Size(17, 90);
            this.panel527.TabIndex = 11;
            // 
            // panel528
            // 
            this.panel528.Controls.Add(this.labelDateOfBirthPage3);
            this.panel528.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel528.Location = new System.Drawing.Point(1322, 0);
            this.panel528.Name = "panel528";
            this.panel528.Size = new System.Drawing.Size(110, 90);
            this.panel528.TabIndex = 12;
            // 
            // labelDateOfBirthPage3
            // 
            this.labelDateOfBirthPage3.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelDateOfBirthPage3.Font = new System.Drawing.Font("Verdana", 24F);
            this.labelDateOfBirthPage3.Location = new System.Drawing.Point(0, 0);
            this.labelDateOfBirthPage3.Name = "labelDateOfBirthPage3";
            this.labelDateOfBirthPage3.Size = new System.Drawing.Size(110, 90);
            this.labelDateOfBirthPage3.TabIndex = 36;
            this.labelDateOfBirthPage3.Text = "DOB:";
            this.labelDateOfBirthPage3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel529
            // 
            this.panel529.Controls.Add(this.labelDateOfBirthResPage3);
            this.panel529.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel529.Location = new System.Drawing.Point(1432, 0);
            this.panel529.Name = "panel529";
            this.panel529.Size = new System.Drawing.Size(253, 90);
            this.panel529.TabIndex = 13;
            // 
            // labelDateOfBirthResPage3
            // 
            this.labelDateOfBirthResPage3.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelDateOfBirthResPage3.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold);
            this.labelDateOfBirthResPage3.Location = new System.Drawing.Point(0, 0);
            this.labelDateOfBirthResPage3.Name = "labelDateOfBirthResPage3";
            this.labelDateOfBirthResPage3.Size = new System.Drawing.Size(253, 90);
            this.labelDateOfBirthResPage3.TabIndex = 41;
            this.labelDateOfBirthResPage3.Text = "06/08/1974";
            this.labelDateOfBirthResPage3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel530
            // 
            this.panel530.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel530.Location = new System.Drawing.Point(1685, 0);
            this.panel530.Name = "panel530";
            this.panel530.Size = new System.Drawing.Size(17, 90);
            this.panel530.TabIndex = 14;
            // 
            // panel531
            // 
            this.panel531.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel531.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel531.Location = new System.Drawing.Point(1702, 0);
            this.panel531.Name = "panel531";
            this.panel531.Size = new System.Drawing.Size(2, 90);
            this.panel531.TabIndex = 15;
            // 
            // panel532
            // 
            this.panel532.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel532.Location = new System.Drawing.Point(1704, 0);
            this.panel532.Name = "panel532";
            this.panel532.Size = new System.Drawing.Size(17, 90);
            this.panel532.TabIndex = 16;
            // 
            // panel533
            // 
            this.panel533.Controls.Add(this.labelStudyNrPage3);
            this.panel533.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel533.Location = new System.Drawing.Point(1721, 0);
            this.panel533.Name = "panel533";
            this.panel533.Size = new System.Drawing.Size(139, 90);
            this.panel533.TabIndex = 17;
            // 
            // labelStudyNrPage3
            // 
            this.labelStudyNrPage3.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelStudyNrPage3.Font = new System.Drawing.Font("Verdana", 24F);
            this.labelStudyNrPage3.Location = new System.Drawing.Point(0, 0);
            this.labelStudyNrPage3.Name = "labelStudyNrPage3";
            this.labelStudyNrPage3.Size = new System.Drawing.Size(139, 90);
            this.labelStudyNrPage3.TabIndex = 36;
            this.labelStudyNrPage3.Text = "Study:";
            this.labelStudyNrPage3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel534
            // 
            this.panel534.Controls.Add(this.labelStudyNumberResPage3);
            this.panel534.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel534.Location = new System.Drawing.Point(1860, 0);
            this.panel534.Name = "panel534";
            this.panel534.Size = new System.Drawing.Size(185, 90);
            this.panel534.TabIndex = 18;
            // 
            // labelStudyNumberResPage3
            // 
            this.labelStudyNumberResPage3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelStudyNumberResPage3.Font = new System.Drawing.Font("Verdana", 24.25F, System.Drawing.FontStyle.Bold);
            this.labelStudyNumberResPage3.Location = new System.Drawing.Point(0, 0);
            this.labelStudyNumberResPage3.Name = "labelStudyNumberResPage3";
            this.labelStudyNumberResPage3.Size = new System.Drawing.Size(185, 90);
            this.labelStudyNumberResPage3.TabIndex = 41;
            this.labelStudyNumberResPage3.Text = "1212212";
            this.labelStudyNumberResPage3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel76
            // 
            this.panel76.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel76.Location = new System.Drawing.Point(2083, 0);
            this.panel76.Name = "panel76";
            this.panel76.Size = new System.Drawing.Size(17, 90);
            this.panel76.TabIndex = 17;
            // 
            // panel74
            // 
            this.panel74.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel74.Location = new System.Drawing.Point(2100, 0);
            this.panel74.Name = "panel74";
            this.panel74.Size = new System.Drawing.Size(17, 90);
            this.panel74.TabIndex = 18;
            // 
            // labelReportText3
            // 
            this.labelReportText3.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelReportText3.Font = new System.Drawing.Font("Verdana", 24F);
            this.labelReportText3.Location = new System.Drawing.Point(2117, 0);
            this.labelReportText3.Name = "labelReportText3";
            this.labelReportText3.Size = new System.Drawing.Size(140, 90);
            this.labelReportText3.TabIndex = 43;
            this.labelReportText3.Text = "Report:";
            this.labelReportText3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPage3ReportNr
            // 
            this.labelPage3ReportNr.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPage3ReportNr.Font = new System.Drawing.Font("Verdana", 24.25F, System.Drawing.FontStyle.Bold);
            this.labelPage3ReportNr.Location = new System.Drawing.Point(2257, 0);
            this.labelPage3ReportNr.Name = "labelPage3ReportNr";
            this.labelPage3ReportNr.Size = new System.Drawing.Size(107, 90);
            this.labelPage3ReportNr.TabIndex = 44;
            this.labelPage3ReportNr.Text = "12";
            this.labelPage3ReportNr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel450
            // 
            this.panel450.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel450.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel450.Location = new System.Drawing.Point(0, 100);
            this.panel450.Name = "panel450";
            this.panel450.Size = new System.Drawing.Size(2336, 1);
            this.panel450.TabIndex = 3;
            // 
            // panel448
            // 
            this.panel448.Controls.Add(this.panel449);
            this.panel448.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel448.Location = new System.Drawing.Point(0, 101);
            this.panel448.Name = "panel448";
            this.panel448.Size = new System.Drawing.Size(2336, 10);
            this.panel448.TabIndex = 5;
            // 
            // panel449
            // 
            this.panel449.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel449.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel449.Location = new System.Drawing.Point(0, 0);
            this.panel449.Name = "panel449";
            this.panel449.Size = new System.Drawing.Size(2336, 2);
            this.panel449.TabIndex = 0;
            // 
            // panel416
            // 
            this.panel416.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel416.Controls.Add(this.panel417);
            this.panel416.Controls.Add(this.panel418);
            this.panel416.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel416.Location = new System.Drawing.Point(0, 111);
            this.panel416.Name = "panel416";
            this.panel416.Size = new System.Drawing.Size(2336, 84);
            this.panel416.TabIndex = 7;
            // 
            // panel418
            // 
            this.panel418.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel418.Controls.Add(this.label9);
            this.panel418.Controls.Add(this.panel316);
            this.panel418.Controls.Add(this.panel315);
            this.panel418.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel418.Location = new System.Drawing.Point(0, 6);
            this.panel418.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.panel418.Name = "panel418";
            this.panel418.Size = new System.Drawing.Size(2334, 76);
            this.panel418.TabIndex = 0;
            // 
            // panel315
            // 
            this.panel315.AutoSize = true;
            this.panel315.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel315.Location = new System.Drawing.Point(0, 0);
            this.panel315.Name = "panel315";
            this.panel315.Size = new System.Drawing.Size(0, 74);
            this.panel315.TabIndex = 1;
            this.panel315.Visible = false;
            // 
            // panel316
            // 
            this.panel316.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel316.Location = new System.Drawing.Point(0, 0);
            this.panel316.Name = "panel316";
            this.panel316.Size = new System.Drawing.Size(26, 74);
            this.panel316.TabIndex = 2;
            this.panel316.Visible = false;
            // 
            // panel417
            // 
            this.panel417.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel417.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel417.Location = new System.Drawing.Point(0, 0);
            this.panel417.Name = "panel417";
            this.panel417.Size = new System.Drawing.Size(2334, 2);
            this.panel417.TabIndex = 1;
            // 
            // panel391
            // 
            this.panel391.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel391.Controls.Add(this.labelStartPageTime);
            this.panel391.Controls.Add(this.labelStartPageDate);
            this.panel391.Controls.Add(this.panel301);
            this.panel391.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel391.Location = new System.Drawing.Point(0, 195);
            this.panel391.Name = "panel391";
            this.panel391.Size = new System.Drawing.Size(2336, 47);
            this.panel391.TabIndex = 9;
            // 
            // panel387
            // 
            this.panel387.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel387.Controls.Add(this.panel388);
            this.panel387.Controls.Add(this.panel389);
            this.panel387.Controls.Add(this.panel390);
            this.panel387.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel387.Location = new System.Drawing.Point(0, 242);
            this.panel387.Name = "panel387";
            this.panel387.Size = new System.Drawing.Size(2336, 150);
            this.panel387.TabIndex = 10;
            // 
            // panel390
            // 
            this.panel390.Controls.Add(this.label65);
            this.panel390.Controls.Add(this.label64);
            this.panel390.Controls.Add(this.labelSample3End);
            this.panel390.Controls.Add(this.labelSample3StartFull);
            this.panel390.Controls.Add(this.label66);
            this.panel390.Controls.Add(this.labelSample3Lead);
            this.panel390.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel390.Location = new System.Drawing.Point(0, 0);
            this.panel390.Name = "panel390";
            this.panel390.Size = new System.Drawing.Size(121, 148);
            this.panel390.TabIndex = 0;
            // 
            // panel389
            // 
            this.panel389.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel389.Location = new System.Drawing.Point(2311, 0);
            this.panel389.Name = "panel389";
            this.panel389.Size = new System.Drawing.Size(23, 148);
            this.panel389.TabIndex = 1;
            // 
            // panel388
            // 
            this.panel388.Controls.Add(this.pictureBoxSample3);
            this.panel388.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel388.Location = new System.Drawing.Point(121, 0);
            this.panel388.Name = "panel388";
            this.panel388.Size = new System.Drawing.Size(2190, 148);
            this.panel388.TabIndex = 2;
            // 
            // pictureBoxSample3
            // 
            this.pictureBoxSample3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxSample3.BackgroundImage")));
            this.pictureBoxSample3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxSample3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxSample3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxSample3.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxSample3.InitialImage")));
            this.pictureBoxSample3.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxSample3.Name = "pictureBoxSample3";
            this.pictureBoxSample3.Size = new System.Drawing.Size(2190, 148);
            this.pictureBoxSample3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxSample3.TabIndex = 0;
            this.pictureBoxSample3.TabStop = false;
            // 
            // panel384
            // 
            this.panel384.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel384.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel384.Location = new System.Drawing.Point(0, 392);
            this.panel384.Name = "panel384";
            this.panel384.Size = new System.Drawing.Size(2336, 5);
            this.panel384.TabIndex = 11;
            // 
            // panel300
            // 
            this.panel300.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel300.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel300.Location = new System.Drawing.Point(0, 397);
            this.panel300.Name = "panel300";
            this.panel300.Size = new System.Drawing.Size(2336, 2);
            this.panel300.TabIndex = 20;
            // 
            // panel283
            // 
            this.panel283.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel283.Controls.Add(this.panel284);
            this.panel283.Controls.Add(this.panel285);
            this.panel283.Controls.Add(this.panel286);
            this.panel283.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel283.Location = new System.Drawing.Point(0, 399);
            this.panel283.Name = "panel283";
            this.panel283.Size = new System.Drawing.Size(2336, 150);
            this.panel283.TabIndex = 25;
            // 
            // panel286
            // 
            this.panel286.Controls.Add(this.label10);
            this.panel286.Controls.Add(this.label11);
            this.panel286.Controls.Add(this.label12);
            this.panel286.Controls.Add(this.label13);
            this.panel286.Controls.Add(this.label15);
            this.panel286.Controls.Add(this.label16);
            this.panel286.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel286.Location = new System.Drawing.Point(0, 0);
            this.panel286.Name = "panel286";
            this.panel286.Size = new System.Drawing.Size(121, 148);
            this.panel286.TabIndex = 0;
            // 
            // panel285
            // 
            this.panel285.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel285.Location = new System.Drawing.Point(2311, 0);
            this.panel285.Name = "panel285";
            this.panel285.Size = new System.Drawing.Size(23, 148);
            this.panel285.TabIndex = 1;
            // 
            // panel284
            // 
            this.panel284.Controls.Add(this.pictureBox17);
            this.panel284.Controls.Add(this.pictureBoxSample3Full);
            this.panel284.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel284.Location = new System.Drawing.Point(121, 0);
            this.panel284.Name = "panel284";
            this.panel284.Size = new System.Drawing.Size(2190, 148);
            this.panel284.TabIndex = 2;
            // 
            // pictureBoxSample3Full
            // 
            this.pictureBoxSample3Full.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxSample3Full.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxSample3Full.Name = "pictureBoxSample3Full";
            this.pictureBoxSample3Full.Size = new System.Drawing.Size(2190, 148);
            this.pictureBoxSample3Full.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxSample3Full.TabIndex = 0;
            this.pictureBoxSample3Full.TabStop = false;
            // 
            // panel277
            // 
            this.panel277.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel277.Controls.Add(this.labelSample3SweepFull);
            this.panel277.Controls.Add(this.labelSample3AmplFull);
            this.panel277.Controls.Add(this.panel280);
            this.panel277.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel277.Location = new System.Drawing.Point(0, 3029);
            this.panel277.Name = "panel277";
            this.panel277.Size = new System.Drawing.Size(2336, 49);
            this.panel277.TabIndex = 26;
            // 
            // panel280
            // 
            this.panel280.Controls.Add(this.labelEndPageTime);
            this.panel280.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel280.Location = new System.Drawing.Point(2111, 0);
            this.panel280.Name = "panel280";
            this.panel280.Size = new System.Drawing.Size(223, 47);
            this.panel280.TabIndex = 5;
            // 
            // labelEndPageTime
            // 
            this.labelEndPageTime.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelEndPageTime.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEndPageTime.Location = new System.Drawing.Point(-27, 0);
            this.labelEndPageTime.Name = "labelEndPageTime";
            this.labelEndPageTime.Size = new System.Drawing.Size(250, 47);
            this.labelEndPageTime.TabIndex = 1;
            this.labelEndPageTime.Text = "10:15:30 AM";
            this.labelEndPageTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panelEventSample3
            // 
            this.panelEventSample3.AutoSize = true;
            this.panelEventSample3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panelEventSample3.Controls.Add(this.panel278);
            this.panelEventSample3.Controls.Add(this.panel299);
            this.panelEventSample3.Controls.Add(this.panel295);
            this.panelEventSample3.Controls.Add(this.panel294);
            this.panelEventSample3.Controls.Add(this.panel290);
            this.panelEventSample3.Controls.Add(this.panel289);
            this.panelEventSample3.Controls.Add(this.panel281);
            this.panelEventSample3.Controls.Add(this.panel276);
            this.panelEventSample3.Controls.Add(this.panel272);
            this.panelEventSample3.Controls.Add(this.panel271);
            this.panelEventSample3.Controls.Add(this.panel267);
            this.panelEventSample3.Controls.Add(this.panel266);
            this.panelEventSample3.Controls.Add(this.panel257);
            this.panelEventSample3.Controls.Add(this.panel255);
            this.panelEventSample3.Controls.Add(this.panel243);
            this.panelEventSample3.Controls.Add(this.panel241);
            this.panelEventSample3.Controls.Add(this.panel226);
            this.panelEventSample3.Controls.Add(this.panel225);
            this.panelEventSample3.Controls.Add(this.panel211);
            this.panelEventSample3.Controls.Add(this.panel210);
            this.panelEventSample3.Controls.Add(this.panel181);
            this.panelEventSample3.Controls.Add(this.panel180);
            this.panelEventSample3.Controls.Add(this.panel154);
            this.panelEventSample3.Controls.Add(this.panel153);
            this.panelEventSample3.Controls.Add(this.panel118);
            this.panelEventSample3.Controls.Add(this.panel117);
            this.panelEventSample3.Controls.Add(this.panel113);
            this.panelEventSample3.Controls.Add(this.panel112);
            this.panelEventSample3.Controls.Add(this.panel92);
            this.panelEventSample3.Controls.Add(this.panel91);
            this.panelEventSample3.Controls.Add(this.panel62);
            this.panelEventSample3.Controls.Add(this.panel35);
            this.panelEventSample3.Controls.Add(this.panel277);
            this.panelEventSample3.Controls.Add(this.panel283);
            this.panelEventSample3.Controls.Add(this.panel300);
            this.panelEventSample3.Controls.Add(this.panel384);
            this.panelEventSample3.Controls.Add(this.panel387);
            this.panelEventSample3.Controls.Add(this.panel391);
            this.panelEventSample3.Controls.Add(this.panel416);
            this.panelEventSample3.Controls.Add(this.panel448);
            this.panelEventSample3.Controls.Add(this.panel450);
            this.panelEventSample3.Controls.Add(this.panelPage2Header);
            this.panelEventSample3.Controls.Add(this.panel500);
            this.panelEventSample3.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.panelEventSample3.Location = new System.Drawing.Point(0, 8);
            this.panelEventSample3.Name = "panelEventSample3";
            this.panelEventSample3.Size = new System.Drawing.Size(2336, 3078);
            this.panelEventSample3.TabIndex = 36;
            // 
            // labelSample3Lead
            // 
            this.labelSample3Lead.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelSample3Lead.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelSample3Lead.Location = new System.Drawing.Point(0, 0);
            this.labelSample3Lead.Name = "labelSample3Lead";
            this.labelSample3Lead.Size = new System.Drawing.Size(121, 20);
            this.labelSample3Lead.TabIndex = 47;
            this.labelSample3Lead.Text = "LEAD I";
            this.labelSample3Lead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample3StartFull
            // 
            this.labelSample3StartFull.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelSample3StartFull.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelSample3StartFull.Location = new System.Drawing.Point(0, 38);
            this.labelSample3StartFull.Name = "labelSample3StartFull";
            this.labelSample3StartFull.Size = new System.Drawing.Size(121, 20);
            this.labelSample3StartFull.TabIndex = 48;
            this.labelSample3StartFull.Text = "10:14:30 AM";
            this.labelSample3StartFull.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample3End
            // 
            this.labelSample3End.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelSample3End.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelSample3End.Location = new System.Drawing.Point(0, 58);
            this.labelSample3End.Name = "labelSample3End";
            this.labelSample3End.Size = new System.Drawing.Size(121, 20);
            this.labelSample3End.TabIndex = 49;
            this.labelSample3End.Text = "10:15:20 AM";
            this.labelSample3End.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.Dock = System.Windows.Forms.DockStyle.Left;
            this.label9.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold);
            this.label9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label9.Location = new System.Drawing.Point(26, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(284, 74);
            this.label9.TabIndex = 3;
            this.label9.Text = "Full disclosure";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample3SweepFull
            // 
            this.labelSample3SweepFull.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample3SweepFull.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSample3SweepFull.Location = new System.Drawing.Point(1658, 0);
            this.labelSample3SweepFull.Name = "labelSample3SweepFull";
            this.labelSample3SweepFull.Size = new System.Drawing.Size(216, 47);
            this.labelSample3SweepFull.TabIndex = 3;
            this.labelSample3SweepFull.Text = "25 mm/sec";
            this.labelSample3SweepFull.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel35
            // 
            this.panel35.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel35.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel35.Location = new System.Drawing.Point(0, 549);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(2336, 5);
            this.panel35.TabIndex = 27;
            // 
            // panel62
            // 
            this.panel62.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel62.Controls.Add(this.panel63);
            this.panel62.Controls.Add(this.panel64);
            this.panel62.Controls.Add(this.panel68);
            this.panel62.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel62.Location = new System.Drawing.Point(0, 554);
            this.panel62.Name = "panel62";
            this.panel62.Size = new System.Drawing.Size(2336, 150);
            this.panel62.TabIndex = 28;
            // 
            // panel63
            // 
            this.panel63.Controls.Add(this.pictureBox18);
            this.panel63.Controls.Add(this.pictureBox1);
            this.panel63.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel63.Location = new System.Drawing.Point(121, 0);
            this.panel63.Name = "panel63";
            this.panel63.Size = new System.Drawing.Size(2190, 148);
            this.panel63.TabIndex = 2;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(2190, 148);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // panel64
            // 
            this.panel64.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel64.Location = new System.Drawing.Point(2311, 0);
            this.panel64.Name = "panel64";
            this.panel64.Size = new System.Drawing.Size(23, 148);
            this.panel64.TabIndex = 1;
            // 
            // panel68
            // 
            this.panel68.Controls.Add(this.label17);
            this.panel68.Controls.Add(this.label18);
            this.panel68.Controls.Add(this.label19);
            this.panel68.Controls.Add(this.label20);
            this.panel68.Controls.Add(this.label21);
            this.panel68.Controls.Add(this.label22);
            this.panel68.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel68.Location = new System.Drawing.Point(0, 0);
            this.panel68.Name = "panel68";
            this.panel68.Size = new System.Drawing.Size(121, 148);
            this.panel68.TabIndex = 0;
            // 
            // panel91
            // 
            this.panel91.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel91.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel91.Location = new System.Drawing.Point(0, 704);
            this.panel91.Name = "panel91";
            this.panel91.Size = new System.Drawing.Size(2336, 5);
            this.panel91.TabIndex = 29;
            // 
            // panel92
            // 
            this.panel92.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel92.Controls.Add(this.panel93);
            this.panel92.Controls.Add(this.panel94);
            this.panel92.Controls.Add(this.panel111);
            this.panel92.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel92.Location = new System.Drawing.Point(0, 709);
            this.panel92.Name = "panel92";
            this.panel92.Size = new System.Drawing.Size(2336, 150);
            this.panel92.TabIndex = 30;
            // 
            // panel93
            // 
            this.panel93.Controls.Add(this.pictureBox19);
            this.panel93.Controls.Add(this.pictureBox2);
            this.panel93.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel93.Location = new System.Drawing.Point(121, 0);
            this.panel93.Name = "panel93";
            this.panel93.Size = new System.Drawing.Size(2190, 148);
            this.panel93.TabIndex = 2;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(2190, 148);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // panel94
            // 
            this.panel94.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel94.Location = new System.Drawing.Point(2311, 0);
            this.panel94.Name = "panel94";
            this.panel94.Size = new System.Drawing.Size(23, 148);
            this.panel94.TabIndex = 1;
            // 
            // panel111
            // 
            this.panel111.Controls.Add(this.label23);
            this.panel111.Controls.Add(this.label24);
            this.panel111.Controls.Add(this.label25);
            this.panel111.Controls.Add(this.label26);
            this.panel111.Controls.Add(this.label27);
            this.panel111.Controls.Add(this.label28);
            this.panel111.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel111.Location = new System.Drawing.Point(0, 0);
            this.panel111.Name = "panel111";
            this.panel111.Size = new System.Drawing.Size(121, 148);
            this.panel111.TabIndex = 0;
            // 
            // panel112
            // 
            this.panel112.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel112.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel112.Location = new System.Drawing.Point(0, 859);
            this.panel112.Name = "panel112";
            this.panel112.Size = new System.Drawing.Size(2336, 5);
            this.panel112.TabIndex = 31;
            // 
            // panel113
            // 
            this.panel113.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel113.Controls.Add(this.panel114);
            this.panel113.Controls.Add(this.panel115);
            this.panel113.Controls.Add(this.panel116);
            this.panel113.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel113.Location = new System.Drawing.Point(0, 864);
            this.panel113.Name = "panel113";
            this.panel113.Size = new System.Drawing.Size(2336, 150);
            this.panel113.TabIndex = 32;
            // 
            // panel114
            // 
            this.panel114.Controls.Add(this.pictureBox20);
            this.panel114.Controls.Add(this.pictureBox3);
            this.panel114.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel114.Location = new System.Drawing.Point(121, 0);
            this.panel114.Name = "panel114";
            this.panel114.Size = new System.Drawing.Size(2190, 148);
            this.panel114.TabIndex = 2;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox3.Location = new System.Drawing.Point(0, 0);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(2190, 148);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 0;
            this.pictureBox3.TabStop = false;
            // 
            // panel115
            // 
            this.panel115.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel115.Location = new System.Drawing.Point(2311, 0);
            this.panel115.Name = "panel115";
            this.panel115.Size = new System.Drawing.Size(23, 148);
            this.panel115.TabIndex = 1;
            // 
            // panel116
            // 
            this.panel116.Controls.Add(this.label67);
            this.panel116.Controls.Add(this.label68);
            this.panel116.Controls.Add(this.label69);
            this.panel116.Controls.Add(this.label70);
            this.panel116.Controls.Add(this.label71);
            this.panel116.Controls.Add(this.label72);
            this.panel116.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel116.Location = new System.Drawing.Point(0, 0);
            this.panel116.Name = "panel116";
            this.panel116.Size = new System.Drawing.Size(121, 148);
            this.panel116.TabIndex = 0;
            // 
            // panel117
            // 
            this.panel117.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel117.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel117.Location = new System.Drawing.Point(0, 1014);
            this.panel117.Name = "panel117";
            this.panel117.Size = new System.Drawing.Size(2336, 5);
            this.panel117.TabIndex = 33;
            // 
            // panel118
            // 
            this.panel118.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel118.Controls.Add(this.panel119);
            this.panel118.Controls.Add(this.panel148);
            this.panel118.Controls.Add(this.panel152);
            this.panel118.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel118.Location = new System.Drawing.Point(0, 1019);
            this.panel118.Name = "panel118";
            this.panel118.Size = new System.Drawing.Size(2336, 150);
            this.panel118.TabIndex = 34;
            // 
            // panel119
            // 
            this.panel119.Controls.Add(this.pictureBox21);
            this.panel119.Controls.Add(this.pictureBox4);
            this.panel119.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel119.Location = new System.Drawing.Point(121, 0);
            this.panel119.Name = "panel119";
            this.panel119.Size = new System.Drawing.Size(2190, 148);
            this.panel119.TabIndex = 2;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox4.Location = new System.Drawing.Point(0, 0);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(2190, 148);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 0;
            this.pictureBox4.TabStop = false;
            // 
            // panel148
            // 
            this.panel148.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel148.Location = new System.Drawing.Point(2311, 0);
            this.panel148.Name = "panel148";
            this.panel148.Size = new System.Drawing.Size(23, 148);
            this.panel148.TabIndex = 1;
            // 
            // panel152
            // 
            this.panel152.Controls.Add(this.label73);
            this.panel152.Controls.Add(this.label74);
            this.panel152.Controls.Add(this.label75);
            this.panel152.Controls.Add(this.label76);
            this.panel152.Controls.Add(this.label77);
            this.panel152.Controls.Add(this.label78);
            this.panel152.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel152.Location = new System.Drawing.Point(0, 0);
            this.panel152.Name = "panel152";
            this.panel152.Size = new System.Drawing.Size(121, 148);
            this.panel152.TabIndex = 0;
            // 
            // panel153
            // 
            this.panel153.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel153.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel153.Location = new System.Drawing.Point(0, 1169);
            this.panel153.Name = "panel153";
            this.panel153.Size = new System.Drawing.Size(2336, 5);
            this.panel153.TabIndex = 35;
            // 
            // panel154
            // 
            this.panel154.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel154.Controls.Add(this.panel155);
            this.panel154.Controls.Add(this.panel156);
            this.panel154.Controls.Add(this.panel179);
            this.panel154.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel154.Location = new System.Drawing.Point(0, 1174);
            this.panel154.Name = "panel154";
            this.panel154.Size = new System.Drawing.Size(2336, 150);
            this.panel154.TabIndex = 36;
            // 
            // panel155
            // 
            this.panel155.Controls.Add(this.pictureBox22);
            this.panel155.Controls.Add(this.pictureBox5);
            this.panel155.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel155.Location = new System.Drawing.Point(121, 0);
            this.panel155.Name = "panel155";
            this.panel155.Size = new System.Drawing.Size(2190, 148);
            this.panel155.TabIndex = 2;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox5.Location = new System.Drawing.Point(0, 0);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(2190, 148);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 0;
            this.pictureBox5.TabStop = false;
            // 
            // panel156
            // 
            this.panel156.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel156.Location = new System.Drawing.Point(2311, 0);
            this.panel156.Name = "panel156";
            this.panel156.Size = new System.Drawing.Size(23, 148);
            this.panel156.TabIndex = 1;
            // 
            // panel179
            // 
            this.panel179.Controls.Add(this.label79);
            this.panel179.Controls.Add(this.label80);
            this.panel179.Controls.Add(this.label81);
            this.panel179.Controls.Add(this.label82);
            this.panel179.Controls.Add(this.label83);
            this.panel179.Controls.Add(this.label84);
            this.panel179.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel179.Location = new System.Drawing.Point(0, 0);
            this.panel179.Name = "panel179";
            this.panel179.Size = new System.Drawing.Size(121, 148);
            this.panel179.TabIndex = 0;
            // 
            // panel180
            // 
            this.panel180.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel180.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel180.Location = new System.Drawing.Point(0, 1324);
            this.panel180.Name = "panel180";
            this.panel180.Size = new System.Drawing.Size(2336, 5);
            this.panel180.TabIndex = 37;
            // 
            // panel181
            // 
            this.panel181.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel181.Controls.Add(this.panel192);
            this.panel181.Controls.Add(this.panel198);
            this.panel181.Controls.Add(this.panel204);
            this.panel181.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel181.Location = new System.Drawing.Point(0, 1329);
            this.panel181.Name = "panel181";
            this.panel181.Size = new System.Drawing.Size(2336, 150);
            this.panel181.TabIndex = 38;
            // 
            // panel192
            // 
            this.panel192.Controls.Add(this.pictureBox23);
            this.panel192.Controls.Add(this.pictureBox6);
            this.panel192.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel192.Location = new System.Drawing.Point(121, 0);
            this.panel192.Name = "panel192";
            this.panel192.Size = new System.Drawing.Size(2190, 148);
            this.panel192.TabIndex = 2;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox6.Location = new System.Drawing.Point(0, 0);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(2190, 148);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 0;
            this.pictureBox6.TabStop = false;
            // 
            // panel198
            // 
            this.panel198.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel198.Location = new System.Drawing.Point(2311, 0);
            this.panel198.Name = "panel198";
            this.panel198.Size = new System.Drawing.Size(23, 148);
            this.panel198.TabIndex = 1;
            // 
            // panel204
            // 
            this.panel204.Controls.Add(this.label29);
            this.panel204.Controls.Add(this.label31);
            this.panel204.Controls.Add(this.label32);
            this.panel204.Controls.Add(this.label33);
            this.panel204.Controls.Add(this.label35);
            this.panel204.Controls.Add(this.label37);
            this.panel204.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel204.Location = new System.Drawing.Point(0, 0);
            this.panel204.Name = "panel204";
            this.panel204.Size = new System.Drawing.Size(121, 148);
            this.panel204.TabIndex = 0;
            // 
            // panel210
            // 
            this.panel210.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel210.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel210.Location = new System.Drawing.Point(0, 1479);
            this.panel210.Name = "panel210";
            this.panel210.Size = new System.Drawing.Size(2336, 5);
            this.panel210.TabIndex = 39;
            // 
            // panel211
            // 
            this.panel211.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel211.Controls.Add(this.panel212);
            this.panel211.Controls.Add(this.panel223);
            this.panel211.Controls.Add(this.panel224);
            this.panel211.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel211.Location = new System.Drawing.Point(0, 1484);
            this.panel211.Name = "panel211";
            this.panel211.Size = new System.Drawing.Size(2336, 150);
            this.panel211.TabIndex = 40;
            // 
            // panel212
            // 
            this.panel212.Controls.Add(this.pictureBox24);
            this.panel212.Controls.Add(this.pictureBox7);
            this.panel212.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel212.Location = new System.Drawing.Point(121, 0);
            this.panel212.Name = "panel212";
            this.panel212.Size = new System.Drawing.Size(2190, 148);
            this.panel212.TabIndex = 2;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox7.Location = new System.Drawing.Point(0, 0);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(2190, 148);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 0;
            this.pictureBox7.TabStop = false;
            // 
            // panel223
            // 
            this.panel223.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel223.Location = new System.Drawing.Point(2311, 0);
            this.panel223.Name = "panel223";
            this.panel223.Size = new System.Drawing.Size(23, 148);
            this.panel223.TabIndex = 1;
            // 
            // panel224
            // 
            this.panel224.Controls.Add(this.label38);
            this.panel224.Controls.Add(this.label39);
            this.panel224.Controls.Add(this.label40);
            this.panel224.Controls.Add(this.label41);
            this.panel224.Controls.Add(this.label42);
            this.panel224.Controls.Add(this.label43);
            this.panel224.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel224.Location = new System.Drawing.Point(0, 0);
            this.panel224.Name = "panel224";
            this.panel224.Size = new System.Drawing.Size(121, 148);
            this.panel224.TabIndex = 0;
            // 
            // panel225
            // 
            this.panel225.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel225.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel225.Location = new System.Drawing.Point(0, 1634);
            this.panel225.Name = "panel225";
            this.panel225.Size = new System.Drawing.Size(2336, 5);
            this.panel225.TabIndex = 41;
            // 
            // panel226
            // 
            this.panel226.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel226.Controls.Add(this.panel227);
            this.panel226.Controls.Add(this.panel228);
            this.panel226.Controls.Add(this.panel238);
            this.panel226.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel226.Location = new System.Drawing.Point(0, 1639);
            this.panel226.Name = "panel226";
            this.panel226.Size = new System.Drawing.Size(2336, 150);
            this.panel226.TabIndex = 42;
            // 
            // panel227
            // 
            this.panel227.Controls.Add(this.pictureBox25);
            this.panel227.Controls.Add(this.pictureBox8);
            this.panel227.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel227.Location = new System.Drawing.Point(121, 0);
            this.panel227.Name = "panel227";
            this.panel227.Size = new System.Drawing.Size(2190, 148);
            this.panel227.TabIndex = 2;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox8.Location = new System.Drawing.Point(0, 0);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(2190, 148);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox8.TabIndex = 0;
            this.pictureBox8.TabStop = false;
            // 
            // panel228
            // 
            this.panel228.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel228.Location = new System.Drawing.Point(2311, 0);
            this.panel228.Name = "panel228";
            this.panel228.Size = new System.Drawing.Size(23, 148);
            this.panel228.TabIndex = 1;
            // 
            // panel238
            // 
            this.panel238.Controls.Add(this.label44);
            this.panel238.Controls.Add(this.label45);
            this.panel238.Controls.Add(this.label46);
            this.panel238.Controls.Add(this.label47);
            this.panel238.Controls.Add(this.label48);
            this.panel238.Controls.Add(this.label49);
            this.panel238.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel238.Location = new System.Drawing.Point(0, 0);
            this.panel238.Name = "panel238";
            this.panel238.Size = new System.Drawing.Size(121, 148);
            this.panel238.TabIndex = 0;
            // 
            // panel241
            // 
            this.panel241.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel241.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel241.Location = new System.Drawing.Point(0, 1789);
            this.panel241.Name = "panel241";
            this.panel241.Size = new System.Drawing.Size(2336, 5);
            this.panel241.TabIndex = 43;
            // 
            // panel243
            // 
            this.panel243.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel243.Controls.Add(this.panel249);
            this.panel243.Controls.Add(this.panel251);
            this.panel243.Controls.Add(this.panel253);
            this.panel243.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel243.Location = new System.Drawing.Point(0, 1794);
            this.panel243.Name = "panel243";
            this.panel243.Size = new System.Drawing.Size(2336, 150);
            this.panel243.TabIndex = 44;
            // 
            // panel249
            // 
            this.panel249.Controls.Add(this.pictureBox26);
            this.panel249.Controls.Add(this.pictureBox9);
            this.panel249.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel249.Location = new System.Drawing.Point(121, 0);
            this.panel249.Name = "panel249";
            this.panel249.Size = new System.Drawing.Size(2190, 148);
            this.panel249.TabIndex = 2;
            // 
            // pictureBox9
            // 
            this.pictureBox9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox9.Location = new System.Drawing.Point(0, 0);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(2190, 148);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox9.TabIndex = 0;
            this.pictureBox9.TabStop = false;
            // 
            // panel251
            // 
            this.panel251.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel251.Location = new System.Drawing.Point(2311, 0);
            this.panel251.Name = "panel251";
            this.panel251.Size = new System.Drawing.Size(23, 148);
            this.panel251.TabIndex = 1;
            // 
            // panel253
            // 
            this.panel253.Controls.Add(this.label50);
            this.panel253.Controls.Add(this.label51);
            this.panel253.Controls.Add(this.label52);
            this.panel253.Controls.Add(this.label53);
            this.panel253.Controls.Add(this.label54);
            this.panel253.Controls.Add(this.label55);
            this.panel253.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel253.Location = new System.Drawing.Point(0, 0);
            this.panel253.Name = "panel253";
            this.panel253.Size = new System.Drawing.Size(121, 148);
            this.panel253.TabIndex = 0;
            // 
            // panel255
            // 
            this.panel255.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel255.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel255.Location = new System.Drawing.Point(0, 1944);
            this.panel255.Name = "panel255";
            this.panel255.Size = new System.Drawing.Size(2336, 5);
            this.panel255.TabIndex = 45;
            // 
            // panel257
            // 
            this.panel257.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel257.Controls.Add(this.panel261);
            this.panel257.Controls.Add(this.panel263);
            this.panel257.Controls.Add(this.panel265);
            this.panel257.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel257.Location = new System.Drawing.Point(0, 1949);
            this.panel257.Name = "panel257";
            this.panel257.Size = new System.Drawing.Size(2336, 150);
            this.panel257.TabIndex = 46;
            // 
            // panel261
            // 
            this.panel261.Controls.Add(this.pictureBox27);
            this.panel261.Controls.Add(this.pictureBox10);
            this.panel261.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel261.Location = new System.Drawing.Point(121, 0);
            this.panel261.Name = "panel261";
            this.panel261.Size = new System.Drawing.Size(2190, 148);
            this.panel261.TabIndex = 2;
            // 
            // pictureBox10
            // 
            this.pictureBox10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox10.Location = new System.Drawing.Point(0, 0);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(2190, 148);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox10.TabIndex = 0;
            this.pictureBox10.TabStop = false;
            // 
            // panel263
            // 
            this.panel263.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel263.Location = new System.Drawing.Point(2311, 0);
            this.panel263.Name = "panel263";
            this.panel263.Size = new System.Drawing.Size(23, 148);
            this.panel263.TabIndex = 1;
            // 
            // panel265
            // 
            this.panel265.Controls.Add(this.label85);
            this.panel265.Controls.Add(this.label86);
            this.panel265.Controls.Add(this.label87);
            this.panel265.Controls.Add(this.label88);
            this.panel265.Controls.Add(this.label89);
            this.panel265.Controls.Add(this.label90);
            this.panel265.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel265.Location = new System.Drawing.Point(0, 0);
            this.panel265.Name = "panel265";
            this.panel265.Size = new System.Drawing.Size(121, 148);
            this.panel265.TabIndex = 0;
            // 
            // panel266
            // 
            this.panel266.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel266.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel266.Location = new System.Drawing.Point(0, 2099);
            this.panel266.Name = "panel266";
            this.panel266.Size = new System.Drawing.Size(2336, 5);
            this.panel266.TabIndex = 47;
            // 
            // panel267
            // 
            this.panel267.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel267.Controls.Add(this.panel268);
            this.panel267.Controls.Add(this.panel269);
            this.panel267.Controls.Add(this.panel270);
            this.panel267.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel267.Location = new System.Drawing.Point(0, 2104);
            this.panel267.Name = "panel267";
            this.panel267.Size = new System.Drawing.Size(2336, 150);
            this.panel267.TabIndex = 48;
            // 
            // panel268
            // 
            this.panel268.Controls.Add(this.pictureBox28);
            this.panel268.Controls.Add(this.pictureBox11);
            this.panel268.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel268.Location = new System.Drawing.Point(121, 0);
            this.panel268.Name = "panel268";
            this.panel268.Size = new System.Drawing.Size(2190, 148);
            this.panel268.TabIndex = 2;
            // 
            // pictureBox11
            // 
            this.pictureBox11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox11.Location = new System.Drawing.Point(0, 0);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(2190, 148);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox11.TabIndex = 0;
            this.pictureBox11.TabStop = false;
            // 
            // panel269
            // 
            this.panel269.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel269.Location = new System.Drawing.Point(2311, 0);
            this.panel269.Name = "panel269";
            this.panel269.Size = new System.Drawing.Size(23, 148);
            this.panel269.TabIndex = 1;
            // 
            // panel270
            // 
            this.panel270.Controls.Add(this.label56);
            this.panel270.Controls.Add(this.label59);
            this.panel270.Controls.Add(this.label60);
            this.panel270.Controls.Add(this.label61);
            this.panel270.Controls.Add(this.label62);
            this.panel270.Controls.Add(this.label63);
            this.panel270.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel270.Location = new System.Drawing.Point(0, 0);
            this.panel270.Name = "panel270";
            this.panel270.Size = new System.Drawing.Size(121, 148);
            this.panel270.TabIndex = 0;
            // 
            // panel271
            // 
            this.panel271.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel271.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel271.Location = new System.Drawing.Point(0, 2254);
            this.panel271.Name = "panel271";
            this.panel271.Size = new System.Drawing.Size(2336, 5);
            this.panel271.TabIndex = 49;
            // 
            // panel272
            // 
            this.panel272.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel272.Controls.Add(this.panel273);
            this.panel272.Controls.Add(this.panel274);
            this.panel272.Controls.Add(this.panel275);
            this.panel272.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel272.Location = new System.Drawing.Point(0, 2259);
            this.panel272.Name = "panel272";
            this.panel272.Size = new System.Drawing.Size(2336, 150);
            this.panel272.TabIndex = 50;
            // 
            // panel273
            // 
            this.panel273.Controls.Add(this.pictureBox29);
            this.panel273.Controls.Add(this.pictureBox12);
            this.panel273.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel273.Location = new System.Drawing.Point(121, 0);
            this.panel273.Name = "panel273";
            this.panel273.Size = new System.Drawing.Size(2190, 148);
            this.panel273.TabIndex = 2;
            // 
            // pictureBox12
            // 
            this.pictureBox12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox12.Location = new System.Drawing.Point(0, 0);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(2190, 148);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox12.TabIndex = 0;
            this.pictureBox12.TabStop = false;
            // 
            // panel274
            // 
            this.panel274.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel274.Location = new System.Drawing.Point(2311, 0);
            this.panel274.Name = "panel274";
            this.panel274.Size = new System.Drawing.Size(23, 148);
            this.panel274.TabIndex = 1;
            // 
            // panel275
            // 
            this.panel275.Controls.Add(this.label91);
            this.panel275.Controls.Add(this.label92);
            this.panel275.Controls.Add(this.label93);
            this.panel275.Controls.Add(this.label94);
            this.panel275.Controls.Add(this.label95);
            this.panel275.Controls.Add(this.label96);
            this.panel275.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel275.Location = new System.Drawing.Point(0, 0);
            this.panel275.Name = "panel275";
            this.panel275.Size = new System.Drawing.Size(121, 148);
            this.panel275.TabIndex = 0;
            // 
            // panel276
            // 
            this.panel276.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel276.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel276.Location = new System.Drawing.Point(0, 2409);
            this.panel276.Name = "panel276";
            this.panel276.Size = new System.Drawing.Size(2336, 5);
            this.panel276.TabIndex = 51;
            // 
            // panel281
            // 
            this.panel281.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel281.Controls.Add(this.panel282);
            this.panel281.Controls.Add(this.panel287);
            this.panel281.Controls.Add(this.panel288);
            this.panel281.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel281.Location = new System.Drawing.Point(0, 2414);
            this.panel281.Name = "panel281";
            this.panel281.Size = new System.Drawing.Size(2336, 150);
            this.panel281.TabIndex = 52;
            // 
            // panel282
            // 
            this.panel282.Controls.Add(this.pictureBox30);
            this.panel282.Controls.Add(this.pictureBox13);
            this.panel282.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel282.Location = new System.Drawing.Point(121, 0);
            this.panel282.Name = "panel282";
            this.panel282.Size = new System.Drawing.Size(2190, 148);
            this.panel282.TabIndex = 2;
            // 
            // pictureBox13
            // 
            this.pictureBox13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox13.Location = new System.Drawing.Point(0, 0);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(2190, 148);
            this.pictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox13.TabIndex = 0;
            this.pictureBox13.TabStop = false;
            // 
            // panel287
            // 
            this.panel287.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel287.Location = new System.Drawing.Point(2311, 0);
            this.panel287.Name = "panel287";
            this.panel287.Size = new System.Drawing.Size(23, 148);
            this.panel287.TabIndex = 1;
            // 
            // panel288
            // 
            this.panel288.Controls.Add(this.label97);
            this.panel288.Controls.Add(this.label99);
            this.panel288.Controls.Add(this.label100);
            this.panel288.Controls.Add(this.label101);
            this.panel288.Controls.Add(this.label102);
            this.panel288.Controls.Add(this.label103);
            this.panel288.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel288.Location = new System.Drawing.Point(0, 0);
            this.panel288.Name = "panel288";
            this.panel288.Size = new System.Drawing.Size(121, 148);
            this.panel288.TabIndex = 0;
            // 
            // panel289
            // 
            this.panel289.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel289.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel289.Location = new System.Drawing.Point(0, 2564);
            this.panel289.Name = "panel289";
            this.panel289.Size = new System.Drawing.Size(2336, 5);
            this.panel289.TabIndex = 53;
            // 
            // panel290
            // 
            this.panel290.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel290.Controls.Add(this.panel291);
            this.panel290.Controls.Add(this.panel292);
            this.panel290.Controls.Add(this.panel293);
            this.panel290.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel290.Location = new System.Drawing.Point(0, 2569);
            this.panel290.Name = "panel290";
            this.panel290.Size = new System.Drawing.Size(2336, 150);
            this.panel290.TabIndex = 54;
            // 
            // panel291
            // 
            this.panel291.Controls.Add(this.pictureBox31);
            this.panel291.Controls.Add(this.pictureBox14);
            this.panel291.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel291.Location = new System.Drawing.Point(121, 0);
            this.panel291.Name = "panel291";
            this.panel291.Size = new System.Drawing.Size(2190, 148);
            this.panel291.TabIndex = 2;
            // 
            // pictureBox14
            // 
            this.pictureBox14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox14.Location = new System.Drawing.Point(0, 0);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(2190, 148);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox14.TabIndex = 0;
            this.pictureBox14.TabStop = false;
            // 
            // panel292
            // 
            this.panel292.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel292.Location = new System.Drawing.Point(2311, 0);
            this.panel292.Name = "panel292";
            this.panel292.Size = new System.Drawing.Size(23, 148);
            this.panel292.TabIndex = 1;
            // 
            // panel293
            // 
            this.panel293.Controls.Add(this.label105);
            this.panel293.Controls.Add(this.label106);
            this.panel293.Controls.Add(this.label107);
            this.panel293.Controls.Add(this.label108);
            this.panel293.Controls.Add(this.label109);
            this.panel293.Controls.Add(this.label110);
            this.panel293.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel293.Location = new System.Drawing.Point(0, 0);
            this.panel293.Name = "panel293";
            this.panel293.Size = new System.Drawing.Size(121, 148);
            this.panel293.TabIndex = 0;
            // 
            // panel294
            // 
            this.panel294.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel294.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel294.Location = new System.Drawing.Point(0, 2719);
            this.panel294.Name = "panel294";
            this.panel294.Size = new System.Drawing.Size(2336, 5);
            this.panel294.TabIndex = 55;
            // 
            // panel295
            // 
            this.panel295.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel295.Controls.Add(this.panel296);
            this.panel295.Controls.Add(this.panel297);
            this.panel295.Controls.Add(this.panel298);
            this.panel295.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel295.Location = new System.Drawing.Point(0, 2724);
            this.panel295.Name = "panel295";
            this.panel295.Size = new System.Drawing.Size(2336, 150);
            this.panel295.TabIndex = 56;
            // 
            // panel296
            // 
            this.panel296.Controls.Add(this.pictureBox32);
            this.panel296.Controls.Add(this.pictureBox15);
            this.panel296.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel296.Location = new System.Drawing.Point(121, 0);
            this.panel296.Name = "panel296";
            this.panel296.Size = new System.Drawing.Size(2190, 148);
            this.panel296.TabIndex = 2;
            // 
            // pictureBox15
            // 
            this.pictureBox15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox15.Location = new System.Drawing.Point(0, 0);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(2190, 148);
            this.pictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox15.TabIndex = 0;
            this.pictureBox15.TabStop = false;
            // 
            // panel297
            // 
            this.panel297.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel297.Location = new System.Drawing.Point(2311, 0);
            this.panel297.Name = "panel297";
            this.panel297.Size = new System.Drawing.Size(23, 148);
            this.panel297.TabIndex = 1;
            // 
            // panel298
            // 
            this.panel298.Controls.Add(this.label111);
            this.panel298.Controls.Add(this.label112);
            this.panel298.Controls.Add(this.label113);
            this.panel298.Controls.Add(this.label114);
            this.panel298.Controls.Add(this.label115);
            this.panel298.Controls.Add(this.label116);
            this.panel298.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel298.Location = new System.Drawing.Point(0, 0);
            this.panel298.Name = "panel298";
            this.panel298.Size = new System.Drawing.Size(121, 148);
            this.panel298.TabIndex = 0;
            // 
            // panel299
            // 
            this.panel299.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel299.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel299.Location = new System.Drawing.Point(0, 2874);
            this.panel299.Name = "panel299";
            this.panel299.Size = new System.Drawing.Size(2336, 5);
            this.panel299.TabIndex = 57;
            // 
            // label64
            // 
            this.label64.Dock = System.Windows.Forms.DockStyle.Top;
            this.label64.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.Location = new System.Drawing.Point(0, 78);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(121, 18);
            this.label64.TabIndex = 50;
            this.label64.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label65
            // 
            this.label65.Dock = System.Windows.Forms.DockStyle.Top;
            this.label65.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.Location = new System.Drawing.Point(0, 96);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(121, 18);
            this.label65.TabIndex = 51;
            this.label65.Text = "<6 seconds>";
            this.label65.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label66
            // 
            this.label66.Dock = System.Windows.Forms.DockStyle.Top;
            this.label66.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.Location = new System.Drawing.Point(0, 20);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(121, 18);
            this.label66.TabIndex = 52;
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label10
            // 
            this.label10.Dock = System.Windows.Forms.DockStyle.Top;
            this.label10.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(0, 96);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(121, 18);
            this.label10.TabIndex = 57;
            this.label10.Text = "<6 seconds>";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label11
            // 
            this.label11.Dock = System.Windows.Forms.DockStyle.Top;
            this.label11.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(0, 78);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(121, 18);
            this.label11.TabIndex = 56;
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label12
            // 
            this.label12.Dock = System.Windows.Forms.DockStyle.Top;
            this.label12.Font = new System.Drawing.Font("Verdana", 12F);
            this.label12.Location = new System.Drawing.Point(0, 58);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(121, 20);
            this.label12.TabIndex = 55;
            this.label12.Text = "10:15:20 AM";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label13
            // 
            this.label13.Dock = System.Windows.Forms.DockStyle.Top;
            this.label13.Font = new System.Drawing.Font("Verdana", 12F);
            this.label13.Location = new System.Drawing.Point(0, 38);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(121, 20);
            this.label13.TabIndex = 54;
            this.label13.Text = "10:14:30 AM";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label15
            // 
            this.label15.Dock = System.Windows.Forms.DockStyle.Top;
            this.label15.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(0, 20);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(121, 18);
            this.label15.TabIndex = 58;
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label16
            // 
            this.label16.Dock = System.Windows.Forms.DockStyle.Top;
            this.label16.Font = new System.Drawing.Font("Verdana", 12F);
            this.label16.Location = new System.Drawing.Point(0, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(121, 20);
            this.label16.TabIndex = 53;
            this.label16.Text = "LEAD I";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label17
            // 
            this.label17.Dock = System.Windows.Forms.DockStyle.Top;
            this.label17.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(0, 96);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(121, 18);
            this.label17.TabIndex = 57;
            this.label17.Text = "<6 seconds>";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label18
            // 
            this.label18.Dock = System.Windows.Forms.DockStyle.Top;
            this.label18.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(0, 78);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(121, 18);
            this.label18.TabIndex = 56;
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label19
            // 
            this.label19.Dock = System.Windows.Forms.DockStyle.Top;
            this.label19.Font = new System.Drawing.Font("Verdana", 12F);
            this.label19.Location = new System.Drawing.Point(0, 58);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(121, 20);
            this.label19.TabIndex = 55;
            this.label19.Text = "10:15:20 AM";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label20
            // 
            this.label20.Dock = System.Windows.Forms.DockStyle.Top;
            this.label20.Font = new System.Drawing.Font("Verdana", 12F);
            this.label20.Location = new System.Drawing.Point(0, 38);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(121, 20);
            this.label20.TabIndex = 54;
            this.label20.Text = "10:14:30 AM";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label21
            // 
            this.label21.Dock = System.Windows.Forms.DockStyle.Top;
            this.label21.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(0, 20);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(121, 18);
            this.label21.TabIndex = 58;
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label22
            // 
            this.label22.Dock = System.Windows.Forms.DockStyle.Top;
            this.label22.Font = new System.Drawing.Font("Verdana", 12F);
            this.label22.Location = new System.Drawing.Point(0, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(121, 20);
            this.label22.TabIndex = 53;
            this.label22.Text = "LEAD I";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label23
            // 
            this.label23.Dock = System.Windows.Forms.DockStyle.Top;
            this.label23.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(0, 96);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(121, 18);
            this.label23.TabIndex = 57;
            this.label23.Text = "<6 seconds>";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label24
            // 
            this.label24.Dock = System.Windows.Forms.DockStyle.Top;
            this.label24.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(0, 78);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(121, 18);
            this.label24.TabIndex = 56;
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label25
            // 
            this.label25.Dock = System.Windows.Forms.DockStyle.Top;
            this.label25.Font = new System.Drawing.Font("Verdana", 12F);
            this.label25.Location = new System.Drawing.Point(0, 58);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(121, 20);
            this.label25.TabIndex = 55;
            this.label25.Text = "10:15:20 AM";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label26
            // 
            this.label26.Dock = System.Windows.Forms.DockStyle.Top;
            this.label26.Font = new System.Drawing.Font("Verdana", 12F);
            this.label26.Location = new System.Drawing.Point(0, 38);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(121, 20);
            this.label26.TabIndex = 54;
            this.label26.Text = "10:14:30 AM";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label27
            // 
            this.label27.Dock = System.Windows.Forms.DockStyle.Top;
            this.label27.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(0, 20);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(121, 18);
            this.label27.TabIndex = 58;
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label28
            // 
            this.label28.Dock = System.Windows.Forms.DockStyle.Top;
            this.label28.Font = new System.Drawing.Font("Verdana", 12F);
            this.label28.Location = new System.Drawing.Point(0, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(121, 20);
            this.label28.TabIndex = 53;
            this.label28.Text = "LEAD I";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label67
            // 
            this.label67.Dock = System.Windows.Forms.DockStyle.Top;
            this.label67.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.Location = new System.Drawing.Point(0, 96);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(121, 18);
            this.label67.TabIndex = 57;
            this.label67.Text = "<6 seconds>";
            this.label67.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label68
            // 
            this.label68.Dock = System.Windows.Forms.DockStyle.Top;
            this.label68.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.Location = new System.Drawing.Point(0, 78);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(121, 18);
            this.label68.TabIndex = 56;
            this.label68.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label69
            // 
            this.label69.Dock = System.Windows.Forms.DockStyle.Top;
            this.label69.Font = new System.Drawing.Font("Verdana", 12F);
            this.label69.Location = new System.Drawing.Point(0, 58);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(121, 20);
            this.label69.TabIndex = 55;
            this.label69.Text = "10:15:20 AM";
            this.label69.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label70
            // 
            this.label70.Dock = System.Windows.Forms.DockStyle.Top;
            this.label70.Font = new System.Drawing.Font("Verdana", 12F);
            this.label70.Location = new System.Drawing.Point(0, 38);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(121, 20);
            this.label70.TabIndex = 54;
            this.label70.Text = "10:14:30 AM";
            this.label70.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label71
            // 
            this.label71.Dock = System.Windows.Forms.DockStyle.Top;
            this.label71.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.Location = new System.Drawing.Point(0, 20);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(121, 18);
            this.label71.TabIndex = 58;
            this.label71.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label72
            // 
            this.label72.Dock = System.Windows.Forms.DockStyle.Top;
            this.label72.Font = new System.Drawing.Font("Verdana", 12F);
            this.label72.Location = new System.Drawing.Point(0, 0);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(121, 20);
            this.label72.TabIndex = 53;
            this.label72.Text = "LEAD I";
            this.label72.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label73
            // 
            this.label73.Dock = System.Windows.Forms.DockStyle.Top;
            this.label73.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.Location = new System.Drawing.Point(0, 96);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(121, 18);
            this.label73.TabIndex = 57;
            this.label73.Text = "<6 seconds>";
            this.label73.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label74
            // 
            this.label74.Dock = System.Windows.Forms.DockStyle.Top;
            this.label74.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.Location = new System.Drawing.Point(0, 78);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(121, 18);
            this.label74.TabIndex = 56;
            this.label74.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label75
            // 
            this.label75.Dock = System.Windows.Forms.DockStyle.Top;
            this.label75.Font = new System.Drawing.Font("Verdana", 12F);
            this.label75.Location = new System.Drawing.Point(0, 58);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(121, 20);
            this.label75.TabIndex = 55;
            this.label75.Text = "10:15:20 AM";
            this.label75.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label76
            // 
            this.label76.Dock = System.Windows.Forms.DockStyle.Top;
            this.label76.Font = new System.Drawing.Font("Verdana", 12F);
            this.label76.Location = new System.Drawing.Point(0, 38);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(121, 20);
            this.label76.TabIndex = 54;
            this.label76.Text = "10:14:30 AM";
            this.label76.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label77
            // 
            this.label77.Dock = System.Windows.Forms.DockStyle.Top;
            this.label77.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.Location = new System.Drawing.Point(0, 20);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(121, 18);
            this.label77.TabIndex = 58;
            this.label77.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label78
            // 
            this.label78.Dock = System.Windows.Forms.DockStyle.Top;
            this.label78.Font = new System.Drawing.Font("Verdana", 12F);
            this.label78.Location = new System.Drawing.Point(0, 0);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(121, 20);
            this.label78.TabIndex = 53;
            this.label78.Text = "LEAD I";
            this.label78.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label79
            // 
            this.label79.Dock = System.Windows.Forms.DockStyle.Top;
            this.label79.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.Location = new System.Drawing.Point(0, 96);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(121, 18);
            this.label79.TabIndex = 57;
            this.label79.Text = "<6 seconds>";
            this.label79.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label80
            // 
            this.label80.Dock = System.Windows.Forms.DockStyle.Top;
            this.label80.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.Location = new System.Drawing.Point(0, 78);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(121, 18);
            this.label80.TabIndex = 56;
            this.label80.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label81
            // 
            this.label81.Dock = System.Windows.Forms.DockStyle.Top;
            this.label81.Font = new System.Drawing.Font("Verdana", 12F);
            this.label81.Location = new System.Drawing.Point(0, 58);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(121, 20);
            this.label81.TabIndex = 55;
            this.label81.Text = "10:15:20 AM";
            this.label81.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label82
            // 
            this.label82.Dock = System.Windows.Forms.DockStyle.Top;
            this.label82.Font = new System.Drawing.Font("Verdana", 12F);
            this.label82.Location = new System.Drawing.Point(0, 38);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(121, 20);
            this.label82.TabIndex = 54;
            this.label82.Text = "10:14:30 AM";
            this.label82.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label83
            // 
            this.label83.Dock = System.Windows.Forms.DockStyle.Top;
            this.label83.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.Location = new System.Drawing.Point(0, 20);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(121, 18);
            this.label83.TabIndex = 58;
            this.label83.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label84
            // 
            this.label84.Dock = System.Windows.Forms.DockStyle.Top;
            this.label84.Font = new System.Drawing.Font("Verdana", 12F);
            this.label84.Location = new System.Drawing.Point(0, 0);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(121, 20);
            this.label84.TabIndex = 53;
            this.label84.Text = "LEAD I";
            this.label84.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label29
            // 
            this.label29.Dock = System.Windows.Forms.DockStyle.Top;
            this.label29.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(0, 96);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(121, 18);
            this.label29.TabIndex = 57;
            this.label29.Text = "<6 seconds>";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label31
            // 
            this.label31.Dock = System.Windows.Forms.DockStyle.Top;
            this.label31.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(0, 78);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(121, 18);
            this.label31.TabIndex = 56;
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label32
            // 
            this.label32.Dock = System.Windows.Forms.DockStyle.Top;
            this.label32.Font = new System.Drawing.Font("Verdana", 12F);
            this.label32.Location = new System.Drawing.Point(0, 58);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(121, 20);
            this.label32.TabIndex = 55;
            this.label32.Text = "10:15:20 AM";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label33
            // 
            this.label33.Dock = System.Windows.Forms.DockStyle.Top;
            this.label33.Font = new System.Drawing.Font("Verdana", 12F);
            this.label33.Location = new System.Drawing.Point(0, 38);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(121, 20);
            this.label33.TabIndex = 54;
            this.label33.Text = "10:14:30 AM";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label35
            // 
            this.label35.Dock = System.Windows.Forms.DockStyle.Top;
            this.label35.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(0, 20);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(121, 18);
            this.label35.TabIndex = 58;
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label37
            // 
            this.label37.Dock = System.Windows.Forms.DockStyle.Top;
            this.label37.Font = new System.Drawing.Font("Verdana", 12F);
            this.label37.Location = new System.Drawing.Point(0, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(121, 20);
            this.label37.TabIndex = 53;
            this.label37.Text = "LEAD I";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label38
            // 
            this.label38.Dock = System.Windows.Forms.DockStyle.Top;
            this.label38.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(0, 96);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(121, 18);
            this.label38.TabIndex = 57;
            this.label38.Text = "<6 seconds>";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label39
            // 
            this.label39.Dock = System.Windows.Forms.DockStyle.Top;
            this.label39.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(0, 78);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(121, 18);
            this.label39.TabIndex = 56;
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label40
            // 
            this.label40.Dock = System.Windows.Forms.DockStyle.Top;
            this.label40.Font = new System.Drawing.Font("Verdana", 12F);
            this.label40.Location = new System.Drawing.Point(0, 58);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(121, 20);
            this.label40.TabIndex = 55;
            this.label40.Text = "10:15:20 AM";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label41
            // 
            this.label41.Dock = System.Windows.Forms.DockStyle.Top;
            this.label41.Font = new System.Drawing.Font("Verdana", 12F);
            this.label41.Location = new System.Drawing.Point(0, 38);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(121, 20);
            this.label41.TabIndex = 54;
            this.label41.Text = "10:14:30 AM";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label42
            // 
            this.label42.Dock = System.Windows.Forms.DockStyle.Top;
            this.label42.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(0, 20);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(121, 18);
            this.label42.TabIndex = 58;
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label43
            // 
            this.label43.Dock = System.Windows.Forms.DockStyle.Top;
            this.label43.Font = new System.Drawing.Font("Verdana", 12F);
            this.label43.Location = new System.Drawing.Point(0, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(121, 20);
            this.label43.TabIndex = 53;
            this.label43.Text = "LEAD I";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label44
            // 
            this.label44.Dock = System.Windows.Forms.DockStyle.Top;
            this.label44.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(0, 96);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(121, 18);
            this.label44.TabIndex = 57;
            this.label44.Text = "<6 seconds>";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label45
            // 
            this.label45.Dock = System.Windows.Forms.DockStyle.Top;
            this.label45.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(0, 78);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(121, 18);
            this.label45.TabIndex = 56;
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label46
            // 
            this.label46.Dock = System.Windows.Forms.DockStyle.Top;
            this.label46.Font = new System.Drawing.Font("Verdana", 12F);
            this.label46.Location = new System.Drawing.Point(0, 58);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(121, 20);
            this.label46.TabIndex = 55;
            this.label46.Text = "10:15:20 AM";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label47
            // 
            this.label47.Dock = System.Windows.Forms.DockStyle.Top;
            this.label47.Font = new System.Drawing.Font("Verdana", 12F);
            this.label47.Location = new System.Drawing.Point(0, 38);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(121, 20);
            this.label47.TabIndex = 54;
            this.label47.Text = "10:14:30 AM";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label48
            // 
            this.label48.Dock = System.Windows.Forms.DockStyle.Top;
            this.label48.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(0, 20);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(121, 18);
            this.label48.TabIndex = 58;
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label49
            // 
            this.label49.Dock = System.Windows.Forms.DockStyle.Top;
            this.label49.Font = new System.Drawing.Font("Verdana", 12F);
            this.label49.Location = new System.Drawing.Point(0, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(121, 20);
            this.label49.TabIndex = 53;
            this.label49.Text = "LEAD I";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label50
            // 
            this.label50.Dock = System.Windows.Forms.DockStyle.Top;
            this.label50.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(0, 96);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(121, 18);
            this.label50.TabIndex = 57;
            this.label50.Text = "<6 seconds>";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label51
            // 
            this.label51.Dock = System.Windows.Forms.DockStyle.Top;
            this.label51.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(0, 78);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(121, 18);
            this.label51.TabIndex = 56;
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label52
            // 
            this.label52.Dock = System.Windows.Forms.DockStyle.Top;
            this.label52.Font = new System.Drawing.Font("Verdana", 12F);
            this.label52.Location = new System.Drawing.Point(0, 58);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(121, 20);
            this.label52.TabIndex = 55;
            this.label52.Text = "10:15:20 AM";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label53
            // 
            this.label53.Dock = System.Windows.Forms.DockStyle.Top;
            this.label53.Font = new System.Drawing.Font("Verdana", 12F);
            this.label53.Location = new System.Drawing.Point(0, 38);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(121, 20);
            this.label53.TabIndex = 54;
            this.label53.Text = "10:14:30 AM";
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label54
            // 
            this.label54.Dock = System.Windows.Forms.DockStyle.Top;
            this.label54.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(0, 20);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(121, 18);
            this.label54.TabIndex = 58;
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label55
            // 
            this.label55.Dock = System.Windows.Forms.DockStyle.Top;
            this.label55.Font = new System.Drawing.Font("Verdana", 12F);
            this.label55.Location = new System.Drawing.Point(0, 0);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(121, 20);
            this.label55.TabIndex = 53;
            this.label55.Text = "LEAD I";
            this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label56
            // 
            this.label56.Dock = System.Windows.Forms.DockStyle.Top;
            this.label56.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(0, 96);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(121, 18);
            this.label56.TabIndex = 57;
            this.label56.Text = "<6 seconds>";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label59
            // 
            this.label59.Dock = System.Windows.Forms.DockStyle.Top;
            this.label59.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(0, 78);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(121, 18);
            this.label59.TabIndex = 56;
            this.label59.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label60
            // 
            this.label60.Dock = System.Windows.Forms.DockStyle.Top;
            this.label60.Font = new System.Drawing.Font("Verdana", 12F);
            this.label60.Location = new System.Drawing.Point(0, 58);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(121, 20);
            this.label60.TabIndex = 55;
            this.label60.Text = "10:15:20 AM";
            this.label60.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label61
            // 
            this.label61.Dock = System.Windows.Forms.DockStyle.Top;
            this.label61.Font = new System.Drawing.Font("Verdana", 12F);
            this.label61.Location = new System.Drawing.Point(0, 38);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(121, 20);
            this.label61.TabIndex = 54;
            this.label61.Text = "10:14:30 AM";
            this.label61.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label62
            // 
            this.label62.Dock = System.Windows.Forms.DockStyle.Top;
            this.label62.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.Location = new System.Drawing.Point(0, 20);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(121, 18);
            this.label62.TabIndex = 58;
            this.label62.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label63
            // 
            this.label63.Dock = System.Windows.Forms.DockStyle.Top;
            this.label63.Font = new System.Drawing.Font("Verdana", 12F);
            this.label63.Location = new System.Drawing.Point(0, 0);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(121, 20);
            this.label63.TabIndex = 53;
            this.label63.Text = "LEAD I";
            this.label63.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label85
            // 
            this.label85.Dock = System.Windows.Forms.DockStyle.Top;
            this.label85.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.Location = new System.Drawing.Point(0, 96);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(121, 18);
            this.label85.TabIndex = 57;
            this.label85.Text = "<6 seconds>";
            this.label85.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label86
            // 
            this.label86.Dock = System.Windows.Forms.DockStyle.Top;
            this.label86.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.Location = new System.Drawing.Point(0, 78);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(121, 18);
            this.label86.TabIndex = 56;
            this.label86.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label87
            // 
            this.label87.Dock = System.Windows.Forms.DockStyle.Top;
            this.label87.Font = new System.Drawing.Font("Verdana", 12F);
            this.label87.Location = new System.Drawing.Point(0, 58);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(121, 20);
            this.label87.TabIndex = 55;
            this.label87.Text = "10:15:20 AM";
            this.label87.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label88
            // 
            this.label88.Dock = System.Windows.Forms.DockStyle.Top;
            this.label88.Font = new System.Drawing.Font("Verdana", 12F);
            this.label88.Location = new System.Drawing.Point(0, 38);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(121, 20);
            this.label88.TabIndex = 54;
            this.label88.Text = "10:14:30 AM";
            this.label88.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label89
            // 
            this.label89.Dock = System.Windows.Forms.DockStyle.Top;
            this.label89.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label89.Location = new System.Drawing.Point(0, 20);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(121, 18);
            this.label89.TabIndex = 58;
            this.label89.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label90
            // 
            this.label90.Dock = System.Windows.Forms.DockStyle.Top;
            this.label90.Font = new System.Drawing.Font("Verdana", 12F);
            this.label90.Location = new System.Drawing.Point(0, 0);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(121, 20);
            this.label90.TabIndex = 53;
            this.label90.Text = "LEAD I";
            this.label90.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label91
            // 
            this.label91.Dock = System.Windows.Forms.DockStyle.Top;
            this.label91.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label91.Location = new System.Drawing.Point(0, 96);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(121, 18);
            this.label91.TabIndex = 57;
            this.label91.Text = "<6 seconds>";
            this.label91.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label92
            // 
            this.label92.Dock = System.Windows.Forms.DockStyle.Top;
            this.label92.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label92.Location = new System.Drawing.Point(0, 78);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(121, 18);
            this.label92.TabIndex = 56;
            this.label92.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label93
            // 
            this.label93.Dock = System.Windows.Forms.DockStyle.Top;
            this.label93.Font = new System.Drawing.Font("Verdana", 12F);
            this.label93.Location = new System.Drawing.Point(0, 58);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(121, 20);
            this.label93.TabIndex = 55;
            this.label93.Text = "10:15:20 AM";
            this.label93.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label94
            // 
            this.label94.Dock = System.Windows.Forms.DockStyle.Top;
            this.label94.Font = new System.Drawing.Font("Verdana", 12F);
            this.label94.Location = new System.Drawing.Point(0, 38);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(121, 20);
            this.label94.TabIndex = 54;
            this.label94.Text = "10:14:30 AM";
            this.label94.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label95
            // 
            this.label95.Dock = System.Windows.Forms.DockStyle.Top;
            this.label95.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label95.Location = new System.Drawing.Point(0, 20);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(121, 18);
            this.label95.TabIndex = 58;
            this.label95.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label96
            // 
            this.label96.Dock = System.Windows.Forms.DockStyle.Top;
            this.label96.Font = new System.Drawing.Font("Verdana", 12F);
            this.label96.Location = new System.Drawing.Point(0, 0);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(121, 20);
            this.label96.TabIndex = 53;
            this.label96.Text = "LEAD I";
            this.label96.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label97
            // 
            this.label97.Dock = System.Windows.Forms.DockStyle.Top;
            this.label97.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label97.Location = new System.Drawing.Point(0, 96);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(121, 18);
            this.label97.TabIndex = 57;
            this.label97.Text = "<6 seconds>";
            this.label97.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label99
            // 
            this.label99.Dock = System.Windows.Forms.DockStyle.Top;
            this.label99.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label99.Location = new System.Drawing.Point(0, 78);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(121, 18);
            this.label99.TabIndex = 56;
            this.label99.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label100
            // 
            this.label100.Dock = System.Windows.Forms.DockStyle.Top;
            this.label100.Font = new System.Drawing.Font("Verdana", 12F);
            this.label100.Location = new System.Drawing.Point(0, 58);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(121, 20);
            this.label100.TabIndex = 55;
            this.label100.Text = "10:15:20 AM";
            this.label100.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label101
            // 
            this.label101.Dock = System.Windows.Forms.DockStyle.Top;
            this.label101.Font = new System.Drawing.Font("Verdana", 12F);
            this.label101.Location = new System.Drawing.Point(0, 38);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(121, 20);
            this.label101.TabIndex = 54;
            this.label101.Text = "10:14:30 AM";
            this.label101.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label102
            // 
            this.label102.Dock = System.Windows.Forms.DockStyle.Top;
            this.label102.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label102.Location = new System.Drawing.Point(0, 20);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(121, 18);
            this.label102.TabIndex = 58;
            this.label102.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label103
            // 
            this.label103.Dock = System.Windows.Forms.DockStyle.Top;
            this.label103.Font = new System.Drawing.Font("Verdana", 12F);
            this.label103.Location = new System.Drawing.Point(0, 0);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(121, 20);
            this.label103.TabIndex = 53;
            this.label103.Text = "LEAD I";
            this.label103.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label105
            // 
            this.label105.Dock = System.Windows.Forms.DockStyle.Top;
            this.label105.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label105.Location = new System.Drawing.Point(0, 96);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(121, 18);
            this.label105.TabIndex = 57;
            this.label105.Text = "<6 seconds>";
            this.label105.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label106
            // 
            this.label106.Dock = System.Windows.Forms.DockStyle.Top;
            this.label106.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label106.Location = new System.Drawing.Point(0, 78);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(121, 18);
            this.label106.TabIndex = 56;
            this.label106.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label107
            // 
            this.label107.Dock = System.Windows.Forms.DockStyle.Top;
            this.label107.Font = new System.Drawing.Font("Verdana", 12F);
            this.label107.Location = new System.Drawing.Point(0, 58);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(121, 20);
            this.label107.TabIndex = 55;
            this.label107.Text = "10:15:20 AM";
            this.label107.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label108
            // 
            this.label108.Dock = System.Windows.Forms.DockStyle.Top;
            this.label108.Font = new System.Drawing.Font("Verdana", 12F);
            this.label108.Location = new System.Drawing.Point(0, 38);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(121, 20);
            this.label108.TabIndex = 54;
            this.label108.Text = "10:14:30 AM";
            this.label108.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label109
            // 
            this.label109.Dock = System.Windows.Forms.DockStyle.Top;
            this.label109.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label109.Location = new System.Drawing.Point(0, 20);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(121, 18);
            this.label109.TabIndex = 58;
            this.label109.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label110
            // 
            this.label110.Dock = System.Windows.Forms.DockStyle.Top;
            this.label110.Font = new System.Drawing.Font("Verdana", 12F);
            this.label110.Location = new System.Drawing.Point(0, 0);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(121, 20);
            this.label110.TabIndex = 53;
            this.label110.Text = "LEAD I";
            this.label110.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label111
            // 
            this.label111.Dock = System.Windows.Forms.DockStyle.Top;
            this.label111.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label111.Location = new System.Drawing.Point(0, 96);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(121, 18);
            this.label111.TabIndex = 57;
            this.label111.Text = "<6 seconds>";
            this.label111.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label112
            // 
            this.label112.Dock = System.Windows.Forms.DockStyle.Top;
            this.label112.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label112.Location = new System.Drawing.Point(0, 78);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(121, 18);
            this.label112.TabIndex = 56;
            this.label112.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label113
            // 
            this.label113.Dock = System.Windows.Forms.DockStyle.Top;
            this.label113.Font = new System.Drawing.Font("Verdana", 12F);
            this.label113.Location = new System.Drawing.Point(0, 58);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(121, 20);
            this.label113.TabIndex = 55;
            this.label113.Text = "10:15:20 AM";
            this.label113.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label114
            // 
            this.label114.Dock = System.Windows.Forms.DockStyle.Top;
            this.label114.Font = new System.Drawing.Font("Verdana", 12F);
            this.label114.Location = new System.Drawing.Point(0, 38);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(121, 20);
            this.label114.TabIndex = 54;
            this.label114.Text = "10:14:30 AM";
            this.label114.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label115
            // 
            this.label115.Dock = System.Windows.Forms.DockStyle.Top;
            this.label115.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label115.Location = new System.Drawing.Point(0, 20);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(121, 18);
            this.label115.TabIndex = 58;
            this.label115.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label116
            // 
            this.label116.Dock = System.Windows.Forms.DockStyle.Top;
            this.label116.Font = new System.Drawing.Font("Verdana", 12F);
            this.label116.Location = new System.Drawing.Point(0, 0);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(121, 20);
            this.label116.TabIndex = 53;
            this.label116.Text = "LEAD I";
            this.label116.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStartPageTime
            // 
            this.labelStartPageTime.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelStartPageTime.Location = new System.Drawing.Point(164, 0);
            this.labelStartPageTime.Name = "labelStartPageTime";
            this.labelStartPageTime.Size = new System.Drawing.Size(147, 45);
            this.labelStartPageTime.TabIndex = 0;
            this.labelStartPageTime.Text = "10:11:00 AM";
            this.labelStartPageTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel301
            // 
            this.panel301.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel301.Location = new System.Drawing.Point(0, 0);
            this.panel301.Name = "panel301";
            this.panel301.Size = new System.Drawing.Size(26, 45);
            this.panel301.TabIndex = 3;
            this.panel301.Visible = false;
            // 
            // labelStartPageDate
            // 
            this.labelStartPageDate.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelStartPageDate.Location = new System.Drawing.Point(26, 0);
            this.labelStartPageDate.Name = "labelStartPageDate";
            this.labelStartPageDate.Size = new System.Drawing.Size(138, 45);
            this.labelStartPageDate.TabIndex = 4;
            this.labelStartPageDate.Text = "12/21/2017";
            this.labelStartPageDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample3AmplFull
            // 
            this.labelSample3AmplFull.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample3AmplFull.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSample3AmplFull.Location = new System.Drawing.Point(1874, 0);
            this.labelSample3AmplFull.Name = "labelSample3AmplFull";
            this.labelSample3AmplFull.Size = new System.Drawing.Size(237, 47);
            this.labelSample3AmplFull.TabIndex = 2;
            this.labelSample3AmplFull.Text = "1 mm/mV";
            this.labelSample3AmplFull.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel278
            // 
            this.panel278.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel278.Controls.Add(this.panel279);
            this.panel278.Controls.Add(this.panel302);
            this.panel278.Controls.Add(this.panel303);
            this.panel278.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel278.Location = new System.Drawing.Point(0, 2879);
            this.panel278.Name = "panel278";
            this.panel278.Size = new System.Drawing.Size(2336, 150);
            this.panel278.TabIndex = 58;
            // 
            // panel279
            // 
            this.panel279.Controls.Add(this.pictureBox33);
            this.panel279.Controls.Add(this.pictureBox16);
            this.panel279.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel279.Location = new System.Drawing.Point(121, 0);
            this.panel279.Name = "panel279";
            this.panel279.Size = new System.Drawing.Size(2190, 148);
            this.panel279.TabIndex = 2;
            // 
            // pictureBox16
            // 
            this.pictureBox16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox16.Location = new System.Drawing.Point(0, 0);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(2190, 148);
            this.pictureBox16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox16.TabIndex = 0;
            this.pictureBox16.TabStop = false;
            // 
            // panel302
            // 
            this.panel302.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel302.Location = new System.Drawing.Point(2311, 0);
            this.panel302.Name = "panel302";
            this.panel302.Size = new System.Drawing.Size(23, 148);
            this.panel302.TabIndex = 1;
            // 
            // panel303
            // 
            this.panel303.Controls.Add(this.label119);
            this.panel303.Controls.Add(this.label120);
            this.panel303.Controls.Add(this.label121);
            this.panel303.Controls.Add(this.label122);
            this.panel303.Controls.Add(this.label123);
            this.panel303.Controls.Add(this.label124);
            this.panel303.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel303.Location = new System.Drawing.Point(0, 0);
            this.panel303.Name = "panel303";
            this.panel303.Size = new System.Drawing.Size(121, 148);
            this.panel303.TabIndex = 0;
            // 
            // label119
            // 
            this.label119.Dock = System.Windows.Forms.DockStyle.Top;
            this.label119.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label119.Location = new System.Drawing.Point(0, 96);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(121, 18);
            this.label119.TabIndex = 57;
            this.label119.Text = "<6 seconds>";
            this.label119.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label120
            // 
            this.label120.Dock = System.Windows.Forms.DockStyle.Top;
            this.label120.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label120.Location = new System.Drawing.Point(0, 78);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(121, 18);
            this.label120.TabIndex = 56;
            this.label120.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label121
            // 
            this.label121.Dock = System.Windows.Forms.DockStyle.Top;
            this.label121.Font = new System.Drawing.Font("Verdana", 12F);
            this.label121.Location = new System.Drawing.Point(0, 58);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(121, 20);
            this.label121.TabIndex = 55;
            this.label121.Text = "10:15:20 AM";
            this.label121.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label122
            // 
            this.label122.Dock = System.Windows.Forms.DockStyle.Top;
            this.label122.Font = new System.Drawing.Font("Verdana", 12F);
            this.label122.Location = new System.Drawing.Point(0, 38);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(121, 20);
            this.label122.TabIndex = 54;
            this.label122.Text = "10:14:30 AM";
            this.label122.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label123
            // 
            this.label123.Dock = System.Windows.Forms.DockStyle.Top;
            this.label123.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label123.Location = new System.Drawing.Point(0, 20);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(121, 18);
            this.label123.TabIndex = 58;
            this.label123.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label124
            // 
            this.label124.Dock = System.Windows.Forms.DockStyle.Top;
            this.label124.Font = new System.Drawing.Font("Verdana", 12F);
            this.label124.Location = new System.Drawing.Point(0, 0);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(121, 20);
            this.label124.TabIndex = 53;
            this.label124.Text = "LEAD I";
            this.label124.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox17
            // 
            this.pictureBox17.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox17.BackgroundImage")));
            this.pictureBox17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox17.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox17.InitialImage")));
            this.pictureBox17.Location = new System.Drawing.Point(0, 0);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(2190, 148);
            this.pictureBox17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox17.TabIndex = 1;
            this.pictureBox17.TabStop = false;
            // 
            // pictureBox18
            // 
            this.pictureBox18.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox18.BackgroundImage")));
            this.pictureBox18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox18.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox18.InitialImage")));
            this.pictureBox18.Location = new System.Drawing.Point(0, 0);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(2190, 148);
            this.pictureBox18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox18.TabIndex = 1;
            this.pictureBox18.TabStop = false;
            // 
            // pictureBox19
            // 
            this.pictureBox19.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox19.BackgroundImage")));
            this.pictureBox19.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox19.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox19.InitialImage")));
            this.pictureBox19.Location = new System.Drawing.Point(0, 0);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(2190, 148);
            this.pictureBox19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox19.TabIndex = 1;
            this.pictureBox19.TabStop = false;
            // 
            // pictureBox20
            // 
            this.pictureBox20.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox20.BackgroundImage")));
            this.pictureBox20.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox20.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox20.InitialImage")));
            this.pictureBox20.Location = new System.Drawing.Point(0, 0);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(2190, 148);
            this.pictureBox20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox20.TabIndex = 1;
            this.pictureBox20.TabStop = false;
            // 
            // pictureBox21
            // 
            this.pictureBox21.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox21.BackgroundImage")));
            this.pictureBox21.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox21.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox21.InitialImage")));
            this.pictureBox21.Location = new System.Drawing.Point(0, 0);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(2190, 148);
            this.pictureBox21.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox21.TabIndex = 2;
            this.pictureBox21.TabStop = false;
            // 
            // pictureBox22
            // 
            this.pictureBox22.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox22.BackgroundImage")));
            this.pictureBox22.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox22.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox22.InitialImage")));
            this.pictureBox22.Location = new System.Drawing.Point(0, 0);
            this.pictureBox22.Name = "pictureBox22";
            this.pictureBox22.Size = new System.Drawing.Size(2190, 148);
            this.pictureBox22.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox22.TabIndex = 2;
            this.pictureBox22.TabStop = false;
            // 
            // pictureBox23
            // 
            this.pictureBox23.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox23.BackgroundImage")));
            this.pictureBox23.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox23.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox23.InitialImage")));
            this.pictureBox23.Location = new System.Drawing.Point(0, 0);
            this.pictureBox23.Name = "pictureBox23";
            this.pictureBox23.Size = new System.Drawing.Size(2190, 148);
            this.pictureBox23.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox23.TabIndex = 2;
            this.pictureBox23.TabStop = false;
            // 
            // pictureBox24
            // 
            this.pictureBox24.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox24.BackgroundImage")));
            this.pictureBox24.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox24.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox24.InitialImage")));
            this.pictureBox24.Location = new System.Drawing.Point(0, 0);
            this.pictureBox24.Name = "pictureBox24";
            this.pictureBox24.Size = new System.Drawing.Size(2190, 148);
            this.pictureBox24.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox24.TabIndex = 2;
            this.pictureBox24.TabStop = false;
            // 
            // pictureBox25
            // 
            this.pictureBox25.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox25.BackgroundImage")));
            this.pictureBox25.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox25.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox25.InitialImage")));
            this.pictureBox25.Location = new System.Drawing.Point(0, 0);
            this.pictureBox25.Name = "pictureBox25";
            this.pictureBox25.Size = new System.Drawing.Size(2190, 148);
            this.pictureBox25.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox25.TabIndex = 2;
            this.pictureBox25.TabStop = false;
            // 
            // pictureBox26
            // 
            this.pictureBox26.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox26.BackgroundImage")));
            this.pictureBox26.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox26.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox26.InitialImage")));
            this.pictureBox26.Location = new System.Drawing.Point(0, 0);
            this.pictureBox26.Name = "pictureBox26";
            this.pictureBox26.Size = new System.Drawing.Size(2190, 148);
            this.pictureBox26.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox26.TabIndex = 2;
            this.pictureBox26.TabStop = false;
            // 
            // pictureBox27
            // 
            this.pictureBox27.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox27.BackgroundImage")));
            this.pictureBox27.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox27.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox27.InitialImage")));
            this.pictureBox27.Location = new System.Drawing.Point(0, 0);
            this.pictureBox27.Name = "pictureBox27";
            this.pictureBox27.Size = new System.Drawing.Size(2190, 148);
            this.pictureBox27.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox27.TabIndex = 2;
            this.pictureBox27.TabStop = false;
            // 
            // pictureBox28
            // 
            this.pictureBox28.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox28.BackgroundImage")));
            this.pictureBox28.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox28.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox28.InitialImage")));
            this.pictureBox28.Location = new System.Drawing.Point(0, 0);
            this.pictureBox28.Name = "pictureBox28";
            this.pictureBox28.Size = new System.Drawing.Size(2190, 148);
            this.pictureBox28.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox28.TabIndex = 3;
            this.pictureBox28.TabStop = false;
            // 
            // pictureBox29
            // 
            this.pictureBox29.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox29.BackgroundImage")));
            this.pictureBox29.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox29.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox29.InitialImage")));
            this.pictureBox29.Location = new System.Drawing.Point(0, 0);
            this.pictureBox29.Name = "pictureBox29";
            this.pictureBox29.Size = new System.Drawing.Size(2190, 148);
            this.pictureBox29.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox29.TabIndex = 3;
            this.pictureBox29.TabStop = false;
            // 
            // pictureBox30
            // 
            this.pictureBox30.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox30.BackgroundImage")));
            this.pictureBox30.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox30.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox30.InitialImage")));
            this.pictureBox30.Location = new System.Drawing.Point(0, 0);
            this.pictureBox30.Name = "pictureBox30";
            this.pictureBox30.Size = new System.Drawing.Size(2190, 148);
            this.pictureBox30.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox30.TabIndex = 3;
            this.pictureBox30.TabStop = false;
            // 
            // pictureBox31
            // 
            this.pictureBox31.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox31.BackgroundImage")));
            this.pictureBox31.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox31.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox31.InitialImage")));
            this.pictureBox31.Location = new System.Drawing.Point(0, 0);
            this.pictureBox31.Name = "pictureBox31";
            this.pictureBox31.Size = new System.Drawing.Size(2190, 148);
            this.pictureBox31.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox31.TabIndex = 3;
            this.pictureBox31.TabStop = false;
            // 
            // pictureBox32
            // 
            this.pictureBox32.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox32.BackgroundImage")));
            this.pictureBox32.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox32.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox32.InitialImage")));
            this.pictureBox32.Location = new System.Drawing.Point(0, 0);
            this.pictureBox32.Name = "pictureBox32";
            this.pictureBox32.Size = new System.Drawing.Size(2190, 148);
            this.pictureBox32.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox32.TabIndex = 3;
            this.pictureBox32.TabStop = false;
            // 
            // pictureBox33
            // 
            this.pictureBox33.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox33.BackgroundImage")));
            this.pictureBox33.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox33.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox33.InitialImage")));
            this.pictureBox33.Location = new System.Drawing.Point(0, 0);
            this.pictureBox33.Name = "pictureBox33";
            this.pictureBox33.Size = new System.Drawing.Size(2190, 148);
            this.pictureBox33.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox33.TabIndex = 3;
            this.pictureBox33.TabStop = false;
            // 
            // CPrintAnalysisMultiForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(2606, 1847);
            this.Controls.Add(this.panelPrintArea3);
            this.Controls.Add(this.panelPrintArea1);
            this.Controls.Add(this.panel21);
            this.Controls.Add(this.toolStrip1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "CPrintAnalysisMultiForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Print Analysis";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CPrintAnalysisMultiForm_FormClosing);
            this.Shown += new System.EventHandler(this.CPrintAnalysisMultiForm_Shown);
            this.panelPrintArea1.ResumeLayout(false);
            this.panel51.ResumeLayout(false);
            this.panel510.ResumeLayout(false);
            this.panel509.ResumeLayout(false);
            this.panel508.ResumeLayout(false);
            this.panel507.ResumeLayout(false);
            this.panel78.ResumeLayout(false);
            this.panel110.ResumeLayout(false);
            this.panel109.ResumeLayout(false);
            this.panel108.ResumeLayout(false);
            this.panel107.ResumeLayout(false);
            this.panel106.ResumeLayout(false);
            this.panel105.ResumeLayout(false);
            this.panelSample2Time.ResumeLayout(false);
            this.panel102.ResumeLayout(false);
            this.panel101.ResumeLayout(false);
            this.panel100.ResumeLayout(false);
            this.panel99.ResumeLayout(false);
            this.panel98.ResumeLayout(false);
            this.panelSeceondStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxZoom2)).EndInit();
            this.panelSample2Lead.ResumeLayout(false);
            this.panel95.ResumeLayout(false);
            this.panelSample2Header.ResumeLayout(false);
            this.panelSample2Header.PerformLayout();
            this.panel90.ResumeLayout(false);
            this.panel89.ResumeLayout(false);
            this.panel89.PerformLayout();
            this.panel88.ResumeLayout(false);
            this.panel87.ResumeLayout(false);
            this.panel86.ResumeLayout(false);
            this.panel86.PerformLayout();
            this.panel85.ResumeLayout(false);
            this.panel149.ResumeLayout(false);
            this.panel83.ResumeLayout(false);
            this.panel82.ResumeLayout(false);
            this.panel81.ResumeLayout(false);
            this.panel80.ResumeLayout(false);
            this.panel79.ResumeLayout(false);
            this.panelFullStrip.ResumeLayout(false);
            this.panel44.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip1)).EndInit();
            this.panel147.ResumeLayout(false);
            this.panel38.ResumeLayout(false);
            this.panel146.ResumeLayout(false);
            this.panel232.ResumeLayout(false);
            this.panel234.ResumeLayout(false);
            this.panel233.ResumeLayout(false);
            this.panel230.ResumeLayout(false);
            this.panel145.ResumeLayout(false);
            this.panelSample1STrip.ResumeLayout(false);
            this.panel37.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxZoom1)).EndInit();
            this.panel144.ResumeLayout(false);
            this.panel142.ResumeLayout(false);
            this.panel142.PerformLayout();
            this.panel222.ResumeLayout(false);
            this.panel222.PerformLayout();
            this.panel221.ResumeLayout(false);
            this.panel220.ResumeLayout(false);
            this.panel219.ResumeLayout(false);
            this.panel219.PerformLayout();
            this.panel218.ResumeLayout(false);
            this.panelEventStats.ResumeLayout(false);
            this.panel162.ResumeLayout(false);
            this.panel215.ResumeLayout(false);
            this.panel213.ResumeLayout(false);
            this.panel214.ResumeLayout(false);
            this.panel209.ResumeLayout(false);
            this.panel205.ResumeLayout(false);
            this.panel206.ResumeLayout(false);
            this.panel207.ResumeLayout(false);
            this.panel208.ResumeLayout(false);
            this.panel203.ResumeLayout(false);
            this.panel199.ResumeLayout(false);
            this.panel200.ResumeLayout(false);
            this.panel201.ResumeLayout(false);
            this.panel202.ResumeLayout(false);
            this.panel197.ResumeLayout(false);
            this.panel193.ResumeLayout(false);
            this.panel194.ResumeLayout(false);
            this.panel195.ResumeLayout(false);
            this.panel196.ResumeLayout(false);
            this.panel191.ResumeLayout(false);
            this.panel187.ResumeLayout(false);
            this.panel188.ResumeLayout(false);
            this.panel189.ResumeLayout(false);
            this.panel190.ResumeLayout(false);
            this.panel122.ResumeLayout(false);
            this.panel185.ResumeLayout(false);
            this.panel184.ResumeLayout(false);
            this.panel183.ResumeLayout(false);
            this.panel160.ResumeLayout(false);
            this.panel28.ResumeLayout(false);
            this.panel45.ResumeLayout(false);
            this.panel46.ResumeLayout(false);
            this.panel178.ResumeLayout(false);
            this.panel176.ResumeLayout(false);
            this.panel177.ResumeLayout(false);
            this.panel175.ResumeLayout(false);
            this.panel173.ResumeLayout(false);
            this.panel174.ResumeLayout(false);
            this.panel172.ResumeLayout(false);
            this.panel170.ResumeLayout(false);
            this.panel171.ResumeLayout(false);
            this.panel167.ResumeLayout(false);
            this.panel169.ResumeLayout(false);
            this.panel168.ResumeLayout(false);
            this.panelCurrentEventStats.ResumeLayout(false);
            this.panel163.ResumeLayout(false);
            this.panel165.ResumeLayout(false);
            this.panelPreviousTransm.ResumeLayout(false);
            this.panelTablePrevious3Trans.ResumeLayout(false);
            this.panel159.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPrevThreeTX)).EndInit();
            this.panel71.ResumeLayout(false);
            this.panelECGSweepAmpIndicator.ResumeLayout(false);
            this.panelSweepSpeed.ResumeLayout(false);
            this.panelAmpPanel.ResumeLayout(false);
            this.panelECGBaselineSTrip.ResumeLayout(false);
            this.panel158.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBaselineReferenceECGStrip)).EndInit();
            this.panelTimeHeadingsPerStrip.ResumeLayout(false);
            this.panel151.ResumeLayout(false);
            this.panel141.ResumeLayout(false);
            this.panel140.ResumeLayout(false);
            this.panel139.ResumeLayout(false);
            this.panel138.ResumeLayout(false);
            this.panel136.ResumeLayout(false);
            this.panel135.ResumeLayout(false);
            this.panel134.ResumeLayout(false);
            this.panel133.ResumeLayout(false);
            this.panel69.ResumeLayout(false);
            this.panel132.ResumeLayout(false);
            this.panel131.ResumeLayout(false);
            this.panel130.ResumeLayout(false);
            this.panel129.ResumeLayout(false);
            this.panel128.ResumeLayout(false);
            this.panel127.ResumeLayout(false);
            this.panel126.ResumeLayout(false);
            this.panel125.ResumeLayout(false);
            this.panelBaseLineHeader.ResumeLayout(false);
            this.panel123.ResumeLayout(false);
            this.panel67.ResumeLayout(false);
            this.panel256.ResumeLayout(false);
            this.panel264.ResumeLayout(false);
            this.panel252.ResumeLayout(false);
            this.panel262.ResumeLayout(false);
            this.panel244.ResumeLayout(false);
            this.panel260.ResumeLayout(false);
            this.panel240.ResumeLayout(false);
            this.panel248.ResumeLayout(false);
            this.panel247.ResumeLayout(false);
            this.panel236.ResumeLayout(false);
            this.panel246.ResumeLayout(false);
            this.panel245.ResumeLayout(false);
            this.panelSecPatBar.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel43.ResumeLayout(false);
            this.panel33.ResumeLayout(false);
            this.panel26.ResumeLayout(false);
            this.panel36.ResumeLayout(false);
            this.panel32.ResumeLayout(false);
            this.panel25.ResumeLayout(false);
            this.panel29.ResumeLayout(false);
            this.panel31.ResumeLayout(false);
            this.panel30.ResumeLayout(false);
            this.panel24.ResumeLayout(false);
            this.panel22.ResumeLayout(false);
            this.panel23.ResumeLayout(false);
            this.panel19.ResumeLayout(false);
            this.panel18.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panelDateTimeofEventStrip.ResumeLayout(false);
            this.panel53.ResumeLayout(false);
            this.panel66.ResumeLayout(false);
            this.panel65.ResumeLayout(false);
            this.panelCardiacEVentReportNumber.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel50.ResumeLayout(false);
            this.panel47.ResumeLayout(false);
            this.panelPrintHeader.ResumeLayout(false);
            this.panelPrintHeader.PerformLayout();
            this.panel56.ResumeLayout(false);
            this.panel58.ResumeLayout(false);
            this.panel59.ResumeLayout(false);
            this.panel61.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCenter)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panelPrintArea3.ResumeLayout(false);
            this.panelPrintArea3.PerformLayout();
            this.panelPage2Footer.ResumeLayout(false);
            this.panel500.ResumeLayout(false);
            this.panel506.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCenter2)).EndInit();
            this.panelPage2Header.ResumeLayout(false);
            this.panel495.ResumeLayout(false);
            this.panel497.ResumeLayout(false);
            this.panel519.ResumeLayout(false);
            this.panel523.ResumeLayout(false);
            this.panel524.ResumeLayout(false);
            this.panel528.ResumeLayout(false);
            this.panel529.ResumeLayout(false);
            this.panel533.ResumeLayout(false);
            this.panel534.ResumeLayout(false);
            this.panel448.ResumeLayout(false);
            this.panel416.ResumeLayout(false);
            this.panel418.ResumeLayout(false);
            this.panel418.PerformLayout();
            this.panel391.ResumeLayout(false);
            this.panel387.ResumeLayout(false);
            this.panel390.ResumeLayout(false);
            this.panel388.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample3)).EndInit();
            this.panel283.ResumeLayout(false);
            this.panel286.ResumeLayout(false);
            this.panel284.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample3Full)).EndInit();
            this.panel277.ResumeLayout(false);
            this.panel280.ResumeLayout(false);
            this.panelEventSample3.ResumeLayout(false);
            this.panel62.ResumeLayout(false);
            this.panel63.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel68.ResumeLayout(false);
            this.panel92.ResumeLayout(false);
            this.panel93.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel111.ResumeLayout(false);
            this.panel113.ResumeLayout(false);
            this.panel114.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel116.ResumeLayout(false);
            this.panel118.ResumeLayout(false);
            this.panel119.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.panel152.ResumeLayout(false);
            this.panel154.ResumeLayout(false);
            this.panel155.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.panel179.ResumeLayout(false);
            this.panel181.ResumeLayout(false);
            this.panel192.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.panel204.ResumeLayout(false);
            this.panel211.ResumeLayout(false);
            this.panel212.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.panel224.ResumeLayout(false);
            this.panel226.ResumeLayout(false);
            this.panel227.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.panel238.ResumeLayout(false);
            this.panel243.ResumeLayout(false);
            this.panel249.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            this.panel253.ResumeLayout(false);
            this.panel257.ResumeLayout(false);
            this.panel261.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            this.panel265.ResumeLayout(false);
            this.panel267.ResumeLayout(false);
            this.panel268.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            this.panel270.ResumeLayout(false);
            this.panel272.ResumeLayout(false);
            this.panel273.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            this.panel275.ResumeLayout(false);
            this.panel281.ResumeLayout(false);
            this.panel282.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            this.panel288.ResumeLayout(false);
            this.panel290.ResumeLayout(false);
            this.panel291.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            this.panel293.ResumeLayout(false);
            this.panel295.ResumeLayout(false);
            this.panel296.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            this.panel298.ResumeLayout(false);
            this.panel278.ResumeLayout(false);
            this.panel279.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            this.panel303.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox33)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelPrintArea1;
        private System.Windows.Forms.Panel panelHorSepPatDet;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panelVert1;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panelDateTimeofEventStrip;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButtonPrint;
        private System.Windows.Forms.Panel panel49;
        private System.Windows.Forms.Panel panel43;
        private System.Windows.Forms.Panel panel48;
        private System.Windows.Forms.Panel panelVert5;
        private System.Windows.Forms.Panel panel36;
        private System.Windows.Forms.Panel panel41;
        private System.Windows.Forms.Panel panelVert4;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.Panel panel34;
        private System.Windows.Forms.Panel panelVert3;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Panel panelVert2;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panelPrintHeader;
        private System.Windows.Forms.Panel panel56;
        private System.Windows.Forms.Panel panel58;
        private System.Windows.Forms.Label labelCenter2;
        private System.Windows.Forms.Panel panel59;
        private System.Windows.Forms.Label labelCenter1;
        private System.Windows.Forms.Panel panel60;
        private System.Windows.Forms.Panel panel61;
        private System.Windows.Forms.PictureBox pictureBoxCenter;
        private System.Windows.Forms.Panel panel52;
        private System.Windows.Forms.Panel panel53;
        private System.Windows.Forms.Panel panel54;
        private System.Windows.Forms.Panel panel65;
        private System.Windows.Forms.Panel panel66;
        private System.Windows.Forms.Label labelSingleEventReportDate;
        private System.Windows.Forms.Label labelSingleEventReportTime;
        private System.Windows.Forms.Panel panelPreviousTransm;
        private System.Windows.Forms.Panel panel73;
        private System.Windows.Forms.Panel panelECGSweepAmpIndicator;
        private System.Windows.Forms.Panel panelECGBaselineSTrip;
        private System.Windows.Forms.Panel panelTimeBarBaselineECG;
        private System.Windows.Forms.Panel panelTimeHeadingsPerStrip;
        private System.Windows.Forms.Panel panelBaseLineHeader;
        private System.Windows.Forms.Panel panel67;
        private System.Windows.Forms.Panel panelSecPatBar;
        private System.Windows.Forms.Panel panelEventStats;
        private System.Windows.Forms.Panel panelCurrentEventStats;
        private System.Windows.Forms.Panel panel120;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel123;
        private System.Windows.Forms.Label labelBaseRefHeader;
        private System.Windows.Forms.Panel panel132;
        private System.Windows.Forms.Panel panel131;
        private System.Windows.Forms.Panel panel130;
        private System.Windows.Forms.Panel panel129;
        private System.Windows.Forms.Panel panel128;
        private System.Windows.Forms.Panel panel127;
        private System.Windows.Forms.Panel panel126;
        private System.Windows.Forms.Panel panel125;
        private System.Windows.Forms.Panel panel124;
        private System.Windows.Forms.Panel panel137;
        private System.Windows.Forms.Panel panel136;
        private System.Windows.Forms.Panel panel135;
        private System.Windows.Forms.Panel panel134;
        private System.Windows.Forms.Panel panel133;
        private System.Windows.Forms.Panel panel69;
        private System.Windows.Forms.Label labelMinRateHeader;
        private System.Windows.Forms.Panel panel141;
        private System.Windows.Forms.Panel panel140;
        private System.Windows.Forms.Panel panel139;
        private System.Windows.Forms.Panel panel138;
        private System.Windows.Forms.Label labelMeanRateHeader;
        private System.Windows.Forms.Label labelMinRateSI;
        private System.Windows.Forms.Label labelMinRate;
        private System.Windows.Forms.Label labelPRHeader;
        private System.Windows.Forms.Label labelMaxRateHeader;
        private System.Windows.Forms.Label labelMeanRateSI;
        private System.Windows.Forms.Label labelMeanRate;
        private System.Windows.Forms.Label labelQRSHeader;
        private System.Windows.Forms.Label labelMaxRateSI;
        private System.Windows.Forms.Label labelMaxRate;
        private System.Windows.Forms.Label labelQTHeader;
        private System.Windows.Forms.Label labelPRSI;
        private System.Windows.Forms.Label labelPR;
        private System.Windows.Forms.Label labelQRSSI;
        private System.Windows.Forms.Label labelQRS;
        private System.Windows.Forms.Panel panel150;
        private System.Windows.Forms.Panel panel149;
        private System.Windows.Forms.Panel panelFullStrip;
        private System.Windows.Forms.Panel panel147;
        private System.Windows.Forms.Panel panel146;
        private System.Windows.Forms.Panel panelSample1STrip;
        private System.Windows.Forms.Panel panel144;
        private System.Windows.Forms.Panel panelLineUnderSampl1;
        private System.Windows.Forms.Panel panel142;
        private System.Windows.Forms.Panel panel151;
        private System.Windows.Forms.Label labelQT;
        private System.Windows.Forms.Panel panel158;
        private System.Windows.Forms.Panel panel157;
        private System.Windows.Forms.Panel panel70;
        private System.Windows.Forms.PictureBox pictureBoxBaselineReferenceECGStrip;
        private System.Windows.Forms.Panel panelSweepSpeed;
        private System.Windows.Forms.Panel panelAmpPanel;
        private System.Windows.Forms.Label labelBaseSweetSpeed;
        private System.Windows.Forms.Label labelBaseAmplitudeSet;
        private System.Windows.Forms.Panel panel71;
        private System.Windows.Forms.Label labelPreviousTransmissions;
        private System.Windows.Forms.Panel panel162;
        private System.Windows.Forms.Panel panel161;
        private System.Windows.Forms.Panel panel160;
        private System.Windows.Forms.Panel panel121;
        private System.Windows.Forms.Panel panel163;
        private System.Windows.Forms.Label labelMeasurement;
        private System.Windows.Forms.Panel panel164;
        private System.Windows.Forms.Panel panel165;
        private System.Windows.Forms.Label labelCurrentEvent;
        private System.Windows.Forms.Panel panel166;
        private System.Windows.Forms.Panel panel178;
        private System.Windows.Forms.Panel panel176;
        private System.Windows.Forms.Panel panel177;
        private System.Windows.Forms.Panel panel175;
        private System.Windows.Forms.Panel panel173;
        private System.Windows.Forms.Panel panel174;
        private System.Windows.Forms.Panel panel172;
        private System.Windows.Forms.Panel panel170;
        private System.Windows.Forms.Panel panel171;
        private System.Windows.Forms.Panel panel167;
        private System.Windows.Forms.Panel panel169;
        private System.Windows.Forms.Panel panel168;
        private System.Windows.Forms.Label labelActivationTypeHeader;
        private System.Windows.Forms.Label labelActivityHeader;
        private System.Windows.Forms.Label labelSymptomsHeader;
        private System.Windows.Forms.Label labelDiagnosisHeader;
        private System.Windows.Forms.Label labelICDcode;
        private System.Windows.Forms.Label labelSymptoms;
        private System.Windows.Forms.Label labelActivationType;
        private System.Windows.Forms.Label labelActivity;
        private System.Windows.Forms.Panel panel215;
        private System.Windows.Forms.Panel panel216;
        private System.Windows.Forms.Panel panel213;
        private System.Windows.Forms.Panel panel214;
        private System.Windows.Forms.Label labelTechMeasure;
        private System.Windows.Forms.Panel panel209;
        private System.Windows.Forms.Panel panel205;
        private System.Windows.Forms.Panel panel206;
        private System.Windows.Forms.Panel panel207;
        private System.Windows.Forms.Panel panel208;
        private System.Windows.Forms.Label labelQTMeasure;
        private System.Windows.Forms.Panel panel203;
        private System.Windows.Forms.Panel panel199;
        private System.Windows.Forms.Panel panel200;
        private System.Windows.Forms.Panel panel201;
        private System.Windows.Forms.Panel panel202;
        private System.Windows.Forms.Label labelQRSMeasure;
        private System.Windows.Forms.Panel panel197;
        private System.Windows.Forms.Panel panel193;
        private System.Windows.Forms.Panel panel194;
        private System.Windows.Forms.Panel panel195;
        private System.Windows.Forms.Panel panel196;
        private System.Windows.Forms.Label labelPRMeasure;
        private System.Windows.Forms.Panel panel191;
        private System.Windows.Forms.Panel panel187;
        private System.Windows.Forms.Panel panel188;
        private System.Windows.Forms.Panel panel189;
        private System.Windows.Forms.Panel panel190;
        private System.Windows.Forms.Label labelRateMeasure;
        private System.Windows.Forms.Panel panel122;
        private System.Windows.Forms.Panel panel186;
        private System.Windows.Forms.Panel panel185;
        private System.Windows.Forms.Label labelAVGMeasurement;
        private System.Windows.Forms.Panel panel184;
        private System.Windows.Forms.Label labelMaxMeasurement;
        private System.Windows.Forms.Panel panel183;
        private System.Windows.Forms.Label labelMinMeasurement;
        private System.Windows.Forms.Panel panel182;
        private System.Windows.Forms.Label labelTechnicianMeasurement;
        private System.Windows.Forms.Label labelQTmeasureSI;
        private System.Windows.Forms.Label labelQRSmeasureSI;
        private System.Windows.Forms.Label labelPRmeasureSI;
        private System.Windows.Forms.Label labelRatemeasureSI;
        private System.Windows.Forms.Label labelQTAvg;
        private System.Windows.Forms.Label labelQTMax;
        private System.Windows.Forms.Label labelQTmin;
        private System.Windows.Forms.Label labelQRSAvg;
        private System.Windows.Forms.Label labelQRSMax;
        private System.Windows.Forms.Label labelQRSmin;
        private System.Windows.Forms.Label labelPRAvg;
        private System.Windows.Forms.Label labelPRMax;
        private System.Windows.Forms.Label labelPRmin;
        private System.Windows.Forms.Label labelRateAVG;
        private System.Windows.Forms.Label labelRateMax;
        private System.Windows.Forms.Label labelRateMin;
        private System.Windows.Forms.Panel panel222;
        private System.Windows.Forms.Panel panel221;
        private System.Windows.Forms.Panel panel220;
        private System.Windows.Forms.Panel panel219;
        private System.Windows.Forms.Panel panel218;
        private System.Windows.Forms.Panel panel217;
        private System.Windows.Forms.Label labelEvent1NrSample;
        private System.Windows.Forms.Label labelSample1Nr;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label labelEvent1NrRes;
        private System.Windows.Forms.Label labelEvent1Nr;
        private System.Windows.Forms.Panel panel143;
        private System.Windows.Forms.Panel panel229;
        private System.Windows.Forms.Label labelLeadZoomStrip1;
        private System.Windows.Forms.Panel panel233;
        private System.Windows.Forms.Panel panel230;
        private System.Windows.Forms.Panel panel145;
        private System.Windows.Forms.Panel panel232;
        private System.Windows.Forms.Label labelSweepSample1;
        private System.Windows.Forms.Panel panel234;
        private System.Windows.Forms.Label labelZoomSample1;
        private System.Windows.Forms.Label labelEndSample1;
        private System.Windows.Forms.Label labelMiddleSample1;
        private System.Windows.Forms.Label labelStartSample1;
        private System.Windows.Forms.Panel panel231;
        private System.Windows.Forms.Panel panel259;
        private System.Windows.Forms.Panel panel258;
        private System.Windows.Forms.Panel panel256;
        private System.Windows.Forms.Panel panel254;
        private System.Windows.Forms.Panel panelVert9;
        private System.Windows.Forms.Panel panel252;
        private System.Windows.Forms.Panel panel250;
        private System.Windows.Forms.Panel panelVert8;
        private System.Windows.Forms.Panel panel244;
        private System.Windows.Forms.Panel panel242;
        private System.Windows.Forms.Panel panelVert7;
        private System.Windows.Forms.Panel panel240;
        private System.Windows.Forms.Panel panel248;
        private System.Windows.Forms.Label labelSerialNr;
        private System.Windows.Forms.Panel panel247;
        private System.Windows.Forms.Label labelSerialNrHeader;
        private System.Windows.Forms.Panel panel239;
        private System.Windows.Forms.Panel panelVert6;
        private System.Windows.Forms.Panel panel237;
        private System.Windows.Forms.Panel panel236;
        private System.Windows.Forms.Panel panel246;
        private System.Windows.Forms.Label labelStartDate;
        private System.Windows.Forms.Panel panel245;
        private System.Windows.Forms.Label labelStartDateHeader;
        private System.Windows.Forms.Panel panel235;
        private System.Windows.Forms.Panel panel264;
        private System.Windows.Forms.Panel panel262;
        private System.Windows.Forms.Label labelRefPhysHeader;
        private System.Windows.Forms.Panel panel260;
        private System.Windows.Forms.Label labelPhysHeader;
        private System.Windows.Forms.Label labelClientHeader;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Label labelDateOfBirth;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Label labelDOBheader;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Label labelPatientFirstName;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label labelPatientLastName;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label labelPatientNameHeader;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.Label labelStudyNr;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Label labelStudyNrHeader;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.Label labelPatID;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Label labelPatIDHeader;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.Label labelPhone2;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Label labelPhone1;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Label labelPhoneHeader;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Label labelZipCity;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Label labelAddress;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Label labelAddressHeader;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Label labelAgeGender;
        private System.Windows.Forms.Panel panel37;
        private System.Windows.Forms.PictureBox pictureBoxZoom1;
        private System.Windows.Forms.Panel panel44;
        private System.Windows.Forms.PictureBox pictureBoxStrip1;
        private System.Windows.Forms.Panel panel40;
        private System.Windows.Forms.Panel panel39;
        private System.Windows.Forms.Panel panel38;
        private System.Windows.Forms.Label labelLeadFullStripSample1;
        private System.Windows.Forms.Panel panel78;
        private System.Windows.Forms.Panel panel77;
        private System.Windows.Forms.Panel panelSample2Time;
        private System.Windows.Forms.Panel panelSeceondStrip;
        private System.Windows.Forms.Panel panelSample2Lead;
        private System.Windows.Forms.Panel panelSample2Header;
        private System.Windows.Forms.Panel panel83;
        private System.Windows.Forms.Label labelSweepFullSample1;
        private System.Windows.Forms.Panel panel82;
        private System.Windows.Forms.Label labelZoomFullSample1;
        private System.Windows.Forms.Panel panel81;
        private System.Windows.Forms.Label labelEndFullSample1;
        private System.Windows.Forms.Panel panel80;
        private System.Windows.Forms.Label labelMiddleFullSample1;
        private System.Windows.Forms.Panel panel79;
        private System.Windows.Forms.Label labelStartFullSample1;
        private System.Windows.Forms.Panel panel90;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Panel panel89;
        private System.Windows.Forms.Label labelEvent2NrSample;
        private System.Windows.Forms.Panel panel88;
        private System.Windows.Forms.Label labelSample;
        private System.Windows.Forms.Panel panel87;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Panel panel86;
        private System.Windows.Forms.Label labelEvent2NrRes;
        private System.Windows.Forms.Panel panel85;
        private System.Windows.Forms.Label labelEvenNrSample2;
        private System.Windows.Forms.Panel panel84;
        private System.Windows.Forms.Panel panel102;
        private System.Windows.Forms.Label labelMiddleSample2;
        private System.Windows.Forms.Panel panel101;
        private System.Windows.Forms.Label labelSweepSample2;
        private System.Windows.Forms.Panel panel100;
        private System.Windows.Forms.Label labelZoomSample2;
        private System.Windows.Forms.Panel panel99;
        private System.Windows.Forms.Label labelEndSample2;
        private System.Windows.Forms.Panel panel98;
        private System.Windows.Forms.Label labelStartSample2;
        private System.Windows.Forms.PictureBox pictureBoxZoom2;
        private System.Windows.Forms.Panel panel97;
        private System.Windows.Forms.Panel panel96;
        private System.Windows.Forms.Panel panel110;
        private System.Windows.Forms.Label labelReportSignDate;
        private System.Windows.Forms.Panel panel109;
        private System.Windows.Forms.Label labelDateReportSign;
        private System.Windows.Forms.Panel panel108;
        private System.Windows.Forms.Label labelPhysSignLine;
        private System.Windows.Forms.Panel panel107;
        private System.Windows.Forms.Label labelPhysSign;
        private System.Windows.Forms.Panel panel106;
        private System.Windows.Forms.Label labelPhysPrintName;
        private System.Windows.Forms.Panel panel105;
        private System.Windows.Forms.Label labelPhysNameReportPrint;
        private System.Windows.Forms.Panel panel104;
        private System.Windows.Forms.Panel panel103;
        private System.Windows.Forms.Panel panelCardiacEVentReportNumber;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel50;
        private System.Windows.Forms.Panel panel47;
        private System.Windows.Forms.Label labelCardiacEventReportNr;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label labelReportNr;
        private System.Windows.Forms.Panel panel51;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel42;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel95;
        private System.Windows.Forms.Label labelLeadSample2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripButton toolStripClipboard1;
        private System.Windows.Forms.Label labelQTSI;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Panel panelPrintArea3;
        private System.Windows.Forms.Panel panel509;
        private System.Windows.Forms.Label labelPage1of;
        private System.Windows.Forms.Panel panel508;
        private System.Windows.Forms.Panel panel507;
        private System.Windows.Forms.Panel panel510;
        private System.Windows.Forms.Label labelPage;
        private System.Windows.Forms.Label labelPageOf;
        private System.Windows.Forms.Label labelPage1ofx;
        private System.Windows.Forms.Label labelLeadID;
        private System.Windows.Forms.Panel panelPage2Footer;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ToolStripButton toolStripClipboard2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label labelPrintDate1Bottom;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ToolStripButton toolStripGenPages;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label labelPage3;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label labelPagexOf3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelPrintDate3Bottom;
        private System.Windows.Forms.Label labelPrintDate;
        private System.Windows.Forms.Label labelEventMiddle;
        private System.Windows.Forms.Label labelEventStart;
        private System.Windows.Forms.Label labelEventEnd;
        private System.Windows.Forms.Panel panelTablePrevious3Trans;
        private System.Windows.Forms.Panel panel159;
        private System.Windows.Forms.DataGridView dataGridViewPrevThreeTX;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnEventID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnEventDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnTimeOfEvent;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnSymptoms;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnFindings;
        private System.Windows.Forms.Panel panel75;
        private System.Windows.Forms.Panel panel72;
        private System.Windows.Forms.Label labelRecordNr;
        private System.Windows.Forms.Label labelRecordNrText;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.Label labelFindingsRemark;
        private System.Windows.Forms.Panel panel45;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.Label labelEvent1Time;
        private System.Windows.Forms.Label labelEvent1Date;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label labelFindingsRythm;
        private System.Windows.Forms.Label labelFindingsClasification;
        private System.Windows.Forms.Panel panel46;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel55;
        private System.Windows.Forms.Label labelMinHrText1;
        private System.Windows.Forms.Label labelMinHrValue1;
        private System.Windows.Forms.Label labelMinHrUnit1;
        private System.Windows.Forms.Label labelMeanHrText1;
        private System.Windows.Forms.Label labelMeanHrValue1;
        private System.Windows.Forms.Label labelMeanHrUnit1;
        private System.Windows.Forms.Label labelMaxHrText1;
        private System.Windows.Forms.Label labelMaxHrValue1;
        private System.Windows.Forms.Label labelMaxHrUnit1;
        private System.Windows.Forms.Label labelPrText1;
        private System.Windows.Forms.Label labelPrValue1;
        private System.Windows.Forms.Label labelPrUnit1;
        private System.Windows.Forms.Label labelQrsText1;
        private System.Windows.Forms.Label labelQrsUnit1;
        private System.Windows.Forms.Label labelQrsValue1;
        private System.Windows.Forms.Label labelQtText1;
        private System.Windows.Forms.Label labelQtUnit1;
        private System.Windows.Forms.Label labelQtValue1;
        private System.Windows.Forms.Panel panel57;
        private System.Windows.Forms.Label labelDateSampleNr2;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label labelTimeSampleNr2;
        private System.Windows.Forms.Label labelBaselineDate;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.Label labelBaselineTime;
        private System.Windows.Forms.Label labelFindingsRythmBL;
        private System.Windows.Forms.Label labelQtUnit2;
        private System.Windows.Forms.Label labelQtValue2;
        private System.Windows.Forms.Label labelQtText2;
        private System.Windows.Forms.Label labelQrsUnit2;
        private System.Windows.Forms.Label labelQrsValue2;
        private System.Windows.Forms.Label labelQrsText2;
        private System.Windows.Forms.Label labelPrUnit2;
        private System.Windows.Forms.Label labelPrValue2;
        private System.Windows.Forms.Label labelPrText2;
        private System.Windows.Forms.Label labelMaxHrUnit2;
        private System.Windows.Forms.Label labelMaxHrValue2;
        private System.Windows.Forms.Label labelMaxHrText2;
        private System.Windows.Forms.Label labelMeanHrValue2;
        private System.Windows.Forms.Label labelMeanHrText2;
        private System.Windows.Forms.Label labelMinHrUnit2;
        private System.Windows.Forms.Label labelMinHrValue2;
        private System.Windows.Forms.Label labelMinHrText2;
        private System.Windows.Forms.Label labelMeanHrUnit2;
        private System.Windows.Forms.Label labelClient;
        private System.Windows.Forms.Label labelRefPhysician;
        private System.Windows.Forms.Label labelPhysician;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panelEventSample3;
        private System.Windows.Forms.Panel panel277;
        private System.Windows.Forms.Panel panel280;
        private System.Windows.Forms.Label labelEndPageTime;
        private System.Windows.Forms.Panel panel283;
        private System.Windows.Forms.Panel panel284;
        private System.Windows.Forms.PictureBox pictureBoxSample3Full;
        private System.Windows.Forms.Panel panel285;
        private System.Windows.Forms.Panel panel286;
        private System.Windows.Forms.Panel panel300;
        private System.Windows.Forms.Panel panel384;
        private System.Windows.Forms.Panel panel387;
        private System.Windows.Forms.Panel panel388;
        private System.Windows.Forms.PictureBox pictureBoxSample3;
        private System.Windows.Forms.Panel panel389;
        private System.Windows.Forms.Panel panel390;
        private System.Windows.Forms.Panel panel391;
        private System.Windows.Forms.Panel panel416;
        private System.Windows.Forms.Panel panel417;
        private System.Windows.Forms.Panel panel418;
        private System.Windows.Forms.Panel panel316;
        private System.Windows.Forms.Panel panel315;
        private System.Windows.Forms.Panel panel448;
        private System.Windows.Forms.Panel panel449;
        private System.Windows.Forms.Panel panel450;
        private System.Windows.Forms.Panel panelPage2Header;
        private System.Windows.Forms.Label labelPage3ReportNr;
        private System.Windows.Forms.Label labelReportText3;
        private System.Windows.Forms.Panel panel74;
        private System.Windows.Forms.Panel panel76;
        private System.Windows.Forms.Panel panel495;
        private System.Windows.Forms.Panel panel534;
        private System.Windows.Forms.Label labelStudyNumberResPage3;
        private System.Windows.Forms.Panel panel533;
        private System.Windows.Forms.Label labelStudyNrPage3;
        private System.Windows.Forms.Panel panel532;
        private System.Windows.Forms.Panel panel531;
        private System.Windows.Forms.Panel panel530;
        private System.Windows.Forms.Panel panel529;
        private System.Windows.Forms.Label labelDateOfBirthResPage3;
        private System.Windows.Forms.Panel panel528;
        private System.Windows.Forms.Label labelDateOfBirthPage3;
        private System.Windows.Forms.Panel panel527;
        private System.Windows.Forms.Panel panel526;
        private System.Windows.Forms.Panel panel525;
        private System.Windows.Forms.Panel panel524;
        private System.Windows.Forms.Label labelPatLastNam3;
        private System.Windows.Forms.Panel panel523;
        private System.Windows.Forms.Label labelPatName3;
        private System.Windows.Forms.Panel panel522;
        private System.Windows.Forms.Panel panel521;
        private System.Windows.Forms.Panel panel520;
        private System.Windows.Forms.Panel panel519;
        private System.Windows.Forms.Label labelPatientIDResult3;
        private System.Windows.Forms.Panel panel497;
        private System.Windows.Forms.Label labelPatientID3;
        private System.Windows.Forms.Panel panel499;
        private System.Windows.Forms.Panel panel500;
        private System.Windows.Forms.Panel panel501;
        private System.Windows.Forms.Panel panel506;
        private System.Windows.Forms.PictureBox pictureBoxCenter2;
        private System.Windows.Forms.Panel panel278;
        private System.Windows.Forms.Panel panel279;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.Panel panel302;
        private System.Windows.Forms.Panel panel303;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.Panel panel299;
        private System.Windows.Forms.Panel panel295;
        private System.Windows.Forms.Panel panel296;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.Panel panel297;
        private System.Windows.Forms.Panel panel298;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.Panel panel294;
        private System.Windows.Forms.Panel panel290;
        private System.Windows.Forms.Panel panel291;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.Panel panel292;
        private System.Windows.Forms.Panel panel293;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Panel panel289;
        private System.Windows.Forms.Panel panel281;
        private System.Windows.Forms.Panel panel282;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.Panel panel287;
        private System.Windows.Forms.Panel panel288;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Panel panel276;
        private System.Windows.Forms.Panel panel272;
        private System.Windows.Forms.Panel panel273;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.Panel panel274;
        private System.Windows.Forms.Panel panel275;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Panel panel271;
        private System.Windows.Forms.Panel panel267;
        private System.Windows.Forms.Panel panel268;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.Panel panel269;
        private System.Windows.Forms.Panel panel270;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Panel panel266;
        private System.Windows.Forms.Panel panel257;
        private System.Windows.Forms.Panel panel261;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.Panel panel263;
        private System.Windows.Forms.Panel panel265;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Panel panel255;
        private System.Windows.Forms.Panel panel243;
        private System.Windows.Forms.Panel panel249;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Panel panel251;
        private System.Windows.Forms.Panel panel253;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Panel panel241;
        private System.Windows.Forms.Panel panel226;
        private System.Windows.Forms.Panel panel227;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Panel panel228;
        private System.Windows.Forms.Panel panel238;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Panel panel225;
        private System.Windows.Forms.Panel panel211;
        private System.Windows.Forms.Panel panel212;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Panel panel223;
        private System.Windows.Forms.Panel panel224;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Panel panel210;
        private System.Windows.Forms.Panel panel181;
        private System.Windows.Forms.Panel panel192;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Panel panel198;
        private System.Windows.Forms.Panel panel204;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Panel panel180;
        private System.Windows.Forms.Panel panel154;
        private System.Windows.Forms.Panel panel155;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Panel panel156;
        private System.Windows.Forms.Panel panel179;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Panel panel153;
        private System.Windows.Forms.Panel panel118;
        private System.Windows.Forms.Panel panel119;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Panel panel148;
        private System.Windows.Forms.Panel panel152;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Panel panel117;
        private System.Windows.Forms.Panel panel113;
        private System.Windows.Forms.Panel panel114;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Panel panel115;
        private System.Windows.Forms.Panel panel116;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Panel panel112;
        private System.Windows.Forms.Panel panel92;
        private System.Windows.Forms.Panel panel93;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel panel94;
        private System.Windows.Forms.Panel panel111;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Panel panel91;
        private System.Windows.Forms.Panel panel62;
        private System.Windows.Forms.Panel panel63;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel64;
        private System.Windows.Forms.Panel panel68;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Panel panel35;
        private System.Windows.Forms.Label labelSample3SweepFull;
        private System.Windows.Forms.Label labelSample3AmplFull;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label labelSample3End;
        private System.Windows.Forms.Label labelSample3StartFull;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label labelSample3Lead;
        private System.Windows.Forms.Label labelStartPageTime;
        private System.Windows.Forms.Label labelStartPageDate;
        private System.Windows.Forms.Panel panel301;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox pictureBox33;
        private System.Windows.Forms.PictureBox pictureBox32;
        private System.Windows.Forms.PictureBox pictureBox31;
        private System.Windows.Forms.PictureBox pictureBox30;
        private System.Windows.Forms.PictureBox pictureBox29;
        private System.Windows.Forms.PictureBox pictureBox28;
        private System.Windows.Forms.PictureBox pictureBox27;
        private System.Windows.Forms.PictureBox pictureBox26;
        private System.Windows.Forms.PictureBox pictureBox25;
        private System.Windows.Forms.PictureBox pictureBox24;
        private System.Windows.Forms.PictureBox pictureBox23;
        private System.Windows.Forms.PictureBox pictureBox22;
        private System.Windows.Forms.PictureBox pictureBox21;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.PictureBox pictureBox17;
    }
}