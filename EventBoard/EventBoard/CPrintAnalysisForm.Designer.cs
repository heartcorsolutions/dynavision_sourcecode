﻿namespace EventBoard
{
    partial class CPrintAnalysisForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CPrintAnalysisForm));
            this.panelPrintArea1 = new System.Windows.Forms.Panel();
            this.panelEventSample2 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel114 = new System.Windows.Forms.Panel();
            this.labelFindings2 = new System.Windows.Forms.Label();
            this.panel115 = new System.Windows.Forms.Panel();
            this.labelRhythms2 = new System.Windows.Forms.Label();
            this.labelFindingsText2 = new System.Windows.Forms.Label();
            this.panel116 = new System.Windows.Forms.Panel();
            this.panel117 = new System.Windows.Forms.Panel();
            this.panel119 = new System.Windows.Forms.Panel();
            this.panel128 = new System.Windows.Forms.Panel();
            this.unitQt2 = new System.Windows.Forms.Label();
            this.unitQrs2 = new System.Windows.Forms.Label();
            this.panel127 = new System.Windows.Forms.Panel();
            this.valueQt2 = new System.Windows.Forms.Label();
            this.valueQrs2 = new System.Windows.Forms.Label();
            this.panel113 = new System.Windows.Forms.Panel();
            this.headerQt2 = new System.Windows.Forms.Label();
            this.headerQrs2 = new System.Windows.Forms.Label();
            this.panel121 = new System.Windows.Forms.Panel();
            this.unitPr2 = new System.Windows.Forms.Label();
            this.unitMaxHr2 = new System.Windows.Forms.Label();
            this.panel122 = new System.Windows.Forms.Panel();
            this.valuePr2 = new System.Windows.Forms.Label();
            this.valueMaxHr2 = new System.Windows.Forms.Label();
            this.panel123 = new System.Windows.Forms.Panel();
            this.headerPr2 = new System.Windows.Forms.Label();
            this.headerMaxHr2 = new System.Windows.Forms.Label();
            this.panel124 = new System.Windows.Forms.Panel();
            this.unitMeanHr2 = new System.Windows.Forms.Label();
            this.unitMinHr2 = new System.Windows.Forms.Label();
            this.panel125 = new System.Windows.Forms.Panel();
            this.valueMeanHr2 = new System.Windows.Forms.Label();
            this.valueMinHr2 = new System.Windows.Forms.Label();
            this.panel126 = new System.Windows.Forms.Panel();
            this.headerMeanHr2 = new System.Windows.Forms.Label();
            this.headerMinHr2 = new System.Windows.Forms.Label();
            this.label146 = new System.Windows.Forms.Label();
            this.panelSample2Time = new System.Windows.Forms.Panel();
            this.labelZoomSample2 = new System.Windows.Forms.Label();
            this.labelSweepSample2 = new System.Windows.Forms.Label();
            this.labelSample2Mid = new System.Windows.Forms.Label();
            this.labelSample2Start = new System.Windows.Forms.Label();
            this.labelSample2End = new System.Windows.Forms.Label();
            this.panelSeceondStrip = new System.Windows.Forms.Panel();
            this.labelLeadSample2 = new System.Windows.Forms.Label();
            this.pictureBoxSample2 = new System.Windows.Forms.PictureBox();
            this.panelSample2Header = new System.Windows.Forms.Panel();
            this.labelClass2 = new System.Windows.Forms.Label();
            this.labelSample2Date = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.labelSample2Time = new System.Windows.Forms.Label();
            this.panel89 = new System.Windows.Forms.Panel();
            this.panel86 = new System.Windows.Forms.Panel();
            this.labelSample2Nr = new System.Windows.Forms.Label();
            this.labelEvenNrSample2 = new System.Windows.Forms.Label();
            this.labelSample2EventNr = new System.Windows.Forms.Label();
            this.panelEventSample1 = new System.Windows.Forms.Panel();
            this.panel97 = new System.Windows.Forms.Panel();
            this.panel50 = new System.Windows.Forms.Panel();
            this.panelActivities3 = new System.Windows.Forms.Panel();
            this.labelActivity1 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.panel55 = new System.Windows.Forms.Panel();
            this.labelSymptoms1 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.panel57 = new System.Windows.Forms.Panel();
            this.labelFindings1 = new System.Windows.Forms.Label();
            this.panel118 = new System.Windows.Forms.Panel();
            this.labelRhythms1 = new System.Windows.Forms.Label();
            this.labelFindingsText1 = new System.Windows.Forms.Label();
            this.panel62 = new System.Windows.Forms.Panel();
            this.panel63 = new System.Windows.Forms.Panel();
            this.panel64 = new System.Windows.Forms.Panel();
            this.panel68 = new System.Windows.Forms.Panel();
            this.unitQt1 = new System.Windows.Forms.Label();
            this.unitQrs1 = new System.Windows.Forms.Label();
            this.unitPr1 = new System.Windows.Forms.Label();
            this.panel69 = new System.Windows.Forms.Panel();
            this.valueQt1 = new System.Windows.Forms.Label();
            this.valueQrs1 = new System.Windows.Forms.Label();
            this.valuePr1 = new System.Windows.Forms.Label();
            this.panel70 = new System.Windows.Forms.Panel();
            this.headerQt1 = new System.Windows.Forms.Label();
            this.headerQrs1 = new System.Windows.Forms.Label();
            this.headerPr1 = new System.Windows.Forms.Label();
            this.panel72 = new System.Windows.Forms.Panel();
            this.unitMaxHr1 = new System.Windows.Forms.Label();
            this.unitMeanHr1 = new System.Windows.Forms.Label();
            this.unitMinHr1 = new System.Windows.Forms.Label();
            this.panel73 = new System.Windows.Forms.Panel();
            this.valueMaxHr1 = new System.Windows.Forms.Label();
            this.valueMeanHr1 = new System.Windows.Forms.Label();
            this.valueMinHr1 = new System.Windows.Forms.Label();
            this.panel75 = new System.Windows.Forms.Panel();
            this.headerMaxHr1 = new System.Windows.Forms.Label();
            this.headerMeanHr1 = new System.Windows.Forms.Label();
            this.headerMinHr1 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.panel149 = new System.Windows.Forms.Panel();
            this.labelSample1AmplFull = new System.Windows.Forms.Label();
            this.labelSample1SweepFull = new System.Windows.Forms.Label();
            this.labelSample1MidFull = new System.Windows.Forms.Label();
            this.labelSample1StartFull = new System.Windows.Forms.Label();
            this.labelSample1EndFull = new System.Windows.Forms.Label();
            this.panelFullStrip = new System.Windows.Forms.Panel();
            this.panel44 = new System.Windows.Forms.Panel();
            this.labelLeadFullStripSample1 = new System.Windows.Forms.Label();
            this.pictureBoxSample1Full = new System.Windows.Forms.PictureBox();
            this.panel146 = new System.Windows.Forms.Panel();
            this.panel234 = new System.Windows.Forms.Panel();
            this.labelSample1Ampl = new System.Windows.Forms.Label();
            this.panel232 = new System.Windows.Forms.Panel();
            this.labelSample1Sweep = new System.Windows.Forms.Label();
            this.panel233 = new System.Windows.Forms.Panel();
            this.labelSample1End = new System.Windows.Forms.Label();
            this.panel230 = new System.Windows.Forms.Panel();
            this.labelSample1Mid = new System.Windows.Forms.Label();
            this.labelSample1Start = new System.Windows.Forms.Label();
            this.panelSample1STrip = new System.Windows.Forms.Panel();
            this.panel37 = new System.Windows.Forms.Panel();
            this.labelLeadZoomStrip1 = new System.Windows.Forms.Label();
            this.pictureBoxSample1 = new System.Windows.Forms.PictureBox();
            this.panel142 = new System.Windows.Forms.Panel();
            this.labelSample1Date = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.labelSample1Time = new System.Windows.Forms.Label();
            this.panel222 = new System.Windows.Forms.Panel();
            this.panel219 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.labelClass1 = new System.Windows.Forms.Label();
            this.labelSample1Nr = new System.Windows.Forms.Label();
            this.labelSample1EventNr = new System.Windows.Forms.Label();
            this.panel48 = new System.Windows.Forms.Panel();
            this.panelBaseLineMFS = new System.Windows.Forms.Panel();
            this.panelSymptoms3 = new System.Windows.Forms.Panel();
            this.labelSymptomsBL = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.panelfindings3 = new System.Windows.Forms.Panel();
            this.labelFindingsBL = new System.Windows.Forms.Label();
            this.panel176 = new System.Windows.Forms.Panel();
            this.labelRhythmsBL = new System.Windows.Forms.Label();
            this.labelFindingsTextBL = new System.Windows.Forms.Label();
            this.panel270 = new System.Windows.Forms.Panel();
            this.panel273 = new System.Windows.Forms.Panel();
            this.panelQRSmeasurements3 = new System.Windows.Forms.Panel();
            this.panel47 = new System.Windows.Forms.Panel();
            this.unitQtBL = new System.Windows.Forms.Label();
            this.unitQrsBL = new System.Windows.Forms.Label();
            this.panel43 = new System.Windows.Forms.Panel();
            this.valueQtBL = new System.Windows.Forms.Label();
            this.valueQrsBL = new System.Windows.Forms.Label();
            this.panel42 = new System.Windows.Forms.Panel();
            this.headerQtBL = new System.Windows.Forms.Label();
            this.headerQrsBL = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.unitPrBL = new System.Windows.Forms.Label();
            this.unitMaxHrBL = new System.Windows.Forms.Label();
            this.panel20 = new System.Windows.Forms.Panel();
            this.valuePrBL = new System.Windows.Forms.Label();
            this.valueMaxHrBL = new System.Windows.Forms.Label();
            this.panel38 = new System.Windows.Forms.Panel();
            this.headerPrBL = new System.Windows.Forms.Label();
            this.headerMaxHrBL = new System.Windows.Forms.Label();
            this.panel39 = new System.Windows.Forms.Panel();
            this.unitMeanHrBL = new System.Windows.Forms.Label();
            this.unitMinHrBL = new System.Windows.Forms.Label();
            this.panel40 = new System.Windows.Forms.Panel();
            this.valueMeanHrBL = new System.Windows.Forms.Label();
            this.valueMinHrBL = new System.Windows.Forms.Label();
            this.panel41 = new System.Windows.Forms.Panel();
            this.headerMeanHrBL = new System.Windows.Forms.Label();
            this.headerMinHrBL = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.panelBlSweepAmpIndicator = new System.Windows.Forms.Panel();
            this.panelSweepSpeed = new System.Windows.Forms.Panel();
            this.labelSampleBlSweep = new System.Windows.Forms.Label();
            this.labelSampleBlMid = new System.Windows.Forms.Label();
            this.labelSampleBlStart = new System.Windows.Forms.Label();
            this.panelAmpPanel = new System.Windows.Forms.Panel();
            this.labelSampleBlAmpl = new System.Windows.Forms.Label();
            this.labelSampleBlEnd = new System.Windows.Forms.Label();
            this.panelECGBaselineSTrip = new System.Windows.Forms.Panel();
            this.pictureBoxBaseline = new System.Windows.Forms.PictureBox();
            this.labelLeadID = new System.Windows.Forms.Label();
            this.panelBaseLineHeader = new System.Windows.Forms.Panel();
            this.labelBaselineDate = new System.Windows.Forms.Label();
            this.label134 = new System.Windows.Forms.Label();
            this.labelBaselineTime = new System.Windows.Forms.Label();
            this.labelBaseRefHeader = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.labelEventNrBL = new System.Windows.Forms.Label();
            this.panel35 = new System.Windows.Forms.Panel();
            this.panelEventStats = new System.Windows.Forms.Panel();
            this.panel31 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel111 = new System.Windows.Forms.Panel();
            this.labelQTmeasureSI = new System.Windows.Forms.Label();
            this.labelQRSmeasureSI = new System.Windows.Forms.Label();
            this.labelPRmeasureSI = new System.Windows.Forms.Label();
            this.labelRatemeasureSI = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.panel96 = new System.Windows.Forms.Panel();
            this.labelQTAvg = new System.Windows.Forms.Label();
            this.labelQRSAvg = new System.Windows.Forms.Label();
            this.labelPRAvg = new System.Windows.Forms.Label();
            this.labelRateAVG = new System.Windows.Forms.Label();
            this.labelAVGMeasurement = new System.Windows.Forms.Label();
            this.panel84 = new System.Windows.Forms.Panel();
            this.labelQTMax = new System.Windows.Forms.Label();
            this.labelQRSMax = new System.Windows.Forms.Label();
            this.labelPRMax = new System.Windows.Forms.Label();
            this.labelRateMax = new System.Windows.Forms.Label();
            this.labelMaxMeasurement = new System.Windows.Forms.Label();
            this.panel46 = new System.Windows.Forms.Panel();
            this.labelQTmin = new System.Windows.Forms.Label();
            this.labelQRSmin = new System.Windows.Forms.Label();
            this.labelPRmin = new System.Windows.Forms.Label();
            this.labelRateMin = new System.Windows.Forms.Label();
            this.labelMinMeasurement = new System.Windows.Forms.Label();
            this.panel45 = new System.Windows.Forms.Panel();
            this.labelQTMeasure = new System.Windows.Forms.Label();
            this.labelQRSMeasure = new System.Windows.Forms.Label();
            this.labelPRMeasure = new System.Windows.Forms.Label();
            this.labelRateMeasure = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.panel28 = new System.Windows.Forms.Panel();
            this.panel30 = new System.Windows.Forms.Panel();
            this.labelICDcode = new System.Windows.Forms.Label();
            this.labelDiagnosisHeader = new System.Windows.Forms.Label();
            this.panel78 = new System.Windows.Forms.Panel();
            this.panel110 = new System.Windows.Forms.Panel();
            this.labelReportSignDate = new System.Windows.Forms.Label();
            this.panel109 = new System.Windows.Forms.Panel();
            this.labelDateReportSign = new System.Windows.Forms.Label();
            this.panel108 = new System.Windows.Forms.Panel();
            this.labelPhysSignLine = new System.Windows.Forms.Label();
            this.panel107 = new System.Windows.Forms.Panel();
            this.labelPhysSign = new System.Windows.Forms.Label();
            this.panel106 = new System.Windows.Forms.Panel();
            this.labelPhysPrintName = new System.Windows.Forms.Label();
            this.panel105 = new System.Windows.Forms.Panel();
            this.labelPhysNameReportPrint = new System.Windows.Forms.Label();
            this.panel77 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panelHorSepPatDet = new System.Windows.Forms.Panel();
            this.panel51 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.labelStudyNr = new System.Windows.Forms.Label();
            this.labelPrintDate1Bottom = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panel510 = new System.Windows.Forms.Panel();
            this.labelPage = new System.Windows.Forms.Label();
            this.panel509 = new System.Windows.Forms.Panel();
            this.labelPage1 = new System.Windows.Forms.Label();
            this.panel508 = new System.Windows.Forms.Panel();
            this.labelPageOf = new System.Windows.Forms.Label();
            this.panel507 = new System.Windows.Forms.Panel();
            this.labelPage1OfX = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panelPhysInfo = new System.Windows.Forms.Panel();
            this.panel67 = new System.Windows.Forms.Panel();
            this.panel258 = new System.Windows.Forms.Panel();
            this.labelClientName = new System.Windows.Forms.Label();
            this.labelClientTel = new System.Windows.Forms.Label();
            this.panel256 = new System.Windows.Forms.Panel();
            this.label46 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.labelClientHeader = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel29 = new System.Windows.Forms.Panel();
            this.panelVert9 = new System.Windows.Forms.Panel();
            this.panel252 = new System.Windows.Forms.Panel();
            this.labelRefPhysicianName = new System.Windows.Forms.Label();
            this.labelRefPhysicianTel = new System.Windows.Forms.Label();
            this.panel250 = new System.Windows.Forms.Panel();
            this.label43 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.labelRefPhysHeader = new System.Windows.Forms.Label();
            this.panelVert8 = new System.Windows.Forms.Panel();
            this.panel244 = new System.Windows.Forms.Panel();
            this.labelPhysicianName = new System.Windows.Forms.Label();
            this.labelPhysicianTel = new System.Windows.Forms.Label();
            this.panel242 = new System.Windows.Forms.Panel();
            this.label41 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.labelPhysHeader = new System.Windows.Forms.Label();
            this.panelVert6 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel25 = new System.Windows.Forms.Panel();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panelSecPatBar = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.labelQC = new System.Windows.Forms.Label();
            this.labelTechnicianMeasurement = new System.Windows.Forms.Label();
            this.labelRoom = new System.Windows.Forms.Label();
            this.labelPatID = new System.Windows.Forms.Label();
            this.labelSerialNr = new System.Windows.Forms.Label();
            this.labelStartDate = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.labelTechText = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.labelPatIDHeader = new System.Windows.Forms.Label();
            this.labelSerialNrHeader = new System.Windows.Forms.Label();
            this.labelStartDateHeader = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel24 = new System.Windows.Forms.Panel();
            this.panel34 = new System.Windows.Forms.Panel();
            this.labelCountry = new System.Windows.Forms.Label();
            this.labelZipCity = new System.Windows.Forms.Label();
            this.labelAddress = new System.Windows.Forms.Label();
            this.labelPhone1 = new System.Windows.Forms.Label();
            this.labelPhone2 = new System.Windows.Forms.Label();
            this.panel17 = new System.Windows.Forms.Panel();
            this.label33 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.labelAddressHeader = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel27 = new System.Windows.Forms.Panel();
            this.labelPatientFirstName = new System.Windows.Forms.Label();
            this.labelDateOfBirth = new System.Windows.Forms.Label();
            this.labelAgeGender = new System.Windows.Forms.Label();
            this.labelPatientLastName = new System.Windows.Forms.Label();
            this.panel26 = new System.Windows.Forms.Panel();
            this.label23 = new System.Windows.Forms.Label();
            this.labelDOBheader = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.panel33 = new System.Windows.Forms.Panel();
            this.panel22 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panelDateTimeofEventStrip = new System.Windows.Forms.Panel();
            this.labelActivationType = new System.Windows.Forms.Label();
            this.labelSingleEventReportTime = new System.Windows.Forms.Label();
            this.labelSingleEventReportDate = new System.Windows.Forms.Label();
            this.panel52 = new System.Windows.Forms.Panel();
            this.panelCardiacEVentReportNumber = new System.Windows.Forms.Panel();
            this.labelReportNr = new System.Windows.Forms.Label();
            this.labelCardiacEventReportNr = new System.Windows.Forms.Label();
            this.panelPrintHeader = new System.Windows.Forms.Panel();
            this.pictureBoxCenter = new System.Windows.Forms.PictureBox();
            this.panel56 = new System.Windows.Forms.Panel();
            this.panel58 = new System.Windows.Forms.Panel();
            this.labelCenter2 = new System.Windows.Forms.Label();
            this.panel59 = new System.Windows.Forms.Panel();
            this.labelCenter1 = new System.Windows.Forms.Label();
            this.panel60 = new System.Windows.Forms.Panel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripGenPages = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonPrint = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripMemUsage = new System.Windows.Forms.ToolStripLabel();
            this.toolStripClipboard1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripClipboard2 = new System.Windows.Forms.ToolStripButton();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel21 = new System.Windows.Forms.Panel();
            this.panelPrintArea2 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panelEventSample5 = new System.Windows.Forms.Panel();
            this.panel160 = new System.Windows.Forms.Panel();
            this.panel163 = new System.Windows.Forms.Panel();
            this.labelFindings5 = new System.Windows.Forms.Label();
            this.panel164 = new System.Windows.Forms.Panel();
            this.labelRhythms5 = new System.Windows.Forms.Label();
            this.labelFindingsText5 = new System.Windows.Forms.Label();
            this.panel166 = new System.Windows.Forms.Panel();
            this.panel167 = new System.Windows.Forms.Panel();
            this.panel169 = new System.Windows.Forms.Panel();
            this.panel170 = new System.Windows.Forms.Panel();
            this.unitQt5 = new System.Windows.Forms.Label();
            this.unitQrs5 = new System.Windows.Forms.Label();
            this.unitPr5 = new System.Windows.Forms.Label();
            this.panel171 = new System.Windows.Forms.Panel();
            this.valueQt5 = new System.Windows.Forms.Label();
            this.valueQrs5 = new System.Windows.Forms.Label();
            this.valuePr5 = new System.Windows.Forms.Label();
            this.panel172 = new System.Windows.Forms.Panel();
            this.headerQt5 = new System.Windows.Forms.Label();
            this.headerQrs5 = new System.Windows.Forms.Label();
            this.headerPr5 = new System.Windows.Forms.Label();
            this.panel173 = new System.Windows.Forms.Panel();
            this.unitMaxHr5 = new System.Windows.Forms.Label();
            this.unitMeanHr5 = new System.Windows.Forms.Label();
            this.unitMinHr5 = new System.Windows.Forms.Label();
            this.panel174 = new System.Windows.Forms.Panel();
            this.valueMaxHr5 = new System.Windows.Forms.Label();
            this.valueMeanHr5 = new System.Windows.Forms.Label();
            this.valueMinHr5 = new System.Windows.Forms.Label();
            this.panel175 = new System.Windows.Forms.Panel();
            this.headerMaxHr5 = new System.Windows.Forms.Label();
            this.headerMeanHr5 = new System.Windows.Forms.Label();
            this.headerMinHr5 = new System.Windows.Forms.Label();
            this.label213 = new System.Windows.Forms.Label();
            this.panel153 = new System.Windows.Forms.Panel();
            this.labelSample5StartFull = new System.Windows.Forms.Label();
            this.labelSample5MidFull = new System.Windows.Forms.Label();
            this.panel238 = new System.Windows.Forms.Panel();
            this.labelSample5SweepFull = new System.Windows.Forms.Label();
            this.panel241 = new System.Windows.Forms.Panel();
            this.labelSample5AmplFull = new System.Windows.Forms.Label();
            this.panel243 = new System.Windows.Forms.Panel();
            this.labelSample5EndFull = new System.Windows.Forms.Label();
            this.panel290 = new System.Windows.Forms.Panel();
            this.panel291 = new System.Windows.Forms.Panel();
            this.labelSample5LeadFull = new System.Windows.Forms.Label();
            this.pictureBoxSample5Full = new System.Windows.Forms.PictureBox();
            this.panel298 = new System.Windows.Forms.Panel();
            this.panel378 = new System.Windows.Forms.Panel();
            this.labelSample5Start = new System.Windows.Forms.Label();
            this.labelSample5Mid = new System.Windows.Forms.Label();
            this.panel383 = new System.Windows.Forms.Panel();
            this.labelSample5Sweep = new System.Windows.Forms.Label();
            this.panel419 = new System.Windows.Forms.Panel();
            this.labelSample5Ampl = new System.Windows.Forms.Label();
            this.panel420 = new System.Windows.Forms.Panel();
            this.labelSample5End = new System.Windows.Forms.Label();
            this.panel421 = new System.Windows.Forms.Panel();
            this.panel422 = new System.Windows.Forms.Panel();
            this.labelSample5Lead = new System.Windows.Forms.Label();
            this.pictureBoxSample5 = new System.Windows.Forms.PictureBox();
            this.panel453 = new System.Windows.Forms.Panel();
            this.labelSample5Date = new System.Windows.Forms.Label();
            this.label119 = new System.Windows.Forms.Label();
            this.labelSample5Time = new System.Windows.Forms.Label();
            this.labelEvent5 = new System.Windows.Forms.Label();
            this.labelClass5 = new System.Windows.Forms.Label();
            this.labelSample5Nr = new System.Windows.Forms.Label();
            this.labelSample5EventNr = new System.Windows.Forms.Label();
            this.panel468 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panelEventSample4 = new System.Windows.Forms.Panel();
            this.panel136 = new System.Windows.Forms.Panel();
            this.panel139 = new System.Windows.Forms.Panel();
            this.labelFindings4 = new System.Windows.Forms.Label();
            this.panel140 = new System.Windows.Forms.Panel();
            this.labelRhythms4 = new System.Windows.Forms.Label();
            this.labelFindingsText4 = new System.Windows.Forms.Label();
            this.panel141 = new System.Windows.Forms.Panel();
            this.panel143 = new System.Windows.Forms.Panel();
            this.panel144 = new System.Windows.Forms.Panel();
            this.panel147 = new System.Windows.Forms.Panel();
            this.unitQt4 = new System.Windows.Forms.Label();
            this.unitQrs4 = new System.Windows.Forms.Label();
            this.unitPr4 = new System.Windows.Forms.Label();
            this.panel148 = new System.Windows.Forms.Panel();
            this.valueQt4 = new System.Windows.Forms.Label();
            this.valueQrs4 = new System.Windows.Forms.Label();
            this.valuePr4 = new System.Windows.Forms.Label();
            this.panel150 = new System.Windows.Forms.Panel();
            this.headerQt4 = new System.Windows.Forms.Label();
            this.headerQrs4 = new System.Windows.Forms.Label();
            this.headerPr4 = new System.Windows.Forms.Label();
            this.panel151 = new System.Windows.Forms.Panel();
            this.unitMaxHr4 = new System.Windows.Forms.Label();
            this.unitMeanHr4 = new System.Windows.Forms.Label();
            this.unitMinHr4 = new System.Windows.Forms.Label();
            this.panel152 = new System.Windows.Forms.Panel();
            this.valueMaxHr4 = new System.Windows.Forms.Label();
            this.valueMeanHr4 = new System.Windows.Forms.Label();
            this.valueMinHr4 = new System.Windows.Forms.Label();
            this.panel157 = new System.Windows.Forms.Panel();
            this.headerMaxHr4 = new System.Windows.Forms.Label();
            this.headerMeanHr4 = new System.Windows.Forms.Label();
            this.headerMinHr4 = new System.Windows.Forms.Label();
            this.label187 = new System.Windows.Forms.Label();
            this.panel266 = new System.Windows.Forms.Panel();
            this.labelSample4StartFull = new System.Windows.Forms.Label();
            this.labelSample4MidFull = new System.Windows.Forms.Label();
            this.panel267 = new System.Windows.Forms.Panel();
            this.labelSample4SweepFull = new System.Windows.Forms.Label();
            this.panel268 = new System.Windows.Forms.Panel();
            this.labelSample4AmplFull = new System.Windows.Forms.Label();
            this.panel269 = new System.Windows.Forms.Panel();
            this.labelSample4EndFull = new System.Windows.Forms.Label();
            this.panel275 = new System.Windows.Forms.Panel();
            this.panel297 = new System.Windows.Forms.Panel();
            this.labelSample4LeadFull = new System.Windows.Forms.Label();
            this.pictureBoxSample4Full = new System.Windows.Forms.PictureBox();
            this.panel305 = new System.Windows.Forms.Panel();
            this.panel306 = new System.Windows.Forms.Panel();
            this.labelSample4Start = new System.Windows.Forms.Label();
            this.labelSample4Mid = new System.Windows.Forms.Label();
            this.panel311 = new System.Windows.Forms.Panel();
            this.labelSample4Sweep = new System.Windows.Forms.Label();
            this.panel312 = new System.Windows.Forms.Panel();
            this.labelSample4Ampl = new System.Windows.Forms.Label();
            this.panel313 = new System.Windows.Forms.Panel();
            this.labelSample4End = new System.Windows.Forms.Label();
            this.panel328 = new System.Windows.Forms.Panel();
            this.panel329 = new System.Windows.Forms.Panel();
            this.labelSample4Lead = new System.Windows.Forms.Label();
            this.pictureBoxSample4 = new System.Windows.Forms.PictureBox();
            this.panel357 = new System.Windows.Forms.Panel();
            this.labelSample4Date = new System.Windows.Forms.Label();
            this.label131 = new System.Windows.Forms.Label();
            this.labelSample4Time = new System.Windows.Forms.Label();
            this.labelEvent4 = new System.Windows.Forms.Label();
            this.labelClass4 = new System.Windows.Forms.Label();
            this.labelSample4Nr = new System.Windows.Forms.Label();
            this.labelSample4EventNr = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panelEventSample3 = new System.Windows.Forms.Panel();
            this.panel32 = new System.Windows.Forms.Panel();
            this.panel103 = new System.Windows.Forms.Panel();
            this.labelFindings3 = new System.Windows.Forms.Label();
            this.panel104 = new System.Windows.Forms.Panel();
            this.labelRhythms3 = new System.Windows.Forms.Label();
            this.labelFindingsText3 = new System.Windows.Forms.Label();
            this.panel112 = new System.Windows.Forms.Panel();
            this.panel120 = new System.Windows.Forms.Panel();
            this.panel129 = new System.Windows.Forms.Panel();
            this.panel130 = new System.Windows.Forms.Panel();
            this.unitQt3 = new System.Windows.Forms.Label();
            this.unitQrs3 = new System.Windows.Forms.Label();
            this.unitPr3 = new System.Windows.Forms.Label();
            this.panel131 = new System.Windows.Forms.Panel();
            this.valueQt3 = new System.Windows.Forms.Label();
            this.valueQrs3 = new System.Windows.Forms.Label();
            this.valuePr3 = new System.Windows.Forms.Label();
            this.panel132 = new System.Windows.Forms.Panel();
            this.headerQt3 = new System.Windows.Forms.Label();
            this.headerQrs3 = new System.Windows.Forms.Label();
            this.headerPr3 = new System.Windows.Forms.Label();
            this.panel133 = new System.Windows.Forms.Panel();
            this.unitMaxHr3 = new System.Windows.Forms.Label();
            this.unitMeanHr3 = new System.Windows.Forms.Label();
            this.unitMinHr3 = new System.Windows.Forms.Label();
            this.panel134 = new System.Windows.Forms.Panel();
            this.valueMaxHr3 = new System.Windows.Forms.Label();
            this.valueMeanHr3 = new System.Windows.Forms.Label();
            this.valueMinHr3 = new System.Windows.Forms.Label();
            this.panel135 = new System.Windows.Forms.Panel();
            this.headerMaxHr3 = new System.Windows.Forms.Label();
            this.headerMeanHr3 = new System.Windows.Forms.Label();
            this.headerMinHr3 = new System.Windows.Forms.Label();
            this.label161 = new System.Windows.Forms.Label();
            this.panel277 = new System.Windows.Forms.Panel();
            this.labelSample3StartFull = new System.Windows.Forms.Label();
            this.labelSample3MidFull = new System.Windows.Forms.Label();
            this.panel278 = new System.Windows.Forms.Panel();
            this.labelSample2Sweep = new System.Windows.Forms.Label();
            this.panel279 = new System.Windows.Forms.Panel();
            this.labelSample2Ampl = new System.Windows.Forms.Label();
            this.panel280 = new System.Windows.Forms.Panel();
            this.labelSample3EndFull = new System.Windows.Forms.Label();
            this.panel283 = new System.Windows.Forms.Panel();
            this.panel284 = new System.Windows.Forms.Panel();
            this.labelSample3LeadFull = new System.Windows.Forms.Label();
            this.pictureBoxSample3Full = new System.Windows.Forms.PictureBox();
            this.panel300 = new System.Windows.Forms.Panel();
            this.panel384 = new System.Windows.Forms.Panel();
            this.labelSample3Start = new System.Windows.Forms.Label();
            this.labelSample3Mid = new System.Windows.Forms.Label();
            this.panel324 = new System.Windows.Forms.Panel();
            this.labelSample3Sweep = new System.Windows.Forms.Label();
            this.panel385 = new System.Windows.Forms.Panel();
            this.labelSample3Ampl = new System.Windows.Forms.Label();
            this.panel386 = new System.Windows.Forms.Panel();
            this.labelSample3End = new System.Windows.Forms.Label();
            this.panel387 = new System.Windows.Forms.Panel();
            this.panel388 = new System.Windows.Forms.Panel();
            this.labelSample3Lead = new System.Windows.Forms.Label();
            this.pictureBoxSample3 = new System.Windows.Forms.PictureBox();
            this.panel416 = new System.Windows.Forms.Panel();
            this.labelSample3Date = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.labelSample3Time = new System.Windows.Forms.Label();
            this.labelEvent3 = new System.Windows.Forms.Label();
            this.labelClass3 = new System.Windows.Forms.Label();
            this.labelSample3Nr = new System.Windows.Forms.Label();
            this.labelSample3EventNr = new System.Windows.Forms.Label();
            this.panel450 = new System.Windows.Forms.Panel();
            this.panel488 = new System.Windows.Forms.Panel();
            this.panelPage2Header = new System.Windows.Forms.Panel();
            this.panel533 = new System.Windows.Forms.Panel();
            this.panel531 = new System.Windows.Forms.Panel();
            this.panel529 = new System.Windows.Forms.Panel();
            this.labelDateOfBirthResPage2 = new System.Windows.Forms.Label();
            this.panel528 = new System.Windows.Forms.Panel();
            this.labelDateOfBirthPage2 = new System.Windows.Forms.Label();
            this.panel527 = new System.Windows.Forms.Panel();
            this.panel526 = new System.Windows.Forms.Panel();
            this.panel525 = new System.Windows.Forms.Panel();
            this.panel524 = new System.Windows.Forms.Panel();
            this.labelPatLastName = new System.Windows.Forms.Label();
            this.panel523 = new System.Windows.Forms.Panel();
            this.labelPatName = new System.Windows.Forms.Label();
            this.panel522 = new System.Windows.Forms.Panel();
            this.panel521 = new System.Windows.Forms.Panel();
            this.panel520 = new System.Windows.Forms.Panel();
            this.panel519 = new System.Windows.Forms.Panel();
            this.labelPatientIDResult = new System.Windows.Forms.Label();
            this.panel497 = new System.Windows.Forms.Panel();
            this.labelPatientID = new System.Windows.Forms.Label();
            this.labelReportText2 = new System.Windows.Forms.Label();
            this.labelPage2ReportNr = new System.Windows.Forms.Label();
            this.panel500 = new System.Windows.Forms.Panel();
            this.pictureBoxCenter2 = new System.Windows.Forms.PictureBox();
            this.panel501 = new System.Windows.Forms.Panel();
            this.panel503 = new System.Windows.Forms.Panel();
            this.labelCenter2p2 = new System.Windows.Forms.Label();
            this.panel504 = new System.Windows.Forms.Panel();
            this.labelCenter1p2 = new System.Windows.Forms.Label();
            this.panel505 = new System.Windows.Forms.Panel();
            this.panelPage2Footer = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.labelStudyNumberResPage2 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.labelPage2x = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.labelPage2OfX = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelPrintDate2Bottom = new System.Windows.Forms.Label();
            this.labelPrintDate = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panelPrintArea3 = new System.Windows.Forms.Panel();
            this.panelDisclosurePage = new System.Windows.Forms.Panel();
            this.panelDisclosureStrips = new System.Windows.Forms.Panel();
            this.panel71 = new System.Windows.Forms.Panel();
            this.labelSFdStripInfo = new System.Windows.Forms.Label();
            this.panel66 = new System.Windows.Forms.Panel();
            this.panel456 = new System.Windows.Forms.Panel();
            this.labelUnitFeed = new System.Windows.Forms.Label();
            this.labelUnitAmp = new System.Windows.Forms.Label();
            this.labelEndPageTime = new System.Windows.Forms.Label();
            this.panel179 = new System.Windows.Forms.Panel();
            this.panel180 = new System.Windows.Forms.Panel();
            this.pictureBoxStrip20 = new System.Windows.Forms.PictureBox();
            this.panel181 = new System.Windows.Forms.Panel();
            this.labelStripTime20 = new System.Windows.Forms.Label();
            this.labelStripDate20 = new System.Windows.Forms.Label();
            this.labelStripLead20 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.panel198 = new System.Windows.Forms.Panel();
            this.panel93 = new System.Windows.Forms.Panel();
            this.panel94 = new System.Windows.Forms.Panel();
            this.pictureBoxStrip19 = new System.Windows.Forms.PictureBox();
            this.panel154 = new System.Windows.Forms.Panel();
            this.labelStripTime19 = new System.Windows.Forms.Label();
            this.labelStripDate19 = new System.Windows.Forms.Label();
            this.labelStripLead19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.panel156 = new System.Windows.Forms.Panel();
            this.panel204 = new System.Windows.Forms.Panel();
            this.panel210 = new System.Windows.Forms.Panel();
            this.pictureBoxStrip18 = new System.Windows.Forms.PictureBox();
            this.panel155 = new System.Windows.Forms.Panel();
            this.labelStripTime18 = new System.Windows.Forms.Label();
            this.labelStripDate18 = new System.Windows.Forms.Label();
            this.labelStripLead18 = new System.Windows.Forms.Label();
            this.label123 = new System.Windows.Forms.Label();
            this.label120 = new System.Windows.Forms.Label();
            this.panel211 = new System.Windows.Forms.Panel();
            this.panel361 = new System.Windows.Forms.Panel();
            this.panel404 = new System.Windows.Forms.Panel();
            this.pictureBoxStrip17 = new System.Windows.Forms.PictureBox();
            this.panel192 = new System.Windows.Forms.Panel();
            this.labelStripTime17 = new System.Windows.Forms.Label();
            this.labelStripDate17 = new System.Windows.Forms.Label();
            this.labelStripLead17 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this.panel410 = new System.Windows.Forms.Panel();
            this.panel352 = new System.Windows.Forms.Panel();
            this.panel353 = new System.Windows.Forms.Panel();
            this.pictureBoxStrip16 = new System.Windows.Forms.PictureBox();
            this.panel212 = new System.Windows.Forms.Panel();
            this.labelStripTime16 = new System.Windows.Forms.Label();
            this.labelStripDate16 = new System.Windows.Forms.Label();
            this.labelStripLead16 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.panel354 = new System.Windows.Forms.Panel();
            this.panel343 = new System.Windows.Forms.Panel();
            this.panel347 = new System.Windows.Forms.Panel();
            this.pictureBoxStrip15 = new System.Windows.Forms.PictureBox();
            this.panel227 = new System.Windows.Forms.Panel();
            this.labelStripTime15 = new System.Windows.Forms.Label();
            this.labelStripDate15 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.labelStripLead15 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.panel348 = new System.Windows.Forms.Panel();
            this.panel340 = new System.Windows.Forms.Panel();
            this.panel341 = new System.Windows.Forms.Panel();
            this.pictureBoxStrip14 = new System.Windows.Forms.PictureBox();
            this.panel320 = new System.Windows.Forms.Panel();
            this.labelStripTime14 = new System.Windows.Forms.Label();
            this.labelStripDate14 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.labelStripLead14 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.panel346 = new System.Windows.Forms.Panel();
            this.panel402 = new System.Windows.Forms.Panel();
            this.panel406 = new System.Windows.Forms.Panel();
            this.pictureBoxStrip13 = new System.Windows.Forms.PictureBox();
            this.panel338 = new System.Windows.Forms.Panel();
            this.labelStripTime13 = new System.Windows.Forms.Label();
            this.labelStripDate13 = new System.Windows.Forms.Label();
            this.labelStripLead13 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.panel407 = new System.Windows.Forms.Panel();
            this.panel362 = new System.Windows.Forms.Panel();
            this.panel363 = new System.Windows.Forms.Panel();
            this.pictureBoxStrip12 = new System.Windows.Forms.PictureBox();
            this.panel344 = new System.Windows.Forms.Panel();
            this.labelStripTime12 = new System.Windows.Forms.Label();
            this.labelStripDate12 = new System.Windows.Forms.Label();
            this.labelStripLead12 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.panel396 = new System.Windows.Forms.Panel();
            this.panel399 = new System.Windows.Forms.Panel();
            this.panel400 = new System.Windows.Forms.Panel();
            this.pictureBoxStrip11 = new System.Windows.Forms.PictureBox();
            this.panel349 = new System.Windows.Forms.Panel();
            this.labelStripTime11 = new System.Windows.Forms.Label();
            this.labelStripDate11 = new System.Windows.Forms.Label();
            this.labelStripLead11 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.panel405 = new System.Windows.Forms.Panel();
            this.panel446 = new System.Windows.Forms.Panel();
            this.panel606 = new System.Windows.Forms.Panel();
            this.pictureBoxStrip10 = new System.Windows.Forms.PictureBox();
            this.panel360 = new System.Windows.Forms.Panel();
            this.labelStripTime10 = new System.Windows.Forms.Label();
            this.labelStripDate10 = new System.Windows.Forms.Label();
            this.labelStripLead10 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.panel607 = new System.Windows.Forms.Panel();
            this.panel443 = new System.Windows.Forms.Panel();
            this.panel444 = new System.Windows.Forms.Panel();
            this.pictureBoxStrip9 = new System.Windows.Forms.PictureBox();
            this.panel397 = new System.Windows.Forms.Panel();
            this.labelStripTime9 = new System.Windows.Forms.Label();
            this.labelStripDate9 = new System.Windows.Forms.Label();
            this.labelStripLead9 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.panel605 = new System.Windows.Forms.Panel();
            this.panel439 = new System.Windows.Forms.Panel();
            this.panel441 = new System.Windows.Forms.Panel();
            this.pictureBoxStrip8 = new System.Windows.Forms.PictureBox();
            this.panel403 = new System.Windows.Forms.Panel();
            this.labelStripTime8 = new System.Windows.Forms.Label();
            this.labelStripDate8 = new System.Windows.Forms.Label();
            this.labelStripLead8 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.panel442 = new System.Windows.Forms.Panel();
            this.panel433 = new System.Windows.Forms.Panel();
            this.panel437 = new System.Windows.Forms.Panel();
            this.pictureBoxStrip7 = new System.Windows.Forms.PictureBox();
            this.panel408 = new System.Windows.Forms.Panel();
            this.labelStripTime7 = new System.Windows.Forms.Label();
            this.labelStripDate7 = new System.Windows.Forms.Label();
            this.labelStripLead7 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.panel438 = new System.Windows.Forms.Panel();
            this.panel431 = new System.Windows.Forms.Panel();
            this.panel432 = new System.Windows.Forms.Panel();
            this.pictureBoxStrip6 = new System.Windows.Forms.PictureBox();
            this.panel413 = new System.Windows.Forms.Panel();
            this.labelStripTime6 = new System.Windows.Forms.Label();
            this.labelStripDate6 = new System.Windows.Forms.Label();
            this.labelStripLead6 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.panel436 = new System.Windows.Forms.Panel();
            this.panel602 = new System.Windows.Forms.Panel();
            this.panel603 = new System.Windows.Forms.Panel();
            this.pictureBoxStrip5 = new System.Windows.Forms.PictureBox();
            this.panel434 = new System.Windows.Forms.Panel();
            this.labelStripTime5 = new System.Windows.Forms.Label();
            this.labelStripDate5 = new System.Windows.Forms.Label();
            this.labelStripLead5 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.panel604 = new System.Windows.Forms.Panel();
            this.panel598 = new System.Windows.Forms.Panel();
            this.panel600 = new System.Windows.Forms.Panel();
            this.pictureBoxStrip4 = new System.Windows.Forms.PictureBox();
            this.panel440 = new System.Windows.Forms.Panel();
            this.labelStripTime4 = new System.Windows.Forms.Label();
            this.labelStripDate4 = new System.Windows.Forms.Label();
            this.labelStripLead4 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.panel601 = new System.Windows.Forms.Panel();
            this.panel596 = new System.Windows.Forms.Panel();
            this.panel597 = new System.Windows.Forms.Panel();
            this.pictureBoxStrip3 = new System.Windows.Forms.PictureBox();
            this.panel445 = new System.Windows.Forms.Panel();
            this.labelStripTime3 = new System.Windows.Forms.Label();
            this.labelStripDate3 = new System.Windows.Forms.Label();
            this.labelStripLead3 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.panel599 = new System.Windows.Forms.Panel();
            this.panel494 = new System.Windows.Forms.Panel();
            this.panel498 = new System.Windows.Forms.Panel();
            this.pictureBoxStrip2 = new System.Windows.Forms.PictureBox();
            this.panel502 = new System.Windows.Forms.Panel();
            this.labelStripTime2 = new System.Windows.Forms.Label();
            this.labelStripDate2 = new System.Windows.Forms.Label();
            this.labelStripLead2 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.panel595 = new System.Windows.Forms.Panel();
            this.panel19 = new System.Windows.Forms.Panel();
            this.panel92 = new System.Windows.Forms.Panel();
            this.pictureBoxStrip1 = new System.Windows.Forms.PictureBox();
            this.panel511 = new System.Windows.Forms.Panel();
            this.labelStripTime1 = new System.Windows.Forms.Label();
            this.labelStripDate1 = new System.Windows.Forms.Label();
            this.labelStripLead1 = new System.Windows.Forms.Label();
            this.label136 = new System.Windows.Forms.Label();
            this.label139 = new System.Windows.Forms.Label();
            this.panel23 = new System.Windows.Forms.Panel();
            this.panel469 = new System.Windows.Forms.Panel();
            this.panel496 = new System.Windows.Forms.Panel();
            this.panel512 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.labelStartPageDateTime = new System.Windows.Forms.Label();
            this.labelStripLength = new System.Windows.Forms.Label();
            this.panel514 = new System.Windows.Forms.Panel();
            this.label28 = new System.Windows.Forms.Label();
            this.labelEventDataTime = new System.Windows.Forms.Label();
            this.label141 = new System.Windows.Forms.Label();
            this.panel515 = new System.Windows.Forms.Panel();
            this.panel535 = new System.Windows.Forms.Panel();
            this.panel536 = new System.Windows.Forms.Panel();
            this.panel553 = new System.Windows.Forms.Panel();
            this.panel554 = new System.Windows.Forms.Panel();
            this.labelReportText3 = new System.Windows.Forms.Label();
            this.panel53 = new System.Windows.Forms.Panel();
            this.panel54 = new System.Windows.Forms.Panel();
            this.labelDateOfBirthResPage3 = new System.Windows.Forms.Label();
            this.labelDateOfBirthPage3 = new System.Windows.Forms.Label();
            this.panel36 = new System.Windows.Forms.Panel();
            this.panel49 = new System.Windows.Forms.Panel();
            this.labelPatLastNam3 = new System.Windows.Forms.Label();
            this.labelPatName3 = new System.Windows.Forms.Label();
            this.panel577 = new System.Windows.Forms.Panel();
            this.panel579 = new System.Windows.Forms.Panel();
            this.labelPatientIDResult3 = new System.Windows.Forms.Label();
            this.labelPatientID3 = new System.Windows.Forms.Label();
            this.labelPage3ReportNr = new System.Windows.Forms.Label();
            this.panel91 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.labelStudyNumberResPage3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelPage3x = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labelPage3OfX = new System.Windows.Forms.Label();
            this.labelPrintDate3Bottom = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.panel15 = new System.Windows.Forms.Panel();
            this.panelPrintArea1.SuspendLayout();
            this.panelEventSample2.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel114.SuspendLayout();
            this.panel115.SuspendLayout();
            this.panel119.SuspendLayout();
            this.panel128.SuspendLayout();
            this.panel127.SuspendLayout();
            this.panel113.SuspendLayout();
            this.panel121.SuspendLayout();
            this.panel122.SuspendLayout();
            this.panel123.SuspendLayout();
            this.panel124.SuspendLayout();
            this.panel125.SuspendLayout();
            this.panel126.SuspendLayout();
            this.panelSample2Time.SuspendLayout();
            this.panelSeceondStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample2)).BeginInit();
            this.panelSample2Header.SuspendLayout();
            this.panelEventSample1.SuspendLayout();
            this.panel50.SuspendLayout();
            this.panelActivities3.SuspendLayout();
            this.panel55.SuspendLayout();
            this.panel57.SuspendLayout();
            this.panel118.SuspendLayout();
            this.panel64.SuspendLayout();
            this.panel68.SuspendLayout();
            this.panel69.SuspendLayout();
            this.panel70.SuspendLayout();
            this.panel72.SuspendLayout();
            this.panel73.SuspendLayout();
            this.panel75.SuspendLayout();
            this.panel149.SuspendLayout();
            this.panelFullStrip.SuspendLayout();
            this.panel44.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample1Full)).BeginInit();
            this.panel146.SuspendLayout();
            this.panel234.SuspendLayout();
            this.panel232.SuspendLayout();
            this.panel233.SuspendLayout();
            this.panel230.SuspendLayout();
            this.panelSample1STrip.SuspendLayout();
            this.panel37.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample1)).BeginInit();
            this.panel142.SuspendLayout();
            this.panelBaseLineMFS.SuspendLayout();
            this.panelSymptoms3.SuspendLayout();
            this.panelfindings3.SuspendLayout();
            this.panel176.SuspendLayout();
            this.panelQRSmeasurements3.SuspendLayout();
            this.panel47.SuspendLayout();
            this.panel43.SuspendLayout();
            this.panel42.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel38.SuspendLayout();
            this.panel39.SuspendLayout();
            this.panel40.SuspendLayout();
            this.panel41.SuspendLayout();
            this.panelBlSweepAmpIndicator.SuspendLayout();
            this.panelSweepSpeed.SuspendLayout();
            this.panelAmpPanel.SuspendLayout();
            this.panelECGBaselineSTrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBaseline)).BeginInit();
            this.panelBaseLineHeader.SuspendLayout();
            this.panelEventStats.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel111.SuspendLayout();
            this.panel96.SuspendLayout();
            this.panel84.SuspendLayout();
            this.panel46.SuspendLayout();
            this.panel45.SuspendLayout();
            this.panel30.SuspendLayout();
            this.panel78.SuspendLayout();
            this.panel110.SuspendLayout();
            this.panel109.SuspendLayout();
            this.panel108.SuspendLayout();
            this.panel107.SuspendLayout();
            this.panel106.SuspendLayout();
            this.panel105.SuspendLayout();
            this.panel51.SuspendLayout();
            this.panel510.SuspendLayout();
            this.panel509.SuspendLayout();
            this.panel508.SuspendLayout();
            this.panel507.SuspendLayout();
            this.panelPhysInfo.SuspendLayout();
            this.panel67.SuspendLayout();
            this.panel258.SuspendLayout();
            this.panel256.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel252.SuspendLayout();
            this.panel250.SuspendLayout();
            this.panel244.SuspendLayout();
            this.panel242.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panelSecPatBar.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel34.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel26.SuspendLayout();
            this.panel33.SuspendLayout();
            this.panelDateTimeofEventStrip.SuspendLayout();
            this.panelCardiacEVentReportNumber.SuspendLayout();
            this.panelPrintHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCenter)).BeginInit();
            this.panel56.SuspendLayout();
            this.panel58.SuspendLayout();
            this.panel59.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.panelPrintArea2.SuspendLayout();
            this.panelEventSample5.SuspendLayout();
            this.panel160.SuspendLayout();
            this.panel163.SuspendLayout();
            this.panel164.SuspendLayout();
            this.panel169.SuspendLayout();
            this.panel170.SuspendLayout();
            this.panel171.SuspendLayout();
            this.panel172.SuspendLayout();
            this.panel173.SuspendLayout();
            this.panel174.SuspendLayout();
            this.panel175.SuspendLayout();
            this.panel153.SuspendLayout();
            this.panel238.SuspendLayout();
            this.panel241.SuspendLayout();
            this.panel243.SuspendLayout();
            this.panel290.SuspendLayout();
            this.panel291.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample5Full)).BeginInit();
            this.panel378.SuspendLayout();
            this.panel383.SuspendLayout();
            this.panel419.SuspendLayout();
            this.panel420.SuspendLayout();
            this.panel421.SuspendLayout();
            this.panel422.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample5)).BeginInit();
            this.panel453.SuspendLayout();
            this.panelEventSample4.SuspendLayout();
            this.panel136.SuspendLayout();
            this.panel139.SuspendLayout();
            this.panel140.SuspendLayout();
            this.panel144.SuspendLayout();
            this.panel147.SuspendLayout();
            this.panel148.SuspendLayout();
            this.panel150.SuspendLayout();
            this.panel151.SuspendLayout();
            this.panel152.SuspendLayout();
            this.panel157.SuspendLayout();
            this.panel266.SuspendLayout();
            this.panel267.SuspendLayout();
            this.panel268.SuspendLayout();
            this.panel269.SuspendLayout();
            this.panel275.SuspendLayout();
            this.panel297.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample4Full)).BeginInit();
            this.panel306.SuspendLayout();
            this.panel311.SuspendLayout();
            this.panel312.SuspendLayout();
            this.panel313.SuspendLayout();
            this.panel328.SuspendLayout();
            this.panel329.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample4)).BeginInit();
            this.panel357.SuspendLayout();
            this.panelEventSample3.SuspendLayout();
            this.panel32.SuspendLayout();
            this.panel103.SuspendLayout();
            this.panel104.SuspendLayout();
            this.panel129.SuspendLayout();
            this.panel130.SuspendLayout();
            this.panel131.SuspendLayout();
            this.panel132.SuspendLayout();
            this.panel133.SuspendLayout();
            this.panel134.SuspendLayout();
            this.panel135.SuspendLayout();
            this.panel277.SuspendLayout();
            this.panel278.SuspendLayout();
            this.panel279.SuspendLayout();
            this.panel280.SuspendLayout();
            this.panel283.SuspendLayout();
            this.panel284.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample3Full)).BeginInit();
            this.panel384.SuspendLayout();
            this.panel324.SuspendLayout();
            this.panel385.SuspendLayout();
            this.panel386.SuspendLayout();
            this.panel387.SuspendLayout();
            this.panel388.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample3)).BeginInit();
            this.panel416.SuspendLayout();
            this.panelPage2Header.SuspendLayout();
            this.panel529.SuspendLayout();
            this.panel528.SuspendLayout();
            this.panel524.SuspendLayout();
            this.panel523.SuspendLayout();
            this.panel519.SuspendLayout();
            this.panel497.SuspendLayout();
            this.panel500.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCenter2)).BeginInit();
            this.panel501.SuspendLayout();
            this.panel503.SuspendLayout();
            this.panel504.SuspendLayout();
            this.panelPage2Footer.SuspendLayout();
            this.panelPrintArea3.SuspendLayout();
            this.panelDisclosurePage.SuspendLayout();
            this.panelDisclosureStrips.SuspendLayout();
            this.panel71.SuspendLayout();
            this.panel456.SuspendLayout();
            this.panel179.SuspendLayout();
            this.panel180.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip20)).BeginInit();
            this.panel181.SuspendLayout();
            this.panel93.SuspendLayout();
            this.panel94.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip19)).BeginInit();
            this.panel154.SuspendLayout();
            this.panel204.SuspendLayout();
            this.panel210.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip18)).BeginInit();
            this.panel155.SuspendLayout();
            this.panel361.SuspendLayout();
            this.panel404.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip17)).BeginInit();
            this.panel192.SuspendLayout();
            this.panel352.SuspendLayout();
            this.panel353.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip16)).BeginInit();
            this.panel212.SuspendLayout();
            this.panel343.SuspendLayout();
            this.panel347.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip15)).BeginInit();
            this.panel227.SuspendLayout();
            this.panel340.SuspendLayout();
            this.panel341.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip14)).BeginInit();
            this.panel320.SuspendLayout();
            this.panel402.SuspendLayout();
            this.panel406.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip13)).BeginInit();
            this.panel338.SuspendLayout();
            this.panel362.SuspendLayout();
            this.panel363.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip12)).BeginInit();
            this.panel344.SuspendLayout();
            this.panel399.SuspendLayout();
            this.panel400.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip11)).BeginInit();
            this.panel349.SuspendLayout();
            this.panel446.SuspendLayout();
            this.panel606.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip10)).BeginInit();
            this.panel360.SuspendLayout();
            this.panel443.SuspendLayout();
            this.panel444.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip9)).BeginInit();
            this.panel397.SuspendLayout();
            this.panel439.SuspendLayout();
            this.panel441.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip8)).BeginInit();
            this.panel403.SuspendLayout();
            this.panel433.SuspendLayout();
            this.panel437.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip7)).BeginInit();
            this.panel408.SuspendLayout();
            this.panel431.SuspendLayout();
            this.panel432.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip6)).BeginInit();
            this.panel413.SuspendLayout();
            this.panel602.SuspendLayout();
            this.panel603.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip5)).BeginInit();
            this.panel434.SuspendLayout();
            this.panel598.SuspendLayout();
            this.panel600.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip4)).BeginInit();
            this.panel440.SuspendLayout();
            this.panel596.SuspendLayout();
            this.panel597.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip3)).BeginInit();
            this.panel445.SuspendLayout();
            this.panel494.SuspendLayout();
            this.panel498.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip2)).BeginInit();
            this.panel502.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel92.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip1)).BeginInit();
            this.panel511.SuspendLayout();
            this.panel512.SuspendLayout();
            this.panel514.SuspendLayout();
            this.panel535.SuspendLayout();
            this.panel554.SuspendLayout();
            this.panel91.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelPrintArea1
            // 
            this.panelPrintArea1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelPrintArea1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panelPrintArea1.Controls.Add(this.panelEventSample2);
            this.panelPrintArea1.Controls.Add(this.panelEventSample1);
            this.panelPrintArea1.Controls.Add(this.panel48);
            this.panelPrintArea1.Controls.Add(this.panelBaseLineMFS);
            this.panelPrintArea1.Controls.Add(this.panelBlSweepAmpIndicator);
            this.panelPrintArea1.Controls.Add(this.panelECGBaselineSTrip);
            this.panelPrintArea1.Controls.Add(this.panelBaseLineHeader);
            this.panelPrintArea1.Controls.Add(this.panel35);
            this.panelPrintArea1.Controls.Add(this.panelEventStats);
            this.panelPrintArea1.Controls.Add(this.panel78);
            this.panelPrintArea1.Controls.Add(this.panel77);
            this.panelPrintArea1.Controls.Add(this.panel6);
            this.panelPrintArea1.Controls.Add(this.panelHorSepPatDet);
            this.panelPrintArea1.Controls.Add(this.panel51);
            this.panelPrintArea1.Controls.Add(this.panelPhysInfo);
            this.panelPrintArea1.Controls.Add(this.panel16);
            this.panelPrintArea1.Controls.Add(this.panelSecPatBar);
            this.panelPrintArea1.Controls.Add(this.panelDateTimeofEventStrip);
            this.panelPrintArea1.Controls.Add(this.panelCardiacEVentReportNumber);
            this.panelPrintArea1.Controls.Add(this.panelPrintHeader);
            this.panelPrintArea1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.panelPrintArea1.Location = new System.Drawing.Point(12, 94);
            this.panelPrintArea1.Name = "panelPrintArea1";
            this.panelPrintArea1.Size = new System.Drawing.Size(1654, 2339);
            this.panelPrintArea1.TabIndex = 0;
            // 
            // panelEventSample2
            // 
            this.panelEventSample2.AutoSize = true;
            this.panelEventSample2.Controls.Add(this.panel11);
            this.panelEventSample2.Controls.Add(this.panelSample2Time);
            this.panelEventSample2.Controls.Add(this.panelSeceondStrip);
            this.panelEventSample2.Controls.Add(this.panelSample2Header);
            this.panelEventSample2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelEventSample2.Location = new System.Drawing.Point(0, 1909);
            this.panelEventSample2.Name = "panelEventSample2";
            this.panelEventSample2.Size = new System.Drawing.Size(1654, 309);
            this.panelEventSample2.TabIndex = 65;
            // 
            // panel11
            // 
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel11.Controls.Add(this.panel114);
            this.panel11.Controls.Add(this.panel116);
            this.panel11.Controls.Add(this.panel117);
            this.panel11.Controls.Add(this.panel119);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel11.Location = new System.Drawing.Point(0, 195);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(1654, 114);
            this.panel11.TabIndex = 64;
            // 
            // panel114
            // 
            this.panel114.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel114.Controls.Add(this.labelFindings2);
            this.panel114.Controls.Add(this.panel115);
            this.panel114.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel114.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel114.Location = new System.Drawing.Point(762, 0);
            this.panel114.Name = "panel114";
            this.panel114.Size = new System.Drawing.Size(890, 112);
            this.panel114.TabIndex = 7;
            // 
            // labelFindings2
            // 
            this.labelFindings2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelFindings2.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelFindings2.Location = new System.Drawing.Point(0, 35);
            this.labelFindings2.Name = "labelFindings2";
            this.labelFindings2.Size = new System.Drawing.Size(888, 75);
            this.labelFindings2.TabIndex = 6;
            this.labelFindings2.Text = "^Atrial Fibrillation, with Occasional PVC\'s and many other findings that the Tech" +
    " can add a lot of text ";
            // 
            // panel115
            // 
            this.panel115.Controls.Add(this.labelRhythms2);
            this.panel115.Controls.Add(this.labelFindingsText2);
            this.panel115.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel115.Location = new System.Drawing.Point(0, 0);
            this.panel115.Name = "panel115";
            this.panel115.Size = new System.Drawing.Size(888, 35);
            this.panel115.TabIndex = 2;
            // 
            // labelRhythms2
            // 
            this.labelRhythms2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelRhythms2.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelRhythms2.Location = new System.Drawing.Point(203, 0);
            this.labelRhythms2.Name = "labelRhythms2";
            this.labelRhythms2.Size = new System.Drawing.Size(685, 35);
            this.labelRhythms2.TabIndex = 70;
            this.labelRhythms2.Text = "^Rhythms";
            // 
            // labelFindingsText2
            // 
            this.labelFindingsText2.AutoSize = true;
            this.labelFindingsText2.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelFindingsText2.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.labelFindingsText2.Location = new System.Drawing.Point(0, 0);
            this.labelFindingsText2.Name = "labelFindingsText2";
            this.labelFindingsText2.Size = new System.Drawing.Size(203, 29);
            this.labelFindingsText2.TabIndex = 2;
            this.labelFindingsText2.Text = "^QC Findings:";
            // 
            // panel116
            // 
            this.panel116.AutoSize = true;
            this.panel116.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel116.Location = new System.Drawing.Point(762, 0);
            this.panel116.Name = "panel116";
            this.panel116.Size = new System.Drawing.Size(0, 112);
            this.panel116.TabIndex = 6;
            // 
            // panel117
            // 
            this.panel117.AutoSize = true;
            this.panel117.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel117.Location = new System.Drawing.Point(762, 0);
            this.panel117.Name = "panel117";
            this.panel117.Size = new System.Drawing.Size(0, 112);
            this.panel117.TabIndex = 3;
            // 
            // panel119
            // 
            this.panel119.Controls.Add(this.panel128);
            this.panel119.Controls.Add(this.panel127);
            this.panel119.Controls.Add(this.panel113);
            this.panel119.Controls.Add(this.panel121);
            this.panel119.Controls.Add(this.panel122);
            this.panel119.Controls.Add(this.panel123);
            this.panel119.Controls.Add(this.panel124);
            this.panel119.Controls.Add(this.panel125);
            this.panel119.Controls.Add(this.panel126);
            this.panel119.Controls.Add(this.label146);
            this.panel119.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel119.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel119.Location = new System.Drawing.Point(0, 0);
            this.panel119.Name = "panel119";
            this.panel119.Size = new System.Drawing.Size(762, 112);
            this.panel119.TabIndex = 9;
            // 
            // panel128
            // 
            this.panel128.Controls.Add(this.unitQt2);
            this.panel128.Controls.Add(this.unitQrs2);
            this.panel128.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel128.Location = new System.Drawing.Point(701, 36);
            this.panel128.Name = "panel128";
            this.panel128.Size = new System.Drawing.Size(61, 76);
            this.panel128.TabIndex = 15;
            // 
            // unitQt2
            // 
            this.unitQt2.Dock = System.Windows.Forms.DockStyle.Top;
            this.unitQt2.Font = new System.Drawing.Font("Verdana", 18F);
            this.unitQt2.Location = new System.Drawing.Point(0, 36);
            this.unitQt2.Name = "unitQt2";
            this.unitQt2.Size = new System.Drawing.Size(61, 36);
            this.unitQt2.TabIndex = 72;
            this.unitQt2.Text = "sec";
            // 
            // unitQrs2
            // 
            this.unitQrs2.Dock = System.Windows.Forms.DockStyle.Top;
            this.unitQrs2.Font = new System.Drawing.Font("Verdana", 18F);
            this.unitQrs2.Location = new System.Drawing.Point(0, 0);
            this.unitQrs2.Name = "unitQrs2";
            this.unitQrs2.Size = new System.Drawing.Size(61, 36);
            this.unitQrs2.TabIndex = 71;
            this.unitQrs2.Text = "sec";
            // 
            // panel127
            // 
            this.panel127.Controls.Add(this.valueQt2);
            this.panel127.Controls.Add(this.valueQrs2);
            this.panel127.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel127.Location = new System.Drawing.Point(618, 36);
            this.panel127.Name = "panel127";
            this.panel127.Size = new System.Drawing.Size(83, 76);
            this.panel127.TabIndex = 14;
            // 
            // valueQt2
            // 
            this.valueQt2.Dock = System.Windows.Forms.DockStyle.Top;
            this.valueQt2.Font = new System.Drawing.Font("Verdana", 18F);
            this.valueQt2.Location = new System.Drawing.Point(0, 36);
            this.valueQt2.Name = "valueQt2";
            this.valueQt2.Size = new System.Drawing.Size(83, 36);
            this.valueQt2.TabIndex = 84;
            this.valueQt2.Text = "9,999";
            this.valueQt2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // valueQrs2
            // 
            this.valueQrs2.Dock = System.Windows.Forms.DockStyle.Top;
            this.valueQrs2.Font = new System.Drawing.Font("Verdana", 18F);
            this.valueQrs2.Location = new System.Drawing.Point(0, 0);
            this.valueQrs2.Name = "valueQrs2";
            this.valueQrs2.Size = new System.Drawing.Size(83, 36);
            this.valueQrs2.TabIndex = 80;
            this.valueQrs2.Text = "9,999";
            this.valueQrs2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel113
            // 
            this.panel113.Controls.Add(this.headerQt2);
            this.panel113.Controls.Add(this.headerQrs2);
            this.panel113.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel113.Location = new System.Drawing.Point(537, 36);
            this.panel113.Name = "panel113";
            this.panel113.Size = new System.Drawing.Size(81, 76);
            this.panel113.TabIndex = 13;
            // 
            // headerQt2
            // 
            this.headerQt2.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerQt2.Font = new System.Drawing.Font("Verdana", 18F);
            this.headerQt2.Location = new System.Drawing.Point(0, 36);
            this.headerQt2.Name = "headerQt2";
            this.headerQt2.Size = new System.Drawing.Size(81, 36);
            this.headerQt2.TabIndex = 78;
            this.headerQt2.Text = "QT:";
            // 
            // headerQrs2
            // 
            this.headerQrs2.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerQrs2.Font = new System.Drawing.Font("Verdana", 18F);
            this.headerQrs2.Location = new System.Drawing.Point(0, 0);
            this.headerQrs2.Name = "headerQrs2";
            this.headerQrs2.Size = new System.Drawing.Size(81, 36);
            this.headerQrs2.TabIndex = 77;
            this.headerQrs2.Text = "QRS:";
            // 
            // panel121
            // 
            this.panel121.Controls.Add(this.unitPr2);
            this.panel121.Controls.Add(this.unitMaxHr2);
            this.panel121.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel121.Location = new System.Drawing.Point(470, 36);
            this.panel121.Name = "panel121";
            this.panel121.Size = new System.Drawing.Size(67, 76);
            this.panel121.TabIndex = 11;
            // 
            // unitPr2
            // 
            this.unitPr2.Dock = System.Windows.Forms.DockStyle.Top;
            this.unitPr2.Font = new System.Drawing.Font("Verdana", 18F);
            this.unitPr2.Location = new System.Drawing.Point(0, 36);
            this.unitPr2.Name = "unitPr2";
            this.unitPr2.Size = new System.Drawing.Size(67, 36);
            this.unitPr2.TabIndex = 71;
            this.unitPr2.Text = "sec";
            // 
            // unitMaxHr2
            // 
            this.unitMaxHr2.Dock = System.Windows.Forms.DockStyle.Top;
            this.unitMaxHr2.Font = new System.Drawing.Font("Verdana", 18F);
            this.unitMaxHr2.Location = new System.Drawing.Point(0, 0);
            this.unitMaxHr2.Name = "unitMaxHr2";
            this.unitMaxHr2.Size = new System.Drawing.Size(67, 36);
            this.unitMaxHr2.TabIndex = 73;
            this.unitMaxHr2.Text = "bpm";
            // 
            // panel122
            // 
            this.panel122.Controls.Add(this.valuePr2);
            this.panel122.Controls.Add(this.valueMaxHr2);
            this.panel122.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel122.Location = new System.Drawing.Point(383, 36);
            this.panel122.Name = "panel122";
            this.panel122.Size = new System.Drawing.Size(87, 76);
            this.panel122.TabIndex = 10;
            // 
            // valuePr2
            // 
            this.valuePr2.Dock = System.Windows.Forms.DockStyle.Top;
            this.valuePr2.Font = new System.Drawing.Font("Verdana", 18F);
            this.valuePr2.Location = new System.Drawing.Point(0, 36);
            this.valuePr2.Name = "valuePr2";
            this.valuePr2.Size = new System.Drawing.Size(87, 36);
            this.valuePr2.TabIndex = 77;
            this.valuePr2.Text = "9,999";
            this.valuePr2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // valueMaxHr2
            // 
            this.valueMaxHr2.Dock = System.Windows.Forms.DockStyle.Top;
            this.valueMaxHr2.Font = new System.Drawing.Font("Verdana", 18F);
            this.valueMaxHr2.Location = new System.Drawing.Point(0, 0);
            this.valueMaxHr2.Name = "valueMaxHr2";
            this.valueMaxHr2.Size = new System.Drawing.Size(87, 36);
            this.valueMaxHr2.TabIndex = 69;
            this.valueMaxHr2.Text = "999";
            this.valueMaxHr2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel123
            // 
            this.panel123.Controls.Add(this.headerPr2);
            this.panel123.Controls.Add(this.headerMaxHr2);
            this.panel123.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel123.Location = new System.Drawing.Point(262, 36);
            this.panel123.Name = "panel123";
            this.panel123.Size = new System.Drawing.Size(121, 76);
            this.panel123.TabIndex = 9;
            // 
            // headerPr2
            // 
            this.headerPr2.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPr2.Font = new System.Drawing.Font("Verdana", 18F);
            this.headerPr2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.headerPr2.Location = new System.Drawing.Point(0, 36);
            this.headerPr2.Name = "headerPr2";
            this.headerPr2.Size = new System.Drawing.Size(121, 36);
            this.headerPr2.TabIndex = 74;
            this.headerPr2.Text = "PR:";
            // 
            // headerMaxHr2
            // 
            this.headerMaxHr2.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerMaxHr2.Font = new System.Drawing.Font("Verdana", 18F);
            this.headerMaxHr2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.headerMaxHr2.Location = new System.Drawing.Point(0, 0);
            this.headerMaxHr2.Name = "headerMaxHr2";
            this.headerMaxHr2.Size = new System.Drawing.Size(121, 36);
            this.headerMaxHr2.TabIndex = 75;
            this.headerMaxHr2.Text = "Max HR:";
            // 
            // panel124
            // 
            this.panel124.Controls.Add(this.unitMeanHr2);
            this.panel124.Controls.Add(this.unitMinHr2);
            this.panel124.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel124.Location = new System.Drawing.Point(193, 36);
            this.panel124.Name = "panel124";
            this.panel124.Size = new System.Drawing.Size(69, 76);
            this.panel124.TabIndex = 8;
            // 
            // unitMeanHr2
            // 
            this.unitMeanHr2.Dock = System.Windows.Forms.DockStyle.Top;
            this.unitMeanHr2.Font = new System.Drawing.Font("Verdana", 18F);
            this.unitMeanHr2.Location = new System.Drawing.Point(0, 36);
            this.unitMeanHr2.Name = "unitMeanHr2";
            this.unitMeanHr2.Size = new System.Drawing.Size(69, 36);
            this.unitMeanHr2.TabIndex = 63;
            this.unitMeanHr2.Text = "bpm";
            // 
            // unitMinHr2
            // 
            this.unitMinHr2.Dock = System.Windows.Forms.DockStyle.Top;
            this.unitMinHr2.Font = new System.Drawing.Font("Verdana", 18F);
            this.unitMinHr2.Location = new System.Drawing.Point(0, 0);
            this.unitMinHr2.Name = "unitMinHr2";
            this.unitMinHr2.Size = new System.Drawing.Size(69, 36);
            this.unitMinHr2.TabIndex = 61;
            this.unitMinHr2.Text = "bpm";
            // 
            // panel125
            // 
            this.panel125.Controls.Add(this.valueMeanHr2);
            this.panel125.Controls.Add(this.valueMinHr2);
            this.panel125.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel125.Location = new System.Drawing.Point(131, 36);
            this.panel125.Name = "panel125";
            this.panel125.Size = new System.Drawing.Size(62, 76);
            this.panel125.TabIndex = 7;
            // 
            // valueMeanHr2
            // 
            this.valueMeanHr2.Dock = System.Windows.Forms.DockStyle.Top;
            this.valueMeanHr2.Font = new System.Drawing.Font("Verdana", 18F);
            this.valueMeanHr2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.valueMeanHr2.Location = new System.Drawing.Point(0, 36);
            this.valueMeanHr2.Name = "valueMeanHr2";
            this.valueMeanHr2.Size = new System.Drawing.Size(62, 36);
            this.valueMeanHr2.TabIndex = 67;
            this.valueMeanHr2.Text = "999";
            this.valueMeanHr2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // valueMinHr2
            // 
            this.valueMinHr2.Dock = System.Windows.Forms.DockStyle.Top;
            this.valueMinHr2.Font = new System.Drawing.Font("Verdana", 18F);
            this.valueMinHr2.Location = new System.Drawing.Point(0, 0);
            this.valueMinHr2.Name = "valueMinHr2";
            this.valueMinHr2.Size = new System.Drawing.Size(62, 36);
            this.valueMinHr2.TabIndex = 64;
            this.valueMinHr2.Text = "999";
            this.valueMinHr2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel126
            // 
            this.panel126.Controls.Add(this.headerMeanHr2);
            this.panel126.Controls.Add(this.headerMinHr2);
            this.panel126.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel126.Location = new System.Drawing.Point(0, 36);
            this.panel126.Name = "panel126";
            this.panel126.Size = new System.Drawing.Size(131, 76);
            this.panel126.TabIndex = 6;
            // 
            // headerMeanHr2
            // 
            this.headerMeanHr2.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerMeanHr2.Font = new System.Drawing.Font("Verdana", 18F);
            this.headerMeanHr2.Location = new System.Drawing.Point(0, 36);
            this.headerMeanHr2.Name = "headerMeanHr2";
            this.headerMeanHr2.Size = new System.Drawing.Size(131, 36);
            this.headerMeanHr2.TabIndex = 64;
            this.headerMeanHr2.Text = "Mean HR:";
            // 
            // headerMinHr2
            // 
            this.headerMinHr2.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerMinHr2.Font = new System.Drawing.Font("Verdana", 18F);
            this.headerMinHr2.Location = new System.Drawing.Point(0, 0);
            this.headerMinHr2.Name = "headerMinHr2";
            this.headerMinHr2.Size = new System.Drawing.Size(131, 36);
            this.headerMinHr2.TabIndex = 63;
            this.headerMinHr2.Text = "Min HR:";
            // 
            // label146
            // 
            this.label146.Dock = System.Windows.Forms.DockStyle.Top;
            this.label146.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.label146.Location = new System.Drawing.Point(0, 0);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(762, 36);
            this.label146.TabIndex = 5;
            this.label146.Text = "QRS Measurements:";
            // 
            // panelSample2Time
            // 
            this.panelSample2Time.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelSample2Time.Controls.Add(this.labelZoomSample2);
            this.panelSample2Time.Controls.Add(this.labelSweepSample2);
            this.panelSample2Time.Controls.Add(this.labelSample2Mid);
            this.panelSample2Time.Controls.Add(this.labelSample2Start);
            this.panelSample2Time.Controls.Add(this.labelSample2End);
            this.panelSample2Time.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSample2Time.Location = new System.Drawing.Point(0, 160);
            this.panelSample2Time.Name = "panelSample2Time";
            this.panelSample2Time.Size = new System.Drawing.Size(1654, 35);
            this.panelSample2Time.TabIndex = 63;
            // 
            // labelZoomSample2
            // 
            this.labelZoomSample2.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelZoomSample2.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelZoomSample2.Location = new System.Drawing.Point(902, 0);
            this.labelZoomSample2.Name = "labelZoomSample2";
            this.labelZoomSample2.Size = new System.Drawing.Size(290, 33);
            this.labelZoomSample2.TabIndex = 2;
            this.labelZoomSample2.Text = "10 mm/mV (0.50 mV)";
            this.labelZoomSample2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSweepSample2
            // 
            this.labelSweepSample2.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSweepSample2.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSweepSample2.Location = new System.Drawing.Point(1192, 0);
            this.labelSweepSample2.Name = "labelSweepSample2";
            this.labelSweepSample2.Size = new System.Drawing.Size(285, 33);
            this.labelSweepSample2.TabIndex = 4;
            this.labelSweepSample2.Text = "25 mm/sec (0.20 Sec)";
            this.labelSweepSample2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample2Mid
            // 
            this.labelSample2Mid.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSample2Mid.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample2Mid.Location = new System.Drawing.Point(591, 0);
            this.labelSample2Mid.Name = "labelSample2Mid";
            this.labelSample2Mid.Size = new System.Drawing.Size(138, 33);
            this.labelSample2Mid.TabIndex = 1;
            this.labelSample2Mid.Text = "10:15:05 AM";
            this.labelSample2Mid.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelSample2Mid.Visible = false;
            // 
            // labelSample2Start
            // 
            this.labelSample2Start.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSample2Start.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample2Start.Location = new System.Drawing.Point(0, 0);
            this.labelSample2Start.Name = "labelSample2Start";
            this.labelSample2Start.Size = new System.Drawing.Size(591, 33);
            this.labelSample2Start.TabIndex = 1;
            this.labelSample2Start.Text = "^^ 99:99:99 AM 99/99/9099  1d 12:34:12";
            this.labelSample2Start.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample2End
            // 
            this.labelSample2End.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample2End.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample2End.Location = new System.Drawing.Point(1477, 0);
            this.labelSample2End.Name = "labelSample2End";
            this.labelSample2End.Size = new System.Drawing.Size(175, 33);
            this.labelSample2End.TabIndex = 1;
            this.labelSample2End.Text = "10:15:10 AM";
            this.labelSample2End.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panelSeceondStrip
            // 
            this.panelSeceondStrip.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelSeceondStrip.Controls.Add(this.labelLeadSample2);
            this.panelSeceondStrip.Controls.Add(this.pictureBoxSample2);
            this.panelSeceondStrip.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSeceondStrip.Location = new System.Drawing.Point(0, 40);
            this.panelSeceondStrip.Name = "panelSeceondStrip";
            this.panelSeceondStrip.Size = new System.Drawing.Size(1654, 120);
            this.panelSeceondStrip.TabIndex = 62;
            // 
            // labelLeadSample2
            // 
            this.labelLeadSample2.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLeadSample2.Location = new System.Drawing.Point(7, 32);
            this.labelLeadSample2.Name = "labelLeadSample2";
            this.labelLeadSample2.Size = new System.Drawing.Size(219, 51);
            this.labelLeadSample2.TabIndex = 47;
            this.labelLeadSample2.Text = "LEAD I";
            this.labelLeadSample2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelLeadSample2.Visible = false;
            // 
            // pictureBoxSample2
            // 
            this.pictureBoxSample2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxSample2.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxSample2.Name = "pictureBoxSample2";
            this.pictureBoxSample2.Size = new System.Drawing.Size(1652, 118);
            this.pictureBoxSample2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxSample2.TabIndex = 6;
            this.pictureBoxSample2.TabStop = false;
            // 
            // panelSample2Header
            // 
            this.panelSample2Header.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelSample2Header.Controls.Add(this.labelClass2);
            this.panelSample2Header.Controls.Add(this.labelSample2Date);
            this.panelSample2Header.Controls.Add(this.label36);
            this.panelSample2Header.Controls.Add(this.labelSample2Time);
            this.panelSample2Header.Controls.Add(this.panel89);
            this.panelSample2Header.Controls.Add(this.panel86);
            this.panelSample2Header.Controls.Add(this.labelSample2Nr);
            this.panelSample2Header.Controls.Add(this.labelEvenNrSample2);
            this.panelSample2Header.Controls.Add(this.labelSample2EventNr);
            this.panelSample2Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSample2Header.Location = new System.Drawing.Point(0, 0);
            this.panelSample2Header.Name = "panelSample2Header";
            this.panelSample2Header.Size = new System.Drawing.Size(1654, 40);
            this.panelSample2Header.TabIndex = 60;
            // 
            // labelClass2
            // 
            this.labelClass2.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelClass2.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold);
            this.labelClass2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelClass2.Location = new System.Drawing.Point(60, 0);
            this.labelClass2.Name = "labelClass2";
            this.labelClass2.Size = new System.Drawing.Size(510, 38);
            this.labelClass2.TabIndex = 20;
            this.labelClass2.Text = "Other";
            this.labelClass2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample2Date
            // 
            this.labelSample2Date.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample2Date.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelSample2Date.Location = new System.Drawing.Point(1010, 0);
            this.labelSample2Date.Name = "labelSample2Date";
            this.labelSample2Date.Size = new System.Drawing.Size(200, 38);
            this.labelSample2Date.TabIndex = 14;
            this.labelSample2Date.Text = "11/23/2016";
            this.labelSample2Date.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label36
            // 
            this.label36.Dock = System.Windows.Forms.DockStyle.Right;
            this.label36.Font = new System.Drawing.Font("Verdana", 20F);
            this.label36.Location = new System.Drawing.Point(1210, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(17, 38);
            this.label36.TabIndex = 13;
            this.label36.Text = "-";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelSample2Time
            // 
            this.labelSample2Time.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample2Time.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelSample2Time.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelSample2Time.Location = new System.Drawing.Point(1227, 0);
            this.labelSample2Time.Name = "labelSample2Time";
            this.labelSample2Time.Size = new System.Drawing.Size(205, 38);
            this.labelSample2Time.TabIndex = 12;
            this.labelSample2Time.Text = "11:15:10 AM";
            this.labelSample2Time.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelSample2Time.Click += new System.EventHandler(this.labelTimeSampleNr2_Click);
            // 
            // panel89
            // 
            this.panel89.AutoSize = true;
            this.panel89.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel89.Location = new System.Drawing.Point(60, 0);
            this.panel89.Name = "panel89";
            this.panel89.Size = new System.Drawing.Size(0, 38);
            this.panel89.TabIndex = 6;
            this.panel89.Visible = false;
            // 
            // panel86
            // 
            this.panel86.AutoSize = true;
            this.panel86.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel86.Location = new System.Drawing.Point(60, 0);
            this.panel86.Name = "panel86";
            this.panel86.Size = new System.Drawing.Size(0, 38);
            this.panel86.TabIndex = 3;
            this.panel86.Visible = false;
            // 
            // labelSample2Nr
            // 
            this.labelSample2Nr.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSample2Nr.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold);
            this.labelSample2Nr.Location = new System.Drawing.Point(0, 0);
            this.labelSample2Nr.Name = "labelSample2Nr";
            this.labelSample2Nr.Size = new System.Drawing.Size(60, 38);
            this.labelSample2Nr.TabIndex = 22;
            this.labelSample2Nr.Text = "2";
            this.labelSample2Nr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelEvenNrSample2
            // 
            this.labelEvenNrSample2.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelEvenNrSample2.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelEvenNrSample2.Location = new System.Drawing.Point(1432, 0);
            this.labelEvenNrSample2.Name = "labelEvenNrSample2";
            this.labelEvenNrSample2.Size = new System.Drawing.Size(108, 38);
            this.labelEvenNrSample2.TabIndex = 21;
            this.labelEvenNrSample2.Text = "Event:";
            this.labelEvenNrSample2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample2EventNr
            // 
            this.labelSample2EventNr.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample2EventNr.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelSample2EventNr.Location = new System.Drawing.Point(1540, 0);
            this.labelSample2EventNr.Name = "labelSample2EventNr";
            this.labelSample2EventNr.Size = new System.Drawing.Size(112, 38);
            this.labelSample2EventNr.TabIndex = 19;
            this.labelSample2EventNr.Text = "^9999";
            this.labelSample2EventNr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panelEventSample1
            // 
            this.panelEventSample1.AutoSize = true;
            this.panelEventSample1.Controls.Add(this.panel97);
            this.panelEventSample1.Controls.Add(this.panel50);
            this.panelEventSample1.Controls.Add(this.panel149);
            this.panelEventSample1.Controls.Add(this.panelFullStrip);
            this.panelEventSample1.Controls.Add(this.panel146);
            this.panelEventSample1.Controls.Add(this.panelSample1STrip);
            this.panelEventSample1.Controls.Add(this.panel142);
            this.panelEventSample1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelEventSample1.Location = new System.Drawing.Point(0, 1177);
            this.panelEventSample1.Name = "panelEventSample1";
            this.panelEventSample1.Size = new System.Drawing.Size(1654, 732);
            this.panelEventSample1.TabIndex = 64;
            // 
            // panel97
            // 
            this.panel97.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel97.Location = new System.Drawing.Point(0, 726);
            this.panel97.Name = "panel97";
            this.panel97.Size = new System.Drawing.Size(1654, 6);
            this.panel97.TabIndex = 59;
            // 
            // panel50
            // 
            this.panel50.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel50.Controls.Add(this.panelActivities3);
            this.panel50.Controls.Add(this.panel55);
            this.panel50.Controls.Add(this.panel57);
            this.panel50.Controls.Add(this.panel62);
            this.panel50.Controls.Add(this.panel63);
            this.panel50.Controls.Add(this.panel64);
            this.panel50.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel50.Location = new System.Drawing.Point(0, 590);
            this.panel50.Name = "panel50";
            this.panel50.Size = new System.Drawing.Size(1654, 136);
            this.panel50.TabIndex = 58;
            // 
            // panelActivities3
            // 
            this.panelActivities3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelActivities3.Controls.Add(this.labelActivity1);
            this.panelActivities3.Controls.Add(this.label69);
            this.panelActivities3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelActivities3.Location = new System.Drawing.Point(1410, 0);
            this.panelActivities3.Name = "panelActivities3";
            this.panelActivities3.Size = new System.Drawing.Size(242, 134);
            this.panelActivities3.TabIndex = 8;
            // 
            // labelActivity1
            // 
            this.labelActivity1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelActivity1.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelActivity1.Location = new System.Drawing.Point(0, 39);
            this.labelActivity1.Name = "labelActivity1";
            this.labelActivity1.Size = new System.Drawing.Size(240, 93);
            this.labelActivity1.TabIndex = 5;
            this.labelActivity1.Text = "Walking up the Stairs";
            // 
            // label69
            // 
            this.label69.Dock = System.Windows.Forms.DockStyle.Top;
            this.label69.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.label69.Location = new System.Drawing.Point(0, 0);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(240, 39);
            this.label69.TabIndex = 4;
            this.label69.Text = "Activities:";
            // 
            // panel55
            // 
            this.panel55.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel55.Controls.Add(this.labelSymptoms1);
            this.panel55.Controls.Add(this.label79);
            this.panel55.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel55.Location = new System.Drawing.Point(1088, 0);
            this.panel55.Name = "panel55";
            this.panel55.Size = new System.Drawing.Size(322, 134);
            this.panel55.TabIndex = 3;
            // 
            // labelSymptoms1
            // 
            this.labelSymptoms1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSymptoms1.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelSymptoms1.Location = new System.Drawing.Point(0, 39);
            this.labelSymptoms1.Name = "labelSymptoms1";
            this.labelSymptoms1.Size = new System.Drawing.Size(320, 93);
            this.labelSymptoms1.TabIndex = 6;
            this.labelSymptoms1.Text = "Walking up the Stairs";
            // 
            // label79
            // 
            this.label79.Dock = System.Windows.Forms.DockStyle.Top;
            this.label79.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.label79.Location = new System.Drawing.Point(0, 0);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(320, 39);
            this.label79.TabIndex = 3;
            this.label79.Text = "Symptoms:";
            // 
            // panel57
            // 
            this.panel57.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel57.Controls.Add(this.labelFindings1);
            this.panel57.Controls.Add(this.panel118);
            this.panel57.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel57.Location = new System.Drawing.Point(491, 0);
            this.panel57.Name = "panel57";
            this.panel57.Size = new System.Drawing.Size(597, 134);
            this.panel57.TabIndex = 7;
            // 
            // labelFindings1
            // 
            this.labelFindings1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelFindings1.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelFindings1.Location = new System.Drawing.Point(0, 39);
            this.labelFindings1.Name = "labelFindings1";
            this.labelFindings1.Size = new System.Drawing.Size(595, 93);
            this.labelFindings1.TabIndex = 6;
            this.labelFindings1.Text = "Atrial Fibrillation, with Occasional PVC\'s and many other findings that the Tech " +
    "can add a lot of text \r\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx \r\ny" +
    "yyyyyyyyyyyyyyyyyyyy\r\n\r\n";
            // 
            // panel118
            // 
            this.panel118.Controls.Add(this.labelRhythms1);
            this.panel118.Controls.Add(this.labelFindingsText1);
            this.panel118.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel118.Location = new System.Drawing.Point(0, 0);
            this.panel118.Name = "panel118";
            this.panel118.Size = new System.Drawing.Size(595, 39);
            this.panel118.TabIndex = 2;
            // 
            // labelRhythms1
            // 
            this.labelRhythms1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelRhythms1.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelRhythms1.Location = new System.Drawing.Point(203, 0);
            this.labelRhythms1.Name = "labelRhythms1";
            this.labelRhythms1.Size = new System.Drawing.Size(392, 39);
            this.labelRhythms1.TabIndex = 70;
            this.labelRhythms1.Text = "Rhythms";
            // 
            // labelFindingsText1
            // 
            this.labelFindingsText1.AutoSize = true;
            this.labelFindingsText1.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelFindingsText1.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.labelFindingsText1.Location = new System.Drawing.Point(0, 0);
            this.labelFindingsText1.Name = "labelFindingsText1";
            this.labelFindingsText1.Size = new System.Drawing.Size(203, 29);
            this.labelFindingsText1.TabIndex = 2;
            this.labelFindingsText1.Text = "^QC Findings:";
            // 
            // panel62
            // 
            this.panel62.AutoSize = true;
            this.panel62.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel62.Location = new System.Drawing.Point(491, 0);
            this.panel62.Name = "panel62";
            this.panel62.Size = new System.Drawing.Size(0, 134);
            this.panel62.TabIndex = 6;
            // 
            // panel63
            // 
            this.panel63.AutoSize = true;
            this.panel63.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel63.Location = new System.Drawing.Point(491, 0);
            this.panel63.Name = "panel63";
            this.panel63.Size = new System.Drawing.Size(0, 134);
            this.panel63.TabIndex = 3;
            // 
            // panel64
            // 
            this.panel64.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel64.Controls.Add(this.panel68);
            this.panel64.Controls.Add(this.panel69);
            this.panel64.Controls.Add(this.panel70);
            this.panel64.Controls.Add(this.panel72);
            this.panel64.Controls.Add(this.panel73);
            this.panel64.Controls.Add(this.panel75);
            this.panel64.Controls.Add(this.label101);
            this.panel64.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel64.Location = new System.Drawing.Point(0, 0);
            this.panel64.Name = "panel64";
            this.panel64.Size = new System.Drawing.Size(491, 134);
            this.panel64.TabIndex = 9;
            // 
            // panel68
            // 
            this.panel68.Controls.Add(this.unitQt1);
            this.panel68.Controls.Add(this.unitQrs1);
            this.panel68.Controls.Add(this.unitPr1);
            this.panel68.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel68.Location = new System.Drawing.Point(430, 39);
            this.panel68.Name = "panel68";
            this.panel68.Size = new System.Drawing.Size(60, 93);
            this.panel68.TabIndex = 11;
            // 
            // unitQt1
            // 
            this.unitQt1.Dock = System.Windows.Forms.DockStyle.Top;
            this.unitQt1.Font = new System.Drawing.Font("Verdana", 18F);
            this.unitQt1.Location = new System.Drawing.Point(0, 60);
            this.unitQt1.Name = "unitQt1";
            this.unitQt1.Size = new System.Drawing.Size(60, 30);
            this.unitQt1.TabIndex = 73;
            this.unitQt1.Text = "sec";
            // 
            // unitQrs1
            // 
            this.unitQrs1.Dock = System.Windows.Forms.DockStyle.Top;
            this.unitQrs1.Font = new System.Drawing.Font("Verdana", 18F);
            this.unitQrs1.Location = new System.Drawing.Point(0, 30);
            this.unitQrs1.Name = "unitQrs1";
            this.unitQrs1.Size = new System.Drawing.Size(60, 30);
            this.unitQrs1.TabIndex = 72;
            this.unitQrs1.Text = "sec";
            // 
            // unitPr1
            // 
            this.unitPr1.Dock = System.Windows.Forms.DockStyle.Top;
            this.unitPr1.Font = new System.Drawing.Font("Verdana", 18F);
            this.unitPr1.Location = new System.Drawing.Point(0, 0);
            this.unitPr1.Name = "unitPr1";
            this.unitPr1.Size = new System.Drawing.Size(60, 30);
            this.unitPr1.TabIndex = 71;
            this.unitPr1.Text = "sec";
            // 
            // panel69
            // 
            this.panel69.Controls.Add(this.valueQt1);
            this.panel69.Controls.Add(this.valueQrs1);
            this.panel69.Controls.Add(this.valuePr1);
            this.panel69.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel69.Location = new System.Drawing.Point(347, 39);
            this.panel69.Name = "panel69";
            this.panel69.Size = new System.Drawing.Size(83, 93);
            this.panel69.TabIndex = 10;
            // 
            // valueQt1
            // 
            this.valueQt1.Dock = System.Windows.Forms.DockStyle.Top;
            this.valueQt1.Font = new System.Drawing.Font("Verdana", 18F);
            this.valueQt1.Location = new System.Drawing.Point(0, 60);
            this.valueQt1.Name = "valueQt1";
            this.valueQt1.Size = new System.Drawing.Size(83, 30);
            this.valueQt1.TabIndex = 76;
            this.valueQt1.Text = "9,999";
            this.valueQt1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // valueQrs1
            // 
            this.valueQrs1.Dock = System.Windows.Forms.DockStyle.Top;
            this.valueQrs1.Font = new System.Drawing.Font("Verdana", 18F);
            this.valueQrs1.Location = new System.Drawing.Point(0, 30);
            this.valueQrs1.Name = "valueQrs1";
            this.valueQrs1.Size = new System.Drawing.Size(83, 30);
            this.valueQrs1.TabIndex = 71;
            this.valueQrs1.Text = "9,999";
            this.valueQrs1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // valuePr1
            // 
            this.valuePr1.Dock = System.Windows.Forms.DockStyle.Top;
            this.valuePr1.Font = new System.Drawing.Font("Verdana", 18F);
            this.valuePr1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.valuePr1.Location = new System.Drawing.Point(0, 0);
            this.valuePr1.Name = "valuePr1";
            this.valuePr1.Size = new System.Drawing.Size(83, 30);
            this.valuePr1.TabIndex = 65;
            this.valuePr1.Text = "9,999";
            this.valuePr1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel70
            // 
            this.panel70.Controls.Add(this.headerQt1);
            this.panel70.Controls.Add(this.headerQrs1);
            this.panel70.Controls.Add(this.headerPr1);
            this.panel70.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel70.Location = new System.Drawing.Point(269, 39);
            this.panel70.Name = "panel70";
            this.panel70.Size = new System.Drawing.Size(78, 93);
            this.panel70.TabIndex = 9;
            // 
            // headerQt1
            // 
            this.headerQt1.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerQt1.Font = new System.Drawing.Font("Verdana", 18F);
            this.headerQt1.Location = new System.Drawing.Point(0, 60);
            this.headerQt1.Name = "headerQt1";
            this.headerQt1.Size = new System.Drawing.Size(78, 30);
            this.headerQt1.TabIndex = 76;
            this.headerQt1.Text = "QT:";
            // 
            // headerQrs1
            // 
            this.headerQrs1.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerQrs1.Font = new System.Drawing.Font("Verdana", 18F);
            this.headerQrs1.Location = new System.Drawing.Point(0, 30);
            this.headerQrs1.Name = "headerQrs1";
            this.headerQrs1.Size = new System.Drawing.Size(78, 30);
            this.headerQrs1.TabIndex = 75;
            this.headerQrs1.Text = "QRS:";
            // 
            // headerPr1
            // 
            this.headerPr1.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPr1.Font = new System.Drawing.Font("Verdana", 18F);
            this.headerPr1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.headerPr1.Location = new System.Drawing.Point(0, 0);
            this.headerPr1.Name = "headerPr1";
            this.headerPr1.Size = new System.Drawing.Size(78, 30);
            this.headerPr1.TabIndex = 74;
            this.headerPr1.Text = "PR:";
            // 
            // panel72
            // 
            this.panel72.Controls.Add(this.unitMaxHr1);
            this.panel72.Controls.Add(this.unitMeanHr1);
            this.panel72.Controls.Add(this.unitMinHr1);
            this.panel72.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel72.Location = new System.Drawing.Point(195, 39);
            this.panel72.Name = "panel72";
            this.panel72.Size = new System.Drawing.Size(74, 93);
            this.panel72.TabIndex = 8;
            // 
            // unitMaxHr1
            // 
            this.unitMaxHr1.Dock = System.Windows.Forms.DockStyle.Top;
            this.unitMaxHr1.Font = new System.Drawing.Font("Verdana", 18F);
            this.unitMaxHr1.Location = new System.Drawing.Point(0, 60);
            this.unitMaxHr1.Name = "unitMaxHr1";
            this.unitMaxHr1.Size = new System.Drawing.Size(74, 30);
            this.unitMaxHr1.TabIndex = 64;
            this.unitMaxHr1.Text = "bpm";
            // 
            // unitMeanHr1
            // 
            this.unitMeanHr1.Dock = System.Windows.Forms.DockStyle.Top;
            this.unitMeanHr1.Font = new System.Drawing.Font("Verdana", 18F);
            this.unitMeanHr1.Location = new System.Drawing.Point(0, 30);
            this.unitMeanHr1.Name = "unitMeanHr1";
            this.unitMeanHr1.Size = new System.Drawing.Size(74, 30);
            this.unitMeanHr1.TabIndex = 63;
            this.unitMeanHr1.Text = "bpm";
            // 
            // unitMinHr1
            // 
            this.unitMinHr1.Dock = System.Windows.Forms.DockStyle.Top;
            this.unitMinHr1.Font = new System.Drawing.Font("Verdana", 18F);
            this.unitMinHr1.Location = new System.Drawing.Point(0, 0);
            this.unitMinHr1.Name = "unitMinHr1";
            this.unitMinHr1.Size = new System.Drawing.Size(74, 30);
            this.unitMinHr1.TabIndex = 61;
            this.unitMinHr1.Text = "bpm";
            // 
            // panel73
            // 
            this.panel73.Controls.Add(this.valueMaxHr1);
            this.panel73.Controls.Add(this.valueMeanHr1);
            this.panel73.Controls.Add(this.valueMinHr1);
            this.panel73.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel73.Location = new System.Drawing.Point(130, 39);
            this.panel73.Name = "panel73";
            this.panel73.Size = new System.Drawing.Size(65, 93);
            this.panel73.TabIndex = 7;
            // 
            // valueMaxHr1
            // 
            this.valueMaxHr1.Dock = System.Windows.Forms.DockStyle.Top;
            this.valueMaxHr1.Font = new System.Drawing.Font("Verdana", 18F);
            this.valueMaxHr1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.valueMaxHr1.Location = new System.Drawing.Point(0, 60);
            this.valueMaxHr1.Name = "valueMaxHr1";
            this.valueMaxHr1.Size = new System.Drawing.Size(65, 30);
            this.valueMaxHr1.TabIndex = 61;
            this.valueMaxHr1.Text = "999";
            this.valueMaxHr1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // valueMeanHr1
            // 
            this.valueMeanHr1.Dock = System.Windows.Forms.DockStyle.Top;
            this.valueMeanHr1.Font = new System.Drawing.Font("Verdana", 18F);
            this.valueMeanHr1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.valueMeanHr1.Location = new System.Drawing.Point(0, 30);
            this.valueMeanHr1.Name = "valueMeanHr1";
            this.valueMeanHr1.Size = new System.Drawing.Size(65, 30);
            this.valueMeanHr1.TabIndex = 56;
            this.valueMeanHr1.Text = "999";
            this.valueMeanHr1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // valueMinHr1
            // 
            this.valueMinHr1.Dock = System.Windows.Forms.DockStyle.Top;
            this.valueMinHr1.Font = new System.Drawing.Font("Verdana", 18F);
            this.valueMinHr1.Location = new System.Drawing.Point(0, 0);
            this.valueMinHr1.Name = "valueMinHr1";
            this.valueMinHr1.Size = new System.Drawing.Size(65, 30);
            this.valueMinHr1.TabIndex = 52;
            this.valueMinHr1.Text = "999";
            this.valueMinHr1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel75
            // 
            this.panel75.Controls.Add(this.headerMaxHr1);
            this.panel75.Controls.Add(this.headerMeanHr1);
            this.panel75.Controls.Add(this.headerMinHr1);
            this.panel75.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel75.Location = new System.Drawing.Point(0, 39);
            this.panel75.Name = "panel75";
            this.panel75.Size = new System.Drawing.Size(130, 93);
            this.panel75.TabIndex = 6;
            // 
            // headerMaxHr1
            // 
            this.headerMaxHr1.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerMaxHr1.Font = new System.Drawing.Font("Verdana", 18F);
            this.headerMaxHr1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.headerMaxHr1.Location = new System.Drawing.Point(0, 60);
            this.headerMaxHr1.Name = "headerMaxHr1";
            this.headerMaxHr1.Size = new System.Drawing.Size(130, 30);
            this.headerMaxHr1.TabIndex = 74;
            this.headerMaxHr1.Text = "Max HR:";
            // 
            // headerMeanHr1
            // 
            this.headerMeanHr1.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerMeanHr1.Font = new System.Drawing.Font("Verdana", 18F);
            this.headerMeanHr1.Location = new System.Drawing.Point(0, 30);
            this.headerMeanHr1.Name = "headerMeanHr1";
            this.headerMeanHr1.Size = new System.Drawing.Size(130, 30);
            this.headerMeanHr1.TabIndex = 64;
            this.headerMeanHr1.Text = "Mean HR:";
            // 
            // headerMinHr1
            // 
            this.headerMinHr1.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerMinHr1.Font = new System.Drawing.Font("Verdana", 18F);
            this.headerMinHr1.Location = new System.Drawing.Point(0, 0);
            this.headerMinHr1.Name = "headerMinHr1";
            this.headerMinHr1.Size = new System.Drawing.Size(130, 30);
            this.headerMinHr1.TabIndex = 63;
            this.headerMinHr1.Text = "Min HR:";
            // 
            // label101
            // 
            this.label101.Dock = System.Windows.Forms.DockStyle.Top;
            this.label101.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.label101.Location = new System.Drawing.Point(0, 0);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(489, 39);
            this.label101.TabIndex = 5;
            this.label101.Text = "QRS Measurements:";
            // 
            // panel149
            // 
            this.panel149.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel149.Controls.Add(this.labelSample1AmplFull);
            this.panel149.Controls.Add(this.labelSample1SweepFull);
            this.panel149.Controls.Add(this.labelSample1MidFull);
            this.panel149.Controls.Add(this.labelSample1StartFull);
            this.panel149.Controls.Add(this.labelSample1EndFull);
            this.panel149.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel149.Location = new System.Drawing.Point(0, 555);
            this.panel149.Name = "panel149";
            this.panel149.Size = new System.Drawing.Size(1654, 35);
            this.panel149.TabIndex = 57;
            // 
            // labelSample1AmplFull
            // 
            this.labelSample1AmplFull.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample1AmplFull.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample1AmplFull.Location = new System.Drawing.Point(902, 0);
            this.labelSample1AmplFull.Name = "labelSample1AmplFull";
            this.labelSample1AmplFull.Size = new System.Drawing.Size(290, 33);
            this.labelSample1AmplFull.TabIndex = 2;
            this.labelSample1AmplFull.Text = "10 mm/mV (0.50 mV)";
            this.labelSample1AmplFull.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample1SweepFull
            // 
            this.labelSample1SweepFull.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample1SweepFull.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample1SweepFull.Location = new System.Drawing.Point(1192, 0);
            this.labelSample1SweepFull.Name = "labelSample1SweepFull";
            this.labelSample1SweepFull.Size = new System.Drawing.Size(285, 33);
            this.labelSample1SweepFull.TabIndex = 3;
            this.labelSample1SweepFull.Text = "25 mm/sec (0.20 Sec)";
            this.labelSample1SweepFull.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample1MidFull
            // 
            this.labelSample1MidFull.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSample1MidFull.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample1MidFull.Location = new System.Drawing.Point(748, 0);
            this.labelSample1MidFull.Name = "labelSample1MidFull";
            this.labelSample1MidFull.Size = new System.Drawing.Size(138, 33);
            this.labelSample1MidFull.TabIndex = 1;
            this.labelSample1MidFull.Text = "10:15:00 AM";
            this.labelSample1MidFull.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelSample1MidFull.Visible = false;
            // 
            // labelSample1StartFull
            // 
            this.labelSample1StartFull.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSample1StartFull.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample1StartFull.Location = new System.Drawing.Point(0, 0);
            this.labelSample1StartFull.Name = "labelSample1StartFull";
            this.labelSample1StartFull.Size = new System.Drawing.Size(748, 33);
            this.labelSample1StartFull.TabIndex = 9;
            this.labelSample1StartFull.Text = "^^ 99:99:99 AM 99/99/9099  1d 12:34:12";
            this.labelSample1StartFull.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample1EndFull
            // 
            this.labelSample1EndFull.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample1EndFull.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample1EndFull.Location = new System.Drawing.Point(1477, 0);
            this.labelSample1EndFull.Name = "labelSample1EndFull";
            this.labelSample1EndFull.Size = new System.Drawing.Size(175, 33);
            this.labelSample1EndFull.TabIndex = 8;
            this.labelSample1EndFull.Text = "10:15:30 AM";
            this.labelSample1EndFull.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panelFullStrip
            // 
            this.panelFullStrip.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelFullStrip.Controls.Add(this.panel44);
            this.panelFullStrip.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelFullStrip.Location = new System.Drawing.Point(0, 315);
            this.panelFullStrip.Name = "panelFullStrip";
            this.panelFullStrip.Size = new System.Drawing.Size(1654, 240);
            this.panelFullStrip.TabIndex = 56;
            // 
            // panel44
            // 
            this.panel44.Controls.Add(this.labelLeadFullStripSample1);
            this.panel44.Controls.Add(this.pictureBoxSample1Full);
            this.panel44.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel44.Location = new System.Drawing.Point(0, 0);
            this.panel44.Name = "panel44";
            this.panel44.Size = new System.Drawing.Size(1652, 238);
            this.panel44.TabIndex = 2;
            // 
            // labelLeadFullStripSample1
            // 
            this.labelLeadFullStripSample1.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLeadFullStripSample1.Location = new System.Drawing.Point(2, 13);
            this.labelLeadFullStripSample1.Name = "labelLeadFullStripSample1";
            this.labelLeadFullStripSample1.Size = new System.Drawing.Size(217, 45);
            this.labelLeadFullStripSample1.TabIndex = 47;
            this.labelLeadFullStripSample1.Text = "LEAD I";
            this.labelLeadFullStripSample1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelLeadFullStripSample1.Visible = false;
            // 
            // pictureBoxSample1Full
            // 
            this.pictureBoxSample1Full.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxSample1Full.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxSample1Full.Name = "pictureBoxSample1Full";
            this.pictureBoxSample1Full.Size = new System.Drawing.Size(1652, 238);
            this.pictureBoxSample1Full.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxSample1Full.TabIndex = 0;
            this.pictureBoxSample1Full.TabStop = false;
            // 
            // panel146
            // 
            this.panel146.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel146.Controls.Add(this.panel234);
            this.panel146.Controls.Add(this.panel232);
            this.panel146.Controls.Add(this.panel233);
            this.panel146.Controls.Add(this.panel230);
            this.panel146.Controls.Add(this.labelSample1Start);
            this.panel146.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel146.Location = new System.Drawing.Point(0, 280);
            this.panel146.Name = "panel146";
            this.panel146.Size = new System.Drawing.Size(1654, 35);
            this.panel146.TabIndex = 55;
            // 
            // panel234
            // 
            this.panel234.Controls.Add(this.labelSample1Ampl);
            this.panel234.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel234.Location = new System.Drawing.Point(902, 0);
            this.panel234.Name = "panel234";
            this.panel234.Size = new System.Drawing.Size(290, 33);
            this.panel234.TabIndex = 5;
            // 
            // labelSample1Ampl
            // 
            this.labelSample1Ampl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSample1Ampl.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample1Ampl.Location = new System.Drawing.Point(0, 0);
            this.labelSample1Ampl.Name = "labelSample1Ampl";
            this.labelSample1Ampl.Size = new System.Drawing.Size(290, 33);
            this.labelSample1Ampl.TabIndex = 2;
            this.labelSample1Ampl.Text = "10 mm/mV (0.50 mV)";
            this.labelSample1Ampl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel232
            // 
            this.panel232.Controls.Add(this.labelSample1Sweep);
            this.panel232.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel232.Location = new System.Drawing.Point(1192, 0);
            this.panel232.Name = "panel232";
            this.panel232.Size = new System.Drawing.Size(285, 33);
            this.panel232.TabIndex = 6;
            // 
            // labelSample1Sweep
            // 
            this.labelSample1Sweep.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSample1Sweep.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample1Sweep.Location = new System.Drawing.Point(0, 0);
            this.labelSample1Sweep.Name = "labelSample1Sweep";
            this.labelSample1Sweep.Size = new System.Drawing.Size(285, 33);
            this.labelSample1Sweep.TabIndex = 3;
            this.labelSample1Sweep.Text = "25 mm/sec (0.20 Sec)";
            this.labelSample1Sweep.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel233
            // 
            this.panel233.Controls.Add(this.labelSample1End);
            this.panel233.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel233.Location = new System.Drawing.Point(1477, 0);
            this.panel233.Name = "panel233";
            this.panel233.Size = new System.Drawing.Size(175, 33);
            this.panel233.TabIndex = 4;
            // 
            // labelSample1End
            // 
            this.labelSample1End.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSample1End.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample1End.Location = new System.Drawing.Point(0, 0);
            this.labelSample1End.Name = "labelSample1End";
            this.labelSample1End.Size = new System.Drawing.Size(175, 33);
            this.labelSample1End.TabIndex = 1;
            this.labelSample1End.Text = "10:15:20 AM";
            this.labelSample1End.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel230
            // 
            this.panel230.Controls.Add(this.labelSample1Mid);
            this.panel230.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel230.Location = new System.Drawing.Point(693, 0);
            this.panel230.Name = "panel230";
            this.panel230.Size = new System.Drawing.Size(197, 33);
            this.panel230.TabIndex = 1;
            this.panel230.Visible = false;
            // 
            // labelSample1Mid
            // 
            this.labelSample1Mid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSample1Mid.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample1Mid.Location = new System.Drawing.Point(0, 0);
            this.labelSample1Mid.Name = "labelSample1Mid";
            this.labelSample1Mid.Size = new System.Drawing.Size(197, 33);
            this.labelSample1Mid.TabIndex = 1;
            this.labelSample1Mid.Text = "10:15:15 AM";
            this.labelSample1Mid.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelSample1Start
            // 
            this.labelSample1Start.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSample1Start.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample1Start.Location = new System.Drawing.Point(0, 0);
            this.labelSample1Start.Name = "labelSample1Start";
            this.labelSample1Start.Size = new System.Drawing.Size(693, 33);
            this.labelSample1Start.TabIndex = 7;
            this.labelSample1Start.Text = "^^ 99:99:99 AM 99/99/9099  1d 12:34:12";
            this.labelSample1Start.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panelSample1STrip
            // 
            this.panelSample1STrip.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelSample1STrip.Controls.Add(this.panel37);
            this.panelSample1STrip.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSample1STrip.Location = new System.Drawing.Point(0, 40);
            this.panelSample1STrip.Name = "panelSample1STrip";
            this.panelSample1STrip.Size = new System.Drawing.Size(1654, 240);
            this.panelSample1STrip.TabIndex = 54;
            // 
            // panel37
            // 
            this.panel37.Controls.Add(this.labelLeadZoomStrip1);
            this.panel37.Controls.Add(this.pictureBoxSample1);
            this.panel37.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel37.Location = new System.Drawing.Point(0, 0);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(1652, 238);
            this.panel37.TabIndex = 5;
            // 
            // labelLeadZoomStrip1
            // 
            this.labelLeadZoomStrip1.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLeadZoomStrip1.Location = new System.Drawing.Point(7, 19);
            this.labelLeadZoomStrip1.Name = "labelLeadZoomStrip1";
            this.labelLeadZoomStrip1.Size = new System.Drawing.Size(220, 45);
            this.labelLeadZoomStrip1.TabIndex = 46;
            this.labelLeadZoomStrip1.Text = "LEAD I";
            this.labelLeadZoomStrip1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelLeadZoomStrip1.Visible = false;
            // 
            // pictureBoxSample1
            // 
            this.pictureBoxSample1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxSample1.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxSample1.Name = "pictureBoxSample1";
            this.pictureBoxSample1.Size = new System.Drawing.Size(1652, 238);
            this.pictureBoxSample1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxSample1.TabIndex = 47;
            this.pictureBoxSample1.TabStop = false;
            // 
            // panel142
            // 
            this.panel142.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel142.Controls.Add(this.labelSample1Date);
            this.panel142.Controls.Add(this.label104);
            this.panel142.Controls.Add(this.labelSample1Time);
            this.panel142.Controls.Add(this.panel222);
            this.panel142.Controls.Add(this.panel219);
            this.panel142.Controls.Add(this.label1);
            this.panel142.Controls.Add(this.labelClass1);
            this.panel142.Controls.Add(this.labelSample1Nr);
            this.panel142.Controls.Add(this.labelSample1EventNr);
            this.panel142.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel142.Location = new System.Drawing.Point(0, 0);
            this.panel142.Name = "panel142";
            this.panel142.Size = new System.Drawing.Size(1654, 40);
            this.panel142.TabIndex = 51;
            // 
            // labelSample1Date
            // 
            this.labelSample1Date.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample1Date.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelSample1Date.Location = new System.Drawing.Point(1010, 0);
            this.labelSample1Date.Name = "labelSample1Date";
            this.labelSample1Date.Size = new System.Drawing.Size(200, 38);
            this.labelSample1Date.TabIndex = 14;
            this.labelSample1Date.Text = "11/23/2016";
            this.labelSample1Date.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label104
            // 
            this.label104.Dock = System.Windows.Forms.DockStyle.Right;
            this.label104.Font = new System.Drawing.Font("Verdana", 20F);
            this.label104.Location = new System.Drawing.Point(1210, 0);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(17, 38);
            this.label104.TabIndex = 13;
            this.label104.Text = "-";
            this.label104.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelSample1Time
            // 
            this.labelSample1Time.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample1Time.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelSample1Time.Location = new System.Drawing.Point(1227, 0);
            this.labelSample1Time.Name = "labelSample1Time";
            this.labelSample1Time.Size = new System.Drawing.Size(205, 38);
            this.labelSample1Time.TabIndex = 11;
            this.labelSample1Time.Text = "10:15:10 AM";
            this.labelSample1Time.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel222
            // 
            this.panel222.AutoSize = true;
            this.panel222.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel222.Location = new System.Drawing.Point(537, 0);
            this.panel222.Name = "panel222";
            this.panel222.Size = new System.Drawing.Size(0, 38);
            this.panel222.TabIndex = 5;
            this.panel222.Visible = false;
            // 
            // panel219
            // 
            this.panel219.AutoSize = true;
            this.panel219.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel219.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel219.Location = new System.Drawing.Point(537, 0);
            this.panel219.Name = "panel219";
            this.panel219.Size = new System.Drawing.Size(0, 38);
            this.panel219.TabIndex = 2;
            this.panel219.Visible = false;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Right;
            this.label1.Font = new System.Drawing.Font("Verdana", 20F);
            this.label1.Location = new System.Drawing.Point(1432, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 38);
            this.label1.TabIndex = 16;
            this.label1.Text = "Event:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelClass1
            // 
            this.labelClass1.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelClass1.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold);
            this.labelClass1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelClass1.Location = new System.Drawing.Point(60, 0);
            this.labelClass1.Name = "labelClass1";
            this.labelClass1.Size = new System.Drawing.Size(477, 38);
            this.labelClass1.TabIndex = 15;
            this.labelClass1.Text = "Other";
            this.labelClass1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample1Nr
            // 
            this.labelSample1Nr.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSample1Nr.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold);
            this.labelSample1Nr.Location = new System.Drawing.Point(0, 0);
            this.labelSample1Nr.Name = "labelSample1Nr";
            this.labelSample1Nr.Size = new System.Drawing.Size(60, 38);
            this.labelSample1Nr.TabIndex = 19;
            this.labelSample1Nr.Text = "1";
            this.labelSample1Nr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample1EventNr
            // 
            this.labelSample1EventNr.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample1EventNr.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelSample1EventNr.Location = new System.Drawing.Point(1540, 0);
            this.labelSample1EventNr.Name = "labelSample1EventNr";
            this.labelSample1EventNr.Size = new System.Drawing.Size(112, 38);
            this.labelSample1EventNr.TabIndex = 17;
            this.labelSample1EventNr.Text = "^9999";
            this.labelSample1EventNr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel48
            // 
            this.panel48.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel48.Location = new System.Drawing.Point(0, 1171);
            this.panel48.Name = "panel48";
            this.panel48.Size = new System.Drawing.Size(1654, 6);
            this.panel48.TabIndex = 48;
            // 
            // panelBaseLineMFS
            // 
            this.panelBaseLineMFS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelBaseLineMFS.Controls.Add(this.panelSymptoms3);
            this.panelBaseLineMFS.Controls.Add(this.panelfindings3);
            this.panelBaseLineMFS.Controls.Add(this.panel270);
            this.panelBaseLineMFS.Controls.Add(this.panel273);
            this.panelBaseLineMFS.Controls.Add(this.panelQRSmeasurements3);
            this.panelBaseLineMFS.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelBaseLineMFS.Location = new System.Drawing.Point(0, 1059);
            this.panelBaseLineMFS.Name = "panelBaseLineMFS";
            this.panelBaseLineMFS.Size = new System.Drawing.Size(1654, 112);
            this.panelBaseLineMFS.TabIndex = 47;
            // 
            // panelSymptoms3
            // 
            this.panelSymptoms3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelSymptoms3.Controls.Add(this.labelSymptomsBL);
            this.panelSymptoms3.Controls.Add(this.label50);
            this.panelSymptoms3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelSymptoms3.Location = new System.Drawing.Point(1343, 0);
            this.panelSymptoms3.Name = "panelSymptoms3";
            this.panelSymptoms3.Size = new System.Drawing.Size(309, 110);
            this.panelSymptoms3.TabIndex = 3;
            // 
            // labelSymptomsBL
            // 
            this.labelSymptomsBL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSymptomsBL.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSymptomsBL.Location = new System.Drawing.Point(0, 30);
            this.labelSymptomsBL.Name = "labelSymptomsBL";
            this.labelSymptomsBL.Size = new System.Drawing.Size(307, 78);
            this.labelSymptomsBL.TabIndex = 7;
            this.labelSymptomsBL.Text = "Symptoms bl";
            // 
            // label50
            // 
            this.label50.Dock = System.Windows.Forms.DockStyle.Top;
            this.label50.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.label50.Location = new System.Drawing.Point(0, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(307, 30);
            this.label50.TabIndex = 3;
            this.label50.Text = "Symptoms:";
            // 
            // panelfindings3
            // 
            this.panelfindings3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelfindings3.Controls.Add(this.labelFindingsBL);
            this.panelfindings3.Controls.Add(this.panel176);
            this.panelfindings3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelfindings3.Location = new System.Drawing.Point(763, 0);
            this.panelfindings3.Name = "panelfindings3";
            this.panelfindings3.Size = new System.Drawing.Size(580, 110);
            this.panelfindings3.TabIndex = 7;
            // 
            // labelFindingsBL
            // 
            this.labelFindingsBL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelFindingsBL.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelFindingsBL.Location = new System.Drawing.Point(0, 30);
            this.labelFindingsBL.Name = "labelFindingsBL";
            this.labelFindingsBL.Size = new System.Drawing.Size(578, 78);
            this.labelFindingsBL.TabIndex = 7;
            this.labelFindingsBL.Text = "^Findings BL";
            // 
            // panel176
            // 
            this.panel176.Controls.Add(this.labelRhythmsBL);
            this.panel176.Controls.Add(this.labelFindingsTextBL);
            this.panel176.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel176.Location = new System.Drawing.Point(0, 0);
            this.panel176.Name = "panel176";
            this.panel176.Size = new System.Drawing.Size(578, 30);
            this.panel176.TabIndex = 3;
            // 
            // labelRhythmsBL
            // 
            this.labelRhythmsBL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelRhythmsBL.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelRhythmsBL.Location = new System.Drawing.Point(203, 0);
            this.labelRhythmsBL.Name = "labelRhythmsBL";
            this.labelRhythmsBL.Size = new System.Drawing.Size(375, 30);
            this.labelRhythmsBL.TabIndex = 70;
            this.labelRhythmsBL.Text = "^Rhythms";
            // 
            // labelFindingsTextBL
            // 
            this.labelFindingsTextBL.AutoSize = true;
            this.labelFindingsTextBL.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelFindingsTextBL.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.labelFindingsTextBL.Location = new System.Drawing.Point(0, 0);
            this.labelFindingsTextBL.Name = "labelFindingsTextBL";
            this.labelFindingsTextBL.Size = new System.Drawing.Size(203, 29);
            this.labelFindingsTextBL.TabIndex = 2;
            this.labelFindingsTextBL.Text = "^QC Findings:";
            // 
            // panel270
            // 
            this.panel270.AutoSize = true;
            this.panel270.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel270.Location = new System.Drawing.Point(763, 0);
            this.panel270.Name = "panel270";
            this.panel270.Size = new System.Drawing.Size(0, 110);
            this.panel270.TabIndex = 6;
            // 
            // panel273
            // 
            this.panel273.AutoSize = true;
            this.panel273.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel273.Location = new System.Drawing.Point(763, 0);
            this.panel273.Name = "panel273";
            this.panel273.Size = new System.Drawing.Size(0, 110);
            this.panel273.TabIndex = 3;
            // 
            // panelQRSmeasurements3
            // 
            this.panelQRSmeasurements3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelQRSmeasurements3.Controls.Add(this.panel47);
            this.panelQRSmeasurements3.Controls.Add(this.panel43);
            this.panelQRSmeasurements3.Controls.Add(this.panel42);
            this.panelQRSmeasurements3.Controls.Add(this.panel4);
            this.panelQRSmeasurements3.Controls.Add(this.panel20);
            this.panelQRSmeasurements3.Controls.Add(this.panel38);
            this.panelQRSmeasurements3.Controls.Add(this.panel39);
            this.panelQRSmeasurements3.Controls.Add(this.panel40);
            this.panelQRSmeasurements3.Controls.Add(this.panel41);
            this.panelQRSmeasurements3.Controls.Add(this.label78);
            this.panelQRSmeasurements3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelQRSmeasurements3.Location = new System.Drawing.Point(0, 0);
            this.panelQRSmeasurements3.Name = "panelQRSmeasurements3";
            this.panelQRSmeasurements3.Size = new System.Drawing.Size(763, 110);
            this.panelQRSmeasurements3.TabIndex = 9;
            // 
            // panel47
            // 
            this.panel47.Controls.Add(this.unitQtBL);
            this.panel47.Controls.Add(this.unitQrsBL);
            this.panel47.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel47.Location = new System.Drawing.Point(711, 36);
            this.panel47.Name = "panel47";
            this.panel47.Size = new System.Drawing.Size(55, 72);
            this.panel47.TabIndex = 14;
            // 
            // unitQtBL
            // 
            this.unitQtBL.Dock = System.Windows.Forms.DockStyle.Top;
            this.unitQtBL.Font = new System.Drawing.Font("Verdana", 18F);
            this.unitQtBL.Location = new System.Drawing.Point(0, 36);
            this.unitQtBL.Name = "unitQtBL";
            this.unitQtBL.Size = new System.Drawing.Size(55, 43);
            this.unitQtBL.TabIndex = 70;
            this.unitQtBL.Text = "sec";
            // 
            // unitQrsBL
            // 
            this.unitQrsBL.Dock = System.Windows.Forms.DockStyle.Top;
            this.unitQrsBL.Font = new System.Drawing.Font("Verdana", 18F);
            this.unitQrsBL.Location = new System.Drawing.Point(0, 0);
            this.unitQrsBL.Name = "unitQrsBL";
            this.unitQrsBL.Size = new System.Drawing.Size(55, 36);
            this.unitQrsBL.TabIndex = 69;
            this.unitQrsBL.Text = "sec";
            // 
            // panel43
            // 
            this.panel43.Controls.Add(this.valueQtBL);
            this.panel43.Controls.Add(this.valueQrsBL);
            this.panel43.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel43.Location = new System.Drawing.Point(620, 36);
            this.panel43.Name = "panel43";
            this.panel43.Size = new System.Drawing.Size(91, 72);
            this.panel43.TabIndex = 13;
            // 
            // valueQtBL
            // 
            this.valueQtBL.Dock = System.Windows.Forms.DockStyle.Top;
            this.valueQtBL.Font = new System.Drawing.Font("Verdana", 18F);
            this.valueQtBL.Location = new System.Drawing.Point(0, 36);
            this.valueQtBL.Name = "valueQtBL";
            this.valueQtBL.Size = new System.Drawing.Size(91, 43);
            this.valueQtBL.TabIndex = 59;
            this.valueQtBL.Text = "9,999";
            this.valueQtBL.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // valueQrsBL
            // 
            this.valueQrsBL.Dock = System.Windows.Forms.DockStyle.Top;
            this.valueQrsBL.Font = new System.Drawing.Font("Verdana", 18F);
            this.valueQrsBL.Location = new System.Drawing.Point(0, 0);
            this.valueQrsBL.Name = "valueQrsBL";
            this.valueQrsBL.Size = new System.Drawing.Size(91, 36);
            this.valueQrsBL.TabIndex = 57;
            this.valueQrsBL.Text = "9,999";
            this.valueQrsBL.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel42
            // 
            this.panel42.Controls.Add(this.headerQtBL);
            this.panel42.Controls.Add(this.headerQrsBL);
            this.panel42.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel42.Location = new System.Drawing.Point(541, 36);
            this.panel42.Name = "panel42";
            this.panel42.Size = new System.Drawing.Size(79, 72);
            this.panel42.TabIndex = 12;
            // 
            // headerQtBL
            // 
            this.headerQtBL.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerQtBL.Font = new System.Drawing.Font("Verdana", 18F);
            this.headerQtBL.Location = new System.Drawing.Point(0, 36);
            this.headerQtBL.Name = "headerQtBL";
            this.headerQtBL.Size = new System.Drawing.Size(79, 43);
            this.headerQtBL.TabIndex = 70;
            this.headerQtBL.Text = "QT:";
            // 
            // headerQrsBL
            // 
            this.headerQrsBL.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerQrsBL.Font = new System.Drawing.Font("Verdana", 18F);
            this.headerQrsBL.Location = new System.Drawing.Point(0, 0);
            this.headerQrsBL.Name = "headerQrsBL";
            this.headerQrsBL.Size = new System.Drawing.Size(79, 36);
            this.headerQrsBL.TabIndex = 69;
            this.headerQrsBL.Text = "QRS:";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.unitPrBL);
            this.panel4.Controls.Add(this.unitMaxHrBL);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(472, 36);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(69, 72);
            this.panel4.TabIndex = 11;
            // 
            // unitPrBL
            // 
            this.unitPrBL.Dock = System.Windows.Forms.DockStyle.Top;
            this.unitPrBL.Font = new System.Drawing.Font("Verdana", 18F);
            this.unitPrBL.Location = new System.Drawing.Point(0, 36);
            this.unitPrBL.Name = "unitPrBL";
            this.unitPrBL.Size = new System.Drawing.Size(69, 43);
            this.unitPrBL.TabIndex = 70;
            this.unitPrBL.Text = "sec";
            // 
            // unitMaxHrBL
            // 
            this.unitMaxHrBL.Dock = System.Windows.Forms.DockStyle.Top;
            this.unitMaxHrBL.Font = new System.Drawing.Font("Verdana", 18F);
            this.unitMaxHrBL.Location = new System.Drawing.Point(0, 0);
            this.unitMaxHrBL.Name = "unitMaxHrBL";
            this.unitMaxHrBL.Size = new System.Drawing.Size(69, 36);
            this.unitMaxHrBL.TabIndex = 69;
            this.unitMaxHrBL.Text = "bpm";
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.valuePrBL);
            this.panel20.Controls.Add(this.valueMaxHrBL);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel20.Location = new System.Drawing.Point(386, 36);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(86, 72);
            this.panel20.TabIndex = 10;
            // 
            // valuePrBL
            // 
            this.valuePrBL.Dock = System.Windows.Forms.DockStyle.Top;
            this.valuePrBL.Font = new System.Drawing.Font("Verdana", 18F);
            this.valuePrBL.Location = new System.Drawing.Point(0, 36);
            this.valuePrBL.Name = "valuePrBL";
            this.valuePrBL.Size = new System.Drawing.Size(86, 43);
            this.valuePrBL.TabIndex = 55;
            this.valuePrBL.Text = "9,999";
            this.valuePrBL.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // valueMaxHrBL
            // 
            this.valueMaxHrBL.Dock = System.Windows.Forms.DockStyle.Top;
            this.valueMaxHrBL.Font = new System.Drawing.Font("Verdana", 18F);
            this.valueMaxHrBL.Location = new System.Drawing.Point(0, 0);
            this.valueMaxHrBL.Name = "valueMaxHrBL";
            this.valueMaxHrBL.Size = new System.Drawing.Size(86, 36);
            this.valueMaxHrBL.TabIndex = 53;
            this.valueMaxHrBL.Text = "999";
            this.valueMaxHrBL.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel38
            // 
            this.panel38.Controls.Add(this.headerPrBL);
            this.panel38.Controls.Add(this.headerMaxHrBL);
            this.panel38.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel38.Location = new System.Drawing.Point(269, 36);
            this.panel38.Name = "panel38";
            this.panel38.Size = new System.Drawing.Size(117, 72);
            this.panel38.TabIndex = 9;
            // 
            // headerPrBL
            // 
            this.headerPrBL.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPrBL.Font = new System.Drawing.Font("Verdana", 18F);
            this.headerPrBL.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.headerPrBL.Location = new System.Drawing.Point(0, 36);
            this.headerPrBL.Name = "headerPrBL";
            this.headerPrBL.Size = new System.Drawing.Size(117, 43);
            this.headerPrBL.TabIndex = 72;
            this.headerPrBL.Text = "PR:";
            // 
            // headerMaxHrBL
            // 
            this.headerMaxHrBL.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerMaxHrBL.Font = new System.Drawing.Font("Verdana", 18F);
            this.headerMaxHrBL.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.headerMaxHrBL.Location = new System.Drawing.Point(0, 0);
            this.headerMaxHrBL.Name = "headerMaxHrBL";
            this.headerMaxHrBL.Size = new System.Drawing.Size(117, 36);
            this.headerMaxHrBL.TabIndex = 67;
            this.headerMaxHrBL.Text = "Max HR:";
            // 
            // panel39
            // 
            this.panel39.Controls.Add(this.unitMeanHrBL);
            this.panel39.Controls.Add(this.unitMinHrBL);
            this.panel39.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel39.Location = new System.Drawing.Point(195, 36);
            this.panel39.Name = "panel39";
            this.panel39.Size = new System.Drawing.Size(74, 72);
            this.panel39.TabIndex = 8;
            // 
            // unitMeanHrBL
            // 
            this.unitMeanHrBL.Dock = System.Windows.Forms.DockStyle.Top;
            this.unitMeanHrBL.Font = new System.Drawing.Font("Verdana", 18F);
            this.unitMeanHrBL.Location = new System.Drawing.Point(0, 36);
            this.unitMeanHrBL.Name = "unitMeanHrBL";
            this.unitMeanHrBL.Size = new System.Drawing.Size(74, 43);
            this.unitMeanHrBL.TabIndex = 62;
            this.unitMeanHrBL.Text = "bpm";
            // 
            // unitMinHrBL
            // 
            this.unitMinHrBL.Dock = System.Windows.Forms.DockStyle.Top;
            this.unitMinHrBL.Font = new System.Drawing.Font("Verdana", 18F);
            this.unitMinHrBL.Location = new System.Drawing.Point(0, 0);
            this.unitMinHrBL.Name = "unitMinHrBL";
            this.unitMinHrBL.Size = new System.Drawing.Size(74, 36);
            this.unitMinHrBL.TabIndex = 61;
            this.unitMinHrBL.Text = "bpm";
            // 
            // panel40
            // 
            this.panel40.Controls.Add(this.valueMeanHrBL);
            this.panel40.Controls.Add(this.valueMinHrBL);
            this.panel40.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel40.Location = new System.Drawing.Point(130, 36);
            this.panel40.Name = "panel40";
            this.panel40.Size = new System.Drawing.Size(65, 72);
            this.panel40.TabIndex = 7;
            // 
            // valueMeanHrBL
            // 
            this.valueMeanHrBL.Dock = System.Windows.Forms.DockStyle.Top;
            this.valueMeanHrBL.Font = new System.Drawing.Font("Verdana", 18F);
            this.valueMeanHrBL.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.valueMeanHrBL.Location = new System.Drawing.Point(0, 36);
            this.valueMeanHrBL.Name = "valueMeanHrBL";
            this.valueMeanHrBL.Size = new System.Drawing.Size(65, 43);
            this.valueMeanHrBL.TabIndex = 51;
            this.valueMeanHrBL.Text = "999";
            this.valueMeanHrBL.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // valueMinHrBL
            // 
            this.valueMinHrBL.Dock = System.Windows.Forms.DockStyle.Top;
            this.valueMinHrBL.Font = new System.Drawing.Font("Verdana", 18F);
            this.valueMinHrBL.Location = new System.Drawing.Point(0, 0);
            this.valueMinHrBL.Name = "valueMinHrBL";
            this.valueMinHrBL.Size = new System.Drawing.Size(65, 36);
            this.valueMinHrBL.TabIndex = 49;
            this.valueMinHrBL.Text = "999";
            this.valueMinHrBL.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel41
            // 
            this.panel41.Controls.Add(this.headerMeanHrBL);
            this.panel41.Controls.Add(this.headerMinHrBL);
            this.panel41.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel41.Location = new System.Drawing.Point(0, 36);
            this.panel41.Name = "panel41";
            this.panel41.Size = new System.Drawing.Size(130, 72);
            this.panel41.TabIndex = 6;
            // 
            // headerMeanHrBL
            // 
            this.headerMeanHrBL.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerMeanHrBL.Font = new System.Drawing.Font("Verdana", 18F);
            this.headerMeanHrBL.Location = new System.Drawing.Point(0, 36);
            this.headerMeanHrBL.Name = "headerMeanHrBL";
            this.headerMeanHrBL.Size = new System.Drawing.Size(130, 43);
            this.headerMeanHrBL.TabIndex = 62;
            this.headerMeanHrBL.Text = "Mean HR:";
            // 
            // headerMinHrBL
            // 
            this.headerMinHrBL.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerMinHrBL.Font = new System.Drawing.Font("Verdana", 18F);
            this.headerMinHrBL.Location = new System.Drawing.Point(0, 0);
            this.headerMinHrBL.Name = "headerMinHrBL";
            this.headerMinHrBL.Size = new System.Drawing.Size(130, 36);
            this.headerMinHrBL.TabIndex = 58;
            this.headerMinHrBL.Text = "Min HR:";
            // 
            // label78
            // 
            this.label78.Dock = System.Windows.Forms.DockStyle.Top;
            this.label78.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.label78.Location = new System.Drawing.Point(0, 0);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(761, 36);
            this.label78.TabIndex = 5;
            this.label78.Text = "QRS Measurements:";
            // 
            // panelBlSweepAmpIndicator
            // 
            this.panelBlSweepAmpIndicator.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelBlSweepAmpIndicator.Controls.Add(this.panelSweepSpeed);
            this.panelBlSweepAmpIndicator.Controls.Add(this.labelSampleBlMid);
            this.panelBlSweepAmpIndicator.Controls.Add(this.labelSampleBlStart);
            this.panelBlSweepAmpIndicator.Controls.Add(this.panelAmpPanel);
            this.panelBlSweepAmpIndicator.Controls.Add(this.labelSampleBlEnd);
            this.panelBlSweepAmpIndicator.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelBlSweepAmpIndicator.Location = new System.Drawing.Point(0, 1024);
            this.panelBlSweepAmpIndicator.Name = "panelBlSweepAmpIndicator";
            this.panelBlSweepAmpIndicator.Size = new System.Drawing.Size(1654, 35);
            this.panelBlSweepAmpIndicator.TabIndex = 46;
            // 
            // panelSweepSpeed
            // 
            this.panelSweepSpeed.Controls.Add(this.labelSampleBlSweep);
            this.panelSweepSpeed.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelSweepSpeed.Location = new System.Drawing.Point(923, 0);
            this.panelSweepSpeed.Name = "panelSweepSpeed";
            this.panelSweepSpeed.Size = new System.Drawing.Size(290, 33);
            this.panelSweepSpeed.TabIndex = 3;
            // 
            // labelSampleBlSweep
            // 
            this.labelSampleBlSweep.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSampleBlSweep.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSampleBlSweep.Location = new System.Drawing.Point(0, 0);
            this.labelSampleBlSweep.Name = "labelSampleBlSweep";
            this.labelSampleBlSweep.Size = new System.Drawing.Size(290, 33);
            this.labelSampleBlSweep.TabIndex = 0;
            this.labelSampleBlSweep.Text = "25 mm/sec (0.20 Sec)";
            this.labelSampleBlSweep.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSampleBlMid
            // 
            this.labelSampleBlMid.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSampleBlMid.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSampleBlMid.Location = new System.Drawing.Point(748, 0);
            this.labelSampleBlMid.Name = "labelSampleBlMid";
            this.labelSampleBlMid.Size = new System.Drawing.Size(138, 33);
            this.labelSampleBlMid.TabIndex = 6;
            this.labelSampleBlMid.Text = "10:15:15 AM";
            this.labelSampleBlMid.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelSampleBlMid.Visible = false;
            // 
            // labelSampleBlStart
            // 
            this.labelSampleBlStart.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSampleBlStart.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSampleBlStart.Location = new System.Drawing.Point(0, 0);
            this.labelSampleBlStart.Name = "labelSampleBlStart";
            this.labelSampleBlStart.Size = new System.Drawing.Size(748, 33);
            this.labelSampleBlStart.TabIndex = 4;
            this.labelSampleBlStart.Text = "^^ 99:99:99 AM 99/99/9099  1d 12:34:12";
            this.labelSampleBlStart.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panelAmpPanel
            // 
            this.panelAmpPanel.Controls.Add(this.labelSampleBlAmpl);
            this.panelAmpPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelAmpPanel.Location = new System.Drawing.Point(1213, 0);
            this.panelAmpPanel.Name = "panelAmpPanel";
            this.panelAmpPanel.Size = new System.Drawing.Size(285, 33);
            this.panelAmpPanel.TabIndex = 2;
            // 
            // labelSampleBlAmpl
            // 
            this.labelSampleBlAmpl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSampleBlAmpl.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSampleBlAmpl.Location = new System.Drawing.Point(0, 0);
            this.labelSampleBlAmpl.Name = "labelSampleBlAmpl";
            this.labelSampleBlAmpl.Size = new System.Drawing.Size(285, 33);
            this.labelSampleBlAmpl.TabIndex = 1;
            this.labelSampleBlAmpl.Text = "10 mm/mV (0.50 mV)";
            this.labelSampleBlAmpl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSampleBlEnd
            // 
            this.labelSampleBlEnd.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSampleBlEnd.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSampleBlEnd.Location = new System.Drawing.Point(1498, 0);
            this.labelSampleBlEnd.Name = "labelSampleBlEnd";
            this.labelSampleBlEnd.Size = new System.Drawing.Size(154, 33);
            this.labelSampleBlEnd.TabIndex = 5;
            this.labelSampleBlEnd.Text = "10:15:20 AM";
            this.labelSampleBlEnd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panelECGBaselineSTrip
            // 
            this.panelECGBaselineSTrip.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelECGBaselineSTrip.Controls.Add(this.pictureBoxBaseline);
            this.panelECGBaselineSTrip.Controls.Add(this.labelLeadID);
            this.panelECGBaselineSTrip.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelECGBaselineSTrip.Location = new System.Drawing.Point(0, 784);
            this.panelECGBaselineSTrip.Name = "panelECGBaselineSTrip";
            this.panelECGBaselineSTrip.Size = new System.Drawing.Size(1654, 240);
            this.panelECGBaselineSTrip.TabIndex = 45;
            // 
            // pictureBoxBaseline
            // 
            this.pictureBoxBaseline.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxBaseline.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxBaseline.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxBaseline.InitialImage")));
            this.pictureBoxBaseline.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxBaseline.Name = "pictureBoxBaseline";
            this.pictureBoxBaseline.Size = new System.Drawing.Size(1652, 238);
            this.pictureBoxBaseline.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxBaseline.TabIndex = 0;
            this.pictureBoxBaseline.TabStop = false;
            // 
            // labelLeadID
            // 
            this.labelLeadID.Font = new System.Drawing.Font("Verdana", 28F);
            this.labelLeadID.Location = new System.Drawing.Point(119, 206);
            this.labelLeadID.Name = "labelLeadID";
            this.labelLeadID.Size = new System.Drawing.Size(198, 47);
            this.labelLeadID.TabIndex = 46;
            this.labelLeadID.Text = "LEAD III";
            this.labelLeadID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelLeadID.Visible = false;
            // 
            // panelBaseLineHeader
            // 
            this.panelBaseLineHeader.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelBaseLineHeader.Controls.Add(this.labelBaselineDate);
            this.panelBaseLineHeader.Controls.Add(this.label134);
            this.panelBaseLineHeader.Controls.Add(this.labelBaselineTime);
            this.panelBaseLineHeader.Controls.Add(this.labelBaseRefHeader);
            this.panelBaseLineHeader.Controls.Add(this.label49);
            this.panelBaseLineHeader.Controls.Add(this.labelEventNrBL);
            this.panelBaseLineHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelBaseLineHeader.Location = new System.Drawing.Point(0, 746);
            this.panelBaseLineHeader.Name = "panelBaseLineHeader";
            this.panelBaseLineHeader.Size = new System.Drawing.Size(1654, 38);
            this.panelBaseLineHeader.TabIndex = 43;
            // 
            // labelBaselineDate
            // 
            this.labelBaselineDate.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelBaselineDate.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelBaselineDate.Location = new System.Drawing.Point(1010, 0);
            this.labelBaselineDate.Name = "labelBaselineDate";
            this.labelBaselineDate.Size = new System.Drawing.Size(200, 36);
            this.labelBaselineDate.TabIndex = 27;
            this.labelBaselineDate.Text = "11/23/2016";
            this.labelBaselineDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label134
            // 
            this.label134.Dock = System.Windows.Forms.DockStyle.Right;
            this.label134.Font = new System.Drawing.Font("Verdana", 20F);
            this.label134.Location = new System.Drawing.Point(1210, 0);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(17, 36);
            this.label134.TabIndex = 26;
            this.label134.Text = "-";
            this.label134.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelBaselineTime
            // 
            this.labelBaselineTime.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelBaselineTime.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelBaselineTime.Location = new System.Drawing.Point(1227, 0);
            this.labelBaselineTime.Name = "labelBaselineTime";
            this.labelBaselineTime.Size = new System.Drawing.Size(205, 36);
            this.labelBaselineTime.TabIndex = 25;
            this.labelBaselineTime.Text = "10:15:10 AM";
            this.labelBaselineTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelBaseRefHeader
            // 
            this.labelBaseRefHeader.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelBaseRefHeader.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold);
            this.labelBaseRefHeader.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.labelBaseRefHeader.Location = new System.Drawing.Point(0, 0);
            this.labelBaseRefHeader.Name = "labelBaseRefHeader";
            this.labelBaseRefHeader.Size = new System.Drawing.Size(463, 36);
            this.labelBaseRefHeader.TabIndex = 23;
            this.labelBaseRefHeader.Text = "Baseline Reference";
            this.labelBaseRefHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label49
            // 
            this.label49.Dock = System.Windows.Forms.DockStyle.Right;
            this.label49.Font = new System.Drawing.Font("Verdana", 20F);
            this.label49.Location = new System.Drawing.Point(1432, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(108, 36);
            this.label49.TabIndex = 29;
            this.label49.Text = "Event:";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelEventNrBL
            // 
            this.labelEventNrBL.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelEventNrBL.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelEventNrBL.Location = new System.Drawing.Point(1540, 0);
            this.labelEventNrBL.Name = "labelEventNrBL";
            this.labelEventNrBL.Size = new System.Drawing.Size(112, 36);
            this.labelEventNrBL.TabIndex = 32;
            this.labelEventNrBL.Text = "^9999";
            this.labelEventNrBL.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel35
            // 
            this.panel35.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel35.Location = new System.Drawing.Point(0, 740);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(1654, 6);
            this.panel35.TabIndex = 42;
            // 
            // panelEventStats
            // 
            this.panelEventStats.Controls.Add(this.panel31);
            this.panelEventStats.Controls.Add(this.panel3);
            this.panelEventStats.Controls.Add(this.panel28);
            this.panelEventStats.Controls.Add(this.panel30);
            this.panelEventStats.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelEventStats.Location = new System.Drawing.Point(0, 543);
            this.panelEventStats.Name = "panelEventStats";
            this.panelEventStats.Size = new System.Drawing.Size(1654, 197);
            this.panelEventStats.TabIndex = 41;
            // 
            // panel31
            // 
            this.panel31.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel31.Location = new System.Drawing.Point(1654, 0);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(12, 197);
            this.panel31.TabIndex = 13;
            this.panel31.Visible = false;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.panel111);
            this.panel3.Controls.Add(this.panel96);
            this.panel3.Controls.Add(this.panel84);
            this.panel3.Controls.Add(this.panel46);
            this.panel3.Controls.Add(this.panel45);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(1187, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(467, 197);
            this.panel3.TabIndex = 12;
            // 
            // panel111
            // 
            this.panel111.Controls.Add(this.labelQTmeasureSI);
            this.panel111.Controls.Add(this.labelQRSmeasureSI);
            this.panel111.Controls.Add(this.labelPRmeasureSI);
            this.panel111.Controls.Add(this.labelRatemeasureSI);
            this.panel111.Controls.Add(this.label105);
            this.panel111.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel111.Location = new System.Drawing.Point(379, 0);
            this.panel111.Name = "panel111";
            this.panel111.Size = new System.Drawing.Size(75, 195);
            this.panel111.TabIndex = 12;
            // 
            // labelQTmeasureSI
            // 
            this.labelQTmeasureSI.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelQTmeasureSI.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelQTmeasureSI.Location = new System.Drawing.Point(0, 152);
            this.labelQTmeasureSI.Name = "labelQTmeasureSI";
            this.labelQTmeasureSI.Size = new System.Drawing.Size(75, 41);
            this.labelQTmeasureSI.TabIndex = 17;
            this.labelQTmeasureSI.Text = "sec";
            this.labelQTmeasureSI.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelQRSmeasureSI
            // 
            this.labelQRSmeasureSI.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelQRSmeasureSI.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelQRSmeasureSI.Location = new System.Drawing.Point(0, 116);
            this.labelQRSmeasureSI.Name = "labelQRSmeasureSI";
            this.labelQRSmeasureSI.Size = new System.Drawing.Size(75, 36);
            this.labelQRSmeasureSI.TabIndex = 16;
            this.labelQRSmeasureSI.Text = "sec";
            this.labelQRSmeasureSI.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelPRmeasureSI
            // 
            this.labelPRmeasureSI.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPRmeasureSI.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelPRmeasureSI.Location = new System.Drawing.Point(0, 74);
            this.labelPRmeasureSI.Name = "labelPRmeasureSI";
            this.labelPRmeasureSI.Size = new System.Drawing.Size(75, 42);
            this.labelPRmeasureSI.TabIndex = 15;
            this.labelPRmeasureSI.Text = "sec";
            this.labelPRmeasureSI.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelRatemeasureSI
            // 
            this.labelRatemeasureSI.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelRatemeasureSI.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelRatemeasureSI.Location = new System.Drawing.Point(0, 36);
            this.labelRatemeasureSI.Name = "labelRatemeasureSI";
            this.labelRatemeasureSI.Size = new System.Drawing.Size(75, 38);
            this.labelRatemeasureSI.TabIndex = 14;
            this.labelRatemeasureSI.Text = "bpm";
            this.labelRatemeasureSI.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label105
            // 
            this.label105.Dock = System.Windows.Forms.DockStyle.Top;
            this.label105.Font = new System.Drawing.Font("Verdana", 24F);
            this.label105.Location = new System.Drawing.Point(0, 0);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(75, 36);
            this.label105.TabIndex = 13;
            this.label105.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel96
            // 
            this.panel96.Controls.Add(this.labelQTAvg);
            this.panel96.Controls.Add(this.labelQRSAvg);
            this.panel96.Controls.Add(this.labelPRAvg);
            this.panel96.Controls.Add(this.labelRateAVG);
            this.panel96.Controls.Add(this.labelAVGMeasurement);
            this.panel96.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel96.Location = new System.Drawing.Point(286, 0);
            this.panel96.Name = "panel96";
            this.panel96.Size = new System.Drawing.Size(93, 195);
            this.panel96.TabIndex = 11;
            // 
            // labelQTAvg
            // 
            this.labelQTAvg.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelQTAvg.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelQTAvg.Location = new System.Drawing.Point(0, 152);
            this.labelQTAvg.Name = "labelQTAvg";
            this.labelQTAvg.Size = new System.Drawing.Size(93, 41);
            this.labelQTAvg.TabIndex = 11;
            this.labelQTAvg.Text = "9,999";
            this.labelQTAvg.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelQRSAvg
            // 
            this.labelQRSAvg.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelQRSAvg.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelQRSAvg.Location = new System.Drawing.Point(0, 116);
            this.labelQRSAvg.Name = "labelQRSAvg";
            this.labelQRSAvg.Size = new System.Drawing.Size(93, 36);
            this.labelQRSAvg.TabIndex = 10;
            this.labelQRSAvg.Text = "9,999";
            this.labelQRSAvg.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelPRAvg
            // 
            this.labelPRAvg.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPRAvg.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelPRAvg.Location = new System.Drawing.Point(0, 74);
            this.labelPRAvg.Name = "labelPRAvg";
            this.labelPRAvg.Size = new System.Drawing.Size(93, 42);
            this.labelPRAvg.TabIndex = 9;
            this.labelPRAvg.Text = "9,999";
            this.labelPRAvg.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelRateAVG
            // 
            this.labelRateAVG.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelRateAVG.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelRateAVG.Location = new System.Drawing.Point(0, 36);
            this.labelRateAVG.Name = "labelRateAVG";
            this.labelRateAVG.Size = new System.Drawing.Size(93, 38);
            this.labelRateAVG.TabIndex = 8;
            this.labelRateAVG.Text = "999";
            this.labelRateAVG.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelAVGMeasurement
            // 
            this.labelAVGMeasurement.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelAVGMeasurement.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelAVGMeasurement.Location = new System.Drawing.Point(0, 0);
            this.labelAVGMeasurement.Name = "labelAVGMeasurement";
            this.labelAVGMeasurement.Size = new System.Drawing.Size(93, 36);
            this.labelAVGMeasurement.TabIndex = 2;
            this.labelAVGMeasurement.Text = "Avg";
            this.labelAVGMeasurement.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel84
            // 
            this.panel84.Controls.Add(this.labelQTMax);
            this.panel84.Controls.Add(this.labelQRSMax);
            this.panel84.Controls.Add(this.labelPRMax);
            this.panel84.Controls.Add(this.labelRateMax);
            this.panel84.Controls.Add(this.labelMaxMeasurement);
            this.panel84.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel84.Location = new System.Drawing.Point(193, 0);
            this.panel84.Name = "panel84";
            this.panel84.Size = new System.Drawing.Size(93, 195);
            this.panel84.TabIndex = 10;
            // 
            // labelQTMax
            // 
            this.labelQTMax.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelQTMax.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelQTMax.Location = new System.Drawing.Point(0, 152);
            this.labelQTMax.Name = "labelQTMax";
            this.labelQTMax.Size = new System.Drawing.Size(93, 41);
            this.labelQTMax.TabIndex = 11;
            this.labelQTMax.Text = "9,999";
            this.labelQTMax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelQRSMax
            // 
            this.labelQRSMax.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelQRSMax.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelQRSMax.Location = new System.Drawing.Point(0, 116);
            this.labelQRSMax.Name = "labelQRSMax";
            this.labelQRSMax.Size = new System.Drawing.Size(93, 36);
            this.labelQRSMax.TabIndex = 10;
            this.labelQRSMax.Text = "9,999";
            this.labelQRSMax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelPRMax
            // 
            this.labelPRMax.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPRMax.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelPRMax.Location = new System.Drawing.Point(0, 74);
            this.labelPRMax.Name = "labelPRMax";
            this.labelPRMax.Size = new System.Drawing.Size(93, 42);
            this.labelPRMax.TabIndex = 9;
            this.labelPRMax.Text = "9,999";
            this.labelPRMax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelRateMax
            // 
            this.labelRateMax.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelRateMax.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelRateMax.Location = new System.Drawing.Point(0, 36);
            this.labelRateMax.Name = "labelRateMax";
            this.labelRateMax.Size = new System.Drawing.Size(93, 38);
            this.labelRateMax.TabIndex = 8;
            this.labelRateMax.Text = "999";
            this.labelRateMax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelMaxMeasurement
            // 
            this.labelMaxMeasurement.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelMaxMeasurement.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelMaxMeasurement.Location = new System.Drawing.Point(0, 0);
            this.labelMaxMeasurement.Name = "labelMaxMeasurement";
            this.labelMaxMeasurement.Size = new System.Drawing.Size(93, 36);
            this.labelMaxMeasurement.TabIndex = 2;
            this.labelMaxMeasurement.Text = "Max";
            this.labelMaxMeasurement.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel46
            // 
            this.panel46.Controls.Add(this.labelQTmin);
            this.panel46.Controls.Add(this.labelQRSmin);
            this.panel46.Controls.Add(this.labelPRmin);
            this.panel46.Controls.Add(this.labelRateMin);
            this.panel46.Controls.Add(this.labelMinMeasurement);
            this.panel46.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel46.Location = new System.Drawing.Point(100, 0);
            this.panel46.Name = "panel46";
            this.panel46.Size = new System.Drawing.Size(93, 195);
            this.panel46.TabIndex = 9;
            // 
            // labelQTmin
            // 
            this.labelQTmin.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelQTmin.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelQTmin.Location = new System.Drawing.Point(0, 152);
            this.labelQTmin.Name = "labelQTmin";
            this.labelQTmin.Size = new System.Drawing.Size(93, 41);
            this.labelQTmin.TabIndex = 10;
            this.labelQTmin.Text = "9,999";
            this.labelQTmin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelQRSmin
            // 
            this.labelQRSmin.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelQRSmin.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelQRSmin.Location = new System.Drawing.Point(0, 116);
            this.labelQRSmin.Name = "labelQRSmin";
            this.labelQRSmin.Size = new System.Drawing.Size(93, 36);
            this.labelQRSmin.TabIndex = 9;
            this.labelQRSmin.Text = "9,999";
            this.labelQRSmin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelPRmin
            // 
            this.labelPRmin.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPRmin.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelPRmin.Location = new System.Drawing.Point(0, 74);
            this.labelPRmin.Name = "labelPRmin";
            this.labelPRmin.Size = new System.Drawing.Size(93, 42);
            this.labelPRmin.TabIndex = 8;
            this.labelPRmin.Text = "9,999";
            this.labelPRmin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelRateMin
            // 
            this.labelRateMin.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelRateMin.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelRateMin.Location = new System.Drawing.Point(0, 36);
            this.labelRateMin.Name = "labelRateMin";
            this.labelRateMin.Size = new System.Drawing.Size(93, 38);
            this.labelRateMin.TabIndex = 7;
            this.labelRateMin.Text = "999";
            this.labelRateMin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelMinMeasurement
            // 
            this.labelMinMeasurement.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelMinMeasurement.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelMinMeasurement.Location = new System.Drawing.Point(0, 0);
            this.labelMinMeasurement.Name = "labelMinMeasurement";
            this.labelMinMeasurement.Size = new System.Drawing.Size(93, 36);
            this.labelMinMeasurement.TabIndex = 1;
            this.labelMinMeasurement.Text = "Min";
            this.labelMinMeasurement.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel45
            // 
            this.panel45.Controls.Add(this.labelQTMeasure);
            this.panel45.Controls.Add(this.labelQRSMeasure);
            this.panel45.Controls.Add(this.labelPRMeasure);
            this.panel45.Controls.Add(this.labelRateMeasure);
            this.panel45.Controls.Add(this.label103);
            this.panel45.Controls.Add(this.label87);
            this.panel45.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel45.Location = new System.Drawing.Point(0, 0);
            this.panel45.Name = "panel45";
            this.panel45.Size = new System.Drawing.Size(100, 195);
            this.panel45.TabIndex = 8;
            // 
            // labelQTMeasure
            // 
            this.labelQTMeasure.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelQTMeasure.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelQTMeasure.Location = new System.Drawing.Point(0, 152);
            this.labelQTMeasure.Name = "labelQTMeasure";
            this.labelQTMeasure.Size = new System.Drawing.Size(100, 41);
            this.labelQTMeasure.TabIndex = 6;
            this.labelQTMeasure.Text = "QT:";
            this.labelQTMeasure.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelQRSMeasure
            // 
            this.labelQRSMeasure.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelQRSMeasure.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelQRSMeasure.Location = new System.Drawing.Point(0, 116);
            this.labelQRSMeasure.Name = "labelQRSMeasure";
            this.labelQRSMeasure.Size = new System.Drawing.Size(100, 36);
            this.labelQRSMeasure.TabIndex = 5;
            this.labelQRSMeasure.Text = "QRS:";
            this.labelQRSMeasure.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelPRMeasure
            // 
            this.labelPRMeasure.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPRMeasure.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelPRMeasure.Location = new System.Drawing.Point(0, 74);
            this.labelPRMeasure.Name = "labelPRMeasure";
            this.labelPRMeasure.Size = new System.Drawing.Size(100, 42);
            this.labelPRMeasure.TabIndex = 4;
            this.labelPRMeasure.Text = "PR:";
            this.labelPRMeasure.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelRateMeasure
            // 
            this.labelRateMeasure.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelRateMeasure.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelRateMeasure.Location = new System.Drawing.Point(0, 36);
            this.labelRateMeasure.Name = "labelRateMeasure";
            this.labelRateMeasure.Size = new System.Drawing.Size(100, 38);
            this.labelRateMeasure.TabIndex = 3;
            this.labelRateMeasure.Text = "Rate:";
            this.labelRateMeasure.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label103
            // 
            this.label103.Dock = System.Windows.Forms.DockStyle.Top;
            this.label103.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label103.Location = new System.Drawing.Point(0, 36);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(100, 0);
            this.label103.TabIndex = 2;
            this.label103.Text = "Rate:";
            this.label103.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label87
            // 
            this.label87.Dock = System.Windows.Forms.DockStyle.Top;
            this.label87.Font = new System.Drawing.Font("Verdana", 24F);
            this.label87.Location = new System.Drawing.Point(0, 0);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(100, 36);
            this.label87.TabIndex = 1;
            this.label87.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel28
            // 
            this.panel28.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel28.Location = new System.Drawing.Point(1123, 0);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(64, 197);
            this.panel28.TabIndex = 6;
            // 
            // panel30
            // 
            this.panel30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel30.Controls.Add(this.labelICDcode);
            this.panel30.Controls.Add(this.labelDiagnosisHeader);
            this.panel30.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel30.Location = new System.Drawing.Point(0, 0);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(1123, 197);
            this.panel30.TabIndex = 3;
            // 
            // labelICDcode
            // 
            this.labelICDcode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelICDcode.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelICDcode.Location = new System.Drawing.Point(0, 36);
            this.labelICDcode.Name = "labelICDcode";
            this.labelICDcode.Size = new System.Drawing.Size(1121, 159);
            this.labelICDcode.TabIndex = 4;
            this.labelICDcode.Text = "ICD10\r\nB\r\nC\r\nD\r\n";
            // 
            // labelDiagnosisHeader
            // 
            this.labelDiagnosisHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDiagnosisHeader.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelDiagnosisHeader.Location = new System.Drawing.Point(0, 0);
            this.labelDiagnosisHeader.Name = "labelDiagnosisHeader";
            this.labelDiagnosisHeader.Size = new System.Drawing.Size(1121, 36);
            this.labelDiagnosisHeader.TabIndex = 1;
            this.labelDiagnosisHeader.Text = "Indication:";
            // 
            // panel78
            // 
            this.panel78.Controls.Add(this.panel110);
            this.panel78.Controls.Add(this.panel109);
            this.panel78.Controls.Add(this.panel108);
            this.panel78.Controls.Add(this.panel107);
            this.panel78.Controls.Add(this.panel106);
            this.panel78.Controls.Add(this.panel105);
            this.panel78.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel78.Location = new System.Drawing.Point(0, 2222);
            this.panel78.Name = "panel78";
            this.panel78.Size = new System.Drawing.Size(1654, 76);
            this.panel78.TabIndex = 33;
            // 
            // panel110
            // 
            this.panel110.Controls.Add(this.labelReportSignDate);
            this.panel110.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel110.Location = new System.Drawing.Point(1412, 0);
            this.panel110.Name = "panel110";
            this.panel110.Size = new System.Drawing.Size(239, 76);
            this.panel110.TabIndex = 7;
            // 
            // labelReportSignDate
            // 
            this.labelReportSignDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelReportSignDate.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelReportSignDate.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.labelReportSignDate.Location = new System.Drawing.Point(0, 0);
            this.labelReportSignDate.Name = "labelReportSignDate";
            this.labelReportSignDate.Size = new System.Drawing.Size(239, 76);
            this.labelReportSignDate.TabIndex = 0;
            this.labelReportSignDate.Text = "^08/23/2016";
            this.labelReportSignDate.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // panel109
            // 
            this.panel109.Controls.Add(this.labelDateReportSign);
            this.panel109.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel109.Location = new System.Drawing.Point(1313, 0);
            this.panel109.Name = "panel109";
            this.panel109.Size = new System.Drawing.Size(99, 76);
            this.panel109.TabIndex = 6;
            // 
            // labelDateReportSign
            // 
            this.labelDateReportSign.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDateReportSign.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelDateReportSign.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.labelDateReportSign.Location = new System.Drawing.Point(0, 0);
            this.labelDateReportSign.Name = "labelDateReportSign";
            this.labelDateReportSign.Size = new System.Drawing.Size(99, 76);
            this.labelDateReportSign.TabIndex = 1;
            this.labelDateReportSign.Text = "Date:";
            this.labelDateReportSign.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // panel108
            // 
            this.panel108.Controls.Add(this.labelPhysSignLine);
            this.panel108.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel108.Location = new System.Drawing.Point(980, 0);
            this.panel108.Name = "panel108";
            this.panel108.Size = new System.Drawing.Size(333, 76);
            this.panel108.TabIndex = 5;
            // 
            // labelPhysSignLine
            // 
            this.labelPhysSignLine.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPhysSignLine.Font = new System.Drawing.Font("Comic Sans MS", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPhysSignLine.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.labelPhysSignLine.Location = new System.Drawing.Point(0, 0);
            this.labelPhysSignLine.Name = "labelPhysSignLine";
            this.labelPhysSignLine.Size = new System.Drawing.Size(333, 76);
            this.labelPhysSignLine.TabIndex = 1;
            this.labelPhysSignLine.Text = "dr. Simon V126\r\n---xxx----";
            this.labelPhysSignLine.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.labelPhysSignLine.Click += new System.EventHandler(this.labelPhysSignLine_Click);
            // 
            // panel107
            // 
            this.panel107.Controls.Add(this.labelPhysSign);
            this.panel107.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel107.Location = new System.Drawing.Point(647, 0);
            this.panel107.Name = "panel107";
            this.panel107.Size = new System.Drawing.Size(333, 76);
            this.panel107.TabIndex = 4;
            // 
            // labelPhysSign
            // 
            this.labelPhysSign.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPhysSign.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelPhysSign.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.labelPhysSign.Location = new System.Drawing.Point(0, 0);
            this.labelPhysSign.Name = "labelPhysSign";
            this.labelPhysSign.Size = new System.Drawing.Size(333, 76);
            this.labelPhysSign.TabIndex = 1;
            this.labelPhysSign.Text = "QC\r\nPhysician signature:";
            this.labelPhysSign.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // panel106
            // 
            this.panel106.Controls.Add(this.labelPhysPrintName);
            this.panel106.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel106.Location = new System.Drawing.Point(271, 0);
            this.panel106.Name = "panel106";
            this.panel106.Size = new System.Drawing.Size(376, 76);
            this.panel106.TabIndex = 3;
            // 
            // labelPhysPrintName
            // 
            this.labelPhysPrintName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPhysPrintName.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelPhysPrintName.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.labelPhysPrintName.Location = new System.Drawing.Point(0, 0);
            this.labelPhysPrintName.Name = "labelPhysPrintName";
            this.labelPhysPrintName.Size = new System.Drawing.Size(376, 76);
            this.labelPhysPrintName.TabIndex = 1;
            this.labelPhysPrintName.Text = "^J. Smithsonian\r\nLead Technician";
            this.labelPhysPrintName.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // panel105
            // 
            this.panel105.Controls.Add(this.labelPhysNameReportPrint);
            this.panel105.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel105.Location = new System.Drawing.Point(0, 0);
            this.panel105.Name = "panel105";
            this.panel105.Size = new System.Drawing.Size(271, 76);
            this.panel105.TabIndex = 2;
            // 
            // labelPhysNameReportPrint
            // 
            this.labelPhysNameReportPrint.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPhysNameReportPrint.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelPhysNameReportPrint.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.labelPhysNameReportPrint.Location = new System.Drawing.Point(0, 0);
            this.labelPhysNameReportPrint.Name = "labelPhysNameReportPrint";
            this.labelPhysNameReportPrint.Size = new System.Drawing.Size(271, 76);
            this.labelPhysNameReportPrint.TabIndex = 2;
            this.labelPhysNameReportPrint.Text = "QC\r\nPhysician name:";
            this.labelPhysNameReportPrint.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // panel77
            // 
            this.panel77.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel77.Location = new System.Drawing.Point(0, 2298);
            this.panel77.Name = "panel77";
            this.panel77.Size = new System.Drawing.Size(1654, 4);
            this.panel77.TabIndex = 32;
            // 
            // panel6
            // 
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 537);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1654, 6);
            this.panel6.TabIndex = 15;
            // 
            // panelHorSepPatDet
            // 
            this.panelHorSepPatDet.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHorSepPatDet.Location = new System.Drawing.Point(0, 536);
            this.panelHorSepPatDet.Name = "panelHorSepPatDet";
            this.panelHorSepPatDet.Size = new System.Drawing.Size(1654, 1);
            this.panelHorSepPatDet.TabIndex = 3;
            // 
            // panel51
            // 
            this.panel51.Controls.Add(this.label14);
            this.panel51.Controls.Add(this.labelStudyNr);
            this.panel51.Controls.Add(this.labelPrintDate1Bottom);
            this.panel51.Controls.Add(this.label8);
            this.panel51.Controls.Add(this.panel510);
            this.panel51.Controls.Add(this.panel509);
            this.panel51.Controls.Add(this.panel508);
            this.panel51.Controls.Add(this.panel507);
            this.panel51.Controls.Add(this.label7);
            this.panel51.Controls.Add(this.label3);
            this.panel51.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel51.Location = new System.Drawing.Point(0, 2302);
            this.panel51.Name = "panel51";
            this.panel51.Size = new System.Drawing.Size(1654, 37);
            this.panel51.TabIndex = 34;
            // 
            // label14
            // 
            this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label14.Font = new System.Drawing.Font("Verdana", 16F);
            this.label14.Location = new System.Drawing.Point(701, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(475, 37);
            this.label14.TabIndex = 8;
            this.label14.Text = "2018 © Techmedic International B.V.";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelStudyNr
            // 
            this.labelStudyNr.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelStudyNr.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.labelStudyNr.Location = new System.Drawing.Point(1176, 0);
            this.labelStudyNr.Name = "labelStudyNr";
            this.labelStudyNr.Size = new System.Drawing.Size(179, 37);
            this.labelStudyNr.TabIndex = 41;
            this.labelStudyNr.Text = "1212212";
            this.labelStudyNr.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelPrintDate1Bottom
            // 
            this.labelPrintDate1Bottom.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPrintDate1Bottom.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.labelPrintDate1Bottom.Location = new System.Drawing.Point(130, 0);
            this.labelPrintDate1Bottom.Name = "labelPrintDate1Bottom";
            this.labelPrintDate1Bottom.Size = new System.Drawing.Size(571, 37);
            this.labelPrintDate1Bottom.TabIndex = 7;
            this.labelPrintDate1Bottom.Text = "12/27/2016 12:23:34 AM+1200 v12345";
            this.labelPrintDate1Bottom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.Dock = System.Windows.Forms.DockStyle.Left;
            this.label8.Font = new System.Drawing.Font("Verdana", 16F);
            this.label8.Location = new System.Drawing.Point(15, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(115, 37);
            this.label8.TabIndex = 6;
            this.label8.Text = "Printed:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel510
            // 
            this.panel510.Controls.Add(this.labelPage);
            this.panel510.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel510.Location = new System.Drawing.Point(1355, 0);
            this.panel510.Name = "panel510";
            this.panel510.Size = new System.Drawing.Size(74, 37);
            this.panel510.TabIndex = 5;
            // 
            // labelPage
            // 
            this.labelPage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPage.Font = new System.Drawing.Font("Verdana", 16F);
            this.labelPage.Location = new System.Drawing.Point(0, 0);
            this.labelPage.Name = "labelPage";
            this.labelPage.Size = new System.Drawing.Size(74, 37);
            this.labelPage.TabIndex = 0;
            this.labelPage.Text = "Page";
            this.labelPage.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel509
            // 
            this.panel509.Controls.Add(this.labelPage1);
            this.panel509.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel509.Location = new System.Drawing.Point(1429, 0);
            this.panel509.Name = "panel509";
            this.panel509.Size = new System.Drawing.Size(66, 37);
            this.panel509.TabIndex = 4;
            // 
            // labelPage1
            // 
            this.labelPage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPage1.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.labelPage1.Location = new System.Drawing.Point(0, 0);
            this.labelPage1.Name = "labelPage1";
            this.labelPage1.Size = new System.Drawing.Size(66, 37);
            this.labelPage1.TabIndex = 0;
            this.labelPage1.Text = "1";
            this.labelPage1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelPage1.Click += new System.EventHandler(this.label53_Click);
            // 
            // panel508
            // 
            this.panel508.Controls.Add(this.labelPageOf);
            this.panel508.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel508.Location = new System.Drawing.Point(1495, 0);
            this.panel508.Name = "panel508";
            this.panel508.Size = new System.Drawing.Size(37, 37);
            this.panel508.TabIndex = 3;
            // 
            // labelPageOf
            // 
            this.labelPageOf.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPageOf.Font = new System.Drawing.Font("Verdana", 16F);
            this.labelPageOf.Location = new System.Drawing.Point(0, 0);
            this.labelPageOf.Name = "labelPageOf";
            this.labelPageOf.Size = new System.Drawing.Size(37, 37);
            this.labelPageOf.TabIndex = 0;
            this.labelPageOf.Text = "of";
            this.labelPageOf.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel507
            // 
            this.panel507.Controls.Add(this.labelPage1OfX);
            this.panel507.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel507.Location = new System.Drawing.Point(1532, 0);
            this.panel507.Name = "panel507";
            this.panel507.Size = new System.Drawing.Size(98, 37);
            this.panel507.TabIndex = 2;
            // 
            // labelPage1OfX
            // 
            this.labelPage1OfX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPage1OfX.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.labelPage1OfX.Location = new System.Drawing.Point(0, 0);
            this.labelPage1OfX.Name = "labelPage1OfX";
            this.labelPage1OfX.Size = new System.Drawing.Size(98, 37);
            this.labelPage1OfX.TabIndex = 1;
            this.labelPage1OfX.Text = "20p1";
            this.labelPage1OfX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label7.Dock = System.Windows.Forms.DockStyle.Right;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label7.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label7.Location = new System.Drawing.Point(1630, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(24, 37);
            this.label7.TabIndex = 1;
            this.label7.Text = "R1";
            this.label7.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label7.Visible = false;
            // 
            // label3
            // 
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Dock = System.Windows.Forms.DockStyle.Left;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label3.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 37);
            this.label3.TabIndex = 0;
            this.label3.Text = "L1";
            this.label3.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label3.Visible = false;
            // 
            // panelPhysInfo
            // 
            this.panelPhysInfo.Controls.Add(this.panel67);
            this.panelPhysInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelPhysInfo.Location = new System.Drawing.Point(0, 428);
            this.panelPhysInfo.Name = "panelPhysInfo";
            this.panelPhysInfo.Size = new System.Drawing.Size(1654, 108);
            this.panelPhysInfo.TabIndex = 5;
            // 
            // panel67
            // 
            this.panel67.Controls.Add(this.panel258);
            this.panel67.Controls.Add(this.panel256);
            this.panel67.Controls.Add(this.panel10);
            this.panel67.Controls.Add(this.panelVert9);
            this.panel67.Controls.Add(this.panel252);
            this.panel67.Controls.Add(this.panel250);
            this.panel67.Controls.Add(this.panelVert8);
            this.panel67.Controls.Add(this.panel244);
            this.panel67.Controls.Add(this.panel242);
            this.panel67.Controls.Add(this.panelVert6);
            this.panel67.Controls.Add(this.panel2);
            this.panel67.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel67.Location = new System.Drawing.Point(0, 0);
            this.panel67.Name = "panel67";
            this.panel67.Size = new System.Drawing.Size(1654, 105);
            this.panel67.TabIndex = 8;
            // 
            // panel258
            // 
            this.panel258.Controls.Add(this.labelClientName);
            this.panel258.Controls.Add(this.labelClientTel);
            this.panel258.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel258.Location = new System.Drawing.Point(1289, 0);
            this.panel258.Name = "panel258";
            this.panel258.Size = new System.Drawing.Size(365, 105);
            this.panel258.TabIndex = 20;
            // 
            // labelClientName
            // 
            this.labelClientName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelClientName.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelClientName.Location = new System.Drawing.Point(0, 0);
            this.labelClientName.Name = "labelClientName";
            this.labelClientName.Size = new System.Drawing.Size(365, 70);
            this.labelClientName.TabIndex = 32;
            this.labelClientName.Text = "New York Hospital";
            // 
            // labelClientTel
            // 
            this.labelClientTel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelClientTel.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelClientTel.Location = new System.Drawing.Point(0, 70);
            this.labelClientTel.Name = "labelClientTel";
            this.labelClientTel.Size = new System.Drawing.Size(365, 35);
            this.labelClientTel.TabIndex = 42;
            this.labelClientTel.Text = "800-217-0520";
            // 
            // panel256
            // 
            this.panel256.Controls.Add(this.label46);
            this.panel256.Controls.Add(this.label44);
            this.panel256.Controls.Add(this.labelClientHeader);
            this.panel256.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel256.Location = new System.Drawing.Point(1187, 0);
            this.panel256.Name = "panel256";
            this.panel256.Size = new System.Drawing.Size(102, 105);
            this.panel256.TabIndex = 18;
            // 
            // label46
            // 
            this.label46.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label46.Font = new System.Drawing.Font("Verdana", 16F);
            this.label46.Location = new System.Drawing.Point(0, 70);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(102, 35);
            this.label46.TabIndex = 42;
            this.label46.Text = "Phone:";
            // 
            // label44
            // 
            this.label44.Dock = System.Windows.Forms.DockStyle.Top;
            this.label44.Font = new System.Drawing.Font("Verdana", 27F);
            this.label44.Location = new System.Drawing.Point(0, 35);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(102, 35);
            this.label44.TabIndex = 40;
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelClientHeader
            // 
            this.labelClientHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelClientHeader.Font = new System.Drawing.Font("Verdana", 16F);
            this.labelClientHeader.Location = new System.Drawing.Point(0, 0);
            this.labelClientHeader.Name = "labelClientHeader";
            this.labelClientHeader.Size = new System.Drawing.Size(102, 35);
            this.labelClientHeader.TabIndex = 20;
            this.labelClientHeader.Text = "Name:";
            // 
            // panel10
            // 
            this.panel10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel10.Controls.Add(this.panel29);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel10.Location = new System.Drawing.Point(1123, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(64, 105);
            this.panel10.TabIndex = 26;
            // 
            // panel29
            // 
            this.panel29.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel29.BackgroundImage")));
            this.panel29.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel29.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel29.Location = new System.Drawing.Point(0, 0);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(64, 120);
            this.panel29.TabIndex = 27;
            // 
            // panelVert9
            // 
            this.panelVert9.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelVert9.Location = new System.Drawing.Point(1121, 0);
            this.panelVert9.Name = "panelVert9";
            this.panelVert9.Size = new System.Drawing.Size(2, 105);
            this.panelVert9.TabIndex = 15;
            // 
            // panel252
            // 
            this.panel252.Controls.Add(this.labelRefPhysicianName);
            this.panel252.Controls.Add(this.labelRefPhysicianTel);
            this.panel252.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel252.Location = new System.Drawing.Point(706, 0);
            this.panel252.Name = "panel252";
            this.panel252.Size = new System.Drawing.Size(415, 105);
            this.panel252.TabIndex = 14;
            // 
            // labelRefPhysicianName
            // 
            this.labelRefPhysicianName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelRefPhysicianName.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelRefPhysicianName.Location = new System.Drawing.Point(0, 0);
            this.labelRefPhysicianName.Name = "labelRefPhysicianName";
            this.labelRefPhysicianName.Size = new System.Drawing.Size(415, 70);
            this.labelRefPhysicianName.TabIndex = 29;
            this.labelRefPhysicianName.Text = "Dr. John Smithsonian";
            // 
            // labelRefPhysicianTel
            // 
            this.labelRefPhysicianTel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelRefPhysicianTel.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelRefPhysicianTel.Location = new System.Drawing.Point(0, 70);
            this.labelRefPhysicianTel.Name = "labelRefPhysicianTel";
            this.labelRefPhysicianTel.Size = new System.Drawing.Size(415, 35);
            this.labelRefPhysicianTel.TabIndex = 41;
            this.labelRefPhysicianTel.Text = "800-217-0520";
            // 
            // panel250
            // 
            this.panel250.Controls.Add(this.label43);
            this.panel250.Controls.Add(this.label40);
            this.panel250.Controls.Add(this.labelRefPhysHeader);
            this.panel250.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel250.Location = new System.Drawing.Point(573, 0);
            this.panel250.Name = "panel250";
            this.panel250.Size = new System.Drawing.Size(133, 105);
            this.panel250.TabIndex = 12;
            // 
            // label43
            // 
            this.label43.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label43.Font = new System.Drawing.Font("Verdana", 16F);
            this.label43.Location = new System.Drawing.Point(0, 70);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(133, 35);
            this.label43.TabIndex = 41;
            this.label43.Text = "Phone:";
            // 
            // label40
            // 
            this.label40.Dock = System.Windows.Forms.DockStyle.Top;
            this.label40.Font = new System.Drawing.Font("Verdana", 27F);
            this.label40.Location = new System.Drawing.Point(0, 35);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(133, 36);
            this.label40.TabIndex = 39;
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelRefPhysHeader
            // 
            this.labelRefPhysHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelRefPhysHeader.Font = new System.Drawing.Font("Verdana", 16F);
            this.labelRefPhysHeader.Location = new System.Drawing.Point(0, 0);
            this.labelRefPhysHeader.Name = "labelRefPhysHeader";
            this.labelRefPhysHeader.Size = new System.Drawing.Size(133, 35);
            this.labelRefPhysHeader.TabIndex = 19;
            this.labelRefPhysHeader.Text = "Ref Phys.:";
            // 
            // panelVert8
            // 
            this.panelVert8.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelVert8.Location = new System.Drawing.Point(571, 0);
            this.panelVert8.Name = "panelVert8";
            this.panelVert8.Size = new System.Drawing.Size(2, 105);
            this.panelVert8.TabIndex = 11;
            // 
            // panel244
            // 
            this.panel244.Controls.Add(this.labelPhysicianName);
            this.panel244.Controls.Add(this.labelPhysicianTel);
            this.panel244.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel244.Location = new System.Drawing.Point(161, 0);
            this.panel244.Name = "panel244";
            this.panel244.Size = new System.Drawing.Size(410, 105);
            this.panel244.TabIndex = 10;
            // 
            // labelPhysicianName
            // 
            this.labelPhysicianName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPhysicianName.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelPhysicianName.Location = new System.Drawing.Point(0, 0);
            this.labelPhysicianName.Name = "labelPhysicianName";
            this.labelPhysicianName.Size = new System.Drawing.Size(410, 70);
            this.labelPhysicianName.TabIndex = 25;
            this.labelPhysicianName.Text = "Dr. John Smithsonian hjsgdhfhag";
            // 
            // labelPhysicianTel
            // 
            this.labelPhysicianTel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelPhysicianTel.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelPhysicianTel.Location = new System.Drawing.Point(0, 70);
            this.labelPhysicianTel.Name = "labelPhysicianTel";
            this.labelPhysicianTel.Size = new System.Drawing.Size(410, 35);
            this.labelPhysicianTel.TabIndex = 40;
            this.labelPhysicianTel.Text = "!800-217-0520";
            // 
            // panel242
            // 
            this.panel242.Controls.Add(this.label41);
            this.panel242.Controls.Add(this.label38);
            this.panel242.Controls.Add(this.labelPhysHeader);
            this.panel242.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel242.Location = new System.Drawing.Point(68, 0);
            this.panel242.Name = "panel242";
            this.panel242.Size = new System.Drawing.Size(93, 105);
            this.panel242.TabIndex = 8;
            // 
            // label41
            // 
            this.label41.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label41.Font = new System.Drawing.Font("Verdana", 16F);
            this.label41.Location = new System.Drawing.Point(0, 70);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(93, 35);
            this.label41.TabIndex = 40;
            this.label41.Text = "Phone:";
            // 
            // label38
            // 
            this.label38.Dock = System.Windows.Forms.DockStyle.Top;
            this.label38.Font = new System.Drawing.Font("Verdana", 27F);
            this.label38.Location = new System.Drawing.Point(0, 35);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(93, 35);
            this.label38.TabIndex = 38;
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPhysHeader
            // 
            this.labelPhysHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPhysHeader.Font = new System.Drawing.Font("Verdana", 16F);
            this.labelPhysHeader.Location = new System.Drawing.Point(0, 0);
            this.labelPhysHeader.Name = "labelPhysHeader";
            this.labelPhysHeader.Size = new System.Drawing.Size(93, 35);
            this.labelPhysHeader.TabIndex = 17;
            this.labelPhysHeader.Text = "Phys.:";
            // 
            // panelVert6
            // 
            this.panelVert6.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelVert6.Location = new System.Drawing.Point(66, 0);
            this.panelVert6.Name = "panelVert6";
            this.panelVert6.Size = new System.Drawing.Size(2, 105);
            this.panelVert6.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Controls.Add(this.panel25);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(66, 105);
            this.panel2.TabIndex = 25;
            // 
            // panel25
            // 
            this.panel25.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel25.BackgroundImage")));
            this.panel25.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel25.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel25.Location = new System.Drawing.Point(0, 0);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(66, 120);
            this.panel25.TabIndex = 26;
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.panel18);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel16.Location = new System.Drawing.Point(0, 426);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(1654, 2);
            this.panel16.TabIndex = 35;
            // 
            // panel18
            // 
            this.panel18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel18.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel18.Location = new System.Drawing.Point(0, 1);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(1654, 1);
            this.panel18.TabIndex = 9;
            // 
            // panelSecPatBar
            // 
            this.panelSecPatBar.Controls.Add(this.panel8);
            this.panelSecPatBar.Controls.Add(this.panel7);
            this.panelSecPatBar.Controls.Add(this.panel5);
            this.panelSecPatBar.Controls.Add(this.panel34);
            this.panelSecPatBar.Controls.Add(this.panel17);
            this.panelSecPatBar.Controls.Add(this.panel9);
            this.panelSecPatBar.Controls.Add(this.panel1);
            this.panelSecPatBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSecPatBar.Location = new System.Drawing.Point(0, 215);
            this.panelSecPatBar.Name = "panelSecPatBar";
            this.panelSecPatBar.Size = new System.Drawing.Size(1654, 211);
            this.panelSecPatBar.TabIndex = 5;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.labelQC);
            this.panel8.Controls.Add(this.labelTechnicianMeasurement);
            this.panel8.Controls.Add(this.labelRoom);
            this.panel8.Controls.Add(this.labelPatID);
            this.panel8.Controls.Add(this.labelSerialNr);
            this.panel8.Controls.Add(this.labelStartDate);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(1418, 2);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(236, 209);
            this.panel8.TabIndex = 6;
            // 
            // labelQC
            // 
            this.labelQC.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelQC.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelQC.Location = new System.Drawing.Point(0, 175);
            this.labelQC.Name = "labelQC";
            this.labelQC.Size = new System.Drawing.Size(236, 32);
            this.labelQC.TabIndex = 49;
            this.labelQC.Text = "^QC";
            // 
            // labelTechnicianMeasurement
            // 
            this.labelTechnicianMeasurement.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelTechnicianMeasurement.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelTechnicianMeasurement.Location = new System.Drawing.Point(0, 140);
            this.labelTechnicianMeasurement.Name = "labelTechnicianMeasurement";
            this.labelTechnicianMeasurement.Size = new System.Drawing.Size(236, 35);
            this.labelTechnicianMeasurement.TabIndex = 47;
            this.labelTechnicianMeasurement.Text = "SHJ";
            // 
            // labelRoom
            // 
            this.labelRoom.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelRoom.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelRoom.Location = new System.Drawing.Point(0, 105);
            this.labelRoom.Name = "labelRoom";
            this.labelRoom.Size = new System.Drawing.Size(236, 35);
            this.labelRoom.TabIndex = 48;
            this.labelRoom.Text = "210 - bed 4";
            // 
            // labelPatID
            // 
            this.labelPatID.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPatID.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelPatID.Location = new System.Drawing.Point(0, 70);
            this.labelPatID.Name = "labelPatID";
            this.labelPatID.Size = new System.Drawing.Size(236, 35);
            this.labelPatID.TabIndex = 46;
            this.labelPatID.Text = "12341234567";
            // 
            // labelSerialNr
            // 
            this.labelSerialNr.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelSerialNr.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelSerialNr.Location = new System.Drawing.Point(0, 35);
            this.labelSerialNr.Name = "labelSerialNr";
            this.labelSerialNr.Size = new System.Drawing.Size(236, 35);
            this.labelSerialNr.TabIndex = 41;
            this.labelSerialNr.Text = "AA102044403";
            // 
            // labelStartDate
            // 
            this.labelStartDate.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStartDate.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelStartDate.Location = new System.Drawing.Point(0, 0);
            this.labelStartDate.Name = "labelStartDate";
            this.labelStartDate.Size = new System.Drawing.Size(236, 35);
            this.labelStartDate.TabIndex = 40;
            this.labelStartDate.Text = "10/23/2016";
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.labelTechText);
            this.panel7.Controls.Add(this.label30);
            this.panel7.Controls.Add(this.labelPatIDHeader);
            this.panel7.Controls.Add(this.labelSerialNrHeader);
            this.panel7.Controls.Add(this.labelStartDateHeader);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel7.Location = new System.Drawing.Point(1279, 2);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(139, 209);
            this.panel7.TabIndex = 5;
            // 
            // labelTechText
            // 
            this.labelTechText.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelTechText.Font = new System.Drawing.Font("Verdana", 16F);
            this.labelTechText.Location = new System.Drawing.Point(0, 140);
            this.labelTechText.Name = "labelTechText";
            this.labelTechText.Size = new System.Drawing.Size(139, 35);
            this.labelTechText.TabIndex = 42;
            this.labelTechText.Text = "Tech:";
            // 
            // label30
            // 
            this.label30.Dock = System.Windows.Forms.DockStyle.Top;
            this.label30.Font = new System.Drawing.Font("Verdana", 16F);
            this.label30.Location = new System.Drawing.Point(0, 105);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(139, 35);
            this.label30.TabIndex = 43;
            this.label30.Text = "Room:";
            // 
            // labelPatIDHeader
            // 
            this.labelPatIDHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPatIDHeader.Font = new System.Drawing.Font("Verdana", 16F);
            this.labelPatIDHeader.Location = new System.Drawing.Point(0, 70);
            this.labelPatIDHeader.Name = "labelPatIDHeader";
            this.labelPatIDHeader.Size = new System.Drawing.Size(139, 35);
            this.labelPatIDHeader.TabIndex = 41;
            this.labelPatIDHeader.Text = "Patient ID:";
            // 
            // labelSerialNrHeader
            // 
            this.labelSerialNrHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelSerialNrHeader.Font = new System.Drawing.Font("Verdana", 16F);
            this.labelSerialNrHeader.Location = new System.Drawing.Point(0, 35);
            this.labelSerialNrHeader.Name = "labelSerialNrHeader";
            this.labelSerialNrHeader.Size = new System.Drawing.Size(139, 35);
            this.labelSerialNrHeader.TabIndex = 36;
            this.labelSerialNrHeader.Text = "Serial nr:";
            // 
            // labelStartDateHeader
            // 
            this.labelStartDateHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStartDateHeader.Font = new System.Drawing.Font("Verdana", 16F);
            this.labelStartDateHeader.Location = new System.Drawing.Point(0, 0);
            this.labelStartDateHeader.Name = "labelStartDateHeader";
            this.labelStartDateHeader.Size = new System.Drawing.Size(139, 35);
            this.labelStartDateHeader.TabIndex = 35;
            this.labelStartDateHeader.Text = "Enrolled:";
            // 
            // panel5
            // 
            this.panel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel5.Controls.Add(this.panel24);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel5.Location = new System.Drawing.Point(1213, 2);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(66, 209);
            this.panel5.TabIndex = 4;
            // 
            // panel24
            // 
            this.panel24.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel24.BackgroundImage")));
            this.panel24.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel24.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel24.Location = new System.Drawing.Point(0, 0);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(66, 120);
            this.panel24.TabIndex = 5;
            // 
            // panel34
            // 
            this.panel34.Controls.Add(this.labelCountry);
            this.panel34.Controls.Add(this.labelZipCity);
            this.panel34.Controls.Add(this.labelAddress);
            this.panel34.Controls.Add(this.labelPhone1);
            this.panel34.Controls.Add(this.labelPhone2);
            this.panel34.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel34.Location = new System.Drawing.Point(688, 2);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(525, 209);
            this.panel34.TabIndex = 3;
            // 
            // labelCountry
            // 
            this.labelCountry.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelCountry.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelCountry.Location = new System.Drawing.Point(0, 105);
            this.labelCountry.Name = "labelCountry";
            this.labelCountry.Size = new System.Drawing.Size(525, 35);
            this.labelCountry.TabIndex = 46;
            this.labelCountry.Text = "Country";
            // 
            // labelZipCity
            // 
            this.labelZipCity.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelZipCity.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelZipCity.Location = new System.Drawing.Point(0, 70);
            this.labelZipCity.Name = "labelZipCity";
            this.labelZipCity.Size = new System.Drawing.Size(525, 35);
            this.labelZipCity.TabIndex = 45;
            this.labelZipCity.Text = "Texas TX 200111";
            // 
            // labelAddress
            // 
            this.labelAddress.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelAddress.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelAddress.Location = new System.Drawing.Point(0, 0);
            this.labelAddress.Name = "labelAddress";
            this.labelAddress.Size = new System.Drawing.Size(525, 70);
            this.labelAddress.TabIndex = 44;
            this.labelAddress.Text = "112 Alpha Street App 1000 abcde ghdfaj";
            // 
            // labelPhone1
            // 
            this.labelPhone1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelPhone1.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelPhone1.Location = new System.Drawing.Point(0, 139);
            this.labelPhone1.Name = "labelPhone1";
            this.labelPhone1.Size = new System.Drawing.Size(525, 35);
            this.labelPhone1.TabIndex = 47;
            this.labelPhone1.Text = "800-217-0520";
            // 
            // labelPhone2
            // 
            this.labelPhone2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelPhone2.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelPhone2.Location = new System.Drawing.Point(0, 174);
            this.labelPhone2.Name = "labelPhone2";
            this.labelPhone2.Size = new System.Drawing.Size(525, 35);
            this.labelPhone2.TabIndex = 48;
            this.labelPhone2.Text = "800-217-0520";
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.label33);
            this.panel17.Controls.Add(this.label37);
            this.panel17.Controls.Add(this.label29);
            this.panel17.Controls.Add(this.label25);
            this.panel17.Controls.Add(this.labelAddressHeader);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel17.Location = new System.Drawing.Point(571, 2);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(117, 209);
            this.panel17.TabIndex = 2;
            // 
            // label33
            // 
            this.label33.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label33.Font = new System.Drawing.Font("Verdana", 16F);
            this.label33.Location = new System.Drawing.Point(0, 139);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(117, 35);
            this.label33.TabIndex = 39;
            this.label33.Text = "Phone:";
            // 
            // label37
            // 
            this.label37.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label37.Font = new System.Drawing.Font("Verdana", 27F);
            this.label37.Location = new System.Drawing.Point(0, 174);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(117, 35);
            this.label37.TabIndex = 40;
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label29
            // 
            this.label29.Dock = System.Windows.Forms.DockStyle.Top;
            this.label29.Font = new System.Drawing.Font("Verdana", 27F);
            this.label29.Location = new System.Drawing.Point(0, 70);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(117, 35);
            this.label29.TabIndex = 38;
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label25
            // 
            this.label25.Dock = System.Windows.Forms.DockStyle.Top;
            this.label25.Font = new System.Drawing.Font("Verdana", 27F);
            this.label25.Location = new System.Drawing.Point(0, 35);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(117, 35);
            this.label25.TabIndex = 37;
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelAddressHeader
            // 
            this.labelAddressHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelAddressHeader.Font = new System.Drawing.Font("Verdana", 16F);
            this.labelAddressHeader.Location = new System.Drawing.Point(0, 0);
            this.labelAddressHeader.Name = "labelAddressHeader";
            this.labelAddressHeader.Size = new System.Drawing.Size(117, 35);
            this.labelAddressHeader.TabIndex = 36;
            this.labelAddressHeader.Text = "Address:";
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.panel27);
            this.panel9.Controls.Add(this.panel26);
            this.panel9.Controls.Add(this.panel33);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel9.Location = new System.Drawing.Point(0, 2);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(571, 209);
            this.panel9.TabIndex = 1;
            // 
            // panel27
            // 
            this.panel27.Controls.Add(this.labelPatientFirstName);
            this.panel27.Controls.Add(this.labelDateOfBirth);
            this.panel27.Controls.Add(this.labelAgeGender);
            this.panel27.Controls.Add(this.labelPatientLastName);
            this.panel27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel27.Location = new System.Drawing.Point(161, 0);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(410, 209);
            this.panel27.TabIndex = 37;
            // 
            // labelPatientFirstName
            // 
            this.labelPatientFirstName.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPatientFirstName.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelPatientFirstName.Location = new System.Drawing.Point(0, 70);
            this.labelPatientFirstName.Name = "labelPatientFirstName";
            this.labelPatientFirstName.Size = new System.Drawing.Size(410, 35);
            this.labelPatientFirstName.TabIndex = 42;
            this.labelPatientFirstName.Text = "Rutger Alexander";
            // 
            // labelDateOfBirth
            // 
            this.labelDateOfBirth.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelDateOfBirth.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelDateOfBirth.Location = new System.Drawing.Point(0, 139);
            this.labelDateOfBirth.Name = "labelDateOfBirth";
            this.labelDateOfBirth.Size = new System.Drawing.Size(410, 35);
            this.labelDateOfBirth.TabIndex = 43;
            this.labelDateOfBirth.Text = "06/08/1974";
            // 
            // labelAgeGender
            // 
            this.labelAgeGender.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelAgeGender.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelAgeGender.Location = new System.Drawing.Point(0, 174);
            this.labelAgeGender.Name = "labelAgeGender";
            this.labelAgeGender.Size = new System.Drawing.Size(410, 35);
            this.labelAgeGender.TabIndex = 44;
            this.labelAgeGender.Text = "41, male";
            // 
            // labelPatientLastName
            // 
            this.labelPatientLastName.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPatientLastName.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelPatientLastName.Location = new System.Drawing.Point(0, 0);
            this.labelPatientLastName.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.labelPatientLastName.Name = "labelPatientLastName";
            this.labelPatientLastName.Size = new System.Drawing.Size(410, 70);
            this.labelPatientLastName.TabIndex = 41;
            this.labelPatientLastName.Text = "Brest van Kempen\r\nhsgkh\r\n";
            // 
            // panel26
            // 
            this.panel26.Controls.Add(this.label23);
            this.panel26.Controls.Add(this.labelDOBheader);
            this.panel26.Controls.Add(this.label19);
            this.panel26.Controls.Add(this.label15);
            this.panel26.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel26.Location = new System.Drawing.Point(66, 0);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(95, 209);
            this.panel26.TabIndex = 36;
            // 
            // label23
            // 
            this.label23.Dock = System.Windows.Forms.DockStyle.Top;
            this.label23.Font = new System.Drawing.Font("Verdana", 27F);
            this.label23.Location = new System.Drawing.Point(0, 35);
            this.label23.Margin = new System.Windows.Forms.Padding(0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(95, 35);
            this.label23.TabIndex = 39;
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelDOBheader
            // 
            this.labelDOBheader.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelDOBheader.Font = new System.Drawing.Font("Verdana", 16F);
            this.labelDOBheader.Location = new System.Drawing.Point(0, 139);
            this.labelDOBheader.Margin = new System.Windows.Forms.Padding(0);
            this.labelDOBheader.Name = "labelDOBheader";
            this.labelDOBheader.Size = new System.Drawing.Size(95, 35);
            this.labelDOBheader.TabIndex = 37;
            this.labelDOBheader.Text = "D.o.b.:";
            // 
            // label19
            // 
            this.label19.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label19.Font = new System.Drawing.Font("Verdana", 27F);
            this.label19.Location = new System.Drawing.Point(0, 174);
            this.label19.Margin = new System.Windows.Forms.Padding(0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(95, 35);
            this.label19.TabIndex = 38;
            this.label19.Text = " ";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label15
            // 
            this.label15.Dock = System.Windows.Forms.DockStyle.Top;
            this.label15.Font = new System.Drawing.Font("Verdana", 16F);
            this.label15.Location = new System.Drawing.Point(0, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(95, 35);
            this.label15.TabIndex = 36;
            this.label15.Text = "Name:";
            // 
            // panel33
            // 
            this.panel33.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel33.Controls.Add(this.panel22);
            this.panel33.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel33.Location = new System.Drawing.Point(0, 0);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(66, 209);
            this.panel33.TabIndex = 38;
            // 
            // panel22
            // 
            this.panel22.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel22.BackgroundImage")));
            this.panel22.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel22.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel22.Location = new System.Drawing.Point(0, 0);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(66, 120);
            this.panel22.TabIndex = 39;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1654, 2);
            this.panel1.TabIndex = 0;
            // 
            // panelDateTimeofEventStrip
            // 
            this.panelDateTimeofEventStrip.Controls.Add(this.labelActivationType);
            this.panelDateTimeofEventStrip.Controls.Add(this.labelSingleEventReportTime);
            this.panelDateTimeofEventStrip.Controls.Add(this.labelSingleEventReportDate);
            this.panelDateTimeofEventStrip.Controls.Add(this.panel52);
            this.panelDateTimeofEventStrip.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelDateTimeofEventStrip.Location = new System.Drawing.Point(0, 149);
            this.panelDateTimeofEventStrip.Name = "panelDateTimeofEventStrip";
            this.panelDateTimeofEventStrip.Size = new System.Drawing.Size(1654, 66);
            this.panelDateTimeofEventStrip.TabIndex = 1;
            // 
            // labelActivationType
            // 
            this.labelActivationType.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelActivationType.Font = new System.Drawing.Font("Verdana", 28F);
            this.labelActivationType.Location = new System.Drawing.Point(1361, 0);
            this.labelActivationType.Name = "labelActivationType";
            this.labelActivationType.Size = new System.Drawing.Size(293, 66);
            this.labelActivationType.TabIndex = 20;
            this.labelActivationType.Text = "Automatic";
            this.labelActivationType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelSingleEventReportTime
            // 
            this.labelSingleEventReportTime.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSingleEventReportTime.Font = new System.Drawing.Font("Verdana", 28F);
            this.labelSingleEventReportTime.Location = new System.Drawing.Point(891, 0);
            this.labelSingleEventReportTime.Name = "labelSingleEventReportTime";
            this.labelSingleEventReportTime.Size = new System.Drawing.Size(419, 66);
            this.labelSingleEventReportTime.TabIndex = 10;
            this.labelSingleEventReportTime.Text = "^99:99:99 AM";
            this.labelSingleEventReportTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSingleEventReportDate
            // 
            this.labelSingleEventReportDate.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSingleEventReportDate.Font = new System.Drawing.Font("Verdana", 28F);
            this.labelSingleEventReportDate.Location = new System.Drawing.Point(604, 0);
            this.labelSingleEventReportDate.Name = "labelSingleEventReportDate";
            this.labelSingleEventReportDate.Size = new System.Drawing.Size(287, 66);
            this.labelSingleEventReportDate.TabIndex = 8;
            this.labelSingleEventReportDate.Text = "^99/99/9999";
            this.labelSingleEventReportDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel52
            // 
            this.panel52.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel52.Location = new System.Drawing.Point(0, 0);
            this.panel52.Name = "panel52";
            this.panel52.Size = new System.Drawing.Size(604, 66);
            this.panel52.TabIndex = 5;
            this.panel52.Paint += new System.Windows.Forms.PaintEventHandler(this.panel52_Paint);
            // 
            // panelCardiacEVentReportNumber
            // 
            this.panelCardiacEVentReportNumber.Controls.Add(this.labelReportNr);
            this.panelCardiacEVentReportNumber.Controls.Add(this.labelCardiacEventReportNr);
            this.panelCardiacEVentReportNumber.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCardiacEVentReportNumber.Location = new System.Drawing.Point(0, 84);
            this.panelCardiacEVentReportNumber.Name = "panelCardiacEVentReportNumber";
            this.panelCardiacEVentReportNumber.Size = new System.Drawing.Size(1654, 65);
            this.panelCardiacEVentReportNumber.TabIndex = 0;
            // 
            // labelReportNr
            // 
            this.labelReportNr.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelReportNr.Font = new System.Drawing.Font("Verdana", 38F, System.Drawing.FontStyle.Bold);
            this.labelReportNr.Location = new System.Drawing.Point(1200, 0);
            this.labelReportNr.Name = "labelReportNr";
            this.labelReportNr.Size = new System.Drawing.Size(454, 65);
            this.labelReportNr.TabIndex = 4;
            this.labelReportNr.Text = "^99 (9)";
            this.labelReportNr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelReportNr.Visible = false;
            // 
            // labelCardiacEventReportNr
            // 
            this.labelCardiacEventReportNr.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelCardiacEventReportNr.Font = new System.Drawing.Font("Verdana", 38F, System.Drawing.FontStyle.Bold);
            this.labelCardiacEventReportNr.Location = new System.Drawing.Point(0, 0);
            this.labelCardiacEventReportNr.Name = "labelCardiacEventReportNr";
            this.labelCardiacEventReportNr.Size = new System.Drawing.Size(1194, 65);
            this.labelCardiacEventReportNr.TabIndex = 3;
            this.labelCardiacEventReportNr.Text = "Cardiac Event Report 99";
            this.labelCardiacEventReportNr.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panelPrintHeader
            // 
            this.panelPrintHeader.Controls.Add(this.pictureBoxCenter);
            this.panelPrintHeader.Controls.Add(this.panel56);
            this.panelPrintHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelPrintHeader.Location = new System.Drawing.Point(0, 0);
            this.panelPrintHeader.Name = "panelPrintHeader";
            this.panelPrintHeader.Size = new System.Drawing.Size(1654, 84);
            this.panelPrintHeader.TabIndex = 4;
            // 
            // pictureBoxCenter
            // 
            this.pictureBoxCenter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBoxCenter.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBoxCenter.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxCenter.Name = "pictureBoxCenter";
            this.pictureBoxCenter.Size = new System.Drawing.Size(400, 84);
            this.pictureBoxCenter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxCenter.TabIndex = 0;
            this.pictureBoxCenter.TabStop = false;
            // 
            // panel56
            // 
            this.panel56.Controls.Add(this.panel58);
            this.panel56.Controls.Add(this.panel59);
            this.panel56.Controls.Add(this.panel60);
            this.panel56.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel56.Location = new System.Drawing.Point(1254, 0);
            this.panel56.Name = "panel56";
            this.panel56.Size = new System.Drawing.Size(400, 84);
            this.panel56.TabIndex = 8;
            // 
            // panel58
            // 
            this.panel58.Controls.Add(this.labelCenter2);
            this.panel58.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel58.Location = new System.Drawing.Point(0, 49);
            this.panel58.Name = "panel58";
            this.panel58.Size = new System.Drawing.Size(400, 34);
            this.panel58.TabIndex = 15;
            // 
            // labelCenter2
            // 
            this.labelCenter2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCenter2.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelCenter2.Location = new System.Drawing.Point(0, 0);
            this.labelCenter2.Name = "labelCenter2";
            this.labelCenter2.Size = new System.Drawing.Size(400, 34);
            this.labelCenter2.TabIndex = 12;
            this.labelCenter2.Text = "c2";
            this.labelCenter2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel59
            // 
            this.panel59.Controls.Add(this.labelCenter1);
            this.panel59.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel59.Location = new System.Drawing.Point(0, 15);
            this.panel59.Name = "panel59";
            this.panel59.Size = new System.Drawing.Size(400, 34);
            this.panel59.TabIndex = 14;
            // 
            // labelCenter1
            // 
            this.labelCenter1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCenter1.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelCenter1.Location = new System.Drawing.Point(0, 0);
            this.labelCenter1.Name = "labelCenter1";
            this.labelCenter1.Size = new System.Drawing.Size(400, 34);
            this.labelCenter1.TabIndex = 11;
            this.labelCenter1.Text = "c1";
            this.labelCenter1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel60
            // 
            this.panel60.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel60.Location = new System.Drawing.Point(0, 0);
            this.panel60.Name = "panel60";
            this.panel60.Size = new System.Drawing.Size(400, 15);
            this.panel60.TabIndex = 13;
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripGenPages,
            this.toolStripButtonPrint,
            this.toolStripButton1,
            this.toolStripMemUsage,
            this.toolStripClipboard1,
            this.toolStripClipboard2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1996, 39);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripGenPages
            // 
            this.toolStripGenPages.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripGenPages.Image = ((System.Drawing.Image)(resources.GetObject("toolStripGenPages.Image")));
            this.toolStripGenPages.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripGenPages.Name = "toolStripGenPages";
            this.toolStripGenPages.Size = new System.Drawing.Size(112, 36);
            this.toolStripGenPages.Text = "Generating Pages...";
            // 
            // toolStripButtonPrint
            // 
            this.toolStripButtonPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonPrint.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonPrint.Image")));
            this.toolStripButtonPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonPrint.Name = "toolStripButtonPrint";
            this.toolStripButtonPrint.Size = new System.Drawing.Size(36, 36);
            this.toolStripButtonPrint.Text = "Print";
            this.toolStripButtonPrint.Click += new System.EventHandler(this.toolStripButton1_Click_1);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton1.Text = "toolStripButton1";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click_2);
            // 
            // toolStripMemUsage
            // 
            this.toolStripMemUsage.Name = "toolStripMemUsage";
            this.toolStripMemUsage.Size = new System.Drawing.Size(125, 36);
            this.toolStripMemUsage.Text = "^p1234 \\r\\nf 12345MB";
            // 
            // toolStripClipboard1
            // 
            this.toolStripClipboard1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripClipboard1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripClipboard1.Image")));
            this.toolStripClipboard1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripClipboard1.Name = "toolStripClipboard1";
            this.toolStripClipboard1.Size = new System.Drawing.Size(72, 36);
            this.toolStripClipboard1.Text = "Clipboard 1";
            this.toolStripClipboard1.Click += new System.EventHandler(this.toolStripClipboard_Click);
            // 
            // toolStripClipboard2
            // 
            this.toolStripClipboard2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripClipboard2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripClipboard2.Image")));
            this.toolStripClipboard2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripClipboard2.Name = "toolStripClipboard2";
            this.toolStripClipboard2.Size = new System.Drawing.Size(72, 36);
            this.toolStripClipboard2.Text = "Clipboard 2";
            this.toolStripClipboard2.Click += new System.EventHandler(this.toolStripClipboard2_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // panel21
            // 
            this.panel21.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel21.Location = new System.Drawing.Point(0, 39);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(1996, 21);
            this.panel21.TabIndex = 2;
            this.panel21.Paint += new System.Windows.Forms.PaintEventHandler(this.panel21_Paint);
            // 
            // panelPrintArea2
            // 
            this.panelPrintArea2.BackColor = System.Drawing.Color.White;
            this.panelPrintArea2.Controls.Add(this.panel14);
            this.panelPrintArea2.Controls.Add(this.panelEventSample5);
            this.panelPrintArea2.Controls.Add(this.panel13);
            this.panelPrintArea2.Controls.Add(this.panelEventSample4);
            this.panelPrintArea2.Controls.Add(this.panel12);
            this.panelPrintArea2.Controls.Add(this.panelEventSample3);
            this.panelPrintArea2.Controls.Add(this.panel488);
            this.panelPrintArea2.Controls.Add(this.panelPage2Header);
            this.panelPrintArea2.Controls.Add(this.panel500);
            this.panelPrintArea2.Controls.Add(this.panelPage2Footer);
            this.panelPrintArea2.Location = new System.Drawing.Point(13, 2472);
            this.panelPrintArea2.Margin = new System.Windows.Forms.Padding(0);
            this.panelPrintArea2.Name = "panelPrintArea2";
            this.panelPrintArea2.Size = new System.Drawing.Size(1654, 2339);
            this.panelPrintArea2.TabIndex = 3;
            // 
            // panel14
            // 
            this.panel14.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel14.Location = new System.Drawing.Point(0, 2258);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(1654, 2);
            this.panel14.TabIndex = 52;
            // 
            // panelEventSample5
            // 
            this.panelEventSample5.AutoSize = true;
            this.panelEventSample5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panelEventSample5.Controls.Add(this.panel160);
            this.panelEventSample5.Controls.Add(this.panel153);
            this.panelEventSample5.Controls.Add(this.panel290);
            this.panelEventSample5.Controls.Add(this.panel298);
            this.panelEventSample5.Controls.Add(this.panel378);
            this.panelEventSample5.Controls.Add(this.panel421);
            this.panelEventSample5.Controls.Add(this.panel453);
            this.panelEventSample5.Controls.Add(this.panel468);
            this.panelEventSample5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelEventSample5.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.panelEventSample5.Location = new System.Drawing.Point(0, 1579);
            this.panelEventSample5.Name = "panelEventSample5";
            this.panelEventSample5.Size = new System.Drawing.Size(1654, 679);
            this.panelEventSample5.TabIndex = 51;
            // 
            // panel160
            // 
            this.panel160.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel160.Controls.Add(this.panel163);
            this.panel160.Controls.Add(this.panel166);
            this.panel160.Controls.Add(this.panel167);
            this.panel160.Controls.Add(this.panel169);
            this.panel160.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel160.Location = new System.Drawing.Point(0, 533);
            this.panel160.Name = "panel160";
            this.panel160.Size = new System.Drawing.Size(1654, 146);
            this.panel160.TabIndex = 59;
            // 
            // panel163
            // 
            this.panel163.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel163.Controls.Add(this.labelFindings5);
            this.panel163.Controls.Add(this.panel164);
            this.panel163.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel163.Location = new System.Drawing.Point(560, 0);
            this.panel163.Name = "panel163";
            this.panel163.Size = new System.Drawing.Size(1092, 144);
            this.panel163.TabIndex = 7;
            // 
            // labelFindings5
            // 
            this.labelFindings5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelFindings5.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelFindings5.Location = new System.Drawing.Point(0, 38);
            this.labelFindings5.Name = "labelFindings5";
            this.labelFindings5.Size = new System.Drawing.Size(1090, 104);
            this.labelFindings5.TabIndex = 6;
            this.labelFindings5.Text = "Atrial Fibrillation, with Occasional PVC\'s and many other findings that the Tech " +
    "can add a lot of text \r\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx \r\ny" +
    "yyyyyyyyyyyyyyyyyyyy\r\n\r\n";
            // 
            // panel164
            // 
            this.panel164.Controls.Add(this.labelRhythms5);
            this.panel164.Controls.Add(this.labelFindingsText5);
            this.panel164.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel164.Location = new System.Drawing.Point(0, 0);
            this.panel164.Name = "panel164";
            this.panel164.Size = new System.Drawing.Size(1090, 38);
            this.panel164.TabIndex = 2;
            // 
            // labelRhythms5
            // 
            this.labelRhythms5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelRhythms5.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelRhythms5.Location = new System.Drawing.Point(229, 0);
            this.labelRhythms5.Name = "labelRhythms5";
            this.labelRhythms5.Size = new System.Drawing.Size(861, 38);
            this.labelRhythms5.TabIndex = 70;
            this.labelRhythms5.Text = "Rhythms";
            // 
            // labelFindingsText5
            // 
            this.labelFindingsText5.AutoSize = true;
            this.labelFindingsText5.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelFindingsText5.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelFindingsText5.Location = new System.Drawing.Point(0, 0);
            this.labelFindingsText5.Name = "labelFindingsText5";
            this.labelFindingsText5.Size = new System.Drawing.Size(229, 32);
            this.labelFindingsText5.TabIndex = 2;
            this.labelFindingsText5.Text = "^QC Findings:";
            // 
            // panel166
            // 
            this.panel166.AutoSize = true;
            this.panel166.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel166.Location = new System.Drawing.Point(560, 0);
            this.panel166.Name = "panel166";
            this.panel166.Size = new System.Drawing.Size(0, 144);
            this.panel166.TabIndex = 6;
            // 
            // panel167
            // 
            this.panel167.AutoSize = true;
            this.panel167.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel167.Location = new System.Drawing.Point(560, 0);
            this.panel167.Name = "panel167";
            this.panel167.Size = new System.Drawing.Size(0, 144);
            this.panel167.TabIndex = 3;
            // 
            // panel169
            // 
            this.panel169.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel169.Controls.Add(this.panel170);
            this.panel169.Controls.Add(this.panel171);
            this.panel169.Controls.Add(this.panel172);
            this.panel169.Controls.Add(this.panel173);
            this.panel169.Controls.Add(this.panel174);
            this.panel169.Controls.Add(this.panel175);
            this.panel169.Controls.Add(this.label213);
            this.panel169.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel169.Location = new System.Drawing.Point(0, 0);
            this.panel169.Name = "panel169";
            this.panel169.Size = new System.Drawing.Size(560, 144);
            this.panel169.TabIndex = 9;
            // 
            // panel170
            // 
            this.panel170.Controls.Add(this.unitQt5);
            this.panel170.Controls.Add(this.unitQrs5);
            this.panel170.Controls.Add(this.unitPr5);
            this.panel170.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel170.Location = new System.Drawing.Point(477, 38);
            this.panel170.Name = "panel170";
            this.panel170.Size = new System.Drawing.Size(78, 104);
            this.panel170.TabIndex = 11;
            // 
            // unitQt5
            // 
            this.unitQt5.Dock = System.Windows.Forms.DockStyle.Top;
            this.unitQt5.Font = new System.Drawing.Font("Verdana", 20F);
            this.unitQt5.Location = new System.Drawing.Point(0, 68);
            this.unitQt5.Name = "unitQt5";
            this.unitQt5.Size = new System.Drawing.Size(78, 34);
            this.unitQt5.TabIndex = 73;
            this.unitQt5.Text = "sec";
            // 
            // unitQrs5
            // 
            this.unitQrs5.Dock = System.Windows.Forms.DockStyle.Top;
            this.unitQrs5.Font = new System.Drawing.Font("Verdana", 20F);
            this.unitQrs5.Location = new System.Drawing.Point(0, 34);
            this.unitQrs5.Name = "unitQrs5";
            this.unitQrs5.Size = new System.Drawing.Size(78, 34);
            this.unitQrs5.TabIndex = 72;
            this.unitQrs5.Text = "sec";
            // 
            // unitPr5
            // 
            this.unitPr5.Dock = System.Windows.Forms.DockStyle.Top;
            this.unitPr5.Font = new System.Drawing.Font("Verdana", 20F);
            this.unitPr5.Location = new System.Drawing.Point(0, 0);
            this.unitPr5.Name = "unitPr5";
            this.unitPr5.Size = new System.Drawing.Size(78, 34);
            this.unitPr5.TabIndex = 71;
            this.unitPr5.Text = "sec";
            // 
            // panel171
            // 
            this.panel171.Controls.Add(this.valueQt5);
            this.panel171.Controls.Add(this.valueQrs5);
            this.panel171.Controls.Add(this.valuePr5);
            this.panel171.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel171.Location = new System.Drawing.Point(381, 38);
            this.panel171.Name = "panel171";
            this.panel171.Size = new System.Drawing.Size(96, 104);
            this.panel171.TabIndex = 10;
            // 
            // valueQt5
            // 
            this.valueQt5.Dock = System.Windows.Forms.DockStyle.Top;
            this.valueQt5.Font = new System.Drawing.Font("Verdana", 20F);
            this.valueQt5.Location = new System.Drawing.Point(0, 68);
            this.valueQt5.Name = "valueQt5";
            this.valueQt5.Size = new System.Drawing.Size(96, 34);
            this.valueQt5.TabIndex = 76;
            this.valueQt5.Text = "0,465";
            // 
            // valueQrs5
            // 
            this.valueQrs5.Dock = System.Windows.Forms.DockStyle.Top;
            this.valueQrs5.Font = new System.Drawing.Font("Verdana", 20F);
            this.valueQrs5.Location = new System.Drawing.Point(0, 34);
            this.valueQrs5.Name = "valueQrs5";
            this.valueQrs5.Size = new System.Drawing.Size(96, 34);
            this.valueQrs5.TabIndex = 71;
            this.valueQrs5.Text = "0,113";
            // 
            // valuePr5
            // 
            this.valuePr5.Dock = System.Windows.Forms.DockStyle.Top;
            this.valuePr5.Font = new System.Drawing.Font("Verdana", 20F);
            this.valuePr5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.valuePr5.Location = new System.Drawing.Point(0, 0);
            this.valuePr5.Name = "valuePr5";
            this.valuePr5.Size = new System.Drawing.Size(96, 34);
            this.valuePr5.TabIndex = 65;
            this.valuePr5.Text = "0,202";
            // 
            // panel172
            // 
            this.panel172.Controls.Add(this.headerQt5);
            this.panel172.Controls.Add(this.headerQrs5);
            this.panel172.Controls.Add(this.headerPr5);
            this.panel172.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel172.Location = new System.Drawing.Point(300, 38);
            this.panel172.Name = "panel172";
            this.panel172.Size = new System.Drawing.Size(81, 104);
            this.panel172.TabIndex = 9;
            // 
            // headerQt5
            // 
            this.headerQt5.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerQt5.Font = new System.Drawing.Font("Verdana", 20F);
            this.headerQt5.Location = new System.Drawing.Point(0, 68);
            this.headerQt5.Name = "headerQt5";
            this.headerQt5.Size = new System.Drawing.Size(81, 34);
            this.headerQt5.TabIndex = 76;
            this.headerQt5.Text = "QT:";
            // 
            // headerQrs5
            // 
            this.headerQrs5.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerQrs5.Font = new System.Drawing.Font("Verdana", 20F);
            this.headerQrs5.Location = new System.Drawing.Point(0, 34);
            this.headerQrs5.Name = "headerQrs5";
            this.headerQrs5.Size = new System.Drawing.Size(81, 34);
            this.headerQrs5.TabIndex = 75;
            this.headerQrs5.Text = "QRS:";
            // 
            // headerPr5
            // 
            this.headerPr5.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPr5.Font = new System.Drawing.Font("Verdana", 20F);
            this.headerPr5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.headerPr5.Location = new System.Drawing.Point(0, 0);
            this.headerPr5.Name = "headerPr5";
            this.headerPr5.Size = new System.Drawing.Size(81, 34);
            this.headerPr5.TabIndex = 74;
            this.headerPr5.Text = "PR:";
            // 
            // panel173
            // 
            this.panel173.Controls.Add(this.unitMaxHr5);
            this.panel173.Controls.Add(this.unitMeanHr5);
            this.panel173.Controls.Add(this.unitMinHr5);
            this.panel173.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel173.Location = new System.Drawing.Point(221, 38);
            this.panel173.Name = "panel173";
            this.panel173.Size = new System.Drawing.Size(79, 104);
            this.panel173.TabIndex = 8;
            // 
            // unitMaxHr5
            // 
            this.unitMaxHr5.Dock = System.Windows.Forms.DockStyle.Top;
            this.unitMaxHr5.Font = new System.Drawing.Font("Verdana", 20F);
            this.unitMaxHr5.Location = new System.Drawing.Point(0, 68);
            this.unitMaxHr5.Name = "unitMaxHr5";
            this.unitMaxHr5.Size = new System.Drawing.Size(79, 34);
            this.unitMaxHr5.TabIndex = 64;
            this.unitMaxHr5.Text = "bpm";
            // 
            // unitMeanHr5
            // 
            this.unitMeanHr5.Dock = System.Windows.Forms.DockStyle.Top;
            this.unitMeanHr5.Font = new System.Drawing.Font("Verdana", 20F);
            this.unitMeanHr5.Location = new System.Drawing.Point(0, 34);
            this.unitMeanHr5.Name = "unitMeanHr5";
            this.unitMeanHr5.Size = new System.Drawing.Size(79, 34);
            this.unitMeanHr5.TabIndex = 63;
            this.unitMeanHr5.Text = "bpm";
            // 
            // unitMinHr5
            // 
            this.unitMinHr5.Dock = System.Windows.Forms.DockStyle.Top;
            this.unitMinHr5.Font = new System.Drawing.Font("Verdana", 20F);
            this.unitMinHr5.Location = new System.Drawing.Point(0, 0);
            this.unitMinHr5.Name = "unitMinHr5";
            this.unitMinHr5.Size = new System.Drawing.Size(79, 34);
            this.unitMinHr5.TabIndex = 61;
            this.unitMinHr5.Text = "bpm";
            // 
            // panel174
            // 
            this.panel174.Controls.Add(this.valueMaxHr5);
            this.panel174.Controls.Add(this.valueMeanHr5);
            this.panel174.Controls.Add(this.valueMinHr5);
            this.panel174.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel174.Location = new System.Drawing.Point(150, 38);
            this.panel174.Name = "panel174";
            this.panel174.Size = new System.Drawing.Size(71, 104);
            this.panel174.TabIndex = 7;
            // 
            // valueMaxHr5
            // 
            this.valueMaxHr5.Dock = System.Windows.Forms.DockStyle.Top;
            this.valueMaxHr5.Font = new System.Drawing.Font("Verdana", 20F);
            this.valueMaxHr5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.valueMaxHr5.Location = new System.Drawing.Point(0, 68);
            this.valueMaxHr5.Name = "valueMaxHr5";
            this.valueMaxHr5.Size = new System.Drawing.Size(71, 34);
            this.valueMaxHr5.TabIndex = 61;
            this.valueMaxHr5.Text = "120";
            // 
            // valueMeanHr5
            // 
            this.valueMeanHr5.Dock = System.Windows.Forms.DockStyle.Top;
            this.valueMeanHr5.Font = new System.Drawing.Font("Verdana", 20F);
            this.valueMeanHr5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.valueMeanHr5.Location = new System.Drawing.Point(0, 34);
            this.valueMeanHr5.Name = "valueMeanHr5";
            this.valueMeanHr5.Size = new System.Drawing.Size(71, 34);
            this.valueMeanHr5.TabIndex = 56;
            this.valueMeanHr5.Text = "110";
            // 
            // valueMinHr5
            // 
            this.valueMinHr5.Dock = System.Windows.Forms.DockStyle.Top;
            this.valueMinHr5.Font = new System.Drawing.Font("Verdana", 20F);
            this.valueMinHr5.Location = new System.Drawing.Point(0, 0);
            this.valueMinHr5.Name = "valueMinHr5";
            this.valueMinHr5.Size = new System.Drawing.Size(71, 34);
            this.valueMinHr5.TabIndex = 52;
            this.valueMinHr5.Text = "100";
            // 
            // panel175
            // 
            this.panel175.Controls.Add(this.headerMaxHr5);
            this.panel175.Controls.Add(this.headerMeanHr5);
            this.panel175.Controls.Add(this.headerMinHr5);
            this.panel175.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel175.Location = new System.Drawing.Point(0, 38);
            this.panel175.Name = "panel175";
            this.panel175.Size = new System.Drawing.Size(150, 104);
            this.panel175.TabIndex = 6;
            // 
            // headerMaxHr5
            // 
            this.headerMaxHr5.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerMaxHr5.Font = new System.Drawing.Font("Verdana", 20F);
            this.headerMaxHr5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.headerMaxHr5.Location = new System.Drawing.Point(0, 68);
            this.headerMaxHr5.Name = "headerMaxHr5";
            this.headerMaxHr5.Size = new System.Drawing.Size(150, 34);
            this.headerMaxHr5.TabIndex = 74;
            this.headerMaxHr5.Text = "Max HR:";
            // 
            // headerMeanHr5
            // 
            this.headerMeanHr5.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerMeanHr5.Font = new System.Drawing.Font("Verdana", 20F);
            this.headerMeanHr5.Location = new System.Drawing.Point(0, 34);
            this.headerMeanHr5.Name = "headerMeanHr5";
            this.headerMeanHr5.Size = new System.Drawing.Size(150, 34);
            this.headerMeanHr5.TabIndex = 64;
            this.headerMeanHr5.Text = "Mean HR:";
            // 
            // headerMinHr5
            // 
            this.headerMinHr5.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerMinHr5.Font = new System.Drawing.Font("Verdana", 20F);
            this.headerMinHr5.Location = new System.Drawing.Point(0, 0);
            this.headerMinHr5.Name = "headerMinHr5";
            this.headerMinHr5.Size = new System.Drawing.Size(150, 34);
            this.headerMinHr5.TabIndex = 63;
            this.headerMinHr5.Text = "Min HR:";
            // 
            // label213
            // 
            this.label213.Dock = System.Windows.Forms.DockStyle.Top;
            this.label213.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.label213.Location = new System.Drawing.Point(0, 0);
            this.label213.Name = "label213";
            this.label213.Size = new System.Drawing.Size(558, 38);
            this.label213.TabIndex = 5;
            this.label213.Text = "QRS Measurements:";
            // 
            // panel153
            // 
            this.panel153.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel153.Controls.Add(this.labelSample5StartFull);
            this.panel153.Controls.Add(this.labelSample5MidFull);
            this.panel153.Controls.Add(this.panel238);
            this.panel153.Controls.Add(this.panel241);
            this.panel153.Controls.Add(this.panel243);
            this.panel153.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel153.Location = new System.Drawing.Point(0, 498);
            this.panel153.Name = "panel153";
            this.panel153.Size = new System.Drawing.Size(1654, 35);
            this.panel153.TabIndex = 26;
            // 
            // labelSample5StartFull
            // 
            this.labelSample5StartFull.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSample5StartFull.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample5StartFull.Location = new System.Drawing.Point(0, 0);
            this.labelSample5StartFull.Name = "labelSample5StartFull";
            this.labelSample5StartFull.Size = new System.Drawing.Size(620, 33);
            this.labelSample5StartFull.TabIndex = 9;
            this.labelSample5StartFull.Text = "10:15:10 AM 10/12/2018  3d 20:22:06 ";
            this.labelSample5StartFull.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample5MidFull
            // 
            this.labelSample5MidFull.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample5MidFull.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample5MidFull.Location = new System.Drawing.Point(653, 0);
            this.labelSample5MidFull.Name = "labelSample5MidFull";
            this.labelSample5MidFull.Size = new System.Drawing.Size(249, 33);
            this.labelSample5MidFull.TabIndex = 8;
            this.labelSample5MidFull.Text = "10:15:00 AM";
            this.labelSample5MidFull.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelSample5MidFull.Visible = false;
            // 
            // panel238
            // 
            this.panel238.Controls.Add(this.labelSample5SweepFull);
            this.panel238.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel238.Location = new System.Drawing.Point(902, 0);
            this.panel238.Name = "panel238";
            this.panel238.Size = new System.Drawing.Size(290, 33);
            this.panel238.TabIndex = 7;
            // 
            // labelSample5SweepFull
            // 
            this.labelSample5SweepFull.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSample5SweepFull.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample5SweepFull.Location = new System.Drawing.Point(0, 0);
            this.labelSample5SweepFull.Name = "labelSample5SweepFull";
            this.labelSample5SweepFull.Size = new System.Drawing.Size(290, 33);
            this.labelSample5SweepFull.TabIndex = 3;
            this.labelSample5SweepFull.Text = "25 mm/sec (0.20 Sec)";
            this.labelSample5SweepFull.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel241
            // 
            this.panel241.Controls.Add(this.labelSample5AmplFull);
            this.panel241.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel241.Location = new System.Drawing.Point(1192, 0);
            this.panel241.Name = "panel241";
            this.panel241.Size = new System.Drawing.Size(285, 33);
            this.panel241.TabIndex = 6;
            // 
            // labelSample5AmplFull
            // 
            this.labelSample5AmplFull.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSample5AmplFull.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample5AmplFull.Location = new System.Drawing.Point(0, 0);
            this.labelSample5AmplFull.Name = "labelSample5AmplFull";
            this.labelSample5AmplFull.Size = new System.Drawing.Size(285, 33);
            this.labelSample5AmplFull.TabIndex = 2;
            this.labelSample5AmplFull.Text = "10 mm/mV (0.50 mV)";
            this.labelSample5AmplFull.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel243
            // 
            this.panel243.Controls.Add(this.labelSample5EndFull);
            this.panel243.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel243.Location = new System.Drawing.Point(1477, 0);
            this.panel243.Name = "panel243";
            this.panel243.Size = new System.Drawing.Size(175, 33);
            this.panel243.TabIndex = 5;
            // 
            // labelSample5EndFull
            // 
            this.labelSample5EndFull.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSample5EndFull.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample5EndFull.Location = new System.Drawing.Point(0, 0);
            this.labelSample5EndFull.Name = "labelSample5EndFull";
            this.labelSample5EndFull.Size = new System.Drawing.Size(175, 33);
            this.labelSample5EndFull.TabIndex = 1;
            this.labelSample5EndFull.Text = "10:15:30 AM";
            this.labelSample5EndFull.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel290
            // 
            this.panel290.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel290.Controls.Add(this.panel291);
            this.panel290.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel290.Location = new System.Drawing.Point(0, 288);
            this.panel290.Name = "panel290";
            this.panel290.Size = new System.Drawing.Size(1654, 210);
            this.panel290.TabIndex = 25;
            // 
            // panel291
            // 
            this.panel291.Controls.Add(this.labelSample5LeadFull);
            this.panel291.Controls.Add(this.pictureBoxSample5Full);
            this.panel291.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel291.Location = new System.Drawing.Point(0, 0);
            this.panel291.Name = "panel291";
            this.panel291.Size = new System.Drawing.Size(1652, 208);
            this.panel291.TabIndex = 2;
            // 
            // labelSample5LeadFull
            // 
            this.labelSample5LeadFull.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSample5LeadFull.Location = new System.Drawing.Point(12, 57);
            this.labelSample5LeadFull.Name = "labelSample5LeadFull";
            this.labelSample5LeadFull.Size = new System.Drawing.Size(212, 45);
            this.labelSample5LeadFull.TabIndex = 47;
            this.labelSample5LeadFull.Text = "LEAD I";
            this.labelSample5LeadFull.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBoxSample5Full
            // 
            this.pictureBoxSample5Full.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxSample5Full.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxSample5Full.Name = "pictureBoxSample5Full";
            this.pictureBoxSample5Full.Size = new System.Drawing.Size(1652, 208);
            this.pictureBoxSample5Full.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxSample5Full.TabIndex = 0;
            this.pictureBoxSample5Full.TabStop = false;
            // 
            // panel298
            // 
            this.panel298.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel298.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel298.Location = new System.Drawing.Point(0, 286);
            this.panel298.Name = "panel298";
            this.panel298.Size = new System.Drawing.Size(1654, 2);
            this.panel298.TabIndex = 20;
            // 
            // panel378
            // 
            this.panel378.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel378.Controls.Add(this.labelSample5Start);
            this.panel378.Controls.Add(this.labelSample5Mid);
            this.panel378.Controls.Add(this.panel383);
            this.panel378.Controls.Add(this.panel419);
            this.panel378.Controls.Add(this.panel420);
            this.panel378.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel378.Location = new System.Drawing.Point(0, 251);
            this.panel378.Name = "panel378";
            this.panel378.Size = new System.Drawing.Size(1654, 35);
            this.panel378.TabIndex = 11;
            // 
            // labelSample5Start
            // 
            this.labelSample5Start.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSample5Start.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample5Start.Location = new System.Drawing.Point(0, 0);
            this.labelSample5Start.Name = "labelSample5Start";
            this.labelSample5Start.Size = new System.Drawing.Size(693, 33);
            this.labelSample5Start.TabIndex = 8;
            this.labelSample5Start.Text = "10:15:10 AM 10/12/2018  6 sec";
            this.labelSample5Start.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelSample5Start.Click += new System.EventHandler(this.labelSample5Start_Click);
            // 
            // labelSample5Mid
            // 
            this.labelSample5Mid.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample5Mid.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample5Mid.Location = new System.Drawing.Point(644, 0);
            this.labelSample5Mid.Name = "labelSample5Mid";
            this.labelSample5Mid.Size = new System.Drawing.Size(258, 33);
            this.labelSample5Mid.TabIndex = 7;
            this.labelSample5Mid.Text = "10:15:15 AM";
            this.labelSample5Mid.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelSample5Mid.Visible = false;
            // 
            // panel383
            // 
            this.panel383.Controls.Add(this.labelSample5Sweep);
            this.panel383.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel383.Location = new System.Drawing.Point(902, 0);
            this.panel383.Name = "panel383";
            this.panel383.Size = new System.Drawing.Size(290, 33);
            this.panel383.TabIndex = 4;
            // 
            // labelSample5Sweep
            // 
            this.labelSample5Sweep.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSample5Sweep.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample5Sweep.Location = new System.Drawing.Point(0, 0);
            this.labelSample5Sweep.Name = "labelSample5Sweep";
            this.labelSample5Sweep.Size = new System.Drawing.Size(290, 33);
            this.labelSample5Sweep.TabIndex = 1;
            this.labelSample5Sweep.Text = "25 mm/sec (0.20 Sec)";
            this.labelSample5Sweep.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelSample5Sweep.Click += new System.EventHandler(this.labelSample5Sweep_Click);
            // 
            // panel419
            // 
            this.panel419.Controls.Add(this.labelSample5Ampl);
            this.panel419.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel419.Location = new System.Drawing.Point(1192, 0);
            this.panel419.Name = "panel419";
            this.panel419.Size = new System.Drawing.Size(285, 33);
            this.panel419.TabIndex = 3;
            // 
            // labelSample5Ampl
            // 
            this.labelSample5Ampl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSample5Ampl.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample5Ampl.Location = new System.Drawing.Point(0, 0);
            this.labelSample5Ampl.Name = "labelSample5Ampl";
            this.labelSample5Ampl.Size = new System.Drawing.Size(285, 33);
            this.labelSample5Ampl.TabIndex = 2;
            this.labelSample5Ampl.Text = "10 mm/mV (0.50 mV)";
            this.labelSample5Ampl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel420
            // 
            this.panel420.Controls.Add(this.labelSample5End);
            this.panel420.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel420.Location = new System.Drawing.Point(1477, 0);
            this.panel420.Name = "panel420";
            this.panel420.Size = new System.Drawing.Size(175, 33);
            this.panel420.TabIndex = 2;
            // 
            // labelSample5End
            // 
            this.labelSample5End.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSample5End.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample5End.Location = new System.Drawing.Point(0, 0);
            this.labelSample5End.Name = "labelSample5End";
            this.labelSample5End.Size = new System.Drawing.Size(175, 33);
            this.labelSample5End.TabIndex = 2;
            this.labelSample5End.Text = "10:15:20 AM";
            this.labelSample5End.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel421
            // 
            this.panel421.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel421.Controls.Add(this.panel422);
            this.panel421.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel421.Location = new System.Drawing.Point(0, 41);
            this.panel421.Name = "panel421";
            this.panel421.Size = new System.Drawing.Size(1654, 210);
            this.panel421.TabIndex = 10;
            // 
            // panel422
            // 
            this.panel422.Controls.Add(this.labelSample5Lead);
            this.panel422.Controls.Add(this.pictureBoxSample5);
            this.panel422.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel422.Location = new System.Drawing.Point(0, 0);
            this.panel422.Name = "panel422";
            this.panel422.Size = new System.Drawing.Size(1652, 208);
            this.panel422.TabIndex = 2;
            // 
            // labelSample5Lead
            // 
            this.labelSample5Lead.Font = new System.Drawing.Font("Verdana", 28F);
            this.labelSample5Lead.Location = new System.Drawing.Point(9, 67);
            this.labelSample5Lead.Name = "labelSample5Lead";
            this.labelSample5Lead.Size = new System.Drawing.Size(206, 49);
            this.labelSample5Lead.TabIndex = 46;
            this.labelSample5Lead.Text = "LEAD I";
            this.labelSample5Lead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelSample5Lead.Visible = false;
            // 
            // pictureBoxSample5
            // 
            this.pictureBoxSample5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxSample5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxSample5.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxSample5.InitialImage")));
            this.pictureBoxSample5.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxSample5.Name = "pictureBoxSample5";
            this.pictureBoxSample5.Size = new System.Drawing.Size(1652, 208);
            this.pictureBoxSample5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxSample5.TabIndex = 0;
            this.pictureBoxSample5.TabStop = false;
            // 
            // panel453
            // 
            this.panel453.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel453.Controls.Add(this.labelSample5Date);
            this.panel453.Controls.Add(this.label119);
            this.panel453.Controls.Add(this.labelSample5Time);
            this.panel453.Controls.Add(this.labelEvent5);
            this.panel453.Controls.Add(this.labelClass5);
            this.panel453.Controls.Add(this.labelSample5Nr);
            this.panel453.Controls.Add(this.labelSample5EventNr);
            this.panel453.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel453.Location = new System.Drawing.Point(0, 1);
            this.panel453.Name = "panel453";
            this.panel453.Size = new System.Drawing.Size(1654, 40);
            this.panel453.TabIndex = 7;
            // 
            // labelSample5Date
            // 
            this.labelSample5Date.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample5Date.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelSample5Date.Location = new System.Drawing.Point(1010, 0);
            this.labelSample5Date.Name = "labelSample5Date";
            this.labelSample5Date.Size = new System.Drawing.Size(200, 38);
            this.labelSample5Date.TabIndex = 16;
            this.labelSample5Date.Text = "11/23/2016";
            this.labelSample5Date.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label119
            // 
            this.label119.Dock = System.Windows.Forms.DockStyle.Right;
            this.label119.Font = new System.Drawing.Font("Verdana", 20F);
            this.label119.Location = new System.Drawing.Point(1210, 0);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(17, 38);
            this.label119.TabIndex = 15;
            this.label119.Text = "-";
            this.label119.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelSample5Time
            // 
            this.labelSample5Time.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample5Time.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelSample5Time.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelSample5Time.Location = new System.Drawing.Point(1227, 0);
            this.labelSample5Time.Name = "labelSample5Time";
            this.labelSample5Time.Size = new System.Drawing.Size(205, 38);
            this.labelSample5Time.TabIndex = 14;
            this.labelSample5Time.Text = "10:15:10 AM";
            this.labelSample5Time.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelEvent5
            // 
            this.labelEvent5.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelEvent5.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelEvent5.Location = new System.Drawing.Point(1432, 0);
            this.labelEvent5.Name = "labelEvent5";
            this.labelEvent5.Size = new System.Drawing.Size(108, 38);
            this.labelEvent5.TabIndex = 3;
            this.labelEvent5.Text = "Event:";
            this.labelEvent5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelClass5
            // 
            this.labelClass5.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelClass5.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold);
            this.labelClass5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelClass5.Location = new System.Drawing.Point(60, 0);
            this.labelClass5.Name = "labelClass5";
            this.labelClass5.Size = new System.Drawing.Size(477, 38);
            this.labelClass5.TabIndex = 17;
            this.labelClass5.Text = "Other";
            this.labelClass5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample5Nr
            // 
            this.labelSample5Nr.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSample5Nr.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold);
            this.labelSample5Nr.Location = new System.Drawing.Point(0, 0);
            this.labelSample5Nr.Name = "labelSample5Nr";
            this.labelSample5Nr.Size = new System.Drawing.Size(60, 38);
            this.labelSample5Nr.TabIndex = 7;
            this.labelSample5Nr.Text = "5";
            this.labelSample5Nr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample5EventNr
            // 
            this.labelSample5EventNr.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample5EventNr.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelSample5EventNr.Location = new System.Drawing.Point(1540, 0);
            this.labelSample5EventNr.Name = "labelSample5EventNr";
            this.labelSample5EventNr.Size = new System.Drawing.Size(112, 38);
            this.labelSample5EventNr.TabIndex = 4;
            this.labelSample5EventNr.Text = "^9999";
            this.labelSample5EventNr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel468
            // 
            this.panel468.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel468.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel468.Location = new System.Drawing.Point(0, 0);
            this.panel468.Name = "panel468";
            this.panel468.Size = new System.Drawing.Size(1654, 1);
            this.panel468.TabIndex = 3;
            // 
            // panel13
            // 
            this.panel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel13.Location = new System.Drawing.Point(0, 1562);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(1654, 17);
            this.panel13.TabIndex = 50;
            // 
            // panelEventSample4
            // 
            this.panelEventSample4.AutoSize = true;
            this.panelEventSample4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panelEventSample4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelEventSample4.Controls.Add(this.panel136);
            this.panelEventSample4.Controls.Add(this.panel266);
            this.panelEventSample4.Controls.Add(this.panel275);
            this.panelEventSample4.Controls.Add(this.panel305);
            this.panelEventSample4.Controls.Add(this.panel306);
            this.panelEventSample4.Controls.Add(this.panel328);
            this.panelEventSample4.Controls.Add(this.panel357);
            this.panelEventSample4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelEventSample4.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.panelEventSample4.Location = new System.Drawing.Point(0, 882);
            this.panelEventSample4.Name = "panelEventSample4";
            this.panelEventSample4.Size = new System.Drawing.Size(1654, 680);
            this.panelEventSample4.TabIndex = 49;
            // 
            // panel136
            // 
            this.panel136.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel136.Controls.Add(this.panel139);
            this.panel136.Controls.Add(this.panel141);
            this.panel136.Controls.Add(this.panel143);
            this.panel136.Controls.Add(this.panel144);
            this.panel136.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel136.Location = new System.Drawing.Point(0, 532);
            this.panel136.Name = "panel136";
            this.panel136.Size = new System.Drawing.Size(1652, 146);
            this.panel136.TabIndex = 58;
            // 
            // panel139
            // 
            this.panel139.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel139.Controls.Add(this.labelFindings4);
            this.panel139.Controls.Add(this.panel140);
            this.panel139.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel139.Location = new System.Drawing.Point(559, 0);
            this.panel139.Name = "panel139";
            this.panel139.Size = new System.Drawing.Size(1091, 144);
            this.panel139.TabIndex = 7;
            // 
            // labelFindings4
            // 
            this.labelFindings4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelFindings4.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelFindings4.Location = new System.Drawing.Point(0, 38);
            this.labelFindings4.Name = "labelFindings4";
            this.labelFindings4.Size = new System.Drawing.Size(1089, 104);
            this.labelFindings4.TabIndex = 6;
            this.labelFindings4.Text = "Atrial Fibrillation, with Occasional PVC\'s and many other findings that the Tech " +
    "can add a lot of text \r\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
            // 
            // panel140
            // 
            this.panel140.Controls.Add(this.labelRhythms4);
            this.panel140.Controls.Add(this.labelFindingsText4);
            this.panel140.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel140.Location = new System.Drawing.Point(0, 0);
            this.panel140.Name = "panel140";
            this.panel140.Size = new System.Drawing.Size(1089, 38);
            this.panel140.TabIndex = 2;
            // 
            // labelRhythms4
            // 
            this.labelRhythms4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelRhythms4.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelRhythms4.Location = new System.Drawing.Point(154, 0);
            this.labelRhythms4.Name = "labelRhythms4";
            this.labelRhythms4.Size = new System.Drawing.Size(935, 38);
            this.labelRhythms4.TabIndex = 70;
            this.labelRhythms4.Text = "Rhythms";
            // 
            // labelFindingsText4
            // 
            this.labelFindingsText4.AutoSize = true;
            this.labelFindingsText4.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelFindingsText4.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelFindingsText4.Location = new System.Drawing.Point(0, 0);
            this.labelFindingsText4.Name = "labelFindingsText4";
            this.labelFindingsText4.Size = new System.Drawing.Size(154, 32);
            this.labelFindingsText4.TabIndex = 2;
            this.labelFindingsText4.Text = "Findings:";
            // 
            // panel141
            // 
            this.panel141.AutoSize = true;
            this.panel141.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel141.Location = new System.Drawing.Point(559, 0);
            this.panel141.Name = "panel141";
            this.panel141.Size = new System.Drawing.Size(0, 144);
            this.panel141.TabIndex = 6;
            // 
            // panel143
            // 
            this.panel143.AutoSize = true;
            this.panel143.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel143.Location = new System.Drawing.Point(559, 0);
            this.panel143.Name = "panel143";
            this.panel143.Size = new System.Drawing.Size(0, 144);
            this.panel143.TabIndex = 3;
            // 
            // panel144
            // 
            this.panel144.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel144.Controls.Add(this.panel147);
            this.panel144.Controls.Add(this.panel148);
            this.panel144.Controls.Add(this.panel150);
            this.panel144.Controls.Add(this.panel151);
            this.panel144.Controls.Add(this.panel152);
            this.panel144.Controls.Add(this.panel157);
            this.panel144.Controls.Add(this.label187);
            this.panel144.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel144.Location = new System.Drawing.Point(0, 0);
            this.panel144.Name = "panel144";
            this.panel144.Size = new System.Drawing.Size(559, 144);
            this.panel144.TabIndex = 9;
            // 
            // panel147
            // 
            this.panel147.Controls.Add(this.unitQt4);
            this.panel147.Controls.Add(this.unitQrs4);
            this.panel147.Controls.Add(this.unitPr4);
            this.panel147.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel147.Location = new System.Drawing.Point(490, 38);
            this.panel147.Name = "panel147";
            this.panel147.Size = new System.Drawing.Size(64, 104);
            this.panel147.TabIndex = 11;
            // 
            // unitQt4
            // 
            this.unitQt4.Dock = System.Windows.Forms.DockStyle.Top;
            this.unitQt4.Font = new System.Drawing.Font("Verdana", 20F);
            this.unitQt4.Location = new System.Drawing.Point(0, 68);
            this.unitQt4.Name = "unitQt4";
            this.unitQt4.Size = new System.Drawing.Size(64, 34);
            this.unitQt4.TabIndex = 73;
            this.unitQt4.Text = "sec";
            // 
            // unitQrs4
            // 
            this.unitQrs4.Dock = System.Windows.Forms.DockStyle.Top;
            this.unitQrs4.Font = new System.Drawing.Font("Verdana", 20F);
            this.unitQrs4.Location = new System.Drawing.Point(0, 34);
            this.unitQrs4.Name = "unitQrs4";
            this.unitQrs4.Size = new System.Drawing.Size(64, 34);
            this.unitQrs4.TabIndex = 72;
            this.unitQrs4.Text = "sec";
            // 
            // unitPr4
            // 
            this.unitPr4.Dock = System.Windows.Forms.DockStyle.Top;
            this.unitPr4.Font = new System.Drawing.Font("Verdana", 20F);
            this.unitPr4.Location = new System.Drawing.Point(0, 0);
            this.unitPr4.Name = "unitPr4";
            this.unitPr4.Size = new System.Drawing.Size(64, 34);
            this.unitPr4.TabIndex = 71;
            this.unitPr4.Text = "sec";
            // 
            // panel148
            // 
            this.panel148.Controls.Add(this.valueQt4);
            this.panel148.Controls.Add(this.valueQrs4);
            this.panel148.Controls.Add(this.valuePr4);
            this.panel148.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel148.Location = new System.Drawing.Point(390, 38);
            this.panel148.Name = "panel148";
            this.panel148.Size = new System.Drawing.Size(100, 104);
            this.panel148.TabIndex = 10;
            // 
            // valueQt4
            // 
            this.valueQt4.Dock = System.Windows.Forms.DockStyle.Top;
            this.valueQt4.Font = new System.Drawing.Font("Verdana", 20F);
            this.valueQt4.Location = new System.Drawing.Point(0, 68);
            this.valueQt4.Name = "valueQt4";
            this.valueQt4.Size = new System.Drawing.Size(100, 34);
            this.valueQt4.TabIndex = 76;
            this.valueQt4.Text = "0,465";
            // 
            // valueQrs4
            // 
            this.valueQrs4.Dock = System.Windows.Forms.DockStyle.Top;
            this.valueQrs4.Font = new System.Drawing.Font("Verdana", 20F);
            this.valueQrs4.Location = new System.Drawing.Point(0, 34);
            this.valueQrs4.Name = "valueQrs4";
            this.valueQrs4.Size = new System.Drawing.Size(100, 34);
            this.valueQrs4.TabIndex = 71;
            this.valueQrs4.Text = "0,113";
            // 
            // valuePr4
            // 
            this.valuePr4.Dock = System.Windows.Forms.DockStyle.Top;
            this.valuePr4.Font = new System.Drawing.Font("Verdana", 20F);
            this.valuePr4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.valuePr4.Location = new System.Drawing.Point(0, 0);
            this.valuePr4.Name = "valuePr4";
            this.valuePr4.Size = new System.Drawing.Size(100, 34);
            this.valuePr4.TabIndex = 65;
            this.valuePr4.Text = "0,202";
            // 
            // panel150
            // 
            this.panel150.Controls.Add(this.headerQt4);
            this.panel150.Controls.Add(this.headerQrs4);
            this.panel150.Controls.Add(this.headerPr4);
            this.panel150.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel150.Location = new System.Drawing.Point(299, 38);
            this.panel150.Name = "panel150";
            this.panel150.Size = new System.Drawing.Size(91, 104);
            this.panel150.TabIndex = 9;
            // 
            // headerQt4
            // 
            this.headerQt4.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerQt4.Font = new System.Drawing.Font("Verdana", 20F);
            this.headerQt4.Location = new System.Drawing.Point(0, 68);
            this.headerQt4.Name = "headerQt4";
            this.headerQt4.Size = new System.Drawing.Size(91, 34);
            this.headerQt4.TabIndex = 76;
            this.headerQt4.Text = "QT:";
            // 
            // headerQrs4
            // 
            this.headerQrs4.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerQrs4.Font = new System.Drawing.Font("Verdana", 20F);
            this.headerQrs4.Location = new System.Drawing.Point(0, 34);
            this.headerQrs4.Name = "headerQrs4";
            this.headerQrs4.Size = new System.Drawing.Size(91, 34);
            this.headerQrs4.TabIndex = 75;
            this.headerQrs4.Text = "QRS:";
            // 
            // headerPr4
            // 
            this.headerPr4.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPr4.Font = new System.Drawing.Font("Verdana", 20F);
            this.headerPr4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.headerPr4.Location = new System.Drawing.Point(0, 0);
            this.headerPr4.Name = "headerPr4";
            this.headerPr4.Size = new System.Drawing.Size(91, 34);
            this.headerPr4.TabIndex = 74;
            this.headerPr4.Text = "PR:";
            // 
            // panel151
            // 
            this.panel151.Controls.Add(this.unitMaxHr4);
            this.panel151.Controls.Add(this.unitMeanHr4);
            this.panel151.Controls.Add(this.unitMinHr4);
            this.panel151.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel151.Location = new System.Drawing.Point(220, 38);
            this.panel151.Name = "panel151";
            this.panel151.Size = new System.Drawing.Size(79, 104);
            this.panel151.TabIndex = 8;
            // 
            // unitMaxHr4
            // 
            this.unitMaxHr4.Dock = System.Windows.Forms.DockStyle.Top;
            this.unitMaxHr4.Font = new System.Drawing.Font("Verdana", 20F);
            this.unitMaxHr4.Location = new System.Drawing.Point(0, 68);
            this.unitMaxHr4.Name = "unitMaxHr4";
            this.unitMaxHr4.Size = new System.Drawing.Size(79, 34);
            this.unitMaxHr4.TabIndex = 64;
            this.unitMaxHr4.Text = "bpm";
            // 
            // unitMeanHr4
            // 
            this.unitMeanHr4.Dock = System.Windows.Forms.DockStyle.Top;
            this.unitMeanHr4.Font = new System.Drawing.Font("Verdana", 20F);
            this.unitMeanHr4.Location = new System.Drawing.Point(0, 34);
            this.unitMeanHr4.Name = "unitMeanHr4";
            this.unitMeanHr4.Size = new System.Drawing.Size(79, 34);
            this.unitMeanHr4.TabIndex = 63;
            this.unitMeanHr4.Text = "bpm";
            // 
            // unitMinHr4
            // 
            this.unitMinHr4.Dock = System.Windows.Forms.DockStyle.Top;
            this.unitMinHr4.Font = new System.Drawing.Font("Verdana", 20F);
            this.unitMinHr4.Location = new System.Drawing.Point(0, 0);
            this.unitMinHr4.Name = "unitMinHr4";
            this.unitMinHr4.Size = new System.Drawing.Size(79, 34);
            this.unitMinHr4.TabIndex = 61;
            this.unitMinHr4.Text = "bpm";
            // 
            // panel152
            // 
            this.panel152.Controls.Add(this.valueMaxHr4);
            this.panel152.Controls.Add(this.valueMeanHr4);
            this.panel152.Controls.Add(this.valueMinHr4);
            this.panel152.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel152.Location = new System.Drawing.Point(149, 38);
            this.panel152.Name = "panel152";
            this.panel152.Size = new System.Drawing.Size(71, 104);
            this.panel152.TabIndex = 7;
            // 
            // valueMaxHr4
            // 
            this.valueMaxHr4.Dock = System.Windows.Forms.DockStyle.Top;
            this.valueMaxHr4.Font = new System.Drawing.Font("Verdana", 20F);
            this.valueMaxHr4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.valueMaxHr4.Location = new System.Drawing.Point(0, 68);
            this.valueMaxHr4.Name = "valueMaxHr4";
            this.valueMaxHr4.Size = new System.Drawing.Size(71, 34);
            this.valueMaxHr4.TabIndex = 61;
            this.valueMaxHr4.Text = "120";
            // 
            // valueMeanHr4
            // 
            this.valueMeanHr4.Dock = System.Windows.Forms.DockStyle.Top;
            this.valueMeanHr4.Font = new System.Drawing.Font("Verdana", 20F);
            this.valueMeanHr4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.valueMeanHr4.Location = new System.Drawing.Point(0, 34);
            this.valueMeanHr4.Name = "valueMeanHr4";
            this.valueMeanHr4.Size = new System.Drawing.Size(71, 34);
            this.valueMeanHr4.TabIndex = 56;
            this.valueMeanHr4.Text = "110";
            // 
            // valueMinHr4
            // 
            this.valueMinHr4.Dock = System.Windows.Forms.DockStyle.Top;
            this.valueMinHr4.Font = new System.Drawing.Font("Verdana", 20F);
            this.valueMinHr4.Location = new System.Drawing.Point(0, 0);
            this.valueMinHr4.Name = "valueMinHr4";
            this.valueMinHr4.Size = new System.Drawing.Size(71, 34);
            this.valueMinHr4.TabIndex = 52;
            this.valueMinHr4.Text = "100";
            // 
            // panel157
            // 
            this.panel157.Controls.Add(this.headerMaxHr4);
            this.panel157.Controls.Add(this.headerMeanHr4);
            this.panel157.Controls.Add(this.headerMinHr4);
            this.panel157.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel157.Location = new System.Drawing.Point(0, 38);
            this.panel157.Name = "panel157";
            this.panel157.Size = new System.Drawing.Size(149, 104);
            this.panel157.TabIndex = 6;
            // 
            // headerMaxHr4
            // 
            this.headerMaxHr4.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerMaxHr4.Font = new System.Drawing.Font("Verdana", 20F);
            this.headerMaxHr4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.headerMaxHr4.Location = new System.Drawing.Point(0, 68);
            this.headerMaxHr4.Name = "headerMaxHr4";
            this.headerMaxHr4.Size = new System.Drawing.Size(149, 34);
            this.headerMaxHr4.TabIndex = 74;
            this.headerMaxHr4.Text = "Max HR:";
            // 
            // headerMeanHr4
            // 
            this.headerMeanHr4.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerMeanHr4.Font = new System.Drawing.Font("Verdana", 20F);
            this.headerMeanHr4.Location = new System.Drawing.Point(0, 34);
            this.headerMeanHr4.Name = "headerMeanHr4";
            this.headerMeanHr4.Size = new System.Drawing.Size(149, 34);
            this.headerMeanHr4.TabIndex = 64;
            this.headerMeanHr4.Text = "Mean HR:";
            // 
            // headerMinHr4
            // 
            this.headerMinHr4.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerMinHr4.Font = new System.Drawing.Font("Verdana", 20F);
            this.headerMinHr4.Location = new System.Drawing.Point(0, 0);
            this.headerMinHr4.Name = "headerMinHr4";
            this.headerMinHr4.Size = new System.Drawing.Size(149, 34);
            this.headerMinHr4.TabIndex = 63;
            this.headerMinHr4.Text = "Min HR:";
            // 
            // label187
            // 
            this.label187.Dock = System.Windows.Forms.DockStyle.Top;
            this.label187.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.label187.Location = new System.Drawing.Point(0, 0);
            this.label187.Name = "label187";
            this.label187.Size = new System.Drawing.Size(557, 38);
            this.label187.TabIndex = 5;
            this.label187.Text = "QRS Measurements:";
            // 
            // panel266
            // 
            this.panel266.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel266.Controls.Add(this.labelSample4StartFull);
            this.panel266.Controls.Add(this.labelSample4MidFull);
            this.panel266.Controls.Add(this.panel267);
            this.panel266.Controls.Add(this.panel268);
            this.panel266.Controls.Add(this.panel269);
            this.panel266.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel266.Location = new System.Drawing.Point(0, 497);
            this.panel266.Name = "panel266";
            this.panel266.Size = new System.Drawing.Size(1652, 35);
            this.panel266.TabIndex = 26;
            // 
            // labelSample4StartFull
            // 
            this.labelSample4StartFull.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSample4StartFull.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample4StartFull.Location = new System.Drawing.Point(0, 0);
            this.labelSample4StartFull.Name = "labelSample4StartFull";
            this.labelSample4StartFull.Size = new System.Drawing.Size(693, 33);
            this.labelSample4StartFull.TabIndex = 9;
            this.labelSample4StartFull.Text = "10:15:10 AM 10/12/2018  6 sec";
            this.labelSample4StartFull.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample4MidFull
            // 
            this.labelSample4MidFull.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample4MidFull.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample4MidFull.Location = new System.Drawing.Point(692, 0);
            this.labelSample4MidFull.Name = "labelSample4MidFull";
            this.labelSample4MidFull.Size = new System.Drawing.Size(208, 33);
            this.labelSample4MidFull.TabIndex = 8;
            this.labelSample4MidFull.Text = "10:15:00 AM";
            this.labelSample4MidFull.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelSample4MidFull.Visible = false;
            // 
            // panel267
            // 
            this.panel267.Controls.Add(this.labelSample4SweepFull);
            this.panel267.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel267.Location = new System.Drawing.Point(900, 0);
            this.panel267.Name = "panel267";
            this.panel267.Size = new System.Drawing.Size(290, 33);
            this.panel267.TabIndex = 7;
            // 
            // labelSample4SweepFull
            // 
            this.labelSample4SweepFull.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSample4SweepFull.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample4SweepFull.Location = new System.Drawing.Point(0, 0);
            this.labelSample4SweepFull.Name = "labelSample4SweepFull";
            this.labelSample4SweepFull.Size = new System.Drawing.Size(290, 33);
            this.labelSample4SweepFull.TabIndex = 3;
            this.labelSample4SweepFull.Text = "25 mm/sec (0.20 Sec)";
            this.labelSample4SweepFull.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel268
            // 
            this.panel268.Controls.Add(this.labelSample4AmplFull);
            this.panel268.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel268.Location = new System.Drawing.Point(1190, 0);
            this.panel268.Name = "panel268";
            this.panel268.Size = new System.Drawing.Size(285, 33);
            this.panel268.TabIndex = 6;
            // 
            // labelSample4AmplFull
            // 
            this.labelSample4AmplFull.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSample4AmplFull.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample4AmplFull.Location = new System.Drawing.Point(0, 0);
            this.labelSample4AmplFull.Name = "labelSample4AmplFull";
            this.labelSample4AmplFull.Size = new System.Drawing.Size(285, 33);
            this.labelSample4AmplFull.TabIndex = 2;
            this.labelSample4AmplFull.Text = "10 mm/mV (0.50 mV)";
            this.labelSample4AmplFull.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel269
            // 
            this.panel269.Controls.Add(this.labelSample4EndFull);
            this.panel269.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel269.Location = new System.Drawing.Point(1475, 0);
            this.panel269.Name = "panel269";
            this.panel269.Size = new System.Drawing.Size(175, 33);
            this.panel269.TabIndex = 5;
            // 
            // labelSample4EndFull
            // 
            this.labelSample4EndFull.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample4EndFull.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample4EndFull.Location = new System.Drawing.Point(-75, 0);
            this.labelSample4EndFull.Name = "labelSample4EndFull";
            this.labelSample4EndFull.Size = new System.Drawing.Size(250, 33);
            this.labelSample4EndFull.TabIndex = 1;
            this.labelSample4EndFull.Text = "10:15:30 AM";
            this.labelSample4EndFull.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel275
            // 
            this.panel275.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel275.Controls.Add(this.panel297);
            this.panel275.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel275.Location = new System.Drawing.Point(0, 287);
            this.panel275.Name = "panel275";
            this.panel275.Size = new System.Drawing.Size(1652, 210);
            this.panel275.TabIndex = 25;
            // 
            // panel297
            // 
            this.panel297.Controls.Add(this.labelSample4LeadFull);
            this.panel297.Controls.Add(this.pictureBoxSample4Full);
            this.panel297.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel297.Location = new System.Drawing.Point(0, 0);
            this.panel297.Name = "panel297";
            this.panel297.Size = new System.Drawing.Size(1650, 208);
            this.panel297.TabIndex = 2;
            // 
            // labelSample4LeadFull
            // 
            this.labelSample4LeadFull.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSample4LeadFull.Location = new System.Drawing.Point(14, 49);
            this.labelSample4LeadFull.Name = "labelSample4LeadFull";
            this.labelSample4LeadFull.Size = new System.Drawing.Size(223, 45);
            this.labelSample4LeadFull.TabIndex = 47;
            this.labelSample4LeadFull.Text = "LEAD I";
            this.labelSample4LeadFull.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBoxSample4Full
            // 
            this.pictureBoxSample4Full.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxSample4Full.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxSample4Full.Name = "pictureBoxSample4Full";
            this.pictureBoxSample4Full.Size = new System.Drawing.Size(1650, 208);
            this.pictureBoxSample4Full.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxSample4Full.TabIndex = 0;
            this.pictureBoxSample4Full.TabStop = false;
            // 
            // panel305
            // 
            this.panel305.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel305.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel305.Location = new System.Drawing.Point(0, 285);
            this.panel305.Name = "panel305";
            this.panel305.Size = new System.Drawing.Size(1652, 2);
            this.panel305.TabIndex = 20;
            // 
            // panel306
            // 
            this.panel306.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel306.Controls.Add(this.labelSample4Start);
            this.panel306.Controls.Add(this.labelSample4Mid);
            this.panel306.Controls.Add(this.panel311);
            this.panel306.Controls.Add(this.panel312);
            this.panel306.Controls.Add(this.panel313);
            this.panel306.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel306.Location = new System.Drawing.Point(0, 250);
            this.panel306.Name = "panel306";
            this.panel306.Size = new System.Drawing.Size(1652, 35);
            this.panel306.TabIndex = 11;
            // 
            // labelSample4Start
            // 
            this.labelSample4Start.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSample4Start.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample4Start.Location = new System.Drawing.Point(0, 0);
            this.labelSample4Start.Name = "labelSample4Start";
            this.labelSample4Start.Size = new System.Drawing.Size(693, 33);
            this.labelSample4Start.TabIndex = 8;
            this.labelSample4Start.Text = "10:15:10 AM 10/12/2018  6 sec";
            this.labelSample4Start.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample4Mid
            // 
            this.labelSample4Mid.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample4Mid.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample4Mid.Location = new System.Drawing.Point(692, 0);
            this.labelSample4Mid.Name = "labelSample4Mid";
            this.labelSample4Mid.Size = new System.Drawing.Size(208, 33);
            this.labelSample4Mid.TabIndex = 7;
            this.labelSample4Mid.Text = "10:15:15 AM";
            this.labelSample4Mid.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelSample4Mid.Visible = false;
            // 
            // panel311
            // 
            this.panel311.Controls.Add(this.labelSample4Sweep);
            this.panel311.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel311.Location = new System.Drawing.Point(900, 0);
            this.panel311.Name = "panel311";
            this.panel311.Size = new System.Drawing.Size(290, 33);
            this.panel311.TabIndex = 4;
            // 
            // labelSample4Sweep
            // 
            this.labelSample4Sweep.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSample4Sweep.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample4Sweep.Location = new System.Drawing.Point(0, 0);
            this.labelSample4Sweep.Name = "labelSample4Sweep";
            this.labelSample4Sweep.Size = new System.Drawing.Size(290, 33);
            this.labelSample4Sweep.TabIndex = 1;
            this.labelSample4Sweep.Text = "25 mm/sec (0.20 Sec)";
            this.labelSample4Sweep.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel312
            // 
            this.panel312.Controls.Add(this.labelSample4Ampl);
            this.panel312.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel312.Location = new System.Drawing.Point(1190, 0);
            this.panel312.Name = "panel312";
            this.panel312.Size = new System.Drawing.Size(285, 33);
            this.panel312.TabIndex = 3;
            // 
            // labelSample4Ampl
            // 
            this.labelSample4Ampl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSample4Ampl.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample4Ampl.Location = new System.Drawing.Point(0, 0);
            this.labelSample4Ampl.Name = "labelSample4Ampl";
            this.labelSample4Ampl.Size = new System.Drawing.Size(285, 33);
            this.labelSample4Ampl.TabIndex = 2;
            this.labelSample4Ampl.Text = "10 mm/mV (0.50 mV)";
            this.labelSample4Ampl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel313
            // 
            this.panel313.Controls.Add(this.labelSample4End);
            this.panel313.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel313.Location = new System.Drawing.Point(1475, 0);
            this.panel313.Name = "panel313";
            this.panel313.Size = new System.Drawing.Size(175, 33);
            this.panel313.TabIndex = 2;
            // 
            // labelSample4End
            // 
            this.labelSample4End.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSample4End.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample4End.Location = new System.Drawing.Point(0, 0);
            this.labelSample4End.Name = "labelSample4End";
            this.labelSample4End.Size = new System.Drawing.Size(175, 33);
            this.labelSample4End.TabIndex = 2;
            this.labelSample4End.Text = "10:15:20 AM";
            this.labelSample4End.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel328
            // 
            this.panel328.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel328.Controls.Add(this.panel329);
            this.panel328.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel328.Location = new System.Drawing.Point(0, 40);
            this.panel328.Name = "panel328";
            this.panel328.Size = new System.Drawing.Size(1652, 210);
            this.panel328.TabIndex = 10;
            // 
            // panel329
            // 
            this.panel329.Controls.Add(this.labelSample4Lead);
            this.panel329.Controls.Add(this.pictureBoxSample4);
            this.panel329.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel329.Location = new System.Drawing.Point(0, 0);
            this.panel329.Name = "panel329";
            this.panel329.Size = new System.Drawing.Size(1650, 208);
            this.panel329.TabIndex = 2;
            // 
            // labelSample4Lead
            // 
            this.labelSample4Lead.Font = new System.Drawing.Font("Verdana", 28F);
            this.labelSample4Lead.Location = new System.Drawing.Point(9, 55);
            this.labelSample4Lead.Name = "labelSample4Lead";
            this.labelSample4Lead.Size = new System.Drawing.Size(217, 49);
            this.labelSample4Lead.TabIndex = 46;
            this.labelSample4Lead.Text = "LEAD I";
            this.labelSample4Lead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelSample4Lead.Visible = false;
            // 
            // pictureBoxSample4
            // 
            this.pictureBoxSample4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxSample4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxSample4.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxSample4.InitialImage")));
            this.pictureBoxSample4.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxSample4.Name = "pictureBoxSample4";
            this.pictureBoxSample4.Size = new System.Drawing.Size(1650, 208);
            this.pictureBoxSample4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxSample4.TabIndex = 0;
            this.pictureBoxSample4.TabStop = false;
            // 
            // panel357
            // 
            this.panel357.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel357.Controls.Add(this.labelSample4Date);
            this.panel357.Controls.Add(this.label131);
            this.panel357.Controls.Add(this.labelSample4Time);
            this.panel357.Controls.Add(this.labelEvent4);
            this.panel357.Controls.Add(this.labelClass4);
            this.panel357.Controls.Add(this.labelSample4Nr);
            this.panel357.Controls.Add(this.labelSample4EventNr);
            this.panel357.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel357.Location = new System.Drawing.Point(0, 0);
            this.panel357.Name = "panel357";
            this.panel357.Size = new System.Drawing.Size(1652, 40);
            this.panel357.TabIndex = 7;
            // 
            // labelSample4Date
            // 
            this.labelSample4Date.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample4Date.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelSample4Date.Location = new System.Drawing.Point(1008, 0);
            this.labelSample4Date.Name = "labelSample4Date";
            this.labelSample4Date.Size = new System.Drawing.Size(200, 38);
            this.labelSample4Date.TabIndex = 21;
            this.labelSample4Date.Text = "11/23/2016";
            this.labelSample4Date.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label131
            // 
            this.label131.Dock = System.Windows.Forms.DockStyle.Right;
            this.label131.Font = new System.Drawing.Font("Verdana", 20F);
            this.label131.Location = new System.Drawing.Point(1208, 0);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(17, 38);
            this.label131.TabIndex = 20;
            this.label131.Text = "-";
            this.label131.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelSample4Time
            // 
            this.labelSample4Time.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample4Time.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelSample4Time.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelSample4Time.Location = new System.Drawing.Point(1225, 0);
            this.labelSample4Time.Name = "labelSample4Time";
            this.labelSample4Time.Size = new System.Drawing.Size(205, 38);
            this.labelSample4Time.TabIndex = 19;
            this.labelSample4Time.Text = "10:15:10 AM";
            this.labelSample4Time.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelEvent4
            // 
            this.labelEvent4.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelEvent4.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelEvent4.Location = new System.Drawing.Point(1430, 0);
            this.labelEvent4.Name = "labelEvent4";
            this.labelEvent4.Size = new System.Drawing.Size(108, 38);
            this.labelEvent4.TabIndex = 10;
            this.labelEvent4.Text = "Event:";
            this.labelEvent4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelClass4
            // 
            this.labelClass4.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelClass4.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold);
            this.labelClass4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelClass4.Location = new System.Drawing.Point(60, 0);
            this.labelClass4.Name = "labelClass4";
            this.labelClass4.Size = new System.Drawing.Size(477, 38);
            this.labelClass4.TabIndex = 22;
            this.labelClass4.Text = "Other";
            this.labelClass4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample4Nr
            // 
            this.labelSample4Nr.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSample4Nr.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold);
            this.labelSample4Nr.Location = new System.Drawing.Point(0, 0);
            this.labelSample4Nr.Name = "labelSample4Nr";
            this.labelSample4Nr.Size = new System.Drawing.Size(60, 38);
            this.labelSample4Nr.TabIndex = 14;
            this.labelSample4Nr.Text = "4";
            this.labelSample4Nr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample4EventNr
            // 
            this.labelSample4EventNr.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample4EventNr.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelSample4EventNr.Location = new System.Drawing.Point(1538, 0);
            this.labelSample4EventNr.Name = "labelSample4EventNr";
            this.labelSample4EventNr.Size = new System.Drawing.Size(112, 38);
            this.labelSample4EventNr.TabIndex = 11;
            this.labelSample4EventNr.Text = "^9999";
            this.labelSample4EventNr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel12
            // 
            this.panel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel12.Location = new System.Drawing.Point(0, 865);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(1654, 17);
            this.panel12.TabIndex = 48;
            // 
            // panelEventSample3
            // 
            this.panelEventSample3.AutoSize = true;
            this.panelEventSample3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panelEventSample3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelEventSample3.Controls.Add(this.panel32);
            this.panelEventSample3.Controls.Add(this.panel277);
            this.panelEventSample3.Controls.Add(this.panel283);
            this.panelEventSample3.Controls.Add(this.panel300);
            this.panelEventSample3.Controls.Add(this.panel384);
            this.panelEventSample3.Controls.Add(this.panel387);
            this.panelEventSample3.Controls.Add(this.panel416);
            this.panelEventSample3.Controls.Add(this.panel450);
            this.panelEventSample3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelEventSample3.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.panelEventSample3.Location = new System.Drawing.Point(0, 183);
            this.panelEventSample3.Name = "panelEventSample3";
            this.panelEventSample3.Size = new System.Drawing.Size(1654, 682);
            this.panelEventSample3.TabIndex = 47;
            // 
            // panel32
            // 
            this.panel32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel32.Controls.Add(this.panel103);
            this.panel32.Controls.Add(this.panel112);
            this.panel32.Controls.Add(this.panel120);
            this.panel32.Controls.Add(this.panel129);
            this.panel32.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel32.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel32.Location = new System.Drawing.Point(0, 533);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(1652, 147);
            this.panel32.TabIndex = 58;
            // 
            // panel103
            // 
            this.panel103.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel103.Controls.Add(this.labelFindings3);
            this.panel103.Controls.Add(this.panel104);
            this.panel103.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel103.Location = new System.Drawing.Point(559, 0);
            this.panel103.Name = "panel103";
            this.panel103.Size = new System.Drawing.Size(1091, 145);
            this.panel103.TabIndex = 7;
            // 
            // labelFindings3
            // 
            this.labelFindings3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelFindings3.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelFindings3.Location = new System.Drawing.Point(0, 39);
            this.labelFindings3.Name = "labelFindings3";
            this.labelFindings3.Size = new System.Drawing.Size(1089, 104);
            this.labelFindings3.TabIndex = 6;
            this.labelFindings3.Text = "Atrial Fibrillation, with Occasional PVC\'s and many other findings that the Tech " +
    "can add a lot of text \r\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx \r\ny" +
    "yyyyyyyyyyyyyyyyyyyy\r\n\r\n";
            // 
            // panel104
            // 
            this.panel104.Controls.Add(this.labelRhythms3);
            this.panel104.Controls.Add(this.labelFindingsText3);
            this.panel104.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel104.Location = new System.Drawing.Point(0, 0);
            this.panel104.Name = "panel104";
            this.panel104.Size = new System.Drawing.Size(1089, 39);
            this.panel104.TabIndex = 2;
            // 
            // labelRhythms3
            // 
            this.labelRhythms3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelRhythms3.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelRhythms3.Location = new System.Drawing.Point(203, 0);
            this.labelRhythms3.Name = "labelRhythms3";
            this.labelRhythms3.Size = new System.Drawing.Size(886, 39);
            this.labelRhythms3.TabIndex = 70;
            this.labelRhythms3.Text = "Rhythms";
            // 
            // labelFindingsText3
            // 
            this.labelFindingsText3.AutoSize = true;
            this.labelFindingsText3.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelFindingsText3.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.labelFindingsText3.Location = new System.Drawing.Point(0, 0);
            this.labelFindingsText3.Name = "labelFindingsText3";
            this.labelFindingsText3.Size = new System.Drawing.Size(203, 29);
            this.labelFindingsText3.TabIndex = 2;
            this.labelFindingsText3.Text = "^QC Findings:";
            // 
            // panel112
            // 
            this.panel112.AutoSize = true;
            this.panel112.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel112.Location = new System.Drawing.Point(559, 0);
            this.panel112.Name = "panel112";
            this.panel112.Size = new System.Drawing.Size(0, 145);
            this.panel112.TabIndex = 6;
            // 
            // panel120
            // 
            this.panel120.AutoSize = true;
            this.panel120.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel120.Location = new System.Drawing.Point(559, 0);
            this.panel120.Name = "panel120";
            this.panel120.Size = new System.Drawing.Size(0, 145);
            this.panel120.TabIndex = 3;
            // 
            // panel129
            // 
            this.panel129.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel129.Controls.Add(this.panel130);
            this.panel129.Controls.Add(this.panel131);
            this.panel129.Controls.Add(this.panel132);
            this.panel129.Controls.Add(this.panel133);
            this.panel129.Controls.Add(this.panel134);
            this.panel129.Controls.Add(this.panel135);
            this.panel129.Controls.Add(this.label161);
            this.panel129.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel129.Location = new System.Drawing.Point(0, 0);
            this.panel129.Name = "panel129";
            this.panel129.Size = new System.Drawing.Size(559, 145);
            this.panel129.TabIndex = 9;
            // 
            // panel130
            // 
            this.panel130.Controls.Add(this.unitQt3);
            this.panel130.Controls.Add(this.unitQrs3);
            this.panel130.Controls.Add(this.unitPr3);
            this.panel130.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel130.Location = new System.Drawing.Point(488, 39);
            this.panel130.Name = "panel130";
            this.panel130.Size = new System.Drawing.Size(66, 104);
            this.panel130.TabIndex = 11;
            // 
            // unitQt3
            // 
            this.unitQt3.Dock = System.Windows.Forms.DockStyle.Top;
            this.unitQt3.Font = new System.Drawing.Font("Verdana", 20F);
            this.unitQt3.Location = new System.Drawing.Point(0, 68);
            this.unitQt3.Name = "unitQt3";
            this.unitQt3.Size = new System.Drawing.Size(66, 34);
            this.unitQt3.TabIndex = 73;
            this.unitQt3.Text = "sec";
            // 
            // unitQrs3
            // 
            this.unitQrs3.Dock = System.Windows.Forms.DockStyle.Top;
            this.unitQrs3.Font = new System.Drawing.Font("Verdana", 20F);
            this.unitQrs3.Location = new System.Drawing.Point(0, 34);
            this.unitQrs3.Name = "unitQrs3";
            this.unitQrs3.Size = new System.Drawing.Size(66, 34);
            this.unitQrs3.TabIndex = 72;
            this.unitQrs3.Text = "sec";
            // 
            // unitPr3
            // 
            this.unitPr3.Dock = System.Windows.Forms.DockStyle.Top;
            this.unitPr3.Font = new System.Drawing.Font("Verdana", 20F);
            this.unitPr3.Location = new System.Drawing.Point(0, 0);
            this.unitPr3.Name = "unitPr3";
            this.unitPr3.Size = new System.Drawing.Size(66, 34);
            this.unitPr3.TabIndex = 71;
            this.unitPr3.Text = "sec";
            // 
            // panel131
            // 
            this.panel131.Controls.Add(this.valueQt3);
            this.panel131.Controls.Add(this.valueQrs3);
            this.panel131.Controls.Add(this.valuePr3);
            this.panel131.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel131.Location = new System.Drawing.Point(390, 39);
            this.panel131.Name = "panel131";
            this.panel131.Size = new System.Drawing.Size(98, 104);
            this.panel131.TabIndex = 10;
            // 
            // valueQt3
            // 
            this.valueQt3.Dock = System.Windows.Forms.DockStyle.Top;
            this.valueQt3.Font = new System.Drawing.Font("Verdana", 20F);
            this.valueQt3.Location = new System.Drawing.Point(0, 68);
            this.valueQt3.Name = "valueQt3";
            this.valueQt3.Size = new System.Drawing.Size(98, 34);
            this.valueQt3.TabIndex = 76;
            this.valueQt3.Text = "0,465";
            // 
            // valueQrs3
            // 
            this.valueQrs3.Dock = System.Windows.Forms.DockStyle.Top;
            this.valueQrs3.Font = new System.Drawing.Font("Verdana", 20F);
            this.valueQrs3.Location = new System.Drawing.Point(0, 34);
            this.valueQrs3.Name = "valueQrs3";
            this.valueQrs3.Size = new System.Drawing.Size(98, 34);
            this.valueQrs3.TabIndex = 71;
            this.valueQrs3.Text = "0,113";
            // 
            // valuePr3
            // 
            this.valuePr3.Dock = System.Windows.Forms.DockStyle.Top;
            this.valuePr3.Font = new System.Drawing.Font("Verdana", 20F);
            this.valuePr3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.valuePr3.Location = new System.Drawing.Point(0, 0);
            this.valuePr3.Name = "valuePr3";
            this.valuePr3.Size = new System.Drawing.Size(98, 34);
            this.valuePr3.TabIndex = 65;
            this.valuePr3.Text = "0,202";
            // 
            // panel132
            // 
            this.panel132.Controls.Add(this.headerQt3);
            this.panel132.Controls.Add(this.headerQrs3);
            this.panel132.Controls.Add(this.headerPr3);
            this.panel132.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel132.Location = new System.Drawing.Point(296, 39);
            this.panel132.Name = "panel132";
            this.panel132.Size = new System.Drawing.Size(94, 104);
            this.panel132.TabIndex = 9;
            // 
            // headerQt3
            // 
            this.headerQt3.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerQt3.Font = new System.Drawing.Font("Verdana", 20F);
            this.headerQt3.Location = new System.Drawing.Point(0, 68);
            this.headerQt3.Name = "headerQt3";
            this.headerQt3.Size = new System.Drawing.Size(94, 34);
            this.headerQt3.TabIndex = 76;
            this.headerQt3.Text = "QT:";
            // 
            // headerQrs3
            // 
            this.headerQrs3.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerQrs3.Font = new System.Drawing.Font("Verdana", 20F);
            this.headerQrs3.Location = new System.Drawing.Point(0, 34);
            this.headerQrs3.Name = "headerQrs3";
            this.headerQrs3.Size = new System.Drawing.Size(94, 34);
            this.headerQrs3.TabIndex = 75;
            this.headerQrs3.Text = "QRS:";
            // 
            // headerPr3
            // 
            this.headerPr3.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPr3.Font = new System.Drawing.Font("Verdana", 20F);
            this.headerPr3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.headerPr3.Location = new System.Drawing.Point(0, 0);
            this.headerPr3.Name = "headerPr3";
            this.headerPr3.Size = new System.Drawing.Size(94, 34);
            this.headerPr3.TabIndex = 74;
            this.headerPr3.Text = "PR:";
            // 
            // panel133
            // 
            this.panel133.Controls.Add(this.unitMaxHr3);
            this.panel133.Controls.Add(this.unitMeanHr3);
            this.panel133.Controls.Add(this.unitMinHr3);
            this.panel133.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel133.Location = new System.Drawing.Point(220, 39);
            this.panel133.Name = "panel133";
            this.panel133.Size = new System.Drawing.Size(76, 104);
            this.panel133.TabIndex = 8;
            // 
            // unitMaxHr3
            // 
            this.unitMaxHr3.Dock = System.Windows.Forms.DockStyle.Top;
            this.unitMaxHr3.Font = new System.Drawing.Font("Verdana", 20F);
            this.unitMaxHr3.Location = new System.Drawing.Point(0, 68);
            this.unitMaxHr3.Name = "unitMaxHr3";
            this.unitMaxHr3.Size = new System.Drawing.Size(76, 34);
            this.unitMaxHr3.TabIndex = 64;
            this.unitMaxHr3.Text = "bpm";
            // 
            // unitMeanHr3
            // 
            this.unitMeanHr3.Dock = System.Windows.Forms.DockStyle.Top;
            this.unitMeanHr3.Font = new System.Drawing.Font("Verdana", 20F);
            this.unitMeanHr3.Location = new System.Drawing.Point(0, 34);
            this.unitMeanHr3.Name = "unitMeanHr3";
            this.unitMeanHr3.Size = new System.Drawing.Size(76, 34);
            this.unitMeanHr3.TabIndex = 63;
            this.unitMeanHr3.Text = "bpm";
            // 
            // unitMinHr3
            // 
            this.unitMinHr3.Dock = System.Windows.Forms.DockStyle.Top;
            this.unitMinHr3.Font = new System.Drawing.Font("Verdana", 20F);
            this.unitMinHr3.Location = new System.Drawing.Point(0, 0);
            this.unitMinHr3.Name = "unitMinHr3";
            this.unitMinHr3.Size = new System.Drawing.Size(76, 34);
            this.unitMinHr3.TabIndex = 61;
            this.unitMinHr3.Text = "bpm";
            // 
            // panel134
            // 
            this.panel134.Controls.Add(this.valueMaxHr3);
            this.panel134.Controls.Add(this.valueMeanHr3);
            this.panel134.Controls.Add(this.valueMinHr3);
            this.panel134.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel134.Location = new System.Drawing.Point(149, 39);
            this.panel134.Name = "panel134";
            this.panel134.Size = new System.Drawing.Size(71, 104);
            this.panel134.TabIndex = 7;
            // 
            // valueMaxHr3
            // 
            this.valueMaxHr3.Dock = System.Windows.Forms.DockStyle.Top;
            this.valueMaxHr3.Font = new System.Drawing.Font("Verdana", 20F);
            this.valueMaxHr3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.valueMaxHr3.Location = new System.Drawing.Point(0, 68);
            this.valueMaxHr3.Name = "valueMaxHr3";
            this.valueMaxHr3.Size = new System.Drawing.Size(71, 34);
            this.valueMaxHr3.TabIndex = 61;
            this.valueMaxHr3.Text = "120";
            // 
            // valueMeanHr3
            // 
            this.valueMeanHr3.Dock = System.Windows.Forms.DockStyle.Top;
            this.valueMeanHr3.Font = new System.Drawing.Font("Verdana", 20F);
            this.valueMeanHr3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.valueMeanHr3.Location = new System.Drawing.Point(0, 34);
            this.valueMeanHr3.Name = "valueMeanHr3";
            this.valueMeanHr3.Size = new System.Drawing.Size(71, 34);
            this.valueMeanHr3.TabIndex = 56;
            this.valueMeanHr3.Text = "110";
            // 
            // valueMinHr3
            // 
            this.valueMinHr3.Dock = System.Windows.Forms.DockStyle.Top;
            this.valueMinHr3.Font = new System.Drawing.Font("Verdana", 20F);
            this.valueMinHr3.Location = new System.Drawing.Point(0, 0);
            this.valueMinHr3.Name = "valueMinHr3";
            this.valueMinHr3.Size = new System.Drawing.Size(71, 34);
            this.valueMinHr3.TabIndex = 52;
            this.valueMinHr3.Text = "100";
            // 
            // panel135
            // 
            this.panel135.Controls.Add(this.headerMaxHr3);
            this.panel135.Controls.Add(this.headerMeanHr3);
            this.panel135.Controls.Add(this.headerMinHr3);
            this.panel135.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel135.Location = new System.Drawing.Point(0, 39);
            this.panel135.Name = "panel135";
            this.panel135.Size = new System.Drawing.Size(149, 104);
            this.panel135.TabIndex = 6;
            // 
            // headerMaxHr3
            // 
            this.headerMaxHr3.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerMaxHr3.Font = new System.Drawing.Font("Verdana", 20F);
            this.headerMaxHr3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.headerMaxHr3.Location = new System.Drawing.Point(0, 68);
            this.headerMaxHr3.Name = "headerMaxHr3";
            this.headerMaxHr3.Size = new System.Drawing.Size(149, 34);
            this.headerMaxHr3.TabIndex = 74;
            this.headerMaxHr3.Text = "Max HR:";
            // 
            // headerMeanHr3
            // 
            this.headerMeanHr3.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerMeanHr3.Font = new System.Drawing.Font("Verdana", 20F);
            this.headerMeanHr3.Location = new System.Drawing.Point(0, 34);
            this.headerMeanHr3.Name = "headerMeanHr3";
            this.headerMeanHr3.Size = new System.Drawing.Size(149, 34);
            this.headerMeanHr3.TabIndex = 64;
            this.headerMeanHr3.Text = "Mean HR:";
            // 
            // headerMinHr3
            // 
            this.headerMinHr3.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerMinHr3.Font = new System.Drawing.Font("Verdana", 20F);
            this.headerMinHr3.Location = new System.Drawing.Point(0, 0);
            this.headerMinHr3.Name = "headerMinHr3";
            this.headerMinHr3.Size = new System.Drawing.Size(149, 34);
            this.headerMinHr3.TabIndex = 63;
            this.headerMinHr3.Text = "Min HR:";
            // 
            // label161
            // 
            this.label161.Dock = System.Windows.Forms.DockStyle.Top;
            this.label161.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.label161.Location = new System.Drawing.Point(0, 0);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(557, 39);
            this.label161.TabIndex = 5;
            this.label161.Text = "QRS Measurements:";
            // 
            // panel277
            // 
            this.panel277.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel277.Controls.Add(this.labelSample3StartFull);
            this.panel277.Controls.Add(this.labelSample3MidFull);
            this.panel277.Controls.Add(this.panel278);
            this.panel277.Controls.Add(this.panel279);
            this.panel277.Controls.Add(this.panel280);
            this.panel277.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel277.Location = new System.Drawing.Point(0, 498);
            this.panel277.Name = "panel277";
            this.panel277.Size = new System.Drawing.Size(1652, 35);
            this.panel277.TabIndex = 26;
            // 
            // labelSample3StartFull
            // 
            this.labelSample3StartFull.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSample3StartFull.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample3StartFull.Location = new System.Drawing.Point(0, 0);
            this.labelSample3StartFull.Name = "labelSample3StartFull";
            this.labelSample3StartFull.Size = new System.Drawing.Size(693, 33);
            this.labelSample3StartFull.TabIndex = 9;
            this.labelSample3StartFull.Text = "10:15:10 AM 10/12/2018  6 sec";
            this.labelSample3StartFull.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample3MidFull
            // 
            this.labelSample3MidFull.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample3MidFull.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample3MidFull.Location = new System.Drawing.Point(700, 0);
            this.labelSample3MidFull.Name = "labelSample3MidFull";
            this.labelSample3MidFull.Size = new System.Drawing.Size(200, 33);
            this.labelSample3MidFull.TabIndex = 8;
            this.labelSample3MidFull.Text = "10:15:00 AM";
            this.labelSample3MidFull.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelSample3MidFull.Visible = false;
            // 
            // panel278
            // 
            this.panel278.Controls.Add(this.labelSample2Sweep);
            this.panel278.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel278.Location = new System.Drawing.Point(900, 0);
            this.panel278.Name = "panel278";
            this.panel278.Size = new System.Drawing.Size(290, 33);
            this.panel278.TabIndex = 7;
            // 
            // labelSample2Sweep
            // 
            this.labelSample2Sweep.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSample2Sweep.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample2Sweep.Location = new System.Drawing.Point(0, 0);
            this.labelSample2Sweep.Name = "labelSample2Sweep";
            this.labelSample2Sweep.Size = new System.Drawing.Size(290, 33);
            this.labelSample2Sweep.TabIndex = 3;
            this.labelSample2Sweep.Text = "25 mm/sec (0.20 Sec)";
            this.labelSample2Sweep.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel279
            // 
            this.panel279.Controls.Add(this.labelSample2Ampl);
            this.panel279.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel279.Location = new System.Drawing.Point(1190, 0);
            this.panel279.Name = "panel279";
            this.panel279.Size = new System.Drawing.Size(285, 33);
            this.panel279.TabIndex = 6;
            // 
            // labelSample2Ampl
            // 
            this.labelSample2Ampl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSample2Ampl.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample2Ampl.Location = new System.Drawing.Point(0, 0);
            this.labelSample2Ampl.Name = "labelSample2Ampl";
            this.labelSample2Ampl.Size = new System.Drawing.Size(285, 33);
            this.labelSample2Ampl.TabIndex = 2;
            this.labelSample2Ampl.Text = "10 mm/mV (0.50 mV)";
            this.labelSample2Ampl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel280
            // 
            this.panel280.Controls.Add(this.labelSample3EndFull);
            this.panel280.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel280.Location = new System.Drawing.Point(1475, 0);
            this.panel280.Name = "panel280";
            this.panel280.Size = new System.Drawing.Size(175, 33);
            this.panel280.TabIndex = 5;
            // 
            // labelSample3EndFull
            // 
            this.labelSample3EndFull.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample3EndFull.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample3EndFull.Location = new System.Drawing.Point(-75, 0);
            this.labelSample3EndFull.Name = "labelSample3EndFull";
            this.labelSample3EndFull.Size = new System.Drawing.Size(250, 33);
            this.labelSample3EndFull.TabIndex = 1;
            this.labelSample3EndFull.Text = "10:15:30 AM";
            this.labelSample3EndFull.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel283
            // 
            this.panel283.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel283.Controls.Add(this.panel284);
            this.panel283.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel283.Location = new System.Drawing.Point(0, 288);
            this.panel283.Name = "panel283";
            this.panel283.Size = new System.Drawing.Size(1652, 210);
            this.panel283.TabIndex = 25;
            // 
            // panel284
            // 
            this.panel284.Controls.Add(this.labelSample3LeadFull);
            this.panel284.Controls.Add(this.pictureBoxSample3Full);
            this.panel284.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel284.Location = new System.Drawing.Point(0, 0);
            this.panel284.Name = "panel284";
            this.panel284.Size = new System.Drawing.Size(1650, 208);
            this.panel284.TabIndex = 2;
            // 
            // labelSample3LeadFull
            // 
            this.labelSample3LeadFull.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSample3LeadFull.Location = new System.Drawing.Point(12, 57);
            this.labelSample3LeadFull.Name = "labelSample3LeadFull";
            this.labelSample3LeadFull.Size = new System.Drawing.Size(220, 45);
            this.labelSample3LeadFull.TabIndex = 47;
            this.labelSample3LeadFull.Text = "LEAD I";
            this.labelSample3LeadFull.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelSample3LeadFull.Visible = false;
            // 
            // pictureBoxSample3Full
            // 
            this.pictureBoxSample3Full.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxSample3Full.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxSample3Full.Name = "pictureBoxSample3Full";
            this.pictureBoxSample3Full.Size = new System.Drawing.Size(1650, 208);
            this.pictureBoxSample3Full.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxSample3Full.TabIndex = 0;
            this.pictureBoxSample3Full.TabStop = false;
            // 
            // panel300
            // 
            this.panel300.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel300.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel300.Location = new System.Drawing.Point(0, 286);
            this.panel300.Name = "panel300";
            this.panel300.Size = new System.Drawing.Size(1652, 2);
            this.panel300.TabIndex = 20;
            // 
            // panel384
            // 
            this.panel384.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel384.Controls.Add(this.labelSample3Start);
            this.panel384.Controls.Add(this.labelSample3Mid);
            this.panel384.Controls.Add(this.panel324);
            this.panel384.Controls.Add(this.panel385);
            this.panel384.Controls.Add(this.panel386);
            this.panel384.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel384.Location = new System.Drawing.Point(0, 251);
            this.panel384.Name = "panel384";
            this.panel384.Size = new System.Drawing.Size(1652, 35);
            this.panel384.TabIndex = 11;
            // 
            // labelSample3Start
            // 
            this.labelSample3Start.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSample3Start.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample3Start.Location = new System.Drawing.Point(0, 0);
            this.labelSample3Start.Name = "labelSample3Start";
            this.labelSample3Start.Size = new System.Drawing.Size(693, 33);
            this.labelSample3Start.TabIndex = 8;
            this.labelSample3Start.Text = "^^ 99:99:99 AM 99/99/9099  1d 12:34:12";
            this.labelSample3Start.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample3Mid
            // 
            this.labelSample3Mid.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample3Mid.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample3Mid.Location = new System.Drawing.Point(700, 0);
            this.labelSample3Mid.Name = "labelSample3Mid";
            this.labelSample3Mid.Size = new System.Drawing.Size(200, 33);
            this.labelSample3Mid.TabIndex = 7;
            this.labelSample3Mid.Text = "10:15:15 AM";
            this.labelSample3Mid.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelSample3Mid.Visible = false;
            // 
            // panel324
            // 
            this.panel324.Controls.Add(this.labelSample3Sweep);
            this.panel324.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel324.Location = new System.Drawing.Point(900, 0);
            this.panel324.Name = "panel324";
            this.panel324.Size = new System.Drawing.Size(290, 33);
            this.panel324.TabIndex = 4;
            // 
            // labelSample3Sweep
            // 
            this.labelSample3Sweep.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSample3Sweep.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample3Sweep.Location = new System.Drawing.Point(0, 0);
            this.labelSample3Sweep.Name = "labelSample3Sweep";
            this.labelSample3Sweep.Size = new System.Drawing.Size(290, 33);
            this.labelSample3Sweep.TabIndex = 1;
            this.labelSample3Sweep.Text = "25 mm/sec (0.20 Sec)";
            this.labelSample3Sweep.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel385
            // 
            this.panel385.Controls.Add(this.labelSample3Ampl);
            this.panel385.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel385.Location = new System.Drawing.Point(1190, 0);
            this.panel385.Name = "panel385";
            this.panel385.Size = new System.Drawing.Size(285, 33);
            this.panel385.TabIndex = 3;
            // 
            // labelSample3Ampl
            // 
            this.labelSample3Ampl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSample3Ampl.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample3Ampl.Location = new System.Drawing.Point(0, 0);
            this.labelSample3Ampl.Name = "labelSample3Ampl";
            this.labelSample3Ampl.Size = new System.Drawing.Size(285, 33);
            this.labelSample3Ampl.TabIndex = 2;
            this.labelSample3Ampl.Text = "10 mm/mV (0.50 mV)";
            this.labelSample3Ampl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel386
            // 
            this.panel386.Controls.Add(this.labelSample3End);
            this.panel386.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel386.Location = new System.Drawing.Point(1475, 0);
            this.panel386.Name = "panel386";
            this.panel386.Size = new System.Drawing.Size(175, 33);
            this.panel386.TabIndex = 2;
            // 
            // labelSample3End
            // 
            this.labelSample3End.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSample3End.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelSample3End.Location = new System.Drawing.Point(0, 0);
            this.labelSample3End.Name = "labelSample3End";
            this.labelSample3End.Size = new System.Drawing.Size(175, 33);
            this.labelSample3End.TabIndex = 2;
            this.labelSample3End.Text = "10:15:20 AM";
            this.labelSample3End.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel387
            // 
            this.panel387.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel387.Controls.Add(this.panel388);
            this.panel387.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel387.Location = new System.Drawing.Point(0, 41);
            this.panel387.Name = "panel387";
            this.panel387.Size = new System.Drawing.Size(1652, 210);
            this.panel387.TabIndex = 10;
            // 
            // panel388
            // 
            this.panel388.Controls.Add(this.labelSample3Lead);
            this.panel388.Controls.Add(this.pictureBoxSample3);
            this.panel388.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel388.Location = new System.Drawing.Point(0, 0);
            this.panel388.Name = "panel388";
            this.panel388.Size = new System.Drawing.Size(1650, 208);
            this.panel388.TabIndex = 2;
            // 
            // labelSample3Lead
            // 
            this.labelSample3Lead.Font = new System.Drawing.Font("Verdana", 28F);
            this.labelSample3Lead.Location = new System.Drawing.Point(8, 47);
            this.labelSample3Lead.Name = "labelSample3Lead";
            this.labelSample3Lead.Size = new System.Drawing.Size(207, 49);
            this.labelSample3Lead.TabIndex = 46;
            this.labelSample3Lead.Text = "LEAD I";
            this.labelSample3Lead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelSample3Lead.Visible = false;
            // 
            // pictureBoxSample3
            // 
            this.pictureBoxSample3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxSample3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxSample3.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxSample3.InitialImage")));
            this.pictureBoxSample3.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxSample3.Name = "pictureBoxSample3";
            this.pictureBoxSample3.Size = new System.Drawing.Size(1650, 208);
            this.pictureBoxSample3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxSample3.TabIndex = 0;
            this.pictureBoxSample3.TabStop = false;
            // 
            // panel416
            // 
            this.panel416.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel416.Controls.Add(this.labelSample3Date);
            this.panel416.Controls.Add(this.label75);
            this.panel416.Controls.Add(this.labelSample3Time);
            this.panel416.Controls.Add(this.labelEvent3);
            this.panel416.Controls.Add(this.labelClass3);
            this.panel416.Controls.Add(this.labelSample3Nr);
            this.panel416.Controls.Add(this.labelSample3EventNr);
            this.panel416.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel416.Location = new System.Drawing.Point(0, 1);
            this.panel416.Name = "panel416";
            this.panel416.Size = new System.Drawing.Size(1652, 40);
            this.panel416.TabIndex = 7;
            // 
            // labelSample3Date
            // 
            this.labelSample3Date.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample3Date.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelSample3Date.Location = new System.Drawing.Point(1008, 0);
            this.labelSample3Date.Name = "labelSample3Date";
            this.labelSample3Date.Size = new System.Drawing.Size(200, 38);
            this.labelSample3Date.TabIndex = 14;
            this.labelSample3Date.Text = "11/23/2016";
            this.labelSample3Date.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label75
            // 
            this.label75.Dock = System.Windows.Forms.DockStyle.Right;
            this.label75.Font = new System.Drawing.Font("Verdana", 20F);
            this.label75.Location = new System.Drawing.Point(1208, 0);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(17, 38);
            this.label75.TabIndex = 13;
            this.label75.Text = "-";
            this.label75.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelSample3Time
            // 
            this.labelSample3Time.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample3Time.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelSample3Time.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelSample3Time.Location = new System.Drawing.Point(1225, 0);
            this.labelSample3Time.Name = "labelSample3Time";
            this.labelSample3Time.Size = new System.Drawing.Size(205, 38);
            this.labelSample3Time.TabIndex = 12;
            this.labelSample3Time.Text = "10:15:10 AM";
            this.labelSample3Time.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelEvent3
            // 
            this.labelEvent3.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelEvent3.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelEvent3.Location = new System.Drawing.Point(1430, 0);
            this.labelEvent3.Name = "labelEvent3";
            this.labelEvent3.Size = new System.Drawing.Size(108, 38);
            this.labelEvent3.TabIndex = 3;
            this.labelEvent3.Text = "Event:";
            this.labelEvent3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelClass3
            // 
            this.labelClass3.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelClass3.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold);
            this.labelClass3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelClass3.Location = new System.Drawing.Point(60, 0);
            this.labelClass3.Name = "labelClass3";
            this.labelClass3.Size = new System.Drawing.Size(477, 38);
            this.labelClass3.TabIndex = 17;
            this.labelClass3.Text = "Other";
            this.labelClass3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample3Nr
            // 
            this.labelSample3Nr.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSample3Nr.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold);
            this.labelSample3Nr.Location = new System.Drawing.Point(0, 0);
            this.labelSample3Nr.Name = "labelSample3Nr";
            this.labelSample3Nr.Size = new System.Drawing.Size(60, 38);
            this.labelSample3Nr.TabIndex = 7;
            this.labelSample3Nr.Text = "3";
            this.labelSample3Nr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSample3EventNr
            // 
            this.labelSample3EventNr.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelSample3EventNr.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelSample3EventNr.Location = new System.Drawing.Point(1538, 0);
            this.labelSample3EventNr.Name = "labelSample3EventNr";
            this.labelSample3EventNr.Size = new System.Drawing.Size(112, 38);
            this.labelSample3EventNr.TabIndex = 4;
            this.labelSample3EventNr.Text = "^9999";
            this.labelSample3EventNr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel450
            // 
            this.panel450.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel450.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel450.Location = new System.Drawing.Point(0, 0);
            this.panel450.Name = "panel450";
            this.panel450.Size = new System.Drawing.Size(1652, 1);
            this.panel450.TabIndex = 3;
            // 
            // panel488
            // 
            this.panel488.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel488.Location = new System.Drawing.Point(0, 172);
            this.panel488.Name = "panel488";
            this.panel488.Size = new System.Drawing.Size(1654, 11);
            this.panel488.TabIndex = 46;
            // 
            // panelPage2Header
            // 
            this.panelPage2Header.Controls.Add(this.panel533);
            this.panelPage2Header.Controls.Add(this.panel531);
            this.panelPage2Header.Controls.Add(this.panel529);
            this.panelPage2Header.Controls.Add(this.panel528);
            this.panelPage2Header.Controls.Add(this.panel527);
            this.panelPage2Header.Controls.Add(this.panel526);
            this.panelPage2Header.Controls.Add(this.panel525);
            this.panelPage2Header.Controls.Add(this.panel524);
            this.panelPage2Header.Controls.Add(this.panel523);
            this.panelPage2Header.Controls.Add(this.panel522);
            this.panelPage2Header.Controls.Add(this.panel521);
            this.panelPage2Header.Controls.Add(this.panel520);
            this.panelPage2Header.Controls.Add(this.panel519);
            this.panelPage2Header.Controls.Add(this.panel497);
            this.panelPage2Header.Controls.Add(this.labelReportText2);
            this.panelPage2Header.Controls.Add(this.labelPage2ReportNr);
            this.panelPage2Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelPage2Header.Location = new System.Drawing.Point(0, 84);
            this.panelPage2Header.Name = "panelPage2Header";
            this.panelPage2Header.Size = new System.Drawing.Size(1654, 88);
            this.panelPage2Header.TabIndex = 45;
            // 
            // panel533
            // 
            this.panel533.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel533.Location = new System.Drawing.Point(1402, 0);
            this.panel533.Name = "panel533";
            this.panel533.Size = new System.Drawing.Size(10, 88);
            this.panel533.TabIndex = 58;
            // 
            // panel531
            // 
            this.panel531.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel531.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel531.Location = new System.Drawing.Point(1400, 0);
            this.panel531.Name = "panel531";
            this.panel531.Size = new System.Drawing.Size(2, 88);
            this.panel531.TabIndex = 57;
            // 
            // panel529
            // 
            this.panel529.Controls.Add(this.labelDateOfBirthResPage2);
            this.panel529.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel529.Location = new System.Drawing.Point(1181, 0);
            this.panel529.Name = "panel529";
            this.panel529.Size = new System.Drawing.Size(219, 88);
            this.panel529.TabIndex = 56;
            // 
            // labelDateOfBirthResPage2
            // 
            this.labelDateOfBirthResPage2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDateOfBirthResPage2.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelDateOfBirthResPage2.Location = new System.Drawing.Point(0, 0);
            this.labelDateOfBirthResPage2.Name = "labelDateOfBirthResPage2";
            this.labelDateOfBirthResPage2.Size = new System.Drawing.Size(219, 88);
            this.labelDateOfBirthResPage2.TabIndex = 41;
            this.labelDateOfBirthResPage2.Text = "06/08/1974";
            this.labelDateOfBirthResPage2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel528
            // 
            this.panel528.Controls.Add(this.labelDateOfBirthPage2);
            this.panel528.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel528.Location = new System.Drawing.Point(1095, 0);
            this.panel528.Name = "panel528";
            this.panel528.Size = new System.Drawing.Size(86, 88);
            this.panel528.TabIndex = 55;
            // 
            // labelDateOfBirthPage2
            // 
            this.labelDateOfBirthPage2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDateOfBirthPage2.Font = new System.Drawing.Font("Verdana", 16F);
            this.labelDateOfBirthPage2.Location = new System.Drawing.Point(0, 0);
            this.labelDateOfBirthPage2.Name = "labelDateOfBirthPage2";
            this.labelDateOfBirthPage2.Size = new System.Drawing.Size(86, 88);
            this.labelDateOfBirthPage2.TabIndex = 36;
            this.labelDateOfBirthPage2.Text = "DOB:";
            this.labelDateOfBirthPage2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel527
            // 
            this.panel527.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel527.Location = new System.Drawing.Point(1078, 0);
            this.panel527.Name = "panel527";
            this.panel527.Size = new System.Drawing.Size(17, 88);
            this.panel527.TabIndex = 54;
            // 
            // panel526
            // 
            this.panel526.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel526.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel526.Location = new System.Drawing.Point(1076, 0);
            this.panel526.Name = "panel526";
            this.panel526.Size = new System.Drawing.Size(2, 88);
            this.panel526.TabIndex = 53;
            // 
            // panel525
            // 
            this.panel525.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel525.Location = new System.Drawing.Point(1059, 0);
            this.panel525.Name = "panel525";
            this.panel525.Size = new System.Drawing.Size(17, 88);
            this.panel525.TabIndex = 52;
            // 
            // panel524
            // 
            this.panel524.Controls.Add(this.labelPatLastName);
            this.panel524.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel524.Location = new System.Drawing.Point(517, 0);
            this.panel524.Name = "panel524";
            this.panel524.Size = new System.Drawing.Size(542, 88);
            this.panel524.TabIndex = 51;
            // 
            // labelPatLastName
            // 
            this.labelPatLastName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPatLastName.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelPatLastName.Location = new System.Drawing.Point(0, 0);
            this.labelPatLastName.Name = "labelPatLastName";
            this.labelPatLastName.Size = new System.Drawing.Size(542, 88);
            this.labelPatLastName.TabIndex = 40;
            this.labelPatLastName.Text = "Brest van Kempen, jgdhgshghags, hjgdhghsaghdfg";
            this.labelPatLastName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel523
            // 
            this.panel523.Controls.Add(this.labelPatName);
            this.panel523.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel523.Location = new System.Drawing.Point(411, 0);
            this.panel523.Name = "panel523";
            this.panel523.Size = new System.Drawing.Size(106, 88);
            this.panel523.TabIndex = 50;
            // 
            // labelPatName
            // 
            this.labelPatName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPatName.Font = new System.Drawing.Font("Verdana", 16F);
            this.labelPatName.Location = new System.Drawing.Point(0, 0);
            this.labelPatName.Name = "labelPatName";
            this.labelPatName.Size = new System.Drawing.Size(106, 88);
            this.labelPatName.TabIndex = 35;
            this.labelPatName.Text = "Name:";
            this.labelPatName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel522
            // 
            this.panel522.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel522.Location = new System.Drawing.Point(394, 0);
            this.panel522.Name = "panel522";
            this.panel522.Size = new System.Drawing.Size(17, 88);
            this.panel522.TabIndex = 49;
            // 
            // panel521
            // 
            this.panel521.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel521.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel521.Location = new System.Drawing.Point(392, 0);
            this.panel521.Name = "panel521";
            this.panel521.Size = new System.Drawing.Size(2, 88);
            this.panel521.TabIndex = 48;
            // 
            // panel520
            // 
            this.panel520.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel520.Location = new System.Drawing.Point(375, 0);
            this.panel520.Name = "panel520";
            this.panel520.Size = new System.Drawing.Size(17, 88);
            this.panel520.TabIndex = 47;
            // 
            // panel519
            // 
            this.panel519.Controls.Add(this.labelPatientIDResult);
            this.panel519.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel519.Location = new System.Drawing.Point(146, 0);
            this.panel519.Name = "panel519";
            this.panel519.Size = new System.Drawing.Size(229, 88);
            this.panel519.TabIndex = 46;
            // 
            // labelPatientIDResult
            // 
            this.labelPatientIDResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPatientIDResult.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelPatientIDResult.Location = new System.Drawing.Point(0, 0);
            this.labelPatientIDResult.Name = "labelPatientIDResult";
            this.labelPatientIDResult.Size = new System.Drawing.Size(229, 88);
            this.labelPatientIDResult.TabIndex = 41;
            this.labelPatientIDResult.Text = "12341234567";
            this.labelPatientIDResult.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel497
            // 
            this.panel497.Controls.Add(this.labelPatientID);
            this.panel497.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel497.Location = new System.Drawing.Point(0, 0);
            this.panel497.Name = "panel497";
            this.panel497.Size = new System.Drawing.Size(146, 88);
            this.panel497.TabIndex = 45;
            // 
            // labelPatientID
            // 
            this.labelPatientID.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPatientID.Font = new System.Drawing.Font("Verdana", 16F);
            this.labelPatientID.Location = new System.Drawing.Point(0, 0);
            this.labelPatientID.Name = "labelPatientID";
            this.labelPatientID.Size = new System.Drawing.Size(146, 88);
            this.labelPatientID.TabIndex = 36;
            this.labelPatientID.Text = "Patient ID:";
            this.labelPatientID.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelReportText2
            // 
            this.labelReportText2.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelReportText2.Font = new System.Drawing.Font("Verdana", 16F);
            this.labelReportText2.Location = new System.Drawing.Point(1416, 0);
            this.labelReportText2.Name = "labelReportText2";
            this.labelReportText2.Size = new System.Drawing.Size(101, 88);
            this.labelReportText2.TabIndex = 43;
            this.labelReportText2.Text = "Report:";
            this.labelReportText2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelPage2ReportNr
            // 
            this.labelPage2ReportNr.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelPage2ReportNr.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelPage2ReportNr.Location = new System.Drawing.Point(1517, 0);
            this.labelPage2ReportNr.Name = "labelPage2ReportNr";
            this.labelPage2ReportNr.Size = new System.Drawing.Size(137, 88);
            this.labelPage2ReportNr.TabIndex = 44;
            this.labelPage2ReportNr.Text = "12345";
            this.labelPage2ReportNr.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel500
            // 
            this.panel500.Controls.Add(this.pictureBoxCenter2);
            this.panel500.Controls.Add(this.panel501);
            this.panel500.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel500.Location = new System.Drawing.Point(0, 0);
            this.panel500.Name = "panel500";
            this.panel500.Size = new System.Drawing.Size(1654, 84);
            this.panel500.TabIndex = 44;
            // 
            // pictureBoxCenter2
            // 
            this.pictureBoxCenter2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBoxCenter2.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBoxCenter2.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxCenter2.Name = "pictureBoxCenter2";
            this.pictureBoxCenter2.Size = new System.Drawing.Size(400, 84);
            this.pictureBoxCenter2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxCenter2.TabIndex = 0;
            this.pictureBoxCenter2.TabStop = false;
            // 
            // panel501
            // 
            this.panel501.Controls.Add(this.panel503);
            this.panel501.Controls.Add(this.panel504);
            this.panel501.Controls.Add(this.panel505);
            this.panel501.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel501.Location = new System.Drawing.Point(1254, 0);
            this.panel501.Name = "panel501";
            this.panel501.Size = new System.Drawing.Size(400, 84);
            this.panel501.TabIndex = 8;
            // 
            // panel503
            // 
            this.panel503.Controls.Add(this.labelCenter2p2);
            this.panel503.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel503.Location = new System.Drawing.Point(0, 49);
            this.panel503.Name = "panel503";
            this.panel503.Size = new System.Drawing.Size(400, 34);
            this.panel503.TabIndex = 15;
            // 
            // labelCenter2p2
            // 
            this.labelCenter2p2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCenter2p2.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelCenter2p2.Location = new System.Drawing.Point(0, 0);
            this.labelCenter2p2.Name = "labelCenter2p2";
            this.labelCenter2p2.Size = new System.Drawing.Size(400, 34);
            this.labelCenter2p2.TabIndex = 12;
            this.labelCenter2p2.Text = "c2";
            this.labelCenter2p2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel504
            // 
            this.panel504.Controls.Add(this.labelCenter1p2);
            this.panel504.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel504.Location = new System.Drawing.Point(0, 15);
            this.panel504.Name = "panel504";
            this.panel504.Size = new System.Drawing.Size(400, 34);
            this.panel504.TabIndex = 14;
            // 
            // labelCenter1p2
            // 
            this.labelCenter1p2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCenter1p2.Font = new System.Drawing.Font("Verdana", 18F);
            this.labelCenter1p2.Location = new System.Drawing.Point(0, 0);
            this.labelCenter1p2.Name = "labelCenter1p2";
            this.labelCenter1p2.Size = new System.Drawing.Size(400, 34);
            this.labelCenter1p2.TabIndex = 11;
            this.labelCenter1p2.Text = "c1";
            this.labelCenter1p2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel505
            // 
            this.panel505.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel505.Location = new System.Drawing.Point(0, 0);
            this.panel505.Name = "panel505";
            this.panel505.Size = new System.Drawing.Size(400, 15);
            this.panel505.TabIndex = 13;
            // 
            // panelPage2Footer
            // 
            this.panelPage2Footer.Controls.Add(this.label2);
            this.panelPage2Footer.Controls.Add(this.labelStudyNumberResPage2);
            this.panelPage2Footer.Controls.Add(this.label57);
            this.panelPage2Footer.Controls.Add(this.labelPage2x);
            this.panelPage2Footer.Controls.Add(this.label58);
            this.panelPage2Footer.Controls.Add(this.labelPage2OfX);
            this.panelPage2Footer.Controls.Add(this.label5);
            this.panelPage2Footer.Controls.Add(this.labelPrintDate2Bottom);
            this.panelPage2Footer.Controls.Add(this.labelPrintDate);
            this.panelPage2Footer.Controls.Add(this.label6);
            this.panelPage2Footer.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelPage2Footer.Location = new System.Drawing.Point(0, 2302);
            this.panelPage2Footer.Name = "panelPage2Footer";
            this.panelPage2Footer.Size = new System.Drawing.Size(1654, 37);
            this.panelPage2Footer.TabIndex = 37;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Left;
            this.label2.Font = new System.Drawing.Font("Verdana", 16F);
            this.label2.Location = new System.Drawing.Point(731, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(440, 37);
            this.label2.TabIndex = 19;
            this.label2.Text = "2018 © Techmedic International B.V.";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelStudyNumberResPage2
            // 
            this.labelStudyNumberResPage2.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelStudyNumberResPage2.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.labelStudyNumberResPage2.Location = new System.Drawing.Point(1157, 0);
            this.labelStudyNumberResPage2.Name = "labelStudyNumberResPage2";
            this.labelStudyNumberResPage2.Size = new System.Drawing.Size(179, 37);
            this.labelStudyNumberResPage2.TabIndex = 42;
            this.labelStudyNumberResPage2.Text = "1212212";
            this.labelStudyNumberResPage2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label57
            // 
            this.label57.Dock = System.Windows.Forms.DockStyle.Right;
            this.label57.Font = new System.Drawing.Font("Verdana", 16F);
            this.label57.Location = new System.Drawing.Point(1336, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(74, 37);
            this.label57.TabIndex = 23;
            this.label57.Text = "Page";
            this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelPage2x
            // 
            this.labelPage2x.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelPage2x.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.labelPage2x.Location = new System.Drawing.Point(1410, 0);
            this.labelPage2x.Name = "labelPage2x";
            this.labelPage2x.Size = new System.Drawing.Size(64, 37);
            this.labelPage2x.TabIndex = 22;
            this.labelPage2x.Text = "2";
            this.labelPage2x.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label58
            // 
            this.label58.Dock = System.Windows.Forms.DockStyle.Right;
            this.label58.Font = new System.Drawing.Font("Verdana", 16F);
            this.label58.Location = new System.Drawing.Point(1474, 0);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(37, 37);
            this.label58.TabIndex = 21;
            this.label58.Text = "of";
            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPage2OfX
            // 
            this.labelPage2OfX.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelPage2OfX.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.labelPage2OfX.Location = new System.Drawing.Point(1511, 0);
            this.labelPage2OfX.Name = "labelPage2OfX";
            this.labelPage2OfX.Size = new System.Drawing.Size(98, 37);
            this.labelPage2OfX.TabIndex = 20;
            this.labelPage2OfX.Text = "20p2";
            this.labelPage2OfX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label5.Dock = System.Windows.Forms.DockStyle.Right;
            this.label5.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label5.Location = new System.Drawing.Point(1609, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 37);
            this.label5.TabIndex = 1;
            this.label5.Text = "R2";
            this.label5.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label5.Visible = false;
            // 
            // labelPrintDate2Bottom
            // 
            this.labelPrintDate2Bottom.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPrintDate2Bottom.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.labelPrintDate2Bottom.Location = new System.Drawing.Point(160, 0);
            this.labelPrintDate2Bottom.Name = "labelPrintDate2Bottom";
            this.labelPrintDate2Bottom.Size = new System.Drawing.Size(571, 37);
            this.labelPrintDate2Bottom.TabIndex = 18;
            this.labelPrintDate2Bottom.Text = "12/27/2016 22:22:22 AM+1200 v12345";
            this.labelPrintDate2Bottom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelPrintDate
            // 
            this.labelPrintDate.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPrintDate.Font = new System.Drawing.Font("Verdana", 16F);
            this.labelPrintDate.Location = new System.Drawing.Point(45, 0);
            this.labelPrintDate.Name = "labelPrintDate";
            this.labelPrintDate.Size = new System.Drawing.Size(115, 37);
            this.labelPrintDate.TabIndex = 17;
            this.labelPrintDate.Text = "Printed:";
            this.labelPrintDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label6.Dock = System.Windows.Forms.DockStyle.Left;
            this.label6.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 37);
            this.label6.TabIndex = 0;
            this.label6.Text = "L2";
            this.label6.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label6.Visible = false;
            // 
            // panelPrintArea3
            // 
            this.panelPrintArea3.BackColor = System.Drawing.Color.White;
            this.panelPrintArea3.Controls.Add(this.panelDisclosurePage);
            this.panelPrintArea3.Controls.Add(this.panel91);
            this.panelPrintArea3.Location = new System.Drawing.Point(13, 4853);
            this.panelPrintArea3.Margin = new System.Windows.Forms.Padding(0);
            this.panelPrintArea3.Name = "panelPrintArea3";
            this.panelPrintArea3.Size = new System.Drawing.Size(1654, 2339);
            this.panelPrintArea3.TabIndex = 4;
            // 
            // panelDisclosurePage
            // 
            this.panelDisclosurePage.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelDisclosurePage.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panelDisclosurePage.Controls.Add(this.panelDisclosureStrips);
            this.panelDisclosurePage.Controls.Add(this.panel469);
            this.panelDisclosurePage.Controls.Add(this.panel496);
            this.panelDisclosurePage.Controls.Add(this.panel512);
            this.panelDisclosurePage.Controls.Add(this.panel514);
            this.panelDisclosurePage.Controls.Add(this.panel535);
            this.panelDisclosurePage.Controls.Add(this.panel553);
            this.panelDisclosurePage.Controls.Add(this.panel554);
            this.panelDisclosurePage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDisclosurePage.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.panelDisclosurePage.Location = new System.Drawing.Point(0, 0);
            this.panelDisclosurePage.Name = "panelDisclosurePage";
            this.panelDisclosurePage.Size = new System.Drawing.Size(1654, 2304);
            this.panelDisclosurePage.TabIndex = 36;
            // 
            // panelDisclosureStrips
            // 
            this.panelDisclosureStrips.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelDisclosureStrips.Controls.Add(this.panel71);
            this.panelDisclosureStrips.Controls.Add(this.panel66);
            this.panelDisclosureStrips.Controls.Add(this.panel456);
            this.panelDisclosureStrips.Controls.Add(this.panel179);
            this.panelDisclosureStrips.Controls.Add(this.panel93);
            this.panelDisclosureStrips.Controls.Add(this.panel204);
            this.panelDisclosureStrips.Controls.Add(this.panel361);
            this.panelDisclosureStrips.Controls.Add(this.panel352);
            this.panelDisclosureStrips.Controls.Add(this.panel343);
            this.panelDisclosureStrips.Controls.Add(this.panel340);
            this.panelDisclosureStrips.Controls.Add(this.panel402);
            this.panelDisclosureStrips.Controls.Add(this.panel362);
            this.panelDisclosureStrips.Controls.Add(this.panel399);
            this.panelDisclosureStrips.Controls.Add(this.panel446);
            this.panelDisclosureStrips.Controls.Add(this.panel443);
            this.panelDisclosureStrips.Controls.Add(this.panel439);
            this.panelDisclosureStrips.Controls.Add(this.panel433);
            this.panelDisclosureStrips.Controls.Add(this.panel431);
            this.panelDisclosureStrips.Controls.Add(this.panel602);
            this.panelDisclosureStrips.Controls.Add(this.panel598);
            this.panelDisclosureStrips.Controls.Add(this.panel596);
            this.panelDisclosureStrips.Controls.Add(this.panel494);
            this.panelDisclosureStrips.Controls.Add(this.panel19);
            this.panelDisclosureStrips.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDisclosureStrips.Location = new System.Drawing.Point(0, 196);
            this.panelDisclosureStrips.Name = "panelDisclosureStrips";
            this.panelDisclosureStrips.Size = new System.Drawing.Size(1654, 2108);
            this.panelDisclosureStrips.TabIndex = 11;
            // 
            // panel71
            // 
            this.panel71.Controls.Add(this.labelSFdStripInfo);
            this.panel71.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel71.Location = new System.Drawing.Point(0, 2040);
            this.panel71.Name = "panel71";
            this.panel71.Size = new System.Drawing.Size(1652, 33);
            this.panel71.TabIndex = 85;
            // 
            // labelSFdStripInfo
            // 
            this.labelSFdStripInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSFdStripInfo.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelSFdStripInfo.Location = new System.Drawing.Point(0, 0);
            this.labelSFdStripInfo.Name = "labelSFdStripInfo";
            this.labelSFdStripInfo.Size = new System.Drawing.Size(1652, 33);
            this.labelSFdStripInfo.TabIndex = 13;
            this.labelSFdStripInfo.Text = "^Start strip 12/21/2017 10:11:00 AM,  555 sec, full event, all channels";
            this.labelSFdStripInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel66
            // 
            this.panel66.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel66.Location = new System.Drawing.Point(0, 2073);
            this.panel66.Name = "panel66";
            this.panel66.Size = new System.Drawing.Size(1652, 33);
            this.panel66.TabIndex = 84;
            // 
            // panel456
            // 
            this.panel456.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel456.Controls.Add(this.labelUnitFeed);
            this.panel456.Controls.Add(this.labelUnitAmp);
            this.panel456.Controls.Add(this.labelEndPageTime);
            this.panel456.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel456.Location = new System.Drawing.Point(0, 1964);
            this.panel456.Name = "panel456";
            this.panel456.Size = new System.Drawing.Size(1652, 40);
            this.panel456.TabIndex = 81;
            // 
            // labelUnitFeed
            // 
            this.labelUnitFeed.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelUnitFeed.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelUnitFeed.Location = new System.Drawing.Point(750, 0);
            this.labelUnitFeed.Name = "labelUnitFeed";
            this.labelUnitFeed.Size = new System.Drawing.Size(345, 38);
            this.labelUnitFeed.TabIndex = 9;
            this.labelUnitFeed.Text = "25 mm/sec (0.20 Sec)";
            this.labelUnitFeed.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelUnitAmp
            // 
            this.labelUnitAmp.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelUnitAmp.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelUnitAmp.Location = new System.Drawing.Point(1095, 0);
            this.labelUnitAmp.Name = "labelUnitAmp";
            this.labelUnitAmp.Size = new System.Drawing.Size(340, 38);
            this.labelUnitAmp.TabIndex = 8;
            this.labelUnitAmp.Text = "10 mm/mV (0.50 mV)";
            this.labelUnitAmp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelEndPageTime
            // 
            this.labelEndPageTime.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelEndPageTime.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelEndPageTime.Location = new System.Drawing.Point(1435, 0);
            this.labelEndPageTime.Name = "labelEndPageTime";
            this.labelEndPageTime.Size = new System.Drawing.Size(215, 38);
            this.labelEndPageTime.TabIndex = 6;
            this.labelEndPageTime.Text = "10:15:30 AM";
            this.labelEndPageTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel179
            // 
            this.panel179.Controls.Add(this.panel180);
            this.panel179.Controls.Add(this.panel198);
            this.panel179.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel179.Location = new System.Drawing.Point(0, 1864);
            this.panel179.Name = "panel179";
            this.panel179.Size = new System.Drawing.Size(1652, 100);
            this.panel179.TabIndex = 83;
            // 
            // panel180
            // 
            this.panel180.Controls.Add(this.pictureBoxStrip20);
            this.panel180.Controls.Add(this.panel181);
            this.panel180.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel180.Location = new System.Drawing.Point(0, 0);
            this.panel180.Name = "panel180";
            this.panel180.Size = new System.Drawing.Size(1652, 95);
            this.panel180.TabIndex = 57;
            // 
            // pictureBoxStrip20
            // 
            this.pictureBoxStrip20.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxStrip20.BackgroundImage")));
            this.pictureBoxStrip20.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxStrip20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxStrip20.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxStrip20.InitialImage")));
            this.pictureBoxStrip20.Location = new System.Drawing.Point(121, 0);
            this.pictureBoxStrip20.Name = "pictureBoxStrip20";
            this.pictureBoxStrip20.Size = new System.Drawing.Size(1531, 95);
            this.pictureBoxStrip20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxStrip20.TabIndex = 4;
            this.pictureBoxStrip20.TabStop = false;
            // 
            // panel181
            // 
            this.panel181.Controls.Add(this.labelStripTime20);
            this.panel181.Controls.Add(this.labelStripDate20);
            this.panel181.Controls.Add(this.labelStripLead20);
            this.panel181.Controls.Add(this.label32);
            this.panel181.Controls.Add(this.label26);
            this.panel181.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel181.Location = new System.Drawing.Point(0, 0);
            this.panel181.Name = "panel181";
            this.panel181.Size = new System.Drawing.Size(121, 95);
            this.panel181.TabIndex = 1;
            // 
            // labelStripTime20
            // 
            this.labelStripTime20.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripTime20.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripTime20.Location = new System.Drawing.Point(0, 58);
            this.labelStripTime20.Name = "labelStripTime20";
            this.labelStripTime20.Size = new System.Drawing.Size(121, 20);
            this.labelStripTime20.TabIndex = 55;
            this.labelStripTime20.Text = "10:15:20 AM";
            this.labelStripTime20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripDate20
            // 
            this.labelStripDate20.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripDate20.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripDate20.Location = new System.Drawing.Point(0, 38);
            this.labelStripDate20.Name = "labelStripDate20";
            this.labelStripDate20.Size = new System.Drawing.Size(121, 20);
            this.labelStripDate20.TabIndex = 54;
            this.labelStripDate20.Text = "01/01/2017";
            this.labelStripDate20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripLead20
            // 
            this.labelStripLead20.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripLead20.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripLead20.Location = new System.Drawing.Point(0, 18);
            this.labelStripLead20.Name = "labelStripLead20";
            this.labelStripLead20.Size = new System.Drawing.Size(121, 20);
            this.labelStripLead20.TabIndex = 53;
            this.labelStripLead20.Text = "LEAD I";
            this.labelStripLead20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelStripLead20.Visible = false;
            // 
            // label32
            // 
            this.label32.Dock = System.Windows.Forms.DockStyle.Top;
            this.label32.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(0, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(121, 18);
            this.label32.TabIndex = 58;
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label26
            // 
            this.label26.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label26.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(0, 77);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(121, 18);
            this.label26.TabIndex = 56;
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel198
            // 
            this.panel198.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel198.Location = new System.Drawing.Point(0, 95);
            this.panel198.Name = "panel198";
            this.panel198.Size = new System.Drawing.Size(1652, 5);
            this.panel198.TabIndex = 56;
            // 
            // panel93
            // 
            this.panel93.Controls.Add(this.panel94);
            this.panel93.Controls.Add(this.panel156);
            this.panel93.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel93.Location = new System.Drawing.Point(0, 1764);
            this.panel93.Name = "panel93";
            this.panel93.Size = new System.Drawing.Size(1652, 100);
            this.panel93.TabIndex = 82;
            // 
            // panel94
            // 
            this.panel94.Controls.Add(this.pictureBoxStrip19);
            this.panel94.Controls.Add(this.panel154);
            this.panel94.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel94.Location = new System.Drawing.Point(0, 0);
            this.panel94.Name = "panel94";
            this.panel94.Size = new System.Drawing.Size(1652, 95);
            this.panel94.TabIndex = 57;
            // 
            // pictureBoxStrip19
            // 
            this.pictureBoxStrip19.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxStrip19.BackgroundImage")));
            this.pictureBoxStrip19.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxStrip19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxStrip19.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxStrip19.InitialImage")));
            this.pictureBoxStrip19.Location = new System.Drawing.Point(121, 0);
            this.pictureBoxStrip19.Name = "pictureBoxStrip19";
            this.pictureBoxStrip19.Size = new System.Drawing.Size(1531, 95);
            this.pictureBoxStrip19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxStrip19.TabIndex = 4;
            this.pictureBoxStrip19.TabStop = false;
            // 
            // panel154
            // 
            this.panel154.Controls.Add(this.labelStripTime19);
            this.panel154.Controls.Add(this.labelStripDate19);
            this.panel154.Controls.Add(this.labelStripLead19);
            this.panel154.Controls.Add(this.label20);
            this.panel154.Controls.Add(this.label17);
            this.panel154.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel154.Location = new System.Drawing.Point(0, 0);
            this.panel154.Name = "panel154";
            this.panel154.Size = new System.Drawing.Size(121, 95);
            this.panel154.TabIndex = 1;
            // 
            // labelStripTime19
            // 
            this.labelStripTime19.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripTime19.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripTime19.Location = new System.Drawing.Point(0, 58);
            this.labelStripTime19.Name = "labelStripTime19";
            this.labelStripTime19.Size = new System.Drawing.Size(121, 20);
            this.labelStripTime19.TabIndex = 55;
            this.labelStripTime19.Text = "10:15:20 AM";
            this.labelStripTime19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripDate19
            // 
            this.labelStripDate19.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripDate19.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripDate19.Location = new System.Drawing.Point(0, 38);
            this.labelStripDate19.Name = "labelStripDate19";
            this.labelStripDate19.Size = new System.Drawing.Size(121, 20);
            this.labelStripDate19.TabIndex = 54;
            this.labelStripDate19.Text = "01/01/2017";
            this.labelStripDate19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripLead19
            // 
            this.labelStripLead19.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripLead19.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripLead19.Location = new System.Drawing.Point(0, 18);
            this.labelStripLead19.Name = "labelStripLead19";
            this.labelStripLead19.Size = new System.Drawing.Size(121, 20);
            this.labelStripLead19.TabIndex = 53;
            this.labelStripLead19.Text = "LEAD I";
            this.labelStripLead19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelStripLead19.Visible = false;
            // 
            // label20
            // 
            this.label20.Dock = System.Windows.Forms.DockStyle.Top;
            this.label20.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(0, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(121, 18);
            this.label20.TabIndex = 58;
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label17
            // 
            this.label17.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label17.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(0, 77);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(121, 18);
            this.label17.TabIndex = 56;
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel156
            // 
            this.panel156.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel156.Location = new System.Drawing.Point(0, 95);
            this.panel156.Name = "panel156";
            this.panel156.Size = new System.Drawing.Size(1652, 5);
            this.panel156.TabIndex = 56;
            // 
            // panel204
            // 
            this.panel204.Controls.Add(this.panel210);
            this.panel204.Controls.Add(this.panel211);
            this.panel204.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel204.Location = new System.Drawing.Point(0, 1664);
            this.panel204.Name = "panel204";
            this.panel204.Size = new System.Drawing.Size(1652, 100);
            this.panel204.TabIndex = 80;
            // 
            // panel210
            // 
            this.panel210.Controls.Add(this.pictureBoxStrip18);
            this.panel210.Controls.Add(this.panel155);
            this.panel210.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel210.Location = new System.Drawing.Point(0, 0);
            this.panel210.Name = "panel210";
            this.panel210.Size = new System.Drawing.Size(1652, 95);
            this.panel210.TabIndex = 57;
            // 
            // pictureBoxStrip18
            // 
            this.pictureBoxStrip18.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxStrip18.BackgroundImage")));
            this.pictureBoxStrip18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxStrip18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxStrip18.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxStrip18.InitialImage")));
            this.pictureBoxStrip18.Location = new System.Drawing.Point(121, 0);
            this.pictureBoxStrip18.Name = "pictureBoxStrip18";
            this.pictureBoxStrip18.Size = new System.Drawing.Size(1531, 95);
            this.pictureBoxStrip18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxStrip18.TabIndex = 4;
            this.pictureBoxStrip18.TabStop = false;
            // 
            // panel155
            // 
            this.panel155.Controls.Add(this.labelStripTime18);
            this.panel155.Controls.Add(this.labelStripDate18);
            this.panel155.Controls.Add(this.labelStripLead18);
            this.panel155.Controls.Add(this.label123);
            this.panel155.Controls.Add(this.label120);
            this.panel155.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel155.Location = new System.Drawing.Point(0, 0);
            this.panel155.Name = "panel155";
            this.panel155.Size = new System.Drawing.Size(121, 95);
            this.panel155.TabIndex = 1;
            // 
            // labelStripTime18
            // 
            this.labelStripTime18.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripTime18.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripTime18.Location = new System.Drawing.Point(0, 58);
            this.labelStripTime18.Name = "labelStripTime18";
            this.labelStripTime18.Size = new System.Drawing.Size(121, 20);
            this.labelStripTime18.TabIndex = 55;
            this.labelStripTime18.Text = "10:15:20 AM";
            this.labelStripTime18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripDate18
            // 
            this.labelStripDate18.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripDate18.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripDate18.Location = new System.Drawing.Point(0, 38);
            this.labelStripDate18.Name = "labelStripDate18";
            this.labelStripDate18.Size = new System.Drawing.Size(121, 20);
            this.labelStripDate18.TabIndex = 54;
            this.labelStripDate18.Text = "01/01/2017";
            this.labelStripDate18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripLead18
            // 
            this.labelStripLead18.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripLead18.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripLead18.Location = new System.Drawing.Point(0, 18);
            this.labelStripLead18.Name = "labelStripLead18";
            this.labelStripLead18.Size = new System.Drawing.Size(121, 20);
            this.labelStripLead18.TabIndex = 53;
            this.labelStripLead18.Text = "LEAD I";
            this.labelStripLead18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelStripLead18.Visible = false;
            // 
            // label123
            // 
            this.label123.Dock = System.Windows.Forms.DockStyle.Top;
            this.label123.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label123.Location = new System.Drawing.Point(0, 0);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(121, 18);
            this.label123.TabIndex = 58;
            this.label123.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label120
            // 
            this.label120.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label120.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label120.Location = new System.Drawing.Point(0, 77);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(121, 18);
            this.label120.TabIndex = 56;
            this.label120.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel211
            // 
            this.panel211.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel211.Location = new System.Drawing.Point(0, 95);
            this.panel211.Name = "panel211";
            this.panel211.Size = new System.Drawing.Size(1652, 5);
            this.panel211.TabIndex = 56;
            // 
            // panel361
            // 
            this.panel361.Controls.Add(this.panel404);
            this.panel361.Controls.Add(this.panel410);
            this.panel361.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel361.Location = new System.Drawing.Point(0, 1564);
            this.panel361.Name = "panel361";
            this.panel361.Size = new System.Drawing.Size(1652, 100);
            this.panel361.TabIndex = 79;
            // 
            // panel404
            // 
            this.panel404.Controls.Add(this.pictureBoxStrip17);
            this.panel404.Controls.Add(this.panel192);
            this.panel404.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel404.Location = new System.Drawing.Point(0, 0);
            this.panel404.Name = "panel404";
            this.panel404.Size = new System.Drawing.Size(1652, 95);
            this.panel404.TabIndex = 57;
            // 
            // pictureBoxStrip17
            // 
            this.pictureBoxStrip17.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxStrip17.BackgroundImage")));
            this.pictureBoxStrip17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxStrip17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxStrip17.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxStrip17.InitialImage")));
            this.pictureBoxStrip17.Location = new System.Drawing.Point(121, 0);
            this.pictureBoxStrip17.Name = "pictureBoxStrip17";
            this.pictureBoxStrip17.Size = new System.Drawing.Size(1531, 95);
            this.pictureBoxStrip17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxStrip17.TabIndex = 4;
            this.pictureBoxStrip17.TabStop = false;
            // 
            // panel192
            // 
            this.panel192.Controls.Add(this.labelStripTime17);
            this.panel192.Controls.Add(this.labelStripDate17);
            this.panel192.Controls.Add(this.labelStripLead17);
            this.panel192.Controls.Add(this.label115);
            this.panel192.Controls.Add(this.label112);
            this.panel192.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel192.Location = new System.Drawing.Point(0, 0);
            this.panel192.Name = "panel192";
            this.panel192.Size = new System.Drawing.Size(121, 95);
            this.panel192.TabIndex = 1;
            // 
            // labelStripTime17
            // 
            this.labelStripTime17.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripTime17.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripTime17.Location = new System.Drawing.Point(0, 58);
            this.labelStripTime17.Name = "labelStripTime17";
            this.labelStripTime17.Size = new System.Drawing.Size(121, 20);
            this.labelStripTime17.TabIndex = 55;
            this.labelStripTime17.Text = "10:15:20 AM";
            this.labelStripTime17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripDate17
            // 
            this.labelStripDate17.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripDate17.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripDate17.Location = new System.Drawing.Point(0, 38);
            this.labelStripDate17.Name = "labelStripDate17";
            this.labelStripDate17.Size = new System.Drawing.Size(121, 20);
            this.labelStripDate17.TabIndex = 54;
            this.labelStripDate17.Text = "01/01/2017";
            this.labelStripDate17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripLead17
            // 
            this.labelStripLead17.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripLead17.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripLead17.Location = new System.Drawing.Point(0, 18);
            this.labelStripLead17.Name = "labelStripLead17";
            this.labelStripLead17.Size = new System.Drawing.Size(121, 20);
            this.labelStripLead17.TabIndex = 53;
            this.labelStripLead17.Text = "LEAD I";
            this.labelStripLead17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelStripLead17.Visible = false;
            // 
            // label115
            // 
            this.label115.Dock = System.Windows.Forms.DockStyle.Top;
            this.label115.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label115.Location = new System.Drawing.Point(0, 0);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(121, 18);
            this.label115.TabIndex = 58;
            this.label115.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label112
            // 
            this.label112.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label112.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label112.Location = new System.Drawing.Point(0, 77);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(121, 18);
            this.label112.TabIndex = 56;
            this.label112.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel410
            // 
            this.panel410.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel410.Location = new System.Drawing.Point(0, 95);
            this.panel410.Name = "panel410";
            this.panel410.Size = new System.Drawing.Size(1652, 5);
            this.panel410.TabIndex = 56;
            // 
            // panel352
            // 
            this.panel352.Controls.Add(this.panel353);
            this.panel352.Controls.Add(this.panel354);
            this.panel352.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel352.Location = new System.Drawing.Point(0, 1464);
            this.panel352.Name = "panel352";
            this.panel352.Size = new System.Drawing.Size(1652, 100);
            this.panel352.TabIndex = 78;
            // 
            // panel353
            // 
            this.panel353.Controls.Add(this.pictureBoxStrip16);
            this.panel353.Controls.Add(this.panel212);
            this.panel353.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel353.Location = new System.Drawing.Point(0, 0);
            this.panel353.Name = "panel353";
            this.panel353.Size = new System.Drawing.Size(1652, 95);
            this.panel353.TabIndex = 57;
            // 
            // pictureBoxStrip16
            // 
            this.pictureBoxStrip16.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxStrip16.BackgroundImage")));
            this.pictureBoxStrip16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxStrip16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxStrip16.InitialImage = null;
            this.pictureBoxStrip16.Location = new System.Drawing.Point(121, 0);
            this.pictureBoxStrip16.Name = "pictureBoxStrip16";
            this.pictureBoxStrip16.Size = new System.Drawing.Size(1531, 95);
            this.pictureBoxStrip16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxStrip16.TabIndex = 4;
            this.pictureBoxStrip16.TabStop = false;
            // 
            // panel212
            // 
            this.panel212.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel212.Controls.Add(this.labelStripTime16);
            this.panel212.Controls.Add(this.labelStripDate16);
            this.panel212.Controls.Add(this.labelStripLead16);
            this.panel212.Controls.Add(this.label109);
            this.panel212.Controls.Add(this.label106);
            this.panel212.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel212.Location = new System.Drawing.Point(0, 0);
            this.panel212.Name = "panel212";
            this.panel212.Size = new System.Drawing.Size(121, 95);
            this.panel212.TabIndex = 1;
            // 
            // labelStripTime16
            // 
            this.labelStripTime16.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripTime16.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripTime16.Location = new System.Drawing.Point(0, 58);
            this.labelStripTime16.Name = "labelStripTime16";
            this.labelStripTime16.Size = new System.Drawing.Size(121, 20);
            this.labelStripTime16.TabIndex = 55;
            this.labelStripTime16.Text = "10:15:20 AM";
            this.labelStripTime16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripDate16
            // 
            this.labelStripDate16.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripDate16.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripDate16.Location = new System.Drawing.Point(0, 38);
            this.labelStripDate16.Name = "labelStripDate16";
            this.labelStripDate16.Size = new System.Drawing.Size(121, 20);
            this.labelStripDate16.TabIndex = 54;
            this.labelStripDate16.Text = "01/01/2017";
            this.labelStripDate16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripLead16
            // 
            this.labelStripLead16.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripLead16.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripLead16.Location = new System.Drawing.Point(0, 18);
            this.labelStripLead16.Name = "labelStripLead16";
            this.labelStripLead16.Size = new System.Drawing.Size(121, 20);
            this.labelStripLead16.TabIndex = 53;
            this.labelStripLead16.Text = "LEAD I";
            this.labelStripLead16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelStripLead16.Visible = false;
            // 
            // label109
            // 
            this.label109.Dock = System.Windows.Forms.DockStyle.Top;
            this.label109.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label109.Location = new System.Drawing.Point(0, 0);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(121, 18);
            this.label109.TabIndex = 58;
            this.label109.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label106
            // 
            this.label106.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label106.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label106.Location = new System.Drawing.Point(0, 77);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(121, 18);
            this.label106.TabIndex = 56;
            this.label106.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel354
            // 
            this.panel354.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel354.Location = new System.Drawing.Point(0, 95);
            this.panel354.Name = "panel354";
            this.panel354.Size = new System.Drawing.Size(1652, 5);
            this.panel354.TabIndex = 56;
            // 
            // panel343
            // 
            this.panel343.Controls.Add(this.panel347);
            this.panel343.Controls.Add(this.panel348);
            this.panel343.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel343.Location = new System.Drawing.Point(0, 1364);
            this.panel343.Name = "panel343";
            this.panel343.Size = new System.Drawing.Size(1652, 100);
            this.panel343.TabIndex = 77;
            // 
            // panel347
            // 
            this.panel347.Controls.Add(this.pictureBoxStrip15);
            this.panel347.Controls.Add(this.panel227);
            this.panel347.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel347.Location = new System.Drawing.Point(0, 0);
            this.panel347.Name = "panel347";
            this.panel347.Size = new System.Drawing.Size(1652, 95);
            this.panel347.TabIndex = 57;
            // 
            // pictureBoxStrip15
            // 
            this.pictureBoxStrip15.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxStrip15.BackgroundImage")));
            this.pictureBoxStrip15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxStrip15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxStrip15.InitialImage = null;
            this.pictureBoxStrip15.Location = new System.Drawing.Point(121, 0);
            this.pictureBoxStrip15.Name = "pictureBoxStrip15";
            this.pictureBoxStrip15.Size = new System.Drawing.Size(1531, 95);
            this.pictureBoxStrip15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxStrip15.TabIndex = 4;
            this.pictureBoxStrip15.TabStop = false;
            // 
            // panel227
            // 
            this.panel227.Controls.Add(this.labelStripTime15);
            this.panel227.Controls.Add(this.labelStripDate15);
            this.panel227.Controls.Add(this.label99);
            this.panel227.Controls.Add(this.labelStripLead15);
            this.panel227.Controls.Add(this.label102);
            this.panel227.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel227.Location = new System.Drawing.Point(0, 0);
            this.panel227.Name = "panel227";
            this.panel227.Size = new System.Drawing.Size(121, 95);
            this.panel227.TabIndex = 1;
            // 
            // labelStripTime15
            // 
            this.labelStripTime15.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripTime15.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripTime15.Location = new System.Drawing.Point(0, 58);
            this.labelStripTime15.Name = "labelStripTime15";
            this.labelStripTime15.Size = new System.Drawing.Size(121, 20);
            this.labelStripTime15.TabIndex = 55;
            this.labelStripTime15.Text = "10:15:20 AM";
            this.labelStripTime15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripDate15
            // 
            this.labelStripDate15.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripDate15.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripDate15.Location = new System.Drawing.Point(0, 38);
            this.labelStripDate15.Name = "labelStripDate15";
            this.labelStripDate15.Size = new System.Drawing.Size(121, 20);
            this.labelStripDate15.TabIndex = 54;
            this.labelStripDate15.Text = "01/01/2017";
            this.labelStripDate15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label99
            // 
            this.label99.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label99.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label99.Location = new System.Drawing.Point(0, 77);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(121, 18);
            this.label99.TabIndex = 56;
            this.label99.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripLead15
            // 
            this.labelStripLead15.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripLead15.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripLead15.Location = new System.Drawing.Point(0, 18);
            this.labelStripLead15.Name = "labelStripLead15";
            this.labelStripLead15.Size = new System.Drawing.Size(121, 20);
            this.labelStripLead15.TabIndex = 53;
            this.labelStripLead15.Text = "LEAD I";
            this.labelStripLead15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelStripLead15.Visible = false;
            // 
            // label102
            // 
            this.label102.Dock = System.Windows.Forms.DockStyle.Top;
            this.label102.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label102.Location = new System.Drawing.Point(0, 0);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(121, 18);
            this.label102.TabIndex = 58;
            this.label102.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel348
            // 
            this.panel348.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel348.Location = new System.Drawing.Point(0, 95);
            this.panel348.Name = "panel348";
            this.panel348.Size = new System.Drawing.Size(1652, 5);
            this.panel348.TabIndex = 56;
            // 
            // panel340
            // 
            this.panel340.Controls.Add(this.panel341);
            this.panel340.Controls.Add(this.panel346);
            this.panel340.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel340.Location = new System.Drawing.Point(0, 1264);
            this.panel340.Name = "panel340";
            this.panel340.Size = new System.Drawing.Size(1652, 100);
            this.panel340.TabIndex = 76;
            // 
            // panel341
            // 
            this.panel341.Controls.Add(this.pictureBoxStrip14);
            this.panel341.Controls.Add(this.panel320);
            this.panel341.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel341.Location = new System.Drawing.Point(0, 0);
            this.panel341.Name = "panel341";
            this.panel341.Size = new System.Drawing.Size(1652, 95);
            this.panel341.TabIndex = 57;
            // 
            // pictureBoxStrip14
            // 
            this.pictureBoxStrip14.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxStrip14.BackgroundImage")));
            this.pictureBoxStrip14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxStrip14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxStrip14.InitialImage = null;
            this.pictureBoxStrip14.Location = new System.Drawing.Point(121, 0);
            this.pictureBoxStrip14.Name = "pictureBoxStrip14";
            this.pictureBoxStrip14.Size = new System.Drawing.Size(1531, 95);
            this.pictureBoxStrip14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxStrip14.TabIndex = 4;
            this.pictureBoxStrip14.TabStop = false;
            // 
            // panel320
            // 
            this.panel320.Controls.Add(this.labelStripTime14);
            this.panel320.Controls.Add(this.labelStripDate14);
            this.panel320.Controls.Add(this.label92);
            this.panel320.Controls.Add(this.labelStripLead14);
            this.panel320.Controls.Add(this.label95);
            this.panel320.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel320.Location = new System.Drawing.Point(0, 0);
            this.panel320.Name = "panel320";
            this.panel320.Size = new System.Drawing.Size(121, 95);
            this.panel320.TabIndex = 1;
            // 
            // labelStripTime14
            // 
            this.labelStripTime14.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripTime14.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripTime14.Location = new System.Drawing.Point(0, 58);
            this.labelStripTime14.Name = "labelStripTime14";
            this.labelStripTime14.Size = new System.Drawing.Size(121, 20);
            this.labelStripTime14.TabIndex = 55;
            this.labelStripTime14.Text = "10:15:20 AM";
            this.labelStripTime14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripDate14
            // 
            this.labelStripDate14.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripDate14.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripDate14.Location = new System.Drawing.Point(0, 38);
            this.labelStripDate14.Name = "labelStripDate14";
            this.labelStripDate14.Size = new System.Drawing.Size(121, 20);
            this.labelStripDate14.TabIndex = 54;
            this.labelStripDate14.Text = "01/01/2017";
            this.labelStripDate14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label92
            // 
            this.label92.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label92.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label92.Location = new System.Drawing.Point(0, 77);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(121, 18);
            this.label92.TabIndex = 56;
            this.label92.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripLead14
            // 
            this.labelStripLead14.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripLead14.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripLead14.Location = new System.Drawing.Point(0, 18);
            this.labelStripLead14.Name = "labelStripLead14";
            this.labelStripLead14.Size = new System.Drawing.Size(121, 20);
            this.labelStripLead14.TabIndex = 53;
            this.labelStripLead14.Text = "LEAD I";
            this.labelStripLead14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelStripLead14.Visible = false;
            // 
            // label95
            // 
            this.label95.Dock = System.Windows.Forms.DockStyle.Top;
            this.label95.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label95.Location = new System.Drawing.Point(0, 0);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(121, 18);
            this.label95.TabIndex = 58;
            this.label95.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel346
            // 
            this.panel346.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel346.Location = new System.Drawing.Point(0, 95);
            this.panel346.Name = "panel346";
            this.panel346.Size = new System.Drawing.Size(1652, 5);
            this.panel346.TabIndex = 56;
            // 
            // panel402
            // 
            this.panel402.Controls.Add(this.panel406);
            this.panel402.Controls.Add(this.panel407);
            this.panel402.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel402.Location = new System.Drawing.Point(0, 1164);
            this.panel402.Name = "panel402";
            this.panel402.Size = new System.Drawing.Size(1652, 100);
            this.panel402.TabIndex = 75;
            // 
            // panel406
            // 
            this.panel406.Controls.Add(this.pictureBoxStrip13);
            this.panel406.Controls.Add(this.panel338);
            this.panel406.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel406.Location = new System.Drawing.Point(0, 0);
            this.panel406.Name = "panel406";
            this.panel406.Size = new System.Drawing.Size(1652, 95);
            this.panel406.TabIndex = 57;
            // 
            // pictureBoxStrip13
            // 
            this.pictureBoxStrip13.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxStrip13.BackgroundImage")));
            this.pictureBoxStrip13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxStrip13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxStrip13.InitialImage = null;
            this.pictureBoxStrip13.Location = new System.Drawing.Point(121, 0);
            this.pictureBoxStrip13.Name = "pictureBoxStrip13";
            this.pictureBoxStrip13.Size = new System.Drawing.Size(1531, 95);
            this.pictureBoxStrip13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxStrip13.TabIndex = 4;
            this.pictureBoxStrip13.TabStop = false;
            // 
            // panel338
            // 
            this.panel338.Controls.Add(this.labelStripTime13);
            this.panel338.Controls.Add(this.labelStripDate13);
            this.panel338.Controls.Add(this.labelStripLead13);
            this.panel338.Controls.Add(this.label62);
            this.panel338.Controls.Add(this.label59);
            this.panel338.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel338.Location = new System.Drawing.Point(0, 0);
            this.panel338.Name = "panel338";
            this.panel338.Size = new System.Drawing.Size(121, 95);
            this.panel338.TabIndex = 1;
            // 
            // labelStripTime13
            // 
            this.labelStripTime13.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripTime13.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripTime13.Location = new System.Drawing.Point(0, 58);
            this.labelStripTime13.Name = "labelStripTime13";
            this.labelStripTime13.Size = new System.Drawing.Size(121, 20);
            this.labelStripTime13.TabIndex = 55;
            this.labelStripTime13.Text = "10:15:20 AM";
            this.labelStripTime13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripDate13
            // 
            this.labelStripDate13.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripDate13.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripDate13.Location = new System.Drawing.Point(0, 38);
            this.labelStripDate13.Name = "labelStripDate13";
            this.labelStripDate13.Size = new System.Drawing.Size(121, 20);
            this.labelStripDate13.TabIndex = 54;
            this.labelStripDate13.Text = "01/01/2017";
            this.labelStripDate13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripLead13
            // 
            this.labelStripLead13.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripLead13.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripLead13.Location = new System.Drawing.Point(0, 18);
            this.labelStripLead13.Name = "labelStripLead13";
            this.labelStripLead13.Size = new System.Drawing.Size(121, 20);
            this.labelStripLead13.TabIndex = 53;
            this.labelStripLead13.Text = "LEAD I";
            this.labelStripLead13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelStripLead13.Visible = false;
            // 
            // label62
            // 
            this.label62.Dock = System.Windows.Forms.DockStyle.Top;
            this.label62.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.Location = new System.Drawing.Point(0, 0);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(121, 18);
            this.label62.TabIndex = 58;
            this.label62.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label59
            // 
            this.label59.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label59.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(0, 77);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(121, 18);
            this.label59.TabIndex = 56;
            this.label59.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel407
            // 
            this.panel407.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel407.Location = new System.Drawing.Point(0, 95);
            this.panel407.Name = "panel407";
            this.panel407.Size = new System.Drawing.Size(1652, 5);
            this.panel407.TabIndex = 56;
            // 
            // panel362
            // 
            this.panel362.Controls.Add(this.panel363);
            this.panel362.Controls.Add(this.panel396);
            this.panel362.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel362.Location = new System.Drawing.Point(0, 1064);
            this.panel362.Name = "panel362";
            this.panel362.Size = new System.Drawing.Size(1652, 100);
            this.panel362.TabIndex = 73;
            // 
            // panel363
            // 
            this.panel363.Controls.Add(this.pictureBoxStrip12);
            this.panel363.Controls.Add(this.panel344);
            this.panel363.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel363.Location = new System.Drawing.Point(0, 0);
            this.panel363.Name = "panel363";
            this.panel363.Size = new System.Drawing.Size(1652, 95);
            this.panel363.TabIndex = 57;
            // 
            // pictureBoxStrip12
            // 
            this.pictureBoxStrip12.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxStrip12.BackgroundImage")));
            this.pictureBoxStrip12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxStrip12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxStrip12.InitialImage = null;
            this.pictureBoxStrip12.Location = new System.Drawing.Point(121, 0);
            this.pictureBoxStrip12.Name = "pictureBoxStrip12";
            this.pictureBoxStrip12.Size = new System.Drawing.Size(1531, 95);
            this.pictureBoxStrip12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxStrip12.TabIndex = 3;
            this.pictureBoxStrip12.TabStop = false;
            // 
            // panel344
            // 
            this.panel344.Controls.Add(this.labelStripTime12);
            this.panel344.Controls.Add(this.labelStripDate12);
            this.panel344.Controls.Add(this.labelStripLead12);
            this.panel344.Controls.Add(this.label89);
            this.panel344.Controls.Add(this.label86);
            this.panel344.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel344.Location = new System.Drawing.Point(0, 0);
            this.panel344.Name = "panel344";
            this.panel344.Size = new System.Drawing.Size(121, 95);
            this.panel344.TabIndex = 1;
            // 
            // labelStripTime12
            // 
            this.labelStripTime12.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripTime12.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripTime12.Location = new System.Drawing.Point(0, 58);
            this.labelStripTime12.Name = "labelStripTime12";
            this.labelStripTime12.Size = new System.Drawing.Size(121, 20);
            this.labelStripTime12.TabIndex = 55;
            this.labelStripTime12.Text = "10:15:20 AM";
            this.labelStripTime12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripDate12
            // 
            this.labelStripDate12.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripDate12.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripDate12.Location = new System.Drawing.Point(0, 38);
            this.labelStripDate12.Name = "labelStripDate12";
            this.labelStripDate12.Size = new System.Drawing.Size(121, 20);
            this.labelStripDate12.TabIndex = 54;
            this.labelStripDate12.Text = "01/01/2017";
            this.labelStripDate12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripLead12
            // 
            this.labelStripLead12.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripLead12.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripLead12.Location = new System.Drawing.Point(0, 18);
            this.labelStripLead12.Name = "labelStripLead12";
            this.labelStripLead12.Size = new System.Drawing.Size(121, 20);
            this.labelStripLead12.TabIndex = 53;
            this.labelStripLead12.Text = "LEAD I";
            this.labelStripLead12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelStripLead12.Visible = false;
            // 
            // label89
            // 
            this.label89.Dock = System.Windows.Forms.DockStyle.Top;
            this.label89.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label89.Location = new System.Drawing.Point(0, 0);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(121, 18);
            this.label89.TabIndex = 58;
            this.label89.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label86
            // 
            this.label86.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label86.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.Location = new System.Drawing.Point(0, 77);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(121, 18);
            this.label86.TabIndex = 56;
            this.label86.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel396
            // 
            this.panel396.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel396.Location = new System.Drawing.Point(0, 95);
            this.panel396.Name = "panel396";
            this.panel396.Size = new System.Drawing.Size(1652, 5);
            this.panel396.TabIndex = 56;
            // 
            // panel399
            // 
            this.panel399.Controls.Add(this.panel400);
            this.panel399.Controls.Add(this.panel405);
            this.panel399.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel399.Location = new System.Drawing.Point(0, 1000);
            this.panel399.Name = "panel399";
            this.panel399.Size = new System.Drawing.Size(1652, 64);
            this.panel399.TabIndex = 71;
            // 
            // panel400
            // 
            this.panel400.Controls.Add(this.pictureBoxStrip11);
            this.panel400.Controls.Add(this.panel349);
            this.panel400.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel400.Location = new System.Drawing.Point(0, 0);
            this.panel400.Name = "panel400";
            this.panel400.Size = new System.Drawing.Size(1652, 59);
            this.panel400.TabIndex = 57;
            // 
            // pictureBoxStrip11
            // 
            this.pictureBoxStrip11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxStrip11.BackgroundImage")));
            this.pictureBoxStrip11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxStrip11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxStrip11.InitialImage = null;
            this.pictureBoxStrip11.Location = new System.Drawing.Point(121, 0);
            this.pictureBoxStrip11.Name = "pictureBoxStrip11";
            this.pictureBoxStrip11.Size = new System.Drawing.Size(1531, 59);
            this.pictureBoxStrip11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxStrip11.TabIndex = 3;
            this.pictureBoxStrip11.TabStop = false;
            // 
            // panel349
            // 
            this.panel349.Controls.Add(this.labelStripTime11);
            this.panel349.Controls.Add(this.labelStripDate11);
            this.panel349.Controls.Add(this.labelStripLead11);
            this.panel349.Controls.Add(this.label54);
            this.panel349.Controls.Add(this.label51);
            this.panel349.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel349.Location = new System.Drawing.Point(0, 0);
            this.panel349.Name = "panel349";
            this.panel349.Size = new System.Drawing.Size(121, 59);
            this.panel349.TabIndex = 1;
            // 
            // labelStripTime11
            // 
            this.labelStripTime11.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripTime11.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripTime11.Location = new System.Drawing.Point(0, 58);
            this.labelStripTime11.Name = "labelStripTime11";
            this.labelStripTime11.Size = new System.Drawing.Size(121, 20);
            this.labelStripTime11.TabIndex = 55;
            this.labelStripTime11.Text = "10:15:20 AM";
            this.labelStripTime11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripDate11
            // 
            this.labelStripDate11.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripDate11.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripDate11.Location = new System.Drawing.Point(0, 38);
            this.labelStripDate11.Name = "labelStripDate11";
            this.labelStripDate11.Size = new System.Drawing.Size(121, 20);
            this.labelStripDate11.TabIndex = 54;
            this.labelStripDate11.Text = "01/01/2017";
            this.labelStripDate11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripLead11
            // 
            this.labelStripLead11.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripLead11.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripLead11.Location = new System.Drawing.Point(0, 18);
            this.labelStripLead11.Name = "labelStripLead11";
            this.labelStripLead11.Size = new System.Drawing.Size(121, 20);
            this.labelStripLead11.TabIndex = 53;
            this.labelStripLead11.Text = "LEAD I";
            this.labelStripLead11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelStripLead11.Visible = false;
            // 
            // label54
            // 
            this.label54.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label54.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(0, 41);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(121, 18);
            this.label54.TabIndex = 58;
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label51
            // 
            this.label51.Dock = System.Windows.Forms.DockStyle.Top;
            this.label51.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(0, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(121, 18);
            this.label51.TabIndex = 56;
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel405
            // 
            this.panel405.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel405.Location = new System.Drawing.Point(0, 59);
            this.panel405.Name = "panel405";
            this.panel405.Size = new System.Drawing.Size(1652, 5);
            this.panel405.TabIndex = 56;
            // 
            // panel446
            // 
            this.panel446.Controls.Add(this.panel606);
            this.panel446.Controls.Add(this.panel607);
            this.panel446.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel446.Location = new System.Drawing.Point(0, 900);
            this.panel446.Name = "panel446";
            this.panel446.Size = new System.Drawing.Size(1652, 100);
            this.panel446.TabIndex = 70;
            // 
            // panel606
            // 
            this.panel606.Controls.Add(this.pictureBoxStrip10);
            this.panel606.Controls.Add(this.panel360);
            this.panel606.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel606.Location = new System.Drawing.Point(0, 0);
            this.panel606.Name = "panel606";
            this.panel606.Size = new System.Drawing.Size(1652, 95);
            this.panel606.TabIndex = 57;
            // 
            // pictureBoxStrip10
            // 
            this.pictureBoxStrip10.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxStrip10.BackgroundImage")));
            this.pictureBoxStrip10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxStrip10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxStrip10.InitialImage = null;
            this.pictureBoxStrip10.Location = new System.Drawing.Point(121, 0);
            this.pictureBoxStrip10.Name = "pictureBoxStrip10";
            this.pictureBoxStrip10.Size = new System.Drawing.Size(1531, 95);
            this.pictureBoxStrip10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxStrip10.TabIndex = 3;
            this.pictureBoxStrip10.TabStop = false;
            // 
            // panel360
            // 
            this.panel360.Controls.Add(this.labelStripTime10);
            this.panel360.Controls.Add(this.labelStripDate10);
            this.panel360.Controls.Add(this.labelStripLead10);
            this.panel360.Controls.Add(this.label48);
            this.panel360.Controls.Add(this.label45);
            this.panel360.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel360.Location = new System.Drawing.Point(0, 0);
            this.panel360.Name = "panel360";
            this.panel360.Size = new System.Drawing.Size(121, 95);
            this.panel360.TabIndex = 1;
            // 
            // labelStripTime10
            // 
            this.labelStripTime10.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripTime10.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripTime10.Location = new System.Drawing.Point(0, 58);
            this.labelStripTime10.Name = "labelStripTime10";
            this.labelStripTime10.Size = new System.Drawing.Size(121, 20);
            this.labelStripTime10.TabIndex = 55;
            this.labelStripTime10.Text = "10:15:20 AM";
            this.labelStripTime10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripDate10
            // 
            this.labelStripDate10.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripDate10.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripDate10.Location = new System.Drawing.Point(0, 38);
            this.labelStripDate10.Name = "labelStripDate10";
            this.labelStripDate10.Size = new System.Drawing.Size(121, 20);
            this.labelStripDate10.TabIndex = 54;
            this.labelStripDate10.Text = "01/01/2017";
            this.labelStripDate10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripLead10
            // 
            this.labelStripLead10.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripLead10.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripLead10.Location = new System.Drawing.Point(0, 18);
            this.labelStripLead10.Name = "labelStripLead10";
            this.labelStripLead10.Size = new System.Drawing.Size(121, 20);
            this.labelStripLead10.TabIndex = 53;
            this.labelStripLead10.Text = "LEAD I";
            this.labelStripLead10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelStripLead10.Visible = false;
            // 
            // label48
            // 
            this.label48.Dock = System.Windows.Forms.DockStyle.Top;
            this.label48.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(0, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(121, 18);
            this.label48.TabIndex = 58;
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label45
            // 
            this.label45.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label45.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(0, 77);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(121, 18);
            this.label45.TabIndex = 56;
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel607
            // 
            this.panel607.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel607.Location = new System.Drawing.Point(0, 95);
            this.panel607.Name = "panel607";
            this.panel607.Size = new System.Drawing.Size(1652, 5);
            this.panel607.TabIndex = 56;
            // 
            // panel443
            // 
            this.panel443.Controls.Add(this.panel444);
            this.panel443.Controls.Add(this.panel605);
            this.panel443.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel443.Location = new System.Drawing.Point(0, 800);
            this.panel443.Name = "panel443";
            this.panel443.Size = new System.Drawing.Size(1652, 100);
            this.panel443.TabIndex = 69;
            // 
            // panel444
            // 
            this.panel444.Controls.Add(this.pictureBoxStrip9);
            this.panel444.Controls.Add(this.panel397);
            this.panel444.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel444.Location = new System.Drawing.Point(0, 0);
            this.panel444.Name = "panel444";
            this.panel444.Size = new System.Drawing.Size(1652, 95);
            this.panel444.TabIndex = 57;
            // 
            // pictureBoxStrip9
            // 
            this.pictureBoxStrip9.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxStrip9.BackgroundImage")));
            this.pictureBoxStrip9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxStrip9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxStrip9.InitialImage = null;
            this.pictureBoxStrip9.Location = new System.Drawing.Point(121, 0);
            this.pictureBoxStrip9.Name = "pictureBoxStrip9";
            this.pictureBoxStrip9.Size = new System.Drawing.Size(1531, 95);
            this.pictureBoxStrip9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxStrip9.TabIndex = 3;
            this.pictureBoxStrip9.TabStop = false;
            // 
            // panel397
            // 
            this.panel397.Controls.Add(this.labelStripTime9);
            this.panel397.Controls.Add(this.labelStripDate9);
            this.panel397.Controls.Add(this.labelStripLead9);
            this.panel397.Controls.Add(this.label42);
            this.panel397.Controls.Add(this.label39);
            this.panel397.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel397.Location = new System.Drawing.Point(0, 0);
            this.panel397.Name = "panel397";
            this.panel397.Size = new System.Drawing.Size(121, 95);
            this.panel397.TabIndex = 1;
            // 
            // labelStripTime9
            // 
            this.labelStripTime9.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripTime9.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripTime9.Location = new System.Drawing.Point(0, 58);
            this.labelStripTime9.Name = "labelStripTime9";
            this.labelStripTime9.Size = new System.Drawing.Size(121, 20);
            this.labelStripTime9.TabIndex = 55;
            this.labelStripTime9.Text = "10:15:20 AM";
            this.labelStripTime9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripDate9
            // 
            this.labelStripDate9.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripDate9.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripDate9.Location = new System.Drawing.Point(0, 38);
            this.labelStripDate9.Name = "labelStripDate9";
            this.labelStripDate9.Size = new System.Drawing.Size(121, 20);
            this.labelStripDate9.TabIndex = 54;
            this.labelStripDate9.Text = "01/01/2017";
            this.labelStripDate9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripLead9
            // 
            this.labelStripLead9.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripLead9.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripLead9.Location = new System.Drawing.Point(0, 18);
            this.labelStripLead9.Name = "labelStripLead9";
            this.labelStripLead9.Size = new System.Drawing.Size(121, 20);
            this.labelStripLead9.TabIndex = 53;
            this.labelStripLead9.Text = "LEAD I";
            this.labelStripLead9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelStripLead9.Visible = false;
            // 
            // label42
            // 
            this.label42.Dock = System.Windows.Forms.DockStyle.Top;
            this.label42.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(0, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(121, 18);
            this.label42.TabIndex = 58;
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label39
            // 
            this.label39.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label39.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(0, 77);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(121, 18);
            this.label39.TabIndex = 56;
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel605
            // 
            this.panel605.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel605.Location = new System.Drawing.Point(0, 95);
            this.panel605.Name = "panel605";
            this.panel605.Size = new System.Drawing.Size(1652, 5);
            this.panel605.TabIndex = 56;
            // 
            // panel439
            // 
            this.panel439.Controls.Add(this.panel441);
            this.panel439.Controls.Add(this.panel442);
            this.panel439.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel439.Location = new System.Drawing.Point(0, 700);
            this.panel439.Name = "panel439";
            this.panel439.Size = new System.Drawing.Size(1652, 100);
            this.panel439.TabIndex = 68;
            // 
            // panel441
            // 
            this.panel441.Controls.Add(this.pictureBoxStrip8);
            this.panel441.Controls.Add(this.panel403);
            this.panel441.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel441.Location = new System.Drawing.Point(0, 0);
            this.panel441.Name = "panel441";
            this.panel441.Size = new System.Drawing.Size(1652, 95);
            this.panel441.TabIndex = 57;
            // 
            // pictureBoxStrip8
            // 
            this.pictureBoxStrip8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxStrip8.BackgroundImage")));
            this.pictureBoxStrip8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxStrip8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxStrip8.InitialImage = null;
            this.pictureBoxStrip8.Location = new System.Drawing.Point(121, 0);
            this.pictureBoxStrip8.Name = "pictureBoxStrip8";
            this.pictureBoxStrip8.Size = new System.Drawing.Size(1531, 95);
            this.pictureBoxStrip8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxStrip8.TabIndex = 3;
            this.pictureBoxStrip8.TabStop = false;
            // 
            // panel403
            // 
            this.panel403.Controls.Add(this.labelStripTime8);
            this.panel403.Controls.Add(this.labelStripDate8);
            this.panel403.Controls.Add(this.labelStripLead8);
            this.panel403.Controls.Add(this.label35);
            this.panel403.Controls.Add(this.label31);
            this.panel403.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel403.Location = new System.Drawing.Point(0, 0);
            this.panel403.Name = "panel403";
            this.panel403.Size = new System.Drawing.Size(121, 95);
            this.panel403.TabIndex = 1;
            // 
            // labelStripTime8
            // 
            this.labelStripTime8.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripTime8.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripTime8.Location = new System.Drawing.Point(0, 58);
            this.labelStripTime8.Name = "labelStripTime8";
            this.labelStripTime8.Size = new System.Drawing.Size(121, 20);
            this.labelStripTime8.TabIndex = 55;
            this.labelStripTime8.Text = "10:15:20 AM";
            this.labelStripTime8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripDate8
            // 
            this.labelStripDate8.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripDate8.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripDate8.Location = new System.Drawing.Point(0, 38);
            this.labelStripDate8.Name = "labelStripDate8";
            this.labelStripDate8.Size = new System.Drawing.Size(121, 20);
            this.labelStripDate8.TabIndex = 54;
            this.labelStripDate8.Text = "01/01/2017";
            this.labelStripDate8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripLead8
            // 
            this.labelStripLead8.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripLead8.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripLead8.Location = new System.Drawing.Point(0, 18);
            this.labelStripLead8.Name = "labelStripLead8";
            this.labelStripLead8.Size = new System.Drawing.Size(121, 20);
            this.labelStripLead8.TabIndex = 53;
            this.labelStripLead8.Text = "LEAD I";
            this.labelStripLead8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelStripLead8.Visible = false;
            // 
            // label35
            // 
            this.label35.Dock = System.Windows.Forms.DockStyle.Top;
            this.label35.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(0, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(121, 18);
            this.label35.TabIndex = 58;
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label31
            // 
            this.label31.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label31.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(0, 77);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(121, 18);
            this.label31.TabIndex = 56;
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel442
            // 
            this.panel442.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel442.Location = new System.Drawing.Point(0, 95);
            this.panel442.Name = "panel442";
            this.panel442.Size = new System.Drawing.Size(1652, 5);
            this.panel442.TabIndex = 56;
            // 
            // panel433
            // 
            this.panel433.Controls.Add(this.panel437);
            this.panel433.Controls.Add(this.panel438);
            this.panel433.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel433.Location = new System.Drawing.Point(0, 600);
            this.panel433.Name = "panel433";
            this.panel433.Size = new System.Drawing.Size(1652, 100);
            this.panel433.TabIndex = 67;
            // 
            // panel437
            // 
            this.panel437.Controls.Add(this.pictureBoxStrip7);
            this.panel437.Controls.Add(this.panel408);
            this.panel437.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel437.Location = new System.Drawing.Point(0, 0);
            this.panel437.Name = "panel437";
            this.panel437.Size = new System.Drawing.Size(1652, 95);
            this.panel437.TabIndex = 57;
            // 
            // pictureBoxStrip7
            // 
            this.pictureBoxStrip7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxStrip7.BackgroundImage")));
            this.pictureBoxStrip7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxStrip7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxStrip7.InitialImage = null;
            this.pictureBoxStrip7.Location = new System.Drawing.Point(121, 0);
            this.pictureBoxStrip7.Name = "pictureBoxStrip7";
            this.pictureBoxStrip7.Size = new System.Drawing.Size(1531, 95);
            this.pictureBoxStrip7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxStrip7.TabIndex = 3;
            this.pictureBoxStrip7.TabStop = false;
            // 
            // panel408
            // 
            this.panel408.Controls.Add(this.labelStripTime7);
            this.panel408.Controls.Add(this.labelStripDate7);
            this.panel408.Controls.Add(this.labelStripLead7);
            this.panel408.Controls.Add(this.label83);
            this.panel408.Controls.Add(this.label80);
            this.panel408.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel408.Location = new System.Drawing.Point(0, 0);
            this.panel408.Name = "panel408";
            this.panel408.Size = new System.Drawing.Size(121, 95);
            this.panel408.TabIndex = 1;
            // 
            // labelStripTime7
            // 
            this.labelStripTime7.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripTime7.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripTime7.Location = new System.Drawing.Point(0, 58);
            this.labelStripTime7.Name = "labelStripTime7";
            this.labelStripTime7.Size = new System.Drawing.Size(121, 20);
            this.labelStripTime7.TabIndex = 55;
            this.labelStripTime7.Text = "10:15:20 AM";
            this.labelStripTime7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripDate7
            // 
            this.labelStripDate7.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripDate7.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripDate7.Location = new System.Drawing.Point(0, 38);
            this.labelStripDate7.Name = "labelStripDate7";
            this.labelStripDate7.Size = new System.Drawing.Size(121, 20);
            this.labelStripDate7.TabIndex = 54;
            this.labelStripDate7.Text = "01/01/2017";
            this.labelStripDate7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripLead7
            // 
            this.labelStripLead7.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripLead7.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripLead7.Location = new System.Drawing.Point(0, 18);
            this.labelStripLead7.Name = "labelStripLead7";
            this.labelStripLead7.Size = new System.Drawing.Size(121, 20);
            this.labelStripLead7.TabIndex = 53;
            this.labelStripLead7.Text = "LEAD I";
            this.labelStripLead7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelStripLead7.Visible = false;
            // 
            // label83
            // 
            this.label83.Dock = System.Windows.Forms.DockStyle.Top;
            this.label83.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.Location = new System.Drawing.Point(0, 0);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(121, 18);
            this.label83.TabIndex = 58;
            this.label83.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label80
            // 
            this.label80.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label80.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.Location = new System.Drawing.Point(0, 77);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(121, 18);
            this.label80.TabIndex = 56;
            this.label80.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel438
            // 
            this.panel438.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel438.Location = new System.Drawing.Point(0, 95);
            this.panel438.Name = "panel438";
            this.panel438.Size = new System.Drawing.Size(1652, 5);
            this.panel438.TabIndex = 56;
            // 
            // panel431
            // 
            this.panel431.Controls.Add(this.panel432);
            this.panel431.Controls.Add(this.panel436);
            this.panel431.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel431.Location = new System.Drawing.Point(0, 500);
            this.panel431.Name = "panel431";
            this.panel431.Size = new System.Drawing.Size(1652, 100);
            this.panel431.TabIndex = 66;
            // 
            // panel432
            // 
            this.panel432.Controls.Add(this.pictureBoxStrip6);
            this.panel432.Controls.Add(this.panel413);
            this.panel432.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel432.Location = new System.Drawing.Point(0, 0);
            this.panel432.Name = "panel432";
            this.panel432.Size = new System.Drawing.Size(1652, 95);
            this.panel432.TabIndex = 57;
            // 
            // pictureBoxStrip6
            // 
            this.pictureBoxStrip6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxStrip6.BackgroundImage")));
            this.pictureBoxStrip6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxStrip6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxStrip6.InitialImage = null;
            this.pictureBoxStrip6.Location = new System.Drawing.Point(121, 0);
            this.pictureBoxStrip6.Name = "pictureBoxStrip6";
            this.pictureBoxStrip6.Size = new System.Drawing.Size(1531, 95);
            this.pictureBoxStrip6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxStrip6.TabIndex = 3;
            this.pictureBoxStrip6.TabStop = false;
            // 
            // panel413
            // 
            this.panel413.Controls.Add(this.labelStripTime6);
            this.panel413.Controls.Add(this.labelStripDate6);
            this.panel413.Controls.Add(this.labelStripLead6);
            this.panel413.Controls.Add(this.label77);
            this.panel413.Controls.Add(this.label74);
            this.panel413.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel413.Location = new System.Drawing.Point(0, 0);
            this.panel413.Name = "panel413";
            this.panel413.Size = new System.Drawing.Size(121, 95);
            this.panel413.TabIndex = 1;
            // 
            // labelStripTime6
            // 
            this.labelStripTime6.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripTime6.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripTime6.Location = new System.Drawing.Point(0, 58);
            this.labelStripTime6.Name = "labelStripTime6";
            this.labelStripTime6.Size = new System.Drawing.Size(121, 20);
            this.labelStripTime6.TabIndex = 55;
            this.labelStripTime6.Text = "10:15:20 AM";
            this.labelStripTime6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripDate6
            // 
            this.labelStripDate6.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripDate6.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripDate6.Location = new System.Drawing.Point(0, 38);
            this.labelStripDate6.Name = "labelStripDate6";
            this.labelStripDate6.Size = new System.Drawing.Size(121, 20);
            this.labelStripDate6.TabIndex = 54;
            this.labelStripDate6.Text = "01/01/2017";
            this.labelStripDate6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripLead6
            // 
            this.labelStripLead6.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripLead6.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripLead6.Location = new System.Drawing.Point(0, 18);
            this.labelStripLead6.Name = "labelStripLead6";
            this.labelStripLead6.Size = new System.Drawing.Size(121, 20);
            this.labelStripLead6.TabIndex = 53;
            this.labelStripLead6.Text = "LEAD I";
            this.labelStripLead6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelStripLead6.Visible = false;
            // 
            // label77
            // 
            this.label77.Dock = System.Windows.Forms.DockStyle.Top;
            this.label77.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.Location = new System.Drawing.Point(0, 0);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(121, 18);
            this.label77.TabIndex = 58;
            this.label77.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label74
            // 
            this.label74.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label74.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.Location = new System.Drawing.Point(0, 77);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(121, 18);
            this.label74.TabIndex = 56;
            this.label74.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel436
            // 
            this.panel436.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel436.Location = new System.Drawing.Point(0, 95);
            this.panel436.Name = "panel436";
            this.panel436.Size = new System.Drawing.Size(1652, 5);
            this.panel436.TabIndex = 56;
            // 
            // panel602
            // 
            this.panel602.Controls.Add(this.panel603);
            this.panel602.Controls.Add(this.panel604);
            this.panel602.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel602.Location = new System.Drawing.Point(0, 400);
            this.panel602.Name = "panel602";
            this.panel602.Size = new System.Drawing.Size(1652, 100);
            this.panel602.TabIndex = 65;
            // 
            // panel603
            // 
            this.panel603.Controls.Add(this.pictureBoxStrip5);
            this.panel603.Controls.Add(this.panel434);
            this.panel603.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel603.Location = new System.Drawing.Point(0, 0);
            this.panel603.Name = "panel603";
            this.panel603.Size = new System.Drawing.Size(1652, 95);
            this.panel603.TabIndex = 57;
            // 
            // pictureBoxStrip5
            // 
            this.pictureBoxStrip5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxStrip5.BackgroundImage")));
            this.pictureBoxStrip5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxStrip5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxStrip5.InitialImage = null;
            this.pictureBoxStrip5.Location = new System.Drawing.Point(121, 0);
            this.pictureBoxStrip5.Name = "pictureBoxStrip5";
            this.pictureBoxStrip5.Size = new System.Drawing.Size(1531, 95);
            this.pictureBoxStrip5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxStrip5.TabIndex = 2;
            this.pictureBoxStrip5.TabStop = false;
            // 
            // panel434
            // 
            this.panel434.Controls.Add(this.labelStripTime5);
            this.panel434.Controls.Add(this.labelStripDate5);
            this.panel434.Controls.Add(this.labelStripLead5);
            this.panel434.Controls.Add(this.label71);
            this.panel434.Controls.Add(this.label68);
            this.panel434.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel434.Location = new System.Drawing.Point(0, 0);
            this.panel434.Name = "panel434";
            this.panel434.Size = new System.Drawing.Size(121, 95);
            this.panel434.TabIndex = 1;
            // 
            // labelStripTime5
            // 
            this.labelStripTime5.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripTime5.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripTime5.Location = new System.Drawing.Point(0, 58);
            this.labelStripTime5.Name = "labelStripTime5";
            this.labelStripTime5.Size = new System.Drawing.Size(121, 20);
            this.labelStripTime5.TabIndex = 55;
            this.labelStripTime5.Text = "10:15:20 AM";
            this.labelStripTime5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripDate5
            // 
            this.labelStripDate5.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripDate5.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripDate5.Location = new System.Drawing.Point(0, 38);
            this.labelStripDate5.Name = "labelStripDate5";
            this.labelStripDate5.Size = new System.Drawing.Size(121, 20);
            this.labelStripDate5.TabIndex = 54;
            this.labelStripDate5.Text = "01/01/2017";
            this.labelStripDate5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripLead5
            // 
            this.labelStripLead5.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripLead5.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripLead5.Location = new System.Drawing.Point(0, 18);
            this.labelStripLead5.Name = "labelStripLead5";
            this.labelStripLead5.Size = new System.Drawing.Size(121, 20);
            this.labelStripLead5.TabIndex = 53;
            this.labelStripLead5.Text = "LEAD I";
            this.labelStripLead5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelStripLead5.Visible = false;
            // 
            // label71
            // 
            this.label71.Dock = System.Windows.Forms.DockStyle.Top;
            this.label71.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.Location = new System.Drawing.Point(0, 0);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(121, 18);
            this.label71.TabIndex = 58;
            this.label71.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label68
            // 
            this.label68.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label68.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.Location = new System.Drawing.Point(0, 77);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(121, 18);
            this.label68.TabIndex = 56;
            this.label68.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel604
            // 
            this.panel604.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel604.Location = new System.Drawing.Point(0, 95);
            this.panel604.Name = "panel604";
            this.panel604.Size = new System.Drawing.Size(1652, 5);
            this.panel604.TabIndex = 56;
            // 
            // panel598
            // 
            this.panel598.Controls.Add(this.panel600);
            this.panel598.Controls.Add(this.panel601);
            this.panel598.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel598.Location = new System.Drawing.Point(0, 300);
            this.panel598.Name = "panel598";
            this.panel598.Size = new System.Drawing.Size(1652, 100);
            this.panel598.TabIndex = 64;
            // 
            // panel600
            // 
            this.panel600.Controls.Add(this.pictureBoxStrip4);
            this.panel600.Controls.Add(this.panel440);
            this.panel600.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel600.Location = new System.Drawing.Point(0, 0);
            this.panel600.Name = "panel600";
            this.panel600.Size = new System.Drawing.Size(1652, 95);
            this.panel600.TabIndex = 57;
            // 
            // pictureBoxStrip4
            // 
            this.pictureBoxStrip4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxStrip4.BackgroundImage")));
            this.pictureBoxStrip4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxStrip4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxStrip4.InitialImage = null;
            this.pictureBoxStrip4.Location = new System.Drawing.Point(121, 0);
            this.pictureBoxStrip4.Name = "pictureBoxStrip4";
            this.pictureBoxStrip4.Size = new System.Drawing.Size(1531, 95);
            this.pictureBoxStrip4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxStrip4.TabIndex = 2;
            this.pictureBoxStrip4.TabStop = false;
            // 
            // panel440
            // 
            this.panel440.Controls.Add(this.labelStripTime4);
            this.panel440.Controls.Add(this.labelStripDate4);
            this.panel440.Controls.Add(this.labelStripLead4);
            this.panel440.Controls.Add(this.label27);
            this.panel440.Controls.Add(this.label24);
            this.panel440.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel440.Location = new System.Drawing.Point(0, 0);
            this.panel440.Name = "panel440";
            this.panel440.Size = new System.Drawing.Size(121, 95);
            this.panel440.TabIndex = 1;
            // 
            // labelStripTime4
            // 
            this.labelStripTime4.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripTime4.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripTime4.Location = new System.Drawing.Point(0, 58);
            this.labelStripTime4.Name = "labelStripTime4";
            this.labelStripTime4.Size = new System.Drawing.Size(121, 20);
            this.labelStripTime4.TabIndex = 55;
            this.labelStripTime4.Text = "10:15:20 AM";
            this.labelStripTime4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripDate4
            // 
            this.labelStripDate4.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripDate4.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripDate4.Location = new System.Drawing.Point(0, 38);
            this.labelStripDate4.Name = "labelStripDate4";
            this.labelStripDate4.Size = new System.Drawing.Size(121, 20);
            this.labelStripDate4.TabIndex = 54;
            this.labelStripDate4.Text = "01/01/2017";
            this.labelStripDate4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripLead4
            // 
            this.labelStripLead4.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripLead4.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripLead4.Location = new System.Drawing.Point(0, 18);
            this.labelStripLead4.Name = "labelStripLead4";
            this.labelStripLead4.Size = new System.Drawing.Size(121, 20);
            this.labelStripLead4.TabIndex = 53;
            this.labelStripLead4.Text = "LEAD I";
            this.labelStripLead4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelStripLead4.Visible = false;
            // 
            // label27
            // 
            this.label27.Dock = System.Windows.Forms.DockStyle.Top;
            this.label27.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(0, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(121, 18);
            this.label27.TabIndex = 58;
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label24
            // 
            this.label24.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label24.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(0, 77);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(121, 18);
            this.label24.TabIndex = 56;
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel601
            // 
            this.panel601.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel601.Location = new System.Drawing.Point(0, 95);
            this.panel601.Name = "panel601";
            this.panel601.Size = new System.Drawing.Size(1652, 5);
            this.panel601.TabIndex = 56;
            // 
            // panel596
            // 
            this.panel596.Controls.Add(this.panel597);
            this.panel596.Controls.Add(this.panel599);
            this.panel596.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel596.Location = new System.Drawing.Point(0, 200);
            this.panel596.Name = "panel596";
            this.panel596.Size = new System.Drawing.Size(1652, 100);
            this.panel596.TabIndex = 63;
            // 
            // panel597
            // 
            this.panel597.Controls.Add(this.pictureBoxStrip3);
            this.panel597.Controls.Add(this.panel445);
            this.panel597.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel597.Location = new System.Drawing.Point(0, 0);
            this.panel597.Name = "panel597";
            this.panel597.Size = new System.Drawing.Size(1652, 95);
            this.panel597.TabIndex = 57;
            // 
            // pictureBoxStrip3
            // 
            this.pictureBoxStrip3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxStrip3.BackgroundImage")));
            this.pictureBoxStrip3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxStrip3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxStrip3.InitialImage = null;
            this.pictureBoxStrip3.Location = new System.Drawing.Point(121, 0);
            this.pictureBoxStrip3.Name = "pictureBoxStrip3";
            this.pictureBoxStrip3.Size = new System.Drawing.Size(1531, 95);
            this.pictureBoxStrip3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxStrip3.TabIndex = 2;
            this.pictureBoxStrip3.TabStop = false;
            // 
            // panel445
            // 
            this.panel445.Controls.Add(this.labelStripTime3);
            this.panel445.Controls.Add(this.labelStripDate3);
            this.panel445.Controls.Add(this.labelStripLead3);
            this.panel445.Controls.Add(this.label66);
            this.panel445.Controls.Add(this.label22);
            this.panel445.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel445.Location = new System.Drawing.Point(0, 0);
            this.panel445.Name = "panel445";
            this.panel445.Size = new System.Drawing.Size(121, 95);
            this.panel445.TabIndex = 1;
            // 
            // labelStripTime3
            // 
            this.labelStripTime3.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripTime3.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripTime3.Location = new System.Drawing.Point(0, 58);
            this.labelStripTime3.Name = "labelStripTime3";
            this.labelStripTime3.Size = new System.Drawing.Size(121, 20);
            this.labelStripTime3.TabIndex = 55;
            this.labelStripTime3.Text = "10:15:20 AM";
            this.labelStripTime3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripDate3
            // 
            this.labelStripDate3.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripDate3.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripDate3.Location = new System.Drawing.Point(0, 38);
            this.labelStripDate3.Name = "labelStripDate3";
            this.labelStripDate3.Size = new System.Drawing.Size(121, 20);
            this.labelStripDate3.TabIndex = 54;
            this.labelStripDate3.Text = "01/01/2017";
            this.labelStripDate3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripLead3
            // 
            this.labelStripLead3.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripLead3.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripLead3.Location = new System.Drawing.Point(0, 18);
            this.labelStripLead3.Name = "labelStripLead3";
            this.labelStripLead3.Size = new System.Drawing.Size(121, 20);
            this.labelStripLead3.TabIndex = 53;
            this.labelStripLead3.Text = "LEAD I";
            this.labelStripLead3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelStripLead3.Visible = false;
            // 
            // label66
            // 
            this.label66.Dock = System.Windows.Forms.DockStyle.Top;
            this.label66.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.Location = new System.Drawing.Point(0, 0);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(121, 18);
            this.label66.TabIndex = 58;
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label22
            // 
            this.label22.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label22.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(0, 77);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(121, 18);
            this.label22.TabIndex = 56;
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel599
            // 
            this.panel599.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel599.Location = new System.Drawing.Point(0, 95);
            this.panel599.Name = "panel599";
            this.panel599.Size = new System.Drawing.Size(1652, 5);
            this.panel599.TabIndex = 56;
            // 
            // panel494
            // 
            this.panel494.Controls.Add(this.panel498);
            this.panel494.Controls.Add(this.panel595);
            this.panel494.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel494.Location = new System.Drawing.Point(0, 100);
            this.panel494.Name = "panel494";
            this.panel494.Size = new System.Drawing.Size(1652, 100);
            this.panel494.TabIndex = 62;
            // 
            // panel498
            // 
            this.panel498.Controls.Add(this.pictureBoxStrip2);
            this.panel498.Controls.Add(this.panel502);
            this.panel498.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel498.Location = new System.Drawing.Point(0, 0);
            this.panel498.Name = "panel498";
            this.panel498.Size = new System.Drawing.Size(1652, 95);
            this.panel498.TabIndex = 57;
            // 
            // pictureBoxStrip2
            // 
            this.pictureBoxStrip2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxStrip2.BackgroundImage")));
            this.pictureBoxStrip2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxStrip2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxStrip2.InitialImage = null;
            this.pictureBoxStrip2.Location = new System.Drawing.Point(121, 0);
            this.pictureBoxStrip2.Name = "pictureBoxStrip2";
            this.pictureBoxStrip2.Size = new System.Drawing.Size(1531, 95);
            this.pictureBoxStrip2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxStrip2.TabIndex = 2;
            this.pictureBoxStrip2.TabStop = false;
            // 
            // panel502
            // 
            this.panel502.Controls.Add(this.labelStripTime2);
            this.panel502.Controls.Add(this.labelStripDate2);
            this.panel502.Controls.Add(this.labelStripLead2);
            this.panel502.Controls.Add(this.label21);
            this.panel502.Controls.Add(this.label18);
            this.panel502.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel502.Location = new System.Drawing.Point(0, 0);
            this.panel502.Name = "panel502";
            this.panel502.Size = new System.Drawing.Size(121, 95);
            this.panel502.TabIndex = 1;
            // 
            // labelStripTime2
            // 
            this.labelStripTime2.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripTime2.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripTime2.Location = new System.Drawing.Point(0, 58);
            this.labelStripTime2.Name = "labelStripTime2";
            this.labelStripTime2.Size = new System.Drawing.Size(121, 20);
            this.labelStripTime2.TabIndex = 49;
            this.labelStripTime2.Text = "10:15:20 AM";
            this.labelStripTime2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripDate2
            // 
            this.labelStripDate2.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripDate2.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripDate2.Location = new System.Drawing.Point(0, 38);
            this.labelStripDate2.Name = "labelStripDate2";
            this.labelStripDate2.Size = new System.Drawing.Size(121, 20);
            this.labelStripDate2.TabIndex = 48;
            this.labelStripDate2.Text = "01/01/2017";
            this.labelStripDate2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripLead2
            // 
            this.labelStripLead2.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripLead2.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripLead2.Location = new System.Drawing.Point(0, 18);
            this.labelStripLead2.Name = "labelStripLead2";
            this.labelStripLead2.Size = new System.Drawing.Size(121, 20);
            this.labelStripLead2.TabIndex = 47;
            this.labelStripLead2.Text = "LEAD I";
            this.labelStripLead2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelStripLead2.Visible = false;
            // 
            // label21
            // 
            this.label21.Dock = System.Windows.Forms.DockStyle.Top;
            this.label21.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(0, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(121, 18);
            this.label21.TabIndex = 52;
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label18
            // 
            this.label18.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label18.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(0, 77);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(121, 18);
            this.label18.TabIndex = 50;
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel595
            // 
            this.panel595.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel595.Location = new System.Drawing.Point(0, 95);
            this.panel595.Name = "panel595";
            this.panel595.Size = new System.Drawing.Size(1652, 5);
            this.panel595.TabIndex = 56;
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.panel92);
            this.panel19.Controls.Add(this.panel23);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel19.Location = new System.Drawing.Point(0, 0);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(1652, 100);
            this.panel19.TabIndex = 61;
            // 
            // panel92
            // 
            this.panel92.Controls.Add(this.pictureBoxStrip1);
            this.panel92.Controls.Add(this.panel511);
            this.panel92.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel92.Location = new System.Drawing.Point(0, 0);
            this.panel92.Name = "panel92";
            this.panel92.Size = new System.Drawing.Size(1652, 95);
            this.panel92.TabIndex = 57;
            // 
            // pictureBoxStrip1
            // 
            this.pictureBoxStrip1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxStrip1.BackgroundImage")));
            this.pictureBoxStrip1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxStrip1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxStrip1.InitialImage = null;
            this.pictureBoxStrip1.Location = new System.Drawing.Point(121, 0);
            this.pictureBoxStrip1.Name = "pictureBoxStrip1";
            this.pictureBoxStrip1.Size = new System.Drawing.Size(1531, 95);
            this.pictureBoxStrip1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxStrip1.TabIndex = 2;
            this.pictureBoxStrip1.TabStop = false;
            // 
            // panel511
            // 
            this.panel511.Controls.Add(this.labelStripTime1);
            this.panel511.Controls.Add(this.labelStripDate1);
            this.panel511.Controls.Add(this.labelStripLead1);
            this.panel511.Controls.Add(this.label136);
            this.panel511.Controls.Add(this.label139);
            this.panel511.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel511.Location = new System.Drawing.Point(0, 0);
            this.panel511.Name = "panel511";
            this.panel511.Size = new System.Drawing.Size(121, 95);
            this.panel511.TabIndex = 1;
            // 
            // labelStripTime1
            // 
            this.labelStripTime1.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripTime1.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripTime1.Location = new System.Drawing.Point(0, 58);
            this.labelStripTime1.Name = "labelStripTime1";
            this.labelStripTime1.Size = new System.Drawing.Size(121, 20);
            this.labelStripTime1.TabIndex = 49;
            this.labelStripTime1.Text = "10:15:20 AM";
            this.labelStripTime1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripDate1
            // 
            this.labelStripDate1.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripDate1.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripDate1.Location = new System.Drawing.Point(0, 38);
            this.labelStripDate1.Name = "labelStripDate1";
            this.labelStripDate1.Size = new System.Drawing.Size(121, 20);
            this.labelStripDate1.TabIndex = 48;
            this.labelStripDate1.Text = "01/01/2017";
            this.labelStripDate1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelStripDate1.Click += new System.EventHandler(this.labelStripDate1_Click);
            // 
            // labelStripLead1
            // 
            this.labelStripLead1.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStripLead1.Font = new System.Drawing.Font("Verdana", 12F);
            this.labelStripLead1.Location = new System.Drawing.Point(0, 18);
            this.labelStripLead1.Name = "labelStripLead1";
            this.labelStripLead1.Size = new System.Drawing.Size(121, 20);
            this.labelStripLead1.TabIndex = 47;
            this.labelStripLead1.Text = "LEAD I";
            this.labelStripLead1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelStripLead1.Visible = false;
            // 
            // label136
            // 
            this.label136.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label136.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label136.Location = new System.Drawing.Point(0, 77);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(121, 18);
            this.label136.TabIndex = 50;
            this.label136.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label139
            // 
            this.label139.Dock = System.Windows.Forms.DockStyle.Top;
            this.label139.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label139.Location = new System.Drawing.Point(0, 0);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(121, 18);
            this.label139.TabIndex = 52;
            this.label139.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel23
            // 
            this.panel23.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel23.Location = new System.Drawing.Point(0, 95);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(1652, 5);
            this.panel23.TabIndex = 56;
            // 
            // panel469
            // 
            this.panel469.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel469.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel469.Location = new System.Drawing.Point(0, 194);
            this.panel469.Name = "panel469";
            this.panel469.Size = new System.Drawing.Size(1654, 2);
            this.panel469.TabIndex = 20;
            // 
            // panel496
            // 
            this.panel496.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel496.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel496.Location = new System.Drawing.Point(0, 184);
            this.panel496.Name = "panel496";
            this.panel496.Size = new System.Drawing.Size(1654, 10);
            this.panel496.TabIndex = 10;
            // 
            // panel512
            // 
            this.panel512.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel512.Controls.Add(this.label16);
            this.panel512.Controls.Add(this.labelStartPageDateTime);
            this.panel512.Controls.Add(this.labelStripLength);
            this.panel512.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel512.Location = new System.Drawing.Point(0, 144);
            this.panel512.Name = "panel512";
            this.panel512.Size = new System.Drawing.Size(1654, 40);
            this.panel512.TabIndex = 9;
            // 
            // label16
            // 
            this.label16.Dock = System.Windows.Forms.DockStyle.Right;
            this.label16.Font = new System.Drawing.Font("Verdana", 20F);
            this.label16.Location = new System.Drawing.Point(1220, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(212, 38);
            this.label16.TabIndex = 59;
            this.label16.Text = "Strip length:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelStartPageDateTime
            // 
            this.labelStartPageDateTime.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelStartPageDateTime.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelStartPageDateTime.Location = new System.Drawing.Point(0, 0);
            this.labelStartPageDateTime.Name = "labelStartPageDateTime";
            this.labelStartPageDateTime.Size = new System.Drawing.Size(1437, 38);
            this.labelStartPageDateTime.TabIndex = 4;
            this.labelStartPageDateTime.Text = "12/21/2017 10:11:00 AM";
            this.labelStartPageDateTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelStripLength
            // 
            this.labelStripLength.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelStripLength.Font = new System.Drawing.Font("Verdana", 20F);
            this.labelStripLength.Location = new System.Drawing.Point(1432, 0);
            this.labelStripLength.Name = "labelStripLength";
            this.labelStripLength.Size = new System.Drawing.Size(220, 38);
            this.labelStripLength.TabIndex = 58;
            this.labelStripLength.Text = "<6 seconds>";
            this.labelStripLength.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelStripLength.Click += new System.EventHandler(this.labelStripLength3_Click);
            // 
            // panel514
            // 
            this.panel514.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel514.Controls.Add(this.label28);
            this.panel514.Controls.Add(this.labelEventDataTime);
            this.panel514.Controls.Add(this.label141);
            this.panel514.Controls.Add(this.panel515);
            this.panel514.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel514.Location = new System.Drawing.Point(0, 104);
            this.panel514.Name = "panel514";
            this.panel514.Size = new System.Drawing.Size(1654, 40);
            this.panel514.TabIndex = 7;
            // 
            // label28
            // 
            this.label28.Dock = System.Windows.Forms.DockStyle.Right;
            this.label28.Font = new System.Drawing.Font("Verdana", 20F);
            this.label28.Location = new System.Drawing.Point(964, 2);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(223, 36);
            this.label28.TabIndex = 11;
            this.label28.Text = "Event at:";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelEventDataTime
            // 
            this.labelEventDataTime.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelEventDataTime.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEventDataTime.Location = new System.Drawing.Point(1187, 2);
            this.labelEventDataTime.Name = "labelEventDataTime";
            this.labelEventDataTime.Size = new System.Drawing.Size(465, 36);
            this.labelEventDataTime.TabIndex = 10;
            this.labelEventDataTime.Text = "12/21/2017 10:11:00 AM";
            this.labelEventDataTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label141
            // 
            this.label141.Dock = System.Windows.Forms.DockStyle.Left;
            this.label141.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold);
            this.label141.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label141.Location = new System.Drawing.Point(0, 2);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(318, 36);
            this.label141.TabIndex = 9;
            this.label141.Text = "Full disclosure";
            this.label141.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel515
            // 
            this.panel515.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel515.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel515.Location = new System.Drawing.Point(0, 0);
            this.panel515.Name = "panel515";
            this.panel515.Size = new System.Drawing.Size(1652, 2);
            this.panel515.TabIndex = 1;
            // 
            // panel535
            // 
            this.panel535.Controls.Add(this.panel536);
            this.panel535.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel535.Location = new System.Drawing.Point(0, 94);
            this.panel535.Name = "panel535";
            this.panel535.Size = new System.Drawing.Size(1654, 10);
            this.panel535.TabIndex = 5;
            // 
            // panel536
            // 
            this.panel536.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel536.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel536.Location = new System.Drawing.Point(0, 0);
            this.panel536.Name = "panel536";
            this.panel536.Size = new System.Drawing.Size(1654, 2);
            this.panel536.TabIndex = 0;
            // 
            // panel553
            // 
            this.panel553.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel553.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel553.Location = new System.Drawing.Point(0, 93);
            this.panel553.Name = "panel553";
            this.panel553.Size = new System.Drawing.Size(1654, 1);
            this.panel553.TabIndex = 3;
            // 
            // panel554
            // 
            this.panel554.Controls.Add(this.labelReportText3);
            this.panel554.Controls.Add(this.panel53);
            this.panel554.Controls.Add(this.panel54);
            this.panel554.Controls.Add(this.labelDateOfBirthResPage3);
            this.panel554.Controls.Add(this.labelDateOfBirthPage3);
            this.panel554.Controls.Add(this.panel36);
            this.panel554.Controls.Add(this.panel49);
            this.panel554.Controls.Add(this.labelPatLastNam3);
            this.panel554.Controls.Add(this.labelPatName3);
            this.panel554.Controls.Add(this.panel577);
            this.panel554.Controls.Add(this.panel579);
            this.panel554.Controls.Add(this.labelPatientIDResult3);
            this.panel554.Controls.Add(this.labelPatientID3);
            this.panel554.Controls.Add(this.labelPage3ReportNr);
            this.panel554.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel554.Location = new System.Drawing.Point(0, 0);
            this.panel554.Name = "panel554";
            this.panel554.Size = new System.Drawing.Size(1654, 93);
            this.panel554.TabIndex = 0;
            // 
            // labelReportText3
            // 
            this.labelReportText3.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelReportText3.Font = new System.Drawing.Font("Verdana", 16F);
            this.labelReportText3.Location = new System.Drawing.Point(1427, 0);
            this.labelReportText3.Name = "labelReportText3";
            this.labelReportText3.Size = new System.Drawing.Size(96, 93);
            this.labelReportText3.TabIndex = 43;
            this.labelReportText3.Text = "Report:";
            this.labelReportText3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel53
            // 
            this.panel53.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel53.Location = new System.Drawing.Point(1399, 0);
            this.panel53.Name = "panel53";
            this.panel53.Size = new System.Drawing.Size(17, 93);
            this.panel53.TabIndex = 52;
            // 
            // panel54
            // 
            this.panel54.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel54.Location = new System.Drawing.Point(1382, 0);
            this.panel54.Name = "panel54";
            this.panel54.Size = new System.Drawing.Size(17, 93);
            this.panel54.TabIndex = 51;
            // 
            // labelDateOfBirthResPage3
            // 
            this.labelDateOfBirthResPage3.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelDateOfBirthResPage3.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelDateOfBirthResPage3.Location = new System.Drawing.Point(1163, 0);
            this.labelDateOfBirthResPage3.Name = "labelDateOfBirthResPage3";
            this.labelDateOfBirthResPage3.Size = new System.Drawing.Size(219, 93);
            this.labelDateOfBirthResPage3.TabIndex = 50;
            this.labelDateOfBirthResPage3.Text = "06/08/1974";
            this.labelDateOfBirthResPage3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelDateOfBirthPage3
            // 
            this.labelDateOfBirthPage3.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelDateOfBirthPage3.Font = new System.Drawing.Font("Verdana", 16F);
            this.labelDateOfBirthPage3.Location = new System.Drawing.Point(1083, 0);
            this.labelDateOfBirthPage3.Name = "labelDateOfBirthPage3";
            this.labelDateOfBirthPage3.Size = new System.Drawing.Size(80, 93);
            this.labelDateOfBirthPage3.TabIndex = 49;
            this.labelDateOfBirthPage3.Text = "DOB:";
            this.labelDateOfBirthPage3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel36
            // 
            this.panel36.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel36.Location = new System.Drawing.Point(1066, 0);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(17, 93);
            this.panel36.TabIndex = 48;
            // 
            // panel49
            // 
            this.panel49.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel49.Location = new System.Drawing.Point(1049, 0);
            this.panel49.Name = "panel49";
            this.panel49.Size = new System.Drawing.Size(17, 93);
            this.panel49.TabIndex = 47;
            // 
            // labelPatLastNam3
            // 
            this.labelPatLastNam3.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPatLastNam3.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelPatLastNam3.Location = new System.Drawing.Point(515, 0);
            this.labelPatLastNam3.Name = "labelPatLastNam3";
            this.labelPatLastNam3.Size = new System.Drawing.Size(534, 93);
            this.labelPatLastNam3.TabIndex = 46;
            this.labelPatLastNam3.Text = "Brest van Kempen, hjgdfgghkgahghsaghfj, hjghdfasgdhsgh";
            this.labelPatLastNam3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelPatName3
            // 
            this.labelPatName3.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPatName3.Font = new System.Drawing.Font("Verdana", 16F);
            this.labelPatName3.Location = new System.Drawing.Point(409, 0);
            this.labelPatName3.Name = "labelPatName3";
            this.labelPatName3.Size = new System.Drawing.Size(106, 93);
            this.labelPatName3.TabIndex = 45;
            this.labelPatName3.Text = "Name:";
            this.labelPatName3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel577
            // 
            this.panel577.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel577.Location = new System.Drawing.Point(392, 0);
            this.panel577.Name = "panel577";
            this.panel577.Size = new System.Drawing.Size(17, 93);
            this.panel577.TabIndex = 44;
            // 
            // panel579
            // 
            this.panel579.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel579.Location = new System.Drawing.Point(375, 0);
            this.panel579.Name = "panel579";
            this.panel579.Size = new System.Drawing.Size(17, 93);
            this.panel579.TabIndex = 43;
            // 
            // labelPatientIDResult3
            // 
            this.labelPatientIDResult3.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPatientIDResult3.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelPatientIDResult3.Location = new System.Drawing.Point(146, 0);
            this.labelPatientIDResult3.Name = "labelPatientIDResult3";
            this.labelPatientIDResult3.Size = new System.Drawing.Size(229, 93);
            this.labelPatientIDResult3.TabIndex = 42;
            this.labelPatientIDResult3.Text = "12341234567";
            this.labelPatientIDResult3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelPatientID3
            // 
            this.labelPatientID3.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPatientID3.Font = new System.Drawing.Font("Verdana", 16F);
            this.labelPatientID3.Location = new System.Drawing.Point(0, 0);
            this.labelPatientID3.Name = "labelPatientID3";
            this.labelPatientID3.Size = new System.Drawing.Size(146, 93);
            this.labelPatientID3.TabIndex = 37;
            this.labelPatientID3.Text = "Patient ID:";
            this.labelPatientID3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPage3ReportNr
            // 
            this.labelPage3ReportNr.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelPage3ReportNr.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.labelPage3ReportNr.Location = new System.Drawing.Point(1523, 0);
            this.labelPage3ReportNr.Name = "labelPage3ReportNr";
            this.labelPage3ReportNr.Size = new System.Drawing.Size(131, 93);
            this.labelPage3ReportNr.TabIndex = 44;
            this.labelPage3ReportNr.Text = "12345";
            this.labelPage3ReportNr.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel91
            // 
            this.panel91.Controls.Add(this.label10);
            this.panel91.Controls.Add(this.labelStudyNumberResPage3);
            this.panel91.Controls.Add(this.label4);
            this.panel91.Controls.Add(this.labelPage3x);
            this.panel91.Controls.Add(this.label9);
            this.panel91.Controls.Add(this.labelPage3OfX);
            this.panel91.Controls.Add(this.labelPrintDate3Bottom);
            this.panel91.Controls.Add(this.label11);
            this.panel91.Controls.Add(this.label12);
            this.panel91.Controls.Add(this.label13);
            this.panel91.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel91.Location = new System.Drawing.Point(0, 2304);
            this.panel91.Name = "panel91";
            this.panel91.Size = new System.Drawing.Size(1654, 35);
            this.panel91.TabIndex = 37;
            // 
            // label10
            // 
            this.label10.Dock = System.Windows.Forms.DockStyle.Left;
            this.label10.Font = new System.Drawing.Font("Verdana", 16F);
            this.label10.Location = new System.Drawing.Point(731, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(440, 35);
            this.label10.TabIndex = 19;
            this.label10.Text = "2018 © Techmedic International B.V.";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelStudyNumberResPage3
            // 
            this.labelStudyNumberResPage3.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelStudyNumberResPage3.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.labelStudyNumberResPage3.Location = new System.Drawing.Point(1156, 0);
            this.labelStudyNumberResPage3.Name = "labelStudyNumberResPage3";
            this.labelStudyNumberResPage3.Size = new System.Drawing.Size(178, 35);
            this.labelStudyNumberResPage3.TabIndex = 42;
            this.labelStudyNumberResPage3.Text = "1212212";
            this.labelStudyNumberResPage3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Right;
            this.label4.Font = new System.Drawing.Font("Verdana", 16F);
            this.label4.Location = new System.Drawing.Point(1334, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 35);
            this.label4.TabIndex = 23;
            this.label4.Text = "Page";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelPage3x
            // 
            this.labelPage3x.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelPage3x.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.labelPage3x.Location = new System.Drawing.Point(1408, 0);
            this.labelPage3x.Name = "labelPage3x";
            this.labelPage3x.Size = new System.Drawing.Size(66, 35);
            this.labelPage3x.TabIndex = 22;
            this.labelPage3x.Text = "3";
            this.labelPage3x.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.Dock = System.Windows.Forms.DockStyle.Right;
            this.label9.Font = new System.Drawing.Font("Verdana", 16F);
            this.label9.Location = new System.Drawing.Point(1474, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 35);
            this.label9.TabIndex = 21;
            this.label9.Text = "of";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPage3OfX
            // 
            this.labelPage3OfX.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelPage3OfX.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.labelPage3OfX.Location = new System.Drawing.Point(1511, 0);
            this.labelPage3OfX.Name = "labelPage3OfX";
            this.labelPage3OfX.Size = new System.Drawing.Size(98, 35);
            this.labelPage3OfX.TabIndex = 20;
            this.labelPage3OfX.Text = "20p2";
            this.labelPage3OfX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPrintDate3Bottom
            // 
            this.labelPrintDate3Bottom.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelPrintDate3Bottom.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.labelPrintDate3Bottom.Location = new System.Drawing.Point(160, 0);
            this.labelPrintDate3Bottom.Name = "labelPrintDate3Bottom";
            this.labelPrintDate3Bottom.Size = new System.Drawing.Size(571, 35);
            this.labelPrintDate3Bottom.TabIndex = 18;
            this.labelPrintDate3Bottom.Text = "12/27/2016 22:22:22 AM+1200 v12345";
            this.labelPrintDate3Bottom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label11
            // 
            this.label11.Dock = System.Windows.Forms.DockStyle.Left;
            this.label11.Font = new System.Drawing.Font("Verdana", 16F);
            this.label11.Location = new System.Drawing.Point(45, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(115, 35);
            this.label11.TabIndex = 17;
            this.label11.Text = "Printed:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label12.Dock = System.Windows.Forms.DockStyle.Right;
            this.label12.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label12.Location = new System.Drawing.Point(1609, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(45, 35);
            this.label12.TabIndex = 1;
            this.label12.Text = "R3";
            this.label12.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label12.Visible = false;
            // 
            // label13
            // 
            this.label13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label13.Dock = System.Windows.Forms.DockStyle.Left;
            this.label13.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label13.Location = new System.Drawing.Point(0, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(45, 35);
            this.label13.TabIndex = 0;
            this.label13.Text = "L3";
            this.label13.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label13.Visible = false;
            // 
            // panel15
            // 
            this.panel15.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel15.Location = new System.Drawing.Point(0, 60);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(1996, 11);
            this.panel15.TabIndex = 5;
            // 
            // CPrintAnalysisForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(2013, 1681);
            this.Controls.Add(this.panel15);
            this.Controls.Add(this.panelPrintArea3);
            this.Controls.Add(this.panelPrintArea2);
            this.Controls.Add(this.panelPrintArea1);
            this.Controls.Add(this.panel21);
            this.Controls.Add(this.toolStrip1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "CPrintAnalysisForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Print Analysis";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CPrintAnalysisForm_FormClosing);
            this.Shown += new System.EventHandler(this.CPrintAnalysisForm_Shown);
            this.panelPrintArea1.ResumeLayout(false);
            this.panelPrintArea1.PerformLayout();
            this.panelEventSample2.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel114.ResumeLayout(false);
            this.panel115.ResumeLayout(false);
            this.panel115.PerformLayout();
            this.panel119.ResumeLayout(false);
            this.panel128.ResumeLayout(false);
            this.panel127.ResumeLayout(false);
            this.panel113.ResumeLayout(false);
            this.panel121.ResumeLayout(false);
            this.panel122.ResumeLayout(false);
            this.panel123.ResumeLayout(false);
            this.panel124.ResumeLayout(false);
            this.panel125.ResumeLayout(false);
            this.panel126.ResumeLayout(false);
            this.panelSample2Time.ResumeLayout(false);
            this.panelSeceondStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample2)).EndInit();
            this.panelSample2Header.ResumeLayout(false);
            this.panelSample2Header.PerformLayout();
            this.panelEventSample1.ResumeLayout(false);
            this.panel50.ResumeLayout(false);
            this.panel50.PerformLayout();
            this.panelActivities3.ResumeLayout(false);
            this.panel55.ResumeLayout(false);
            this.panel57.ResumeLayout(false);
            this.panel118.ResumeLayout(false);
            this.panel118.PerformLayout();
            this.panel64.ResumeLayout(false);
            this.panel68.ResumeLayout(false);
            this.panel69.ResumeLayout(false);
            this.panel70.ResumeLayout(false);
            this.panel72.ResumeLayout(false);
            this.panel73.ResumeLayout(false);
            this.panel75.ResumeLayout(false);
            this.panel149.ResumeLayout(false);
            this.panelFullStrip.ResumeLayout(false);
            this.panel44.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample1Full)).EndInit();
            this.panel146.ResumeLayout(false);
            this.panel234.ResumeLayout(false);
            this.panel232.ResumeLayout(false);
            this.panel233.ResumeLayout(false);
            this.panel230.ResumeLayout(false);
            this.panelSample1STrip.ResumeLayout(false);
            this.panel37.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample1)).EndInit();
            this.panel142.ResumeLayout(false);
            this.panel142.PerformLayout();
            this.panelBaseLineMFS.ResumeLayout(false);
            this.panelBaseLineMFS.PerformLayout();
            this.panelSymptoms3.ResumeLayout(false);
            this.panelfindings3.ResumeLayout(false);
            this.panel176.ResumeLayout(false);
            this.panel176.PerformLayout();
            this.panelQRSmeasurements3.ResumeLayout(false);
            this.panel47.ResumeLayout(false);
            this.panel43.ResumeLayout(false);
            this.panel42.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.panel38.ResumeLayout(false);
            this.panel39.ResumeLayout(false);
            this.panel40.ResumeLayout(false);
            this.panel41.ResumeLayout(false);
            this.panelBlSweepAmpIndicator.ResumeLayout(false);
            this.panelSweepSpeed.ResumeLayout(false);
            this.panelAmpPanel.ResumeLayout(false);
            this.panelECGBaselineSTrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBaseline)).EndInit();
            this.panelBaseLineHeader.ResumeLayout(false);
            this.panelEventStats.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel111.ResumeLayout(false);
            this.panel96.ResumeLayout(false);
            this.panel84.ResumeLayout(false);
            this.panel46.ResumeLayout(false);
            this.panel45.ResumeLayout(false);
            this.panel30.ResumeLayout(false);
            this.panel78.ResumeLayout(false);
            this.panel110.ResumeLayout(false);
            this.panel109.ResumeLayout(false);
            this.panel108.ResumeLayout(false);
            this.panel107.ResumeLayout(false);
            this.panel106.ResumeLayout(false);
            this.panel105.ResumeLayout(false);
            this.panel51.ResumeLayout(false);
            this.panel510.ResumeLayout(false);
            this.panel509.ResumeLayout(false);
            this.panel508.ResumeLayout(false);
            this.panel507.ResumeLayout(false);
            this.panelPhysInfo.ResumeLayout(false);
            this.panel67.ResumeLayout(false);
            this.panel258.ResumeLayout(false);
            this.panel256.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel252.ResumeLayout(false);
            this.panel250.ResumeLayout(false);
            this.panel244.ResumeLayout(false);
            this.panel242.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panelSecPatBar.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel34.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel27.ResumeLayout(false);
            this.panel26.ResumeLayout(false);
            this.panel33.ResumeLayout(false);
            this.panelDateTimeofEventStrip.ResumeLayout(false);
            this.panelCardiacEVentReportNumber.ResumeLayout(false);
            this.panelPrintHeader.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCenter)).EndInit();
            this.panel56.ResumeLayout(false);
            this.panel58.ResumeLayout(false);
            this.panel59.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panelPrintArea2.ResumeLayout(false);
            this.panelPrintArea2.PerformLayout();
            this.panelEventSample5.ResumeLayout(false);
            this.panel160.ResumeLayout(false);
            this.panel160.PerformLayout();
            this.panel163.ResumeLayout(false);
            this.panel164.ResumeLayout(false);
            this.panel164.PerformLayout();
            this.panel169.ResumeLayout(false);
            this.panel170.ResumeLayout(false);
            this.panel171.ResumeLayout(false);
            this.panel172.ResumeLayout(false);
            this.panel173.ResumeLayout(false);
            this.panel174.ResumeLayout(false);
            this.panel175.ResumeLayout(false);
            this.panel153.ResumeLayout(false);
            this.panel238.ResumeLayout(false);
            this.panel241.ResumeLayout(false);
            this.panel243.ResumeLayout(false);
            this.panel290.ResumeLayout(false);
            this.panel291.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample5Full)).EndInit();
            this.panel378.ResumeLayout(false);
            this.panel383.ResumeLayout(false);
            this.panel419.ResumeLayout(false);
            this.panel420.ResumeLayout(false);
            this.panel421.ResumeLayout(false);
            this.panel422.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample5)).EndInit();
            this.panel453.ResumeLayout(false);
            this.panelEventSample4.ResumeLayout(false);
            this.panel136.ResumeLayout(false);
            this.panel136.PerformLayout();
            this.panel139.ResumeLayout(false);
            this.panel140.ResumeLayout(false);
            this.panel140.PerformLayout();
            this.panel144.ResumeLayout(false);
            this.panel147.ResumeLayout(false);
            this.panel148.ResumeLayout(false);
            this.panel150.ResumeLayout(false);
            this.panel151.ResumeLayout(false);
            this.panel152.ResumeLayout(false);
            this.panel157.ResumeLayout(false);
            this.panel266.ResumeLayout(false);
            this.panel267.ResumeLayout(false);
            this.panel268.ResumeLayout(false);
            this.panel269.ResumeLayout(false);
            this.panel275.ResumeLayout(false);
            this.panel297.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample4Full)).EndInit();
            this.panel306.ResumeLayout(false);
            this.panel311.ResumeLayout(false);
            this.panel312.ResumeLayout(false);
            this.panel313.ResumeLayout(false);
            this.panel328.ResumeLayout(false);
            this.panel329.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample4)).EndInit();
            this.panel357.ResumeLayout(false);
            this.panelEventSample3.ResumeLayout(false);
            this.panel32.ResumeLayout(false);
            this.panel32.PerformLayout();
            this.panel103.ResumeLayout(false);
            this.panel104.ResumeLayout(false);
            this.panel104.PerformLayout();
            this.panel129.ResumeLayout(false);
            this.panel130.ResumeLayout(false);
            this.panel131.ResumeLayout(false);
            this.panel132.ResumeLayout(false);
            this.panel133.ResumeLayout(false);
            this.panel134.ResumeLayout(false);
            this.panel135.ResumeLayout(false);
            this.panel277.ResumeLayout(false);
            this.panel278.ResumeLayout(false);
            this.panel279.ResumeLayout(false);
            this.panel280.ResumeLayout(false);
            this.panel283.ResumeLayout(false);
            this.panel284.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample3Full)).EndInit();
            this.panel384.ResumeLayout(false);
            this.panel324.ResumeLayout(false);
            this.panel385.ResumeLayout(false);
            this.panel386.ResumeLayout(false);
            this.panel387.ResumeLayout(false);
            this.panel388.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample3)).EndInit();
            this.panel416.ResumeLayout(false);
            this.panelPage2Header.ResumeLayout(false);
            this.panel529.ResumeLayout(false);
            this.panel528.ResumeLayout(false);
            this.panel524.ResumeLayout(false);
            this.panel523.ResumeLayout(false);
            this.panel519.ResumeLayout(false);
            this.panel497.ResumeLayout(false);
            this.panel500.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCenter2)).EndInit();
            this.panel501.ResumeLayout(false);
            this.panel503.ResumeLayout(false);
            this.panel504.ResumeLayout(false);
            this.panelPage2Footer.ResumeLayout(false);
            this.panelPrintArea3.ResumeLayout(false);
            this.panelDisclosurePage.ResumeLayout(false);
            this.panelDisclosureStrips.ResumeLayout(false);
            this.panel71.ResumeLayout(false);
            this.panel456.ResumeLayout(false);
            this.panel179.ResumeLayout(false);
            this.panel180.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip20)).EndInit();
            this.panel181.ResumeLayout(false);
            this.panel93.ResumeLayout(false);
            this.panel94.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip19)).EndInit();
            this.panel154.ResumeLayout(false);
            this.panel204.ResumeLayout(false);
            this.panel210.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip18)).EndInit();
            this.panel155.ResumeLayout(false);
            this.panel361.ResumeLayout(false);
            this.panel404.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip17)).EndInit();
            this.panel192.ResumeLayout(false);
            this.panel352.ResumeLayout(false);
            this.panel353.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip16)).EndInit();
            this.panel212.ResumeLayout(false);
            this.panel343.ResumeLayout(false);
            this.panel347.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip15)).EndInit();
            this.panel227.ResumeLayout(false);
            this.panel340.ResumeLayout(false);
            this.panel341.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip14)).EndInit();
            this.panel320.ResumeLayout(false);
            this.panel402.ResumeLayout(false);
            this.panel406.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip13)).EndInit();
            this.panel338.ResumeLayout(false);
            this.panel362.ResumeLayout(false);
            this.panel363.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip12)).EndInit();
            this.panel344.ResumeLayout(false);
            this.panel399.ResumeLayout(false);
            this.panel400.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip11)).EndInit();
            this.panel349.ResumeLayout(false);
            this.panel446.ResumeLayout(false);
            this.panel606.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip10)).EndInit();
            this.panel360.ResumeLayout(false);
            this.panel443.ResumeLayout(false);
            this.panel444.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip9)).EndInit();
            this.panel397.ResumeLayout(false);
            this.panel439.ResumeLayout(false);
            this.panel441.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip8)).EndInit();
            this.panel403.ResumeLayout(false);
            this.panel433.ResumeLayout(false);
            this.panel437.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip7)).EndInit();
            this.panel408.ResumeLayout(false);
            this.panel431.ResumeLayout(false);
            this.panel432.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip6)).EndInit();
            this.panel413.ResumeLayout(false);
            this.panel602.ResumeLayout(false);
            this.panel603.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip5)).EndInit();
            this.panel434.ResumeLayout(false);
            this.panel598.ResumeLayout(false);
            this.panel600.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip4)).EndInit();
            this.panel440.ResumeLayout(false);
            this.panel596.ResumeLayout(false);
            this.panel597.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip3)).EndInit();
            this.panel445.ResumeLayout(false);
            this.panel494.ResumeLayout(false);
            this.panel498.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip2)).EndInit();
            this.panel502.ResumeLayout(false);
            this.panel19.ResumeLayout(false);
            this.panel92.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStrip1)).EndInit();
            this.panel511.ResumeLayout(false);
            this.panel512.ResumeLayout(false);
            this.panel514.ResumeLayout(false);
            this.panel535.ResumeLayout(false);
            this.panel554.ResumeLayout(false);
            this.panel91.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelPrintArea1;
        private System.Windows.Forms.Panel panelHorSepPatDet;
        private System.Windows.Forms.Panel panelDateTimeofEventStrip;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButtonPrint;
        private System.Windows.Forms.Panel panelPrintHeader;
        private System.Windows.Forms.Panel panel56;
        private System.Windows.Forms.Panel panel58;
        private System.Windows.Forms.Label labelCenter2;
        private System.Windows.Forms.Panel panel59;
        private System.Windows.Forms.Label labelCenter1;
        private System.Windows.Forms.Panel panel60;
        private System.Windows.Forms.PictureBox pictureBoxCenter;
        private System.Windows.Forms.Panel panel52;
        private System.Windows.Forms.Panel panelSecPatBar;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel78;
        private System.Windows.Forms.Panel panel77;
        private System.Windows.Forms.Panel panel110;
        private System.Windows.Forms.Label labelReportSignDate;
        private System.Windows.Forms.Panel panel109;
        private System.Windows.Forms.Label labelDateReportSign;
        private System.Windows.Forms.Panel panel108;
        private System.Windows.Forms.Label labelPhysSignLine;
        private System.Windows.Forms.Panel panel107;
        private System.Windows.Forms.Label labelPhysSign;
        private System.Windows.Forms.Panel panel106;
        private System.Windows.Forms.Label labelPhysPrintName;
        private System.Windows.Forms.Panel panel105;
        private System.Windows.Forms.Label labelPhysNameReportPrint;
        private System.Windows.Forms.Panel panelCardiacEVentReportNumber;
        private System.Windows.Forms.Panel panel51;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripButton toolStripClipboard1;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Panel panelPrintArea2;
        private System.Windows.Forms.Panel panel509;
        private System.Windows.Forms.Label labelPage1;
        private System.Windows.Forms.Panel panel508;
        private System.Windows.Forms.Panel panel507;
        private System.Windows.Forms.Panel panel510;
        private System.Windows.Forms.Label labelPage;
        private System.Windows.Forms.Label labelPageOf;
        private System.Windows.Forms.Label labelPage1OfX;
        private System.Windows.Forms.Panel panelPage2Footer;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ToolStripButton toolStripClipboard2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label labelPrintDate1Bottom;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ToolStripButton toolStripGenPages;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label labelPage2x;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label labelPage2OfX;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelPrintDate2Bottom;
        private System.Windows.Forms.Label labelPrintDate;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.Panel panelPrintArea3;
        private System.Windows.Forms.Panel panel91;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelPage3x;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelPage3OfX;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label labelPrintDate3Bottom;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panelDisclosurePage;
        private System.Windows.Forms.Panel panel469;
        private System.Windows.Forms.Panel panelDisclosureStrips;
        private System.Windows.Forms.Panel panel496;
        private System.Windows.Forms.Panel panel512;
        private System.Windows.Forms.Label labelStartPageDateTime;
        private System.Windows.Forms.Panel panel514;
        private System.Windows.Forms.Panel panel515;
        private System.Windows.Forms.Panel panel535;
        private System.Windows.Forms.Panel panel536;
        private System.Windows.Forms.Panel panel553;
        private System.Windows.Forms.Panel panel554;
        private System.Windows.Forms.Label labelPage3ReportNr;
        private System.Windows.Forms.Label labelStripLength;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel panel494;
        private System.Windows.Forms.Panel panel498;
        private System.Windows.Forms.PictureBox pictureBoxStrip2;
        private System.Windows.Forms.Panel panel502;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label labelStripTime2;
        private System.Windows.Forms.Label labelStripDate2;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label labelStripLead2;
        private System.Windows.Forms.Panel panel595;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Panel panel92;
        private System.Windows.Forms.PictureBox pictureBoxStrip1;
        private System.Windows.Forms.Panel panel511;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.Label labelStripTime1;
        private System.Windows.Forms.Label labelStripDate1;
        private System.Windows.Forms.Label label139;
        private System.Windows.Forms.Label labelStripLead1;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Panel panel456;
        private System.Windows.Forms.Panel panel204;
        private System.Windows.Forms.Panel panel210;
        private System.Windows.Forms.PictureBox pictureBoxStrip18;
        private System.Windows.Forms.Panel panel155;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.Label labelStripTime18;
        private System.Windows.Forms.Label labelStripDate18;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.Label labelStripLead18;
        private System.Windows.Forms.Panel panel211;
        private System.Windows.Forms.Panel panel361;
        private System.Windows.Forms.Panel panel404;
        private System.Windows.Forms.PictureBox pictureBoxStrip17;
        private System.Windows.Forms.Panel panel192;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.Label labelStripTime17;
        private System.Windows.Forms.Label labelStripDate17;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label labelStripLead17;
        private System.Windows.Forms.Panel panel410;
        private System.Windows.Forms.Panel panel352;
        private System.Windows.Forms.Panel panel353;
        private System.Windows.Forms.PictureBox pictureBoxStrip16;
        private System.Windows.Forms.Panel panel212;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label labelStripTime16;
        private System.Windows.Forms.Label labelStripDate16;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label labelStripLead16;
        private System.Windows.Forms.Panel panel354;
        private System.Windows.Forms.Panel panel343;
        private System.Windows.Forms.Panel panel347;
        private System.Windows.Forms.PictureBox pictureBoxStrip15;
        private System.Windows.Forms.Panel panel227;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label labelStripTime15;
        private System.Windows.Forms.Label labelStripDate15;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label labelStripLead15;
        private System.Windows.Forms.Panel panel348;
        private System.Windows.Forms.Panel panel340;
        private System.Windows.Forms.Panel panel341;
        private System.Windows.Forms.PictureBox pictureBoxStrip14;
        private System.Windows.Forms.Panel panel320;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label labelStripTime14;
        private System.Windows.Forms.Label labelStripDate14;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label labelStripLead14;
        private System.Windows.Forms.Panel panel346;
        private System.Windows.Forms.Panel panel402;
        private System.Windows.Forms.Panel panel406;
        private System.Windows.Forms.PictureBox pictureBoxStrip13;
        private System.Windows.Forms.Panel panel338;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label labelStripTime13;
        private System.Windows.Forms.Label labelStripDate13;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label labelStripLead13;
        private System.Windows.Forms.Panel panel407;
        private System.Windows.Forms.Panel panel362;
        private System.Windows.Forms.Panel panel363;
        private System.Windows.Forms.PictureBox pictureBoxStrip12;
        private System.Windows.Forms.Panel panel344;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label labelStripTime12;
        private System.Windows.Forms.Label labelStripDate12;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label labelStripLead12;
        private System.Windows.Forms.Panel panel396;
        private System.Windows.Forms.Panel panel399;
        private System.Windows.Forms.Panel panel400;
        private System.Windows.Forms.PictureBox pictureBoxStrip11;
        private System.Windows.Forms.Panel panel349;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label labelStripTime11;
        private System.Windows.Forms.Label labelStripDate11;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label labelStripLead11;
        private System.Windows.Forms.Panel panel405;
        private System.Windows.Forms.Panel panel446;
        private System.Windows.Forms.Panel panel606;
        private System.Windows.Forms.PictureBox pictureBoxStrip10;
        private System.Windows.Forms.Panel panel360;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label labelStripTime10;
        private System.Windows.Forms.Label labelStripDate10;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label labelStripLead10;
        private System.Windows.Forms.Panel panel607;
        private System.Windows.Forms.Panel panel443;
        private System.Windows.Forms.Panel panel444;
        private System.Windows.Forms.PictureBox pictureBoxStrip9;
        private System.Windows.Forms.Panel panel397;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label labelStripTime9;
        private System.Windows.Forms.Label labelStripDate9;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label labelStripLead9;
        private System.Windows.Forms.Panel panel605;
        private System.Windows.Forms.Panel panel439;
        private System.Windows.Forms.Panel panel441;
        private System.Windows.Forms.PictureBox pictureBoxStrip8;
        private System.Windows.Forms.Panel panel403;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label labelStripTime8;
        private System.Windows.Forms.Label labelStripDate8;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label labelStripLead8;
        private System.Windows.Forms.Panel panel442;
        private System.Windows.Forms.Panel panel433;
        private System.Windows.Forms.Panel panel437;
        private System.Windows.Forms.PictureBox pictureBoxStrip7;
        private System.Windows.Forms.Panel panel408;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label labelStripTime7;
        private System.Windows.Forms.Label labelStripDate7;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label labelStripLead7;
        private System.Windows.Forms.Panel panel438;
        private System.Windows.Forms.Panel panel431;
        private System.Windows.Forms.Panel panel432;
        private System.Windows.Forms.PictureBox pictureBoxStrip6;
        private System.Windows.Forms.Panel panel413;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label labelStripTime6;
        private System.Windows.Forms.Label labelStripDate6;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label labelStripLead6;
        private System.Windows.Forms.Panel panel436;
        private System.Windows.Forms.Panel panel602;
        private System.Windows.Forms.Panel panel603;
        private System.Windows.Forms.PictureBox pictureBoxStrip5;
        private System.Windows.Forms.Panel panel434;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label labelStripTime5;
        private System.Windows.Forms.Label labelStripDate5;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label labelStripLead5;
        private System.Windows.Forms.Panel panel604;
        private System.Windows.Forms.Panel panel598;
        private System.Windows.Forms.Panel panel600;
        private System.Windows.Forms.PictureBox pictureBoxStrip4;
        private System.Windows.Forms.Panel panel440;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label labelStripTime4;
        private System.Windows.Forms.Label labelStripDate4;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label labelStripLead4;
        private System.Windows.Forms.Panel panel601;
        private System.Windows.Forms.Panel panel596;
        private System.Windows.Forms.Panel panel597;
        private System.Windows.Forms.PictureBox pictureBoxStrip3;
        private System.Windows.Forms.Panel panel445;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label labelStripTime3;
        private System.Windows.Forms.Label labelStripDate3;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label labelStripLead3;
        private System.Windows.Forms.Panel panel599;
        private System.Windows.Forms.Panel panel179;
        private System.Windows.Forms.Panel panel180;
        private System.Windows.Forms.PictureBox pictureBoxStrip20;
        private System.Windows.Forms.Panel panel181;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label labelStripDate20;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label labelStripLead20;
        private System.Windows.Forms.Label labelStripTime20;
        private System.Windows.Forms.Panel panel198;
        private System.Windows.Forms.Panel panel93;
        private System.Windows.Forms.Panel panel94;
        private System.Windows.Forms.PictureBox pictureBoxStrip19;
        private System.Windows.Forms.Panel panel154;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label labelStripDate19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label labelStripLead19;
        private System.Windows.Forms.Label labelStripTime19;
        private System.Windows.Forms.Panel panel156;
        private System.Windows.Forms.Label labelStudyNr;
        private System.Windows.Forms.Label labelStudyNumberResPage2;
        private System.Windows.Forms.Label labelStudyNumberResPage3;
        private System.Windows.Forms.Panel panel34;
        private System.Windows.Forms.Label labelPhone2;
        private System.Windows.Forms.Label labelPhone1;
        private System.Windows.Forms.Label labelCountry;
        private System.Windows.Forms.Label labelZipCity;
        private System.Windows.Forms.Label labelAddress;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label labelAddressHeader;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Label labelAgeGender;
        private System.Windows.Forms.Label labelDateOfBirth;
        private System.Windows.Forms.Label labelPatientLastName;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label labelDOBheader;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label labelSerialNr;
        private System.Windows.Forms.Label labelStartDate;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label labelSerialNrHeader;
        private System.Windows.Forms.Label labelStartDateHeader;
        private System.Windows.Forms.Label labelPatID;
        private System.Windows.Forms.Label labelPatIDHeader;
        private System.Windows.Forms.Label labelReportNr;
        private System.Windows.Forms.Label labelCardiacEventReportNr;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panelEventSample4;
        private System.Windows.Forms.Panel panel266;
        private System.Windows.Forms.Label labelSample4StartFull;
        private System.Windows.Forms.Label labelSample4MidFull;
        private System.Windows.Forms.Panel panel267;
        private System.Windows.Forms.Label labelSample4SweepFull;
        private System.Windows.Forms.Panel panel268;
        private System.Windows.Forms.Label labelSample4AmplFull;
        private System.Windows.Forms.Panel panel269;
        private System.Windows.Forms.Label labelSample4EndFull;
        private System.Windows.Forms.Panel panel275;
        private System.Windows.Forms.Panel panel297;
        private System.Windows.Forms.Label labelSample4LeadFull;
        private System.Windows.Forms.PictureBox pictureBoxSample4Full;
        private System.Windows.Forms.Panel panel305;
        private System.Windows.Forms.Panel panel306;
        private System.Windows.Forms.Label labelSample4Start;
        private System.Windows.Forms.Label labelSample4Mid;
        private System.Windows.Forms.Panel panel311;
        private System.Windows.Forms.Label labelSample4Sweep;
        private System.Windows.Forms.Panel panel312;
        private System.Windows.Forms.Label labelSample4Ampl;
        private System.Windows.Forms.Panel panel313;
        private System.Windows.Forms.Label labelSample4End;
        private System.Windows.Forms.Panel panel328;
        private System.Windows.Forms.Panel panel329;
        private System.Windows.Forms.Label labelSample4Lead;
        private System.Windows.Forms.PictureBox pictureBoxSample4;
        private System.Windows.Forms.Panel panel357;
        private System.Windows.Forms.Label labelSample4Date;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.Label labelSample4Time;
        private System.Windows.Forms.Label labelSample4Nr;
        private System.Windows.Forms.Label labelSample4EventNr;
        private System.Windows.Forms.Label labelEvent4;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panelEventSample3;
        private System.Windows.Forms.Panel panel277;
        private System.Windows.Forms.Label labelSample3StartFull;
        private System.Windows.Forms.Label labelSample3MidFull;
        private System.Windows.Forms.Panel panel278;
        private System.Windows.Forms.Label labelSample2Sweep;
        private System.Windows.Forms.Panel panel279;
        private System.Windows.Forms.Label labelSample2Ampl;
        private System.Windows.Forms.Panel panel280;
        private System.Windows.Forms.Label labelSample3EndFull;
        private System.Windows.Forms.Panel panel283;
        private System.Windows.Forms.Panel panel284;
        private System.Windows.Forms.Label labelSample3LeadFull;
        private System.Windows.Forms.PictureBox pictureBoxSample3Full;
        private System.Windows.Forms.Panel panel300;
        private System.Windows.Forms.Panel panel384;
        private System.Windows.Forms.Label labelSample3Start;
        private System.Windows.Forms.Label labelSample3Mid;
        private System.Windows.Forms.Panel panel324;
        private System.Windows.Forms.Label labelSample3Sweep;
        private System.Windows.Forms.Panel panel385;
        private System.Windows.Forms.Label labelSample3Ampl;
        private System.Windows.Forms.Panel panel386;
        private System.Windows.Forms.Label labelSample3End;
        private System.Windows.Forms.Panel panel387;
        private System.Windows.Forms.Panel panel388;
        private System.Windows.Forms.Label labelSample3Lead;
        private System.Windows.Forms.PictureBox pictureBoxSample3;
        private System.Windows.Forms.Panel panel416;
        private System.Windows.Forms.Label labelSample3Date;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label labelSample3Time;
        private System.Windows.Forms.Label labelSample3Nr;
        private System.Windows.Forms.Label labelSample3EventNr;
        private System.Windows.Forms.Label labelEvent3;
        private System.Windows.Forms.Panel panel450;
        private System.Windows.Forms.Panel panel488;
        private System.Windows.Forms.Panel panelPage2Header;
        private System.Windows.Forms.Label labelPage2ReportNr;
        private System.Windows.Forms.Label labelReportText2;
        private System.Windows.Forms.Panel panel500;
        private System.Windows.Forms.Panel panel501;
        private System.Windows.Forms.Panel panel503;
        private System.Windows.Forms.Label labelCenter2p2;
        private System.Windows.Forms.Panel panel504;
        private System.Windows.Forms.Label labelCenter1p2;
        private System.Windows.Forms.Panel panel505;
        private System.Windows.Forms.Panel panelEventSample5;
        private System.Windows.Forms.Panel panel153;
        private System.Windows.Forms.Label labelSample5StartFull;
        private System.Windows.Forms.Label labelSample5MidFull;
        private System.Windows.Forms.Panel panel238;
        private System.Windows.Forms.Label labelSample5SweepFull;
        private System.Windows.Forms.Panel panel241;
        private System.Windows.Forms.Label labelSample5AmplFull;
        private System.Windows.Forms.Panel panel243;
        private System.Windows.Forms.Label labelSample5EndFull;
        private System.Windows.Forms.Panel panel290;
        private System.Windows.Forms.Panel panel291;
        private System.Windows.Forms.Label labelSample5LeadFull;
        private System.Windows.Forms.PictureBox pictureBoxSample5Full;
        private System.Windows.Forms.Panel panel298;
        private System.Windows.Forms.Panel panel378;
        private System.Windows.Forms.Label labelSample5Start;
        private System.Windows.Forms.Label labelSample5Mid;
        private System.Windows.Forms.Panel panel383;
        private System.Windows.Forms.Label labelSample5Sweep;
        private System.Windows.Forms.Panel panel419;
        private System.Windows.Forms.Label labelSample5Ampl;
        private System.Windows.Forms.Panel panel420;
        private System.Windows.Forms.Label labelSample5End;
        private System.Windows.Forms.Panel panel421;
        private System.Windows.Forms.Panel panel422;
        private System.Windows.Forms.Label labelSample5Lead;
        private System.Windows.Forms.PictureBox pictureBoxSample5;
        private System.Windows.Forms.Panel panel453;
        private System.Windows.Forms.Label labelSample5Nr;
        private System.Windows.Forms.Label labelSample5Date;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.Label labelSample5Time;
        private System.Windows.Forms.Label labelSample5EventNr;
        private System.Windows.Forms.Label labelEvent5;
        private System.Windows.Forms.Panel panel468;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label labelEventDataTime;
        private System.Windows.Forms.Label label141;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Panel panelPhysInfo;
        private System.Windows.Forms.Panel panel67;
        private System.Windows.Forms.Panel panel258;
        private System.Windows.Forms.Label labelClientTel;
        private System.Windows.Forms.Label labelClientName;
        private System.Windows.Forms.Panel panel256;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label labelClientHeader;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panelVert9;
        private System.Windows.Forms.Panel panel252;
        private System.Windows.Forms.Label labelRefPhysicianTel;
        private System.Windows.Forms.Label labelRefPhysicianName;
        private System.Windows.Forms.Panel panel250;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label labelRefPhysHeader;
        private System.Windows.Forms.Panel panelVert8;
        private System.Windows.Forms.Panel panel244;
        private System.Windows.Forms.Label labelPhysicianTel;
        private System.Windows.Forms.Label labelPhysicianName;
        private System.Windows.Forms.Panel panel242;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label labelPhysHeader;
        private System.Windows.Forms.Panel panelVert6;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Label labelTechnicianMeasurement;
        private System.Windows.Forms.Label labelTechText;
        private System.Windows.Forms.Panel panelEventStats;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Label labelDiagnosisHeader;
        private System.Windows.Forms.Panel panelBlSweepAmpIndicator;
        private System.Windows.Forms.Label labelSampleBlMid;
        private System.Windows.Forms.Label labelSampleBlStart;
        private System.Windows.Forms.Panel panelSweepSpeed;
        private System.Windows.Forms.Label labelSampleBlSweep;
        private System.Windows.Forms.Panel panelAmpPanel;
        private System.Windows.Forms.Label labelSampleBlAmpl;
        private System.Windows.Forms.Label labelSampleBlEnd;
        private System.Windows.Forms.Panel panelECGBaselineSTrip;
        private System.Windows.Forms.Label labelLeadID;
        private System.Windows.Forms.PictureBox pictureBoxBaseline;
        private System.Windows.Forms.Panel panelBaseLineHeader;
        private System.Windows.Forms.Label labelBaselineDate;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.Label labelBaselineTime;
        private System.Windows.Forms.Label labelBaseRefHeader;
        private System.Windows.Forms.Panel panel35;
        private System.Windows.Forms.Panel panelBaseLineMFS;
        private System.Windows.Forms.Panel panelSymptoms3;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Panel panelfindings3;
        private System.Windows.Forms.Panel panel270;
        private System.Windows.Forms.Panel panel273;
        private System.Windows.Forms.Panel panelQRSmeasurements3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label unitPrBL;
        private System.Windows.Forms.Label unitMaxHrBL;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Panel panel38;
        private System.Windows.Forms.Label headerPrBL;
        private System.Windows.Forms.Label headerMaxHrBL;
        private System.Windows.Forms.Panel panel39;
        private System.Windows.Forms.Label unitMinHrBL;
        private System.Windows.Forms.Panel panel40;
        private System.Windows.Forms.Panel panel41;
        private System.Windows.Forms.Label headerMeanHrBL;
        private System.Windows.Forms.Label headerMinHrBL;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Panel panel43;
        private System.Windows.Forms.Panel panel42;
        private System.Windows.Forms.Label headerQtBL;
        private System.Windows.Forms.Label headerQrsBL;
        private System.Windows.Forms.Label valueMinHrBL;
        private System.Windows.Forms.Panel panel47;
        private System.Windows.Forms.Label unitQtBL;
        private System.Windows.Forms.Label unitQrsBL;
        private System.Windows.Forms.Label valueQtBL;
        private System.Windows.Forms.Label valueQrsBL;
        private System.Windows.Forms.Label valuePrBL;
        private System.Windows.Forms.Label valueMaxHrBL;
        private System.Windows.Forms.Label unitMeanHrBL;
        private System.Windows.Forms.Label valueMeanHrBL;
        private System.Windows.Forms.Panel panel48;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel111;
        private System.Windows.Forms.Label labelQTmeasureSI;
        private System.Windows.Forms.Label labelQRSmeasureSI;
        private System.Windows.Forms.Label labelPRmeasureSI;
        private System.Windows.Forms.Label labelRatemeasureSI;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Panel panel96;
        private System.Windows.Forms.Label labelQTAvg;
        private System.Windows.Forms.Label labelQRSAvg;
        private System.Windows.Forms.Label labelPRAvg;
        private System.Windows.Forms.Label labelRateAVG;
        private System.Windows.Forms.Label labelAVGMeasurement;
        private System.Windows.Forms.Panel panel84;
        private System.Windows.Forms.Label labelQTMax;
        private System.Windows.Forms.Label labelQRSMax;
        private System.Windows.Forms.Label labelPRMax;
        private System.Windows.Forms.Label labelRateMax;
        private System.Windows.Forms.Label labelMaxMeasurement;
        private System.Windows.Forms.Panel panel46;
        private System.Windows.Forms.Label labelQTmin;
        private System.Windows.Forms.Label labelQRSmin;
        private System.Windows.Forms.Label labelPRmin;
        private System.Windows.Forms.Label labelRateMin;
        private System.Windows.Forms.Label labelMinMeasurement;
        private System.Windows.Forms.Label labelSymptomsBL;
        private System.Windows.Forms.Label labelFindingsBL;
        private System.Windows.Forms.Panel panel176;
        private System.Windows.Forms.Label labelRhythmsBL;
        private System.Windows.Forms.Label labelFindingsTextBL;
        private System.Windows.Forms.Panel panel45;
        private System.Windows.Forms.Label labelQTMeasure;
        private System.Windows.Forms.Label labelQRSMeasure;
        private System.Windows.Forms.Label labelPRMeasure;
        private System.Windows.Forms.Label labelRateMeasure;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label labelICDcode;
        private System.Windows.Forms.Panel panel160;
        private System.Windows.Forms.Panel panel163;
        private System.Windows.Forms.Label labelFindings5;
        private System.Windows.Forms.Panel panel164;
        private System.Windows.Forms.Label labelRhythms5;
        private System.Windows.Forms.Label labelFindingsText5;
        private System.Windows.Forms.Panel panel166;
        private System.Windows.Forms.Panel panel167;
        private System.Windows.Forms.Panel panel169;
        private System.Windows.Forms.Panel panel170;
        private System.Windows.Forms.Label unitQt5;
        private System.Windows.Forms.Label unitQrs5;
        private System.Windows.Forms.Label unitPr5;
        private System.Windows.Forms.Panel panel171;
        private System.Windows.Forms.Label valueQt5;
        private System.Windows.Forms.Label valueQrs5;
        private System.Windows.Forms.Label valuePr5;
        private System.Windows.Forms.Panel panel172;
        private System.Windows.Forms.Label headerQt5;
        private System.Windows.Forms.Label headerQrs5;
        private System.Windows.Forms.Label headerPr5;
        private System.Windows.Forms.Panel panel173;
        private System.Windows.Forms.Label unitMaxHr5;
        private System.Windows.Forms.Label unitMeanHr5;
        private System.Windows.Forms.Label unitMinHr5;
        private System.Windows.Forms.Panel panel174;
        private System.Windows.Forms.Label valueMaxHr5;
        private System.Windows.Forms.Label valueMeanHr5;
        private System.Windows.Forms.Label valueMinHr5;
        private System.Windows.Forms.Panel panel175;
        private System.Windows.Forms.Label headerMaxHr5;
        private System.Windows.Forms.Label headerMeanHr5;
        private System.Windows.Forms.Label headerMinHr5;
        private System.Windows.Forms.Label label213;
        private System.Windows.Forms.Panel panel136;
        private System.Windows.Forms.Panel panel139;
        private System.Windows.Forms.Label labelFindings4;
        private System.Windows.Forms.Panel panel140;
        private System.Windows.Forms.Label labelRhythms4;
        private System.Windows.Forms.Label labelFindingsText4;
        private System.Windows.Forms.Panel panel141;
        private System.Windows.Forms.Panel panel143;
        private System.Windows.Forms.Panel panel144;
        private System.Windows.Forms.Panel panel147;
        private System.Windows.Forms.Label unitQt4;
        private System.Windows.Forms.Label unitQrs4;
        private System.Windows.Forms.Label unitPr4;
        private System.Windows.Forms.Panel panel148;
        private System.Windows.Forms.Label valueQt4;
        private System.Windows.Forms.Label valueQrs4;
        private System.Windows.Forms.Label valuePr4;
        private System.Windows.Forms.Panel panel150;
        private System.Windows.Forms.Label headerQt4;
        private System.Windows.Forms.Label headerQrs4;
        private System.Windows.Forms.Label headerPr4;
        private System.Windows.Forms.Panel panel151;
        private System.Windows.Forms.Label unitMaxHr4;
        private System.Windows.Forms.Label unitMeanHr4;
        private System.Windows.Forms.Label unitMinHr4;
        private System.Windows.Forms.Panel panel152;
        private System.Windows.Forms.Label valueMaxHr4;
        private System.Windows.Forms.Label valueMeanHr4;
        private System.Windows.Forms.Label valueMinHr4;
        private System.Windows.Forms.Panel panel157;
        private System.Windows.Forms.Label headerMaxHr4;
        private System.Windows.Forms.Label headerMeanHr4;
        private System.Windows.Forms.Label headerMinHr4;
        private System.Windows.Forms.Label label187;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.Panel panel103;
        private System.Windows.Forms.Label labelFindings3;
        private System.Windows.Forms.Panel panel104;
        private System.Windows.Forms.Label labelRhythms3;
        private System.Windows.Forms.Label labelFindingsText3;
        private System.Windows.Forms.Panel panel112;
        private System.Windows.Forms.Panel panel120;
        private System.Windows.Forms.Panel panel129;
        private System.Windows.Forms.Panel panel130;
        private System.Windows.Forms.Label unitQt3;
        private System.Windows.Forms.Label unitQrs3;
        private System.Windows.Forms.Label unitPr3;
        private System.Windows.Forms.Panel panel131;
        private System.Windows.Forms.Label valueQt3;
        private System.Windows.Forms.Label valueQrs3;
        private System.Windows.Forms.Label valuePr3;
        private System.Windows.Forms.Panel panel132;
        private System.Windows.Forms.Label headerQt3;
        private System.Windows.Forms.Label headerQrs3;
        private System.Windows.Forms.Label headerPr3;
        private System.Windows.Forms.Panel panel133;
        private System.Windows.Forms.Label unitMaxHr3;
        private System.Windows.Forms.Label unitMeanHr3;
        private System.Windows.Forms.Label unitMinHr3;
        private System.Windows.Forms.Panel panel134;
        private System.Windows.Forms.Label valueMaxHr3;
        private System.Windows.Forms.Label valueMeanHr3;
        private System.Windows.Forms.Label valueMinHr3;
        private System.Windows.Forms.Panel panel135;
        private System.Windows.Forms.Label headerMaxHr3;
        private System.Windows.Forms.Label headerMeanHr3;
        private System.Windows.Forms.Label headerMinHr3;
        private System.Windows.Forms.Label label161;
        private System.Windows.Forms.Label labelClass5;
        private System.Windows.Forms.Label labelClass4;
        private System.Windows.Forms.Label labelClass3;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Panel panelEventSample1;
        private System.Windows.Forms.Panel panelEventSample2;
        private System.Windows.Forms.Panel panel97;
        private System.Windows.Forms.Panel panel50;
        private System.Windows.Forms.Panel panelActivities3;
        private System.Windows.Forms.Label labelActivity1;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Panel panel55;
        private System.Windows.Forms.Label labelSymptoms1;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Panel panel57;
        private System.Windows.Forms.Label labelFindings1;
        private System.Windows.Forms.Panel panel118;
        private System.Windows.Forms.Label labelRhythms1;
        private System.Windows.Forms.Label labelFindingsText1;
        private System.Windows.Forms.Panel panel62;
        private System.Windows.Forms.Panel panel63;
        private System.Windows.Forms.Panel panel64;
        private System.Windows.Forms.Panel panel68;
        private System.Windows.Forms.Label unitQt1;
        private System.Windows.Forms.Label unitQrs1;
        private System.Windows.Forms.Label unitPr1;
        private System.Windows.Forms.Panel panel69;
        private System.Windows.Forms.Label valueQt1;
        private System.Windows.Forms.Label valueQrs1;
        private System.Windows.Forms.Label valuePr1;
        private System.Windows.Forms.Panel panel70;
        private System.Windows.Forms.Label headerQt1;
        private System.Windows.Forms.Label headerQrs1;
        private System.Windows.Forms.Label headerPr1;
        private System.Windows.Forms.Panel panel72;
        private System.Windows.Forms.Label unitMaxHr1;
        private System.Windows.Forms.Label unitMeanHr1;
        private System.Windows.Forms.Label unitMinHr1;
        private System.Windows.Forms.Panel panel73;
        private System.Windows.Forms.Label valueMaxHr1;
        private System.Windows.Forms.Label valueMeanHr1;
        private System.Windows.Forms.Label valueMinHr1;
        private System.Windows.Forms.Panel panel75;
        private System.Windows.Forms.Label headerMaxHr1;
        private System.Windows.Forms.Label headerMeanHr1;
        private System.Windows.Forms.Label headerMinHr1;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Panel panel149;
        private System.Windows.Forms.Label labelSample1SweepFull;
        private System.Windows.Forms.Label labelSample1MidFull;
        private System.Windows.Forms.Panel panelFullStrip;
        private System.Windows.Forms.Panel panel44;
        private System.Windows.Forms.Label labelLeadFullStripSample1;
        private System.Windows.Forms.PictureBox pictureBoxSample1Full;
        private System.Windows.Forms.Panel panel146;
        private System.Windows.Forms.Panel panel232;
        private System.Windows.Forms.Label labelSample1Sweep;
        private System.Windows.Forms.Panel panel234;
        private System.Windows.Forms.Label labelSample1Ampl;
        private System.Windows.Forms.Panel panel233;
        private System.Windows.Forms.Label labelSample1End;
        private System.Windows.Forms.Panel panel230;
        private System.Windows.Forms.Label labelSample1Mid;
        private System.Windows.Forms.Panel panelSample1STrip;
        private System.Windows.Forms.Panel panel37;
        private System.Windows.Forms.Label labelLeadZoomStrip1;
        private System.Windows.Forms.PictureBox pictureBoxSample1;
        private System.Windows.Forms.Panel panel142;
        private System.Windows.Forms.Label labelSample1Date;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label labelSample1Time;
        private System.Windows.Forms.Panel panel222;
        private System.Windows.Forms.Panel panel219;
        private System.Windows.Forms.Label labelSample1EventNr;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelClass1;
        private System.Windows.Forms.Label labelSample1Nr;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel114;
        private System.Windows.Forms.Label labelFindings2;
        private System.Windows.Forms.Panel panel115;
        private System.Windows.Forms.Label labelRhythms2;
        private System.Windows.Forms.Label labelFindingsText2;
        private System.Windows.Forms.Panel panel116;
        private System.Windows.Forms.Panel panel117;
        private System.Windows.Forms.Panel panel119;
        private System.Windows.Forms.Panel panel128;
        private System.Windows.Forms.Label unitQt2;
        private System.Windows.Forms.Label unitQrs2;
        private System.Windows.Forms.Panel panel127;
        private System.Windows.Forms.Label valueQt2;
        private System.Windows.Forms.Label valueQrs2;
        private System.Windows.Forms.Panel panel113;
        private System.Windows.Forms.Label headerQt2;
        private System.Windows.Forms.Label headerQrs2;
        private System.Windows.Forms.Panel panel121;
        private System.Windows.Forms.Label unitPr2;
        private System.Windows.Forms.Label unitMaxHr2;
        private System.Windows.Forms.Panel panel122;
        private System.Windows.Forms.Label valuePr2;
        private System.Windows.Forms.Label valueMaxHr2;
        private System.Windows.Forms.Panel panel123;
        private System.Windows.Forms.Label headerPr2;
        private System.Windows.Forms.Label headerMaxHr2;
        private System.Windows.Forms.Panel panel124;
        private System.Windows.Forms.Label unitMeanHr2;
        private System.Windows.Forms.Label unitMinHr2;
        private System.Windows.Forms.Panel panel125;
        private System.Windows.Forms.Label valueMeanHr2;
        private System.Windows.Forms.Label valueMinHr2;
        private System.Windows.Forms.Panel panel126;
        private System.Windows.Forms.Label headerMeanHr2;
        private System.Windows.Forms.Label headerMinHr2;
        private System.Windows.Forms.Label label146;
        private System.Windows.Forms.Label labelSample2Mid;
        private System.Windows.Forms.Label labelZoomSample2;
        private System.Windows.Forms.Label labelSample2End;
        private System.Windows.Forms.Label labelSample2Start;
        private System.Windows.Forms.Panel panelSeceondStrip;
        private System.Windows.Forms.Label labelLeadSample2;
        private System.Windows.Forms.PictureBox pictureBoxSample2;
        private System.Windows.Forms.Panel panelSample2Header;
        private System.Windows.Forms.Label labelSample2EventNr;
        private System.Windows.Forms.Label labelEvenNrSample2;
        private System.Windows.Forms.Label labelClass2;
        private System.Windows.Forms.Label labelSample2Date;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label labelSample2Time;
        private System.Windows.Forms.Panel panel89;
        private System.Windows.Forms.Panel panel86;
        private System.Windows.Forms.Label labelSample2Nr;
        private System.Windows.Forms.Label labelEventNrBL;
        private System.Windows.Forms.ToolStripLabel toolStripMemUsage;
        private System.Windows.Forms.Label labelActivationType;
        private System.Windows.Forms.Label labelSingleEventReportTime;
        private System.Windows.Forms.Label labelSingleEventReportDate;
        private System.Windows.Forms.Label labelSample1Start;
        private System.Windows.Forms.Label labelSample1AmplFull;
        private System.Windows.Forms.Label labelSample1StartFull;
        private System.Windows.Forms.Label labelSample1EndFull;
        private System.Windows.Forms.Panel panelSample2Time;
        private System.Windows.Forms.Label labelSweepSample2;
        private System.Windows.Forms.Label labelRoom;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label labelPatientFirstName;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.PictureBox pictureBoxCenter2;
        private System.Windows.Forms.Panel panel533;
        private System.Windows.Forms.Panel panel531;
        private System.Windows.Forms.Panel panel529;
        private System.Windows.Forms.Label labelDateOfBirthResPage2;
        private System.Windows.Forms.Panel panel528;
        private System.Windows.Forms.Label labelDateOfBirthPage2;
        private System.Windows.Forms.Panel panel527;
        private System.Windows.Forms.Panel panel526;
        private System.Windows.Forms.Panel panel525;
        private System.Windows.Forms.Panel panel524;
        private System.Windows.Forms.Label labelPatLastName;
        private System.Windows.Forms.Panel panel523;
        private System.Windows.Forms.Label labelPatName;
        private System.Windows.Forms.Panel panel522;
        private System.Windows.Forms.Panel panel521;
        private System.Windows.Forms.Panel panel520;
        private System.Windows.Forms.Panel panel519;
        private System.Windows.Forms.Label labelPatientIDResult;
        private System.Windows.Forms.Panel panel497;
        private System.Windows.Forms.Label labelPatientID;
        private System.Windows.Forms.Label labelReportText3;
        private System.Windows.Forms.Label labelPatLastNam3;
        private System.Windows.Forms.Label labelPatName3;
        private System.Windows.Forms.Panel panel577;
        private System.Windows.Forms.Panel panel579;
        private System.Windows.Forms.Label labelPatientIDResult3;
        private System.Windows.Forms.Label labelPatientID3;
        private System.Windows.Forms.Label labelUnitAmp;
        private System.Windows.Forms.Label labelEndPageTime;
        private System.Windows.Forms.Panel panel53;
        private System.Windows.Forms.Panel panel54;
        private System.Windows.Forms.Label labelDateOfBirthResPage3;
        private System.Windows.Forms.Label labelDateOfBirthPage3;
        private System.Windows.Forms.Panel panel36;
        private System.Windows.Forms.Panel panel49;
        private System.Windows.Forms.Label labelUnitFeed;
        private System.Windows.Forms.Label labelQC;
        private System.Windows.Forms.Panel panel71;
        private System.Windows.Forms.Label labelSFdStripInfo;
        private System.Windows.Forms.Panel panel66;
    }
}