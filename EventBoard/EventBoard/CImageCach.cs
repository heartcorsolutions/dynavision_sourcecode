﻿using Program_Base;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace EventBoard
{
    public class CImageCachNode
    {
        public UInt32 mIndex = 0;
        public string mFilePath;
        public string mFileName;
        public UInt16 mRetryCount = 0;
        public DateTime mLastUsed;
        public DateTime mLastTryLoad;
        public Int32 mDataGridRow;

        public Image mImage = null;    // keep image private. Use mGetImage() or mbIsImageEmpty() 

        CLoadOneIamgeThread mLoadClass = null;

        Thread mLoadThread = null;

        public CImageCachNode( UInt32 AIndex, string AFilePath, string AFileName, Int32 ADataGridRow)
        {
            mIndex = AIndex;
            mFilePath = AFilePath;
            mFileName = AFileName;
            mRetryCount = 0;
            mImage = null;
            mLastUsed = DateTime.Now;
            mDataGridRow = ADataGridRow;
        }
/*
        public bool mbIsImageEmpty()
        {
            return mImage == null;
        }

        public Image mGetImage()
        {
            return mImage != null ? mImage : CImageCach.sGetEmptyImage();
        }

        public void mSetImage( Image AImage )
        {
            mImage = AImage;
        }
*/        
        public bool mbStartThread( UInt16 ASimLoadDelayTck, DateTime ANow, CImageCach AImageCach, UInt16 AMaxRetry )
        {
            bool bOk = false;
            if (mLoadClass == null)
            {
                mLoadClass = new CLoadOneIamgeThread(this, ASimLoadDelayTck, AImageCach, AMaxRetry);
            }
            if (mLoadClass != null)
            {
                if (mLoadThread == null)
                {
                    mLoadThread = new Thread(new ThreadStart(mLoadClass.mRun));
                }
                if (mLoadThread != null)
                {
                    ThreadState state = mLoadThread.ThreadState;
                    mLastTryLoad = ANow;
                    try
                    {
                        mLoadThread.Start();
                        bOk = true;

                    }
                    catch(Exception ex)
                    {
                        bOk = false;
                    }
                }
            }
            return bOk;
        }

        public bool mbCleanTread()
        {
            bool bOk = true;

            try
            {
                if (mLoadThread != null)
                {
                    if (mLoadThread.ThreadState == ThreadState.Running)
                    {
                        bOk = false;
                        mLoadThread.Abort();
                    }
                    mLoadThread = null;
                }
                if (mLoadClass != null)
                {
                    mLoadClass = null;
                }
            }
            catch (Exception ex)
            {
                // disregard abort error
            }
            return bOk;
        }

        public bool mbCleanFinishedTread(DateTime ANowDT, UInt32 AMaxNrTry )
        {
            bool bOk = false;

            try
            {
                if (mLoadThread != null && ( mImage != null || mRetryCount >= AMaxNrTry))
                {
                    if (mLoadThread.ThreadState == ThreadState.Stopped)
                    {
                        if( (ANowDT - mLastTryLoad).TotalSeconds > 5 )
                        {
                            mLoadThread.Abort();
                            mLoadThread = null;

                            if (mLoadClass != null)
                            {
                                mLoadClass = null;
                            }
                            bOk = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // disregard abort error
            }
            return bOk;
        }



        public bool mbIsThreadRunning()
        {
            return mLoadThread != null && mLoadThread.IsAlive;
        }
    }

    public class CLoadOneIamgeThread
    {
        private CImageCachNode mImageCachNode = null;
        private UInt16 mSimLoadDelayTck;
        private CImageCach mImageCash;
        private UInt16 mMaxNrRetry = 6;

        public CLoadOneIamgeThread( CImageCachNode ANode, UInt16 ASimLoadDelayTck, CImageCach AImageCash, UInt16 AMaxNrRetry)
        {
            mImageCachNode = ANode;
            mSimLoadDelayTck = ASimLoadDelayTck;
            mImageCash = AImageCash;
            mMaxNrRetry = AMaxNrRetry;
        }
        public string mPathCombine(string APath, string AFileName)
        {
            string s = APath;
            int n = APath == null ? 0 : s.Length;
            if (n > 0 && s[n - 1] != '\\')
            {
                s += "\\";
            }
            return s + AFileName;
        }

        public void mRun()
        {
            if(mImageCachNode != null && mImageCachNode.mImage == null )
            {
                Image img = null;
                try
                {
                    if (++mImageCachNode.mRetryCount > 1)
                    {
                        int i = mImageCachNode.mRetryCount; // retry

                    }

                    if (mImageCachNode.mRetryCount >= 2/* max loop is 2 ???? mMaxNrRetry*/ && mMaxNrRetry > 1 )
                    {
                        img = CImageCach.sGetNoDataImage(); // do not retry again
                    }
                    else
                    {
                        img = Image.FromFile(mPathCombine/*Path.Combine*/(mImageCachNode.mFilePath, mImageCachNode.mFileName));
                        if (img != null)
                        {
                            int tn = this.GetHashCode();

        //                    CProgram.sLogLine("ImageCach thread(" + tn.ToString("X") + ") loaded "
        //                        + mImageCachNode.mIndex.ToString() + "= " + mImageCachNode.mFileName + ", try= " + mImageCachNode.mRetryCount.ToString());
                        }
                        if (mSimLoadDelayTck > 0)
                        {
                            Thread.Sleep(mSimLoadDelayTck);
                        }
                    }
                }
                catch (Exception /*ex*/)
                {
                    CProgram.sLogLine("Thread failed to try loading " + 
                            mImageCachNode.mIndex.ToString() + "=" +  mImageCachNode.mFileName + ", try= " + mImageCachNode.mRetryCount.ToString());
                    img = null;
                }
                if( img != null )
                {
                    mImageCachNode.mImage = img;
                    mImageCachNode.mLastUsed = DateTime.Now;

                    if( mImageCash != null )
                    {
                        mImageCash.mUpdateGridImage(mImageCachNode);
                    }
                }
            }
        }
    }

    public class CImageCach
    {
        List<CImageCachNode> mSearchImages = null;
        List<CImageCachNode> mLoadedImages = null;
        UInt16 mMaxTryLoad = 6;
        double mMaxUpdateSec = 2.0;
        UInt16 mLoadLoopThreads = 10;
        UInt16 mSimLoadDelayTck = 0;
        public float mMaxCachHour = 48;
        DateTime mLastCachCheck;
        double mRetryLoadDelay = 0.5;

        private DataGridView mDataGrid = null;  // update image in datagrid after load when a row index is givven
        private UInt16 mColumnImage = 0;
        private UInt16 mColumnIndex = 0;

        private static Image _sEmptyImage = null;
        private static Image _sNoDataImage = null;

        private DateTime _mLastErrorDT;


        public CImageCach( double AMaxUpdateSec, UInt16 AMaxTryLoad, UInt16 ANrThreads, float AMaxCachHours, UInt16 ASimLoadDelayTck)
        {
            mSearchImages = new List<CImageCachNode>();
            mLoadedImages = new List<CImageCachNode>();

            mMaxUpdateSec = AMaxUpdateSec;
            mMaxTryLoad = AMaxTryLoad;
            mLoadLoopThreads = ANrThreads;
            mSimLoadDelayTck = ASimLoadDelayTck;
            mMaxCachHour = AMaxCachHours;
        }

         public static void sSetEmptyImage(Image AEmptyImage)
        {
            _sEmptyImage = AEmptyImage;
        }
        public static Image sGetEmptyImage()
        {
            return _sEmptyImage;
        }
        public static void sSetNoDataImage(Image AEmptyImage)
        {
            _sNoDataImage = AEmptyImage;
        }
        public static Image sGetNoDataImage()
        {
            return _sNoDataImage;
        }
        public static bool sIsEmptyImage( Image AImage )
        {
            return AImage == null || AImage == _sEmptyImage;
        }

        public Image mGetDisplayImage( Image AImage )
        {
            return AImage == null ? _sEmptyImage : AImage;
        }

        public void mSetDataGrid( DataGridView ADataGrid, UInt16 AColumnIndex, UInt16 AColumnImage)
        {
            mDataGrid = ADataGrid;  // update image in datagrid after load when a row index is givven
            mColumnImage = AColumnImage;
            mColumnIndex = AColumnIndex;
        }

         public void mUpdateGridImage( CImageCachNode AImageNode)
        {
            if ( AImageNode != null && AImageNode.mImage != null )
            {
                Int32 row = AImageNode.mDataGridRow;
                if ( row >= 0)
                {
                    if (mDataGrid != null && mDataGrid.Rows.Count > row )
                    {
                        try
                        {
                            if (mDataGrid.Rows[row].Cells[mColumnIndex].Value.ToString() == AImageNode.mIndex.ToString())
                            {
                                // make sure that it is the right row met the correct index
                                mDataGrid.Rows[row].Cells[mColumnImage].Value = mGetDisplayImage( AImageNode.mImage );
        //                        CProgram.sLogLine("show image " + AImageNode.mIndex + " at row " + row.ToString());
                            }
                        }
                        catch( Exception)
                        {

                        }
                    }
                }
            }
        }

    public double mGetMaxUpdateSec()
        {
            return mMaxUpdateSec;
        }
        CImageCachNode mFindImageInLists(UInt32 AIndex, string AFileName)
        {
            CImageCachNode foundIcn = null;
            if ( mLoadedImages != null )
            {
                foreach( CImageCachNode icn in mLoadedImages )
                {
                    if( icn.mIndex == AIndex && icn.mFileName == AFileName )
                    {
                        foundIcn = icn;
                        break;
                    }
                }
            }
            if (foundIcn == null && mSearchImages != null)
            {
                foreach (CImageCachNode icn in mSearchImages)
                {
                    if (icn.mIndex == AIndex && icn.mFileName == AFileName)
                    {
                        foundIcn = icn;
                        break;
                    }
                }
            }
            return foundIcn;
        }

        CImageCachNode mFindSearchFirstInLists(out UInt32 ArNrActiveThreads, DateTime ANow)
        {
            CImageCachNode foundIcn = null;
            UInt16 foundCount = UInt16.MaxValue;

            ArNrActiveThreads = 0;
            if (mSearchImages != null)
            {
                foreach (CImageCachNode icn in mSearchImages)
                {
                    if( icn.mImage == null )
                    {
                        if( icn.mbIsThreadRunning())
                        {
                            ++ArNrActiveThreads;
                        }
                        else if( icn.mRetryCount < mMaxTryLoad)
                        {
                            if( icn.mRetryCount < foundCount )
                            {
                                double sec = icn.mLastTryLoad > DateTime.MinValue ? (ANow - icn.mLastTryLoad).TotalSeconds : 9999999.0;

                                if( sec >= mRetryLoadDelay) // only retry when a sertain time has past to prevent double load, 
                                {                               // retry count passage to quickly  and prevent clogging the pc and FTP
                                    foundCount = icn.mRetryCount;
                                    foundIcn = icn;
                                    if (foundCount == 0)
                                    {
// need to walk past all to find nr of running threads                                        break;  // will not find a lower number
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return foundIcn;
        }

        UInt16 mStartSearchImages( DateTime ANow, out UInt32 ANrLoadImages )
        {
            UInt16 nThreadsStarted = 0;
            UInt32 nThreadsRunning;
            UInt32 nMax = 0;
            UInt32 nSearch = 0;
            UInt32 nFailed = 0;
            CImageCachNode nci = null;

            try
            {
                for (uint i = 0; i < mLoadLoopThreads; ++i)
                {
                    nci = mFindSearchFirstInLists(out nThreadsRunning, ANow );
                    if( nThreadsRunning > nMax)
                    {
                        nMax = nThreadsRunning;
                    }
                    if (nci == null || nThreadsRunning >= mLoadLoopThreads)
                    {
                        break;
                    }
                    if( nci.mbStartThread(mSimLoadDelayTck, ANow, this, mMaxTryLoad))
                    {
                        ++nThreadsRunning;
                        if (nThreadsRunning > nMax)
                        {
                            nMax = nThreadsRunning;
                        }
                        ++nThreadsStarted;
                    }
                }
                foreach (CImageCachNode icn in mSearchImages)
                {
                    if (icn.mImage == null)
                    {
                        if (icn.mRetryCount < mMaxTryLoad)
                        {
                            ++nSearch;
                        }
                        else
                        {
                            ++nFailed;
                        }
                    }
                    else
                    {
//                        if( icn.mbCleanFinishedTread(ANow, mMaxTryLoad))
                        {
//leave moving to another place                            mSearchImages.Remove(icn);
//                            mLoadedImages.Insert(0, icn);
                        }
                    }
                }
                }
            catch ( Exception ex )
            {
                DateTime dt = DateTime.Now;

                if (_mLastErrorDT == null || (dt - _mLastErrorDT).TotalSeconds > 2.0)
                {
                    CProgram.sLogError("failed to start a loading thread");
                    // prevent logging repeat loop
                    _mLastErrorDT = dt;
                }
            }
            ANrLoadImages = nSearch;
            return (UInt16)(/*nThreadsStarted +*/ nMax);
        }

        public UInt32 mCheckSearchImagesDone(DateTime ANow, out UInt32 ArNrSearch, out UInt32 ArNrFailed)
        {
            UInt32 nSearch = 0;
            UInt32 nFailed = 0;
            UInt32 nMoved = 0;

            try
            {
                int nTot = mSearchImages.Count;

                //                foreach (CImageCachNode icn in mSearchImages)
                for ( int i = 0; i < nTot;)
                {
                    CImageCachNode icn = mSearchImages[i];

                    if (icn.mImage == null)
                    {
                        if (icn.mRetryCount < mMaxTryLoad)
                        {
                            ++nSearch;
                        }
                        else
                        {
                            ++nFailed;
                        }
                        ++i;
                    }
                    else
                    {
                        if (icn.mbCleanFinishedTread(ANow, mMaxTryLoad))
                        {
                            mSearchImages.Remove(icn);
                            mLoadedImages.Insert(0, icn);
                            ++nMoved;
                            --nTot;
                        }
                        else
                        {
                            ++nSearch;
                            ++i;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                DateTime dt = DateTime.Now;

                if (_mLastErrorDT == null || (dt - _mLastErrorDT).TotalSeconds > 2.0)
                {
                    CProgram.sLogException("failed check search", ex);
                    // prevent logging repeat loop
                    _mLastErrorDT = dt;
                }
            }
            ArNrSearch = nSearch;
            ArNrFailed = nFailed;
            return nMoved;
        }
        public UInt32 mRemoveOldImages( DateTime ANow, double AMaxAgeHours )
        {
            UInt32 nRemoved = 0;
            UInt32 nLoaded = 0;
            UInt32 nSearch = 0;
            double maxAgeHours = AMaxAgeHours < 0.01 ? mMaxCachHour : AMaxAgeHours;
            DateTime dt = ANow.AddHours(-maxAgeHours);

            if (mLoadedImages != null)
            {
                try
                {
                    int nTot = mLoadedImages.Count;

                    for (int i = 0; i < nTot;)
                    {
                        CImageCachNode icn = mLoadedImages[i];

                        //                        foreach (CImageCachNode icn in mLoadedImages)
                        ++nLoaded;
                        if (icn.mLastUsed < dt && false == icn.mbIsThreadRunning())
                        {
                            if (icn.mImage != null)
                            {
//                                icn.mImage.Dispose();
                                icn.mImage = null;
                            }
                            mLoadedImages.Remove(icn);
                            icn.mbCleanTread();
                            ++nRemoved;
                            --nTot;
                        }
                        else
                        {
                            ++i;
                        }
                    }
                }
                catch (Exception ex)
                {
                    DateTime dt2 = DateTime.Now;

                    if (_mLastErrorDT == null || (dt2 - _mLastErrorDT).TotalSeconds > 2.0)
                    {
                        CProgram.sLogException("failed remove old from loaded", ex);
                        // prevent logging repeat loop
                        _mLastErrorDT = dt2;
                    }
                }
            }
            try
            {
                int nTot = mSearchImages.Count;

                for (int i = 0; i < nTot;)
                {
                    CImageCachNode icn = mSearchImages[i];
                    //                    foreach (CImageCachNode icn in mSearchImages)
                    ++nSearch;
                    if (icn.mLastUsed < dt && false == icn.mbIsThreadRunning())
                    {
                        mSearchImages.Remove(icn);
                        icn.mbCleanTread();
                        ++nRemoved;
                        --nTot;
                    }
                    else
                    {
                        ++i;
                    }
                }
            }
            catch (Exception ex)
            {
                DateTime dt2 = DateTime.Now;

                if (_mLastErrorDT == null || (dt2 - _mLastErrorDT).TotalSeconds > 2.0)
                {
                    CProgram.sLogException("failed remove old from search", ex);
                    // prevent logging repeat loop
                    _mLastErrorDT = dt2;
                }
            }
            if ( nRemoved > 0 )
            {
                CProgram.sLogLine("ImageCach:" + nRemoved.ToString() + " removed, " + nLoaded.ToString() + " loaded, " + nSearch.ToString() + " searching");
                CProgram.sMemoryCleanup(true, true);
            }
            return nRemoved;
        }

        public UInt32 mTryLoadImages( out UInt16 ANrThreadsActive )
        {
            UInt32 nLoadImages = 0;
            DateTime dt = DateTime.Now;
            UInt16 nThreads = mStartSearchImages(dt, out nLoadImages );
            double hours = (dt - mLastCachCheck).TotalHours;

            ANrThreadsActive = nThreads;

            if ( hours >= 0.25 ) // test every 15 minutes (nThreads > 0 && hours > 1 ) || hours > 24 )
            {
                    mRemoveOldImages(dt, 0);
                    mLastCachCheck = dt;
            }
            return nLoadImages;
        }
        public UInt32 mGetNrOfLoadedImages()
        {
            UInt32 n = mLoadedImages == null ? 0 : (UInt32)mLoadedImages.Count;

            return n;
        }
        public UInt32 mGetNrOfSearchImages()
        {
            UInt32 n = mSearchImages == null ? 0 : (UInt32)mSearchImages.Count;

            return n;
        }

        public string mPathCombine(string APath, string AFileName)
        {
            string s = APath;
            int n = APath == null ? 0 : s.Length;
            if (n > 0 && s[n - 1] != '\\')
            {
                s += "\\";
            }
            return s + AFileName;
        } 

        public Image mGetImage(bool AbDirectLoad, UInt32 AIndex, string AFilePath, string AFileName, Int32 ARow)
        {
            Image img = null;
            CImageCachNode icn = mFindImageInLists(AIndex, AFileName);
            bool bNew = false;
            DateTime dt = DateTime.Now;

            if (icn != null)
            {
                icn.mDataGridRow = ARow;

                if (icn.mImage != null)
                {
                    icn.mLastUsed = DateTime.Now;
                    return icn.mImage;                  // found valid image => return
                }
                if (icn.mRetryCount > mMaxTryLoad)
                {
                    return _sEmptyImage;    // do not waist time to try and load, return null                   
                }
            }
            else
            {
                icn = new CImageCachNode(AIndex, AFilePath, AFileName, ARow ); // find new image

                if (icn == null)
                {
                    return _sEmptyImage;
                }
                bNew = true;
            }

            if (AbDirectLoad)
            {
                bool bTry = bNew;

                if( bTry == false)
                {
                    double sec = icn.mLastTryLoad > DateTime.MinValue ? (dt - icn.mLastTryLoad).TotalSeconds : 0.0;

                    bTry = sec >= mRetryLoadDelay; // only retry when a sertain time has past to prevent double load, 
                                                   // retry count passage to quickly  and prevent clogging the pc and FTP
                    if (bTry == false)
                    {
                        // curently running, wait for thread
                        for (int i = 0; i < 30; ++i)        // max 300 ms
                        {
                            // thread is running give it a small time
                            Thread.Sleep(10);
                            img = icn.mImage;       // if loaded then use it, if not bad luck;
                            if (img != null)
                            {
                                break;
                            }
                        }
                    }
                }
                if( bTry )
                {
                    try
                    {
                        ++icn.mRetryCount;
                        img = Image.FromFile(mPathCombine/*Path.Combine*/(icn.mFilePath, icn.mFileName));
                        if( img != null )
                        {
                            //CProgram.sLogLine("ImageCach Direct loaded " + icn.mIndex.ToString() + "= " + icn.mFileName + ", try= " + icn.mRetryCount.ToString());
                        }
                        if (mSimLoadDelayTck > 0)
                        {
                            Thread.Sleep(mSimLoadDelayTck);
                        }
                    }
                    catch (Exception /*ex*/)
                    {
                        try
                        {
                            if( File.Exists(mPathCombine/*Path.Combine*/(icn.mFilePath, icn.mFileName)))
                            {
                                CProgram.sLogLine("failed to try loading " + icn.mIndex.ToString() + "= " + icn.mFileName + ", try= " + icn.mRetryCount.ToString());
                            }
                            else
                            {
                                img = _sNoDataImage;
                                // no file 
                            }
                        }
                        catch( Exception)
                        {

                        }

                    }
                    icn.mImage = img;
                }
            }
            if (img != null)
            {
                icn.mLastUsed = dt;

                if (bNew == false)
                {
                    icn.mbCleanTread();                 // move found image to loaded images;
                    mSearchImages.Remove(icn);
                }
                mLoadedImages.Insert(0, icn);
            }
            else if (bNew)
            {
                mSearchImages.Add( icn);
            }
            return mGetDisplayImage( img );
        }
    }
}
