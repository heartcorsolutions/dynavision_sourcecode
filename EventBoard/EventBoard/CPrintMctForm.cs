﻿using Event_Base;
using EventBoard.EntryForms;
using EventBoard.Event_Base;
using EventboardEntryForms;
using PdfSharp;
using PdfSharp.Pdf;
using Program_Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace EventBoard
{
    public partial class CPrintMctForm : Form
    {
        public CStudyInfo _mStudyInfo = null;
        public CPatientInfo _mPatientInfo = null;
        public CDeviceInfo _mDeviceInfo = null;
        public CTzReport _mReport = null;
        public CMCTTrendDisplay _mFormMct = null;
        Chart _mChartAll = null;

        public string _mCreadedByInitials;
        bool _mbAnonymize;
        bool _mbBlackWhite;

        UInt32 _mTestOffset = 0;

        public UInt16 _mStudyReportNr = 0;
        public UInt32 _mReportIX = 0;

        public DateTime _mTimeFrameFrom;
        public DateTime _mTimeFrameTo;
        public bool _bTimeFrameUseAM;

        Bitmap[] mPrintBitmapArray;

        UInt16 mNrPages = 0;
//        UInt16 mNrSamples = 0;
//        UInt16 mCurrentSample = 0;
        UInt16 mCurrentPrintPage = 0;
        const UInt16 _cNrSamplesFirstPage = 0;
        const UInt16 _cNrSamplesPerPage = 3;
        private bool mbFirstPage = true;

        private bool _mbHideReportNr = false;

        private bool _mbValidated = false;
        private bool _mbCountAnOnly = false;
        private bool _mbPrintStat = true;
        private bool _mbPrintDur = true;
        private bool _mbQCd = false;


        public CPrintMctForm(CMCTTrendDisplay AFormMct, Chart AChartAll, CTzReport AReport, bool AbValidated, CStudyInfo AStudyInfo, CPatientInfo APatientInfo, CDeviceInfo ADeviceInfo, 
            DateTime ATimeFrameFrom, DateTime ATimeFrameTo, bool AbSmall, string ACreadedByInitials, bool AbAnonymize, bool AbBlackWhite,
            bool AbCountAnOnly, bool AbPrintStat, bool AbPrintDur, bool AbQCd)

        {
            bool bOk = false;

            try
            {
                _mFormMct = AFormMct;
                _mChartAll = AChartAll;
                _mStudyInfo = AStudyInfo;
                _mPatientInfo = APatientInfo;
                _mDeviceInfo = ADeviceInfo;
                _mReport = AReport;
                _mbValidated = AbValidated;
                _mCreadedByInitials = ACreadedByInitials;
                _mTimeFrameFrom = ATimeFrameFrom;
                _mTimeFrameTo = ATimeFrameTo;
                _mbAnonymize = AbAnonymize;
                _mbBlackWhite = AbBlackWhite;
                _mbCountAnOnly = AbCountAnOnly;
                _mbPrintStat = AbPrintStat;
                _mbPrintDur = AbPrintDur;
                _mbQCd = AbQCd;

                 _mTestOffset = 0;

                string timeFormat = CProgram.sGetShowTimeFormat().ToUpper();
                _bTimeFrameUseAM = timeFormat.Contains("t");

                if (_mReport != null && _mStudyInfo != null && _mPatientInfo != null && _mDeviceInfo != null)
                {
                    bOk = true;
                }
                _mbHideReportNr = Properties.Settings.Default.HideReportNr;

                if (bOk)
                {
                    InitializeComponent();

                      if (AbSmall)
                    {
                        Width = 233;
                        Height = 85;
                    }

                    string reportStr = "";

                    _mStudyReportNr = 0;

                    if (false == _mbAnonymize && _mStudyInfo != null)
                    {
                        if (_mStudyInfo.mbGetNextReportNr(out _mStudyReportNr))
                        {
                            if (false == _mbHideReportNr)
                            {
                                reportStr = _mStudyReportNr.ToString();
                            }
                        }
                    }
                    labelReportNr.Text = reportStr;

                    CProgram.sbLogMemSize("open MCT print S" + _mStudyInfo.mIndex_KEY.ToString() + " report " + _mStudyReportNr.ToString());
                    toolStrip1.Focus();

//                    labelTextQC1.Text = _mbQCd ? "QC" : "";
  //                  labelTextQC2.Text = labelTextQC1.Text;

                    mSetFormValues();

                    mSetStudy();
                    mUpdateStatsCharts();
                    mUpdateStatsTable();

                    BringToFront();
                }
                else
                {
                    CProgram.sPromptError(false, "PrintStudyEventForm", "Form not initialized");
                    Close();
                }

            }
            catch (Exception ex)
            {
                CProgram.sLogException("CPrintMctForm() failed to init", ex);
                Close();
            }

        }

        private void mSetStudy()
        {
            if (_mStudyInfo != null && _mPatientInfo != null && _mDeviceInfo != null)
            {
                //                labelStudy.Text = _mStudyInfo.mIndex_KEY.ToString();
                //                labelRecorderID.Text = _mDeviceID;
                labelPatIDHeader.Text = _mPatientInfo.mGetPatientSocIDText() + ":";
                labelPatID.Text = _mbAnonymize ? "" : _mPatientInfo.mGetPatientSocIDValue(false)/* _mPatientID.mDecrypt()*/;
                //                labelPatName.Text = _mPatientInfo.mGetFullName();
                //                labelDoB.Text = CProgram.sDateToString(_mPatientInfo._mPatientDateOfBirth.mDecryptToDate());

                int age = _mPatientInfo.mGetAgeYears();
                string s = ( age >= 0 ? age.ToString() + " years,  " : "" ) + CPatientInfo.sGetGenderString((DGender)_mPatientInfo._mPatientGender_IX);
                labelAgeGender.Text = s;
                labelStartDate.Text = CProgram.sDateToString(_mStudyInfo._mStudyStartDate);

                DateTime nowDevice = DateTime.UtcNow.AddMinutes(_mReport._mTimeZoneOffsetMin);
                TimeSpan ts = _mTimeFrameTo - _mTimeFrameFrom; // _mReport._mEndTimeDT - _mReport._mStartTimeDT;//( nowDevice - _mStudyInfo._mStudyStartDate );
                double d = ts.TotalDays + (0.2 / 24);   // round up to get hole hour
                int days = (int)d;
                int hours = (int)((d - days) * 24);
                labelDaysHours.Text = days.ToString() + " days and " + hours.ToString() + " hours";

 //               labelNow.Text = CProgram.sTimeToString(DateTime.Now);

                labelFirstDate.Text = CProgram.sDateTimeToString(_mTimeFrameFrom); // _mReport._mStartTimeDT);
                labelTrendTo.Text = CProgram.sDateTimeToString(_mTimeFrameTo);

                //                _mStartPeriodDT = DateTime.SpecifyKind(_mReport._mStartTimeDT, DateTimeKind.Local );
                //               _mEndPeriodDT = DateTime.SpecifyKind(_mReport._mEndTimeDT, DateTimeKind.Local);

                s = "";
                if (CDvtmsData._sEnumListStudyProcedures != null)
                {
                    s = CDvtmsData._sEnumListStudyProcedures.mFillString(_mStudyInfo._mStudyProcCodeSet, true, " ", true, "\r\n");
                }

                labelDiagnosis.Text = s;

                labelTotalBeats.Text = "";
            }
        }

        private string mTimeStr(double AT)
        {
            int i = (int)AT;
            int sec = i % 60;
            i /= 60;
            int min = i % 60;
            i /= 60;
            int hour = i;
            //int days = i / 24;

            string s = hour.ToString("D2") + ":" + min.ToString("D02") + ":" + sec.ToString("D02");
            return s;
        }

        private string mTimeDayStr(double AT)
        {
            int i = (int)AT;
            int sec = i % 60;
            i /= 60;
            int min = i % 60;
            i /= 60;
            int hour = i % 24;
            int days = i / 24;

            string s = days.ToString() + "d " + hour.ToString("D2") + ":" + min.ToString("D02") + ":" + sec.ToString("D02");
            return s;
        }

        private string mPerStr(double AT, double ATotal)
        {
            double d = ATotal <= 0.001 ? 0.0 : 100.0 * AT / ATotal;
            return d.ToString("0.00");
        }

        private void mSetChartValueColor( Chart AChartBar,  UInt16 APointIndex, UInt32 AValue, string AClassification )
        {
            try
            {
                DataPoint pointBar = AChartBar.Series[0].Points[APointIndex];
                Color color = CDvtmsData.sPrintClassificationColor(AClassification, _mbBlackWhite);

                pointBar.SetValueY(AValue+ _mTestOffset);
                pointBar.Color = color;
            }
            catch( Exception ex )
            {
                CProgram.sLogException("MCTTrendForm() failed to set chart point " + APointIndex.ToString() + " " + AClassification, ex);

            }
        }
        private void mSetChartSeriesColor(Chart AChart, UInt16 ASeriesIndex, string AClassification)
        {
            try
            { 
               Color color = CDvtmsData.sPrintClassificationColor(AClassification, _mbBlackWhite);

                AChart.Series[ASeriesIndex].Color = color;
              }
            catch (Exception ex)
            {
                CProgram.sLogException("MCTTrendForm() failed to set chart series " + ASeriesIndex.ToString() + " " + AClassification, ex);

            }
        }
        public void mClearChart(Chart AChart, Double AAxisMin, Double AAxisMax, string ALabelFormat)
        {
            mFillChart(AChart, null, AAxisMin, AAxisMax, ALabelFormat);
        }
            public void mFillChart(Chart AChart, CMctReportItem ASerie0, Double AAxisMin, Double AAxisMax, string ALabelFormat)
        {
            string serieNames = "";
            try
            {
                // 
                AChart.Series[0].Points.Clear();
                AChart.ChartAreas[0].AxisX.Minimum = AAxisMin;
                AChart.ChartAreas[0].AxisX.Maximum = AAxisMax;
                AChart.ChartAreas[0].AxisX.LabelStyle.Format = ALabelFormat;

                if (ASerie0 != null)
                {
                    UInt32 value = 0;
                    UInt32 lastValue = 0;
                    bool bFirst = true;
                    bool bDo = true; // false;
                    float scale = _mbValidated ? 1.0F : 0.5F;
                    serieNames = ASerie0._mLabel;

                    foreach (CMctReportPoint point in ASerie0._mList)
                    {
                        if (point._mDateTime >= _mTimeFrameFrom && point._mDateTime <= _mTimeFrameTo)
                        {
                            if (value != point._mValue)
                            {
                                if (bFirst)
                                {
                                    AChart.Series[0].Points.AddXY(AAxisMin, 0);
                                    bFirst = false;
                                }
                                bDo = true;
                                if (bDo) AChart.Series[0].Points.AddXY(point._mDateTime.ToOADate(), value > 0 ? scale : 0);
                                value = point._mValue;

                                AChart.Series[0].Points.AddXY(point._mDateTime.ToOADate(), point._mValue > 0 ? scale : 0);
                                lastValue = value;
                            }
                        }
                        else
                        {
                            value = point._mValue;
                        }
                    }
                    if (false == bFirst)
                    {
                        AChart.Series[0].Points.AddXY(AAxisMax, lastValue > 0 ? scale : 0);
                    }

                }
                //AChart.Series[1].Points.Clear();

            }
            catch (Exception ex)
            {
                CProgram.sLogException("MCT Print failed to show stats chart " + serieNames, ex);
            }
        }
 
        private void mUpdateStatsCharts()
        {
            try
            {

                if (_mReport != null)
                {
                    DateTime firstDT = DateTime.MaxValue;
                    DateTime lastDT = DateTime.MinValue;
 
                    DateTime startPeriodDT = DateTime.SpecifyKind(_mTimeFrameFrom/*_mReport._mStartTimeDT*/, DateTimeKind.Local);
                    DateTime endPeriodDT = DateTime.SpecifyKind(_mTimeFrameTo/*_mReport._mEndTimeDT*/, DateTimeKind.Local);
                    TimeSpan ts = endPeriodDT - startPeriodDT;


                    int hours = (int)(ts.TotalHours + 0.1);
                    int days = hours / 24;
                    hours = hours % 24;

                    //labelTrendStart.Text = CProgram.sDateTimeToString(startPeriodDT);
                    //labelTrendEnd.Text = CProgram.sDateTimeToString(endPeriodDT);

                    //labelTrendTime.Text = days.ToString() + " days " + hours.ToString() + " hours";
                    labelTotalBeats.Text = "";


                    // chart HR
                    double xAxisMin = startPeriodDT.ToOADate();
                    double xAxisMax = endPeriodDT.ToOADate();
                    string labelFormat = CProgram.sGetShowTimeFormat() + "\n" + CProgram.sGetShowDateFormat();

                    chartHR.Series[0].Points.Clear();
                    chartHR.Series[1].Points.Clear();
                    chartHR.Series[2].Points.Clear();
                    chartHR.ChartAreas[0].AxisX.Minimum = xAxisMin;
                    chartHR.ChartAreas[0].AxisX.Maximum = xAxisMax;
                    chartHR.ChartAreas[0].AxisX.LabelStyle.Format = labelFormat;

                    if (false == _mbBlackWhite)
                    {
                        chartHR.Series[0].Color = Color.Blue;    // min

                        chartHR.Series[1].Color = Color.Red;    // max
                    
                        if( _mbValidated )
                        {
                            mSetChartSeriesColor(chartTachy, 0, CDvtmsData._cFindings_Tachy);
                            mSetChartSeriesColor(chartBrady, 0, CDvtmsData._cFindings_Brady);
                            mSetChartSeriesColor(chartPause, 0, CDvtmsData._cFindings_Pause);
                            mSetChartSeriesColor(chartAfib, 0, CDvtmsData._cFindings_AF);
                            mSetChartSeriesColor(chartAflut, 0, CDvtmsData._cFindings_AFL);
                            mSetChartSeriesColor(chartPAC, 0, CDvtmsData._cFindings_PAC);
                            mSetChartSeriesColor(chartPVC, 0, CDvtmsData._cFindings_PVC);
                            mSetChartSeriesColor(chartPace, 0, CDvtmsData._cFindings_Pace);
                            mSetChartSeriesColor(chartVtach, 0, CDvtmsData._cFindings_VT);
                            mSetChartSeriesColor(chartAbN, 0, CDvtmsData._cFindings_AbNormal);

                            mSetChartSeriesColor(chartOther, 0, CDvtmsData._cFindings_Other);
                        }
                    }

                    // HR average
                    foreach (CMctReportPoint point in _mReport._mItemHrAvg._mList)
                    {
                        if (point._mDateTime >= _mTimeFrameFrom && point._mDateTime <= _mTimeFrameTo)
                        {
                            chartHR.Series[2].Points.AddXY(point._mDateTime.ToOADate(), point._mValue);
                            if (point._mDateTime < firstDT) firstDT = point._mDateTime;
                            if (point._mDateTime > lastDT) lastDT = point._mDateTime;
                        }
                    }

                    // HR Min
                    foreach (CMctReportPoint point in _mReport._mItemHrMin._mList)
                    {
                        if (point._mDateTime >= _mTimeFrameFrom && point._mDateTime <= _mTimeFrameTo)
                        {
                            chartHR.Series[0].Points.AddXY(point._mDateTime.ToOADate(), point._mValue);
                        }
                    }

                    // HR Max

                    foreach (CMctReportPoint point in _mReport._mItemHrMax._mList)
                    {
                        if (point._mDateTime >= _mTimeFrameFrom && point._mDateTime <= _mTimeFrameTo)
                        {
                            chartHR.Series[1].Points.AddXY(point._mDateTime.ToOADate(), point._mValue);
                        }
                    }

                    // chart Tachy
                    mFillChart(chartTachy, _mReport.mGetTachy(_mbValidated), xAxisMin, xAxisMax, labelFormat);

                    /*                    UInt32 value = 0;
                                        bool bDo = false;

                                        chartTachy.Series[0].Points.Clear();
                                        chartTachy.ChartAreas[0].AxisX.Minimum = xAxisMin;
                                        chartTachy.ChartAreas[0].AxisX.Maximum = xAxisMax;
                                        chartTachy.ChartAreas[0].AxisX.LabelStyle.Format = labelFormat;

                                        foreach (CMctReportPoint point in _mReport.mGetTachy(_mbValidated)._mList)
                                        {
                                            if (point._mDateTime >= _mTimeFrameFrom && point._mDateTime <= _mTimeFrameTo)
                                            {
                                                if (bDo) chartTachy.Series[0].Points.AddXY(point._mDateTime.ToOADate(), value);
                                                value = point._mValue;
                                                bDo = true;
                                                chartTachy.Series[0].Points.AddXY(point._mDateTime.ToOADate(), point._mValue);
                                            }
                                            else
                                            {
                                                value = point._mValue;
                                            }
                                        }
                    */
                    // chart Brady
                    mFillChart(chartBrady, _mReport.mGetBrady(_mbValidated), xAxisMin, xAxisMax, labelFormat);
                    /*
                                        chartBrady.Series[0].Points.Clear();
                                        chartBrady.ChartAreas[0].AxisX.Minimum = xAxisMin;
                                        chartBrady.ChartAreas[0].AxisX.Maximum = xAxisMax;
                                        chartBrady.ChartAreas[0].AxisX.LabelStyle.Format = labelFormat;
                                        bDo = false;

                                        foreach (CMctReportPoint point in _mReport.mGetBrady(_mbValidated)._mList)
                                        {
                                            if (point._mDateTime >= _mTimeFrameFrom && point._mDateTime <= _mTimeFrameTo)
                                            {
                                                if (bDo) chartBrady.Series[0].Points.AddXY(point._mDateTime.ToOADate(), value);

                                                bDo = true;
                                                chartBrady.Series[0].Points.AddXY(point._mDateTime.ToOADate(), point._mValue);
                                            }
                                            value = point._mValue;
                                        }
                    */
                    // chart Pause
                    if( _mFormMct._mbValidatePause )
                    {
                        mFillChart(chartPause, _mReport.mGetPause(_mbValidated), xAxisMin, xAxisMax, labelFormat);
                    }
                    else
                    {
                        mClearChart(chartPause, xAxisMin, xAxisMax, labelFormat);
                    }
                    /*                    chartPause.Series[0].Points.Clear();
                                        chartPause.ChartAreas[0].AxisX.Minimum = xAxisMin;
                                        chartPause.ChartAreas[0].AxisX.Maximum = xAxisMax;
                                        chartPause.ChartAreas[0].AxisX.LabelStyle.Format = labelFormat;
                                        bDo = false;

                                        foreach (CMctReportPoint point in _mReport.mGetPause(_mbValidated)._mList)
                                        {
                                            if (point._mDateTime >= _mTimeFrameFrom && point._mDateTime <= _mTimeFrameTo)
                                            {
                                                if (bDo) chartPause.Series[0].Points.AddXY(point._mDateTime.ToOADate(), value);
                                                bDo = true;
                                                chartPause.Series[0].Points.AddXY(point._mDateTime.ToOADate(), point._mValue);
                                            }
                                            value = point._mValue;
                                        }
                     */                   // chart Afib
                    if (_mFormMct._mbValidateAFib)
                    {
                        mFillChart(chartAfib, _mReport.mGetAfib(_mbValidated), xAxisMin, xAxisMax, labelFormat);
                    }
                    else
                    {
                        mClearChart(chartAfib, xAxisMin, xAxisMax, labelFormat);
                    }

                    /*                    chartAfib.Series[0].Points.Clear();
                                        chartAfib.ChartAreas[0].AxisX.Minimum = xAxisMin;
                                        chartAfib.ChartAreas[0].AxisX.Maximum = xAxisMax;
                                        chartAfib.ChartAreas[0].AxisX.LabelStyle.Format = labelFormat;
                                        bDo = false;

                                        foreach (CMctReportPoint point in _mReport.mGetAfib(_mbValidated)._mList)
                                        {
                                            if (point._mDateTime >= _mTimeFrameFrom && point._mDateTime <= _mTimeFrameTo)
                                            {
                                                if (bDo) chartAfib.Series[0].Points.AddXY(point._mDateTime.ToOADate(), value);
                                                bDo = true;
                                                chartAfib.Series[0].Points.AddXY(point._mDateTime.ToOADate(), point._mValue);
                                            }
                                            value = point._mValue;
                                        }
                    */
                    // chart Manual
                    mFillChart(chartManual, _mReport.mGetManual(_mbValidated), xAxisMin, xAxisMax, labelFormat);
                    /*                    chartManual.Series[0].Points.Clear();
                                        chartManual.ChartAreas[0].AxisX.Minimum = xAxisMin;
                                        chartManual.ChartAreas[0].AxisX.Maximum = xAxisMax;
                                        chartManual.ChartAreas[0].AxisX.LabelStyle.Format = labelFormat;

                                        foreach (CMctReportPoint point in _mReport.mGetManual(_mbValidated)._mList)
                                        {
                                            if (point._mDateTime >= _mTimeFrameFrom && point._mDateTime <= _mTimeFrameTo)
                                            {
                                                chartManual.Series[0].Points.AddXY(point._mDateTime.ToOADate(), point._mValue);
                                            }
                                        }
                    */
                    // chart Pace
                    mFillChart(chartPace, _mReport.mGetPace(_mbValidated), xAxisMin, xAxisMax, labelFormat);
                    /*                    chartPace.Series[0].Points.Clear();
                                        chartPace.ChartAreas[0].AxisX.Minimum = xAxisMin;
                                        chartPace.ChartAreas[0].AxisX.Maximum = xAxisMax;
                                        chartPace.ChartAreas[0].AxisX.LabelStyle.Format = labelFormat;

                                        foreach (CMctReportPoint point in _mReport.mGetPace(_mbValidated)._mList)
                                        {
                                            if (point._mDateTime >= _mTimeFrameFrom && point._mDateTime <= _mTimeFrameTo)
                                            {
                                                chartPace.Series[0].Points.AddXY(point._mDateTime.ToOADate(), point._mValue);
                                            }
                                        }
                    */
                    // chart PAC
                    if (_mFormMct._mbValidatePacSveb)
                    {
                        mFillChart(chartPAC, _mReport.mGetPAC(_mbValidated), xAxisMin, xAxisMax, labelFormat);
                    }
                    else
                    {
                        mClearChart(chartPAC, xAxisMin, xAxisMax, labelFormat);
                    }

                    /*                    chartPAC.Series[0].Points.Clear();
                                        chartPAC.ChartAreas[0].AxisX.Minimum = xAxisMin;
                                        chartPAC.ChartAreas[0].AxisX.Maximum = xAxisMax;
                                        chartPAC.ChartAreas[0].AxisX.LabelStyle.Format = labelFormat;

                                        foreach (CMctReportPoint point in _mReport.mGetPAC(true)._mList)
                                        {
                                            if (point._mDateTime >= _mTimeFrameFrom && point._mDateTime <= _mTimeFrameTo)
                                            {
                                                chartPAC.Series[0].Points.AddXY(point._mDateTime.ToOADate(), point._mValue);
                                            }
                                        }
                                        */
                    // chart PVC
                    if (_mFormMct._mbValidatePvcVeb)
                    {
                        mFillChart(chartPVC, _mReport.mGetPVC(_mbValidated), xAxisMin, xAxisMax, labelFormat);
                    }
                    else
                    {
                        mClearChart(chartPVC, xAxisMin, xAxisMax, labelFormat);
                    }
                    

                    /*                    chartPVC.Series[0].Points.Clear();
                                        chartPVC.ChartAreas[0].AxisX.Minimum = xAxisMin;
                                        chartPVC.ChartAreas[0].AxisX.Maximum = xAxisMax;
                                        chartPVC.ChartAreas[0].AxisX.LabelStyle.Format = labelFormat;

                                        foreach (CMctReportPoint point in _mReport.mGetPVC(true)._mList)
                                        {
                                            if (point._mDateTime >= _mTimeFrameFrom && point._mDateTime <= _mTimeFrameTo)
                                            {
                                                chartPVC.Series[0].Points.AddXY(point._mDateTime.ToOADate(), point._mValue);
                                            }
                                        }
                    */
                    mFillChart(chartAflut, _mReport.mGetAflut(_mbValidated), xAxisMin, xAxisMax, labelFormat);

                    mFillChart(chartOther, _mReport.mGetOther(_mbValidated), xAxisMin, xAxisMax, labelFormat);

                    mFillChart(chartVtach, _mReport.mGetVtach(_mbValidated), xAxisMin, xAxisMax, labelFormat);

                    mFillChart(chartAbN, _mReport.mGetAbNorm(_mbValidated), xAxisMin, xAxisMax, labelFormat);

                    chartAll.Series[0].Points.Clear();
                    chartAll.ChartAreas[0].AxisX.Minimum = xAxisMin;
                    chartAll.ChartAreas[0].AxisX.Maximum = xAxisMax;

                    if( _mChartAll.Series[0].Points.Count == 0)
                    {
                        chartAll.Visible = false;
                    }
                    else
                    {
                        // copy chart All
                        chartAll.Visible = true;

                        foreach (DataPoint point in _mChartAll.Series[0].Points)
                        {
                            chartAll.Series[0].Points.Add(point);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("MCTTrendForm() failed to show charts", ex);
            }

        }
 
        private void mUpdateStatsTable()
        {
            try
            {

                panelStatHeader.Visible = _mbPrintStat;
                panelStatInfo.Visible = _mbPrintStat;
                panelDuration.Visible = _mbPrintDur;
                panelPer.Visible = _mbPrintDur;

                if (_mReport != null)
                {
//                    DateTime firstDT = DateTime.MaxValue;
//                    DateTime lastDT = DateTime.MinValue;
                    UInt32 nTachy = 0; // _mReport.mGetTachy(_mbValidated)._mCountOff2On;
                    double tTachy = 0; // _mReport.mGetTachy(_mbValidated)._mDurationOnSec;

                    UInt32 nBrady = 0; // _mReport.mGetBrady(_mbValidated)._mCountOff2On;
                    double tBrady = 0; // _mReport.mGetBrady(_mbValidated)._mDurationOnSec;

                    UInt32 nPause = 0; // _mReport.mGetPause(_mbValidated)._mCountOff2On;
                    double tPause = 0; // _mReport.mGetPause(_mbValidated)._mDurationOnSec;

                    UInt32 nAfib = 0; // _mReport.mGetAfib(_mbValidated)._mCountOff2On;
                    double tAfib = 0;// _mReport.mGetAfib(_mbValidated)._mDurationOnSec;

                    UInt32 nPAC = 0;
                    double tPAC = 0;

                    UInt32 nPVC = 0;
                    double tPVC = 0;

                    UInt32 nManual = 0; //  _mReport.mGetManual(_mbValidated)._mCountOff2On;
                    //                double tManual = _mReport.mGetManual(_mbValidated)._mDurationOnSec;
                    //                double perManual = tManual / tTotal;

                     UInt32 nAflut = 0;
                    double tAflut = 0;

                    UInt32 nPace = 0;
                    double tPace = 0;

                    UInt32 nVtach = 0;
                    double tVtach = 0;

                    UInt32 nOther = 0;
                    double tOther = 0;

                    UInt32 nNorm = 0;
                    double tNorm = 0;

                    UInt32 nAbNorm = 0;
                    double tAbNorm = 0;


                    if (_mDeviceInfo._mDeviceSerialNr != _mReport._mDeviceID)
                    {
                        //                        labelSnrError.Text = "!= " + _mReport._mDeviceID;
                        //                        labelDeviceError.Text = _mDeviceInfo._mDeviceSerialNr + " != " + _mReport._mDeviceID;
                    }
                    else
                    {
                        //                        labelSnrError.Text = "";
                    }
                    //                    UInt32 nPace = _mReport.mGetPace(_mbValidated)._mCountOff2On;
                    //!                    UInt32 nPvc = _mReport._mItemPvc._mCountOff2On;

                    DateTime startPeriodDT = DateTime.SpecifyKind(_mTimeFrameFrom/*_mReport._mStartTimeDT*/, DateTimeKind.Local);
                    DateTime endPeriodDT = DateTime.SpecifyKind(_mTimeFrameTo/*_mReport._mEndTimeDT*/, DateTimeKind.Local);
                    TimeSpan ts = endPeriodDT - startPeriodDT;
                    double tPeriod = ts.TotalSeconds;
                    int hours = (int)(ts.TotalHours + 0.1);
                    int days = hours / 24;
                    hours = hours % 24;

                    //labelTrendStart.Text = CProgram.sDateTimeToString(startPeriodDT);
                    //labelTrendEnd.Text = CProgram.sDateTimeToString(endPeriodDT);

                    //labelTrendTime.Text = days.ToString() + " days " + hours.ToString() + " hours";
                    labelTotalBeats.Text = "";

//                    double tPeriod = firstDT < DateTime.MaxValue ? (lastDT - firstDT).TotalSeconds + 0.01 : 0.001F; //_mReport._mItemRecording._mDurationOnSec + 0.001;
                    double tManual = 0; // not used

                    bool bLogAll = false;

/*  old20180918                  nTachy = _mbCountAnOnly && _mbValidated ? _mFormMct.mCountAnalyzed(out tTachy, CDvtmsData._cFindings_Tachy, bLogAll)
                    : _mReport.mGetTachy(_mbValidated).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tTachy, bLogAll);

                    nBrady = _mbCountAnOnly && _mbValidated ? _mFormMct.mCountAnalyzed(out tBrady, CDvtmsData._cFindings_Brady, bLogAll)
                                        : _mReport.mGetBrady(_mbValidated).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tBrady, bLogAll);

                    nPause = _mbCountAnOnly && _mbValidated ? _mFormMct.mCountAnalyzed(out tPause, CDvtmsData._cFindings_Pause, bLogAll)
                                        : _mReport.mGetPause(_mbValidated).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tPause, bLogAll);

                    nAfib = _mbCountAnOnly && _mbValidated ? _mFormMct.mCountAnalyzed(out tAfib, CDvtmsData._cFindings_AF, bLogAll)
                                        : _mReport.mGetAfib(_mbValidated).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tAfib, bLogAll);


                    nManual = _mbCountAnOnly && _mbValidated ? _mFormMct.mCountManualAnalyzed(out tManual, bLogAll)
                                        : _mReport.mGetManual(_mbValidated).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tManual, bLogAll);

                    nPAC = _mbCountAnOnly && _mbValidated ? _mFormMct.mCountAnalyzed(out tPAC, CDvtmsData._cFindings_PAC, bLogAll)
                                        : _mReport.mGetPAC(_mbValidated).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tPAC, bLogAll);

                    nPVC = _mbCountAnOnly && _mbValidated ? _mFormMct.mCountAnalyzed(out tPVC, CDvtmsData._cFindings_PVC, bLogAll)
                                        : _mReport.mGetPVC(_mbValidated).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tPVC, bLogAll);

                    nPace = _mbCountAnOnly && _mbValidated ? _mFormMct.mCountAnalyzed(out tPace, CDvtmsData._cFindings_Pace, bLogAll)
                    : _mReport.mGetPace(_mbValidated).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tPace, bLogAll);

                    nVtach = _mbCountAnOnly && _mbValidated ? _mFormMct.mCountAnalyzed(out tVtach, CDvtmsData._cFindings_VT, bLogAll)
                                        : _mReport.mGetVtach(_mbValidated).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tVtach, bLogAll);

                    nAflut = _mbCountAnOnly && _mbValidated ? _mFormMct.mCountAnalyzed(out tAflut, CDvtmsData._cFindings_AFL, bLogAll)
                                        : _mReport.mGetAflut(_mbValidated).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tAflut, bLogAll);

                    nOther = _mbCountAnOnly && _mbValidated ? _mFormMct.mCountAnalyzed(out tOther, CDvtmsData._cFindings_Other, bLogAll)
                                        : _mReport.mGetOther(_mbValidated).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tOther, bLogAll);
*/
                    nTachy = _mReport.mGetTachy(_mbValidated).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tTachy, bLogAll);

                    nBrady = _mReport.mGetBrady(_mbValidated).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tBrady, bLogAll);

                    nPause = _mReport.mGetPause(_mbValidated).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tPause, bLogAll);

                    nAfib = _mReport.mGetAfib(_mbValidated).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tAfib, bLogAll);


                    nManual = _mReport.mGetManual(_mbValidated).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tManual, bLogAll);

                    nPAC = _mReport.mGetPAC(_mbValidated).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tPAC, bLogAll);

                    nPVC = _mReport.mGetPVC(_mbValidated).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tPVC, bLogAll);

                    nPace = _mReport.mGetPace(_mbValidated).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tPace, bLogAll);

                    nVtach = _mReport.mGetVtach(_mbValidated).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tVtach, bLogAll);

                    nAbNorm = _mReport.mGetAbNorm(_mbValidated).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tAbNorm, bLogAll);

                    nAflut =  _mReport.mGetAflut(_mbValidated).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tAflut, bLogAll);

                    nOther = _mReport.mGetOther(_mbValidated).mCountDuration(_mTimeFrameFrom, _mTimeFrameTo, out tOther, bLogAll);

                    if (_mbCountAnOnly && _mbValidated)// 20180918 show count only, duration from graph
                    {
                        double tCountAnOn;
                        nTachy = _mFormMct.mCountAnalyzed(out tCountAnOn, CDvtmsData._cFindings_Tachy, bLogAll);

                        nBrady = _mFormMct.mCountAnalyzed(out tCountAnOn, CDvtmsData._cFindings_Brady, bLogAll);

                        nPause = _mFormMct.mCountAnalyzed(out tCountAnOn, CDvtmsData._cFindings_Pause, bLogAll);

                        nAfib = _mFormMct.mCountAnalyzed(out tCountAnOn, CDvtmsData._cFindings_AF, bLogAll);

                        nManual = _mFormMct.mCountManualAnalyzed(out tCountAnOn, bLogAll);
 
                        nPAC = _mFormMct.mCountAnalyzed(out tCountAnOn, CDvtmsData._cFindings_PAC, bLogAll);

                        nPVC = _mFormMct.mCountAnalyzed(out tCountAnOn, CDvtmsData._cFindings_PVC, bLogAll);

                        nPace = _mFormMct.mCountAnalyzed(out tCountAnOn, CDvtmsData._cFindings_Pace, bLogAll);

                        nVtach = _mFormMct.mCountAnalyzed(out tCountAnOn, CDvtmsData._cFindings_VT, bLogAll);

                        nAflut = _mFormMct.mCountAnalyzed(out tCountAnOn, CDvtmsData._cFindings_AFL, bLogAll);

                        nOther = _mFormMct.mCountAnalyzed(out tOther, CDvtmsData._cFindings_Other, bLogAll);

                    }


                    nNorm = 0;
                    tNorm = 0;
                    if ((_mbCountAnOnly )&& _mbValidated)
                    {
                        nNorm = _mFormMct.mCountAnalyzed(out tNorm, CDvtmsData._cFindings_Normal, bLogAll);

                        nAbNorm = _mFormMct.mCountAnalyzed(out tAbNorm, CDvtmsData._cFindings_AbNormal, bLogAll);
                    }


                    double tTotal = tTachy + tBrady + tPause + tAfib + /*tPAC + tPVC + tPace */+tAflut + tVtach;// + tManual;
                    uint nTotal = nTachy + nBrady + nPause + nAfib + nPAC + nPVC + nPace + nAflut + nVtach + nOther + nNorm + nAbNorm;// + nManual;

                    uint nAll = nTachy + nBrady + nPause + nAfib + nPAC + nPVC + nPace + nAflut + nVtach + nOther + + nNorm + nAbNorm + nManual;

                    uint nMax = nTachy;
                    if (nBrady > nMax) nMax = nBrady;
                    if (nPause > nMax) nMax = nPause;
                    if (nAfib > nMax) nMax = nAfib;
                    if (nPAC > nMax) nMax = nPAC;
                    if (nPVC > nMax) nMax = nPVC;
                    if (nPace > nMax) nMax = nPace;
                    if (nAflut > nMax) nMax = nAflut;
                    if (nVtach > nMax) nMax = nVtach;
                    if (nOther > nMax) nMax = nOther;
                    if (nNorm > nMax) nMax = nNorm;
                    if (nAbNorm > nMax) nMax = nAbNorm;
                    if (nManual > nMax) nMax = nManual;

                    // Episodes Summery graph

                    if (nAll == 0)
                    {
                        chartSummaryBar.Visible = false;
                    }
                    else
                    {
                        chartSummaryBar.Visible = true;
                        /*                        chartSummaryBar.Series[0].Points[0].SetValueY(nTachy); 
                                                chartSummaryBar.Series[0].Points[1].SetValueY(nBrady);
                                                chartSummaryBar.Series[0].Points[2].SetValueY(nPause);
                                                chartSummaryBar.Series[0].Points[3].SetValueY(nAfib);
                                                chartSummaryBar.Series[0].Points[4].SetValueY(nPAC);
                                                chartSummaryBar.Series[0].Points[5].SetValueY(nPVC);
                                                chartSummaryBar.Series[0].Points[6].SetValueY(nManual);
                                                *///
                                                  //                        AxisY.Maximum = Double.NaN; // sets the Maximum to NaN
                                                  //                        AxisY.Minimum = Double.NaN; // sets the Minimum to NaN

                        chartSummaryBar.ChartAreas[0].AxisY.LabelStyle.Interval = nMax < 10 ? 1 : 0;
                        chartSummaryBar.ChartAreas[0].AxisY.MajorGrid.Interval = nMax < 10 ? 1 : 0;
                        chartSummaryBar.ChartAreas[0].AxisY.MajorTickMark.Interval = nMax < 10 ? 1 : 0;

                        UInt16 iChart = 0;
                        mSetChartValueColor(chartSummaryBar, iChart++, nTachy, CDvtmsData._cFindings_Tachy);
                        mSetChartValueColor(chartSummaryBar, iChart++, nBrady, CDvtmsData._cFindings_Brady);
                        mSetChartValueColor(chartSummaryBar, iChart++, nPause, CDvtmsData._cFindings_Pause);
                        mSetChartValueColor(chartSummaryBar, iChart++, nAfib, CDvtmsData._cFindings_AF);
                        mSetChartValueColor(chartSummaryBar, iChart++, nAflut, CDvtmsData._cFindings_AFL);
                        mSetChartValueColor(chartSummaryBar, iChart++, nPAC, CDvtmsData._cFindings_PAC);
                        mSetChartValueColor(chartSummaryBar, iChart++, nPVC, CDvtmsData._cFindings_PVC);
                        mSetChartValueColor(chartSummaryBar, iChart++, nPace, CDvtmsData._cFindings_Pace);
                        mSetChartValueColor(chartSummaryBar, iChart++, nVtach, CDvtmsData._cFindings_VT);
                        mSetChartValueColor(chartSummaryBar, iChart++, nOther, CDvtmsData._cFindings_Other);
                        mSetChartValueColor(chartSummaryBar, iChart++, nNorm, CDvtmsData._cFindings_Normal);
                        mSetChartValueColor(chartSummaryBar, iChart++, nAbNorm, CDvtmsData._cFindings_AbNormal);

                        mSetChartValueColor(chartSummaryBar, iChart++, nManual, CDvtmsData._cFindings_Manual);

                        chartSummaryBar.ChartAreas[0].RecalculateAxesScale();
                        chartSummaryBar.Refresh();

                        /*                        chartSummeryPie.Series[0].Points[0].SetValueY(nTachy); chartSummeryPie.Series[0].Points[0].IsEmpty = nTachy == 0;
                                                chartSummeryPie.Series[0].Points[1].SetValueY(nBrady); chartSummeryPie.Series[0].Points[1].IsEmpty = nBrady == 0;
                                                chartSummeryPie.Series[0].Points[2].SetValueY(nPause); chartSummeryPie.Series[0].Points[2].IsEmpty = nPause == 0;
                                                chartSummeryPie.Series[0].Points[3].SetValueY(nAfib); chartSummeryPie.Series[0].Points[3].IsEmpty = nAfib == 0;
                                                chartSummeryPie.Series[0].Points[4].SetValueY(nPAC); chartSummeryPie.Series[0].Points[4].IsEmpty = nPAC == 0;
                                                chartSummeryPie.Series[0].Points[5].SetValueY(nPVC); chartSummeryPie.Series[0].Points[5].IsEmpty = nPVC == 0;
                                                chartSummeryPie.Series[0].Points[6].SetValueY(nManual); chartSummeryPie.Series[0].Points[6].IsEmpty = nManual == 0;
                        */
                    }

                    labelEpTachy.Text = nTachy.ToString();
                    labelEpBrady.Text = nBrady.ToString();
                    labelEpPause.Text = nPause.ToString();
                    labelEpAfib.Text = nAfib.ToString();
                    labelEpAflut.Text = nAflut.ToString();
                    labelEpPAC.Text = nPAC.ToString();
                    labelEpPVC.Text = nPVC.ToString();
                    labelEpPace.Text = nPace.ToString();
                    labelEpVtach.Text = nVtach.ToString();
                    labelEpOther.Text = nOther.ToString();
                    labelEpNormal.Text = nNorm.ToString();
                    labelEpAbNormal.Text = nAbNorm.ToString();

                    labelEpMan.Text = nManual.ToString();
                    labelEpTotal.Text = nTotal.ToString();

                    labelGraphTachy.Text = labelEpTachy.Text;
                    labelGraphBrady.Text = labelEpBrady.Text;
                    labelGraphPause.Text = labelEpPause.Text;
                    labelGraphAfib.Text = labelEpAfib.Text;
                    labelGraphAflut.Text = labelEpAflut.Text;
                    labelGraphPAC.Text = labelEpPAC.Text;
                    labelGraphPVC.Text = labelEpPVC.Text;
                    labelGraphPace.Text = labelEpPace.Text;
                    labelGraphVtach.Text = labelEpVtach.Text;
                    labelGraphAbN.Text = labelEpAbNormal.Text;
                    labelGraphOther.Text = labelEpOther.Text;
                    labelGraphManual.Text = labelEpMan.Text;
                    labelGraphAll.Text = labelEpTotal.Text;

                    labelDurTachy.Text = mTimeStr(tTachy);
                    labelDurBrady.Text = mTimeStr(tBrady);
                    labelDurPause.Text = mTimeStr(tPause);
                    labelDurAfib.Text = mTimeStr(tAfib);
                    labelDurAflut.Text = mTimeStr(tAflut);
                    labelDurPAC.Text = "";
                    labelDurPVC.Text = "";
                    labelDurPace.Text = "";
                    labelDurVtach.Text = mTimeStr(tVtach);
                    labelDurAbNormal.Text = mTimeStr(tAbNorm);
                    labelDurOther.Text = "";
                    labelDurNormal.Text = "";

                    labelDurMan.Text = "";
                    labelDurTotal.Text = mTimeStr(tTotal);
//                    labelDurPeriod.Text = mTimeStr(tPeriod);

                    if (tPeriod < 0.01) tPeriod = _mReport.mGetDurationSec();

                    labelPerTachy.Text = mPerStr(tTachy, tPeriod);
                    labelPerBrady.Text = mPerStr(tBrady, tPeriod);
                    labelPerPause.Text = mPerStr(tPause, tPeriod);
                    labelPerAfib.Text = mPerStr(tAfib, tPeriod);
                    labelPerAflut.Text = mPerStr(tAflut, tPeriod);
                    labelPerPAC.Text = "";
                    labelPerPVC.Text = "";
                    labelPerPace.Text = "";
                    labelPerVtach.Text = mPerStr(tVtach, tPeriod);
                    labelPerAbNormal.Text = mPerStr(tAbNorm, tPeriod);
                    labelPerOther.Text = "";
                    labelPerNormal.Text = "";


                    labelPerMan.Text = "";
                    labelPerTotal.Text = mPerStr(tTotal, tPeriod);

                    //                    labelTimeMonitored.Text = mTimeDayStr(tPeriod);
                    //                    labelTimeAfib.Text = mTimeStr(tAfib);
                    //labelAfibHrMax.Text = "";
                    //labelLongAfib.Text = "";

                    //labelAfibHrAvg.Text = "";
                    //labelNoAfibHrAvg.Text = "";
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("MCTTrendForm() failed to show stats table", ex);

            }

        }

        public void mSetFormValues()
        {
            try
            {
                labelPage1.Text = "";


                Image img = CDvtmsData.sGetPrintCenterImage(_mbBlackWhite);
                pictureBoxCenter.Image = img;
                string s = CDvtmsData.sGetPrintCenterLine1();

                labelCenter1.Text = s;

                s = CDvtmsData.sGetPrintCenterLine2();
                labelCenter2.Text = s;

                mNrPages = 1;
                mCurrentPrintPage = 1;
                //test mNrPages = 2;

                mPrintBitmapArray = new Bitmap[mNrPages + 1];   // [0] is empty, currentPage starts at 1

                labelPage1xOfX.Text = mNrPages.ToString();
                labelPage1of.Text = mCurrentPrintPage.ToString();

                mInitHeader();

                mInitStats();
                mInitFooter();

                if (_mbValidated)
                {
                    labelNote1.Text = "";// "Note: the event data in the trend graphs and episode tables are a combination of \"non validated automatic triggered events\" and validated events.";
                    labelNote2.Text = ""; // "Be carefull with diagnostic purposes!";
                }
                else
                {
                    labelNote1.Text = "Note: the event data in the trend graphs and episode tables are \"automatic triggered events\" without human verification.";
                    labelNote2.Text = "Do not use for diagnostic purposes!";
                }


                toolStrip1.Focus();
            }
            catch ( Exception ex )
            {
                CProgram.sLogException("Set Analysis Print Form  error", ex);
            }
        }

        void mInitHeader()
        {
            DateTime dt;
//            string s;
            bool bStudy = _mStudyInfo != null && _mStudyInfo.mIndex_KEY > 0;
            bool bPatient = _mPatientInfo != null && _mPatientInfo.mIndex_KEY > 0;

            string userTitle = Properties.Settings.Default.PrintReportMCT;
            if (userTitle == null || userTitle.Length < 1)
            {
                userTitle = "Mobile Cardiac Telemetry Report ";
            }
            if( false == _mbHideReportNr )
            {
                userTitle += "  " + labelReportNr.Text;
            }
            labelCardiacEventReportNr.Text = userTitle;
            labelUser.Text =  FormEventBoard._sbPrintHideTechName ? "" : _mCreadedByInitials;// "SHJ";
            if (FormEventBoard._sbPrintHideTechName) labelTechText.Text = "";


            //labelReportNr.Text = ""; // _mStudyReport.mIndex_KEY == 0 ? "" : _mStudyReport.mIndex_KEY.ToString();

            /* s = "";
            if (bStudy && _mStudyInfo._mClient_IX > 0)
            {
                CClientInfo client = new EventboardEntryForms.CClientInfo(_mStudyInfo.mGetSqlConnection());

                if (client != null)
                {
                    client.mbDoSqlSelectIndex(_mStudyInfo._mClient_IX, CSqlDataTableRow.sGetMask((UInt16)DClientInfoVars.Label));
                    s = client._mLabel;
                }
            }
            labelClientName.Text = s; // "New York Hospital";
                                      //            labelClientHeader.Text = "CLIENT:";
                                      */
            CClientInfo.sFillPrintClientName(_mbAnonymize, _mStudyInfo, labelClientName, labelClientTel, true);

            /*
            s = "";
            if (bStudy && _mStudyInfo._mRefPhysisian_IX > 0)
            {
                CPhysicianInfo physician = new CPhysicianInfo(_mStudyInfo.mGetSqlConnection());

                if (physician != null)
                {
                    physician.mbDoSqlSelectIndex(_mStudyInfo._mRefPhysisian_IX, CSqlDataTableRow.sGetMask((UInt16)DPhysicianInfoVars.Label));
                    s = physician._mLabel;
                }
            }
            labelRefPhysicianName.Text = s; // "Dr. John Smithsonian";
            //labelRefPhysHeader.Text = "REFERRING PHYSICIAN:";
            */
            CPhysicianInfo.sFillPrintPhysicianName(_mbAnonymize, _mStudyInfo, true, labelRefPhysicianName, labelRefPhysicianTel, true);
            /*
            s = "";
            if (bStudy && _mStudyInfo._mPhysisian_IX > 0)
            {
                CPhysicianInfo physician = new CPhysicianInfo(_mStudyInfo.mGetSqlConnection());

                if (physician != null)
                {
                    physician.mbDoSqlSelectIndex(_mStudyInfo._mPhysisian_IX, CSqlDataTableRow.sGetMask((UInt16)DPhysicianInfoVars.Label));
                    s = physician._mLabel;
                }
            }
            hgfhdga.Text = s; // "Dr. John Smithsonian";
            //labelPhysHeader.Text = "PHYSICIAN:";
 */
            CPhysicianInfo.sFillPrintPhysicianName(_mbAnonymize, _mStudyInfo, false, labelPhysicianName, labelPhysicianTel, true);
            
            string s = "";
            if( _mDeviceInfo != null && _mStudyInfo._mStudyRecorder_IX > 0 )
            {
                if( _mDeviceInfo.mbDoSqlSelectIndex(_mStudyInfo._mStudyRecorder_IX, _mDeviceInfo.mMaskValid))
                {
                    s = _mDeviceInfo._mDeviceSerialNr;
                }
            }
            labelSerialNr.Text = s;
            
                                                      //            labelSerialNrHeader.Text = "RECORDER SERIAL#:";

            labelStartDate.Text = bStudy ? CProgram.sDateToString(_mStudyInfo._mStudyStartDate) : ""; // "10/23/2016"; study start
                                                                                                      //            labelStartDateHeader.Text = "START DATE:";
            labelStudyNr.Text = bStudy ? CDvtmsData.sGetPrintStudyCode() +_mStudyInfo.mIndex_KEY.ToString() : ""; // "1212212";

            //labelStudyNrHeader.Text = "STUDY#:";
            labelPatIDHeader.Text = _mPatientInfo.mGetPatientSocIDText() + ":";
            labelPatID.Text = bPatient ? _mPatientInfo.mGetPatientSocIDValue(false)/* _mPatientID.mDecrypt()*/ : "";
            //labelPatientIDResult.Text = labelPatID.Text;

            labelPhone2.Text = bPatient ? _mPatientInfo._mPatientPhone2.mDecrypt() : ""; // "800-217-0520";
            labelPhone1.Text = bPatient ? _mPatientInfo._mPatientPhone1.mDecrypt() : ""; // "800-217-0520";
                                                                                         //labelPhoneHeader.Text = "PHONE:";
                                                                                         /*            labelZipCity.Text = bPatient ? (CProgram.sbGetImperial() ? _mPatientInfo._mPatientCity + ", " + _mPatientInfo._mPatientZip.mDecrypt()
                                                                                                                                 : _mPatientInfo._mPatientZip.mDecrypt() + "  " + _mPatientInfo._mPatientCity ) : ""; // "Texas TX 200111";
                                                                                                     labelAddress.Text = bPatient ? _mPatientInfo._mPatientAddress.mDecrypt() : ""; // "Alpha Street 112, Houston";
                                                                                                     labelCountry.Text = _mPatientInfo._mPatientState + ", " + _mPatientInfo._mPatientCountryCode;
                                                                                           */
            CPatientInfo.sFillPrintPatientAddress(_mbAnonymize, _mPatientInfo, labelAddress, labelZipCity, labelCountry);
            //labelAddressHeader.Text = "ADDRESS:";
            /* 
             int age = _mPatientInfo.mGetAgeYears();
            string ageGender = bPatient && age >= 0 ? age.ToString() : "";
            if (bPatient && _mPatientInfo._mPatientGender_IX > 0)
            {
                ageGender += " " + CPatientInfo.sGetGenderString((DGender)_mPatientInfo._mPatientGender_IX);
            }
            labelAgeGender.Text = ageGender;  //"41, male";

            labelDateOfBirth.Text = bPatient && age >= 0 ? CProgram.sDateToString(_mPatientInfo._mPatientDateOfBirth.mDecryptToDate()) : ""; //  "06/08/1974";
            */
            CPatientInfo.sFillPrintPatientDOB(_mbAnonymize, _mPatientInfo, null, labelDateOfBirth, labelAgeGender);

            //            labelDOBheader.Text = "DATE OF BIRTH:";

/*            string fullName = bPatient ? _mPatientInfo.mGetFullName() :"";
            int pos = fullName.IndexOf(',');
            string firstName = "", lastName = fullName; ;
            if (pos > 0)
            {
                lastName = fullName.Substring(0, pos);
                firstName = fullName.Substring(pos + 1);
                if (firstName.StartsWith(", "))
                {
                    firstName = firstName.Substring(2);
                }
            }
            labelPatientFirstName.Text = firstName;  //"Rutger Alexander";
            labelPatientLastName.Text = lastName;  //"Brest van Kempen";
*/                                                   //            labelPatLastName.Text = fullName;
            CPatientInfo.sFillPrintPatientName(_mbAnonymize, _mPatientInfo, null, labelPatientLastName, labelPatientFirstName);

            //labelPatientNameHeader.Text = "PATIENT NAME:";
            dt = DateTime.Now;  
            //labelSingleEventReportTime.Text = CProgram.sTimeToString(dt);// "10:15 AM";
            labelSingleEventReportDate.Text = CProgram.sDateTimeToString(dt);// "11/12/2016";

            s = "";
            if (bStudy)
            {
                if (CDvtmsData._sEnumListStudyProcedures != null)
                {
                    s = CDvtmsData._sEnumListStudyProcedures.mFillString(_mStudyInfo._mStudyProcCodeSet, true, " ", true, "\r\n");
                }
            }
         }
        private void mInitStats()
        {
            {
                try
                {

                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Set MCT Print Form stats error", ex);
                }
            }
        }
        void mInitFooter()
        {
            //            labelReportSignDate.Text = "_________"; //CProgram.sDateToString( DateTime.Now );// "08/23/2016";
            //'labelDateReportSign.Text = "Date:";
            //labelPhysSignLine.Text = "_______________";
            //labelPhysSign.Text = "Physician signature:";
            labelPhysPrintName.Text = "_______________"; // "J. Smithsonian";
                                                         //labelPhysNameReportPrint.Text = "Physician\'s name:";
                                                         //label7.Text = ">R";
                                                         //label3.Text = "L<";
            string date = CProgram.sPrintDateTimeVersion(DateTime.Now);

            labelPrintDate1Bottom.Text = date;

            CDvtmsData.sFillPrintPhysicianLabels(CProgram.sGetProgUserIX(), CProgram.sGetProgUserInitials(), "MCT", _mbQCd,
            labelPhysNameReportPrint, labelPhysPrintName,
           labelPhysSign, labelPhysSignLine,
          labelDateReportSign, labelReportSignDate);

        }



        [System.Runtime.InteropServices.DllImportAttribute("gdi32.dll")]
        private static extern int BitBlt(
     IntPtr hdcDest,     // handle to destination DC (device context)
     int nXDest,         // x-coord of destination upper-left corner
     int nYDest,         // y-coord of destination upper-left corner
     int nWidth,         // width of destination rectangle
     int nHeight,        // height of destination rectangle
     IntPtr hdcSrc,      // handle to source DC
     int nXSrc,          // x-coordinate of source upper-left corner
     int nYSrc,          // y-coordinate of source upper-left corner
     System.Int32 dwRop  // raster operation code
     );

        [System.Runtime.InteropServices.DllImportAttribute("gdi32.dll")]
        public static extern IntPtr SelectObject(IntPtr hdc, IntPtr obj);

        [System.Runtime.InteropServices.DllImportAttribute("gdi32.dll")]
        public static extern void DeleteObject(IntPtr obj);

        void mPanelToClipBoard1(Control APanel)
        {
            CProgram.sPromptError(true, "Print", "Do not use this function");
            try
            {
                Bitmap bmp = new Bitmap(APanel.Width, APanel.Height);

                using (Graphics G = Graphics.FromImage(bmp))
                {
                    G.Clear(Color.Cyan);
                }

                Graphics g1 = APanel.CreateGraphics();
                Image MyImage = new Bitmap(APanel.ClientRectangle.Width, APanel.ClientRectangle.Height, g1);
                Graphics g2 = Graphics.FromImage(bmp);
                IntPtr dc1 = g1.GetHdc();
                IntPtr dc2 = g2.GetHdc();
                BitBlt(dc2, 0, 0, ClientRectangle.Width, ClientRectangle.Height, dc1, 0, 0, 13369376);
                g1.ReleaseHdc(dc1);
                g2.ReleaseHdc(dc2);


               Clipboard.SetImage(bmp);

            }
            catch (Exception /*ex*/)
            {
                //CProgram.sLogException("Panel to Clipboard error", ex);

            }
        }
/*
     Bitmap mPanelToImage(Control APanel, bool AbInvert, bool AbToClipBoard)
        {
            Bitmap bitmap = null;
            try
            {
                bitmap = new Bitmap(APanel.Width, APanel.Height);

                using (Graphics G = Graphics.FromImage(bitmap))
                {
                    G.Clear(Color.White);           // needed for init
                }

                if (AbInvert) InvertZOrderOfControls(APanel.Controls);
                APanel.DrawToBitmap(bitmap, new System.Drawing.Rectangle(0, 0, APanel.Width, APanel.Height));
                if (AbInvert) InvertZOrderOfControls(APanel.Controls);

                if (AbToClipBoard)
                {
                    Clipboard.SetImage(bitmap);
                }
            }
            catch (Exception /*ex* /)
            {
                //                CProgram.sLogException("Image to Clipboard error", ex);
                bitmap = null;
            }
            return bitmap;
        }
*/


        private bool mbPrintImage()
        {
            bool bOk = false;
            string fullName = "";
            bool bTestRemove = false;

            if (mPrintBitmapArray != null && mNrPages > 0)
            {
                try
                {
                    toolStripGenPages.Text = "PDF generarating...";
                    CPdfDocRec pdfDocRec = new CPdfDocRec(CDvtmsData.sGetDBaseConnection());
                    UInt32 studyIX = _mStudyInfo == null ? 0 : _mStudyInfo.mIndex_KEY;
//                    UInt16 studySubNr = 0; // _mStudyReport._mReportNr;
                    UInt32 patientIX = _mPatientInfo == null ? 0 : _mPatientInfo.mIndex_KEY;
                    string reportType = CStudyInfo.sGetStudyPdfCode(DStudyPdfCode.MCT, _mbQCd); ; // _mStudyReport.mGetMaskReportCode(_mStudyReport._mReportSectionMask);
                    UInt32 refID = 0; // _mStudyReport.mIndex_KEY;
                    string refLetter = "M";
                    PdfDocument pdfDocument;
                    PageOrientation pageOrientation = PageOrientation.Portrait;
                    string extraName = _mbQCd ? "QC" : "";

                    bool bInitials = _mCreadedByInitials != null && _mCreadedByInitials.Length > 0;

                    if (false == bInitials)
                    {
                        if (CProgram.sbReqLabel("Print pdf", "User Initials", ref _mCreadedByInitials, "", false))
                        {
                            bInitials = _mCreadedByInitials != null && _mCreadedByInitials.Length > 0;
                        }
                    }
                    CProgram.sbLogMemSize("printing MCT print S" + _mStudyInfo.mIndex_KEY.ToString() + " report " + _mStudyReportNr.ToString() + " " + mNrPages.ToString() + " pages");

                    if (bInitials && pdfDocRec != null
                        && null != (pdfDocument = pdfDocRec.mbNewDocument(_mCreadedByInitials, studyIX, _mStudyReportNr, patientIX, reportType, refLetter, refID, extraName, _mTimeFrameFrom, _mTimeFrameTo)))
                    {
                        Bitmap bitmap;
                        bOk = true;
                        for (int i = 1; i <= mNrPages; ++i)
                        {
                            bitmap = mPrintBitmapArray[i];
                            bOk &= pdfDocRec.mbAddImagePage(pageOrientation, bitmap);
                        }
                        if (bOk == false)
                        {
                        }
                        if(_mbAnonymize)
                        {
                            _mStudyReportNr = 0;
                            bOk = true;
                        }
                        else if( _mStudyInfo.mbSetNextReportNr(_mStudyReportNr))
                        {
                           bOk = true;                            
                        }
                        else
                        {
                            bOk = false;
                        }

                        if (bOk)
                        {
                            toolStripGenPages.Text = "PDF saving...";
                            bTestRemove = true;
                            bOk = pdfDocRec.mbSaveToStudyPdf(out fullName, false == _mbAnonymize);
                            if (bOk)
                            {
                                CProgram.sbLogMemSize("saved MCT print S" + _mStudyInfo.mIndex_KEY.ToString() + " report " 
                                    + _mStudyReportNr.ToString() + " " + mNrPages.ToString() + " pages " + Path.GetFileName( fullName));

                                toolStripGenPages.Text = "PDF opening...";
                                _mReportIX = pdfDocRec.mIndex_KEY;
                                Process.Start(fullName);
                                Close();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("PdfPrint MCT failed", ex);
                    bTestRemove = true;
                }
                if (bTestRemove && _mReportIX == 0)
                {
                    try
                    {
                        bTestRemove = fullName.Length == 0 || false == File.Exists(fullName);
                    }
                    catch (Exception ex)
                    {
                        bTestRemove = false;
                    }
                    if (bTestRemove)
                    {
                        _mStudyInfo.mbReverseNextReportNr(_mStudyReportNr); // something failed revert to previousNr Reports
                    }
                }

            }
            return bOk;
        }

        private bool mbPrintImageOld()
        {
            bool bOk = false;
            try
            {
                PrintDocument pd = new PrintDocument();

                System.Drawing.Printing.Margins oldMargins = pd.DefaultPageSettings.Margins;
                System.Drawing.Printing.Margins margins = new System.Drawing.Printing.Margins(50, 50, 50, 50);
                pd.DefaultPageSettings.Margins = margins;

                mCurrentPrintPage = 1;
                pd.PrintPage += mPrintPage;

                PrintDialog printDialog1 = new PrintDialog();

                if (printDialog1 != null)
                {
                    printDialog1.Document = pd;
                    DialogResult result = printDialog1.ShowDialog(this);
                    if (result == DialogResult.OK)
                    {
                        PrintPreviewDialog printPreviewDialog = new PrintPreviewDialog();

                        if (printPreviewDialog != null)
                        {
                            printPreviewDialog.ClientSize = new System.Drawing.Size(600, 900);
                            printPreviewDialog.Location = new System.Drawing.Point(10, 10);
                            printPreviewDialog.Name = "PrintPreviewDialog1";

                            // Associate the event-handling method with the  
                            // document's PrintPage event. 
                            // Set the minimum size the dialog can be resized to. 
                            printPreviewDialog.MinimumSize = new System.Drawing.Size(375, 250);

                            // Set the UseAntiAlias property to true, which will allow the  
                            // operating system to smooth fonts. 
                            printPreviewDialog.UseAntiAlias = true;

                            printPreviewDialog.Document = pd;
                            printPreviewDialog.StartPosition = FormStartPosition.CenterParent;


                            // Call the ShowDialog method. This will trigger the document's
                            //  PrintPage event.
                            printPreviewDialog.ShowDialog( this );
                        }
                        bOk = true;
                    }
                }

                pd.Dispose();
            }
            catch (Exception)
            {

            }
            return bOk;
        }

        //The Print Event handeler
        private void mPrintPage(object o, PrintPageEventArgs e)
        {
            try
            {
              
                Bitmap bitmap = mCurrentPrintPage <= mNrPages ? mPrintBitmapArray[ mCurrentPrintPage ] : null;  

                if (bitmap != null && bitmap.Width > 0 && bitmap.Height > 0)
                {
                    //Adjust the size of the image to the page to print the full image without loosing any part of it
                    Rectangle m = e.MarginBounds;

                    if ((double)bitmap.Width / (double)bitmap.Height > (double)m.Width / (double)m.Height) // image is wider
                    {
                        m.Height = (int)((double)bitmap.Height / (double)bitmap.Width * (double)m.Width);
                    }
                    else
                    {
                        m.Width = (int)((double)bitmap.Width / (double)bitmap.Height * (double)m.Height);
                    }
                    e.Graphics.DrawImage(bitmap, m);
                }
                if( ++mCurrentPrintPage <= mNrPages)
                {
                    e.HasMorePages = true;
                }
                else
                {
                    e.HasMorePages = false;
                    mCurrentPrintPage = 1;  // set ready for the print by print preview
                }

            }
            catch (Exception)
            {

            }
        }
/*        public static void InvertZOrderOfControls(Control.ControlCollection ControlList)
        {
            // do not process empty control list
            if (ControlList.Count == 0)
                return;
            // only re-order if list is writable
            if (!ControlList.IsReadOnly)
            {
                SortedList<int, Control> sortedChildControls = new SortedList<int, Control>();
                // find all none docked controls and sort in to list
                foreach (Control ctrlChild in ControlList)
                {
                    if (ctrlChild.Dock == DockStyle.None)
                        sortedChildControls.Add(ControlList.GetChildIndex(ctrlChild), ctrlChild);
                }
                // re-order the controls in the parent by swapping z-order of first 
                // and last controls in the list and moving towards the center
                for (int i = 0; i < sortedChildControls.Count / 2; i++)
                {
                    Control ctrlChild1 = sortedChildControls.Values[i];
                    Control ctrlChild2 = sortedChildControls.Values[sortedChildControls.Count - 1 - i];
                    int zOrder1 = ControlList.GetChildIndex(ctrlChild1);
                    int zOrder2 = ControlList.GetChildIndex(ctrlChild2);
                    ControlList.SetChildIndex(ctrlChild1, zOrder2);
                    ControlList.SetChildIndex(ctrlChild2, zOrder1);
                }
            }
            // try to invert the z-order of child controls
            foreach (Control ctrlChild in ControlList)
            {
                try { InvertZOrderOfControls(ctrlChild.Controls); }
                catch { }
            }
        }
*/
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
        }

        private void mCapture3Pages()
        {
            if (mPrintBitmapArray != null && mPrintBitmapArray.Length > 1)
            {
                mPrintBitmapArray[1] = CPdfDocRec.sPanelToImage(panelPrintArea1, false, true);
            }
        }
 
        private void mCapturePageN( UInt16 APageNr)
        {
            if (mPrintBitmapArray != null && mPrintBitmapArray.Length > 3)
            {
//                mPrintBitmapArray[APageNr] = mPanelToImage(panelPrintArea3, false, false);
                // todo multiple pages
            }
        }

        private void toolStripButton1_Click_1(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            try
            {
                mCapture3Pages();
                mbPrintImage();
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Print Analysis", ex);
            }
        }

        private void panel64_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel52_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label39_Click(object sender, EventArgs e)
        {

        }

        private void label57_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label104_Click(object sender, EventArgs e)
        {

        }

        private void label102_Click(object sender, EventArgs e)
        {

        }

        private void panel113_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBoxBaselineReferenceECGStrip_Click(object sender, EventArgs e)
        {

        }

        private void panel62_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel51_Paint(object sender, PaintEventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;

            try
            {
                if(mbFirstPage)
                {
                    mCapture3Pages();
                    mbFirstPage = false;
                }
                else
                {
                    toolStripGenPages.Text = "Page " + mCurrentPrintPage.ToString() + " / " + mNrPages.ToString();

//                    mCapturePageN( mCurrentPrintPage);
                }
                
/*                if( mCurrentPrintPage <= mNrPages )
                {
                    ++mCurrentPrintPage;
                    timer1.Interval = 1000;
                    timer1.Enabled = true;
                }
                else
                {
  */                  timer1.Enabled = false;
                    toolStripGenPages.Text = "Done.";

                    if (mbPrintImage())
                    {
                        Close();
                    }
 //               }
            }
            catch ( Exception ex)
            {
                CProgram.sLogException("Print MCT", ex);
            }
        }

        private void CPrintMctForm_Shown(object sender, EventArgs e)
        {
            BringToFront();
            timer1.Enabled = true;
            toolStrip1.Focus();
            //            mPanelToClipBoard1(panelPrintArea);
            Application.DoEvents();
        }

        private void labelRatemeasureSI_Click(object sender, EventArgs e)
        {

        }

        private void CPrintMctForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            timer1.Enabled = false;
        }

        private void toolStripClipboard_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;

            if (mPrintBitmapArray != null && mPrintBitmapArray.Length > 3)
            {
                Clipboard.SetImage(mPrintBitmapArray[1]);
            }
        }

        /*        private bool mbEnterLable( Label ArLabel, string AName )
                {
                    bool bOk = false;

                    if( ArLabel != null )
                    {
                        string s = ArLabel.Text;

                            if( CProgram.sbReqLabel( "Enter MCT header", "Patient Last name", ref s, "" ))
                        {
                            ArLabel.Text = s;
                            bOk = true;

                        }
                    }
                    return bOk;
                }
        */
        private void toolStripEnterData_Click(object sender, EventArgs e)
        {

        }

        private void dataGridViewPrevThreeTX_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void labelPhysSignLine_Click(object sender, EventArgs e)
        {

        }

        private void labelSerialNrHeader_Click(object sender, EventArgs e)
        {

        }

        private void labelPhysPrintName_Click(object sender, EventArgs e)
        {

        }

        private void label53_Click(object sender, EventArgs e)
        {

        }

        private void label93_Click(object sender, EventArgs e)
        {

        }

        private void label78_Click(object sender, EventArgs e)
        {

        }

        private void label19_Click(object sender, EventArgs e)
        {

        }

        private void toolStripClipboard2_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;

            if (mPrintBitmapArray != null && mPrintBitmapArray.Length > 3)
            {
                Clipboard.SetImage( mPrintBitmapArray[2]);
            }
        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void labelPatientNameHeader_Click(object sender, EventArgs e)
        {

        }

        private void label46_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void labelCardiacEventReportNr_Click(object sender, EventArgs e)
        {

        }

        private void panel68_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label72_Click(object sender, EventArgs e)
        {

        }

        private void panel122_Paint(object sender, PaintEventArgs e)
        {

        }

        private void toolStripButton1_Click_2(object sender, EventArgs e)
        {
            if (mPrintBitmapArray != null && mPrintBitmapArray.Length > 3)
            {
                Clipboard.SetImage(mPrintBitmapArray[3]);
            }
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            if (mPrintBitmapArray != null && mPrintBitmapArray.Length > 4)
            {
                Clipboard.SetImage(mPrintBitmapArray[4]);
            }

        }

        private void panel77_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panelTablePrevious3Trans_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label115_Click(object sender, EventArgs e)
        {

        }

        private void labelPage2PatLastName_Click(object sender, EventArgs e)
        {

        }

        private void labelDiagnosis_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void chartSummaryBar_Click(object sender, EventArgs e)
        {

        }

        private void CPrintMctForm_Load(object sender, EventArgs e)
        {

        }

        private void panelPrintArea1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label43_Click(object sender, EventArgs e)
        {

        }

        private void label21_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void panelPrintHeader_Paint(object sender, PaintEventArgs e)
        {

        }

        private void labelZipCity_Click(object sender, EventArgs e)
        {

        }

        private void labelCountry_Click(object sender, EventArgs e)
        {

        }

        private void labelAddress_Click(object sender, EventArgs e)
        {

        }

        private void labelGraphVtach_Click(object sender, EventArgs e)
        {

        }
    }
}
