﻿using Event_Base;
using EventBoard.EntryForms;
using EventBoard.Event_Base;
using EventboardEntryForms;
using PdfSharp;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using Program_Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventBoard
{
    public partial class CPrintAnalys12Form : Form
    {
        UInt32 mRecordIndex;
//        UInt32 mAnalysisIndex;
        UInt16 mAnalysisNr;
        CRecordMit mRecordDb;
        CRecordMit mRecordFile;
        CRecAnalysis mRecAnalysis;
        CRecAnalysisFile mRecAnalysisFile;
        string mRecordFilePath;
        bool _mbAnonymize;


        Bitmap mPrint1Bitmap;
        Bitmap mPrint2Bitmap;

        UInt16 mNrPages = 0;
        UInt16 mNrSamples = 0;
        UInt16 mCurrentPrintPage = 0;
        const UInt16 _cNrSamplesFirstPage = 2;
        const UInt16 _cNrSamplesPerPage = 4;

        public CDeviceInfo _mDeviceInfo = null;
        public CStudyInfo _mStudyInfo = null;
        public CPatientInfo _mPatientInfo = null;

        public string _mCreadedByInitials;
        public UInt16 _mStudyReportNr = 0;
        public UInt32 _mReportIX = 0;

        private bool _mbHideReportNr = false;
        private bool _mbBlackWhite = false;

        float mChannelsAmplitudeRange = 0.0F;
        float mChannelsAmplitudeMin = 0.0F;
        float mChannelsAmplitudeMax = 0.0F;
        bool mbDoCalcAmplitude = true;
        bool _mbQCd = false;

        public CPrintAnalys12Form(FormAnalyze AAnalyzeForm, bool AbSmall, string ACreadedByInitials, bool AbAnonymize, 
                        bool AbPrintBlackWhite, bool AbQCd)
        {
            try
            {
                mRecordIndex = AAnalyzeForm._mRecordIndex;
                //            mAnalysisIndex = AAnalyzeForm.mAna;
                mAnalysisNr = AAnalyzeForm._mAnalysisSeqNr;
                mRecordDb = AAnalyzeForm._mRecordDb;
                mRecordFile = AAnalyzeForm._mRecordFile;
                mRecordFilePath = AAnalyzeForm._mRecordFilePath;
                mRecAnalysis = AAnalyzeForm._mRecAnalysis;
                mRecAnalysisFile = AAnalyzeForm._mRecAnalysisFile;
                _mbAnonymize = AbAnonymize;
                _mbBlackWhite = AbPrintBlackWhite;

                _mCreadedByInitials = ACreadedByInitials;

                _mDeviceInfo = AAnalyzeForm._mDeviceInfo;
                _mStudyInfo = AAnalyzeForm._mStudyInfo;
                _mPatientInfo = AAnalyzeForm._mPatientInfo;
                _mbQCd = AbQCd;

                InitializeComponent();

                _mbHideReportNr = Properties.Settings.Default.HideReportNr;

                if (AbSmall)
                {
                    Width = 333;
                    Height = 85;
                }

                toolStrip1.Focus();

                string reportStr = "";

                _mStudyReportNr = 0;

                if (false == _mbAnonymize && _mStudyInfo != null)
                {
                    if (_mStudyInfo.mbGetNextReportNr(out _mStudyReportNr))
                    {
                        if (false == _mbHideReportNr)
                        {
                            reportStr = _mStudyReportNr.ToString();
                        }
                    }
                }
                CProgram.sbLogMemSize("opening Analysis12 print R#" + mRecordDb.mIndex_KEY.ToString() + " S"
                    + mRecordDb.mStudy_IX.ToString() + "." + mRecordDb.mSeqNrInStudy.ToString() + " report "
+ _mStudyReportNr.ToString() );

                if (_mStudyReportNr == 0)
                {
                    labelReportText2.Text = "";
                }
                labelReportNr.Text = reportStr;
                labelPage2ReportNr.Text = reportStr;

                labelPhysicianText.Text = _mbQCd ? "QC Physician:" : "Physician:";
                labelSignatureText.Text = _mbQCd ? "QC Signature:" : "Signature:";
                labelFindingsText.Text = _mbQCd ? "QC Findings:" : "Findings:";

                mSetFormValues();

                BringToFront();

            }
            catch ( Exception ex)
            {
                CProgram.sLogException("Init print Analysis", ex);
            }
        }

        public void mSetFormValues()
        {
            try
            {
                Image img = CDvtmsData.sGetPrintCenterImage(_mbBlackWhite);
                pictureBoxCenter1.Image = img;
                pictureBoxCenter2.Image = img;
                string s = CDvtmsData.sGetPrintCenterLine1();

                labelCenter1.Text = s;
                labelCenter1p2.Text = s;

                s = CDvtmsData.sGetPrintCenterLine2();
                labelCenter2.Text = s;
                labelCenter2p2.Text = s;

                string userTitle = ""; // Properties.Settings.Default.PrintReportEvent;
                if( userTitle == null || userTitle.Length < 1 )
                {
                    userTitle = "12 Lead Cardiac Event Report";
                }
                if( _mbHideReportNr == false )
                {
                  userTitle += labelReportNr.Text;
                }

//                labelReportNr.Text = userTitle;
                if (_mbHideReportNr)
                {
                    labelReportText2.Text = "";
                }

                if (mRecordDb == null || mRecordFile == null || mRecAnalysis == null || mRecAnalysisFile == null)
                {
                    CProgram.sPromptError(false, "PrintAnalysis", "Form not initialized");
                    Close();
                }
                else
                {
                    mNrSamples = mRecAnalysisFile.mCountActiveStrips();
//                    mNrPages = (UInt16)(mNrSamples <= _cNrSamplesFirstPage ? 1 : 2 + ( mNrSamples - _cNrSamplesFirstPage - 1 ) / _cNrSamplesPerPage);
                    mNrPages = 1;
                    mCurrentPrintPage = 1;
                    //test mNrPages = 2;
                    if (mNrPages > 2) mNrPages = 2; // for now maximum strips = 6 => 2 pages
                    labelPagexOfX.Text = mNrPages.ToString();
                    labelPage1ofx.Text = mNrPages.ToString();

                    mInitHeader();
                    mFillResults();
                    mInitCurrent();
                    if (mRecordFile.mNrSignals <= 6)
                    {
                        splitContainer1.Panel2Collapsed = true;
                    }

                    mInitFullStrip(0);
                    mbDoCalcAmplitude = true;
                    mInitSample(0, 0, panelChannel1);
                    mInitSample(0, 1, panelChannel2);
                    mInitSample(0, 2, panelChannel3);
                    mInitSample(0, 3, panelChannel4);
                    mInitSample(0, 4, panelChannel5);
                    mInitSample(0, 5, panelChannel6);
                    mInitSample(0, 6, panelChannel7);
                    mInitSample(0, 7, panelChannel8);
                    mInitSample(0, 8, panelChannel9);
                    mInitSample(0, 9, panelChannel10);
                    mInitSample(0, 10, panelChannel11);
                    mInitSample(0, 11, panelChannel12);
                    mInitFooter();
                }

                toolStrip1.Focus();
            }
            catch ( Exception ex )
                {
                CProgram.sLogException("Set Analysis Print Form  error", ex);
            }
        }

        void mInitHeader()
        {
            DateTime dt;
            string s;
            bool bStudy = false == _mbAnonymize && _mStudyInfo != null && _mStudyInfo.mIndex_KEY > 0;
            bool bPatient = _mPatientInfo != null && _mPatientInfo.mIndex_KEY > 0;

            labelStudyNrHeader.Text = CDvtmsData.sGetPrintStudyLabel() + ":";

            s = "";
            if( bStudy && _mStudyInfo._mClient_IX > 0 )
            {
                CClientInfo client = new EventboardEntryForms.CClientInfo(_mStudyInfo.mGetSqlConnection());

                if( client != null)
                {
                    client.mbDoSqlSelectIndex(_mStudyInfo._mClient_IX, CSqlDataTableRow.sGetMask((UInt16)DClientInfoVars.Label));
                    s = client._mLabel;
                }
            }
            labelClient.Text = s; // "New York Hospital";
                                  //            labelClientHeader.Text = "CLIENT:";

            s = "";
            if( bStudy && _mStudyInfo._mRefPhysisian_IX > 0)
            {
                CPhysicianInfo physician = new CPhysicianInfo(_mStudyInfo.mGetSqlConnection());

                if( physician != null )
                {
                    physician.mbDoSqlSelectIndex(_mStudyInfo._mRefPhysisian_IX, CSqlDataTableRow.sGetMask((UInt16)DPhysicianInfoVars.Label));
                    s = physician._mLabel;
                }
            }
            //labelRefPhysician.Text = s; // "Dr. John Smithsonian";
            //labelRefPhysHeader.Text = "REFERRING PHYSICIAN:";
            s = "";
            if (bStudy && _mStudyInfo._mPhysisian_IX > 0)
            {
                CPhysicianInfo physician = new CPhysicianInfo(_mStudyInfo.mGetSqlConnection());

                if (physician != null)
                {
                    physician.mbDoSqlSelectIndex(_mStudyInfo._mPhysisian_IX, CSqlDataTableRow.sGetMask((UInt16)DPhysicianInfoVars.Label));
                    s = physician._mLabel;
                }
            }
            else
            {
                s = mRecordDb.mPhysicianName.mDecrypt();
            }
            labelPhysician.Text = s; // "Dr. John Smithsonian";
            //labelPhysHeader.Text = "PHYSICIAN:";
            labelSerialNr.Text = mRecordDb.mDeviceID; // "AA102044403";
                                                           //            labelSerialNrHeader.Text = "RECORDER SERIAL#:";

        //    labelStartDate.Text = bStudy ? CProgram.sDateToString( _mStudyInfo._mStudyStartDate ): ""; // "10/23/2016"; study start
                                           //            labelStartDateHeader.Text = "START DATE:";
            labelStudyNr.Text = bStudy ? CDvtmsData.sGetPrintStudyCode() + _mStudyInfo.mIndex_KEY.ToString() : "R#" + mRecordDb.mIndex_KEY.ToString(); // "1212212";

            string studyRecStr = CRecordMit.sGetPrintStudyRecord(mRecordDb);

            labelStudyNumberResPage1.Text = studyRecStr;
//            labelReportNr.Text = ;
            labelStudyNumberResPage2.Text = studyRecStr;
            //labelStudyNrHeader.Text = "STUDY#:";
            //labelPatID.Text = bPatient ? _mPatientInfo._mPatientID.mDecrypt() :  mRecordDb.mPatientID.mDecrypt(); // "12341234";
            //        labelPatientIDResult.Text = labelPatID.Text;
            labelPatientIDResult.Text = _mbAnonymize ? "" : (bPatient ? _mPatientInfo.mGetPatientSocIDValue(false) : mRecordDb.mPatientID.mDecrypt());
            //            labelPatIDHeader.Text = "PATIENT ID:";
            //         labelPhone2.Text = bPatient ? _mPatientInfo._mPatientPhone2.mDecrypt() : ""; // "800-217-0520";
            //       labelPhone1.Text = bPatient ? _mPatientInfo._mPatientPhone1.mDecrypt() : ""; // "800-217-0520";
            //labelPhoneHeader.Text = "PHONE:";
            //        labelZipCity.Text = bPatient ? _mPatientInfo._mPatientZip.mDecrypt() + "  " + _mPatientInfo._mPatientCity : ""; // "Texas TX 200111";
            //labelAddress.Text = bPatient ? _mPatientInfo._mPatientAddress.mDecrypt() : ""; // "Alpha Street 112, Houston";
            //labelAddressHeader.Text = "ADDRESS:";
            int ageYears = bPatient ? _mPatientInfo.mGetAgeYears() : mRecordDb.mGetAgeYears();// + ", "; // + 
            string ageGender = ageYears >= 0 ? " ("+ageYears.ToString() + ")" : "";// + ", "; // + 
            if( bPatient && _mPatientInfo._mPatientGender_IX > 0 )
            {
                ageGender += " " + CPatientInfo.sGetGenderString((DGender)_mPatientInfo._mPatientGender_IX).Substring(0,1);
            }
            // labelAgeGender.Text = ageGender;  //"41, male";

            labelDateOfBirth.Text = (_mbAnonymize || ageYears < 0 ? "" : CProgram.sDateToString(bPatient ? _mPatientInfo._mPatientDateOfBirth.mDecryptToDate() : mRecordDb.mGetBirthDate())) //  "06/08/1974";
                + ageGender;
            labelDateOfBirthResPage2.Text = labelDateOfBirth.Text;
            //            labelDOBheader.Text = "DATE OF BIRTH:";

            string fullName = _mbAnonymize ? "" : (bPatient ? _mPatientInfo.mGetFullName() : mRecordDb.mPatientTotalName.mDecrypt());
 /*           int pos = fullName.IndexOf(',');
            string firstName = "", lastName = fullName; ;
            if( pos > 0)
            {
                lastName = fullName.Substring(0, pos);
                firstName = fullName.Substring(pos + 1);
                if( firstName.StartsWith(", "))
                {
                    firstName = firstName.Substring(2);
                }
            }
            labelPatientFirstName.Text = firstName;  //"Rutger Alexander";
*/    //        labelPatientLastName.Text = lastName;  //"Brest van Kempen";
            labelPatLastName.Text = fullName;
            //labelPatientNameHeader.Text = "PATIENT NAME:";
            dt = mRecordDb.mGetDeviceTime(mRecordDb.mEventUTC);
     //       labelSingleEventReportTime.Text = CProgram.sTimeToString(dt);// "10:15 AM";
            labelSingleEventReportDate.Text = CProgram.sDateToString(dt);// "11/12/2016";
              
//            labelReportNr.Text = mRecordDb.mSeqNrInStudy.ToString() + ( mAnalysisNr > 1 ? "(" + mAnalysisNr + ")" : "" ); // "175";
            //labelCardiacEventReportNr.Text = "Cardiac Event Report #";
            //label1.Text = "800-217-0520";
            //labelCenter1.Text = "www.cardiolabs.com";

        }

        /*       bool mbGetBaseLine()
               {
                   bool bOk = false;

                   if (_mBaseLineRecordDb != null && _mBaseLineRecordFile != null && _mBaseLineRecAnalysis != null && _mBaseLineRecAnalysisFile != null ) //&& _mBaseLineStrip != null
                        //&& String.Compare(_mBaseLineRecordDb.mPatientID.mDecrypt(), mRecordDb.mPatientID.mDecrypt(), true) == 0)
                   {
                       bOk = true; // already have it
                   }
                   else
                   {
                       // lets try and find it
                       CRecordMit baseLineRecordDb = null;
                       CRecordMit baseLineRecordFile = null;
                       CRecAnalysis baseLineRecAnalysis = null;
                       CRecAnalysisFile baseLineRecAnalysisFile = null;
                       CMeasureStrip baseLineStrip = null;



                       if (baseLineRecordDb != null && baseLineRecordFile != null && baseLineRecAnalysis != null && baseLineRecAnalysisFile != null && baseLineStrip != null) ;
       //                     && String.Compare(_mBaseLineRecordDb.mPatientID.mDecrypt(), mRecordDb.mPatientID.mDecrypt(), true) == 0)
                       {

                           _mBaseLineRecordDb = baseLineRecordDb;
                           _mBaseLineRecordFile = baseLineRecordFile;
                           _mBaseLineRecAnalysis = baseLineRecAnalysis;
                           _mBaseLineRecAnalysisFile = baseLineRecAnalysisFile;
                           _mBaseLineStrip = baseLineStrip;
                           bOk = true; // found it
                       }
                   }

                   return bOk;
               }
       */
        public string mGetEventNrString(UInt32 AEventNr, UInt16 AAnalysisNr, UInt32 ARecordIndex)
        {
            string s = AEventNr.ToString();

            if (AAnalysisNr > 1) s += " - " + AAnalysisNr.ToString();

            // s += " R#" + ARecordIndex.ToString(); // "175";
            return s;
        }

        void mFillResultsLineTime( Label ALabelMin, Label ALabelMax, Label ALabelAvg, DMeasure2Tag ATag)
        {
            string min = "";
            string max = "";
            string avg = "";

            CMeasureStat ms = mRecAnalysisFile.mFindStat(true, (UInt16)ATag);
            if (ms != null && ms._mbActive && ms._mCount > 0)
            {
                avg = mTimeStringUnit(ms._mMean);
                if(ms._mCount > 1)
                {
                    min = mTimeStringUnit(ms._mMin);
                    max = mTimeStringUnit(ms._mMax);
                }
            }

            if (ALabelMin != null) ALabelMin.Text = min;
            if (ALabelMax != null) ALabelMax.Text = max;
            if (ALabelAvg != null) ALabelAvg.Text = avg;

        }

        void mFillResults()
        {
            string  rhythm, qtcb, qtcf;

           rhythm = qtcb = qtcf = "";


            mFillResultsLineTime(labelResultRRmin, labelResultRRmax, labelResultRRavg, DMeasure2Tag.RRtime);
            mFillResultsLineTime(labelResultPRmin, labelResultPRmax, labelResultPRavg, DMeasure2Tag.PRtime);
            mFillResultsLineTime(labelResultQRSmin, labelResultQRSmax, labelResultQRSavg, DMeasure2Tag.QRStime);
            mFillResultsLineTime(labelResultQTmin, labelResultQTmax, labelResultQTavg, DMeasure2Tag.QTtime);

            if (_mStudyInfo != null && _mStudyInfo.mIndex_KEY > 0)
            {
                //  labelRecordNr.Text = "";
                // labelRecordNrText.Text = "";
            }
            else
            {
                //  labelRecordNr.Text = mRecordIndex.ToString();
            }

            rhythm = mRecAnalysis._mAnalysisClassSet.mbIsEmpty() || CDvtmsData._sEnumListFindingsClass == null ? "" 
                : "Findings classification: " + CDvtmsData._sEnumListFindingsClass.mGetLabel(mRecAnalysis._mAnalysisClassSet.mGetValue(0));


            if (CDvtmsData._sEnumListStudyRhythms != null)
            {
                rhythm +=  "\n" +  CDvtmsData._sEnumListStudyRhythms.mFillString(mRecAnalysis._mAnalysisRhythmSet, false, " ", true, "\n");
            }


            labelFindingsRemark.Text = mRecAnalysis._mAnalysisRemark; // findings maken ? "Atrial Fibrillation, with Occasional PVC\'s";
            //labelFindingsHeader.Text = "Findings:";
            //labelActivationType.Text = mFirstWord(mRecordDb.mEventTypeString);//            "Patient / Recorder";
            //labelActivationTypeHeader.Text = "Type of Activation:";
            labelActivity.Text = mRecAnalysis._mActivitiesRemark; // "Walking up the Stairs";
            //labelActivityHeader.Text = "Activity:";
            labelSymptoms.Text = mRecAnalysis._mSymptomsRemark;// "Palpitation";
                                                               //labelSymptomsHeader.Text = "Symptoms:";
            UInt16 nQt;
            float fRR, fQT, fQTcB, fQTcF;

            if (mRecAnalysisFile.mbFindQTc(out nQt, out fRR, out fQT, out fQTcB, out fQTcF))
            {
                qtcb = mTimeStringUnit(fQTcB);
                qtcf = mTimeStringUnit(fQTcF);
            }
            labelResultQTcBavg.Text = qtcb;
            labelResultQTcFavg.Text = qtcf;
            labelResultQTcBmin.Text = "";
            labelResultQTcFmin.Text = "";
            labelResultQTcBmax.Text = "";
            labelResultQTcFmax.Text = "";

            labelRythm.Text = rhythm;
        }

        /*        void mBaseLine()
                {
                    if (_mBaseLineRecordDb != null && _mBaseLineRecordFile != null && _mBaseLineRecAnalysis != null && _mBaseLineRecAnalysisFile != null ) //&& _mBaseLineStrip != null
        //                && String.Compare( _mBaseLineRecordDb.mPatientID.mDecrypt(), mRecordDb.mPatientID.mDecrypt(), true ) == 0 )
                        // test patient 
                    {

                        DateTime dtEvent = _mBaseLineRecordDb.mGetDeviceTime(mRecordDb.mEventUTC);

                        bool bAutoSizeA = false;
                        float cursorStart = -1.0F;
                        float cursorDuration = 0.0F;
                        float unitT = 0.2F;
                        float unitA = 0.5F;
                        int width = 10;// pictureBoxFullECGStrip.Width;
                        int height = 10;// pictureBoxFullECGStrip.Height;

                        CMeasureStrip mStrip = _mBaseLineRecAnalysisFile.mGetStrip(0); 


                        UInt16 channel = mStrip == null ? (UInt16)0 :  mStrip._mChannel;
                        float startT = mStrip == null ? 0 : mStrip._mStartTimeSec;
                        float durationT = mStrip == null ? _mBaseLineRecordFile.mGetSamplesTotalTime() : mStrip._mDurationSec;
                        float startA = mStrip == null ? -10 : mStrip._mStartA;
                        float endA = mStrip == null ? 10 : mStrip._mEndA;

                        //labelLeadID.Text = mStrip == null ? "" : _mBaseLineRecordDb.mGetChannelName(channel); ;

                        DateTime dtStart = _mBaseLineRecordDb.mStartRecUTC == DateTime.MinValue ? DateTime.MinValue : _mBaseLineRecordDb.mGetDeviceTime(_mBaseLineRecordDb.mStartRecUTC.AddSeconds(startT));
                        string stripLen = "     " + ((int)(durationT + 0.499)).ToString() + "  sec";


                        Image img = mMakeBaseChartImage(width, height, channel, bAutoSizeA,
                            startT, durationT, startA, endA, cursorStart, cursorDuration, unitT, unitA);

                        //pictureBoxFullECGStrip.Image = img;

                        labelBaseSweepSpeed.Text = _mBaseLineRecordFile.mGetShowSpeedString(unitT);
                        labelBaseAmplitudeSet.Text = _mBaseLineRecordFile.mGetShowAmplitudeString(unitA);
                        labelEventMiddle.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT * 0.5F)); // "10:15:15";
                        labelEventEnd.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT)); // "10:15:20";
                        labelEventStart.Text = CProgram.sTimeToString(dtStart) + stripLen;//  "10:15:10";

                        //                labelTechnicianMeasurement.Text = "";// "SHJ";
                        //labelTechMeasure.Text = "Technician:";
                        //labelQTmeasureSI.Text = "(s)";


         //               mInitStripHr(_mBaseLineRecAnalysisFile, 0, labelMinRateHeader, labelMinRate, labelMinRateSI, labelMeanRateHeader, labelMeanRate, labelMeanRateSI,
           //                                         labelMaxRateHeader, labelMaxRate, labelMaxRateSI);
         //               mInitStripTimeTag(_mBaseLineRecAnalysisFile, 0, DMeasure2Tag.PRtime, labelPRHeader, labelPR, labelPRSI);
          //              mInitStripTimeTag(_mBaseLineRecAnalysisFile, 0, DMeasure2Tag.QRStime, labelQRSHeader, labelQRS, labelQTSI);
           //             mInitStripTimeTag(_mBaseLineRecAnalysisFile, 0, DMeasure2Tag.QTtime, labelQTHeader, labelQT, labelQTSI);

                        string refBL = ""; // _mBaseLineRecordDb.mSeqNrInStudy.ToString();

                        refBL += _mBaseLineRecAnalysis._mAnalysisRemark;
                        //labelFindingsRythmBL.Text = refBL;
                        /*
                        CMeasureStat ms;
                        string min, mean, max;
                                        min = mean = max = "";
                                        ms = _mBaseLineRecAnalysisFile.mFindStat(true, (UInt16)DMeasure2Tag.RRtime);
                                        if (ms != null && ms._mCount > 0)
                                        {
                                            mean = mBpmString(ms._mMean);
                                            if (ms._mCount > 1)
                                            {
                                                min = mBpmString(ms._mMax);
                                                max = mBpmString(ms._mMin);
                                            }
                                        }
                                        labelMaxRate.Text = max; // "120";
                                        labelMeanRate.Text = mean; // "110";
                                        labelMinRate.Text = min; // "100";

                                        if( mean == "" )
                                        {
                                            labelMeanRateSI.Text = "";
                                            labelMeanRate.Text = ""; // "110";
                                        }
                                        if ( min == "" )
                                        {
                                            labelMaxRateSI.Text = "";
                                            labelMaxRate.Text = ""; // "120";
                                            labelMaxRateHeader.Text = "";
                                            labelMinRateSI.Text = "";
                                            labelMinRate.Text = ""; // "100";
                                            labelMinRateHeader.Text = "";
                                        }

                                        min = mean = max = "";
                                        ms = _mBaseLineRecAnalysisFile.mFindStat(true, (UInt16)DMeasure2Tag.QTtime);
                                        if (ms != null && ms._mCount > 0)
                                        {
                                            mean = mTimeString(ms._mMean);
                                            if (ms._mCount > 1)
                                            {
                                                min = mTimeString(ms._mMin);
                                                max = mTimeString(ms._mMax);
                                            }
                                        }

                                        labelQT.Text = mean;
                                        if (mean == "")
                                        {
                                            labelQTSI.Text = "";
                                            labelQTHeader.Text = ""; //  "0,465";
                                        }

                                        min = mean = max = "";
                                        ms = _mBaseLineRecAnalysisFile.mFindStat(true, (UInt16)DMeasure2Tag.QRStime);
                                        if (ms != null && ms._mCount > 0)
                                        {
                                            mean = mTimeString(ms._mMean);
                                            if (ms._mCount > 1)
                                            {
                                                min = mTimeString(ms._mMin);
                                                max = mTimeString(ms._mMax);
                                            }
                                        }
                                        labelQRS.Text = mean;
                                        if (mean == "")
                                        {
                                            labelQRSSI.Text = "";
                                            labelQRSHeader.Text = "";
                                        }
                                        min = mean = max = "";
                                        ms = _mBaseLineRecAnalysisFile.mFindStat(true, (UInt16)DMeasure2Tag.PRtime);
                                        if (ms != null && ms._mCount > 0)
                                        {
                                            mean = mTimeString(ms._mMean);
                                            if (ms._mCount > 1)
                                            {
                                                min = mTimeString(ms._mMin);
                                                max = mTimeString(ms._mMax);
                                            }
                                        }
                                        labelPR.Text = mean;
                                        if (mean == "")
                                        {
                                            labelPRSI.Text = "";
                                            labelPRHeader.Text = "";
                                        }
                        * /
                    }
                    else
                    {
                      //  panelECGBaselineSTrip.Visible = false;
                        //panelTimeBarBaselineECG.Visible = false;
                       // panelTimeHeadingsPerStrip.Visible = false;
                        //panelBaseLineHeader.Visible = false;
                       // panelECGSweepAmpIndicator.Visible = false;
        /*
                        labelBaseSweetSpeed.Text = "";
                        labelBaseAmplitudeSet.Text = "";
                        labelEventMiddle.Text = ""; // "10:15:15";
                        labelEventEnd.Text = ""; // "10:15:20";
                        labelEventStart.Text = "";//  "10:15:10";
                        labelQTSI.Text = "";
                        labelQT.Text = ""; //  "0,465";
                        labelQTHeader.Text = "";
                        labelQRSSI.Text = "";
                        labelQRS.Text = ""; // "0,113";
                        labelQRSHeader.Text = "";
                        labelPRSI.Text = "";
                        labelPR.Text = ""; // "0,20";
                        labelPRHeader.Text = "";
                        labelMaxRateSI.Text = "";
                        labelMaxRate.Text = ""; // "120";
                        labelMaxRateHeader.Text = "";
                        labelMeanRateSI.Text = "";
                        labelMeanRate.Text = ""; // "110";
                        labelMeanRateHeader.Text = "";
                        labelMinRateSI.Text = "";
                        labelMinRate.Text = ""; // "100";
                        labelMinRateHeader.Text  = "";
                        labelLeadID.Text = ""; // "LEAD I";
                        //labelBaseRefHeader.Text = "Baseline Reference";
          * /         }
                }
        */
        /*
         *        void mInitPrevious()
                {
                    // get findings list and log last 3
                }
        */
        public string mFirstWord(string ALine)
        {
            string s = "";
            int n = ALine == null ? 0 : ALine.Length;
            int l = 0;

            for (int i = 0; i < n; ++i)
            {
                char c = ALine[i];
                if (char.IsLetterOrDigit(c))
                {
                    s += c;
                    ++l;
                }
                else if (l > 0)
                {
                    break;
                }
            }
            return s;
        }
        public string mOneLine(string ALine)
        {
            string s = "";
            int n = ALine == null ? 0 : ALine.Length;
            int l = 0;

            for (int i = 0; i < n; ++i)
            {
                char c = ALine[i];
                if (char.IsLetterOrDigit(c))
                {
                    s += c;
                    ++l;
                }
                else if (c == '\r' || c == '\n')
                {
                    break;
                }
                else if (c >= ' ')
                {
                    s += c;
                    ++l;
                }
                else if (l > 0)
                {
                    s += " ";
                }
            }
            return s;
        }


        string mBpmString( float ATimeSec  )
        {
            float f = Math.Abs(ATimeSec);
            float bpm = f < 0.001 ? 0.0F : 1 / f;
            int i = (int)(bpm * 60 + 0.5F);
            return i.ToString(); // bpm.ToString("0.0");
        }
        string mTimeStringUnit(float ATimeSec)
        {
            int i = (int)(ATimeSec * 1000F + 0.5F);
            return i.ToString() + " ms";
        }
        string mTimeString(float ATimeSec)
        {
            int i = (int)(ATimeSec * 1000F + 0.5F);
            return i.ToString();
        }

        void mInitCurrent()
        {
            CMeasureStat ms;
            string min, mean, max;
            bool bMeasure = false;
            bool bMinMax = false;

 //           labelTechnicianMeasurement.Text = _mCreadedByInitials;// "SHJ";
   //         if (labelTechnicianMeasurement.Text=="")
            {
     //           labelTechMeasure.Text = "";
            }

            min = mean = max = "";
            ms = mRecAnalysisFile.mFindStat(true, (UInt16)DMeasure2Tag.RRtime);
            if( ms != null && ms._mbActive && ms._mCount > 0 )
            {
                bMeasure = true;
                mean = mBpmString(ms._mMean);
                if( ms._mCount > 1 )
                {
                    bMinMax = true;
                    min = mBpmString(ms._mMax);
                    max = mBpmString(ms._mMin);
                }
            }
  //          labelRateAVG.Text = mean;  //"76";
  //          labelRateMax.Text = max;  //"76";
  //          labelRateMin.Text = min;  //"76";
            if( mean == "" )
            {
 //               labelRateMeasure.Text = "";
 //               labelRatemeasureSI.Text = "";
            }

            min = mean = max = "";
            ms = mRecAnalysisFile.mFindStat(true, (UInt16)DMeasure2Tag.QTtime);
            if (ms != null && ms._mbActive && ms._mCount > 0)
            {
                bMeasure = true;
                mean = mTimeStringUnit(ms._mMean);
                if (ms._mCount > 1)
                {
                    bMinMax = true;
                    min = mTimeStringUnit(ms._mMin);
                    max = mTimeStringUnit(ms._mMax);
                }
            }
    //        labelQTAvg.Text = mean; // "76";
    //        labelQTMax.Text = max;  //"76";
    //        labelQTmin.Text = min;  //"76";
            if (mean == "")
            {
   //             labelQTmeasureSI.Text = "";
   //             labelQTMeasure.Text = "";
            }

            min = mean = max = "";
            ms = mRecAnalysisFile.mFindStat(true, (UInt16)DMeasure2Tag.QRStime);
            if (ms != null && ms._mbActive && ms._mCount > 0)
            {
                bMeasure = true;
                mean = mTimeStringUnit(ms._mMean);
                if (ms._mCount > 1)
                {
                    bMinMax = true;
                    min = mTimeStringUnit(ms._mMin);
                    max = mTimeStringUnit(ms._mMax);
                }
            }
  //          labelQRSAvg.Text = mean;  //"76";
    //        labelQRSMax.Text = max;  //"76";
   //         labelQRSmin.Text = min;  //"76";
            if (mean == "")
            {
   //             labelQRSmeasureSI.Text = "";
    //            labelQRSMeasure.Text = "";
            }

            min = mean = max = "";
            ms = mRecAnalysisFile.mFindStat(true, (UInt16)DMeasure2Tag.PRtime);
            if (ms != null && ms._mbActive && ms._mCount > 0)
            {
                bMeasure = true;
                mean = mTimeStringUnit(ms._mMean);
                if (ms._mCount > 1)
                {
                    bMinMax = true;
                    min = mTimeStringUnit(ms._mMin);
                    max = mTimeStringUnit(ms._mMax);
                }
            }
  //          labelPRAvg.Text = mean;  //"76";
     //       labelPRMax.Text = max;  //"76";
     //       labelPRmin.Text = min;  //"76";
            if (mean == "")
            {
    //            labelPRmeasureSI.Text = "";
          //      labelPRMeasure.Text = "";
            }

            if ( bMeasure == false )
            {
                //labelAVGMeasurement.Text = "";
                //labelMeasurement.Text = "";

            }
            if( bMinMax == false)
            {
                //labelMaxMeasurement.Text = "";
               // labelMinMeasurement.Text = "";
            }
            if( _mStudyInfo != null && _mStudyInfo.mIndex_KEY > 0)
            {
              //  labelRecordNr.Text = "";
               // labelRecordNrText.Text = "";
            }
            else
            {
              //  labelRecordNr.Text = mRecordIndex.ToString();
            }

            //labelFindingsClasification.Text = mRecAnalysis._mAnalysisClassSet.mbIsEmpty() || CDvtmsData._sEnumListFindingsClass == null ? "" 
            //    : CDvtmsData._sEnumListFindingsClass.mGetLabel(mRecAnalysis._mAnalysisClassSet.mGetValue(0));


            string s = "";

            if(CDvtmsData._sEnumListStudyRhythms != null)
            {
                s = CDvtmsData._sEnumListStudyRhythms.mFillString(mRecAnalysis._mAnalysisRhythmSet, false, " ", true, ", ");
            }
            //labelFindingsRythm.Text = s;


            labelFindingsRemark.Text = mRecAnalysis._mAnalysisRemark; // findings maken ? "Atrial Fibrillation, with Occasional PVC\'s";
            //labelFindingsHeader.Text = "Findings:";
            //labelActivationType.Text = mFirstWord(mRecordDb.mEventTypeString);//            "Patient / Recorder";
            //labelActivationTypeHeader.Text = "Type of Activation:";
            labelActivity.Text = mRecAnalysis._mActivitiesRemark; // "Walking up the Stairs";
            //labelActivityHeader.Text = "Activity:";
            labelSymptoms.Text = mRecAnalysis._mSymptomsRemark;// "Palpitation";
                                                               //labelSymptomsHeader.Text = "Symptoms:";
            s = "";
            if (_mStudyInfo != null)
            {
                if (CDvtmsData._sEnumListStudyProcedures != null)
                {
                    s = CDvtmsData._sEnumListStudyProcedures.mFillString(_mStudyInfo._mStudyProcCodeSet, true, " ", true, "\r\n");
                }
            }

       //      labelICDcode.Text = s; // "ICD10";
//            labelDiagnosisHeader.Text = "Diagnosis:";/
//            labelMeasurement.Text = "Measurements";
//            labelCurrentEvent.Text = "Current Event";
//            labelPreviousTransmissions.Text = "Previous Three Transmissions";

        }

        Image mMakeChartImage( int AWidth, int AHeight, float ARangeA, UInt16 AChannel, bool AbAutoSizeA,
            float AStartT, float ADurationT, float AStartA, float AEndA, 
            float ACursorStart, float ACursorDuration,
            float AUnitT, float AUnitA, float AMaxRange)
        {
            CStripChart chart = new CStripChart();
            float startA = AStartA;
            float endA = AEndA;
            Image img = null;

            mRecordFile.mInitChart(AChannel, chart, AStartT, ADurationT, AUnitA, AUnitT, AMaxRange);

            if( false == AbAutoSizeA)
            {
                startA = chart.mGetStartA();
                endA = chart.mGetEndA();
                img = mRecordFile.mCreateChartImage3FixedRangeA(AChannel, chart, AWidth, AHeight, ARangeA, ACursorStart, ACursorDuration, 
                    _mbBlackWhite ? DChartDrawTo.Print_BW : DChartDrawTo.Print_color);
            }
            else
            {
                img = mRecordFile.mCreateChartImage2(AChannel, chart, AWidth, AHeight, ACursorStart, ACursorDuration, _mbBlackWhite ? DChartDrawTo.Print_BW : DChartDrawTo.Print_color);
            }
  
            return img;
        }

        void mInitSample(UInt16 AStripNr, UInt16 AChannel, Panel APanel)
        {
            // zomm 1
            DateTime dtEvent = mRecordDb.mGetDeviceTime(mRecordDb.mEventUTC);
            CMeasureStrip ms = mRecAnalysisFile.mFindActiveStrip(AStripNr);
            int stripIndex = mRecAnalysisFile.mGetStripIndex(ms);
            UInt16 channel;
            float startT, durationT, startA, endA;

            bool bAutoSizeA = false;
            float cursorStart = -1.0F;
            float cursorDuration = 0.0F;
            float unitT = 0.2F;
            float unitA = 0.5F;
            int width = APanel.Width;
            int height = APanel.Height;

            DateTime dtStart;
            if ( ms == null )
            {
                channel = 0;
                startT = mRecordDb.mEventTimeSec - 6.0F;
                durationT = 12.0F;
                bAutoSizeA = true;
                startA = 10.0F;
                endA = -10.0F;
//                dtStart = mRecordDb.mStartRecUTC == DateTime.MinValue ? DateTime.MinValue : mRecordDb.mGetDeviceTime(mRecordDb.mStartRecUTC.AddSeconds(startT));
                dtStart = mRecordFile.mBaseUTC == DateTime.MinValue ? DateTime.MinValue : mRecordFile.mGetDeviceTime(mRecordFile.mBaseUTC.AddSeconds(startT));
            }
            else
            {
                channel = AChannel; // ms._mChannel;
                startT = ms._mStartTimeSec;
                durationT = ms._mDurationSec;
                startA = ms._mStartA;
                endA = ms._mEndA;
                ++stripIndex;

//                dtStart = mRecordDb.mStartRecUTC == DateTime.MinValue ? DateTime.MinValue : mRecordDb.mGetDeviceTime(mRecordDb.mStartRecUTC.AddSeconds(startT));
                dtStart = mRecordFile.mBaseUTC == DateTime.MinValue ? DateTime.MinValue : mRecordFile.mGetDeviceTime(mRecordFile.mBaseUTC.AddSeconds(startT));
                if (mbDoCalcAmplitude)
                {
                    float length = mRecordFile.mGetSamplesTotalTime();

 
                    mChannelsAmplitudeRange = 0.0F;
                    mChannelsAmplitudeMin = 1e6F;
                    mChannelsAmplitudeMax = -1e6F;
                    float min, max, range;
                    for (UInt16 i = 0; i < mRecordFile.mNrSignals; ++i)
                    {
                        if (mRecordFile.mbGetAmplitudeRange(i, 0, length, out min, out max, CRecordMit.sGetMaxAmplitudeRange()))
                        {
                            range = max - min;
                            if (range > mChannelsAmplitudeRange) mChannelsAmplitudeRange = range;
                            if (min < mChannelsAmplitudeMin) mChannelsAmplitudeMin = min;
                            if (max > mChannelsAmplitudeMax) mChannelsAmplitudeMax = max;
                        }
                    }
                    mbDoCalcAmplitude = false;

                    string stripLen = "     " + ((int)(durationT + 0.499)).ToString() + " sec";
                    labelTopStripTime.Text = CProgram.sTimeToString(dtStart) + stripLen; //"10:15:10";
                    labelTopStripFeed.Text = mRecordFile.mGetShowSpeedString(unitT);
                    labelTopStripAmpl.Text = mRecordFile.mGetShowAmplitudeString(unitA);
                }
            }

            Image img = mMakeChartImage(width, height, mChannelsAmplitudeRange, channel,  bAutoSizeA,
                startT, durationT, startA, endA, cursorStart, cursorDuration, unitT, unitA, CRecordMit.sGetMaxAmplitudeRange());

            APanel.BackgroundImage = img;
        }

        void mInitStripTimeTag(CRecAnalysisFile AAnalysisFile, CMeasureStrip AMs, DMeasure2Tag ATagID, Label ALabelMeanText, Label ALabelMeanValue, Label ALabelMeanUnit)
        {
            bool bMean = false;
            string mean = "";

            try
            {
                CMeasureStat pStat = AAnalysisFile == null || AMs == null ? null : AAnalysisFile.mCalcStripStat(AMs, ATagID);

                if (pStat != null && pStat._mCount > 0)
                {
                    mean = mTimeStringUnit(pStat._mMean);
                    bMean = true;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("mInitStripTimeTag", ex);
            }
            ALabelMeanValue.Text = mean;
            ALabelMeanValue.Visible = bMean;
            ALabelMeanText.Visible = bMean;
            ALabelMeanUnit.Visible = bMean;
        }

        void mInitFullStrip(UInt16 AStripNr)
        {
            DateTime dtEvent = mRecordDb.mGetDeviceTime(mRecordDb.mEventUTC);
            CMeasureStrip ms = mRecAnalysisFile.mFindActiveStrip(AStripNr);
            UInt16 channel;
            float startT, durationT, startA, endA;

            bool bAutoSizeA = true;
            float cursorStart = -1.0F;
            float cursorDuration = 0.0F;
            float unitT = 1.0F;
            float unitA = 0.5F;
            int width = panelFullStrip.Width;
            int height = panelFullStrip.Height;

            startT = 0.0F;
            durationT = mRecordFile.mGetSamplesTotalTime();
            startA = -10.0F;
            endA = 10.0F;

            if ( ms == null )
            {
                channel = 0;
                cursorStart = -1; //  mRecordDb.mEventTimeSec - 6.0F;
                cursorDuration = -1; // 12.0F;
            }
            else
            {
                channel = ms._mChannel;
                cursorStart = ms._mStartTimeSec;
                cursorDuration = ms._mDurationSec;
            }
            DateTime dtStart = mRecordFile.mBaseUTC == DateTime.MinValue ? DateTime.MinValue : mRecordFile.mGetDeviceTime(mRecordFile.mBaseUTC.AddSeconds(startT));
            // used to be db


            Image img = mMakeChartImage(width, height, 0.0F, channel, bAutoSizeA,
                startT, durationT, startA, endA, cursorStart, cursorDuration, unitT, unitA, CRecordMit.sGetMaxAmplitudeRange());

            panelFullStrip.BackgroundImage = img;
            string stripLen = "     " + ((int)(durationT+0.499)).ToString() + " sec";

            //labelSweepFullSample1.Text = mRecordFile.mGetShowSpeedString(unitT); ;// "25 mm/sec";
            //labelZoomFullSample1.Text = mRecordFile.mGetShowAmplitudeString(unitA); //"1 mm/mV";
            labelEventEnd.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT)); //"10:15:20";
            labelEventMiddle.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT * 0.5F)); //"10:15:15";
            labelEventStart.Text = CProgram.sTimeToString(dtStart) + " " + CProgram.sDateToString( dtStart ) + stripLen; //"10:15:10";
            labelSingleEventReportDate.Text = CProgram.sDateToString(dtStart);
            labelBaseSweepSpeed.Text = mRecordFile.mGetShowSpeedString(unitT);
            labelBaseAmplitudeSet.Text = mRecordFile.mGetShowAmplitudeString(unitA);

            labelSampleRate.Text = mRecordFile.mSampleFrequency.ToString() + " Sps";
            labelHPF.Text = "";
            labelNotch.Text = "";

        }
        void mInitFooter()
        {
            /*            //labelReportSignDate.Text = "_________"; //CProgram.sDateToString( DateTime.Now );// "08/23/2016";
                        //'labelDateReportSign.Text = "Date:";
                        //labelPhysSignLine.Text = "_______________";
                        //labelPhysSign.Text = "Physician signature:";
                        labelPhysPrintName.Text = ""; // "_______________"; // "J. Smithsonian";
                                                                          //labelPhysNameReportPrint.Text = "Physician\'s name:";
                                                                          //label7.Text = ">R";
                                                                          //label3.Text = "L<";
            */
            string date = CProgram.sPrintDateTimeVersion(DateTime.Now);

            labelPrintDate1Bottom.Text = date;
            labelPrintDate2Bottom.Text = date;

            CDvtmsData.sFillPrintPhysicianLabels(CProgram.sGetProgUserIX(), CProgram.sGetProgUserInitials(), "Analysis12", _mbQCd,
                        labelPhysicianText, labelPhysPrintName,
                       labelSignatureText, labelPhysPrintSign,
                      null, null);

        }
        /*        void mInitSample3()
                {
                    // zomm 1
                    CMeasureStrip ms = mRecAnalysisFile.mFindActiveStrip(2);

                    if (ms == null)
                    {
                        panelEventSample3.Visible = false;
                    }
                    else
                    {
                        PictureBox pictBox = pictureBoxSample3;
                        int stripIndex = mRecAnalysisFile.mGetStripIndex(ms);

                        bool bAutoSizeA = false;
                        float cursorStart = -1.0F;
                        float cursorDuration = 0.0F;
                        float unitT = 0.2F;
                        float unitA = 0.5F;
                        int width = pictBox.Width;
                        int height = pictBox.Height;
                        UInt16 channel = ms._mChannel;
                        float startT = ms._mStartTimeSec;
                        float durationT = ms._mDurationSec;
                        float startA = ms._mStartA;
                        float endA = ms._mEndA;
                        ++stripIndex;

                        DateTime dtEvent = mRecordDb.mGetDeviceTime(mRecordDb.mEventUTC);

                        // plot sample

                        DateTime dtStart = mRecordDb.mStartRecUTC == DateTime.MinValue ? DateTime.MinValue : mRecordDb.mGetDeviceTime(mRecordDb.mStartRecUTC.AddSeconds(startT));

                        string stripLen = "     " + ((int)(durationT + 0.499)).ToString() + "  sec";

                        Image img = mMakeChartImage(width, height, mChannelsAmplitudeRange, channel, bAutoSizeA,
                            startT, durationT, startA, endA, cursorStart, cursorDuration, unitT, unitA);

                        pictBox.Image = img;

                        labelSample3Sweep.Text = mRecordFile.mGetShowSpeedString(unitT); ;// "25 mm/sec";
                        labelSample3Ampl.Text = mRecordFile.mGetShowAmplitudeString(unitA); //"1 mm/mV";
                        labelSample3End.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT)); //"10:15:20";
                        labelSample3Mid.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT * 0.5F)); //"10:15:15";
                        labelSample3Start.Text = CProgram.sTimeToString(dtStart) + stripLen; //"10:15:10";
                        labelSample3Lead.Text = mRecordDb.mGetChannelName(channel); //"LEAD I";
                        labelSample3Time.Text = CProgram.sTimeToString(dtEvent); //"10:15:10";
                        labelSample3Date.Text = CProgram.sDateToString(dtEvent); //"11/23/2016";
                        labelSample3Nr.Text = stripIndex < 0 ? "" : stripIndex.ToString(); // record study nr  "1";
                        labelSample3EventNr.Text = mGetEventNrString(mRecordDb.mSeqNrInStudy, mAnalysisNr, mRecordIndex); 

                        // Plot full strip with cursor
                        pictBox = pictureBoxSample3Full;

                        bAutoSizeA = true;
                        cursorStart = -1.0F;
                        cursorDuration = 0.0F;
                        unitT = 1.0F;
                        unitA = 0.5F;
                        width = pictBox.Width;
                        height = pictBox.Height;

                        startT = 0.0F;
                        durationT = mRecordFile.mGetSamplesTotalTime();
                        startA = -10.0F;
                        endA = 10.0F;

                        cursorStart = ms._mStartTimeSec;
                        cursorDuration = ms._mDurationSec;
                        dtStart = mRecordDb.mGetDeviceTime(mRecordDb.mStartRecUTC.AddSeconds(startT));

                        img = mMakeChartImage(width, height, mChannelsAmplitudeRange, channel, bAutoSizeA,
                            startT, durationT, startA, endA, cursorStart, cursorDuration, unitT, unitA);

                        pictBox.Image = img;
                        stripLen = "     " + ((int)(durationT + 0.499)).ToString() + "  sec";

                        labelSample3SweepFull.Text = mRecordFile.mGetShowSpeedString(unitT); ;// "25 mm/sec";
                        labelSample3AmplFull.Text = mRecordFile.mGetShowAmplitudeString(unitA); //"1 mm/mV";
                        labelSample3EndFull.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT)); //"10:15:20";
                        labelSample3MidFull.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT * 0.5F)); //"10:15:15";
                        labelSample3StartFull.Text = CProgram.sTimeToString(dtStart) + stripLen; //"10:15:10";
                        labelSample3LeadFull.Text = mRecordDb.mGetChannelName(channel); //"LEAD I";
                    }
                    mInitStripHr(mRecAnalysisFile, 2, labelMinHrText3, labelMinHrValue3, labelMinHrUnit3, labelMeanHrText3, labelMeanHrValue3, labelMeanHrUnit3,
                                                     labelMaxHrText3, labelMaxHrValue3, labelMaxHrUnit3);
                    mInitStripTimeTag(mRecAnalysisFile, 2, DMeasure2Tag.PRtime, labelPrText3, labelPrValue3, labelPrUnit3);
                    mInitStripTimeTag(mRecAnalysisFile, 2, DMeasure2Tag.QRStime, labelQrsText3, labelQrsValue3, labelQrsUnit3);
                    mInitStripTimeTag(mRecAnalysisFile, 2, DMeasure2Tag.QTtime, labelQtText3, labelQtValue3, labelQtUnit3);
                }

                void mInitSample4()
                {
                    // zomm 1
                    CMeasureStrip ms = mRecAnalysisFile.mFindActiveStrip(3);

                    if (ms == null)
                    {
                        panelEventSample4.Visible = false;
                    }
                    else
                    {
                        PictureBox pictBox = pictureBoxSample4;
                        int stripIndex = mRecAnalysisFile.mGetStripIndex(ms);

                        bool bAutoSizeA = false;
                        float cursorStart = -1.0F;
                        float cursorDuration = 0.0F;
                        float unitT = 0.2F;
                        float unitA = 0.5F;
                        int width = pictBox.Width;
                        int height = pictBox.Height;
                        UInt16 channel = ms._mChannel;
                        float startT = ms._mStartTimeSec;
                        float durationT = ms._mDurationSec;
                        float startA = ms._mStartA;
                        float endA = ms._mEndA;
                        ++stripIndex;

                        DateTime dtEvent = mRecordDb.mGetDeviceTime(mRecordDb.mEventUTC);

                        // plot sample

                        DateTime dtStart = mRecordDb.mStartRecUTC == DateTime.MinValue ? DateTime.MinValue : mRecordDb.mGetDeviceTime(mRecordDb.mStartRecUTC.AddSeconds(startT));

                        string stripLen = "     " + ((int)(durationT + 0.499)).ToString() + "  sec";

                        Image img = mMakeChartImage(width, height, mChannelsAmplitudeRange, channel, bAutoSizeA,
                            startT, durationT, startA, endA, cursorStart, cursorDuration, unitT, unitA);

                        pictBox.Image = img;

                        labelSample4Sweep.Text = mRecordFile.mGetShowSpeedString(unitT); ;// "25 mm/sec";
                        labelSample4Ampl.Text = mRecordFile.mGetShowAmplitudeString(unitA); //"1 mm/mV";
                        labelSample4End.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT)); //"10:15:20";
                        labelSample4Mid.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT * 0.5F)); //"10:15:15";
                        labelSample4Start.Text = CProgram.sTimeToString(dtStart) + stripLen; //"10:15:10";
                        labelSample4Lead.Text = mRecordDb.mGetChannelName(channel); //"LEAD I";
                        labelSample4Time.Text = CProgram.sTimeToString(dtEvent); //"10:15:10";
                        labelSample4Date.Text = CProgram.sDateToString(dtEvent); //"11/23/2016";
                        labelSample4Nr.Text = stripIndex < 0 ? "" : stripIndex.ToString(); // record study nr  "1";
                        labelSample4EventNr.Text = mGetEventNrString(mRecordDb.mSeqNrInStudy, mAnalysisNr, mRecordIndex); 

                        // Plot full strip with cursor
                        pictBox = pictureBoxSample4Full;

                        bAutoSizeA = true;
                        cursorStart = -1.0F;
                        cursorDuration = 0.0F;
                        unitT = 1.0F;
                        unitA = 0.5F;
                        width = pictBox.Width;
                        height = pictBox.Height;

                        startT = 0.0F;
                        durationT = mRecordFile.mGetSamplesTotalTime();
                        startA = -10.0F;
                        endA = 10.0F;

                        cursorStart = ms._mStartTimeSec;
                        cursorDuration = ms._mDurationSec;
                        dtStart = mRecordDb.mGetDeviceTime(mRecordDb.mStartRecUTC.AddSeconds(startT));

                        img = mMakeChartImage(width, height, mChannelsAmplitudeRange, channel, bAutoSizeA,
                            startT, durationT, startA, endA, cursorStart, cursorDuration, unitT, unitA);

                        pictBox.Image = img;
                        stripLen = "     " + ((int)(durationT + 0.499)).ToString() + "  sec";

                        labelSample4SweepFull.Text = mRecordFile.mGetShowSpeedString(unitT); ;// "25 mm/sec";
                        labelSample4AmplFull.Text = mRecordFile.mGetShowAmplitudeString(unitA); //"1 mm/mV";
                        labelSample4EndFull.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT)); //"10:15:20";
                        labelSample4MidFull.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT * 0.5F)); //"10:15:15";
                        labelSample4StartFull.Text = CProgram.sTimeToString(dtStart) + stripLen; //"10:15:10";
                        labelSample4LeadFull.Text = mRecordDb.mGetChannelName(channel); //"LEAD I";
                    }
                    mInitStripHr(mRecAnalysisFile, 3, labelMinHrText4, labelMinHrValue4, labelMinHrUnit4, labelMeanHrText4, labelMeanHrValue4, labelMeanHrUnit4,
                                                     labelMaxHrText4, labelMaxHrValue4, labelMaxHrUnit4);
                    mInitStripTimeTag(mRecAnalysisFile, 3, DMeasure2Tag.PRtime, labelPrText4, labelPrValue4, labelPrUnit4);
                    mInitStripTimeTag(mRecAnalysisFile, 3, DMeasure2Tag.QRStime, labelQrsText4, labelQrsValue4, labelQrsUnit4);
                    mInitStripTimeTag(mRecAnalysisFile, 3, DMeasure2Tag.QTtime, labelQtText4, labelQtValue4, labelQtUnit4);
                }

                void mInitSample5()
                {
                    // zomm 1
                    CMeasureStrip ms = mRecAnalysisFile.mFindActiveStrip(4);

                    if (ms == null)
                    {
                        panelEventSample5.Visible = false;
                    }
                    else
                    {
                        PictureBox pictBox = pictureBoxSample5;
                        int stripIndex = mRecAnalysisFile.mGetStripIndex(ms);

                        bool bAutoSizeA = false;
                        float cursorStart = -1.0F;
                        float cursorDuration = 0.0F;
                        float unitT = 0.2F;
                        float unitA = 0.5F;
                        int width = pictBox.Width;
                        int height = pictBox.Height;
                        UInt16 channel = ms._mChannel;
                        float startT = ms._mStartTimeSec;
                        float durationT = ms._mDurationSec;
                        float startA = ms._mStartA;
                        float endA = ms._mEndA;
                        ++stripIndex;

                        DateTime dtEvent = mRecordDb.mGetDeviceTime(mRecordDb.mEventUTC);

                        // plot sample

                        DateTime dtStart = mRecordDb.mStartRecUTC == DateTime.MinValue ? DateTime.MinValue : mRecordDb.mGetDeviceTime(mRecordDb.mStartRecUTC.AddSeconds(startT));

                        string stripLen = "     " + ((int)(durationT + 0.499)).ToString() + "  sec";

                        Image img = mMakeChartImage(width, height, mChannelsAmplitudeRange, channel, bAutoSizeA,
                            startT, durationT, startA, endA, cursorStart, cursorDuration, unitT, unitA);

                        pictBox.Image = img;

                        labelSample5Sweep.Text = mRecordFile.mGetShowSpeedString(unitT); ;// "25 mm/sec";
                        labelSample5Ampl.Text = mRecordFile.mGetShowAmplitudeString(unitA); //"1 mm/mV";
                        labelSample5End.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT)); //"10:15:20";
                        labelSample5Mid.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT * 0.5F)); //"10:15:15";
                        labelSample5Start.Text = CProgram.sTimeToString(dtStart) + stripLen; //"10:15:10";
                        labelSample5Lead.Text = mRecordDb.mGetChannelName(channel); //"LEAD I";
                        labelSample5Time.Text = CProgram.sTimeToString(dtEvent); //"10:15:10";
                        labelSample5Date.Text = CProgram.sDateToString(dtEvent); //"11/23/2016";
                        labelSample5Nr.Text = stripIndex < 0 ? "" : stripIndex.ToString(); // record study nr  "1";
                        labelSample5EventNr.Text = mGetEventNrString(mRecordDb.mSeqNrInStudy, mAnalysisNr, mRecordIndex); 

                        // Plot full strip with cursor
                        pictBox = pictureBoxSample5Full;

                        bAutoSizeA = true;
                        cursorStart = -1.0F;
                        cursorDuration = 0.0F;
                        unitT = 1.0F;
                        unitA = 0.5F;
                        width = pictBox.Width;
                        height = pictBox.Height;

                        startT = 0.0F;
                        durationT = mRecordFile.mGetSamplesTotalTime();
                        startA = -10.0F;
                        endA = 10.0F;

                        cursorStart = ms._mStartTimeSec;
                        cursorDuration = ms._mDurationSec;
                        dtStart = mRecordDb.mGetDeviceTime(mRecordDb.mStartRecUTC.AddSeconds(startT));

                        img = mMakeChartImage(width, height, mChannelsAmplitudeRange, channel, bAutoSizeA,
                            startT, durationT, startA, endA, cursorStart, cursorDuration, unitT, unitA);

                        pictBox.Image = img;
                        stripLen = "     " + ((int)(durationT + 0.499)).ToString() + "  sec";

                        labelSample5SweepFull.Text = mRecordFile.mGetShowSpeedString(unitT); ;// "25 mm/sec";
                        labelSample5AmplFull.Text = mRecordFile.mGetShowAmplitudeString(unitA); //"1 mm/mV";
                        labelSample5EndFull.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT)); //"10:15:20";
                        labelSample5MidFull.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT * 0.5F)); //"10:15:15";
                        labelSample5StartFull.Text = CProgram.sTimeToString(dtStart) + stripLen; //"10:15:10";
                        labelSample5LeadFull.Text = mRecordDb.mGetChannelName(channel); //"LEAD I";
                    }
                    mInitStripHr(mRecAnalysisFile, 4, labelMinHrText5, labelMinHrValue5, labelMinHrUnit5, labelMeanHrText5, labelMeanHrValue5, labelMeanHrUnit5,
                                                     labelMaxHrText5, labelMaxHrValue5, labelMaxHrUnit5);
                    mInitStripTimeTag(mRecAnalysisFile, 4, DMeasure2Tag.PRtime, labelPrText5, labelPrValue5, labelPrUnit5);
                    mInitStripTimeTag(mRecAnalysisFile, 4, DMeasure2Tag.QRStime, labelQrsText5, labelQrsValue5, labelQrsUnit5);
                    mInitStripTimeTag(mRecAnalysisFile, 4, DMeasure2Tag.QTtime, labelQtText5, labelQtValue5, labelQtUnit5);
                }

                void mInitSample6()
                {
                    // zomm 1
                    CMeasureStrip ms = mRecAnalysisFile.mFindActiveStrip(5);

                    if (ms == null)
                    {
                        panelEventSample6.Visible = false;
                    }
                    else
                    {
                        PictureBox pictBox = pictureBoxSample6;
                        int stripIndex = mRecAnalysisFile.mGetStripIndex(ms);

                        bool bAutoSizeA = false;
                        float cursorStart = -1.0F;
                        float cursorDuration = 0.0F;
                        float unitT = 0.2F;
                        float unitA = 0.5F;
                        int width = pictBox.Width;
                        int height = pictBox.Height;
                        UInt16 channel = ms._mChannel;
                        float startT = ms._mStartTimeSec;
                        float durationT = ms._mDurationSec;
                        float startA = ms._mStartA;
                        float endA = ms._mEndA;
                        ++stripIndex;

                        DateTime dtEvent = mRecordDb.mGetDeviceTime(mRecordDb.mEventUTC);

                        // plot sample

                        DateTime dtStart = mRecordDb.mStartRecUTC == DateTime.MinValue ? DateTime.MinValue : mRecordDb.mGetDeviceTime(mRecordDb.mStartRecUTC.AddSeconds(startT));

                        string stripLen = "     " + ((int)(durationT + 0.499)).ToString() + "  sec";

                        Image img = mMakeChartImage(width, height, mChannelsAmplitudeRange, channel, bAutoSizeA,
                            startT, durationT, startA, endA, cursorStart, cursorDuration, unitT, unitA);

                        pictBox.Image = img;

                        labelSample6Sweep.Text = mRecordFile.mGetShowSpeedString(unitT); ;// "25 mm/sec";
                        labelSample6Ampl.Text = mRecordFile.mGetShowAmplitudeString(unitA); //"1 mm/mV";
                        labelSample6End.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT)); //"10:15:20";
                        labelSample6Mid.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT * 0.5F)); //"10:15:15";
                        labelSample6Start.Text = CProgram.sTimeToString(dtStart) + stripLen; //"10:15:10";
                        labelSample6Lead.Text = mRecordDb.mGetChannelName(channel); //"LEAD I";
                        labelSample6Time.Text = CProgram.sTimeToString(dtEvent); //"10:15:10";
                        labelSample6Date.Text = CProgram.sDateToString(dtEvent); //"11/23/2016";
                        labelSample6Nr.Text = stripIndex < 0 ? "" : stripIndex.ToString(); // record study nr  "1";
                        labelSample6EventNr.Text = mGetEventNrString(mRecordDb.mSeqNrInStudy, mAnalysisNr, mRecordIndex); 

                        // Plot full strip with cursor
                        pictBox = pictureBoxSample6Full;

                        bAutoSizeA = true;
                        cursorStart = -1.0F;
                        cursorDuration = 0.0F;
                        unitT = 1.0F;
                        unitA = 0.5F;
                        width = pictBox.Width;
                        height = pictBox.Height;

                        startT = 0.0F;
                        durationT = mRecordFile.mGetSamplesTotalTime();
                        startA = -10.0F;
                        endA = 10.0F;

                        cursorStart = ms._mStartTimeSec;
                        cursorDuration = ms._mDurationSec;
                        dtStart = mRecordDb.mGetDeviceTime(mRecordDb.mStartRecUTC.AddSeconds(startT));

                        img = mMakeChartImage(width, height, mChannelsAmplitudeRange, channel, bAutoSizeA,
                            startT, durationT, startA, endA, cursorStart, cursorDuration, unitT, unitA);

                        pictBox.Image = img;
                        stripLen = "     " + ((int)(durationT + 0.499)).ToString() + "  sec";

                        labelSample6SweepFull.Text = mRecordFile.mGetShowSpeedString(unitT); ;// "25 mm/sec";
                        labelSample6AmplFull.Text = mRecordFile.mGetShowAmplitudeString(unitA); //"1 mm/mV";
                        labelSample6EndFull.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT)); //"10:15:20";
                        labelSample6MidFull.Text = dtStart == DateTime.MinValue ? "" : CProgram.sTimeToString(dtStart.AddSeconds(durationT * 0.5F)); //"10:15:15";
                        labelSample6StartFull.Text = CProgram.sTimeToString(dtStart) + stripLen; //"10:15:10";
                        labelSample6LeadFull.Text = mRecordDb.mGetChannelName(channel); //"LEAD I";
                    }
                    mInitStripHr(mRecAnalysisFile, 5, labelMinHrText6, labelMinHrValue6, labelMinHrUnit6, labelMeanHrText6, labelMeanHrValue6, labelMeanHrUnit6,
                                                     labelMaxHrText6, labelMaxHrValue6, labelMaxHrUnit6);
                    mInitStripTimeTag(mRecAnalysisFile, 5, DMeasure2Tag.PRtime, labelPrText6, labelPrValue6, labelPrUnit6);
                    mInitStripTimeTag(mRecAnalysisFile, 5, DMeasure2Tag.QRStime, labelQrsText6, labelQrsValue6, labelQrsUnit6);
                    mInitStripTimeTag(mRecAnalysisFile, 5, DMeasure2Tag.QTtime, labelQtText6, labelQtValue6, labelQtUnit6);
                }


                */

        [System.Runtime.InteropServices.DllImportAttribute("gdi32.dll")]
        private static extern int BitBlt(
     IntPtr hdcDest,     // handle to destination DC (device context)
     int nXDest,         // x-coord of destination upper-left corner
     int nYDest,         // y-coord of destination upper-left corner
     int nWidth,         // width of destination rectangle
     int nHeight,        // height of destination rectangle
     IntPtr hdcSrc,      // handle to source DC
     int nXSrc,          // x-coordinate of source upper-left corner
     int nYSrc,          // y-coordinate of source upper-left corner
     System.Int32 dwRop  // raster operation code
     );

        [System.Runtime.InteropServices.DllImportAttribute("gdi32.dll")]
        public static extern IntPtr SelectObject(IntPtr hdc, IntPtr obj);

        [System.Runtime.InteropServices.DllImportAttribute("gdi32.dll")]
        public static extern void DeleteObject(IntPtr obj);

        void mPanelToClipBoard1(Control APanel)
        {
            CProgram.sPromptError(true, "Print", "Do not use this function");
            try
            {
                Bitmap bmp = new Bitmap(APanel.Width, APanel.Height);

                using (Graphics G = Graphics.FromImage(bmp))
                {
                    G.Clear(Color.Cyan);
                }

                Graphics g1 = APanel.CreateGraphics();
                Image MyImage = new Bitmap(APanel.ClientRectangle.Width, APanel.ClientRectangle.Height, g1);
                Graphics g2 = Graphics.FromImage(bmp);
                IntPtr dc1 = g1.GetHdc();
                IntPtr dc2 = g2.GetHdc();
                BitBlt(dc2, 0, 0, ClientRectangle.Width, ClientRectangle.Height, dc1, 0, 0, 13369376);
                g1.ReleaseHdc(dc1);
                g2.ReleaseHdc(dc2);



                Clipboard.SetImage(bmp);

            }
            catch (Exception /*ex*/)
            {
                //CProgram.sLogException("Panel to Clipboard error", ex);

            }
        }

/*        void mPanelToClipBoard(Control APanel)
        {
            CProgram.sPromptError(true, "Print", "Do not use this function");
            try
            {
                mPrint1Bitmap = new Bitmap(APanel.Width, APanel.Height);

                using (Graphics G = Graphics.FromImage(mPrint1Bitmap))
                {
                    G.Clear(Color.White);           // needed for init
                }

                Graphics g1 = APanel.CreateGraphics();
                Image MyImage = new Bitmap(APanel.ClientRectangle.Width, APanel.ClientRectangle.Height, g1);
                Graphics g2 = Graphics.FromImage(mPrint1Bitmap);
                IntPtr dc1 = g1.GetHdc();
                IntPtr dc2 = g2.GetHdc();
                BitBlt(dc2, 0, 0, ClientRectangle.Width, ClientRectangle.Height, dc1, 0, 0, 13369376);
                g1.ReleaseHdc(dc1);
                g2.ReleaseHdc(dc2);

                Clipboard.SetImage(mPrint1Bitmap);

            }
            catch (Exception /*ex* /)
            {
                //CProgram.sLogException("Image to Clipboard error", ex);
            }
        }

        Bitmap mPanelToImage(Control APanel, bool AbInvert, bool AbToClipBoard)
        {
            Bitmap bitmap = null;
            try
            {
                bitmap = new Bitmap(APanel.Width, APanel.Height);

                using (Graphics G = Graphics.FromImage(bitmap))
                {
                    G.Clear(Color.White);           // needed for init
                }

                if (AbInvert) InvertZOrderOfControls(APanel.Controls);
                APanel.DrawToBitmap(bitmap, new System.Drawing.Rectangle(0, 0, APanel.Width, APanel.Height));
                if (AbInvert) InvertZOrderOfControls(APanel.Controls);

                if (AbToClipBoard)
                {
                    Clipboard.SetImage(bitmap);
                }
            }
            catch (Exception /*ex* /)
            {
                //                CProgram.sLogException("Image to Clipboard error", ex);
                bitmap = null;
            }
            return bitmap;
        }
        */
        private bool mbPrintImages()
        {
            return mbPrint2Pdf();
        }


        private bool mbPrintImagesToPrinter()
        {
            bool bOk = false;
            try
            {
                PrintDocument pd = new PrintDocument();

                Margins oldMargins = pd.DefaultPageSettings.Margins;
                Margins margins = new Margins(50, 50, 50, 50);
                pd.DefaultPageSettings.Margins = margins;

                pd.PrintPage += mPrintPage;

                PrintDialog printDialog1 = new PrintDialog();

                if (printDialog1 != null)
                {
                    printDialog1.Document = pd;
                    DialogResult result = printDialog1.ShowDialog(this);
                    if (result == DialogResult.OK)
                    {
                        PrintPreviewDialog printPreviewDialog = new PrintPreviewDialog();

                        if (printPreviewDialog != null)
                        {
                            printPreviewDialog.ClientSize = new System.Drawing.Size(600, 900);
                            printPreviewDialog.Location = new System.Drawing.Point(10, 10);
                            printPreviewDialog.Name = "PrintPreviewDialog1";

                            // Associate the event-handling method with the  
                            // document's PrintPage event. 
                            // Set the minimum size the dialog can be resized to. 
                            printPreviewDialog.MinimumSize = new System.Drawing.Size(375, 250);

                            // Set the UseAntiAlias property to true, which will allow the  
                            // operating system to smooth fonts. 
                            printPreviewDialog.UseAntiAlias = true;

                            printPreviewDialog.Document = pd;
                            printPreviewDialog.StartPosition = FormStartPosition.CenterParent;


                            // Call the ShowDialog method. This will trigger the document's
                            //  PrintPage event.
                            printPreviewDialog.ShowDialog( this );
                        }
                        bOk = true;
                    }
               
/*
                else
                {
                    //here to select the printer attached to user PC
//                    PrintDialog printDialog1 = new PrintDialog();

                    if (printDialog1 != null)
                    {
                        printDialog1.Document = pd;
                        DialogResult result = printDialog1.ShowDialog(this);
                        if (result == DialogResult.OK)
                        {
                            pd.Print();//this will trigger the Print Event handeler PrintPage
                            bOk = true;
                        }
                    }
*/
                }

                pd.Dispose();
            }
            catch (Exception)
            {

            }
            return bOk;
        }

        //The Print Event handeler
        private void mPrintPage(object o, PrintPageEventArgs e)
        {
            try
            {
              
                Bitmap bitmap = mCurrentPrintPage == 1 ? mPrint1Bitmap : mPrint2Bitmap;

                if (bitmap != null && bitmap.Width > 0 && bitmap.Height > 0)
                {
                    //Adjust the size of the image to the page to print the full image without loosing any part of it
                    Rectangle m = e.MarginBounds;

                    if ((double)bitmap.Width / (double)bitmap.Height > (double)m.Width / (double)m.Height) // image is wider
                    {
                        m.Height = (int)((double)bitmap.Height / (double)bitmap.Width * (double)m.Width);
                    }
                    else
                    {
                        m.Width = (int)((double)bitmap.Width / (double)bitmap.Height * (double)m.Height);
                    }
                    e.Graphics.DrawImage(bitmap, m);
                }
                if( ++mCurrentPrintPage <= mNrPages)
                {
                    e.HasMorePages = true;
                }
                else
                {
                    e.HasMorePages = false;
                    mCurrentPrintPage = 1;  // set ready for the print by print preview
                }

            }
            catch (Exception)
            {

            }
        }
        public static void InvertZOrderOfControls(Control.ControlCollection ControlList)
        {
            // do not process empty control list
            if (ControlList.Count == 0)
                return;
            // only re-order if list is writable
            if (!ControlList.IsReadOnly)
            {
                SortedList<int, Control> sortedChildControls = new SortedList<int, Control>();
                // find all none docked controls and sort in to list
                foreach (Control ctrlChild in ControlList)
                {
                    if (ctrlChild.Dock == DockStyle.None)
                        sortedChildControls.Add(ControlList.GetChildIndex(ctrlChild), ctrlChild);
                }
                // re-order the controls in the parent by swapping z-order of first 
                // and last controls in the list and moving towards the center
                for (int i = 0; i < sortedChildControls.Count / 2; i++)
                {
                    Control ctrlChild1 = sortedChildControls.Values[i];
                    Control ctrlChild2 = sortedChildControls.Values[sortedChildControls.Count - 1 - i];
                    int zOrder1 = ControlList.GetChildIndex(ctrlChild1);
                    int zOrder2 = ControlList.GetChildIndex(ctrlChild2);
                    ControlList.SetChildIndex(ctrlChild1, zOrder2);
                    ControlList.SetChildIndex(ctrlChild2, zOrder1);
                }
            }
            // try to invert the z-order of child controls
            foreach (Control ctrlChild in ControlList)
            {
                try { InvertZOrderOfControls(ctrlChild.Controls); }
                catch { }
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
        }

        private void toolStripButton1_Click_1(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            try
            {
                mPrint1Bitmap = CPdfDocRec.sPanelToImage(panelPrintArea1, false, true);
                mPrint2Bitmap = CPdfDocRec.sPanelToImage(panelPrintArea2, false, true);
                mbPrintImages();
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Print Analysis", ex);
            }
        }

        private void panel64_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel52_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label39_Click(object sender, EventArgs e)
        {

        }

        private void label57_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label104_Click(object sender, EventArgs e)
        {

        }

        private void label102_Click(object sender, EventArgs e)
        {

        }

        private void panel113_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBoxBaselineReferenceECGStrip_Click(object sender, EventArgs e)
        {

        }

        private void panel62_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel51_Paint(object sender, PaintEventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;

            try
            {
                mPrint1Bitmap = CPdfDocRec.sPanelToImage(panelPrintArea1, false, true);
                mPrint2Bitmap = CPdfDocRec.sPanelToImage(panelPrintArea2, false, true);

                toolStripGenPages.Text = "Done.";

                if (mbPrintImages())
                {
                    Close();
                }
            }
            catch ( Exception ex)
            {
                CProgram.sLogException("Print Analysis", ex);
            }
        }

        private void CPrintAnalys12Form_Shown(object sender, EventArgs e)
        {
            BringToFront();
            timer1.Enabled = true;
            toolStrip1.Focus();
            //            mPanelToClipBoard1(panelPrintArea);
            Application.DoEvents();
        }

        private void labelRatemeasureSI_Click(object sender, EventArgs e)
        {

        }

        private void CPrintAnalys12Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            timer1.Enabled = false;
            if(mPrint1Bitmap != null )
            {
                mPrint1Bitmap = null;
            }
            if (mPrint2Bitmap != null)
            {
                mPrint2Bitmap = null;
            }

        }

        private void toolStripClipboard_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;

            //mPrint1Bitmap = mPanelToImage(panelPrintArea1, false, true);
            if(mPrint1Bitmap != null)
            {
                Clipboard.SetImage(mPrint1Bitmap);
            }
        }

        private bool mbEnterLable( Label ArLabel, string AName )
        {
            bool bOk = false;

            if( ArLabel != null )
            {
                string s = ArLabel.Text;
                    
                    if( CProgram.sbReqLabel( "Enter Analysis header", "Patient Last name", ref s, "", false ))
                {
                    ArLabel.Text = s;
                    bOk = true;

                }
            }
            return bOk;
        }
        private void toolStripEnterData_Click(object sender, EventArgs e)
        {

        }

        private void dataGridViewPrevThreeTX_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void labelPhysSignLine_Click(object sender, EventArgs e)
        {

        }

        private void labelSerialNrHeader_Click(object sender, EventArgs e)
        {

        }

        private void labelPhysPrintName_Click(object sender, EventArgs e)
        {

        }

        private void label53_Click(object sender, EventArgs e)
        {

        }

        private void label93_Click(object sender, EventArgs e)
        {

        }

        private void label78_Click(object sender, EventArgs e)
        {

        }

        private void label19_Click(object sender, EventArgs e)
        {

        }

        private void toolStripClipboard2_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;

//            mPrint2Bitmap = mPanelToImage(panelPrintArea2, false, true);
            if (mPrint2Bitmap != null)
            {
                Clipboard.SetImage(mPrint2Bitmap);
            }

        }

        private void label14_Click(object sender, EventArgs e)
        {
            /*                // Create a new PDF document

                            PdfDocument document = new PdfDocument();

                            document.Info.Title = "Created with PDFsharp";

                            // Create an empty page

                            PdfPage page = document.AddPage();
                            // Get an XGraphics object for drawing
                            page.Size = PdfSharp.PageSize.A4;
                                                page.Orientation = PageOrientation.Landscape;
                            XGraphics gfx = XGraphics.FromPdfPage(page);

                            double wPage = page.Width;
                            double hPage = page.Height;
                            double fMargin = 0.05;
                            double fMarginLeft = 0.055;
                            double x = wPage * fMarginLeft;
                            double y = hPage * fMargin;
                            double w = wPage * (1 - fMargin - fMarginLeft);
                            double h = hPage * (1 - 2 * fMargin);

                            // Create a font
                            XFont font = new XFont("Verdana", 20, XFontStyle.BoldItalic);
                            // Draw the text
                            gfx.DrawString("Hello, World!", font, XBrushes.Black,
                            new XRect(0, 0, page.Width, y),
                            XStringFormats.Center);


                            XImage image = XImage.FromGdiPlusImage(mPrint1Bitmap);
                            // Left position in point

                            gfx.DrawImage(image, x, y, w, h);

                            if (mPrint2Bitmap != null && mNrPages >= 2)
                            {
                                page = document.AddPage();
                                // Get an XGraphics object for drawing
                                gfx = XGraphics.FromPdfPage(page);
                                page.Size = PdfSharp.PageSize.A4;
                                wPage = page.Width;
                                hPage = page.Height;
                                x = wPage * fMarginLeft;
                                y = hPage * fMargin;
                                w = wPage * (1 - fMargin - fMarginLeft);
                                h = hPage * (1 - 2 * fMargin);

                                // Draw the text
                                gfx.DrawString("Hello, World, this is page 2!", font, XBrushes.Black,
                                new XRect(0, 0, page.Width, y),
                                XStringFormats.Center);


                                image = XImage.FromGdiPlusImage(mPrint2Bitmap);
                                // Left position in point

                                gfx.DrawImage(image, x, y, w, h);

                            }

                            // Save the document...
                            const string filename = "HelloWorld.pdf";
                            document.Save(filename);
                            // ...and start a viewer.
                            Process.Start(filename);

              */
        }

        private bool mbPrint2Pdf()
        {
            bool bOk = false;
            string fullName = "";
            bool bTestRemove = false;

            if (mPrint1Bitmap != null)
            {
                try
                {
                    toolStripGenPages.Text = "PDF generarating...";
                    CPdfDocRec pdfDocRec = new CPdfDocRec(CDvtmsData.sGetDBaseConnection());
                    UInt32 studyIX = _mStudyInfo == null ? 0 : _mStudyInfo.mIndex_KEY;
                    UInt16 studySubNr = mRecordDb.mSeqNrInStudy;
                    UInt32 patientIX = _mPatientInfo == null ? 0 : _mPatientInfo.mIndex_KEY;
                    string reportType = CStudyInfo.sGetStudyPdfCode(DStudyPdfCode.Event, _mbQCd);
                    /*                    bool bAnnalysis = mRecAnalysis != null && mRecAnalysis.mIndex_KEY != 0;
                                        UInt32 refID = bAnnalysis ? mRecAnalysis.mIndex_KEY : mRecordDb.mIndex_KEY;
                                        string refLetter = bAnnalysis ? "A" : "R";
                    */
                    bool bStudy = studyIX != 0 && studySubNr != 0;
                    UInt32 refID = bStudy ? studySubNr : mRecordDb.mIndex_KEY;
                    string refLetter = bStudy ? "r" : "R";
                    DateTime dt = mRecordDb.mGetDeviceTime(mRecordDb.mEventUTC);
                    PdfDocument pdfDocument;
                    PageOrientation pageOrientation = PageOrientation.Landscape;
                    bool bInitials = _mCreadedByInitials != null && _mCreadedByInitials.Length > 0;
                    string extraName = _mbQCd ? "QC" : "";

                    if (false == bInitials)
                    {
                        if (CProgram.sbReqLabel("Print pdf", "User Initials", ref _mCreadedByInitials, "", false))
                        {
                            bInitials = _mCreadedByInitials != null && _mCreadedByInitials.Length > 0;
                        }
                    }
                    CProgram.sbLogMemSize("generated Analysis12 print R#" + mRecordDb.mIndex_KEY.ToString() + " S"
                        + mRecordDb.mStudy_IX.ToString() + "." + mRecordDb.mSeqNrInStudy.ToString() + " report "
+ _mStudyReportNr.ToString() + " " + mNrPages.ToString() + " pages ");


                    if (bInitials && pdfDocRec != null
                        && null != (pdfDocument = pdfDocRec.mbNewDocument(_mCreadedByInitials, studyIX, _mStudyReportNr /*studySubNr*/, patientIX, reportType, refLetter, refID, extraName, dt, dt)))
                    {
                        if (pdfDocRec.mbAddImagePage(pageOrientation, mPrint1Bitmap))
                        {
                            if (mPrint2Bitmap != null && mNrPages >= 2)
                            {
                                pdfDocRec.mbAddImagePage(pageOrientation, mPrint2Bitmap);
                            }
                            toolStripGenPages.Text = "PDF saving...";

                            if (_mStudyInfo != null && _mStudyInfo.mIndex_KEY > 0)
                            {
                                if (_mbAnonymize)
                                {
                                    _mStudyReportNr = 0;
                                }
                                else
                                {
                                    _mStudyInfo.mbSetNextReportNr(_mStudyReportNr);
                                }
                                bOk = pdfDocRec.mbSaveToStudyPdf(out fullName, false == _mbAnonymize);
                                bTestRemove = true;
                            }
                            else
                            {
                                // no study save to use record
                                bOk = pdfDocRec.mbSaveToDir(Path.GetDirectoryName(mRecordFilePath), out fullName, false);
                                bTestRemove = true;
                            }

                            if (bOk)
                            {
                                CProgram.sbLogMemSize("saved Analysis12 print R#" + mRecordDb.mIndex_KEY.ToString() + " S"
                                    + mRecordDb.mStudy_IX.ToString() + "." + mRecordDb.mSeqNrInStudy.ToString() + " report "
+ _mStudyReportNr.ToString() + " " + mNrPages.ToString() + " pages " + Path.GetFileName(fullName));

                                toolStripGenPages.Text = "PDF opening...";
                                _mReportIX = pdfDocRec.mIndex_KEY;
                                Process.Start(fullName);
                                Close();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("PdfPrint analyze12 event failed", ex);
                    bTestRemove = true;
                }
                if (bTestRemove && _mReportIX == 0)
                {
                    try
                    {
                        bTestRemove = fullName.Length == 0 || false == File.Exists(fullName);
                    }
                    catch (Exception ex)
                    {
                        bTestRemove = false;
                    }
                    if (bTestRemove)
                    {
                        _mStudyInfo.mbReverseNextReportNr(_mStudyReportNr); // something failed revert to previousNr Reports
                    }
                }
            }
            return bOk;
        }
/*        private void mPrint2Pdfold()
        {
            if (mPrint1Bitmap != null)
            {
                // Create a new PDF document

                PdfDocument document = new PdfDocument();

                document.Info.Title = "Created with PDFsharp";

                // Create an empty page

                PdfPage page = document.AddPage();
                // Get an XGraphics object for drawing
                page.Size = PdfSharp.PageSize.A4;
                page.Orientation = PageOrientation.Landscape;
                XGraphics gfx = XGraphics.FromPdfPage(page);
 
                double wPage = page.Width;
                double hPage = page.Height;
                double fMargin = 0.05;
                double fMarginLeft = 0.055;
                double x = wPage * fMarginLeft;
                double y = hPage * fMargin;
                double w = wPage * (1 - fMargin - fMarginLeft);
                double h = hPage * (1 - 2 * fMargin);

                // Create a font
                XFont font = new XFont("Verdana", 20, XFontStyle.BoldItalic);
                // Draw the text
                gfx.DrawString("Hello, World!", font, XBrushes.Black,
                new XRect(0, 0, page.Width, y ),
                XStringFormats.Center);


                XImage image = XImage.FromGdiPlusImage(mPrint1Bitmap);
                // Left position in point

                gfx.DrawImage(image, x, y, w, h);

                if (mPrint2Bitmap != null && mNrPages >= 2 )
                {
                    page = document.AddPage();
                    // Get an XGraphics object for drawing
                    page.Size = PdfSharp.PageSize.A4;
                    page.Orientation = PageOrientation.Portrait;

                    gfx = XGraphics.FromPdfPage(page);
                    wPage = page.Width;
                    hPage = page.Height;
                    x = wPage * fMarginLeft;
                    y = hPage * fMargin;
                    w = wPage * (1 - fMargin - fMarginLeft);
                    h = hPage * (1 - 2 * fMargin);

                    // Draw the text
                    gfx.DrawString("Hello, World, this is page 2!", font, XBrushes.Black,
                    new XRect(0, 0, page.Width, y),
                    XStringFormats.Center);


                    image = XImage.FromGdiPlusImage(mPrint2Bitmap);
                    // Left position in point

                    gfx.DrawImage(image, x, y, w, h);

                }

                // Save the document...
                const string filename = "HelloWorld.pdf";
                document.Save(filename);
                // ...and start a viewer.
                Process.Start(filename);
            }
        }
*/
        private void toolStripButton1_Click_2(object sender, EventArgs e)
        {
            mbPrint2Pdf();
        }

        private void migraDocPrintDocument1_PrintPage(object sender, PrintPageEventArgs e)
        {
        }

        private void label100_Click(object sender, EventArgs e)
        {

        }

        private void label43_Click(object sender, EventArgs e)
        {

        }

        private void label48_Click(object sender, EventArgs e)
        {

        }

        private void label91_Click(object sender, EventArgs e)
        {

        }

        private void label52_Click(object sender, EventArgs e)
        {

        }

        private void labelPatName_Click(object sender, EventArgs e)
        {

        }

        private void labelStudyNrHeader_Click(object sender, EventArgs e)
        {

        }

        private void label38_Click(object sender, EventArgs e)
        {

        }

        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void labelFindingsRemark_Click(object sender, EventArgs e)
        {

        }

        private void labelClientHeader_Click(object sender, EventArgs e)
        {

        }

        private void label37_Click(object sender, EventArgs e)
        {

        }

        private void label45_Click(object sender, EventArgs e)
        {

        }

        private void panelTimeHeadingsPerStrip_Paint(object sender, PaintEventArgs e)
        {

        }

        private void labelEventEnd_Click(object sender, EventArgs e)
        {

        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
    }
}
