-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema nl0001soft
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema nl0001soft
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `nl0001soft` DEFAULT CHARACTER SET utf8 ;
USE `nl0001soft` ;

-- -----------------------------------------------------
-- Table `nl0001soft`.`d_clientinfo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `nl0001soft`.`d_clientinfo` (
  `d_ClientInfo_Key` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Key index holding record number\n',
  `Active` TINYINT(4) NULL DEFAULT '0' COMMENT 'Bool, indicates if record is active',
  `CreatedUTC` DATETIME NOT NULL COMMENT 'Record creation Date Time in UTC',
  `CreatedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that created the record',
  `CreatedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to the device that created the record',
  `CreatedProgID` INT(11) NOT NULL COMMENT 'Program ID( programNr, run, major, minor, builkd) that was used to create record\n',
  `ChangedUTC` DATETIME NULL DEFAULT NULL COMMENT 'Date Time UTC of last change',
  `ChangedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that changed the record',
  `ChangedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to device that changed the record',
  `ChangedProgID` INT(11) NULL DEFAULT '0' COMMENT 'ProgID of program that changed the record',
  `Label` VARCHAR(32) NOT NULL COMMENT 'Descriptive label (short Organisation + department combination)\n',
  `FullName` VARCHAR(64) NULL DEFAULT NULL,
  `Address` TEXT NULL DEFAULT NULL,
  `ZipCode` VARCHAR(16) NULL DEFAULT NULL,
  `HouseNr` VARCHAR(8) NULL DEFAULT NULL,
  `State` VARCHAR(48) NULL DEFAULT NULL,
  `City` VARCHAR(48) NULL DEFAULT NULL,
  `CountryCode` VARCHAR(4) NULL DEFAULT NULL,
  `CentralTelNr` VARCHAR(32) NULL DEFAULT NULL,
  `DirectTelNr` VARCHAR(32) NULL DEFAULT NULL,
  `FaxNr` VARCHAR(32) NULL DEFAULT NULL,
  `EMail` VARCHAR(128) NULL DEFAULT NULL,
  `DepartmentName` VARCHAR(64) NULL DEFAULT NULL,
  `DepartmentHead` VARCHAR(64) NULL DEFAULT NULL,
  `DepartmentTelNr` VARCHAR(32) NULL DEFAULT NULL,
  PRIMARY KEY (`d_ClientInfo_Key`, `Label`),
  UNIQUE INDEX `Label_UNIQUE` (`Label` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 26
DEFAULT CHARACTER SET = utf8
COMMENT = 'ClientInfo Info record, holding Hospital+department info';


-- -----------------------------------------------------
-- Table `nl0001soft`.`d_deviceinfo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `nl0001soft`.`d_deviceinfo` (
  `d_deviceinfo_Key` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Key index holding record number\n',
  `Active` TINYINT(4) NULL DEFAULT '0' COMMENT 'Bool, indicates if record is active',
  `CreatedUTC` DATETIME NOT NULL COMMENT 'Record creation Date Time in UTC',
  `CreatedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that created the record',
  `CreatedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to the device that created the record',
  `CreatedProgID` INT(11) NOT NULL COMMENT 'Program ID( programNr, run, major, minor, builkd) that was used to create record\n',
  `ChangedUTC` DATETIME NULL DEFAULT NULL COMMENT 'Date Time UTC of last change',
  `ChangedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that changed the record',
  `ChangedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to device that changed the record',
  `ChangedProgID` INT(11) NULL DEFAULT '0' COMMENT 'ProgID of program that changed the record',
  `DeviceSerialNr` VARCHAR(32) NOT NULL,
  `DeviceModel_IX` INT(11) NOT NULL,
  `FirmwareVersion` VARCHAR(16) NULL DEFAULT NULL,
  `DateFirstInService` DATE NULL DEFAULT NULL,
  `DateNextService` DATE NULL DEFAULT NULL,
  `RemarkLabel` VARCHAR(48) NULL DEFAULT NULL,
  `LogBook` TEXT NULL DEFAULT NULL,
  `ActiveStudy_IX` INT(11) NULL DEFAULT NULL,
  `ActiveRecording_IX` INT(11) NULL DEFAULT NULL,
  `ActiveRefID` VARCHAR(32) NULL DEFAULT NULL,
  `ActiveRefName` VARCHAR(32) NULL DEFAULT NULL,
  `OperatingMode` VARCHAR(32) NULL DEFAULT NULL,
  `ChannelNames` VARCHAR(128) NULL DEFAULT NULL,
  `State_IX` SMALLINT(6) NULL DEFAULT '0',
  `InvertChannelsMask` INT(11) NULL DEFAULT NULL,
  `ChannelUnits` VARCHAR(128) NULL DEFAULT NULL,
  `RecorderStartUTC` TIMESTAMP NULL DEFAULT NULL,
  `RecorderEndUTC` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`d_deviceinfo_Key`, `DeviceModel_IX`, `DeviceSerialNr`))
ENGINE = InnoDB
AUTO_INCREMENT = 15
DEFAULT CHARACTER SET = utf8
COMMENT = 'Device Info, info of one unique device of type ModelInfo';


-- -----------------------------------------------------
-- Table `nl0001soft`.`d_devicemodel`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `nl0001soft`.`d_devicemodel` (
  `d_devicemodel_Key` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Key index holding record number\n',
  `Active` TINYINT(4) NULL DEFAULT '0' COMMENT 'Bool, indicates if record is active',
  `CreatedUTC` DATETIME NOT NULL COMMENT 'Record creation Date Time in UTC',
  `CreatedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that created the record',
  `CreatedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to the device that created the record',
  `CreatedProgID` INT(11) NOT NULL COMMENT 'Program ID( programNr, run, major, minor, builkd) that was used to create record\n',
  `ChangedUTC` DATETIME NULL DEFAULT NULL COMMENT 'Date Time UTC of last change',
  `ChangedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that changed the record',
  `ChangedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to device that changed the record',
  `ChangedProgID` INT(11) NULL DEFAULT '0' COMMENT 'ProgID of program that changed the record',
  `ModelLabel` VARCHAR(32) NOT NULL,
  `Manufacturer` VARCHAR(64) NULL DEFAULT NULL,
  `ModelType` VARCHAR(32) NULL DEFAULT NULL,
  `DeviceColor` VARCHAR(32) NULL DEFAULT NULL,
  `ServiceInterval` SMALLINT(6) NULL DEFAULT NULL,
  `ManufacturerAddress` TEXT NULL DEFAULT NULL,
  `ManufacturerZipCode` VARCHAR(16) NULL DEFAULT NULL,
  `ManufacturerStreetNr` VARCHAR(8) NULL DEFAULT NULL,
  `ManufacturersState` VARCHAR(48) NULL DEFAULT NULL,
  `ManufacturerCity` VARCHAR(48) NULL DEFAULT NULL,
  `ManufacturerCountryCode` VARCHAR(4) NULL DEFAULT NULL,
  `ManufacturerPhone1` VARCHAR(32) NULL DEFAULT NULL,
  `ManufacturerPhone2` VARCHAR(32) NULL DEFAULT NULL,
  `ManufacturerCell` VARCHAR(32) NULL DEFAULT NULL,
  `ManufacturerSkype` VARCHAR(64) NULL DEFAULT NULL,
  `ManufacturerEmail` VARCHAR(128) NULL DEFAULT NULL,
  `ManufacturerWebSite` VARCHAR(128) NULL DEFAULT NULL,
  `OperatingModesSet` VARCHAR(128) NULL DEFAULT NULL,
  `DefaultOperatingMode` VARCHAR(16) NULL DEFAULT NULL,
  `DefaultChannelsLabels` VARCHAR(128) NULL DEFAULT NULL,
  `ExtraData` TEXT NULL DEFAULT NULL,
  `DefaultInvertChannelsMask` INT(11) NULL DEFAULT NULL,
  `DefaultChannelsUnits` VARCHAR(128) NULL DEFAULT NULL,
  PRIMARY KEY (`d_devicemodel_Key`, `ModelLabel`),
  UNIQUE INDEX `ModelLabel_UNIQUE` (`ModelLabel` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8
COMMENT = 'Name Info record';


-- -----------------------------------------------------
-- Table `nl0001soft`.`d_emptyrecord`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `nl0001soft`.`d_emptyrecord` (
  `d_Name_Key` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Key index holding record number\n',
  `Active` TINYINT(4) NULL DEFAULT '0' COMMENT 'Bool, indicates if record is active',
  `CreatedUTC` DATETIME NOT NULL COMMENT 'Record creation Date Time in UTC',
  `CreatedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that created the record',
  `CreatedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to the device that created the record',
  `CreatedProgID` INT(11) NOT NULL COMMENT 'Program ID( programNr, run, major, minor, builkd) that was used to create record\n',
  `ChangedUTC` DATETIME NULL DEFAULT NULL COMMENT 'Date Time UTC of last change',
  `ChangedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that changed the record',
  `ChangedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to device that changed the record',
  `ChangedProgID` INT(11) NULL DEFAULT '0' COMMENT 'ProgID of program that changed the record',
  PRIMARY KEY (`d_Name_Key`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Name Info record';


-- -----------------------------------------------------
-- Table `nl0001soft`.`d_patientinfo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `nl0001soft`.`d_patientinfo` (
  `d_patientinfo_Key` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Key index holding record number\n',
  `Active` TINYINT(4) NULL DEFAULT '0' COMMENT 'Bool, indicates if record is active',
  `CreatedUTC` DATETIME NOT NULL COMMENT 'Record creation Date Time in UTC',
  `CreatedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that created the record',
  `CreatedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to the device that created the record',
  `CreatedProgID` INT(11) NOT NULL COMMENT 'Program ID( programNr, run, major, minor, builkd) that was used to create record\n',
  `ChangedUTC` DATETIME NULL DEFAULT NULL COMMENT 'Date Time UTC of last change',
  `ChangedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that changed the record',
  `ChangedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to device that changed the record',
  `ChangedProgID` INT(11) NULL DEFAULT '0' COMMENT 'ProgID of program that changed the record',
  `PatientID_ES` VARCHAR(16) NULL DEFAULT NULL,
  `SocSecNr_ES` VARCHAR(16) NOT NULL,
  `PatientFirstName_ES` VARCHAR(32) NULL DEFAULT NULL,
  `PatientMiddleName_ES` VARCHAR(16) NULL DEFAULT NULL,
  `PatientLastName_ES` VARCHAR(64) NULL DEFAULT NULL,
  `PatientDateOfBirth_EI` INT(11) NULL DEFAULT NULL,
  `PatientBirthYear` SMALLINT(6) NULL DEFAULT NULL,
  `PatientGender_IX` TINYINT(4) NULL DEFAULT NULL,
  `PatientRace_IX` SMALLINT(6) NULL DEFAULT NULL,
  `PatientHeightM` FLOAT NULL DEFAULT NULL,
  `PatientWeightKg` FLOAT NULL DEFAULT NULL,
  `PatientAddress_ES` VARCHAR(128) NULL DEFAULT NULL,
  `PatientZip_ES` VARCHAR(16) NULL DEFAULT NULL,
  `PatientHouseNumber_ES` VARCHAR(8) NULL DEFAULT NULL,
  `PatientCity` VARCHAR(32) NULL DEFAULT NULL,
  `PatientState` VARCHAR(32) NULL DEFAULT NULL,
  `PatientCountryCode` VARCHAR(4) NULL DEFAULT NULL,
  `PatientPhone1_ES` VARCHAR(16) NULL DEFAULT NULL,
  `PatientPhone2_ES` VARCHAR(16) NULL DEFAULT NULL,
  `PatientCell_ES` VARCHAR(16) NULL DEFAULT NULL,
  `PatientEmail_ES` VARCHAR(128) NULL DEFAULT NULL,
  `HistoryStringSet` VARCHAR(64) NULL DEFAULT NULL,
  `AllergiesStringSet` VARCHAR(64) NULL DEFAULT NULL,
  `MedicationStringSet` VARCHAR(64) NULL DEFAULT NULL,
  `SymptomsStringSet` VARCHAR(64) NULL DEFAULT NULL,
  `RemarkLabel` VARCHAR(32) NULL DEFAULT NULL,
  `RemarkText` TEXT NULL DEFAULT NULL,
  `PatientSkype_ES` VARCHAR(64) NULL DEFAULT NULL,
  `State_IX` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`d_patientinfo_Key`, `SocSecNr_ES`),
  UNIQUE INDEX `SocSecNr_ES_UNIQUE` (`SocSecNr_ES` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 30
DEFAULT CHARACTER SET = utf8
COMMENT = 'Patient Info record';


-- -----------------------------------------------------
-- Table `nl0001soft`.`d_physicianinfo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `nl0001soft`.`d_physicianinfo` (
  `d_physicianinfo_Key` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Key index holding record number\n',
  `Active` TINYINT(4) NULL DEFAULT '0' COMMENT 'Bool, indicates if record is active',
  `CreatedUTC` DATETIME NOT NULL COMMENT 'Record creation Date Time in UTC',
  `CreatedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that created the record',
  `CreatedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to the device that created the record',
  `CreatedProgID` INT(11) NOT NULL COMMENT 'Program ID( programNr, run, major, minor, builkd) that was used to create record\n',
  `ChangedUTC` DATETIME NULL DEFAULT NULL COMMENT 'Date Time UTC of last change',
  `ChangedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that changed the record',
  `ChangedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to device that changed the record',
  `ChangedProgID` INT(11) NULL DEFAULT '0' COMMENT 'ProgID of program that changed the record',
  `Label` VARCHAR(32) NULL DEFAULT NULL,
  `GenderTitle` VARCHAR(32) NULL DEFAULT NULL,
  `AcademicTitle` VARCHAR(32) NULL DEFAULT NULL,
  `FirstName_ES` VARCHAR(64) NULL DEFAULT NULL,
  `MiddleName_ES` VARCHAR(32) NULL DEFAULT NULL,
  `LastName_ES` VARCHAR(64) NULL DEFAULT NULL,
  `DateOfBirth_EI` INT(11) NULL DEFAULT NULL,
  `Gender_IX` TINYINT(4) NULL DEFAULT '0',
  `Hospital_IX` INT(11) NULL DEFAULT '0',
  `Address_ES` TEXT NULL DEFAULT NULL,
  `Zip_ES` VARCHAR(16) NULL DEFAULT NULL,
  `HouseNr_ES` VARCHAR(8) NULL DEFAULT NULL,
  `City` VARCHAR(32) NULL DEFAULT NULL,
  `State` VARCHAR(32) NULL DEFAULT NULL,
  `CountryCode` VARCHAR(4) NULL DEFAULT NULL,
  `Phone1_ES` VARCHAR(24) NULL DEFAULT NULL,
  `Phone2_ES` VARCHAR(24) NULL DEFAULT NULL,
  `Cell_ES` VARCHAR(24) NULL DEFAULT NULL,
  `Email_ES` VARCHAR(128) NULL DEFAULT NULL,
  `Skype_ES` VARCHAR(128) NULL DEFAULT NULL,
  `Instruction_ES` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`d_physicianinfo_Key`))
ENGINE = InnoDB
AUTO_INCREMENT = 10
DEFAULT CHARACTER SET = utf8
COMMENT = ' Info record';


-- -----------------------------------------------------
-- Table `nl0001soft`.`d_recanalysis`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `nl0001soft`.`d_recanalysis` (
  `d_recAnalysis_Key` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Key index holding record number\n',
  `Active` TINYINT(4) NULL DEFAULT '0' COMMENT 'Bool, indicates if record is active',
  `CreatedUTC` DATETIME NOT NULL COMMENT 'Record creation Date Time in UTC',
  `CreatedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that created the record',
  `CreatedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to the device that created the record',
  `CreatedProgID` INT(11) NOT NULL COMMENT 'Program ID( programNr, run, major, minor, builkd) that was used to create record\n',
  `ChangedUTC` DATETIME NULL DEFAULT NULL COMMENT 'Date Time UTC of last change',
  `ChangedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that changed the record',
  `ChangedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to device that changed the record',
  `ChangedProgID` INT(11) NULL DEFAULT '0' COMMENT 'ProgID of program that changed the record',
  `Recording_IX` INT(11) NOT NULL,
  `SeqInRecording` SMALLINT(6) NOT NULL,
  `Study_IX` INT(11) NULL DEFAULT '0',
  `AnalysisClassSet` VARCHAR(64) NULL DEFAULT NULL,
  `AnalysisRemark` VARCHAR(250) NULL DEFAULT NULL,
  `SymptomsCodeSet` VARCHAR(64) NULL DEFAULT NULL,
  `SymptomsRemark` VARCHAR(250) NULL DEFAULT NULL,
  `ActivitiesCodeSet` VARCHAR(64) NULL DEFAULT NULL,
  `ActivitiesRemark` VARCHAR(250) NULL DEFAULT NULL,
  `State` SMALLINT(6) NULL DEFAULT '0',
  `Technician_IX` INT(11) NULL DEFAULT '0',
  `MeasuredUTC` DATETIME NULL DEFAULT NULL,
  `Physician_IX` INT(11) NULL DEFAULT '0',
  `ReportedUTC` DATETIME NULL DEFAULT NULL,
  `AnalysisRhythmSet` VARCHAR(64) NULL DEFAULT NULL,
  `ReportNr` SMALLINT(6) NULL DEFAULT NULL,
  `StudyReportNr` SMALLINT(6) NULL DEFAULT NULL,
  `StudyReportUTC` DATETIME NULL DEFAULT NULL,
  `StudyReport_IX` INT(11) NULL DEFAULT NULL,
  `MeasuredHrN` SMALLINT(6) NULL DEFAULT '0' COMMENT 'added 20170120',
  `MeasuredHrMin` FLOAT NULL DEFAULT '0' COMMENT 'added 20170120\n',
  `MeasuredHrMean` FLOAT NULL DEFAULT '0' COMMENT 'added 20170120',
  `MeasuredHrMax` FLOAT NULL DEFAULT '0' COMMENT 'added 20170120',
  `IncludeReportTable` INT(11) NULL DEFAULT '0' COMMENT 'added 20170120',
  `IncludeReportStrip` INT(11) NULL DEFAULT '0' COMMENT 'added 20170120',
  `ManualEvent` TINYINT(4) NULL DEFAULT '0',
  `NrSelectedStrips` SMALLINT(6) NULL DEFAULT '0',
  `NrTotalStrips` SMALLINT(6) NULL DEFAULT '0',
  PRIMARY KEY (`d_recAnalysis_Key`, `Recording_IX`, `SeqInRecording`))
ENGINE = InnoDB
AUTO_INCREMENT = 14
DEFAULT CHARACTER SET = utf8
COMMENT = 'Name Info record';


-- -----------------------------------------------------
-- Table `nl0001soft`.`d_record`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `nl0001soft`.`d_record` (
  `d_record_Key` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Key index holding record number\n',
  `ReadUTC` DATETIME NOT NULL COMMENT 'DateTime in UTC that the software read the record files and created the record\n\n',
  `FileName` VARCHAR(64) NOT NULL COMMENT 'Filename of the recording',
  `DeviceID` CHAR(32) NOT NULL COMMENT 'Device ID as string given by the device (Header file)\n',
  `TimeZoneOffsetMin` INT(11) NULL DEFAULT '0' COMMENT 'Time zone offset: for device local time add this to UTC',
  `DeviceModel` VARCHAR(64) CHARACTER SET 'utf16' NULL DEFAULT NULL COMMENT 'Device Model given by device',
  `Device_IX` INT(1) NULL DEFAULT '0' COMMENT 'Index to Device record (found using DeviceID and Moodel).\n',
  `PatientID_ES` VARCHAR(64) NULL DEFAULT NULL COMMENT 'Encrypted PatientID given by recording\n',
  `PatientTotalName_ES` VARCHAR(128) CHARACTER SET 'utf16' NULL DEFAULT NULL COMMENT 'Encrypted Patient Total Name from record files (JSON)',
  `Patient_IX` INT(1) NULL DEFAULT '0' COMMENT 'Index to Patient record.',
  `PatientGender` INT(11) NULL DEFAULT '0' COMMENT 'Index to Patient Gender\n',
  `PatientBirthDate_EI` INT(11) NULL DEFAULT '0' COMMENT 'Encrypted Patient BirthDate (from json) file.\n',
  `PatientBirthYear` YEAR(4) NULL DEFAULT '0000' COMMENT 'Patient Birth year (can be used for research)',
  `RecLabel` VARCHAR(32) NULL DEFAULT NULL COMMENT 'Short Remark label regarding this recording (Initialy from JSON)',
  `RecRemark_ES` VARCHAR(250) NULL DEFAULT NULL COMMENT 'Remark regarding this recording',
  `PhysicianName_ES` VARCHAR(128) NULL DEFAULT NULL COMMENT 'Physiicians name',
  `Physician_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to prescribing physician\n',
  `Study_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to corresponding procedure record',
  `StartRecUTC` DATETIME NULL DEFAULT NULL COMMENT 'Start Date and time in UTC of beginning of measurement record',
  `RecDurationSec` FLOAT NULL DEFAULT '0' COMMENT 'Duration of recording in seconds',
  `EventTypeString` VARCHAR(128) NOT NULL COMMENT 'Event message givven by device in string form',
  `EventType_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to the EventType (generated from device message and DeviceEvent table)',
  `EventUTC` DATETIME NULL DEFAULT NULL COMMENT 'Event Date and Time in UTC\n',
  `EventTimeSec` FLOAT NULL DEFAULT NULL COMMENT 'Offset to event from start of recording',
  `TransmitUTC` DATETIME NULL DEFAULT NULL,
  `RecState` INT(11) NULL DEFAULT '0' COMMENT 'Index in State indicating the recording state (New, Recording, Recorded)',
  `ImportConverter` INT(11) NULL DEFAULT '0' COMMENT 'Import converter used (program enum)\n',
  `ImportMethod` INT(11) NULL DEFAULT '0' COMMENT 'Import Method enum (0=unknown, 1=manual, 2=automatic)',
  `NrSignals` SMALLINT(6) NULL DEFAULT '0' COMMENT 'Number of signals in recording',
  `SignalLabels` VARCHAR(128) NULL DEFAULT NULL COMMENT 'List of signal names in recording seperated by \',\'',
  `StoredAt` VARCHAR(64) NULL DEFAULT NULL COMMENT 'Stored at direcory.\n Recording:YM<Read.YYYYMM>\\D<Read.DD>\\<StoredAt>\\<filename>',
  `Active` TINYINT(4) NULL DEFAULT '0' COMMENT 'Bool, indicates if record is active',
  `CreatedUTC` DATETIME NOT NULL COMMENT 'Record creation Date Time in UTC',
  `CreatedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that created the record',
  `CreatedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to the device that created the record',
  `CreatedProgID` INT(11) NOT NULL COMMENT 'Program ID( programNr, run, major, minor, builkd) that was used to create record\n',
  `ChangedUTC` DATETIME NULL DEFAULT NULL COMMENT 'Date Time UTC of last change',
  `ChangedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that changed the record',
  `ChangedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to device that changed the record',
  `ChangedProgID` INT(11) NULL DEFAULT '0' COMMENT 'ProgID of program that changed the record',
  `DeviceMode` VARCHAR(32) NULL DEFAULT NULL COMMENT 'New 20161212',
  `SeqNrInStudy` SMALLINT(6) NULL DEFAULT '0' COMMENT 'New 20161212\nRecord sequence number within study (starts at 1) / Study.NrRecords\n',
  `NrAnalysis` SMALLINT(6) NULL DEFAULT '0' COMMENT 'New 20161212',
  `InvertChannelsMask` INT(11) NULL DEFAULT NULL,
  `ExtraData` TEXT NULL DEFAULT NULL,
  `SignalUnits` VARCHAR(128) NULL DEFAULT NULL,
  PRIMARY KEY (`d_record_Key`, `DeviceID`, `ReadUTC`))
ENGINE = InnoDB
AUTO_INCREMENT = 1910
DEFAULT CHARACTER SET = utf8
COMMENT = 'Stores vital Recording data.\nThe recording itself is stored at Recording:\\YM<readUTC.YYYYMM>\\D<read.DD>\\<Stored_AT>\\';


-- -----------------------------------------------------
-- Table `nl0001soft`.`d_studyinfo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `nl0001soft`.`d_studyinfo` (
  `d_studyinfo_Key` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Key index holding record number\n',
  `Active` TINYINT(4) NULL DEFAULT '0' COMMENT 'Bool, indicates if record is active',
  `CreatedUTC` DATETIME NOT NULL COMMENT 'Record creation Date Time in UTC',
  `CreatedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that created the record',
  `CreatedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to the device that created the record',
  `CreatedProgID` INT(11) NOT NULL DEFAULT '0' COMMENT 'Program ID( programNr, run, major, minor, builkd) that was used to create record\n',
  `ChangedUTC` DATETIME NULL DEFAULT NULL COMMENT 'Date Time UTC of last change',
  `ChangedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that changed the record',
  `ChangedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to device that changed the record',
  `ChangedProgID` INT(11) NULL DEFAULT '0' COMMENT 'ProgID of program that changed the record',
  `StudyRemarkLabel` VARCHAR(32) NULL DEFAULT NULL,
  `Client_IX` INT(11) NULL DEFAULT NULL,
  `StudyStartDate` DATE NOT NULL,
  `HospitalRoom` VARCHAR(16) NULL DEFAULT NULL,
  `HospitalBed` VARCHAR(8) NULL DEFAULT NULL,
  `Physisian_IX` INT(11) NULL DEFAULT NULL,
  `RefPhysisian_IX` INT(11) NULL DEFAULT NULL,
  `StudyRecorder_IX` INT(11) NULL DEFAULT NULL,
  `StudyMonitoringDays` SMALLINT(6) NULL DEFAULT NULL,
  `StudyTypeCode` VARCHAR(16) NULL DEFAULT NULL,
  `ReportIntervalSet` VARCHAR(32) NULL DEFAULT NULL,
  `StudyPhysicianInstruction` TEXT NULL DEFAULT NULL,
  `StudyProcCodeSet` VARCHAR(64) NULL DEFAULT NULL,
  `NrRecords` SMALLINT(6) NULL DEFAULT NULL,
  `NrReports` SMALLINT(6) NULL DEFAULT NULL,
  `StudyState` SMALLINT(6) NULL DEFAULT '0',
  `Patient_IX` INT(11) NULL DEFAULT '0',
  `BaseLineAnalysis_IX` INT(11) NULL DEFAULT '0',
  `d_studyinfocol` VARCHAR(45) NULL DEFAULT NULL,
  `MctIntervalSet` VARCHAR(32) NULL DEFAULT NULL,
  `RecorderStartUTC` TIMESTAMP NULL DEFAULT NULL,
  `RecorderEndUTC` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`d_studyinfo_Key`))
ENGINE = InnoDB
AUTO_INCREMENT = 25
DEFAULT CHARACTER SET = utf8
COMMENT = 'Study Info record';


-- -----------------------------------------------------
-- Table `nl0001soft`.`d_studyreport`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `nl0001soft`.`d_studyreport` (
  `d_studyreport_KEY` INT(11) NOT NULL AUTO_INCREMENT,
  `Study_IX` VARCHAR(45) NOT NULL,
  `ReportNr` SMALLINT(6) NULL DEFAULT NULL,
  `ReportTableSet` TEXT NULL DEFAULT NULL,
  `ReportStripSet` TEXT NULL DEFAULT NULL,
  `ReportSectionMask` SMALLINT(6) NULL DEFAULT NULL,
  `Summary_ES` TEXT NULL DEFAULT NULL,
  `Conclusion_ES` TEXT NULL DEFAULT NULL,
  `ReportUTC` TIMESTAMP NULL DEFAULT NULL,
  `ReportedBy_IX` INT(11) NULL DEFAULT NULL,
  `Active` TINYINT(4) NULL DEFAULT '0' COMMENT 'Bool, indicates if record is active',
  `CreatedUTC` DATETIME NOT NULL COMMENT 'Record creation Date Time in UTC',
  `CreatedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that created the record',
  `CreatedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to the device that created the record',
  `CreatedProgID` INT(11) NOT NULL COMMENT 'Program ID( programNr, run, major, minor, builkd) that was used to create record\n',
  `ChangedUTC` DATETIME NULL DEFAULT NULL COMMENT 'Date Time UTC of last change',
  `ChangedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that changed the record',
  `ChangedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to device that changed the record',
  `ChangedProgID` INT(11) NULL DEFAULT '0' COMMENT 'ProgID of program that changed the record',
  PRIMARY KEY (`d_studyreport_KEY`, `Study_IX`))
ENGINE = InnoDB
AUTO_INCREMENT = 16
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `nl0001soft`.`e_studyreportinterval`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `nl0001soft`.`e_studyreportinterval` (
  `e_StudyReportInterval_Key` INT(11) NOT NULL COMMENT 'Key index holding record number\n',
  `Active` TINYINT(4) NULL DEFAULT '0' COMMENT 'Bool, indicates if record is active',
  `CreatedUTC` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Record creation Date Time in UTC',
  `CreatedBy_IX` INT(11) UNSIGNED NULL DEFAULT '0' COMMENT 'Index to user that created the record',
  `CreatedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to the device that created the record',
  `CreatedProgID` INT(11) NOT NULL COMMENT 'Program ID( programNr, run, major, minor, builkd) that was used to create record\n',
  `ChangedUTC` DATETIME NULL DEFAULT NULL COMMENT 'Date Time UTC of last change',
  `ChangedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that changed the record',
  `ChangedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to device that changed the record',
  `ChangedProgID` INT(11) NULL DEFAULT '0' COMMENT 'ProgID of program that changed the record',
  `SortValue` TINYINT(4) NULL DEFAULT '0',
  `GroupValue` TINYINT(4) NULL DEFAULT '0',
  `FloatValue` FLOAT NULL DEFAULT '0',
  `Code` CHAR(16) NULL DEFAULT NULL COMMENT 'Code for enum (used for Set)\n',
  `Label` CHAR(250) NOT NULL,
  PRIMARY KEY (`e_StudyReportInterval_Key`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'enum list for Study Report Interval (set)';


-- -----------------------------------------------------
-- Table `nl0001soft`.`e_studytype`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `nl0001soft`.`e_studytype` (
  `e_studytype_Key` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Key index holding record number\n',
  `Active` TINYINT(4) NULL DEFAULT '0' COMMENT 'Bool, indicates if record is active',
  `CreatedUTC` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Record creation Date Time in UTC',
  `CreatedBy_IX` INT(11) UNSIGNED NULL DEFAULT '0' COMMENT 'Index to user that created the record',
  `CreatedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to the device that created the record',
  `CreatedProgID` INT(11) NOT NULL COMMENT 'Program ID( programNr, run, major, minor, builkd) that was used to create record\n',
  `ChangedUTC` DATETIME NULL DEFAULT NULL COMMENT 'Date Time UTC of last change',
  `ChangedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that changed the record',
  `ChangedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to device that changed the record',
  `ChangedProgID` INT(11) NULL DEFAULT '0' COMMENT 'ProgID of program that changed the record',
  `SortValue` TINYINT(4) NULL DEFAULT '0',
  `GroupValue` TINYINT(4) NULL DEFAULT '0',
  `FloatValue` FLOAT NULL DEFAULT '0',
  `Code` CHAR(16) NULL DEFAULT NULL COMMENT 'Code for enum (used for Set)\n',
  `Label` CHAR(250) NOT NULL,
  PRIMARY KEY (`e_studytype_Key`))
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = utf8
COMMENT = 'enum list for StudyType';


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
