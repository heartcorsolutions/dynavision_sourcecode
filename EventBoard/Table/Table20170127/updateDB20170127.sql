-- MySQL Workbench Synchronization

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

ALTER TABLE `nl0012soft`.`d_deviceinfo` 
ADD COLUMN `RecorderStartUTC` TIMESTAMP NULL DEFAULT NULL AFTER `ChannelUnits`,
ADD COLUMN `RecorderEndUTC` TIMESTAMP NULL DEFAULT NULL AFTER `RecorderStartUTC`;

ALTER TABLE `nl0012soft`.`d_physicianinfo` 
ADD COLUMN `Instruction_ES` TEXT NULL DEFAULT NULL AFTER `Skype_ES`;

ALTER TABLE `nl0012soft`.`d_recanalysis` 
ADD COLUMN `MeasuredHrN` SMALLINT(6) NULL DEFAULT '0' COMMENT 'added 20170120' AFTER `StudyReport_IX`,
ADD COLUMN `MeasuredHrMin` FLOAT(11) NULL DEFAULT '0' COMMENT 'added 20170120\n' AFTER `MeasuredHrN`,
ADD COLUMN `MeasuredHrMean` FLOAT(11) NULL DEFAULT '0' COMMENT 'added 20170120' AFTER `MeasuredHrMin`,
ADD COLUMN `MeasuredHrMax` FLOAT(11) NULL DEFAULT '0' COMMENT 'added 20170120' AFTER `MeasuredHrMean`,
ADD COLUMN `IncludeReportTable` INT(11) NULL DEFAULT '0' COMMENT 'added 20170120' AFTER `MeasuredHrMax`,
ADD COLUMN `IncludeReportStrip` INT(11) NULL DEFAULT '0' COMMENT 'added 20170120' AFTER `IncludeReportTable`,
ADD COLUMN `ManualEvent` TINYINT(4) NULL DEFAULT '0' AFTER `IncludeReportStrip`,
ADD COLUMN `NrSelectedStrips` SMALLINT(6) NULL DEFAULT '0' AFTER `ManualEvent`,
ADD COLUMN `NrTotalStrips` SMALLINT(6) NULL DEFAULT '0' AFTER `NrSelectedStrips`;

ALTER TABLE `nl0012soft`.`d_studyinfo` 
ADD COLUMN `d_studyinfocol` VARCHAR(45) NULL DEFAULT NULL AFTER `BaseLineAnalysis_IX`,
ADD COLUMN `MctIntervalSet` VARCHAR(32) NULL DEFAULT NULL AFTER `d_studyinfocol`,
ADD COLUMN `RecorderStartUTC` TIMESTAMP NULL DEFAULT NULL AFTER `MctIntervalSet`,
ADD COLUMN `RecorderEndUTC` TIMESTAMP NULL DEFAULT NULL AFTER `RecorderStartUTC`;

CREATE TABLE IF NOT EXISTS `nl0012soft`.`d_studyreport` (
  `d_studyreport_KEY` INT(11) NOT NULL AUTO_INCREMENT,
  `Study_IX` VARCHAR(45) NOT NULL,
  `ReportNr` SMALLINT(6) NULL DEFAULT NULL,
  `ReportTableSet` TEXT NULL DEFAULT NULL,
  `ReportStripSet` TEXT NULL DEFAULT NULL,
  `ReportSectionMask` SMALLINT(6) NULL DEFAULT NULL,
  `Summary_ES` VARCHAR(45) NULL DEFAULT NULL,
  `Conclusion_ES` VARCHAR(45) NULL DEFAULT NULL,
  `ReportUTC` TIMESTAMP NULL DEFAULT NULL,
  `ReportedBy_IX` INT(11) NULL DEFAULT NULL,
  `Active` TINYINT(4) NULL DEFAULT '0' COMMENT 'Bool, indicates if record is active',
  `CreatedUTC` DATETIME NOT NULL COMMENT 'Record creation Date Time in UTC',
  `CreatedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that created the record',
  `CreatedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to the device that created the record',
  `CreatedProgID` INT(11) NOT NULL COMMENT 'Program ID( programNr, run, major, minor, builkd) that was used to create record\n',
  `ChangedUTC` DATETIME NULL DEFAULT NULL COMMENT 'Date Time UTC of last change',
  `ChangedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that changed the record',
  `ChangedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to device that changed the record',
  `ChangedProgID` INT(11) NULL DEFAULT '0' COMMENT 'ProgID of program that changed the record',
  PRIMARY KEY (`d_studyreport_KEY`, `Study_IX`))
ENGINE = InnoDB
AUTO_INCREMENT = 15
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
