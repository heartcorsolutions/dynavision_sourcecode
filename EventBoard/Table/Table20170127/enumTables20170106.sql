-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: ServerSimon-HP    Database: nl0001soft
-- ------------------------------------------------------
-- Server version	5.7.13-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `e_studyreportinterval`
--

DROP TABLE IF EXISTS `e_studyreportinterval`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `e_studyreportinterval` (
  `e_StudyReportInterval_Key` int(11) NOT NULL COMMENT 'Key index holding record number\n',
  `Active` tinyint(4) DEFAULT '0' COMMENT 'Bool, indicates if record is active',
  `CreatedUTC` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Record creation Date Time in UTC',
  `CreatedBy_IX` int(11) unsigned DEFAULT '0' COMMENT 'Index to user that created the record',
  `CreatedDevice_IX` int(11) DEFAULT '0' COMMENT 'Index to the device that created the record',
  `CreatedProgID` int(11) NOT NULL COMMENT 'Program ID( programNr, run, major, minor, builkd) that was used to create record\n',
  `ChangedUTC` datetime DEFAULT NULL COMMENT 'Date Time UTC of last change',
  `ChangedBy_IX` int(11) DEFAULT '0' COMMENT 'Index to user that changed the record',
  `ChangedDevice_IX` int(11) DEFAULT '0' COMMENT 'Index to device that changed the record',
  `ChangedProgID` int(11) DEFAULT '0' COMMENT 'ProgID of program that changed the record',
  `SortValue` tinyint(4) DEFAULT '0',
  `GroupValue` tinyint(4) DEFAULT '0',
  `FloatValue` float DEFAULT '0',
  `Code` char(16) DEFAULT NULL COMMENT 'Code for enum (used for Set)\n',
  `Label` char(250) NOT NULL,
  PRIMARY KEY (`e_StudyReportInterval_Key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='enum list for Study Report Interval (set)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `e_studyreportinterval`
--

LOCK TABLES `e_studyreportinterval` WRITE;
/*!40000 ALTER TABLE `e_studyreportinterval` DISABLE KEYS */;
INSERT INTO `e_studyreportinterval` VALUES (1,1,'2016-11-18 15:32:22',0,0,0,NULL,0,0,0,0,0,0,'EER','Every Event Report'),(9,1,'2016-11-18 15:38:48',0,0,0,NULL,0,0,0,0,0,1,'DER','Daily Event Report'),(10,1,'2016-11-18 15:38:48',0,0,0,NULL,0,0,0,0,0,7,'WER','Weekly Event Report'),(11,1,'2016-11-18 15:38:48',0,0,0,NULL,0,0,0,0,0,9999,'ESR','End of Session Report');
/*!40000 ALTER TABLE `e_studyreportinterval` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `e_studytype`
--

DROP TABLE IF EXISTS `e_studytype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `e_studytype` (
  `e_studytype_Key` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Key index holding record number\n',
  `Active` tinyint(4) DEFAULT '0' COMMENT 'Bool, indicates if record is active',
  `CreatedUTC` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Record creation Date Time in UTC',
  `CreatedBy_IX` int(11) unsigned DEFAULT '0' COMMENT 'Index to user that created the record',
  `CreatedDevice_IX` int(11) DEFAULT '0' COMMENT 'Index to the device that created the record',
  `CreatedProgID` int(11) NOT NULL COMMENT 'Program ID( programNr, run, major, minor, builkd) that was used to create record\n',
  `ChangedUTC` datetime DEFAULT NULL COMMENT 'Date Time UTC of last change',
  `ChangedBy_IX` int(11) DEFAULT '0' COMMENT 'Index to user that changed the record',
  `ChangedDevice_IX` int(11) DEFAULT '0' COMMENT 'Index to device that changed the record',
  `ChangedProgID` int(11) DEFAULT '0' COMMENT 'ProgID of program that changed the record',
  `SortValue` tinyint(4) DEFAULT '0',
  `GroupValue` tinyint(4) DEFAULT '0',
  `FloatValue` float DEFAULT '0',
  `Code` char(16) DEFAULT NULL COMMENT 'Code for enum (used for Set)\n',
  `Label` char(250) NOT NULL,
  PRIMARY KEY (`e_studytype_Key`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='enum list for StudyType';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `e_studytype`
--

LOCK TABLES `e_studytype` WRITE;
/*!40000 ALTER TABLE `e_studytype` DISABLE KEYS */;
INSERT INTO `e_studytype` VALUES (1,1,'2016-11-18 15:18:19',0,0,0,NULL,0,0,0,0,0,0,'E3L1c','3L - 1 chl event'),(2,1,'2016-11-18 15:15:58',0,0,0,NULL,0,0,0,0,0,0,'E2L1c','2L - 1 ch event'),(3,1,'2016-11-18 15:22:09',0,0,0,NULL,0,0,0,0,0,0,'E3L2c','3L - 2 ch event'),(4,1,'2016-11-18 15:22:09',0,0,0,NULL,0,0,0,0,0,0,'M2L1c','2L - 1 ch MCT'),(5,1,'2016-11-18 15:25:54',0,0,0,NULL,0,0,0,0,0,0,'M3L1c','2L - 1 ch MCT'),(6,1,'2016-11-18 15:25:54',0,0,0,NULL,0,0,0,0,0,0,'M3L2c','3L - 2 ch MCT'),(7,1,'2016-11-18 15:25:54',0,0,0,NULL,0,0,0,0,0,0,'H5L2c','5L - 2 ch Holter'),(8,1,'2016-11-18 15:25:54',0,0,0,NULL,0,0,0,0,0,0,'R5L5c','5L - 5 ch Real-time monitoring');
/*!40000 ALTER TABLE `e_studytype` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-06 10:11:34
