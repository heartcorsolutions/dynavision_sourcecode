-- MySQL Workbench Synchronization

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE TABLE IF NOT EXISTS `nl0012soft`.`d_clientinfo` (
  `d_ClientInfo_Key` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Key index holding record number\n',
  `Active` TINYINT(4) NULL DEFAULT '0' COMMENT 'Bool, indicates if record is active',
  `CreatedUTC` DATETIME NOT NULL COMMENT 'Record creation Date Time in UTC',
  `CreatedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that created the record',
  `CreatedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to the device that created the record',
  `CreatedProgID` INT(11) NOT NULL COMMENT 'Program ID( programNr, run, major, minor, builkd) that was used to create record\n',
  `ChangedUTC` DATETIME NULL DEFAULT NULL COMMENT 'Date Time UTC of last change',
  `ChangedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that changed the record',
  `ChangedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to device that changed the record',
  `ChangedProgID` INT(11) NULL DEFAULT '0' COMMENT 'ProgID of program that changed the record',
  `Label` VARCHAR(32) NOT NULL COMMENT 'Descriptive label (short Organisation + department combination)\n',
  `FullName` VARCHAR(64) NULL DEFAULT NULL,
  `Address` TEXT NULL DEFAULT NULL,
  `ZipCode` VARCHAR(16) NULL DEFAULT NULL,
  `HouseNr` VARCHAR(8) NULL DEFAULT NULL,
  `State` VARCHAR(48) NULL DEFAULT NULL,
  `City` VARCHAR(48) NULL DEFAULT NULL,
  `CountryCode` VARCHAR(4) NULL DEFAULT NULL,
  `CentralTelNr` VARCHAR(32) NULL DEFAULT NULL,
  `DirectTelNr` VARCHAR(32) NULL DEFAULT NULL,
  `FaxNr` VARCHAR(32) NULL DEFAULT NULL,
  `EMail` VARCHAR(128) NULL DEFAULT NULL,
  `DepartmentName` VARCHAR(64) NULL DEFAULT NULL,
  `DepartmentHead` VARCHAR(64) NULL DEFAULT NULL,
  `DepartmentTelNr` VARCHAR(32) NULL DEFAULT NULL,
  PRIMARY KEY (`d_ClientInfo_Key`, `Label`),
  UNIQUE INDEX `Label_UNIQUE` (`Label` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 23
DEFAULT CHARACTER SET = utf8
COMMENT = 'ClientInfo Info record, holding Hospital+department info';

CREATE TABLE IF NOT EXISTS `nl0012soft`.`d_deviceinfo` (
  `d_deviceinfo_Key` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Key index holding record number\n',
  `Active` TINYINT(4) NULL DEFAULT '0' COMMENT 'Bool, indicates if record is active',
  `CreatedUTC` DATETIME NOT NULL COMMENT 'Record creation Date Time in UTC',
  `CreatedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that created the record',
  `CreatedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to the device that created the record',
  `CreatedProgID` INT(11) NOT NULL COMMENT 'Program ID( programNr, run, major, minor, builkd) that was used to create record\n',
  `ChangedUTC` DATETIME NULL DEFAULT NULL COMMENT 'Date Time UTC of last change',
  `ChangedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that changed the record',
  `ChangedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to device that changed the record',
  `ChangedProgID` INT(11) NULL DEFAULT '0' COMMENT 'ProgID of program that changed the record',
  `DeviceSerialNr` VARCHAR(32) NOT NULL,
  `DeviceModel_IX` INT(11) NOT NULL,
  `FirmwareVersion` VARCHAR(16) NULL DEFAULT NULL,
  `DateFirstInService` DATE NULL DEFAULT NULL,
  `DateNextService` DATE NULL DEFAULT NULL,
  `RemarkLabel` VARCHAR(48) NULL DEFAULT NULL,
  `LogBook` TEXT NULL DEFAULT NULL,
  `ActiveStudy_IX` INT(11) NULL DEFAULT NULL,
  `ActiveRecording_IX` INT(11) NULL DEFAULT NULL,
  `ActiveRefID` VARCHAR(32) NULL DEFAULT NULL,
  `ActiveRefName` VARCHAR(32) NULL DEFAULT NULL,
  `OperatingMode` VARCHAR(32) NULL DEFAULT NULL,
  `ChannelNames` VARCHAR(128) NULL DEFAULT NULL,
  `State_IX` SMALLINT(6) NULL DEFAULT '0',
  `InvertChannelsMask` INT(11) NULL DEFAULT NULL,
  `ChannelUnits` VARCHAR(128) NULL DEFAULT NULL,
  PRIMARY KEY (`d_deviceinfo_Key`, `DeviceModel_IX`, `DeviceSerialNr`))
ENGINE = InnoDB
AUTO_INCREMENT = 12
DEFAULT CHARACTER SET = utf8
COMMENT = 'Device Info, info of one unique device of type ModelInfo';

CREATE TABLE IF NOT EXISTS `nl0012soft`.`d_devicemodel` (
  `d_devicemodel_Key` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Key index holding record number\n',
  `Active` TINYINT(4) NULL DEFAULT '0' COMMENT 'Bool, indicates if record is active',
  `CreatedUTC` DATETIME NOT NULL COMMENT 'Record creation Date Time in UTC',
  `CreatedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that created the record',
  `CreatedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to the device that created the record',
  `CreatedProgID` INT(11) NOT NULL COMMENT 'Program ID( programNr, run, major, minor, builkd) that was used to create record\n',
  `ChangedUTC` DATETIME NULL DEFAULT NULL COMMENT 'Date Time UTC of last change',
  `ChangedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that changed the record',
  `ChangedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to device that changed the record',
  `ChangedProgID` INT(11) NULL DEFAULT '0' COMMENT 'ProgID of program that changed the record',
  `ModelLabel` VARCHAR(32) NOT NULL,
  `Manufacturer` VARCHAR(64) NULL DEFAULT NULL,
  `ModelType` VARCHAR(32) NULL DEFAULT NULL,
  `DeviceColor` VARCHAR(32) NULL DEFAULT NULL,
  `ServiceInterval` SMALLINT(6) NULL DEFAULT NULL,
  `ManufacturerAddress` TEXT NULL DEFAULT NULL,
  `ManufacturerZipCode` VARCHAR(16) NULL DEFAULT NULL,
  `ManufacturerStreetNr` VARCHAR(8) NULL DEFAULT NULL,
  `ManufacturersState` VARCHAR(48) NULL DEFAULT NULL,
  `ManufacturerCity` VARCHAR(48) NULL DEFAULT NULL,
  `ManufacturerCountryCode` VARCHAR(4) NULL DEFAULT NULL,
  `ManufacturerPhone1` VARCHAR(32) NULL DEFAULT NULL,
  `ManufacturerPhone2` VARCHAR(32) NULL DEFAULT NULL,
  `ManufacturerCell` VARCHAR(32) NULL DEFAULT NULL,
  `ManufacturerSkype` VARCHAR(64) NULL DEFAULT NULL,
  `ManufacturerEmail` VARCHAR(128) NULL DEFAULT NULL,
  `ManufacturerWebSite` VARCHAR(128) NULL DEFAULT NULL,
  `OperatingModesSet` VARCHAR(128) NULL DEFAULT NULL,
  `DefaultOperatingMode` VARCHAR(16) NULL DEFAULT NULL,
  `DefaultChannelsLabels` VARCHAR(128) NULL DEFAULT NULL,
  `ExtraData` TEXT NULL DEFAULT NULL,
  `DefaultInvertChannelsMask` INT(11) NULL DEFAULT NULL,
  `DefaultChannelsUnits` VARCHAR(128) NULL DEFAULT NULL,
  PRIMARY KEY (`d_devicemodel_Key`, `ModelLabel`),
  UNIQUE INDEX `ModelLabel_UNIQUE` (`ModelLabel` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8
COMMENT = 'Name Info record';

CREATE TABLE IF NOT EXISTS `nl0012soft`.`d_emptyrecord` (
  `d_Name_Key` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Key index holding record number\n',
  `Active` TINYINT(4) NULL DEFAULT '0' COMMENT 'Bool, indicates if record is active',
  `CreatedUTC` DATETIME NOT NULL COMMENT 'Record creation Date Time in UTC',
  `CreatedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that created the record',
  `CreatedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to the device that created the record',
  `CreatedProgID` INT(11) NOT NULL COMMENT 'Program ID( programNr, run, major, minor, builkd) that was used to create record\n',
  `ChangedUTC` DATETIME NULL DEFAULT NULL COMMENT 'Date Time UTC of last change',
  `ChangedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that changed the record',
  `ChangedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to device that changed the record',
  `ChangedProgID` INT(11) NULL DEFAULT '0' COMMENT 'ProgID of program that changed the record',
  PRIMARY KEY (`d_Name_Key`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Name Info record';

CREATE TABLE IF NOT EXISTS `nl0012soft`.`d_patientinfo` (
  `d_patientinfo_Key` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Key index holding record number\n',
  `Active` TINYINT(4) NULL DEFAULT '0' COMMENT 'Bool, indicates if record is active',
  `CreatedUTC` DATETIME NOT NULL COMMENT 'Record creation Date Time in UTC',
  `CreatedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that created the record',
  `CreatedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to the device that created the record',
  `CreatedProgID` INT(11) NOT NULL COMMENT 'Program ID( programNr, run, major, minor, builkd) that was used to create record\n',
  `ChangedUTC` DATETIME NULL DEFAULT NULL COMMENT 'Date Time UTC of last change',
  `ChangedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that changed the record',
  `ChangedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to device that changed the record',
  `ChangedProgID` INT(11) NULL DEFAULT '0' COMMENT 'ProgID of program that changed the record',
  `PatientID_ES` VARCHAR(16) NULL DEFAULT NULL,
  `SocSecNr_ES` VARCHAR(16) NOT NULL,
  `PatientFirstName_ES` VARCHAR(32) NULL DEFAULT NULL,
  `PatientMiddleName_ES` VARCHAR(16) NULL DEFAULT NULL,
  `PatientLastName_ES` VARCHAR(64) NULL DEFAULT NULL,
  `PatientDateOfBirth_EI` INT(11) NULL DEFAULT NULL,
  `PatientBirthYear` SMALLINT(6) NULL DEFAULT NULL,
  `PatientGender_IX` TINYINT(4) NULL DEFAULT NULL,
  `PatientRace_IX` SMALLINT(6) NULL DEFAULT NULL,
  `PatientHeightM` FLOAT(11) NULL DEFAULT NULL,
  `PatientWeightKg` FLOAT(11) NULL DEFAULT NULL,
  `PatientAddress_ES` VARCHAR(128) NULL DEFAULT NULL,
  `PatientZip_ES` VARCHAR(16) NULL DEFAULT NULL,
  `PatientHouseNumber_ES` VARCHAR(8) NULL DEFAULT NULL,
  `PatientCity` VARCHAR(32) NULL DEFAULT NULL,
  `PatientState` VARCHAR(32) NULL DEFAULT NULL,
  `PatientCountryCode` VARCHAR(4) NULL DEFAULT NULL,
  `PatientPhone1_ES` VARCHAR(16) NULL DEFAULT NULL,
  `PatientPhone2_ES` VARCHAR(16) NULL DEFAULT NULL,
  `PatientCell_ES` VARCHAR(16) NULL DEFAULT NULL,
  `PatientEmail_ES` VARCHAR(128) NULL DEFAULT NULL,
  `HistoryStringSet` VARCHAR(64) NULL DEFAULT NULL,
  `AllergiesStringSet` VARCHAR(64) NULL DEFAULT NULL,
  `MedicationStringSet` VARCHAR(64) NULL DEFAULT NULL,
  `SymptomsStringSet` VARCHAR(64) NULL DEFAULT NULL,
  `RemarkLabel` VARCHAR(32) NULL DEFAULT NULL,
  `RemarkText` TEXT NULL DEFAULT NULL,
  `PatientSkype_ES` VARCHAR(64) NULL DEFAULT NULL,
  `State_IX` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`d_patientinfo_Key`, `SocSecNr_ES`),
  UNIQUE INDEX `SocSecNr_ES_UNIQUE` (`SocSecNr_ES` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 26
DEFAULT CHARACTER SET = utf8
COMMENT = 'Patient Info record';

CREATE TABLE IF NOT EXISTS `nl0012soft`.`d_physicianinfo` (
  `d_physicianinfo_Key` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Key index holding record number\n',
  `Active` TINYINT(4) NULL DEFAULT '0' COMMENT 'Bool, indicates if record is active',
  `CreatedUTC` DATETIME NOT NULL COMMENT 'Record creation Date Time in UTC',
  `CreatedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that created the record',
  `CreatedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to the device that created the record',
  `CreatedProgID` INT(11) NOT NULL COMMENT 'Program ID( programNr, run, major, minor, builkd) that was used to create record\n',
  `ChangedUTC` DATETIME NULL DEFAULT NULL COMMENT 'Date Time UTC of last change',
  `ChangedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that changed the record',
  `ChangedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to device that changed the record',
  `ChangedProgID` INT(11) NULL DEFAULT '0' COMMENT 'ProgID of program that changed the record',
  `Label` VARCHAR(32) NULL DEFAULT NULL,
  `GenderTitle` VARCHAR(32) NULL DEFAULT NULL,
  `AcademicTitle` VARCHAR(32) NULL DEFAULT NULL,
  `FirstName_ES` VARCHAR(64) NULL DEFAULT NULL,
  `MiddleName_ES` VARCHAR(32) NULL DEFAULT NULL,
  `LastName_ES` VARCHAR(64) NULL DEFAULT NULL,
  `DateOfBirth_EI` INT(11) NULL DEFAULT NULL,
  `Gender_IX` TINYINT(4) NULL DEFAULT '0',
  `Hospital_IX` INT(11) NULL DEFAULT '0',
  `Address_ES` TEXT NULL DEFAULT NULL,
  `Zip_ES` VARCHAR(16) NULL DEFAULT NULL,
  `HouseNr_ES` VARCHAR(8) NULL DEFAULT NULL,
  `City` VARCHAR(32) NULL DEFAULT NULL,
  `State` VARCHAR(32) NULL DEFAULT NULL,
  `CountryCode` VARCHAR(4) NULL DEFAULT NULL,
  `Phone1_ES` VARCHAR(24) NULL DEFAULT NULL,
  `Phone2_ES` VARCHAR(24) NULL DEFAULT NULL,
  `Cell_ES` VARCHAR(24) NULL DEFAULT NULL,
  `Email_ES` VARCHAR(128) NULL DEFAULT NULL,
  `Skype_ES` VARCHAR(128) NULL DEFAULT NULL,
  PRIMARY KEY (`d_physicianinfo_Key`))
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8
COMMENT = ' Info record';
CREATE TABLE IF NOT EXISTS `nl0012soft`.`d_recanalysis` (
  `d_recAnalysis_Key` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Key index holding record number\n',
  `Active` TINYINT(4) NULL DEFAULT '0' COMMENT 'Bool, indicates if record is active',
  `CreatedUTC` DATETIME NOT NULL COMMENT 'Record creation Date Time in UTC',
  `CreatedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that created the record',
  `CreatedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to the device that created the record',
  `CreatedProgID` INT(11) NOT NULL COMMENT 'Program ID( programNr, run, major, minor, builkd) that was used to create record\n',
  `ChangedUTC` DATETIME NULL DEFAULT NULL COMMENT 'Date Time UTC of last change',
  `ChangedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that changed the record',
  `ChangedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to device that changed the record',
  `ChangedProgID` INT(11) NULL DEFAULT '0' COMMENT 'ProgID of program that changed the record',
  `Recording_IX` INT(11) NOT NULL,
  `SeqInRecording` SMALLINT(6) NOT NULL,
  `Study_IX` INT(11) NULL DEFAULT '0',
  `AnalysisClassSet` VARCHAR(64) NULL DEFAULT NULL,
  `AnalysisRemark` VARCHAR(250) NULL DEFAULT NULL,
  `SymptomsCodeSet` VARCHAR(64) NULL DEFAULT NULL,
  `SymptomsRemark` VARCHAR(250) NULL DEFAULT NULL,
  `ActivitiesCodeSet` VARCHAR(64) NULL DEFAULT NULL,
  `ActivitiesRemark` VARCHAR(250) NULL DEFAULT NULL,
  `State` SMALLINT(6) NULL DEFAULT '0',
  `Technician_IX` INT(11) NULL DEFAULT '0',
  `MeasuredUTC` DATETIME NULL DEFAULT NULL,
  `Physician_IX` INT(11) NULL DEFAULT '0',
  `ReportedUTC` DATETIME NULL DEFAULT NULL,
  `AnalysisRhythmSet` VARCHAR(64) NULL DEFAULT NULL,
  `ReportNr` smallint(6) DEFAULT NULL,
  `StudyReportNr` smallint(6) DEFAULT NULL,
  `StudyReportUTC` datetime DEFAULT NULL,
  `StudyReport_IX` int(11) DEFAULT NULL,
  PRIMARY KEY (`d_recAnalysis_Key`, `Recording_IX`, `SeqInRecording`))
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8
COMMENT = 'Name Info record';


CREATE TABLE IF NOT EXISTS `nl0012soft`.`d_studyinfo` (
  `d_studyinfo_Key` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Key index holding record number\n',
  `Active` TINYINT(4) NULL DEFAULT '0' COMMENT 'Bool, indicates if record is active',
  `CreatedUTC` DATETIME NOT NULL COMMENT 'Record creation Date Time in UTC',
  `CreatedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that created the record',
  `CreatedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to the device that created the record',
  `CreatedProgID` INT(11) NOT NULL DEFAULT '0' COMMENT 'Program ID( programNr, run, major, minor, builkd) that was used to create record\n',
  `ChangedUTC` DATETIME NULL DEFAULT NULL COMMENT 'Date Time UTC of last change',
  `ChangedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that changed the record',
  `ChangedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to device that changed the record',
  `ChangedProgID` INT(11) NULL DEFAULT '0' COMMENT 'ProgID of program that changed the record',
  `StudyRemarkLabel` VARCHAR(32) NULL DEFAULT NULL,
  `Client_IX` INT(11) NULL DEFAULT NULL,
  `StudyStartDate` DATE NOT NULL,
  `HospitalRoom` VARCHAR(16) NULL DEFAULT NULL,
  `HospitalBed` VARCHAR(8) NULL DEFAULT NULL,
  `Physisian_IX` INT(11) NULL DEFAULT NULL,
  `RefPhysisian_IX` INT(11) NULL DEFAULT NULL,
  `StudyRecorder_IX` INT(11) NULL DEFAULT NULL,
  `StudyMonitoringDays` SMALLINT(6) NULL DEFAULT NULL,
  `StudyTypeCode` VARCHAR(16) NULL DEFAULT NULL,
  `ReportIntervalSet` VARCHAR(32) NULL DEFAULT NULL,
  `StudyPhysicianInstruction` TEXT NULL DEFAULT NULL,
  `StudyProcCodeSet` VARCHAR(64) NULL DEFAULT NULL,
  `NrRecords` SMALLINT(6) NULL DEFAULT NULL,
  `NrReports` SMALLINT(6) NULL DEFAULT NULL,
  `StudyState` SMALLINT(6) NULL DEFAULT '0',
  `Patient_IX` INT(11) NULL DEFAULT '0',
  `BaseLineAnalysis_IX` INT(11) NULL DEFAULT '0',
  PRIMARY KEY (`d_studyinfo_Key`))
ENGINE = InnoDB
AUTO_INCREMENT = 21
DEFAULT CHARACTER SET = utf8
COMMENT = 'Study Info record';

CREATE TABLE IF NOT EXISTS `nl0012soft`.`e_studyreportinterval` (
  `e_StudyReportInterval_Key` INT(11) NOT NULL COMMENT 'Key index holding record number\n',
  `Active` TINYINT(4) NULL DEFAULT '0' COMMENT 'Bool, indicates if record is active',
  `CreatedUTC` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Record creation Date Time in UTC',
  `CreatedBy_IX` INT(11) UNSIGNED NULL DEFAULT '0' COMMENT 'Index to user that created the record',
  `CreatedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to the device that created the record',
  `CreatedProgID` INT(11) NOT NULL COMMENT 'Program ID( programNr, run, major, minor, builkd) that was used to create record\n',
  `ChangedUTC` DATETIME NULL DEFAULT NULL COMMENT 'Date Time UTC of last change',
  `ChangedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that changed the record',
  `ChangedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to device that changed the record',
  `ChangedProgID` INT(11) NULL DEFAULT '0' COMMENT 'ProgID of program that changed the record',
  `SortValue` TINYINT(4) NULL DEFAULT '0',
  `GroupValue` TINYINT(4) NULL DEFAULT '0',
  `FloatValue` FLOAT(11) NULL DEFAULT '0',
  `Code` CHAR(16) NULL DEFAULT NULL COMMENT 'Code for enum (used for Set)\n',
  `Label` CHAR(250) NOT NULL,
  PRIMARY KEY (`e_StudyReportInterval_Key`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'enum list for Study Report Interval (set)';

CREATE TABLE IF NOT EXISTS `nl0012soft`.`e_studytype` (
  `e_studytype_Key` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Key index holding record number\n',
  `Active` TINYINT(4) NULL DEFAULT '0' COMMENT 'Bool, indicates if record is active',
  `CreatedUTC` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Record creation Date Time in UTC',
  `CreatedBy_IX` INT(11) UNSIGNED NULL DEFAULT '0' COMMENT 'Index to user that created the record',
  `CreatedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to the device that created the record',
  `CreatedProgID` INT(11) NOT NULL COMMENT 'Program ID( programNr, run, major, minor, builkd) that was used to create record\n',
  `ChangedUTC` DATETIME NULL DEFAULT NULL COMMENT 'Date Time UTC of last change',
  `ChangedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that changed the record',
  `ChangedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to device that changed the record',
  `ChangedProgID` INT(11) NULL DEFAULT '0' COMMENT 'ProgID of program that changed the record',
  `SortValue` TINYINT(4) NULL DEFAULT '0',
  `GroupValue` TINYINT(4) NULL DEFAULT '0',
  `FloatValue` FLOAT(11) NULL DEFAULT '0',
  `Code` CHAR(16) NULL DEFAULT NULL COMMENT 'Code for enum (used for Set)\n',
  `Label` CHAR(250) NOT NULL,
  PRIMARY KEY (`e_studytype_Key`))
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = utf8
COMMENT = 'enum list for StudyType';

ALTER TABLE `nl0012soft`.`d_record` 
DROP COLUMN `recordcol`,
ADD COLUMN `DeviceMode` VARCHAR(32) NULL DEFAULT NULL COMMENT 'New 20161212' AFTER `ChangedProgID`,
ADD COLUMN `SeqNrInStudy` SMALLINT(6) NULL DEFAULT '0' COMMENT 'New 20161212\nRecord sequence number within study (starts at 1) / Study.NrRecords\n' AFTER `DeviceMode`,
ADD COLUMN `NrAnalysis` SMALLINT(6) NULL DEFAULT '0' COMMENT 'New 20161212' AFTER `SeqNrInStudy`,
ADD COLUMN `InvertChannelsMask` INT(11) NULL DEFAULT NULL AFTER `NrAnalysis`,
ADD COLUMN `ExtraData` TEXT NULL DEFAULT NULL AFTER `InvertChannelsMask`,
ADD COLUMN `SignalUnits` VARCHAR(128) NULL DEFAULT NULL AFTER `ExtraData`,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`d_record_Key`, `DeviceID`, `ReadUTC`), 
COMMENT = 'Stores vital Recording data.\nThe recording itself is stored at Recording:\\YM<readUTC.YYYYMM>\\D<read.DD>\\<Stored_AT>\\' ;

ALTER TABLE `nl0012soft`.`d_record` 
CHANGE COLUMN `SignalLabels` `SignalLabels` VARCHAR(128) NULL DEFAULT NULL COMMENT 'List of signal names in recording seperated by \',\'';


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

ALTER TABLE `nl0012soft`.`d_record` 
CHANGE COLUMN `SignalLabels` `SignalLabels` VARCHAR(128) NULL DEFAULT NULL COMMENT 'List of signal names in recording seperated by \',\'';
