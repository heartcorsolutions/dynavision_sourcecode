-- MySQL Workbench Synchronization

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

ALTER TABLE `nl0012soft`.`d_clientinfo` 
CHANGE COLUMN `HouseNr` `HouseNr` VARCHAR(16) NULL DEFAULT NULL COMMENT '20190304:Naar 16 chars' ;

ALTER TABLE `nl0012soft`.`d_deviceinfo` 
ADD COLUMN `Client_IX` INT(11) NULL DEFAULT '0' AFTER `RecorderEndUTC`,
ADD COLUMN `AddStudyMode` SMALLINT(6) NULL DEFAULT '0' AFTER `Client_IX`,
ADD COLUMN `RefIdMode` SMALLINT(6) NULL DEFAULT '0' AFTER `AddStudyMode`,
ADD COLUMN `DeployText` TEXT NULL DEFAULT NULL AFTER `RefIdMode`,
ADD COLUMN `ServiceText` TEXT NULL DEFAULT NULL AFTER `DeployText`,
ADD COLUMN `NextStateMode` SMALLINT(6) NULL DEFAULT '0' AFTER `ServiceText`,
ADD COLUMN `TimeZoneMode` SMALLINT(6) NULL DEFAULT '0' AFTER `NextStateMode`,
ADD COLUMN `TimeZoneDefaultMin` SMALLINT(6) NULL DEFAULT '0' AFTER `TimeZoneMode`,
ADD COLUMN `TimeZoneOffsetMin` SMALLINT(6) NULL DEFAULT '0' AFTER `TimeZoneDefaultMin`,
ADD COLUMN `IReaderNr` SMALLINT(6) NULL DEFAULT '0' AFTER `TimeZoneOffsetMin`;

ALTER TABLE `nl0012soft`.`d_patientinfo` 
CHANGE COLUMN `PatientHouseNumber_ES` `PatientHouseNumber_ES` VARCHAR(16) NULL DEFAULT NULL ;

ALTER TABLE `nl0012soft`.`d_pdfdocrec` 
CHANGE COLUMN `StudySubNr` `StudySubNr` SMALLINT(6) NULL DEFAULT NULL ;

ALTER TABLE `nl0012soft`.`d_physicianinfo` 
CHANGE COLUMN `HouseNr_ES` `HouseNr_ES` VARCHAR(16) NULL DEFAULT NULL COMMENT '20190325 Naar 16 chars' ;

ALTER TABLE `nl0012soft`.`d_recanalysis` 
CHANGE COLUMN `ReportedUTC` `ReportedUTC` DATETIME NULL DEFAULT NULL COMMENT 'Used as strip start when multiple analysis in one record and in one STAT Analyze screen';

ALTER TABLE `nl0012soft`.`d_record` 
ADD COLUMN `Trial_IX` INT(11) NULL DEFAULT '0' AFTER `SignalUnits`,
ADD COLUMN `StudyPermissions` BIGINT(20) NULL DEFAULT '0' AFTER `Trial_IX`,
ADD COLUMN `Client_IX` INT(11) NULL DEFAULT '0' AFTER `StudyPermissions`;

ALTER TABLE `nl0012soft`.`d_studyinfo` 
ADD COLUMN `StudyPermissions` BIGINT(20) NULL DEFAULT '0' AFTER `RecorderEndUTC`,
ADD COLUMN `Trial_IX` INT(11) NULL DEFAULT '0' AFTER `StudyPermissions`;


CREATE TABLE IF NOT EXISTS `nl0012soft`.`d_trialinfo` (
  `d_TrialInfo_Key` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Key index holding record number\n',
  `Active` TINYINT(4) NULL DEFAULT '0' COMMENT 'Bool, indicates if record is active',
  `CreatedUTC` DATETIME NOT NULL COMMENT 'Record creation Date Time in UTC',
  `CreatedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that created the record',
  `CreatedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to the device that created the record',
  `CreatedProgID` INT(11) NOT NULL COMMENT 'Program ID( programNr, run, major, minor, builkd) that was used to create record\n',
  `ChangedUTC` DATETIME NULL DEFAULT NULL COMMENT 'Date Time UTC of last change',
  `ChangedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that changed the record',
  `ChangedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to device that changed the record',
  `ChangedProgID` INT(11) NULL DEFAULT '0' COMMENT 'ProgID of program that changed the record',
  `Label` VARCHAR(32) NOT NULL,
  `SponsorClient_IX` INT(11) NULL DEFAULT '0',
  `TrialStartDate` DATE NOT NULL,
  `TrialStopDate` DATE NOT NULL,
  `ReportIntervalSet` VARCHAR(32) NULL DEFAULT NULL,
  `StudyFreeText` TEXT NULL DEFAULT NULL,
  `StudyPermissions` BIGINT(20) NULL DEFAULT '0',
  `TrialState` SMALLINT(6) NULL DEFAULT NULL,
  `StudyMonitoringDays` SMALLINT(6) NULL DEFAULT '0',
  `StudyTypeCode` VARCHAR(16) NULL DEFAULT NULL,
  `StudyProcCodeSet` VARCHAR(64) NULL DEFAULT NULL,
  `MctIntervalSet` VARCHAR(32) NULL DEFAULT NULL,
  `StudyRemarkLabel` VARCHAR(32) NULL DEFAULT NULL,
  PRIMARY KEY (`d_TrialInfo_Key`),
  UNIQUE INDEX `Label_UNIQUE` (`Label` ASC),
  UNIQUE INDEX `d_TrialInfo_Key_UNIQUE` (`d_TrialInfo_Key` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8
COMMENT = 'Trial Info record';

CREATE TABLE IF NOT EXISTS `nl0012soft`.`e_studypermissions` (
  `e_StudyPermissions_Key` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Key index holding record number\n',
  `Active` TINYINT(4) NULL DEFAULT '0' COMMENT 'Bool, indicates if record is active',
  `CreatedUTC` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Record creation Date Time in UTC',
  `CreatedBy_IX` INT(11) UNSIGNED NULL DEFAULT '0' COMMENT 'Index to user that created the record',
  `CreatedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to the device that created the record',
  `CreatedProgID` INT(11) NOT NULL COMMENT 'Program ID( programNr, run, major, minor, builkd) that was used to create record\n',
  `ChangedUTC` DATETIME NULL DEFAULT NULL COMMENT 'Date Time UTC of last change',
  `ChangedBy_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to user that changed the record',
  `ChangedDevice_IX` INT(11) NULL DEFAULT '0' COMMENT 'Index to device that changed the record',
  `ChangedProgID` INT(11) NULL DEFAULT '0' COMMENT 'ProgID of program that changed the record',
  `SortValue` TINYINT(4) NULL DEFAULT '0',
  `GroupValue` TINYINT(4) NULL DEFAULT '0',
  `FloatValue` FLOAT(11) NULL DEFAULT '0',
  `Code` CHAR(16) NULL DEFAULT NULL COMMENT 'Code for enum (used for Set)\n',
  `Label` CHAR(250) NOT NULL,
  PRIMARY KEY (`e_StudyPermissions_Key`))
ENGINE = InnoDB
AUTO_INCREMENT = 20
DEFAULT CHARACTER SET = utf8
COMMENT = 'enum list for StudyType';


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
