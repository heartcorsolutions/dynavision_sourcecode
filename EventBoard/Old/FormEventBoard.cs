﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Program_Base;
using Event_Base;
using System.IO;
using System.Diagnostics;
using EventboardEntryForms;
using EventBoardEntryForms;

namespace EventBoard
{
    enum DTriageGridField
    {
           PSA, Patient,Event, Recorded, Remark, EventStrip, Triage, Index, State, FilePath, FileName, NrSignals, Study, Active, ModelNr, NrGridFields
    }

    enum DTriageViewMode
    {
        Triage, Analyze, Report, Seperator, ListAll, ListRead, ListNormal, ListNoise, ListLeadOff, ListBaseLine, ListTriaged, ListAnalyzed, ListReported,
        NrViewModes
    }

    public partial class FormEventBoard : Form
    {
        private String mRecordingDir;
        private string mHeaViewExe;
        private string mScpViewExe;
        private CSqlDBaseConnection mDBaseServer;

        private CEncryptedString mSqlServer = null;
        private CEncryptedInt mSqlPort = null;
        private CEncryptedString mSqlDbName = null;
        private CEncryptedString mSqlUser = null;
        private CEncryptedString mSqlPassword = null;
        Font mTriageFont = null;
        private bool mbShowTriageFields = false;

        private DateTime mLastTriageUpdateDT;
        private DateTime mLastActivityWriteDT;
        private int mTriageUpdSec = 0;
        private DateTime mLastTriageActivityDT;

        private string mCurrentPatientID;

        CImageCach mImageCach = null;

        Image mTriageImgOther = null;
        Image mTriageImgRead = null;
        Image mTriageImgTriaged = null;
        Image mTriageImgNoise = null;
        Image mTriageImgNormal = null;
        Image mTriageImgLeadOff = null;

        int mWidthTriageStripNormal = 0;
        int mWidthScreenNormal = 0;


        bool mbUpdatingGrid = false;
        bool mbUpdatingTriage = false;

        private void UserControl1_Paint(object sender, PaintEventArgs e)

        {
            Panel pn = sender as Panel;

            if (pn != null)
            {
                Rectangle rectangle = pn.RectangleToScreen(pn.ClientRectangle);

                ControlPaint.DrawBorder(e.Graphics, rectangle, Color.White, ButtonBorderStyle.Solid);
            }
        }

        public FormEventBoard()
        {
            try
            {
                InitializeComponent();

                mLastActivityWriteDT = DateTime.Now;
                mLastTriageActivityDT = DateTime.Now;

                //panel11.Paint += new PaintEventHandler(UserControl1_Paint);
                CProgram.sSetProgramMainForm(this, "");
                mRecordingDir = Path.GetFullPath(Properties.Settings.Default.RecordingDir);

                mSqlDbName = new CEncryptedString("SqlDbName", DEncryptLevel.L1_Program, 32);
                if (mSqlDbName != null && mSqlDbName.mbSetEncrypted(Properties.Settings.Default.SqlDbName))
                {
                    Properties.Settings.Default.SqlDbName = mSqlDbName.mGetEncrypted(); // store after incription has changed
                }

                string centerName = mSqlDbName.mDecrypt();
                string centerFileName = centerName + ".CenterName";
                // check if directory points to the correct center
                if (false == File.Exists(Path.Combine(mRecordingDir, centerFileName)))
                {
                    if (false == File.Exists(Path.Combine(mRecordingDir, "..\\" + centerFileName)))
                    {
                        CProgram.sLogError("Center=" + centerName + ", recording dir = " + mRecordingDir);
                        CProgram.sPromptError(true, "Config error!", "Center name not set correctly in recording dir, check configuration!! ");
                        mSqlDbName.mbEncrypt("----");
                        Close();
                        return;
                    }
                }
                labelDrive.Text = mRecordingDir.Substring(0, 2);
                string orgLabel = CProgram.sGetOrganisationLabel();
                if (centerName.ToUpper() != orgLabel.ToUpper())
                {
                    CProgram.sLogError("Center= " + centerName + ", Org = " + orgLabel);
                    CProgram.sPromptError(true, "Config error!", "Center name not set equal to Org Label, check configuration!! ");
                    mSqlDbName.mbEncrypt("----");
                    Close();
                    return;
                }

                string progDir = CProgram.sGetProgDir();
                // check center name in program dir
 /*               if (false == File.Exists(Path.Combine(progDir, "..\\..\\" + centerFileName)))
                {
                    if (false == File.Exists(Path.Combine(progDir, "..\\" + centerFileName)))
                    {
                        CProgram.sLogError("Center=" + centerName + ", recording dir = " + mRecordingDir);
                        CProgram.sPromptError(true, "Config error!", "Center name not set correctly in program dir, check configuration!! ");
                        mSqlDbName.mbEncrypt("----");
                        Close();
                        return;
                    }
                }
*/                //                Text = CProgram.sGetProgTitle() + " " + centerName; // set title
                // CProgram.sbSetProgramOrganisation(centerName, CProgram.sGetDeviceID()); // for now set organisation to center
                CProgram.sSetProgTitleFormat("DVTMS", true, CProgram.sGetProgVersionLevel(), DShowOrganisation.First, "");
                CProgram.sSetProgramTitle("", true);

                mHeaViewExe = Properties.Settings.Default.HeaViewExe;
                mScpViewExe = Properties.Settings.Default.ScpViewExe;

                mSqlServer = new CEncryptedString("SqlServer", DEncryptLevel.L1_Program, 64);
                if (mSqlServer != null && mSqlServer.mbSetEncrypted(Properties.Settings.Default.SqlServer))
                {
                    Properties.Settings.Default.SqlServer = mSqlServer.mGetEncrypted(); // store after incription has changed
                }
                mSqlPort = new CEncryptedInt("SqlPort", DEncryptLevel.L1_Program);
                if (mSqlPort != null && mSqlPort.mbSetEncrypted(Properties.Settings.Default.SqlPort))
                {
                    Properties.Settings.Default.SqlPort = mSqlPort.mGetEncryptedInt(); // store after incription has changed
                }
                mSqlUser = new CEncryptedString("SqlUser", DEncryptLevel.L1_Program, 32);
                if (mSqlUser != null && mSqlUser.mbSetEncrypted(Properties.Settings.Default.SqlUser))
                {
                    Properties.Settings.Default.SqlUser = mSqlUser.mGetEncrypted(); // store after incription has changed
                }
                mSqlPassword = new CEncryptedString("SqlPassword", DEncryptLevel.L1_Program, 32);
                if (mSqlPassword != null && mSqlPassword.mbSetEncrypted(Properties.Settings.Default.SqlPassword))
                {
                    Properties.Settings.Default.SqlPassword = mSqlPassword.mGetEncrypted(); // store after incription has changed
                }

                string dateFormat = Properties.Settings.Default.DateFormat;
                string timeFormat = Properties.Settings.Default.TimeFormat;
                bool bFloatWithDot = Properties.Settings.Default.FloatWithDot;
                bool bImperial = Properties.Settings.Default.ImperialUnits;

                CProgram.sSetShowDateTimeFormat(dateFormat, timeFormat);
                CProgram.sSetImperial(bImperial);
                CProgram.sSetUserDecimal(bFloatWithDot ? DFloatDecimal.Dot : DFloatDecimal.Comma);

                int triageNrHours = Properties.Settings.Default.TriageNrHours;
                if (triageNrHours < 1) triageNrHours = 1;

                if (triageNrHours % 24 == 0) { triageNrHours /= 24; listBoxViewUnit.TopIndex = 1; }
                else { listBoxViewUnit.TopIndex = 0; }
                textBoxViewTime.Text = triageNrHours.ToString();

                string triageMode = Properties.Settings.Default.TriageMode;
                int modeIndex = listBoxEventMode.Items.IndexOf(triageMode);     // make a  DTriageViewMode from string
                if (modeIndex < 0) modeIndex = 0;
                //if ( DTriageViewMode)
                listBoxEventMode.SelectedIndex = modeIndex;     // D

                mTriageUpdSec = Properties.Settings.Default.TriageUpdSec;

                mUpdateTriageViewMode();
                mUpdateTriageStrips();
                mUpdateTriageTime(DateTime.Now);
                labelUpdateTS.Text = ".";
                labelUpdateTC.Text = ".";
                //                listBoxViewUnit.SelectedIndex = 0;
                //                listBoxEventMode.SelectedIndex = 0;
                mTriageFont = new Font("Courier New", 10.2F, FontStyle.Regular);

                UInt16 maxTryLoad = (UInt16)Properties.Settings.Default.ImgCachRetryCount;
                double updSec = Properties.Settings.Default.ImgCachUpdSec;
                UInt16 nrThreads = (UInt16)Properties.Settings.Default.ImgCachNrThreads;
                UInt16 maxHours = (UInt16)Properties.Settings.Default.ImgMaxCachHours;
                UInt16 simLoadDelay = (UInt16)Properties.Settings.Default.ImgCachDelayTck;
#if DEBUG
                simLoadDelay += 0;
#endif
                if (simLoadDelay > 0)
                {
                    CProgram.sLogLine("ImageCach simulating delay TCK = " + simLoadDelay.ToString());
                }
                mLoadTriageButtonImages();
                mImageCach = new CImageCach(updSec, maxTryLoad, nrThreads, maxHours, simLoadDelay);
                if (mImageCach != null)
                {
                    Image img = mLoadSingleImageFile(Path.Combine(CProgram.sGetProgDir(), "Icons\\TriageStripEmpty.png"));
                    CImageCach.sSetEmptyImage(img);
                }
                UInt16 priority = (UInt16)DPriority.High;
                buttonHigh.Text = CRecordMit.sGetPriorityValue(priority).ToString() + " " + CRecordMit.sGetPriorityText(priority);
                priority = (UInt16)DPriority.Normal;
                buttonNormal2.Text = CRecordMit.sGetPriorityValue(priority).ToString() + " " + CRecordMit.sGetPriorityText(priority);
                priority = (UInt16)DPriority.Low;
                buttonLow.Text = CRecordMit.sGetPriorityValue(priority).ToString() + " " + CRecordMit.sGetPriorityText(priority);


                mWidthTriageStripNormal = dataGridTriage.Columns[(int)DTriageGridField.EventStrip].Width;
                mWidthScreenNormal = Size.Width;
                mDisableUnactive();


                if(mbCreateDBaseConnection())
                {
                    CDvtmsData.sSetDBaseServer(mDBaseServer);
                }
                else
                {
                    CProgram.sPromptError(true, CProgram.sMakeProgTitle("Startup", true), "Failed to connect to dBase, please check connection and settings!");
                    Close();
                }
            }
            catch (Exception ex)
            {
                CProgram.sPromptException(true, "MainForm", "Exception during Main Form constructor", ex);
            }
        }

        private void mTriageUpdateReset()
        {
            if( mTriageUpdSec > 0 )
            {
                mLastTriageActivityDT = DateTime.Now;
            }
        }

        private void mDisableUnactive()
        {
            //toolStripTop.Enabled = false;
            buttonRefresh.Enabled = false;
            labelDate.Text = "-";
            labelTime.Text = "-";
            //panelView.Enabled = false;
            panelPriority.Enabled = false;
            buttonAnalyzeAdd.Enabled = false;
        }

        Image mLoadSingleImageFile(string AFullName)
        {
            Image img = null;
            try
            {
                img = Image.FromFile(AFullName);
            }
            catch (Exception /*ex*/)
            {
                CProgram.sLogError("Failed loading image file " + AFullName);
                img = null;
            }
            return img;
        }
        public void mLoadTriageButtonImages()
        {
            string imgPath = Path.Combine(CProgram.sGetProgDir(), "Icons\\Triage\\");

            mTriageImgOther = mLoadSingleImageFile(Path.Combine(imgPath, "TriageButtonInactiveShade.png"));
            mTriageImgRead = mLoadSingleImageFile(Path.Combine(imgPath, "TriageButtonIdleShade.png"));
            mTriageImgTriaged = mLoadSingleImageFile(Path.Combine(imgPath, "TriageButtonAnalysShade.png"));
            mTriageImgNoise = mLoadSingleImageFile(Path.Combine(imgPath, "TriageButtonNoiseShade.png"));
            mTriageImgNormal = mLoadSingleImageFile(Path.Combine(imgPath, "TriageButtonNormalShade.png"));
            mTriageImgLeadOff = mLoadSingleImageFile(Path.Combine(imgPath, "TriageButtonLeadoffShade.png"));
        }

        public void mShowBusy(bool AbBusy)
        {
            this.Cursor = AbBusy ? Cursors.WaitCursor : Cursors.Default;
        }
        public string mPathCombine(string APath, string AFileName)
        {
            string s = APath;
            int n = APath == null ? 0 : s.Length;
            if (n > 0 && s[n - 1] != '\\')
            {
                s += "\\";
            }
            return s + AFileName;
        }
        public string mPathCombine(string APath, string ADir1, string ADir2, string AFileName)
        {
            string s = APath;
            int n = APath == null ? 0 : s.Length;
            if (n > 0 && s[n - 1] != '\\')
            {
                s += "\\";
            }
            return s + ADir1 + "\\" + ADir2 + "\\" + AFileName;
        }

        private bool mbCreateDirectory(String ADirName)
        {
            bool bOk = Directory.Exists(ADirName);

            if (bOk == false)
            {
                Directory.CreateDirectory(ADirName);

                bOk = Directory.Exists(ADirName);
            }
            return bOk;
        }
        public bool mbCreateDBaseConnection()
        {
            bool bOk = mDBaseServer != null;

            if (false == bOk)
            {
                if (mSqlServer != null && mSqlPort != null && mSqlUser != null && mSqlPassword != null)
                {
                    mDBaseServer = new CSqlDBaseConnection();

                    if (mDBaseServer != null)
                    {
                        string server = mSqlServer.mDecrypt();
                        int port = mSqlPort.mDecrypt();
                        string user = mSqlUser.mDecrypt();
                        string password = mSqlPassword.mDecrypt();
                        string dbName = mSqlDbName.mDecrypt();

                        bOk = mDBaseServer.mbSqlSetupServer(server, port, false, dbName, user, password);
                        if (false == bOk)
                        {
                            CProgram.sLogError("Invalid settings for server " + server);
                        }
                        else
                        {
                            bOk = mDBaseServer.mbSqlCreateConnection();
                            if (false == bOk)
                            {
                                CProgram.sLogError("Failed connection to server " + server);
                            }
                            else
                            {
                                CProgram.sLogLine("Connected to dBase " + mDBaseServer.mSqlGetServerName() + "." + mDBaseServer.mSqlGetDBaseName());
                            }
                        }
                    }

                }
                if (false == bOk)
                {
                    mDBaseServer.mSqlDisposeConnection();
                    mDBaseServer = null;
                }
            }
            return bOk;
        }

        public void mDisposeDBaseConnection()
        {
            if (mDBaseServer != null)
            {
                mDBaseServer.mSqlDisposeConnection();
                CProgram.sLogLine("DisConnected from dBase " + mDBaseServer.mSqlGetServerName() + "." + mDBaseServer.mSqlGetDBaseName());
                mDBaseServer = null;
            }
        }

        public bool mbAddSqlDbRow(out int ArIndex, CRecordMit ARec)
        {
            bool bOk = false;
            int index = 0;

            if (ARec != null)
            {
                try
                {
                    if (mbCreateDBaseConnection())
                    {
                        CSqlCmd cmd = ARec.mGetSqlCmd(mDBaseServer);

                        if (cmd == null)
                        {
                            CProgram.sLogLine("Failed to init cmd");

                        }
                        else if (false == cmd.mbPrepareInsertCmd(ARec.mMaskValid, true, true))
                        {
                            CProgram.sLogLine("Failed to prepare SQL row in table " + ARec.mGetDbTableName());
                        }
                        else if (cmd.mbExecuteInsertCmd())
                        {
                            index = (int)ARec.mIndex_KEY;   // return new key
                            bOk = true;
                            CProgram.sLogLine("Successfull insert in " + ARec.mGetDbTableName() + "[" + ARec.mIndex_KEY + "]");
                        }
                        else
                        {
                            CProgram.sLogLine("Failed to add SQL row in table " + ARec.mGetDbTableName());
                        }
                        cmd.mCloseCmd();
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Failed add Row", ex);
                }
            }
            ArIndex = index;
            mTriageUpdateReset();
            return bOk;
        }

        /*        public int mGetAgeYears(DateTime ADateTime)
                {
                    int age = 0;

                    if (ADateTime != null && ADateTime > DateTime.MinValue)
                    {
                        DateTime dt = DateTime.Now;

                        age = dt.Year - ADateTime.Year;

                        if (ADateTime.Month < dt.Month
                            || (ADateTime.Month == dt.Month && ADateTime.Day < dt.Day))
                        {
                            --age;
                        }
                    }
                    return age;
                }
        */
        /*       public int mGetAgeYears(int ABirthDay)
               {
                   int age = 0;

                   if (ABirthDay > 0)
                   {
                       int year = ABirthDay / 10000;
                       int month = (ABirthDay / 100) % 100;
                       int day = ABirthDay % 100;
                       DateTime dt = DateTime.Now;

                       age = dt.Year - year;

                       if (month < dt.Month
                           || (month == dt.Month && day < dt.Day))
                       {
                           --age;
                       }
                   }
                   return age;
               }
       */
        public string mGetStateString(UInt16 APriority, UInt16 ARecState, UInt16 ATableRowState, UInt16 AEventNr, UInt16 ANrEvents, UInt16 AToDo, UInt16 ADone)
        {
            //            string s = APriority.ToString() + " " + CRecordMit.sGetPriorityLetters(APriority) + "\n" + CRecordMit.sGetRecStateChar(ARecState) + "\n"
            //                    + CSqlDataTableRow.sGetActiveChar(ATableRowState) + "\n" + AEventNr.ToString() + "/" + ANrEvents.ToString();
            bool b = ANrEvents == 0;
            string s = CRecordMit.sGetPriorityValue(APriority).ToString() + " " + CRecordMit.sGetPriorityLetters(APriority) + " " //+ CRecordMit.sGetRecStateChar(ARecState) + "  "
                    //+ CSqlDataTableRow.sGetActiveChar(ATableRowState)
                    + "\n# " + AEventNr.ToString() + " / " + (b ? "-" : ANrEvents.ToString())
                    + "\nToDo: " + (b ? "-" : AToDo.ToString())
                    + "\nDone: " + (b ? "-" : ADone.ToString())
                    ;
            return s;
        }

        public string mGetTriageButtonTextOld(DRecState AState)
        {
            string noise = "Noise ";
            string normal = "Normal";
            string s1 = "──────";
            string leadOff = "Lead off";
            string analyze = "Analyze ";
            string s2 = "───────";

            switch ((DRecState)AState)
            {
                //                case DRecState.Unknown:     
                //                case DRecState.Recording: 
                //                case DRecState.Recorded: 
                //                case DRecState.Read: 
                //                case DRecState.Processed: 
                case DRecState.Noise: noise = noise.ToUpper(); break;
                case DRecState.LeadOff: leadOff = leadOff.ToUpper(); break;
                case DRecState.Normal: normal = normal.ToUpper(); break;
                //case DRecState.BaseLine: 
                case DRecState.Triaged: analyze = analyze.ToUpper(); break;
                    //case DRecState.Analyzed: 
                    //                case DRecState.Reported: 
            }
            string s = noise + " │ " + leadOff + " \n" + s1 + "─┼─" + s2 + "\n" + normal + " │ " + analyze;

            return s;
        }

        public Image mGetTriageButtonImage(DRecState AState)
        {
            switch ((DRecState)AState)
            {
                //                case DRecState.Unknown:     
                //                case DRecState.Recording: 
                //                case DRecState.Recorded: 
                case DRecState.Received:
                    return mTriageImgRead;
                case DRecState.Processed:
                    return mTriageImgRead;
                case DRecState.Noise:
                    return mTriageImgNoise;
                case DRecState.LeadOff:
                    return mTriageImgLeadOff;
                case DRecState.Normal:
                    return mTriageImgNormal;
                //case DRecState.BaseLine: 
                case DRecState.Triaged:
                    return mTriageImgTriaged;
                    //case DRecState.Analyzed: 
                    //                case DRecState.Reported: 
            }
            return mTriageImgOther;
        }

        public void mUpdateGridRowState(DataGridViewRow AGridRow)
        {
            if (AGridRow != null)
            {
                string stateStr = AGridRow.Cells[(int)DTriageGridField.State].Value.ToString();
                if (stateStr.Length > 2) stateStr = stateStr.Substring(0, 2);
                UInt16 state = 0;
                UInt16.TryParse(stateStr, out state);

                string priorityStr = AGridRow.Cells[(int)DTriageGridField.PSA].Value.ToString();
                UInt16 priority = 0;
                priorityStr = priorityStr.Substring(0, 1);
                UInt16.TryParse(priorityStr, out priority);

                string activeStr = AGridRow.Cells[(int)DTriageGridField.Active].Value.ToString();
                UInt16 active = 0;
                UInt16.TryParse(activeStr, out active);

                UInt16 countIndex = 1;
                UInt16 countTotal = 0; // future info from study or record list
                UInt16 countToDo = 0;
                UInt16 countDone = 0;
                string s = mGetStateString(priority, state, active, countIndex, countTotal, countToDo, countDone);

                AGridRow.Cells[(int)DTriageGridField.PSA].Value = s;

                //s = mGetTriageButtonText((DRecState)state);
                Image img = mGetTriageButtonImage((DRecState)state);
                AGridRow.Cells[(int)DTriageGridField.Triage].Value = img;

                AGridRow.DefaultCellStyle.BackColor = CRecordMit.sGetRecStateBgColor(state);
            }
            mTriageUpdateReset();
        }

        public string mFirstWord(string ALine)
        {
            string s = "";
            int n = ALine == null ? 0 : ALine.Length;
            int l = 0;

            for (int i = 0; i < n; ++i)
            {
                char c = ALine[i];
                if (char.IsLetterOrDigit(c))
                {
                    s += c;
                    ++l;
                }
                else if (l > 0)
                {
                    break;
                }
            }
            return s;
        }


        bool mbTriageStoreGrid(bool AbDirectLoad, CRecordMit ARec, bool AbLoadImage, string ACursorIndexKey)
        {
            bool bOk = true;
            UInt16 countIndex = 1;
            UInt16 countTotal = 0; // future info from study or record list
            UInt16 countToDo = 0;
            UInt16 countDone = 0;
            string indexKey = ARec.mIndex_KEY.ToString();
            bool bCursor = ACursorIndexKey != null && ACursorIndexKey.Length > 0 && ACursorIndexKey == indexKey;

            int n = dataGridTriage.Rows.Count;

            string eventTimeString = "", patientDateString = "";
            DateTime dt = ARec.mGetEventUTC();
            //            string patientName = ARec.mPatientTotalName.mDecrypt();
            string state = ARec.mRecState.ToString("D2") + "\n" + CRecordMit.sGetStateUserString(ARec.mRecState);
            string converter = CRecordMit.sGetConverterUserString(ARec.mImportConverter);
            string method = CRecordMit.sGetMethodUserString(ARec.mImportMethod);

            if (dt > DateTime.MinValue) eventTimeString = CProgram.sDateTimeToString(ARec.mGetDeviceTime(dt));
            if (ARec.mPatientBirthDate.mbNotZero()) patientDateString = CProgram.sDateToString(ARec.mPatientBirthDate.mDecryptToDate());

            dt = CProgram.sDateTimeToLocal(ARec.mReceivedUTC);
            string read = CProgram.sDateTimeToFormatString(dt, "yyyy/MM/dd HH:mm:ss");

            if (ARec.mEventUTC > DateTime.MinValue)
            {
                int min = (int)((ARec.mReceivedUTC - ARec.mEventUTC).TotalMinutes + 0.499);

                if (min >= 30)
                {
                    read += "\nE+" + (min / 60).ToString() + ":" + (min % 60).ToString("00");
                }
            }
            if (ARec.mTransmitUTC > DateTime.MinValue)
            {
                int min = (int)((ARec.mReceivedUTC - ARec.mTransmitUTC).TotalMinutes + 0.499);

                if (min >= 30)
                {
                    read += "\nT+" + (min / 60).ToString() + ":" + (min % 60).ToString("00");
                }
            }

            string filePath = "";
            if (ARec.mReceivedUTC > DateTime.MinValue && ARec.mStoredAt.Length > 0)
            {
                string ym, d;

                ym = "YM" + ARec.mReceivedUTC.Year.ToString() + ARec.mReceivedUTC.Month.ToString("00");
                d = "D" + ARec.mReceivedUTC.Day.ToString("00");

                filePath = mPathCombine /*Path.Combine*/(mRecordingDir, ym, d, ARec.mStoredAt);
            }
            string eventStr = mFirstWord(ARec.mEventTypeString) + "\n" + eventTimeString;
            string remark = ARec.mRecLabel;
            string psa = mGetStateString(2, ARec.mRecState, (UInt16)(ARec.mbActive ? 1 : 0), countIndex, countTotal, countToDo, countDone);
            //string triage = mGetTriageButtonText((DRecState)ARec.mRecState);
            Image triageImg = mGetTriageButtonImage((DRecState)ARec.mRecState);
            Image eventStrip = AbLoadImage ? mLoadImage(AbDirectLoad, ARec.mIndex_KEY, filePath, 0, ARec.mFileName, false, n) : null;

            string study = ARec.mStudy_IX.ToString();
            string active = ARec.mbActive ? "1" : "0";

            int age = ARec.mGetAgeYears();// mGetAgeYears(ARec.mPatientBirthDate.mDecrypt());

            string patient = ARec.mPatientID.mDecrypt() + "\n" + ARec.mPatientTotalName.mDecrypt();
            if (age > 0) patient += "\n" + age.ToString() + " years";

            int offset = ARec.mTimeZoneOffsetMin;
            string utcOffset = "UTC";
            if (offset >= 0) utcOffset += "+"; else { utcOffset += "-"; offset = -offset; }
            utcOffset += (offset / 60).ToString("00") + ":" + (offset % 60).ToString("00");


            string modelNr = "Model: " + ARec.mDeviceModel + "\n#: " + ARec.mDeviceID //+ "\n" //+ CRecordMit.sGetConverterUserString(ARec.mImportConverter) 
                                 + "\n" + utcOffset
                    + "\n\nimport=" + CRecordMit.sGetMethodUserString(ARec.mImportMethod);

            // calculate abs path dir
            //DTriageGridFields: Index, State, Count, Patient, Recorded, Event, 
            //           Remark, EventStrip, Triage, FilePath, FileName, NrSignals, Study, Active
            dataGridTriage.Rows.Add(psa, patient, eventStr, read, remark, eventStrip, triageImg,
                indexKey, state, filePath, ARec.mFileName, ARec.mNrSignals.ToString(), study, active, modelNr);

            if (dataGridTriage.Rows.Count > n)
            {
                DataGridViewRow gridRow = dataGridTriage.Rows[n];

                if (gridRow != null)
                {
                    gridRow.DefaultCellStyle.BackColor = CRecordMit.sGetRecStateBgColor(ARec.mRecState);
                    gridRow.Cells[(int)DTriageGridField.Triage].Style.Font = mTriageFont;
                    if (bCursor)
                    {
                        dataGridTriage.CurrentCell = gridRow.Cells[0];  // set cursor back to current
                    }
                }
            }
            mTriageUpdateReset();
            return bOk;
        }
        public string mGetImageFullName(ushort ASignalIndex, string AFileName, bool AbOld)
        {
            string s = "Full";

            if (AbOld == false)
            {
                s += (ASignalIndex + 1).ToString("00");   // program uses 0, user starts at 1
            }
            return s + "_" + AFileName + ".png";
        }
        public string mGetImageEventName(ushort ASignalIndex, string AFileName, bool AbOld)
        {
            string s = "Event";

            if (AbOld == false)
            {
                s += (ASignalIndex + 1).ToString("00");
            }
            return s + "_" + AFileName + ".png";
        }

        public Image mLoadImage(bool AbDirectLoad, UInt32 AIndex, string AFilePath, ushort ASignalIndex, string AFileName, bool AbFull, Int32 ARow)
        {
            Image img = null;
            string fileName = "";

            try
            {
                fileName = AbFull ? "Full" : "Event";

                fileName += (ASignalIndex + 1).ToString("00");   // program uses 0, user starts at 1

                fileName += "_" + Path.GetFileNameWithoutExtension(AFileName) + ".png";

                if (mImageCach == null)
                {
                    string filePath = mPathCombine /*Path.Combine*/(AFilePath, fileName);

                    img = Image.FromFile(filePath);
                }
                else
                {
                    img = mImageCach.mGetImage(AbDirectLoad, AIndex, AFilePath, fileName, ARow);
                }
            }
            catch (Exception /*ex*/)
            {
                CProgram.sLogError("Failed " + (AbDirectLoad ? "direct " : "") + "loading " + AIndex.ToString()
                    + ": " + AFilePath + fileName);
                img = null;
            }

            return img;
        }

        public void mUpdateTriageStripImages(bool AbDirectLoad, UInt32 AIndex, string AFilePath, string AFileName, ushort ASignalIndex)
        {
            Image img = null;
            string fileName = "";


            try
            {
                if (AFilePath != null && AFilePath.Length > 0 && AFileName != null && AFileName.Length > 0)
                {
                    fileName = mGetImageFullName(ASignalIndex, AFileName, false);

                    if (mImageCach == null)
                    {
                        string filePath = mPathCombine /*Path.Combine*/(AFilePath, fileName);

                        img = Image.FromFile(filePath);
                    }
                    else
                    {
                        img = mImageCach.mGetImage(AbDirectLoad, AIndex, AFilePath, fileName, -1);
                    }
                }
            }
            catch (Exception /*ex*/)
            {
                CProgram.sLogError("Failed " + (AbDirectLoad ? "direct " : "") + "loading " + AIndex.ToString()
                    + ": " + AFilePath + fileName);
                img = null;
            }
            panelTriageStrip0.BackgroundImage = img;
            mTriageUpdateReset();
        }

        public void mUpdateTriageStrips()
        {
            mTriageUpdateReset();
            if (mbUpdatingGrid)
            {
                return;
            }
            // show pictures
            try
            {
                DateTime dt = DateTime.Now;
                ushort signalIndex = 0, nrSignals = 0;
                string strIndex = comboBoxTriageChannel.Text;
                labelUpdateTC.Text = ">UpdTC";
                if (strIndex != null && strIndex.Length > 1) strIndex = strIndex.Substring(0, 1);

                if (false == ushort.TryParse(strIndex, out signalIndex))
                {
                    signalIndex = 1;
                }
                if (signalIndex > 0) --signalIndex;     // program starts at 0, user at 1

                DataGridViewRow index = dataGridTriage.CurrentRow;
                int rowIndex = 0;

                dataGridTriage.UseWaitCursor = true;
                mShowBusy(true);

                if (index != null && Index.Index >= 0 && index.Cells.Count >= 12)
                {
                    rowIndex = index.Index + 1;

                    string filePath = index.Cells[(int)DTriageGridField.FilePath].Value.ToString();
                    string fileName = index.Cells[(int)DTriageGridField.FileName].Value.ToString();
                    string patName = index.Cells[(int)DTriageGridField.Patient].Value.ToString();
                    string indexKey = index.Cells[(int)DTriageGridField.Index].Value.ToString();
                    string studyIndex = index.Cells[(int)DTriageGridField.Study].Value.ToString();
                    string channels = index.Cells[(int)DTriageGridField.NrSignals].Value.ToString();
                    string readTime = index.Cells[(int)DTriageGridField.Recorded].Value.ToString();
                    string studyRem = ".";
                    string modelNr = index.Cells[(int)DTriageGridField.ModelNr].Value.ToString();
                    string state = index.Cells[(int)DTriageGridField.State].Value.ToString();
                    UInt32 indexKeyNr = 0;

                    UInt16.TryParse(channels, out nrSignals);
                    if (UInt32.TryParse(indexKey, out indexKeyNr) && indexKeyNr != 0)
                    {
                        mUpdateTriageStripImages(true, indexKeyNr, filePath, Path.GetFileNameWithoutExtension(fileName), signalIndex);

                        if (index.Cells[(int)DTriageGridField.EventStrip].Value == null)
                        {
                            bool bLoad = false;
                            Image img = mLoadImage(bLoad, indexKeyNr, filePath, 0, fileName, false, index.Index);

                            if (img != null)
                            {
                                index.Cells[(int)DTriageGridField.EventStrip].Value = img;
                            }

                        }
                    }

                    DateTime readDT = DateTime.MinValue;

                    if (CProgram.sbParseDtLocal(readTime, out readDT))
                    {
                        DateTime readUTC = CProgram.sDateTimeToUTC(readDT);

                        modelNr += "\nR.UTC= " + CProgram.sTimeToString(readUTC);
                    }
                    int pos = state.IndexOf('\n');
                    if (pos >= 0) state = state.Substring(pos + 1);
                    modelNr += "\nstate=" + state;
                    string patID = "";
                    pos = patName.IndexOf('\n');
                    if (pos >= 0)
                    {
                        patID = patName.Substring(0, pos);
                        patName = patName.Substring(pos + 1);
                    }

                    labelPatientID.Text = patID;
                    labelTriagePatient.Text = patName;
                    labelTriageStudy.Text = studyRem;
                    labelRecNr.Text = indexKey;
                    labelRecIndex.Text = indexKey;
                    labelStudyIndex.Text = studyIndex;
                    labelNrChannels.Text = nrSignals.ToString();
                    labelFindingCodes.Text = "-";
                    labelTriageStudy.Text = "-";
                    labelModelNr.Text = modelNr;
                    textBoxStudyRemark.Text = "";
                    textBoxFindings.Text = "";
                    labelStudyProcCodes.Text = "";
                }
                else
                {
                    rowIndex = 0;
                    labelPatientID.Text = "";
                    labelTriagePatient.Text = "";
                    labelTriageStudy.Text = "";
                    labelRecNr.Text = "";
                    labelRecIndex.Text = "-";
                    labelStudyIndex.Text = "";
                    labelNrChannels.Text = "-";
                    labelPatientID.Text = "-";
                    labelTriageStudy.Text = "-";

                    labelFindingCodes.Text = "-";
                    labelModelNr.Text = "";
                    panelTriageStrip0.BackgroundImage = null;
                    textBoxStudyRemark.Text = "";
                    textBoxFindings.Text = "";
                    labelStudyProcCodes.Text = "";
                }
                labelTriageRow.Text = rowIndex.ToString();
                double sec = (DateTime.Now - dt).TotalSeconds;
                labelUpdateTC.Text = "UpdTC= " + sec.ToString("0.000") + "sec.";
            }
            catch (Exception e2)
            {
                CProgram.sLogException("Failed  update strip", e2);
            }
            finally
            {
                dataGridTriage.UseWaitCursor = false;
                mShowBusy(false);
            }
        }
        public void mUpadteTriageRowStrips(uint ARow, bool AbDirectLoad, bool AbLoadFull, UInt16 AFullIndex, bool AbDirectLoadFull)
        {
            mTriageUpdateReset();
            // show pictures
            try
            {
                if (ARow < dataGridTriage.Rows.Count)
                {
                    DataGridViewRow index = dataGridTriage.Rows[(int)ARow];

                    if (index != null && Index.Index >= 0 && index.Cells.Count >= 12)
                    {
                        UInt32 indexKeyNr = 0;

                        string filePath = index.Cells[(int)DTriageGridField.FilePath].Value.ToString();
                        string fileName = index.Cells[(int)DTriageGridField.FileName].Value.ToString();
                        string indexKey = index.Cells[(int)DTriageGridField.Index].Value.ToString();

                        if (UInt32.TryParse(indexKey, out indexKeyNr) && indexKeyNr != 0)
                        {
                            if (index.Cells[(int)DTriageGridField.EventStrip].Value == null)
                            {
                                Image img = mLoadImage(AbDirectLoad, indexKeyNr, filePath, 0, fileName, false, (Int32)ARow);

                                if (img != null)
                                {
                                    index.Cells[(int)DTriageGridField.EventStrip].Value = img;
                                }
                            }
                            if (AbLoadFull)
                            {
                                mUpdateTriageStripImages(AbDirectLoadFull, indexKeyNr, filePath, Path.GetFileNameWithoutExtension(fileName), AFullIndex);
                            }
                        }
                    }
                }
            }
            catch (Exception e2)
            {
                CProgram.sLogException("Failed  update strip", e2);
            }
        }

        private void mTriageOpenEventViewer()
        {
            mTriageUpdateReset();
            try
            {
                string toFile = "";

                if (mbGetTriageCursorFilePath(out toFile))
                {
                    string ext = Path.GetExtension(toFile);
                    string exe = mHeaViewExe;

                    if (ext == ".scp")
                    {
                        exe = mScpViewExe;
                    }
                    else
                    {
                        toFile = Path.ChangeExtension(toFile, "dat");
                    }
                    if (File.Exists(exe))
                    {
                        ProcessStartInfo startInfo = new ProcessStartInfo();
                        startInfo.FileName = exe;
                        startInfo.Arguments = toFile;
                        Process.Start(startInfo);
                    }
                }
            }
            catch (Exception e2)
            {
                CProgram.sLogException("Failed toolstripopen", e2);
            }
        }
        private void mTriageOpenEventDir()
        {
            mTriageUpdateReset();
            // open explorer wit file at cursor
            try
            {
                string toFile = "";

                if (mbGetTriageCursorFilePath(out toFile))
                {
                    string toPath = Path.GetDirectoryName(toFile);
                    ProcessStartInfo startInfo = new ProcessStartInfo();
                    startInfo.FileName = @"explorer";
                    startInfo.Arguments = toPath;
                    Process.Start(startInfo);
                    //                    mbGetCursorUpdateRead("+");

                }
            }
            catch (Exception e2)
            {
                CProgram.sLogException("Failed open explorer", e2);
            }
        }

        bool mbGetCursorIndexState(out int ArIndexKey, out string ArState)
        {
            bool bOk = false;
            int indexKey = 0;
            string state = "";

            try
            {
                DataGridViewRow index = dataGridTriage.CurrentRow;

                if (index != null && index.Cells.Count >= 10)
                {
                    string indexString = index.Cells[(int)DTriageGridField.Index].Value.ToString();

                    state = index.Cells[(int)DTriageGridField.State].Value.ToString();

                    if (state.Length > 3) state = state.Remove(0, 3);

                    bOk = int.TryParse(indexString, out indexKey);

                    if (bOk)
                    {
                        if (indexKey == 0)
                        {
                            CProgram.sLogLine("Cursor has an invalid index key (0)");
                            bOk = false;
                        }
                    }
                }

            }
            catch (Exception e2)
            {
                bOk = false;
                CProgram.sLogException("Failed get cursor for index", e2);
            }
            ArIndexKey = indexKey;
            ArState = state;

            return bOk;
        }
        bool mbGetTriageCursorFilePath(out string AFilePath)
        {
            bool bOk = false;

            AFilePath = "";
            try
            {
                DataGridViewRow index = dataGridTriage.CurrentRow;

                if (index != null && index.Cells.Count >= 12)
                {
                    string filePath = index.Cells[(int)DTriageGridField.FilePath].Value.ToString();
                    string fileName = index.Cells[(int)DTriageGridField.FileName].Value.ToString();

                    if (filePath != null && filePath.Length > 0)
                    {
                        if (Directory.Exists(filePath))
                        {
                            AFilePath = mPathCombine /*Path.Combine*/(filePath, fileName);

                            bOk = true;
                        }
                        else
                        {
                            CProgram.sLogLine("Data file path " + filePath + " does not exist");
                        }
                    }
                }

            }
            catch (Exception e2)
            {
                bOk = false;
                CProgram.sLogException("Failed get cursor path", e2);
            }
            return bOk;
        }

        bool mbGetTriageCursorIndexState(out int ArIndexKey, out string ArState)
        {
            bool bOk = false;
            int indexKey = 0;
            string state = "";

            try
            {
                DataGridViewRow index = dataGridTriage.CurrentRow;

                if (index != null && index.Cells.Count >= 10)
                {
                    string indexString = index.Cells[(int)DTriageGridField.Index].Value.ToString();
                    state = index.Cells[(int)DTriageGridField.State].Value.ToString();

                    if (state.Length > 3) state = state.Remove(0, 3);


                    bOk = int.TryParse(indexString, out indexKey);

                    if (bOk)
                    {
                        if (indexKey == 0)
                        {
                            CProgram.sLogLine("Cursor has an invalid index key (0)");
                            bOk = false;
                        }
                    }
                }

            }
            catch (Exception e2)
            {
                bOk = false;
                CProgram.sLogException("Failed get cursor for index", e2);
            }
            ArIndexKey = indexKey;
            ArState = state;

            return bOk;
        }

        void mUpdateTriageTime(DateTime ANow)
        {
            int sec = 0;
            int min = 0;
            string s = "--:--";

             if (mLastTriageUpdateDT != null && mLastTriageUpdateDT > DateTime.MinValue)
            {
                sec = (int)(ANow - mLastTriageUpdateDT).TotalSeconds;
                min = sec / 60;
                sec %= 60;

                s = min.ToString() + ":" + sec.ToString("00");
            }
          
            if (mTriageUpdSec >= 15)
            {
                if (mLastTriageUpdateDT > mLastTriageActivityDT)
                {
                    mLastTriageActivityDT = mLastTriageUpdateDT;
                }
                sec = (int)(ANow - mLastTriageActivityDT).TotalSeconds;
                sec = mTriageUpdSec - sec;
                if( sec < 15 )
                {
                    
                    if( sec <= 0 )
                    {
                        s += "\nT- 0";
                        mLastTriageActivityDT = ANow;
                        mUpdateTriageScreen();
                    } 
                    else
                    {
                        s += "\nT-" + sec.ToString();
                    }
                }
            }
            labelTriageUpdTime.Text = s;
        }
        public void mUpdateTriageScreen()
        {
            if( mbUpdatingTriage )
            {
                return;
            }
            mbUpdatingTriage = true;

            DateTime dt = DateTime.Now;
            string s = "";
            string indexKey = "";

            labelUpdateTS.Text = ">UpdTS";


            mUpdateTriageViewMode();

            string offset = "";
            double sec = (int)((dt - DateTime.UtcNow).TotalSeconds);
            if (sec < 0) sec -= 5;
            else
            {
                sec += 5; offset = "+";
            }
            int min = (int)(sec) / 60;
            int hour = min / 60;
            min %= 60;
            offset += hour.ToString("D2") + ":" + min.ToString("D2");

            labelUtcOffset.Text = "UTC" + offset;

            // get current cursor index
            DataGridViewRow row = dataGridTriage.CurrentRow;
            if (row != null && row.Cells.Count > (int)DTriageGridField.Index)
            {
                indexKey = row.Cells[(int)DTriageGridField.Index].Value.ToString();
            }

            if (false == mbTriageReadSqlDbTable( indexKey ))
            {
                s += "!t!";
            }


            s += "UpdTS= ";
            mUpdateTriageTime(dt);
            sec = (DateTime.Now - dt).TotalSeconds;
            s += sec.ToString("0.000") + "sec.";
            labelUpdateTS.Text = s;
            mLastTriageUpdateDT = DateTime.Now;
            mTriageUpdateReset();

            mUpdateTriageStrips();

            timerTriage.Enabled = true;
            mbUpdatingTriage = false;

        }
        public void mUpdateTriageViewMode()
        {

            string viewModeStr1 = "Event";
            string viewModeStr2 = listBoxEventMode.Text;

            if (viewModeStr2 == null || viewModeStr2.Length < 1 || viewModeStr2[0] == '-')
            {
                viewModeStr2 = "Triage";
            }
            /*

                        int viewMode = listBoxEventMode. TopIndex; // SelectedIndex;


                        if (viewMode >= 0 && viewMode < (int)DTriageViewMode.NrViewModes)
                        {
                            if (viewMode == (int)DTriageViewMode.Seperator)
                            {
                                viewModeStr2 = "Triage";
                            }
                            else
                            {
                                viewModeStr2 = listBoxEventMode.Items[viewMode].ToString();     //.SelectedItem.ToString();
                            }
                            viewModeStr1 = "Event";
                        }
            */
            labelViewMode1.Text = viewModeStr1 + ": " + viewModeStr2;
            //            labelViewMode2.Text = ""; // viewModeStr2;
        }

        public string mMakeTriageSearchString(CRecordMit ANewRec)
        {
            string whereStr = "";
            string viewModeStr = listBoxEventMode.Text;
            int viewMode = 0;
            bool bTriage = true;

            if (viewModeStr == null || viewModeStr.Length < 1 || viewModeStr[0] == '-')
            {
                viewModeStr = "Triage";
            }
            else
            {
                viewMode = listBoxEventMode.Items.IndexOf(viewModeStr);
            }

            if (viewMode >= 0 && viewMode < (int)DTriageViewMode.NrViewModes && ANewRec != null)
            {
                string name = ANewRec.mVarGetName((UInt16)DSqlRecordVars.RecState_IX);

                if (name != null && name.Length > 1)
                {

                    switch ((DTriageViewMode)viewMode)
                    {
                        case DTriageViewMode.Triage:
                            whereStr = name + "=" + ((int)(DRecState.Received)).ToString();
                            break;
                        case DTriageViewMode.Analyze:
                            whereStr = name + "=" + ((int)(DRecState.Triaged)).ToString();
                            break;
                        case DTriageViewMode.Report:
                            whereStr = name + "=" + ((int)(DRecState.Analyzed)).ToString();
                            bTriage = false;
                            break;
                        case DTriageViewMode.Seperator:
                            whereStr = name + "=" + ((int)(DRecState.Received)).ToString();
                            break;
                        case DTriageViewMode.ListAll:
                            whereStr = ""; // name + "=" + ((int)(DRecState.)).ToString();
                            bTriage = false;
                            break;
                        case DTriageViewMode.ListRead:
                            whereStr = name + "=" + ((int)(DRecState.Received)).ToString();
                            break;
                        case DTriageViewMode.ListNormal:
                            whereStr = name + "=" + ((int)(DRecState.Normal)).ToString();
                            break;
                        case DTriageViewMode.ListNoise:
                            whereStr = name + "=" + ((int)(DRecState.Noise)).ToString();
                            break;
                        case DTriageViewMode.ListLeadOff:
                            whereStr = name + "=" + ((int)(DRecState.LeadOff)).ToString();
                            break;
                        case DTriageViewMode.ListBaseLine:
                            whereStr = name + "=" + ((int)(DRecState.BaseLine)).ToString();
                            break;
                        case DTriageViewMode.ListTriaged:
                            whereStr = name + "=" + ((int)(DRecState.Triaged)).ToString();
                            break;
                        case DTriageViewMode.ListAnalyzed:
                            whereStr = name + "=" + ((int)(DRecState.Analyzed)).ToString();
                            bTriage = false;
                            break;
                        case DTriageViewMode.ListReported:
                            whereStr = name + "=" + ((int)(DRecState.Reported)).ToString();
                            bTriage = false;
                            break;
                    }
                }
                dataGridTriage.Columns[(int)DTriageGridField.Triage].Visible = bTriage;
            }

            return whereStr;
        }

        public bool mbTriageReadSqlDbTable( string ACursorIndexKey )
        {
            bool bOk = false;
            bool bCursor = false;
            CSqlCmd cmd = null;
            try
            {
                int nrHours = 48;
                DateTime dt = DateTime.Now;

                mShowBusy(true);
                //int unit = listBoxViewUnit.SelectedIndex; werkt niet
                // string unitStr = listBoxViewUnit.SelectedItem != null ? listBoxViewUnit.SelectedItem.ToString() : "";
                int unit = listBoxViewUnit.TopIndex;

                if (int.TryParse(textBoxViewTime.Text, out nrHours) && nrHours > 0)
                {
                    if (unit == 1)
                    {
                        nrHours *= 24;  // days
                    }

                }
                else
                {
                    nrHours = unit == 1 ? 7 * 24 : 48;
                }

                if (mbCreateDBaseConnection())
                {
                    CRecordMit rec = new CRecordMit();

                    if (rec == null)
                    {
                        CProgram.sLogLine("Failed to init cmd");
                    }
                    else
                    {
                        cmd = rec.mGetSqlCmd(mDBaseServer);

                        if (cmd == null)
                        {
                            CProgram.sLogLine("Failed to init cmd");

                        }
                        else if (false == cmd.mbPrepareSelectCmd(true, rec.mMaskValid))
                        {
                            CProgram.sLogLine("Failed to prepare SQL row in table " + rec.mGetDbTableName());
                        }
                        else
                        {
                            DateTime utc = CProgram.sGetUtcNow();
                            string name = rec.mVarGetName((UInt16)DSqlRecordVars.ReceivedUTC);
                            string cmp = rec.mSqlDateTimeCompareSqlString(name, utc, DSqlCompareDateTime.Hour, nrHours);
                            UInt64 mask = cmd.mGetCmdVarFlags();
                            string sqlWhere = mMakeTriageSearchString(rec);
                            int n = 0;
                            double loadTime = mImageCach != null ? mImageCach.mGetMaxUpdateSec() : 4.0;
                            int nLoadDirect = 1;
                            int nDirect = 0;
                            bool bLoadDirect;
                            double dbTime, dbExec;

                            if (mImageCach != null) mImageCach.mSetDataGrid(null, 0, 0);
                            dataGridTriage.Rows.Clear();
                            mbUpdatingGrid = true;

                            cmd.mWhereAddString(cmp);

                            if (sqlWhere != null && sqlWhere.Length > 0)
                            {
                                cmd.mWhereAddString(sqlWhere);
                            }
                            if (radioButtonCurrentPatient.Checked)
                            {
                                if (mCurrentPatientID != null && mCurrentPatientID.Length > 0)
                                {
                                    rec.mPatientID.mbSetEncrypted(mCurrentPatientID);

                                    string varName = rec.mVarGetName((UInt16)DSqlRecordVars.PatientID_ES);
                                    string s = varName + "='" + rec.mPatientID.mGetEncrypted() + "'";

                                    cmd.mWhereAddString(s);
                                }
                            }

                            if (cmd.mbExecuteSelectCmd())
                            {
                                bool bError;
                                bOk = true;

                                bCursor = true;
                                dataGridTriage.UseWaitCursor = true;

                                dbExec = (DateTime.Now - dt).TotalSeconds;

/*                                string header, line;
                                rec.mbGetVarNames(out header, mask, ", ");
                                CProgram.sLogLine("#, " + header);
*/
                                while (cmd.mbReadOneRow(out bError))
                                {
                                    bLoadDirect = false; // n < nLoadDirect || (DateTime.Now - dt).TotalSeconds < loadTime;
                                    if (bLoadDirect) ++nDirect;
//                                    rec.mbGetVarValues(out line, mask, ", ");
                                    mbTriageStoreGrid(bLoadDirect, rec, false, ACursorIndexKey);
                                    ++n;
                                    //CProgram.sLogLine(cmd.mGetNrRowsRead().ToString() + ", " + line);
                                    rec.mClear(); // clear for next read
                                }
                                bOk = false == bError;
                                dbTime = (DateTime.Now - dt).TotalSeconds;

                                ushort signalIndex = 0;
                                string strIndex = comboBoxTriageChannel.Text;
                                if (strIndex != null && strIndex.Length > 1) strIndex = strIndex.Substring(0, 1);

                                if (false == ushort.TryParse(strIndex, out signalIndex))
                                {
                                    signalIndex = 1;
                                }
                                if (signalIndex > 0) --signalIndex;     // program starts at 0, user at 1

                                //Data grid has been made, enable thread update enable
                                if (mImageCach != null) mImageCach.mSetDataGrid(dataGridTriage, (Int32)DTriageGridField.Index, (Int32)DTriageGridField.EventStrip);

                                nDirect = 0;
                                for (uint i = 0; i < n; ++i)
                                {
                                    bLoadDirect = i < nLoadDirect || (DateTime.Now - dt).TotalSeconds < loadTime;
                                    if (bLoadDirect) ++nDirect;

                                    mUpadteTriageRowStrips(i, bLoadDirect, i == 0, signalIndex, true);

                                    if (i == 0)
                                    {
                                        UInt16 nrThreads;
                                        mImageCach.mTryLoadImages(out nrThreads);
                                    }
                                }

                                TimeSpan ts = DateTime.Now - dt;
                                CProgram.sLogLine((bOk ? "" : "!Incomplete ") + "Updated " + n.ToString() + " table rows in " + ts.TotalSeconds.ToString("0.000")
                                                + " sec. (nDirect= " + nDirect.ToString() + ", exec= " + dbExec.ToString("0.000") + ", db= " + dbTime.ToString("0.000") + " sec)");
                            }
                            else
                            {
                                CProgram.sLogLine("Failed to search SQL row in table " + rec.mGetDbTableName() + " {?=" + cmd.mGetWhereString() + "}");
                            }
                            labelTriageRowCount.Text = n.ToString();
                        }
                        cmd.mCloseCmd();
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed read sql table", ex);
            }
            finally
            {
                if( cmd != null )
                {
                    cmd.mCloseCmd();
                }
                if (bCursor)
                {
                    dataGridTriage.UseWaitCursor = false;
                }
                mbUpdatingGrid = false;
                mUpdateTriageStrips();
                mShowBusy(false);
            }
            return bOk;
        }

        public bool mbTriageUpdateDbState(CRecordMit ARecordDb, UInt64 AFlags )
        {
            bool bOk = false;

            if( ARecordDb != null && ARecordDb.mIndex_KEY > 0 && ARecordDb.mRecState > 0  )
            {
                try
                {
                    if (mbCreateDBaseConnection())
                    {
                        CSqlCmd cmd = ARecordDb.mGetSqlCmd(mDBaseServer);
                        UInt64 mask = AFlags | CRecordMit.sGetMask((UInt16)DSqlRecordVars.RecState_IX);

                        if (cmd == null)
                        {
                            CProgram.sLogLine("Failed to init cmd");
                        }
                        else if (false == cmd.mbPrepareUpdateCmd(true, mask, true, true))
                        {
                            CProgram.sLogLine("Failed to prepare update SQL row in table " + ARecordDb.mGetDbTableName());
                        }
                        else if (cmd.mbExecuteUpdateCmd())
                        {
                            DataGridViewRow row;
                            string newState = ((DRecState)ARecordDb.mRecState).ToString();
                            string indexStr = ARecordDb.mIndex_KEY.ToString();

                            int n = dataGridTriage.Rows.Count;

                            for( int i = 0; i < n; ++i )
                            {
                                row = dataGridTriage.Rows[i];
                                if (row != null && row.Cells.Count > (int)DTriageGridField.Index )
                                {
                                    if(row.Cells[(int)DTriageGridField.Index] != null )
                                    {
                                        string s = row.Cells[(int)DTriageGridField.Index].Value.ToString();

                                        if (indexStr.CompareTo(s) == 0)
                                        {

                                            row.Cells[(int)DTriageGridField.State].Value = ARecordDb.mRecState.ToString("D2") + "\n" + newState;
                                            mUpdateGridRowState(row);
                                            break;
                                        }
                                    }
                                }

                            }
                            bOk = true;
                            CProgram.sLogLine("Successfull updated  " + ARecordDb.mIndex_KEY.ToString() + " to " + newState);
                        }
                        else
                        {
                            CProgram.sLogLine("Failed to update SQL row[" + ARecordDb.mIndex_KEY.ToString() + "] in table " + ARecordDb.mGetDbTableName());
                        }
                        cmd.mCloseCmd();
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Failed update Row", ex);
                }

            }
            mTriageUpdateReset();
            return bOk;
        }
        public void mTriageSetNewState(DRecState AState)
        {
            int index = 0;
            string oldState = "";

            if (mbGetCursorIndexState(out index, out oldState))
            {
                CRecordMit rec = new CRecordMit();
                string newState = CRecordMit.sGetStateUserString((UInt16)AState);

                if (rec != null)
                {
                    rec.mIndex_KEY = (UInt32)index;
                    rec.mRecState = (UInt16)AState;

                    try
                    {
                        if (mbCreateDBaseConnection())
                        {
                            CSqlCmd cmd = rec.mGetSqlCmd(mDBaseServer);
                            UInt64 mask = CRecordMit.sGetMask((UInt16)DSqlRecordVars.RecState_IX);

                            if (cmd == null)
                            {
                                CProgram.sLogLine("Failed to init cmd");
                            }
                            else if (false == cmd.mbPrepareUpdateCmd(true, mask, true, true))
                            {
                                CProgram.sLogLine("Failed to prepare update SQL row in table " + rec.mGetDbTableName());
                            }
                            else if (cmd.mbExecuteUpdateCmd())
                            {
                                DataGridViewRow row = dataGridTriage.CurrentRow;
                                if (row != null)
                                {
                                    int i = (int)(AState);
                                    row.Cells[(int)DTriageGridField.State].Value = i.ToString("D2") + "\n" + newState;
                                    mUpdateGridRowState(row);
                                }
                                CProgram.sLogLine("Successfull updated  " + index.ToString() + "  from " + oldState + " to " + newState);
                            }
                            else
                            {
                                CProgram.sLogLine("Failed to update SQL row[" + index.ToString() + "] in table " + rec.mGetDbTableName());
                            }
                            cmd.mCloseCmd();
                        }
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException("Failed update Row", ex);
                    }

                }
            }
            else
            {
                CProgram.sLogLine("Select row in table first.");
            }
            mTriageUpdateReset();
        }


        private void toolStripDropDownButton1_Click(object sender, EventArgs e)
        {

        }

        private void contextMenuSettings_Opening(object sender, CancelEventArgs e)
        {

        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void toolStripUserOk_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Anayze_Click(object sender, EventArgs e)
        {
            Form formAnalyze = new FormAnalyze( this );

            formAnalyze.ShowDialog();

        }

        private void label1ProcedureDone24_Click(object sender, EventArgs e)
        {

        }

        private void comboBoxChannel_SelectedIndexChanged(object sender, EventArgs e)
        {
            mUpdateTriageStrips();
        }

        private void dataGridTriage_SelectionChanged(object sender, EventArgs e)
        {
            mUpdateTriageStrips();
        }

        private void buttonNoise_Click(object sender, EventArgs e)
        {
            mTriageSetNewState(DRecState.Noise);
        }

        private void buttonNormal_Click(object sender, EventArgs e)
        {
            mTriageSetNewState(DRecState.Normal);
        }

        private void buttonLeadOff_Click(object sender, EventArgs e)
        {
            mTriageSetNewState(DRecState.LeadOff);
        }

        private void buttonAnalyze_Click(object sender, EventArgs e)
        {
            mTriageSetNewState(DRecState.Triaged);
        }

        private void buttonRead_Click(object sender, EventArgs e)
        {
            mTriageSetNewState(DRecState.Received);
        }

        private void buttonAnalyzed_Click(object sender, EventArgs e)
        {
            mTriageSetNewState(DRecState.Analyzed);
        }

        private void panel48_Click(object sender, EventArgs e)
        {
            mUpdateTriageScreen();
        }

        private void timerTriage_Tick(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;

            mUpdateTriageTime(dt);

            if (mImageCach != null)
            {
                UInt16 nrThreads;
                UInt32 n = mImageCach.mTryLoadImages(out nrThreads);

                string s = n == 0 ? "" : "icl= " + n.ToString();
                if (nrThreads > 0)
                {
                    s += ", nt= " + nrThreads.ToString();
                }
                labelLoadImages.Text = s;

            }
 
            if ((dt - mLastActivityWriteDT).TotalSeconds > 60)
            {
                mLastActivityWriteDT = dt;
                CProgram.sWriteLastActiveFile(mRecordingDir);
            }
        }

        private void panel48_Paint(object sender, PaintEventArgs e)
        {

        }

        private void buttonEventView_Click(object sender, EventArgs e)
        {
            mTriageOpenEventViewer();
        }

        private void buttonEventFolder_Click(object sender, EventArgs e)
        {
            mTriageOpenEventDir();
        }

        private void labelRecIndex_Click(object sender, EventArgs e)
        {

        }

        private void dataGridTriage_CellClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridTriage_MouseUp(object sender, MouseEventArgs e)
        {
            int x = e.X;
            int y = e.Y;

            DataGridView.HitTestInfo hitInfo = dataGridTriage.HitTest(x, y);

            if (hitInfo != null)
            {
                int c = hitInfo.ColumnIndex;
                int r = hitInfo.RowIndex;
                if (r >= 0 && c == (int)DTriageGridField.Triage)
                {
                    int centerX = 79; // text = 63;
                    int centerY = 37; // text = 30;
                    int border = 2;
                    //int hight = 70;
                    int cX = x - hitInfo.ColumnX;
                    int rY = y - hitInfo.RowY;

                    int iX = 0, iY = 0;

                    if (cX > border)
                    {
                        if (cX < centerX - border) iX = 1;
                        else if (cX > centerX + border) iX = 2;
                    }
                    if (rY > border)
                    {
                        if (rY < centerY - border) iY = 1;
                        else if (rY > centerY + border && rY < Height - border) iY = 2;
                    }
                    if (iX > 0 && iY > 0)
                    {
                        DataGridViewRow row = dataGridTriage.Rows[r];

                        CProgram.sLogLine("Triage[" + r.ToString() + "] hit at (" + cX.ToString() + ", " + rY.ToString() + ")");

                        if (dataGridTriage.CurrentRow != row)
                        {
                            dataGridTriage.CurrentCell = row.Cells[c];
                        }
                        if (iY == 1)
                        {
                            if (iX == 1) mTriageSetNewState(DRecState.Normal); else mTriageSetNewState(DRecState.LeadOff);
                            //text                             if (iX == 1) mTriageSetNewState(DRecState.Noise); else mTriageSetNewState(DRecState.LeadOff);
                        }
                        else
                        {
                            if (iX == 1) mTriageSetNewState(DRecState.Noise); else mTriageSetNewState(DRecState.Triaged);
                            //text                            if (iX == 1) mTriageSetNewState(DRecState.Normal); else mTriageSetNewState(DRecState.Triaged);
                        }
                        if (dataGridTriage.CurrentRow != row)
                        {
                            CProgram.sPromptError(true, "", "Current row is not correct");
                        }
                    }
                }
            }
        }

        private void buttonAnalyzed_Click_1(object sender, EventArgs e)
        {
            mTriageSetNewState(DRecState.Analyzed);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            mTriageSetNewState(DRecState.BaseLine);
        }

        private void panel22_Paint(object sender, PaintEventArgs e)
        {

        }

        private void buttonTriageFields_Click(object sender, EventArgs e)
        {
            mbShowTriageFields = !mbShowTriageFields;

            for (DTriageGridField i = DTriageGridField.Triage; ++i < DTriageGridField.NrGridFields;)
            {
                dataGridTriage.Columns[(int)i].Visible = mbShowTriageFields;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int n = dataGridTriage.Rows.Count;

            if (n > 0)
            {
                dataGridTriage.CurrentCell = dataGridTriage.Rows[0].Cells[0];
                mUpdateTriageStrips();
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            int n = dataGridTriage.Rows.Count;

            if (n > 0)
            {
                dataGridTriage.CurrentCell = dataGridTriage.Rows[n - 1].Cells[0];
                mUpdateTriageStrips();
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            listBoxEventMode.SelectedIndex = (int)DTriageViewMode.Triage;
            tabControl1.SelectedIndex = 0;
            mUpdateTriageScreen();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            listBoxEventMode.SelectedIndex = (int)DTriageViewMode.Analyze;
            tabControl1.SelectedIndex = 0;
            mUpdateTriageScreen();

        }

        private void button12_Click(object sender, EventArgs e)
        {
            listBoxEventMode.SelectedIndex = (int)DTriageViewMode.Report;
            tabControl1.SelectedIndex = 0;
            mUpdateTriageScreen();
        }

        private void buttonRead_Click_1(object sender, EventArgs e)
        {
            mTriageSetNewState(DRecState.Reported);
        }

        private void panel63_Paint(object sender, PaintEventArgs e)
        {
        }

        private void panelViewInc_Click(object sender, EventArgs e)
        {
            int nrHours = 48;
            int unit = listBoxViewUnit.TopIndex;

            if (int.TryParse(textBoxViewTime.Text, out nrHours) && nrHours > 0)
            {
                if (unit == 1)
                {
                    nrHours += 1;  // days
                }
                else
                {
                    nrHours += 4;
                }
                textBoxViewTime.Text = nrHours.ToString();
            }
        }

        private void panelViewDec_Click(object sender, EventArgs e)
        {
            int nrHours = 48;
            int unit = listBoxViewUnit.TopIndex;

            if (int.TryParse(textBoxViewTime.Text, out nrHours) && nrHours > 0)
            {
                if (unit == 1)
                {
                    nrHours -= 1;  // days
                }
                else
                {
                    nrHours -= 4;
                }
                if (nrHours < 1)
                {
                    nrHours = 1;
                }
                textBoxViewTime.Text = nrHours.ToString();
            }

        }

        private void buttonTriageReported_Click(object sender, EventArgs e)
        {
            mTriageSetNewState(DRecState.Reported);
        }

        private void buttonTestLoadImage_Click(object sender, EventArgs e)
        {
            if (mImageCach != null)
            {
                UInt16 nrThreads;
                mImageCach.mTryLoadImages(out nrThreads);
            }
        }

        private void mWindowSizeChanged()
        {
            int newWidth = dataGridTriage.Size.Width - 50;

            if (dataGridTriage.Columns.Count >= (int)DTriageGridField.Triage)
            {
                for (int i = 0; i <= (int)DTriageGridField.Triage; ++i)
                {
                    if (i != (int)DTriageGridField.EventStrip)
                    {
                        if (dataGridTriage.Columns[i].Visible)
                        {
                            newWidth -= dataGridTriage.Columns[i].Width;
                        }
                    }
                }
                if (newWidth < 100) newWidth = 100;

                dataGridTriage.Columns[(int)DTriageGridField.EventStrip].Width = newWidth;

            }
        }
        private void FormEventBoard_SizeChanged(object sender, EventArgs e)
        {
            mWindowSizeChanged();

            /*                int deltaWidth = Size.Width - mWidthScreenNormal;
                        int width = dataGridTriage.Columns[(int)DTriageGridField.EventStrip].Width;
                        int newWidth = width + deltaWidth;

                        if (newWidth > 100)
                        {
                            dataGridTriage.Columns[(int)DTriageGridField.EventStrip].Width = newWidth;
                            mWidthScreenNormal = Size.Width;

                        }
            */
        }

        private void dataGridTriage_CellMouseMove(object sender, DataGridViewCellMouseEventArgs e)
        {
            /*           int x = e.X;
                       int y = e.Y;


                       DataGridView.HitTestInfo hitInfo = dataGridTriage.HitTest(x, y);
           */
            //if (hitInfo != null)
            {
                Cursor oldCursor = dataGridTriage.Cursor;
                Cursor newCursor = Cursors.Default;
                int c = e.ColumnIndex; // hitInfo.ColumnIndex;
                int r = e.RowIndex; // hitInfo.RowIndex;
                if (r >= 0)
                {
                    if (c == (int)DTriageGridField.Triage)
                    {
                        newCursor = Cursors.Hand;
                    }
                }
                if (newCursor != oldCursor)
                {
                    dataGridTriage.Cursor = newCursor;
                }
            }
        }

        private void toolStripEvents_Click(object sender, EventArgs e)
        {

        }

        public void mStartAnalyze()
        {
            if (mbCreateDBaseConnection())
            {
                FormAnalyze formAnalyze = new FormAnalyze( this );

                if (formAnalyze != null)
                {
                    try
                    {
                        UInt32 recordIndex = 0;
                        bool bNew = true;
                        UInt32 analysisIndex = 0;
                        UInt16 analysisNr = 0;

                        int index = 0;
                        string oldState = "";

                        if (mbGetCursorIndexState(out index, out oldState))
                        {
                            recordIndex = (uint)index;
 
                            if (formAnalyze.mbLoadRecord2(false, recordIndex, bNew, analysisIndex, analysisNr, mDBaseServer, mRecordingDir))
                            {
                                formAnalyze.mInitScreens();

                                if (formAnalyze != null)
                                {
                                    try
                                    {
                                        formAnalyze.Show();
//                                        formAnalyze.ShowDialog();
                                    }
                                    catch (Exception ex)
                                    {
                                        CProgram.sLogException("Analysis Form run", ex);
                                    }
/*                                    if (formAnalyze.mRecAnalysis != null && formAnalyze.mRecAnalysisFile != null)
                                    {

                                    }
                                    if (formAnalyze.mRecordFile != null)
                                    {

                                    }
*/                                }
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException("Analyze form", ex);
                    }
                }
            }
            mTriageUpdateReset();
        }
        private void Anayze_Click_1(object sender, EventArgs e)
        {
            mStartAnalyze();
        }

        private void FormEventBoard_Shown(object sender, EventArgs e)
        {
            mWindowSizeChanged();

            int triageNrHours = Properties.Settings.Default.TriageNrHours;
            if (triageNrHours < 1) triageNrHours = 1;

            if (triageNrHours % 24 == 0) { triageNrHours /= 24; listBoxViewUnit.TopIndex = 1; }
            else { listBoxViewUnit.TopIndex = 0; }

        }

        private void listBoxViewUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i = listBoxViewUnit.TopIndex;

            /*             if( listBoxViewUnit.TopIndex == 0 )
                        {
                            listBoxViewUnit.TopIndex = 1;
                        }
                        else
                        {
                            listBoxViewUnit.TopIndex = 0;
                        }
            */
        }

        private void toolStripUsers_Click(object sender, EventArgs e)
        {
            CUserForm form = new CUserForm();

            if (form != null)
            {
                form.ShowDialog();
            }

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            CDeviceModelForm form = new CDeviceModelForm();

            if (form != null)
            {
                form.ShowDialog();
            }

        }

        private void toolStripDepartments_Click(object sender, EventArgs e)
        {
            CClientInfoForm form = new CClientInfoForm();

            if (form != null)
            {
                form.ShowDialog();
            }
        }

        private void toolStripCustomers_Click(object sender, EventArgs e)
        {
            CHospitalListForm form = new CHospitalListForm();

            if (form != null)
            {
                form.ShowDialog();
            }
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            CicdCodeForm form = new CicdCodeForm();

            if (form != null)
            {
                form.ShowDialog();
            }

        }

        private void toolStripWorkNew_Click(object sender, EventArgs e)
        {
            CStudyPatientForm form = new CStudyPatientForm();

            if (form != null)
            {
                form.ShowDialog();
            }

        }

        private void toolStripStudyList(object sender, EventArgs e)
        {
            CStudyListForm form = new CStudyListForm();

            if (form != null)
            {
                form.ShowDialog();
            }

        }

        private void toolStripDevices_Click(object sender, EventArgs e)
        {
            CDeviceListForm form = new CDeviceListForm();

            if (form != null)
            {
                form.ShowDialog();
            }

        }

        private void listBoxViewUnit_MouseClick(object sender, MouseEventArgs e)
        {

            if (listBoxViewUnit.TopIndex == 0)
            {
                listBoxViewUnit.TopIndex = 1;
            }
            else
            {
                listBoxViewUnit.TopIndex = 0;
            }
        }

        private void toolStripExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void toolStripHelpdesk_Click(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            string exeName = bCtrl ? "helpdesk2.exe" : "helpdesk.exe";
            string exePath = Path.Combine("..\\helpdesk\\", exeName);

            if (File.Exists(exePath))
            {
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = exePath;
                //startInfo.Arguments;
                Process.Start(startInfo);
            }

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void buttonFindings_Click(object sender, EventArgs e)
        {
            CPreviousFindingsForm form = new CPreviousFindingsForm();

            if (form != null)
            {
                form.ShowDialog();
            }
        }

        private void buttonSetState_Click(object sender, EventArgs e)
        {
            contextMenuState.Show(buttonSetState.PointToScreen(new Point(0, 0)));

        }

        private void contextMenuState_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (e.ClickedItem != null)
            {
                string text = e.ClickedItem.Text;
                DRecState state = DRecState.Unknown;
                DRecState i = state;

                while (++i < DRecState.NrRecStates)
                {
                    if (text == i.ToString())
                    {
                        state = i;
                        break;
                    }
                }
                if (state != DRecState.Unknown)
                {
                    mTriageSetNewState(state);
                }
            }
        }

        private void contextMenuState_Opening(object sender, CancelEventArgs e)
        {

        }

        private void toolStripPatient_Click(object sender, EventArgs e)
        {
            CUserListForm form = new CUserListForm();

                if( form != null )
            {
                form.ShowDialog();
            }
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            mUpdateTriageScreen();
        }

        private void toolStripWorld_Click(object sender, EventArgs e)
        {
            CStudyListForm form = new CStudyListForm();

            if (form != null)
            {
                form.ShowDialog();
            }
        }

        private void toolStripPatientList_Click(object sender, EventArgs e)
        {
            CPatientListForm form = new CPatientListForm();

            if (form != null)
            {
                form.ShowDialog();
            }
        }

        private void toolStripAddDevice_Click(object sender, EventArgs e)
        {
            CDeviceForm form = new CDeviceForm();

            if (form != null)
            {
                form.ShowDialog();
            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            mCurrentPatientID = "";
        }

        public string mOneLine(string ALine)
        {
            string s = "";
            int n = ALine == null ? 0 : ALine.Length;
            int l = 0;

            for (int i = 0; i < n; ++i)
            {
                char c = ALine[i];
                if (char.IsLetterOrDigit(c))
                {
                    s += c;
                    ++l;
                }
                else if (c == '\r' || c == '\n')
                {
                    break;
                }
                else if( c >=  ' ' )
                {
                    s += c;
                    ++l;
                }
                else if (l > 0)
                {
                    s += " ";
                }
           }
            return s;
        }


        private void radioButtonCurrentPatient_CheckedChanged(object sender, EventArgs e)
        {
            mCurrentPatientID = "";

            try
            {
                DataGridViewRow index = dataGridTriage.CurrentRow;

                if (index != null && index.Cells.Count >= 10)
                {
                    string patient = index.Cells[(int)DTriageGridField.Patient].Value.ToString();
                    mCurrentPatientID = mOneLine(patient);
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("select patient", ex);
            }
        }

        private void labelModelNr_Click(object sender, EventArgs e)
        {

        }

        private void dataGridTriage_DoubleClick(object sender, EventArgs e)
        {
            mStartAnalyze();
        }

        private void buttonSetState_Click_1(object sender, EventArgs e)
        {
            contextMenuState.Show(buttonSetState.PointToScreen(new Point(0, 0)));
        }
    }
}
