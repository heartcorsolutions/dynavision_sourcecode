﻿namespace EventBoard
{
    partial class FormEventBoard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormEventBoard));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolStripTop = new System.Windows.Forms.ToolStrip();
            this.toolStripWorkNew = new System.Windows.Forms.ToolStripButton();
            this.toolStripWorld = new System.Windows.Forms.ToolStripButton();
            this.toolStripPatientList = new System.Windows.Forms.ToolStripButton();
            this.toolStripDepartments = new System.Windows.Forms.ToolStripButton();
            this.toolStripCustomers = new System.Windows.Forms.ToolStripButton();
            this.toolStripUsers = new System.Windows.Forms.ToolStripButton();
            this.toolStripPatient = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripDevices = new System.Windows.Forms.ToolStripButton();
            this.toolStripAddDevice = new System.Windows.Forms.ToolStripButton();
            this.toolStripAlarmSignal = new System.Windows.Forms.ToolStripButton();
            this.toolStripNewEvent = new System.Windows.Forms.ToolStripButton();
            this.toolStripLoad = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripEvents = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripExit = new System.Windows.Forms.ToolStripButton();
            this.toolStripUserCancel = new System.Windows.Forms.ToolStripButton();
            this.toolStripUserLabel = new System.Windows.Forms.ToolStripLabel();
            this.toolStripUserOk = new System.Windows.Forms.ToolStripButton();
            this.toolStripUserLock = new System.Windows.Forms.ToolStripButton();
            this.toolStripSelectUser = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSettings = new System.Windows.Forms.ToolStripButton();
            this.toolStripHelpdesk = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.contextMenuSettings = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.globalSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.showDashBoardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel46 = new System.Windows.Forms.Panel();
            this.labelViewMode1 = new System.Windows.Forms.Label();
            this.panel38 = new System.Windows.Forms.Panel();
            this.labelUpdateTS = new System.Windows.Forms.Label();
            this.labelUpdateTC = new System.Windows.Forms.Label();
            this.labelStudyIndex = new System.Windows.Forms.Label();
            this.labelRecIndex = new System.Windows.Forms.Label();
            this.panel44 = new System.Windows.Forms.Panel();
            this.button12 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel21 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.label2UserMsg = new System.Windows.Forms.Label();
            this.ValueUserMsg = new System.Windows.Forms.Label();
            this.label1UserMsg = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.buttonRefresh = new System.Windows.Forms.Button();
            this.panel43 = new System.Windows.Forms.Panel();
            this.button8 = new System.Windows.Forms.Button();
            this.labelTime = new System.Windows.Forms.Label();
            this.labelDate = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label2EventReport = new System.Windows.Forms.Label();
            this.valueEventReport = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label2EventAnalyze = new System.Windows.Forms.Label();
            this.valueEventAnalyze = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label2EventTriage = new System.Windows.Forms.Label();
            this.valueEventTriage = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label2EventTotalOpen = new System.Windows.Forms.Label();
            this.valueEventTotalOpen = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.label2EventDone24 = new System.Windows.Forms.Label();
            this.valueEventDone24 = new System.Windows.Forms.Label();
            this.label1EventDone24 = new System.Windows.Forms.Label();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel19 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label2ReportToDo = new System.Windows.Forms.Label();
            this.ValueReportToDo24 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label2ProcedureOpen = new System.Windows.Forms.Label();
            this.valueStudyOpen = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label2ProcedureDone24 = new System.Windows.Forms.Label();
            this.valueStudyDone24 = new System.Windows.Forms.Label();
            this.label1ProcedureDone24 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.panel16 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageTriage = new System.Windows.Forms.TabPage();
            this.panel23 = new System.Windows.Forms.Panel();
            this.dataGridTriage = new System.Windows.Forms.DataGridView();
            this.Count = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Patient = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Event = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Read = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Remark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EventStrip = new System.Windows.Forms.DataGridViewImageColumn();
            this.Triage = new System.Windows.Forms.DataGridViewImageColumn();
            this.Index = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.State = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FilePath = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NrSignals = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Study = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Active = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ModelSnr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumTrailer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel25 = new System.Windows.Forms.Panel();
            this.panel32 = new System.Windows.Forms.Panel();
            this.panel29 = new System.Windows.Forms.Panel();
            this.textBoxFindings = new System.Windows.Forms.TextBox();
            this.panel36 = new System.Windows.Forms.Panel();
            this.labelFindingCodes = new System.Windows.Forms.Label();
            this.buttonFindings = new System.Windows.Forms.Button();
            this.panel58 = new System.Windows.Forms.Panel();
            this.labelModelNr = new System.Windows.Forms.Label();
            this.panel55 = new System.Windows.Forms.Panel();
            this.buttonSetState = new System.Windows.Forms.Button();
            this.buttonEventFolder = new System.Windows.Forms.Button();
            this.buttonEventView = new System.Windows.Forms.Button();
            this.panelPriority = new System.Windows.Forms.Panel();
            this.panel50 = new System.Windows.Forms.Panel();
            this.buttonLow = new System.Windows.Forms.Button();
            this.panel53 = new System.Windows.Forms.Panel();
            this.panel51 = new System.Windows.Forms.Panel();
            this.buttonNormal2 = new System.Windows.Forms.Button();
            this.panel52 = new System.Windows.Forms.Panel();
            this.panel37 = new System.Windows.Forms.Panel();
            this.buttonHigh = new System.Windows.Forms.Button();
            this.panel40 = new System.Windows.Forms.Panel();
            this.panel49 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.panel34 = new System.Windows.Forms.Panel();
            this.textBoxStudyRemark = new System.Windows.Forms.TextBox();
            this.panel35 = new System.Windows.Forms.Panel();
            this.labelStudyProcCodes = new System.Windows.Forms.Label();
            this.labelTriageStudy = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.panel33 = new System.Windows.Forms.Panel();
            this.labelTriagePatient = new System.Windows.Forms.Label();
            this.panel57 = new System.Windows.Forms.Panel();
            this.labelPatientID = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.panel41 = new System.Windows.Forms.Panel();
            this.panel56 = new System.Windows.Forms.Panel();
            this.panel22 = new System.Windows.Forms.Panel();
            this.checkBoxInvert = new System.Windows.Forms.CheckBox();
            this.panel42 = new System.Windows.Forms.Panel();
            this.labelNrChannels = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.panel63 = new System.Windows.Forms.Panel();
            this.labelRecNr = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBoxTriageChannel = new System.Windows.Forms.ComboBox();
            this.panel66 = new System.Windows.Forms.Panel();
            this.buttonAnalyzeAdd = new System.Windows.Forms.Button();
            this.Anayze = new System.Windows.Forms.Button();
            this.panel24 = new System.Windows.Forms.Panel();
            this.panel65 = new System.Windows.Forms.Panel();
            this.labelTriageUpdTime = new System.Windows.Forms.Label();
            this.panel64 = new System.Windows.Forms.Panel();
            this.labelUtcOffset = new System.Windows.Forms.Label();
            this.labelLoadImages = new System.Windows.Forms.Label();
            this.panel61 = new System.Windows.Forms.Panel();
            this.labelDrive = new System.Windows.Forms.Label();
            this.buttonTriageFields = new System.Windows.Forms.Button();
            this.panel47 = new System.Windows.Forms.Panel();
            this.panel48 = new System.Windows.Forms.Panel();
            this.panel45 = new System.Windows.Forms.Panel();
            this.listBoxEventMode = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.panel39 = new System.Windows.Forms.Panel();
            this.textBoxViewTime = new System.Windows.Forms.TextBox();
            this.panel62 = new System.Windows.Forms.Panel();
            this.panelViewDec = new System.Windows.Forms.Panel();
            this.panelViewInc = new System.Windows.Forms.Panel();
            this.listBoxViewUnit = new System.Windows.Forms.ListBox();
            this.panel30 = new System.Windows.Forms.Panel();
            this.panel28 = new System.Windows.Forms.Panel();
            this.panelView = new System.Windows.Forms.Panel();
            this.radioButtonCurrentPatient = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.panel26 = new System.Windows.Forms.Panel();
            this.panel27 = new System.Windows.Forms.Panel();
            this.button7 = new System.Windows.Forms.Button();
            this.panel59 = new System.Windows.Forms.Panel();
            this.panel31 = new System.Windows.Forms.Panel();
            this.labelTriageRowCount = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelTriageRow = new System.Windows.Forms.Label();
            this.panel60 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.panelTriageStrip0 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPageAnalyze = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPageReport = new System.Windows.Forms.TabPage();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPageStudy = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.timerTriage = new System.Windows.Forms.Timer(this.components);
            this.contextMenuState = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTop.SuspendLayout();
            this.contextMenuSettings.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel46.SuspendLayout();
            this.panel38.SuspendLayout();
            this.panel44.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel43.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel7.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPageTriage.SuspendLayout();
            this.panel23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridTriage)).BeginInit();
            this.panel25.SuspendLayout();
            this.panel32.SuspendLayout();
            this.panel29.SuspendLayout();
            this.panel36.SuspendLayout();
            this.panel58.SuspendLayout();
            this.panel55.SuspendLayout();
            this.panelPriority.SuspendLayout();
            this.panel50.SuspendLayout();
            this.panel51.SuspendLayout();
            this.panel37.SuspendLayout();
            this.panel49.SuspendLayout();
            this.panel34.SuspendLayout();
            this.panel35.SuspendLayout();
            this.panel33.SuspendLayout();
            this.panel57.SuspendLayout();
            this.panel41.SuspendLayout();
            this.panel56.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel42.SuspendLayout();
            this.panel63.SuspendLayout();
            this.panel66.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel65.SuspendLayout();
            this.panel64.SuspendLayout();
            this.panel61.SuspendLayout();
            this.panel47.SuspendLayout();
            this.panel45.SuspendLayout();
            this.panel39.SuspendLayout();
            this.panel62.SuspendLayout();
            this.panel28.SuspendLayout();
            this.panelView.SuspendLayout();
            this.panel26.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel59.SuspendLayout();
            this.panel31.SuspendLayout();
            this.tabPageAnalyze.SuspendLayout();
            this.tabPageReport.SuspendLayout();
            this.tabPageStudy.SuspendLayout();
            this.contextMenuState.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripTop
            // 
            this.toolStripTop.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStripTop.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripWorkNew,
            this.toolStripWorld,
            this.toolStripPatientList,
            this.toolStripDepartments,
            this.toolStripCustomers,
            this.toolStripUsers,
            this.toolStripPatient,
            this.toolStripButton1,
            this.toolStripDevices,
            this.toolStripAddDevice,
            this.toolStripAlarmSignal,
            this.toolStripNewEvent,
            this.toolStripLoad,
            this.toolStripSeparator1,
            this.toolStripButton2,
            this.toolStripEvents,
            this.toolStripSeparator2,
            this.toolStripExit,
            this.toolStripUserCancel,
            this.toolStripUserLabel,
            this.toolStripUserOk,
            this.toolStripUserLock,
            this.toolStripSelectUser,
            this.toolStripSettings,
            this.toolStripHelpdesk,
            this.toolStripButton3});
            this.toolStripTop.Location = new System.Drawing.Point(0, 0);
            this.toolStripTop.Name = "toolStripTop";
            this.toolStripTop.Size = new System.Drawing.Size(1584, 39);
            this.toolStripTop.TabIndex = 0;
            this.toolStripTop.Text = "toolStrip";
            this.toolStripTop.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStrip1_ItemClicked);
            // 
            // toolStripWorkNew
            // 
            this.toolStripWorkNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripWorkNew.Image = ((System.Drawing.Image)(resources.GetObject("toolStripWorkNew.Image")));
            this.toolStripWorkNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripWorkNew.Name = "toolStripWorkNew";
            this.toolStripWorkNew.Size = new System.Drawing.Size(36, 36);
            this.toolStripWorkNew.Text = "toolStripWorkNew";
            this.toolStripWorkNew.ToolTipText = "New Study";
            this.toolStripWorkNew.Click += new System.EventHandler(this.toolStripWorkNew_Click);
            // 
            // toolStripWorld
            // 
            this.toolStripWorld.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripWorld.Image = ((System.Drawing.Image)(resources.GetObject("toolStripWorld.Image")));
            this.toolStripWorld.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripWorld.Name = "toolStripWorld";
            this.toolStripWorld.Size = new System.Drawing.Size(36, 36);
            this.toolStripWorld.Text = "toolStripWork";
            this.toolStripWorld.ToolTipText = "Study List";
            this.toolStripWorld.Click += new System.EventHandler(this.toolStripWorld_Click);
            // 
            // toolStripPatientList
            // 
            this.toolStripPatientList.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripPatientList.Image = ((System.Drawing.Image)(resources.GetObject("toolStripPatientList.Image")));
            this.toolStripPatientList.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripPatientList.Name = "toolStripPatientList";
            this.toolStripPatientList.Size = new System.Drawing.Size(36, 36);
            this.toolStripPatientList.Text = "Patient List";
            this.toolStripPatientList.Click += new System.EventHandler(this.toolStripPatientList_Click);
            // 
            // toolStripDepartments
            // 
            this.toolStripDepartments.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripDepartments.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDepartments.Image")));
            this.toolStripDepartments.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDepartments.Name = "toolStripDepartments";
            this.toolStripDepartments.Size = new System.Drawing.Size(36, 36);
            this.toolStripDepartments.Text = "toolStripDepartments";
            this.toolStripDepartments.ToolTipText = "New Hospital";
            this.toolStripDepartments.Click += new System.EventHandler(this.toolStripDepartments_Click);
            // 
            // toolStripCustomers
            // 
            this.toolStripCustomers.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripCustomers.Image = ((System.Drawing.Image)(resources.GetObject("toolStripCustomers.Image")));
            this.toolStripCustomers.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripCustomers.Name = "toolStripCustomers";
            this.toolStripCustomers.Size = new System.Drawing.Size(36, 36);
            this.toolStripCustomers.Text = "toolStripCustomers";
            this.toolStripCustomers.ToolTipText = "Hospital List";
            this.toolStripCustomers.Click += new System.EventHandler(this.toolStripCustomers_Click);
            // 
            // toolStripUsers
            // 
            this.toolStripUsers.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripUsers.Image = ((System.Drawing.Image)(resources.GetObject("toolStripUsers.Image")));
            this.toolStripUsers.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripUsers.Name = "toolStripUsers";
            this.toolStripUsers.Size = new System.Drawing.Size(36, 36);
            this.toolStripUsers.Text = "toolStripUsers";
            this.toolStripUsers.ToolTipText = "New User";
            this.toolStripUsers.Click += new System.EventHandler(this.toolStripUsers_Click);
            // 
            // toolStripPatient
            // 
            this.toolStripPatient.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripPatient.Image = ((System.Drawing.Image)(resources.GetObject("toolStripPatient.Image")));
            this.toolStripPatient.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripPatient.Name = "toolStripPatient";
            this.toolStripPatient.Size = new System.Drawing.Size(36, 36);
            this.toolStripPatient.ToolTipText = "User List";
            this.toolStripPatient.Click += new System.EventHandler(this.toolStripPatient_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton1.Text = "toolStripButton1";
            this.toolStripButton1.ToolTipText = "New Device model";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripDevices
            // 
            this.toolStripDevices.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripDevices.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDevices.Image")));
            this.toolStripDevices.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDevices.Name = "toolStripDevices";
            this.toolStripDevices.Size = new System.Drawing.Size(36, 36);
            this.toolStripDevices.Text = "toolStripDevices";
            this.toolStripDevices.ToolTipText = "Device List";
            this.toolStripDevices.Click += new System.EventHandler(this.toolStripDevices_Click);
            // 
            // toolStripAddDevice
            // 
            this.toolStripAddDevice.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripAddDevice.Image = ((System.Drawing.Image)(resources.GetObject("toolStripAddDevice.Image")));
            this.toolStripAddDevice.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripAddDevice.Name = "toolStripAddDevice";
            this.toolStripAddDevice.Size = new System.Drawing.Size(36, 36);
            this.toolStripAddDevice.Text = "Add Device";
            this.toolStripAddDevice.Click += new System.EventHandler(this.toolStripAddDevice_Click);
            // 
            // toolStripAlarmSignal
            // 
            this.toolStripAlarmSignal.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripAlarmSignal.Enabled = false;
            this.toolStripAlarmSignal.Image = ((System.Drawing.Image)(resources.GetObject("toolStripAlarmSignal.Image")));
            this.toolStripAlarmSignal.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripAlarmSignal.Name = "toolStripAlarmSignal";
            this.toolStripAlarmSignal.Size = new System.Drawing.Size(36, 36);
            this.toolStripAlarmSignal.Text = "toolStripAlarmSignal";
            // 
            // toolStripNewEvent
            // 
            this.toolStripNewEvent.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripNewEvent.Enabled = false;
            this.toolStripNewEvent.Image = ((System.Drawing.Image)(resources.GetObject("toolStripNewEvent.Image")));
            this.toolStripNewEvent.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripNewEvent.Name = "toolStripNewEvent";
            this.toolStripNewEvent.Size = new System.Drawing.Size(36, 36);
            this.toolStripNewEvent.Text = "toolStripNewEvent";
            this.toolStripNewEvent.ToolTipText = "Add new Event";
            // 
            // toolStripLoad
            // 
            this.toolStripLoad.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripLoad.Enabled = false;
            this.toolStripLoad.Image = ((System.Drawing.Image)(resources.GetObject("toolStripLoad.Image")));
            this.toolStripLoad.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripLoad.Name = "toolStripLoad";
            this.toolStripLoad.Size = new System.Drawing.Size(36, 36);
            this.toolStripLoad.Text = "toolStripLoad";
            this.toolStripLoad.ToolTipText = "Load event from file";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton2.Text = "toolStripButton2";
            this.toolStripButton2.ToolTipText = "ICD Code List";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripEvents
            // 
            this.toolStripEvents.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripEvents.Image = ((System.Drawing.Image)(resources.GetObject("toolStripEvents.Image")));
            this.toolStripEvents.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripEvents.Name = "toolStripEvents";
            this.toolStripEvents.Size = new System.Drawing.Size(36, 36);
            this.toolStripEvents.Text = "toolStripEvents";
            this.toolStripEvents.ToolTipText = "List Events";
            this.toolStripEvents.Click += new System.EventHandler(this.toolStripEvents_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripExit
            // 
            this.toolStripExit.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripExit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripExit.Image = ((System.Drawing.Image)(resources.GetObject("toolStripExit.Image")));
            this.toolStripExit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripExit.Name = "toolStripExit";
            this.toolStripExit.Size = new System.Drawing.Size(36, 36);
            this.toolStripExit.Text = "toolStripExit";
            this.toolStripExit.Click += new System.EventHandler(this.toolStripExit_Click);
            // 
            // toolStripUserCancel
            // 
            this.toolStripUserCancel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripUserCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripUserCancel.Enabled = false;
            this.toolStripUserCancel.Image = ((System.Drawing.Image)(resources.GetObject("toolStripUserCancel.Image")));
            this.toolStripUserCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripUserCancel.Name = "toolStripUserCancel";
            this.toolStripUserCancel.Size = new System.Drawing.Size(36, 36);
            this.toolStripUserCancel.Text = "toolStripUserCancel";
            // 
            // toolStripUserLabel
            // 
            this.toolStripUserLabel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripUserLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripUserLabel.Enabled = false;
            this.toolStripUserLabel.Name = "toolStripUserLabel";
            this.toolStripUserLabel.Size = new System.Drawing.Size(27, 36);
            this.toolStripUserLabel.Text = "SVlr";
            this.toolStripUserLabel.ToolTipText = "User Label";
            // 
            // toolStripUserOk
            // 
            this.toolStripUserOk.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripUserOk.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripUserOk.Enabled = false;
            this.toolStripUserOk.Image = ((System.Drawing.Image)(resources.GetObject("toolStripUserOk.Image")));
            this.toolStripUserOk.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripUserOk.Name = "toolStripUserOk";
            this.toolStripUserOk.Size = new System.Drawing.Size(36, 36);
            this.toolStripUserOk.ToolTipText = "Reactivate User";
            this.toolStripUserOk.Click += new System.EventHandler(this.toolStripUserOk_Click);
            // 
            // toolStripUserLock
            // 
            this.toolStripUserLock.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripUserLock.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripUserLock.Enabled = false;
            this.toolStripUserLock.Image = ((System.Drawing.Image)(resources.GetObject("toolStripUserLock.Image")));
            this.toolStripUserLock.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripUserLock.Name = "toolStripUserLock";
            this.toolStripUserLock.Size = new System.Drawing.Size(36, 36);
            this.toolStripUserLock.ToolTipText = "Lock User";
            // 
            // toolStripSelectUser
            // 
            this.toolStripSelectUser.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSelectUser.Enabled = false;
            this.toolStripSelectUser.Name = "toolStripSelectUser";
            this.toolStripSelectUser.Size = new System.Drawing.Size(118, 39);
            this.toolStripSelectUser.Text = "Select User...";
            this.toolStripSelectUser.ToolTipText = "Select User";
            this.toolStripSelectUser.Click += new System.EventHandler(this.toolStripDropDownButton1_Click);
            // 
            // toolStripSettings
            // 
            this.toolStripSettings.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSettings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripSettings.Enabled = false;
            this.toolStripSettings.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSettings.Image")));
            this.toolStripSettings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSettings.Name = "toolStripSettings";
            this.toolStripSettings.Size = new System.Drawing.Size(36, 36);
            this.toolStripSettings.Text = "toolStripSettings";
            this.toolStripSettings.ToolTipText = "Settings";
            // 
            // toolStripHelpdesk
            // 
            this.toolStripHelpdesk.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripHelpdesk.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripHelpdesk.Image = ((System.Drawing.Image)(resources.GetObject("toolStripHelpdesk.Image")));
            this.toolStripHelpdesk.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripHelpdesk.Name = "toolStripHelpdesk";
            this.toolStripHelpdesk.Size = new System.Drawing.Size(36, 36);
            this.toolStripHelpdesk.Text = "Helpdesk";
            this.toolStripHelpdesk.Click += new System.EventHandler(this.toolStripHelpdesk_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton3.Text = "toolStripButton3";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // contextMenuSettings
            // 
            this.contextMenuSettings.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.settingsToolStripMenuItem,
            this.userSettingsToolStripMenuItem,
            this.globalSettingsToolStripMenuItem,
            this.settingsToolStripMenuItem1,
            this.showDashBoardToolStripMenuItem});
            this.contextMenuSettings.Name = "contextMenuSettings";
            this.contextMenuSettings.Size = new System.Drawing.Size(181, 136);
            this.contextMenuSettings.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuSettings_Opening);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.toolStripMenuItem1.Text = "toolStripMenuItem1";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.settingsToolStripMenuItem.Text = "Program Settings";
            // 
            // userSettingsToolStripMenuItem
            // 
            this.userSettingsToolStripMenuItem.Name = "userSettingsToolStripMenuItem";
            this.userSettingsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.userSettingsToolStripMenuItem.Text = "User Settings";
            // 
            // globalSettingsToolStripMenuItem
            // 
            this.globalSettingsToolStripMenuItem.Name = "globalSettingsToolStripMenuItem";
            this.globalSettingsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.globalSettingsToolStripMenuItem.Text = "Global Settings";
            // 
            // settingsToolStripMenuItem1
            // 
            this.settingsToolStripMenuItem1.Name = "settingsToolStripMenuItem1";
            this.settingsToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.settingsToolStripMenuItem1.Text = "Alarm Settings";
            // 
            // showDashBoardToolStripMenuItem
            // 
            this.showDashBoardToolStripMenuItem.Name = "showDashBoardToolStripMenuItem";
            this.showDashBoardToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.showDashBoardToolStripMenuItem.Text = "Show DashBoard";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.panel46);
            this.panel1.Controls.Add(this.panel38);
            this.panel1.Controls.Add(this.panel44);
            this.panel1.Controls.Add(this.panel14);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel18);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 39);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1584, 79);
            this.panel1.TabIndex = 2;
            // 
            // panel46
            // 
            this.panel46.Controls.Add(this.labelViewMode1);
            this.panel46.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel46.Location = new System.Drawing.Point(786, 0);
            this.panel46.Name = "panel46";
            this.panel46.Size = new System.Drawing.Size(329, 77);
            this.panel46.TabIndex = 11;
            // 
            // labelViewMode1
            // 
            this.labelViewMode1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelViewMode1.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelViewMode1.ForeColor = System.Drawing.Color.Black;
            this.labelViewMode1.Location = new System.Drawing.Point(0, 0);
            this.labelViewMode1.Name = "labelViewMode1";
            this.labelViewMode1.Size = new System.Drawing.Size(329, 77);
            this.labelViewMode1.TabIndex = 0;
            this.labelViewMode1.Text = "XXXX";
            this.labelViewMode1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel38
            // 
            this.panel38.Controls.Add(this.labelUpdateTS);
            this.panel38.Controls.Add(this.labelUpdateTC);
            this.panel38.Controls.Add(this.labelStudyIndex);
            this.panel38.Controls.Add(this.labelRecIndex);
            this.panel38.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel38.Location = new System.Drawing.Point(1115, 0);
            this.panel38.Name = "panel38";
            this.panel38.Size = new System.Drawing.Size(121, 77);
            this.panel38.TabIndex = 9;
            // 
            // labelUpdateTS
            // 
            this.labelUpdateTS.AutoSize = true;
            this.labelUpdateTS.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelUpdateTS.Location = new System.Drawing.Point(0, 45);
            this.labelUpdateTS.Name = "labelUpdateTS";
            this.labelUpdateTS.Size = new System.Drawing.Size(119, 16);
            this.labelUpdateTS.TabIndex = 3;
            this.labelUpdateTS.Text = "updTS=0.000sec";
            // 
            // labelUpdateTC
            // 
            this.labelUpdateTC.AutoSize = true;
            this.labelUpdateTC.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelUpdateTC.Location = new System.Drawing.Point(0, 61);
            this.labelUpdateTC.Name = "labelUpdateTC";
            this.labelUpdateTC.Size = new System.Drawing.Size(119, 16);
            this.labelUpdateTC.TabIndex = 2;
            this.labelUpdateTC.Text = "updTC=0.000sec";
            // 
            // labelStudyIndex
            // 
            this.labelStudyIndex.AutoSize = true;
            this.labelStudyIndex.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStudyIndex.Location = new System.Drawing.Point(0, 16);
            this.labelStudyIndex.Name = "labelStudyIndex";
            this.labelStudyIndex.Size = new System.Drawing.Size(27, 16);
            this.labelStudyIndex.TabIndex = 1;
            this.labelStudyIndex.Text = "S#";
            // 
            // labelRecIndex
            // 
            this.labelRecIndex.AutoSize = true;
            this.labelRecIndex.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelRecIndex.Location = new System.Drawing.Point(0, 0);
            this.labelRecIndex.Name = "labelRecIndex";
            this.labelRecIndex.Size = new System.Drawing.Size(26, 16);
            this.labelRecIndex.TabIndex = 0;
            this.labelRecIndex.Text = "R#";
            this.labelRecIndex.Click += new System.EventHandler(this.labelRecIndex_Click);
            // 
            // panel44
            // 
            this.panel44.Controls.Add(this.button12);
            this.panel44.Controls.Add(this.button11);
            this.panel44.Controls.Add(this.button10);
            this.panel44.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel44.Location = new System.Drawing.Point(1236, 0);
            this.panel44.Name = "panel44";
            this.panel44.Size = new System.Drawing.Size(98, 77);
            this.panel44.TabIndex = 10;
            this.panel44.Visible = false;
            // 
            // button12
            // 
            this.button12.Dock = System.Windows.Forms.DockStyle.Top;
            this.button12.Location = new System.Drawing.Point(0, 52);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(98, 26);
            this.button12.TabIndex = 2;
            this.button12.Text = "Report";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // button11
            // 
            this.button11.Dock = System.Windows.Forms.DockStyle.Top;
            this.button11.Location = new System.Drawing.Point(0, 26);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(98, 26);
            this.button11.TabIndex = 1;
            this.button11.Text = "Analyze";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button10
            // 
            this.button10.Dock = System.Windows.Forms.DockStyle.Top;
            this.button10.Location = new System.Drawing.Point(0, 0);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(98, 26);
            this.button10.TabIndex = 0;
            this.button10.Text = "Triage";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // panel14
            // 
            this.panel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel14.Controls.Add(this.panel21);
            this.panel14.Controls.Add(this.label1UserMsg);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel14.Location = new System.Drawing.Point(1334, 0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(116, 77);
            this.panel14.TabIndex = 3;
            // 
            // panel21
            // 
            this.panel21.Controls.Add(this.panel15);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel21.Location = new System.Drawing.Point(0, 25);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(114, 50);
            this.panel21.TabIndex = 6;
            // 
            // panel15
            // 
            this.panel15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel15.Controls.Add(this.label2UserMsg);
            this.panel15.Controls.Add(this.ValueUserMsg);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel15.Location = new System.Drawing.Point(0, 0);
            this.panel15.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(114, 50);
            this.panel15.TabIndex = 7;
            // 
            // label2UserMsg
            // 
            this.label2UserMsg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2UserMsg.ForeColor = System.Drawing.Color.Black;
            this.label2UserMsg.Location = new System.Drawing.Point(0, 0);
            this.label2UserMsg.Name = "label2UserMsg";
            this.label2UserMsg.Size = new System.Drawing.Size(112, 21);
            this.label2UserMsg.TabIndex = 6;
            this.label2UserMsg.Text = "Messages:";
            this.label2UserMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ValueUserMsg
            // 
            this.ValueUserMsg.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ValueUserMsg.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ValueUserMsg.ForeColor = System.Drawing.Color.Black;
            this.ValueUserMsg.Location = new System.Drawing.Point(0, 21);
            this.ValueUserMsg.Name = "ValueUserMsg";
            this.ValueUserMsg.Size = new System.Drawing.Size(112, 27);
            this.ValueUserMsg.TabIndex = 5;
            this.ValueUserMsg.Text = "-";
            this.ValueUserMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1UserMsg
            // 
            this.label1UserMsg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1UserMsg.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1UserMsg.ForeColor = System.Drawing.Color.Black;
            this.label1UserMsg.Location = new System.Drawing.Point(0, 0);
            this.label1UserMsg.Name = "label1UserMsg";
            this.label1UserMsg.Size = new System.Drawing.Size(114, 25);
            this.label1UserMsg.TabIndex = 5;
            this.label1UserMsg.Text = "User";
            this.label1UserMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.panel8);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(1450, 0);
            this.panel4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(132, 77);
            this.panel4.TabIndex = 2;
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.panel17);
            this.panel8.Controls.Add(this.labelTime);
            this.panel8.Controls.Add(this.labelDate);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(130, 75);
            this.panel8.TabIndex = 0;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.buttonRefresh);
            this.panel17.Controls.Add(this.panel43);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel17.Location = new System.Drawing.Point(0, 38);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(128, 35);
            this.panel17.TabIndex = 4;
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonRefresh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonRefresh.FlatAppearance.BorderSize = 0;
            this.buttonRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRefresh.Image = ((System.Drawing.Image)(resources.GetObject("buttonRefresh.Image")));
            this.buttonRefresh.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonRefresh.Location = new System.Drawing.Point(0, 0);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(104, 35);
            this.buttonRefresh.TabIndex = 0;
            this.buttonRefresh.Text = "refresh";
            this.buttonRefresh.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonRefresh.UseVisualStyleBackColor = false;
            // 
            // panel43
            // 
            this.panel43.Controls.Add(this.button8);
            this.panel43.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel43.Location = new System.Drawing.Point(104, 0);
            this.panel43.Name = "panel43";
            this.panel43.Size = new System.Drawing.Size(24, 35);
            this.panel43.TabIndex = 2;
            // 
            // button8
            // 
            this.button8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.button8.FlatAppearance.BorderSize = 0;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.Font = new System.Drawing.Font("Webdings", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.button8.Location = new System.Drawing.Point(0, 12);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(24, 23);
            this.button8.TabIndex = 2;
            this.button8.Text = "5";
            this.button8.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button8.UseVisualStyleBackColor = true;
            // 
            // labelTime
            // 
            this.labelTime.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelTime.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTime.ForeColor = System.Drawing.Color.Black;
            this.labelTime.Location = new System.Drawing.Point(0, 19);
            this.labelTime.Name = "labelTime";
            this.labelTime.Size = new System.Drawing.Size(128, 19);
            this.labelTime.TabIndex = 3;
            this.labelTime.Text = "10:23 pm";
            this.labelTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelTime.UseMnemonic = false;
            // 
            // labelDate
            // 
            this.labelDate.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDate.ForeColor = System.Drawing.Color.Black;
            this.labelDate.Location = new System.Drawing.Point(0, 0);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new System.Drawing.Size(128, 19);
            this.labelDate.TabIndex = 2;
            this.labelDate.Text = "ma 24/6/2016";
            this.labelDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.panel20);
            this.panel3.Controls.Add(this.label1EventDone24);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(304, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(482, 77);
            this.panel3.TabIndex = 1;
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.panel11);
            this.panel20.Controls.Add(this.panel12);
            this.panel20.Controls.Add(this.panel10);
            this.panel20.Controls.Add(this.panel9);
            this.panel20.Controls.Add(this.panel13);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel20.Location = new System.Drawing.Point(0, 25);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(480, 50);
            this.panel20.TabIndex = 6;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.label2EventReport);
            this.panel11.Controls.Add(this.valueEventReport);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel11.Location = new System.Drawing.Point(397, 0);
            this.panel11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(83, 50);
            this.panel11.TabIndex = 11;
            // 
            // label2EventReport
            // 
            this.label2EventReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2EventReport.ForeColor = System.Drawing.Color.Black;
            this.label2EventReport.Location = new System.Drawing.Point(0, 0);
            this.label2EventReport.Name = "label2EventReport";
            this.label2EventReport.Size = new System.Drawing.Size(83, 23);
            this.label2EventReport.TabIndex = 6;
            this.label2EventReport.Text = "Report:";
            this.label2EventReport.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // valueEventReport
            // 
            this.valueEventReport.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.valueEventReport.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.valueEventReport.ForeColor = System.Drawing.Color.Black;
            this.valueEventReport.Location = new System.Drawing.Point(0, 23);
            this.valueEventReport.Name = "valueEventReport";
            this.valueEventReport.Size = new System.Drawing.Size(83, 27);
            this.valueEventReport.TabIndex = 5;
            this.valueEventReport.Text = "-";
            this.valueEventReport.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.label2EventAnalyze);
            this.panel12.Controls.Add(this.valueEventAnalyze);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel12.Location = new System.Drawing.Point(290, 0);
            this.panel12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(107, 50);
            this.panel12.TabIndex = 10;
            // 
            // label2EventAnalyze
            // 
            this.label2EventAnalyze.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2EventAnalyze.ForeColor = System.Drawing.Color.Black;
            this.label2EventAnalyze.Location = new System.Drawing.Point(0, 0);
            this.label2EventAnalyze.Name = "label2EventAnalyze";
            this.label2EventAnalyze.Size = new System.Drawing.Size(107, 23);
            this.label2EventAnalyze.TabIndex = 6;
            this.label2EventAnalyze.Text = "Analyze:";
            this.label2EventAnalyze.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // valueEventAnalyze
            // 
            this.valueEventAnalyze.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.valueEventAnalyze.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.valueEventAnalyze.ForeColor = System.Drawing.Color.Black;
            this.valueEventAnalyze.Location = new System.Drawing.Point(0, 23);
            this.valueEventAnalyze.Name = "valueEventAnalyze";
            this.valueEventAnalyze.Size = new System.Drawing.Size(107, 27);
            this.valueEventAnalyze.TabIndex = 5;
            this.valueEventAnalyze.Text = "-";
            this.valueEventAnalyze.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.label2EventTriage);
            this.panel10.Controls.Add(this.valueEventTriage);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel10.Location = new System.Drawing.Point(195, 0);
            this.panel10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(95, 50);
            this.panel10.TabIndex = 9;
            // 
            // label2EventTriage
            // 
            this.label2EventTriage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2EventTriage.ForeColor = System.Drawing.Color.Black;
            this.label2EventTriage.Location = new System.Drawing.Point(0, 0);
            this.label2EventTriage.Name = "label2EventTriage";
            this.label2EventTriage.Size = new System.Drawing.Size(95, 23);
            this.label2EventTriage.TabIndex = 6;
            this.label2EventTriage.Text = "Triage:";
            this.label2EventTriage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // valueEventTriage
            // 
            this.valueEventTriage.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.valueEventTriage.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.valueEventTriage.ForeColor = System.Drawing.Color.Black;
            this.valueEventTriage.Location = new System.Drawing.Point(0, 23);
            this.valueEventTriage.Name = "valueEventTriage";
            this.valueEventTriage.Size = new System.Drawing.Size(95, 27);
            this.valueEventTriage.TabIndex = 5;
            this.valueEventTriage.Text = "-";
            this.valueEventTriage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.label2EventTotalOpen);
            this.panel9.Controls.Add(this.valueEventTotalOpen);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel9.Location = new System.Drawing.Point(103, 0);
            this.panel9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(92, 50);
            this.panel9.TabIndex = 8;
            // 
            // label2EventTotalOpen
            // 
            this.label2EventTotalOpen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2EventTotalOpen.ForeColor = System.Drawing.Color.Black;
            this.label2EventTotalOpen.Location = new System.Drawing.Point(0, 0);
            this.label2EventTotalOpen.Name = "label2EventTotalOpen";
            this.label2EventTotalOpen.Size = new System.Drawing.Size(92, 23);
            this.label2EventTotalOpen.TabIndex = 6;
            this.label2EventTotalOpen.Text = "To Do:";
            this.label2EventTotalOpen.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // valueEventTotalOpen
            // 
            this.valueEventTotalOpen.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.valueEventTotalOpen.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.valueEventTotalOpen.ForeColor = System.Drawing.Color.Black;
            this.valueEventTotalOpen.Location = new System.Drawing.Point(0, 23);
            this.valueEventTotalOpen.Name = "valueEventTotalOpen";
            this.valueEventTotalOpen.Size = new System.Drawing.Size(92, 27);
            this.valueEventTotalOpen.TabIndex = 5;
            this.valueEventTotalOpen.Text = "-";
            this.valueEventTotalOpen.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.label2EventDone24);
            this.panel13.Controls.Add(this.valueEventDone24);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel13.Location = new System.Drawing.Point(0, 0);
            this.panel13.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(103, 50);
            this.panel13.TabIndex = 7;
            // 
            // label2EventDone24
            // 
            this.label2EventDone24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2EventDone24.ForeColor = System.Drawing.Color.Black;
            this.label2EventDone24.Location = new System.Drawing.Point(0, 0);
            this.label2EventDone24.Name = "label2EventDone24";
            this.label2EventDone24.Size = new System.Drawing.Size(103, 23);
            this.label2EventDone24.TabIndex = 6;
            this.label2EventDone24.Text = "Done<24hrs:";
            this.label2EventDone24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // valueEventDone24
            // 
            this.valueEventDone24.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.valueEventDone24.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.valueEventDone24.ForeColor = System.Drawing.Color.Black;
            this.valueEventDone24.Location = new System.Drawing.Point(0, 23);
            this.valueEventDone24.Name = "valueEventDone24";
            this.valueEventDone24.Size = new System.Drawing.Size(103, 27);
            this.valueEventDone24.TabIndex = 5;
            this.valueEventDone24.Text = "-";
            this.valueEventDone24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1EventDone24
            // 
            this.label1EventDone24.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1EventDone24.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1EventDone24.ForeColor = System.Drawing.Color.Black;
            this.label1EventDone24.Location = new System.Drawing.Point(0, 0);
            this.label1EventDone24.Name = "label1EventDone24";
            this.label1EventDone24.Size = new System.Drawing.Size(480, 25);
            this.label1EventDone24.TabIndex = 5;
            this.label1EventDone24.Text = "EVENTS";
            this.label1EventDone24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel18
            // 
            this.panel18.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel18.Location = new System.Drawing.Point(288, 0);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(16, 77);
            this.panel18.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.panel19);
            this.panel2.Controls.Add(this.label1ProcedureDone24);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.ForeColor = System.Drawing.Color.White;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(288, 77);
            this.panel2.TabIndex = 0;
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.panel6);
            this.panel19.Controls.Add(this.panel5);
            this.panel19.Controls.Add(this.panel7);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel19.Location = new System.Drawing.Point(0, 25);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(286, 50);
            this.panel19.TabIndex = 6;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.label2ReportToDo);
            this.panel6.Controls.Add(this.ValueReportToDo24);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(194, 0);
            this.panel6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(92, 50);
            this.panel6.TabIndex = 7;
            // 
            // label2ReportToDo
            // 
            this.label2ReportToDo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2ReportToDo.ForeColor = System.Drawing.Color.Black;
            this.label2ReportToDo.Location = new System.Drawing.Point(0, 0);
            this.label2ReportToDo.Name = "label2ReportToDo";
            this.label2ReportToDo.Size = new System.Drawing.Size(92, 23);
            this.label2ReportToDo.TabIndex = 6;
            this.label2ReportToDo.Text = "To Report:";
            this.label2ReportToDo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ValueReportToDo24
            // 
            this.ValueReportToDo24.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ValueReportToDo24.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ValueReportToDo24.ForeColor = System.Drawing.Color.Black;
            this.ValueReportToDo24.Location = new System.Drawing.Point(0, 23);
            this.ValueReportToDo24.Name = "ValueReportToDo24";
            this.ValueReportToDo24.Size = new System.Drawing.Size(92, 27);
            this.ValueReportToDo24.TabIndex = 5;
            this.ValueReportToDo24.Text = "-";
            this.ValueReportToDo24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.label2ProcedureOpen);
            this.panel5.Controls.Add(this.valueStudyOpen);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel5.Location = new System.Drawing.Point(97, 0);
            this.panel5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(97, 50);
            this.panel5.TabIndex = 5;
            // 
            // label2ProcedureOpen
            // 
            this.label2ProcedureOpen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2ProcedureOpen.ForeColor = System.Drawing.Color.Black;
            this.label2ProcedureOpen.Location = new System.Drawing.Point(0, 0);
            this.label2ProcedureOpen.Name = "label2ProcedureOpen";
            this.label2ProcedureOpen.Size = new System.Drawing.Size(97, 23);
            this.label2ProcedureOpen.TabIndex = 6;
            this.label2ProcedureOpen.Text = "Open:";
            this.label2ProcedureOpen.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // valueStudyOpen
            // 
            this.valueStudyOpen.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.valueStudyOpen.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.valueStudyOpen.ForeColor = System.Drawing.Color.Black;
            this.valueStudyOpen.Location = new System.Drawing.Point(0, 23);
            this.valueStudyOpen.Name = "valueStudyOpen";
            this.valueStudyOpen.Size = new System.Drawing.Size(97, 27);
            this.valueStudyOpen.TabIndex = 5;
            this.valueStudyOpen.Text = "-";
            this.valueStudyOpen.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.label2ProcedureDone24);
            this.panel7.Controls.Add(this.valueStudyDone24);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel7.ForeColor = System.Drawing.Color.White;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(97, 50);
            this.panel7.TabIndex = 6;
            // 
            // label2ProcedureDone24
            // 
            this.label2ProcedureDone24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2ProcedureDone24.ForeColor = System.Drawing.Color.Black;
            this.label2ProcedureDone24.Location = new System.Drawing.Point(0, 0);
            this.label2ProcedureDone24.Name = "label2ProcedureDone24";
            this.label2ProcedureDone24.Size = new System.Drawing.Size(97, 23);
            this.label2ProcedureDone24.TabIndex = 6;
            this.label2ProcedureDone24.Text = "Done<24hrs:";
            this.label2ProcedureDone24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // valueStudyDone24
            // 
            this.valueStudyDone24.BackColor = System.Drawing.Color.Transparent;
            this.valueStudyDone24.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.valueStudyDone24.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.valueStudyDone24.ForeColor = System.Drawing.Color.Black;
            this.valueStudyDone24.Location = new System.Drawing.Point(0, 23);
            this.valueStudyDone24.Name = "valueStudyDone24";
            this.valueStudyDone24.Size = new System.Drawing.Size(97, 27);
            this.valueStudyDone24.TabIndex = 5;
            this.valueStudyDone24.Text = "-";
            this.valueStudyDone24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1ProcedureDone24
            // 
            this.label1ProcedureDone24.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1ProcedureDone24.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1ProcedureDone24.ForeColor = System.Drawing.Color.Black;
            this.label1ProcedureDone24.Location = new System.Drawing.Point(0, 0);
            this.label1ProcedureDone24.Name = "label1ProcedureDone24";
            this.label1ProcedureDone24.Size = new System.Drawing.Size(286, 25);
            this.label1ProcedureDone24.TabIndex = 5;
            this.label1ProcedureDone24.Text = "STUDY";
            this.label1ProcedureDone24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label1ProcedureDone24.Click += new System.EventHandler(this.label1ProcedureDone24_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.statusStrip1.Location = new System.Drawing.Point(0, 799);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1584, 22);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.Gainsboro;
            this.panel16.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel16.Location = new System.Drawing.Point(0, 118);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(1584, 13);
            this.panel16.TabIndex = 4;
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl1.Controls.Add(this.tabPageTriage);
            this.tabControl1.Controls.Add(this.tabPageAnalyze);
            this.tabControl1.Controls.Add(this.tabPageReport);
            this.tabControl1.Controls.Add(this.tabPageStudy);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.ItemSize = new System.Drawing.Size(140, 23);
            this.tabControl1.Location = new System.Drawing.Point(0, 131);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(3, 13, 3, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1584, 668);
            this.tabControl1.TabIndex = 5;
            // 
            // tabPageTriage
            // 
            this.tabPageTriage.BackColor = System.Drawing.Color.Gainsboro;
            this.tabPageTriage.Controls.Add(this.panel23);
            this.tabPageTriage.Controls.Add(this.panel25);
            this.tabPageTriage.Controls.Add(this.panel24);
            this.tabPageTriage.Controls.Add(this.panelTriageStrip0);
            this.tabPageTriage.Controls.Add(this.label2);
            this.tabPageTriage.Location = new System.Drawing.Point(4, 27);
            this.tabPageTriage.Name = "tabPageTriage";
            this.tabPageTriage.Size = new System.Drawing.Size(1576, 637);
            this.tabPageTriage.TabIndex = 2;
            this.tabPageTriage.Text = "Event";
            // 
            // panel23
            // 
            this.panel23.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel23.Controls.Add(this.dataGridTriage);
            this.panel23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel23.Location = new System.Drawing.Point(0, 48);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(1576, 396);
            this.panel23.TabIndex = 3;
            // 
            // dataGridTriage
            // 
            this.dataGridTriage.AllowUserToAddRows = false;
            this.dataGridTriage.AllowUserToDeleteRows = false;
            this.dataGridTriage.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridTriage.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Count,
            this.Patient,
            this.Event,
            this.Read,
            this.Remark,
            this.EventStrip,
            this.Triage,
            this.Index,
            this.State,
            this.FilePath,
            this.FileName,
            this.NrSignals,
            this.Study,
            this.Active,
            this.ModelSnr,
            this.ColumTrailer});
            this.dataGridTriage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridTriage.Location = new System.Drawing.Point(0, 0);
            this.dataGridTriage.MultiSelect = false;
            this.dataGridTriage.Name = "dataGridTriage";
            this.dataGridTriage.ReadOnly = true;
            this.dataGridTriage.RowHeadersWidth = 20;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridTriage.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridTriage.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            this.dataGridTriage.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Verdana", 10F);
            this.dataGridTriage.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridTriage.RowTemplate.Height = 70;
            this.dataGridTriage.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridTriage.ShowEditingIcon = false;
            this.dataGridTriage.Size = new System.Drawing.Size(1576, 396);
            this.dataGridTriage.TabIndex = 0;
            this.dataGridTriage.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridTriage_CellClick);
            this.dataGridTriage.CellMouseMove += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridTriage_CellMouseMove);
            this.dataGridTriage.SelectionChanged += new System.EventHandler(this.dataGridTriage_SelectionChanged);
            this.dataGridTriage.DoubleClick += new System.EventHandler(this.dataGridTriage_DoubleClick);
            this.dataGridTriage.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dataGridTriage_MouseUp);
            // 
            // Count
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Count.DefaultCellStyle = dataGridViewCellStyle1;
            this.Count.HeaderText = "Count";
            this.Count.Name = "Count";
            this.Count.ReadOnly = true;
            this.Count.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Count.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Count.ToolTipText = "Priority & State";
            this.Count.Width = 76;
            // 
            // Patient
            // 
            this.Patient.HeaderText = "Patient";
            this.Patient.Name = "Patient";
            this.Patient.ReadOnly = true;
            this.Patient.Width = 140;
            // 
            // Event
            // 
            this.Event.HeaderText = "Event";
            this.Event.Name = "Event";
            this.Event.ReadOnly = true;
            this.Event.Width = 110;
            // 
            // Read
            // 
            this.Read.HeaderText = "Received";
            this.Read.Name = "Read";
            this.Read.ReadOnly = true;
            this.Read.Width = 110;
            // 
            // Remark
            // 
            this.Remark.HeaderText = "Remark (RSA)";
            this.Remark.Name = "Remark";
            this.Remark.ReadOnly = true;
            this.Remark.Width = 170;
            // 
            // EventStrip
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.NullValue = null;
            dataGridViewCellStyle2.Padding = new System.Windows.Forms.Padding(2, 2, 0, 0);
            this.EventStrip.DefaultCellStyle = dataGridViewCellStyle2;
            this.EventStrip.HeaderText = "Event Strip";
            this.EventStrip.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
            this.EventStrip.Name = "EventStrip";
            this.EventStrip.ReadOnly = true;
            this.EventStrip.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.EventStrip.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.EventStrip.Width = 775;
            // 
            // Triage
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Courier New", 10.2F);
            dataGridViewCellStyle3.NullValue = null;
            dataGridViewCellStyle3.Padding = new System.Windows.Forms.Padding(2, 2, 0, 0);
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Triage.DefaultCellStyle = dataGridViewCellStyle3;
            this.Triage.HeaderText = "Triage";
            this.Triage.Name = "Triage";
            this.Triage.ReadOnly = true;
            this.Triage.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Triage.ToolTipText = "Execute Action";
            this.Triage.Width = 160;
            // 
            // Index
            // 
            this.Index.HeaderText = "Index";
            this.Index.Name = "Index";
            this.Index.ReadOnly = true;
            this.Index.Visible = false;
            // 
            // State
            // 
            this.State.HeaderText = "State";
            this.State.Name = "State";
            this.State.ReadOnly = true;
            this.State.Visible = false;
            // 
            // FilePath
            // 
            this.FilePath.HeaderText = "FilePath";
            this.FilePath.Name = "FilePath";
            this.FilePath.ReadOnly = true;
            this.FilePath.Visible = false;
            // 
            // FileName
            // 
            this.FileName.HeaderText = "File Name";
            this.FileName.Name = "FileName";
            this.FileName.ReadOnly = true;
            this.FileName.Visible = false;
            // 
            // NrSignals
            // 
            this.NrSignals.HeaderText = "NrSignals";
            this.NrSignals.Name = "NrSignals";
            this.NrSignals.ReadOnly = true;
            this.NrSignals.Visible = false;
            // 
            // Study
            // 
            this.Study.HeaderText = "Study";
            this.Study.Name = "Study";
            this.Study.ReadOnly = true;
            this.Study.Visible = false;
            // 
            // Active
            // 
            this.Active.HeaderText = "Active";
            this.Active.Name = "Active";
            this.Active.ReadOnly = true;
            this.Active.Visible = false;
            // 
            // ModelSnr
            // 
            this.ModelSnr.HeaderText = "ModelSnr";
            this.ModelSnr.Name = "ModelSnr";
            this.ModelSnr.ReadOnly = true;
            this.ModelSnr.Visible = false;
            // 
            // ColumTrailer
            // 
            this.ColumTrailer.HeaderText = "C";
            this.ColumTrailer.Name = "ColumTrailer";
            this.ColumTrailer.ReadOnly = true;
            this.ColumTrailer.Visible = false;
            this.ColumTrailer.Width = 10;
            // 
            // panel25
            // 
            this.panel25.Controls.Add(this.panel32);
            this.panel25.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel25.Location = new System.Drawing.Point(0, 444);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(1576, 101);
            this.panel25.TabIndex = 5;
            // 
            // panel32
            // 
            this.panel32.Controls.Add(this.panel29);
            this.panel32.Controls.Add(this.panel58);
            this.panel32.Controls.Add(this.panel55);
            this.panel32.Controls.Add(this.panelPriority);
            this.panel32.Controls.Add(this.panel34);
            this.panel32.Controls.Add(this.panel33);
            this.panel32.Controls.Add(this.panel41);
            this.panel32.Controls.Add(this.panel66);
            this.panel32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel32.Location = new System.Drawing.Point(0, 0);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(1576, 101);
            this.panel32.TabIndex = 0;
            // 
            // panel29
            // 
            this.panel29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel29.Controls.Add(this.textBoxFindings);
            this.panel29.Controls.Add(this.panel36);
            this.panel29.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel29.Location = new System.Drawing.Point(641, 0);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(332, 101);
            this.panel29.TabIndex = 15;
            // 
            // textBoxFindings
            // 
            this.textBoxFindings.BackColor = System.Drawing.Color.Gainsboro;
            this.textBoxFindings.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxFindings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxFindings.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxFindings.Location = new System.Drawing.Point(0, 31);
            this.textBoxFindings.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.textBoxFindings.Multiline = true;
            this.textBoxFindings.Name = "textBoxFindings";
            this.textBoxFindings.ReadOnly = true;
            this.textBoxFindings.Size = new System.Drawing.Size(330, 68);
            this.textBoxFindings.TabIndex = 5;
            this.textBoxFindings.Text = "<finding code>\r\n2\r\n1234567890123456789012345678901234567890\r\n<remark>";
            // 
            // panel36
            // 
            this.panel36.Controls.Add(this.labelFindingCodes);
            this.panel36.Controls.Add(this.buttonFindings);
            this.panel36.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel36.Location = new System.Drawing.Point(0, 0);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(330, 31);
            this.panel36.TabIndex = 2;
            // 
            // labelFindingCodes
            // 
            this.labelFindingCodes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelFindingCodes.Location = new System.Drawing.Point(123, 0);
            this.labelFindingCodes.Name = "labelFindingCodes";
            this.labelFindingCodes.Size = new System.Drawing.Size(207, 31);
            this.labelFindingCodes.TabIndex = 3;
            this.labelFindingCodes.Text = ".";
            this.labelFindingCodes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // buttonFindings
            // 
            this.buttonFindings.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonFindings.Image = ((System.Drawing.Image)(resources.GetObject("buttonFindings.Image")));
            this.buttonFindings.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonFindings.Location = new System.Drawing.Point(0, 0);
            this.buttonFindings.Name = "buttonFindings";
            this.buttonFindings.Size = new System.Drawing.Size(123, 31);
            this.buttonFindings.TabIndex = 4;
            this.buttonFindings.Text = "Findings:";
            this.buttonFindings.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonFindings.UseVisualStyleBackColor = true;
            this.buttonFindings.Click += new System.EventHandler(this.buttonFindings_Click);
            // 
            // panel58
            // 
            this.panel58.Controls.Add(this.labelModelNr);
            this.panel58.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel58.Location = new System.Drawing.Point(1005, 0);
            this.panel58.Name = "panel58";
            this.panel58.Size = new System.Drawing.Size(162, 101);
            this.panel58.TabIndex = 13;
            // 
            // labelModelNr
            // 
            this.labelModelNr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelModelNr.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelModelNr.Location = new System.Drawing.Point(0, 0);
            this.labelModelNr.Name = "labelModelNr";
            this.labelModelNr.Size = new System.Drawing.Size(162, 101);
            this.labelModelNr.TabIndex = 0;
            this.labelModelNr.Text = ".";
            this.labelModelNr.Click += new System.EventHandler(this.labelModelNr_Click);
            // 
            // panel55
            // 
            this.panel55.Controls.Add(this.buttonSetState);
            this.panel55.Controls.Add(this.buttonEventFolder);
            this.panel55.Controls.Add(this.buttonEventView);
            this.panel55.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel55.Location = new System.Drawing.Point(1167, 0);
            this.panel55.Name = "panel55";
            this.panel55.Size = new System.Drawing.Size(63, 101);
            this.panel55.TabIndex = 11;
            // 
            // buttonSetState
            // 
            this.buttonSetState.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonSetState.Font = new System.Drawing.Font("Verdana", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSetState.Location = new System.Drawing.Point(0, 45);
            this.buttonSetState.Name = "buttonSetState";
            this.buttonSetState.Size = new System.Drawing.Size(63, 16);
            this.buttonSetState.TabIndex = 12;
            this.buttonSetState.Text = "Set State";
            this.buttonSetState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonSetState.UseVisualStyleBackColor = true;
            this.buttonSetState.Click += new System.EventHandler(this.buttonSetState_Click_1);
            // 
            // buttonEventFolder
            // 
            this.buttonEventFolder.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonEventFolder.Image = ((System.Drawing.Image)(resources.GetObject("buttonEventFolder.Image")));
            this.buttonEventFolder.Location = new System.Drawing.Point(0, 61);
            this.buttonEventFolder.Name = "buttonEventFolder";
            this.buttonEventFolder.Size = new System.Drawing.Size(63, 40);
            this.buttonEventFolder.TabIndex = 11;
            this.buttonEventFolder.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonEventFolder.UseVisualStyleBackColor = true;
            this.buttonEventFolder.Click += new System.EventHandler(this.buttonEventFolder_Click);
            // 
            // buttonEventView
            // 
            this.buttonEventView.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonEventView.Image = ((System.Drawing.Image)(resources.GetObject("buttonEventView.Image")));
            this.buttonEventView.Location = new System.Drawing.Point(0, 0);
            this.buttonEventView.Name = "buttonEventView";
            this.buttonEventView.Size = new System.Drawing.Size(63, 45);
            this.buttonEventView.TabIndex = 10;
            this.buttonEventView.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonEventView.UseVisualStyleBackColor = true;
            this.buttonEventView.Click += new System.EventHandler(this.buttonEventView_Click);
            // 
            // panelPriority
            // 
            this.panelPriority.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelPriority.Controls.Add(this.panel50);
            this.panelPriority.Controls.Add(this.panel51);
            this.panelPriority.Controls.Add(this.panel37);
            this.panelPriority.Controls.Add(this.panel49);
            this.panelPriority.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelPriority.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelPriority.Location = new System.Drawing.Point(1230, 0);
            this.panelPriority.Name = "panelPriority";
            this.panelPriority.Size = new System.Drawing.Size(108, 101);
            this.panelPriority.TabIndex = 4;
            // 
            // panel50
            // 
            this.panel50.Controls.Add(this.buttonLow);
            this.panel50.Controls.Add(this.panel53);
            this.panel50.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel50.Location = new System.Drawing.Point(0, 68);
            this.panel50.Name = "panel50";
            this.panel50.Size = new System.Drawing.Size(106, 24);
            this.panel50.TabIndex = 3;
            // 
            // buttonLow
            // 
            this.buttonLow.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonLow.Location = new System.Drawing.Point(10, 0);
            this.buttonLow.Name = "buttonLow";
            this.buttonLow.Size = new System.Drawing.Size(92, 24);
            this.buttonLow.TabIndex = 7;
            this.buttonLow.Text = "3 Low";
            this.buttonLow.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonLow.UseVisualStyleBackColor = true;
            // 
            // panel53
            // 
            this.panel53.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel53.Location = new System.Drawing.Point(0, 0);
            this.panel53.Name = "panel53";
            this.panel53.Size = new System.Drawing.Size(10, 24);
            this.panel53.TabIndex = 6;
            // 
            // panel51
            // 
            this.panel51.Controls.Add(this.buttonNormal2);
            this.panel51.Controls.Add(this.panel52);
            this.panel51.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel51.Location = new System.Drawing.Point(0, 44);
            this.panel51.Name = "panel51";
            this.panel51.Size = new System.Drawing.Size(106, 24);
            this.panel51.TabIndex = 4;
            // 
            // buttonNormal2
            // 
            this.buttonNormal2.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonNormal2.Location = new System.Drawing.Point(10, 0);
            this.buttonNormal2.Name = "buttonNormal2";
            this.buttonNormal2.Size = new System.Drawing.Size(92, 24);
            this.buttonNormal2.TabIndex = 7;
            this.buttonNormal2.Text = "2 Normal";
            this.buttonNormal2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonNormal2.UseVisualStyleBackColor = true;
            // 
            // panel52
            // 
            this.panel52.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel52.Location = new System.Drawing.Point(0, 0);
            this.panel52.Name = "panel52";
            this.panel52.Size = new System.Drawing.Size(10, 24);
            this.panel52.TabIndex = 6;
            // 
            // panel37
            // 
            this.panel37.Controls.Add(this.buttonHigh);
            this.panel37.Controls.Add(this.panel40);
            this.panel37.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel37.Location = new System.Drawing.Point(0, 20);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(106, 24);
            this.panel37.TabIndex = 1;
            // 
            // buttonHigh
            // 
            this.buttonHigh.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonHigh.Location = new System.Drawing.Point(10, 0);
            this.buttonHigh.Name = "buttonHigh";
            this.buttonHigh.Size = new System.Drawing.Size(92, 24);
            this.buttonHigh.TabIndex = 6;
            this.buttonHigh.Text = "1 High";
            this.buttonHigh.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonHigh.UseVisualStyleBackColor = true;
            // 
            // panel40
            // 
            this.panel40.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel40.Location = new System.Drawing.Point(0, 0);
            this.panel40.Name = "panel40";
            this.panel40.Size = new System.Drawing.Size(10, 24);
            this.panel40.TabIndex = 5;
            // 
            // panel49
            // 
            this.panel49.Controls.Add(this.label14);
            this.panel49.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel49.Location = new System.Drawing.Point(0, 0);
            this.panel49.Name = "panel49";
            this.panel49.Size = new System.Drawing.Size(106, 20);
            this.panel49.TabIndex = 2;
            // 
            // label14
            // 
            this.label14.Dock = System.Windows.Forms.DockStyle.Left;
            this.label14.Location = new System.Drawing.Point(0, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(100, 20);
            this.label14.TabIndex = 1;
            this.label14.Text = " Priority:   ";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel34
            // 
            this.panel34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel34.Controls.Add(this.textBoxStudyRemark);
            this.panel34.Controls.Add(this.panel35);
            this.panel34.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel34.Location = new System.Drawing.Point(301, 0);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(340, 101);
            this.panel34.TabIndex = 2;
            // 
            // textBoxStudyRemark
            // 
            this.textBoxStudyRemark.BackColor = System.Drawing.Color.Gainsboro;
            this.textBoxStudyRemark.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxStudyRemark.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxStudyRemark.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxStudyRemark.Location = new System.Drawing.Point(0, 31);
            this.textBoxStudyRemark.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.textBoxStudyRemark.Multiline = true;
            this.textBoxStudyRemark.Name = "textBoxStudyRemark";
            this.textBoxStudyRemark.ReadOnly = true;
            this.textBoxStudyRemark.Size = new System.Drawing.Size(338, 68);
            this.textBoxStudyRemark.TabIndex = 4;
            this.textBoxStudyRemark.Text = "<procedure code>\r\n2\r\n1234567890123456789012345678901234567890\r\n<remark>";
            // 
            // panel35
            // 
            this.panel35.Controls.Add(this.labelStudyProcCodes);
            this.panel35.Controls.Add(this.labelTriageStudy);
            this.panel35.Controls.Add(this.label12);
            this.panel35.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel35.Location = new System.Drawing.Point(0, 0);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(338, 31);
            this.panel35.TabIndex = 2;
            // 
            // labelStudyProcCodes
            // 
            this.labelStudyProcCodes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelStudyProcCodes.Location = new System.Drawing.Point(168, 0);
            this.labelStudyProcCodes.Name = "labelStudyProcCodes";
            this.labelStudyProcCodes.Size = new System.Drawing.Size(170, 31);
            this.labelStudyProcCodes.TabIndex = 5;
            this.labelStudyProcCodes.Text = "<codes>";
            this.labelStudyProcCodes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelTriageStudy
            // 
            this.labelTriageStudy.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelTriageStudy.Location = new System.Drawing.Point(118, 0);
            this.labelTriageStudy.Name = "labelTriageStudy";
            this.labelTriageStudy.Size = new System.Drawing.Size(50, 31);
            this.labelTriageStudy.TabIndex = 3;
            this.labelTriageStudy.Text = "123";
            this.labelTriageStudy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label12
            // 
            this.label12.Dock = System.Windows.Forms.DockStyle.Left;
            this.label12.Image = ((System.Drawing.Image)(resources.GetObject("label12.Image")));
            this.label12.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label12.Location = new System.Drawing.Point(0, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(118, 31);
            this.label12.TabIndex = 2;
            this.label12.Text = "Study #:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel33
            // 
            this.panel33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel33.Controls.Add(this.labelTriagePatient);
            this.panel33.Controls.Add(this.panel57);
            this.panel33.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel33.Location = new System.Drawing.Point(0, 0);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(301, 101);
            this.panel33.TabIndex = 1;
            // 
            // labelTriagePatient
            // 
            this.labelTriagePatient.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelTriagePatient.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTriagePatient.Location = new System.Drawing.Point(0, 31);
            this.labelTriagePatient.Name = "labelTriagePatient";
            this.labelTriagePatient.Size = new System.Drawing.Size(299, 68);
            this.labelTriagePatient.TabIndex = 3;
            this.labelTriagePatient.Text = "John Philips";
            // 
            // panel57
            // 
            this.panel57.Controls.Add(this.labelPatientID);
            this.panel57.Controls.Add(this.label9);
            this.panel57.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel57.Location = new System.Drawing.Point(0, 0);
            this.panel57.Name = "panel57";
            this.panel57.Size = new System.Drawing.Size(299, 31);
            this.panel57.TabIndex = 1;
            // 
            // labelPatientID
            // 
            this.labelPatientID.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPatientID.Location = new System.Drawing.Point(137, 0);
            this.labelPatientID.Name = "labelPatientID";
            this.labelPatientID.Size = new System.Drawing.Size(162, 31);
            this.labelPatientID.TabIndex = 2;
            this.labelPatientID.Text = "-";
            this.labelPatientID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.Dock = System.Windows.Forms.DockStyle.Left;
            this.label9.Image = ((System.Drawing.Image)(resources.GetObject("label9.Image")));
            this.label9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label9.Location = new System.Drawing.Point(0, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(137, 31);
            this.label9.TabIndex = 1;
            this.label9.Text = "Patient ID:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel41
            // 
            this.panel41.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel41.Controls.Add(this.panel56);
            this.panel41.Controls.Add(this.panel42);
            this.panel41.Controls.Add(this.panel63);
            this.panel41.Controls.Add(this.comboBoxTriageChannel);
            this.panel41.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel41.Location = new System.Drawing.Point(1338, 0);
            this.panel41.Name = "panel41";
            this.panel41.Size = new System.Drawing.Size(149, 101);
            this.panel41.TabIndex = 6;
            // 
            // panel56
            // 
            this.panel56.Controls.Add(this.panel22);
            this.panel56.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel56.Location = new System.Drawing.Point(0, 25);
            this.panel56.Name = "panel56";
            this.panel56.Size = new System.Drawing.Size(147, 26);
            this.panel56.TabIndex = 3;
            // 
            // panel22
            // 
            this.panel22.Controls.Add(this.checkBoxInvert);
            this.panel22.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel22.Location = new System.Drawing.Point(0, 0);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(147, 26);
            this.panel22.TabIndex = 0;
            // 
            // checkBoxInvert
            // 
            this.checkBoxInvert.AutoSize = true;
            this.checkBoxInvert.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxInvert.Enabled = false;
            this.checkBoxInvert.Location = new System.Drawing.Point(0, 0);
            this.checkBoxInvert.Name = "checkBoxInvert";
            this.checkBoxInvert.Size = new System.Drawing.Size(148, 26);
            this.checkBoxInvert.TabIndex = 0;
            this.checkBoxInvert.Text = "Invert Channel";
            this.checkBoxInvert.UseVisualStyleBackColor = true;
            // 
            // panel42
            // 
            this.panel42.Controls.Add(this.labelNrChannels);
            this.panel42.Controls.Add(this.label15);
            this.panel42.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel42.Location = new System.Drawing.Point(0, 54);
            this.panel42.Name = "panel42";
            this.panel42.Size = new System.Drawing.Size(147, 19);
            this.panel42.TabIndex = 0;
            // 
            // labelNrChannels
            // 
            this.labelNrChannels.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelNrChannels.Location = new System.Drawing.Point(94, 0);
            this.labelNrChannels.Name = "labelNrChannels";
            this.labelNrChannels.Size = new System.Drawing.Size(25, 19);
            this.labelNrChannels.TabIndex = 2;
            this.labelNrChannels.Text = "3";
            // 
            // label15
            // 
            this.label15.Dock = System.Windows.Forms.DockStyle.Left;
            this.label15.Location = new System.Drawing.Point(0, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(94, 19);
            this.label15.TabIndex = 1;
            this.label15.Text = "Channels:";
            // 
            // panel63
            // 
            this.panel63.Controls.Add(this.labelRecNr);
            this.panel63.Controls.Add(this.label6);
            this.panel63.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel63.Location = new System.Drawing.Point(0, 0);
            this.panel63.Name = "panel63";
            this.panel63.Size = new System.Drawing.Size(147, 25);
            this.panel63.TabIndex = 2;
            // 
            // labelRecNr
            // 
            this.labelRecNr.AutoSize = true;
            this.labelRecNr.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelRecNr.Location = new System.Drawing.Point(80, 0);
            this.labelRecNr.Name = "labelRecNr";
            this.labelRecNr.Size = new System.Drawing.Size(14, 18);
            this.labelRecNr.TabIndex = 1;
            this.labelRecNr.Text = ".";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Left;
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 18);
            this.label6.TabIndex = 0;
            this.label6.Text = "Event #:";
            // 
            // comboBoxTriageChannel
            // 
            this.comboBoxTriageChannel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.comboBoxTriageChannel.FormattingEnabled = true;
            this.comboBoxTriageChannel.Items.AddRange(new object[] {
            "1. Lead I",
            "2. Lead II",
            "3. Lead III"});
            this.comboBoxTriageChannel.Location = new System.Drawing.Point(0, 73);
            this.comboBoxTriageChannel.Name = "comboBoxTriageChannel";
            this.comboBoxTriageChannel.Size = new System.Drawing.Size(147, 26);
            this.comboBoxTriageChannel.TabIndex = 1;
            this.comboBoxTriageChannel.Text = "1. Lead I";
            this.comboBoxTriageChannel.SelectedIndexChanged += new System.EventHandler(this.comboBoxChannel_SelectedIndexChanged);
            // 
            // panel66
            // 
            this.panel66.Controls.Add(this.buttonAnalyzeAdd);
            this.panel66.Controls.Add(this.Anayze);
            this.panel66.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel66.Location = new System.Drawing.Point(1487, 0);
            this.panel66.Name = "panel66";
            this.panel66.Size = new System.Drawing.Size(89, 101);
            this.panel66.TabIndex = 14;
            // 
            // buttonAnalyzeAdd
            // 
            this.buttonAnalyzeAdd.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonAnalyzeAdd.Image = ((System.Drawing.Image)(resources.GetObject("buttonAnalyzeAdd.Image")));
            this.buttonAnalyzeAdd.Location = new System.Drawing.Point(0, 61);
            this.buttonAnalyzeAdd.Name = "buttonAnalyzeAdd";
            this.buttonAnalyzeAdd.Size = new System.Drawing.Size(89, 40);
            this.buttonAnalyzeAdd.TabIndex = 2;
            this.buttonAnalyzeAdd.Text = "0";
            this.buttonAnalyzeAdd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAnalyzeAdd.UseVisualStyleBackColor = true;
            // 
            // Anayze
            // 
            this.Anayze.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Anayze.Dock = System.Windows.Forms.DockStyle.Top;
            this.Anayze.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Anayze.Image = ((System.Drawing.Image)(resources.GetObject("Anayze.Image")));
            this.Anayze.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Anayze.Location = new System.Drawing.Point(0, 0);
            this.Anayze.Name = "Anayze";
            this.Anayze.Size = new System.Drawing.Size(89, 45);
            this.Anayze.TabIndex = 1;
            this.Anayze.Text = "STAT";
            this.Anayze.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Anayze.UseVisualStyleBackColor = true;
            this.Anayze.Click += new System.EventHandler(this.Anayze_Click_1);
            // 
            // panel24
            // 
            this.panel24.Controls.Add(this.panel65);
            this.panel24.Controls.Add(this.panel64);
            this.panel24.Controls.Add(this.panel61);
            this.panel24.Controls.Add(this.panel47);
            this.panel24.Controls.Add(this.panel45);
            this.panel24.Controls.Add(this.panel39);
            this.panel24.Controls.Add(this.panel30);
            this.panel24.Controls.Add(this.panel28);
            this.panel24.Controls.Add(this.panel26);
            this.panel24.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel24.Location = new System.Drawing.Point(0, 0);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(1576, 48);
            this.panel24.TabIndex = 4;
            // 
            // panel65
            // 
            this.panel65.Controls.Add(this.labelTriageUpdTime);
            this.panel65.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel65.Location = new System.Drawing.Point(1327, 0);
            this.panel65.Name = "panel65";
            this.panel65.Size = new System.Drawing.Size(54, 48);
            this.panel65.TabIndex = 10;
            // 
            // labelTriageUpdTime
            // 
            this.labelTriageUpdTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelTriageUpdTime.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTriageUpdTime.Location = new System.Drawing.Point(0, 0);
            this.labelTriageUpdTime.Name = "labelTriageUpdTime";
            this.labelTriageUpdTime.Size = new System.Drawing.Size(54, 48);
            this.labelTriageUpdTime.TabIndex = 2;
            this.labelTriageUpdTime.Text = "22:55";
            this.labelTriageUpdTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel64
            // 
            this.panel64.Controls.Add(this.labelUtcOffset);
            this.panel64.Controls.Add(this.labelLoadImages);
            this.panel64.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel64.Location = new System.Drawing.Point(1423, 0);
            this.panel64.Name = "panel64";
            this.panel64.Size = new System.Drawing.Size(122, 48);
            this.panel64.TabIndex = 9;
            // 
            // labelUtcOffset
            // 
            this.labelUtcOffset.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelUtcOffset.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUtcOffset.Location = new System.Drawing.Point(0, 0);
            this.labelUtcOffset.Name = "labelUtcOffset";
            this.labelUtcOffset.Size = new System.Drawing.Size(122, 18);
            this.labelUtcOffset.TabIndex = 2;
            this.labelUtcOffset.Text = "-";
            // 
            // labelLoadImages
            // 
            this.labelLoadImages.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelLoadImages.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLoadImages.Location = new System.Drawing.Point(0, 30);
            this.labelLoadImages.Name = "labelLoadImages";
            this.labelLoadImages.Size = new System.Drawing.Size(122, 18);
            this.labelLoadImages.TabIndex = 0;
            // 
            // panel61
            // 
            this.panel61.Controls.Add(this.labelDrive);
            this.panel61.Controls.Add(this.buttonTriageFields);
            this.panel61.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel61.Location = new System.Drawing.Point(1545, 0);
            this.panel61.Name = "panel61";
            this.panel61.Size = new System.Drawing.Size(31, 48);
            this.panel61.TabIndex = 8;
            // 
            // labelDrive
            // 
            this.labelDrive.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDrive.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDrive.Location = new System.Drawing.Point(0, 0);
            this.labelDrive.Name = "labelDrive";
            this.labelDrive.Size = new System.Drawing.Size(31, 14);
            this.labelDrive.TabIndex = 1;
            this.labelDrive.Text = "-";
            this.labelDrive.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonTriageFields
            // 
            this.buttonTriageFields.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonTriageFields.Location = new System.Drawing.Point(0, 26);
            this.buttonTriageFields.Name = "buttonTriageFields";
            this.buttonTriageFields.Size = new System.Drawing.Size(31, 22);
            this.buttonTriageFields.TabIndex = 0;
            this.buttonTriageFields.Text = "tf";
            this.buttonTriageFields.UseVisualStyleBackColor = true;
            this.buttonTriageFields.Click += new System.EventHandler(this.buttonTriageFields_Click);
            // 
            // panel47
            // 
            this.panel47.Controls.Add(this.panel48);
            this.panel47.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel47.Location = new System.Drawing.Point(1284, 0);
            this.panel47.Name = "panel47";
            this.panel47.Size = new System.Drawing.Size(43, 48);
            this.panel47.TabIndex = 7;
            // 
            // panel48
            // 
            this.panel48.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel48.BackgroundImage")));
            this.panel48.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel48.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel48.Location = new System.Drawing.Point(0, 0);
            this.panel48.Name = "panel48";
            this.panel48.Size = new System.Drawing.Size(43, 46);
            this.panel48.TabIndex = 0;
            this.panel48.Click += new System.EventHandler(this.panel48_Click);
            this.panel48.Paint += new System.Windows.Forms.PaintEventHandler(this.panel48_Paint);
            // 
            // panel45
            // 
            this.panel45.Controls.Add(this.listBoxEventMode);
            this.panel45.Controls.Add(this.label10);
            this.panel45.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel45.Location = new System.Drawing.Point(1145, 0);
            this.panel45.Name = "panel45";
            this.panel45.Size = new System.Drawing.Size(139, 48);
            this.panel45.TabIndex = 5;
            // 
            // listBoxEventMode
            // 
            this.listBoxEventMode.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.listBoxEventMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.listBoxEventMode.FormattingEnabled = true;
            this.listBoxEventMode.Items.AddRange(new object[] {
            "Triage",
            "Analyze",
            "Report",
            "--------",
            "List All",
            "List Received",
            "List Normal",
            "List Noise",
            "List Lead Off",
            "List Base Line",
            "List Triaged",
            "List Analyzed",
            "List Reported"});
            this.listBoxEventMode.Location = new System.Drawing.Point(0, 22);
            this.listBoxEventMode.MaxDropDownItems = 20;
            this.listBoxEventMode.Name = "listBoxEventMode";
            this.listBoxEventMode.Size = new System.Drawing.Size(139, 26);
            this.listBoxEventMode.TabIndex = 9;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Top;
            this.label10.Location = new System.Drawing.Point(0, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(103, 18);
            this.label10.TabIndex = 0;
            this.label10.Text = "View Mode:";
            // 
            // panel39
            // 
            this.panel39.Controls.Add(this.textBoxViewTime);
            this.panel39.Controls.Add(this.panel62);
            this.panel39.Controls.Add(this.listBoxViewUnit);
            this.panel39.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel39.Location = new System.Drawing.Point(1045, 0);
            this.panel39.Name = "panel39";
            this.panel39.Size = new System.Drawing.Size(100, 48);
            this.panel39.TabIndex = 4;
            // 
            // textBoxViewTime
            // 
            this.textBoxViewTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxViewTime.Location = new System.Drawing.Point(0, 0);
            this.textBoxViewTime.Name = "textBoxViewTime";
            this.textBoxViewTime.Size = new System.Drawing.Size(82, 27);
            this.textBoxViewTime.TabIndex = 4;
            this.textBoxViewTime.Text = "48";
            // 
            // panel62
            // 
            this.panel62.Controls.Add(this.panelViewDec);
            this.panel62.Controls.Add(this.panelViewInc);
            this.panel62.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel62.Location = new System.Drawing.Point(82, 0);
            this.panel62.Name = "panel62";
            this.panel62.Size = new System.Drawing.Size(18, 26);
            this.panel62.TabIndex = 2;
            // 
            // panelViewDec
            // 
            this.panelViewDec.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelViewDec.BackgroundImage")));
            this.panelViewDec.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panelViewDec.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelViewDec.Location = new System.Drawing.Point(0, 14);
            this.panelViewDec.Name = "panelViewDec";
            this.panelViewDec.Size = new System.Drawing.Size(18, 14);
            this.panelViewDec.TabIndex = 1;
            this.panelViewDec.Click += new System.EventHandler(this.panelViewDec_Click);
            // 
            // panelViewInc
            // 
            this.panelViewInc.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelViewInc.BackgroundImage")));
            this.panelViewInc.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panelViewInc.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelViewInc.Location = new System.Drawing.Point(0, 0);
            this.panelViewInc.Name = "panelViewInc";
            this.panelViewInc.Size = new System.Drawing.Size(18, 14);
            this.panelViewInc.TabIndex = 0;
            this.panelViewInc.Click += new System.EventHandler(this.panelViewInc_Click);
            this.panelViewInc.Paint += new System.Windows.Forms.PaintEventHandler(this.panel63_Paint);
            // 
            // listBoxViewUnit
            // 
            this.listBoxViewUnit.AllowDrop = true;
            this.listBoxViewUnit.CausesValidation = false;
            this.listBoxViewUnit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.listBoxViewUnit.ItemHeight = 18;
            this.listBoxViewUnit.Items.AddRange(new object[] {
            "Hours",
            "Days"});
            this.listBoxViewUnit.Location = new System.Drawing.Point(0, 26);
            this.listBoxViewUnit.Name = "listBoxViewUnit";
            this.listBoxViewUnit.ScrollAlwaysVisible = true;
            this.listBoxViewUnit.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.listBoxViewUnit.Size = new System.Drawing.Size(100, 22);
            this.listBoxViewUnit.TabIndex = 1;
            this.listBoxViewUnit.MouseClick += new System.Windows.Forms.MouseEventHandler(this.listBoxViewUnit_MouseClick);
            // 
            // panel30
            // 
            this.panel30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel30.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel30.Location = new System.Drawing.Point(1032, 0);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(13, 48);
            this.panel30.TabIndex = 3;
            // 
            // panel28
            // 
            this.panel28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel28.Controls.Add(this.panelView);
            this.panel28.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel28.Location = new System.Drawing.Point(235, 0);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(797, 48);
            this.panel28.TabIndex = 2;
            // 
            // panelView
            // 
            this.panelView.Controls.Add(this.radioButtonCurrentPatient);
            this.panelView.Controls.Add(this.radioButton4);
            this.panelView.Controls.Add(this.radioButton3);
            this.panelView.Controls.Add(this.radioButton2);
            this.panelView.Controls.Add(this.radioButton1);
            this.panelView.Controls.Add(this.label7);
            this.panelView.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelView.Location = new System.Drawing.Point(0, 3);
            this.panelView.Name = "panelView";
            this.panelView.Size = new System.Drawing.Size(795, 43);
            this.panelView.TabIndex = 1;
            // 
            // radioButtonCurrentPatient
            // 
            this.radioButtonCurrentPatient.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioButtonCurrentPatient.Location = new System.Drawing.Point(604, 0);
            this.radioButtonCurrentPatient.Name = "radioButtonCurrentPatient";
            this.radioButtonCurrentPatient.Size = new System.Drawing.Size(190, 43);
            this.radioButtonCurrentPatient.TabIndex = 4;
            this.radioButtonCurrentPatient.TabStop = true;
            this.radioButtonCurrentPatient.Text = "Current Patient";
            this.radioButtonCurrentPatient.UseVisualStyleBackColor = true;
            this.radioButtonCurrentPatient.CheckedChanged += new System.EventHandler(this.radioButtonCurrentPatient_CheckedChanged);
            // 
            // radioButton4
            // 
            this.radioButton4.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioButton4.Enabled = false;
            this.radioButton4.Location = new System.Drawing.Point(470, 0);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(134, 43);
            this.radioButton4.TabIndex = 3;
            this.radioButton4.Text = "High (---)";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioButton3.Enabled = false;
            this.radioButton3.Location = new System.Drawing.Point(319, 0);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(151, 43);
            this.radioButton3.TabIndex = 2;
            this.radioButton3.Text = "Medium (---)";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioButton2.Enabled = false;
            this.radioButton2.Location = new System.Drawing.Point(185, 0);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(134, 43);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.Text = "Low (---)";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.Checked = true;
            this.radioButton1.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioButton1.Location = new System.Drawing.Point(66, 0);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(119, 43);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "All (---)";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // label7
            // 
            this.label7.Dock = System.Windows.Forms.DockStyle.Left;
            this.label7.Location = new System.Drawing.Point(0, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(66, 43);
            this.label7.TabIndex = 6;
            this.label7.Text = "View:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel26
            // 
            this.panel26.Controls.Add(this.panel27);
            this.panel26.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel26.Location = new System.Drawing.Point(0, 0);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(235, 48);
            this.panel26.TabIndex = 1;
            // 
            // panel27
            // 
            this.panel27.Controls.Add(this.button7);
            this.panel27.Controls.Add(this.panel59);
            this.panel27.Controls.Add(this.button2);
            this.panel27.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel27.Location = new System.Drawing.Point(0, 5);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(235, 43);
            this.panel27.TabIndex = 0;
            // 
            // button7
            // 
            this.button7.Dock = System.Windows.Forms.DockStyle.Left;
            this.button7.Font = new System.Drawing.Font("Webdings", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.button7.Location = new System.Drawing.Point(171, 0);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(34, 43);
            this.button7.TabIndex = 7;
            this.button7.Text = ":";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // panel59
            // 
            this.panel59.Controls.Add(this.panel31);
            this.panel59.Controls.Add(this.panel60);
            this.panel59.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel59.Location = new System.Drawing.Point(34, 0);
            this.panel59.Name = "panel59";
            this.panel59.Size = new System.Drawing.Size(137, 43);
            this.panel59.TabIndex = 12;
            // 
            // panel31
            // 
            this.panel31.Controls.Add(this.labelTriageRowCount);
            this.panel31.Controls.Add(this.label5);
            this.panel31.Controls.Add(this.labelTriageRow);
            this.panel31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel31.Location = new System.Drawing.Point(0, 2);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(137, 41);
            this.panel31.TabIndex = 12;
            // 
            // labelTriageRowCount
            // 
            this.labelTriageRowCount.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelTriageRowCount.Location = new System.Drawing.Point(76, 0);
            this.labelTriageRowCount.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.labelTriageRowCount.Name = "labelTriageRowCount";
            this.labelTriageRowCount.Size = new System.Drawing.Size(61, 41);
            this.labelTriageRowCount.TabIndex = 11;
            this.labelTriageRowCount.Text = " 0";
            this.labelTriageRowCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Left;
            this.label5.Location = new System.Drawing.Point(61, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 41);
            this.label5.TabIndex = 10;
            this.label5.Text = " / ";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelTriageRow
            // 
            this.labelTriageRow.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelTriageRow.Location = new System.Drawing.Point(0, 0);
            this.labelTriageRow.Name = "labelTriageRow";
            this.labelTriageRow.Size = new System.Drawing.Size(61, 41);
            this.labelTriageRow.TabIndex = 12;
            this.labelTriageRow.Text = "8";
            this.labelTriageRow.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel60
            // 
            this.panel60.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel60.Location = new System.Drawing.Point(0, 0);
            this.panel60.Margin = new System.Windows.Forms.Padding(0);
            this.panel60.Name = "panel60";
            this.panel60.Size = new System.Drawing.Size(137, 2);
            this.panel60.TabIndex = 0;
            // 
            // button2
            // 
            this.button2.Dock = System.Windows.Forms.DockStyle.Left;
            this.button2.Font = new System.Drawing.Font("Webdings", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.button2.Location = new System.Drawing.Point(0, 0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(34, 43);
            this.button2.TabIndex = 2;
            this.button2.Text = "9";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // panelTriageStrip0
            // 
            this.panelTriageStrip0.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelTriageStrip0.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelTriageStrip0.Location = new System.Drawing.Point(0, 545);
            this.panelTriageStrip0.Name = "panelTriageStrip0";
            this.panelTriageStrip0.Size = new System.Drawing.Size(1576, 92);
            this.panelTriageStrip0.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(962, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "Triage";
            // 
            // tabPageAnalyze
            // 
            this.tabPageAnalyze.BackColor = System.Drawing.Color.Gainsboro;
            this.tabPageAnalyze.Controls.Add(this.label3);
            this.tabPageAnalyze.Location = new System.Drawing.Point(4, 27);
            this.tabPageAnalyze.Name = "tabPageAnalyze";
            this.tabPageAnalyze.Size = new System.Drawing.Size(1576, 637);
            this.tabPageAnalyze.TabIndex = 3;
            this.tabPageAnalyze.Text = "Analyze";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(517, 141);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 18);
            this.label3.TabIndex = 1;
            this.label3.Text = "Analyze";
            // 
            // tabPageReport
            // 
            this.tabPageReport.BackColor = System.Drawing.Color.Gainsboro;
            this.tabPageReport.Controls.Add(this.label4);
            this.tabPageReport.Location = new System.Drawing.Point(4, 27);
            this.tabPageReport.Name = "tabPageReport";
            this.tabPageReport.Size = new System.Drawing.Size(1576, 637);
            this.tabPageReport.TabIndex = 4;
            this.tabPageReport.Text = "Report";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(517, 141);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 18);
            this.label4.TabIndex = 1;
            this.label4.Text = "Report";
            // 
            // tabPageStudy
            // 
            this.tabPageStudy.BackColor = System.Drawing.Color.Gainsboro;
            this.tabPageStudy.Controls.Add(this.label1);
            this.tabPageStudy.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPageStudy.Location = new System.Drawing.Point(4, 27);
            this.tabPageStudy.Name = "tabPageStudy";
            this.tabPageStudy.Size = new System.Drawing.Size(1576, 637);
            this.tabPageStudy.TabIndex = 5;
            this.tabPageStudy.Text = "Study";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(871, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Procedure";
            // 
            // timerTriage
            // 
            this.timerTriage.Interval = 1000;
            this.timerTriage.Tick += new System.EventHandler(this.timerTriage_Tick);
            // 
            // contextMenuState
            // 
            this.contextMenuState.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.toolStripMenuItem3,
            this.toolStripMenuItem4,
            this.toolStripMenuItem5,
            this.toolStripMenuItem6,
            this.toolStripMenuItem7,
            this.toolStripMenuItem8,
            this.toolStripMenuItem9});
            this.contextMenuState.Name = "contextMenuState";
            this.contextMenuState.Size = new System.Drawing.Size(123, 180);
            this.contextMenuState.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuState_Opening);
            this.contextMenuState.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenuState_ItemClicked);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(122, 22);
            this.toolStripMenuItem2.Text = "Received";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(122, 22);
            this.toolStripMenuItem3.Text = "Normal";
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(122, 22);
            this.toolStripMenuItem4.Text = "Noise";
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(122, 22);
            this.toolStripMenuItem5.Text = "LeadOff";
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(122, 22);
            this.toolStripMenuItem6.Text = "Triaged";
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(122, 22);
            this.toolStripMenuItem7.Text = "Analyzed";
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(122, 22);
            this.toolStripMenuItem8.Text = "BaseLine";
            // 
            // toolStripMenuItem9
            // 
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            this.toolStripMenuItem9.Size = new System.Drawing.Size(122, 22);
            this.toolStripMenuItem9.Text = "Reported";
            // 
            // FormEventBoard
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.MidnightBlue;
            this.ClientSize = new System.Drawing.Size(1584, 821);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel16);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.toolStripTop);
            this.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FormEventBoard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DVTMS  EventBoard - 2 v10.0.1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Shown += new System.EventHandler(this.FormEventBoard_Shown);
            this.SizeChanged += new System.EventHandler(this.FormEventBoard_SizeChanged);
            this.toolStripTop.ResumeLayout(false);
            this.toolStripTop.PerformLayout();
            this.contextMenuSettings.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel46.ResumeLayout(false);
            this.panel38.ResumeLayout(false);
            this.panel38.PerformLayout();
            this.panel44.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel21.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.panel43.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel19.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPageTriage.ResumeLayout(false);
            this.tabPageTriage.PerformLayout();
            this.panel23.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridTriage)).EndInit();
            this.panel25.ResumeLayout(false);
            this.panel32.ResumeLayout(false);
            this.panel29.ResumeLayout(false);
            this.panel29.PerformLayout();
            this.panel36.ResumeLayout(false);
            this.panel58.ResumeLayout(false);
            this.panel55.ResumeLayout(false);
            this.panelPriority.ResumeLayout(false);
            this.panel50.ResumeLayout(false);
            this.panel51.ResumeLayout(false);
            this.panel37.ResumeLayout(false);
            this.panel49.ResumeLayout(false);
            this.panel34.ResumeLayout(false);
            this.panel34.PerformLayout();
            this.panel35.ResumeLayout(false);
            this.panel33.ResumeLayout(false);
            this.panel57.ResumeLayout(false);
            this.panel41.ResumeLayout(false);
            this.panel56.ResumeLayout(false);
            this.panel22.ResumeLayout(false);
            this.panel22.PerformLayout();
            this.panel42.ResumeLayout(false);
            this.panel63.ResumeLayout(false);
            this.panel63.PerformLayout();
            this.panel66.ResumeLayout(false);
            this.panel24.ResumeLayout(false);
            this.panel65.ResumeLayout(false);
            this.panel64.ResumeLayout(false);
            this.panel61.ResumeLayout(false);
            this.panel47.ResumeLayout(false);
            this.panel45.ResumeLayout(false);
            this.panel45.PerformLayout();
            this.panel39.ResumeLayout(false);
            this.panel39.PerformLayout();
            this.panel62.ResumeLayout(false);
            this.panel28.ResumeLayout(false);
            this.panelView.ResumeLayout(false);
            this.panel26.ResumeLayout(false);
            this.panel27.ResumeLayout(false);
            this.panel59.ResumeLayout(false);
            this.panel31.ResumeLayout(false);
            this.tabPageAnalyze.ResumeLayout(false);
            this.tabPageAnalyze.PerformLayout();
            this.tabPageReport.ResumeLayout(false);
            this.tabPageReport.PerformLayout();
            this.tabPageStudy.ResumeLayout(false);
            this.tabPageStudy.PerformLayout();
            this.contextMenuState.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStripTop;
        private System.Windows.Forms.ToolStripButton toolStripWorkNew;
        private System.Windows.Forms.ToolStripButton toolStripNewEvent;
        private System.Windows.Forms.ToolStripButton toolStripLoad;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripWorld;
        private System.Windows.Forms.ToolStripButton toolStripCustomers;
        private System.Windows.Forms.ToolStripButton toolStripUsers;
        private System.Windows.Forms.ToolStripButton toolStripDepartments;
        private System.Windows.Forms.ToolStripButton toolStripDevices;
        private System.Windows.Forms.ToolStripButton toolStripEvents;
        private System.Windows.Forms.ToolStripButton toolStripAlarmSignal;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripExit;
        private System.Windows.Forms.ToolStripLabel toolStripUserLabel;
        private System.Windows.Forms.ToolStripButton toolStripUserCancel;
        private System.Windows.Forms.ToolStripComboBox toolStripSelectUser;
        private System.Windows.Forms.ToolStripButton toolStripUserLock;
        private System.Windows.Forms.ToolStripButton toolStripSettings;
        private System.Windows.Forms.ContextMenuStrip contextMenuSettings;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem userSettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem globalSettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStripButton toolStripUserOk;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStripButton toolStripPatient;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label labelTime;
        private System.Windows.Forms.Label labelDate;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageTriage;
        private System.Windows.Forms.TabPage tabPageAnalyze;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Button buttonRefresh;
        private System.Windows.Forms.TabPage tabPageReport;
        private System.Windows.Forms.TabPage tabPageStudy;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Label label2UserMsg;
        private System.Windows.Forms.Label ValueUserMsg;
        private System.Windows.Forms.Label label1UserMsg;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label label2EventReport;
        private System.Windows.Forms.Label valueEventReport;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label label2EventAnalyze;
        private System.Windows.Forms.Label valueEventAnalyze;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label2EventTriage;
        private System.Windows.Forms.Label valueEventTriage;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label2EventTotalOpen;
        private System.Windows.Forms.Label valueEventTotalOpen;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label label2EventDone24;
        private System.Windows.Forms.Label valueEventDone24;
        private System.Windows.Forms.Label label1EventDone24;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label2ReportToDo;
        private System.Windows.Forms.Label ValueReportToDo24;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label2ProcedureOpen;
        private System.Windows.Forms.Label valueStudyOpen;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label2ProcedureDone24;
        private System.Windows.Forms.Label valueStudyDone24;
        private System.Windows.Forms.Label label1ProcedureDone24;
        private System.Windows.Forms.Panel panelTriageStrip0;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.Panel panelPriority;
        private System.Windows.Forms.Panel panel37;
        private System.Windows.Forms.Panel panel34;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.Panel panelView;
        private System.Windows.Forms.RadioButton radioButtonCurrentPatient;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Panel panel40;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel41;
        private System.Windows.Forms.ComboBox comboBoxTriageChannel;
        private System.Windows.Forms.Panel panel42;
        private System.Windows.Forms.Label labelNrChannels;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ToolStripMenuItem showDashBoardToolStripMenuItem;
        private System.Windows.Forms.Panel panel43;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.DataGridView dataGridTriage;
        private System.Windows.Forms.Panel panel39;
        private System.Windows.Forms.Panel panel38;
        private System.Windows.Forms.Label labelStudyIndex;
        private System.Windows.Forms.Label labelRecIndex;
        private System.Windows.Forms.Panel panel44;
        private System.Windows.Forms.Panel panel45;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel46;
        private System.Windows.Forms.Label labelViewMode1;
        private System.Windows.Forms.Panel panel47;
        private System.Windows.Forms.Panel panel48;
        private System.Windows.Forms.Label labelUpdateTC;
        private System.Windows.Forms.Label labelUpdateTS;
        private System.Windows.Forms.Timer timerTriage;
        private System.Windows.Forms.ComboBox listBoxEventMode;
        private System.Windows.Forms.Panel panel49;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel51;
        private System.Windows.Forms.Panel panel52;
        private System.Windows.Forms.Panel panel50;
        private System.Windows.Forms.Panel panel53;
        private System.Windows.Forms.Panel panel55;
        private System.Windows.Forms.Button buttonEventFolder;
        private System.Windows.Forms.Button buttonEventView;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Panel panel58;
        private System.Windows.Forms.Panel panel35;
        private System.Windows.Forms.Label labelTriageStudy;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label labelTriagePatient;
        private System.Windows.Forms.Panel panel57;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelModelNr;
        private System.Windows.Forms.Button buttonLow;
        private System.Windows.Forms.Button buttonNormal2;
        private System.Windows.Forms.Button buttonHigh;
        private System.Windows.Forms.Panel panel59;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.Label labelTriageRowCount;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel60;
        private System.Windows.Forms.Panel panel61;
        private System.Windows.Forms.Button buttonTriageFields;
        private System.Windows.Forms.TextBox textBoxViewTime;
        private System.Windows.Forms.Panel panel62;
        private System.Windows.Forms.Panel panelViewDec;
        private System.Windows.Forms.Panel panelViewInc;
        private System.Windows.Forms.Panel panel63;
        private System.Windows.Forms.Label labelRecNr;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel64;
        private System.Windows.Forms.Label labelLoadImages;
        private System.Windows.Forms.Panel panel65;
        private System.Windows.Forms.Label labelTriageUpdTime;
        private System.Windows.Forms.Label labelUtcOffset;
        private System.Windows.Forms.Label labelDrive;
        private System.Windows.Forms.Panel panel66;
        private System.Windows.Forms.Button buttonAnalyzeAdd;
        private System.Windows.Forms.Button Anayze;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ListBox listBoxViewUnit;
        private System.Windows.Forms.Label labelTriageRow;
        private System.Windows.Forms.Label labelPatientID;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.Panel panel36;
        private System.Windows.Forms.Label labelFindingCodes;
        private System.Windows.Forms.ToolStripButton toolStripHelpdesk;
        private System.Windows.Forms.Button buttonFindings;
        private System.Windows.Forms.TextBox textBoxStudyRemark;
        private System.Windows.Forms.Label labelStudyProcCodes;
        private System.Windows.Forms.TextBox textBoxFindings;
        private System.Windows.Forms.ContextMenuStrip contextMenuState;
        private System.Windows.Forms.Panel panel56;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem9;
        private System.Windows.Forms.ToolStripButton toolStripPatientList;
        private System.Windows.Forms.ToolStripButton toolStripAddDevice;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.Button buttonSetState;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.CheckBox checkBoxInvert;
        private System.Windows.Forms.DataGridViewTextBoxColumn Count;
        private System.Windows.Forms.DataGridViewTextBoxColumn Patient;
        private System.Windows.Forms.DataGridViewTextBoxColumn Event;
        private System.Windows.Forms.DataGridViewTextBoxColumn Read;
        private System.Windows.Forms.DataGridViewTextBoxColumn Remark;
        private System.Windows.Forms.DataGridViewImageColumn EventStrip;
        private System.Windows.Forms.DataGridViewImageColumn Triage;
        private System.Windows.Forms.DataGridViewTextBoxColumn Index;
        private System.Windows.Forms.DataGridViewTextBoxColumn State;
        private System.Windows.Forms.DataGridViewTextBoxColumn FilePath;
        private System.Windows.Forms.DataGridViewTextBoxColumn FileName;
        private System.Windows.Forms.DataGridViewTextBoxColumn NrSignals;
        private System.Windows.Forms.DataGridViewTextBoxColumn Study;
        private System.Windows.Forms.DataGridViewTextBoxColumn Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn ModelSnr;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumTrailer;
    }
}

