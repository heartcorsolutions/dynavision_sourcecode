﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Program_Base;
using System.IO;

namespace i_Reader
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            CProgram.sSetProgLogConsole(DDebugFlags.FullDebug); // AllOff);
            CProgram.sSetProgLogFile(DDebugFlags.FullDebug); // StandardLog);  // enables file logging
            CProgram.sSetProgLogScreen(DDebugFlags.FullDebug & (~DDebugFlags.Class)); // StandardLog);
            CProgram.sSetPromptFlags(DDebugFlags.FullDebug); // StandardPrompt);

            StreamWriter lockStream = null;
            ushort forceRunNr = Properties.Settings.Default.ForceProgRunNumber;
            if (CProgram.sbStartProgram("i-Reader", 11, 1, "Techmedic", "TMI1", 0x36C281A5, (byte)forceRunNr))
            {
                string header = "DVTMS";

                try
                {
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);

                    CProgram.sSetProgTitleFormat(header, true, DProgramVersionLevel.Beta, DShowOrganisation.First, ""); // set version info in title α= alpha(normal), β= beta, v= version

                    if (false == CProgram.sbLockFilePath(CProgram.sGetProgDir(), CProgram.sGetProgName(), ref lockStream))
                    {
                        CProgram.sPromptError(true, "Program single run lock failed", "Check for running program in task manager!");
                    }
                    else
                    {
                        bool bLicOk = CLicKeyDev.sbLicenseRequest(14, false, DLicensePC.SinglePC, null);

                        if (bLicOk)
                        {
                            string tail = "";
                            int daysLeft = CLicKeyDev.sGetDaysLeft();
                            //                        int daysLeft = 100;
                            if (daysLeft < 7)
                            {
                                tail = daysLeft.ToString() + " days left!";
                            }
                            CProgram.sSetProgTitleFormat(header, true, CProgram.sGetProgVersionLevel(), DShowOrganisation.First, tail); // set version info in title α= alpha, β= beta, v= version
                                                                                                                                        //CProgram.sSetProgramTitle("Example licensed program", true);
                            CProgram.sLogLine("Creating main window...");
                            FormIReader form = new FormIReader();
                            CProgram.sLogLine("Running program main window...");
                            Application.Run(form);
                        }
                        CProgram.sbUnLockFilePath(CProgram.sGetProgDir(), CProgram.sGetProgName(), ref lockStream);
                    }
                }
                catch ( Exception Ex )
                {
                    CProgram.sLogException("Program exit, Uncaught Exception", Ex);
                }
                CProgram.sEndProgram();
            }
        }
    }
}
