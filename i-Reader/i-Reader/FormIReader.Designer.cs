﻿namespace i_Reader
{
    partial class FormIReader
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormIReader));
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.dgIndex = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgFile = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NrSignals = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Strip = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgReadDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeviceID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgEventTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgEventType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PatientID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PatientBirthDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgConverter = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Method = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gdStoredAt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.logBox = new System.Windows.Forms.TextBox();
            this.labelProgressInfo = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.labelMemUsage = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.labelUpdTimer = new System.Windows.Forms.Label();
            this.labelSirQueue = new System.Windows.Forms.Label();
            this.labelTzHrFilter = new System.Windows.Forms.Label();
            this.labelTzQueue = new System.Windows.Forms.Label();
            this.labelListInfo = new System.Windows.Forms.Label();
            this.labelAmplRange = new System.Windows.Forms.Label();
            this.panelExtra = new System.Windows.Forms.Panel();
            this.panelMail = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.labelMailState2 = new System.Windows.Forms.Label();
            this.labelMailState1 = new System.Windows.Forms.Label();
            this.buttonKillSmtpCue = new System.Windows.Forms.Button();
            this.checkBoxMailEnable = new System.Windows.Forms.CheckBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.buttonSendMail = new System.Windows.Forms.Button();
            this.labelUtcTime = new System.Windows.Forms.Label();
            this.buttonLogToClipboard = new System.Windows.Forms.Button();
            this.pictureBoxFull = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.labelReadImgUtc = new System.Windows.Forms.Label();
            this.labelImageFull = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBoxEvent = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxSignal = new System.Windows.Forms.ComboBox();
            this.labelImageEvent = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.toolStripMenuExtra = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonFixEnd = new System.Windows.Forms.ToolStripButton();
            this.toolStripLast = new System.Windows.Forms.ToolStripButton();
            this.toolStripOpenFolder = new System.Windows.Forms.ToolStripButton();
            this.toolStripOpenEvent = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.LoadTableFromSql = new System.Windows.Forms.ToolStripButton();
            this.toolStripNrHours = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabelHours = new System.Windows.Forms.ToolStripLabel();
            this.toolStripDropDownSearch = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripEditSearch = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripEqualsCursor = new System.Windows.Forms.ToolStripButton();
            this.toolStripTest = new System.Windows.Forms.ToolStripButton();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.openZipFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.openHeaFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.openScpFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.openTZeFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.toolStripRun = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabelTotal = new System.Windows.Forms.ToolStripLabel();
            this.toolStripTotalRead = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabelError = new System.Windows.Forms.ToolStripLabel();
            this.toolStripErrorCount = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabelSkipped = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSkipped = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripProcessTotal = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabelDivide = new System.Windows.Forms.ToolStripLabel();
            this.toolStripProcessIndex = new System.Windows.Forms.ToolStripLabel();
            this.toolStripProcessLabel = new System.Windows.Forms.ToolStripLabel();
            this.toolStripStop = new System.Windows.Forms.ToolStripButton();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonBL = new System.Windows.Forms.ToolStripButton();
            this.toolStripAutoZip = new System.Windows.Forms.ToolStripButton();
            this.toolStripAutoHSir = new System.Windows.Forms.ToolStripButton();
            this.toolStripAddSirona = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonSirAC = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonSirHolter = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonPaSir = new System.Windows.Forms.ToolStripButton();
            this.toolStripAutoTZe = new System.Windows.Forms.ToolStripButton();
            this.toolStripAddTZ = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonTzRB = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonTzSkip = new System.Windows.Forms.ToolStripButton();
            this.toolStripTzAutoCollect = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonPaTZ = new System.Windows.Forms.ToolStripButton();
            this.toolStripAutoDV2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonPaDV2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripAutoDVX = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonSkipDVX = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonPaDVX = new System.Windows.Forms.ToolStripButton();
            this.toolStripAddDevice = new System.Windows.Forms.ToolStripButton();
            this.toolStripImport = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonWarn = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonHelp = new System.Windows.Forms.ToolStripButton();
            this.openDV2ScpFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.contextMenuHelp = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.openManualToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openChangeHistoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openLicenseInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openDownloadLinksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openContentFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tMIHelpdeskToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openStandardTeamviewerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.logActiveLicinseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sironaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tZToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dV2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dVXToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.manageUsersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rewriteUserListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.makeUserPinToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.createSQLEncryptedPasswordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createSmtpEncryptedPasswordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.programInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.programInfoToClipboardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.openLogFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openLogFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addLogLineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newLogFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.extraLoggingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enableLogFlushToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.openRecodingFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.checkDBCollectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testFilesDurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createTestFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.countTestFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readTestFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteTestFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recordingCreateTestFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recordingCountTestFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recordingTestFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recordingDeleteTestFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.folderCreateTestFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.folderCountTestFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.folderReadTestFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.folderDeleteTestFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testParseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.parseDoubleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.parseExactDateTimeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.progParseDoubleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.progParseDateTimeZoneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.makeLauncherVersionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.timerProcessMail = new System.Windows.Forms.Timer(this.components);
            this.contextMenuImport = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.importSironaZipToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importSironaHeaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
            this.importTZEventToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importTZScpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator16 = new System.Windows.Forms.ToolStripSeparator();
            this.importDV2ScpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importEdfToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator17 = new System.Windows.Forms.ToolStripSeparator();
            this.dVXTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reverseBytesDatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importDVXHeaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importDVX1DatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listDatInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importDVX1EvtToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importDVX1EvtRecToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.importNoninToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.convertDVXSpO2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importDVXSpO2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator18 = new System.Windows.Forms.ToolStripSeparator();
            this.dupRecordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.preAnalysisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cardioLogsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.heaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuAddDevice = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addSironaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listSironaLicenceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.addTZDeviceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listTZLicenseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.addDV2DeviceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listDV2LicenseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.addDVX1DeviceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listDVXLicenseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reloadDVXLicenseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reloadDeviceLicensesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createDVXDefaultsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.addEDFDeviceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialogDat = new System.Windows.Forms.OpenFileDialog();
            this.folderBrowserDialogTest = new System.Windows.Forms.FolderBrowserDialog();
            this.openFileDialogDVX = new System.Windows.Forms.OpenFileDialog();
            this.statusStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panelExtra.SuspendLayout();
            this.panelMail.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFull)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxEvent)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel7.SuspendLayout();
            this.toolStripMenuExtra.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.toolStrip.SuspendLayout();
            this.contextMenuHelp.SuspendLayout();
            this.contextMenuImport.SuspendLayout();
            this.contextMenuAddDevice.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip.Location = new System.Drawing.Point(0, 631);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(1344, 22);
            this.statusStrip.TabIndex = 1;
            this.statusStrip.Text = "....";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(14, 17);
            this.toolStripStatusLabel1.Text = "_";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 44);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dataGridView);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Panel2.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.splitContainer1.Size = new System.Drawing.Size(1344, 587);
            this.splitContainer1.SplitterDistance = 303;
            this.splitContainer1.TabIndex = 2;
            // 
            // dataGridView
            // 
            this.dataGridView.AllowDrop = true;
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgIndex,
            this.dgFile,
            this.NrSignals,
            this.Strip,
            this.dgReadDate,
            this.DeviceID,
            this.dgEventTime,
            this.dgEventType,
            this.dgState,
            this.PatientID,
            this.PatientBirthDate,
            this.dgConverter,
            this.Method,
            this.gdStoredAt});
            this.dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView.Location = new System.Drawing.Point(0, 0);
            this.dataGridView.MultiSelect = false;
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            this.dataGridView.ShowEditingIcon = false;
            this.dataGridView.Size = new System.Drawing.Size(1344, 303);
            this.dataGridView.TabIndex = 0;
            this.dataGridView.SelectionChanged += new System.EventHandler(this.dataGridView_SelectionChanged);
            this.dataGridView.Click += new System.EventHandler(this.dataGridView_Click);
            this.dataGridView.DoubleClick += new System.EventHandler(this.dataGridView_DoubleClick);
            // 
            // dgIndex
            // 
            this.dgIndex.HeaderText = "#";
            this.dgIndex.Name = "dgIndex";
            this.dgIndex.ReadOnly = true;
            this.dgIndex.Width = 39;
            // 
            // dgFile
            // 
            this.dgFile.HeaderText = "File";
            this.dgFile.Name = "dgFile";
            this.dgFile.ReadOnly = true;
            this.dgFile.Width = 48;
            // 
            // NrSignals
            // 
            this.NrSignals.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.NrSignals.HeaderText = "Nr Sign.";
            this.NrSignals.Name = "NrSignals";
            this.NrSignals.ReadOnly = true;
            this.NrSignals.Width = 40;
            // 
            // Strip
            // 
            this.Strip.HeaderText = "Strip";
            this.Strip.Name = "Strip";
            this.Strip.ReadOnly = true;
            this.Strip.Width = 53;
            // 
            // dgReadDate
            // 
            this.dgReadDate.HeaderText = "Received  Date Time";
            this.dgReadDate.Name = "dgReadDate";
            this.dgReadDate.ReadOnly = true;
            this.dgReadDate.Width = 101;
            // 
            // DeviceID
            // 
            this.DeviceID.HeaderText = "Device ID";
            this.DeviceID.Name = "DeviceID";
            this.DeviceID.ReadOnly = true;
            this.DeviceID.Width = 74;
            // 
            // dgEventTime
            // 
            this.dgEventTime.HeaderText = "Event dev. Time";
            this.dgEventTime.Name = "dgEventTime";
            this.dgEventTime.ReadOnly = true;
            this.dgEventTime.Width = 80;
            // 
            // dgEventType
            // 
            this.dgEventType.HeaderText = "Event Type";
            this.dgEventType.Name = "dgEventType";
            this.dgEventType.ReadOnly = true;
            this.dgEventType.Width = 80;
            // 
            // dgState
            // 
            this.dgState.HeaderText = "State";
            this.dgState.Name = "dgState";
            this.dgState.ReadOnly = true;
            this.dgState.Width = 57;
            // 
            // PatientID
            // 
            this.PatientID.HeaderText = "Study # Pat. ID";
            this.PatientID.Name = "PatientID";
            this.PatientID.ReadOnly = true;
            this.PatientID.Width = 87;
            // 
            // PatientBirthDate
            // 
            this.PatientBirthDate.HeaderText = "Patient Birth";
            this.PatientBirthDate.Name = "PatientBirthDate";
            this.PatientBirthDate.ReadOnly = true;
            this.PatientBirthDate.Visible = false;
            this.PatientBirthDate.Width = 82;
            // 
            // dgConverter
            // 
            this.dgConverter.HeaderText = "Converter";
            this.dgConverter.Name = "dgConverter";
            this.dgConverter.ReadOnly = true;
            this.dgConverter.Width = 78;
            // 
            // Method
            // 
            this.Method.HeaderText = "Import";
            this.Method.Name = "Method";
            this.Method.ReadOnly = true;
            this.Method.Width = 61;
            // 
            // gdStoredAt
            // 
            this.gdStoredAt.HeaderText = "Stored at";
            this.gdStoredAt.Name = "gdStoredAt";
            this.gdStoredAt.ReadOnly = true;
            this.gdStoredAt.Width = 69;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.logBox);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.labelProgressInfo);
            this.splitContainer2.Panel2.Controls.Add(this.panel3);
            this.splitContainer2.Panel2.Controls.Add(this.panel4);
            this.splitContainer2.Panel2.Controls.Add(this.panelExtra);
            this.splitContainer2.Panel2.Controls.Add(this.pictureBoxFull);
            this.splitContainer2.Panel2.Controls.Add(this.panel2);
            this.splitContainer2.Panel2.Controls.Add(this.pictureBoxEvent);
            this.splitContainer2.Panel2.Controls.Add(this.panel1);
            this.splitContainer2.Panel2.Controls.Add(this.panel7);
            this.splitContainer2.Size = new System.Drawing.Size(1344, 280);
            this.splitContainer2.SplitterDistance = 683;
            this.splitContainer2.TabIndex = 0;
            // 
            // logBox
            // 
            this.logBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.logBox.Location = new System.Drawing.Point(0, 0);
            this.logBox.Multiline = true;
            this.logBox.Name = "logBox";
            this.logBox.ReadOnly = true;
            this.logBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.logBox.Size = new System.Drawing.Size(683, 280);
            this.logBox.TabIndex = 1;
            // 
            // labelProgressInfo
            // 
            this.labelProgressInfo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelProgressInfo.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.errorProvider1.SetIconAlignment(this.labelProgressInfo, System.Windows.Forms.ErrorIconAlignment.MiddleLeft);
            this.labelProgressInfo.Location = new System.Drawing.Point(0, 205);
            this.labelProgressInfo.Name = "labelProgressInfo";
            this.labelProgressInfo.Size = new System.Drawing.Size(657, 17);
            this.labelProgressInfo.TabIndex = 10;
            this.labelProgressInfo.Text = "^20190909246060 Wait Scan file EndRun 123/2345 76A0010119_pid20190708sir_20190910" +
    "153533.hea\r\n ";
            this.labelProgressInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.labelMemUsage);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 222);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(657, 17);
            this.panel3.TabIndex = 7;
            // 
            // labelMemUsage
            // 
            this.labelMemUsage.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelMemUsage.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.errorProvider1.SetIconAlignment(this.labelMemUsage, System.Windows.Forms.ErrorIconAlignment.MiddleLeft);
            this.labelMemUsage.Location = new System.Drawing.Point(530, 0);
            this.labelMemUsage.Name = "labelMemUsage";
            this.labelMemUsage.Size = new System.Drawing.Size(127, 17);
            this.labelMemUsage.TabIndex = 8;
            this.labelMemUsage.Text = "^p1234 f 12345MB";
            this.labelMemUsage.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.labelUpdTimer);
            this.panel4.Controls.Add(this.labelSirQueue);
            this.panel4.Controls.Add(this.labelTzHrFilter);
            this.panel4.Controls.Add(this.labelTzQueue);
            this.panel4.Controls.Add(this.labelListInfo);
            this.panel4.Controls.Add(this.labelAmplRange);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(0, 239);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(657, 16);
            this.panel4.TabIndex = 6;
            // 
            // labelUpdTimer
            // 
            this.labelUpdTimer.AutoSize = true;
            this.labelUpdTimer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelUpdTimer.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelUpdTimer.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.errorProvider1.SetIconAlignment(this.labelUpdTimer, System.Windows.Forms.ErrorIconAlignment.MiddleLeft);
            this.labelUpdTimer.Location = new System.Drawing.Point(454, 0);
            this.labelUpdTimer.Name = "labelUpdTimer";
            this.labelUpdTimer.Size = new System.Drawing.Size(122, 15);
            this.labelUpdTimer.TabIndex = 10;
            this.labelUpdTimer.Text = "upd. timer  ^99 mS";
            this.labelUpdTimer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelUpdTimer.Click += new System.EventHandler(this.labelUpdTimer_Click);
            this.labelUpdTimer.DoubleClick += new System.EventHandler(this.labelUpdTimer_DoubleClick);
            // 
            // labelSirQueue
            // 
            this.labelSirQueue.AutoSize = true;
            this.labelSirQueue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelSirQueue.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSirQueue.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.errorProvider1.SetIconAlignment(this.labelSirQueue, System.Windows.Forms.ErrorIconAlignment.MiddleLeft);
            this.labelSirQueue.Location = new System.Drawing.Point(379, 0);
            this.labelSirQueue.Name = "labelSirQueue";
            this.labelSirQueue.Size = new System.Drawing.Size(75, 15);
            this.labelSirQueue.TabIndex = 14;
            this.labelSirQueue.Text = "^Sir Queue";
            this.labelSirQueue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelTzHrFilter
            // 
            this.labelTzHrFilter.AutoSize = true;
            this.labelTzHrFilter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelTzHrFilter.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelTzHrFilter.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.errorProvider1.SetIconAlignment(this.labelTzHrFilter, System.Windows.Forms.ErrorIconAlignment.MiddleLeft);
            this.labelTzHrFilter.Location = new System.Drawing.Point(297, 0);
            this.labelTzHrFilter.Name = "labelTzHrFilter";
            this.labelTzHrFilter.Size = new System.Drawing.Size(82, 15);
            this.labelTzHrFilter.TabIndex = 12;
            this.labelTzHrFilter.Text = "^Tz HR Filter";
            this.labelTzHrFilter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelTzHrFilter.Click += new System.EventHandler(this.labelTzHrFilter_Click);
            // 
            // labelTzQueue
            // 
            this.labelTzQueue.AutoSize = true;
            this.labelTzQueue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelTzQueue.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelTzQueue.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.errorProvider1.SetIconAlignment(this.labelTzQueue, System.Windows.Forms.ErrorIconAlignment.MiddleLeft);
            this.labelTzQueue.Location = new System.Drawing.Point(226, 0);
            this.labelTzQueue.Name = "labelTzQueue";
            this.labelTzQueue.Size = new System.Drawing.Size(71, 15);
            this.labelTzQueue.TabIndex = 11;
            this.labelTzQueue.Text = "^Tz Queue";
            this.labelTzQueue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelListInfo
            // 
            this.labelListInfo.AutoSize = true;
            this.labelListInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelListInfo.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelListInfo.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.errorProvider1.SetIconAlignment(this.labelListInfo, System.Windows.Forms.ErrorIconAlignment.MiddleLeft);
            this.labelListInfo.Location = new System.Drawing.Point(104, 0);
            this.labelListInfo.Name = "labelListInfo";
            this.labelListInfo.Size = new System.Drawing.Size(122, 15);
            this.labelListInfo.TabIndex = 9;
            this.labelListInfo.Text = "^9999 rows < 9999";
            this.labelListInfo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelListInfo.DoubleClick += new System.EventHandler(this.labelListInfo_DoubleClick);
            // 
            // labelAmplRange
            // 
            this.labelAmplRange.AutoSize = true;
            this.labelAmplRange.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelAmplRange.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelAmplRange.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.errorProvider1.SetIconAlignment(this.labelAmplRange, System.Windows.Forms.ErrorIconAlignment.MiddleLeft);
            this.labelAmplRange.Location = new System.Drawing.Point(0, 0);
            this.labelAmplRange.Name = "labelAmplRange";
            this.labelAmplRange.Size = new System.Drawing.Size(104, 15);
            this.labelAmplRange.TabIndex = 13;
            this.labelAmplRange.Text = "Ampl R= 8.0 mV";
            this.labelAmplRange.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelAmplRange.Click += new System.EventHandler(this.labelAmplRange_Click);
            // 
            // panelExtra
            // 
            this.panelExtra.Controls.Add(this.panelMail);
            this.panelExtra.Controls.Add(this.labelUtcTime);
            this.panelExtra.Controls.Add(this.buttonLogToClipboard);
            this.panelExtra.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelExtra.Location = new System.Drawing.Point(0, 255);
            this.panelExtra.Name = "panelExtra";
            this.panelExtra.Size = new System.Drawing.Size(657, 25);
            this.panelExtra.TabIndex = 5;
            // 
            // panelMail
            // 
            this.panelMail.Controls.Add(this.panel6);
            this.panelMail.Controls.Add(this.buttonKillSmtpCue);
            this.panelMail.Controls.Add(this.checkBoxMailEnable);
            this.panelMail.Controls.Add(this.panel5);
            this.panelMail.Controls.Add(this.buttonSendMail);
            this.panelMail.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelMail.Location = new System.Drawing.Point(26, 0);
            this.panelMail.Name = "panelMail";
            this.panelMail.Size = new System.Drawing.Size(235, 25);
            this.panelMail.TabIndex = 8;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.labelMailState2);
            this.panel6.Controls.Add(this.labelMailState1);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(89, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(111, 25);
            this.panel6.TabIndex = 17;
            // 
            // labelMailState2
            // 
            this.labelMailState2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelMailState2.Location = new System.Drawing.Point(0, 12);
            this.labelMailState2.Name = "labelMailState2";
            this.labelMailState2.Size = new System.Drawing.Size(111, 13);
            this.labelMailState2.TabIndex = 3;
            this.labelMailState2.Text = "Mail state2";
            this.labelMailState2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelMailState2.Click += new System.EventHandler(this.labelMailState2_Click);
            // 
            // labelMailState1
            // 
            this.labelMailState1.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelMailState1.Location = new System.Drawing.Point(0, 0);
            this.labelMailState1.Name = "labelMailState1";
            this.labelMailState1.Size = new System.Drawing.Size(111, 13);
            this.labelMailState1.TabIndex = 2;
            this.labelMailState1.Text = "Mail state";
            this.labelMailState1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelMailState1.Click += new System.EventHandler(this.labelMailState1_Click);
            // 
            // buttonKillSmtpCue
            // 
            this.buttonKillSmtpCue.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonKillSmtpCue.Location = new System.Drawing.Point(200, 0);
            this.buttonKillSmtpCue.Name = "buttonKillSmtpCue";
            this.buttonKillSmtpCue.Size = new System.Drawing.Size(35, 25);
            this.buttonKillSmtpCue.TabIndex = 16;
            this.buttonKillSmtpCue.Text = "Kill";
            this.buttonKillSmtpCue.UseVisualStyleBackColor = true;
            this.buttonKillSmtpCue.Click += new System.EventHandler(this.buttonKillSmtpCue_Click);
            // 
            // checkBoxMailEnable
            // 
            this.checkBoxMailEnable.AutoSize = true;
            this.checkBoxMailEnable.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxMailEnable.Location = new System.Drawing.Point(44, 0);
            this.checkBoxMailEnable.Margin = new System.Windows.Forms.Padding(1);
            this.checkBoxMailEnable.Name = "checkBoxMailEnable";
            this.checkBoxMailEnable.Size = new System.Drawing.Size(45, 25);
            this.checkBoxMailEnable.TabIndex = 14;
            this.checkBoxMailEnable.Text = "Mail";
            this.checkBoxMailEnable.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel5.Location = new System.Drawing.Point(34, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(10, 25);
            this.panel5.TabIndex = 15;
            // 
            // buttonSendMail
            // 
            this.buttonSendMail.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonSendMail.Font = new System.Drawing.Font("Webdings", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.buttonSendMail.Image = ((System.Drawing.Image)(resources.GetObject("buttonSendMail.Image")));
            this.buttonSendMail.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.buttonSendMail.Location = new System.Drawing.Point(0, 0);
            this.buttonSendMail.Name = "buttonSendMail";
            this.buttonSendMail.Size = new System.Drawing.Size(34, 25);
            this.buttonSendMail.TabIndex = 13;
            this.buttonSendMail.UseVisualStyleBackColor = true;
            this.buttonSendMail.Click += new System.EventHandler(this.buttonSendMail_Click);
            // 
            // labelUtcTime
            // 
            this.labelUtcTime.AutoSize = true;
            this.labelUtcTime.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelUtcTime.Location = new System.Drawing.Point(647, 0);
            this.labelUtcTime.Name = "labelUtcTime";
            this.labelUtcTime.Size = new System.Drawing.Size(10, 13);
            this.labelUtcTime.TabIndex = 7;
            this.labelUtcTime.Text = ".";
            this.labelUtcTime.DoubleClick += new System.EventHandler(this.labelUtcTime_DoubleClick);
            // 
            // buttonLogToClipboard
            // 
            this.buttonLogToClipboard.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonLogToClipboard.BackgroundImage")));
            this.buttonLogToClipboard.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonLogToClipboard.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.buttonLogToClipboard.Location = new System.Drawing.Point(0, 0);
            this.buttonLogToClipboard.Name = "buttonLogToClipboard";
            this.buttonLogToClipboard.Size = new System.Drawing.Size(26, 25);
            this.buttonLogToClipboard.TabIndex = 9;
            this.buttonLogToClipboard.UseVisualStyleBackColor = true;
            this.buttonLogToClipboard.Click += new System.EventHandler(this.buttonLogToClipboard_Click);
            // 
            // pictureBoxFull
            // 
            this.pictureBoxFull.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBoxFull.Location = new System.Drawing.Point(0, 139);
            this.pictureBoxFull.Name = "pictureBoxFull";
            this.pictureBoxFull.Size = new System.Drawing.Size(657, 60);
            this.pictureBoxFull.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxFull.TabIndex = 4;
            this.pictureBoxFull.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.labelReadImgUtc);
            this.panel2.Controls.Add(this.labelImageFull);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 119);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(657, 20);
            this.panel2.TabIndex = 3;
            // 
            // labelReadImgUtc
            // 
            this.labelReadImgUtc.AutoSize = true;
            this.labelReadImgUtc.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelReadImgUtc.Location = new System.Drawing.Point(647, 0);
            this.labelReadImgUtc.Name = "labelReadImgUtc";
            this.labelReadImgUtc.Size = new System.Drawing.Size(10, 13);
            this.labelReadImgUtc.TabIndex = 8;
            this.labelReadImgUtc.Text = ".";
            // 
            // labelImageFull
            // 
            this.labelImageFull.AutoSize = true;
            this.labelImageFull.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelImageFull.Location = new System.Drawing.Point(23, 0);
            this.labelImageFull.Name = "labelImageFull";
            this.labelImageFull.Size = new System.Drawing.Size(10, 13);
            this.labelImageFull.TabIndex = 1;
            this.labelImageFull.Text = ".";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Left;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "full:";
            // 
            // pictureBoxEvent
            // 
            this.pictureBoxEvent.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBoxEvent.Location = new System.Drawing.Point(0, 59);
            this.pictureBoxEvent.Name = "pictureBoxEvent";
            this.pictureBoxEvent.Size = new System.Drawing.Size(657, 60);
            this.pictureBoxEvent.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxEvent.TabIndex = 2;
            this.pictureBoxEvent.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.comboBoxSignal);
            this.panel1.Controls.Add(this.labelImageEvent);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 39);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(657, 20);
            this.panel1.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Right;
            this.label2.Location = new System.Drawing.Point(576, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Signal: ";
            // 
            // comboBoxSignal
            // 
            this.comboBoxSignal.Dock = System.Windows.Forms.DockStyle.Right;
            this.comboBoxSignal.FormattingEnabled = true;
            this.comboBoxSignal.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12"});
            this.comboBoxSignal.Location = new System.Drawing.Point(618, 0);
            this.comboBoxSignal.Name = "comboBoxSignal";
            this.comboBoxSignal.Size = new System.Drawing.Size(39, 21);
            this.comboBoxSignal.TabIndex = 2;
            this.comboBoxSignal.Text = "1";
            this.comboBoxSignal.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            this.comboBoxSignal.SelectionChangeCommitted += new System.EventHandler(this.comboBoxSignal_SelectionChangeCommitted);
            this.comboBoxSignal.TextUpdate += new System.EventHandler(this.comboBoxSignal_TextUpdate);
            this.comboBoxSignal.TextChanged += new System.EventHandler(this.comboBoxSignal_TextChanged);
            this.comboBoxSignal.Validated += new System.EventHandler(this.comboBoxSignal_Validated);
            // 
            // labelImageEvent
            // 
            this.labelImageEvent.AutoSize = true;
            this.labelImageEvent.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelImageEvent.Location = new System.Drawing.Point(37, 0);
            this.labelImageEvent.Name = "labelImageEvent";
            this.labelImageEvent.Size = new System.Drawing.Size(10, 13);
            this.labelImageEvent.TabIndex = 1;
            this.labelImageEvent.Text = ".";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "event:";
            // 
            // panel7
            // 
            this.panel7.AutoSize = true;
            this.panel7.Controls.Add(this.toolStripMenuExtra);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(657, 39);
            this.panel7.TabIndex = 11;
            // 
            // toolStripMenuExtra
            // 
            this.toolStripMenuExtra.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStripMenuExtra.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonFixEnd,
            this.toolStripLast,
            this.toolStripOpenFolder,
            this.toolStripOpenEvent,
            this.toolStripSeparator3,
            this.LoadTableFromSql,
            this.toolStripNrHours,
            this.toolStripLabelHours,
            this.toolStripDropDownSearch,
            this.toolStripEditSearch,
            this.toolStripEqualsCursor,
            this.toolStripTest});
            this.toolStripMenuExtra.Location = new System.Drawing.Point(0, 0);
            this.toolStripMenuExtra.Name = "toolStripMenuExtra";
            this.toolStripMenuExtra.Size = new System.Drawing.Size(657, 39);
            this.toolStripMenuExtra.TabIndex = 0;
            this.toolStripMenuExtra.Text = "toolStrip1";
            // 
            // toolStripButtonFixEnd
            // 
            this.toolStripButtonFixEnd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonFixEnd.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripButtonFixEnd.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonFixEnd.Image")));
            this.toolStripButtonFixEnd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonFixEnd.Name = "toolStripButtonFixEnd";
            this.toolStripButtonFixEnd.Size = new System.Drawing.Size(23, 36);
            this.toolStripButtonFixEnd.Text = ".";
            this.toolStripButtonFixEnd.ToolTipText = "Fix cursor to End";
            this.toolStripButtonFixEnd.Click += new System.EventHandler(this.toolStripButtonFixEnd_Click);
            // 
            // toolStripLast
            // 
            this.toolStripLast.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripLast.Image = ((System.Drawing.Image)(resources.GetObject("toolStripLast.Image")));
            this.toolStripLast.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripLast.Name = "toolStripLast";
            this.toolStripLast.Size = new System.Drawing.Size(36, 36);
            this.toolStripLast.Text = "Run";
            this.toolStripLast.ToolTipText = "View last row";
            this.toolStripLast.Click += new System.EventHandler(this.toolStripLast_Click);
            // 
            // toolStripOpenFolder
            // 
            this.toolStripOpenFolder.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripOpenFolder.Image = ((System.Drawing.Image)(resources.GetObject("toolStripOpenFolder.Image")));
            this.toolStripOpenFolder.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripOpenFolder.Name = "toolStripOpenFolder";
            this.toolStripOpenFolder.Size = new System.Drawing.Size(36, 36);
            this.toolStripOpenFolder.Text = "Open folder";
            this.toolStripOpenFolder.Click += new System.EventHandler(this.toolStripButtonOpenFolder_Click);
            // 
            // toolStripOpenEvent
            // 
            this.toolStripOpenEvent.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripOpenEvent.Image = ((System.Drawing.Image)(resources.GetObject("toolStripOpenEvent.Image")));
            this.toolStripOpenEvent.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripOpenEvent.Name = "toolStripOpenEvent";
            this.toolStripOpenEvent.Size = new System.Drawing.Size(36, 36);
            this.toolStripOpenEvent.Text = "toolStripButton2";
            this.toolStripOpenEvent.ToolTipText = "Open Event";
            this.toolStripOpenEvent.Click += new System.EventHandler(this.toolStripOpenEvent_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 39);
            // 
            // LoadTableFromSql
            // 
            this.LoadTableFromSql.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.LoadTableFromSql.Image = ((System.Drawing.Image)(resources.GetObject("LoadTableFromSql.Image")));
            this.LoadTableFromSql.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.LoadTableFromSql.Name = "LoadTableFromSql";
            this.LoadTableFromSql.Size = new System.Drawing.Size(36, 36);
            this.LoadTableFromSql.ToolTipText = "Load list from dBase";
            this.LoadTableFromSql.Click += new System.EventHandler(this.toolStripLoadBD_Click);
            // 
            // toolStripNrHours
            // 
            this.toolStripNrHours.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripNrHours.Name = "toolStripNrHours";
            this.toolStripNrHours.Size = new System.Drawing.Size(32, 39);
            this.toolStripNrHours.Text = "999";
            // 
            // toolStripLabelHours
            // 
            this.toolStripLabelHours.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabelHours.Name = "toolStripLabelHours";
            this.toolStripLabelHours.Size = new System.Drawing.Size(44, 36);
            this.toolStripLabelHours.Text = "hours";
            // 
            // toolStripDropDownSearch
            // 
            this.toolStripDropDownSearch.Items.AddRange(new object[] {
            "* (All)",
            "#",
            "File",
            "NrSignals",
            "Read Date Time",
            "Device ID",
            "Event Time",
            "Event Type",
            "State",
            "Patient ID",
            "Patient Birth",
            "Converter",
            "Import",
            "Stored at"});
            this.toolStripDropDownSearch.MergeAction = System.Windows.Forms.MergeAction.MatchOnly;
            this.toolStripDropDownSearch.Name = "toolStripDropDownSearch";
            this.toolStripDropDownSearch.Size = new System.Drawing.Size(100, 39);
            this.toolStripDropDownSearch.ToolTipText = "Select search criteria";
            this.toolStripDropDownSearch.Visible = false;
            // 
            // toolStripEditSearch
            // 
            this.toolStripEditSearch.Name = "toolStripEditSearch";
            this.toolStripEditSearch.Size = new System.Drawing.Size(80, 39);
            this.toolStripEditSearch.ToolTipText = "Compare Search text (date format 29-07-2016)";
            this.toolStripEditSearch.Visible = false;
            // 
            // toolStripEqualsCursor
            // 
            this.toolStripEqualsCursor.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripEqualsCursor.Image = ((System.Drawing.Image)(resources.GetObject("toolStripEqualsCursor.Image")));
            this.toolStripEqualsCursor.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripEqualsCursor.Name = "toolStripEqualsCursor";
            this.toolStripEqualsCursor.Size = new System.Drawing.Size(36, 36);
            this.toolStripEqualsCursor.Text = "=";
            this.toolStripEqualsCursor.ToolTipText = "= Cursor value";
            this.toolStripEqualsCursor.Visible = false;
            // 
            // toolStripTest
            // 
            this.toolStripTest.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripTest.Enabled = false;
            this.toolStripTest.Image = ((System.Drawing.Image)(resources.GetObject("toolStripTest.Image")));
            this.toolStripTest.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripTest.Name = "toolStripTest";
            this.toolStripTest.Size = new System.Drawing.Size(36, 36);
            this.toolStripTest.Text = "Test";
            this.toolStripTest.ToolTipText = "Test function";
            this.toolStripTest.Visible = false;
            // 
            // timer
            // 
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // openZipFileDialog
            // 
            this.openZipFileDialog.FileName = "openZipFileDialog";
            this.openZipFileDialog.Filter = "Intricon zip|*.Zip|All|*.*";
            this.openZipFileDialog.Multiselect = true;
            this.openZipFileDialog.Title = "Load Intricon Zip File";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // openHeaFileDialog
            // 
            this.openHeaFileDialog.FileName = "openZipFileDialog";
            this.openHeaFileDialog.Filter = "MIT HEA|*.hea|All|*.*";
            this.openHeaFileDialog.Multiselect = true;
            this.openHeaFileDialog.Title = "Import MIT HEA File";
            this.openHeaFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.openHeaFileDialog_FileOk);
            // 
            // openScpFileDialog
            // 
            this.openScpFileDialog.FileName = "openZipFileDialog";
            this.openScpFileDialog.Filter = "SCP|*.scp|All|*.*";
            this.openScpFileDialog.Multiselect = true;
            this.openScpFileDialog.Title = "Import SCP File";
            // 
            // openTZeFileDialog
            // 
            this.openTZeFileDialog.FileName = "openZipFileDialog";
            this.openTZeFileDialog.Filter = "TZe|*.tze|All|*.*";
            this.openTZeFileDialog.Multiselect = true;
            this.openTZeFileDialog.Title = "Import TZe File";
            // 
            // toolStripRun
            // 
            this.toolStripRun.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripRun.Image = ((System.Drawing.Image)(resources.GetObject("toolStripRun.Image")));
            this.toolStripRun.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripRun.Name = "toolStripRun";
            this.toolStripRun.Size = new System.Drawing.Size(36, 41);
            this.toolStripRun.Text = "Run";
            this.toolStripRun.Click += new System.EventHandler(this.toolStripRun_Click);
            // 
            // toolStripLabelTotal
            // 
            this.toolStripLabelTotal.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabelTotal.Name = "toolStripLabelTotal";
            this.toolStripLabelTotal.Size = new System.Drawing.Size(42, 41);
            this.toolStripLabelTotal.Text = "Total:";
            // 
            // toolStripTotalRead
            // 
            this.toolStripTotalRead.Name = "toolStripTotalRead";
            this.toolStripTotalRead.Size = new System.Drawing.Size(18, 41);
            this.toolStripTotalRead.Text = "0";
            // 
            // toolStripLabelError
            // 
            this.toolStripLabelError.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabelError.Name = "toolStripLabelError";
            this.toolStripLabelError.Size = new System.Drawing.Size(43, 41);
            this.toolStripLabelError.Text = "Error:";
            // 
            // toolStripErrorCount
            // 
            this.toolStripErrorCount.Name = "toolStripErrorCount";
            this.toolStripErrorCount.Size = new System.Drawing.Size(18, 41);
            this.toolStripErrorCount.Text = "0";
            // 
            // toolStripLabelSkipped
            // 
            this.toolStripLabelSkipped.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabelSkipped.Name = "toolStripLabelSkipped";
            this.toolStripLabelSkipped.Size = new System.Drawing.Size(62, 41);
            this.toolStripLabelSkipped.Text = "Skipped:";
            // 
            // toolStripSkipped
            // 
            this.toolStripSkipped.Name = "toolStripSkipped";
            this.toolStripSkipped.Size = new System.Drawing.Size(18, 41);
            this.toolStripSkipped.Text = "0";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 44);
            // 
            // toolStripProcessTotal
            // 
            this.toolStripProcessTotal.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripProcessTotal.Name = "toolStripProcessTotal";
            this.toolStripProcessTotal.Size = new System.Drawing.Size(18, 41);
            this.toolStripProcessTotal.Text = "0";
            // 
            // toolStripLabelDivide
            // 
            this.toolStripLabelDivide.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripLabelDivide.Name = "toolStripLabelDivide";
            this.toolStripLabelDivide.Size = new System.Drawing.Size(27, 41);
            this.toolStripLabelDivide.Text = " / ";
            // 
            // toolStripProcessIndex
            // 
            this.toolStripProcessIndex.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripProcessIndex.Name = "toolStripProcessIndex";
            this.toolStripProcessIndex.Size = new System.Drawing.Size(18, 41);
            this.toolStripProcessIndex.Text = "0";
            // 
            // toolStripProcessLabel
            // 
            this.toolStripProcessLabel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripProcessLabel.Name = "toolStripProcessLabel";
            this.toolStripProcessLabel.Size = new System.Drawing.Size(37, 41);
            this.toolStripProcessLabel.Text = "^x:";
            // 
            // toolStripStop
            // 
            this.toolStripStop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripStop.Image = ((System.Drawing.Image)(resources.GetObject("toolStripStop.Image")));
            this.toolStripStop.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripStop.Name = "toolStripStop";
            this.toolStripStop.Size = new System.Drawing.Size(36, 41);
            this.toolStripStop.Text = "Stop";
            this.toolStripStop.Click += new System.EventHandler(this.toolStripStop_Click);
            // 
            // toolStrip
            // 
            this.toolStrip.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStrip.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStop,
            this.toolStripButtonBL,
            this.toolStripAutoZip,
            this.toolStripAutoHSir,
            this.toolStripAddSirona,
            this.toolStripButtonSirAC,
            this.toolStripButtonSirHolter,
            this.toolStripButtonPaSir,
            this.toolStripAutoTZe,
            this.toolStripAddTZ,
            this.toolStripButtonTzRB,
            this.toolStripButtonTzSkip,
            this.toolStripTzAutoCollect,
            this.toolStripButtonPaTZ,
            this.toolStripAutoDV2,
            this.toolStripButtonPaDV2,
            this.toolStripAutoDVX,
            this.toolStripButtonSkipDVX,
            this.toolStripButtonPaDVX,
            this.toolStripRun,
            this.toolStripAddDevice,
            this.toolStripImport,
            this.toolStripLabelTotal,
            this.toolStripTotalRead,
            this.toolStripLabelError,
            this.toolStripErrorCount,
            this.toolStripLabelSkipped,
            this.toolStripSkipped,
            this.toolStripSeparator1,
            this.toolStripButtonWarn,
            this.toolStripButtonHelp,
            this.toolStripProcessTotal,
            this.toolStripLabelDivide,
            this.toolStripProcessIndex,
            this.toolStripProcessLabel});
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(1344, 44);
            this.toolStrip.TabIndex = 0;
            this.toolStrip.Text = "Sirona";
            // 
            // toolStripButtonBL
            // 
            this.toolStripButtonBL.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonBL.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonBL.Image")));
            this.toolStripButtonBL.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonBL.Name = "toolStripButtonBL";
            this.toolStripButtonBL.Size = new System.Drawing.Size(48, 41);
            this.toolStripButtonBL.Text = "0 BL";
            this.toolStripButtonBL.ToolTipText = "Device Temporary Black List";
            this.toolStripButtonBL.Click += new System.EventHandler(this.toolStripButtonBL_Click);
            // 
            // toolStripAutoZip
            // 
            this.toolStripAutoZip.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripAutoZip.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripAutoZip.Image = ((System.Drawing.Image)(resources.GetObject("toolStripAutoZip.Image")));
            this.toolStripAutoZip.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripAutoZip.Name = "toolStripAutoZip";
            this.toolStripAutoZip.Size = new System.Drawing.Size(49, 41);
            this.toolStripAutoZip.Text = "_Zip";
            this.toolStripAutoZip.ToolTipText = "Auto Read  Sirona Zip";
            this.toolStripAutoZip.Click += new System.EventHandler(this.toolStripAutoZip_Click);
            // 
            // toolStripAutoHSir
            // 
            this.toolStripAutoHSir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripAutoHSir.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripAutoHSir.Image = ((System.Drawing.Image)(resources.GetObject("toolStripAutoHSir.Image")));
            this.toolStripAutoHSir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripAutoHSir.Name = "toolStripAutoHSir";
            this.toolStripAutoHSir.Size = new System.Drawing.Size(59, 41);
            this.toolStripAutoHSir.Text = "_HSir";
            this.toolStripAutoHSir.ToolTipText = "Auto read Http Sirona";
            this.toolStripAutoHSir.Click += new System.EventHandler(this.toolStripAutoHSir_Click);
            // 
            // toolStripAddSirona
            // 
            this.toolStripAddSirona.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripAddSirona.Image = ((System.Drawing.Image)(resources.GetObject("toolStripAddSirona.Image")));
            this.toolStripAddSirona.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripAddSirona.Name = "toolStripAddSirona";
            this.toolStripAddSirona.Size = new System.Drawing.Size(36, 41);
            this.toolStripAddSirona.Text = "add Sirona device";
            this.toolStripAddSirona.ToolTipText = "add Sirona device";
            this.toolStripAddSirona.Visible = false;
            this.toolStripAddSirona.Click += new System.EventHandler(this.toolStripAddSirona_Click);
            // 
            // toolStripButtonSirAC
            // 
            this.toolStripButtonSirAC.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonSirAC.Font = new System.Drawing.Font("Verdana", 9F);
            this.toolStripButtonSirAC.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonSirAC.Image")));
            this.toolStripButtonSirAC.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonSirAC.Name = "toolStripButtonSirAC";
            this.toolStripButtonSirAC.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonSirAC.Size = new System.Drawing.Size(37, 41);
            this.toolStripButtonSirAC.Text = "+AC";
            this.toolStripButtonSirAC.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.toolStripButtonSirAC.ToolTipText = "Sirona Auto Collect";
            this.toolStripButtonSirAC.Click += new System.EventHandler(this.toolStripButtonSirAC_Click);
            // 
            // toolStripButtonSirHolter
            // 
            this.toolStripButtonSirHolter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonSirHolter.Font = new System.Drawing.Font("Verdana", 9F);
            this.toolStripButtonSirHolter.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonSirHolter.Image")));
            this.toolStripButtonSirHolter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonSirHolter.Name = "toolStripButtonSirHolter";
            this.toolStripButtonSirHolter.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonSirHolter.Size = new System.Drawing.Size(37, 41);
            this.toolStripButtonSirHolter.Text = "+HE";
            this.toolStripButtonSirHolter.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.toolStripButtonSirHolter.ToolTipText = "Sirona Holter Event";
            this.toolStripButtonSirHolter.Click += new System.EventHandler(this.toolStripButtonSirHolter_Click);
            // 
            // toolStripButtonPaSir
            // 
            this.toolStripButtonPaSir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonPaSir.Font = new System.Drawing.Font("Verdana", 9F);
            this.toolStripButtonPaSir.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonPaSir.Image")));
            this.toolStripButtonPaSir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonPaSir.Name = "toolStripButtonPaSir";
            this.toolStripButtonPaSir.Size = new System.Drawing.Size(50, 41);
            this.toolStripButtonPaSir.Text = "?PaSir";
            this.toolStripButtonPaSir.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.toolStripButtonPaSir.ToolTipText = "Sirona Pre-Analysis";
            this.toolStripButtonPaSir.Click += new System.EventHandler(this.toolStripButtonPaSir_Click);
            // 
            // toolStripAutoTZe
            // 
            this.toolStripAutoTZe.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripAutoTZe.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripAutoTZe.Image = ((System.Drawing.Image)(resources.GetObject("toolStripAutoTZe.Image")));
            this.toolStripAutoTZe.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripAutoTZe.Name = "toolStripAutoTZe";
            this.toolStripAutoTZe.Size = new System.Drawing.Size(56, 41);
            this.toolStripAutoTZe.Text = "_Tze";
            this.toolStripAutoTZe.ToolTipText = "Auto Read  TZ events";
            this.toolStripAutoTZe.Click += new System.EventHandler(this.toolStripAutoTZe_Click);
            // 
            // toolStripAddTZ
            // 
            this.toolStripAddTZ.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripAddTZ.Image = ((System.Drawing.Image)(resources.GetObject("toolStripAddTZ.Image")));
            this.toolStripAddTZ.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripAddTZ.Name = "toolStripAddTZ";
            this.toolStripAddTZ.Size = new System.Drawing.Size(36, 41);
            this.toolStripAddTZ.Text = "toolStripAddTZ";
            this.toolStripAddTZ.ToolTipText = "Add new TZ device";
            this.toolStripAddTZ.Visible = false;
            this.toolStripAddTZ.Click += new System.EventHandler(this.toolStripAddTZ_Click);
            // 
            // toolStripButtonTzRB
            // 
            this.toolStripButtonTzRB.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonTzRB.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripButtonTzRB.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonTzRB.Image")));
            this.toolStripButtonTzRB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonTzRB.Name = "toolStripButtonTzRB";
            this.toolStripButtonTzRB.Size = new System.Drawing.Size(29, 41);
            this.toolStripButtonTzRB.Text = "+R";
            this.toolStripButtonTzRB.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.toolStripButtonTzRB.ToolTipText = "TZ Request + Bulk upload";
            this.toolStripButtonTzRB.Click += new System.EventHandler(this.toolStripButtonTzRB_Click);
            // 
            // toolStripButtonTzSkip
            // 
            this.toolStripButtonTzSkip.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonTzSkip.Font = new System.Drawing.Font("Verdana", 9F);
            this.toolStripButtonTzSkip.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonTzSkip.Image")));
            this.toolStripButtonTzSkip.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonTzSkip.Name = "toolStripButtonTzSkip";
            this.toolStripButtonTzSkip.Size = new System.Drawing.Size(52, 41);
            this.toolStripButtonTzSkip.Text = "SkipTZ";
            this.toolStripButtonTzSkip.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.toolStripButtonTzSkip.ToolTipText = "TZ Skip same";
            this.toolStripButtonTzSkip.Click += new System.EventHandler(this.toolStripButtonTzSkip_Click);
            // 
            // toolStripTzAutoCollect
            // 
            this.toolStripTzAutoCollect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripTzAutoCollect.Font = new System.Drawing.Font("Verdana", 9F);
            this.toolStripTzAutoCollect.Image = ((System.Drawing.Image)(resources.GetObject("toolStripTzAutoCollect.Image")));
            this.toolStripTzAutoCollect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripTzAutoCollect.Name = "toolStripTzAutoCollect";
            this.toolStripTzAutoCollect.Size = new System.Drawing.Size(45, 41);
            this.toolStripTzAutoCollect.Text = "ac off";
            this.toolStripTzAutoCollect.ToolTipText = "TZ Auto Collect Files";
            this.toolStripTzAutoCollect.Click += new System.EventHandler(this.toolStripTzAutoCollect_Click);
            // 
            // toolStripButtonPaTZ
            // 
            this.toolStripButtonPaTZ.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonPaTZ.Font = new System.Drawing.Font("Verdana", 9F);
            this.toolStripButtonPaTZ.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonPaTZ.Image")));
            this.toolStripButtonPaTZ.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonPaTZ.Name = "toolStripButtonPaTZ";
            this.toolStripButtonPaTZ.Size = new System.Drawing.Size(47, 41);
            this.toolStripButtonPaTZ.Text = "?PaTz";
            this.toolStripButtonPaTZ.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.toolStripButtonPaTZ.ToolTipText = "TZ Pre-Analysis";
            this.toolStripButtonPaTZ.Click += new System.EventHandler(this.toolStripButtonPaTZ_Click);
            // 
            // toolStripAutoDV2
            // 
            this.toolStripAutoDV2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripAutoDV2.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripAutoDV2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripAutoDV2.Image")));
            this.toolStripAutoDV2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripAutoDV2.Name = "toolStripAutoDV2";
            this.toolStripAutoDV2.Size = new System.Drawing.Size(59, 41);
            this.toolStripAutoDV2.Text = "_DV2";
            this.toolStripAutoDV2.ToolTipText = "Auto read DV2";
            this.toolStripAutoDV2.Click += new System.EventHandler(this.toolStripAutoDV2_Click);
            // 
            // toolStripButtonPaDV2
            // 
            this.toolStripButtonPaDV2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonPaDV2.Font = new System.Drawing.Font("Verdana", 9F);
            this.toolStripButtonPaDV2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonPaDV2.Image")));
            this.toolStripButtonPaDV2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonPaDV2.Name = "toolStripButtonPaDV2";
            this.toolStripButtonPaDV2.Size = new System.Drawing.Size(59, 41);
            this.toolStripButtonPaDV2.Text = "?PaDV2";
            this.toolStripButtonPaDV2.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.toolStripButtonPaDV2.ToolTipText = "DV2 Pre-Analysis";
            this.toolStripButtonPaDV2.Click += new System.EventHandler(this.toolStripButtonPaDV2_Click);
            // 
            // toolStripAutoDVX
            // 
            this.toolStripAutoDVX.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripAutoDVX.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripAutoDVX.Image = ((System.Drawing.Image)(resources.GetObject("toolStripAutoDVX.Image")));
            this.toolStripAutoDVX.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripAutoDVX.Name = "toolStripAutoDVX";
            this.toolStripAutoDVX.Size = new System.Drawing.Size(60, 41);
            this.toolStripAutoDVX.Text = "_DVX";
            this.toolStripAutoDVX.ToolTipText = "Auto read DVX";
            this.toolStripAutoDVX.Click += new System.EventHandler(this.toolStripAutoDVX_Click);
            // 
            // toolStripButtonSkipDVX
            // 
            this.toolStripButtonSkipDVX.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonSkipDVX.Font = new System.Drawing.Font("Verdana", 9F);
            this.toolStripButtonSkipDVX.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonSkipDVX.Image")));
            this.toolStripButtonSkipDVX.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonSkipDVX.Name = "toolStripButtonSkipDVX";
            this.toolStripButtonSkipDVX.Size = new System.Drawing.Size(62, 41);
            this.toolStripButtonSkipDVX.Text = "SkipDVX";
            this.toolStripButtonSkipDVX.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.toolStripButtonSkipDVX.ToolTipText = "SkipDVX not needed for new DVX firmware";
            this.toolStripButtonSkipDVX.Click += new System.EventHandler(this.toolStripButtonSkipDVX_Click);
            // 
            // toolStripButtonPaDVX
            // 
            this.toolStripButtonPaDVX.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonPaDVX.Font = new System.Drawing.Font("Verdana", 9F);
            this.toolStripButtonPaDVX.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonPaDVX.Image")));
            this.toolStripButtonPaDVX.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonPaDVX.Name = "toolStripButtonPaDVX";
            this.toolStripButtonPaDVX.Size = new System.Drawing.Size(59, 41);
            this.toolStripButtonPaDVX.Text = "?PaDVX";
            this.toolStripButtonPaDVX.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.toolStripButtonPaDVX.ToolTipText = "DVX Pre-Analysis";
            this.toolStripButtonPaDVX.Click += new System.EventHandler(this.toolStripButtonPaDVX_Click);
            // 
            // toolStripAddDevice
            // 
            this.toolStripAddDevice.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripAddDevice.Image = ((System.Drawing.Image)(resources.GetObject("toolStripAddDevice.Image")));
            this.toolStripAddDevice.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripAddDevice.Name = "toolStripAddDevice";
            this.toolStripAddDevice.Size = new System.Drawing.Size(36, 41);
            this.toolStripAddDevice.Text = "toolStripButton2";
            this.toolStripAddDevice.ToolTipText = "Add Device";
            this.toolStripAddDevice.Click += new System.EventHandler(this.toolStripAddDevice_Click);
            // 
            // toolStripImport
            // 
            this.toolStripImport.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripImport.Image = ((System.Drawing.Image)(resources.GetObject("toolStripImport.Image")));
            this.toolStripImport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripImport.Name = "toolStripImport";
            this.toolStripImport.Size = new System.Drawing.Size(36, 41);
            this.toolStripImport.Text = "toolStripButton1";
            this.toolStripImport.ToolTipText = "Import file";
            this.toolStripImport.Click += new System.EventHandler(this.toolStripImport_Click);
            // 
            // toolStripButtonWarn
            // 
            this.toolStripButtonWarn.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonWarn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonWarn.Font = new System.Drawing.Font("Arial Narrow", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripButtonWarn.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.toolStripButtonWarn.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonWarn.Image")));
            this.toolStripButtonWarn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonWarn.Name = "toolStripButtonWarn";
            this.toolStripButtonWarn.Size = new System.Drawing.Size(30, 41);
            this.toolStripButtonWarn.Text = "!";
            this.toolStripButtonWarn.Click += new System.EventHandler(this.toolStripButtonWarn_Click);
            // 
            // toolStripButtonHelp
            // 
            this.toolStripButtonHelp.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonHelp.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonHelp.Image")));
            this.toolStripButtonHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonHelp.Name = "toolStripButtonHelp";
            this.toolStripButtonHelp.Size = new System.Drawing.Size(36, 41);
            this.toolStripButtonHelp.Text = "Menu Help";
            this.toolStripButtonHelp.Click += new System.EventHandler(this.toolStripButtonHelp_Click);
            // 
            // openDV2ScpFileDialog
            // 
            this.openDV2ScpFileDialog.FileName = "openZipFileDialog";
            this.openDV2ScpFileDialog.Filter = "SCP|*.scp|All|*.*";
            this.openDV2ScpFileDialog.Multiselect = true;
            this.openDV2ScpFileDialog.Title = "Import DV2 SCP File";
            // 
            // contextMenuHelp
            // 
            this.contextMenuHelp.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openManualToolStripMenuItem,
            this.openChangeHistoryToolStripMenuItem,
            this.openLicenseInfoToolStripMenuItem,
            this.openDownloadLinksToolStripMenuItem,
            this.openContentFolderToolStripMenuItem,
            this.toolStripSeparator4,
            this.tMIHelpdeskToolStripMenuItem,
            this.openStandardTeamviewerToolStripMenuItem,
            this.toolStripSeparator5,
            this.logActiveLicinseToolStripMenuItem,
            this.toolStripSeparator10,
            this.manageUsersToolStripMenuItem,
            this.toolStripMenuItem1,
            this.createSQLEncryptedPasswordToolStripMenuItem,
            this.createSmtpEncryptedPasswordToolStripMenuItem,
            this.programInfoToolStripMenuItem});
            this.contextMenuHelp.Name = "contextMenuHelp";
            this.contextMenuHelp.Size = new System.Drawing.Size(248, 292);
            this.contextMenuHelp.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuHelp_Opening);
            // 
            // openManualToolStripMenuItem
            // 
            this.openManualToolStripMenuItem.Name = "openManualToolStripMenuItem";
            this.openManualToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.openManualToolStripMenuItem.Text = "Open Manual";
            this.openManualToolStripMenuItem.Click += new System.EventHandler(this.openManualToolStripMenuItem_Click);
            // 
            // openChangeHistoryToolStripMenuItem
            // 
            this.openChangeHistoryToolStripMenuItem.Name = "openChangeHistoryToolStripMenuItem";
            this.openChangeHistoryToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.openChangeHistoryToolStripMenuItem.Text = "Open Change History";
            this.openChangeHistoryToolStripMenuItem.Click += new System.EventHandler(this.openChangeHistoryToolStripMenuItem_Click);
            // 
            // openLicenseInfoToolStripMenuItem
            // 
            this.openLicenseInfoToolStripMenuItem.Name = "openLicenseInfoToolStripMenuItem";
            this.openLicenseInfoToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.openLicenseInfoToolStripMenuItem.Text = "Open License Info";
            this.openLicenseInfoToolStripMenuItem.Click += new System.EventHandler(this.openLicenseInfoToolStripMenuItem_Click);
            // 
            // openDownloadLinksToolStripMenuItem
            // 
            this.openDownloadLinksToolStripMenuItem.Name = "openDownloadLinksToolStripMenuItem";
            this.openDownloadLinksToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.openDownloadLinksToolStripMenuItem.Text = "Open Download links";
            this.openDownloadLinksToolStripMenuItem.Visible = false;
            this.openDownloadLinksToolStripMenuItem.Click += new System.EventHandler(this.openDownloadLinksToolStripMenuItem_Click);
            // 
            // openContentFolderToolStripMenuItem
            // 
            this.openContentFolderToolStripMenuItem.Name = "openContentFolderToolStripMenuItem";
            this.openContentFolderToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.openContentFolderToolStripMenuItem.Text = "Open Content folder";
            this.openContentFolderToolStripMenuItem.Click += new System.EventHandler(this.openContentFolderToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(244, 6);
            // 
            // tMIHelpdeskToolStripMenuItem
            // 
            this.tMIHelpdeskToolStripMenuItem.Name = "tMIHelpdeskToolStripMenuItem";
            this.tMIHelpdeskToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.tMIHelpdeskToolStripMenuItem.Text = "TMI Helpdesk";
            this.tMIHelpdeskToolStripMenuItem.Click += new System.EventHandler(this.tMIHelpdeskToolStripMenuItem_Click);
            // 
            // openStandardTeamviewerToolStripMenuItem
            // 
            this.openStandardTeamviewerToolStripMenuItem.Name = "openStandardTeamviewerToolStripMenuItem";
            this.openStandardTeamviewerToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.openStandardTeamviewerToolStripMenuItem.Text = "Open standard Teamviewer";
            this.openStandardTeamviewerToolStripMenuItem.Click += new System.EventHandler(this.openStandardTeamviewerToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(244, 6);
            // 
            // logActiveLicinseToolStripMenuItem
            // 
            this.logActiveLicinseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sironaToolStripMenuItem,
            this.tZToolStripMenuItem,
            this.dV2ToolStripMenuItem,
            this.dVXToolStripMenuItem});
            this.logActiveLicinseToolStripMenuItem.Name = "logActiveLicinseToolStripMenuItem";
            this.logActiveLicinseToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.logActiveLicinseToolStripMenuItem.Text = "Log Active License";
            // 
            // sironaToolStripMenuItem
            // 
            this.sironaToolStripMenuItem.Name = "sironaToolStripMenuItem";
            this.sironaToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.sironaToolStripMenuItem.Text = "Sirona";
            this.sironaToolStripMenuItem.Click += new System.EventHandler(this.sironaToolStripMenuItem_Click);
            // 
            // tZToolStripMenuItem
            // 
            this.tZToolStripMenuItem.Name = "tZToolStripMenuItem";
            this.tZToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.tZToolStripMenuItem.Text = "TZ";
            this.tZToolStripMenuItem.Click += new System.EventHandler(this.tZToolStripMenuItem_Click);
            // 
            // dV2ToolStripMenuItem
            // 
            this.dV2ToolStripMenuItem.Name = "dV2ToolStripMenuItem";
            this.dV2ToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.dV2ToolStripMenuItem.Text = "DV2";
            this.dV2ToolStripMenuItem.Click += new System.EventHandler(this.dV2ToolStripMenuItem_Click);
            // 
            // dVXToolStripMenuItem
            // 
            this.dVXToolStripMenuItem.Name = "dVXToolStripMenuItem";
            this.dVXToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.dVXToolStripMenuItem.Text = "DVX";
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(244, 6);
            // 
            // manageUsersToolStripMenuItem
            // 
            this.manageUsersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rewriteUserListToolStripMenuItem,
            this.addUserToolStripMenuItem,
            this.makeUserPinToolStripMenuItem,
            this.testUserToolStripMenuItem});
            this.manageUsersToolStripMenuItem.Name = "manageUsersToolStripMenuItem";
            this.manageUsersToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.manageUsersToolStripMenuItem.Text = "Manage Users";
            // 
            // rewriteUserListToolStripMenuItem
            // 
            this.rewriteUserListToolStripMenuItem.Name = "rewriteUserListToolStripMenuItem";
            this.rewriteUserListToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.rewriteUserListToolStripMenuItem.Text = "Rewrite User List";
            this.rewriteUserListToolStripMenuItem.Click += new System.EventHandler(this.rewriteUserListToolStripMenuItem_Click);
            // 
            // addUserToolStripMenuItem
            // 
            this.addUserToolStripMenuItem.Name = "addUserToolStripMenuItem";
            this.addUserToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.addUserToolStripMenuItem.Text = "Add User";
            this.addUserToolStripMenuItem.Click += new System.EventHandler(this.addUserToolStripMenuItem_Click);
            // 
            // makeUserPinToolStripMenuItem
            // 
            this.makeUserPinToolStripMenuItem.Name = "makeUserPinToolStripMenuItem";
            this.makeUserPinToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.makeUserPinToolStripMenuItem.Text = "Make User Pin";
            this.makeUserPinToolStripMenuItem.Click += new System.EventHandler(this.makeUserPinToolStripMenuItem_Click);
            // 
            // testUserToolStripMenuItem
            // 
            this.testUserToolStripMenuItem.Name = "testUserToolStripMenuItem";
            this.testUserToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.testUserToolStripMenuItem.Text = "Test User";
            this.testUserToolStripMenuItem.Click += new System.EventHandler(this.testUserToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(244, 6);
            // 
            // createSQLEncryptedPasswordToolStripMenuItem
            // 
            this.createSQLEncryptedPasswordToolStripMenuItem.Name = "createSQLEncryptedPasswordToolStripMenuItem";
            this.createSQLEncryptedPasswordToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.createSQLEncryptedPasswordToolStripMenuItem.Text = "Create SQL encrypted password";
            this.createSQLEncryptedPasswordToolStripMenuItem.Click += new System.EventHandler(this.createSQLEncryptedPasswordToolStripMenuItem_Click);
            // 
            // createSmtpEncryptedPasswordToolStripMenuItem
            // 
            this.createSmtpEncryptedPasswordToolStripMenuItem.Name = "createSmtpEncryptedPasswordToolStripMenuItem";
            this.createSmtpEncryptedPasswordToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.createSmtpEncryptedPasswordToolStripMenuItem.Text = "Create smtp encrypted password";
            this.createSmtpEncryptedPasswordToolStripMenuItem.Click += new System.EventHandler(this.createSmtpEncryptedPasswordToolStripMenuItem_Click);
            // 
            // programInfoToolStripMenuItem
            // 
            this.programInfoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.programInfoToClipboardToolStripMenuItem,
            this.toolStripSeparator6,
            this.openLogFileToolStripMenuItem,
            this.openLogFolderToolStripMenuItem,
            this.addLogLineToolStripMenuItem,
            this.newLogFileToolStripMenuItem,
            this.extraLoggingToolStripMenuItem,
            this.enableLogFlushToolStripMenuItem,
            this.toolStripSeparator7,
            this.openRecodingFolderToolStripMenuItem,
            this.toolStripSeparator8,
            this.checkDBCollectToolStripMenuItem,
            this.testFilesDurationToolStripMenuItem,
            this.testParseToolStripMenuItem,
            this.makeLauncherVersionToolStripMenuItem,
            this.toolStripSeparator9});
            this.programInfoToolStripMenuItem.Name = "programInfoToolStripMenuItem";
            this.programInfoToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.programInfoToolStripMenuItem.Text = "Program Info";
            // 
            // programInfoToClipboardToolStripMenuItem
            // 
            this.programInfoToClipboardToolStripMenuItem.Name = "programInfoToClipboardToolStripMenuItem";
            this.programInfoToClipboardToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.programInfoToClipboardToolStripMenuItem.Text = "Program info to Clipboard ";
            this.programInfoToClipboardToolStripMenuItem.Click += new System.EventHandler(this.programInfoToClipboardToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(213, 6);
            // 
            // openLogFileToolStripMenuItem
            // 
            this.openLogFileToolStripMenuItem.Name = "openLogFileToolStripMenuItem";
            this.openLogFileToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.openLogFileToolStripMenuItem.Text = "Open Log File";
            this.openLogFileToolStripMenuItem.Click += new System.EventHandler(this.openLogFileToolStripMenuItem_Click);
            // 
            // openLogFolderToolStripMenuItem
            // 
            this.openLogFolderToolStripMenuItem.Name = "openLogFolderToolStripMenuItem";
            this.openLogFolderToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.openLogFolderToolStripMenuItem.Text = "Open Log Folder";
            this.openLogFolderToolStripMenuItem.Click += new System.EventHandler(this.openLogFolderToolStripMenuItem_Click_1);
            // 
            // addLogLineToolStripMenuItem
            // 
            this.addLogLineToolStripMenuItem.Name = "addLogLineToolStripMenuItem";
            this.addLogLineToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.addLogLineToolStripMenuItem.Text = "Add Log Line";
            this.addLogLineToolStripMenuItem.Click += new System.EventHandler(this.addLogLineToolStripMenuItem_Click);
            // 
            // newLogFileToolStripMenuItem
            // 
            this.newLogFileToolStripMenuItem.Name = "newLogFileToolStripMenuItem";
            this.newLogFileToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.newLogFileToolStripMenuItem.Text = "New Log File";
            this.newLogFileToolStripMenuItem.Click += new System.EventHandler(this.newLogFileToolStripMenuItem_Click);
            // 
            // extraLoggingToolStripMenuItem
            // 
            this.extraLoggingToolStripMenuItem.Name = "extraLoggingToolStripMenuItem";
            this.extraLoggingToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.extraLoggingToolStripMenuItem.Text = "Enable Extra logging";
            this.extraLoggingToolStripMenuItem.Click += new System.EventHandler(this.extraLoggingToolStripMenuItem_Click);
            // 
            // enableLogFlushToolStripMenuItem
            // 
            this.enableLogFlushToolStripMenuItem.Name = "enableLogFlushToolStripMenuItem";
            this.enableLogFlushToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.enableLogFlushToolStripMenuItem.Text = "Enable Log Flush";
            this.enableLogFlushToolStripMenuItem.Click += new System.EventHandler(this.enableLogFlushToolStripMenuItem_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(213, 6);
            // 
            // openRecodingFolderToolStripMenuItem
            // 
            this.openRecodingFolderToolStripMenuItem.Name = "openRecodingFolderToolStripMenuItem";
            this.openRecodingFolderToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.openRecodingFolderToolStripMenuItem.Text = "Open Recoding Folder";
            this.openRecodingFolderToolStripMenuItem.Click += new System.EventHandler(this.openRecodingFolderToolStripMenuItem_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(213, 6);
            // 
            // checkDBCollectToolStripMenuItem
            // 
            this.checkDBCollectToolStripMenuItem.Name = "checkDBCollectToolStripMenuItem";
            this.checkDBCollectToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.checkDBCollectToolStripMenuItem.Text = "Check DB Collect";
            this.checkDBCollectToolStripMenuItem.Click += new System.EventHandler(this.checkDBCollectToolStripMenuItem_Click);
            // 
            // testFilesDurationToolStripMenuItem
            // 
            this.testFilesDurationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createTestFilesToolStripMenuItem,
            this.countTestFilesToolStripMenuItem,
            this.readTestFilesToolStripMenuItem,
            this.deleteTestFilesToolStripMenuItem,
            this.recordingCreateTestFilesToolStripMenuItem,
            this.recordingCountTestFilesToolStripMenuItem,
            this.recordingTestFilesToolStripMenuItem,
            this.recordingDeleteTestFilesToolStripMenuItem,
            this.folderCreateTestFilesToolStripMenuItem,
            this.folderCountTestFilesToolStripMenuItem,
            this.folderReadTestFilesToolStripMenuItem,
            this.folderDeleteTestFilesToolStripMenuItem});
            this.testFilesDurationToolStripMenuItem.Name = "testFilesDurationToolStripMenuItem";
            this.testFilesDurationToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.testFilesDurationToolStripMenuItem.Text = "Test Files Duration";
            // 
            // createTestFilesToolStripMenuItem
            // 
            this.createTestFilesToolStripMenuItem.Name = "createTestFilesToolStripMenuItem";
            this.createTestFilesToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.createTestFilesToolStripMenuItem.Text = "Local - Create Test Files";
            this.createTestFilesToolStripMenuItem.Click += new System.EventHandler(this.createTestFilesToolStripMenuItem_Click);
            // 
            // countTestFilesToolStripMenuItem
            // 
            this.countTestFilesToolStripMenuItem.Name = "countTestFilesToolStripMenuItem";
            this.countTestFilesToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.countTestFilesToolStripMenuItem.Text = "Local - Count Test Files";
            this.countTestFilesToolStripMenuItem.Click += new System.EventHandler(this.countTestFilesToolStripMenuItem_Click);
            // 
            // readTestFilesToolStripMenuItem
            // 
            this.readTestFilesToolStripMenuItem.Name = "readTestFilesToolStripMenuItem";
            this.readTestFilesToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.readTestFilesToolStripMenuItem.Text = "Local - Read Test Files";
            this.readTestFilesToolStripMenuItem.Click += new System.EventHandler(this.readTestFilesToolStripMenuItem_Click);
            // 
            // deleteTestFilesToolStripMenuItem
            // 
            this.deleteTestFilesToolStripMenuItem.Name = "deleteTestFilesToolStripMenuItem";
            this.deleteTestFilesToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.deleteTestFilesToolStripMenuItem.Text = "Local - Delete Test Files";
            this.deleteTestFilesToolStripMenuItem.Click += new System.EventHandler(this.deleteTestFilesToolStripMenuItem_Click);
            // 
            // recordingCreateTestFilesToolStripMenuItem
            // 
            this.recordingCreateTestFilesToolStripMenuItem.Name = "recordingCreateTestFilesToolStripMenuItem";
            this.recordingCreateTestFilesToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.recordingCreateTestFilesToolStripMenuItem.Text = "Recording - Create Test Files";
            this.recordingCreateTestFilesToolStripMenuItem.Click += new System.EventHandler(this.recordingCreateTestFilesToolStripMenuItem_Click);
            // 
            // recordingCountTestFilesToolStripMenuItem
            // 
            this.recordingCountTestFilesToolStripMenuItem.Name = "recordingCountTestFilesToolStripMenuItem";
            this.recordingCountTestFilesToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.recordingCountTestFilesToolStripMenuItem.Text = "Recording - Count Test Files ";
            this.recordingCountTestFilesToolStripMenuItem.Click += new System.EventHandler(this.recordingCountTestFilesToolStripMenuItem_Click);
            // 
            // recordingTestFilesToolStripMenuItem
            // 
            this.recordingTestFilesToolStripMenuItem.Name = "recordingTestFilesToolStripMenuItem";
            this.recordingTestFilesToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.recordingTestFilesToolStripMenuItem.Text = "Recording - Read Test Files";
            this.recordingTestFilesToolStripMenuItem.Click += new System.EventHandler(this.recordingTestFilesToolStripMenuItem_Click);
            // 
            // recordingDeleteTestFilesToolStripMenuItem
            // 
            this.recordingDeleteTestFilesToolStripMenuItem.Name = "recordingDeleteTestFilesToolStripMenuItem";
            this.recordingDeleteTestFilesToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.recordingDeleteTestFilesToolStripMenuItem.Text = "Recording - Delete Test Files";
            this.recordingDeleteTestFilesToolStripMenuItem.Click += new System.EventHandler(this.recordingDeleteTestFilesToolStripMenuItem_Click);
            // 
            // folderCreateTestFilesToolStripMenuItem
            // 
            this.folderCreateTestFilesToolStripMenuItem.Name = "folderCreateTestFilesToolStripMenuItem";
            this.folderCreateTestFilesToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.folderCreateTestFilesToolStripMenuItem.Text = "Folder - Create Test Files";
            this.folderCreateTestFilesToolStripMenuItem.Click += new System.EventHandler(this.folderCreateTestFilesToolStripMenuItem_Click);
            // 
            // folderCountTestFilesToolStripMenuItem
            // 
            this.folderCountTestFilesToolStripMenuItem.Name = "folderCountTestFilesToolStripMenuItem";
            this.folderCountTestFilesToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.folderCountTestFilesToolStripMenuItem.Text = "Folder -  Count Test Files";
            this.folderCountTestFilesToolStripMenuItem.Click += new System.EventHandler(this.folderCountTestFilesToolStripMenuItem_Click);
            // 
            // folderReadTestFilesToolStripMenuItem
            // 
            this.folderReadTestFilesToolStripMenuItem.Name = "folderReadTestFilesToolStripMenuItem";
            this.folderReadTestFilesToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.folderReadTestFilesToolStripMenuItem.Text = "Folder -  Read Test Files";
            this.folderReadTestFilesToolStripMenuItem.Click += new System.EventHandler(this.folderReadTestFilesToolStripMenuItem_Click);
            // 
            // folderDeleteTestFilesToolStripMenuItem
            // 
            this.folderDeleteTestFilesToolStripMenuItem.Name = "folderDeleteTestFilesToolStripMenuItem";
            this.folderDeleteTestFilesToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.folderDeleteTestFilesToolStripMenuItem.Text = "Folder -  Delete Test Files";
            this.folderDeleteTestFilesToolStripMenuItem.Click += new System.EventHandler(this.folderDeleteTestFilesToolStripMenuItem_Click);
            // 
            // testParseToolStripMenuItem
            // 
            this.testParseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.parseDoubleToolStripMenuItem,
            this.parseExactDateTimeToolStripMenuItem,
            this.progParseDoubleToolStripMenuItem,
            this.progParseDateTimeZoneToolStripMenuItem});
            this.testParseToolStripMenuItem.Name = "testParseToolStripMenuItem";
            this.testParseToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.testParseToolStripMenuItem.Text = "Test Parse";
            // 
            // parseDoubleToolStripMenuItem
            // 
            this.parseDoubleToolStripMenuItem.Name = "parseDoubleToolStripMenuItem";
            this.parseDoubleToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.parseDoubleToolStripMenuItem.Text = "Parse double";
            this.parseDoubleToolStripMenuItem.Click += new System.EventHandler(this.parseDoubleToolStripMenuItem_Click);
            // 
            // parseExactDateTimeToolStripMenuItem
            // 
            this.parseExactDateTimeToolStripMenuItem.Name = "parseExactDateTimeToolStripMenuItem";
            this.parseExactDateTimeToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.parseExactDateTimeToolStripMenuItem.Text = "Parse Exact Date Time";
            this.parseExactDateTimeToolStripMenuItem.Click += new System.EventHandler(this.parseExactDateTimeToolStripMenuItem_Click);
            // 
            // progParseDoubleToolStripMenuItem
            // 
            this.progParseDoubleToolStripMenuItem.Name = "progParseDoubleToolStripMenuItem";
            this.progParseDoubleToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.progParseDoubleToolStripMenuItem.Text = "Prog Parse Double";
            this.progParseDoubleToolStripMenuItem.Click += new System.EventHandler(this.progParseDoubleToolStripMenuItem_Click);
            // 
            // progParseDateTimeZoneToolStripMenuItem
            // 
            this.progParseDateTimeZoneToolStripMenuItem.Name = "progParseDateTimeZoneToolStripMenuItem";
            this.progParseDateTimeZoneToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.progParseDateTimeZoneToolStripMenuItem.Text = "Prog Parse Date Time Zone";
            // 
            // makeLauncherVersionToolStripMenuItem
            // 
            this.makeLauncherVersionToolStripMenuItem.Name = "makeLauncherVersionToolStripMenuItem";
            this.makeLauncherVersionToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.makeLauncherVersionToolStripMenuItem.Text = "Make Launcher Version";
            this.makeLauncherVersionToolStripMenuItem.Click += new System.EventHandler(this.makeLauncherVersionToolStripMenuItem_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(213, 6);
            // 
            // timerProcessMail
            // 
            this.timerProcessMail.Interval = 2000;
            this.timerProcessMail.Tick += new System.EventHandler(this.timerProcessMail_Tick);
            // 
            // contextMenuImport
            // 
            this.contextMenuImport.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.contextMenuImport.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.importSironaZipToolStripMenuItem,
            this.importSironaHeaToolStripMenuItem,
            this.toolStripSeparator15,
            this.importTZEventToolStripMenuItem,
            this.importTZScpToolStripMenuItem,
            this.toolStripSeparator16,
            this.importDV2ScpToolStripMenuItem,
            this.importEdfToolStripMenuItem,
            this.toolStripSeparator17,
            this.dVXTestToolStripMenuItem,
            this.importDVX1DatToolStripMenuItem,
            this.importDVX1EvtToolStripMenuItem,
            this.importDVX1EvtRecToolStripMenuItem,
            this.toolStripSeparator2,
            this.importNoninToolStripMenuItem,
            this.convertDVXSpO2ToolStripMenuItem,
            this.importDVXSpO2ToolStripMenuItem,
            this.toolStripSeparator18,
            this.dupRecordToolStripMenuItem,
            this.preAnalysisToolStripMenuItem});
            this.contextMenuImport.Name = "contextMenuImport";
            this.contextMenuImport.Size = new System.Drawing.Size(197, 626);
            this.contextMenuImport.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuImport_Opening);
            // 
            // importSironaZipToolStripMenuItem
            // 
            this.importSironaZipToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("importSironaZipToolStripMenuItem.Image")));
            this.importSironaZipToolStripMenuItem.Name = "importSironaZipToolStripMenuItem";
            this.importSironaZipToolStripMenuItem.Size = new System.Drawing.Size(196, 38);
            this.importSironaZipToolStripMenuItem.Text = "Import Sirona Zip";
            this.importSironaZipToolStripMenuItem.Click += new System.EventHandler(this.importSironaZipToolStripMenuItem_Click);
            // 
            // importSironaHeaToolStripMenuItem
            // 
            this.importSironaHeaToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("importSironaHeaToolStripMenuItem.Image")));
            this.importSironaHeaToolStripMenuItem.Name = "importSironaHeaToolStripMenuItem";
            this.importSironaHeaToolStripMenuItem.Size = new System.Drawing.Size(196, 38);
            this.importSironaHeaToolStripMenuItem.Text = "Import Sirona Hea";
            this.importSironaHeaToolStripMenuItem.Click += new System.EventHandler(this.importSironaHeaToolStripMenuItem_Click);
            // 
            // toolStripSeparator15
            // 
            this.toolStripSeparator15.Name = "toolStripSeparator15";
            this.toolStripSeparator15.Size = new System.Drawing.Size(193, 6);
            // 
            // importTZEventToolStripMenuItem
            // 
            this.importTZEventToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("importTZEventToolStripMenuItem.Image")));
            this.importTZEventToolStripMenuItem.Name = "importTZEventToolStripMenuItem";
            this.importTZEventToolStripMenuItem.Size = new System.Drawing.Size(196, 38);
            this.importTZEventToolStripMenuItem.Text = "Import TZ event";
            this.importTZEventToolStripMenuItem.Click += new System.EventHandler(this.importTZEventToolStripMenuItem_Click);
            // 
            // importTZScpToolStripMenuItem
            // 
            this.importTZScpToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("importTZScpToolStripMenuItem.Image")));
            this.importTZScpToolStripMenuItem.Name = "importTZScpToolStripMenuItem";
            this.importTZScpToolStripMenuItem.Size = new System.Drawing.Size(196, 38);
            this.importTZScpToolStripMenuItem.Text = "Import TZ Scp";
            this.importTZScpToolStripMenuItem.Click += new System.EventHandler(this.importTZScpToolStripMenuItem_Click);
            // 
            // toolStripSeparator16
            // 
            this.toolStripSeparator16.Name = "toolStripSeparator16";
            this.toolStripSeparator16.Size = new System.Drawing.Size(193, 6);
            // 
            // importDV2ScpToolStripMenuItem
            // 
            this.importDV2ScpToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("importDV2ScpToolStripMenuItem.Image")));
            this.importDV2ScpToolStripMenuItem.Name = "importDV2ScpToolStripMenuItem";
            this.importDV2ScpToolStripMenuItem.Size = new System.Drawing.Size(196, 38);
            this.importDV2ScpToolStripMenuItem.Text = "Import DV2 Scp";
            this.importDV2ScpToolStripMenuItem.Click += new System.EventHandler(this.importDV2ScpToolStripMenuItem_Click);
            // 
            // importEdfToolStripMenuItem
            // 
            this.importEdfToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("importEdfToolStripMenuItem.Image")));
            this.importEdfToolStripMenuItem.Name = "importEdfToolStripMenuItem";
            this.importEdfToolStripMenuItem.Size = new System.Drawing.Size(196, 38);
            this.importEdfToolStripMenuItem.Text = "Import EDF";
            this.importEdfToolStripMenuItem.Visible = false;
            this.importEdfToolStripMenuItem.Click += new System.EventHandler(this.importEdfToolStripMenuItem_Click);
            // 
            // toolStripSeparator17
            // 
            this.toolStripSeparator17.Name = "toolStripSeparator17";
            this.toolStripSeparator17.Size = new System.Drawing.Size(193, 6);
            // 
            // dVXTestToolStripMenuItem
            // 
            this.dVXTestToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.reverseBytesDatToolStripMenuItem,
            this.importDVXHeaToolStripMenuItem});
            this.dVXTestToolStripMenuItem.Name = "dVXTestToolStripMenuItem";
            this.dVXTestToolStripMenuItem.Size = new System.Drawing.Size(196, 38);
            this.dVXTestToolStripMenuItem.Text = "DVX Test";
            this.dVXTestToolStripMenuItem.Click += new System.EventHandler(this.dVXTestToolStripMenuItem_Click);
            // 
            // reverseBytesDatToolStripMenuItem
            // 
            this.reverseBytesDatToolStripMenuItem.Name = "reverseBytesDatToolStripMenuItem";
            this.reverseBytesDatToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.reverseBytesDatToolStripMenuItem.Text = "Reverse Bytes Dat";
            this.reverseBytesDatToolStripMenuItem.Click += new System.EventHandler(this.reverseBytesDatToolStripMenuItem_Click);
            // 
            // importDVXHeaToolStripMenuItem
            // 
            this.importDVXHeaToolStripMenuItem.Name = "importDVXHeaToolStripMenuItem";
            this.importDVXHeaToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.importDVXHeaToolStripMenuItem.Text = "Import DVX Hea";
            this.importDVXHeaToolStripMenuItem.Click += new System.EventHandler(this.importDVXHeaToolStripMenuItem_Click);
            // 
            // importDVX1DatToolStripMenuItem
            // 
            this.importDVX1DatToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listDatInfoToolStripMenuItem});
            this.importDVX1DatToolStripMenuItem.Name = "importDVX1DatToolStripMenuItem";
            this.importDVX1DatToolStripMenuItem.Size = new System.Drawing.Size(196, 38);
            this.importDVX1DatToolStripMenuItem.Text = "Import DVX1 Dat";
            this.importDVX1DatToolStripMenuItem.Click += new System.EventHandler(this.importDVX1DatToolStripMenuItem_Click);
            // 
            // listDatInfoToolStripMenuItem
            // 
            this.listDatInfoToolStripMenuItem.Name = "listDatInfoToolStripMenuItem";
            this.listDatInfoToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.listDatInfoToolStripMenuItem.Text = "List DVX1 Dat info";
            this.listDatInfoToolStripMenuItem.Click += new System.EventHandler(this.listDatInfoToolStripMenuItem_Click);
            // 
            // importDVX1EvtToolStripMenuItem
            // 
            this.importDVX1EvtToolStripMenuItem.Name = "importDVX1EvtToolStripMenuItem";
            this.importDVX1EvtToolStripMenuItem.Size = new System.Drawing.Size(196, 38);
            this.importDVX1EvtToolStripMenuItem.Text = "Import DVX1 Evt";
            this.importDVX1EvtToolStripMenuItem.Click += new System.EventHandler(this.importDVX1EvtToolStripMenuItem_Click);
            // 
            // importDVX1EvtRecToolStripMenuItem
            // 
            this.importDVX1EvtRecToolStripMenuItem.Name = "importDVX1EvtRecToolStripMenuItem";
            this.importDVX1EvtRecToolStripMenuItem.Size = new System.Drawing.Size(196, 38);
            this.importDVX1EvtRecToolStripMenuItem.Text = "Import DVX1 EvtRec";
            this.importDVX1EvtRecToolStripMenuItem.Click += new System.EventHandler(this.importDVX1EvtRecToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(193, 6);
            // 
            // importNoninToolStripMenuItem
            // 
            this.importNoninToolStripMenuItem.Name = "importNoninToolStripMenuItem";
            this.importNoninToolStripMenuItem.Size = new System.Drawing.Size(196, 38);
            this.importNoninToolStripMenuItem.Text = "Convert Nonin#7";
            this.importNoninToolStripMenuItem.Click += new System.EventHandler(this.convertNonin7ToolStripMenuItem_Click);
            // 
            // convertDVXSpO2ToolStripMenuItem
            // 
            this.convertDVXSpO2ToolStripMenuItem.Name = "convertDVXSpO2ToolStripMenuItem";
            this.convertDVXSpO2ToolStripMenuItem.Size = new System.Drawing.Size(196, 38);
            this.convertDVXSpO2ToolStripMenuItem.Text = "Convert DVX SpO2";
            this.convertDVXSpO2ToolStripMenuItem.Click += new System.EventHandler(this.convertDVXSpO2ToolStripMenuItem_Click);
            // 
            // importDVXSpO2ToolStripMenuItem
            // 
            this.importDVXSpO2ToolStripMenuItem.Name = "importDVXSpO2ToolStripMenuItem";
            this.importDVXSpO2ToolStripMenuItem.Size = new System.Drawing.Size(196, 38);
            this.importDVXSpO2ToolStripMenuItem.Text = "Import DVX SpO2";
            this.importDVXSpO2ToolStripMenuItem.Click += new System.EventHandler(this.importDVXSpO2ToolStripMenuItem_Click_1);
            // 
            // toolStripSeparator18
            // 
            this.toolStripSeparator18.Name = "toolStripSeparator18";
            this.toolStripSeparator18.Size = new System.Drawing.Size(193, 6);
            // 
            // dupRecordToolStripMenuItem
            // 
            this.dupRecordToolStripMenuItem.Name = "dupRecordToolStripMenuItem";
            this.dupRecordToolStripMenuItem.Size = new System.Drawing.Size(196, 38);
            this.dupRecordToolStripMenuItem.Text = "Dup Record";
            this.dupRecordToolStripMenuItem.Click += new System.EventHandler(this.dupRecordToolStripMenuItem_Click);
            // 
            // preAnalysisToolStripMenuItem
            // 
            this.preAnalysisToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cardioLogsToolStripMenuItem});
            this.preAnalysisToolStripMenuItem.Name = "preAnalysisToolStripMenuItem";
            this.preAnalysisToolStripMenuItem.Size = new System.Drawing.Size(196, 38);
            this.preAnalysisToolStripMenuItem.Text = "PreAnalysis";
            // 
            // cardioLogsToolStripMenuItem
            // 
            this.cardioLogsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.heaToolStripMenuItem,
            this.scpToolStripMenuItem});
            this.cardioLogsToolStripMenuItem.Name = "cardioLogsToolStripMenuItem";
            this.cardioLogsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.cardioLogsToolStripMenuItem.Text = "CardioLogs";
            // 
            // heaToolStripMenuItem
            // 
            this.heaToolStripMenuItem.Name = "heaToolStripMenuItem";
            this.heaToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.heaToolStripMenuItem.Text = "Hea";
            this.heaToolStripMenuItem.Click += new System.EventHandler(this.heaToolStripMenuItem_Click);
            // 
            // scpToolStripMenuItem
            // 
            this.scpToolStripMenuItem.Name = "scpToolStripMenuItem";
            this.scpToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.scpToolStripMenuItem.Text = "Scp";
            this.scpToolStripMenuItem.Click += new System.EventHandler(this.scpToolStripMenuItem_Click);
            // 
            // contextMenuAddDevice
            // 
            this.contextMenuAddDevice.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.contextMenuAddDevice.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addSironaToolStripMenuItem,
            this.listSironaLicenceToolStripMenuItem,
            this.toolStripSeparator11,
            this.addTZDeviceToolStripMenuItem,
            this.listTZLicenseToolStripMenuItem,
            this.toolStripSeparator12,
            this.addDV2DeviceToolStripMenuItem,
            this.listDV2LicenseToolStripMenuItem,
            this.toolStripSeparator13,
            this.addDVX1DeviceToolStripMenuItem,
            this.listDVXLicenseToolStripMenuItem,
            this.reloadDVXLicenseToolStripMenuItem,
            this.reloadDeviceLicensesToolStripMenuItem,
            this.createDVXDefaultsToolStripMenuItem,
            this.toolStripSeparator14,
            this.addEDFDeviceToolStripMenuItem});
            this.contextMenuAddDevice.Name = "contextMenuStrip1";
            this.contextMenuAddDevice.Size = new System.Drawing.Size(212, 484);
            this.contextMenuAddDevice.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuAddDevice_Opening);
            // 
            // addSironaToolStripMenuItem
            // 
            this.addSironaToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("addSironaToolStripMenuItem.Image")));
            this.addSironaToolStripMenuItem.Name = "addSironaToolStripMenuItem";
            this.addSironaToolStripMenuItem.Size = new System.Drawing.Size(211, 38);
            this.addSironaToolStripMenuItem.Text = "Add Sirona device";
            this.addSironaToolStripMenuItem.Click += new System.EventHandler(this.addSironaToolStripMenuItem_Click);
            // 
            // listSironaLicenceToolStripMenuItem
            // 
            this.listSironaLicenceToolStripMenuItem.Name = "listSironaLicenceToolStripMenuItem";
            this.listSironaLicenceToolStripMenuItem.Size = new System.Drawing.Size(211, 38);
            this.listSironaLicenceToolStripMenuItem.Text = "List Sirona Licence";
            this.listSironaLicenceToolStripMenuItem.Click += new System.EventHandler(this.listSironaLicenceToolStripMenuItem_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(208, 6);
            // 
            // addTZDeviceToolStripMenuItem
            // 
            this.addTZDeviceToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("addTZDeviceToolStripMenuItem.Image")));
            this.addTZDeviceToolStripMenuItem.Name = "addTZDeviceToolStripMenuItem";
            this.addTZDeviceToolStripMenuItem.Size = new System.Drawing.Size(211, 38);
            this.addTZDeviceToolStripMenuItem.Text = "Add TZ device";
            this.addTZDeviceToolStripMenuItem.Click += new System.EventHandler(this.addTZDeviceToolStripMenuItem_Click);
            // 
            // listTZLicenseToolStripMenuItem
            // 
            this.listTZLicenseToolStripMenuItem.Name = "listTZLicenseToolStripMenuItem";
            this.listTZLicenseToolStripMenuItem.Size = new System.Drawing.Size(211, 38);
            this.listTZLicenseToolStripMenuItem.Text = "List TZ License";
            this.listTZLicenseToolStripMenuItem.Click += new System.EventHandler(this.listTZLicenseToolStripMenuItem_Click);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(208, 6);
            // 
            // addDV2DeviceToolStripMenuItem
            // 
            this.addDV2DeviceToolStripMenuItem.Name = "addDV2DeviceToolStripMenuItem";
            this.addDV2DeviceToolStripMenuItem.Size = new System.Drawing.Size(211, 38);
            this.addDV2DeviceToolStripMenuItem.Text = "Add DV2 Device";
            this.addDV2DeviceToolStripMenuItem.Click += new System.EventHandler(this.addDV2DeviceToolStripMenuItem_Click);
            // 
            // listDV2LicenseToolStripMenuItem
            // 
            this.listDV2LicenseToolStripMenuItem.Name = "listDV2LicenseToolStripMenuItem";
            this.listDV2LicenseToolStripMenuItem.Size = new System.Drawing.Size(211, 38);
            this.listDV2LicenseToolStripMenuItem.Text = "List DV2 License";
            this.listDV2LicenseToolStripMenuItem.Click += new System.EventHandler(this.listDV2LicenseToolStripMenuItem_Click);
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(208, 6);
            // 
            // addDVX1DeviceToolStripMenuItem
            // 
            this.addDVX1DeviceToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("addDVX1DeviceToolStripMenuItem.Image")));
            this.addDVX1DeviceToolStripMenuItem.Name = "addDVX1DeviceToolStripMenuItem";
            this.addDVX1DeviceToolStripMenuItem.Size = new System.Drawing.Size(211, 38);
            this.addDVX1DeviceToolStripMenuItem.Text = "Add DVX Device";
            this.addDVX1DeviceToolStripMenuItem.Click += new System.EventHandler(this.addDVX1DeviceToolStripMenuItem_Click);
            // 
            // listDVXLicenseToolStripMenuItem
            // 
            this.listDVXLicenseToolStripMenuItem.Name = "listDVXLicenseToolStripMenuItem";
            this.listDVXLicenseToolStripMenuItem.Size = new System.Drawing.Size(211, 38);
            this.listDVXLicenseToolStripMenuItem.Text = "List DVX License";
            this.listDVXLicenseToolStripMenuItem.Click += new System.EventHandler(this.listDVXLicenseToolStripMenuItem_Click);
            // 
            // reloadDVXLicenseToolStripMenuItem
            // 
            this.reloadDVXLicenseToolStripMenuItem.Name = "reloadDVXLicenseToolStripMenuItem";
            this.reloadDVXLicenseToolStripMenuItem.Size = new System.Drawing.Size(211, 38);
            this.reloadDVXLicenseToolStripMenuItem.Text = "Reload DVX License";
            this.reloadDVXLicenseToolStripMenuItem.Click += new System.EventHandler(this.reloadDVXLicenseToolStripMenuItem_Click);
            // 
            // reloadDeviceLicensesToolStripMenuItem
            // 
            this.reloadDeviceLicensesToolStripMenuItem.Name = "reloadDeviceLicensesToolStripMenuItem";
            this.reloadDeviceLicensesToolStripMenuItem.Size = new System.Drawing.Size(211, 38);
            this.reloadDeviceLicensesToolStripMenuItem.Text = "Reload Device Licenses";
            this.reloadDeviceLicensesToolStripMenuItem.Click += new System.EventHandler(this.reloadDeviceLicensesToolStripMenuItem_Click);
            // 
            // createDVXDefaultsToolStripMenuItem
            // 
            this.createDVXDefaultsToolStripMenuItem.Name = "createDVXDefaultsToolStripMenuItem";
            this.createDVXDefaultsToolStripMenuItem.Size = new System.Drawing.Size(211, 38);
            this.createDVXDefaultsToolStripMenuItem.Text = "Create DVX defaults";
            this.createDVXDefaultsToolStripMenuItem.Click += new System.EventHandler(this.createDVXDefaultsToolStripMenuItem_Click);
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(208, 6);
            // 
            // addEDFDeviceToolStripMenuItem
            // 
            this.addEDFDeviceToolStripMenuItem.Name = "addEDFDeviceToolStripMenuItem";
            this.addEDFDeviceToolStripMenuItem.Size = new System.Drawing.Size(211, 38);
            this.addEDFDeviceToolStripMenuItem.Text = "Add EDF Device";
            // 
            // openFileDialogDat
            // 
            this.openFileDialogDat.FileName = "openZipFileDialog";
            this.openFileDialogDat.Filter = "MIT DAT|*.dat|All|*.*";
            this.openFileDialogDat.Multiselect = true;
            this.openFileDialogDat.Title = "Select MIT DAT File";
            // 
            // openFileDialogDVX
            // 
            this.openFileDialogDVX.FileName = "openZipFileDialog";
            this.openFileDialogDVX.Filter = "DVX DAT|*.dat|All|*.*";
            this.openFileDialogDVX.Multiselect = true;
            this.openFileDialogDVX.Title = "Select DVX File";
            // 
            // FormIReader
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1344, 653);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.toolStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormIReader";
            this.Text = "45";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormIReader_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormIReader_FormClosed);
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panelExtra.ResumeLayout(false);
            this.panelExtra.PerformLayout();
            this.panelMail.ResumeLayout(false);
            this.panelMail.PerformLayout();
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFull)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxEvent)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.toolStripMenuExtra.ResumeLayout(false);
            this.toolStripMenuExtra.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.contextMenuHelp.ResumeLayout(false);
            this.contextMenuImport.ResumeLayout(false);
            this.contextMenuAddDevice.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.OpenFileDialog openZipFileDialog;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.TextBox logBox;
        private System.Windows.Forms.PictureBox pictureBoxFull;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label labelImageFull;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBoxEvent;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelImageEvent;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.OpenFileDialog openHeaFileDialog;
        private System.Windows.Forms.OpenFileDialog openScpFileDialog;
        private System.Windows.Forms.ComboBox comboBoxSignal;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panelExtra;
        private System.Windows.Forms.Label labelUtcTime;
        private System.Windows.Forms.Label labelReadImgUtc;
        private System.Windows.Forms.OpenFileDialog openTZeFileDialog;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripButton toolStripRun;
        private System.Windows.Forms.ToolStripLabel toolStripLabelTotal;
        private System.Windows.Forms.ToolStripLabel toolStripTotalRead;
        private System.Windows.Forms.ToolStripLabel toolStripLabelError;
        private System.Windows.Forms.ToolStripLabel toolStripErrorCount;
        private System.Windows.Forms.ToolStripLabel toolStripLabelSkipped;
        private System.Windows.Forms.ToolStripLabel toolStripSkipped;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripProcessTotal;
        private System.Windows.Forms.ToolStripLabel toolStripLabelDivide;
        private System.Windows.Forms.ToolStripLabel toolStripProcessIndex;
        private System.Windows.Forms.ToolStripLabel toolStripProcessLabel;
        private System.Windows.Forms.ToolStripButton toolStripStop;
        private System.Windows.Forms.ToolStripButton toolStripAutoZip;
        private System.Windows.Forms.ToolStripButton toolStripAutoTZe;
        private System.Windows.Forms.ToolStripButton toolStripAutoDV2;
        private System.Windows.Forms.OpenFileDialog openDV2ScpFileDialog;
        private System.Windows.Forms.ToolStripButton toolStripAddTZ;
        private System.Windows.Forms.ToolStripButton toolStripAutoHSir;
        private System.Windows.Forms.Panel panelMail;
        private System.Windows.Forms.ToolStripButton toolStripButtonHelp;
        private System.Windows.Forms.ContextMenuStrip contextMenuHelp;
        private System.Windows.Forms.ToolStripMenuItem openManualToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openChangeHistoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openLicenseInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openDownloadLinksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tMIHelpdeskToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openStandardTeamviewerToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripAddSirona;
        private System.Windows.Forms.ToolStripMenuItem extraLoggingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createSQLEncryptedPasswordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createSmtpEncryptedPasswordToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgIndex;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgFile;
        private System.Windows.Forms.DataGridViewTextBoxColumn NrSignals;
        private System.Windows.Forms.DataGridViewTextBoxColumn Strip;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgReadDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeviceID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgEventTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgEventType;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgState;
        private System.Windows.Forms.DataGridViewTextBoxColumn PatientID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PatientBirthDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgConverter;
        private System.Windows.Forms.DataGridViewTextBoxColumn Method;
        private System.Windows.Forms.DataGridViewTextBoxColumn gdStoredAt;
        private System.Windows.Forms.Button buttonLogToClipboard;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.CheckBox checkBoxMailEnable;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button buttonSendMail;
        private System.Windows.Forms.Timer timerProcessMail;
        private System.Windows.Forms.Button buttonKillSmtpCue;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label labelMailState2;
        private System.Windows.Forms.Label labelMailState1;
        private System.Windows.Forms.ToolStripButton toolStripButtonTzRB;
        private System.Windows.Forms.ToolStripButton toolStripButtonBL;
        private System.Windows.Forms.ToolStripButton toolStripAddDevice;
        private System.Windows.Forms.ToolStripButton toolStripImport;
        private System.Windows.Forms.ContextMenuStrip contextMenuImport;
        private System.Windows.Forms.ToolStripMenuItem importSironaZipToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importSironaHeaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importTZEventToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importTZScpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importDV2ScpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importEdfToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuAddDevice;
        private System.Windows.Forms.ToolStripMenuItem addSironaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addTZDeviceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addDV2DeviceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addDVX1DeviceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addEDFDeviceToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialogDat;
        private System.Windows.Forms.ToolStripButton toolStripButtonTzSkip;
        private System.Windows.Forms.ToolStripMenuItem listSironaLicenceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listTZLicenseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reloadDeviceLicensesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addLogLineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newLogFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem checkDBCollectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logActiveLicinseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sironaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tZToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dV2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testFilesDurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createTestFilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem countTestFilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem readTestFilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteTestFilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recordingCreateTestFilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recordingCountTestFilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recordingTestFilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recordingDeleteTestFilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem folderCreateTestFilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem folderCountTestFilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem folderReadTestFilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem folderDeleteTestFilesToolStripMenuItem;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialogTest;
        private System.Windows.Forms.ToolStripMenuItem programInfoToClipboardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openLogFolderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openLogFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openRecodingFolderToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButtonWarn;
        private System.Windows.Forms.ToolStripButton toolStripTzAutoCollect;
        private System.Windows.Forms.ToolStripMenuItem testParseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem parseDoubleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem parseExactDateTimeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem progParseDoubleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem progParseDateTimeZoneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enableLogFlushToolStripMenuItem;
        private System.Windows.Forms.Label labelListInfo;
        private System.Windows.Forms.Label labelUpdTimer;
        private System.Windows.Forms.Label labelTzQueue;
        private System.Windows.Forms.Label labelTzHrFilter;
        private System.Windows.Forms.Label labelAmplRange;
        private System.Windows.Forms.ToolStripMenuItem makeLauncherVersionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem programInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openContentFolderToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.Label labelSirQueue;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label labelMemUsage;
        private System.Windows.Forms.ToolStripButton toolStripButtonSirAC;
        private System.Windows.Forms.ToolStripButton toolStripButtonSirHolter;
        private System.Windows.Forms.ToolStripMenuItem listDV2LicenseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageUsersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rewriteUserListToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem makeUserPinToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.Label labelProgressInfo;
        private System.Windows.Forms.ToolStripMenuItem dVXTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reverseBytesDatToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importDVXHeaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importDVX1DatToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importDVX1EvtToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importDVX1EvtRecToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripAutoDVX;
        private System.Windows.Forms.ToolStripMenuItem dVXToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialogDVX;
        private System.Windows.Forms.ToolStripMenuItem createDVXDefaultsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        private System.Windows.Forms.ToolStripMenuItem listDVXLicenseToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ToolStripMenuItem reloadDVXLicenseToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator15;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator16;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator17;
        private System.Windows.Forms.ToolStripMenuItem listDatInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator18;
        private System.Windows.Forms.ToolStripMenuItem dupRecordToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButtonSkipDVX;
        private System.Windows.Forms.ToolStripMenuItem preAnalysisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cardioLogsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem heaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem scpToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButtonPaSir;
        private System.Windows.Forms.ToolStripButton toolStripButtonPaTZ;
        private System.Windows.Forms.ToolStripButton toolStripButtonPaDV2;
        private System.Windows.Forms.ToolStripButton toolStripButtonPaDVX;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.ToolStrip toolStripMenuExtra;
        private System.Windows.Forms.ToolStripButton toolStripOpenFolder;
        private System.Windows.Forms.ToolStripButton toolStripOpenEvent;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton LoadTableFromSql;
        private System.Windows.Forms.ToolStripTextBox toolStripNrHours;
        private System.Windows.Forms.ToolStripLabel toolStripLabelHours;
        private System.Windows.Forms.ToolStripComboBox toolStripDropDownSearch;
        private System.Windows.Forms.ToolStripTextBox toolStripEditSearch;
        private System.Windows.Forms.ToolStripButton toolStripEqualsCursor;
        private System.Windows.Forms.ToolStripButton toolStripTest;
        private System.Windows.Forms.ToolStripButton toolStripLast;
        private System.Windows.Forms.ToolStripButton toolStripButtonFixEnd;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem importNoninToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importDVXSpO2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem convertDVXSpO2ToolStripMenuItem;
    }
}

