﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using i_Reader.Properties;
using System.Threading;
using System.IO;
using System.Diagnostics;
using System.IO.Compression;
using Ionic.Zip;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
//using System.Data;
//using System.Data.SqlClient;
using System.Security;
using MySql.Data.MySqlClient;
using Program_Base;
using Event_Base;
using EventBoard.EntryForms;
using EventboardEntryForms;
using i_Reader.Event_Base;
using EventBoard.Event_Base;
using System.Net.Mail;
using System.Globalization;
using DynaVisionGraph.Filter;

namespace i_Reader
{
    enum DAutoRun { Disabled, Off, On }
    enum DProcessStep { Off, Entry, Start, Loop, End, Exit, Done }

    enum DDatagridColumn { RecNr, File, NrSignals, StripLen, Received, DeviceID, EventTime, EventType, State, PatID, PatDOB, Converter, Import, StoredAt, NR_Columns }

    public partial class FormIReader : Form
    {
        const Int32 _cLastActivityWriteSec = 120;

        bool mbInitialized = true;

        //beta version in program.cs + version (major.minor) in AssemblyInfo.cs 
        // Launch version.json defaults:

        public string versionLabel = "Fix Device Model Import Sirona dB2019March25";
        public string versionHeader = "Fix Device Model and Import Sirona !req. dB2019March25!";
        public string[] versionText = { "This version requires data base and ireader update dB2019March25!",
            "Fix corrupt values in Sirona json Strip Length",
            "Device selected model stored in Record"
        };

        public class CTzSkipLastFileNr
        {
            public string _mDeviceID;
            public UInt32 _mLastFileNr;

            public CTzSkipLastFileNr()
            {
                _mDeviceID = "";
                _mLastFileNr = 0;
            }
        };
        public class CDvxSkipLastFile
        {
            public UInt32 _mDeviceNR;
            public string _mFileName;
            public DateTime _mUtc;
            public string _mEventType;

            public CDvxSkipLastFile()
            {
                _mDeviceNR = 0;
                _mFileName = "";
                _mUtc = DateTime.MinValue;
                _mEventType = "";   // to lower and without number
            }
            public void mSetEvent(CDvxEvtRec ADvxEvtRec)
            {

                if ( ADvxEvtRec != null )
                {
                    string newEventType = CDvxEvtRec.sRemoveNrEvents(ADvxEvtRec._mEventType);
                    if (newEventType != null && newEventType.Length > 0)
                    {
                        newEventType = newEventType.ToLower();
                    }

                    _mEventType = newEventType;
                    _mUtc = ADvxEvtRec._mEventUTC;
                    _mFileName = ADvxEvtRec._mFileName;
                }
            }
        };

        const UInt32 _cActionEmptySize = 36;

        private DateTime _mLogFileDT;

        private const char _cCharOff = '0';
        private const char _cCharEntry = '»';
        private const char _cCharExit = '«';
        private const char _cCharStart = '+';
        private const char _cCharEnd = '-';
        private const char _cCharDone = '√';
        private const string _cCharProc = "●□₀◊";

        private Color _cColorWait = Color.WhiteSmoke;
        private Color _cColorDisabled = Color.Gray;
        private Color _cColorRunScan = Color.LightGreen;
        private Color _cColorRunFile = Color.LightBlue;
        private Color _cColorUpdate = Color.LightYellow;
        private Color _cColorError = Color.Red;


        // from Aplication Settings:
        private String _mZipFromDir;
        private String _mHeaFromDir;
        private String _mScpFromDir;
        private string _mTzFromDir;
        private String _mDV2FromDir;
        private String _mDVXImportDir;
        private String _mDVXStudyDir;

        private string _mHttpSirFromDir;
        private string _mHttpSirCommandDir;
        private string _mHttpSirStatusDir;
        private String _mRecordingDir;
        private bool _mbMoveZip;
        private bool _mbWipeUnziped;
        private bool _mbWipeJson;
        private bool _mbHttpWipeJson;
        private String _mRecStartName;
        private int _mCheckIntervalSec;
        private int _mMinFileAgeSec;
        private int _mMaxFileAgeHour;
        private int _mMinDvxFileAgeSec;
        private int _mWaitDelRunTimeMin = 60;
        private bool _mbRunAtStartup;
        private bool _mbDeleteZipOnOk;
        private string _mHeaViewExe;
        private string _mScpViewExe;

        static bool _sbWarnTZeOff = true;
        static bool _sbWarnDV2Off = true;

        private static bool _mbLockImportDir = true;
        private static StreamWriter _mZipLockFile = null;
        private static StreamWriter _mTzLockFile = null;
        private static StreamWriter _mDV2LockFile = null;
        private static StreamWriter _mDVXLockFile = null;
        private static StreamWriter _mHttpSirLockFile = null;

        private int _mPrePlotFullWidth;
        private int _mPrePlotFullHeight;
        private int _mPrePlotEventWidth;
        private int _mPrePlotEventHeight;
        private float _mPrePlotEventBeforeSec;
        private float _mPrePlotEventAfterSec;
        private float _mPrePlotEventUnitSec;
        private float _mPrePlotFullUnitSec;

        private bool _sbShowImported = false;

        private string _mZipPw;

        // program state

        private uint _mTotalCount = 0;
        private uint _mErrorCount = 0;
        private uint _mSkippedCount = 0;
        private bool _mbRun;             // run scan on timer active
        private DateTime _mLastRunTime;
        private DateTime _mStartRunTimeDT;
        private DateTime _mLastUtcUpdate;
        private DateTime _mLastDriveTest = DateTime.MinValue;

        private UInt32 _mMaxNrRowsListed = 50000;
        static private bool _sbFloatHasDot = true;


        private bool _mbRunLoop = false;     // running in process convertion list 
        private List<string> _mFileRunList = null;
        private int _mRunIndex = 0;
        private int _mRunN = 0;
        private DateTime _mRunScanTime;
        public bool _mbRunningConversion = false;    // running conversion of currentFile
        private String _mRunFilePath;    // private to get / set RunFile functions

        private UInt16 _mAutoRunSirZip = 0;
        private UInt16 _mAutoRunTZe = 0;
        private UInt16 _mAutoRunDV2 = 0;
        private UInt16 _mAutoRunDVX = 0;
        private UInt16 _mAutoRunHttpSir = 0;
        private float _mDisableErrorZipHour = 0.0F;

        //        private MySqlConnection mSqlConnection;
        private CEncryptedString _mSqlServer = null;
        private CEncryptedInt _mSqlPort = null;
        private CEncryptedString _mSqlDbName = null;
        private CEncryptedString _mSqlUser = null;
        //        private CEncryptedString _mSqlPassword = null;
        private CPasswordString _mSqlPassword = null;
        private DSqlSslMode _mSqlSslMode = DSqlSslMode.Default;

        string _mTestSqlUser = "", _mTestSqlServer = "", _mTestSmtpUser = "", _mTestStmpServer = "";    // used for encrypt password

        private CSqlDBaseConnection _mDBaseServer;

        private DateTime _mLastActivityWriteDT;
        private DateTime _mLastActivityLogDT;

        private DateTime _mLastScanDtTz = DateTime.MinValue;
        private DateTime _mLastScanDtSir = DateTime.MinValue;
        private DateTime _mLastScanDtDV2 = DateTime.MinValue;
        private DateTime _mLastScanDtDVX = DateTime.MinValue;
        private DateTime _mLastScanDtHttpSir = DateTime.MinValue;

        private DateTime _mStartScanDtTz = DateTime.MinValue;
        private DateTime _mStartScanDtSir = DateTime.MinValue;
        private DateTime _mStartScanDtDV2 = DateTime.MinValue;
        private DateTime _mStartScanDtDVX = DateTime.MinValue;
        private DateTime _mStartScanDtHttpSir = DateTime.MinValue;

        private DateTime _mLastAutoCollectDtSir = DateTime.MinValue;
        private bool _mbSirAutoCollectActive = false;
        private bool _mbSirAutoCollectHolterEvent = true;
        private bool _mbSirAutoCollectHolterMerge = true;

        private DateTime _mLastAutoCollectDtTz = DateTime.MinValue;
        private bool _mbTzAutoCollectActive = false;
        private bool _mbTzAutoCollectReqEvent = true;

        private UInt16 _mTzAutoColScanMin = 15; // repeat scan for missing files every x minutes 

        private float _mTzActionDeadHours = 4.0F;

        private bool mbLogUnits = true;
        private int _mWarnScanSec = 2;

        private DateTime _mProcessStartEntryDT = DateTime.MinValue;
        private float _mProcessMaxEntryTimeSec = 3600.0F;

        private DateTime _mProcessStartLoopDT = DateTime.MinValue;
        private float _mProcessMaxLoopTimeSec = 3600.0F;

        private DateTime _mProcessUpdateDT = DateTime.MinValue;
        private float _mProcessUpdateTimeSec = 0.3F;

        private DateTime _mProcessHandleDT = DateTime.MinValue;
        private float _mProcessHandleTimeSec = 0.3F;
        private bool _mbProcessHandleMsgs = true;

        private DProcessStep _mProcessStep = DProcessStep.Off;
        private UInt16 _ProcessCharLoop = 0;
        private Int32 _ProcessLoopIndex = -1;   // 0 ... _ProcessLoopTotal -1
        private Int32 _ProcessLoopTotal = -1;
        private string _mProcessNextLabel;

        private Int32 _ProcessShownIndex = 9999999;
        private Int32 _ProcessShownTotal = 9999999;
        private string _mProcessShownLabel = "xzxzxz";
        private DProcessStep _mProcessShownStep = DProcessStep.End;

        private bool _mbLogExtra = false;
        private bool _mbTimerEnterd = false;

        private UInt32 _mSironaTemplateModel = 0;
        private string _mSironaTemplateFirmware = "";
        private bool _mbSironaTemplateSetAvailable = false;

        private UInt32 _mDvxTemplateModel = 0;
        private string _mDvxTemplateFirmware = "";
        private bool _mbDvxTemplateSetAvailable = false;

        private UInt32 _mDV2TemplateModel = 0;
        private string _mDV2TemplateFirmware = "";
        private bool _mbDV2TemplateSetAvailable = false;

        private UInt32 _mTzTemplateModel = 0;
        private string _mTzTemplateFirmware = "";
        private bool _mbTzTemplateSetAvailable = false;

        public bool _mbMailEventReceivedEnable = false;
        public bool _mbMailEventReceivedPict = true;
        public string _mMailEventReceivedFrom = "";
        public string _mMailEventReceivedTo = "";
        public string _mMailEventReceivedSubject = "";
        public string _mMailEventReceivedBody = "";

        public UInt32 _mMailProcessCounter = 0;
        public char[] _cMailLoopChars = { '-', '/', '|', '\\' };
        public const UInt32 _cMailNrLoopChars = 4;

        List<CTzSkipLastFileNr> _mTzSkipList = null;        // list of TZ device ID with last scp file number

        //        UInt32 _mTzSkipScpFileNr = UInt32.MaxValue; // last file number of the tz scp file
        //        string _mTzSkipSerialNr;
        Int16 _mTzSkipCmpMin = -1;      // TZ skip test values
        Int16 _mTzSkipCmpMax = -1;
        const Int16 _cTzSkipRange = 5;

        UInt16 _mDvxWaitCompleteMin = 30;
        Int16 _mDvxSkipCmpSec = -1; // skip if event is within seconds of previous one
        Int16 _mDvxRepCmpSec = -1;  // repeat event when previous is longer then X sec
        bool _mbDvxSkipSameOnly = true; // skip only if event type is same
        List<CDvxSkipLastFile> _mDvxSkipList = null;


        //        UInt32 _mTzQueueMaxAgeHours = 72;

        private UInt16 _mCheckSystemMin = 30;
        private UInt16 _mCheckMemUsedMB = 800;
        private UInt16 _mCheckMemFreeMB = 200;
        private UInt16 _mCheckDiskLocalGB = 10;
        private UInt16 _mCheckDiskDataGB = 10;
        private string _mCheckServerTzUrl = "";
        private string _mCheckServerSironaUrl = "";

        DateTime _mTimerUpdDT = DateTime.Now;

        public static bool sbTzQuickFilter = false;

        UInt16 _mMainTimerMilSec = 100;
        UInt16 _mPauseTimerMilSec = 1000;
        string _mProgMainTitle;
        string _mProgRunState = "";

        Icon _mIconMain = null;
        Icon _mIconRun = null;
        Icon _mIconScan = null;
        Icon _mIconWait = null;
        Icon _mIconBreak = null;
        Icon _mIconStopped = null;
        NotifyIcon _mNotifyIcon = null;
        string _mNotifyHeader = "";
        string _mNotifyText = "";
        DateTime _mNotifyDT;

        string _mCardioLogsUrl = "";
        string _mCardioLogsToken = "";
        UInt16 _mCardiologsEventMin = 10;
        string _mTzPaMethod = "";
        string _mSironaPaMethod = "";
        string _mDvxPaMethod = "";
        string _mDv2PaMethod = "";

        bool _mbDoPaTZ = false;
        bool _mbDoPaSirona = false;
        bool _mbDoPaDV2 = false;
        bool _mbDoPaDVX = false;

        bool _mbCursorFixEnd = false;


        public FormIReader()
        {
            try
            {
                bool bIsProgrammer = CLicKeyDev.sbDeviceIsProgrammer();
                int i = 0;
                _mLastRunTime = DateTime.Now;
                _mLastActivityWriteDT = _mLastRunTime;
                _mLastActivityLogDT = _mLastRunTime;
                _mStartRunTimeDT = DateTime.MinValue;

                _mLogFileDT = _mLastRunTime;
                _mbRun = false;
                _mbRunningConversion = false;
                _mbRunLoop = false;

                _mbLogExtra = CLicKeyDev.sbDeviceIsProgrammer();//sGetOrgNr() < 30;
                InitializeComponent();
                sCheckFloatHasDot();

                labelProgressInfo.Text = "Init program";
                mLoadIcons();
                mSetRunTimerCheckState("Init Prog");

                toolStripProcessLabel.Text = "Init";

                DateTime startDT = DateTime.Now;
                string startTime = CProgram.sDateTimeToYMDHMS(startDT);
                string startZone = CProgram.sMakeUtcOffsetString(CProgram.sGetLocalTimeZoneOffsetMin());

                string cultureName = Properties.Settings.Default.CultureFormat;
                string dateFormat = Properties.Settings.Default.DateFormat;
                string timeFormat = Properties.Settings.Default.TimeFormat;
                bool bFloatWithDot = Properties.Settings.Default.FloatWithDot;
                bool bImperial = Properties.Settings.Default.ImperialUnits;

                CProgram.sSetShowDateTimeImperialFormat(cultureName, bImperial, dateFormat, timeFormat);
                CProgram.sSetImperial(bImperial);
                CProgram.sSetUserDecimal(bFloatWithDot ? DFloatDecimal.Dot : DFloatDecimal.Comma);
                CProgram.sSetProgTitleFormat("DVTMS", true, CProgram.sGetProgVersionLevel(), DShowOrganisation.First, "");
                _mProgMainTitle = "   < start " + startTime + startZone + " > ";
                //CProgram.sSetProgramTitle("   <start " + startTime + startZone + ">", true);

                String t;
                t = "Program start at " + startDT.ToString() + startZone;
                CProgram.sLogLine(t);

                string zipFromDir = Properties.Settings.Default.FromDir;
                string heaFromDir = Properties.Settings.Default.HeaFromDir;
                string tzFromDir = Properties.Settings.Default.TZeFromDir;
                string scpFromDir = Properties.Settings.Default.ScpFromDir;
                string DV2FromDir = Properties.Settings.Default.DV2FromDir;
                string httpSirFromDir = Properties.Settings.Default.HttpSirFromDir;
                string dvxImportDir = Properties.Settings.Default.DvxImportDir;
                string dvxStudyDir = Properties.Settings.Default.DvxStudyFolder;

                _mRecordingDir = Properties.Settings.Default.RecordingDir;

                string dbName = Properties.Settings.Default.SqlDbName;
                int dbPort = Properties.Settings.Default.SqlPort;
                string dbServer = Properties.Settings.Default.SqlServer;
                int dbSslMode = Properties.Settings.Default.SqlSslMode;
                string dbUser = Properties.Settings.Default.SqlUser;
                string dbPW = Properties.Settings.Default.SqlPassword;
                UInt32 sqlGlobalRowLimit = Properties.Settings.Default.SqlGlobalRowLimit;

                CDvtmsData._sbUserListEnfored = Properties.Settings.Default.UserListEnfored;
                CDvtmsData._sUserPinLength = Properties.Settings.Default.UserPinLength;

                _mAutoRunSirZip = (UInt16)Properties.Settings.Default.autoRunZip;
                _mAutoRunTZe = (UInt16)Properties.Settings.Default.autoRunTZe;
                _mAutoRunDV2 = (UInt16)Properties.Settings.Default.AutoRunDV2;
                _mAutoRunHttpSir = (UInt16)Properties.Settings.Default.AutoRunHttpSir;
                _mAutoRunDVX = (UInt16)Properties.Settings.Default.AutoRunDVX;


                CSironaStateSettings.sSetHolterReqLength(Properties.Settings.Default.SirMaxReqLengthMin, Properties.Settings.Default.SirHolterReqLengthMin);

                bool bTestProgDir = true;

                if (CProgram.sbIsEmptyConfig(dbName) || CProgram.sbIsEmptyConfig(_mRecordingDir) || CProgram.sbIsEmptyConfig(dbUser))
                {
                    if (bIsProgrammer)
                    {
                        string dataDir = "d:\\Data\\";
                        string cntrDir = dataDir;
                        bool bDemo = CProgram.sbAskYesNo("Select Center", "Center = NL0022Demo (cancel = US0001Soft)?");

                        if (bDemo)
                        {
                            // 32199E6E-$4C8DE91A-15BC5A91-6996FE00-0E54403C-36F3B124-50EF40F1-55A3F556-6B547F3C
                            uint nr = 22;
                            cntrDir = "m:\\";
                            dataDir = cntrDir;

                            dbName = "NL" + nr.ToString("0000"); nr = 0x32199E6E; dbPW = nr.ToString("X8") + "-$"; nr = 192; dbServer = nr.ToString() + ".";
                            dbName += "Demo"; nr = 0x4C8DE91A; dbPW += nr.ToString("X8"); nr = 168; dbServer += nr.ToString() + ".";
                            nr = 0x15BC5A91; dbPW += nr.ToString("X8"); nr = 0; dbServer += nr.ToString() + ".";
                            dbPort = 3306; nr = 0x6996FE00; dbPW += nr.ToString("X8"); nr = 36; dbServer += nr.ToString();
                            dbUser = dbName + "01dbUser"; nr = 0x0E54403C; dbPW += nr.ToString("X8");
                            nr = 0x36F3B124; dbPW += nr.ToString("X8"); nr = 0x50EF40F1; dbPW += nr.ToString("X8");
                            nr = 0x55A3F556; dbPW += nr.ToString("X8"); nr = 0x6B547F3C; dbPW += nr.ToString("X8");

                            dvxStudyDir = "m:\\DvxDataSimon\\";

                            CProgram.sbSetProgramOrganisation(dbName, 1);
                            bTestProgDir = false;
                        }
                        else
                        {
                            uint nr = 1;
                            dataDir = "d:\\Data\\";
                            cntrDir = dataDir;
                            dbName = "NL" + nr.ToString("0000"); nr = 0x321CAE66; dbPW = nr.ToString("X8") + "-$"; nr = 192; dbServer = nr.ToString() + ".";
                            dbName += "Soft"; nr = 0xD82D02B5; dbPW += nr.ToString("X8"); nr = 168; dbServer += nr.ToString() + ".";
                            cntrDir += dbName + "\\"; nr = 0xDE9A2E85; dbPW += nr.ToString("X8"); nr = 0; dbServer += nr.ToString() + ".";
                            dbPort = 3306; nr = 0x73B799EE; dbPW += nr.ToString("X8"); nr = 36; dbServer += nr.ToString();
                            dbUser = CProgram.sGetPcUserName(); nr = 0xB1C2FD57; dbPW += nr.ToString("X8");
                            dvxStudyDir = cntrDir;
                        }
                        if (sqlGlobalRowLimit > 1)
                        {
                            CSqlDBaseConnection.sSetGlobalSqlRowLimit(sqlGlobalRowLimit);
                        }

                        dbSslMode = (int)DSqlSslMode.REQUIRED;
                        _mRecordingDir = cntrDir + "Recording\\";
                        zipFromDir = cntrDir + "FTPIntricon";
                        heaFromDir = dataDir + "";
                        tzFromDir = cntrDir + "HttpTZ\\";
                        scpFromDir = dataDir + "ScpTz\\";
                        DV2FromDir = cntrDir + "DV2\\";
                        httpSirFromDir = cntrDir + "HttpSir\\Uploads\\";
                        //                        dvxStudyDir = cntrDir;  // starts in data direcory without study folder
                        CProgram.sLogLine("Using Programmer defaults for recording and dBase " + dbName);

                        UInt32 holterMin = CSironaStateSettings.sGetHolterReqLengthSec() / 60;
                        if (holterMin > 10)
                        {
                            CSironaStateSettings.sSetHolterReqLength(15, 15);
                            CSirAutoCollect._sReqAgeMin = 10;     // Only request for files that are at least x minutes old
                            CSirAutoCollect._sScanTimeMin = 5;      // wait to do a scan after last scan
                            CSirAutoCollect._sMaxActionsInQueue = 2;
                        }
                    }
                    else
                    {
                        CProgram.sPromptError(true, "Missing configuration file!!", "Setup Configuration File!!");
                        Close();
                        return;
                    }
                }
                CProgram.sSetProgramMainForm(this, _mProgMainTitle);

                _mRecordingDir = mCheckPath(_mRecordingDir);

                _mZipFromDir = mCheckPath(zipFromDir);
                _mHeaFromDir = mCheckPath(heaFromDir);
                _mTzFromDir = mCheckPath(tzFromDir);
                _mScpFromDir = mCheckPath(scpFromDir);
                _mDV2FromDir = mCheckPath(DV2FromDir);
                _mHttpSirFromDir = httpSirFromDir;
                _mDVXStudyDir = mCheckPath(dvxStudyDir);
                _mDVXImportDir = dvxImportDir;
                CDvxEvtRec.sbSetDvxStudyFolder(_mDVXStudyDir, _mAutoRunDVX > 0);  // load alarm types from Enums\e_DvxAlarmType.txt

                //            string dbName = Properties.Settings.Default.SqlDbName2;
                //            CProgram.sLogLine("!SqlDbName2!=" + dbName + " zipFrom= " + mZipFromDir );
                _mSqlDbName = new CEncryptedString("SqlDbName", DEncryptLevel.L1_Program, 32);
                if (_mSqlDbName != null && _mSqlDbName.mbSetEncrypted(dbName))
                {
                    Properties.Settings.Default.SqlDbName = _mSqlDbName.mGetEncrypted(); // store after incription has changed
                }

                string centerName = _mSqlDbName.mDecrypt();

                // not allowed, comes from lic key!!!            CProgram.sbSetProgramOrganisation(centerName, CProgram.sGetDeviceID());
                //     CProgram.sSetProgramMainForm(this, "");

                CProgram.sLogLine("Center DB name:  " + centerName);
                string centerFileName = centerName + ".CenterName";
                string centerFile = Path.Combine(_mRecordingDir, centerFileName);
                // check if directory points to the correct center
                if (false == File.Exists(centerFile))
                {
                    if (false == File.Exists(Path.Combine(_mRecordingDir, "..\\" + centerFileName)))
                    {
                        string fullPath = Path.GetFullPath(_mRecordingDir);
                        string drive = fullPath.Substring(0, 2).ToUpper();
                        CProgram.sLogError("Center=" + centerName + ", recording dir = " + _mRecordingDir);
                        //                        CProgram.sPromptError(true, "Config error!", "Center name not set correctly in recording dir, check configuration!! ");
                        CProgram.sPromptError(true, "Check drive " + drive + " and configuration!!", "Center file " + centerFile + " not found!");
                        _mSqlDbName.mbEncrypt("----");
                        Close();
                        return;
                    }
                }
                if (false == File.Exists(Path.Combine(_mZipFromDir, centerFileName)))
                {
                    if (false == File.Exists(Path.Combine(_mZipFromDir, "..\\" + centerFileName)))
                    {
                        CProgram.sLogError("Center=" + centerName + ", zip from dir = " + _mZipFromDir);
                        CProgram.sPromptError(true, "Config error!", "Center name not set correctly in zip from dir, check configuration!! ");
                        _mSqlDbName.mbEncrypt("----");
                        Close();
                        return;
                    }
                }
                //Text = CProgram.sGetProgTitle() + " " + centerName; // set title
                string progDir = CProgram.sGetProgDir();
                CProgram.sLogLine("progDir=" + progDir);
                // check center name in program dir
                // (skip test for NL0022Demo
                if (bTestProgDir
                    && false == File.Exists(Path.Combine(progDir, "..\\..\\" + centerFileName)))
                {
                    if (false == File.Exists(Path.Combine(progDir, "..\\" + centerFileName)))
                    {
                        CProgram.sLogError("Center=" + centerName + ", program dir = " + progDir);
                        CProgram.sPromptError(true, "Config error!", "Center name not set correctly in program dir, check configuration!! ");
                        _mSqlDbName.mbEncrypt("----");
                        Close();

                        return;
                    }
                }
                string orgLabel = CProgram.sGetOrganisationLabel();
                if (centerName.ToUpper() != orgLabel.ToUpper())
                {
                    CProgram.sLogError("Center= " + centerName + ", Org = " + orgLabel);
                    CProgram.sPromptError(true, "Config error!", "Center name not equal to database Label, check configuration!! ");
                    _mSqlDbName.mbEncrypt("----");
                    Close();
                    return;
                }
                CProgram.sLogLine("orgLabel=" + orgLabel);

                _mSqlServer = new CEncryptedString("SqlServer", DEncryptLevel.L1_Program, 64);
                if (_mSqlServer != null && _mSqlServer.mbSetEncrypted(dbServer))
                {
                    Properties.Settings.Default.SqlServer = _mSqlServer.mGetEncrypted(); // store after incription has changed
                }
                _mSqlPort = new CEncryptedInt("SqlPort", DEncryptLevel.L1_Program);
                if (_mSqlPort != null && _mSqlPort.mbSetEncrypted(dbPort))
                {
                    Properties.Settings.Default.SqlPort = _mSqlPort.mGetEncryptedInt(); // store after incription has changed
                }
                _mSqlSslMode = dbSslMode <= 0 ? DSqlSslMode.Default : (dbSslMode > (int)DSqlSslMode.VERIFY_IDENTITY ? DSqlSslMode.VERIFY_IDENTITY : (DSqlSslMode)dbSslMode);

                _mSqlUser = new CEncryptedString("SqlUser", DEncryptLevel.L1_Program, 32);
                if (_mSqlUser != null && _mSqlUser.mbSetEncrypted(dbUser))
                {
                    Properties.Settings.Default.SqlUser = _mSqlUser.mGetEncrypted(); // store after incription has changed
                }
                /*                _mSqlPassword = new CEncryptedString("SqlPassword", DEncryptLevel.L1_Program, 32);
                                if (_mSqlPassword != null && _mSqlPassword.mbSetEncrypted(Properties.Settings.Default.SqlPassword))
                                {
                                    Properties.Settings.Default.SqlPassword = _mSqlPassword.mGetEncrypted(); // store after incription has changed
                                }
                */
                _mSqlPassword = new CPasswordString(_mSqlUser.mDecrypt() + "@" + _mSqlServer.mDecrypt() + ":sqlPW", orgLabel);

                if (_mSqlPassword != null)
                {
                    _mSqlPassword.mbSetEncrypted(dbPW);

                    if (false == _mSqlPassword.mbIsEncrypted())
                    {
                        if (CProgram.sbAskYesNo("SqlPassword is not Encrypted!", "Change to encrypted password! Continue anyway?"))
                        {
                            _mSqlPassword.mbEncrypt(_mSqlPassword.mGetEncrypted());
                        }
                        else
                        {
                            CProgram.sLogError("Password for dBase is not encrypted, close program");
                            Close();
                            return;
                        }
                    }
                }
                //                CProgram.sLogLine("sqlServer=" + _mSqlServer.mDecrypt() + ", ssl mode= " + _mSqlSslMode.ToString());

                _mbMoveZip = Properties.Settings.Default.MoveZip;
                _mbWipeUnziped = Properties.Settings.Default.WipeUnziped;
                _mbWipeJson = Properties.Settings.Default.WipeJson;
                _mbHttpWipeJson = Properties.Settings.Default.HttpWipeJson;
                _mRecStartName = Properties.Settings.Default.RecStartName;
                _mCheckIntervalSec = Properties.Settings.Default.CheckIntervalSec;
                _mMinDvxFileAgeSec = Properties.Settings.Default.MinDvxFileAgeSec;

                if (_mCheckIntervalSec < 30 && bIsProgrammer == false) _mCheckIntervalSec = 30;
                _mMinFileAgeSec = (bIsProgrammer ? 20 : 45) + _mCheckIntervalSec / 2;

                _mMaxFileAgeHour = Properties.Settings.Default.WipeFileAfterHours;
                int minHours = bIsProgrammer ? 1 : 6;
                if (_mMaxFileAgeHour < 1)
                {
                    _mMaxFileAgeHour = 99 * 365 * 24;   // 99 years effectively off
                }
                if (_mMaxFileAgeHour < minHours)
                {
                    _mMaxFileAgeHour = minHours;
                }

                CSirAutoCollect._sScanTimeMin = Properties.Settings.Default.SirAcScanMin;
                _mbSirAutoCollectHolterEvent = Properties.Settings.Default.SirAcHolterEvent;
                _mbSirAutoCollectActive = Properties.Settings.Default.SirAcActive;
                CSirAutoCollect._sMaxActionsInQueue = Properties.Settings.Default.SirAcMaxQueue;

                mUpdateSirAutoCollect();
                mUpdateSirHE();

                CTZeReader._sbEventBulkUpload = Properties.Settings.Default.TzEventBulkUpload;
                CTZeReader._sbEventRequestScp = Properties.Settings.Default.TzEventRequestScp;

                mUpdateTzRB();

                _mTzSkipCmpMin = Properties.Settings.Default.TzSkipScpCmpMin;
                _mTzSkipCmpMax = Properties.Settings.Default.TzSkipScpCmpMax;

                mUpdateTzSkip();
                if (_mTzSkipCmpMin >= 0)
                {
                    CProgram.sLogLine("TZ skip min=" + _mTzSkipCmpMin.ToString() + ", max=" + _mTzSkipCmpMax.ToString());
                }

                _mDvxWaitCompleteMin = Properties.Settings.Default.DvxWaitCompleteMin;
                _mDvxSkipCmpSec = Properties.Settings.Default.DvxSkipCmpSec;
                _mDvxRepCmpSec = Properties.Settings.Default.DvxSkipCmpSec;
                _mbDvxSkipSameOnly = Properties.Settings.Default.DvxSkipSameOnly;

                if (_mDvxSkipCmpSec >= 0 || _mDvxRepCmpSec > 0)
                {
                    CProgram.sLogLine("DVX skip=" + (_mbDvxSkipSameOnly ? " same " : " All " )+  _mDvxSkipCmpSec.ToString() + " sec, rep=" + _mDvxRepCmpSec.ToString() + " sec");
                }
                mUpdateDvxSkip();

                _mTzAutoColScanMin = Properties.Settings.Default.TzAcScanMin;
                _mbTzAutoCollectReqEvent = Properties.Settings.Default.TzAcReqEvent;
                _mbTzAutoCollectActive = Properties.Settings.Default.TzAcActive;

                CTzAutoCollect._sMinFileSize = Properties.Settings.Default.TcAcMinFileSize;        // only add files that are a minimum size
                CTzAutoCollect._sReqAgeMin = Properties.Settings.Default.TcAcReqAgeMinMin;   // Only request for files that are at least x minutes old
                CTzAutoCollect._sScanTimeMin = (UInt32)(_mTzAutoColScanMin / 2 + 1);
                CTzAutoCollect._sMaxTryRequest = Properties.Settings.Default.TzAcMaxTry;
                if (CTzAutoCollect._sMaxTryRequest < 1) CTzAutoCollect._sMaxTryRequest = 1;       // maximum number of request for a file
                CTzAutoCollect._sWaitTryRequestMin = Properties.Settings.Default.TzAcWaitTryMin;
                if (CTzAutoCollect._sWaitTryRequestMin < 1) CTzAutoCollect._sWaitTryRequestMin = 1; // wait time before requesting the same file again

                CTzAutoCollect._sActionMaxNrReq = Properties.Settings.Default.TzAcActionMaxReq;
                if (CTzAutoCollect._sActionMaxNrReq < 1) CTzAutoCollect._sActionMaxNrReq = 1;                  // max nr File request in a action
                CTzAutoCollect._sActionMaxNrInBlock = Properties.Settings.Default.TzAcActionMaxInBlock;
                if (CTzAutoCollect._sActionMaxNrInBlock < 1) CTzAutoCollect._sActionMaxNrInBlock = 1;
                else if (CTzAutoCollect._sActionMaxNrInBlock > 250) CTzAutoCollect._sActionMaxNrInBlock = 250;// must be <= 250 _ 

                CTzAutoCollect._sActionMaxNrOfBlocks = Properties.Settings.Default.TzAcActionMaxBlocks;  // must be <= 250 _ 
                if (CTzAutoCollect._sActionMaxNrOfBlocks < 1) CTzAutoCollect._sActionMaxNrOfBlocks = 1;
                else if (CTzAutoCollect._sActionMaxNrOfBlocks > 250) CTzAutoCollect._sActionMaxNrOfBlocks = 250;// must be <= 250 _ 
                CTzAutoCollect._sMaxActionsInQueue = Properties.Settings.Default.TzAcActionMaxQueue;
                if (CTzAutoCollect._sMaxActionsInQueue < 1) CTzAutoCollect._sMaxActionsInQueue = 1;   // do not add action if queue is full occupied 

                float tzActionDeadHours = Properties.Settings.Default.TzActioDeadHours;

                if (tzActionDeadHours > 0.1)
                {
                    _mTzActionDeadHours = tzActionDeadHours;
                }

                mUpdateTzAutoCollect();

                _mbRunAtStartup = Properties.Settings.Default.RunAtStartup;
                _mbDeleteZipOnOk = Properties.Settings.Default.DeleteZipOnOk;
                _mHeaViewExe = Properties.Settings.Default.HeaViewExe;
                _mScpViewExe = Properties.Settings.Default.ScpViewExe;
                _mZipPw = Properties.Settings.Default.ZipPw;

                _mPrePlotFullWidth = Properties.Settings.Default.PrePlotFullWidth;
                _mPrePlotFullHeight = Properties.Settings.Default.PrePlotFullHeight;
                _mPrePlotEventWidth = Properties.Settings.Default.PrePlotEventWidth;
                _mPrePlotEventHeight = Properties.Settings.Default.PrePlotEventHeight;
                _mPrePlotEventBeforeSec = Properties.Settings.Default.PrePlotEventBeforeSec;
                _mPrePlotEventAfterSec = Properties.Settings.Default.PrePlotEventAfterSec;
                _mPrePlotEventUnitSec = Properties.Settings.Default.PrePlotEventUnitSec;
                _mPrePlotFullUnitSec = Properties.Settings.Default.PrePlotFullUnitSec;


                _mbLockImportDir = Properties.Settings.Default.LockImportDir;
                _mDisableErrorZipHour = Properties.Settings.Default.disableErrorZipHour;

                UInt16 checkMemUsedMB = Properties.Settings.Default.CheckMemUsedMB;
                UInt16 checkMemFreeMB = Properties.Settings.Default.CheckMemFreeMB;
                UInt16 checkMemAvailMB = Properties.Settings.Default.CheckMemAvailMB;
                UInt16 maxWinProgMB = Properties.Settings.Default.MaxWinProgMB;
                UInt16 minWinFreeMB = Properties.Settings.Default.MinWinFreeMB;
                _mCheckDiskLocalGB = Properties.Settings.Default.CheckDiskDataGB;
                _mCheckDiskDataGB = Properties.Settings.Default.CheckDiskDataGB;
                _mCheckServerTzUrl = Properties.Settings.Default.CheckServerTzUrl;
                _mCheckServerSironaUrl = Properties.Settings.Default.CheckServerSironaUrl;

                CProgram.sSetProgMemLimits(checkMemUsedMB, checkMemFreeMB, checkMemAvailMB, maxWinProgMB, minWinFreeMB);

                toolStripAutoZip.Visible = _mAutoRunSirZip > 0;
                toolStripAutoHSir.Visible = _mAutoRunHttpSir > 0;
                toolStripButtonSirAC.Visible = _mAutoRunHttpSir > 0;
                toolStripButtonSirHolter.Visible = _mAutoRunHttpSir > 0;
                // later                toolStripButtonPaSir.Visible = _mAutoRunSirZip > 0 || _mAutoRunHttpSir > 0;

                toolStripAutoTZe.Visible = _mAutoRunTZe > 0;
                toolStripButtonTzRB.Visible = _mAutoRunTZe > 0;
                toolStripButtonTzSkip.Visible = _mAutoRunTZe > 0;
                toolStripTzAutoCollect.Visible = _mAutoRunTZe > 0;
                // later                toolStripButtonPaTZ.Visible = _mAutoRunTZe > 0;

                toolStripAutoDV2.Visible = _mAutoRunDV2 > 0;
                // later                toolStripButtonPaDV2.Visible = _mAutoRunDV2 > 0;

                toolStripAutoDVX.Visible = _mAutoRunDVX > 0;
                toolStripButtonSkipDVX.Visible = _mAutoRunDVX > 0;
// later                toolStripButtonPaDVX.Visible = _mAutoRunDVX > 0;

                mUpdateAutoRunFlags();

                CProgram.sSetProgLogScreen(logBox, 100000, 0);
                int nrHours = Properties.Settings.Default.readNrHours;
                if (nrHours < 0) nrHours = 0;
                toolStripNrHours.Text = nrHours.ToString();

                CPatientInfo.sSetPatientSocID(Properties.Settings.Default.UsePatientSocID, Properties.Settings.Default.PatientIDText, Properties.Settings.Default.SocSecText);

                mbLogImportDirExist("Sir ZipFromDir", ref _mZipFromDir, _mAutoRunSirZip);
                mbLogImportDirExist("TzeFromDir", ref _mTzFromDir, _mAutoRunTZe);
                mbLogImportDirExist("DV2FromDir", ref _mDV2FromDir, _mAutoRunDV2);
                mbLogImportDirExist("DvxStudyDir", ref dvxStudyDir, _mAutoRunDVX);
                if (mbLogImportDirExist("HSirFromDir", ref _mHttpSirFromDir, _mAutoRunHttpSir))
                {
                    string sirDir = Path.GetFullPath(Path.Combine(_mHttpSirFromDir, ".."));
                    _mHttpSirCommandDir = Path.Combine(sirDir, "command");
                    if (false == Directory.Exists(_mHttpSirCommandDir)) CProgram.sLogError("Sirona command dir missing!! " + _mHttpSirCommandDir);
                    _mHttpSirStatusDir = Path.Combine(sirDir, "status");
                    if (false == Directory.Exists(_mHttpSirStatusDir)) CProgram.sLogError("Sirona status dir missing!!" + _mHttpSirStatusDir);
                }

                bool bRecording = Directory.Exists(_mRecordingDir);
                CProgram.sLogLine("RecordingDir = " + _mRecordingDir + (bRecording ? "" : " Missing!"));
                t = "moveZip = " + _mbMoveZip.ToString() + ", WipeUnziped = " + _mbWipeUnziped.ToString() + ", t = " + _mRecStartName
                    + ", interval = " + _mCheckIntervalSec.ToString();
                CProgram.sLogLine(t);
                CProgram.sLogLine("CheckInterval=" + _mCheckIntervalSec.ToString() + ", minFileAge= " + _mMinFileAgeSec.ToString() + "Sec" + (bIsProgrammer ? " programmer mode" : ""));

                mUpdateBlackList();

                CTzDeviceSettings._sbUseQueue = Properties.Settings.Default.TzUseQueue;
                CSironaStateSettings._sbUseQueue = Properties.Settings.Default.SirUseQueue;
                CDeviceInfo.sbModelFromDeviceInfo = Properties.Settings.Default.ModelFromDevice;

                bool bLogFlush = Properties.Settings.Default.ProgLogFlush;
                if (bIsProgrammer)
                {
                    bool b = true; // CTzDeviceSettings._sbUseQueue;
                    if (b)
                    {
                        b = true;
                    }
                    CTzDeviceSettings._sbUseQueue = b;  // enable que
                    CSironaStateSettings._sbUseQueue = b;

                    bLogFlush = true;
                }

                if (CTzDeviceSettings._sbUseQueue)
                {
                    CProgram.sLogLine("TZ Queue enabled");
                    labelTzQueue.Text = "TZ queue";
                }
                else
                {
                    labelTzQueue.Text = "-tzq";
                }
                if (CSironaStateSettings._sbUseQueue)
                {
                    CProgram.sLogLine("Sirona Queue enabled");
                    labelSirQueue.Text = "Sir queue";
                }
                else
                {
                    labelSirQueue.Text = "-sirq";
                }
                CProgram.sSetProgramLogFlush(bLogFlush);
                CProgram.sLogLine("ProgLogFlush " + (bLogFlush ? "Enabled" : "Disabled!"));

                _mbRun = !_mbRunAtStartup;    // force update;
                mSetRun(_mbRunAtStartup);

                int daysLeft = CLicKeyDev.sGetDaysLeft();
                labelUtcTime.Text = "days left = " + daysLeft.ToString();

                if (false == bRecording)
                {
                    CProgram.sPromptError(true, "Startup Failed", "Failed to connect to dBase, please check connection and settings!");
                    Close();
                }

                if (mbCreateDBaseConnection())
                {
                    string enumPath = Path.Combine(CProgram.sGetProgDir(), "enums");
                    CDvtmsData.sSetDBaseServer(_mRecordingDir, _mDBaseServer, enumPath);

                    // needs CDvtmsData to be setup
                    if (_mHttpSirFromDir != null && _mHttpSirFromDir.Length > 4)
                    {
                        string path;
                        if (false == CSironaStateSettings.sbGetSironaDevicePath(out path, null))
                        {
                            CProgram.sLogError("Invalid htpSir device dir: " + path);
                        }

                    }
                    if (_mTzFromDir != null && _mTzFromDir.Length > 4)
                    {
                        string path;
                        if (false == CDvtmsData.sbGetDeviceTypeDir(out path, DDeviceType.TZ)) //"TzAera")
                        {
                            CProgram.sLogError("Invalid TZ device dir: " + path);
                        }
                    }
                    if (_mDV2FromDir != null && _mDV2FromDir.Length > 4)
                    {
                        string path;
                        if (false == CDvtmsData.sbGetDeviceTypeDir(out path, DDeviceType.DV2))
                        {
                            CProgram.sLogError("Invalid DV2 device dir: " + path);
                        }
                    }
                    if (_mAutoRunDVX > 0)
                    {
                        string path;
                        if (false == CDvtmsData.sbGetDeviceTypeDir(out path, DDeviceType.DVX))
                        {
                            CProgram.sLogError("Invalid DVX device dir: " + path);
                        }
                    }
                }
                else
                {
                    CProgram.sPromptError(true, "Startup Failed", "Failed to connect to dBase, please check connection and settings!");
                    Close();
                }
                mbProcessEnter(" ", 0, false);
                mbProcessExit("start...");

                if (mbMailSetup(orgLabel))
                {
                    timerProcessMail.Enabled = true;
                    checkBoxMailEnable.Checked = true;
                }
                else
                {
                    panelMail.Enabled = false;
                    checkBoxMailEnable.Checked = false;
                }
                _mMainTimerMilSec = Properties.Settings.Default.MainTimerMilSec;
                if (_mMainTimerMilSec < 50)
                {
                    _mMainTimerMilSec = 50;
                }
                timer.Interval = _mMainTimerMilSec;
                UInt32 maxRows = Properties.Settings.Default.MaxNrRowsListed;
                if (maxRows > 10)
                {
                    _mMaxNrRowsListed = maxRows;
                }
                CProgram.sLogLine("MainTimer= " + _mMainTimerMilSec + "milSec, max. " + _mMaxNrRowsListed.ToString() + " rows");
                labelUpdTimer.Text = "upd. timer " + _mMainTimerMilSec.ToString() + " mSec";
                labelListInfo.Text = "- rows < " + _mMaxNrRowsListed.ToString();
                toolStripButtonWarn.Text = "_";

                float limitAmplRange = Properties.Settings.Default.LimitAmplRange;
                CRecordMit.sbLimitAmplitudeRange = limitAmplRange >= 1.0F;
                CRecordMit.sLimitAmplitudeRange = limitAmplRange;

                sbTzQuickFilter = Properties.Settings.Default.TzQuickFilter;
                mShowAmplRange();
                mShowTzHrFilter();

                makeLauncherVersionToolStripMenuItem.Visible = CLicKeyDev.sGetOrgNr() < 30;
                mCheckDiskMemSrvr(true);
                mStatusLine(".");

                CProgram.sLogLine("Sirona auto collect " + (_mbSirAutoCollectActive ? "ON" : "off")
                    + ", scan " + CSirAutoCollect._sScanTimeMin.ToString()
                    + " min, max nr in queue = " + CSirAutoCollect._sMaxActionsInQueue.ToString());

                CProgram.sLogLine("Sirona Holter Event = " + (_mbSirAutoCollectHolterEvent ? "On" : "Off")
                   + " " + CSironaStateSettings.sGetHolterReqLengthSec().ToString() + " sec, req max "
                   + CSironaStateSettings.sGetMaxReqLengthSec().ToString() + " sec");


                if (CCardioLogsAPI._cbEnableCardiologs == false)
                {
                    _mCardioLogsUrl = null;   // not implemented
                    _mCardioLogsToken = "Not Implemented";
                    _mCardiologsEventMin = 0;
                }
                else
                {
                    _mCardioLogsUrl = Properties.Settings.Default.CardioLogsURL;     // https://api.us.cardiologs.com/v1/ecg
                    _mCardioLogsToken = Properties.Settings.Default.CardioLogsToken; // HpM5vMprviNoQ7iwiQCW    cGsxyxLhjt8un3sgUo2y
                    _mCardiologsEventMin = Properties.Settings.Default.CardiologsEventMin;
                }
                _mTzPaMethod = Properties.Settings.Default.TzPaMethod;
                _mSironaPaMethod = Properties.Settings.Default.SironaPaMethod;
                _mDvxPaMethod = Properties.Settings.Default.DvxPaMethod;
                _mDv2PaMethod = Properties.Settings.Default.DV2PaMethod;



                if ( bIsProgrammer)
                {
                    _mCardioLogsToken = "HpM5vM";
                    _mCardioLogsUrl = "https://api.us.cardiologs.com/v1/ecg";
                    _mCardioLogsToken += "prviNoQ7";
                    _mTzPaMethod = "CardioLogs";
                    _mCardioLogsToken += "iwiQCW";
                    _mSironaPaMethod = _mTzPaMethod;
                    _mDvxPaMethod = _mTzPaMethod;
                    _mDv2PaMethod = _mTzPaMethod;
                }

                cardioLogsToolStripMenuItem.Visible = _mCardioLogsUrl != null && _mCardioLogsUrl.Length > 1 && _mCardioLogsToken != null && _mCardioLogsToken.Length > 1;

                if (_mAutoRunTZe > 0 && _mTzPaMethod != null && _mTzPaMethod.Length > 1)
                {
                    _mbDoPaTZ = true;
                    CProgram.sLogLine("TZ PA=" + _mTzPaMethod);
                }
                if (_mAutoRunSirZip > 0 && _mSironaPaMethod != null && _mSironaPaMethod.Length > 1)
                {
                    _mbDoPaSirona = true;
                    CProgram.sLogLine("Sirona PA=" + _mSironaPaMethod);
                }
                if (_mAutoRunDVX > 0 && _mDvxPaMethod != null && _mDvxPaMethod.Length > 1)
                {
                    _mbDoPaDVX = true;
                    CProgram.sLogLine("DVX PA=" + _mDvxPaMethod);
                }
                if (_mAutoRunDV2 > 0 && _mDv2PaMethod != null && _mDv2PaMethod.Length > 1)
                {
                    _mbDoPaDV2 = true;
                    CProgram.sLogLine("DV2 PA=" + _mDv2PaMethod);
                }

                toolStripButtonPaTZ.Visible = _mbDoPaTZ;
                toolStripButtonPaSir.Visible = _mbDoPaSirona;
                toolStripButtonPaDV2.Visible = _mbDoPaDV2;
                toolStripButtonPaDVX.Visible = _mbDoPaDVX;
                mUpdatePaLabels();


                labelProgressInfo.Text = _mbRunAtStartup ? "Auto run enabled." : "Start up done.";
                mbInitialized = true;
                timer.Enabled = true;

            }
            catch (Exception)
            {
                CProgram.sLogLine("I-Reader form create failed");
            }
        }

        private void mUpdatePaLabels()
        {
            toolStripButtonPaTZ.Text = _mbDoPaTZ ? "+PA" : "-pa";
            toolStripButtonPaSir.Text = _mbDoPaSirona ? "+PA" : "-pa";
            toolStripButtonPaDV2.Text = _mbDoPaDV2 ? "+PA" : "-pa";
            toolStripButtonPaDVX.Text = _mbDoPaDVX ? "+PA" : "-pa";
        }


        private Icon mLoadOneIcon(string AName, Icon ADefaultIcon)
        {
            Icon icon = null;

            try
            {
                icon = Icon.ExtractAssociatedIcon(Path.Combine(CProgram.sGetProgDir(), AName + ".ico"));
            }
            catch (Exception)
            {
                CProgram.sLogLine("I-Reader failed load icon " + AName);
            }
            return icon == null ? ADefaultIcon : icon;
        }


        private void notifyIcon1_DoubleClick(object Sender, EventArgs e)
        {
            // Show the form when the user double clicks on the notify icon.

            // Set the WindowState to normal if the form is minimized.
            if (this.WindowState == FormWindowState.Minimized)
                this.WindowState = FormWindowState.Normal;

            // Activate the form.
            this.Activate();
        }


        private void mShowAmplRange()
        {

            labelAmplRange.Text = "AmplR " + (CRecordMit.sbLimitAmplitudeRange ? CRecordMit.sLimitAmplitudeRange.ToString("0.0") + "mV" : "off");
        }

        private void mSetAmplRange()
        {
            float range = CRecordMit.sGetMaxAmplitudeRange();
            if (CProgram.sbReqFloat("Preview Image ECG Aplitude Range", "Max Amplitude Range (0=off)", ref range, "0.0", "mV", 0, 1e9F))
            {
                if (range < 0.5F || range >= 1e8F)
                {
                    CRecordMit.sbLimitAmplitudeRange = false;
                }
                else
                {
                    CRecordMit.sbLimitAmplitudeRange = true;
                    CRecordMit.sLimitAmplitudeRange = range;
                }
            }
            mShowAmplRange();

        }
        private void mShowTzHrFilter()
        {
            labelTzHrFilter.Text = sbTzQuickFilter ? "TZ HP Filter" : "TZ No Filter";

            CTzQuickFilter.sTzQuickFilters = new List<Filter>();

            if (sbTzQuickFilter)
            {
                Filter filter = new HighPassFilter();
                filter.Enabled = true;
                CTzQuickFilter.sTzQuickFilters.Add(filter);
            }

        }
        private void mSetTzHrFilter()
        {
            if (CProgram.sbReqBool("TZ quick Filter on Load SCP", "High Pass Filter", ref sbTzQuickFilter))
            {

            }
            mShowTzHrFilter();
        }

        public string mCheckPath(string APath)
        {
            string path = "";

            if (APath != null && APath.Length > 0)
            {
                try
                {
                    path = Path.GetFullPath(APath);

                    if (false == Directory.Exists(path))
                    {
                        CProgram.sLogLine("I-Reader path does not exist: " + APath);
                    }
                }
                catch (Exception)
                {
                    CProgram.sLogLine("I-Reader check Path failed for " + APath);
                }
            }
            return path;
        }
        static private bool mbLogImportDirExist(string ALabel, ref string APath, UInt16 AMode)
        {
            bool bExist = false;
            bool bOn = AMode > 0;
            string s;
            string path = "?";

            try
            {
                if (APath != null && APath.Length > 1)
                {
                    path = Path.GetFullPath(APath);
                    bExist = Directory.Exists(path);
                }
            }
            catch (Exception)
            {
            }
            if (bExist)
            {
                s = bOn ? "  On+Exists" : "  present";
                CProgram.sLogLine(ALabel + " = " + path + " autoRun= " + AMode.ToString() + s);
            }
            else if (path != null && path.Length > 1)
            {
                s = bOn ? "On and Missing!!" : " -";
                CProgram.sLogLine(ALabel + " = " + APath + " autoRun= " + AMode.ToString() + s);
            }
            return bExist;
        }

        public bool mbProcessUpdateScreen(bool AbForce, bool AbCheckRun)
        {
            bool bCont = true;
            DateTime nowDT = DateTime.Now;
            double dT = (nowDT - _mProcessUpdateDT).TotalSeconds;

            if (AbForce || dT >= _mProcessUpdateTimeSec)
            {

                if (AbForce || _mProcessNextLabel != _mProcessShownLabel || _mProcessStep != _mProcessShownStep)
                {
                    string label = "";

                    switch (_mProcessStep)
                    {
                        case DProcessStep.Off: label += _cCharOff; break;
                        case DProcessStep.Entry: label += _cCharOff; break;
                        case DProcessStep.Start: label += _cCharStart; break;
                        case DProcessStep.Loop:
                            if (++_ProcessCharLoop >= _cCharProc.Length) _ProcessCharLoop = 0;
                            label += _cCharProc[_ProcessCharLoop];
                            break;
                        case DProcessStep.End: label += _cCharEnd; break;
                        case DProcessStep.Exit: label += _cCharExit; break;
                        case DProcessStep.Done: label += _cCharDone; break;
                    }
                    label += _mProcessNextLabel;
                    toolStripProcessLabel.Text = label;
                    _mProcessShownLabel = _mProcessNextLabel;
                    _mProcessShownStep = _mProcessStep;
                }
                if (AbForce || _ProcessShownIndex != _ProcessLoopIndex)
                {
                    int i = _ProcessLoopIndex < 0 || _ProcessLoopTotal <= 0 ? -1 : _ProcessLoopIndex + 1;

                    if (i > _ProcessLoopTotal)
                    {
                        i = _ProcessLoopTotal;
                    }
                    toolStripProcessIndex.Text = i < 0 ? "." : i.ToString();
                    _ProcessShownIndex = _ProcessLoopIndex;
                }
                if (AbForce || _ProcessShownTotal != _ProcessLoopTotal)
                {
                    toolStripProcessTotal.Text = _ProcessLoopTotal <= 0 ? "." : _ProcessLoopTotal.ToString();
                    _ProcessShownTotal = _ProcessLoopTotal;
                }
                _mProcessUpdateDT = nowDT;

                if (_mProcessStep != DProcessStep.Off && _mProcessStep != DProcessStep.Exit)
                {
                    if (_mbProcessHandleMsgs)
                    {
                        dT = (nowDT - _mProcessHandleDT).TotalSeconds;
                        if (dT >= _mProcessHandleTimeSec)
                        {
                            _mProcessHandleDT = nowDT;
                            Application.DoEvents();  // handle all windows messages to make application reactive
                        }
                    }
                    dT = (nowDT - _mProcessStartLoopDT).TotalSeconds;
                    if (dT >= _mProcessMaxLoopTimeSec)
                    {
                        CProgram.sLogError("Exeeding loop time limit (" + dT.ToString("0.0") + " > " + _mProcessMaxLoopTimeSec.ToString("0.0") + " sec)");
                        bCont = false;
                        _mProcessStartLoopDT = nowDT;
                    }
                    dT = (nowDT - _mProcessStartEntryDT).TotalSeconds;
                    if (dT >= _mProcessMaxEntryTimeSec)
                    {
                        CProgram.sLogError("Exeeding entry time limit (" + dT.ToString("0.0") + " > " + _mProcessMaxEntryTimeSec.ToString("0.0") + " sec)");
                        bCont = false;
                        _mProcessStartEntryDT = nowDT;
                    }
                }
                mUpdateMemUse();
            }
            if (bCont && AbCheckRun && false == _mbRun)
            {
                bCont = false;
                CProgram.sLogError("End due to run canceled");
            }
            return bCont;
        }

        public void mProcessSetInfo(string AExtraInfo)
        {
            string label = CProgram.sDateTimeToYMDHMS(DateTime.Now);

            if (_mbRun)
            {
                if (_mbRunLoop)
                {
                    // running scan loop
                    label += " Run ";
                }
                else
                {
                    // waiting for scan loop to start
                    label += " Wait ";
                }
            }
            else
            {
                if (_mbRunLoop)
                {
                    // running scan loop
                    label += " Break ";
                }
                else
                {
                    // waiting for scan loop to start
                    label += " Stopped ";
                }
            }

            label += _mProcessNextLabel;

            switch (_mProcessStep)
            {
                case DProcessStep.Off: label += " Off "; break;
                case DProcessStep.Entry: label += " Entry "; break;
                case DProcessStep.Start: label += " Start "; break;
                case DProcessStep.Loop:
                    label += " Loop " + (_ProcessLoopIndex >= 0 ? _ProcessLoopIndex.ToString() : "") + " / ";
                    break;
                case DProcessStep.End: label += " End "; break;
                case DProcessStep.Exit: label += " Exit "; break;
                case DProcessStep.Done: label += " Done "; break;
            }
            label += _ProcessLoopTotal > 0 ? _ProcessLoopTotal.ToString() : "";
            label += " " + AExtraInfo;
            labelProgressInfo.Text = label;
        }



        private void mUpdateMemUse()
        {
            UInt32 progMB = CProgram.sGetProgUsedMemoryMB();
            UInt32 freeMB = CProgram.sGetFreeMemoryMB();
            bool bProgMemError = progMB >= _mCheckMemUsedMB;
            bool bFreeMemError = freeMB <= _mCheckMemFreeMB;

            labelMemUsage.BackColor = bProgMemError || bFreeMemError ? Color.Orange : panelExtra.BackColor;
            labelMemUsage.Text = (bProgMemError ? "p-" : "P ") + progMB.ToString()
            + (bFreeMemError ? " f-" : " F ") + freeMB.ToString()
            + "MB";
        }

        public bool mbProcessEnter(string ALabel, Int32 ATotalNr, bool AbCheckRun)
        {
            DateTime nowDT = DateTime.Now;

            _mProcessStep = DProcessStep.Entry;
            _mProcessNextLabel = ALabel;
            _ProcessLoopIndex = -1;
            _ProcessLoopTotal = ATotalNr;
            _mProcessStartEntryDT = nowDT;
            _mProcessStartLoopDT = nowDT;
            mProcessSetInfo("");
            return mbProcessUpdateScreen(true, AbCheckRun);
        }
        public bool mbProcessStartLoop(string ALabel, Int32 ATotalNr, bool AbCheckRun)
        {
            DateTime nowDT = DateTime.Now;

            _mProcessStep = DProcessStep.Start;
            _mProcessNextLabel = ALabel;
            _ProcessLoopIndex = 0;
            _ProcessLoopTotal = ATotalNr;
            _mProcessStartLoopDT = nowDT;
            mProcessSetInfo("");
            return mbProcessUpdateScreen(true, AbCheckRun);
        }
        public bool mbProcessNextLoop(Int32 AIndexNr, bool AbCheckRun, string AItemInfo)
        {
            if (_mProcessStep != DProcessStep.Loop && _mProcessStep != DProcessStep.Start)
            {
                CProgram.sLogError("Process loop without start");
            }
            _mProcessStep = DProcessStep.Loop;
            _ProcessLoopIndex = AIndexNr;
            mProcessSetInfo(AItemInfo);

            return mbProcessUpdateScreen(AIndexNr <= 1, AbCheckRun);

        }
        public bool mbProcessEndLoop(Int32 AIndexNr, bool AbCheckRun, string AEndInfo)
        {
            if (_mProcessStep != DProcessStep.Loop && _mProcessStep != DProcessStep.Start)
            {
                CProgram.sLogError("Process loop without start");
            }
            _ProcessLoopIndex = AIndexNr;
            _mProcessStep = DProcessStep.End;
            mProcessSetInfo(AEndInfo);
            return mbProcessUpdateScreen(true, AbCheckRun);

        }
        public bool mbProcessExit(string AExitInfo)
        {
            _mProcessStep = DProcessStep.End;

            mProcessSetInfo(AExitInfo);

            return mbProcessUpdateScreen(true, false);
        }
        public void mProcessCheckExit()
        {
            if (_mProcessStep != DProcessStep.Off && _mProcessStep != DProcessStep.Exit)
            {
                mbProcessExit("xxx");
            }
        }

        float mProcessDuration()
        {
            return (float)(DateTime.Now - _mProcessStartEntryDT).TotalSeconds;
        }
        float mProcessLoopDuration()
        {
            return (float)(DateTime.Now - _mProcessStartLoopDT).TotalSeconds;
        }

        public bool mbLockImportPath(string AImportPath, string ALockName, ref StreamWriter ArLockStream)
        {
            bool bOk = false;

            if (AImportPath != null && AImportPath.Length > 4)
            {
                if (false == _mbLockImportDir)
                {
                    bOk = true;
                }
                else
                {
                    bOk = CProgram.sbLockFilePath(AImportPath, ALockName, ref ArLockStream);
                }
            }
            return bOk;
        }

        public bool mbUnLockImportPath(string AImportPath, string ALockName, ref StreamWriter ArLockStream)
        {
            bool bOk = false;

            if (AImportPath != null && AImportPath.Length > 4)
            {
                if (false == _mbLockImportDir)
                {
                    bOk = true;
                }
                else if (ArLockStream == null)
                {
                    bOk = CProgram.sbUnLockFilePath(AImportPath, ALockName, ref ArLockStream);
                }
            }
            return bOk;
        }

        public bool mbLockZipImportPath()
        {
            return mbLockImportPath(_mZipFromDir, "SirZip", ref _mZipLockFile);
        }
        public bool mbLockTzImportPath()
        {
            return mbLockImportPath(_mTzFromDir, "TZ", ref _mTzLockFile);
        }
        public bool mbLockDV2ImportPath()
        {
            return mbLockImportPath(_mDV2FromDir, "DV2", ref _mDV2LockFile);
        }
        public bool mbLockDVXImportPath()
        {
            bool bOk = false;
            if (_mDVXStudyDir != null && _mDVXStudyDir.Length > 2)
            {
                string dvxDeviceFolder = Path.Combine(_mDVXStudyDir, CDvxEvtRec._cDvxDeviceFolder);
                bOk = mbLockImportPath(dvxDeviceFolder, "DVX", ref _mDVXLockFile);
            }
            return bOk;
        }
        public bool mbLockHttpSirImportPath()
        {
            return mbLockImportPath(_mHttpSirFromDir, "HttpSir", ref _mHttpSirLockFile);
        }
        public bool mbUnLockZipImportPath()
        {
            return mbUnLockImportPath(_mZipFromDir, "SirZip", ref _mZipLockFile);
        }
        public bool mbUnLockTzImportPath()
        {
            return mbUnLockImportPath(_mTzFromDir, "TZ", ref _mTzLockFile);
        }
        public bool mbUnLockDV2ImportPath()
        {
            return mbUnLockImportPath(_mDV2FromDir, "DV2", ref _mDV2LockFile);
        }
        public bool mbUnLockDVXImportPath()
        {
            return mbUnLockImportPath(_mDVXStudyDir, "DVX", ref _mDVXLockFile);
        }
        public bool mbUnLockHttpSirImportPath()
        {
            return mbUnLockImportPath(_mHttpSirFromDir, "HttpSir", ref _mHttpSirLockFile);
        }

        public void mUnLockAllImportPath()
        {
            mbUnLockZipImportPath();
            mbUnLockTzImportPath();
            mbUnLockDV2ImportPath();
            mbUnLockDVXImportPath();
            mbUnLockHttpSirImportPath();
        }
        private void mUpdateSirHE()
        {
            string label = _mbSirAutoCollectHolterEvent ? "+HE" : "-he";
            UInt32 holterMin = CSironaStateSettings.sGetHolterReqLengthSec() / 60;

            label += " " + CSironaStateSettings.sGetHolterReqLengthSec() / 60;
            //label += "\r\n";
            //label += _mbSir? "+B" : "-b";

            toolStripButtonSirHolter.Text = label;
        }
        private void mEditSirHE()
        {
            bool bHE = _mbSirAutoCollectHolterEvent;

            if (CProgram.sbReqBool("Sirona Holter Event", "Holter Event Active", ref bHE))
            {
                UInt32 holterMin = CSironaStateSettings.sGetHolterReqLengthSec() / 60;
                UInt32 reqMin = CSironaStateSettings.sGetMaxReqLengthSec() / 60;
                _mbSirAutoCollectHolterEvent = bHE;

                if (CProgram.sbReqUInt32("Sirona Event settings", "Sirona Holter strip length (0=off)", ref holterMin, "min", 0, 9999))
                {
                    CProgram.sbReqUInt32("Sirona Event settings", "Sirona max reques strip length", ref reqMin, "min", 0, 9999);

                    CSironaStateSettings.sSetHolterReqLength(reqMin, holterMin);
                }
                holterMin = CSironaStateSettings.sGetHolterReqLengthSec() / 60;
                reqMin = CSironaStateSettings.sGetMaxReqLengthSec() / 60;
                CProgram.sLogLine("Sirona Holter Event = " + (bHE ? "On" : "Off")
                   + " " + holterMin.ToString() + " min, req max " + reqMin.ToString() + " min");
            }
            mUpdateSirHE();
        }
        private void mUpdateSirAutoCollect()
        {
            string label = _mbSirAutoCollectActive ? "AC " + CSirAutoCollect._sScanTimeMin.ToString() : "ac off";

            //label += ? "\r\n+R evt" : "\r\n-r evt";
            toolStripButtonSirAC.Text = label;
        }

        private void mEditSirAutoCollect()
        {
            UInt16 min = (UInt16)CSirAutoCollect._sScanTimeMin;      // TZ skip test values

            bool bOn = _mbSirAutoCollectActive;

            if (CProgram.sbReqBool("Sirona Auto Collect settings", "Sirona Auto Collect Active", ref bOn))
            {
                if (bOn)
                {
                    if (CProgram.sbReqUInt16("Sirona Auto Collect settings", "Scan missing every", ref min, "min", 1, 9999))
                    {
                        CSirAutoCollect._sScanTimeMin = min;

                        CProgram.sbReqUInt32("Sirona Auto Collect settings", "Sirona max nr in queue", ref CSirAutoCollect._sMaxActionsInQueue, "", 1, 9999);


                        CProgram.sLogLine("Sirona auto collect set scan " + min.ToString() + " min, max nr in queue = " + CSirAutoCollect._sMaxActionsInQueue.ToString());
                    }
                    else
                    {
                        bOn = false;
                    }
                }
                else
                {
                    CProgram.sLogLine("Sirona auto collect set off");
                }
                _mbSirAutoCollectActive = bOn;
            }
            mUpdateSirAutoCollect();
        }

        private void mUpdateTzRB()
        {
            string label = "";

            label += CTZeReader._sbEventRequestScp ? "+R" : "-r";
            label += "\r\n";
            label += CTZeReader._sbEventBulkUpload ? "+B" : "-b";

            toolStripButtonTzRB.Text = label;
        }
        private void mEditTzRB()
        {
            bool bR = CTZeReader._sbEventRequestScp;
            bool bB = CTZeReader._sbEventBulkUpload;

            if (CProgram.sbReqBool("TZ Event import config", "Import Event Request Scp", ref bR)
                && CProgram.sbReqBool("TZ Event import config", "Import Event Bulk Upload", ref bB)
                )
            {
                CTZeReader._sbEventRequestScp = bR;
                CTZeReader._sbEventBulkUpload = bB;

                CProgram.sLogLine("TZ Event Request Scp= " + (bR ? "On" : "Off") + ", Bulk Upload= " + (bB ? "On" : "Off"));
            }
            mUpdateTzRB();
        }
        private void mUpdateTzSkip()
        {
            string label = "";

            label += "Skip\r\n";

            if (_mTzSkipCmpMin > _cTzSkipRange)
            {
                _mTzSkipCmpMin = _cTzSkipRange;
            }
            if (_mTzSkipCmpMax > _cTzSkipRange)
            {
                _mTzSkipCmpMax = _cTzSkipRange;
            }
            if (_mTzSkipCmpMin < 0 || _mTzSkipCmpMax < 0)
            {
                label += "off";
            }
            else
            {
                label += _mTzSkipCmpMin.ToString() + ";" + _mTzSkipCmpMax.ToString();
            }
            toolStripButtonTzSkip.Text = label;
        }

        private void mEditTzSkip()
        {
            Int16 min = _mTzSkipCmpMin;      // TZ skip test values
            Int16 max = _mTzSkipCmpMax;
            bool bOn = min >= 0 && max >= 0;

            if (CProgram.sbReqBool("TZ Skip settings", "TZ Skip active", ref bOn))
            {
                if (bOn)
                {
                    if (min < 0) min = 0;
                    if (max < 0) max = 0;
                    if (CProgram.sbReqInt16("TZ Skip settings", "TZ Skip compare range min ", ref min, "scp files", -1, _cTzSkipRange)
                        && CProgram.sbReqInt16("TZ Skip settings", "TZ Skip compare range plus ", ref max, "scp files", -1, _cTzSkipRange))
                    {
                        _mTzSkipCmpMin = min;
                        _mTzSkipCmpMax = max;
                    }
                }
                else
                {
                    _mTzSkipCmpMin = -1;
                    _mTzSkipCmpMax = -1;
                }
                CProgram.sLogLine("TZ skip set min=" + _mTzSkipCmpMin.ToString() + ", max=" + _mTzSkipCmpMax.ToString());
            }
            mUpdateTzSkip();
        }
        private void mUpdateTzAutoCollect()
        {
            string label = _mbTzAutoCollectActive ? "AC " + _mTzAutoColScanMin.ToString() : "ac off";

            _mbTzAutoCollectReqEvent = true; // for now

            label += _mbTzAutoCollectReqEvent ? "\r\n+R evt" : "\r\n-r evt";
            toolStripTzAutoCollect.Text = label;
        }

        private void mEditTzAutoCollect()
        {
            UInt16 min = _mTzAutoColScanMin;      // TZ skip test values

            bool bOn = _mbTzAutoCollectActive;
            bool bReq = _mbTzAutoCollectReqEvent;

            if (CProgram.sbReqBool("TZ Auto Collect settings", "Active", ref bOn))
            {
                if (bOn)
                {
                    if (CProgram.sbReqUInt16("TZ Auto Collect settings", "Scan missing every", ref min, "min", 1, 9999))
                    {
                        //                       if (CProgram.sbReqBool("TZ Auto Collect settings", "Create event on Request", ref bReq))
                        {
                            _mbTzAutoCollectActive = true;
                            _mbTzAutoCollectReqEvent = bReq;
                            _mTzAutoColScanMin = min;
                            CTzAutoCollect._sScanTimeMin = (UInt32)(min / 2 + 1);
                            CProgram.sLogLine("TZ auto collect set " + min.ToString() + " min, req event " + (bReq ? "ON" : "off"));

                            if (CTZeReader._sbEventRequestScp)
                            {
                                CProgram.sAskWarning("TZ Auto Collect settings", "Auto collect generates Request events, turn off +R if not wanted.");
                            }

                            CProgram.sbReqUInt32("TZ Auto Collect settings", "TZ max nr in queue", ref CTzAutoCollect._sMaxActionsInQueue, "", 1, 9999);
                        }
                    }
                }
                else
                {
                    _mbTzAutoCollectActive = false;
                    CProgram.sLogLine("TZ auto collect set =" + _mTzSkipCmpMin.ToString() + ", max=" + _mTzSkipCmpMax.ToString());
                }
            }
            mUpdateTzAutoCollect();
        }

        CDvxSkipLastFile mGetDvxSkipLastFile( UInt32 ADeviceNR)
        {
            CDvxSkipLastFile foundDevice = null;

            try
            {
                if (_mDvxSkipList == null)
                {
                    _mDvxSkipList = new List<CDvxSkipLastFile>();
                }
                if (_mDvxSkipList != null && ADeviceNR !=  0)
                {
                    foreach (CDvxSkipLastFile skipFile in _mDvxSkipList)
                    {
                        if (skipFile._mDeviceNR == ADeviceNR)
                        {
                            foundDevice = skipFile;
                            break;
                        }
                    }
                    if (foundDevice == null)
                    {
                        foundDevice = new CDvxSkipLastFile();
                        if (foundDevice != null)
                        {
                            foundDevice._mDeviceNR = ADeviceNR; // new device
                            _mDvxSkipList.Add(foundDevice);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Find DvxSkipLastFile Failed " + ADeviceNR, ex);
            }
            if (_mDvxSkipList == null)
            {
                _mDvxSkipList = new List<CDvxSkipLastFile>();
            }
            return foundDevice;
        }



        private void mUpdateDvxSkip()
        {
            string label = "skip off";

            if( _mDvxSkipCmpSec > 0)
            {
                label = "Skip"+ (_mbDvxSkipSameOnly ? "= " : "≠ ") + _mDvxSkipCmpSec.ToString();

                if( _mDvxRepCmpSec > 0)
                {
                    label += "\r\nrep " + _mDvxRepCmpSec.ToString();
                }
            }
            toolStripButtonSkipDVX.Text = label;
        }

        private void mEditDvxSkip()
        {
            Int16 skip = _mDvxSkipCmpSec;
            Int16 rep = _mDvxRepCmpSec;
            bool bSame = _mbDvxSkipSameOnly;

            bool bOn = skip >= 0 || rep > 0;

            if (CProgram.sbReqBool("Dvx Skip settings", "DVX Skip active", ref bOn))
            {
                if (bOn)
                {
                    if (skip < 0) skip = 0;
                    if (rep < 0) rep = 0;
                    if (CProgram.sbReqInt16("DVX Skip settings (-1 = off)", "DVX Skip compare range ", ref skip, "sec", -1, 3600)
                        && CProgram.sbReqBool("DVX Skip settings", "DVX Skip same only", ref bSame)
                        && CProgram.sbReqInt16("DVX Skip settings (-1 = off)", "DVX Skip repeat after ", ref rep, "sec", -1, 3600))
                    {
                        _mDvxSkipCmpSec = skip;
                        _mDvxRepCmpSec = rep;
                        _mbDvxSkipSameOnly = bSame;
                    }
                }
                else
                {
                    _mDvxSkipCmpSec = -1;
                    _mDvxRepCmpSec = -1;
                    _mbDvxSkipSameOnly = true;
                }
                CProgram.sLogLine("DVX skip=" + (_mbDvxSkipSameOnly ? " same " : " all ") + _mDvxSkipCmpSec.ToString() + " sec, rep=" + _mDvxRepCmpSec.ToString() + " sec" );
            }
            
            mUpdateDvxSkip();
        }



        static public void sCheckFloatHasDot()
        {
            float f = 3.14F;
            string s = f.ToString();

            _sbFloatHasDot = s.IndexOf('.') >= 0;
        }

        static public string sCorrectFloatDot(string AString)
        {
            if (AString != null)
            {
                if (_sbFloatHasDot)
                {
                    return AString.Replace(',', '.');
                }
                else
                {
                    return AString.Replace('.', ',');
                }
            }
            return AString;
        }

        /*        public void CProgram.sLogLine(String AString)
                {
                    String text;

                    text = DateTime.Now.ToString("HH:mm:ss") + ": " + AString + "\r\n";
                    logBox.Text = text + logBox.Text;
                }
        */
        public void mStatusLine(String AString)
        {
            statusStrip.Text = AString;
        }

        private void mUpdateAutoRunFlags()

        {
            toolStripAutoZip.Text = _mAutoRunSirZip == (UInt16)DAutoRun.Off ? "◦ zip" : "√ Zip";
            toolStripAutoTZe.Text = _mAutoRunTZe == (UInt16)DAutoRun.Off ? "◦ tze" : "√ TZe";
            toolStripAutoDV2.Text = _mAutoRunDV2 == (UInt16)DAutoRun.Off ? "◦ dv2" : "√ DV2";
            toolStripAutoDVX.Text = _mAutoRunDVX == (UInt16)DAutoRun.Off ? "◦ dvx" : "√ DVX";
            toolStripAutoHSir.Text = _mAutoRunHttpSir == (UInt16)DAutoRun.Off ? "◦ hsir" : "√ HSir";
        }
        void mUpdateIndex(int AIndex)
        {
            //?
            Action action = () => toolStripProcessIndex.Text = AIndex.ToString();
            this.Invoke(action);
        }

        public string mDateTimeToString(DateTime ADateTime) // used by fill grid
        {
            return CProgram.sDateTimeToString(ADateTime);
            //            return ADateTime.ToString("dd-MM-yyyy HH:mm:ss");
        }
        public bool mbParseDateTime(string AString, out DateTime ADateTime)
        {
            return CProgram.sbParseDtUser(AString, out ADateTime);
            //            return ADateTime.ToString("dd-MM-yyyy HH:mm:ss");
        }

        public string mDateToString(DateTime ADateTime)
        {
            return CProgram.sDateToString(ADateTime);
            //            return ADateTime.ToString("dd-MM-yyyy");
        }

        public string mTimeToString(DateTime ADateTime)
        {
            return CProgram.sTimeToString(ADateTime);
            //            return ADateTime.ToString("HH:mm:ss");
        }

        public string mSqlDateFormat()
        {
            return "%d-%m-%Y";  // must be equal to above string output
        }

        private string mGetUtcOffsetString(DateTime ADateTime, TimeZoneInfo ATimeZone)
        {
            string str;
            int hours, min;
            TimeSpan utcOffset = ATimeZone.BaseUtcOffset;
            bool b = ATimeZone.IsDaylightSavingTime(ADateTime);

            if (b)
            {
                TimeSpan offset = new TimeSpan(1, 0, 0);
                utcOffset += offset;
            }
            hours = Math.Abs(utcOffset.Hours);
            min = Math.Abs(utcOffset.Minutes);

            if (utcOffset >= TimeSpan.Zero)
            {
                if (b)
                {
                    str = "UP";
                }
                else
                {
                    str = "Up";
                }
            }
            else
            {
                if (b)
                {
                    str = "UM";
                }
                else
                {
                    str = "Um";
                }

            }
            str += hours.ToString("00") + min.ToString("00");

            return str;
        }

        private void mIncreaseTotalCount()
        {
            ++_mTotalCount;
            toolStripTotalRead.Text = _mTotalCount.ToString();
            Thread.Sleep(1);
        }

        private void mIncreaseErrorCount()
        {
            ++_mTotalCount;
            toolStripTotalRead.Text = _mTotalCount.ToString();
            ++_mErrorCount;
            toolStripErrorCount.Text = _mErrorCount.ToString();
            Thread.Sleep(1);
        }

        private void mIncreaseSkippedCount()
        {
            ++_mSkippedCount;
            toolStripSkipped.Text = _mSkippedCount.ToString();
            Thread.Sleep(1);
        }


        private void mSetRun(bool AbRun)
        {
            if (AbRun)
            {
                if (_mbRun != AbRun)
                {
                    _mbRun = true;
                    CProgram.sLogLine("Start Run.");
                    CRecDeviceList.sDeviceListClear();

                    toolStripAddTZ.Visible = false;
                    toolStripAddSirona.Visible = false;
                    toolStripAddDevice.Visible = false;
                    toolStripImport.Visible = false;
                    /*                    toolStripLoadOne.Enabled = false;
                                        toolStripButtonLoadHea.Enabled = false;
                                        toolStripButtonTZe.Enabled = false;
                                        toolStripImportSCP.Enabled = false;
                    */
                    LoadTableFromSql.Visible = false;
                    toolStripNrHours.Visible = false;
                    toolStripLabelHours.Visible = false;
                    toolStripDropDownSearch.Visible = false;
                    toolStripEditSearch.Visible = false;
                    toolStripEqualsCursor.Visible = false;
                    toolStripRun.Enabled = false;
                    if (toolStripAutoZip.Checked || toolStripAutoHSir.Checked)
                    {
                        CRecDeviceList.sbSironaDeviceListsCheck(_mHttpSirFromDir, true, false);
                    }
                    if (toolStripAutoTZe.Checked)
                    {
                        CRecDeviceList.sbTzDeviceListsCheck(_mTzFromDir, true, false);
                    }
                    if (toolStripAutoDV2.Checked)
                    {
                        CRecDeviceList.sbDV2DeviceListsCheck(_mDV2FromDir, true, false);
                    }
                    if (toolStripAutoDVX.Checked)
                    {
                        CRecDeviceList.sbDVXDeviceListsCheck(true, false);
                    }
                    _mLastRunTime = DateTime.Now;
                    _mStartRunTimeDT = DateTime.Now;
                    mProcessSetInfo("Start Run")
;
                }
            }
            else
            {
                if (_mbRun != AbRun)
                {
                    _mbRun = false;
                    CProgram.sLogLine("Stop Run.");
                    toolStripAddDevice.Visible = true;
                    toolStripImport.Visible = true;
                    LoadTableFromSql.Visible = true;
                    toolStripNrHours.Visible = true;
                    toolStripLabelHours.Visible = true;
                    //                   toolStripDropDownSearch.Visible = true;
                    //                   toolStripEditSearch.Visible = true;
                    //                   toolStripEqualsCursor.Visible = true;

                    toolStripRun.Enabled = true;
                    _mbRunLoop = false;
                    _mStartRunTimeDT = DateTime.MinValue;

                    _mStartScanDtTz = DateTime.MinValue;
                    _mStartScanDtSir = DateTime.MinValue;
                    _mStartScanDtDV2 = DateTime.MinValue;
                    _mStartScanDtDVX = DateTime.MinValue;
                    _mStartScanDtHttpSir = DateTime.MinValue;

                    mUnLockAllImportPath();
                    mProcessSetInfo("Stop Run");
                }
            }
            mCheckRunTimerState();
            if (timer.Enabled != AbRun)
            {
                timer.Enabled = AbRun;
            }
        }

        public void mSetRunIndex(int AIndex)
        {
            if (AIndex >= 0 && AIndex < _mRunN && _mFileRunList != null && AIndex < _mFileRunList.Count)
            {
                _mRunIndex = AIndex;
                mSetRunFilePath(_mFileRunList[AIndex]);

                string fileName = mGetRunFileName();

                mbProcessNextLoop(AIndex, false, fileName);
                //                toolStripProcessIndex.Text = _mRunIndex.ToString();
                CProgram.sLogLine("Processing " + fileName + "...");
            }
            else
            {
                _mbRunLoop = false;
                //                toolStripProcessIndex.Text = "-";
            }
        }

        public string mGetRunFilePath()
        {
            // private String mRunFilePath;    // private to get / set RunFile functions
            return _mRunFilePath;
        }
        public string mGetRunFileName()
        {
            // private String mRunFilePath;    // private to get / set RunFile functions
            string fileName = "";

            if (_mRunFilePath != null && _mRunFilePath.Length > 0)
            {
                int pos = _mRunFilePath.IndexOf('$');   // could hold double file path

                fileName = Path.GetFileName(pos >= 0 ? _mRunFilePath.Substring(0, pos) : _mRunFilePath);
            }
            return fileName;
        }

        public bool mbIsRunNamePresent()
        {
            return _mRunFilePath != null && _mRunFilePath.Length > 0;
        }

        public void mSetRunFilePath(string AFilePath)
        {
            _mRunFilePath = AFilePath;
        }

        //private void mCheckCleanOldFiles


        UInt32 mScanFromZipDir(double ADelFilesOlderThanHours)
        {
            UInt32 nFiles = 0;
            UInt32 nNoDevice = 0;
            DateTime dt = DateTime.UtcNow;
            string fileName = "?";
            try
            {
                CRecDeviceList.sbSironaDeviceListsCheck(null, true, false); //load license
                if (CRecDeviceList.sGetSironaDeviceCount() > 0 && mbLockZipImportPath())
                //                   if (Directory.Exists(_mZipFromDir))
                {
                    string[] fileList = Directory.GetFiles(_mZipFromDir, "*.zip");
                    Int32 nZipFiles = (fileList == null ? 0 : fileList.Length);

                    if (_mbLogExtra) CProgram.sLogInfo("Scan SirZip: " + nFiles.ToString() + " files in " + _mZipFromDir);

                    if (mbProcessStartLoop("scan SirZip", nZipFiles, true))
                    {
                        mSetRunTimerProgress("SirZip", 0, nZipFiles);
                        Int32 i = 0;
                        for (; i < nZipFiles; ++i)
                        {
                            fileName = fileList[i];
                            if (fileName.EndsWith(".zip")
                                && mbProcessNextLoop(i, true, fileName))
                            {
                                string name = Path.GetFileName(fileName);
                                int j = name.IndexOf('_');  // filename = <snr>_<pat ID>_<yyyymmddhhmmss>.zip
                                bool bIsDevice = j > 0;
                                string deviceStr = "";

                                mSetRunTimerProgress("SirZip", i, nZipFiles);
                                if (bIsDevice)
                                {
                                    deviceStr = name.Substring(0, j);
                                    bIsDevice = CRecDeviceList.sbSironaIsInLicenseList(deviceStr, false);
                                }

                                if (bIsDevice)
                                {
                                    if (CRecDeviceList.sbNotInBlackList(deviceStr))
                                    {
                                        _mFileRunList.Add(fileName);
                                        if (_mbLogExtra) CProgram.sLogLine("Sirona Zip toDo: " + name);
                                        ++nFiles;
                                    }
                                    else if (_mbLogExtra)
                                    {
                                        CProgram.sLogLine("Scan Sirona Zip " + deviceStr + " black listed!");
                                    }

                                    fileList[i] = "";
                                }
                                else
                                {
                                    fileList[i] = "";
                                    CProgram.sLogLine("Sirona device not in license list: " + name);
                                    try
                                    {
                                        File.Move(fileName, fileName + "_NoDevice");
                                    }
                                    catch (Exception)
                                    {
                                    }
                                }
                            }
                        }
                        mbProcessEndLoop(i, true, nFiles.ToString() + " files");
                        mSetRunTimerProgress("SirZip", nZipFiles, nZipFiles);
                    }
                    _mLastScanDtSir = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Scan Sirona Zip error at file " + nFiles, ex);

                //                nFiles = 0;
            }
            double sec = (DateTime.UtcNow - dt).TotalSeconds;

            if (sec > _mWarnScanSec || _mbLogExtra)
            {
                string s = "Scan Sirona Zip " + nFiles.ToString() + " in " + sec.ToString("0.0") + " sec";
                if (nNoDevice > 0) s += ", " + nNoDevice.ToString() + " files not in Device";
                CProgram.sLogLine(s);
            }
            return nFiles;
        }

        private void mScanOneSironaCmdStatusDir(string ASerialNr, bool AbDoAutoCollect)
        {
            string devicePath = null;
            string name = ASerialNr;
            // move device\<snr>\toServer\<snr>.SirSet to uploads\<snr>.json 
            try
            {
                if (CDvtmsData.sbGetDeviceUnitDir(out devicePath, DDeviceType.Sirona, ASerialNr, false))
                {
                    if (CSironaStateSettings._sbUseQueue)
                    {
                        mScanOneSironaCmdQueue(devicePath, ASerialNr);
                    }
                    else
                    {
                        mScanOneSironaCmdNoQueue(devicePath, ASerialNr);
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed on Sirona\\command\\" + name, ex);
            }
            //  check status\<snr>.json om deze te renamen naar *.SirStatus (del old) en naar device\<snr>\fromServer\<snr>.SirStatus te copieren
            try
            {
                if (devicePath != null)
                {
                    CSironaStateSettings.sbMakeStatusFileName(out name, ASerialNr);

                    string fromFile = Path.Combine(_mHttpSirStatusDir, ASerialNr + ".json");

                    if (File.Exists(fromFile))
                    {
                        string toFile = Path.Combine(devicePath, CSironaStateSettings._csFromServerName, name);
                        string fromFile2 = Path.Combine(_mHttpSirStatusDir, name);

                        try
                        {
                            if (File.Exists(fromFile2))
                            {
                                File.Delete(fromFile2);
                            }
                            File.Move(fromFile, fromFile2);
                            File.Copy(fromFile2, toFile, true);
                            CProgram.sLogLine("Copied Sirona\\status\\ " + name);
                        }
                        catch (Exception ex)
                        {
                            CProgram.sLogException("Failed copy Sirona\\status\\" + name, ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed on Sirona\\status\\" + name, ex);
            }

            if (AbDoAutoCollect)
            {
                UInt32 nrReqFiles = 0;

                try
                {
                    if (devicePath != null)
                    {
                        bool bScanOk = false;

                        if (ASerialNr == "76A0010119")
                        {
                            bScanOk = true;
                        }
                        // do a auto collect run
                        if (mbSirAutoCollectScan(out bScanOk, out nrReqFiles, ASerialNr, true, false))
                        {
                            //                            acResult = "+ AutoCollect(";
                            if (false == bScanOk)
                            {
                                //                              acResult += "Failed ";
                            }
                            //                        acResult += nrReqFiles.ToString() + ") ";
                        }
                    }
                }

                catch (Exception ex)
                {
                    CProgram.sLogException("Failed on Auto collect" + name, ex);
                }
                if (nrReqFiles > 0) // when a new request is made do an extra CMD que loop to get the request faster to the device
                {
                    try
                    {
                        if (CSironaStateSettings._sbUseQueue)
                        {
                            mScanOneSironaCmdQueue(devicePath, ASerialNr);
                        }
                        else
                        {
                            mScanOneSironaCmdNoQueue(devicePath, ASerialNr);
                        }
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException("Failed 2 on Sirona\\command\\" + name, ex);
                    }
                }
            }
        }
        private bool mbSirAutoCollectScan(out bool ArbScanOk, out UInt32 ArReqNrFiles, string ADeviceSnr, bool AbRunAutomatic, bool AbLogExtra)
        {
            bool bScanDone = false;
            bool bScanOk = false;
            bool bEnabled = false;
            UInt32 nrReqFiles = 0;

            string header = "Sir AC[" + ADeviceSnr + "]:";

            DateTime startDT = DateTime.Now;
            string acResult = "";
            bool bLogExtra = _mbLogExtra /*|| CLicKeyDev.sbDeviceIsProgrammer() */|| AbLogExtra;

            try
            {
                CDeviceInfo device = new CDeviceInfo(_mDBaseServer);
                string result = "";
                string deviceDir = "";

                if (device != null
                    && _mHttpSirFromDir != null && _mHttpSirFromDir.Length > 0 && CDvtmsData.sbGetDeviceUnitDir(out deviceDir, DDeviceType.Sirona, ADeviceSnr, false))
                {

                    int nSerial = device.mFindLoadSerial(0, ADeviceSnr, 0); // loads device

                    if (nSerial == 0 || device.mIndex_KEY == 0)
                    {
                        acResult = "Unknown device";
                    }
                    else if (device._mAddStudyMode == (UInt16)DAddStudyModeEnum.ActiveStudy)    // only auto collect when device is for one study
                    {
                        CSirAutoCollect _mCollect = new CSirAutoCollect("ECG", _mbSirAutoCollectHolterMerge);
                        if (_mCollect != null)
                        {
                            bScanOk = _mCollect.mbRunCycle(out result, out bEnabled, out bScanDone, deviceDir, ADeviceSnr, device._mActiveStudy_IX,
                                            device._mRecorderStartUTC, device._mRecorderEndUTC, AbRunAutomatic);
                            if (bLogExtra && bEnabled || false == bScanOk && bScanDone)
                            {
                                //                                CProgram.sLogInfo(header + "ECG= " + result);
                            }
                            if (bScanDone || false == bScanOk && bScanDone)
                            {
                                string s = "ECG " + (bEnabled ? (bScanDone ? "scanned " : "skipped ") : " AC Disabled ");

                                nrReqFiles = 1;
                                //                                s += _mCollect._mActionReqNrFiles.ToString() + "/" + _mCollectScp._mCalcFoundMissing.ToString();
                                acResult += s + " " + result;
                            }
                            else
                            {
                                acResult += " no scan: " + result;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException(header + "failed!", ex);
                bScanOk = false;
            }
            double tSec = (DateTime.Now - startDT).TotalSeconds;
            if (bEnabled && (bScanDone || tSec >= 2.0 || bLogExtra))
            {
                CProgram.sLogLine(header + acResult + " in " + tSec.ToString("0.000") + "sec");
            }

            ArbScanOk = bScanOk;
            ArReqNrFiles = nrReqFiles;
            return bScanDone;
        }

        private void mScanOneSironaCmdNoQueue(string ADevicePath, string ASerialNr)
        {
            string name = ASerialNr;
            // move device\<snr>\toServer\<snr>.SirSet to uploads\<snr>.json 
            try
            {
                CSironaStateSettings.sbMakeSettingsFileName(out name, ASerialNr);

                string fromFile = Path.Combine(ADevicePath, CSironaStateSettings._csToServerName, name);

                if (File.Exists(fromFile))
                {
                    string toFile = Path.Combine(_mHttpSirCommandDir, ASerialNr + ".json");

                    try
                    {
                        if (File.Exists(toFile))
                        {
                            File.Delete(toFile);
                        }
                        File.Move(fromFile, toFile);
                        CProgram.sLogLine("Moved to Sirona\\command\\ " + name);
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException("Failed Sirona\\command\\ Move " + fromFile + " to " + toFile, ex);
                    }
                }

            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed on Sirona\\command\\" + name, ex);
            }
        }
        private void mScanOneSironaCmdQueue(string ADevicePath, string ASerialNr)
        {
            string name = ASerialNr;
            // 
            // if uploads\<snr>.FileName exists and no uploads\<snr>.json (command is collected
            // then read uploads\<snr>.FileName and remove the file from queue
            // queue move first  device\<snr>\toServer\<snr>_yyyyMMddHHmmss.SirSet to uploads\<snr>.json 
            // when no uploads\<snr>.json  exists
            try
            {
                string fromDir = Path.Combine(ADevicePath, CSironaStateSettings._csToServerName);
                DirectoryInfo dirInfo = new DirectoryInfo(fromDir);
                int nrInQueue = 0;
                string firstName = "";


                if (dirInfo == null || false == dirInfo.Exists)
                {
                }
                else
                {
                    string toFile = Path.Combine(_mHttpSirCommandDir, ASerialNr + ".json");
                    bool bToExists = File.Exists(toFile);

                    string storeFile = Path.ChangeExtension(toFile, "FileName");
                    bool bStoreExists = File.Exists(storeFile);

                    FileInfo[] fileList = dirInfo.GetFiles("*." + CSironaStateSettings._cSettingFileExt);

                    if (fileList != null && fileList.Length > 0)
                    {
                        foreach (FileInfo fi in fileList)
                        {
                            if (firstName.Length == 0)
                            {
                                firstName = fi.Name;
                            }
                            ++nrInQueue;
                        }
                    }
                    if (nrInQueue == 0)
                    {
                        // no files in queue -> check if command file does not exist
                        if (bToExists)
                        {
                            try
                            {
                                File.Delete(toFile);
                                if (_mbLogExtra)
                                {
                                    CProgram.sLogLine("Removed Sirona " + toFile);
                                }
                            }
                            catch (Exception ex)
                            {
                                CProgram.sLogException("Failed Sirona\\command\\ delete " + toFile, ex);
                            }
                        }
                        if (bStoreExists)
                        {
                            try
                            {
                                File.Delete(storeFile);
                            }
                            catch (Exception ex)
                            {
                                CProgram.sLogException("Failed Sirona\\command\\ delete " + storeFile, ex);
                            }
                        }
                    }
                    else if (bToExists)
                    {
                        // command file is still there => do nothing
                        if (_mbLogExtra)
                        {
                            CProgram.sLogLine("Sirona command present, " + nrInQueue.ToString() + " in queue " + ASerialNr);
                        }
                    }
                    else
                    {
                        if (bStoreExists)
                        {
                            string lastName = File.ReadAllText(storeFile);

                            if (lastName != null && lastName.Length > 3)
                            {
                                // remove last file and determin next
                                lastName = lastName.Trim();
                                string delFile = Path.Combine(fromDir, lastName);
                                int oldNrInQue = nrInQueue;

                                nrInQueue = 0;
                                firstName = "";
                                foreach (FileInfo fi in fileList)
                                {
                                    if (fi.Name == lastName)
                                    {
                                        // delete last file
                                        try
                                        {
                                            File.Delete(delFile);
                                            if (_mbLogExtra)
                                            {
                                                CProgram.sLogLine("Removed Sirona " + lastName + " from queue " + oldNrInQue);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            CProgram.sLogException("Failed device Sirona delete " + delFile, ex);
                                        }
                                    }
                                    else
                                    {
                                        if (firstName.Length == 0)
                                        {
                                            firstName = fi.Name;    // next first name
                                        }
                                        ++nrInQueue;
                                    }
                                }

                            }
                        }
                        if (nrInQueue > 0 && firstName.Length > 0)
                        {
                            // copy new setting
                            string firstFile = Path.Combine(fromDir, firstName);
                            try
                            {
                                File.Copy(firstFile, toFile);

                                File.WriteAllText(storeFile, firstName);
                                CProgram.sLogLine("Copy to Sirona\\command\\ " + firstName + " " + nrInQueue.ToString() + " in queue");
                            }
                            catch (Exception ex)
                            {
                                CProgram.sLogException("Failed copy Sirona " + firstName + " to " + toFile, ex);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed on Sirona\\command\\" + name, ex);
            }
        }

        private bool mbSironaCheckStudyDataFile(string AFilePath, FileInfo AFileInfo, string[] AFileList, string AHeaderText)
        {
            bool bIsDataFile = false;

            // do not use in case of brmw

            try
            {
                int nFilesInList = AFileList == null ? 0 : AFileList.Length;

                if (nFilesInList > 0)
                {
                    string name = Path.GetFileNameWithoutExtension(AFilePath);
                    //                  bool bIsDevice = false;
                    string deviceStr;
                    int i = name.IndexOf('_');

                    if (i <= 0)
                    {
                        string toName = AFilePath + "_badName" + CProgram.sDateTimeToYMDHMS(DateTime.Now);
                        try
                        {
                            if (_mbLogExtra) CProgram.sLogLine(AHeaderText + "BAD name: " + toName);
                            File.Move(AFilePath, toName);
                        }
                        catch (Exception)
                        {

                        }
                    }
                    else    // filename should be of <snr>_<refID>_<date><time>.*
                    {
                        deviceStr = name.Substring(0, i);
                        string rest = name.Substring(i + 1);

                        if (false == CRecDeviceList.sbNotInBlackList(deviceStr))
                        {
                            // black listed
                        }
                        else if (false == CRecDeviceList.mbSironaIsInDeviceList(deviceStr, false))
                        {
                            CProgram.sLogLine(AHeaderText + deviceStr + " not active Sirona devivce: " + name);
                            try
                            {
                                File.Move(AFilePath, AFilePath + "_NoDevice");
                            }
                            catch (Exception)
                            {
                            }
                        }
                        else
                        {
                            string datFile = Path.ChangeExtension(AFilePath, ".dat");
                            //                            bIsDevice = true;

                            for (i = 0; i < nFilesInList; ++i)
                            {
                                if (AFileList[i] == datFile)
                                {
                                    bIsDataFile = true;
                                    break;
                                }
                            }

                            // determin device
                            CDeviceInfo device = new CDeviceInfo(_mDBaseServer);
                            UInt32 studyIX = 0;
                            string studyDir = "";
                            bool bRefIdOK = false;
                            bool bIsRecording = false;
                            bool bDeviceFound = false;

                            try
                            {
                                if (device != null)
                                {
                                    int nSerial = device.mFindLoadSerial(0, deviceStr, 0); // loads device
                                    bDeviceFound = nSerial > 0;
                                    if (nSerial > 0 && device._mActiveStudy_IX > 0)
                                    {
                                        bRefIdOK = device._mActiveRefID.mbIsEmpty();

                                        if (false == bRefIdOK)
                                        {
                                            i = rest.LastIndexOf('_');  // can have '_'in file name

                                            if (i <= 0)
                                            {
                                                // assume empty refID in file
                                            }
                                            else
                                            {
                                                string refID = rest.Substring(0, i);
                                                bRefIdOK = refID == device._mActiveRefID.mDecrypt();
                                            }
                                        }
                                        bIsRecording = device.mbIsRecording();
                                        if (bRefIdOK && bIsRecording)
                                        {
                                            // device is active assigned to a study (can not check time because event is unknown
                                            studyIX = device._mActiveStudy_IX;
                                            if (false == CDvtmsData.sbCreateStudyRecorderDir(out studyDir, studyIX)
                                            || studyDir == null || studyDir.Length < 6)
                                            {
                                                studyIX = 0;
                                            }
                                        }
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                CProgram.sLogLine("Scan directory Sirona failed find device info: " + deviceStr);
                                studyIX = 0;
                            }
                            string datStr = bIsDataFile ? "ECG " : "";

                            // check within study time
                            bool bCopyToStudy = device != null && device._mAddStudyMode == (UInt16)DAddStudyModeEnum.ActiveStudy
                                && AFileInfo.LastWriteTimeUtc >= device._mRecorderStartUTC && AFileInfo.LastWriteTimeUtc <= device._mRecorderEndUTC;

                            if (studyIX == 0 || false == bCopyToStudy)
                            {
                                /*if (_mbLogExtra) */
                                {
                                    string s = datStr + "Sirona Http Ignoring " + deviceStr + " \\ " + name;
                                    if (false == bDeviceFound)
                                    {
                                        s += ", No Device";
                                    }
                                    else
                                    {
                                        if (false == bRefIdOK) s += ", RefID not equal";
                                        if (false == bIsRecording) s += ", Not in recording mode";
                                    }
                                    // to many log files
                                    if (CLicKeyDev.sbDeviceIsProgrammer())
                                    {
                                        CProgram.sLogLine(s);
                                    }
                                }
                            }
                            else
                            {
                                int nCopy = 0, nTry = 0;

                                for (i = 0; i < nFilesInList; ++i)
                                {
                                    string filePathFrom = AFileList[i];
                                    string fileNameFrom = Path.GetFileNameWithoutExtension(filePathFrom);

                                    if (fileNameFrom == name)   // all files name.*
                                    {
                                        string toPath = Path.Combine(studyDir, Path.GetFileName(filePathFrom));


                                        ++nTry;
                                        try
                                        {
                                            FileInfo toInfo = new FileInfo(toPath);
                                            FileInfo fromInfo = new FileInfo(filePathFrom);

                                            if (fromInfo != null && toInfo != null && toInfo.Exists)
                                            //if (File.Exists(toPath))
                                            {
                                                // file already exists

                                                if (toInfo.Length <= fromInfo.Length)
                                                {
                                                    // new file is bigger-> rename old and copy new
                                                    string renPath = toPath + "_old" + CProgram.sDateTimeToYMDHMS(toInfo.LastWriteTime);
                                                    File.Move(toPath, renPath);
                                                    if (_mbLogExtra) CProgram.sLogLine(datStr + "HttpSir rename old: " + renPath);
                                                }
                                                else
                                                {
                                                    // file already exists, move to study with new name
                                                    toPath += "_dbl" + CProgram.sDateTimeToYMDHMS(DateTime.Now);
                                                }
                                            }

                                            if (bIsDataFile)
                                            {
                                                File.Copy(filePathFrom, toPath);
                                                if (_mbLogExtra) CProgram.sLogLine(datStr + "HttpSir copy file to: " + toPath);

                                            }
                                            else
                                            {
                                                File.Move(filePathFrom, toPath);
                                                if (_mbLogExtra) CProgram.sLogLine(datStr + "HttpSir move file to: " + toPath);
                                            }
                                            ++nCopy;
                                            AFileList[i] = ""; // handled
                                        }
                                        catch (Exception ex)
                                        {
                                            CProgram.sLogException("Http Sirona move/copy to " + toPath, ex);

                                            //                                            if (_mbLogExtra) CProgram.sLogLine("HttpSir failed file to: " + toPath);
                                        }
                                    }
                                }
                                CProgram.sLogLine(AHeaderText + (bIsDataFile ? "Copied " : "Moved ") + nCopy.ToString() + (nCopy != nTry ? " / " + nTry.ToString() : "")
                                    + " Sirona Http " + name + " to Study " + studyIX.ToString());
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Http Sirona import failed for " + AFilePath, ex);
            }

            return bIsDataFile;
        }

        UInt32 mScanFromHttpSirDir(double ADelFileAgeHour)
        {
            UInt32 nFiles = 0;
            Int32 nList = 0;
            DateTime dt = DateTime.UtcNow;
            Int32 nDevice = 0;
            string devicePath = null;

            try
            {
                if (CDvtmsData.sbGetDeviceTypeDir(out devicePath, DDeviceType.Sirona))
                {
                    bool bDoAutoCollect = false;

                    if (_mbSirAutoCollectActive)
                    {
                        bDoAutoCollect = _mLastAutoCollectDtSir == DateTime.MinValue;

                        if (false == bDoAutoCollect && CSirAutoCollect._sScanTimeMin > 0)
                        {
                            double tAutoCollectMin = (DateTime.Now - _mLastAutoCollectDtSir).TotalMinutes;

                            if (tAutoCollectMin > CSirAutoCollect._sScanTimeMin) // repeat scan for missing files every x minutes 
                            {
                                bDoAutoCollect = true;
                            }
                        }
                        if (bDoAutoCollect)
                        {
                            _mLastAutoCollectDtSir = DateTime.Now;
                        }
                    }

                    // scan device\<snr>\toServer\<snr>.SirSet voor move naar uploads\<snr>.json 
                    //  en check status\<snr>.json om deze te renamen naar *.SirStatus (del old) en naar device\<snr>\fromServer\<snr>.SirStatus te copieren

                    if (CRecDeviceList.sbSironaDeviceListsCheck(_mHttpSirFromDir, true, false))
                    {
                        Int32 n = CRecDeviceList.sGetSironaDeviceCount();

                        mSetRunTimerProgress("HttpSir cmd", 0, n);
                        if (mbProcessStartLoop("cmd HttpSir", n, true))
                        {
                            int i = 0;
                            foreach (string deviceStr in CRecDeviceList.sGetSironaDeviceList())
                            {
                                mSetRunTimerProgress("HttpSir cmd", ++i, n);
                                if (mbProcessNextLoop(nDevice, true, deviceStr))
                                {
                                    if (CRecDeviceList.sbNotInBlackList(deviceStr))
                                    {
                                        mScanOneSironaCmdStatusDir(deviceStr, bDoAutoCollect);
                                        ++nDevice;
                                    }
                                    else if (_mbLogExtra)
                                    {
                                        CProgram.sLogLine("Scan Sirona cmd " + deviceStr + " black listed!");
                                    }
                                }
                            }
                            mbProcessEndLoop(nDevice, false, nDevice.ToString() + " devices / " + n.ToString());
                            mSetRunTimerProgress("HttpSir cmd", i, n);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Error Scan Sirona directory: " + devicePath, ex);
            }
            double sec = (DateTime.UtcNow - dt).TotalSeconds;

            if (sec > _mWarnScanSec * 0.5F || _mbLogExtra)
            {
                CProgram.sLogLine("Scan Sirona " + nDevice.ToString() + " device cmd in " + sec.ToString("0.0") + " sec");
            }

            // scan for new files
            try
            {
                if (devicePath != null && mbLockHttpSirImportPath())
                {
                    string[] fileList = Directory.GetFiles(_mHttpSirFromDir, "*.*");

                    nList = (fileList == null ? 0 : fileList.Length);

                    if (_mbLogExtra) CProgram.sLogInfo("Scan HttpSir: " + nFiles.ToString() + " files in " + _mHttpSirFromDir);

                    mSetRunTimerProgress("HttpSir", 0, nList);
                    if (mbProcessStartLoop("scan HttpSir", nList, true))
                    {
                        Int32 i = 0;

                        for (; i < nList; ++i)
                        {
                            mSetRunTimerProgress("HttpSir", i, nList);
                            string fileName = fileList[i];
                            // filename can be cleared when it is processed already 
                            if (mbProcessNextLoop(i, true, fileName))
                            {

                                if (fileName != null && fileName.EndsWith(".hea"))
                                {
                                    FileInfo fi = new FileInfo(fileName);

                                    if (fi != null)
                                    {
                                        if (fi != null && fi.Exists)
                                        {
                                            double sec2 = (DateTime.UtcNow - fi.LastWriteTimeUtc).TotalSeconds;

                                            if (sec2 >= _mMinFileAgeSec)
                                            {
                                                string headerText = "HttpSir[" + i.ToString() + "/" + nList.ToString() + "] ";
                                                if (mbSironaCheckStudyDataFile(fileName, fi, fileList, headerText))  // processes files in list
                                                {
                                                    _mFileRunList.Add(fileName);
                                                    fileList[i] = "";   // do not clean
                                                    ++nFiles;
                                                    if (_mbLogExtra) CProgram.sLogLine(headerText + " toDo: " + Path.GetFileName(fileName));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        mbProcessEndLoop(i, false, nFiles.ToString() + " files / " + nList.ToString());
                    }

                    if (ADelFileAgeHour > 6)  // only clean old files when program has scanned for 30 minutes
                    {
                        mSetRunTimerProgress("HttpSir clean", 0, nList);
                        if (mbProcessStartLoop("clean HttpSir", nList, true))
                        {
                            /// delete remaining files if they are old
                            DateTime oldUTC = DateTime.UtcNow.AddHours(-ADelFileAgeHour);
                            int nDeleted = 0, nFailed = 0;
                            Int32 i = 0;
                            Int32 nLoop = nList;
                            for (; i < nLoop; ++i)
                            {
                                mSetRunTimerProgress("HttpSir clean", i, nList);
                                string fileName = fileList[i];
                                // filename can be cleared when it is processed already 
                                if (fileName != null && fileName.Length > 3)
                                {
                                    if (fileName.EndsWith(".Lock"))
                                    {
                                        --nList;
                                    }
                                    else
                                    {
                                        if (mbProcessNextLoop(i, true, fileName))
                                        {
                                            FileInfo fi = new FileInfo(fileName);

                                            if (fi != null && fi.Exists && fi.LastWriteTimeUtc < oldUTC)
                                            {
                                                try
                                                {
                                                    File.Delete(fileName);
                                                    CProgram.sLogInfo(i.ToString() + "/" + nList.ToString() + " Deleted old Sirona " + fileName);
                                                }
                                                catch (Exception)
                                                {
                                                    CProgram.sLogInfo(i.ToString() + "/" + nList.ToString() + "Failed deleted old Sirona " + fileName);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            mbProcessEndLoop(i, false, nList.ToString() + " left / " + nLoop.ToString());

                            if (nDeleted > 0 || nFailed > 0)
                            {
                                CProgram.sLogLine("scan old Http Sirona: " + nDeleted.ToString() + " deleted " + (nFailed == 0 ? "" : ", " + nFailed.ToString() + " faile delete"));
                            }
                        }
                    }
                    _mLastScanDtHttpSir = DateTime.Now;
                }
                else
                {
                    CProgram.sLogLine("Scan directory does not exist: " + _mHttpSirFromDir);
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Error Scan Sirona directory: " + _mHttpSirFromDir, ex);
                nFiles = 0;
            }
            sec = (DateTime.UtcNow - dt).TotalSeconds;

            if (sec > _mWarnScanSec || _mbLogExtra)
            {
                CProgram.sLogLine("Scan Sirona Http " + nFiles.ToString() + " in " + sec.ToString("0.0") + " sec, " + nList.ToString() + " work files");
            }

            return nFiles;
        }

        UInt32 mScanOneSnrFromTZeDir(out UInt32 ArEventFiles, string ASerialNr, string ATimeStr, double ADelFileAgeHour, string AHeaderText)
        {
            DateTime dtUtcNow = DateTime.UtcNow;
            UInt32 nTzeFiles = 0;
            string uploadDir = "";
            string[] fileList = null;
            double tDirList = 0;

            string storeSnr = CDvtmsData.sGetTzStoreSnrName(ASerialNr);
            string remSnr = storeSnr != ASerialNr ? "->" + storeSnr : "";

            ArEventFiles = 0;
            try
            {
                if (_mTzFromDir != null && _mTzFromDir.Length > 3)
                {
                    // get all files from TZ Area upload directory 
                    uploadDir = Path.Combine(_mTzFromDir, storeSnr, "uploads");
                    fileList = Directory.GetFiles(uploadDir);

                    nTzeFiles = (UInt32)(fileList == null ? 0 : fileList.Length);

                    tDirList = (DateTime.UtcNow - dtUtcNow).TotalSeconds;
                }
            }
            catch (Exception)
            {
                // do not care if directory list fails to collect
            }

            if (nTzeFiles > 0)
            {
                double minAgeSec = _mMinFileAgeSec;
                bool bDelOldFile = ADelFileAgeHour >= 6;
                double maxAgeSec = bDelOldFile ? ADelFileAgeHour * 3600 : 1e12; // files older then 12 hours are removed

                #region determin device and study
                // determin device

                CDeviceInfo device = new CDeviceInfo(_mDBaseServer);
                UInt32 studyIX = 0;
                string studyDir = "";

                if (_mbLogExtra) CProgram.sLogInfo(AHeaderText + " Scan snr TZ: " + nTzeFiles.ToString() + " files in " + uploadDir);

                try
                {
                    if (device != null)
                    {
                        int nSerial = device.mFindLoadSerial(0, ASerialNr, 0); // loads device

                        if (nSerial == 0)
                        {
                            string altDeviceStr = CDvtmsData.sGetTzAltSnrName(ASerialNr);
                            if (altDeviceStr != null && altDeviceStr.Length > 0)
                            {
                                nSerial = device.mFindLoadSerial(0, altDeviceStr, 0);// needed for tz??
                                if (nSerial > 0)
                                {
                                    remSnr += "->" + altDeviceStr;
                                }
                            }
                        }

                        if (nSerial > 0 && device._mActiveStudy_IX > 0)
                        {
                            if (device.mbIsRecording())
                            {
                                // device is active assigned to a study (can not check time because event is unknown
                                studyIX = device._mActiveStudy_IX;
                                if (false == CDvtmsData.sbCreateStudyRecorderDir(out studyDir, studyIX)
                                || studyDir == null || studyDir.Length < 6)
                                {
                                    studyIX = 0;
                                }
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    CProgram.sLogLine("Scan directory TZe failed find device: " + ASerialNr + remSnr);
                }
                #endregion

                UInt32 nFailMove = 0;
                string failMoveName = "";
                UInt32 nFailDelete = 0;
                string failDeleteName = "";
                UInt32 nWarnDelete = 0;
                string warnDeleteName = "";
                DateTime logUTC = DateTime.UtcNow;
                //                bool bIsProgrammer = CLicKeyDev.sbDeviceIsProgrammer(); // when programmer you can copy old files


                for (int i = 0; i < nTzeFiles; ++i)
                {
                    #region handle file
                    string filePath = fileList[i];
                    string fileName = Path.GetFileName(filePath);
                    FileInfo fi = null;

                    if ((DateTime.UtcNow - logUTC).TotalSeconds > 2)
                    {
                        Application.DoEvents();
                        logUTC = DateTime.UtcNow;
                    }
                    try
                    {
                        fi = new FileInfo(filePath);
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException("Failed find TZe " + ASerialNr + remSnr + " run file " + filePath, ex);
                        fi = null;
                    }

                    if (fi != null && fi.Length > 10)      // wait for other process to have written the file
                    {
                        double ageSec = Math.Abs((dtUtcNow - fi.LastWriteTimeUtc).TotalSeconds);

                        if (ageSec > minAgeSec)
                        {
                            // check within study time
                            bool bCopyToStudy = device != null && device._mAddStudyMode == (UInt16)DAddStudyModeEnum.ActiveStudy  // only copy files to study when device is active for one study
                                && fi.LastWriteTimeUtc >= device._mRecorderStartUTC && fi.LastWriteTimeUtc <= device._mRecorderEndUTC;

                            if (studyIX > 0 && bCopyToStudy)
                            {
                                // there is a study => copy tze file and move other files to study\Recorder
                                // the scp files must be at the study first because one a file is send it is not resend again.
                                // event can use previous send scp files
                                // SV20171115 copy multiple TZ sesions => add scan YYYYMMDDhhmmss
                                try
                                {
                                    string toFileName = Path.GetFileNameWithoutExtension(fileName) + "_" + ATimeStr + Path.GetExtension(fileName);
                                    string toFilePath = Path.Combine(studyDir, toFileName);

                                    if (filePath.EndsWith(".tze"))
                                    {
                                        mShowTzHrFilter(); // clear filter
                                        File.Copy(filePath, toFilePath);
                                        CProgram.sLogLine(AHeaderText + ": " + i.ToString() + "/" + nTzeFiles.ToString() + ": copied to study " + studyIX.ToString() + " = " + ASerialNr + ": " + toFileName);
                                        _mFileRunList.Add(toFilePath + "$" + filePath);   // add .tze to be converted (still in scan directory)
                                        if (_mbLogExtra) CProgram.sLogLine("Tz S" + studyIX.ToString() + " toDo: " + ASerialNr + remSnr + " \\ " + Path.GetFileNameWithoutExtension(fileName));
                                        fileList[i] = "";
                                        ++ArEventFiles;
                                    }
                                    else
                                    {
                                        // other files move to study
                                        File.Move(filePath, toFilePath);
                                        CProgram.sLogLine(AHeaderText + ": " + i.ToString() + "/" + nTzeFiles.ToString() + ":  moved to study " + studyIX.ToString() + " = " + ASerialNr + remSnr + ": " + toFileName);
                                    }

                                }
                                catch (Exception ex)
                                {
                                    if (nFailMove == 0) failMoveName = fileName + "->S" + studyIX.ToString();
                                    ++nFailMove;
                                    if (_mbLogExtra) CProgram.sLogLine("TZ" + i.ToString() + "/" + nTzeFiles.ToString() + ": Failed Move file " + fileName + "to study " + studyIX.ToString());
                                    //                                                CProgram.sLogException("Failed Move TZ file " + filePath, ex);
                                }
                            }
                            else if (bDelOldFile && ageSec > maxAgeSec && false == filePath.EndsWith(".Lock"))
                            {
                                try
                                {
                                    // old file just discard
                                    File.Delete(filePath);
                                    CProgram.sLogLine("TZ" + i.ToString() + "/" + nTzeFiles.ToString() + ": removed old from uploads: " + ASerialNr + "->" + fileName);
                                }
                                catch (Exception ex)
                                {
                                    if (nFailDelete == 0) failDeleteName = fileName;
                                    ++nFailDelete;
                                    //                                                    CProgram.sLogException("Failed delete old TZ file " + filePath, ex);
                                }
                            }
                            else
                            {
                                // no study => only import events
                                if (filePath.EndsWith(".tze"))
                                {
                                    _mFileRunList.Add(filePath);   // add .tze to be converted
                                    if (_mbLogExtra) CProgram.sLogLine("Tz" + i.ToString() + "/" + nTzeFiles.ToString() + ": S0 toDo: " + ASerialNr + " \\ " + Path.GetFileNameWithoutExtension(fileName));
                                    fileList[i] = "";
                                    ++ArEventFiles;
                                }
                                else if (ageSec > 2 * minAgeSec && ageSec < 3 * minAgeSec && false == filePath.EndsWith(".Lock"))
                                {
                                    if (nWarnDelete == 0) warnDeleteName = fileName;
                                    ++nWarnDelete;
                                    //                                                CProgram.sLogLine("TZ can not move to unknown study: " + deviceStr + "->" + fileName);
                                }
                            }
                        }
                    }
                    #endregion
                }

                if (nFailMove > 0)
                {
                    CProgram.sLogError(AHeaderText + " Failed " + nFailMove.ToString() + " TZ files to move to study "
                        + studyIX.ToString() + " from " + ASerialNr + remSnr + " / " + failMoveName);
                }
                if (nFailDelete > 0)
                {
                    CProgram.sLogError(AHeaderText + " Failed " + nFailDelete.ToString() + " TZ files to delete from "
                        + ASerialNr + remSnr + " / " + failDeleteName);
                }
                if (nWarnDelete > 0)
                {
                    CProgram.sLogError(AHeaderText + "Warning " + nWarnDelete.ToString() + " TZ files not move and planned to delete from "
                        + ASerialNr + remSnr + " / " + warnDeleteName);
                }

            }
            return nTzeFiles;
        }

        private bool mbTzAutoCollectScan(out bool ArbScanOk, out UInt32 ArReqNrFiles, string ADeviceSnr, bool AbRunAutomatic, bool AbLogExtra)
        {
            bool bScanDone = false;
            bool bScanOk = false;
            UInt32 nrReqFilesScp = 0;
            UInt32 nrReqFilesTzr = 0;

            string header = "TZ Auto Collect [" + ADeviceSnr + "]: ";

            DateTime startDT = DateTime.Now;
            string acResult = "";
            bool bLogExtra = _mbLogExtra || CLicKeyDev.sbDeviceIsProgrammer() || AbLogExtra;

            bLogExtra = true;

            try
            {
                CDeviceInfo device = new CDeviceInfo(_mDBaseServer);
                bool bScpScanOk = false;
                bool bScpEnabled = false;
                string scpResult = "";
                bool bScpScanDone = false;
                bool bTzrScanOk = false;
                string tzrResult = "";
                bool bTzrScanDone = false;
                bool bTzrEnabled = false;
                string deviceDir = "";

                if (device != null
                    && _mTzFromDir != null && _mTzFromDir.Length > 0 && CDvtmsData.sbGetDeviceUnitDir(out deviceDir, DDeviceType.TZ, ADeviceSnr, false))
                {

                    int nSerial = device.mFindLoadSerial(0, ADeviceSnr, 0); // loads device

                    if (nSerial == 0 || device.mIndex_KEY == 0)
                    {
                        acResult = "Unknown device";
                    }
                    else if (device._mAddStudyMode == (UInt16)DAddStudyModeEnum.ActiveStudy)    // only auto collect when device is for one study
                    {
                        CTzAutoCollect _mCollectScp = new CTzAutoCollect("SCP", DTzCollectFileFormat.D6, ".scp");
                        if (_mCollectScp != null)
                        {
                            bScpScanOk = _mCollectScp.mbRunCycle(out scpResult, out bScpEnabled, out bScpScanDone, deviceDir, ADeviceSnr, device._mActiveStudy_IX,
                                            device._mRecorderStartUTC, device._mRecorderEndUTC, AbRunAutomatic);
                            if (bLogExtra && bScpEnabled)
                            {
                                CProgram.sLogInfo(header + "SCP= " + scpResult);
                            }
                            if (bScpScanOk)
                            {
                                string s = "SCP " + (bScpEnabled ? (bScpScanDone ? "scanned " : "skipped ") : " AC Disabled ");

                                nrReqFilesScp = _mCollectScp._mActionReqNrFiles;
                                s += _mCollectScp._mActionReqNrFiles.ToString() + "/" + _mCollectScp._mCalcFoundMissing.ToString();
                                if (_mCollectScp._mActionSeqNr > 0)
                                {
                                    s += "{" + _mCollectScp._mActionSeqNr.ToString() + "}";
                                }
                                acResult += s;
                            }
                            else
                            {
                                acResult += "SCP scan not run";
                            }
                        }
                        CTzAutoCollect _mCollectTzr = new CTzAutoCollect("TZR", DTzCollectFileFormat.YYYYMMDD_HH, ".tzr");
                        if (_mCollectTzr != null)
                        {
                            bTzrScanOk = _mCollectTzr.mbRunCycle(out tzrResult, out bTzrEnabled, out bTzrScanDone, deviceDir, ADeviceSnr, device._mActiveStudy_IX,
                                             device._mRecorderStartUTC, device._mRecorderEndUTC, AbRunAutomatic);
                            if (bLogExtra && bTzrEnabled)
                            {
                                CProgram.sLogInfo(header + "TZR= " + tzrResult);
                            }
                            if (bTzrScanOk)
                            {
                                string s = ", TZR " + (bTzrEnabled ? (bTzrScanDone ? "scanned " : "skipped ") : " AC Disabled ");

                                nrReqFilesTzr = _mCollectTzr._mActionReqNrFiles;
                                s += _mCollectTzr._mActionReqNrFiles.ToString() + "/" + _mCollectTzr._mCalcFoundMissing.ToString();
                                if (_mCollectTzr._mActionSeqNr > 0)
                                {
                                    s += "{" + _mCollectTzr._mActionSeqNr.ToString() + "}";
                                }
                                acResult += s;
                            }
                            else
                            {
                                acResult += ", TZR scan not run";
                            }
                        }
                        bScanOk = bScpScanOk || bTzrScanOk;
                        bScanDone = bScpScanDone || bTzrScanDone;
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException(header + "failed!", ex);
                bScanOk = false;
            }
            double tSec = (DateTime.Now - startDT).TotalSeconds;
            if (bScanOk || tSec >= 2.0 || bLogExtra)
            {
                CProgram.sLogLine(header + acResult + " in " + tSec.ToString("0.000") + "sec");
            }

            ArbScanOk = bScanOk;
            ArReqNrFiles = nrReqFilesScp + nrReqFilesTzr;
            return bScanDone;
        }

        UInt32 mScanFromTZeDir(double ADelFileAgeHour)
        {
            UInt32 nScanFiles = 0;
            UInt32 nEventFiles = 0;
            DateTime dtUtcNow = DateTime.UtcNow;
            UInt16 nDevices = 0;

            try
            {
                if (false == mbLockTzImportPath())
                {
                    if (_sbWarnTZeOff)
                    {
                        CProgram.sLogLine("Scan directory TZe is invalid => OFF");
                        _sbWarnTZeOff = false;
                    }
                }
                else
                {
                    string dtNowString = CProgram.sDateTimeToYMDHMS(dtUtcNow);

                    if (CRecDeviceList.sbTzDeviceListsCheck(_mTzFromDir, true, false))
                    {
                        Int32 nList = CRecDeviceList.sGetTzDeviceCount();

                        mSetRunTimerProgress("TZ cmd", 0, nList);
                        if (mbProcessStartLoop("cmd HttpTZ", nList, true))
                        {
                            double tDeviceMaxSec = 0;
                            string nameDeviceMax = "";
                            Int32 i = 0;
                            double t;

                            bool bDoAutoCollect = false;

                            if (_mbTzAutoCollectActive)
                            {
                                bDoAutoCollect = _mLastAutoCollectDtTz == DateTime.MinValue;

                                if (false == bDoAutoCollect && _mTzAutoColScanMin > 0)
                                {
                                    double tAutoCollectMin = (DateTime.Now - _mLastAutoCollectDtTz).TotalMinutes;

                                    if (tAutoCollectMin > _mTzAutoColScanMin) // repeat scan for missing files every x minutes 
                                    {
                                        bDoAutoCollect = true;
                                    }
                                }
                                if (bDoAutoCollect)
                                {
                                    _mLastAutoCollectDtTz = DateTime.Now;
                                }
                            }

                            // move files from TZ Area device directory to server download for all TZ devices

                            foreach (string deviceStr in CRecDeviceList.sGetTzDeviceList())
                            {
                                string storeSnr = CDvtmsData.sGetTzStoreSnrName(deviceStr);
                                string remSnr = storeSnr != deviceStr ? "->" + storeSnr : "";
                                string acResult = "";
                                try
                                {
                                    DateTime dtStart = DateTime.Now;

                                    if (mbProcessNextLoop(i, true, deviceStr))
                                    {
                                        mSetRunTimerProgress("TZ cmd", i, nList);
                                        string altName = CDvtmsData.sGetTzAltSnrName(deviceStr);
                                        if (CRecDeviceList.sbNotInBlackList(deviceStr)
                                            || (altName != null && altName.Length != 0 && CRecDeviceList.sbNotInBlackList(altName))
                                            )
                                        {
                                            ++nDevices;
                                            UInt32 nrReqFiles = 0;

                                            if (bDoAutoCollect)
                                            {
                                                bool bScanOk = false;

                                                if (mbTzAutoCollectScan(out bScanOk, out nrReqFiles, deviceStr, true, false))
                                                {
                                                    acResult = "+ AutoCollect(";
                                                    if (false == bScanOk)
                                                    {
                                                        acResult += "Failed ";
                                                    }
                                                    acResult += nrReqFiles.ToString() + ") ";
                                                }
                                            }
                                            // Move all config and action files from device\TzAera\<snt>\ToServer\ to HttpTZ\<snr>\downloads\
                                            mMoveTzDeviceFilesToServer(deviceStr, 0);

                                            t = (DateTime.Now - dtStart).TotalSeconds;
                                            if (t > _mWarnScanSec * 0.25F || _mbLogExtra || nrReqFiles > 0)
                                            {
                                                CProgram.sLogLine("TZ Scan cmd " + acResult + deviceStr + remSnr + " " + " in " + t.ToString("0.0") + " sec");
                                            }
                                            if (t > tDeviceMaxSec)
                                            {
                                                tDeviceMaxSec = t;
                                                nameDeviceMax = deviceStr;
                                            }
                                        }
                                        else if (_mbLogExtra)
                                        {
                                            CProgram.sLogLine("Scan cmd TZ " + deviceStr + " black listed!");
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    CProgram.sLogException("TZ scan cmd for " + deviceStr + remSnr, ex);
                                }
                                ++i;
                            }
                            mbProcessEndLoop(i, false, nDevices.ToString() + " devices /  " + nList.ToString());
                            mSetRunTimerProgress("TZ cmd", i, nList);
                            t = mProcessLoopDuration();
                            if (t > _mWarnScanSec * 0.5F || _mbLogExtra)
                            {
                                CProgram.sLogLine("Scan cmd TZ " + nDevices.ToString() + " devices in " + t.ToString("0.0") + " sec, max time " + tDeviceMaxSec.ToString("0.00") + " sec by " + nameDeviceMax);
                            }
                        }
                        mSetRunTimerProgress("httpTZ", 0, nList);
                        if (mbProcessStartLoop("scan HttpTZ", nList, true))
                        {
                            double tDeviceMaxSec = 0;
                            string nameDeviceMax = "";
                            Int32 i = 0;
                            double t;
                            UInt32 n, nEvent;
                            // scan files from TZ upload directory  for all TZ devices
                            nDevices = 0;

                            foreach (string deviceStr in CRecDeviceList.sGetTzDeviceList())
                            {
                                string storeSnr = CDvtmsData.sGetTzStoreSnrName(deviceStr);
                                string remSnr = storeSnr != deviceStr ? "->" + storeSnr : "";
                                try
                                {
                                    DateTime dtStart = DateTime.Now;

                                    if (mbProcessNextLoop(i, true, deviceStr))
                                    {
                                        mSetRunTimerProgress("httpTZ", i, nList);
                                        string altName = CDvtmsData.sGetTzAltSnrName(deviceStr);
                                        if (CRecDeviceList.sbNotInBlackList(deviceStr)
                                            || (altName != null && altName.Length != 0 && CRecDeviceList.sbNotInBlackList(altName))
                                            )
                                        {
                                            ++nDevices;
                                            string headerText = "HttpTZ[" + i.ToString() + "/" + nList.ToString() + "] ";
                                            // scan files for event files HttpTZ\<snr>\uploads\
                                            n = mScanOneSnrFromTZeDir(out nEvent, deviceStr, dtNowString, ADelFileAgeHour, headerText);

                                            nScanFiles += n;
                                            nEventFiles += nEvent;
                                            t = (DateTime.Now - dtStart).TotalSeconds;
                                            if (t > _mWarnScanSec * 0.25F || _mbLogExtra)
                                            {
                                                CProgram.sLogLine("Scan TZ " + n.ToString() + " files in " + deviceStr + remSnr + " " + " in " + t.ToString("0.0") + " sec, " + nEvent.ToString() + " possible events");
                                            }
                                            if (t > tDeviceMaxSec)
                                            {
                                                tDeviceMaxSec = t;
                                                nameDeviceMax = deviceStr;
                                            }
                                        }
                                        else if (_mbLogExtra)
                                        {
                                            CProgram.sLogLine("Scan cmd TZ " + deviceStr + " black listed!");
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    CProgram.sLogException("TZ scan files for " + deviceStr + remSnr, ex);
                                }
                                ++i;
                            }
                            mbProcessEndLoop(i, false, nScanFiles.ToString() + " files in " + nDevices.ToString() + " devices / " + nList);
                            mSetRunTimerProgress("httpTZ", i, nList);
                            t = mProcessLoopDuration();
                            if (t > _mWarnScanSec * 0.5F || _mbLogExtra)
                            {
                                CProgram.sLogLine("Scan TZ " + nScanFiles.ToString() + " files in " + nDevices.ToString() + " TZ devices in " + t.ToString("0.0") + " sec, "
                                    + nEventFiles.ToString() + "event,  max time " + tDeviceMaxSec.ToString("0.00") + " sec by " + nameDeviceMax);
                            }
                        }
                        _mLastScanDtTz = DateTime.Now;
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Error Scan TZe directory: " + _mTzFromDir, ex);
                //                nFiles = 0;
            }
            double sec = (DateTime.UtcNow - dtUtcNow).TotalSeconds;

            /*            if (sec > _mWarnScanSec)
                        {
                            CProgram.sLogLine("Scan TZ " + nDevices.ToString() + " devices " + nEventFiles.ToString() + " event files in " + sec.ToString("0.0") + " sec, " + nScanFiles.ToString() + " files in uploads");
                        }
            */
            return nEventFiles;
        }

        UInt32 mScanOneSnrFromDV2Dir(string ASerialNr, double ADelFileAgeHour)
        {
            UInt32 nFiles = 0;
            UInt32 nDV2Files = 0;

            string dirStr = "";
            string[] fileList = null;

            try
            {
                dirStr = Path.Combine(_mDV2FromDir, ASerialNr);

                // get all files from DV2 import directory 
                fileList = Directory.GetFiles(dirStr);

                nDV2Files = (UInt32)(fileList == null ? 0 : fileList.Length);

            }
            catch (Exception)
            {
                // if directory does not exists do not care
            }

            if (nDV2Files > 0)
            {
                #region determin device
                // determin device
                CDeviceInfo device = new CDeviceInfo(_mDBaseServer);
                UInt32 studyIX = 0;
                string studyDir = "";

                if (_mbLogExtra) CProgram.sLogInfo("Scan " + ASerialNr + " DV2: " + nDV2Files.ToString() + " files in " + dirStr);

                try
                {
                    if (device != null)
                    {
                        int nSerial = device.mFindLoadSerial(0, ASerialNr, 0); // loads device

                        if (nSerial > 0 && device._mActiveStudy_IX > 0)
                        {
                            if (device._mState_IX == (UInt16)DDeviceState.Assigned || device._mState_IX == (UInt16)DDeviceState.Intransit || device._mState_IX == (UInt16)DDeviceState.Recording)
                            {
                                // device is active assigned to a study (can not check time because event is unknown
                                studyIX = device._mActiveStudy_IX;
                                if (false == CDvtmsData.sbCreateStudyRecorderDir(out studyDir, studyIX)
                                || studyDir == null || studyDir.Length < 6)
                                {
                                    studyIX = 0;
                                }
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    CProgram.sLogLine("Scan directory DV2 failed find device: " + ASerialNr);
                }
                #endregion
                double minAgeSec = 60 + _mMinFileAgeSec;    // files come in from 
                //                    double maxAgeSec = 18 * 3600; // files older then 12 hours are removed
                DateTime logUTC = DateTime.UtcNow;

                for (int i = 0; i < nDV2Files; ++i)
                {
                    #region handle file
                    string filePath = fileList[i];

                    if (filePath.EndsWith(".scp"))
                    {
                        FileInfo fi = null;

                        if ((DateTime.UtcNow - logUTC).TotalSeconds > 2)
                        {
                            Application.DoEvents();
                            logUTC = DateTime.UtcNow;
                        }
                        try
                        {
                            fi = new FileInfo(filePath);
                        }
                        catch (Exception ex)
                        {
                            CProgram.sLogException("Failed find run file " + filePath, ex);
                            fi = null;
                        }

                        if (fi != null && fi.Length > 10)      // wait for other process (FTP service) to have written the file
                        {
                            double ageSec = (DateTime.UtcNow - fi.LastWriteTime).TotalSeconds;

                            if (Math.Abs(ageSec) > minAgeSec)
                            {
                                _mFileRunList.Add(filePath);   // add .scp to be converted
                                if (_mbLogExtra) CProgram.sLogLine("DV2" + i.ToString() + "/" + nDV2Files.ToString() + ": S" + studyIX.ToString() + " toDo: " + ASerialNr + " \\ " + Path.GetFileName(filePath));

                                fileList[i] = "";
                                ++nFiles;
                            }
                        }
                    }
                    #endregion
                }
            }
            _mLastScanDtDV2 = DateTime.Now;
            return nFiles;
        }

        UInt32 mScanFromDV2Dir(double ADelFileAgeHour)
        {
            UInt32 nFiles = 0;
            DateTime dt = DateTime.UtcNow;
            UInt32 nDevices = 0;

            try
            {
                if (false == mbLockDV2ImportPath())
                {
                    if (_sbWarnDV2Off)
                    {
                        CProgram.sLogLine("Scan directory DV2 is empty => OFF");
                        _sbWarnDV2Off = false;
                    }
                }
                //else //if (Directory.Exists(_mDV2FromDir))
                else if (CRecDeviceList.sbDV2DeviceListsCheck(_mDV2FromDir, true, false))
                {
                    DateTime dtNow = DateTime.Now;
                    Int32 nList = CRecDeviceList.sGetDV2DeviceCount();

                    mSetRunTimerProgress("Scan-DV2", 0, nList);
                    if (mbProcessStartLoop("scan DV2", nList, true))
                    {
                        double t, tDeviceMaxSec = 0;
                        string nameDeviceMax = "";
                        Int32 i = 0;
                        UInt32 n;

                        foreach (string deviceStr in CRecDeviceList.sGetDV2DeviceList())
                        {
                            mSetRunTimerProgress("DV2", 0, nList);
                            try
                            {
                                DateTime dtStart = DateTime.Now;

                                if (mbProcessNextLoop(i, true, deviceStr))
                                {
                                    if (CRecDeviceList.sbNotInBlackList(deviceStr))
                                    {
                                        ++nDevices;
                                        // scan files for event files HttpTZ\<snr>\uploads\
                                        n = mScanOneSnrFromDV2Dir(deviceStr, ADelFileAgeHour);

                                        nFiles += n;
                                        t = (DateTime.Now - dtStart).TotalSeconds;
                                        if (t > _mWarnScanSec * 0.25F || _mbLogExtra)
                                        {
                                            CProgram.sLogLine("Scan DV2[" + nDevices.ToString() + "] " + n.ToString() + " files in " + deviceStr + " " + " in " + t.ToString("0.0") + " sec");
                                        }
                                        if (t > tDeviceMaxSec)
                                        {
                                            tDeviceMaxSec = t;
                                            nameDeviceMax = deviceStr;
                                        }
                                    }
                                    else if (_mbLogExtra)
                                    {
                                        CProgram.sLogLine("Scan DV2 " + deviceStr + " black listed!");
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                CProgram.sLogException("DV2 scan files for " + deviceStr, ex);
                            }
                            ++i;

                        }
                        mbProcessEndLoop(i, false, nFiles.ToString() + " in " + nDevices.ToString());
                        mSetRunTimerProgress("Scan-DV2", i, nList);
                        t = mProcessLoopDuration();
                        if (t > _mWarnScanSec * 0.5F || _mbLogExtra)
                        {
                            CProgram.sLogLine("Scan " + nFiles.ToString() + " files in " + nList.ToString() + " DV2 devices in " + t.ToString("0.0") + " sec, "
                                + "event,  max time " + tDeviceMaxSec.ToString("0.00") + " sec by " + nameDeviceMax);
                        }

                    }

                }
                _mLastScanDtDV2 = DateTime.Now;
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Error Scan DV2[" + nDevices.ToString() + "] directory: " + _mTzFromDir, ex);
                //                nFiles = 0;
            }
            double sec = (DateTime.UtcNow - dt).TotalSeconds;

            /*//if (sec > _mWarnScanSec || _mbLogExtra)
            {
                CProgram.sLogLine("Scan "+ nDevices + " DV2: " + nFiles.ToString() + "in " + sec.ToString("0.0") + " sec");
            }
            */
            return nFiles;
        }
        UInt32 mScanOneSnrFromDVXDir(out string ArDvxStudyInfo, string ASerialNr)
        {
            UInt32 nFiles = 0;
            UInt32 nDVXFiles = 0;
            //            UInt32 studyIX = 0;
            string dvxStudyInfo = "";
            UInt32 dvxStudyNr = 0;

            string[] fileList = null;

            DateTime startUTC = DateTime.UtcNow;
            double tTotal, tExtract, tList, /*tStudy,*/ tScan; 

            string dvxDataPath;
            tTotal = tExtract = tList = tScan = 0;

            if (mbExtractActiveDVXStudy(out dvxStudyNr, out dvxDataPath, ASerialNr))
            {
                dvxStudyInfo = "dvxS" + dvxStudyNr;
                tTotal = (DateTime.UtcNow - startUTC).TotalSeconds;
                tExtract = tTotal;
                if (_mbLogExtra) 
                {
                    CProgram.sLogLine("scan DVX " + ASerialNr + ": " + dvxDataPath);
                }
                /* 
                 * 
                 * Get device study nr
                 * // read device._cEvtRecExtension or default._cEvtRecExtension 
                 * scan device sub dir
                 * find files *._cEvtentExtension without _cEvtRecExtension
                 * 
                 * 
                */
                try
                {
                    // get all files from DVX import directory 
                    fileList = Directory.GetFiles(dvxDataPath);

                    nDVXFiles = (UInt32)(fileList == null ? 0 : fileList.Length);
                    tTotal = (DateTime.UtcNow - startUTC).TotalSeconds;
                    tList = tTotal - tExtract;

                }
                catch (Exception)
                {
                    // if directory does not exists do not care
                }

                if (nDVXFiles > 0)
                {
                    /*
                    #region determin device
                    
                    // determin device
                    CDeviceInfo device = new CDeviceInfo(_mDBaseServer);

                    if (_mbLogExtra) CProgram.sLogInfo("Scan " + ASerialNr + " DVX: " + nDVXFiles.ToString() + " files in " + dvxDataPath);

                    try
                    {
                        if (device != null)
                        {
                            int nSerial = device.mFindLoadSerial(0, ASerialNr, 0); // loads device

                            if (nSerial > 0 && device._mActiveStudy_IX > 0)
                            {
                                if (device._mState_IX == (UInt16)DDeviceState.Assigned || device._mState_IX == (UInt16)DDeviceState.Intransit || device._mState_IX == (UInt16)DDeviceState.Recording)
                                {
                                    // device is active assigned to a study (can not check time because event is unknown
                                    studyIX = device._mActiveStudy_IX;
                                }
                            }
                        }
                    }
                    catch (Exception)
                    {
                        CProgram.sLogLine("Scan directory DVX failed find device: " + ASerialNr);
                    }
                    
                    #endregion
                    tTotal = (DateTime.UtcNow - startUTC).TotalSeconds;
                    tStudy = tTotal - tList;
                    */
                    dvxStudyInfo += " " + nDVXFiles + " files";
                    double minAgeSec = 60 + _mMinDvxFileAgeSec;    // files come in from 
                    DateTime logUTC = DateTime.UtcNow;

                    for (int i = 0; i < nDVXFiles; ++i)
                    {
                        #region handle file
                        string filePath = fileList[i];

                        if (filePath.EndsWith(CDvxEvtRec._cEventExtension))     // *.dat.evt
                        {
                            string datFileName = Path.GetFileNameWithoutExtension(filePath);        // *.dat
                            string recDoneFile = Path.ChangeExtension(filePath, CDvxEvtRec._cEvtRecExtension); // *.dat.DvxEvtRec
                            string recErrorFile = Path.ChangeExtension(recDoneFile, CDvxEvtRec._cEvtErrorExtension);    // *.dat.DvxEvtError
                            string recSkipFile = Path.ChangeExtension(recDoneFile, CDvxEvtRec._cEvtSkipExtension);    // *.dat.DvxEvtSkip

                            bool bAdd = true;

                            if (fileList.Contains(recDoneFile))
                            {
                                bAdd = false;   // already done
                            }
                            else if (fileList.Contains(recErrorFile))
                            {
                                bAdd = false;   // already done with error
                            }
                            else if (fileList.Contains(recSkipFile))
                            {
                                bAdd = false;   // already done with a skip
                            }
                            if (bAdd)
                            {
                                FileInfo fi = null;

                                if ((DateTime.UtcNow - logUTC).TotalSeconds > 2)
                                {
                                    Application.DoEvents();
                                    logUTC = DateTime.UtcNow;
                                }
                                try
                                {
                                    fi = new FileInfo(filePath);
                                }
                                catch (Exception ex)
                                {
                                    CProgram.sLogException("Failed find run file " + filePath, ex);
                                    fi = null;
                                }

                                if (fi != null && fi.Length > 10)      // wait for other process (FTP service) to have written the file
                                {
                                    double ageSec = (DateTime.UtcNow - fi.LastWriteTime).TotalSeconds;

                                    if (Math.Abs(ageSec) > minAgeSec)
                                    {
                                            _mFileRunList.Add(filePath);   // add .evt to be converted
                                            if (_mbLogExtra) CProgram.sLogLine("DVX" + i.ToString() + "/" + nDVXFiles.ToString() /*+ ": S" + studyIX.ToString() */ 
                                                + " toDo: " + ASerialNr + " \\ " + Path.GetFileName(filePath));

                                            fileList[i] = "";
                                            ++nFiles;
                                    }
                                }
                            }
                        }
                        #endregion
                    }
                    tTotal = (DateTime.UtcNow - startUTC).TotalSeconds;
                    tScan = tTotal - tList;

                }
            }
            tTotal = (DateTime.UtcNow - startUTC).TotalSeconds;
            if (_mbLogExtra || tTotal > 3)
            {
                string s = "scan DVX " + ASerialNr + ": t=" + tTotal.ToString("0.00") + " tE=" + tExtract.ToString("0.00")
                    + " tL[" + nDVXFiles + "]=" + tList.ToString("0.00")
                    /*+ " tStudy["+ studyIX + "]=" + tStudy.ToString("0.00")*/  + " tScan=" + tScan.ToString("0.00") + "->" + nFiles;
                CProgram.sLogLine(s);
            }
            ArDvxStudyInfo = dvxStudyInfo;
            return nFiles;
        }

        bool mbExtractActiveDVXStudy( out UInt32 ArDvxStudyIX, out string ArDvxDataPath, string ADvxSNR)
        {
            bool bOk = false;
            UInt32 dvxStudyIX = 0;
            string dvxStudyDataPath = "";

            if (ADvxSNR != null && ADvxSNR.Length > 1)
            {
                string dvxStudyRoot = _mDVXStudyDir;
                string dvxDeviceConfigFile;

                if (dvxStudyRoot == null || dvxStudyRoot.Length <= 2)
                {
                    dvxStudyRoot = Path.GetFullPath(Path.Combine(_mRecordingDir, ".."));
                }

                try
                {
                    dvxDeviceConfigFile = Path.Combine(dvxStudyRoot, CDvxEvtRec._cDvxDeviceFolder, ADvxSNR, "Config", "common.config");

                    if (File.Exists(dvxDeviceConfigFile))
                    {
                        Byte[] configData = File.ReadAllBytes(dvxDeviceConfigFile);
                        int nBytes = configData.Length;

                        /* c:\Projects\MonitorSystem\monitor\TechMedic.Utility\WriteConfig.cs 
                         * writer 
                                     using (BinaryWriter writer = new BinaryWriter(File.Open(filePath, FileMode.Create)))
                {
                   0 WriteInt32(writer, settings.SerialNumber);
                   4 WriteIp(writer, settings.ServerIp);
                   8 WriteInt16(writer, settings.FtpPort);
                    10WriteInt16(writer, settings.UdpPort);
                    12WriteString(writer, settings.FtpUsername);
                    44WriteString(writer, settings.FtpPassword);
                    76WriteString(writer, settings.StudyFolder);
                    WriteString(writer, settings.WifiSsid);
                    WriteString(writer, settings.WifiPassword);
                    WriteString(writer, settings.CellularApnAddress);
                    WriteString(writer, settings.CellularName);
                    WriteString(writer, settings.CellularPassword);
                    WriteInt16(writer, settings.CellularPin);
                    WriteInt16(writer, settings.PostEventInterval);
                    WriteInt16(writer, settings.HolterInterval);
                    writer.Write(settings.UseVibration);
                    WriteInt8(writer, settings.RetriesOnFail);
                    WriteInt16(writer, 2700);
                    WriteInt16(writer, 4095);
                    WriteInt8(writer, settings.ConnectionMode);
                    WriteInt8(writer, settings.QrsChannel);
                    WriteInt8(writer, settings.RespirationChannel);
                    WriteInt8(writer, settings.GpsMode);
                    WriteInt32(writer, settings.DeviceMode);
                    //TODO: Ntp Server Address
                    WriteIp(writer, settings.ServerIp);
                    WriteInt32(writer, settings.NtpPort);
                }
 */
                        int startString = 76;
                        int maxString = 32;
                        string relStudy = "";


                        if (nBytes < startString + maxString)
                        {
                            CProgram.sLogError("Bad config file for DVX snr: " + ADvxSNR);

                        }
                        else
                        {
                            for (int i = 0; i < maxString; ++i)
                            {
                                char c = (char)configData[startString + i];

                                if (c == '\0')
                                {
                                    break;
                                }
                                if (i > 0 || c != '\\') // remove first '\' char
                                {
                                    relStudy += c;
                                }
                            }


                            if (relStudy.Length <= 1)
                            {
                                CProgram.sLogLine("No DVX Study in config for DVX snr: " + ADvxSNR);
                            }
                            else
                            {
                                dvxStudyDataPath = Path.Combine(dvxStudyRoot, relStudy, ADvxSNR);

                                bOk = Directory.Exists(dvxStudyDataPath);

                                if( bOk )
                                {
                                    // current format \00\00\05\000005\DVX\
                                    // 

                                    try
                                    {
                                        int len = relStudy.Length;
                                        if (len >= 11 && relStudy.EndsWith("\\DVX\\"))
                                        {
                                            int pos = len - 11;
                                            string dvxS = relStudy.Substring(pos, 6);
                                            UInt32.TryParse(dvxS, out dvxStudyIX);
                                        }
                                    }
                                    catch (Exception)
                                    {
                                    }
                                }

                                if (false == bOk)
                                {
                                    CProgram.sLogError("DVX snr: " + ADvxSNR + ", unknown study path " + dvxStudyDataPath);
                                }
                            }
                        }

                    }
                    else
                    {
                        CProgram.sLogError("No config for DVX snr: " + ADvxSNR + "@ " + dvxDeviceConfigFile);

                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Extract DVX current study failed: " + ADvxSNR, ex);
                }
            }
            else
            {
                CProgram.sLogError("Bad DVX snr: " + ADvxSNR);
            }

            ArDvxDataPath = dvxStudyDataPath;
            ArDvxStudyIX = dvxStudyIX;
            return bOk;
        }

        UInt32 mScanFromDVXDir()
        {
            UInt32 nFiles = 0;
            DateTime dt = DateTime.UtcNow;
            UInt32 nDevices = 0;

            try
            {
                if (false == mbLockDVXImportPath())
                {
                    CProgram.sLogError("DVX lock import failed");
                }
                //else //if (Directory.Exists(_mDV2FromDir))
                else if (CRecDeviceList.sbDVXDeviceListsCheck(true, false))
                {
                    DateTime dtNow = DateTime.Now;
                    Int32 nList = CRecDeviceList.sGetDVXDeviceCount();

                    mSetRunTimerProgress("Scan-DVX", 0, nList);
                    if (mbProcessStartLoop("scan DVX", nList, true))
                    {
                        double t, tDeviceMaxSec = 0;
                        string nameDeviceMax = "";
                        Int32 i = 0;
                        UInt32 n;

                        foreach (string deviceStr in CRecDeviceList.sGetDVXDeviceList())
                        {
                            mSetRunTimerProgress("DVX", 0, nList);
                            try
                            {
                                DateTime dtStart = DateTime.Now;

                                if (mbProcessNextLoop(i, true, deviceStr))
                                {
                                    if (CRecDeviceList.sbNotInBlackList(deviceStr))
                                    {
                                        string dvxStudyInfo;
                                        ++nDevices;
                                        // scan files for event files HttpTZ\<snr>\uploads\
                                        n = mScanOneSnrFromDVXDir(out dvxStudyInfo, deviceStr);

                                        nFiles += n;
                                        t = (DateTime.Now - dtStart).TotalSeconds;
                                        if (t > _mWarnScanSec * 0.25F || _mbLogExtra)
                                        {
                                            CProgram.sLogLine("Scan DVX[" + nDevices.ToString() + "] " + n.ToString() 
                                                + " files in " + deviceStr + " " + dvxStudyInfo + " in " + t.ToString("0.0") + " sec");
                                        }
                                        if (t > tDeviceMaxSec)
                                        {
                                            tDeviceMaxSec = t;
                                            nameDeviceMax = deviceStr;
                                        }
                                    }
                                    else if (_mbLogExtra)
                                    {
                                        CProgram.sLogLine("Scan DVX " + deviceStr + " black listed!");
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                CProgram.sLogException("DVX scan files for " + deviceStr, ex);
                            }
                            ++i;

                        }
                        mbProcessEndLoop(i, false, nFiles.ToString() + " in " + nDevices.ToString());
                        mSetRunTimerProgress("Scan-DVX", i, nList);
                        t = mProcessLoopDuration();
                        if (t > _mWarnScanSec * 0.5F || _mbLogExtra)
                        {
                            CProgram.sLogLine("Scan " + nFiles.ToString() + " files in " + nList.ToString() + " DVX devices in " + t.ToString("0.0") + " sec, "
                                + "event,  max time " + tDeviceMaxSec.ToString("0.00") + " sec by " + nameDeviceMax);
                        }
                    }

                }
                _mLastScanDtDVX = DateTime.Now;
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Error Scan DVX[" + nDevices.ToString() + "] directory: " + _mDVXStudyDir, ex);
                //                nFiles = 0;
            }
            double sec = (DateTime.UtcNow - dt).TotalSeconds;

            return nFiles;
        }

        double mCalcDelFileAgeHour(ref DateTime ArStartScanDT, double AAgeFileHour, int AWaitUntilRunTimeMin)
        {
            double delFileAge = 876000;   // 100 * 365 * 24 = 876000 hour

            if (ArStartScanDT == DateTime.MinValue)
            {
                ArStartScanDT = DateTime.Now;
            }
            else if (AAgeFileHour > 6.0 && AWaitUntilRunTimeMin > 0) // lower is off
            {
                int waitMin = AWaitUntilRunTimeMin < 10 ? 10 : AWaitUntilRunTimeMin;
                double runMin = (DateTime.Now - ArStartScanDT).TotalMinutes;
                if (runMin >= waitMin)
                {
                    delFileAge = AAgeFileHour;
                }
            }
            return delFileAge;
        }

        // still needed a standard delete old files and implement them into functions
        // using DirectoryINFO getFiles => FileInfo[]

        public void mTimerScanFromDirs()
        {
            _mRunN = 0;
            if (_mbRun)
            {
                UInt32 nFilesZip = 0;
                UInt32 nFilesTZe = 0;
                UInt32 nFilesDV2 = 0;
                UInt32 nFilesDVX = 0;
                UInt32 nFilesHttpSir = 0;

                _mRunScanTime = DateTime.Now;
                mStatusLine("Scanning for files...");
                //                DateTime startTime = DateTime.Now;
                try
                {
                    mProcessSetInfo(">>scan");
                    _mRunN = 0;
                    _mRunIndex = 0;
                    _mbRunLoop = false;

                    if (_mFileRunList == null)
                    {
                        _mFileRunList = new List<string>();
                    }

                    if (_mFileRunList != null)
                    {
                        _mFileRunList.Clear();
                        if (_mAutoRunSirZip == (UInt16)DAutoRun.On)
                        {
                            mSetRunTimerProgress("Scan-SirZip", 0, 0);
                            mProcessSetInfo(">>scan SirZip");
                            double delFileAgeHour = mCalcDelFileAgeHour(ref _mStartScanDtSir, _mMaxFileAgeHour, _mWaitDelRunTimeMin);

                            nFilesZip = mScanFromZipDir(delFileAgeHour);
                        }
                        if (_mAutoRunTZe == (UInt16)DAutoRun.On)
                        {
                            mSetRunTimerProgress("Scan-TZ", 0, 0);
                            mProcessSetInfo(">>scan TZ");
                            double delFileAgeHour = mCalcDelFileAgeHour(ref _mStartScanDtTz, _mMaxFileAgeHour, _mWaitDelRunTimeMin);
                            nFilesTZe = mScanFromTZeDir(delFileAgeHour);
                        }
                        if (_mAutoRunDV2 == (UInt16)DAutoRun.On)
                        {
                            mSetRunTimerProgress("Scan-DV2", 0, 0);
                            mProcessSetInfo(">>scan DV2");
                            double delFileAgeHour = mCalcDelFileAgeHour(ref _mStartScanDtDV2, _mMaxFileAgeHour, _mWaitDelRunTimeMin);

                            nFilesDV2 = mScanFromDV2Dir(delFileAgeHour);
                        }
                        if (_mAutoRunDVX == (UInt16)DAutoRun.On)
                        {
                            mSetRunTimerProgress("Scan-DVX", 0, 0);
                            mProcessSetInfo(">>scan DVX");

                            nFilesDVX = mScanFromDVXDir();
                        }
                        if (_mAutoRunHttpSir == (UInt16)DAutoRun.On)
                        {
                            mSetRunTimerProgress("Scan-HttpSir", 0, 0);
                            mProcessSetInfo(">>scan HSir");
                            double delFileAgeHour = mCalcDelFileAgeHour(ref _mStartScanDtHttpSir, _mMaxFileAgeHour, _mWaitDelRunTimeMin);

                            nFilesHttpSir = mScanFromHttpSirDir(delFileAgeHour);
                        }
                        _mRunN = _mFileRunList.Count();
                        _mRunIndex = 0;
                    }
                    if (_mRunN > 0)
                    {
                        mbProcessStartLoop("File", _mRunN, false);
                        //toolStripProcessTotal.Text = _mRunN.ToString();
                        _mbRunLoop = true;   // next timer convert this file
                        mSetRunIndex(0);
                    }
                    else
                    {
                        _mbRunLoop = false;
                        mSetRunIndex(0);
                        mProcessSetInfo("<<scan done");
                    }

                }
                catch (Exception e)
                {
                    CProgram.sLogException("Failed to scan dir", e);
                }

                string sLog = "";
                if (nFilesZip > 0) { sLog += nFilesZip.ToString() + " Zip"; }
                if (nFilesTZe > 0) { if (sLog.Length > 0) sLog += ", "; sLog += nFilesTZe.ToString() + " Tze"; }
                if (nFilesDV2 > 0) { if (sLog.Length > 0) sLog += ", "; sLog += nFilesDV2.ToString() + " DV2"; }
                if (nFilesDVX > 0) { if (sLog.Length > 0) sLog += ", "; sLog += nFilesDVX.ToString() + " DVX"; }
                if (nFilesHttpSir > 0) { if (sLog.Length > 0) sLog += ", "; sLog += nFilesHttpSir.ToString() + " HSir"; }
                if (sLog.Length == 0) { sLog = "none"; }
                mStatusLine("Scan found " + sLog + ": " + _mRunN.ToString() + " files for conversion.");

                double scanSec = (DateTime.Now - _mRunScanTime).TotalSeconds;

                if (_mRunN > 0 || scanSec > 4)
                {
                    CProgram.sLogLine(">>>>Scan & copy found " + sLog + ": " + _mRunN.ToString() + " files in " + scanSec.ToString("0.0") + " Sec");
                    CProgram.sLogLine(">>>>");
                }
                if (scanSec < 2)
                {
                    int t = (int)((2 - scanSec) * 1000);
                    if (t > 1)
                    {
                        Thread.Sleep(t);    // give Notify and title time to be viewed
                    }
                }
            }
            else
            {
                _mbRunLoop = false;
            }
        }

        private void mTimerRunLoop()
        {
            if (_mbRun == false)
            {
                _mbRunLoop = false;
                _mLastRunTime = DateTime.Now;
                _mStartRunTimeDT = DateTime.MinValue;
                mbProcessExit("");

            }
            else
            {
                DateTime dtNow = DateTime.Now;
                _mLastRunTime = dtNow;
                _mbRunningConversion = true;
                #region importRunFile
                string filePath = "?path";
                string fileName = "?file";
                Int16 timeZoneOffset = CProgram.sGetLocalTimeZoneOffsetMin();

                if (_mRunIndex <= 1)
                {
                    mbProcessEnter("File", _mRunN, false);
                    mbProcessStartLoop("File", _mRunN, false);
                }

                try
                {
                    filePath = mGetRunFilePath();
                    fileName = mGetRunFileName();


                    if (mbProcessNextLoop(_mRunIndex, true, fileName))
                    {
                        mSetRunTimerProgress("import", _mRunIndex, _mRunN);
                        if (filePath != null && filePath.Length > 0)
                        {
                            FileInfo fi = null;
                            int pos = _mRunFilePath.IndexOf('$');   // could hold double file path

                            string testFilePath = pos >= 0 ? _mRunFilePath.Substring(0, pos) : _mRunFilePath;

                            try
                            {
                                fi = new FileInfo(testFilePath);
                            }
                            catch (Exception ex)
                            {
                                CProgram.sLogException("Failed find run file " + testFilePath, ex);
                                fi = null;
                            }

                            if (fi != null && fi.Length > 10)      // wait for other process (FTP service) to have written the file
                            {
                                double ageSec = (dtNow - fi.LastWriteTime).TotalSeconds;

                                if (Math.Abs(ageSec) > _mMinFileAgeSec)
                                {
                                    DateTime utcNow = DateTime.UtcNow;

                                    while (utcNow.Hour >= 23 && utcNow.Minute >= 59 && utcNow.Second >= 55)
                                    {
                                        Thread.Sleep(1000); // prevent import during date change
                                        utcNow = DateTime.UtcNow;
                                    }

                                    if (filePath.EndsWith(".zip"))
                                    {
                                        mConvertIntriconZipFile(filePath, 0, DImportMethod.Automatic);
                                    }
                                    else if (filePath.EndsWith(".tze"))     // could hold double file name (study+scan)
                                    {

                                        mImportTZeFile(filePath, DImportMethod.Automatic, true, true);
                                    }
                                    else if (filePath.EndsWith(".scp"))
                                    {
                                        mImportDV2File(filePath, 0, DImportMethod.Automatic, timeZoneOffset);
                                    }
                                    else if (filePath.EndsWith(CDvxEvtRec._cEventExtension))
                                    {
                                        mbImportOneDVXStudyEvent(filePath, DImportMethod.Automatic, true);
                                    }
                                    else if (filePath.EndsWith(".hea"))
                                    {
                                        mConvertHttpSirFile(filePath, DImportMethod.Automatic);
                                    }
                                    else
                                    {
                                        CProgram.sLogLine("Skip unknown type " + fileName + " age is " + ageSec.ToString("0.0") + " Sec");
                                    }
                                }
                                else
                                {
                                    CProgram.sLogLine("Skip " + fileName + " age is " + ageSec.ToString("0.0")
                                        + " < " + _mMinFileAgeSec.ToString("0.0") + " Sec");
                                }
                            }
                            else
                            {
                                CProgram.sLogLine("Skip " + fileName + " file is " + (fi == null ? 0 : fi.Length).ToString() + " bytes");
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Failed handle run file " + filePath, ex);
                }
                #endregion  importFRunFile
                _mbRunningConversion = false;
                int iNext = _mRunIndex + 1;
                mSetRunIndex(iNext);
                if (iNext >= _mRunN)
                {
                    TimeSpan dT = DateTime.Now - _mRunScanTime;
                    CProgram.sLogLine("<<<");
                    CProgram.sLogLine("<<<<Processed " + _mRunN.ToString() + " in " + dT.TotalSeconds.ToString("0.0") + " Sec");
                    CProgram.sLogLine("<<<");
                    _mbRunLoop = false;
                    mbProcessExit("Run import done " + _mRunN.ToString() + " files");
                }
            }

        }

        private void mSetRunColor(Color AColor)
        {
            if (toolStripStop.BackColor != AColor)
            {
                toolStripStop.BackColor = AColor;
            }
        }

        private void mCheckDiskMemSrvr(bool AbTest)
        {
            bool bFirst = _mLastDriveTest == DateTime.MinValue;
            if (AbTest || bFirst || (DateTime.Now - _mLastDriveTest).TotalMinutes > _mCheckSystemMin) //every 15 minutes check harddisk and free space
            {
                string showText = "test: ";
                _mLastDriveTest = DateTime.Now;

                string resultDrive;

                bool bWarn = false == CProgram.sbCheckFreeDriveSpace(out resultDrive, _mCheckDiskLocalGB, _mRecordingDir, _mCheckDiskDataGB, bFirst);

                showText += resultDrive + ", ";
                string resultMem;

                bWarn |= false == CProgram.sbCheckMemSize(out resultMem, "chkDskMem", bFirst);

                showText += resultMem + ", ";

                if (mbTestDB())
                {
                    showText += "DB, ";
                }
                else
                {
                    showText += "-DB!, ";
                    bWarn = true;
                }
                string content;
                string url = _mCheckServerSironaUrl;
                bool bTestUrl = url != null && url.ToLower().StartsWith("http");

                if (false == bTestUrl && CLicKeyDev.sbDeviceIsProgrammer())
                {
                    url = "https://nl0022demo.dvtms.com:63180/hello";    // Sirona
                    bTestUrl = true;
                }
                if (bTestUrl)
                {
                    if (false == CProgram.sbTestHttpConnectFromUrl(out content, url))
                    {
                        showText += "-svrSir!, ";
                        bWarn = true;
                    }
                    else
                    {
                        showText += "+svrSir, ";
                        sWriteLastScanFile(_mRecordingDir, "SirServer");
                        CProgram.sLogLine("Written LastScan SirServer " + CProgram.sDateTimeToYMDHMS(_mLastDriveTest));
                    }
                }
                url = _mCheckServerTzUrl;
                bTestUrl = url != null && url.ToLower().StartsWith("http");

                if (false == bTestUrl && CLicKeyDev.sbDeviceIsProgrammer())
                {
                    url = "http://nl0022demo.dvtms.com:63129/hello";    // tz
                    bTestUrl = true;
                }
                if (bTestUrl)
                {
                    if (false == CProgram.sbTestHttpConnectFromUrl(out content, url))
                    {
                        showText += "-svrTZ!, ";
                        bWarn = true;
                    }
                    else
                    {
                        showText += "+svrTZ, ";
                        sWriteLastScanFile(_mRecordingDir, "TzServer");
                        CProgram.sLogLine("Written LastScan TzServer " + CProgram.sDateTimeToYMDHMS(_mLastDriveTest));
                    }
                }

                int daysLeft = CLicKeyDev.sGetDaysLeft();

                showText += "dl= " + daysLeft.ToString() + " =";

                double tTest = (DateTime.Now - _mLastDriveTest).TotalSeconds;
                showText += tTest.ToString("0.0") + "s";

                CProgram.sMemoryCleanup(AbTest, AbTest);
                CProgram.sLogInfo(showText + " @" + _mCheckSystemMin.ToString() + "min");

                toolStripButtonWarn.Text = bWarn ? "!" : ".";
                toolStripButtonWarn.ToolTipText = showText;
            }
        }

        private void mCheckListSize()
        {
            int n = 0;

            if (dataGridView.Rows != null)
            {
                n = dataGridView.Rows.Count;
            }

            if (n < _mMaxNrRowsListed)
            {
                labelListInfo.Text = n.ToString() + " rows < " + _mMaxNrRowsListed.ToString();

            }
            else if (n == _mMaxNrRowsListed)
            {
                labelListInfo.Text = n.ToString() + " rows at max";

            }
            else
            {
                labelListInfo.Text = n.ToString() + " rows > " + _mMaxNrRowsListed.ToString();

                int i = n / 100 + 1;

                try
                {
                    /*                  dataGridView.Visible = false;
                                      dataGridView.Enabled = false;
                                      while( --i >= 0 )
                                      {
                                          dataGridView.Rows.RemoveAt(0);

                                      }
                                      dataGridView.Enabled = true;
                                      dataGridView.Visible = true;
                    */
                }
                catch (Exception ex)
                {

                }
            }
        }
        private void mLoadIcons()
        {
            _mIconMain = mLoadOneIcon("I-Reader", this.Icon);
            _mIconRun = mLoadOneIcon("I-Reader-Run", _mIconMain);
            _mIconWait = mLoadOneIcon("I-Reader-Wait", _mIconMain);
            _mIconBreak = mLoadOneIcon("I-Reader-Break", _mIconMain);
            _mIconStopped = mLoadOneIcon("I-Reader-Stopped", _mIconMain);
            _mIconScan = mLoadOneIcon("I-Reader-Scan", _mIconMain);
            _mNotifyIcon = new NotifyIcon();

            if (_mNotifyIcon != null)
            {
                _mNotifyHeader = CProgram.sGetOrganisationLabel() + "\r\n" + CProgram.sGetProgRunName();
                _mNotifyText = _mNotifyHeader;
                _mProgRunState = ""; // Stopped / Wait / Scan / Run
                _mNotifyDT = DateTime.Now;

                _mNotifyIcon.Icon = _mIconStopped;
                _mNotifyIcon.Visible = false;   // only visible  when IReader is in RUN mode
                _mNotifyIcon.Text = _mNotifyText;

                // Handle the DoubleClick event to activate the form.
                _mNotifyIcon.DoubleClick += new System.EventHandler(this.notifyIcon1_DoubleClick);

                /*
                 NL0000ABCD
                 IReader[4] 
                 scan 22:33:44
                 23s TZ 12 / 345
*/
            }
        }

        private void mSetRunTimerCheckState(string AState, bool bStopped = false)
        {
            if (AState != _mProgRunState)
            {
                _mProgRunState = AState;
                _mNotifyDT = DateTime.Now;
                CProgram.sSetProgramTitle(_mProgMainTitle + " " + _mProgRunState, false);
            }
            if (_mNotifyIcon != null)
            {
                int sec = (int)(0.5 + (DateTime.Now - _mNotifyDT).TotalSeconds);
                _mNotifyText = _mNotifyHeader + "\r\n" + AState;

                _mNotifyIcon.Text = _mNotifyText
                    + (bStopped ? " " + CProgram.sTimeToString(_mNotifyDT) : " " + sec.ToString() + "s");

                Application.DoEvents();
            }
        }

        private void mSetRunTimerScanState(string AState)
        {
            Color color = Color.Yellow;
            Icon icon = _mIconScan;
            bool bIconVisible = true;
            if (logBox.BackColor != color)
            {
                logBox.BackColor = color;
            }
            if (icon != null)
            {
                if (this.Icon != icon)
                {
                    this.Icon = icon;
                }

                if (_mNotifyIcon != null)
                {
                    if (_mNotifyIcon.Icon != icon)
                    {
                        _mNotifyIcon.Icon = icon;
                    }
                    if (_mNotifyIcon.Visible != bIconVisible)
                    {
                        _mNotifyIcon.Visible = bIconVisible;
                    }
                }
            }
            mSetRunTimerCheckState(AState);
        }
        private void mSetRunTimerProgress(string ALabel, int AIndex, int ATotalN)
        {
            if (_mNotifyIcon != null)
            {
                int sec = (int)(0.5 + (DateTime.Now - _mNotifyDT).TotalSeconds);

                string s = _mNotifyText + " " + sec.ToString() + "s\r\n" + ALabel;
                if (ATotalN > 0)
                {
                    if (AIndex >= ATotalN)
                    {
                        s += " #" + ATotalN.ToString();
                    }
                    else
                    {
                        s += " " + AIndex + " / " + ATotalN.ToString();
                    }
                }
                int len = s.Length;
                if (len > 50)
                {
                    int i = len;
                }
                if (len > 63)
                {
                    s = s.Substring(0, 63) + ">";
                }
                _mNotifyIcon.Text = s;
            }
        }


        private void mCheckRunTimerState()

        {
            Color color = Color.White;
            int timerMilSec = 1000;
            string state = "";
            Icon icon = _mIconMain;
            bool bIconVisible = true;
            bool bStopped = false;

            if (_mbRun)
            {
                if (_mbRunLoop)
                {
                    // running scan loop
                    color = Color.LightGreen;
                    timerMilSec = _mMainTimerMilSec;
                    state = "Run";
                    icon = _mIconRun;
                }
                else
                {
                    // waiting for scan loop to start
                    color = Color.LightBlue;
                    timerMilSec = _mPauseTimerMilSec;
                    state = "Wait";
                    icon = _mIconWait;
                }
            }
            else
            {
                if (_mbRunLoop)
                {
                    // running scan loop
                    color = Color.LightPink;
                    timerMilSec = _mMainTimerMilSec;
                    state = "Break";
                    icon = _mIconBreak;
                }
                else
                {
                    // waiting for scan loop to start
                    color = Color.Orange;
                    timerMilSec = _mPauseTimerMilSec;
                    state = "Stopped";
                    icon = _mIconStopped;
                    bIconVisible = true; // show stopped as well
                    bStopped = true;
                }
            }
            if (timer.Interval != timerMilSec)
            {
                timer.Interval = timerMilSec;
            }
            if (logBox.BackColor != color)
            {
                logBox.BackColor = color;
            }
            if (icon != null)
            {
                if (this.Icon != icon)
                {
                    this.Icon = icon;
                }

                if (_mNotifyIcon != null)
                {
                    if (_mNotifyIcon.Icon != icon)
                    {
                        _mNotifyIcon.Icon = icon;
                    }
                    if (_mNotifyIcon.Visible != bIconVisible)
                    {
                        _mNotifyIcon.Visible = bIconVisible;
                    }
                }

            }
            mSetRunTimerCheckState(state, bStopped);
        }


        private void timer_Tick(object sender, EventArgs e)
        {
            mTimerTick();
        }

        private void mTimerTick()
        {
            try
            {
                timer.Enabled = false;

                if (_mbTimerEnterd)
                {
                    CProgram.sLogError("Reentry of timer detected");
                    mSetRunColor(_cColorError);
                }

                _mbTimerEnterd = true;
                DateTime dtNow = DateTime.Now;
                int daysLeft = CLicKeyDev.sGetDaysLeft();

                TimeSpan span = dtNow - _mLastRunTime;
                int dt = _mCheckIntervalSec - (int)span.TotalSeconds;

                if (daysLeft < 0)
                {
                    if (_mbRun)
                    {
                        mSetRun(false);
                    }
                    mProcessCheckExit();
                    mSetRunColor(Color.Orange);
                    labelUtcTime.BackColor = Color.Red;
                    toolStrip.BackColor = Color.Red;

                    mProcessSetInfo("License expired");
                    CProgram.sLogError("License expired");
                }
                else
                {
                    if (_mLogFileDT.Year != dtNow.Year || _mLogFileDT.Month != dtNow.Month || _mLogFileDT.Day != dtNow.Day)
                    {
                        _mLogFileDT = dtNow;
                        CProgram.sCreateNewLogFile("New day " + CProgram.sDateToString(_mLogFileDT));
                    }
                    if ((DateTime.Now - _mTimerUpdDT).TotalSeconds >= 5)
                    {
                        _mTimerUpdDT = DateTime.Now;
                        mCheckListSize();
                        mUpdateMemUse();            // show mem on screen
                    }
                    if (_mbRunLoop)
                    {
                        mSetRunColor(_cColorRunFile);
                        mTimerRunLoop();
                        _mLastRunTime = DateTime.Now;

                        int warnSec = 30 * 60;
                        if (_mbRunLoop && dt <= -warnSec)   // warn after 30 min running
                        {
                            CProgram.sLogWarning("Run loop continues and no check test for " + (-dt / 60) + " min");
                            _mLastRunTime = dtNow.AddSeconds(-warnSec / 2); // repeat warn after half the time
                            mCheckDiskMemSrvr(false);   // log drive and mem
                        }
                        if( false == _mbRunLoop && _mbCursorFixEnd)
                        {
                            mMoveCursorToEnd();
                        }
                    }
                    else if (dt <= 0)
                    {
                        mStatusLine(".");
                        _mLastRunTime = DateTime.Now;

                        if (_mbRun && (daysLeft > 0 || dtNow.Hour < 23))  // stop scanning in last hour of license
                        {
                            mProcessSetInfo("..");

                            if (mbTestDB())
                            {
                                mSetRunColor(_cColorRunScan);
                                mSetRunTimerScanState("Scan");
                                mbProcessEnter("Scan", 0, false);
                                mTimerScanFromDirs();
                                mbProcessExit("Scan done, " + (_mRunN > 0 ? "importing " + _mRunN.ToString() + " files" : " no files"));

                                span = DateTime.Now - _mLastRunTime;
                                dt = _mCheckIntervalSec - (int)span.TotalSeconds;
                                if (dt < 4)
                                {
                                    _mLastRunTime = _mLastRunTime.AddSeconds(4 - dt);
                                    CProgram.sLogLine("Next scan time " + ((int)dt).ToString() + " increased to 4 seconds");
                                }
                            }
                            else
                            {
                                mProcessSetInfo("?DB?");
                                mSetRunColor(_cColorError);
                                toolStripButtonWarn.Text = "!DB";
                            }
                        }
                        else
                        {
                            mProcessSetInfo("-");
                            mSetRunColor(_cColorUpdate);
                            mProcessCheckExit();
                        }
                        mCheckDiskMemSrvr(false);   // log drive and mem free
                    }
                    else
                    {
                        int min = dt / 60;
                        int sec = dt % 60;
                        string timeStr = min.ToString() + ":" + sec.ToString("00");

                        mProcessCheckExit();

                        toolStripProcessIndex.Text = timeStr;

                        mProcessSetInfo(" clock " + timeStr);

                        mSetRunColor(_cColorWait);

                        if(_mbCursorFixEnd && sec == 1)
                        {
                            mMoveCursorToEnd();
                        }
                    }

                    mWriteLastActivity(daysLeft);
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Timer Tick", ex);
            }
            mCheckRunTimerState();
            _mbTimerEnterd = false;
            timer.Enabled = _mbRun;
        }

        private void mMoveCursorToEnd()
        {
            if (dataGridView.Rows != null)
            {
                int n = dataGridView.Rows.Count;

                if (n > 1)
                {
                    int iLast = n - 1;
                    DataGridViewRow row = dataGridView.Rows[iLast];

                    if (row != null && dataGridView.CurrentRow != row && row.Cells != null && row.Cells.Count > 0)
                    {
                        dataGridView.CurrentCell = row.Cells[0];
                    }
                }

            }
        }
            private void mWriteLastActivity(Int32 ADaysLeft)
        {
            DateTime dtUtc = DateTime.UtcNow;
            DateTime dtNow = DateTime.Now;
            TimeSpan span = dtUtc - _mLastUtcUpdate;

            if (span.TotalSeconds > 15 || span.TotalSeconds < -1)
            {
                span = dtNow - dtUtc;
                int sec = (int)span.TotalSeconds;       // time zone calculation
                int min = (sec < 0 ? sec - 5 : sec + 5) / 60;

                string doneSir = (_mLastScanDtSir != DateTime.MinValue && (DateTime.Now - _mLastScanDtSir).TotalSeconds < 60) ? "Sir " : "";
                string doneTZe = (_mLastScanDtTz != DateTime.MinValue && (DateTime.Now - _mLastScanDtTz).TotalSeconds < 60) ? "TZe " : "";
                string doneDV2 = (_mLastScanDtDV2 != DateTime.MinValue && (DateTime.Now - _mLastScanDtDV2).TotalSeconds < 60) ? "DV2 " : "";
                string doneDVX = (_mLastScanDtDVX != DateTime.MinValue && (DateTime.Now - _mLastScanDtDVX).TotalSeconds < 60) ? "DVX " : "";
                string doneHSir = (_mLastScanDtHttpSir != DateTime.MinValue && (DateTime.Now - _mLastScanDtHttpSir).TotalSeconds < 60) ? "HSir " : "";
                string s = doneSir + doneTZe + doneDV2 + doneHSir + "scan at Utc= " + CProgram.sDateTimeToString(dtUtc) + " Off=";

                s += CProgram.sTimeZoneOffsetStringLong((Int16)min);
                s += " dl= " + ADaysLeft.ToString();

                labelUtcTime.Text = s;
                Color color = Color.Red;
                if (ADaysLeft > 14)
                {
                    color = panelExtra.BackColor;
                }
                else if (ADaysLeft > 0 || ADaysLeft == 0 && dtNow.Hour < 23)
                {
                    color = Color.Orange;
                }
                labelUtcTime.BackColor = color;
                toolStrip.BackColor = color;

                _mLastUtcUpdate = dtUtc;
            }
            if ((dtNow - _mLastActivityWriteDT).TotalSeconds > _cLastActivityWriteSec)
            {
                try
                {
                    bool bLog = (dtNow - _mLastActivityLogDT).TotalSeconds > 600;
                    string strLog = "";

                    if (bLog)
                    {
                        strLog = " (nT=" + _mTotalCount.ToString() + ", nE=" + _mErrorCount.ToString() + ", nS=" + _mSkippedCount.ToString() + ")";
                        if (_mbRun && _mStartRunTimeDT > DateTime.MinValue)
                        {
                            double hours = (DateTime.Now - _mStartRunTimeDT).TotalHours;
                            int iHours = (int)hours;
                            strLog += " " + iHours.ToString() + " hours";
                        }
                        _mLastActivityLogDT = dtNow;
                    }
                    _mLastActivityWriteDT = dtNow;
                    CProgram.sWriteLastActiveFile(_mRecordingDir, true, CLicKeyDev.sGetLogLines());   // if timer is off the file is not written, just what is needed to see

                    if (_mLastScanDtSir != DateTime.MinValue && (DateTime.Now - _mLastScanDtSir).TotalSeconds < 180)
                    {
                        sWriteLastScanFile(_mRecordingDir, "Sir");
                        if (bLog)
                        {
                            CProgram.sLogLine("Written LastScan Sir " + CProgram.sDateTimeToYMDHMS(_mLastScanDtSir) + strLog);
                        }
                    }
                    if (_mLastScanDtTz != DateTime.MinValue && (DateTime.Now - _mLastScanDtTz).TotalSeconds < 180)
                    {
                        sWriteLastScanFile(_mRecordingDir, "Tz");
                        if (bLog)
                        {
                            CProgram.sLogLine("Written LastScan Tz " + CProgram.sDateTimeToYMDHMS(_mLastScanDtTz) + strLog);
                        }
                    }

                    if (_mLastScanDtDV2 != DateTime.MinValue && (DateTime.Now - _mLastScanDtDV2).TotalSeconds < 180)
                    {
                        sWriteLastScanFile(_mRecordingDir, "DV2");
                        if (bLog)
                        {
                            CProgram.sLogLine("Written LastScan DV2 " + CProgram.sDateTimeToYMDHMS(_mLastScanDtDV2) + strLog);
                        }
                    }
                    if (_mLastScanDtDVX != DateTime.MinValue && (DateTime.Now - _mLastScanDtDVX).TotalSeconds < 180)
                    {
                        sWriteLastScanFile(_mRecordingDir, "DVX");
                        if (bLog)
                        {
                            CProgram.sLogLine("Written LastScan DVX " + CProgram.sDateTimeToYMDHMS(_mLastScanDtDVX) + strLog);
                        }
                    }
                    if (_mLastScanDtHttpSir != DateTime.MinValue && (DateTime.Now - _mLastScanDtHttpSir).TotalSeconds < 180)
                    {
                        sWriteLastScanFile(_mRecordingDir, "HSir");
                        if (bLog)
                        {
                            CProgram.sLogLine("Written LastScan HSir " + CProgram.sDateTimeToYMDHMS(_mLastScanDtHttpSir) + strLog);
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("timer write last active files", ex);
                }
            }
        }

        public static bool sWriteLastScanFile(string APath, string AName)
        {
            bool bOk = false;

            string fileName = AName + ".lastScan";

            try
            {
                using (StreamWriter fs = new StreamWriter(Path.Combine(APath, fileName)))
                {
                    if (fs != null)
                    {
                        string s = "Time: " + DateTimeOffset.Now.ToString()
                            + "\ncomputer: " + CProgram.sGetPcUserName() + "\t" + CProgram.sGetPcName()
                            + "\nProg: " + CProgram.sGetProgName() + " " + CProgram.sGetProgVersionLevelString() + CProgram.sGetProgVersion()
                            + "\nStarted: " + CProgram.sGetProgStartString();

                        fs.WriteLine(s);
                        fs.Close();
                        bOk = true;
                    }
                }
            }
            catch (Exception)
            {
                bOk = false;
            }
            if (bOk == false)
            {
                CProgram.sLogError("Failed writing " + fileName);
            }
            return bOk;
        }


        public bool mbChangeToDateDir(out String ArToDir, String ABaseDir, DateTime AUtc)
        {
            bool bOk = false;
            string toDir = ABaseDir;

            try
            {
                int yearMonth = AUtc.Year * 100 + AUtc.Month;
                int day = AUtc.Day;
                String ym = "YM" + yearMonth.ToString();
                String d = "D" + day.ToString("00");

                if (Directory.Exists(toDir))
                {
                    toDir = Path.Combine(_mRecordingDir, ym);
                    if (Directory.Exists(toDir) == false)
                    {
                        Directory.CreateDirectory(toDir);
                    }
                    toDir = Path.Combine(toDir, d);
                    if (Directory.Exists(toDir))
                    {
                        bOk = true;
                    }
                    else
                    {
                        Directory.CreateDirectory(toDir);
                        bOk = Directory.Exists(toDir);
                    }
                }
            }
            catch (Exception e)
            {
                bOk = false;
                CProgram.sLogError("toDir = " + toDir);
                CProgram.sLogException("Failed to change to Date dir", e);
            }
            if (bOk)
            {
                ArToDir = toDir;
            }
            else
            {
                ArToDir = "::::";
            }
            return bOk;
        }

        private String mCreateUtcName(DateTime AUtc, TimeZoneInfo ATimeZone)
        {
            string name = "D" + AUtc.ToString("yyyyMMdd") + "T" + AUtc.ToString("HHmmss") + mGetUtcOffsetString(AUtc, ATimeZone);

            return name;
        }

        private String mCreateDtZoneFileName(DateTime ADt, Int16 ATimeZoneOffsetMin)
        {
            string tz = "ZP";
            int min = ATimeZoneOffsetMin;
            if (min < 0)
            {
                min = -min;
                tz = "ZM";
            }
            tz += (min / 60).ToString("D2") /*+ ":" */+ (min % 60).ToString("D2");
            string name = "D" + ADt.ToString("yyyyMMdd") + "T" + ADt.ToString("HHmmss") + tz;

            return name;
        }
        private bool mbCreateDirectory(String ADirName)
        {
            bool bOk = Directory.Exists(ADirName);

            if (bOk == false)
            {
                Directory.CreateDirectory(ADirName);

                bOk = Directory.Exists(ADirName);
            }
            return bOk;
        }

        /*        public void mCloseSqlConnection()
                {
                    if (mSqlConnection != null)
                    {
                        mSqlConnection.Close();
                    }
                }

                public bool mbCreateSqlConnection()
                {
                    bool bOk = false;

                    if (mSqlConnection != null)
                    {
                        // connection already exists
                        bOk = true;
                    }
                    else
                    {
                        string server = Properties.Settings.Default.sqlServer;
                        int port = Properties.Settings.Default.sqlPort;
                        string user = Properties.Settings.Default.sqlUser;
                        string password = Properties.Settings.Default.sqlPassword;
                        string connString = @"SERVER=" + server + ";Port=" + port.ToString() + ";DATABASE=iReader; UID=" + user + ";password=" + password + ";";


                        try
                        {
                             mSqlConnection = new MySqlConnection(connString);

                            if (mSqlConnection != null)
                            {
                                mSqlConnection.Open();
                                CProgram.sLogLine("SQL connected");
                                bOk = true;
                            }
                        }
                        catch (Exception e)
                        {
                            bOk = false;
                            CProgram.sLogException( "Failed to create SQL connection", e);
                            mSqlConnection = null;
                        }
                        finally
                        {
                            mCloseSqlConnection();
                        }
                    }
                    return bOk;
                }
        */
        string mSqlDateTimeString(DateTime ADateTime)
        {
            return ADateTime.ToString("yyyy-MM-dd HH:mm:ss");
        }
        string mSqlDateString(DateTime ADateTime)
        {
            return ADateTime.ToString("yyyy-MM-dd");
        }

        public bool mbAddSqlDbRow(out int ArIndex, CRecordMit ARec, string ARecordFilePath )
        {
            bool bOk = false;
            int index = 0;
            CSqlCmd cmd = null;
            int tryCount = 1;
            int maxCount = 30;      // max delay is maxCount * 2 * waitTime
            int waitTime = 2000;
            int warnCount = CLicKeyDev.sbDeviceIsProgrammer() ? 2 : 26;

            if (ARec != null)
            {
                if( ARec.mRecDurationSec < 0.005)
                {
                    ARec.mRecDurationSec = ARec.mGetSamplesTotalTime();
                }

                do
                {
                    try
                    {
                        if (mbCreateDBaseConnection())
                        {
                            cmd = ARec.mCreateSqlCmd(_mDBaseServer);

                            if (cmd == null)
                            {
                                CProgram.sLogLine("Failed to init cmd");

                            }
                            else if (false == cmd.mbPrepareInsertCmd(ARec.mMaskValid, true, true))
                            {
                                CProgram.sLogLine("Failed to prepare SQL row in table " + ARec.mGetDbTableName());
                            }
                            else if (cmd.mbExecuteInsertCmd())
                            {
                                index = (int)ARec.mIndex_KEY;   // return new key
                                bOk = index > 0;
                                if (bOk)
                                {
                                    CProgram.sLogLine("Successfull insert in " + ARec.mGetDbTableName() + "[" + ARec.mIndex_KEY + "]");
                                }
                                else
                                {
                                    CProgram.sLogLine("Insert unexpected index in " + ARec.mGetDbTableName() + "[" + ARec.mIndex_KEY + "]");
                                }
                            }
                            else
                            {
                                CProgram.sLogLine("Failed to add SQL row in table " + ARec.mGetDbTableName());
                            }
                            cmd.mCloseCmd();
                        }
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException("Failed add Row", ex);
                    }
                    finally
                    {
                        if (cmd != null)
                        {
                            cmd.mCloseCmd();
                        }
                    }
                    if (index > 0)
                    {
                        break;
                    }
                    else
                    {
                        CProgram.sLogError("!! wait !! Failed insert record in dBase for " + ARec.mFileName + " try count= " + tryCount.ToString() + " < " + maxCount);

                        mDisposeDBaseConnection();
                        Thread.Sleep(waitTime);
                        Application.DoEvents();
                        if (tryCount >= warnCount)
                        {
                            CProgram.sLogError("!! Error !! Failed insert record in dBase for " + ARec.mFileName + " try count= " + tryCount.ToString() + " < " + maxCount);
                            if( CLicKeyDev.sbDeviceIsProgrammer())
                            {
                                if (false == CProgram.sbAskOkCancel("DBase insert record failed " + tryCount.ToString() + "/" + maxCount.ToString(), 
                                    "Insert " + ARec.mFileName + " in DB failed, Continue?"))
                                {
                                    CProgram.sLogError("!! Stop  Promt !! Failed insert record in dBase for " 
                                        + ARec.mFileName + " try count= " + tryCount.ToString() + " < " + maxCount);
                                    bOk = false;

                                    break;
                                }
                            }
                        }
                        else
                        {
                            Thread.Sleep(waitTime);
                            Application.DoEvents();
                        }
                    }
                } while (++tryCount <= maxCount);
                ARec.mbWriteRefFile(Path.GetDirectoryName(ARecordFilePath));
            }
            ArIndex = index;
            return bOk;
        }

        private bool mbCreateImages(string AFilePath, CRecordMit ARec, out double ArPlotTimeSec)
        {
            bool bOk = false;

            ArPlotTimeSec = 0.0;
            if (ARec != null)
            {
                double plotTimeSec;
                bOk = true;
                for (ushort j = 0; j < ARec.mNrSignals; ++j)
                {
                    bOk &= mbCreateOneImages(AFilePath, ARec, j, out plotTimeSec);
                    ArPlotTimeSec += plotTimeSec;
                }
            }
            return bOk;
        }
        private bool mbCreateOneImages(string AFilePath, CRecordMit ARec, ushort ASignalIndex, out double ArPlotTimeSec)
        {
            UInt16 nImages = 0;
            DateTime plotStart = DateTime.Now;

            try
            {
                string toPath = Path.GetDirectoryName(AFilePath);
                string imgResult;

                float t0, length, cursorStart, cursorLength;
                int width, height;
                string filePath = Path.GetFileNameWithoutExtension(AFilePath);
                Image img;


                // plot event  strip
                float tEvent = ARec.mGetEventSec();
                float tStripLength = ARec.mGetSamplesTotalTime();
                t0 = tEvent - _mPrePlotEventBeforeSec;

                if (t0 > tStripLength)
                {
                    if (t0 < tStripLength + 60)
                    {
                        t0 = tStripLength - _mPrePlotEventBeforeSec - _mPrePlotEventAfterSec;
                    }
                }
                if (t0 < 0.0F) t0 = 0.0F;
                float t1 = t0 + _mPrePlotEventBeforeSec + _mPrePlotEventAfterSec;
                if (t1 > tStripLength)
                {
                    t1 = tStripLength;
                    t0 = t1 - _mPrePlotEventBeforeSec - _mPrePlotEventAfterSec;
                    if (t0 < 0.0F)
                    {
                        t0 = 0.0F;
                    }
                }
                length = t1 - t0;
                width = _mPrePlotEventWidth;
                height = _mPrePlotEventHeight;

                cursorStart = t0;
                cursorLength = length;
                img = ARec.mCreateChartImage(ASignalIndex, t0, length, width, height, _mPrePlotEventUnitSec, -1.0F, -1.0F, DChartDrawTo.Display, CRecordMit.sGetMaxAmplitudeRange());

                if (_sbShowImported)
                {
                    pictureBoxEvent.Image = img;
                }
                imgResult = ARec.mIndex_KEY.ToString() + "=";

                if (img != null)
                {
                    string imgFile = Path.Combine(mGetImageEventName(ASignalIndex, filePath, false));

                    imgResult += imgFile + " " + t0.ToString("0.0") + "(" + length.ToString("0.0")
                            + " sec " + width.ToString() + "x" + height.ToString() + ")";

                    imgFile = Path.Combine(toPath, imgFile);

                    try
                    {
                        img.Save(imgFile, System.Drawing.Imaging.ImageFormat.Png);
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException("Save image failed:" + imgFile, ex);
                    }

                    if (File.Exists(imgFile))
                    {
                        imgResult += " saved.";
                        ++nImages;
                    }
                }
                if (_sbShowImported)
                {
                    labelImageEvent.Text = imgResult;
                }
                // plot full strip
                t0 = 0.0F;
                length = ARec.mGetSamplesTotalTime();
                //                                            float minA = rec.mGetSamplesMinValue();
                //                                            float maxA = rec.mGetSamplesMaxValue();
                width = _mPrePlotFullWidth;
                height = _mPrePlotFullHeight;

                img = ARec.mCreateChartImage(ASignalIndex, t0, length, width, height, _mPrePlotEventUnitSec, cursorStart, cursorLength, DChartDrawTo.Display, CRecordMit.sGetMaxAmplitudeRange());// mPrePlotFullUnitSec);

                if (_sbShowImported)
                {
                    pictureBoxFull.Image = img;
                }
                imgResult = ARec.mIndex_KEY.ToString() + "=";

                if (img != null)
                {
                    string imgFile = Path.Combine(mGetImageFullName(ASignalIndex, filePath, false));

                    imgResult += imgFile + "@" + tEvent.ToString("0.0") + "(" + length.ToString("0.0")
                            + " sec, " + width.ToString() + "x" + height.ToString() + ")";

                    imgFile = Path.Combine(toPath, imgFile);

                    try
                    {
                        img.Save(imgFile, System.Drawing.Imaging.ImageFormat.Png);
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogError("Save image failed:" + imgFile);
                    }

                    if (File.Exists(imgFile))
                    {
                        imgResult += " saved.";
                        ++nImages;
                    }
                }
                if (_sbShowImported)
                {
                    labelImageFull.Text = imgResult;

                    string result = "-";
                    if (ARec.mReceivedUTC > DateTime.MinValue)
                    {
                        result = "ReceivedUTC= " + CProgram.sDateTimeToString(ARec.mReceivedUTC) + CProgram.sMakeUtcOffsetString(ARec.mTimeZoneOffsetMin);
                    }

                    labelReadImgUtc.Text = result;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Create image " + ASignalIndex.ToString() + " failed:" + AFilePath, ex);
            }
            TimeSpan ts = DateTime.Now - plotStart;
            ArPlotTimeSec = ts.TotalSeconds;
            return nImages == 2;
        }

        bool mbStoreGrid(CRecordMit ARec)
        {
            bool bOk = true;
            UInt32 index = 0;
            string name = "";
            try
            {
                if (ARec != null)
                {
                    index = ARec.mIndex_KEY;
                    name = ARec.mFileName;

                    string eventTimeString = "", patientDateString = "";
                    DateTime dtRead = CProgram.sUtcToLocal(ARec.mReceivedUTC);

                    DateTime dtEventUtc = ARec.mGetEventUTC();
                    //            string patientName = ARec.mPatientTotalName.mDecrypt();
                    string state = CRecordMit.sGetStateUserString(ARec.mRecState);
                    string converter = CRecordMit.sGetConverterUserString(ARec.mImportConverter);
                    string method = CRecordMit.sGetMethodUserString(ARec.mImportMethod);

                    if (dtEventUtc > DateTime.MinValue)
                    {
                        double durationSec = ARec.mRecDurationSec < 1 ? ARec.mGetSamplesTotalTime() : ARec.mRecDurationSec;
                        DateTime endRecUtc = ARec.mBaseUTC.AddSeconds(ARec.mRecDurationSec);

                        if(dtEventUtc > endRecUtc)
                        {
                            endRecUtc = dtEventUtc;
                        }
                        eventTimeString = CProgram.sDateTimeToString(ARec.mGetDeviceTime(dtEventUtc)) + CProgram.sMakeUtcOffsetString(ARec.mTimeZoneOffsetMin);

                        int min = (int)((ARec.mReceivedUTC - endRecUtc).TotalMinutes);  // compare end record time because event strip can be an hour
                        if (min > 30 || min < -30)
                        {
                            eventTimeString += " !E" + CProgram.sMakeUtcOffsetString(min) + "!";
                        }
                    }
                    if (ARec.mTransmitUTC > DateTime.MinValue)
                    {
                        int min = (int)((ARec.mReceivedUTC - ARec.mTransmitUTC).TotalMinutes);
                        if (min > 30 || min < -30)
                        {
                            eventTimeString += " !T" + CProgram.sMakeUtcOffsetString(min) + "!";
                        }
                    }

                    if (ARec.mPatientBirthDate.mbNotZero()) patientDateString = mDateToString(ARec.mPatientBirthDate.mDecryptToDate());

                    string studyPatID = ARec.mStudy_IX == 0 ? "P:" + ARec.mPatientID.mDecrypt() : "S#" + ARec.mStudy_IX.ToString() + "." + ARec.mSeqNrInStudy.ToString();
                    if (ARec._mStudyTrial_IX > 0) studyPatID += " T" + ARec._mStudyTrial_IX.ToString();
                    if (ARec._mStudyClient_IX > 0) studyPatID += " C" + ARec._mStudyClient_IX.ToString();

                    string readDtStr = mDateTimeToString(dtRead);
                    float lenSec1 = ARec.mGetSamplesTotalTime();
                    float lenSec2 = ARec.mRecDurationSec > lenSec1 ? ARec.mRecDurationSec : lenSec1;
                    string stripLength = CProgram.sPrintTimeSpan_dhmSec(lenSec2, "-");

                    dataGridView.Rows.Add(ARec.mIndex_KEY.ToString(), ARec.mFileName, ARec.mNrSignals.ToString(), stripLength, readDtStr, ARec.mDeviceID,
                        eventTimeString, ARec.mEventTypeString, state, studyPatID, patientDateString, converter, method, ARec.mStoredAt);
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed to add to table #" + index.ToString() + " " + name, ex);
            }
            return bOk;
        }

        bool mbGetCursorIndexState(out int ArIndexKey, out string ArState)
        {
            bool bOk = false;
            int indexKey = 0;
            string state = "";

            try
            {
                DataGridViewRow index = dataGridView.CurrentRow;

                if (index != null && index.Cells.Count >= (int)DDatagridColumn.NrSignals)
                {
                    string indexString = index.Cells[(int)DDatagridColumn.RecNr].Value.ToString();
                    state = index.Cells[(int)DDatagridColumn.State].Value.ToString();

                    bOk = int.TryParse(indexString, out indexKey);

                    if (bOk)
                    {
                        if (indexKey == 0)
                        {
                            CProgram.sLogLine("Cursor has an invalid index key (0)");
                            bOk = false;
                        }
                    }
                }

            }
            catch (Exception e2)
            {
                bOk = false;
                CProgram.sLogException("Failed get cursor for index", e2);
            }
            ArIndexKey = indexKey;
            ArState = state;

            return bOk;
        }

        bool mbGetCursorFilePath(out string AFilePath, out UInt32 ARecIndex, out DateTime AReceivedUTC)
        {
            bool bOk = false;
            DateTime dt = DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Utc);

            AFilePath = "";
            ARecIndex = 0;
            try
            {
                DataGridViewRow index = dataGridView.CurrentRow;

                if (index != null && index.Cells.Count >= (int)DDatagridColumn.NR_Columns)
                {
                    string recName = index.Cells[(int)DDatagridColumn.RecNr].Value.ToString();
                    string fileName = index.Cells[(int)DDatagridColumn.File].Value.ToString();
                    string readTime = index.Cells[(int)DDatagridColumn.Received].Value.ToString();
                    string storedAt = index.Cells[(int)DDatagridColumn.StoredAt].Value.ToString();

                    UInt32.TryParse(recName, out ARecIndex);

                    if (fileName.Length > 0 && mbParseDateTime(readTime, out dt) /*received.Length > 9*/  && storedAt.Length > 0)
                    //                        if (fileName.Length > 0 && CProgram.sbParseDtLocal(readTime, out dt, true) /*received.Length > 9*/  && storedAt.Length > 0)
                    {
                        string ym, d;

                        dt = CProgram.sDateTimeToUTC(dt);   // int table is local time

                        ym = "YM" + dt.Year.ToString() + dt.Month.ToString("00"); // received.Substring(1, 6);
                        d = "D" + dt.Day.ToString("00"); // received.Substring(7, 2);

                        string toPath = Path.Combine(_mRecordingDir, ym, d, storedAt);

                        if (Directory.Exists(toPath))
                        {
                            string toFile = Path.Combine(toPath, fileName);
                            /*                            toFile = Path.ChangeExtension(toFile, "dat");

                                                        if (File.Exists(toFile))
                                                        {
                                                            AFilePath = toFile;
                                                            bOk = true;
                                                        }
                                                        else
                                                        {
                                                            CProgram.sLogLine("Data file " + toFile + " does not exist");
                                                        }
                            */
                            AFilePath = toFile;
                            bOk = true;
                        }
                        else
                        {
                            CProgram.sLogLine("Data path " + toPath + " does not exist");
                        }
                    }

                }
            }
            catch (Exception e2)
            {
                bOk = false;
                CProgram.sLogException("Failed get cursor path", e2);
            }
            AReceivedUTC = dt;
            return bOk;
        }
        private void toolStripEqualsCursor_Click(object sender, EventArgs e)
        {
            try
            {
                DataGridViewRow index = dataGridView.CurrentRow;

                if (index != null && index.Cells.Count >= 12)
                {
                    string methode = toolStripDropDownSearch.Text;

                    if (methode == null || methode.Length < 0)
                    {
                        CProgram.sLogLine("Select row first.");
                    }
                    else
                    {
                        switch (methode)
                        {
                            case "*(All)":
                                CProgram.sLogLine("Select msearch methode first.");
                                toolStripEditSearch.Text = "";
                                break;
                            case "#":
                                toolStripEditSearch.Text = index.Cells[(int)DDatagridColumn.RecNr].Value.ToString();
                                break;
                            case "File":
                                toolStripEditSearch.Text = index.Cells[(int)DDatagridColumn.File].Value.ToString();
                                break;
                            case "NrSignals":
                                toolStripEditSearch.Text = index.Cells[(int)DDatagridColumn.NrSignals].Value.ToString();
                                break;
                            case "Read Date Time":
                                toolStripEditSearch.Text = index.Cells[(int)DDatagridColumn.Received].Value.ToString().Substring(0, 10);
                                break;
                            case "Device ID":
                                toolStripEditSearch.Text = index.Cells[(int)DDatagridColumn.DeviceID].Value.ToString();
                                break;
                            case "Event Time":
                                toolStripEditSearch.Text = index.Cells[(int)DDatagridColumn.EventTime].Value.ToString().Substring(0, 10);
                                break;
                            case "Event Type":
                                toolStripEditSearch.Text = index.Cells[(int)DDatagridColumn.EventType].Value.ToString();
                                break;
                            case "State":
                                toolStripEditSearch.Text = index.Cells[(int)DDatagridColumn.State].Value.ToString();
                                break;
                            case "Patient ID":
                                toolStripEditSearch.Text = index.Cells[(int)DDatagridColumn.PatID].Value.ToString();
                                break;
                            case "Patient Birth":
                                toolStripEditSearch.Text = index.Cells[(int)DDatagridColumn.PatDOB].Value.ToString();
                                break;
                            case "Converter":
                                toolStripEditSearch.Text = index.Cells[(int)DDatagridColumn.Converter].Value.ToString();
                                break;
                            case "Import":
                                toolStripEditSearch.Text = index.Cells[(int)DDatagridColumn.Import].Value.ToString();
                                break;
                            case "Stored at":
                                toolStripEditSearch.Text = index.Cells[(int)DDatagridColumn.StoredAt].Value.ToString();
                                break;
                            default:
                                CProgram.sLogLine("Select msearch methode first.");
                                break;
                        }
                    }
                }
            }
            catch (Exception e2)
            {
                CProgram.sLogException("Failed toolstripequal", e2);
            }
        }
        private string mMakeSqlSearchString()
        {
            string sqlWhere;
            string methode = toolStripDropDownSearch.Text;
            string search = toolStripEditSearch.Text;
            string time = search;

            if (search == null || search.Length < 1 || methode == null || methode.Length < 0 || _mDBaseServer == null)
            {
                sqlWhere = "";
            }
            else
            {
                string searchString = _mDBaseServer.mSqlConvertToSqlString(search);
                switch (methode)
                {
                    case "*(All)":
                        sqlWhere = "";
                        break;
                    case "#":
                        sqlWhere = "d_record_KEY=" + search;
                        break;
                    case "File":
                        sqlWhere = "INSTR(FileName," + searchString + ")>0";
                        break;
                    case "NrSignals":
                        sqlWhere = "nrSignals=" + search;
                        break;
                    case "Read Date Time":
                        sqlWhere = "INSTR(DATE_FORMAT(ReadUTC, '" + mSqlDateFormat() + " %T'),'" + time + "')> 0";
                        break;
                    case "Device ID":
                        sqlWhere = "INSTR(DeviceID," + searchString + ")>0";
                        break;
                    case "Event Time":
                        sqlWhere = "INSTR(DATE_FORMAT(EventTime, '" + mSqlDateFormat() + " %T'),'" + time + "')> 0";
                        break;
                    case "Event Type":
                        sqlWhere = "INSTR(EventTypeString," + searchString + ")>0";
                        break;
                    case "State":
                        UInt16 state;
                        CRecordMit.sbParseStateUserString(out state, search);
                        sqlWhere = "RecState=" + state.ToString();
                        break;
                    case "Patient ID":
                        sqlWhere = "INSTR(PatientID_ES," + searchString + ")>0";
                        break;
                    case "Patient Birth":
                        sqlWhere = "INSTR(DATE_FORMAT(PatientBirthDate_EI, '" + mSqlDateFormat() + "'),'" + search + "')> 0";
                        break;
                    case "Converter":
                        UInt16 converter;
                        CRecordMit.sbParseConverterUserString(out converter, search);
                        sqlWhere = "ImportConverter=" + converter.ToString();
                        break;
                    case "Method":
                        UInt16 method;
                        CRecordMit.sbParseMethodUserString(out method, search);
                        sqlWhere = "ImportConverter=" + method.ToString();
                        break;
                    case "Stored at":
                        sqlWhere = "INSTR(StoredAt," + searchString + ")>0";
                        break;
                    default:
                        sqlWhere = "";
                        break;
                }
            }
            return sqlWhere;
        }

        public string mGetFirstLine(string AText, UInt16 AMaxChars)
        {
            string line = "";
            int n = AText != null ? AText.Length : 0;

            for (int i = 0; i < n; ++i)
            {
                char c = AText[i];

                if (c == '\t') c = ' ';

                if (c < ' ')
                {
                    break;
                }
                line += c;
                if (i >= AMaxChars)
                {
                    break;
                }
            }
            return line;
        }

        private bool mbSirCheckHolterEvent(ref bool AbDoEvent, ref CRecordMit ARecord, string AFilePath)
        {
            bool bOk = false;
            bool bDoEvent = AbDoEvent;  // should already be true 

            if (ARecord != null && AFilePath != null && AFilePath.Length > 0)
            {
                string deviceSnr = ARecord.mDeviceID;
                string oldFileName = Path.GetFileNameWithoutExtension(AFilePath);
                UInt32 stripLengthSec = (UInt32)(ARecord.mGetSamplesTotalTime() + 0.5);
                string stripLengthStr = "H" + stripLengthSec.ToString("0000");    // strip length
                UInt32 nStudyFiles = 0, nErrorFiles = 0;
                UInt32 studyIX = 0;

                if (CSironaStateSettings.sbIsHolter(stripLengthSec))
                {
                    // holter event
                    ARecord.mEventTypeString += " Holter";

                    ARecord.mbSetEventSec(0);   // set event to start of strip to get consistentt event time for holter strips
                    // rename study files 


                    CDeviceInfo device = new CDeviceInfo(_mDBaseServer);

                    if (device != null)
                    {
                        int n = device.mFindLoadSerial(0, deviceSnr, 0);

                        if (n == 1 && device.mIndex_KEY > 0 && device._mActiveStudy_IX > 0 && device._mAddStudyMode == (UInt16)DAddStudyModeEnum.ActiveStudy)
                        {
                            string studyPath;

                            studyIX = device._mActiveStudy_IX;

                            if (CDvtmsData.sbGetStudyRecorderDir(out studyPath, studyIX, true))
                            {
                                string[] files = Directory.GetFiles(studyPath);

                                if (files != null)
                                {
                                    string searchName = oldFileName.ToLower();
                                    int searchNameLength = searchName.Length;

                                    foreach (string filePath in files)
                                    {
                                        string fileName = Path.GetFileName(filePath);
                                        string lowerName = fileName.ToLower();
                                        if (lowerName.StartsWith(searchName))

                                        {
                                            // <snr>_<ID>_yyyyMMddhhmmss.* -> H_<snr>_<ID>_yyyyMMddhhmmssHllll.*
                                            // rename file as holter starting with "H_" and adding strip length
                                            string newName = "H_" + searchName + stripLengthStr + fileName.Substring(searchNameLength);
                                            try
                                            {
                                                File.Move(filePath, Path.Combine(studyPath, newName));
                                                ++nStudyFiles;
                                            }
                                            catch (Exception ex)
                                            {
                                                CProgram.sLogException("S" + studyIX.ToString() + " failed Holter rename " + filePath, ex);
                                                ++nErrorFiles;
                                            }
                                        }
                                    }
                                    if (nErrorFiles > 0)
                                    {
                                        bDoEvent = true;
                                        ARecord.mRecRemark.mAddLine("failed ren H_*");
                                    }
                                }
                            }
                        }
                    }
                    // check Sirona Auto Collect to see if an event is wanted

                    if (nErrorFiles > 0)
                    {
                        bDoEvent = true;
                        CProgram.sLogLine(nErrorFiles.ToString() + " failed, " + nStudyFiles.ToString() + " renamed, study " + studyIX.ToString() + " holter files for Sirona " + oldFileName);
                    }
                    else if (nStudyFiles == 0)
                    {
                        bDoEvent = true;
                        CProgram.sLogLine("Study " + studyIX.ToString() + " has no holter files for Sirona " + oldFileName);
                    }
                    else
                    {

                        bDoEvent = _mbSirAutoCollectHolterEvent;
                        if (_mbSirAutoCollectHolterEvent)
                        {
                            string devicePath;

                            bDoEvent = false;

                            if (CDvtmsData.sbGetDeviceUnitDir(out devicePath, DDeviceType.Sirona, deviceSnr, true))
                            {
                                CSirAutoCollect sirCollect = new CSirAutoCollect("ECG", true);

                                if (sirCollect != null)
                                {
                                    bool bPresent;

                                    if (sirCollect.mbLoadParamFile(out bPresent, devicePath, deviceSnr))
                                    {
                                        if (bPresent)
                                        {
                                            bDoEvent = sirCollect._mbGenEvent;  // generate event
                                        }
                                    }
                                }
                            }
                        }
                        CProgram.sLogLine("Study " + studyIX.ToString() + " has " + nStudyFiles.ToString() + " Sirona holter files " +
                            (bDoEvent ? " Event: " : " No event: ") + oldFileName);
                        if (false == bDoEvent && nStudyFiles > 0)
                        {
                            // record is a holter event and is not added to data base => remove files
                            string parentDir = Path.GetDirectoryName(AFilePath);
                            if (parentDir.EndsWith(ARecord.mStoredAt) && parentDir.Contains("Record"))
                            {
                                try
                                {
                                    Directory.Delete(parentDir, true);
                                    if (_mbLogExtra)
                                    {
                                        CProgram.sLogLine("S" + studyIX.ToString() + " " + deviceSnr + " removed Holter Event folder " + parentDir);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    CProgram.sLogException("S" + studyIX.ToString() + " " + deviceSnr + " failed remove Holter Event folder " + parentDir, ex);
                                }

                            }
                        }
                        else
                        {
                            bOk = bDoEvent && nStudyFiles > 0;
                        }
                    }
                }
                bOk = true; // just continue
            }
            AbDoEvent = bDoEvent;
            return bOk;
        }

        private CRecordMit mImportIntriconFiles(out bool ArbDoEvent, String AFilePath, UInt32 ASetInvChannels, DateTime AReceivedUTC, Int16 ADefaultTimeZoneOffset, string AStoreName,
                                        DImportConverter AImportConverter, DImportMethod AImportMethod, string AManualName, bool AbWipeJson)
        {
            bool bOk = false;
            CRecordMit rec = null;
            string fileName = Path.GetFileName(AFilePath);
            bool bDoEvent = false;

            try
            {
                DateTime startTime = DateTime.Now;
                double dTDataRead = 0.0, dTPlot = 0.0;

                rec = new CRecordMit(_mDBaseServer);

                if (rec != null)
                {
                    rec.mStoredAt = AStoreName;

                    bOk = rec.mbReadFile(AFilePath, ASetInvChannels, AReceivedUTC, ADefaultTimeZoneOffset, DMoveTimeZone.None, true, AbWipeJson, AImportConverter, AImportMethod, AManualName);

                    if (bOk)
                    {
                        bDoEvent = true;

                        if (AImportMethod == DImportMethod.Automatic && AImportConverter == DImportConverter.SironaHttp)
                        {
                            bOk = mbSirCheckHolterEvent(ref bDoEvent, ref rec, AFilePath);
                        }
                    }

                    if (bOk && bDoEvent)
                    {
                        TimeSpan ts = DateTime.Now - startTime;
                        dTDataRead = ts.TotalSeconds;

                        //                        CProgram.sLogLine(rec.mGetJsonInfo());

                        // rec.mbWriteCsvFile(AFilePath);

                        int index = 0;
                        //                       mbAddSqlRow(out index, rec, AStoreName, AUtcStoreString);

                        if (rec.mDeviceModel == null || rec.mDeviceModel.Length == 0)
                        {
                            if (fileName.EndsWith(".tze") || fileName.EndsWith(".scp"))
                            {
                                rec.mDeviceModel = "TZ";
                            }
                            else
                            {
                                rec.mDeviceModel = "Sirona";
                            }
                        }
                        if (rec.mRecLabel == null || rec.mRecLabel.Length == 0)
                        {
                            if (rec.mRecRemark != null && rec.mRecRemark.mbNotEmpty())
                            {
                                rec.mRecLabel = mGetFirstLine(rec.mRecRemark.mDecrypt(), 16);
                            }
                        }

                        int newIndex;
                        DateTime startDB = DateTime.Now;
                        bOk = mbAddSqlDbRow(out newIndex, rec, AFilePath);
                        double dbSec = (DateTime.Now - startDB).TotalSeconds;

                        DateTime startStudy = DateTime.Now;
                        double studySec = 0.0;
                        if (bOk)
                        {
                            mbDoPreAnalysis(_mbDoPaSirona, _mSironaPaMethod, rec, Path.GetDirectoryName(AFilePath));

                            mbFindStudy(rec);   // neads record number to update
                            studySec = (DateTime.Now - startStudy).TotalSeconds;

                            if (DImportMethod.Automatic == AImportMethod)
                            {
                                mbSendReceivedMail(AFilePath, rec);
                            }
                        }
                        else
                        {
                            CProgram.sLogError("mbAddSqlDbRow failed");
                        }
                        mbCreateImages(AFilePath, rec, out dTPlot);  // plot later, needs invert mask from device
                        mbStoreGrid(rec);

                        TimeSpan dT = DateTime.Now - startTime;

                        /*CProgram.sLogLine("[" + newIndex.ToString() + "]Import " + fileName + " in " + dT.TotalSeconds.ToString("0.00") + " sec"
                            + " read " + rec.mGetNrSamples().ToString() + " samples(" + rec.mGetSamplesTotalTime().ToString("0.0")
                            + " s)"); // in " + dTDataRead.ToString("0.00") + " sec Plot(" + dTPlot.ToString("0.00") + " s)");
                            */
                        CProgram.sLogLine("[" + newIndex.ToString() + "]Import " + fileName + " in " + dT.TotalSeconds.ToString("0.00") + " Sec"
   + "(" + rec.mNrSignals.ToString() + "x" + rec.mGetNrSamples().ToString() + " " + rec.mGetSamplesTotalTime().ToString("0")
   + "s) ld=" + dTDataRead.ToString("0.00") + " pl=" + dTPlot.ToString("0.00")
   + " db=" + dbSec.ToString("0.00") + " st=" + studySec.ToString("0.00") + " S" + rec.mStudy_IX.ToString());

                    }
                }
            }
            catch (Exception e)
            {
                CProgram.sLogException("Failed import Intricon file", e);

            }
            if (!bOk)
            {
                CProgram.sLogError("Import " + fileName + " failed!");
                rec = null;
            }
            ArbDoEvent = bDoEvent;
            return rec;
        }

        bool mbFindStudy(CRecordMit ARec)
        {
            bool bOk = false;

            if (ARec != null)
            {
                string addStudyChar = "?";
                bool bDevice = false;
                string showError = "";

                try
                {
                    CDeviceInfo device = new CDeviceInfo(_mDBaseServer);
                    CStudyInfo study = null;
                    CPatientInfo patient = null;

                    if (device != null)
                    {
                        //                        bOk = device.mbLoadCheckFromRecord(false, ARec, true, ref study, ref patient, false, true);
                        bOk = device.mbLoadCheckFromRecordIR(ref showError, ARec, true, ref study, ref patient, false);

                        if (device.mIndex_KEY > 0)
                        {
                            bDevice = true;
                            addStudyChar = device.mGetAddStudyModeChar();
                            ARec.mModifyInvertMask(device._mInvertChannelsMask);        // set ECG invert according do device defaults
                        }
                    }
                }
                catch (Exception e)
                {
                    CProgram.sLogException("Failed find study", e);
                }

                if (ARec.mStudy_IX > 0)
                {
                    CProgram.sLogLine("R#" + ARec.mIndex_KEY.ToString() + " device #" + ARec.mDeviceID + " => "
                        + addStudyChar + "S#" + ARec.mStudy_IX.ToString()
                        + "." + ARec.mSeqNrInStudy.ToString()
                        + " P" + ARec.mPatient_IX.ToString()
                        + " T" + ARec._mStudyTrial_IX.ToString()
                        + " C" + ARec._mStudyClient_IX.ToString()
                        );
                }
                else if (bDevice)
                {
                    CProgram.sLogLine("R#" + ARec.mIndex_KEY.ToString() + " device #" + ARec.mDeviceID + " -> " + addStudyChar);
                }
                else
                {
                    CProgram.sLogLine("R#" + ARec.mIndex_KEY.ToString() + " device #" + ARec.mDeviceID + " -> not found");

                }
            }
            return bOk;
        }

        private bool mbUnzip(string AFilePath, string AToPath)
        {
            bool bZipOk = false;

            try
            {
                if (File.Exists(AFilePath))
                {
                    try
                    {
                        string orgZipPw = "i4&gmX5iyBOL";

                        using (ZipFile zip = ZipFile.Read(AFilePath))
                        {
                            bool bZipPw = _mZipPw != null && _mZipPw.Length > 0;

                            if (bZipPw)
                            {
                                try
                                {
                                    zip.Password = _mZipPw;
                                    zip.ExtractAll(AToPath);
                                    bZipOk = true;
                                }
                                catch (Exception ex)
                                {
                                    CProgram.sLogException("Unzip userPW " + AFilePath, ex);
                                }
                            }
                            if (bZipOk == false)
                            {
                                try
                                {
                                    zip.Password = orgZipPw;
                                    zip.ExtractAll(AToPath);
                                    bZipOk = true;
                                }
                                catch (Exception ex)
                                {
                                    CProgram.sLogException("Unzip stdPW" + AFilePath, ex);
                                }
                            }
                            if (bZipOk == false && CLicKeyDev.sbDeviceIsProgrammer())
                            {
                                string pw = "";
                                if (CProgram.sbReqLabel("Sirona unzip", "zip PW", ref pw, "", false))
                                {
                                    try
                                    {
                                        zip.Password = pw;
                                        zip.ExtractAll(AToPath);
                                        bZipOk = true;
                                    }
                                    catch (Exception ex)
                                    {
                                        CProgram.sLogException("Unzip dev dPW" + AFilePath, ex);
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException("Unzip " + AFilePath, ex);
                    }
                }
                if (bZipOk == false)
                {
                    bool bRenameOk = false;

                    string extension = "ErrorUnzip" + CProgram.sDateTimeToYMDHMS(DateTime.Now);
                    string badFilePath = Path.ChangeExtension(AFilePath, extension);

                    try
                    {
                        File.Move(AFilePath, badFilePath);
                        bRenameOk = true;
                    }
                    catch (Exception)
                    {
                    }

                    /*                   int maxTry = 5;
                                       int i = 1;

                                       do
                                       {
                                           extension = "ErrorUnzip" + i.ToString();
                                           badFilePath = Path.ChangeExtension(AFilePath, extension);

                                           try
                                           {
                                               File.Move(AFilePath, badFilePath);
                                               bRenameOk = true;
                                           }
                                           catch (Exception)
                                           {
                                           }

                                       } while (false == bRenameOk && ++i <= maxTry);
                   */
                    if (bRenameOk)
                    {
                        CProgram.sLogLine("Failed unzip, renamed: " + badFilePath);
                    }
                    else
                    {
                        CProgram.sLogLine("!! Failed unzip file and failed rename to " + badFilePath);
                    }
                }
            }
            catch (Exception)
            {
            }
            return bZipOk;
        }

        private bool mbCheckExistDestination(string AFromFilePath, string AToFilePath)
        {
            bool bExists = false;

            // check if file exists.
            //if exists check the size (size from bigger then current assume not processesd)

            FileInfo fiFrom = null;
            FileInfo fiTo = null;

            try
            {
                fiTo = new FileInfo(AToFilePath);
            }
            catch (Exception)
            {
                fiTo = null;
            }
            if (fiTo != null && fiTo.Exists)
            {
                bExists = true;
                try
                {
                    fiFrom = new FileInfo(AFromFilePath);
                }
                catch (Exception)
                {
                    fiFrom = null;
                }
                if (fiFrom != null)
                {
                    long sizeFrom = fiFrom.Exists ? fiFrom.Length : 0;
                    long sizeTo = fiTo.Exists ? fiTo.Length : 0;

                    if (sizeFrom > sizeTo)
                    {
                        string newName = Path.ChangeExtension(AToFilePath, Path.GetExtension(AToFilePath) + "_" + CProgram.sDateTimeToYMDHMS(DateTime.Now));

                        try
                        {
                            File.Move(AToFilePath, newName);
                            CProgram.sLogLine("File size " + sizeFrom.ToString() + ">" + sizeTo.ToString() + " rename:" + newName);
                            bExists = false;    // from bigger then 
                        }
                        catch (Exception)
                        {
                            CProgram.sLogLine("File size " + sizeFrom.ToString() + ">" + sizeTo.ToString() + " rename failed:" + newName);
                        }
                    }
                }

            }
            return bExists;
        }

        private UInt32 mFindImportFileInTable(string AFileName)
        {
            UInt32 foundRecNr = 0;


            try
            {
                int n = dataGridView.Rows.Count;
                while (n > 0)
                {
                    --n;
                    DataGridViewRow dgv = dataGridView.Rows[n];
                    if (dgv != null && dgv.Cells.Count >= (int)DDatagridColumn.NR_Columns)
                    {
                        string gridFileName = dgv.Cells[(int)DDatagridColumn.File].Value.ToString();
                        if (gridFileName == AFileName)
                        {
                            // file in grid found
                            string recStr = dgv.Cells[0].Value.ToString();

                            if (UInt32.TryParse(recStr, out foundRecNr))
                            {
                                if (foundRecNr > 0)
                                {
                                    break;  // found filename
                                }
                            }
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Search in table failed for " + AFileName, ex); ;
            }
            return foundRecNr;
        }

        private void mConvertIntriconZipFile(String AFilePath, UInt32 ASetInvertChannels, DImportMethod AImportMethod)
        {
            bool bOk = false;
            bool bDoEvent = true;
            bool bError = false;
            bool bDataRead = false;
            bool bDataCvs = false;
            double dTDataRed = 0.0F;
            double dTPlot = 0.0F;
            CRecordMit rec = null; // MIT record file
            DateTime startTime = DateTime.Now;
            String toPath;
            bool bMoveZip = _mbMoveZip && AImportMethod == DImportMethod.Automatic;
            bool bDelZip = _mbDeleteZipOnOk && AImportMethod == DImportMethod.Automatic;

            string fileNameExt = Path.GetFileName(AFilePath);
            mProcessSetInfo("zip=>" + fileNameExt);
            DateTime receiveUTC = DateTime.UtcNow;
            Int16 timeZoneOffset = CProgram.sGetLocalTimeZoneOffsetMin();

            UInt32 foundRecNr = mFindImportFileInTable(fileNameExt);

            if (foundRecNr > 0)
            {
                string newFileName = AFilePath + CProgram.sDateTimeToYMDHMS(startTime) + "R" + foundRecNr.ToString();
                try
                {
                    File.Move(AFilePath, newFileName);
                    CProgram.sLogLine("Sirona zip " + fileNameExt + " already imported R#= " + foundRecNr);
                }
                catch (Exception)
                {
                    CProgram.sLogError("Sirona Zip " + fileNameExt + " already imported and failed rename R#= " + foundRecNr);
                }
            }
            else
            if (mbChangeToDateDir(out toPath, _mRecordingDir, receiveUTC) == false)
            {
                bError = true;
            }
            else
            {
                string fileName = Path.GetFileNameWithoutExtension(AFilePath);
                string storeName = /*mCreateUtcName(receiveUTC, TimeZoneInfo.Local) + "_" +*/ _mRecStartName + "_" + fileName;

                storeName = CSqlDBaseConnection.sSqlLimitSqlString(storeName, CRecordMit.cStoredAtMaxLen);

                // use original  file name for directory to recognize existing files
                toPath = Path.Combine(toPath, storeName);

                try
                {
                    if (mbCreateDirectory(toPath))
                    {
                        String toFile = Path.Combine(toPath, fileNameExt);

                        //                        if (File.Exists(toFile))
                        if (mbCheckExistDestination(AFilePath, toFile))
                        {
                            #region zipExists
                            CProgram.sLogLine("Sirona Zip: Destination already exists (skip and wait next day)! " + fileNameExt);

                            /*                            bool bMoved = false;
                            int recNr = 0;

                            int n = dataGridView.Rows.Count;

                            while (n > 0)
                            {
                                --n;
                                DataGridViewRow dgv = dataGridView.Rows[n];
                                if (dgv != null && dgv.Cells.Count >= (int)DDatagridColumn.NR_Columns)
                                {
                                    string gridFileName = dgv.Cells[(int)DDatagridColumn.File].Value.ToString();
                                    if (gridFileName == fileNameExt)
                                    {
                                        // file in grid found
                                        string recStr = dgv.Cells[0].Value.ToString();

                                        if (int.TryParse(recStr, out recNr))
                                        {
                                            if (bMoveZip)
                                            {
                                                bMoved = true;
                                                string newFileName = Path.ChangeExtension(AFilePath, ".zip" + CProgram.sDateTimeToYMDHMS(startTime) + "-skip_R"+recNr.ToString());
                                                File.Move(AFilePath, newFileName);
                                            }
                                        }
                                        break;
                                    }
                                }
                            }
                            if (bMoved)
                            {
                                CProgram.sLogLine("Destination already exists (skip + renamed)! " + fileNameExt + " R#= " + recNr);
                            }
                            else
                            {
                                CProgram.sLogLine("Destination already exists (skip)! " + fileNameExt + " R#= " + recNr);
                            }
 */
                            #endregion
                        }
                        else
                        {
                            CProgram.sLogLine("Copy file to " + storeName + "\\" + fileNameExt + "...");
                            mStatusLine("Copy file to " + storeName + "\\" + fileNameExt + "...");
                            File.Copy(AFilePath, toFile);

                            if (File.Exists(toFile))
                            {
                                mStatusLine("Unzipping " + storeName + "\\" + fileNameExt + "...");

                                try
                                {
                                    bool bZipOk = mbUnzip(toFile, toPath);

                                    if (bZipOk == false)
                                    {
                                        bError = true;
                                    }
                                    else
                                    {
                                        try
                                        {
                                            DateTime time = DateTime.Now;
                                            string manualName = CRecordMit.sMakeManualName(DImportConverter.IntriconSironaZip, AImportMethod, AFilePath);

                                            rec = mImportIntriconFiles(out bDoEvent, toFile, ASetInvertChannels, receiveUTC, timeZoneOffset, storeName, DImportConverter.IntriconSironaZip,
                                                AImportMethod, manualName, _mbWipeJson);
                                        }
                                        catch (Exception)
                                        {
                                        }
                                        if (rec == null)
                                        {
                                            bOk = false;
                                            bError = true;
                                        }
                                        else if (bDoEvent && rec.mIndex_KEY == 0)
                                        {
                                            bError = true;
                                            bOk = false;
                                            CProgram.sLogLine("Failed to add record, rename zip to try again: " + AFilePath);
                                            string badFilePath = Path.ChangeExtension(toFile, "R0_zip");

                                            try
                                            {
                                                File.Move(toFile, badFilePath); // rename current zip to try again
                                            }
                                            catch (Exception)
                                            {
                                            }
                                        }
                                        else
                                        {
                                            bOk = true;
                                        }

                                        if (bOk && bMoveZip)
                                        {
                                            mStatusLine("Delete original " + AFilePath);
                                            try
                                            {
                                                File.Delete(AFilePath);
                                            }
                                            catch (Exception e)
                                            {
                                                CProgram.sLogException("Failed delete import Intricon Zip file " + fileNameExt, e);
                                            }
                                        }
                                    }
                                }
                                catch (Exception e)
                                {
                                    CProgram.sLogException("Failed to import Intricon Zip file " + fileNameExt, e);
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    CProgram.sLogException("Failed to import Intricon Zip file " + fileNameExt, e);
                    bError = true;
                }
            }

            if (bOk)
            {
                TimeSpan dT = DateTime.Now - startTime;
                CProgram.sLogLine("Convert " + fileNameExt + " finished in " + dT.TotalSeconds.ToString("0.00") + " Sec.");
                mIncreaseTotalCount();
            }
            else if (bError)
            {
                if (_mDisableErrorZipHour > 0.01)
                {
                    mCheckDisableImportFile(AFilePath);
                }

                mIncreaseErrorCount();
            }
            else
            {
                mIncreaseSkippedCount();
            }
        }


        private string mSironaExtratctSnr(string AName)
        {
            string snr = "";

            if (AName != null)
            {
                int i = AName.IndexOf('_');

                if (i > 0)
                {
                    snr = AName.Substring(0, i);
                }
                else
                {
                    snr = AName;
                }

            }
            return snr;
        }
        private void mConvertHttpSirFile(String AFilePath, DImportMethod AImportMethod)
        {
            try
            {
                string datFilePath = Path.ChangeExtension(AFilePath, ".dat");
                bool bEcgEvent = File.Exists(datFilePath);

                string name = Path.GetFileNameWithoutExtension(AFilePath);
                string deviceSerial = mSironaExtratctSnr(name);
#if newF
                string devicePath = null;

                // find study

                bool bMoved2Study = false;
                DateTime nowUTC = DateTime.UtcNow;
                UInt16 nDevice = 0;
                CDeviceInfo device = new CDeviceInfo(_mDBaseServer);
                UInt32 studyIX = 0;


                if (device != null)
                {
                    nDevice = device.mFindLoadSerial(0, deviceSerial, 0);

                    if (nDevice == 0)
                    {
                        CProgram.sLogLine("Sirona " + deviceSerial + ": unknown device!");
                    }
                    else if (nDevice > 1)
                    {
                        CProgram.sLogLine("Sirona " + deviceSerial + ": multiple devices n=" + nDevice.ToString());

                    }
                    else if (0 == (studyIX = device._mActiveStudy_IX ))
                    {
                        CProgram.sLogLine("Sirona " + deviceSerial + ": no study active!");
                    }
                    else if (device.mbIsRecording())
                        {
                        CProgram.sLogLine("Sirona " + deviceSerial + ": not assigned (study=" + studyIX.ToString() + ")");
                    }
                    else if (nowUTC < device._mRecorderStartUTC || nowUTC >= device._mRecorderEndUTC)
                    {
                        CProgram.sLogLine("Sirona " + deviceSerial + ": outside time window (study=" + studyIX.ToString() + ")");
                    }
                    else
                    {
                        string[] files = Directory.GetFiles(Path.ChangeExtension(AFilePath, ".*"));
                        int nFiles = files == null ? 0 : files.Length;

                        if (nFiles > 0)
                        {
                            string studyDir;

                            bMoved2Study = CDvtmsData.sbCreateStudyRecorderDir(out studyDir, device._mActiveStudy_IX);

                            if( bMoved2Study)
                            {
                                // copy or move files to study

                                int nCopy = 0;

                                for( int i = 0; i < nFiles; ++i )
                                {
                                    string from = files[i];
                                    string name2 = Path.GetFileName(from);
                                    string to = Path.Combine(studyDir, name2);

                                    if (bEcgEvent)
                                    {
                                        try
                                        {
                                            File.Copy(from, to, true);
                                            ++nCopy;
                                        }
                                        catch( Exception ex )
                                        {
                                            CProgram.sLogException("Failed to copy to study "+ studyIX.ToString() + ": " + name2, ex);
                                        }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            File.Move(from, to);
                                            ++nCopy;
                                        }
                                        catch (Exception ex)
                                        {
                                            CProgram.sLogException("Failed to move to study " + studyIX.ToString() + ": " + name2, ex);
                                        }
                                    }
                                }
                                CProgram.sLogLine(nCopy.ToString() + (bEcgEvent ? " copied to study " : "moved to study ") + studyIX.ToString() + " " + name);
                            }
                        }
                    }
                }

                if (false == bMoved2Study)
                {
                    // no data check file to old for conversion
                    mCheckDisableImportFile(AFilePath);
                }
                else if ( bEcgEvent )
                {
#endif
                mConvertHttpSirFileInclDat(AFilePath, AImportMethod);
                //               }
            }
            catch (Exception e)
            {
                CProgram.sLogException("Failed convert HttpSirFile file " + AFilePath, e);
            }
        }

        private void mConvertHttpSirFileInclDat(String AFilePath, DImportMethod AImportMethod)
        {
            bool bOk = false;
            //#if hjgfhdakj
            bool bError = false;
            bool bDataRead = false;
            bool bDataCvs = false;
            double dTDataRed = 0.0F;
            double dTPlot = 0.0F;
            CRecordMit rec = null; // MIT record file
            DateTime startTime = DateTime.Now;
            String toPath;
            bool bMoveHea = _mbMoveZip && AImportMethod == DImportMethod.Automatic;
            bool bDelHea = _mbDeleteZipOnOk && AImportMethod == DImportMethod.Automatic;

            string fileNameExt = Path.GetFileName(AFilePath);
            mProcessSetInfo("HttpSir>>" + fileNameExt);
            DateTime receiveUTC = DateTime.UtcNow;
            Int16 timeZoneOffset = CProgram.sGetLocalTimeZoneOffsetMin();

            UInt32 foundRecNr = mFindImportFileInTable(fileNameExt);

            if (foundRecNr > 0)
            {
                string newFileName = AFilePath + CProgram.sDateTimeToYMDHMS(startTime) + "R" + foundRecNr.ToString();
                try
                {
                    File.Move(AFilePath, newFileName);
                    CProgram.sLogLine("HttpSir " + fileNameExt + " already imported R#= " + foundRecNr);
                }
                catch (Exception)
                {
                    CProgram.sLogError("HttpSir " + fileNameExt + " already imported and failed rename R#= " + foundRecNr);
                }
            }
            else

            if (mbChangeToDateDir(out toPath, _mRecordingDir, receiveUTC) == false)
            {
                bError = true;
            }
            else
            {
                string fileName = Path.GetFileNameWithoutExtension(AFilePath);
                string storeName = /*mCreateUtcName(receiveUTC, TimeZoneInfo.Local) + "_" +*/ _mRecStartName + "_" + fileName;

                storeName = CSqlDBaseConnection.sSqlLimitSqlString(storeName, CRecordMit.cStoredAtMaxLen);

                // use original  file name for directory to recognize existing files
                toPath = Path.Combine(toPath, storeName);

                try
                {
                    if (mbCreateDirectory(toPath))
                    {
                        String toFile = Path.Combine(toPath, fileNameExt);

                        if (mbCheckExistDestination(AFilePath, toFile))
                        {
                            #region heaExists
                            bool bMoved = false;
                            int recNr = 0;

                            int n = dataGridView.Rows.Count;

                            while (n > 0)
                            {
                                --n;
                                DataGridViewRow dgv = dataGridView.Rows[n];
                                if (dgv != null && dgv.Cells.Count >= (int)DDatagridColumn.NR_Columns)
                                {
                                    string gridFileName = dgv.Cells[(int)DDatagridColumn.File].Value.ToString();
                                    if (gridFileName == fileNameExt)
                                    {
                                        // file in grid found
                                        string recStr = dgv.Cells[(int)DDatagridColumn.RecNr].Value.ToString();

                                        if (int.TryParse(recStr, out recNr))
                                        {
                                            if (bMoveHea)
                                            {
                                                bMoved = true;
                                                string newFileName = Path.ChangeExtension(AFilePath, ".skip" + CProgram.sDateTimeToYMDHMS(startTime) + "-hea");
                                                File.Move(AFilePath, newFileName);
                                            }
                                        }
                                        break;
                                    }
                                }
                            }
                            if (bMoved)
                            {
                                CProgram.sLogLine("Destination already exists (skip + renamed)! " + fileNameExt + " R#= " + recNr);
                            }
                            else
                            {
                                CProgram.sLogLine("Destination already exists (skip)! " + fileNameExt + " R#= " + recNr);
                            }
                            #endregion
                        }
                        else
                        {
                            #region heaNew
                            CProgram.sLogLine("Copy file(s) to " + storeName + "\\" + fileNameExt + "...");
                            mStatusLine("Copy file to " + storeName + "\\" + fileNameExt + "...");
                            //                            File.Copy(AFilePath, toFile);

                            string fromPath = Path.GetDirectoryName(AFilePath);
                            string[] files = System.IO.Directory.GetFiles(fromPath);
                            string name, toFilePath;
                            bool bDoEvent = true;

                            bOk = files != null ? files.Length > 0 : false;

                            try
                            {
                                // Copy the files with the same name.
                                foreach (string s in files)
                                {
                                    name = System.IO.Path.GetFileName(s);
                                    if (name.Contains(fileName))
                                    {
                                        toFilePath = System.IO.Path.Combine(toPath, name);
                                        System.IO.File.Copy(s, toFilePath, true);
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                bOk = false;
                                CProgram.sLogException("Failed to copy import Http Sirona file " + fileNameExt, e);
                            }
                            if (bOk && File.Exists(toFile))
                            {
                                try
                                {
                                    DateTime time = DateTime.Now;
                                    string manualName = CRecordMit.sMakeManualName(DImportConverter.SironaHttp, AImportMethod, AFilePath);

                                    rec = mImportIntriconFiles(out bDoEvent, toFile, 0, receiveUTC, timeZoneOffset, storeName, DImportConverter.SironaHttp,
                                        AImportMethod, manualName, _mbHttpWipeJson);
                                }
                                catch (Exception)
                                {
                                }
                                if (rec == null)
                                {
                                    bOk = false;
                                    bError = true;
                                }
                                else if (bDoEvent && rec.mIndex_KEY == 0)
                                {
                                    bError = true;
                                    bOk = false;
                                    CProgram.sLogLine("Failed to add record, rename hea to try again: " + AFilePath);
                                    string badFilePath = Path.ChangeExtension(toFile, "R0_hea");

                                    try
                                    {
                                        File.Move(toFile, badFilePath); // rename current zip to try again
                                    }
                                    catch (Exception)
                                    {
                                    }
                                }
                                else
                                {
                                    bOk = true;
                                }

                                if (bOk && bMoveHea && false == bError)
                                {
                                    mStatusLine("Delete original " + AFilePath);
                                    foreach (string s in files)
                                    {
                                        name = System.IO.Path.GetFileName(s);
                                        if (name.Contains(fileName))
                                        {
                                            try
                                            {
                                                File.Delete(s);
                                            }
                                            catch (Exception e)
                                            {
                                                CProgram.sLogException("Failed delete import Http Sirona file " + name, e);
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion
                        }
                    }
                }
                catch (Exception e)
                {
                    CProgram.sLogException("Failed to import Intricon Zip file " + fileNameExt, e);
                    bError = true;
                }
            }

            if (bOk)
            {
                TimeSpan dT = DateTime.Now - startTime;
                CProgram.sLogLine("Convert " + fileNameExt + " finished in " + dT.TotalSeconds.ToString("0.00") + " Sec.");
                mIncreaseTotalCount();
            }
            else if (bError)
            {
                if (_mDisableErrorZipHour > 0.01)
                {
                    mCheckDisableImportFile(AFilePath);
                }

                mIncreaseErrorCount();
            }
            else
            {
                mIncreaseSkippedCount();
            }
        }


        private void mCheckDisableImportFile(String AFilePath)
        {
            if (_mDisableErrorZipHour > 0.01 && _mStartRunTimeDT > DateTime.MinValue)
            {
                try
                {
                    double hours = (DateTime.Now - _mStartRunTimeDT).TotalHours;

                    if (hours >= 0.50)  // only rename error files when program has scanned for 30 minutes
                    {
                        try
                        {
                            FileInfo fi = new FileInfo(AFilePath);
                            if (fi != null && fi.Exists)
                            {
                                hours = (DateTime.UtcNow - fi.LastWriteTimeUtc).TotalHours;

                                if (hours >= _mDisableErrorZipHour)
                                {
                                    string newFileName = Path.ChangeExtension(AFilePath, Path.GetExtension(AFilePath) + "_ErrorOld" + CProgram.sDateTimeToYMDHMS(DateTime.Now));
                                    File.Move(AFilePath, newFileName);
                                    CProgram.sLogError("rename bad import to " + newFileName);
                                }
                            }
                        }
                        catch (Exception)
                        {
                            // do not care
                        }

                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("check disable file " + AFilePath, ex);
                }
            }
        }


        private void mImportHeaFile(String AFilePath, DImportMethod AImportMethod, bool AbWipeJson)
        {
            bool bOk = false;
            bool bDoEvent = false;
            bool bError = false;
            bool bDataRead = false;
            bool bDataCvs = false;
            double dTDataRed = 0.0F;
            double dTPlot = 0.0F;
            CRecordMit rec = null; // MIT record file
            DateTime startTime = DateTime.Now;
            String toPath;

            string fileNameExe = Path.GetFileName(AFilePath);

            mProcessSetInfo("HEA>>" + fileNameExe);
            DateTime receiveUTC = DateTime.UtcNow;
            Int16 timeZoneOffset = CProgram.sGetLocalTimeZoneOffsetMin();

            if (mbChangeToDateDir(out toPath, _mRecordingDir, receiveUTC) == false)
            {
                bError = true;
            }
            else
            {
                string fileName = Path.GetFileNameWithoutExtension(AFilePath);
                string startName = "ImportHEA";
                string storeName = /*mCreateUtcName(receiveUTC, TimeZoneInfo.Local) + "_" +*/ startName + "_" + fileName;

                storeName = CSqlDBaseConnection.sSqlLimitSqlString(storeName, CRecordMit.cStoredAtMaxLen);

                // use original  file name for directory to recognize existing files
                toPath = Path.Combine(toPath, storeName);

                try
                {
                    if (mbCreateDirectory(toPath))
                    {
                        String toFile = Path.Combine(toPath, fileNameExe);

                        if (File.Exists(toFile))
                        {
                            CProgram.sLogLine("Destination already exists (skip)!");
                        }
                        else
                        {
                            string fromPath = Path.GetDirectoryName(AFilePath);
                            string[] files = System.IO.Directory.GetFiles(fromPath);
                            string name, toFilePath;

                            // Copy the files with the same name.
                            foreach (string s in files)
                            {
                                name = System.IO.Path.GetFileName(s);
                                if (name.Contains(fileName))
                                {
                                    toFilePath = System.IO.Path.Combine(toPath, name);
                                    System.IO.File.Copy(s, toFilePath, true);
                                }
                            }

                            if (File.Exists(toFile))
                            {
                                mStatusLine("Importing " + storeName + "\\" + fileNameExe + "...");

                                try
                                {
                                    bOk = true;
                                    DateTime time = DateTime.Now;
                                    string manualName = CRecordMit.sMakeManualName(DImportConverter.IntriconSironaMit, AImportMethod, AFilePath);

                                    rec = mImportIntriconFiles(out bDoEvent, toFile, 0, receiveUTC, timeZoneOffset, storeName, DImportConverter.IntriconSironaMit,
                                        AImportMethod, manualName, AbWipeJson);
                                    if (rec == null)
                                    {
                                        bOk = false;
                                        bError = true;
                                    }
                                    else
                                    {
                                        bOk = true;
                                    }
                                }
                                catch (Exception e)
                                {
                                    CProgram.sLogException("Failed to Import Header File " + AFilePath, e);
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    CProgram.sLogException("Failed to Import Header File " + AFilePath, e);
                    bError = true;
                }
            }

            if (bOk)
            {
                TimeSpan dT = DateTime.Now - startTime;
                CProgram.sLogLine("Import " + fileNameExe + " finished in " + dT.TotalSeconds.ToString("0.00") + " Sec.");
                mIncreaseTotalCount();
            }
            else if (bError)
            {
                mIncreaseErrorCount();
            }
            else
            {
                mIncreaseSkippedCount();
            }
        }


        bool mbCheckDaysLeft()
        {
            int daysLeft = CLicKeyDev.sGetDaysLeft();
            bool bOk = daysLeft >= 0;

            if (!bOk)
            {
                CProgram.sPromptError(true, "DVTMS: i-Reader", "License past end time!, contact dealer!");
                Close();

            }
            return bOk;
        }
        private void toolStripLoadOne_Click(object sender, EventArgs e)
        {
            mButtonLoadOneZip();
        }
        private void mButtonLoadOneZip()
        {
            if (!mbCheckDaysLeft())
            {
                return;
            }

            if (_mbRunningConversion || _mbRunLoop)
            {
                CProgram.sLogLine("File conversion already running, press stop first!");
            }
            else
            {
                mSetRun(false);

                openZipFileDialog.RestoreDirectory = true;
                openZipFileDialog.InitialDirectory = _mZipFromDir;
                if (false == mbIsRunNamePresent())
                {
                    openZipFileDialog.FileName = _mZipFromDir;
                }
                else
                {
                    openZipFileDialog.FileName = Path.Combine(_mZipFromDir, mGetRunFileName());
                }

                if (openZipFileDialog.ShowDialog() == DialogResult.OK)
                {
                    _mbRunningConversion = true;

                    String selectedFile = openZipFileDialog.FileName;
                    CProgram.sSetBusy(true);

                    CProgram.sLogLine("Convert file " + selectedFile);

                    foreach (String file in openZipFileDialog.FileNames)
                    {
                        CProgram.sLogLine("Convert file " + file);

                        mConvertIntriconZipFile(file, 0, DImportMethod.Manual);
                    }
                    CProgram.sSetBusy(false);

                    _mbRunningConversion = false;
                }
            }

        }

        private void toolStripStop_Click(object sender, EventArgs e)
        {
            mSetRun(false);
        }

        private void toolStripRun_Click(object sender, EventArgs e)
        {
            mSetRun(true);
        }

        private void toolStripOpenEvent_Click(object sender, EventArgs e)
        {
            try
            {
                string toFile = "";
                DateTime dtUtc;
                UInt32 recIndex;

                if (mbGetCursorFilePath(out toFile, out recIndex, out dtUtc))
                {
                    string ext = Path.GetExtension(toFile);
                    string exe = _mHeaViewExe;

                    if (ext == ".scp")
                    {
                        exe = _mScpViewExe;
                    }
                    else
                    {
                        toFile = Path.ChangeExtension(toFile, "dat");
                    }
                    if (File.Exists(exe))
                    {
                        ProcessStartInfo startInfo = new ProcessStartInfo();
                        startInfo.FileName = exe;
                        startInfo.Arguments = toFile;
                        Process.Start(startInfo);
                    }
                }
            }
            catch (Exception e2)
            {
                CProgram.sLogException("Failed toolStripOpen", e2);
            }
        }

        private void toolStripButtonOpenFolder_Click(object sender, EventArgs e)
        {
            // open explorer with file at cursor
            try
            {
                string toFile = "";
                string toPath = "";
                DateTime dtUtc;
                UInt32 recIndex;

                bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

                if (bCtrl) //&& CLicKeyDev.sbDeviceIsProgrammer())
                {
                    bool bShift = (Control.ModifierKeys & Keys.Shift) == Keys.Shift;

                    if (bShift)
                    {
                        // check .dvxEvtRec
                        if (mbGetCursorFilePath(out toFile, out recIndex, out dtUtc))
                        {
                            string ext = Path.GetExtension(toFile);

                            if (ext == CDvxEvtRec._cEvtRecExtension)
                            {
                                // open event record
                                bool bChecksum;
                                CDvxEvtRec dvxRec = CDvxEvtRec.sLoadDvxEvtRecFile(out bChecksum, toFile, false);

                                if (dvxRec != null)
                                {
                                    if (dvxRec._mSourceFilePath != null && dvxRec._mSourceFilePath.Length > 5)
                                    {
                                        toPath = Path.GetDirectoryName(dvxRec._mSourceFilePath);
                                        CProgram.sLogLine("Opening dvx source path:" + toPath);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        // check if study nr is in 

                        try
                        {
                            DataGridViewRow index = dataGridView.CurrentRow;

                            if (index != null && index.Cells.Count >= (int)DDatagridColumn.NR_Columns)
                            {
                                string studyName = index.Cells[(int)DDatagridColumn.PatID].Value.ToString();

                                if (studyName != null && studyName.StartsWith("S#"))
                                {
                                    string studyNr = "";
                                    int pos = 1;
                                    int n = studyName.Length;
                                    UInt32 studyIX;

                                    while (++pos < n)
                                    {
                                        char c = studyName[pos];

                                        if (Char.IsDigit(c))
                                        {
                                            studyNr += c;
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                    if (UInt32.TryParse(studyNr, out studyIX) && studyIX > 0)
                                    {
                                        CDvtmsData.sbGetStudyCaseDir(out toPath, studyIX, false);
                                        CProgram.sLogLine("Opening study S" + studyIX.ToString() + ":" + toPath);
                                    }
                                }
                            }
                        }
                        catch (Exception e2)
                        {
                            CProgram.sLogException("Failed get cursor study recording  path", e2);
                        }

                    }

                }
                else if (mbGetCursorFilePath(out toFile, out recIndex, out dtUtc))
                {
                    toPath = Path.GetDirectoryName(toFile);
                    CProgram.sLogLine("Opening record R" + recIndex.ToString() + ":" + toPath);
                }
                if (toPath.Length > 0)
                {
                    ProcessStartInfo startInfo = new ProcessStartInfo();
                    startInfo.FileName = @"explorer";
                    startInfo.Arguments = toPath;
                    Process.Start(startInfo);
                    //                    mbGetCursorUpdateRead("+");

                }
            }
            catch (Exception e2)
            {
                CProgram.sLogException("Failed open explorer", e2);
            }
        }

        public bool mbReadSqlDbTable()
        {
            bool bOk = false;
            CSqlCmd cmd = null;
            try
            {
                CProgram.sSetBusy(true);
                int nrHours = 24;

                if (toolStripNrHours.Text == null || false == int.TryParse(toolStripNrHours.Text, out nrHours))
                {
                    nrHours = 1;
                }
                if (mbCreateDBaseConnection())
                {
                    CRecordMit rec = new CRecordMit(_mDBaseServer);

                    if (rec == null)
                    {
                        CProgram.sLogLine("Failed to init cmd");
                    }
                    else
                    {
                        cmd = rec.mCreateSqlCmd(_mDBaseServer);

                        if (cmd == null)
                        {
                            CProgram.sLogLine("Failed to init cmd");

                        }
                        else if (false == cmd.mbPrepareSelectCmd(true, rec.mMaskValid))
                        {
                            CProgram.sLogLine("Failed to prepare SQL row in table " + rec.mGetDbTableName());
                        }
                        else
                        {
                            DateTime utc = CProgram.sGetUtcNow();
                            string name = rec.mVarGetName((UInt16)DRecordMitVars.ReceivedUTC);
                            string cmp = rec.mSqlDateTimeCompareSqlString(name, utc, DSqlCompareDateTime.Hour, nrHours);
                            UInt64 mask = cmd.mGetCmdVarFlags();
                            string sqlWhere = mMakeSqlSearchString();
                            int n = 0;
                            DateTime dt = DateTime.Now;

                            dataGridView.Rows.Clear();
                            cmd.mWhereAddString(cmp);

                            if (sqlWhere != null && sqlWhere.Length > 0)
                            {
                                cmd.mWhereAddString(sqlWhere);
                            }

                            if (cmd.mbExecuteSelectCmd())
                            {
                                bool bError;
                                bOk = true;
                                //                                string header, line;
                                //                                rec.mbGetVarNames(out header, mask, ", ");
                                //                                CProgram.sLogLine("#, " + header);                        

                                while (cmd.mbReadOneRow(out bError))
                                {
                                    //                                   rec.mbGetVarValues(out line, mask, ", ");
                                    mbStoreGrid(rec);
                                    ++n;
                                    //                                   CProgram.sLogLine( cmd.mGetNrRowsRead().ToString() +", " + line);
                                    rec.mClear(); // clear for next read
                                }
                                bOk = false == bError;
                                TimeSpan ts = DateTime.Now - dt;
                                CProgram.sLogLine((bOk ? "" : "!Incomplete! ") + "Updated " + n.ToString() + " table rows in " + ts.TotalSeconds.ToString("0.000") + " sec.");

                                if (n > _mMaxNrRowsListed)
                                {
                                    if (CProgram.sbReqInt32("Loaded " + n.ToString() + " rows", "Increase show maximum ", ref n, "rows", 10, 9999999))
                                    {
                                        _mMaxNrRowsListed = (UInt32)n;
                                    }
                                }
                            }
                            else
                            {
                                CProgram.sLogLine("Failed to search SQL row in table " + rec.mGetDbTableName() + " {?=" + cmd.mGetWhereString() + "}");
                            }
                        }
                        cmd.mCloseCmd();
                    }

                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed read sql table", ex);
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.mCloseCmd();
                }
            }
            CProgram.sSetBusy(false);
            return bOk;
        }

        private void toolStripLoadBD_Click(object sender, EventArgs e)
        {
            mbReadSqlDbTable();
        }

        private void toolStripDropDownSearch_Click(object sender, EventArgs e)
        {
            toolStripEditSearch.Text = "";
        }


        public string mGetImageFullName(ushort ASignalIndex, string AFileName, bool AbOld)
        {
            string s = "Full";

            if (AbOld == false)
            {
                s += (ASignalIndex + 1).ToString("00");   // program uses 0, user starts at 1
            }
            return s + "_" + AFileName + ".png";
        }
        public string mGetImageEventName(ushort ASignalIndex, string AFileName, bool AbOld)
        {
            string s = "Event";

            if (AbOld == false)
            {
                s += (ASignalIndex + 1).ToString("00");
            }
            return s + "_" + AFileName + ".png";
        }
        public void mUpdateStrips()
        {
            // show pictures
            try
            {
                string toFile = "";
                DateTime dt = DateTime.Now;
                DateTime dtReceivedUTC;
                ushort signalIndex = 0;
                string strIndex = comboBoxSignal.Text;
                UInt32 recIndex;

                if (false == ushort.TryParse(strIndex, out signalIndex))
                {
                    signalIndex = 1;
                }
                if (signalIndex > 0) --signalIndex;     // program starts at 0, user at 1

                if (mbGetCursorFilePath(out toFile, out recIndex, out dtReceivedUTC))
                {
                    bool bFound = false;
                    bool bOld = false;
                    string fileName = Path.GetFileNameWithoutExtension(toFile);
                    string toPath = Path.GetDirectoryName(toFile);
                    string name = mGetImageFullName(signalIndex, fileName, false);
                    string toFilePath = Path.Combine(toPath, name);

                    string result = recIndex.ToString() + "<" + name;

                    pictureBoxFull.Image = null;

                    bFound = File.Exists(toFilePath);
                    if (false == bFound && signalIndex == 0)
                    {
                        name = mGetImageFullName(0, fileName, true);
                        toFilePath = Path.Combine(toPath, name);

                        bFound = File.Exists(toFilePath);
                        if (bFound)
                        {
                            signalIndex = 0;
                            bOld = true;
                        }
                    }

                    if (bFound)
                    {
                        Image img = Image.FromFile(toFilePath);

                        if (img != null)
                        {
                            pictureBoxFull.Image = img;

                            result += "(" + img.Width.ToString() + "x" + img.Height.ToString() + ")";
                        }
                        else
                        {
                            result += " failed to load Full image!";
                        }
                    }
                    else
                    {
                        result += " does not exist!";
                    }
                    labelImageFull.Text = result;

                    pictureBoxEvent.Image = null;
                    name = mGetImageEventName(signalIndex, fileName, bOld);
                    result = recIndex.ToString() + "<" + name;
                    toFilePath = Path.Combine(toPath, name);

                    if (File.Exists(toFilePath))
                    {
                        Image img = Image.FromFile(toFilePath);

                        if (img != null)
                        {
                            pictureBoxEvent.Image = img;

                            result += "(" + img.Width.ToString() + "x" + img.Height.ToString() + ")";
                        }
                        else
                        {
                            result += " failed to load Event image!";
                        }
                    }
                    else
                    {
                        result += " does not exist!";
                    }
                    TimeSpan ts = DateTime.Now - dt;

                    result += " in " + ts.TotalSeconds.ToString("0.000") + " sec";

                    labelImageEvent.Text = result;

                    result = "-";
                    if (dtReceivedUTC > DateTime.MinValue)
                    {
                        result = "ReceivedUTC= " + CProgram.sDateTimeToString(dtReceivedUTC);
                    }
                    labelReadImgUtc.Text = result;
                }
            }
            catch (Exception e2)
            {
                CProgram.sLogException("Failed  update strip", e2);
            }
        }

        private void dataGridView_DoubleClick(object sender, EventArgs e)
        {
            // show pictures
            CProgram.sSetBusy(true);
            mUpdateStrips();
            CProgram.sSetBusy(false);
        }

        private void dataGridView_Click(object sender, EventArgs e)
        {
            // show pictures
            CProgram.sSetBusy(true);
            mUpdateStrips();
            CProgram.sSetBusy(false);

        }

        private void dataGridView_SelectionChanged(object sender, EventArgs e)
        {
            // show pictures
            CProgram.sSetBusy(true);
            mUpdateStrips();
            CProgram.sSetBusy(false);
        }

        private void toolStripButtonLoadHea_Click(object sender, EventArgs e)
        {
            mButtonLoadHea();
        }

        private bool mMergeHeaFiles(out string AFileMergedHea, out string AFileMergedDat, string[] AFileList)
        {
            bool bOk = false;
            string tempFileHea = "";
            string tempFileDat = "";
            try
            {
                string[] files = AFileList;
                string tempName = "merge" + CProgram.sDateTimeToYMDHMS(DateTime.Now);

                tempFileDat = Path.Combine(CProgram.sGetProgDir(), tempName + ".dat");
                tempFileHea = Path.Combine(CProgram.sGetProgDir(), tempName + ".hea");
                Array.Sort(files);

                if (File.Exists(tempFileHea))
                {
                    File.Delete(tempFileHea);
                }
                if (File.Exists(tempFileDat))
                {
                    File.Delete(tempFileDat);
                }
                string lines = File.ReadAllText(files[0]);

                foreach (string file in files)
                {
                    lines += "#file=" + file + "\r\n";
                }
                File.WriteAllText(tempFileHea, lines);

                using (FileStream writerDat = File.Create(tempFileDat))
                {
                    foreach (string file in files)
                    {
                        string fileDat = Path.ChangeExtension(file, ".dat");
                        byte[] data = File.ReadAllBytes(fileDat);
                        writerDat.Write(data, 0, data.Length);
                        CProgram.sLogLine("Merging Hea:" + file);
                    }
                    writerDat.Flush();
                    writerDat.Close();
                    bOk = true;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Merge HEA", ex);
            }

            AFileMergedHea = tempFileHea;
            AFileMergedDat = tempFileDat;
            return bOk;

        }

        private void mButtonLoadHea()
        {
            if (!mbCheckDaysLeft())
            {
                return;
            }
            if (_mbRunningConversion || _mbRunLoop)
            {
                CProgram.sLogLine("File conversion already running, press stop first!");
            }
            else
            {
                mSetRun(false);

                //openHeaFileDialog.RestoreDirectory = true;
                openHeaFileDialog.Title = "Import MIT HEA File";

                openHeaFileDialog.InitialDirectory = _mHeaFromDir;
                if (false == mbIsRunNamePresent())
                {
                    openHeaFileDialog.FileName = _mHeaFromDir;
                }
                else
                {
                    openHeaFileDialog.FileName = Path.Combine(_mHeaFromDir, mGetRunFileName());
                }

                if (openHeaFileDialog.ShowDialog() == DialogResult.OK)
                {
                    int nFiles = openHeaFileDialog.FileNames == null ? 0 : openHeaFileDialog.FileNames.Length;
                    bool bMerge = nFiles > 1;
                    if (bMerge)
                    {
                        if (false == CProgram.sbReqBool("Import HEA", "Merge DAT files (continuous same sps and nr signals)", ref bMerge))
                        {
                            nFiles = 0;
                        }
                    }

                    if (nFiles > 0)
                    {
                        if (bMerge)
                        {
                            _mbRunningConversion = true;
                            CProgram.sSetBusy(true);
                            string tempFileHea, tempFileDat;

                            if (mMergeHeaFiles(out tempFileHea, out tempFileDat, openHeaFileDialog.FileNames))
                            {
                                CProgram.sLogLine("Import file " + tempFileHea);
                                mImportHeaFile(tempFileHea, DImportMethod.Manual, false);
                                if (File.Exists(tempFileHea))
                                {
                                    File.Delete(tempFileHea);
                                }
                                if (File.Exists(tempFileDat))
                                {
                                    File.Delete(tempFileDat);
                                }

                            }

                            CProgram.sSetBusy(false);
                            _mbRunningConversion = false;
                        }
                        else
                        {
                            _mbRunningConversion = true;
                            CProgram.sSetBusy(true);

                            foreach (String file in openHeaFileDialog.FileNames)
                            {
                                CProgram.sLogLine("Import file " + file);
                                mImportHeaFile(file, DImportMethod.Manual, false);
                            }

                            CProgram.sSetBusy(false);
                            _mbRunningConversion = false;
                        }
                    }
                }
            }
        }


        public bool mbCreateDBaseConnection()
        {
            bool bOk = _mDBaseServer != null;

            if (bOk)
            {
                try
                {
                    //mDBaseServer.mCloseSqlCmd(false);
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Close command db connection to server:" + _mSqlServer, ex);
                }
            }
            else
            {
                _mDBaseServer = new CSqlDBaseConnection();

                if (_mDBaseServer != null && _mSqlServer != null)
                {
                    string enumPath = Path.Combine(CProgram.sGetProgDir(), "enums");
                    CDvtmsData.sSetDBaseServer(_mRecordingDir, _mDBaseServer, enumPath);  // needed for collection outside FormIReader

                    string server = _mSqlServer.mDecrypt();
                    bOk = _mDBaseServer.mbSqlSetupServer(server, _mSqlPort.mDecrypt(),
                        _mSqlSslMode, _mSqlDbName.mDecrypt(), _mSqlUser.mDecrypt(), _mSqlPassword.mDecrypt());
                    if (false == bOk)
                    {
                        CProgram.sLogError("Invalid settings for server " + server);
                    }
                    else
                    {
                        bOk = _mDBaseServer.mbSqlCreateConnection();
                        if (false == bOk)
                        {
                            CProgram.sLogError("Failed connection to server " + server);
                        }
                        else
                        {
                            CProgram.sLogLine("Connected to dBase " + _mDBaseServer.mSqlGetServerName() + "." + _mDBaseServer.mSqlGetDBaseName() + ", ssl mode= " + _mSqlSslMode.ToString());
                        }
                    }
                }
                if (false == bOk)
                {
                    _mDBaseServer.mSqlDisposeConnection();
                    _mDBaseServer = null;
                }
            }
            return bOk;
        }
        public bool mbTestDBaseConnection(out string ArState)
        {
            string state = "?";
            bool bOk = _mDBaseServer != null;

            if (bOk)
            {
                try
                {
                    _mDBaseServer.mCloseSqlCmd(true);
                    state = "OK";
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Close command db connection to server:" + _mSqlServer, ex);
                    state = "Cmd error ";
                }
                bOk = CDvtmsData.sbTestDBaseServer();  // testc by downloading a known enum list;
                if (false == bOk)
                {
                    state += "Failed";
                    CProgram.sLogError("test DBase connection: " + state + " =>disconnecting DB");
                    mDisposeDBaseConnection();
                }
            }
            else
            {
                state = "Not connected";
            }
            ArState = state;
            return bOk;
        }

        public void mDisposeDBaseConnection()
        {
            if (_mDBaseServer != null)
            {
                _mDBaseServer.mSqlDisposeConnection();
                CProgram.sLogLine("DisConnected from dBase " + _mDBaseServer.mSqlGetServerName() + "." + _mDBaseServer.mSqlGetDBaseName());
                _mDBaseServer = null;
            }
        }

        private bool mbTestDB()
        {
            int iLoop = 1;
            UInt64 loadMask = CSqlDataTableRow.sGetMask((UInt16)DDeviceInfoVars.DeviceSerialNr);
            //bool bError = true;

            do
            {
                try
                {
                    //CDeviceInfo device = new CDeviceInfo(_mDBaseServer);

                    if (iLoop > 2)
                    {
                        mDisposeDBaseConnection();
                        System.Threading.Thread.Sleep(100);
                        mbCreateDBaseConnection();
                    }

                    if (CDvtmsData.sbTestDBaseServer())
                    //                    if (device.mbDoSqlTestSelectIndex(out bError, 1, loadMask)) // check if we can do a select
                    {
                        iLoop = 0;  // got device 1, DB connection ok
                    }
                    else
                    {
                        ++iLoop;
                        CProgram.sLogError("Check DB connection failed " + iLoop.ToString());

                        /*                      if (bError == false)
                                              {
                                                  iLoop = 0;  // did not give an error( exception ) so that is good as well 
                                              }
                                              else
                                              {
                                                  ++iLoop;    // select gave an error
                                              }
                      */
                    }

                }
                catch (Exception e)
                {
                    CProgram.sLogException("Check DB connection Failed, try " + iLoop.ToString(), e);
                    ++iLoop;
                }
            } while (iLoop > 0 && iLoop < 5);

            return iLoop == 0;

        }


        private void toolStripTest_Click(object sender, EventArgs e)
        {
            CProgram.sLogLine("Test");

            if (mbCreateDBaseConnection())
            {
                CRecordMit table = new CRecordMit(_mDBaseServer);

                //CSqlDataTableRow table = new  CSqlDataTableRow(mDBaseServer, "record");
                DSqlDataType type;
                string name, sqlName, typeName;

                table.mSetSqlConnection(_mDBaseServer);

                for (ushort i = 0; i < CSqlDataTableRow.cMaxNrColums; ++i)
                {
                    type = table.mVarGetInfo(i, out name, out sqlName);
                    if (type != DSqlDataType.SqlError)
                    {
                        typeName = type.ToString();
                        CProgram.sLogLine(i.ToString("00") + "= " + typeName + "\t" + name + "\t" + sqlName);
                    }
                }
                UInt64 mask = table.mMaskAll;
                table.mbGetVarNames(out name, mask, ", ");
                CProgram.sLogLine("nameList=0x" + mask.ToString("X") + "= " + name);
                table.mbGetVarSetList(out name, mask, ", ");
                CProgram.sLogLine("nameSet=0x" + mask.ToString("X") + "= " + name);
                table.mSetCreated2CurrentUser();
                table.mbGetVarSetList(out name, mask, ", ");
                CProgram.sLogLine("nameSet=0x" + mask.ToString("X") + "= " + name);
                table.mSetChanged2CurrentUser();
                table.mbGetVarWhereList(out name, mask);
                CProgram.sLogLine("nameWhere=0x" + mask.ToString("X") + "= " + name);
                mDisposeDBaseConnection();
            }
        }

        private void toolStripImportTzSCP_Click(object sender, EventArgs e)
        {
            mButtonImportTzSCP();
        }
        private void mButtonImportTzSCP()
        {
            if (!mbCheckDaysLeft())
            {
                return;
            }
            if (_mbRunningConversion || _mbRunLoop)
            {
                CProgram.sLogLine("File conversion already running, press stop first!");
            }
            else
            {
                mSetRun(false);

                openScpFileDialog.Title = "Import SCP File";

                //openHeaFileDialog.RestoreDirectory = true;
                openScpFileDialog.InitialDirectory = _mScpFromDir;
                if (false == mbIsRunNamePresent())
                {
                    openScpFileDialog.FileName = _mScpFromDir;
                }
                else
                {
                    openScpFileDialog.FileName = Path.Combine(_mScpFromDir, mGetRunFileName());
                }

                if (openScpFileDialog.ShowDialog() == DialogResult.OK)
                {
                    int nFiles = openScpFileDialog.FileNames == null ? 0 : openScpFileDialog.FileNames.Length;
                    bool bMerge = nFiles > 1;
                    if (bMerge)
                    {
                        if (false == CProgram.sbReqBool("Import SCP", "Merge SCP files as one event", ref bMerge))
                        {
                            nFiles = 0;
                        }
                    }

                    if (nFiles > 0)
                    {
                        if (bMerge)
                        {
                            string newName = "merge" + CProgram.sDateTimeToYMDHMS(DateTime.Now);
                            _mbRunningConversion = true;

                            CProgram.sSetBusy(true);

                            mImportTzScpFiles(openScpFileDialog.FileNames, newName);

                            CProgram.sSetBusy(false);
                            _mbRunningConversion = false;
                        }
                        else
                        {
                            _mbRunningConversion = true;

                            CProgram.sSetBusy(true);
                            foreach (String file in openScpFileDialog.FileNames)
                            {
                                CProgram.sLogLine("Import file " + file);
                                mImportTzScpFile(file);
                            }

                            CProgram.sSetBusy(false);
                            _mbRunningConversion = false;
                        }
                    }
                }
            }

        }
        private void mImportTzScpFile(String AFilePath)
        {
            bool bOk = false;
            bool bError = false;
            bool bDataRead = false;
            bool bDataCvs = false;
            double dTDataRed = 0.0F;
            double dTPlot = 0.0F;
            CRecordMit rec = null; // MIT record file
            DateTime startTime = DateTime.Now;
            String toPath;

            string fileNameExe = Path.GetFileName(AFilePath);
            mProcessSetInfo("TzScp>>" + fileNameExe);
            DateTime receiveUTC = DateTime.UtcNow;
            Int16 timeZoneOffset = CProgram.sGetLocalTimeZoneOffsetMin();

            if (mbChangeToDateDir(out toPath, _mRecordingDir, receiveUTC) == false)
            {
                bError = true;
            }
            else
            {
                DateTime utcFile = File.GetLastWriteTimeUtc(AFilePath);
                string utcTime = utcFile.ToString("yyyyMMddhhmmss");
                string fileName = Path.GetFileNameWithoutExtension(AFilePath);
                string startName = "TzScp";
                string storeName = /*mCreateUtcName(receiveUTC, TimeZoneInfo.Local) + "_" +*/ startName + "" + utcTime + "_" + fileName;
                string utcTimeString = mCreateUtcName(receiveUTC, TimeZoneInfo.Local);

                storeName = CSqlDBaseConnection.sSqlLimitSqlString(storeName, CRecordMit.cStoredAtMaxLen);

                // use original  file name for directory to recognize existing files
                toPath = Path.Combine(toPath, storeName);

                try
                {
                    if (mbCreateDirectory(toPath))
                    {
                        String toFile = Path.Combine(toPath, fileNameExe);

                        if (File.Exists(toFile))
                        {
                            CProgram.sLogLine("Destination already exists (skip)!");
                        }
                        else
                        {
                            string fromPath = Path.GetDirectoryName(AFilePath);
                            string[] files = System.IO.Directory.GetFiles(fromPath);
                            string name, toFilePath;

                            // Copy the files with the same name.
                            foreach (string s in files)
                            {
                                name = System.IO.Path.GetFileName(s);
                                if (name.Contains(fileName))
                                {
                                    toFilePath = System.IO.Path.Combine(toPath, name);
                                    System.IO.File.Copy(s, toFilePath, true);
                                }
                            }

                            if (File.Exists(toFile))
                            {
                                mStatusLine("Importing " + storeName + "\\" + fileNameExe + "...");

                                try
                                {
                                    bOk = true;
                                    DateTime time = DateTime.Now;
                                    string manualName = CRecordMit.sMakeManualName(DImportConverter.TZAeraScp, DImportMethod.Manual, AFilePath);

                                    rec = mImportScpFiles(toFile, 0, receiveUTC, timeZoneOffset, storeName, utcTimeString, DImportMethod.Manual, manualName, DImportConverter.TZAeraScp,
                                        false, CLicKeyDev.sbDeviceIsProgrammer());
                                    if (rec == null)
                                    {
                                        bOk = false;
                                        bError = true;
                                    }
                                    else
                                    {
                                        bOk = true;
                                    }
                                }
                                catch (Exception e)
                                {
                                    CProgram.sLogException("Failed to Import Scp File", e);
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    CProgram.sLogException("Failed to Import SCP File", e);
                    bError = true;
                }
            }

            if (bOk)
            {
                TimeSpan dT = DateTime.Now - startTime;
                CProgram.sLogLine("Import " + fileNameExe + " finished in " + dT.TotalSeconds.ToString("0.00") + " Sec.");
                mIncreaseTotalCount();
            }
            else if (bError)
            {
                mIncreaseErrorCount();
            }
            else
            {
                mIncreaseSkippedCount();
            }
        }
        private void mImportTzScpFiles(String[] AFileList, string ANewName)
        {
            if (AFileList != null && AFileList.Length > 0 && ANewName != null && ANewName.Length > 0)
            {
                bool bOk = false;
                bool bError = false;
                bool bDataRead = false;
                bool bDataCvs = false;
                double dTDataRed = 0.0F;
                double dTPlot = 0.0F;
                CRecordMit rec = null; // MIT record file
                DateTime startTime = DateTime.Now;
                String toPath;
                Array.Sort(AFileList);

                string firstName = Path.GetFileName(AFileList[0]);

                mProcessSetInfo("TzScp>>" + ANewName);
                DateTime receiveUTC = DateTime.UtcNow;
                Int16 timeZoneOffset = CProgram.sGetLocalTimeZoneOffsetMin();

                if (mbChangeToDateDir(out toPath, _mRecordingDir, receiveUTC) == false)
                {
                    bError = true;
                }
                else
                {
                    DateTime utcFile = File.GetLastWriteTimeUtc(AFileList[0]);
                    string utcTime = utcFile.ToString("yyyyMMddhhmmss");
                    string startName = "TzScp";
                    string storeName = /*mCreateUtcName(receiveUTC, TimeZoneInfo.Local) + "_" +*/ startName + "_" + utcTime + "_" + ANewName;
                    string utcTimeString = mCreateUtcName(receiveUTC, TimeZoneInfo.Local);

                    storeName = CSqlDBaseConnection.sSqlLimitSqlString(storeName, CRecordMit.cStoredAtMaxLen);

                    // use original  file name for directory to recognize existing files
                    toPath = Path.Combine(toPath, storeName);

                    try
                    {
                        if (mbCreateDirectory(toPath))
                        {
                            String toFile = Path.Combine(toPath, firstName);

                            if (File.Exists(toFile))
                            {
                                CProgram.sLogLine("Destination already exists (skip)!");
                            }
                            else
                            {
                                string name, toFilePath;

                                // Copy the files with the same name.
                                foreach (string s in AFileList)
                                {
                                    name = Path.GetFileName(s);
                                    toFilePath = System.IO.Path.Combine(toPath, name);
                                    System.IO.File.Copy(s, toFilePath, true);
                                }

                                if (File.Exists(toFile))
                                {
                                    mStatusLine("Importing " + storeName + "\\" + firstName + "...");

                                    try
                                    {
                                        bOk = true;
                                        DateTime time = DateTime.Now;
                                        string manualName = CRecordMit.sMakeManualName(DImportConverter.TZAeraScp, DImportMethod.Manual, AFileList[0]);

                                        rec = mImportScpFiles(toFile, 0, receiveUTC, timeZoneOffset, storeName, utcTimeString, DImportMethod.Manual,
                                            manualName, DImportConverter.TZAeraScp, false, CLicKeyDev.sbDeviceIsProgrammer());
                                        if (rec == null)
                                        {
                                            bOk = false;
                                            bError = true;
                                        }
                                        else
                                        {
                                            bOk = true;
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        CProgram.sLogException("Failed to Import Scp Files " + ANewName, e);
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        CProgram.sLogException("Failed to Import SCP File", e);
                        bError = true;
                    }
                }

                if (bOk)
                {
                    TimeSpan dT = DateTime.Now - startTime;
                    CProgram.sLogLine("Import " + ANewName + " finished in " + dT.TotalSeconds.ToString("0.00") + " Sec.");
                    mIncreaseTotalCount();
                }
                else if (bError)
                {
                    mIncreaseErrorCount();
                }
                else
                {
                    mIncreaseSkippedCount();
                }
            }
        }

        private CRecordMit mImportScpFiles(String AFilePath, UInt32 ASetInvertChannels, DateTime AReceivedUTC, Int16 ADefaultTimeZoneOffset, string AStoreName,
                        string AUtcStoreString, DImportMethod AImportMethod, string AManualName, DImportConverter AImportConverter, bool AbApplyTimeZoneCor, bool AbLogResult)
        {
            bool bOk = false;
            CRecordMit rec = null;
            string fileName = Path.GetFileName(AFilePath);
            CProgram.sLogLine("Importing SCP file " + fileName + "...");

            try
            {
                DateTime startTime = DateTime.Now;
                double dTDataRead = 0.0, dTPlot = 0.0;
                DateTime sampleUTC = DateTime.UtcNow;
                string fileIndexName = fileName;
                UInt32 sampleIndex = 0;

                rec = CScpReader.sReadAllScp(Path.GetDirectoryName(AFilePath), ASetInvertChannels, _mDBaseServer, ADefaultTimeZoneOffset, ref sampleUTC, fileIndexName, sampleIndex, AbApplyTimeZoneCor, AbLogResult);

                TimeSpan ts = DateTime.Now - startTime;
                dTDataRead = ts.TotalSeconds;


                if (rec != null)
                {
                    rec.mReceivedUTC = AReceivedUTC;           // received time = read time
                    rec.mFileName = fileName;
                    rec.mStoredAt = AStoreName;
                    rec.mImportMethod = (UInt16)AImportMethod;            // Import method enum
                    rec.mbSetEventUTC(sampleUTC);

                    rec.mSignalLabels = "";

                    rec.mRecState = (UInt16)DRecState.Received;

                    if (rec.mDeviceModel == null || rec.mDeviceModel.Length == 0)
                    {
                        rec.mDeviceModel = "{SCP}";
                    }
                    if (rec.mRecLabel == null || rec.mRecLabel.Length == 0)
                    {
                        if (rec.mRecRemark != null && rec.mRecRemark.mbNotEmpty())
                        {
                            rec.mRecLabel = mGetFirstLine(rec.mRecRemark.mDecrypt(), 16);
                        }
                    }
                    rec.mCheckManualName(AManualName);

                    //mbCreateImages(AFilePath, rec, out dTPlot);

                    int newIndex;
                    mbAddSqlDbRow(out newIndex, rec, AFilePath);

                    bOk = rec.mIndex_KEY != 0;

                    if (bOk)
                    {
                        string method = "";
                        bool bEnabled = false;

                        switch (AImportConverter)
                        {
                            case DImportConverter.DV2Scp: method = _mDv2PaMethod; bEnabled = _mbDoPaDV2;  break;
                            case DImportConverter.TZAeraScp: method = _mTzPaMethod; bEnabled = _mbDoPaTZ; break;
                        }

                         mbDoPreAnalysis(bEnabled, method, rec, Path.GetDirectoryName(AFilePath));

                        mbFindStudy(rec);   // needs record number to update

                        if (DImportMethod.Automatic == AImportMethod)
                        {
                            mbSendReceivedMail(AFilePath, rec);
                        }
                    }
                    mbCreateImages(AFilePath, rec, out dTPlot);  // plot later, needs invert mask from device

                    mbStoreGrid(rec);


                    TimeSpan dT = DateTime.Now - startTime;

                    CProgram.sLogLine("[" + newIndex.ToString() + "]Import " + fileName + " in " + dT.TotalSeconds.ToString("0.00") + " Sec."
                        + " read " + rec.mGetNrSamples().ToString() + " samples(" + rec.mGetSamplesTotalTime().ToString("0.00")
                        + "s)"); // in " + dTDataRead.ToString("0.00") + " sec. Plot in " + dTPlot.ToString("0.00") + " sec.");
                }
            }
            catch (Exception e)
            {
                CProgram.sLogException("Failed import Scp file", e);

            }
            if (!bOk)
            {
                CProgram.sLogLine("Import " + fileName + " failed!");
                rec = null;
            }
            return rec;

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBoxSignal_Validated(object sender, EventArgs e)
        {
            //            mUpdateStrips();
        }

        private void comboBoxSignal_SelectionChangeCommitted(object sender, EventArgs e)
        {
            //            mUpdateStrips();
        }

        private void comboBoxSignal_TextUpdate(object sender, EventArgs e)
        {
            //            mUpdateStrips();
        }

        private void comboBoxSignal_TextChanged(object sender, EventArgs e)
        {
            CProgram.sSetBusy(true);
            mUpdateStrips();
            CProgram.sSetBusy(false);
        }

        public void mSetNewState(DRecState AState)
        {
            int index = 0;
            string oldState = "";

            if (mbGetCursorIndexState(out index, out oldState))
            {
                CRecordMit rec = new CRecordMit(_mDBaseServer);
                string newState = CRecordMit.sGetStateUserString((UInt16)AState);

                if (rec != null)
                {
                    CSqlCmd cmd = null;

                    rec.mIndex_KEY = (UInt32)index;
                    rec.mRecState = (UInt16)AState;

                    try
                    {
                        if (mbCreateDBaseConnection())
                        {
                            cmd = rec.mCreateSqlCmd(_mDBaseServer);
                            UInt64 mask = CRecordMit.sGetMask((UInt16)DRecordMitVars.RecState_IX);

                            if (cmd == null)
                            {
                                CProgram.sLogLine("Failed to init cmd");
                            }
                            else if (false == cmd.mbPrepareUpdateCmd(true, mask, true, true, 0))
                            {
                                CProgram.sLogLine("Failed to prepare update SQL row in table " + rec.mGetDbTableName());
                            }
                            else if (cmd.mbExecuteUpdateCmd())
                            {
                                DataGridViewRow row = dataGridView.CurrentRow;
                                if (row != null)
                                {
                                    row.Cells[(int)DDatagridColumn.State].Value = newState;
                                }
                                CProgram.sLogLine("Successfull updated  " + index.ToString() + "  from " + oldState + " to " + newState);
                            }
                            else
                            {
                                CProgram.sLogLine("Failed to update SQL row[" + index.ToString() + "] in table " + rec.mGetDbTableName());
                            }
                            cmd.mCloseCmd();
                        }
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException("Failed add Row", ex);
                    }
                    finally
                    {
                        if (cmd != null)
                        {
                            cmd.mCloseCmd();
                        }
                    }

                }
            }
            else
            {
                CProgram.sLogLine("Select row in table first.");
            }
        }

        private void buttonNoise_Click(object sender, EventArgs e)
        {

            mSetNewState(DRecState.Noise);
        }

        private void buttonLeadOff_Click(object sender, EventArgs e)
        {
            mSetNewState(DRecState.LeadOff);
        }

        private void buttonNormal_Click(object sender, EventArgs e)
        {
            mSetNewState(DRecState.Excluded);
        }

        private void buttonBaseLine_Click(object sender, EventArgs e)
        {
            mSetNewState(DRecState.BaseLine);
        }

        private void buttonTriaged_Click(object sender, EventArgs e)
        {
            mSetNewState(DRecState.Triaged);
        }

        private void buttonRead_Click(object sender, EventArgs e)
        {
            mSetNewState(DRecState.Received);
        }

        private void toolStripLast_Click(object sender, EventArgs e)
        {
            int n = dataGridView.Rows.Count;
            if (n > 0)
            {
                dataGridView.CurrentCell = dataGridView.Rows[n - 1].Cells[0];
            }
        }

        private CRecordMit mImportTZeFiles(CTZeReader ATZe, String AFilePath, UInt32 ASetInvertChannels, DateTime AReceivedUTC, Int16 ADefaultTimeOffset,
            string AStoreName, string AUtcStoreString, DImportMethod AImportMethod, string AManualName, bool AbApplyTimeZoneCor, bool AbLogResult)
        {
            bool bOk = false;
            CRecordMit rec = null;
            string fileName = Path.GetFileName(AFilePath);


            try
            {
                DateTime startTime = DateTime.Now;
                DateTime sampleUTC = DateTime.MinValue;
                DateTime eventUTC, baseUTC;
                double lengthSec;
                double dTDataRead = 0.0, dTPlot = 0.0;
                string eventMethod = "Automatic";
                string recordPath = Path.GetDirectoryName(AFilePath);

                CProgram.sLogLine("Importing TZe file " + fileName + "...("
                    + ATZe._mEventFileIndexName + "[" + ATZe._mEventSampleIndex.ToString() + "]");

                mShowTzHrFilter(); //reset filters
                rec = CScpReader.sReadAllScp(recordPath, ASetInvertChannels, _mDBaseServer, ADefaultTimeOffset,
                    ref sampleUTC, ATZe._mEventFileIndexName, ATZe._mEventSampleIndex, AbApplyTimeZoneCor, AbLogResult);   // read all scp files into one record

                bOk = rec != null;
                if (bOk)
                {
                    dTDataRead = (DateTime.Now - startTime).TotalSeconds;

                    // From TZe in dBase 
                    rec.mReceivedUTC = AReceivedUTC;           // received time = read time
                    rec.mFileName = fileName;
                    rec.mStoredAt = AStoreName;
                    rec.mImportConverter = (UInt16)DImportConverter.TZAeraScp;         // import converter enum
                    rec.mImportMethod = (UInt16)AImportMethod;            // Import method enum
                    rec.mTimeZoneOffsetMin = ATZe._mTimeOffsetMin;

                    if (ATZe._mDeviceID != null && ATZe._mDeviceID.Length > 0) rec.mDeviceID = ATZe._mDeviceID;
                    if (ATZe._mPatientID != null && ATZe._mPatientID.Length > 0) rec.mPatientID.mbEncrypt(ATZe._mPatientID);

                    if (ATZe._mTag == (UInt16)DTzEventTags.PATIENT_TAG)
                    {
                        eventMethod = "Manual";
                    }
                    rec.mEventTypeString = eventMethod;
                    rec.mRecRemark.mbEncrypt(ATZe._mRemLabel);


                    baseUTC = rec.mBaseUTC;
                    lengthSec = rec.mGetSamplesTotalTime();
                    eventUTC = ATZe.mGetEventUtc();

                    string sampleLine = "";

                    if (sampleUTC != DateTime.MinValue)
                    {
                        double diffSec = (eventUTC - sampleUTC).TotalSeconds;

                        sampleLine = " diffSample= " + diffSec.ToString("0.000");

                        eventUTC = sampleUTC;
                    }
                    double eventSec = eventUTC == DateTime.MinValue ? 0 : (eventUTC - baseUTC).TotalSeconds;

                    string line = "TZe " + fileName + " event ";


                    if (eventSec < 0)
                    {
                        line += (-eventSec).ToString("0.000") + " before ";

                    }
                    else if (eventSec <= lengthSec)
                    {
                        line += (eventSec).ToString("0.000") + " in ";

                    }
                    else
                    {
                        line += (eventSec - lengthSec).ToString("0.000") + " after ";
                    }
                    line += " stripUTC " + CProgram.sDateTimeToYMDHMS(baseUTC) + "Z" + CProgram.sTimeZoneOffsetStringLong((Int16)rec.mTimeZoneOffsetMin) + " " + lengthSec.ToString("0.0");
                    line += sampleLine;
                    CProgram.sLogLine(line);

                    rec.mbSetEventUTC(eventUTC);            // event time relative to base

                    //rec.mPatientBirthDate;
                    //rec.mPhysicianName;
                    //rec.mPatientComment;
                    //rec.mEventTypeString;
                    //rec.mTransmitTimeUTC;
                    //rec.mTransmitTimeLocal;
                    // rec.mPatientBirthYear;
                    rec.mRecState = (UInt16)DRecState.Received;

                    if (rec.mDeviceModel == null || rec.mDeviceModel.Length == 0)
                    {
                        rec.mDeviceModel = "{SCP}";
                    }
                    if (rec.mRecLabel == null || rec.mRecLabel.Length == 0)
                    {
                        if (rec.mRecRemark != null && rec.mRecRemark.mbNotEmpty())
                        {
                            //                            rec.mRecLabel = mGetFirstLine(rec.mRecRemark.mDecrypt(), 16);
                        }
                    }
                    rec.mCheckManualName(AManualName);

                    //mbCreateImages(AFilePath, rec, out dTPlot);

                    // if TZ the snr must be changed first (not nice to do it double but need new DEviceID before insert in DB (PK field) 
                    /*                   try
                                       {
                                           string altDeviceStr = rec.mGetAlternateSerialNr();
                                           if (altDeviceStr != null && altDeviceStr.Length > 0)
                                           {
                                               CDeviceInfo device = new CDeviceInfo( CDvtmsData.sGetDBaseConnection());

                                               if (device != null)
                                               {
                                                   int nSerial = device.mFindLoadSerial(0, rec.mDeviceID, 0); // loads device

                                                   if (nSerial == 0)
                                                   {
                                                       nSerial = device.mFindLoadSerial(0, altDeviceStr, 0);// needed for tz
                                                       if (nSerial > 0)
                                                       {
                                                           rec.mRecRemark.mbEncrypt(rec.mRecRemark.mDecrypt() + "\r\n" + rec.mDeviceID + "=" + altDeviceStr);
                                                           rec.mDeviceID = altDeviceStr;
                                                       }
                                                   }
                                               }
                                           }

                                       }
                                       catch (Exception ex)
                                       {
                                           CProgram.sLogException("Failed find alternate snr at import TZ file", e);
                                       }
                    */
                    int newIndex;
                    DateTime startDB = DateTime.Now;
                    bOk = mbAddSqlDbRow(out newIndex, rec, AFilePath);
                    double dbSec = (DateTime.Now - startDB).TotalSeconds;

                    DateTime startStudy = DateTime.Now;
                    double studySec = 0.0;
                    if (bOk)
                    {
                        mbDoPreAnalysis(_mbDoPaTZ, _mTzPaMethod, rec, recordPath);

                        mbFindStudy(rec);   // neads record number to update
                        studySec = (DateTime.Now - startStudy).TotalSeconds;
                        if (DImportMethod.Automatic == AImportMethod)
                        {
                            mbSendReceivedMail(AFilePath, rec);
                        }
                    }
                    mbCreateImages(AFilePath, rec, out dTPlot);  // plot later, needs invert mask from device
                    mbStoreGrid(rec);
                    TimeSpan dT = DateTime.Now - startTime;

                    /*                    CProgram.sLogLine("[" + newIndex.ToString() + "]Import " + fileName + " in " + dT.TotalSeconds.ToString("0.00") + " Sec."
                                            + " read " + rec.mGetNrSamples().ToString() + " samples(" + rec.mGetSamplesTotalTime().ToString("0.00")
                                            + "s) read in " + dTDataRead.ToString("0.00") + " sec. Plot in " + dTPlot.ToString("0.00") + " sec.");
                      */
                    CProgram.sLogLine("[" + newIndex.ToString() + "]Import " + fileName + " in " + dT.TotalSeconds.ToString("0.00") + " Sec"
                      + "(" + rec.mNrSignals.ToString() + "x" + rec.mGetNrSamples().ToString() + " " + rec.mGetSamplesTotalTime().ToString("0")
                      + "s) ld=" + dTDataRead.ToString("0.00") + " pl=" + dTPlot.ToString("0.00")
                      + " db=" + dbSec.ToString("0.00") + " st=" + studySec.ToString("0.00") + " S" + rec.mStudy_IX.ToString());

                }
            }
            catch (Exception e)
            {
                CProgram.sLogException("Failed import TZ file", e);

            }
            if (!bOk)
            {
                CProgram.sLogLine("Import " + fileName + " failed!");
                rec = null;
            }
            return rec;

        }

        private bool mbImportTZeFile(out bool AbError, CTZeReader ATZe, String AFilePath, UInt32 ASetInvertChannels, DImportMethod AImportMethod)
        {
            bool bOk = false;
            bool bError = false;
            bool bDataRead = false;
            bool bDataCvs = false;
            double dTDataRed = 0.0F;
            double dTPlot = 0.0F;
            CRecordMit rec = null; // MIT record file
            DateTime startTime = DateTime.Now;
            DateTime eventBaseDT = DateTime.MinValue;
            String toPath;

            string fileNameExt = Path.GetFileName(AFilePath);
            mProcessSetInfo("Tze>>" + fileNameExt);
            DateTime receiveUTC = DateTime.UtcNow;
            Int16 timeZoneOffset = CProgram.sGetLocalTimeZoneOffsetMin();

            if (mbChangeToDateDir(out toPath, _mRecordingDir, receiveUTC) == false)
            {
                bError = true;
            }
            else
            {
                DateTime utcEvent = ATZe.mGetEventUtc();
                DateTime dtEvent = utcEvent.AddMinutes(ATZe._mTimeOffsetMin);

                string eventTime = mCreateDtZoneFileName(dtEvent, ATZe._mTimeOffsetMin);

                //File.GetLastWriteTimeUtc(AFilePath);
                //                string utcTime = utcEvent.ToString("yyyyMMddhhmmss");   // => local time
                string fileName = Path.GetFileNameWithoutExtension(AFilePath);
                string startName = "TZe";
                string storeName = /*mCreateUtcName(receiveUTC, TimeZoneInfo.Local) + "_" +*/ startName + "" + eventTime + "_" + fileName;
                string utcTimeString = mCreateUtcName(receiveUTC, TimeZoneInfo.Local);

                // TZeD20190326T123305ZP0100_I200010_016228_001_20190326111213 -> 59 char + 4 x '_' expanded = 63 char < 64
                // 12345678901234567890123456789012345678901234567890123456789
                storeName = CSqlDBaseConnection.sSqlLimitSqlString(storeName, CRecordMit.cStoredAtMaxLen);

                // use original  file name for directory to recognize existing files
                toPath = Path.Combine(toPath, storeName);

                try
                {
                    if (mbCreateDirectory(toPath))
                    {
                        String toFile = Path.Combine(toPath, fileNameExt);

                        if (File.Exists(toFile))
                        {
                            CProgram.sLogLine("Destination already exists (skip)!");
                        }
                        else
                        {
                            string fromPath = Path.GetDirectoryName(AFilePath);
                            string[] files = System.IO.Directory.GetFiles(fromPath);
                            string name, toFilePath;
                            int nCopyFiles = 0;

                            // Copy the files with the same name.
                            foreach (string s in files)                     // copy tze
                            {
                                name = System.IO.Path.GetFileName(s);
                                if (name.Contains(fileName))
                                {
                                    toFilePath = System.IO.Path.Combine(toPath, name);
                                    ++nCopyFiles;
                                    System.IO.File.Copy(s, toFilePath, true);
                                }
                            }
                            // copy scp files
                            for (int i = 0; i < ATZe._mNrFiles; ++i)
                            {
                                UInt32 fileNr = (UInt32)(ATZe._mFirstFileNr + i);
                                string scpSearchFile = ATZe._mDeviceID + "_" + fileNr.ToString("D6");
                                string scpFoundFile = "";
                                string fromFilePath = "";
                                toFilePath = "";

                                if (fileNr == ATZe._mEventFileIndex && ATZe._mEventFileIndex != 0)
                                {
                                    ATZe._mEventFileIndexName = scpSearchFile;  // in this file create sampleUTC
                                }
                                // SV20171115 copy multiple TZ sesions
                                foreach (string s in files)                     // copy scp
                                {
                                    name = System.IO.Path.GetFileName(s);
                                    if (name.EndsWith(".scp") && name.StartsWith(scpSearchFile))
                                    {
                                        if (scpFoundFile.Length == 0 || name.CompareTo(scpFoundFile) > 0)
                                        {
                                            scpFoundFile = name; // find last file <snr>_<file nr>_YYYYMMDDHHmmss.scp
                                        }
                                    }
                                }
                                if (scpFoundFile.Length > 0)
                                {
                                    try
                                    {
                                        fromFilePath = System.IO.Path.Combine(fromPath, scpFoundFile);
                                        toFilePath = System.IO.Path.Combine(toPath, scpFoundFile);
                                        ++nCopyFiles;
                                        System.IO.File.Copy(fromFilePath, toFilePath, true);
                                    }
                                    catch (Exception e)
                                    {
                                        CProgram.sLogException("Failed to copy TZe scp file: " + toFilePath, e);
                                        bError = true;
                                    }
                                }
                                else
                                {
                                    CProgram.sLogError("!Missing scp file " + scpSearchFile);
                                }
                                /*                                string scpFile = ATZe._mDeviceID + "_" + (ATZe._mFirstFileNr + i).ToString("D6") + ".scp";
                                                                string fromScp = Path.Combine(fromPath, scpFile);
                                                                string toScp = Path.Combine(toPath, scpFile);

                                                                !!sv
                                                                try
                                                                {
                                                                    File.Copy(fromScp, toScp, true);
                                                                }
                                                                catch (Exception e)
                                                                {
                                                                    CProgram.sLogException("Failed to copy TZe scp file: " + scpFile, e);
                                                                    bError = true;
                                                                }
                                */
                            }
                            double copySec = (DateTime.Now - startTime).TotalSeconds;
                            if (copySec >= 3.0)
                            {
                                CProgram.sLogLine("Copy " + nCopyFiles.ToString() + " files " + fileNameExt + " in " + copySec.ToString("0.00") + " Sec.");
                            }

                            if (File.Exists(toFile))
                            {
                                mStatusLine("Importing " + storeName + "\\" + fileNameExt + "...");

                                try
                                {
                                    bOk = true;

                                    string manualName = CRecordMit.sMakeManualName(DImportConverter.TZAeraScp, AImportMethod, AFilePath);

                                    rec = mImportTZeFiles(ATZe, toFile, ASetInvertChannels, receiveUTC, timeZoneOffset, storeName,
                                        utcTimeString, AImportMethod, manualName, false, CLicKeyDev.sbDeviceIsProgrammer());

                                    if (rec == null)
                                    {
                                        bOk = false;
                                        bError = true;
                                    }
                                    else
                                    {
                                        bOk = true;
                                    }
                                }
                                catch (Exception e)
                                {
                                    CProgram.sLogException("Failed to Import TZe File " + AFilePath, e);
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    CProgram.sLogException("Failed to TZe Header File " + AFilePath, e);
                    bError = true;
                }
            }

            if (bOk)
            {
                TimeSpan dT = DateTime.Now - startTime;
                CProgram.sLogLine("Import " + fileNameExt + " finished in " + dT.TotalSeconds.ToString("0.00") + " Sec.");
                mIncreaseTotalCount();
            }
            else if (bError)
            {
                mIncreaseErrorCount();
            }
            else
            {
                mIncreaseSkippedCount();
            }
            AbError = bError;
            return bOk;
        }

        private void mWriteDoneFile(string ALogText, string ASerialNr, string AFileExt, UInt32 AActionFileID, string AActionFileName)
        {
            string result = "failed write!";

            string deviceDir = null;
            string storeSnr = CDvtmsData.sGetTzStoreSnrName(ASerialNr);
            string remSnr = storeSnr != ASerialNr ? "->" + storeSnr : "";

            if (_mTzFromDir != null && _mTzFromDir.Length > 0 && CDvtmsData.sbGetDeviceUnitDir(out deviceDir, DDeviceType.TZ, ASerialNr, false))
            {
                string doneName = Path.ChangeExtension(storeSnr, AFileExt);
                try
                {
                    string text = AActionFileID.ToString() + "\r\n" + AActionFileName + "\r\n";
                    File.WriteAllText(Path.Combine(deviceDir, doneName), text);
                    result = "written";
                }
                catch (Exception ex)
                {
                    CProgram.sLogException(" Failed write " + ASerialNr + remSnr + ": "
                           + doneName + " about " + ALogText + " " + AActionFileID.ToString(), ex);
                }

            }
            CProgram.sLogLine("TZe status " + AActionFileName + " " + ALogText + " from action "
                + AActionFileID.ToString() + result);
        }

        CTzSkipLastFileNr mGetTzSkipLastFileNr(string ADeviceID)
        {
            CTzSkipLastFileNr foundDevice = null;

            try
            {
                if (_mTzSkipList == null)
                {
                    _mTzSkipList = new List<CTzSkipLastFileNr>();
                }
                if (_mTzSkipList != null && ADeviceID != null && ADeviceID.Length > 0)
                {
                    foreach (CTzSkipLastFileNr skipFile in _mTzSkipList)
                    {
                        if (skipFile._mDeviceID == ADeviceID)
                        {
                            foundDevice = skipFile;
                            break;
                        }
                    }
                    if (foundDevice == null)
                    {
                        foundDevice = new CTzSkipLastFileNr();
                        if (foundDevice != null)
                        {
                            foundDevice._mDeviceID = ADeviceID; // new device
                            _mTzSkipList.Add(foundDevice);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Find TzSkipLastFileNr Failed " + ADeviceID, ex);
            }
            if (_mTzSkipList == null)
            {
                _mTzSkipList = new List<CTzSkipLastFileNr>();
            }
            return foundDevice;
        }

        public bool mImportTZeFile(string AFilePath, DImportMethod AImportMethod, bool AbCheckSkip, bool AbDoActionQueue)
        {
            bool bOk = false;
            bool bError = false;
            int pos = AFilePath.IndexOf('$');
            bool bOldFilePresent = pos >= 0;
            string oldFilePath = "";

            if (bOldFilePresent)
            {
                // import from study, get the scan file name after the '$'
                oldFilePath = AFilePath.Substring(pos + 1);
                AFilePath = AFilePath.Substring(0, pos);
            }

            try
            {
                CTZeReader tze = new CTZeReader();

                if (tze != null)
                {
                    string file = Path.GetFileName(AFilePath);
                    string note = "";

                    if (tze.mbLoadFile(AFilePath))
                    {
                        string s = tze._mRemData;

                        note = ":<" + tze._mTag.ToString() + "> " + tze._mFirstFileNr.ToString() + "n" + tze._mNrFiles.ToString();

                        if (tze._mRemLabel != null && tze._mRemLabel.Length > 0)
                        {
                            note = ", note: " + tze._mRemLabel;
                        }
                        //CProgram.sLogLine("data: " + tze._mRemData);

                        if (tze._mTag == (UInt16)DTzEventTags.SCP_RETRANSMIT_TAG)       // must not occure because
                        {
                            uint actionFileID = tze._mTagData;  // try during Auto collect
                            // retransmit of SCP file must not generate an event
                            CProgram.sLogLine("TZe status " + file + " Retransmited SCP " + actionFileID);
                        }
                        else if (tze._mNrFiles > 0)
                        {
                            CTzSkipLastFileNr skipDevice = null;
                            bool bSkip = false;

                            if (AbCheckSkip && _mTzSkipCmpMin >= 0 && _mTzSkipCmpMax >= 0)
                            {
                                skipDevice = mGetTzSkipLastFileNr(tze._mDeviceID); // find device last scp file number

                                if (tze._mTag != (UInt16)DTzEventTags.PATIENT_TAG)
                                {
                                    if (skipDevice != null)
                                    {
                                        int min = (int)skipDevice._mLastFileNr - _mTzSkipCmpMin;
                                        int max = (int)skipDevice._mLastFileNr + _mTzSkipCmpMax;
                                        if (tze._mFirstFileNr >= min && tze._mFirstFileNr <= max)
                                        {
                                            bSkip = true;
                                        }
                                    }
                                }
                            }
                            if (bSkip)
                            {
                                bOk = false;
                                CProgram.sLogLine("Skip simular " + (tze._mFirstFileNr - skipDevice._mLastFileNr).ToString() + " TZ Import " + file + note);
                            }
                            else
                            {
                                CProgram.sLogLine("TZe Import " + file + note);
                                bOk = mbImportTZeFile(out bError, tze, AFilePath, 0, AImportMethod);
                                if (skipDevice != null)
                                {
                                    skipDevice._mLastFileNr = tze._mFirstFileNr;
                                }
                            }
                        }
                        else
                        {
                            if (s != null && s.Length > 0)
                            {
                                s = s.Trim();
                                int posLine = s.LastIndexOf('\n');
                                if (posLine < 0)
                                {
                                    posLine = s.LastIndexOf('\r');
                                }
                                if (posLine > 0 && posLine >= s.Length - 2)
                                {
                                    s = s.Substring(0, posLine - 2).Trim();
                                    posLine = s.LastIndexOf('\n'); // remove last empty line
                                    if (posLine < 0)
                                    {
                                        posLine = s.LastIndexOf('\r');
                                    }
                                }
                                if (posLine > 1)
                                {
                                    note += "<" + s.Substring(posLine + 1).Trim();  // last line
                                }
                                else
                                {
                                    note += ">" + s.Trim();  // only 1 line
                                }
                            }

                            if (tze._mTag == (UInt16)DTzEventTags.ACTIONS_SUCCESS_TAG)
                            {
                                uint actionFileID = tze._mTagData;
                                // remove action with file number
                                CProgram.sLogLine("TZe status " + file + " Action done " + actionFileID);
                                if (AbDoActionQueue)
                                {
                                    mMoveTzDeviceFilesToServer(tze._mDeviceID, actionFileID);
                                    mWriteDoneFile("Action done", tze._mDeviceID, ".ActionDone", actionFileID, file);
                                }
                            }
                            else if (tze._mTag == (UInt16)DTzEventTags.ACTIONS_FAILURE_TAG)
                            {
                                uint actionFileID = tze._mTagData;
                                // just log so that is easier seen in log file
                                CProgram.sLogLine("TZe status" + file + " Action Failed code " + actionFileID);
                            }
                            else if (tze._mTag == (UInt16)DTzEventTags.SMS_MSG_RECEIVED)
                            {
                                uint actionFileID = tze._mTagData;
                                // log responce
                                if (AbDoActionQueue)
                                {
                                    mWriteDoneFile("Message received", tze._mDeviceID, ".MsgReceived", actionFileID, file);
                                }
                            }
                            else if (tze._mTag == (UInt16)DTzEventTags.SETTINGS_SUCCESS_TAG)
                            {
                                uint actionFileID = tze._mTagData;
                                // log responce
                                if (AbDoActionQueue)
                                {
                                    mWriteDoneFile("Setting success", tze._mDeviceID, ".SettingSet", actionFileID, file);
                                }
                            }
                            else
                            {
                                CProgram.sLogLine("TZe status " + file + note);
                            }
                        }
                    }
                    else
                    {
                        CProgram.sLogLine(file + " failed, note: " + note);
                        //                        CProgram.sLogLine("failed, data: " + tze._mRemData);
                        bError = true;
                    }
                }

            }
            catch (Exception ex)
            {
                CProgram.sLogException("Import TZe failed " + AFilePath, ex);
                bOk = false;
            }

            if (DImportMethod.Automatic == AImportMethod)
            {
                string renFile = "";
                string dtString = CProgram.sDateTimeToYMDHMS(DateTime.UtcNow);
                try
                {
                    renFile = Path.ChangeExtension(AFilePath, "tze_done" + dtString);

                    if (bError)
                    {
                        bOk = false;
                        renFile = Path.ChangeExtension(AFilePath, "tze_error" + dtString);
                    }
                    else if (false == bOk)
                    {
                        renFile = Path.ChangeExtension(AFilePath, "tze_skip" + dtString);
                    }

                    File.Move(AFilePath, renFile);

                    if (bOldFilePresent)
                    {
                        try
                        {
                            // delete the file from the scan directory
                            File.Delete(oldFilePath);
                        }
                        catch (Exception ex)
                        {
                            CProgram.sLogException("Import TZe failed delete file " + oldFilePath, ex);
                            bOk = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Import TZe rename to failed " + renFile, ex);
                    bOk = false;
                }

            }
            return bOk;
        }

        private CRecordMit mImportDV2File(String AFilePath, UInt32 ASetInvertChannels, DateTime AReceivedUTC, Int16 ADefaultTimeOffset, string AStoreName, DImportMethod AImportMethod,
                string AManualName, string ADefaultDeviceID, bool AbApplyTimeZoneCor, bool AbLogResult)
        {
            bool bOk = false;
            CRecordMit rec = null;
            string fileName = Path.GetFileName(AFilePath);

            CProgram.sLogLine("Importing DV2 file " + fileName + "...");

            try
            {
                DateTime startTime = DateTime.Now;
                double dTDataRead = 0.0, dTPlot = 0.0;
                string eventMethod = "Measure";

                // read scp files into one record
                rec = CScpReader.sReadOneScp(AFilePath, ASetInvertChannels, _mDBaseServer, ADefaultTimeOffset, DImportConverter.DV2Scp, AbApplyTimeZoneCor, AbLogResult);

                bOk = rec != null;
                if (bOk)
                {
                    // From TZa in dBase 
                    rec.mReceivedUTC = AReceivedUTC;           // received time = read time
                    rec.mFileName = fileName;
                    rec.mStoredAt = AStoreName;
                    //rec.mbSetEventUTC(ATZe.mGetEventUtc());            // event time relative to base
                    rec.mImportConverter = (UInt16)DImportConverter.DV2Scp;         // import converter enum
                    rec.mImportMethod = (UInt16)AImportMethod;            // Import method enum
                                                                          //rec.mTimeZoneOffsetMin = ATZe._mTimeOffsetMin;

                    //                    if (ATZe._mDeviceID != null && ATZe._mDeviceID.Length > 0) rec.mDeviceID = ATZe._mDeviceID;
                    //                    if (ATZe._mPatientID != null && ATZe._mPatientID.Length > 0) rec.mPatientID.mbEncrypt(ATZe._mPatientID);
                    rec.mEventTypeString = eventMethod;
                    //                    rec.mRecRemark.mbEncrypt(ATZe._mRemLabel);

                    //rec.mPatientBirthDate;
                    //rec.mPhysicianName;
                    //rec.mPatientComment;
                    //rec.mEventTypeString;
                    //rec.mTransmitTimeUTC;
                    //rec.mTransmitTimeLocal;
                    // rec.mPatientBirthYear;
                    rec.mRecState = (UInt16)DRecState.Received;

                    if (rec.mPatientID.mbIsEmpty())
                    {
                        int pos = fileName.IndexOf('_');
                        if (pos > 0)
                        {
                            string patID = fileName.Substring(0, pos);
                            rec.mPatientID.mbEncrypt(CProgram.sTrimString(patID));    // first part of filename is patient ID from DV2 system
                        }
                    }

                    if (rec.mDeviceID == null || rec.mDeviceID.Length == 0 || rec.mDeviceID.ToLower() == "unknown")
                    {
                        rec.mDeviceID = ADefaultDeviceID;
                    }

                    if (rec.mDeviceModel == null || rec.mDeviceModel.Length == 0)
                    {
                        rec.mDeviceModel = "{DV2SCP}";
                    }
                    if (rec.mRecLabel == null || rec.mRecLabel.Length == 0)
                    {
                        if (rec.mRecRemark != null && rec.mRecRemark.mbNotEmpty())
                        {
                            //                            rec.mRecLabel = mGetFirstLine(rec.mRecRemark.mDecrypt(), 16);
                        }
                    }
                    rec.mCheckManualName(AManualName);

                    // mbCreateImages(AFilePath, rec, out dTPlot);

                    int newIndex;
                    bOk = mbAddSqlDbRow(out newIndex, rec, AFilePath);

                    if (bOk)
                    {
                        mbDoPreAnalysis(_mbDoPaDV2, _mDv2PaMethod, rec, Path.GetDirectoryName(AFilePath));

                        mbFindStudy(rec);   // neads record number to update

                        if (DImportMethod.Automatic == AImportMethod)
                        {
                            mbSendReceivedMail(AFilePath, rec);
                        }
                    }
                    mbCreateImages(AFilePath, rec, out dTPlot);  // plot later, needs invert mask from device
                    mbStoreGrid(rec);
                    TimeSpan dT = DateTime.Now - startTime;

                    CProgram.sLogLine("[" + newIndex.ToString() + "]Import " + fileName + " in " + dT.TotalSeconds.ToString("0.00") + " Sec."
                        + " read " + rec.mGetNrSamples().ToString() + " samples(" + rec.mGetSamplesTotalTime().ToString("0.00")
                        + "s)");// in " + dTDataRead.ToString("0.00") + " sec. Plot in " + dTPlot.ToString("0.00") + " sec.");
                }
            }
            catch (Exception e)
            {
                CProgram.sLogException("Failed import Intricon file " + AFilePath, e);

            }
            if (!bOk)
            {
                CProgram.sLogLine("Import " + fileName + " failed!");
                rec = null;
            }
            return rec;

        }

        public bool mImportDV2File(string AFilePath, UInt32 ASetInvertChannels, DImportMethod AImportMethod, Int16 ADefaultTimeZoneMin)
        {
            bool bOk = false;
            bool bError = false;
            bool bDataRead = false;
            bool bDataCvs = false;
            double dTDataRed = 0.0F;
            double dTPlot = 0.0F;
            CRecordMit rec = null; // MIT record file
            DateTime startTime = DateTime.Now;
            String toPath;
            bool bMoveFiles = AImportMethod == DImportMethod.Automatic;

            string fileNameExt = Path.GetFileName(AFilePath);
            mProcessSetInfo("DV2>>" + fileNameExt);
            DateTime receiveUTC = DateTime.UtcNow;


            if (mbChangeToDateDir(out toPath, _mRecordingDir, receiveUTC) == false)
            {
                bError = true;
            }
            else
            {
                string fileName = Path.GetFileNameWithoutExtension(AFilePath);
                string storeName = /*mCreateUtcName(receiveUTC, TimeZoneInfo.Local) + "_" +*/  "DV2_" + fileName;

                storeName = CSqlDBaseConnection.sSqlLimitSqlString(storeName, CRecordMit.cStoredAtMaxLen);

                // use original  file name for directory to recognize existing files
                toPath = Path.Combine(toPath, storeName);

                try
                {
                    if (mbCreateDirectory(toPath))
                    {
                        String toFile = Path.Combine(toPath, fileNameExt);

                        //                        if (File.Exists(toFile))
                        if (mbCheckExistDestination(AFilePath, toFile))
                        {
                            #region scpExists
                            bool bMoved = false;
                            int recNr = 0;

                            int n = dataGridView.Rows.Count;

                            while (n > 0)
                            {
                                --n;
                                DataGridViewRow dgv = dataGridView.Rows[n];
                                if (dgv != null && dgv.Cells.Count >= (int)DDatagridColumn.NR_Columns)
                                {
                                    string gridFileName = dgv.Cells[(int)DDatagridColumn.RecNr].Value.ToString();
                                    if (gridFileName == fileNameExt)
                                    {
                                        // file in grid found
                                        string recStr = dgv.Cells[0].Value.ToString();

                                        if (int.TryParse(recStr, out recNr))
                                        {
                                            if (bMoveFiles)
                                            {
                                                bMoved = true;
                                                string newFileName = Path.ChangeExtension(AFilePath, Path.GetExtension(AFilePath) +
                                                    "_skip" + CProgram.sDateTimeToYMDHMS(startTime) + "R" + recNr.ToString());
                                                File.Move(AFilePath, newFileName);
                                            }
                                        }
                                        break;
                                    }
                                }
                            }
                            if (bMoved)
                            {
                                CProgram.sLogLine("Destination already exists (skip + renamed)! " + fileNameExt + " R#= " + recNr);
                            }
                            else
                            {
                                CProgram.sLogLine("Destination already exists (skip)! " + fileNameExt + " R#= " + recNr);
                            }
                            #endregion
                        }
                        else
                        {
                            mStatusLine("Copy file to " + storeName + "\\" + fileNameExt + "...");

                            // copy all files starting with fileName 

                            string fromPath = Path.GetDirectoryName(AFilePath);
                            string parentID = Path.GetFileName(fromPath);   // use parentID as deviceID
                            mbCopyFiles(fromPath, fileName, toPath);

                            if (File.Exists(toFile))
                            {

                                try
                                {
                                    DateTime time = DateTime.Now;
                                    string manualName = CRecordMit.sMakeManualName(DImportConverter.DV2Scp, AImportMethod, AFilePath);

                                    rec = mImportDV2File(toFile, ASetInvertChannels, receiveUTC, ADefaultTimeZoneMin, storeName, AImportMethod, manualName, parentID,
                                        true, CLicKeyDev.sbDeviceIsProgrammer());
                                    if (rec == null)
                                    {
                                        bOk = false;
                                        bError = true;
                                    }
                                    else if (rec.mIndex_KEY == 0)
                                    {
                                        bError = true;
                                        CProgram.sLogLine("Failed to add record, rename scp to try again: " + AFilePath);
                                        string badFilePath = Path.ChangeExtension(toFile, "R0_scp");

                                        try
                                        {
                                            File.Move(toFile, badFilePath); // rename current zip to try again
                                        }
                                        catch (Exception)
                                        {
                                        }
                                    }
                                    else
                                    {
                                        bOk = true;
                                    }

                                    if (bOk && bMoveFiles)
                                    {
                                        mStatusLine("Delete original " + AFilePath);
                                        try
                                        {
                                            // delet all files starting with 
                                            mbDeleteFiles(fromPath, fileName);
                                        }
                                        catch (Exception e)
                                        {
                                            CProgram.sLogException("Failed delete DV2Scp file " + fileNameExt, e);
                                        }
                                    }
                                }
                                catch (Exception e)
                                {
                                    CProgram.sLogException("Failed to import DV2Scp file " + fileNameExt, e);
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    CProgram.sLogException("Failed to import DV2Scp file " + fileNameExt, e);
                    bError = true;
                }
            }

            if (bOk)
            {
                TimeSpan dT = DateTime.Now - startTime;
                CProgram.sLogLine("Convert " + fileNameExt + " finished in " + dT.TotalSeconds.ToString("0.00") + " Sec.");
                mIncreaseTotalCount();
            }
            else if (bError)
            {
                if (_mDisableErrorZipHour > 0.01)
                {
                    mCheckDisableImportFile(AFilePath);
                }

                mIncreaseErrorCount();
            }
            else
            {
                mIncreaseSkippedCount();
            }
            return bOk;
        }
        public bool mbCopyFiles(string AFromPath, string AStartFileName, string AToPath)
        {
            bool bOk = false;

            try
            {
                string[] files = System.IO.Directory.GetFiles(AFromPath);
                string name, toFilePath;

                if (files != null && files.Length > 0)
                {
                    // Copy the files with the same name.
                    foreach (string s in files)                     // copy tze
                    {
                        name = System.IO.Path.GetFileName(s);
                        if (name.StartsWith(AStartFileName))
                        {
                            toFilePath = System.IO.Path.Combine(AToPath, name);
                            System.IO.File.Copy(s, toFilePath, true);
                        }
                    }
                    bOk = true;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed to copy files " + AFromPath + " \\ " + AStartFileName + "*.* to " + AToPath, ex);
                bOk = false;
            }
            return bOk;
        }

        public bool mbCopyOneFile(string AFromFilePath, string AToFilePath, bool AbOverwrite)
        {
            bool bOk = false;

            try
            {
                File.Copy(AFromFilePath, AToFilePath, AbOverwrite);
                bOk = true;
            }
            catch (Exception ex)
            {
                if (AbOverwrite)
                {
                    CProgram.sLogException("Failed to copy file " + AFromFilePath + " to " + AToFilePath, ex);
                }
                bOk = false;
            }
            return bOk;
        }




        public bool mbDeleteFiles(string AFromPath, string AStartFileName)
        {
            bool bOk = false;

            try
            {
                string[] files = System.IO.Directory.GetFiles(AFromPath);
                string name;

                if (files != null && files.Length > 0)
                {
                    // Copy the files with the same name.
                    foreach (string s in files)                     // copy tze
                    {
                        name = System.IO.Path.GetFileName(s);
                        if (name.StartsWith(AStartFileName))
                        {
                            try
                            {
                                File.Delete(s);
                            }
                            catch (Exception)
                            {
                                // do not care just continue
                            }
                        }
                    }
                    bOk = true;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed to delete files " + AFromPath + " \\ " + AStartFileName + "*.*", ex);
                bOk = false;
            }
            return bOk;
        }

        private void toolStripButtonTZe_Click(object sender, EventArgs e)
        {
            mButtonLoadTZe();
        }
        private void mButtonLoadTZe()
        {
            if (!mbCheckDaysLeft())
            {
                return;
            }
            if (_mbRunningConversion || _mbRunLoop)
            {
                CProgram.sLogLine("File conversion already running, press stop first!");
            }
            else
            {
                mSetRun(false);

                //openHeaFileDialog.RestoreDirectory = true;
                openTZeFileDialog.InitialDirectory = _mTzFromDir;
                if (false == mbIsRunNamePresent())
                {
                    openTZeFileDialog.FileName = _mTzFromDir;
                }
                else
                {
                    openTZeFileDialog.FileName = Path.Combine(_mTzFromDir, mGetRunFileName());
                }

                CTZeReader tze = new CTZeReader();

                if (tze != null && openTZeFileDialog.ShowDialog() == DialogResult.OK)
                {
                    _mbRunningConversion = true;

                    CProgram.sSetBusy(true);
                    foreach (String file in openTZeFileDialog.FileNames)
                    {
                        mImportTZeFile(file, DImportMethod.Manual, false, false);
                    }

                    CProgram.sSetBusy(false);
                    _mbRunningConversion = false;
                }

            }
        }

        private void toolStripAutoZip_Click(object sender, EventArgs e)
        {
            _mAutoRunSirZip = _mAutoRunSirZip == (UInt16)DAutoRun.Off ? (UInt16)DAutoRun.On : (UInt16)DAutoRun.Off;

            mUpdateAutoRunFlags();

        }

        private void toolStripAutoTZe_Click(object sender, EventArgs e)
        {
            _mAutoRunTZe = _mAutoRunTZe == (UInt16)DAutoRun.Off ? (UInt16)DAutoRun.On : (UInt16)DAutoRun.Off;
            mUpdateAutoRunFlags();

        }

        private void labelUtcTime_DoubleClick(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;
            bool bAlt = (Control.ModifierKeys & Keys.Alt) == Keys.Alt;

            if (bCtrl == false && bAlt)
            {
                CLicKeyDev.sbReopenLicenseRequest();
            }

        }

        private void toolStripAutoDV2_Click(object sender, EventArgs e)
        {
            _mAutoRunDV2 = _mAutoRunDV2 == (UInt16)DAutoRun.Off ? (UInt16)DAutoRun.On : (UInt16)DAutoRun.Off;
            mUpdateAutoRunFlags();
        }
        private void toolStripAutoDVX_Click(object sender, EventArgs e)
        {
            _mAutoRunDVX = _mAutoRunDVX == (UInt16)DAutoRun.Off ? (UInt16)DAutoRun.On : (UInt16)DAutoRun.Off;
            mUpdateAutoRunFlags();
        }

        private void toolStripImportDV2Scp_Click(object sender, EventArgs e)
        {
            mButtonImportDV2Scp();
        }
        private void mButtonImportDV2Scp()
        {
            if (!mbCheckDaysLeft())
            {
                return;
            }
            if (_mbRunningConversion || _mbRunLoop)
            {
                CProgram.sLogLine("File conversion already running, press stop first!");
            }
            else
            {
                mSetRun(false);

                //openHeaFileDialog.RestoreDirectory = true;
                openDV2ScpFileDialog.InitialDirectory = _mDV2FromDir;
                if (false == mbIsRunNamePresent())
                {
                    openDV2ScpFileDialog.FileName = _mScpFromDir;
                }
                else
                {
                    openDV2ScpFileDialog.FileName = Path.Combine(_mDV2FromDir, mGetRunFileName());
                }

                if (openDV2ScpFileDialog.ShowDialog() == DialogResult.OK)
                {

                    Int16 timeZoneOffset = CProgram.sGetLocalTimeZoneOffsetMin();
                    float tzHours = timeZoneOffset / 60.0F;

                    if (CProgram.sbReqFloat("Import DV2", "Time Zone offset", ref tzHours, "0.0", "hours", -99, 99))
                    {
                        timeZoneOffset = (Int16)(tzHours * 60);
                        _mbRunningConversion = true;
                        CProgram.sSetBusy(true);
                        foreach (String file in openDV2ScpFileDialog.FileNames)
                        {
                            CProgram.sLogLine("Import file " + file);
                            mImportDV2File(file, 0, DImportMethod.Manual, timeZoneOffset);
                        }
                        CProgram.sSetBusy(false);
                        _mbRunningConversion = false;
                    }
                }
            }

        }

        private bool mbGetTzDeviceToServerPath(out string ArDeviceToServerPath, string ASerialNr)
        {
            bool bOk = false;
            string deviceDir = null;

            if (CDvtmsData.sbGetDeviceUnitDir(out deviceDir, DDeviceType.TZ, ASerialNr, false))
            {
                deviceDir = Path.Combine(deviceDir, "ToServer");
                bOk = true;
            }
            ArDeviceToServerPath = deviceDir;
            return bOk;
        }
        private bool mMoveTzDeviceFilesToServer(string ASerialNr, UInt32 ADelActionID)
        {
            return CTzDeviceSettings._sbUseQueue
                ? mMoveTzDeviceFilesToServerUseQueueReaderManaged(ASerialNr, ADelActionID)
                : mMoveTzDeviceFilesToServerNoQueue(ASerialNr);
        }
        private bool mMoveTzDeviceFilesToServerNoQueue(string ASerialNr)
        {
            bool bOk = false;
            string deviceDir = null;
            string storeSnr = CDvtmsData.sGetTzStoreSnrName(ASerialNr);
            string remSnr = storeSnr != ASerialNr ? "->" + storeSnr : "";

            try
            {
                if (_mTzFromDir != null && _mTzFromDir.Length > 0 && CDvtmsData.sbGetDeviceUnitDir(out deviceDir, DDeviceType.TZ, ASerialNr, false))
                {
                    string devicePath = Path.Combine(deviceDir, "ToServer");

                    string[] fileList = Directory.GetFiles(devicePath);
                    int nFiles = fileList == null ? 0 : fileList.Length;

                    DateTime logUTC = DateTime.UtcNow;

                    if (nFiles > 0)
                    {
                        string serverDir = Path.Combine(_mTzFromDir, storeSnr, "downloads");

                        for (int j = 0; j < nFiles; ++j)
                        {
                            string from = fileList[j];
                            string fileName = Path.GetFileName(from);
                            string to = Path.Combine(serverDir, fileName);
                            FileInfo info = null;
                            int size = 0;
                            int ageSec = 0;

                            try
                            {
                                if ((DateTime.UtcNow - logUTC).TotalSeconds > 2)
                                {
                                    Application.DoEvents();
                                    logUTC = DateTime.UtcNow;
                                }
                                try
                                {
                                    info = new FileInfo(from);
                                }
                                catch (Exception)
                                {
                                    info = null;
                                }
                                if (info != null && (size = (int)info.Length) >= 16
                                    && (ageSec = (int)(DateTime.UtcNow - info.LastWriteTimeUtc).TotalSeconds) > 10)
                                {
                                    int ageMin = ageSec / 60;
                                    // wait at least 10 seconds after file has been written
                                    if (File.Exists(to))
                                    {
                                        File.Delete(to);
                                    }
                                    File.Move(from, to);
                                    CProgram.sLogLine("Moved " + ASerialNr + remSnr + ": " + fileName + " to server("
                                        + size.ToString() + ", " + ageMin.ToString() + "min)");
                                }
                                else
                                {
                                    int ageMin = ageSec / 60;
                                    CProgram.sLogLine("Skipped " + ASerialNr + remSnr + ": " + fileName + " to server("
                                         + size.ToString() + ", " + ageMin.ToString() + "min)");
                                }
                            }
                            catch (Exception)
                            {
                                // if move failed no exception
                                CProgram.sLogLine("Moved " + ASerialNr + remSnr + ": " + fileName + " to server Failed");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Move Tz device to server " + ASerialNr + remSnr + " failed", ex);
            }
            return bOk;
        }
        /* Nm
                private bool mMoveTzDeviceFilesToServerUseQueueNodeManaged(string ASerialNr)
                {
                    bool bOk = false;
                    string deviceDir = null;
                    string storeSnr = CDvtmsData.sGetTzStoreSnrName(ASerialNr);
                    string remSnr = storeSnr != ASerialNr ? "->" + storeSnr : "";
                    Int32 nPresent = 0;
                    Int32 nUploaded = 0;
                    Int32 nRemoved = 0;
                    Int32 nDone = 0;
                    Int32 nOldRemoved = 0;
                    Int32 nFilesDevice = 0;
                    Int32 nFilesServer = 0;
                    DateTime startUTC = DateTime.UtcNow;

                    try
                    {
                        if (_mTzFromDir != null && _mTzFromDir.Length > 0 && CDvtmsData.sbGetDeviceUnitDir(out deviceDir, DDeviceType.TZ, ASerialNr, false))
                        {
                            string devicePath = Path.Combine(deviceDir, "ToServer");

                            DirectoryInfo dirInfoDevice = new DirectoryInfo(devicePath);
                            FileInfo[] fileListDevice = dirInfoDevice.GetFiles("*.*");
                            nFilesDevice = fileListDevice == null ? 0 : fileListDevice.Length;
                            int ageSec, ageHours, size, iDevice, iServer;

                            string serverDir = Path.Combine(_mTzFromDir, storeSnr, "downloads");

                            if (Directory.Exists(serverDir))
                            {
                                DirectoryInfo dirInfoServer = new DirectoryInfo(serverDir);
                                FileInfo[] fileListServer = dirInfoServer.GetFiles("*.*");
                                nFilesServer = fileListServer == null ? 0 : fileListServer.Length;

                                if( storeSnr == "L000001")
                                {
                                    int kk = 0;
                                }
                                iDevice = 0;
                                // check if device actions/ settings are on server
                                foreach (FileInfo fiDevice in fileListDevice)
                                {
                                    ++iDevice;
                                    string orgName = fiDevice.Name;

                                    if (orgName.StartsWith(storeSnr)
                                        && (size = (int)fiDevice.Length) >= 16
                                        && (ageSec = (int)((startUTC - fiDevice.LastWriteTimeUtc).TotalSeconds)) > 10)

                                    {
                                        int ageMin = ageSec / 60;
                                        // wait at least 10 seconds after file has been written
                                        if (orgName.EndsWith(CTzDeviceSettings._cSettingFileExt))
                                        {
                                            // move settings file
                                            string toFile = Path.Combine(serverDir, orgName);

                                            // wait at least 10 seconds after file has been written
                                            if (File.Exists(toFile))
                                            {
                                                File.Delete(toFile);
                                            }
                                            File.Move(fiDevice.FullName, toFile);
                                            ++nUploaded;
                                            CProgram.sLogLine("[" + iDevice.ToString() + "/" + nFilesDevice.ToString() + " Moved " + ASerialNr + remSnr + ": "
                                                + orgName + " to server Nm(" + size.ToString() + ", " + ageMin.ToString() + "min)");
                                        }
                                        else if (orgName.EndsWith(CTzDeviceSettings._cActionFileExt))
                                        {
                                            string orgDone = orgName + "_done";
                                            bool bFound = false;

                                            // find if server has the file name
                                            foreach (FileInfo fiServer in fileListServer)
                                            {
                                                string serverName = fiServer.Name;

                                                if (serverName == orgName)
                                                {
                                                    // file exists in device and server => do nothing
                                                    ++nPresent;
                                                    bFound = true;
                                                    break;
                                                }
                                                else if (serverName.StartsWith(orgDone))
                                                {
                                                    // file is marked done op server => remove from device
                                                    try
                                                    {
                                                        File.Delete(fiDevice.FullName);
                                                    }
                                                    catch (Exception)
                                                    {

                                                    }

                                                    ++nDone;
                                                    CProgram.sLogLine("[" + iDevice.ToString() + "/" + nFilesDevice.ToString() + " Remove " + ASerialNr + remSnr + ": "
                                                         + serverName + " done on server Nm(" + size.ToString() + ", " + ageMin.ToString() + "min)");
                                                    bFound = true;
                                                    break;
                                                }
                                            }
                                            if (false == bFound)
                                            {
                                                // not on server => copy to server
                                                string toFile = Path.Combine(serverDir, orgName);

                                                File.Copy(fiDevice.FullName, toFile);
                                                ++nUploaded;
                                                CProgram.sLogLine("[" + iDevice.ToString() + "/" + nFilesDevice.ToString() + " upload " + ASerialNr + remSnr + ": "
                                                  + orgName + " to server Nm(" + size.ToString() + ", " + ageMin.ToString() + "min)");

                                            }
                                        }
                                    }
                                }
                                //
                                // check files on server
                                iServer = 0;
                                foreach (FileInfo fiServer in fileListServer)
                                {
                                    string serverName = fiServer.Name;
                                    ++iServer;
                                    if (serverName.StartsWith(storeSnr))
                                    {
                                        ageHours = (int)(startUTC - fiServer.LastWriteTimeUtc).TotalHours;
                                        if (serverName.EndsWith(CTzDeviceSettings._cActionFileExt))
                                        {
                                            // check if original is in device
                                            bool bFound = false;
                                            // check if the original is still in the device folder
                                            foreach (FileInfo fiDevice in fileListDevice)
                                            {
                                                if (serverName == fiDevice.Name)
                                                {
                                                    bFound = true;
                                                    break;
                                                }
                                            }
                                            if (false == bFound)
                                            {
                                                try
                                                {
                                                    File.Delete(fiServer.FullName);
                                                    ++nRemoved;
                                                }
                                                catch (Exception)
                                                {
                                                }
                                                CProgram.sLogLine("[" + iServer.ToString() + "/" + nFilesServer.ToString() + " Clear " + ASerialNr + remSnr + ": "
                                                     + serverName + " from device, remove on server Nm(" + fiServer.Length.ToString() + ", " + ageHours.ToString() + "hours)");
                                            }
                                        }
                                        else if (serverName.Contains(".tza_done") || serverName.Contains(".tzs_done"))
                                        {
                                            if (ageHours > _mTzQueueMaxAgeHours)
                                            {
                                                // remove old tza files
                                                try
                                                {
                                                    File.Delete(fiServer.FullName);
                                                    ++nRemoved;
                                                }
                                                catch (Exception)
                                                {
                                                }
                                            }
                                        }
                                        else
                                        {
                                        }
                                    }

                                }
                                ageSec = (int)(DateTime.UtcNow - startUTC).TotalSeconds;
                                int nTot = nRemoved + nUploaded+ nDone;
                                if( ageSec > 2 || nPresent > 2 || nTot > 4)
                                {
                                    CProgram.sLogLine("Tz Queue Nm " + ASerialNr + remSnr + ": " + ageSec.ToString() + "sec, nPresent=" + nPresent.ToString()
                                        + ", nRemoved=" + nRemoved.ToString() + ", nUploaded=" + nUploaded.ToString() + ", nDone=" + nDone.ToString());
                                }
                            }
                            else
                            {
                                CProgram.sLogLine("Missing TZ " + ASerialNr + " server Nm dir:" + serverDir);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException("Move Tz device to server Nm queue " + ASerialNr + remSnr + " failed", ex);
                    }
                    return bOk;
                }
        */
        private bool mMoveTzDeviceFilesToServerUseQueueReaderManaged(string ASerialNr, UInt32 ADelActionID)
        {
            bool bOk = false;
            string deviceDir = null;
            string storeSnr = CDvtmsData.sGetTzStoreSnrName(ASerialNr);
            string remSnr = storeSnr != ASerialNr ? "->" + storeSnr : "";
            bool bDoDelAction = ADelActionID > 0;
            bool bDeletedAction = false;
            bool bServerExists = false;
            bool bServerEmpty = false;

            string nameNextAction = "";
            string fileNextAction = "";
            int sizeNextAction = 0;
            int minNextAction = 0;
            bool bFoundNextAction = false;

            Int32 nPresent = 0;
            Int32 nFilesDevice = 0;
            Int32 nUploaded = 0;
            DateTime startUTC = DateTime.UtcNow;

            try
            {
                if (_mTzFromDir != null && _mTzFromDir.Length > 0 && CDvtmsData.sbGetDeviceUnitDir(out deviceDir, DDeviceType.TZ, ASerialNr, false))
                {
                    string devicePath = Path.Combine(deviceDir, "ToServer");

                    DirectoryInfo dirInfoDevice = new DirectoryInfo(devicePath);
                    FileInfo[] fileListDevice = dirInfoDevice.GetFiles("*.*");
                    nFilesDevice = fileListDevice == null ? 0 : fileListDevice.Length;
                    int ageSec;

                    string serverDir = Path.Combine(_mTzFromDir, storeSnr, "downloads");
                    if (storeSnr == "L000001")
                    {
                        int kk = 0;
                    }
                    string actionStoreName = storeSnr + CTzDeviceSettings._cActionFileExt;
                    string actionSearchDelName = storeSnr + "_" + ADelActionID.ToString("00000");

                    if (false == Directory.Exists(serverDir))
                    {
                        CProgram.sLogLine("Missing TZ " + ASerialNr + " server Rm dir:" + serverDir);
                    }
                    else
                    {

                        // determin state on server
                        string fileServerAction = Path.Combine(serverDir, actionStoreName);
                        FileInfo fiServer = new FileInfo(fileServerAction);

                        bServerExists = fiServer != null && fiServer.Exists && fiServer.Length >= _cActionEmptySize;
                        bServerEmpty = bServerExists && fiServer.Length == _cActionEmptySize;

                        if (bServerExists && false == bServerEmpty)
                        {
                            double serverHour = (DateTime.UtcNow - fiServer.LastWriteTimeUtc).TotalHours;

                            if (serverHour > _mTzActionDeadHours) // check old actions
                            {
                                FileInfo fiServerDone = new FileInfo(fileServerAction + "_done");

                                if (fiServerDone != null && fiServerDone.Exists)
                                {
                                    double doneMin = (DateTime.UtcNow - fiServerDone.LastWriteTimeUtc).TotalMinutes;
                                    if (doneMin > 1 && doneMin < 90)
                                    {
                                        bServerExists = false;    // overwrite old action if there is a GET (prevent hang on an bad action or missing ACTION_SUCCESS)
                                        CProgram.sLogLine("TZ old action after GET " + ASerialNr + remSnr + ": "
                                            + serverHour.ToString("0.0") + " hours, Get " + doneMin.ToString("0.0") + "min -> replace");

                                        string fileOld = "";
                                        string fileID = fileServerAction + "_file";
                                        try
                                        {
                                            if (File.Exists(fileID))
                                            {
                                                fileOld = File.ReadAllText(fileID);

                                                if (fileOld != null && fileID.Length > 0)
                                                {
                                                    fileOld = Path.Combine(devicePath, fileOld.Trim());

                                                    if (File.Exists(fileOld))
                                                    {
                                                        File.Delete(fileOld);
                                                        CProgram.sLogLine("TZ old action after GET " + ASerialNr + remSnr + ": deleteing " + fileOld);
                                                    }
                                                    else
                                                    {
                                                        CProgram.sLogLine("TZ old action after GET " + ASerialNr + remSnr + ": not in queue " + fileOld);
                                                    }
                                                }
                                                //File.Delete(fileID);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            CProgram.sLogException("TZ old action after GET " + ASerialNr + remSnr + "!", ex);
                                        }
                                    }
                                }
                            }
                        }

                        int size, iDevice = 0;
                        // check if device actions/ settings are on server
                        foreach (FileInfo fiDevice in fileListDevice)
                        {
                            ++iDevice;
                            string orgName = fiDevice.Name;

                            if (orgName.StartsWith(storeSnr)
                                && (size = (int)fiDevice.Length) >= 16
                                && (ageSec = (int)((startUTC - fiDevice.LastWriteTimeUtc).TotalSeconds)) > 10)

                            {
                                int ageMin = ageSec / 60;
                                // wait at least 10 seconds after file has been written
                                if (orgName.EndsWith(CTzDeviceSettings._cSettingFileExt))
                                {
                                    // move settings file to server
                                    string toFile = Path.Combine(serverDir, orgName);

                                    if (File.Exists(toFile))
                                    {
                                        File.Delete(toFile);
                                    }
                                    File.Move(fiDevice.FullName, toFile);
                                    CProgram.sLogLine("[" + iDevice.ToString() + "/" + nFilesDevice.ToString() + " Moved " + ASerialNr + remSnr + ": "
                                        + orgName + " to server Rm(" + size.ToString() + "B, " + ageMin.ToString() + "min)");
                                    ++nUploaded;
                                }
                                else if (orgName.EndsWith(CTzDeviceSettings._cActionFileExt))
                                {
                                    // action files in device toServer
                                    if (bDoDelAction && orgName.StartsWith(actionSearchDelName))
                                    {
                                        // found the action file that has to be deleted
                                        try
                                        {
                                            File.Delete(fiDevice.FullName);
                                            CProgram.sLogLine("[" + iDevice.ToString() + "/" + nFilesDevice.ToString() + " Remove " + ASerialNr + remSnr + ": "
                                                  + orgName + " from RM queue (" + size.ToString() + "B, " + ageMin.ToString() + "min)");
                                            bDeletedAction = true;
                                        }
                                        catch (Exception ex)
                                        {
                                            CProgram.sLogException("[" + iDevice.ToString() + "/" + nFilesDevice.ToString() + " Failed Remove " + ASerialNr + remSnr + ": "
                                                   + orgName + " from RM queue (" + size.ToString() + "B, " + ageMin.ToString() + "min)", ex);
                                        }
                                        if (bDeletedAction)
                                        {
                                            // found the action file that has to be deleted
                                            string doneName = storeSnr + ".ActionDone";
                                            try
                                            {
                                                string text = ADelActionID.ToString() + "\r\n" + orgName + "\r\n";
                                                File.WriteAllText(Path.Combine(deviceDir, doneName), text);
                                            }
                                            catch (Exception ex)
                                            {
                                                CProgram.sLogException(" Failed write " + ASerialNr + remSnr + ": "
                                                       + doneName + " about removing " + orgName, ex);
                                            }

                                        }
                                    }
                                    else
                                    {
                                        ++nPresent;
                                        if (false == bFoundNextAction)
                                        {
                                            // found the first action in the list
                                            fileNextAction = fiDevice.FullName;
                                            nameNextAction = orgName;
                                            sizeNextAction = size;
                                            minNextAction = ageMin;
                                            bFoundNextAction = true;
                                        }
                                    }
                                }
                            }
                            // end loop through  device list
                        }
                        if (storeSnr == "L000001")
                        {
                            int kk = 0;
                        }
                        if (bFoundNextAction)
                        {
                            bool bOldAction = nameNextAction == actionStoreName;
                            if (bDoDelAction || false == bServerExists || bServerEmpty || bOldAction)
                            {
                                // copy action to server
                                try
                                {
                                    if (sizeNextAction <= _cActionEmptySize)
                                    {
                                        File.Copy(fileNextAction, fileServerAction, true);
                                        CProgram.sLogLine("TZ Copy/Move " + ASerialNr + remSnr + ": "
                                              + nameNextAction + " to Rm queue (" + sizeNextAction.ToString() + "B(empty), " + minNextAction.ToString() + "min)");
                                        File.Delete(fileNextAction);    // empty actions should not holdup the queue
                                    }
                                    else
                                    {
                                        File.Copy(fileNextAction, fileServerAction, true);


                                        CProgram.sLogLine("TZ Copy " + ASerialNr + remSnr + ": "
                                              + nameNextAction + " to Rm queue (" + sizeNextAction.ToString() + "B, " + minNextAction.ToString() + "min)");
                                    }
                                    // save action name used for the action (to be used when an old action is hanging up the queue)
                                    try
                                    {
                                        File.WriteAllText(fileServerAction + "_file", nameNextAction);
                                    }
                                    catch (Exception ex)
                                    {
                                        CProgram.sLogException("TZ Failed write name to file " + ASerialNr + remSnr + ": "
                                               + nameNextAction + " server Rm queue ", ex);
                                    }

                                    ++nUploaded;
                                    try
                                    {
                                        // remove the action done file to track next GET
                                        string fileServerDone = fileServerAction + "_done";
                                        if (File.Exists(fileServerDone))
                                        {
                                            File.Delete(fileServerDone);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        CProgram.sLogException("TZ Failed delete done " + ASerialNr + remSnr + ": "
                                               + nameNextAction + " server Rm queue ", ex);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    CProgram.sLogException("TZ Failed Copy " + ASerialNr + remSnr + ": "
                                           + nameNextAction + " from Rm queue (" + sizeNextAction.ToString() + "B, " + minNextAction.ToString() + "min)", ex);
                                }

                                if (bOldAction && minNextAction > 120 && (DateTime.Now - _mStartRunTimeDT).TotalMinutes > 90)
                                // old action: delete after 120 min (must use new Eventboard)
                                {
                                    // remove old setting from device ToServer: old way without queue
                                    try
                                    {
                                        File.Delete(fileNextAction);
                                        CProgram.sLogLine("TZ Removed old " + ASerialNr + remSnr + ": "
                                              + nameNextAction + " from Rm queue (" + sizeNextAction.ToString() + "B, " + minNextAction.ToString() + "min)");
                                    }
                                    catch (Exception ex)
                                    {
                                        CProgram.sLogException("TZ Failed Remove old " + ASerialNr + remSnr + ": "
                                               + nameNextAction + " from Rm queue (" + sizeNextAction.ToString() + "B, " + minNextAction.ToString() + "min)", ex);
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (false == bServerExists || bDoDelAction || false == bServerEmpty)
                            {
                                // copy empty to server download
                                string pathEmpty;

                                if (false == CDvtmsData.sbGetDeviceTypeDir(out pathEmpty, DDeviceType.TZ))
                                {
                                    CProgram.sLogError("TZ missing device dir! ");
                                }
                                else
                                {
                                    string nameEmpty = "empty" + CTzDeviceSettings._cActionFileExt;
                                    string fileEmpty = Path.Combine(pathEmpty, nameEmpty);
                                    bool bCopyEmpty = File.Exists(fileEmpty);

                                    if (false == bCopyEmpty)
                                    {
                                        fileEmpty = Path.Combine(CProgram.sGetProgDir(), nameEmpty);
                                        bCopyEmpty = File.Exists(fileEmpty);
                                    }

                                    if (false == bCopyEmpty)
                                    {
                                        CProgram.sLogError("TZ missing emty action file! " + nameEmpty);
                                    }
                                    else
                                    {
                                        string strReason = "";
                                        if (false == bServerExists) strReason += "server missing ";
                                        else if (false == bServerEmpty) strReason += "server not empty ";
                                        if (bDoDelAction) strReason += "delete last ";

                                        // copy empty action to server
                                        try
                                        {
                                            File.Copy(fileEmpty, fileServerAction, true);
                                            CProgram.sLogLine("TZ Copy " + ASerialNr + remSnr + ": empty action to to Rm queue " + strReason);
                                            ++nUploaded;
                                            try
                                            {
                                                // remove the action done file to track next GET
                                                string fileServerDone = fileServerAction + "_done";
                                                if (File.Exists(fileServerDone))
                                                {
                                                    File.Delete(fileServerDone);
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                CProgram.sLogException("TZ Failed delete done " + ASerialNr + remSnr + ": empty to server Rm queue ", ex);
                                            }
                                            try
                                            {
                                                // remove the action name file to track next GET
                                                string fileServerID = fileServerAction + "_file";
                                                if (File.Exists(fileServerID))
                                                {
                                                    File.Delete(fileServerID);
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                CProgram.sLogException("TZ Failed delete file ID " + ASerialNr + remSnr + ": empty to server Rm queue ", ex);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            CProgram.sLogException("TZ Failed Copy " + ASerialNr + remSnr + ": empty action to Rm queue ", ex);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    ageSec = (int)(DateTime.UtcNow - startUTC).TotalSeconds;
                    if (ageSec > 2 || nPresent > 2 || bDoDelAction || _mbLogExtra)
                    {
                        string s = "Tz Rm Queue " + ASerialNr + remSnr + ": " + ageSec.ToString() + "sec, nPresent=" + nPresent.ToString();
                        if (bDeletedAction)
                        {
                            s += ", removed " + ADelActionID.ToString();
                        }
                        else if (bDoDelAction)
                        {
                            s += ", failed remove " + actionSearchDelName;
                        }
                        s += ", nUploaded=" + nUploaded.ToString();
                        CProgram.sLogLine(s);
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Move Tz device to server Rm queue " + ASerialNr + remSnr + " failed", ex);
            }
            return bOk;
        }

        private bool mbTzCreateBaseDirs()
        {
            bool bOk = true;

            if (_mTzFromDir == null || _mTzFromDir.Length <= 2)
            {
                CProgram.sLogLine("Http TZ from directory is not defined!");
                bOk = false;
            }
            if (false == Directory.Exists(_mTzFromDir))
            {
                CProgram.sLogLine("Http TZ from directory is not defined!");
                bOk = false;
            }
            if (bOk)
            {
                // create HttpTZ directories
            }
            return bOk;
        }

        private bool mbTzCreateOneDevice(ref UInt32 ArCount, ref UInt32 ArCreated, string ALogPreText, String AModelTitle,
            ref CDeviceInfo ArDeviceTemplate, string ASerialNr, string ADefaultSettingsFile, string ADefaultActionFile)
        {
            bool bOk = false;

            string serialNr;
            string storeName;
            string altName;
            string remSnr = "";
            string deviceDir = "";
            bool bDeviceDirCreated = false;
            bool bServerDirCreated = false;
            bool bDeviceDbCreated = false;
            bool bDeviceDefaultsCopied = false;
            bool bCopyDefaults = ADefaultSettingsFile != null && null != ADefaultActionFile;

            string header = ALogPreText + " " + AModelTitle + " " + ASerialNr;
            string result = "";
            int lenStr, pos;

            try
            {
                if (ASerialNr == null || ASerialNr.Length == 0)
                {
                    return true;    // skip line
                }
                pos = ASerialNr.IndexOf('=');
                if (pos > 0)
                {
                    serialNr = ASerialNr.Substring(0, pos).Trim();  // remove '='
                }
                else
                {
                    serialNr = ASerialNr.Trim();
                }

                lenStr = serialNr != null ? serialNr.Length : 0;

                if (lenStr == 0)
                {
                    return true;    // skip line
                }
                if (false == Char.IsLetterOrDigit(serialNr[0]))
                {
                    if (serialNr[0] == '+' && lenStr > 0)
                    {
                        serialNr = serialNr.Substring(1);   // only accept
                    }
                    else
                    {
                        return true;    // skip line
                    }
                }
                altName = CDvtmsData.sGetTzAltSnrName(serialNr);
                bOk = CRecDeviceList.sbTzIsInLicenseList(serialNr, true);
                if (false == bOk)
                {
                    bOk = CRecDeviceList.sbTzIsInLicenseList(altName, true);
                }
                if (false == bOk)
                {
                    result = "Not in license list.";
                }
                if (bOk)
                {
                    storeName = CDvtmsData.sGetTzStoreSnrName(serialNr);
                    if (storeName != serialNr)
                    {
                        //                            result += "[" + name + "] ";
                        remSnr = "->" + storeName;
                    }

                    // device / <storeName> / ToServer
                    if (CDvtmsData.sbGetDeviceUnitDir(out deviceDir, DDeviceType.TZ, storeName, true))
                    {
                        bOk = true;
                    }
                    else if (false == CDvtmsData.sbCreateDeviceUnitDir(out deviceDir, DDeviceType.TZ, storeName))
                    {
                        result += " create Device directory failed";
                    }
                    else
                    {
                        bDeviceDirCreated = true;
                        bOk = true;
                    }
                    if (bOk)
                    {
                        // device / <storeName> / ToServer
                        string subDir = Path.Combine(deviceDir, "ToServer");

                        if (false == Directory.Exists(subDir))
                        {
                            if (null == Directory.CreateDirectory(subDir))
                            {
                                bOk = false;
                                result += " device ToServer dir failed create";
                            }
                            else
                            {
                                bDeviceDirCreated = true;
                            }
                        }
                        if (bOk && bCopyDefaults)
                        {
                            bDeviceDefaultsCopied = mbCopyOneFile(ADefaultSettingsFile, Path.Combine(deviceDir, storeName + Path.GetExtension(ADefaultSettingsFile)), false);
                            bDeviceDefaultsCopied &= mbCopyOneFile(ADefaultSettingsFile, Path.Combine(subDir, storeName + Path.GetExtension(ADefaultSettingsFile)), false);

                            bDeviceDefaultsCopied &= mbCopyOneFile(ADefaultActionFile, Path.Combine(deviceDir, storeName + Path.GetExtension(ADefaultActionFile)), false);

                            string actionName = storeName;
                            if (CTzDeviceSettings._sbUseQueue)
                            {
                                actionName = CTzDeviceSettings.sCombineFileIDUtc(storeName, CTzDeviceSettings._cFileID_UNKNOWN, DateTime.UtcNow);
                                // Action name must contain the fileID and save time to be able to remove the action from the queue
                            }
                            bDeviceDefaultsCopied &= mbCopyOneFile(ADefaultActionFile, Path.Combine(subDir, actionName + Path.GetExtension(ADefaultActionFile)), false);
                        }
                        if (bOk)
                        {
                            // device / <storeName> / ToServer
                            subDir = Path.Combine(deviceDir, "FromServer");
                            if (false == Directory.Exists(subDir))
                            {
                                if (null == Directory.CreateDirectory(subDir))
                                {
                                    bOk = false;
                                    result += " device FromServer dir failed create";
                                }
                                else
                                {
                                    bDeviceDirCreated = true;
                                }
                            }
                        }
                    }
                    if (bOk)
                    {
                        string serverDir = Path.Combine(_mTzFromDir, storeName);

                        if (false == Directory.Exists(serverDir))
                        {
                            if (null == Directory.CreateDirectory(serverDir))
                            {
                                bOk = false;
                                result += " device server dir failed create";
                            }
                            else
                            {
                                bServerDirCreated = true;
                            }
                        }

                        string subDir = Path.Combine(serverDir, "downloads");

                        if (false == Directory.Exists(subDir))
                        {
                            if (null == Directory.CreateDirectory(subDir))
                            {
                                bOk = false;
                                result += " device server downloads dir failed create";
                            }
                        }
                        if (bOk)
                        {
                            subDir = Path.Combine(serverDir, "uploads");
                            if (false == Directory.Exists(subDir))
                            {
                                if (null == Directory.CreateDirectory(subDir))
                                {
                                    bOk = false;
                                    result += " device server uploads dir failed create";
                                }
                                else
                                {
                                    bServerDirCreated = true;
                                }
                            }
                        }
                    }
                }

                if (bOk)
                {
                    UInt32 indexKey = 0;

                    if (bDeviceDirCreated && bServerDirCreated)
                    {
                        result += "device+server dir created, ";
                    }
                    else if (bDeviceDirCreated)
                    {
                        result += "device dir created, ";
                    }
                    else if (bServerDirCreated)
                    {
                        result += "server dir created, ";
                    }
                    if (bDeviceDefaultsCopied)
                    {
                        result += "defaults copied, ";
                    }
                    bOk = mbCreateDeviceInDB(out bDeviceDbCreated, out indexKey, AModelTitle, ref ArDeviceTemplate, serialNr, altName);

                    if (bOk)
                    {
                        result += "device #" + indexKey.ToString();
                        if (bDeviceDbCreated)
                        {
                            result += " created";
                        }
                    }
                    else
                    {
                        result += "failed device in dBase!";
                    }
                }
                if (bOk)
                {
                    CProgram.sLogLine(header + remSnr + ": " + result);
                    ++ArCount;
                    if (bDeviceDirCreated || bDeviceDbCreated || bServerDirCreated)
                    {
                        ++ArCreated;
                    }

                }
                else
                {
                    CProgram.sLogError(header + remSnr + ": " + result);
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Create " + AModelTitle + " in dBase device list failed for " + ASerialNr + remSnr, ex);
                bOk = false;
            }

            return bOk;
        }

        private void mTzCreateDevices()
        {
            string text = "";
            string modelTitle = "TZ";
            CDeviceInfo deviceTemplate = null;
            string defaultSettingsFile = null;
            string defaultActionFile = null;
            string defaultsSettingsName = "defaults.tzs";
            string defaultsActionsName = "defaults.tza";
            string title = "Create " + modelTitle + " devices";
            bool bCopyDefaults = true;

            if (!mbCheckDaysLeft())
            {
                return;
            }
            try
            {

                bool bOk = ReqTextForm.sbReqText(title, "List serial numbers (*=license device file, do not use * with multiple i-readers)", ref text);

                if (bOk)
                {
                    bOk = mbTzCreateBaseDirs();
                }

                if (bOk)
                {
                    bOk = mbCreateDeviceTemplate(modelTitle, ref deviceTemplate,
                        ref _mTzTemplateModel, ref _mTzTemplateFirmware, ref _mbTzTemplateSetAvailable);
                }
                if (bOk)
                {
                    bOk = CProgram.sbReqBool(title, "Use defaults (No intake check)", ref bCopyDefaults);
                }
                if (bCopyDefaults)
                {
                    string devicePath;

                    bOk = CDvtmsData.sbCreateDeviceTypeDir(out devicePath, DDeviceType.TZ);

                    defaultSettingsFile = Path.Combine(devicePath, defaultsSettingsName);
                    if (false == File.Exists(defaultSettingsFile))
                    {
                        bOk = false;
                        CProgram.sLogLine("Missing defaults file: " + defaultSettingsFile);
                    }
                    defaultActionFile = Path.Combine(devicePath, defaultsActionsName);
                    if (false == File.Exists(defaultActionFile))
                    {
                        bOk = false;
                        CProgram.sLogLine("Missing defaults file: " + defaultActionFile);
                    }
                    if (false == bOk)
                    {
                        CProgram.sPromptError(true, title, "Missing defaults to copy");
                    }
                }

                if (bOk)
                {
                    text = text.Trim();
                    if (text != null && text.Length > 0)
                    {
                        CRecDeviceList.sDeviceListClear(); // always clear list and reload the new device license file
                        CRecDeviceList.sbTzDeviceListsCheck(_mTzFromDir, true, false);

                        if (text[0] == '*')
                        {
                            // from device list
                            modelTitle += "*";

                            int n = CRecDeviceList.sGetTzDeviceCount();

                            if (n > 0)
                            {
                                int i = 0;
                                UInt32 nOk = 0;
                                UInt32 nCreated = 0;

                                if (mbProcessStartLoop("Create TZ", n, false))
                                {
                                    foreach (string snr in CRecDeviceList.sGetTzLicenseList())
                                    {
                                        mbProcessNextLoop(++i, false, snr);
                                        if (mbTzCreateOneDevice(ref nOk, ref nCreated, "[" + i.ToString() + "/" + n.ToString() + "]", modelTitle,
                                            ref deviceTemplate, snr, defaultSettingsFile, defaultActionFile))
                                        {

                                        }
                                        else if (false == CProgram.sbPromptOkCancel(true, "Create " + modelTitle + " " + n.ToString() + " devices",
                                            i.ToString() + " " + snr + " failed, Continue?"))
                                        {
                                            CProgram.sLogError("Create " + modelTitle + " devices canceled");
                                            break;
                                        }
                                    }
                                    mbProcessEndLoop(i, false, nCreated.ToString() + " TZ created");
                                }
                                CProgram.sLogLine("Create " + modelTitle + " " + nOk.ToString() + " devices done / " + n.ToString() + " licensed, " + nCreated.ToString() + " created");
                                if (nCreated > 0)
                                {
                                    CProgram.sAskOk("Create " + modelTitle + " devices", nCreated.ToString() + " devices created, finish configuration in Eventboard!");
                                }
                            }
                            else
                            {
                                CProgram.sLogError("Create " + modelTitle + " no devices in license list");
                            }
                        }
                        else
                        {
                            string[] lines = text.Split();
                            int i, n = lines == null ? 0 : lines.Length;
                            UInt32 nOk = 0;
                            UInt32 nCreated = 0;
                            string snr;

                            if (mbProcessStartLoop("Create TZ", n, false))
                            {
                                for (i = 0; i < n;)
                                {
                                    snr = lines[i];
                                    mbProcessNextLoop(++i, false, snr);

                                    if (mbTzCreateOneDevice(ref nOk, ref nCreated, "[" + i.ToString() + "/" + n.ToString() + "]", modelTitle,
                                        ref deviceTemplate, snr, defaultSettingsFile, defaultActionFile))
                                    {

                                    }
                                    else if (false == CProgram.sbPromptOkCancel(true, "Create " + modelTitle + " " + n.ToString() + " devices",
                                        i.ToString() + " " + snr + " failed, Continue?"))
                                    {
                                        CProgram.sLogError("Create " + modelTitle + " devices canceled");
                                        break;
                                    }
                                }
                                mbProcessEndLoop(i, false, nCreated.ToString() + " TZ created");
                            }
                            CProgram.sLogLine("Create " + modelTitle + " " + nOk.ToString() + " devices done / " + n.ToString() + " lines, " + nCreated.ToString() + " created");
                            if (nCreated > 0)
                            {
                                CProgram.sAskOk("Create " + modelTitle + " devices", nCreated.ToString() + " devices created, finish configuration in Eventboard!");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Create Tz devices failed ", ex);
            }

        }

        private void toolStripAddTZ_Click(object sender, EventArgs e)
        {
            mButtonAddTZ();
        }
        private void mButtonAddTZ()
        {
            if (!mbCheckDaysLeft())
            {
                return;
            }
            if (_mbRunningConversion || _mbRunLoop)
            {
                CProgram.sLogLine("File conversion already running, press stop first!");
            }
            else
            {
                mTzCreateDevices();
            }
        }
        private void mTzCreateOneDeviceOld()
        {
            if (!mbCheckDaysLeft())
            {
                return;
            }
            if (_mbRunningConversion || _mbRunLoop)
            {
                CProgram.sLogLine("File conversion already running, press stop first!");
            }
            else if (_mTzFromDir == null || _mTzFromDir.Length <= 2)
            {
                CProgram.sLogLine("TZ from directory is not defined!");
            }
            else if (false == Directory.Exists(_mTzFromDir))
            {
                CProgram.sLogLine("TZ from directory is not defined!");
            }
            else
            {
                string snr = "";
                string remSnr = "";
                string result = "";
                string name = "";
                bool bOk = false;
                bool bCont = true;

                mSetRun(false);

                try
                {
                    if (CProgram.sbReqLabel("Add TZ device", "Serial NR", ref snr, "", false) && snr.Length > 0)
                    {
                        // create HttpTZ directories
                        string snr2 = snr.Trim();
                        name = CDvtmsData.sGetTzStoreSnrName(snr2);
                        if (name != snr2)
                        {
                            //                            result += "[" + name + "] ";
                            remSnr = "->" + name;
                        }

                        string serverDir = Path.Combine(_mTzFromDir, name);
                        string deviceDir;

                        if (Directory.Exists(serverDir)) { result += "TZ server dir exists"; bOk = true; }
                        else if (null != Directory.CreateDirectory(serverDir)) { result += "TZ server dir created"; bOk = true; }
                        else { result += "TZ server dir failed create"; }

                        if (bOk)
                        {
                            string subDir = Path.Combine(serverDir, "downloads");

                            if (false == Directory.Exists(subDir) && (null == Directory.CreateDirectory(subDir))) { bOk = false; result += ", TZ server downloads dir failed create"; }
                        }
                        if (bOk)
                        {
                            string subDir = Path.Combine(serverDir, "uploads");

                            if (false == Directory.Exists(subDir) && (null == Directory.CreateDirectory(subDir))) { bOk = false; result += ", TZ server uploads dir failed create"; }
                        }
                        if (bOk)
                        {
                            if (false == CDvtmsData.sbCreateDeviceTypeDir(out deviceDir, DDeviceType.TZ))//, "TzAera"))
                            {
                                result += ", create TZ Device directory failed";
                                bOk = false;
                            }
                            else
                            {
                                serverDir = Path.Combine(deviceDir, name);
                                if (Directory.Exists(serverDir)) { result += ", TZ device dir exists"; }
                                else if (null != Directory.CreateDirectory(serverDir)) { result += ", TZ device dir created"; }
                                else { result += "TZ server dir failed create"; bOk = false; }

                                if (bOk)
                                {
                                    string subDir = Path.Combine(serverDir, "ToServer");

                                    if (false == Directory.Exists(subDir) && (null == Directory.CreateDirectory(subDir))) { bOk = false; result += ", TZ device ToServer dir failed create"; }
                                }
                            }
                        }
                    }
                    CProgram.sLogLine("TZ " + snr + remSnr + ": " + result);
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Create TZ " + snr + remSnr + " failed:" + result, ex);
                }
            }
        }

        private void toolStripAutoHSir_Click(object sender, EventArgs e)
        {
            _mAutoRunHttpSir = _mAutoRunHttpSir == (UInt16)DAutoRun.Off ? (UInt16)DAutoRun.On : (UInt16)DAutoRun.Off;
            mUpdateAutoRunFlags();
        }

        private void mOpenDoc(string ADocName)
        {
            string docName = "local" + ADocName;
            string docPath = Path.Combine(CProgram.sGetProgDir(), docName);
            bool bOpen = false;

            try
            {
                if (File.Exists(docPath))
                {
                    bOpen = true;
                }
                else
                {
                    docName = ADocName;
                    docPath = Path.Combine(CProgram.sGetProgDir(), docName);
                    bOpen = File.Exists(docPath);
                }

                if (bOpen)
                {
                    ProcessStartInfo startInfo = new ProcessStartInfo();
                    startInfo.FileName = docPath;
                    //startInfo.Arguments;
                    Process.Start(startInfo);
                }
                else
                {
                    CProgram.sAskWarning("Eventboard", "File " + ADocName + " not found!");
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed open doc " + ADocName, ex);
            }
        }

        private void mOpenManual()
        {
            mOpenDoc("Content\\1. Manuals\\I-ReaderManual.pdf");
        }
        private void mOpenHystory()
        {
            mOpenDoc("Content\\0. Release notes\\ChangeHistoryI-Reader.rtf");
        }
        private void mOpenDownload()
        {
            mOpenDoc("DownloadI-Reader.rtf");
        }

        private void toolStripButtonHelp_Click(object sender, EventArgs e)
        {
            contextMenuHelp.Show(Control.MousePosition);
        }

        private void openManualToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mOpenManual();
        }

        private void openChangeHistoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mOpenHystory();
        }

        private void openLicenseInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CLicKeyDev.sbReopenLicenseRequest();
        }

        private void openDownloadLinksToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mOpenDownload();
        }
        private void mOpenHelpDesk(bool AbTV)
        {
            string exeName = AbTV ? "helpdesk2.exe" : "helpdesk.exe";
            string exePath = Path.Combine("helpdesk\\", exeName);

            if (File.Exists(exePath))
            {
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = exePath;
                //startInfo.Arguments;
                Process.Start(startInfo);
            }
            else
            {
                CProgram.sPromptWarning(true, "Open helpdesk", "failed to find helpdesk file");
            }

        }

        private void tMIHelpdeskToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mOpenHelpDesk(false);
        }

        private void openStandardTeamviewerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mOpenHelpDesk(true);
        }
        /*
                private bool mbCreateOneSirona( out bool ArDeviceCreated, string ADeviceSnr, UInt32 AModelNr, UInt16 AState)

                {
                    bool bOk = false;
                    bool bDeviceDirCreated = false;
                    bool bDeviceRowCreated = false;
                    bool bDeviceSeverCreated = false;

                    CDeviceInfo deviceInfo;

                }
        */

        private bool mbCreateDeviceTemplate(String AModelTitle, ref CDeviceInfo ArDeviceTemplate,
            ref UInt32 ArModel, ref string ArFirmWare, ref bool ArbAvailable)
        {
            bool bOk = false;

            try
            {
                if (ArDeviceTemplate != null)
                {
                    bOk = true; // already exist
                }
                else
                {
                    string title = "Create " + AModelTitle + " Devices";
                    CDeviceInfo deviceInfo = new CDeviceInfo(CDvtmsData.sGetDBaseConnection());
                    CDeviceModel modelInfo = new CDeviceModel(CDvtmsData.sGetDBaseConnection());
                    List<CSqlDataTableRow> modelList = null;
                    UInt16 labelVarNr = (UInt16)DDeviceModelVars.ModelLabel;
                    UInt64 loadMask = modelInfo.mMaskIndexKey | CSqlDataTableRow.sGetMask(labelVarNr);
                    string selectedModel = "";

                    List<string> enumList = new List<string>();
                    bool bError;

                    if (modelInfo != null && deviceInfo != null && enumList != null)
                    {
                        // load model list
                        if (modelInfo.mbDoSqlSelectList(out bError, out modelList, loadMask, 0, true, ""))
                        {
                            UInt16 enumIndex = 0;
                            if (modelInfo.mbListFillStringList(ref enumList, modelList, labelVarNr, ArModel, out enumIndex))
                            {
                                if (ReqEnumForm.sbReqEnumFromSqlList(title, "Model", enumList, ref enumIndex, ""))
                                {
                                    selectedModel = enumList[enumIndex];

                                    ArModel = modelInfo.mGetIndexKeyFromList(modelList, labelVarNr, selectedModel);
                                }
                            }
                        }
                        if (ArModel > 0)
                        {
                            if (CProgram.sbReqLabel(title, "Firmware", ref ArFirmWare, "", false)
                                && CProgram.sbReqBool(title, "Set to Available (No intake check)", ref ArbAvailable))
                            {
                                deviceInfo._mDeviceModel_IX = ArModel;
                                deviceInfo._mFirmwareVersion = ArFirmWare;
                                deviceInfo._mState_IX = (UInt16)(ArbAvailable ? DDeviceState.Available : DDeviceState.New);
                                deviceInfo._mDateFirstInService = DateTime.Now;
                                deviceInfo._mDateNextService = deviceInfo._mDateFirstInService;


                                CProgram.sLogLine(AModelTitle + "template: " + selectedModel + ", fw= " + ArFirmWare + ", "
                                    + (ArbAvailable ? " set available " : "set new"));

                                ArDeviceTemplate = deviceInfo;
                                bOk = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Create Sirona template failed", ex);
            }
            return bOk;
        }

        private bool mbCreateDeviceInDB(out bool ArbCreated, out UInt32 ArIndexKey, String AModelTitle, ref CDeviceInfo ArDeviceTemplate, string ASerialNr, string AAltSerialNr = null)
        {
            bool bOk = false;
            UInt32 indexKey = 0;
            bool bCreated = false;
            bool bPresent = false;

            try
            {
                if (ArDeviceTemplate != null && ASerialNr != null && ASerialNr.Length > 0)
                {
                    CDeviceInfo deviceInfo = (CDeviceInfo)ArDeviceTemplate.mCreateCopy();
                    List<CSqlDataTableRow> deviceList = null;
                    UInt16 snrVarNr = (UInt16)DDeviceInfoVars.DeviceSerialNr;

                    UInt64 loadMask = deviceInfo.mMaskIndexKey | CSqlDataTableRow.sGetMask(snrVarNr);
                    UInt64 whereMask = CSqlDataTableRow.sGetMask(snrVarNr);
                    bool bError;

                    if (deviceInfo != null)
                    {
                        deviceInfo._mDeviceSerialNr = ASerialNr;
                        // load model list
                        if (deviceInfo.mbDoSqlSelectList(out bError, out deviceList, loadMask, whereMask, false, ""))
                        {
                            bPresent = false == bError && deviceList != null && deviceList.Count > 0;
                            if (bPresent)
                            {
                                indexKey = deviceList[0].mIndex_KEY;
                                bOk = true;
                            }
                        }
                        if (false == bPresent && AAltSerialNr != null && AAltSerialNr.Length > 0)
                        {
                            deviceInfo._mDeviceSerialNr = AAltSerialNr;
                            // load model list
                            if (deviceInfo.mbDoSqlSelectList(out bError, out deviceList, loadMask, whereMask, false, ""))
                            {
                                bPresent = false == bError && deviceList != null && deviceList.Count > 0;
                                if (bPresent)
                                {
                                    indexKey = deviceList[0].mIndex_KEY;
                                    bOk = true;
                                }
                            }
                        }
                        if (false == bPresent)
                        {
                            deviceInfo = (CDeviceInfo)ArDeviceTemplate.mCreateCopy();
                            deviceInfo._mDeviceSerialNr = ASerialNr;
                            deviceInfo.mIndex_KEY = 0;
                            bOk = deviceInfo.mbDoSqlInsert();
                            if (bOk)
                            {
                                indexKey = deviceInfo.mIndex_KEY;

                                bCreated = indexKey > 0;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Create " + AModelTitle + " in devile list failed", ex);
            }
            ArbCreated = bCreated;
            ArIndexKey = indexKey;
            return bOk;
        }

        private bool mbSironaCreateBaseDirs()
        {
            bool bOk = true;

            if (_mHttpSirFromDir == null || _mHttpSirFromDir.Length <= 2)
            {
                CProgram.sLogLine("Http Sirona from directory is not defined!");
                bOk = false;
            }
            if (false == Directory.Exists(_mHttpSirFromDir))
            {
                CProgram.sLogLine("Http Sirona from directory is not defined!");
                bOk = false;
            }
            if (bOk)
            {
                // create HttpSir directories

                string serverDir = Path.GetFullPath(Path.Combine(_mHttpSirFromDir, ".."));

                string deviceDir;
                string result = "";

                if (bOk)
                {
                    string subDir = Path.Combine(serverDir, "command");

                    if (false == Directory.Exists(subDir) && (null == Directory.CreateDirectory(subDir))) { bOk = false; result += " Http Sirona server downloads dir failed create"; }
                }
                if (bOk)
                {
                    string subDir = Path.Combine(serverDir, "uploads");

                    if (false == Directory.Exists(subDir) && (null == Directory.CreateDirectory(subDir))) { bOk = false; result += ", Http Sirona server Uploads dir failed create"; }
                }
                if (bOk)
                {
                    string subDir = Path.Combine(serverDir, "status");

                    if (false == Directory.Exists(subDir) && (null == Directory.CreateDirectory(subDir))) { bOk = false; result += ",  Http Sirona server downloads dir failed create"; }
                }
                if (bOk)
                {
                    if (false == CDvtmsData.sbCreateDeviceTypeDir(out deviceDir, DDeviceType.Sirona))//"Sirona"))
                    {
                        result += ", create Sirona Device directory failed";
                        bOk = false;
                    }
                }
                if (false == bOk)
                {
                    CProgram.sLogLine(result);
                }
            }
            return bOk;
        }

        private bool mbSironaCreateOneDevice(ref UInt32 ArCount, ref UInt32 ArCreated, string ALogPreText, String AModelTitle,
            ref CDeviceInfo ArDeviceTemplate, string ASerialNr, string ADefaultSettingsFile)
        {
            bool bOk = false;
            string serialNr;
            string deviceDir = "";
            bool bDeviceDirCreated = false;
            bool bDeviceDbCreated = false;
            bool bDeviceDefaultsCopied = false;
            bool bCopyDefaults = ADefaultSettingsFile != null;
            string header = ALogPreText + " " + AModelTitle + " " + ASerialNr + ":  ";
            string result = "";
            int lenStr, pos;

            try
            {
                if (ASerialNr == null || ASerialNr.Length == 0)
                {
                    return true;    // skip line
                }
                pos = ASerialNr.IndexOf('=');
                if (pos > 0)
                {
                    serialNr = ASerialNr.Substring(0, pos).Trim();  // remove '='
                }
                else
                {
                    serialNr = ASerialNr.Trim();
                }

                lenStr = serialNr != null ? serialNr.Length : 0;

                if (lenStr == 0)
                {
                    return true;    // skip line
                }
                if (false == Char.IsLetterOrDigit(serialNr[0]))
                {
                    if (serialNr[0] == '+' && lenStr > 0)
                    {
                        serialNr = serialNr.Substring(1);   // only accept
                    }
                    else
                    {
                        return true;    // skip line
                    }
                }

                bOk = CRecDeviceList.sbSironaIsInLicenseList(serialNr, true);
                if (false == bOk)
                {
                    result += "Not in license list.";
                }
                if (bOk)
                {
                    if (CDvtmsData.sbGetDeviceUnitDir(out deviceDir, DDeviceType.Sirona, serialNr, true))
                    {
                        bOk = true;
                    }
                    else if (false == CDvtmsData.sbCreateDeviceUnitDir(out deviceDir, DDeviceType.Sirona, serialNr))
                    {
                        result += " create Device directory failed";
                    }
                    else
                    {
                        bDeviceDirCreated = true;
                        bOk = true;
                    }
                }
                if (bOk)
                {
                    string subDir = Path.Combine(deviceDir, "ToServer");

                    if (false == Directory.Exists(subDir))
                    {
                        if (null == Directory.CreateDirectory(subDir))
                        {
                            bOk = false;
                            result += " device ToServer dir failed create";
                        }
                        else
                        {
                            bDeviceDirCreated = true;
                        }

                    }

                    if (bOk && bCopyDefaults)
                    {

                        bDeviceDefaultsCopied = mbCopyOneFile(ADefaultSettingsFile, Path.Combine(deviceDir, serialNr + Path.GetExtension(ADefaultSettingsFile)), false);
                        bDeviceDefaultsCopied &= mbCopyOneFile(ADefaultSettingsFile, Path.Combine(subDir, serialNr + Path.GetExtension(ADefaultSettingsFile)), false);
                    }

                    if (bOk)
                    {
                        subDir = Path.Combine(deviceDir, "FromServer");
                        if (false == Directory.Exists(subDir))
                        {
                            if (null == Directory.CreateDirectory(subDir))
                            {
                                bOk = false;
                                result += " device FromServer dir failed create";
                            }
                            else
                            {
                                bDeviceDirCreated = true;
                            }
                        }
                    }
                }
                if (bOk)
                {
                    UInt32 indexKey = 0;

                    if (bDeviceDirCreated)
                    {
                        result += "device dir created, ";
                    }
                    if (bDeviceDefaultsCopied)
                    {
                        result += "defaults copied, ";
                    }
                    bOk = mbCreateDeviceInDB(out bDeviceDbCreated, out indexKey, AModelTitle, ref ArDeviceTemplate, serialNr);

                    if (bOk)
                    {
                        result += "device #" + indexKey.ToString();
                        if (bDeviceDbCreated)
                        {
                            result += " created";
                        }
                    }
                    else
                    {
                        result += "failed device in dBase!";
                    }
                }
                if (bOk)


                {
                    CProgram.sLogLine(header + result);
                    ++ArCount;
                    if (bDeviceDirCreated || bDeviceDbCreated)
                    {
                        ++ArCreated;
                    }
                }
                else
                {
                    CProgram.sLogError(header + result);
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Create " + AModelTitle + " in dBase device list failed for " + ASerialNr, ex);
            }

            return bOk;
        }

        private void mSironaCreateDevices()
        {
            string text = "";
            string modelTitle = "Sirona";
            CDeviceInfo deviceTemplate = null;
            string defaultSettingsFile = null;
            string defaultsSettingsName = "defaults.SirSet";
            string title = "Create " + modelTitle + " devices";
            bool bCopyDefaults = false;

            if (!mbCheckDaysLeft())
            {
                return;
            }
            try
            {
                bool bOk = ReqTextForm.sbReqText("Create " + modelTitle + " devices", "List serial numbers (*=license device file)(no remarks)", ref text);

                if (bOk)
                {
                    /*bOk = do not care if it is not ok in case of Zip only */
                    mbSironaCreateBaseDirs();
                }
                if (bOk)
                {
                    bOk = mbCreateDeviceTemplate(modelTitle, ref deviceTemplate,
                        ref _mSironaTemplateModel, ref _mSironaTemplateFirmware, ref _mbSironaTemplateSetAvailable);
                }
                if (bOk)
                {
                    bOk = CProgram.sbReqBool(title, "Use defaults (No intake check)", ref bCopyDefaults);
                }
                if (bCopyDefaults)
                {
                    string devicePath;

                    bOk = CDvtmsData.sbCreateDeviceTypeDir(out devicePath, DDeviceType.Sirona);

                    defaultSettingsFile = Path.Combine(devicePath, defaultsSettingsName);
                    if (false == File.Exists(defaultSettingsFile))
                    {
                        bOk = false;
                        CProgram.sLogLine("Missing defaults file: " + defaultSettingsFile);
                    }
                    if (false == bOk)
                    {
                        CProgram.sPromptError(true, title, "Missing defaults to copy");
                    }
                }

                if (bOk)
                {
                    text = text.Trim();
                    if (text != null && text.Length > 0)
                    {
                        CRecDeviceList.sDeviceListClear(); // always clear list and reload the new device license file
                        CRecDeviceList.sbSironaDeviceListsCheck(_mHttpSirFromDir, true, false);

                        if (text[0] == '*')
                        {
                            // from device list
                            modelTitle += "*";

                            int n = CRecDeviceList.sGetSironaDeviceCount();
                            if (n > 0)
                            {
                                int i = 0;
                                UInt32 nOk = 0;
                                UInt32 nCreated = 0;

                                if (mbProcessStartLoop("Create Sirona", n, false))
                                {
                                    foreach (string snr in CRecDeviceList.sGetSironaLicenseList())
                                    {
                                        mbProcessNextLoop(++i, false, snr);
                                        if (mbSironaCreateOneDevice(ref nOk, ref nCreated, "[" + i.ToString() + "/" + n.ToString() + "]", modelTitle,
                                            ref deviceTemplate, snr, defaultSettingsFile))
                                        {
                                        }
                                        else if (false == CProgram.sbPromptOkCancel(true, "Create " + modelTitle + " " + n.ToString() + " devices",
                                            i.ToString() + " " + snr + " failed, Continue?"))
                                        {
                                            CProgram.sLogError("Create " + modelTitle + " devices canceled");
                                            break;
                                        }
                                    }
                                    mbProcessEndLoop(i, false, nCreated.ToString() + " Sirona created");
                                }
                                CProgram.sLogLine("Create " + modelTitle + " " + nOk.ToString() + " devices done / " + n.ToString() + " licensed, " + nCreated.ToString() + " created");
                                if (nCreated > 0)
                                {
                                    CProgram.sAskOk("Create " + modelTitle + " devices", nCreated.ToString() + " devices created, finish configuration in Eventboard!");
                                }
                            }
                            else
                            {
                                CProgram.sLogError("Create " + modelTitle + " no devices in license list");
                            }
                        }
                        else
                        {
                            string[] lines = text.Split();
                            int i, n = lines == null ? 0 : lines.Length;
                            UInt32 nOk = 0;
                            UInt32 nCreated = 0;

                            string snr;

                            if (mbProcessStartLoop("Create Sirona", n, false))
                            {
                                for (i = 0; i < n;)
                                {
                                    snr = lines[i];
                                    mbProcessNextLoop(++i, false, snr);
                                    if (mbSironaCreateOneDevice(ref nOk, ref nCreated, "[" + i.ToString() + "/" + n.ToString() + "]", modelTitle,
                                        ref deviceTemplate, snr, defaultSettingsFile))
                                    {

                                    }
                                    else if (false == CProgram.sbPromptOkCancel(true, "Create " + modelTitle + " " + n.ToString() + " devices",
                                        i.ToString() + " " + snr + " failed, Continue?"))
                                    {
                                        CProgram.sLogError("Create " + modelTitle + " devices canceled");
                                        break;
                                    }
                                }
                                mbProcessEndLoop(i, false, nCreated.ToString() + " Sirona created");
                            }
                            CProgram.sLogLine("Create " + modelTitle + " " + nOk.ToString() + " devices done,  / " + n.ToString() + " lines, " + nCreated.ToString() + " created");
                            if (nCreated > 0)
                            {
                                CProgram.sAskOk("Create " + modelTitle + " devices", nCreated.ToString() + " devices created, finish configuration in Eventboard!");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Create Sirona device failed ", ex);
            }

        }

        private void toolStripAddSirona_Click(object sender, EventArgs e)
        {
            mButtonAddSirona();
        }
        private void mButtonAddSirona()
        {
            if (!mbCheckDaysLeft())
            {
                return;
            }
            if (_mbRunningConversion || _mbRunLoop)
            {
                CProgram.sLogLine("File conversion already running, press stop first!");
            }
            else
            {
                mSironaCreateDevices();
            }
        }
        private void mSironaCreateOneDeviceOld()
        {
            if (!mbCheckDaysLeft())
            {
                return;
            }
            if (_mbRunningConversion || _mbRunLoop)
            {
                CProgram.sLogLine("File conversion already running, press stop first!");
            }
            else if (_mHttpSirFromDir == null || _mHttpSirFromDir.Length <= 2)
            {
                CProgram.sLogLine("Http Sirona from directory is not defined!");
            }
            else if (false == Directory.Exists(_mHttpSirFromDir))
            {
                CProgram.sLogLine("Http Sirona from directory is not defined!");
            }
            else
            {
                string name = "";
                string result = "";
                bool bOk = false;

                try
                {
                    if (CProgram.sbReqLabel("Add Http Sirona device", "Serial NR", ref name, "", false) && name.Length > 0)
                    {
                        // create HttpSir directories

                        string serverDir = Path.GetFullPath(Path.Combine(_mHttpSirFromDir, ".."));
                        //                        string serverDir = _mHttpSirFromDir; //  Path.Combine(mHttpSirFromDir, name);
                        string deviceDir;

                        if (Directory.Exists(serverDir)) { result = "Http Sirona server dir exists"; bOk = true; }
                        //                        else if (null != Directory.CreateDirectory(serverDir)) { result = " server dir created"; bOk = true; }
                        //                       else { result = "TZ server dir failed create"; }
                        bOk = true;

                        if (bOk)
                        {
                            string subDir = Path.Combine(serverDir, "command");

                            if (false == Directory.Exists(subDir) && (null == Directory.CreateDirectory(subDir))) { bOk = false; result += ", Http Sirona server downloads dir failed create"; }
                        }
                        if (bOk)
                        {
                            string subDir = Path.Combine(serverDir, "uploads");

                            if (false == Directory.Exists(subDir) && (null == Directory.CreateDirectory(subDir))) { bOk = false; result += ", Http Sirona server Uploads dir failed create"; }
                        }
                        if (bOk)
                        {
                            string subDir = Path.Combine(serverDir, "status");

                            if (false == Directory.Exists(subDir) && (null == Directory.CreateDirectory(subDir))) { bOk = false; result += ",  Http Sirona server downloads dir failed create"; }
                        }
                        if (bOk)
                        {
                            if (false == CDvtmsData.sbCreateDeviceTypeDir(out deviceDir, DDeviceType.Sirona))//"Sirona"))
                            {
                                result += ", create Sirona Device directory failed";
                                bOk = false;
                            }
                            else
                            {
                                serverDir = Path.Combine(deviceDir, name);
                                if (Directory.Exists(serverDir)) { result += ", Sirona device dir exists"; }
                                else if (null != Directory.CreateDirectory(serverDir)) { result += ", Sirona device dir created"; }
                                else { result += "Sirona device dir failed create"; bOk = false; }

                                if (bOk)
                                {
                                    string subDir = Path.Combine(serverDir, "ToServer");

                                    if (false == Directory.Exists(subDir) && (null == Directory.CreateDirectory(subDir))) { bOk = false; result += ", Sirona device ToServer dir failed create"; }
                                }
                                if (bOk)
                                {
                                    string subDir = Path.Combine(serverDir, "FromServer");

                                    if (false == Directory.Exists(subDir) && (null == Directory.CreateDirectory(subDir))) { bOk = false; result += ", Sirona device FromServer dir failed create"; }
                                }
                            }
                        }
                    }
                    CProgram.sLogLine("Sirona " + name + ": " + result);
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Create Sirona " + name + " failed:" + result, ex);
                }
            }
        }

        private void contextMenuHelp_Opening(object sender, CancelEventArgs e)
        {

        }

        private void buttonLogToClipboard_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(logBox.Text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // mail
        }

        private void buttonSendMail_Click(object sender, EventArgs e)
        {
            // send test mail

            List<string> fileList = new List<string>();

            if (fileList != null)
            {
                fileList.Add("license.png");
            }
            mbMailSendOne("Test", "Test EventMail " + DateTime.Now.ToString("HH:mm:ss"), "Test body", fileList, false);
        }

        private void createSQLEncryptedPasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //string _mTestSqlUser = "", _mTestSqlServer = "", _mTestSmtpUser = "", _mTestStmpServer = "";    // used for encrypt password

            try
            {
                string center = CLicKeyDev.sGetOrgLabel();
                string plainText = "";

                if (CProgram.sbReqLabel("Create SQL Password", "SQL Server", ref _mTestSqlServer, "", false)
                    && CProgram.sbReqLabel("Create SQL Password", "User", ref _mTestSqlUser, "", false)
                    && CProgram.sbReqLabel("Create SQL Password", "Password", ref plainText, "", false))
                {
                    string encrypted, decrypted;
                    CPasswordString pw = new CPasswordString(_mTestSqlUser.Trim() + "@" + _mTestSqlServer.Trim() + ":sqlPW", center);

                    bool b = pw.mbEncrypt(plainText);

                    encrypted = pw.mGetEncrypted();
                    if (b == false)
                    {
                        CProgram.sLogError("Encrypt SQL PW item server= " + _mTestSqlServer + ", user= " + _mTestSqlUser + " failed encrypt");
                    }
                    else
                    {
                        decrypted = pw.mDecrypt();

                        if (plainText == decrypted)
                        {
                            CProgram.sLogLine("Encrypt SQL PW item server= " + _mTestSqlServer + ", user= " + _mTestSqlUser + " successful copied to Clipboard.");
                            Clipboard.SetText(encrypted);
                            CProgram.sAskOk("Encrypt SQL password", "Encrypt password is on Clipboard, press ok to clear Clipboard");
                            Clipboard.Clear();
                        }
                        else
                        {
                            CProgram.sLogError("Encrypt SQL PW item server= " + _mTestSqlServer + ", user= " + _mTestSqlUser + " failed reverse");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Encrypt SQL PW", ex);
            }

        }

        private void createSmtpEncryptedPasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //string _mTestSqlUser = "", _mTestSqlServer = "", _mTestSmtpUser = "", _mTestStmpServer = "";    // used for encrypt password

            try
            {
                string center = CLicKeyDev.sGetOrgLabel();
                string plainText = "";

                if (CProgram.sbReqLabel("Create SMTP Password", "smtp Server", ref _mTestStmpServer, "", false)
                    && CProgram.sbReqLabel("Create SMTP Password", "User", ref _mTestSmtpUser, "", false)
                    && CProgram.sbReqLabel("Create SMTP Password", "Password", ref plainText, "", false))
                {
                    string encrypted, decrypted;
                    CPasswordString pw = new CPasswordString(_mTestSmtpUser.Trim() + "@" + _mTestStmpServer.Trim() + ":smtpPW", center);

                    bool b = pw.mbEncrypt(plainText);

                    encrypted = pw.mGetEncrypted();
                    if (b == false)
                    {
                        CProgram.sLogError("Encrypt SMTP PW item server= " + _mTestStmpServer.Trim() + ", user= " + _mTestSmtpUser.Trim() + " failed encrypt");
                    }
                    else
                    {
                        decrypted = pw.mDecrypt();

                        if (plainText == decrypted)
                        {
                            CProgram.sLogLine("Encrypt SMTP PW item server= " + _mTestStmpServer.Trim() + ", user= " + _mTestSmtpUser.Trim() + " successful copied to Clipboard.");
                            Clipboard.SetText(encrypted);
                            CProgram.sAskOk("Encrypt SMTP password", "Encrypt password is on Clipboard, press ok to clear Clipboard");
                            Clipboard.Clear();
                        }
                        else
                        {
                            CProgram.sLogError("Encrypt SMTP PW item server= " + _mTestStmpServer.Trim() + ", user= " + _mTestSmtpUser.Trim() + " failed reverse");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Encrypt SMTP PW", ex);
            }

        }

        private void extraLoggingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _mbLogExtra = CProgram.sbAskYesNo("Debug Extra logging = " + (_mbLogExtra ? "On" : "Off"), "Enable Extra Logging");

            CProgram.sLogLine("Debug Extra logging = " + (_mbLogExtra ? "On" : "Off"));
        }

        private void timerProcessMail_Tick(object sender, EventArgs e)
        {
            mDoProcessSmtpCue();
            if (_mMailProcessCounter < 4)
            {
                CProgram.sLogLine("Mail process cue " + (checkBoxMailEnable.Checked ? "running" : "off"));
            }
        }

        private void buttonKillSmtpCue_Click(object sender, EventArgs e)
        {
            UInt32 n = CSendMail.sKillMailCue();

            mDoProcessSmtpCue();
        }

        public bool mbMailSetup(string ACenter)
        {
            bool bRun = false;

            try
            {
                _mbMailEventReceivedEnable = Properties.Settings.Default.MailEventReceivedEnable; // get values from settings file

                if (_mbMailEventReceivedEnable)
                {
                    _mbMailEventReceivedPict = Properties.Settings.Default.MailEventReceivedPict;
                    // get values from settings file
                    _mMailEventReceivedFrom = Properties.Settings.Default.MailEventReceivedFrom;
                    _mMailEventReceivedTo = Properties.Settings.Default.MailEventReceivedTo;
                    _mMailEventReceivedSubject = Properties.Settings.Default.MailEventReceivedSubject;
                    _mMailEventReceivedBody = Properties.Settings.Default.MailEventReceivedBody;

                    if (_mMailEventReceivedTo == null || _mMailEventReceivedTo.IndexOf('@') < 0 || _mMailEventReceivedFrom == null || _mMailEventReceivedFrom.IndexOf('@') < 0)
                    {
                        CProgram.sLogLine("Invalid mail adres");
                    }
                    else
                    {
                        string mailSmtpServer = Properties.Settings.Default.mailSmtpServer;
                        UInt16 mailSmtpPort = Properties.Settings.Default.mailSmtpPort;
                        bool bMailSmtpSsl = Properties.Settings.Default.mailSmtpSsl;
                        string mailSmtpUser = Properties.Settings.Default.mailSmtpUser;
                        string mailSmtpPassword = Properties.Settings.Default.mailSmtpPassword;
                        UInt32 mailSmtpMinTimeSec = Properties.Settings.Default.mailSmtpMinTimeSec;
                        UInt16 mailSmtpNrThreads = Properties.Settings.Default.mailSmtpNrThreads;

                        if (CLicKeyDev.sbDeviceIsProgrammer())
                        {
                            if (mailSmtpUser == null || mailSmtpUser.Length == 0)
                            {
                                mailSmtpUser = "s.vlaar.tm@gmail.com";
                            }
                            if (mailSmtpPassword == null || mailSmtpPassword.Length == 0)
                            {
                                if (ACenter == "NL0001Soft")
                                {
                                    mailSmtpPassword = "32271E17-$6F0CF13924BDBF54D5BC09D67B6D4BE6";
                                }
                                else if (ACenter == "NL0022Demo")
                                {
                                    mailSmtpPassword = "3229AE92-$E240AF3E1A98DC9B243A698ACBD3F279";
                                }
                            }
                        }
                        // 
                        // 

                        CPasswordString smtpPW = null;
                        if (mailSmtpPassword != null && mailSmtpPassword.Length > 0)
                        {
                            string name = mailSmtpUser.Trim() + "@" + mailSmtpServer.Trim() + ":smtpPW";

                            smtpPW = new CPasswordString(name, ACenter);
                            smtpPW.mbSetEncrypted(mailSmtpPassword);

                            if (CLicKeyDev.sbDeviceIsProgrammer())
                            {
                                string pw = smtpPW.mDecrypt();

                                if (false == smtpPW.mbDecrypt(out pw, false))
                                {
                                    CProgram.sLogError("smtp password decrypt error");
                                }

                            }
                        }
                        bRun = CSendMail.sbSetSmtpServer(mailSmtpServer, mailSmtpPort, bMailSmtpSsl, mailSmtpUser, smtpPW, mailSmtpMinTimeSec, mailSmtpNrThreads);
                        if (false == bRun)
                        {
                            CProgram.sLogLine("Failed setup smtp client!");
                        }
                        else
                        {
                            CProgram.sLogLine("Setup smtp server:" + mailSmtpServer);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Setup Mail", ex);
            }
            return bRun;
            // if( mbMailSetup( string ACenter )) start timer for processing mail cue
        }

        private void labelMailState1_Click(object sender, EventArgs e)
        {
            CSendMail.sLogMailCue();
        }

        private void labelMailState2_Click(object sender, EventArgs e)
        {
            CSendMail.sLogMailCue();
        }

        private void toolStripButtonTzRB_Click(object sender, EventArgs e)
        {
            mEditTzRB();
        }
        private void toolStripButtonTzSkip_Click(object sender, EventArgs e)
        {
            mEditTzSkip();
        }

        private void mUpdateBlackList()
        {
            UInt32 n = CRecDeviceList.sGetBlackListCount();

            toolStripButtonBL.Text = n.ToString() + "\r\nBL";
        }

        private void toolStripButtonBL_Click(object sender, EventArgs e)
        {
            UInt32 n = CRecDeviceList.sEditBlackList();

            if (n > 0)
            {
                CProgram.sLogLine(n.ToString() + " Blacklisted, old files can be deleted"
                    + (_mMaxFileAgeHour > 0 ? " after " + _mMaxFileAgeHour.ToString() + "hours!" : "!"));
            }
            mUpdateBlackList();
        }

        private void toolStripAddDevice_Click(object sender, EventArgs e)
        {
            contextMenuAddDevice.Show(Control.MousePosition);
        }

        private void toolStripImport_Click(object sender, EventArgs e)
        {
            contextMenuImport.Show(Control.MousePosition);
        }

        private void importSironaZipToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mButtonLoadOneZip();
        }

        private void importSironaHeaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mButtonLoadHea();
        }

        private void importTZEventToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mButtonLoadTZe();
        }

        private void importTZScpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mButtonImportTzSCP();
        }

        private void importDV2ScpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mButtonImportDV2Scp();
        }

        private void importDVX1EcgToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mSelectImportDvxHeaDat();
        }

        private void mSelectImportDvxHeaDat()
        {
            if (!mbCheckDaysLeft())
            {
                return;
            }
            if (_mbRunningConversion || _mbRunLoop)
            {
                CProgram.sLogLine("File conversion already running, press stop first!");
            }
            else
            {
                mSetRun(false);

                //openHeaFileDialog.RestoreDirectory = true;
                openHeaFileDialog.Title = "Import MIT HEA File";

                openFileDialogDat.InitialDirectory = _mDVXImportDir;  // no DAT
                if (false == mbIsRunNamePresent())
                {
                    openFileDialogDat.FileName = _mDVXImportDir;
                }
                else
                {
                    openFileDialogDat.FileName = Path.Combine(_mDVXImportDir, mGetRunFileName());
                }

                if (openFileDialogDat.ShowDialog() == DialogResult.OK)
                {
                    int nFiles = openFileDialogDat.FileNames == null ? 0 : openHeaFileDialog.FileNames.Length;
                    bool bMerge = nFiles > 1;
                    if (bMerge)
                    {
                        if (false == CProgram.sbReqBool("Import DVX DAT", "Merge DAT files (continuous same sps and nr signals)", ref bMerge))
                        {
                            nFiles = 0;
                        }
                    }

                    if (nFiles > 0)
                    {
                        if (bMerge)
                        {
                            _mbRunningConversion = true;
                            CProgram.sSetBusy(true);
                            string tempFileHea, tempFileDat;

                            if (mMergeDvxDatFiles(out tempFileHea, out tempFileDat, openFileDialogDat.FileNames, true, 0))
                            {
                                CProgram.sLogLine("DVX Import file " + tempFileHea);
                                mImportHeaFile(tempFileHea, DImportMethod.Manual, false);
                                if (File.Exists(tempFileHea))
                                {
                                    File.Delete(tempFileHea);
                                }
                                if (File.Exists(tempFileDat))
                                {
                                    File.Delete(tempFileDat);
                                }

                            }

                            CProgram.sSetBusy(false);
                            _mbRunningConversion = false;
                        }
                        else
                        {
                            _mbRunningConversion = true;
                            CProgram.sSetBusy(true);
                            string tempFileHea, tempFileDat;
                            UInt32 i = 0;

                            foreach (String file in openFileDialogDat.FileNames)
                            {
                                CProgram.sLogLine("DVX Import file " + file);

                                if (mMergeDvxDatFiles(out tempFileHea, out tempFileDat, openHeaFileDialog.FileNames, false, i))
                                {
                                    CProgram.sLogLine(i.ToString() + ": DVX Import file " + tempFileHea);
                                    mImportHeaFile(tempFileHea, DImportMethod.Manual, false);
                                    if (File.Exists(tempFileHea))
                                    {
                                        File.Delete(tempFileHea);
                                    }
                                    if (File.Exists(tempFileDat))
                                    {
                                        File.Delete(tempFileDat);
                                    }
                                }
                                ++i;

                            }

                            CProgram.sSetBusy(false);
                            _mbRunningConversion = false;
                        }
                    }
                }
            }
        }

        private bool mMergeDvxDatFiles(out string AFileMergedHea, out string AFileMergedDat, string[] AFileList, bool AbMerge, UInt32 AFileIndex)
        {
            bool bOk = false;

            string tempFileHea = "";
            string tempFileDat = "";
#if jhjkgds
            UInt32 iStart = AFileIndex;
            Int32 nFiles = AFileList == null ? 0 : AFileList.Length;
            string title;

            if (nFiles > 0)
            {
                try
                {
                    string[] files = AFileList;
                    string tempName = "merge" + CProgram.sDateTimeToYMDHMS(DateTime.Now);

                    if (AbMerge)
                    {
                        title = "";
                        iStart = 0;
                        Array.Sort(files);
                    }
                    else
                    {
                        nFiles = 1;
                    }
                    !!!

                    string firstPathDat = files[0];
                    string fileName = Path.GetFileNameWithoutExtension(firstPathDat);
                    string firstPathHea = Path.ChangeExtension(firstPathDat, ".hea" );



                    tempFileDat = Path.Combine(CProgram.sGetProgDir(), tempName + "_" + fileName + ".dat");
                    tempFileHea = Path.Combine(CProgram.sGetProgDir(), tempName + ".hea");

                    if (File.Exists(tempFileHea))
                    {
                        File.Delete(tempFileHea);
                    }
                    if (File.Exists(tempFileDat))
                    {
                        File.Delete(tempFileDat);
                    }
                    string lines = File.ReadAllText(files[0]);

                    foreach (string file in files)
                    {
                        lines += "#file=" + file + "\r\n";
                    }
                    File.WriteAllText(tempFileHea, lines);

                    using (FileStream writerDat = File.Create(tempFileDat))
                    {
                        foreach (string file in files)
                        {
                            string fileDat = Path.ChangeExtension(file, ".dat");
                            byte[] data = File.ReadAllBytes(fileDat);
                            writerDat.Write(data, 0, data.Length);
                            CProgram.sLogLine("Merging Hea:" + file);
                        }
                        writerDat.Flush();
                        writerDat.Close();
                        bOk = true;
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("DVX Merge Dat", ex);
                }
            }
#endif
            AFileMergedHea = tempFileHea;
            AFileMergedDat = tempFileDat;
            return bOk;

        }




        private void addSironaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mButtonAddSirona();
        }

        private void addTZDeviceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mButtonAddTZ();
        }

        private void contextMenuImport_Opening(object sender, CancelEventArgs e)
        {

        }

        public bool mbMailSendOne(string ALabel, string ATitle, string ABody, List<string> AFileList, bool AbStartDirectly)
        {
            bool bOk = false;

            if (_mbMailEventReceivedEnable)
            {
                try
                {
                    if (CSendMail.sbCheckSmtpClient())
                    {
                        string mailTitle = ATitle == null || ATitle.Length == 0
                            ? "Test" + "( " + DateTime.Now.ToString("HH:mm:ss") + ")" : ATitle;
                        string mailBody = ABody + "\r\n\r\n<<<< " + CProgram.sGetProgNameVersionNow() + " >>>>>>>>";

                        MailMessage message = new MailMessage(new MailAddress(_mMailEventReceivedFrom, ""), new MailAddress(_mMailEventReceivedTo, ""));

                        message.Sender = new MailAddress(_mMailEventReceivedFrom, "");
                        message.Subject = mailTitle;
                        message.Body = mailBody;
                        message.IsBodyHtml = false;

                        if (AFileList != null && _mbMailEventReceivedPict)
                        {
                            foreach (string fileName in AFileList)
                            {
                                Attachment attachment = new Attachment(fileName);
                                message.Attachments.Add(attachment);
                            }
                        }
                        if (false == CSendMail.sbStartSendMail(ALabel, message, AbStartDirectly, 0, false))
                        {
                            CProgram.sLogLine("Failed send mail!" + ALabel);
                        }
                        else
                        {
                            bOk = true;
                        }

                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Send Mail: " + ALabel, ex);
                }

            }
            return bOk;
        }
        private void mDoProcessSmtpCue()
        {
            UInt32 cueN = 0, sendN = 0, clearN = 0;
            string s1 = "";
            string s2 = "";
            UInt32 charIndex = _mMailProcessCounter++ % _cMailNrLoopChars;
            char c = _cMailLoopChars[charIndex];

            if (CSendMail.sbProcessMailCue(out cueN, out sendN, out clearN))
            {
                s1 = c + "  que= " + cueN.ToString()
                + ", Error= " + CSendMail._sMailSendErrorCounter.ToString();
                s2 = "Sending= " + sendN.ToString()
                + ", OK= " + CSendMail._sMailSendOkCounter.ToString();
            }
            else
            {
                s1 = c + "  que= " + (cueN == 0 ? "-" : cueN.ToString())
                + ", Error= " + CSendMail._sMailSendErrorCounter.ToString();
                s2 = "Sending= " + (sendN == 0 ? "-" : sendN.ToString())
                + ", OK= " + CSendMail._sMailSendOkCounter.ToString();
            }
            labelMailState1.Text = s1;
            labelMailState2.Text = s2;
        }

        private void reversBytesDatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mButtonDvxRevHea();
        }

        private void listSironaLicenceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mListSironaLicense();
        }
        private void mListSironaLicense()
        {
            List<string> listLic = CRecDeviceList.sGetSironaLicenseList();
            int nLicense = listLic == null ? 0 : listLic.Count;
            int nDevice = 0;
            string snrs = "";


            if (nLicense > 0)
            {
                foreach (string s in listLic)
                {
                    if (CRecDeviceList.sbSironaIsInLicenseList(s, false))
                    {
                        snrs += "+" + s;
                        ++nDevice;
                    }
                    else
                    {
                        snrs += "-" + s;
                    }
                }
                CProgram.sLogLine(snrs);
            }
            CProgram.sLogLine("Sirona: " + nDevice.ToString() + " active / " + nLicense.ToString() + " in device license");
        }

        private void listTZLicenseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mListTzLicense();
        }
        private void mListTzLicense()
        {
            List<string> listLic = CRecDeviceList.sGetTzLicenseList();
            int nLicense = listLic == null ? 0 : listLic.Count;
            int nDevice = 0;
            string snrs = "";

            if (nLicense > 0)
            {
                foreach (string s in listLic)
                {
                    if (CRecDeviceList.sbTzSnrIsInDeviceList(s, false))
                    {
                        snrs += "+" + s;
                        ++nDevice;
                    }
                    else
                    {
                        snrs += "-" + s;
                    }
                }
                CProgram.sLogLine(snrs);
            }
            CProgram.sLogLine("Tz: " + nDevice.ToString() + " active / " + nLicense.ToString() + " in device license");
        }

        private void reloadDeviceLicensesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CRecDeviceList.sDeviceListClear();
            CRecDeviceList.sbAllDeviceListsCheck(true, false);
        }

        private void addLogLineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string s = "";

            if (ReqTextForm.sbReqText("Add Log Line", "Log text", ref s))
            {
                CProgram.sLogLine(s);
            }
        }

        private void newLogFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string reason = "";

            if (CProgram.sbReqLabel("Create New Log File", "Reason", ref reason, "", false))
            {
                CProgram.sCreateNewLogFile(reason);
            }
        }

        private bool mbSendReceivedMail(string AFilePath, CRecordMit ARec)
        {
            bool bSend = false;
            string label = "";

            if (checkBoxMailEnable.Checked)
            {
                try
                {
                    string id = CRecordMit.sGetPrintStudyRecord(ARec);
                    label = "Event " + id;

                    // send test mail
                    string recPath = Path.GetDirectoryName(AFilePath);
                    string[] pngFiles = Directory.GetFiles(recPath, "*.png");

                    List<string> fileList = null;
                    if (pngFiles != null && pngFiles.Length > 0)
                    {
                        fileList = new List<string>();
                        foreach (string fileName in pngFiles)
                        {
                            if (fileName.EndsWith(".png"))
                            {
                                fileList.Add(fileName);
                            }
                        }
                    }

                    string eventDT = ARec.mGetDeviceTimeString(ARec.mGetEventUTC(), DTimeZoneShown.Long);

                    //                    string title = "Event " + id + " " + ARec.mDeviceID + ": " + ARec.mEventTypeString
                    //                      + " " + eventDT;
                    string title = ARec.mEventTypeString + " " + ARec.mDeviceID + " " + eventDT + " EventMail " + id;
                    string body = "Attached are the preview images belonging to the event:\r\n"
                        + title + "\r\n\r\n"
                        ;

                    bSend = mbMailSendOne(label, title, body, fileList, false);
                    if (bSend)
                    {
                        CProgram.sLogLine("Mail created: " + ARec.mDeviceID + "->" + label);
                    }
                    else
                    {
                        CProgram.sLogError("Failed create mail " + ARec.mDeviceID + "->" + label);
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Send Event Mail: " + label, ex);
                }
            }
            return bSend;
        }

        private void checkDBCollectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mCheckDbCollect();
        }

        private void sironaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string license;
            UInt32 n = CRecDeviceList.sGetActiveSironaDeviceList(out license, "+");

            CProgram.sLogLine(n.ToString() + " active Sirona= " + license);
        }

        private void tZToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string license;
            UInt32 n = CRecDeviceList.sGetActiveTzDeviceList(out license, "+");

            CProgram.sLogLine(n.ToString() + " active TZ= " + license);

        }

        private void dV2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string license;
            UInt32 n = CRecDeviceList.sGetActiveDV2DeviceList(out license, "+");

            CProgram.sLogLine(n.ToString() + " active DV2= " + license);
        }

        private void FormIReader_FormClosed(object sender, FormClosedEventArgs e)
        {
            CProgram.sLogLine("Main form is Closed");
            if (_mNotifyIcon != null)
            {
                _mNotifyIcon.Icon = _mIconStopped;
                _mNotifyIcon.Text = "!" + _mNotifyHeader + " Exit!";
                _mNotifyIcon.Visible = false;
                _mNotifyIcon.Dispose();
                _mNotifyIcon = null;
            }
        }

        private void FormIReader_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (CProgram.sbHandleFormClosing(mbInitialized, e) && _mNotifyIcon != null)
            {
                _mNotifyIcon.Icon = _mIconStopped;
                _mNotifyIcon.Text = "!" + _mNotifyHeader + " Exit!";
                _mNotifyIcon.Visible = false;
                _mNotifyIcon.Dispose();
                _mNotifyIcon = null;
            }
        }

        private void createTestFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string baseDir = CProgram.sGetProgDir();
            string testDir = "TestFiles";
            string result;

            CProgram.sbTestFilesWrite(out result, baseDir, testDir, 10000, 1000000);

            //CProgram.sLogLine(result);
        }

        private void countTestFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string baseDir = CProgram.sGetProgDir();
            string testDir = "TestFiles";
            string result;

            CProgram.sbTestFilesCount(out result, baseDir, testDir);

            //            CProgram.sLogLine(result);
        }

        private void readTestFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string baseDir = CProgram.sGetProgDir();
            string testDir = "TestFiles";
            string result;

            CProgram.sbTestFilesRead(out result, baseDir, testDir);

            //            CProgram.sLogLine(result);
        }

        private void deleteTestFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string baseDir = CProgram.sGetProgDir();
            string testDir = "TestFiles";
            string result;

            CProgram.sbTestFilesDelete(out result, baseDir, testDir);

            //            CProgram.sLogLine(result);

        }

        private void recordingCreateTestFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string baseDir = _mRecordingDir;
            string testDir = "TestFiles";
            string result;

            CProgram.sbTestFilesWrite(out result, baseDir, testDir, 10000, 1000000);

            //CProgram.sLogLine(result);

        }

        private void recordingCountTestFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string baseDir = _mRecordingDir;
            string testDir = "TestFiles";
            string result;

            CProgram.sbTestFilesCount(out result, baseDir, testDir);

            //            CProgram.sLogLine(result);

        }

        private void recordingTestFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string baseDir = _mRecordingDir;
            string testDir = "TestFiles";
            string result;

            CProgram.sbTestFilesRead(out result, baseDir, testDir);

            //            CProgram.sLogLine(result);

        }

        private void folderCreateTestFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == folderBrowserDialogTest.ShowDialog())
            {
                string baseDir = folderBrowserDialogTest.SelectedPath;
                string testDir = "TestFiles";
                string result;

                CProgram.sbTestFilesWrite(out result, baseDir, testDir, 10000, 1000000);

            }
        }

        private void folderCountTestFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == folderBrowserDialogTest.ShowDialog())
            {
                string baseDir = folderBrowserDialogTest.SelectedPath;
                string testDir = "TestFiles";
                string result;

                CProgram.sbTestFilesCount(out result, baseDir, testDir);

            }

        }

        private void folderReadTestFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == folderBrowserDialogTest.ShowDialog())
            {
                string baseDir = folderBrowserDialogTest.SelectedPath;
                string testDir = "TestFiles";
                string result;

                CProgram.sbTestFilesRead(out result, baseDir, testDir);
            }
        }

        private void folderDeleteTestFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == folderBrowserDialogTest.ShowDialog())
            {
                string baseDir = folderBrowserDialogTest.SelectedPath;
                string testDir = "TestFiles";
                string result;

                CProgram.sbTestFilesDelete(out result, baseDir, testDir);
            }

        }

        private void programInfoToClipboardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string s = Text + " _ " + CProgram.sGetUserAtPcString();

                if (_mRecordingDir != null && _mRecordingDir.Length > 2)
                {
                    s += " _ " + _mRecordingDir;
                }
                Clipboard.SetText(s);
            }
            catch (Exception)
            {
            }
        }

        private void recordingDeleteTestFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string baseDir = _mRecordingDir;
            string testDir = "TestFiles";
            string result;

            CProgram.sbTestFilesDelete(out result, baseDir, testDir);
        }

        private void openLogFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CProgram.sDoOpenLogFolder();
        }

        private void openLogFolderToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            CProgram.sDoOpenLogFolder();
        }

        private void openLogFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CProgram.sDoOpenLogFile();
        }

        private void openRecodingFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CProgram.sDoOpenFolder(_mRecordingDir);
        }

        private void toolStripButtonWarn_Click(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;
            if (bCtrl)
            {
                toolStripButtonWarn.Text = "c";
                string showText = toolStripButtonWarn.ToolTipText + " @c" + CProgram.sTimeToString(DateTime.Now);
                CProgram.sLogLine(showText);
                toolStripButtonWarn.ToolTipText = showText;
            }
            else
            {
                mCheckDiskMemSrvr(true);
            }
        }

        private void toolStripTzAutoCollect_Click(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;
            if (bCtrl)
            {
                if (_mbRun)
                {
                    _mLastAutoCollectDtTz = DateTime.MinValue;
                    if (_mbTzAutoCollectActive)
                    {
                        CProgram.sLogLine("TZ Auto Collect run on next scan loop.");
                    }
                    else
                    {
                        CProgram.sLogLine("TZ Auto Collect is off!");
                    }
                }
                else
                {
                    string serialNr = "";
                    if (CProgram.sbReqLabel("TZ Auto collect SCP+TZR files", "Serial number device", ref serialNr, "", false))
                    {
                        DateTime startDT = DateTime.Now;
                        string acResult = "";
                        bool bScanOk = false;
                        UInt32 nrReqFiles = 0;
                        bool bAutomatic = false;

                        if (CProgram.sbReqBool("TZ Auto collect SCP + TZR for " + serialNr, "Run Automatic", ref bAutomatic))
                        {
                            CProgram.sLogLine("TZ Auto Collect rune once for snr: " + serialNr);

                            if (mbTzAutoCollectScan(out bScanOk, out nrReqFiles, serialNr, bAutomatic, true))
                            {
                                acResult = "+ AutoCollect(";
                                if (false == bScanOk)
                                {
                                    acResult += "Failed ";
                                }
                                acResult += nrReqFiles.ToString() + ") ";
                            }
                            double tSec = (DateTime.Now - startDT).TotalSeconds;

                            CProgram.sLogLine("TZ Auto Collect rune once for snr: " + serialNr + " " + acResult + " in " + tSec.ToString("0.000") + "sec");
                        }
                    }
                }
            }
            else
            {
                mEditTzAutoCollect();
            }
        }

        static string _sParseDouble = "";
        private void parseDoubleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            while (CProgram.sbReqLabel("Test Parse Double", "double text", ref _sParseDouble, "", false))
            {
                double d = 0;

                if (double.TryParse(_sParseDouble, out d))
                {
                    CProgram.sLogLine("Parse double(" + _sParseDouble + ")=" + d.ToString());
                }
                else
                {
                    CProgram.sLogLine("Failed Parse double(" + _sParseDouble + ")!");
                }
            }
        }

        private void progParseDoubleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            while (CProgram.sbReqLabel("Test Program Parse Double", "double text", ref _sParseDouble, "", false))
            {
                double d = 0;

                if (CProgram.sbParseDouble(_sParseDouble, ref d))
                {
                    CProgram.sLogLine("Prog Parse double(" + _sParseDouble + ")=" + d.ToString());
                }
                else
                {
                    CProgram.sLogLine("Failed Prog Parse double(" + _sParseDouble + ")!");
                }
            }
        }

        static string _sParseDtFormat = "H:m:s d/M/yyyy";
        static string _sParseDtText = "";
        private void parseExactDateTimeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            while (CProgram.sbReqLabel("Test Parse Date Time", "DT format", ref _sParseDtFormat, "", false)
                && CProgram.sbReqLabel("Test Parse Date Time", "DT(" + _sParseDtFormat + " text", ref _sParseDtText, "", false)
                )
            {
                DateTime dt;

                if (DateTime.TryParseExact(_sParseDtText, _sParseDtFormat, CultureInfo.InvariantCulture, DateTimeStyles.AllowWhiteSpaces, out dt))
                {
                    CProgram.sLogLine("Parse DT(" + _sParseDtFormat + ")(" + _sParseDtText + ")=" + dt.ToString("yyyy/MM/dd HH:mm:ss"));
                }
                else
                {
                    CProgram.sLogLine("Failed Parse double(" + _sParseDouble + ")!");
                }
            }
        }

        private void enableLogFlushToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool bFlush = CProgram.sbGetProgramLogFlush();

            if (CProgram.sbReqBool("Set Program Log Flush", "Flush each log line", ref bFlush))
            {
                CProgram.sSetProgramLogFlush(bFlush);
                CProgram.sLogLine("ProgLogFlush " + (bFlush ? "Enabled" : "Disabled!"));
            }
        }

        private void labelUpdTimer_Click(object sender, EventArgs e)
        {
        }

        private void labelUpdTimer_DoubleClick(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl)
            {
                Int32 i = timer.Interval;
                if (CProgram.sbReqInt32("Main update Timer", "Interval", ref i, "mSec", 50, 2000))
                {
                    timer.Interval = i;
                    labelUpdTimer.Text = "Upd. Timer= " + i.ToString() + " mSec";
                }
            }
        }

        private void labelListInfo_DoubleClick(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;

            if (bCtrl)
            {
                UInt32 i = _mMaxNrRowsListed;
                if (CProgram.sbReqUInt32("Set number of rows listed", "maximum", ref i, "rows", 10, 999999))
                {
                    _mMaxNrRowsListed = i;
                    labelListInfo.Text = "- rows < " + i.ToString();
                }
            }
        }

        private void labelAmplRange_Click(object sender, EventArgs e)
        {
            mSetAmplRange();
        }

        private void labelTzHrFilter_Click(object sender, EventArgs e)
        {
            mSetTzHrFilter();
        }

        private void makeLauncherVersionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CProgram.sbWriteLaunchVersion(true, versionLabel, versionHeader, versionText);
        }

        private void helloToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //xx
        }

        private void openContentFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string dir = Path.Combine(CProgram.sGetProgDir(), "Content");
            CProgram.sDoOpenFolder(dir);
        }

        private void toolStripButtonSirAC_Click(object sender, EventArgs e)
        {
            bool bCtrl = (Control.ModifierKeys & Keys.Control) == Keys.Control;
            if (bCtrl)
            {
                if (_mbRun)
                {

                    _mLastAutoCollectDtSir = DateTime.MinValue;
                    if (_mbSirAutoCollectActive)
                    {
                        CProgram.sLogLine("Sirona Auto Collect run on next scan loop.");
                    }
                    else
                    {
                        CProgram.sLogLine("Sirona Auto Collect is off!");
                    }
                }
                else
                {
                    string serialNr = "";
                    if (CProgram.sbReqLabel("Sirona Auto collect files", "Serial number device", ref serialNr, "", false))
                    {
                        DateTime startDT = DateTime.Now;
                        string acResult = "";
                        bool bScanOk = false;
                        UInt32 nrReqFiles = 0;
                        bool bAutomatic = false;

                        if (CProgram.sbReqBool("Sirona Auto collect ECG for " + serialNr, "Run Automatic", ref bAutomatic))
                        {
                            CProgram.sLogLine("Sirona Auto Collect run once for snr: " + serialNr);

                            if (mbSirAutoCollectScan(out bScanOk, out nrReqFiles, serialNr, bAutomatic, true))
                            {
                                acResult = "+ AutoCollect(";
                                if (false == bScanOk)
                                {
                                    acResult += "Failed ";
                                }
                                acResult += nrReqFiles.ToString() + ") ";
                            }
                            double tSec = (DateTime.Now - startDT).TotalSeconds;

                            CProgram.sLogLine("Sirona Auto Collect rune once for snr: " + serialNr + " " + acResult + " in " + tSec.ToString("0.000") + "sec");
                        }
                    }
                }
            }
            else
            {
                mEditSirAutoCollect();
            }

        }

        private void toolStripButtonSirHolter_Click(object sender, EventArgs e)
        {
            mEditSirHE();
        }

        private void listDV2LicenseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mListDV2License();
        }
        private void mListDV2License()
        {
            List<string> listLic = CRecDeviceList.sGetDV2DeviceList();
            int nLicense = listLic == null ? 0 : listLic.Count;
            int nDevice = 0;
            string snrs = "";


            if (nLicense > 0)
            {
                foreach (string s in listLic)
                {
                    if (CRecDeviceList.sbDV2IsInLicenseList(s, false))
                    {
                        snrs += "+" + s;
                        ++nDevice;
                    }
                    else
                    {
                        snrs += "-" + s;
                    }
                }
                CProgram.sLogLine(snrs);
            }
            CProgram.sLogLine("DV2: " + nDevice.ToString() + " active / " + nLicense.ToString() + " in device license");
        }

        private void rewriteUserListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CDvtmsData.sbUserListRewrite();
        }

        private void addUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CDvtmsData.sbCreateUserIX();
        }

        private void makeUserPinToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CDvtmsData.sbCreateUserPW("", 0);
        }

        private void testUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CDvtmsData.sbUserListTest();
        }

        private void importEdfToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void importDVX1EventToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void reverseBytesDatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mButtonDvxRevHea();
        }

        private void importDVXHeaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mSelectImportDvxHeaDat();
        }

        private void importDVX1DatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mSelectImportDvx1Dat();
        }

        private void importDVX1EvtToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mSelectImportDvx1Evt();
        }

        private void importDVX1EvtRecToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mSelectImportDvx1EvtRec();
        }

        private void mSelectImportDvx1Dat()
        {
            mSelectImportDvxFiles("DVX ECG", CDvxEvtRec._cEcgExtension, true);
            //CProgram.sLogLine("mSelectImportDvx1Dat() not implemented yet");

        }
        private void mSelectImportDvx1Evt()
        {
            mSelectImportDvxFiles("DVX Event", CDvxEvtRec._cEventExtension, false);
            // CProgram.sLogLine("mSelectImportDvx1Evt() not implemented yet");
        }
        private void mSelectImportDvx1EvtRec()
        {
            mSelectImportDvxFiles("DVX Record", CDvxEvtRec._cEvtRecExtension, false);
            //      CProgram.sLogLine("mSelectImportDvx1EvtRect() not implemented yet");
        }

        private void mSelectImportDvxFiles(string ASourceName, string AFileExtension, bool AbMergeFiles)
        {
            if (!mbCheckDaysLeft())
            {
                return;
            }
            if (_mbRunningConversion || _mbRunLoop)
            {
                CProgram.sLogLine("File conversion already running, press stop first!");
            }
            else
            {
                mSetRun(false);

                UInt32 dvxStudyNR = 0;
                bool bMergeFiles = AbMergeFiles;
                bool bUseStudyFolder = true;
                string baseFolder = "";
                string baseName = "";
                string title = "Import " + ASourceName + " (0=Import folder)";
                bool bOk = false;


                if (CProgram.sbReqUInt32(title, "DVX Study nr", ref dvxStudyNR, "", 0, Int32.MaxValue))
                {
                    if (dvxStudyNR == 0)
                    {
                        baseFolder = _mDVXImportDir == null || _mDVXImportDir.Length < 2 ? _mRecordingDir : _mDVXImportDir;
                        baseName = "Import";
                        bUseStudyFolder = false;
                        bOk = true;
                    }
                    else
                    {
                        bOk = CDvxEvtRec.sbGetStudyDeviceDataFolder(out baseFolder, dvxStudyNR, 0);
                        if (false == bOk)
                        {
                            CProgram.sAskWarning(title, "Unknown DVX Study " + dvxStudyNR.ToString());
                        }
                        else
                        {
                            baseName = "DVXStudy " + dvxStudyNR.ToString();
                        }
                    }
                    if (bOk)
                    {
                        title = "Select " + ASourceName + " " + baseName;
                        if (bMergeFiles)
                        {
                            title += " (Merging)";
                        }
                        openFileDialogDVX.Title = title;
                        openFileDialogDVX.Filter = ASourceName + "|*" + AFileExtension + "|All|*.*";
                        openFileDialogDVX.FilterIndex = 1;
                        openFileDialogDVX.InitialDirectory = baseFolder;
                        if (false == mbIsRunNamePresent())
                        {
                            openFileDialogDVX.FileName = baseFolder;
                        }
                        else
                        {
                            string name = Path.ChangeExtension(mGetRunFileName(), AFileExtension);
                            openFileDialogDVX.FileName = Path.Combine(baseFolder, name);
                        }


                        if (openFileDialogDVX.ShowDialog() == DialogResult.OK)
                        {
                            string[] files = openFileDialogDVX.FileNames;
                            int nFiles = files == null ? 0 : files.Length;

                            if (nFiles > 0)
                            {
                                string firstFile = files[0];
                                string copyStudy = "";

                                if (bUseStudyFolder)
                                {
                                    string s1 = baseFolder.ToLower();
                                    string s2 = firstFile.ToLower();
                                    bUseStudyFolder = s2.StartsWith(s1); // check if files are loaded from selected study
                                }
                                if (bUseStudyFolder)
                                {
                                    if (false == CProgram.sbReqBool("Import " + ASourceName, "Keep Files in DVX Study Folder (no Copy)", ref bUseStudyFolder))
                                    {
                                        nFiles = 0;
                                    }
                                }
                                if (nFiles > 0)
                                {
                                    if (bUseStudyFolder)
                                    {
                                        copyStudy = "copy dvxS" + dvxStudyNR.ToString();
                                    }
                                    if (bMergeFiles)
                                    {
                                        _mbRunningConversion = true;
                                        CProgram.sSetBusy(true);

                                        string header = "DVX import(" + copyStudy + ") merge " + nFiles.ToString();
                                        mbImportOneDVXFile(header, firstFile, dvxStudyNR, files, DImportMethod.Manual, bUseStudyFolder, false);

                                        CProgram.sSetBusy(false);
                                        _mbRunningConversion = false;
                                    }
                                    else
                                    {
                                        _mbRunningConversion = true;
                                        CProgram.sSetBusy(true);
                                        UInt32 i = 0;

                                        foreach (String file in files)
                                        {
                                            ++i;
                                            string header = "DVX import(" + copyStudy + ") " + i.ToString() + " / " + nFiles.ToString();
                                            mbImportOneDVXFile(header, file, dvxStudyNR, null, DImportMethod.Manual, bUseStudyFolder, false);
                                        }
                                        CProgram.sSetBusy(false);
                                        _mbRunningConversion = false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public bool mbImportOneDVXStudyEvent(string AEventFilePath, DImportMethod AImportMethod, bool AbCheckSkip)
        {
            string header = "DVX event import";
            bool bOk = mbImportOneDVXFile(header, AEventFilePath, 0, null, AImportMethod, true, AbCheckSkip);

            return bOk;
        }

        private enum DVX_Import_EXT { Unkown, Error, DvxEvtRec, Event, Dat };


        private bool mbDvxCheckSkipEvent( CDvxSkipLastFile ASkipDevice, CDvxEvtRec ADvxEvtRec, out double ArAfterSec)
        {
            bool bSkip = false;
            double afterSec = 0;


            if(ASkipDevice != null && ADvxEvtRec != null && _mDvxSkipCmpSec >= 0)
            {
                if(ASkipDevice._mUtc != DateTime.MinValue && ASkipDevice._mEventType != null && ASkipDevice._mEventType.Length > 0 
                    && false == CRecordMit.sbIsManualEvent(ASkipDevice._mEventType))
                {
                    string newEventType = CDvxEvtRec.sRemoveNrEvents(ADvxEvtRec._mEventType);
                    if (newEventType != null && newEventType.Length > 0 
                        && false == CRecordMit.sbIsManualEvent(newEventType) )
                    {
                        afterSec = (ADvxEvtRec._mEventUTC - ASkipDevice._mUtc).TotalSeconds;

                        bSkip = afterSec < _mDvxSkipCmpSec && (_mDvxRepCmpSec <= 0 || afterSec < _mDvxRepCmpSec);

                        if( bSkip && _mbDvxSkipSameOnly)
                        {
                            if( newEventType.ToLower() != ASkipDevice._mEventType)
                            {
                                bSkip = false;  // not the same event
                            }
                        }
                    }
                }
            }
            ArAfterSec = afterSec;
            return bSkip;
        }


        public bool mbImportOneDVXFile(string AHeader, string AFilePath, UInt32 ADvxStudyNr, string[] AMergeFiles, DImportMethod AImportMethod, bool AbUseStudyFolder, bool AbCheckSkip)
        {
            bool bOk = false;
            bool bError = false;
            bool bSkip = false;
            bool bWait = false;

            bool bDataRead = false;
            bool bDataCvs = false;
            double dTDataRed = 0.0F;
            double dTPlot = 0.0F;
            CRecordMit rec = null; // MIT record file
            DateTime startTime = DateTime.Now;
            String toPath;

            DateTime receiveUTC = DateTime.UtcNow;
            string copyChar = AbUseStudyFolder ? "" : "©";
            UInt32 dvxStudyNr = ADvxStudyNr;
            string addRemark = "";
            string resultText = "";
            bool bCopyFiles = false == AbUseStudyFolder;
            bool bMergeFiles = AMergeFiles != null && AMergeFiles.Length > 0;
            string fileName = Path.GetFileName(AFilePath);
            string dvxEvtRecName = Path.ChangeExtension(fileName, CDvxEvtRec._cEvtRecExtension);
            string storeName = "";
            string fileExt = Path.GetExtension(fileName);
            CDvxEvtRec dvxEvtRec = null;
            DVX_Import_EXT importExt = DVX_Import_EXT.Unkown;
            UInt32 deviceSNR = 0;
            DateTime eventBaseUTC = DateTime.MinValue;
            bool bCheckSum = false;
            string filesUsed = "";
            string configFilePath = "";
            bool bEcgConfig = false;
            bool bEvtPresent = false;
            CDvxSkipLastFile skipDevice = null;
            DateTime lastUTC = DateTime.MinValue;
            bool bFoundPrimairy = false;

            string logHeader = copyChar + AHeader + ": " + fileName;

            string[] allFilesInDir = null;

            try
            {
                if (AbUseStudyFolder && dvxStudyNr == 0)
                {
                    if (AImportMethod == DImportMethod.Automatic)
                    {
                        // determin study nr from file path

                        if (false == CDvxEvtRec.sbFindStudy(AFilePath, out dvxStudyNr))
                        {
                            CProgram.sLogWarning("Failed extract DVX study number from path " + AFilePath);
                            addRemark = "Bad DVX study dir";
                        }
                    }
                    if (dvxStudyNr == 0)
                    {
                        bCopyFiles = true;
                    }
                }
                mProcessSetInfo(logHeader);
                CProgram.sLogLine(">>>" + logHeader + " (dvxS" + dvxStudyNr.ToString() + ")");

                if (false == CDvxEvtRec.sbParseFileName(fileName, out deviceSNR, out eventBaseUTC, out fileExt))
                {
                    if( AImportMethod == DImportMethod.Manual 
                        && CProgram.sbAskOkCancel( "Manual import DVX: " + fileName, "No DVX name + time in file name, continue"))
                    {
                        if (eventBaseUTC.Year < 1900)
                        {
                            eventBaseUTC = DateTime.UtcNow;
                        }
                        // file manualy imported but not according to devNr_yyymmddhh
                    }
                    else
                    {
                        bError = true;
                        resultText = "Bad file name";

                    }
                }

                if (fileExt == CDvxEvtRec._cEvtRecExtension)
                {
                    dvxEvtRec = CDvxEvtRec.sLoadDvxEvtRecFile(out bCheckSum, AFilePath, false);
                    if (dvxEvtRec != null)
                    {
                        importExt = DVX_Import_EXT.DvxEvtRec;
                        dvxEvtRec._mValuesOrigin = fileName + "#" + deviceSNR.ToString();
                        bCopyFiles = false == dvxEvtRec._mbLoadFromStudy;
                    }
                    else
                    {
                        bError = true;
                        resultText = "Bad file";
                    }
                }
                else
                {
                    dvxEvtRec = CDvxEvtRec.sLoadDeviceFile(out bCheckSum, deviceSNR, false);

                    if (dvxEvtRec == null)
                    {
                        bError = true;
                        resultText = "Bad file";
                    }
                    else
                    {
                        UInt16 eventLevel;
                        UInt32 nrEvents;

                        importExt = DVX_Import_EXT.Event;


                        dvxEvtRec.mClearRecording(deviceSNR);
                        dvxEvtRec._mFileName = fileName;
                        dvxEvtRec._mFileStartUTC = eventBaseUTC;
                        dvxEvtRec._mbLoadFromStudy = bCopyFiles == false;
                        dvxEvtRec._mStudyNr = dvxStudyNr;
                        dvxEvtRec._mSourceFilePath = AFilePath;

                        dvxEvtRec.mClearFileList();
                        dvxEvtRec._mbManualImport = AImportMethod == DImportMethod.Manual;

                        bEcgConfig = dvxEvtRec.mbCheckLoadEcgConfig(out configFilePath, AFilePath, _mbLogExtra);   // modifies samples per second and gain
                        bEvtPresent = dvxEvtRec.mbCheckLoadEventFile(out eventLevel, out nrEvents, AFilePath, false, _mbLogExtra);  // sets eventUTC and eventSample

                        if (bEvtPresent)
                        {
                            if (eventLevel == (UInt16)DDvxEvent_Priority.Disregard)
                            {
                                bError = true;
                                resultText = "Existing event " + dvxEvtRec._mEventType;
                            }
                        }
                        if (false == bError)
                        {
                            if (bMergeFiles)
                            {
                                DateTime startUTC = DateTime.MaxValue;
                                DateTime endUTC = DateTime.MinValue;
                                DateTime utc;
                                UInt32 snr;
                                string ext;

                                if (fileExt == CDvxEvtRec._cEcgExtension)
                                {
                                    importExt = DVX_Import_EXT.Dat;

                                    foreach (string datFile in AMergeFiles)
                                    {
                                        dvxEvtRec.mbAddFileToList(datFile);

                                        if (CDvxEvtRec.sbParseFileName(datFile, out snr, out utc, out ext))
                                        {
                                            if (utc < startUTC) startUTC = utc; // determin rough strip range
                                            if (utc > endUTC) endUTC = utc;     
                                        }
                                    }
                                }
                                bFoundPrimairy = true;
                                lastUTC = DateTime.UtcNow;      // no test

                                // determin Pleth files when loaded

                                if (dvxEvtRec.mbDeterminPlethFileRange(ref allFilesInDir, AFilePath, startUTC, endUTC))
                                {



                                }

                            }
                            else
                            {
                                if (dvxEvtRec.mbDeterminFileRange(ref allFilesInDir, AFilePath, out lastUTC, out bFoundPrimairy ))
                                {



                                }
                            }
                        }
                        if (false == bError && AbCheckSkip)
                        {
                            // check if strip is complete (enough files to fill post)
                            double lastPostSec = (lastUTC - dvxEvtRec._mFileStartUTC).TotalSeconds;
                            double eventAgeMin = (DateTime.UtcNow - dvxEvtRec._mFileStartUTC).TotalMinutes;
                            double postSec = dvxEvtRec._mPostEventSec;
                            double waitMin = _mDvxWaitCompleteMin;

                            if( dvxEvtRec._mbPlethLoad )
                            {
                                postSec += dvxEvtRec._mPlethPostEcgSec; // add time to wait for pleth files
                                waitMin += 1 + dvxEvtRec._mPlethPostEcgSec / 60;
                            }

                            if (( lastPostSec < postSec || false == bFoundPrimairy )
                                && eventAgeMin < waitMin)
                            {
                                bWait = true;
                                resultText = "Wait for Event complete " + lastPostSec.ToString("0.0") + "<" + dvxEvtRec._mPostEventSec + " sec"
                                    + (dvxEvtRec._mbPlethLoad ? " pleth": "" )
                                  +  ( bFoundPrimairy ? "" : " -evt")  ;
                            }

                            if (_mDvxSkipCmpSec >= 0 || _mDvxRepCmpSec > 0)
                            {// check if event is repeated

                                skipDevice = mGetDvxSkipLastFile(deviceSNR); // find device last file number

                                if( mbDvxCheckSkipEvent(skipDevice, dvxEvtRec, out lastPostSec))
                                {
                                    bSkip = true;
                                    resultText = "Skip simular event " + lastPostSec.ToString("0.0") + "<" + _mDvxSkipCmpSec + " sec";
                                }
                            }
                        }
                    }
                }
                if (false == bCheckSum)
                {
                    logHeader = "~" + logHeader;
                }
                if (bError == false && false == bSkip && false == bWait && dvxEvtRec != null)
                {
                    // * make recording folder

                    if (mbChangeToDateDir(out toPath, _mRecordingDir, receiveUTC) == false)
                    {
                        bError = true;
                    }
                    else
                    {
                        storeName = "DVX_" + Path.GetFileNameWithoutExtension(AFilePath);

                        storeName = CSqlDBaseConnection.sSqlLimitSqlString(storeName, CRecordMit.cStoredAtMaxLen);

                        toPath = Path.Combine(toPath, storeName);

                        try
                        {
                            if (mbCreateDirectory(toPath))
                            {
                                String toFile = Path.Combine(toPath, dvxEvtRecName);

                                if (File.Exists(toFile))
                                {
                                    bError = true;
                                    resultText = "already exists";
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            CProgram.sLogException("DVX Failed create Recording folder for " + storeName, e);
                            bError = true;
                        }
                    }
                    if (bError == false)
                    {
                        if (bCopyFiles /*|| bMergeFiles*/)
                        {
                            // copy files to recording folder
                            if (false == dvxEvtRec.mbCopyFiles(ref allFilesInDir, AFilePath, toPath))
                            {
                                bError = true;
                                resultText = "failed copy files";
                            }
                            dvxEvtRec.mbCopyConfigFolder(AFilePath, toPath);
                        }
                    }
                    if (bError == false)
                    {
                        // read ECG data
                        string toFilePath = Path.Combine(toPath, dvxEvtRecName);
                        DateTime readDT = DateTime.UtcNow;

                        rec = dvxEvtRec.mLoadEcgData(out resultText, toFilePath, 0, DMoveTimeZone.None);

                        double dTDataRead = (DateTime.UtcNow - readDT).TotalSeconds;

                        if (rec == null)
                        {
                            bError = true;
                            resultText = "failed load ECG data";
                        }
                        else
                        {
                            string addRem = "";
                            rec.mImportConverter = (UInt16)DImportConverter.DVX1;
                            rec.mImportMethod = (UInt16)AImportMethod;
                            rec.mFileName = dvxEvtRecName;
                            rec.mStoredAt = storeName;
                            rec.mReceivedUTC = DateTime.UtcNow;
                            rec.mRecState = (UInt16)DRecState.Received;

                            if (dvxStudyNr > 0)
                            {
                                addRem += "dvxS" + dvxStudyNr.ToString() + (bCopyFiles ? "c" : "");
                            }
                            if (_mbLogExtra)
                            {
                                string listFiles = CDvxEvtRec.sCombineUsedFiles(dvxEvtRec._mDatFiles, 4);
                                addRem += "{" + (-dvxEvtRec._mPreEventSec).ToString()
                                    + "," + dvxEvtRec._mPostEventSec.ToString() + "}" + dvxEvtRec.mGetNrDatFiles().ToString() 
                                    + "=" + (bFoundPrimairy ? "" : "!" ) + listFiles;
                            }
                            else if( false == bFoundPrimairy)
                            {
                                addRem += "-evt.dat";
                            }
                            if( addRem.Length > 0 )
                            {
                                rec.mRecRemark.mAddLine(addRem);
                            }
                            int newIndex;
                            DateTime startDB = DateTime.Now;
                            bOk = mbAddSqlDbRow(out newIndex, rec, AFilePath);
                            double dbSec = (DateTime.Now - startDB).TotalSeconds;

                            dvxEvtRec._mRecordIX = rec.mIndex_KEY;
                            if (bOk)
                            {
                                if( skipDevice != null )
                                {
                                    skipDevice.mSetEvent(dvxEvtRec);
                                }
                                dvxEvtRec.mbSaveFile(toFilePath);
                            }

                            mbDoPreAnalysis(_mbDoPaDVX, _mDvxPaMethod, rec, toPath);
                            DateTime startStudy = DateTime.Now;
                            double studySec = 0.0;
                            if (bOk && DImportMethod.Automatic == AImportMethod)
                            {
                                mbFindStudy(rec);   // neads record number to update
                                studySec = (DateTime.Now - startStudy).TotalSeconds;
                                mbSendReceivedMail(AFilePath, rec);
                            }
                            mbCreateImages(toFilePath, rec, out dTPlot);  // plot later, needs invert mask from device
                            mbStoreGrid(rec);
                            TimeSpan dT = DateTime.Now - startTime;

                            CProgram.sLogLine("[" + newIndex.ToString() + "]Import DVX " + fileName + " in " + dT.TotalSeconds.ToString("0.00") + " Sec"
                              + "(" + rec.mNrSignals.ToString() + "x" + rec.mGetNrSamples().ToString() + " " + rec.mGetSamplesTotalTime().ToString("0")
                              + "s) ld=" + dTDataRead.ToString("0.00") + " pl=" + dTPlot.ToString("0.00")
                              + " db=" + dbSec.ToString("0.00") + " st=" + studySec.ToString("0.00") + " S" + rec.mStudy_IX.ToString());

                            if (bOk && AImportMethod == DImportMethod.Automatic)
                            {
                                string orgFilePath = Path.ChangeExtension(AFilePath, CDvxEvtRec._cEvtRecExtension);
                                dvxEvtRec.mbSaveFile(orgFilePath);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                CProgram.sLogException("Failed to import DVX file " + fileName, e);
                bError = true;
            }

            if (bOk)
            {
                TimeSpan dT = DateTime.Now - startTime;
                CProgram.sLogLine(logHeader + " imported " + resultText + " in " + dT.TotalSeconds.ToString("0.00") + " Sec.");
                mProcessSetInfo(logHeader + " " + resultText);
                mIncreaseTotalCount();

            }
            else if (bError)
            {
                CProgram.sLogLine(logHeader + " error " + resultText);
                mProcessSetInfo(logHeader + " error " + resultText);

                if (AImportMethod == DImportMethod.Automatic)
                {
                    string orgFilePath = Path.ChangeExtension(AFilePath, CDvxEvtRec._cEvtErrorExtension);
                    try
                    {
                        string text = CProgram.sDateTimeToYMDHMS(DateTime.Now) + ": " + logHeader + " error " + resultText + " $=" + AFilePath;
                        File.WriteAllText(orgFilePath, text);

                        if (dvxEvtRec != null)
                        {
                            dvxEvtRec.mbSaveFile(orgFilePath + CDvxEvtRec._cEvtRecExtension);
                        }
                    }
                    catch (Exception e)
                    {
                        CProgram.sLogException("Failed to write import error " + orgFilePath, e);
                    }
                }

                if (_mDisableErrorZipHour > 0.01)
                {
                    //                    mCheckDisableImportFile(AFilePath);
                }

                mIncreaseErrorCount();
            }
            else
            {
                bool bLog = _mbLogExtra;
                mProcessSetInfo(logHeader + " - " + resultText);

                if( bSkip && AImportMethod == DImportMethod.Automatic)
                {
                    mIncreaseSkippedCount();
                    bLog = true;
                    // write skip file??

                    string orgFilePath = Path.ChangeExtension(AFilePath, CDvxEvtRec._cEvtSkipExtension);
                    try
                    {

                        string text = CProgram.sDateTimeToYMDHMS(DateTime.Now) + ": " + logHeader + " " + resultText + " $=" + AFilePath;
                        File.WriteAllText(orgFilePath, text);

                        if (dvxEvtRec != null)
                        {
                            dvxEvtRec.mbSaveFile(orgFilePath + CDvxEvtRec._cEvtRecExtension);
                        }
                    }
                    catch (Exception e)
                    {
                        CProgram.sLogException("Failed to write import error " + orgFilePath, e);
                    }

                }
                if (bLog)
                {
                    CProgram.sLogLine(logHeader + " - " + resultText);
                }
            }
            return bOk;
        }

        private void dVXToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string license;
            UInt32 n = CRecDeviceList.sGetActiveDVXDeviceList(out license, "+");

            CProgram.sLogLine(n.ToString() + " active DVX= " + license);

        }

        private void createDVXDefaultsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CDvxEvtRec dvxDefaults = CDvxEvtRec.sCheckDefaults();
        }

        private void addDVX1DeviceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mButtonAddDvx();
        }
        private void mButtonAddDvx()
        {
            if (!mbCheckDaysLeft())
            {
                return;
            }
            if (_mbRunningConversion || _mbRunLoop)
            {
                CProgram.sLogLine("File conversion already running, press stop first!");
            }
            else
            {
                mDvxCreateDevices();
            }
        }

        private bool mbDvxCreateOneDevice(ref UInt32 ArCount, ref UInt32 ArCreated, string ALogPreText, String AModelTitle,
        ref CDeviceInfo ArDeviceTemplate, string ASerialNr)
        {
            bool bOk = false;
            string serialNr;
            string deviceDir = "";
            bool bDeviceDirCreated = false;
            bool bDeviceDbCreated = false;
            string header = ALogPreText + " " + AModelTitle + " " + ASerialNr + ":  ";
            string result = "";
            int lenStr, pos;

            try
            {
                if (ASerialNr == null || ASerialNr.Length == 0)
                {
                    return true;    // skip line
                }
                pos = ASerialNr.IndexOf('=');
                if (pos > 0)
                {
                    serialNr = ASerialNr.Substring(0, pos).Trim();  // remove '='
                }
                else
                {
                    serialNr = ASerialNr.Trim();
                }

                lenStr = serialNr != null ? serialNr.Length : 0;

                if (lenStr == 0)
                {
                    return true;    // skip line
                }
                if (false == Char.IsLetterOrDigit(serialNr[0]))
                {
                    if (serialNr[0] == '+' && lenStr > 0)
                    {
                        serialNr = serialNr.Substring(1);   // only accept
                    }
                    else
                    {
                        return true;    // skip line
                    }
                }

                bOk = CRecDeviceList.sbDVXIsInLicenseList(serialNr, true);
                if (false == bOk)
                {
                    result += "Not in license list.";
                }
                if (bOk)
                {
                    if (CDvtmsData.sbGetDeviceUnitDir(out deviceDir, DDeviceType.DVX, serialNr, true))
                    {
                        bOk = true;
                    }
                    else if (false == CDvtmsData.sbCreateDeviceUnitDir(out deviceDir, DDeviceType.DVX, serialNr))
                    {
                        result += " create Device directory failed";
                    }
                    else
                    {
                        bDeviceDirCreated = true;
                        bOk = true;
                    }
                }
                if (bOk)
                {
                    UInt32 indexKey = 0;

                    if (bDeviceDirCreated)
                    {
                        result += "device dir created, ";
                    }
                    bOk = mbCreateDeviceInDB(out bDeviceDbCreated, out indexKey, AModelTitle, ref ArDeviceTemplate, serialNr);

                    if (bOk)
                    {
                        result += "device #" + indexKey.ToString();
                        if (bDeviceDbCreated)
                        {
                            result += " created";
                        }
                    }
                    else
                    {
                        result += "failed device in dBase!";
                    }
                }
                if (bOk)
                {
                    CProgram.sLogLine(header + result);
                    ++ArCount;
                    if (bDeviceDirCreated || bDeviceDbCreated)
                    {
                        ++ArCreated;
                    }
                }
                else
                {
                    CProgram.sLogError(header + result);
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Create " + AModelTitle + " in dBase device list failed for " + ASerialNr, ex);
            }

            return bOk;
        }

        private void mDvxCreateDevices()
        {
            string text = "";
            string modelTitle = "DVX";
            CDeviceInfo deviceTemplate = null;
            string title = "Create " + modelTitle + " devices";

            if (!mbCheckDaysLeft())
            {
                return;
            }
            try
            {
                bool bOk = ReqTextForm.sbReqText("Create " + modelTitle + " devices", "List serial numbers (*=license device file)(no remarks)", ref text);

                if (bOk)
                {
                    bOk = mbCreateDeviceTemplate(modelTitle, ref deviceTemplate,
                        ref _mDvxTemplateModel, ref _mDvxTemplateFirmware, ref _mbDvxTemplateSetAvailable);
                }
                //                if (bCopyDefaults)
                {
                    string devicePath;

                    bOk = CDvtmsData.sbCreateDeviceTypeDir(out devicePath, DDeviceType.DVX);
                }

                if (bOk)
                {
                    text = text.Trim();
                    if (text != null && text.Length > 0)
                    {
                        CRecDeviceList.sDeviceListClear(); // always clear list and reload the new device license file
                        CRecDeviceList.sbDVXDeviceListsCheck(true, false);

                        if (text[0] == '*')
                        {
                            // from device list
                            modelTitle += "*";

                            int n = CRecDeviceList.sGetDVXDeviceCount();
                            if (n > 0)
                            {
                                int i = 0;
                                UInt32 nOk = 0;
                                UInt32 nCreated = 0;

                                if (mbProcessStartLoop("Create DVX", n, false))
                                {
                                    foreach (string snr in CRecDeviceList.sGetDvxLicenseList())
                                    {
                                        mbProcessNextLoop(++i, false, snr);
                                        if (mbDvxCreateOneDevice(ref nOk, ref nCreated, "[" + i.ToString() + "/" + n.ToString() + "]", modelTitle,
                                            ref deviceTemplate, snr))
                                        {
                                        }
                                        else if (false == CProgram.sbPromptOkCancel(true, "Create " + modelTitle + " " + n.ToString() + " devices",
                                            i.ToString() + " " + snr + " failed, Continue?"))
                                        {
                                            CProgram.sLogError("Create " + modelTitle + " devices canceled");
                                            break;
                                        }
                                    }
                                    mbProcessEndLoop(i, false, nCreated.ToString() + " DVX created");
                                }
                                CProgram.sLogLine("Create " + modelTitle + " " + nOk.ToString() + " devices done / " + n.ToString() + " licensed, " + nCreated.ToString() + " created");
                                if (nCreated > 0)
                                {
                                    CProgram.sAskOk("Create " + modelTitle + " devices", nCreated.ToString() + " devices created, finish configuration in Eventboard!");
                                }
                            }
                            else
                            {
                                CProgram.sLogError("Create " + modelTitle + " no devices in license list");
                            }
                        }
                        else
                        {
                            string[] lines = text.Split();
                            int i, n = lines == null ? 0 : lines.Length;
                            UInt32 nOk = 0;
                            UInt32 nCreated = 0;

                            string snr;

                            if (mbProcessStartLoop("Create DVX", n, false))
                            {
                                for (i = 0; i < n;)
                                {
                                    snr = lines[i];
                                    mbProcessNextLoop(++i, false, snr);
                                    if (mbDvxCreateOneDevice(ref nOk, ref nCreated, "[" + i.ToString() + "/" + n.ToString() + "]", modelTitle,
                                        ref deviceTemplate, snr))
                                    {

                                    }
                                    else if (false == CProgram.sbPromptOkCancel(true, "Create " + modelTitle + " " + n.ToString() + " devices",
                                        i.ToString() + " " + snr + " failed, Continue?"))
                                    {
                                        CProgram.sLogError("Create " + modelTitle + " devices canceled");
                                        break;
                                    }
                                }
                                mbProcessEndLoop(i, false, nCreated.ToString() + " DVX created");
                            }
                            CProgram.sLogLine("Create " + modelTitle + " " + nOk.ToString() + " devices done,  / " + n.ToString() + " lines, " + nCreated.ToString() + " created");
                            if (nCreated > 0)
                            {
                                CProgram.sAskOk("Create " + modelTitle + " devices", nCreated.ToString() + " devices created, finish configuration in Eventboard!");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Create DVX device failed ", ex);
            }
        }

        private void mListDVXLicense()
        {
            List<string> listLic = CRecDeviceList.sGetDVXDeviceList();
            int nLicense = listLic == null ? 0 : listLic.Count;
            int nDevice = 0;
            string snrs = "";

            if (nLicense > 0)
            {
                foreach (string s in listLic)
                {
                    if (CRecDeviceList.sbDVXIsInLicenseList(s, false))
                    {
                        snrs += "+" + s;
                        ++nDevice;
                    }
                    else
                    {
                        snrs += "-" + s;
                    }
                }
                CProgram.sLogLine(snrs);
            }
            CProgram.sLogLine("DVX: " + nDevice.ToString() + " active / " + nLicense.ToString() + " in device license");
        }

        private void listDVXLicenseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mListDVXLicense();
        }

        private void reloadDVXLicenseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CRecDeviceList.sbDVXDeviceListsCheck(true, true);
        }

        private void contextMenuAddDevice_Opening(object sender, CancelEventArgs e)
        {

        }

        private void reloadDVXLicenseToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CRecDeviceList.sbDVXDeviceListsCheck(true, true);
        }

        private void addDV2DeviceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mButtonAddDV2();
        }

        private void mButtonAddDV2()
        {
            if (!mbCheckDaysLeft())
            {
                return;
            }
            if (_mbRunningConversion || _mbRunLoop)
            {
                CProgram.sLogLine("File conversion already running, press stop first!");
            }
            else
            {
                mDvxCreateDevices();
            }
        }

        private bool mbDV2CreateOneDevice(ref UInt32 ArCount, ref UInt32 ArCreated, string ALogPreText, String AModelTitle,
        ref CDeviceInfo ArDeviceTemplate, string ASerialNr)
        {
            bool bOk = false;
            string serialNr;
            string deviceDir = "";
            bool bDeviceDirCreated = false;
            bool bDeviceDbCreated = false;
            string header = ALogPreText + " " + AModelTitle + " " + ASerialNr + ":  ";
            string result = "";
            int lenStr, pos;

            try
            {
                if (ASerialNr == null || ASerialNr.Length == 0)
                {
                    return true;    // skip line
                }
                pos = ASerialNr.IndexOf('=');
                if (pos > 0)
                {
                    serialNr = ASerialNr.Substring(0, pos).Trim();  // remove '='
                }
                else
                {
                    serialNr = ASerialNr.Trim();
                }

                lenStr = serialNr != null ? serialNr.Length : 0;

                if (lenStr == 0)
                {
                    return true;    // skip line
                }
                if (false == Char.IsLetterOrDigit(serialNr[0]))
                {
                    if (serialNr[0] == '+' && lenStr > 0)
                    {
                        serialNr = serialNr.Substring(1);   // only accept
                    }
                    else
                    {
                        return true;    // skip line
                    }
                }

                bOk = CRecDeviceList.sbDV2IsInLicenseList(serialNr, true);
                if (false == bOk)
                {
                    result += "Not in license list.";
                }
                if (bOk)
                {
                    if (CDvtmsData.sbGetDeviceUnitDir(out deviceDir, DDeviceType.DV2, serialNr, true))
                    {
                        bOk = true;
                    }
                    else if (false == CDvtmsData.sbCreateDeviceUnitDir(out deviceDir, DDeviceType.DV2, serialNr))
                    {
                        result += " create Device directory failed";
                    }
                    else
                    {
                        bDeviceDirCreated = true;
                        bOk = true;
                    }
                }
                if (bOk)
                {
                    UInt32 indexKey = 0;

                    if (bDeviceDirCreated)
                    {
                        result += "device dir created, ";
                    }
                    bOk = mbCreateDeviceInDB(out bDeviceDbCreated, out indexKey, AModelTitle, ref ArDeviceTemplate, serialNr);

                    if (bOk)
                    {
                        result += "device #" + indexKey.ToString();
                        if (bDeviceDbCreated)
                        {
                            result += " created";
                        }
                    }
                    else
                    {
                        result += "failed device in dBase!";
                    }
                }
                if (bOk)
                {
                    CProgram.sLogLine(header + result);
                    ++ArCount;
                    if (bDeviceDirCreated || bDeviceDbCreated)
                    {
                        ++ArCreated;
                    }
                }
                else
                {
                    CProgram.sLogError(header + result);
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Create " + AModelTitle + " in dBase device list failed for " + ASerialNr, ex);
            }

            return bOk;
        }

        private void mDV2CreateDevices()
        {
            string text = "";
            string modelTitle = "DV2";
            CDeviceInfo deviceTemplate = null;
            string title = "Create " + modelTitle + " devices";

            if (!mbCheckDaysLeft())
            {
                return;
            }
            try
            {
                bool bOk = ReqTextForm.sbReqText("Create " + modelTitle + " devices", "List serial numbers (*=license device file)(no remarks)", ref text);

                if (bOk)
                {
                    bOk = mbCreateDeviceTemplate(modelTitle, ref deviceTemplate,
                        ref _mDV2TemplateModel, ref _mDV2TemplateFirmware, ref _mbDV2TemplateSetAvailable);
                }
                //                if (bCopyDefaults)
                {
                    string devicePath;

                    bOk = CDvtmsData.sbCreateDeviceTypeDir(out devicePath, DDeviceType.DV2);
                }

                if (bOk)
                {
                    text = text.Trim();
                    if (text != null && text.Length > 0)
                    {
                        CRecDeviceList.sDeviceListClear(); // always clear list and reload the new device license file
                        CRecDeviceList.sbDV2DeviceListsCheck(_mDV2FromDir, true, false);

                        if (text[0] == '*')
                        {
                            // from device list
                            modelTitle += "*";

                            int n = CRecDeviceList.sGetDV2DeviceCount();
                            if (n > 0)
                            {
                                int i = 0;
                                UInt32 nOk = 0;
                                UInt32 nCreated = 0;

                                if (mbProcessStartLoop("Create DV2", n, false))
                                {
                                    foreach (string snr in CRecDeviceList.sGetDvxLicenseList())
                                    {
                                        mbProcessNextLoop(++i, false, snr);
                                        if (mbDV2CreateOneDevice(ref nOk, ref nCreated, "[" + i.ToString() + "/" + n.ToString() + "]", modelTitle,
                                            ref deviceTemplate, snr))
                                        {
                                        }
                                        else if (false == CProgram.sbPromptOkCancel(true, "Create " + modelTitle + " " + n.ToString() + " devices",
                                            i.ToString() + " " + snr + " failed, Continue?"))
                                        {
                                            CProgram.sLogError("Create " + modelTitle + " devices canceled");
                                            break;
                                        }
                                    }
                                    mbProcessEndLoop(i, false, nCreated.ToString() + " DV2 created");
                                }
                                CProgram.sLogLine("Create " + modelTitle + " " + nOk.ToString() + " devices done / " + n.ToString() + " licensed, " + nCreated.ToString() + " created");
                                if (nCreated > 0)
                                {
                                    CProgram.sAskOk("Create " + modelTitle + " devices", nCreated.ToString() + " devices created, finish configuration in Eventboard!");
                                }
                            }
                            else
                            {
                                CProgram.sLogError("Create " + modelTitle + " no devices in license list");
                            }
                        }
                        else
                        {
                            string[] lines = text.Split();
                            int i, n = lines == null ? 0 : lines.Length;
                            UInt32 nOk = 0;
                            UInt32 nCreated = 0;

                            string snr;

                            if (mbProcessStartLoop("Create DV2", n, false))
                            {
                                for (i = 0; i < n;)
                                {
                                    snr = lines[i];
                                    mbProcessNextLoop(++i, false, snr);
                                    if (mbDV2CreateOneDevice(ref nOk, ref nCreated, "[" + i.ToString() + "/" + n.ToString() + "]", modelTitle,
                                        ref deviceTemplate, snr))
                                    {

                                    }
                                    else if (false == CProgram.sbPromptOkCancel(true, "Create " + modelTitle + " " + n.ToString() + " devices",
                                        i.ToString() + " " + snr + " failed, Continue?"))
                                    {
                                        CProgram.sLogError("Create " + modelTitle + " devices canceled");
                                        break;
                                    }
                                }
                                mbProcessEndLoop(i, false, nCreated.ToString() + " DVX created");
                            }
                            CProgram.sLogLine("Create " + modelTitle + " " + nOk.ToString() + " devices done,  / " + n.ToString() + " lines, " + nCreated.ToString() + " created");
                            if (nCreated > 0)
                            {
                                CProgram.sAskOk("Create " + modelTitle + " devices", nCreated.ToString() + " devices created, finish configuration in Eventboard!");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Create DV2 device failed ", ex);
            }
        }

        private void listDatInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mMenuListDvxDatInfo();
        }

        private void mMenuListDvxDatInfo()
        {
            if (!mbCheckDaysLeft())
            {
                return;
            }
            if (_mbRunningConversion || _mbRunLoop)
            {
                CProgram.sLogLine("File conversion already running, press stop first!");
            }
            else
            {
                mSetRun(false);

                UInt32 studyNR = 0;
                bool bUseStudyFolder = true;
                string baseFolder = "";
                string baseName = "";
                string title = "List DVX dat info (0=Import folder)";
                bool bOk = false;


                if (CProgram.sbReqUInt32(title, "DVX Study nr", ref studyNR, "", 0, Int32.MaxValue))
                {
                    if (studyNR == 0)
                    {
                        baseFolder = _mDVXImportDir == null || _mDVXImportDir.Length < 2 ? _mRecordingDir : _mDVXImportDir;
                        baseName = "Import";
                        bOk = true;
                    }
                    else
                    {
                        bOk = CDvxEvtRec.sbGetStudyDeviceDataFolder(out baseFolder, studyNR, 0);
                        if (false == bOk)
                        {
                            CProgram.sAskWarning(title, "Unknown DVX Study " + studyNR.ToString());
                        }
                        else
                        {
                            baseName = "dvxStudy " + studyNR.ToString();
                        }
                    }
                    if (bOk)
                    {
                        title = "Select multiple DVX dat files (1=all) " + baseName;
                        openFileDialogDVX.Title = title;
                        openFileDialogDVX.Filter = "DVX ECG|*" + CDvxEvtRec._cEcgExtension + "|All|*.*";
                        openFileDialogDVX.FilterIndex = 1;
                        openFileDialogDVX.InitialDirectory = baseFolder;
                        if (false == mbIsRunNamePresent())
                        {
                            openFileDialogDVX.FileName = baseFolder;
                        }
                        else
                        {
                            string name = Path.ChangeExtension(mGetRunFileName(), CDvxEvtRec._cEcgExtension);
                            openFileDialogDVX.FileName = Path.Combine(baseFolder, name);
                        }

                        if (openFileDialogDVX.ShowDialog() == DialogResult.OK)
                        {
                            string[] files = openFileDialogDVX.FileNames;
                            int nFiles = files == null ? 0 : files.Length;
                            //bool bLogAllFiles = false;

                            if( nFiles == 1 )
                            {
                                // one file -> select all
                                string path = Path.GetDirectoryName(files[0]);
                                try
                                {
                                    DirectoryInfo dir = new DirectoryInfo(path);
                                    FileInfo[]  allFiles = dir.GetFiles("*" + CDvxEvtRec._cEcgExtension);
                                    int n = allFiles == null ? 0 : allFiles.Length;
                                    if (n > 0)
                                    {
                                        files = new string[n];

                                        for( int i = 0; i < n; ++i )
                                        {
                                            files[i] = allFiles[i].FullName;
                                        }
                                        nFiles = n;
                                    }
                                }
                                catch( Exception ex )
                                {

                                }
                            }

                            if (nFiles > 0)
                            //                                && CProgram.sbReqBool( title, "Log all files", ref bLogAllFiles))
                            {
                                string reportTextAll, reportTextShort, reportResult;
                                string firstFile = files[0];
                                string firstName = Path.GetFileNameWithoutExtension(firstFile);
                                CDvxEvtRec dvxEvtRec = null;
                                DateTime startUTC = DateTime.UtcNow;

//                                Array.Sort(files);

                                CProgram.sSetBusy(true);

                                if (mbListDvxDatInfo(out reportTextAll, out reportTextShort, out reportResult, studyNR, files, out dvxEvtRec))
                                {

                                    CProgram.sLogLine("DVX1 list " + nFiles.ToString() + " files, " + reportResult + firstFile);

                                    string path = Path.GetDirectoryName(firstFile);
                                    string resultFile = Path.Combine(path, "list_" + firstName + "_" + CProgram.sDateTimeToYMDHMS(DateTime.Now) + "_All.txt");

                                    try
                                    {
                                        File.WriteAllText(resultFile, reportTextAll);

                                        resultFile = Path.Combine(path, "list_" + firstName + "_" + CProgram.sDateTimeToYMDHMS(DateTime.Now) + "_Short.txt");
                                        File.WriteAllText(resultFile, reportTextShort);

                                        double sec = (DateTime.UtcNow - startUTC).TotalSeconds;

                                        CProgram.sLogLine(sec.ToString("0.0") + " sec: DVX list result written to " + resultFile);

                                    }
                                    catch (Exception ex)
                                    {
                                        CProgram.sLogException("Failed write file " + resultFile, ex);
                                    }
                                }
                                else
                                {
                                    CProgram.sLogLine("DVX1 list " + nFiles.ToString() + " files FAILED! " + firstFile);
                                }
                                CProgram.sSetBusy(false);
                            }
                        }
                    }
                }
            }
        }

        public bool mbListDvxDatInfo(out string ArReportTextAll, out string ArReportTextShort, out string ArReportResult, UInt32 AStudyNr, string[] AMergeFiles, out CDvxEvtRec ArDvxEvtRec)
        {
            bool bOk = false;

            CDvxEvtRec dvxEvtRec = null;
            Int32 nrFiles = AMergeFiles == null ? 0 : AMergeFiles.Length;
            bool bCheckSum;
            UInt32 deviceSNR = 0;
            DateTime utc;
            UInt32 samplesPerSec;

            string fileExt;

            ArReportResult = "";
            ArReportTextAll = "";
            ArReportTextShort = "";

            try
            {
                if (nrFiles == 0)
                {
                    ArReportResult = "No files selected";

                }
                else
                {
                    if (false == CDvxEvtRec.sbParseFileName(AMergeFiles[0], out deviceSNR, out utc, out fileExt))
                    {
                        ArReportResult = "Bad DVX file name";
                    }
                    else
                    {
                        dvxEvtRec = CDvxEvtRec.sLoadDeviceFile(out bCheckSum, deviceSNR, false);

                        if (dvxEvtRec == null)
                        {
                            ArReportResult = "No device config";
                        }
                        else
                        {
                            if (false == bCheckSum)
                            {
                                CProgram.sLogLine("Checksum device settings " + CDvxEvtRec.sGetDeviceName(deviceSNR) + " modified");
                            }
                            samplesPerSec = (UInt32)(dvxEvtRec._mSamplesPerSec + 0.5F);

                            if (CProgram.sbReqUInt32("DVX list files", "Use samples per sec", ref samplesPerSec, "Sps", 0, 99999))
                            {
                                string configFilePath;
                                bool bEcgConfig = dvxEvtRec.mbCheckLoadEcgConfig(out configFilePath, AMergeFiles[0], _mbLogExtra);   // modifies samples per second and gain

                                bOk = dvxEvtRec.mbListDvxDatInfo(out ArReportTextAll, out ArReportTextShort, out ArReportResult, AStudyNr, AMergeFiles, samplesPerSec);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed list DVX files ", ex);
            }
            ArDvxEvtRec = dvxEvtRec;
            return bOk;
        }

        private void dupRecordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mDupRecordItem();
        }

        private CRecordMit mDupOneRecord(CRecordMit ARecord, Int32 AIndex, string ALabel, string ARecordPath, FileInfo[] AFiles, bool AbAddToStudy)
        {
            CRecordMit dupRec = null;

            bool bOk = false;

            if (ARecord != null && ARecordPath != null && ARecordPath.Length > 3)
            {
                bool bCopyFiles = AFiles != null;

                try
                {
                    dupRec = ARecord.mCreateCopy() as CRecordMit;

                    if (dupRec != null)
                    {
                        if (bCopyFiles)
                        {
                            Int32 nrFiles = AFiles == null ? 0 : AFiles.Length;
                            UInt32 nCopiedFiles = 0;
                            string remLabel = ALabel;

                            if (AFiles != null)
                            {
                                // create duplicate folder
                                string storeName = ARecord.mStoredAt + "-" + AIndex;
                                string copyPath = Path.Combine(Path.GetDirectoryName(ARecordPath), storeName);

                                dupRec.mStoredAt = storeName;
                                if (Directory.Exists(copyPath))
                                {
                                    CProgram.sLogLine(ALabel + " dir already exists:" + copyPath);
                                }
                                else
                                {
                                    Directory.CreateDirectory(copyPath);

                                    for (int i = 0; i < nrFiles; ++i)
                                    {
                                        string fileName = AFiles[i].Name;
                                        string fromFile = Path.Combine(ARecordPath, fileName);
                                        string toFile = Path.Combine(copyPath, fileName);

                                        File.Copy(fromFile, toFile);
                                        ++nCopiedFiles;
                                    }
                                }
                            }
                            dupRec.mStudy_IX = 0;
                            dupRec.mSeqNrInStudy = 0;

                            if (dupRec.mbDoSqlInsert())
                            {
                                if (AbAddToStudy)
                                {
                                    try
                                    {
                                        CStudyInfo study = new CStudyInfo(ARecord.mGetSqlConnection());

                                        if (study != null)
                                        {
                                            if (study.mbDoSqlSelectIndex(ARecord.mStudy_IX, study.mMaskValid))
                                            {
                                                dupRec.mSeqNrInStudy = ++study._mNrRecords;

                                                UInt64 saveMask = CSqlDataTableRow.sGetMask((UInt16)DStudyInfoVars.NrRecords);

                                                if (study.mbDoSqlUpdate(saveMask, true)) // update study nrRecords
                                                {
                                                    dupRec.mStudy_IX = ARecord.mStudy_IX;
                                                }
                                                else
                                                {
                                                    dupRec.mSeqNrInStudy = 0;
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        CProgram.sLogException("dupRecord add to Study Failed " + ALabel, ex);
                                        dupRec.mStudy_IX = 0;
                                        dupRec.mSeqNrInStudy = 0;
                                    }
                                }
                                dupRec.mRecRemark.mbEncrypt(remLabel + "\r\n" + dupRec.mRecRemark.mDecrypt());

                                bOk = dupRec.mbDoSqlUpdate(dupRec.mGetValidMask(true), true);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("dupRecord Failed " + ALabel, ex);
                }
            }

            return bOk ? dupRec : null;
        }

        private bool mbDupRecords(CRecordMit ARecord, UInt32 ANrCopies, UInt32 AStartIndex, string ARecordPath, FileInfo[] AFiles, UInt64 ATotalFilesSize, UInt32 AMaxNrToStudy)
        {
            bool bOk = false;
            bool bAddToStudy = AMaxNrToStudy > 0;
            bool bCopyFiles = AFiles != null;

            if (ARecord != null && ARecordPath != null && ARecordPath.Length > 3)
            {
                UInt32 nCopies = 0;
                UInt32 indexOffset = AStartIndex == 0 ? 0 : AStartIndex - 1;
                UInt32 nAddedToStudy = 0;
                UInt32 nNrRecInStudy = 0;
                UInt32 recordIX = ARecord.mIndex_KEY;
                UInt32 studyIX = ARecord.mStudy_IX;
                UInt32 mb = bCopyFiles ? (UInt32)(ATotalFilesSize * ANrCopies / 1000000) : 0;
                DateTime startRunUtc = DateTime.UtcNow;
                string title = (bCopyFiles ? "copy" : "dup") + "R" + recordIX.ToString();

                if (bAddToStudy) title += "@S" + studyIX;

                CProgram.sLogLine("start " + title + " " + ANrCopies + " copies..." + (bCopyFiles ? mb + "MB" : ""));


                if (mbProcessStartLoop(title, (Int32)ANrCopies, false))
                {
                    string label = "";


                    //                    mSetRunTimerProgress("SirZip", 0, nZipFiles);
                    Int32 i = 0;
                    Int32 index;
                    for (; ++i <= ANrCopies; )
                    {
                        DateTime startCopyUtc = DateTime.UtcNow;
                        index = (Int32)(indexOffset + i);

                        label = title + "-" + index.ToString();

                        if (mbProcessNextLoop(i-1, false, label))
                        {
                            CRecordMit dupRec = mDupOneRecord(ARecord, index, label, ARecordPath, AFiles, nNrRecInStudy < AMaxNrToStudy);


                            //                            mSetRunTimerProgress("SirZip", i, nZipFiles);
                            if (dupRec == null)
                            {
                                bOk = false;
                                CProgram.sLogLine(i + ":dupRecord " + label + " failed ");
                                break;
                            }
                            ++nCopies;
                            if (dupRec.mStudy_IX > 0 && dupRec.mSeqNrInStudy > 0)
                            {
                                ++nAddedToStudy;
                                if (dupRec.mSeqNrInStudy > nNrRecInStudy)
                                {
                                    nNrRecInStudy = dupRec.mSeqNrInStudy;
                                }
                            }
                            bOk = true;
                            CProgram.sLogLine(i + ":" + label + " in " + (DateTime.UtcNow - startCopyUtc).TotalSeconds.ToString("0.0") + " sec, R"
                                + dupRec.mIndex_KEY + (dupRec.mStudy_IX > 0 ? " S" + dupRec.mStudy_IX + "." + dupRec.mSeqNrInStudy : ""));
                        }
                    }
                    mbProcessEndLoop(i, false, i + " copies");
                    mSetRunTimerProgress(title, i, (Int32)ANrCopies);

                }

                double totalTimeSec = (DateTime.UtcNow - startRunUtc).TotalSeconds;

                if (bOk)
                {
                    CProgram.sLogLine(nCopies + " copies " + title + " in " + totalTimeSec.ToString("0.0") + " sec"
                        + (bAddToStudy ? ", " + nAddedToStudy + " added to study " + studyIX + " / " + nNrRecInStudy : ""));
                }
                else
                {
                    CProgram.sLogLine("failed, done " + nCopies + " copies " + title + " in " + totalTimeSec.ToString("0.0") + " sec"
                        + (bAddToStudy ? ", " + nAddedToStudy + " added to study " + studyIX + " / " + nNrRecInStudy : ""));
                }
            }
            return bOk;
        }

        private void mDupRecordItem()
        {
            UInt32 recordIX = 0;

            try
            {
                if (CProgram.sbReqUInt32("Duplicate Record", "Record index", ref recordIX, "", 1, 99999999))
                {
                    CRecordMit rec = new CRecordMit(CDvtmsData.sGetDBaseConnection());

                    if (rec == null || false == rec.mbDoSqlSelectIndex(recordIX, rec.mGetValidMask(true)))
                    {
                        CProgram.sPromptWarning(true, "Dup record", "Record " + recordIX.ToString() + " does not exists");
                    }
                    else
                    {
                        UInt32 studyIX = rec.mStudy_IX;
                        bool bAddToStudy = studyIX > 0;
                        UInt32 nrCopies = 0;
                        UInt32 startIndex = 1;
                        UInt32 maxNrCopies = 65000;
                        UInt32 nrRecordsInStudy = 0;
                        UInt32 maxNrRecordsInStudy = 65000;
                        UInt32 maxCopySize = 1000000000;
                        bool bCopyFiles = false;
                        CStudyInfo studyInfo = null;
                        bool bContinue = true;
                        string recordPath = "";
                        FileInfo[] files = null;
                        Int32 nrFiles = 0;
                        UInt64 totalFileFize = 0;

                        string title = "Dup Record R" + recordIX.ToString() + " dev=" + rec.mDeviceID + " S" + rec.mStudy_IX.ToString();

                        if (studyIX > 0)
                        {
                            title += "." + rec.mSeqNrInStudy.ToString();

                            studyInfo = new CStudyInfo(CDvtmsData.sGetDBaseConnection());

                            if (studyInfo == null || false == studyInfo.mbDoSqlSelectIndex(studyIX, studyInfo.mGetValidMask(true)))
                            {
                                bContinue = false;
                                CProgram.sPromptWarning(true, "Dup Record " + recordIX.ToString(), "Study " + studyIX.ToString() + " does not exists");
                            }
                            else
                            {
                                nrRecordsInStudy = studyInfo._mNrRecords;
                                title += " / " + studyInfo._mNrRecords.ToString();
                            }
                        }
                        title += " Evt" + CProgram.sDateTimeToYMDHMS(rec.mGetDeviceTime(rec.mEventUTC));


                        recordPath = CDvtmsData.sGetRecordingCaseDir(rec);
                        if (recordPath == null || recordPath.Length < 3 )
                        {
                            bContinue = false;
                            CProgram.sPromptWarning(true, "Dup Record " + recordIX.ToString(), "Study " + studyIX.ToString() + " does not exists");
                        }

                        if (bContinue)
                        {

                            if (CProgram.sbReqUInt32(title, "nr of duplicates", ref nrCopies, "", 1, maxNrCopies)
                                && CProgram.sbReqUInt32(title, "start Index", ref startIndex, "", 1, maxNrCopies)
                                && CProgram.sbReqBool(title, "copy files", ref bCopyFiles)
                                && (studyIX == 0 || CProgram.sbReqBool(title, "add to study", ref bAddToStudy)))
                            {
                                if (bCopyFiles)
                                {
                                    DirectoryInfo dirInfo = new DirectoryInfo(recordPath);

                                    if (dirInfo != null)
                                    {
                                        files = dirInfo.GetFiles();

                                        nrFiles = files == null ? 0 : files.Length;

                                        if (nrFiles == 0)
                                        {
                                            bContinue = false;
                                            CProgram.sPromptWarning(true, title, "No files in Record folder");

                                        }
                                        else
                                        {
                                            for (int i = 0; i < nrFiles; ++i)
                                            {
                                                totalFileFize += (UInt64)files[i].Length;
                                            }

                                            if (totalFileFize > maxCopySize)
                                            {
                                                bContinue = false;
                                                CProgram.sPromptWarning(true, title, nrFiles + " files to big to copy (" + totalFileFize + ">" + maxCopySize + ")");
                                            }
                                            else
                                            {
                                                UInt32 mb = (UInt32)(nrCopies * totalFileFize / 1000000.0F + 0.999999F);
                                                if (false == CProgram.sbAskOkCancel(title, "copy record " + nrCopies + " times (" + mb + "MB)"
                                                    + (bAddToStudy ? " add to S" + studyIX : "")))
                                                {
                                                    bContinue = false;
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (false == CProgram.sbAskOkCancel(title, "duplicate record " + nrCopies + " times"
                                        + (bAddToStudy ? " add to S" + studyIX : "")))
                                    {
                                        bContinue = false;
                                    }
                                }
                                if (bContinue)
                                {
                                    mbDupRecords(rec, nrCopies, startIndex, recordPath, files, totalFileFize, bAddToStudy ? maxNrRecordsInStudy : 0);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed dup record R" + recordIX.ToString(), ex);
            }
        }

        private void toolStripButtonSkipDVX_Click(object sender, EventArgs e)
        {
            mEditDvxSkip();
        }

        private bool mbCardioLogsCallApi(out String ArResultText, out String ArUploadEcgID, out String ArAnalysisJson, CRecordMit ARecord, String ARecordPath, UInt32 ARecordIX, bool AbLogOk, bool AbLogLoop, bool AbDeleteTempZip)
        {
            bool bOk = false;
            string result = "";
            string ecgID = "";
            string analysisJson = "";

            if (CCardioLogsAPI._cbEnableCardiologs == false)
            {
                result = "Not implemented";
            }
            else
            {

                try
                {
                    if (ARecord == null || ARecord.mIndex_KEY == 0
                        || ARecordPath == null || ARecordPath.Length < 6)
                    {
                        CProgram.sLogError("Bad call to mbCardioLogsCallApi");
                    }
                    else
                    {
                        float stripLenSec = ARecord.mGetSamplesTotalTime();

                        if (stripLenSec < 10)
                        {
                            CProgram.sLogWarning("mbCardioLogsCallApi skip strip length " + stripLenSec.ToString("0.0") + " sec");
                            result = "Skipped";
                            bOk = true;
                        }
                        else if (stripLenSec >= _mCardiologsEventMin * 60 + 1)
                        {
                            CProgram.sLogWarning("mbCardioLogsCallApi skip strip length " + stripLenSec.ToString("0.0") + " sec >= " + _mCardiologsEventMin + " min.");
                            result = "Skipped";
                            bOk = true;
                        }
                        else
                        {
                            string name = "PA_R" + ARecordIX.ToString("000000");
                            string heaFilePath = Path.Combine(ARecordPath, name + ".hea");

                            bOk = ARecord.mbSaveMIT(heaFilePath, 16, false);

                            if (bOk)
                            {
                                CCardioLogsAPI clApi = new CCardioLogsAPI(_mCardioLogsUrl, _mCardioLogsToken, 30, 1, 20, 10);

                                if (clApi != null)
                                {
                                    bOk = clApi.mbCallApi(out result, out ecgID, out analysisJson, heaFilePath, ARecordIX, AbLogOk, AbLogLoop, AbDeleteTempZip);
                                }
                            }
                            if (bOk && AbDeleteTempZip)
                            {
                                string datFilePath = Path.Combine(ARecordPath, name + ".dat");

                                try
                                {
                                    File.Delete(heaFilePath);
                                }
                                catch (Exception)
                                {
                                }
                                try
                                {
                                    File.Delete(datFilePath);
                                }
                                catch (Exception)
                                {
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Failed CardioLogCall R" + ARecordIX.ToString() + " R" + ARecordIX, ex);
                }
            }
            ArResultText = result;
            ArUploadEcgID = ecgID;
            ArAnalysisJson = analysisJson;

            return bOk;
        }

        private bool mbDoPreAnalysis(bool AbMethodEnabled, String APaMethod, CRecordMit ARecord, String ARecordPath)
        {
            bool bOk = false;
            string method = APaMethod == null ? "" : APaMethod.ToLower();
            UInt32 recordIX = ARecord == null ? 0 : ARecord.mIndex_KEY;

            if (recordIX == 0)
            {
                CProgram.sLogError("DoPreAnalysis(" + APaMethod + ") no Record index ");
            }
            else if(AbMethodEnabled && method.Length  > 0)
            {
                bool bDoPa = ARecord.mImportMethod == (UInt16)DImportMethod.Automatic;

                if( false == bDoPa)
                {
                    bDoPa = CProgram.sbAskYesNo("PreAnalysis R" + recordIX.ToString("00000" ), "Do PA " + APaMethod);
                }
                if (bDoPa)
                {
                    DateTime startUTC = DateTime.UtcNow;
                    string paLine = "";

                    if (method == "cardiologs")
                    {
                        string result = "";
                        string ecgID = "";
                        string analysisJson = "";

                        bool bDelZip = false == _mbLogExtra;

                        bOk = mbCardioLogsCallApi(out result, out ecgID, out analysisJson, ARecord, ARecordPath, recordIX, _mbLogExtra, false, bDelZip);

                        method += "(" + ecgID + ")";
                        if (bOk)
                        {
                            paLine = result;

/*                            string abnormalities = "";

                            bOk = CardioLogsAPI.sbParseResultToString(out abnormalities, analysisJson, "abnormalities"); // "abnormalities": ["pause", "av_block", "other_svt"],
                            if (bOk)
                            {
                                paLine = 

                            }
*/                        }
                        else
                        {
                            paLine = "failed";
                        }
                    }
                    if (paLine.Length > 0)  // add to remark and update record
                    {
                        string remark = ARecord.mRecRemark.mDecrypt();

                        string newRemark = "X=" + paLine;

                        if( remark != null && remark.Length > 0)
                        {
                            newRemark += "\r\n";
                        }
                        ARecord.mRecRemark.mbEncrypt(newRemark + remark);

                        ARecord.mbDoSqlUpdate(CRecordMit.sGetMask( (UInt16)DRecordMitVars.RecRemark_ES ), true);
                    }
                    double sec = (DateTime.UtcNow - startUTC).TotalSeconds;

                    string stripInfo = "(" + ARecord.mNrSignals + "x" + ARecord.mGetSamplesTotalTime().ToString("0.0") + "@" + ARecord.mSampleFrequency + ")";
                    if( bOk )
                    {
                        CProgram.sLogLine("PA " + method + stripInfo +" in " + sec.ToString("0.0") + " sec:" + paLine);

                    }
                    else
                    {
                        CProgram.sLogWarning("PA " + method + stripInfo + " failed in " + sec.ToString("0.0") + " sec:" + paLine);
                    }
                }
            }
            return bOk;
        }

            private bool mbCardioLogsCallApi(out String ArResultText, out String ArUploadEcgID, out String ArAnalysisJson, String AFilePath, UInt32 ARecordIX, bool AbLogLoop, bool AbDeleteTempZip)
        {
            bool bOk = false;
            string result = "";
            string ecgID = "";
            string analysisJson = "";

            try
            {
                CCardioLogsAPI clApi = new CCardioLogsAPI(_mCardioLogsUrl, _mCardioLogsToken, 30, 1, 20, 10);

                if( clApi != null )
                {
                    bOk = clApi.mbCallApi(out result, out ecgID, out analysisJson, AFilePath, ARecordIX, true, AbLogLoop, AbDeleteTempZip);                    
                }

            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed CardioLogCall R" + ARecordIX.ToString() +  " " + AFilePath, ex);
            }


            ArResultText = result;
            ArUploadEcgID = ecgID;
            ArAnalysisJson = analysisJson;

            return bOk;
        }


        private void heaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mCarioLogsHea();
        }
        private void mCarioLogsHea()
        {
            if (!mbCheckDaysLeft())
            {
                return;
            }
            if (_mbRunningConversion || _mbRunLoop)
            {
                CProgram.sLogLine("File conversion already running, press stop first!");
            }
            else
            {
                mSetRun(false);

                openHeaFileDialog.Title = "Select MIT HEA File";

                //openHeaFileDialog.RestoreDirectory = true;
                openHeaFileDialog.InitialDirectory = _mHeaFromDir;
                if (false == mbIsRunNamePresent())
                {
                    openHeaFileDialog.FileName = _mHeaFromDir;
                }
                else
                {
                    openHeaFileDialog.FileName = Path.Combine(_mHeaFromDir, mGetRunFileName());
                }

                if (openHeaFileDialog.ShowDialog() == DialogResult.OK)
                {
                    int nFiles = openHeaFileDialog.FileNames == null ? 0 : openHeaFileDialog.FileNames.Length;

                    if (nFiles > 0)
                    {
                        _mbRunningConversion = true;
                        CProgram.sSetBusy(true);

                        foreach (String file in openHeaFileDialog.FileNames)
                        {
                            string result = "";
                            string ecgID = "";
                            string analysisJson = "";
                            bool bLogLoop = true;
                            bool bDeleteTempZip = true; 

                            CProgram.sLogLine("Test CardioLogs file " + file);

                            mbCardioLogsCallApi(out result, out ecgID, out analysisJson, file, 0, bLogLoop, bDeleteTempZip);
                        }

                        CProgram.sSetBusy(false);
                        _mbRunningConversion = false;

                    }
                }
            }
        }

        private void openHeaFileDialog_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void scpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mCardioLogsScp();
        }
        private void mCardioLogsScp()
        {
            if (!mbCheckDaysLeft())
            {
                return;
            }
            if (_mbRunningConversion || _mbRunLoop)
            {
                CProgram.sLogLine("File conversion already running, press stop first!");
            }
            else
            {
                mSetRun(false);

                openScpFileDialog.Title = "Select SCP File";

                openHeaFileDialog.RestoreDirectory = true;
                openScpFileDialog.InitialDirectory = _mScpFromDir;
                if (false == mbIsRunNamePresent())
                {
                    openHeaFileDialog.FileName = _mScpFromDir;
                }
                else
                {
                    openScpFileDialog.FileName = Path.Combine(_mTzFromDir, mGetRunFileName());
                }

                if (openScpFileDialog.ShowDialog() == DialogResult.OK)
                {
                    int nFiles = openScpFileDialog.FileNames == null ? 0 : openScpFileDialog.FileNames.Length;

                    if (nFiles > 0)
                    {
                        _mbRunningConversion = true;
                        CProgram.sSetBusy(true);

                        foreach (String file in openScpFileDialog.FileNames)
                        {
                            string result = "";
                            string ecgID = "";
                            string analysisJson = "";
                            bool bLogLoop = true;
                            bool bDeleteTempZip = true; ;

                            CProgram.sLogLine("Test CardioLogs file " + file);

                            mbCardioLogsCallApi(out result, out ecgID, out analysisJson, file, 0, bLogLoop, bDeleteTempZip);
                        }

                        CProgram.sSetBusy(false);
                        _mbRunningConversion = false;
                    }
                }
            }
        }


        private void toolStripButtonPaTZ_Click(object sender, EventArgs e)
        {
            bool bEnabled = _mbDoPaTZ;

            if( CProgram.sbReqBool("TZ PreAnalysis " + _mTzPaMethod, "Enalbe PreAnalysis " + _mTzPaMethod, ref bEnabled))
            {
                _mbDoPaTZ = bEnabled;
            }
            mUpdatePaLabels();
        }

        private void toolStripButtonPaSir_Click(object sender, EventArgs e)
        {
            bool bEnabled = _mbDoPaSirona;

            if (CProgram.sbReqBool("Sirona PreAnalysis " + _mSironaPaMethod, "Enalbe PreAnalysis " + _mSironaPaMethod, ref bEnabled))
            {
                _mbDoPaSirona = bEnabled;
            }
            mUpdatePaLabels();

        }

        private void toolStripButtonPaDV2_Click(object sender, EventArgs e)
        {
            bool bEnabled = _mbDoPaDV2;

            if (CProgram.sbReqBool("DV2 PreAnalysis " + _mDv2PaMethod, "Enalbe PreAnalysis " + _mDv2PaMethod, ref bEnabled))
            {
                _mbDoPaDV2 = bEnabled;
            }
            mUpdatePaLabels();

        }

        private void toolStripButtonPaDVX_Click(object sender, EventArgs e)
        {
            bool bEnabled = _mbDoPaDVX;

            if (CProgram.sbReqBool("DV2 PreAnalysis " + _mDvxPaMethod, "Enalbe PreAnalysis " + _mDvxPaMethod, ref bEnabled))
            {
                _mbDoPaDVX = bEnabled;
            }
            mUpdatePaLabels();
        }

        private void toolStripButtonFixEnd_Click(object sender, EventArgs e)
        {
            _mbCursorFixEnd = !_mbCursorFixEnd;
            toolStripButtonFixEnd.Text = _mbCursorFixEnd ? "√" : ".";
        }

        private void convertNonin7ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mDoMenuConvertNonin7();
        }
        public bool mbConvertNonin7File(string AHeader, string AFilePath, UInt32 AStudyNr, string[] AMergeFiles, DImportMethod AImportMethod, bool AbUseStudyFolder, bool AbCheckSkip)
        {
            bool bOk = false;
            bool bError = false;

            try
            {
                bool bWriteCsv = true; // CProgram.sbCtrlKeyPressed();

                CSpO2Nonin spo2Nonin = new CSpO2Nonin(bWriteCsv);

                if (null != spo2Nonin)
                {
                    CRecordMit rec = spo2Nonin.mReadOneFile(AFilePath);

                    bOk = rec != null;

                    if( rec !=null )
                    {
                        spo2Nonin.mbWriteMit16(rec);
                    }
                    spo2Nonin.mClose();
                }
            }
            catch (Exception e)
            {
                CProgram.sLogException("Failed to convert Nonin file " + AFilePath, e);
                bError = true;
            }
            return bOk;
        }
        private void mDoMenuConvertNonin7()
        {
            if (!mbCheckDaysLeft())
            {
                return;
            }
            if (_mbRunningConversion || _mbRunLoop)
            {
                CProgram.sLogLine("File conversion already running, press stop first!");
            }
            else
            {
                mSetRun(false);

                UInt32 dvxStudyNR = 0;
                bool bMergeFiles = false;// AbMergeFiles;
                bool bUseStudyFolder = true;
                string baseFolder = "";
                string baseName = "";
                string sourceName = "Nonin7";
                string fileExtention = ".nonin";
                string title = "Convert " + sourceName + " (0=Import folder)";
                bool bOk = false;


                if (CProgram.sbReqUInt32(title, "DVX Study nr", ref dvxStudyNR, "", 0, Int32.MaxValue))
                {
                    if (dvxStudyNR == 0)
                    {
                        baseFolder = _mDVXImportDir == null || _mDVXImportDir.Length < 2 ? _mRecordingDir : _mDVXImportDir;
                        baseName = "Import";
                        bUseStudyFolder = false;
                        bOk = true;
                    }
                    else
                    {
                        bOk = CDvxEvtRec.sbGetStudyDeviceDataFolder(out baseFolder, dvxStudyNR, 0);
                        if (false == bOk)
                        {
                            CProgram.sAskWarning(title, "Unknown DVX Study " + dvxStudyNR.ToString());
                        }
                        else
                        {
                            baseName = "dvxStudy " + dvxStudyNR.ToString();
                        }
                    }
                    if (bOk)
                    {
                        title = "Select " + sourceName + " " + baseName;
                        if (bMergeFiles)
                        {
                            title += " (Merging)";
                        }
                        openFileDialogDVX.Title = title;
                        openFileDialogDVX.Filter = sourceName + "|*" + fileExtention + "|All|*.*";
                        openFileDialogDVX.FilterIndex = 1;
                        openFileDialogDVX.InitialDirectory = baseFolder;
                        if (false == mbIsRunNamePresent())
                        {
                            openFileDialogDVX.FileName = baseFolder;
                        }
                        else
                        {
                            string name = Path.ChangeExtension(mGetRunFileName(), fileExtention);
                            openFileDialogDVX.FileName = Path.Combine(baseFolder, name);
                        }


                        if (openFileDialogDVX.ShowDialog() == DialogResult.OK)
                        {
                            string[] files = openFileDialogDVX.FileNames;
                            int nFiles = files == null ? 0 : files.Length;

                            if (nFiles > 0)
                            {
                                string firstFile = files[0];
                                string copyStudy = "";

                                if (bUseStudyFolder)
                                {
                                    string s1 = baseFolder.ToLower();
                                    string s2 = firstFile.ToLower();
                                    bUseStudyFolder = s2.StartsWith(s1); // check if files are loaded from selected study
                                }
                                if (bUseStudyFolder)
                                {
                                    if (false == CProgram.sbReqBool("Import " + sourceName, "Keep Files in Study Folder (no Copy)", ref bUseStudyFolder))
                                    {
                                        nFiles = 0;
                                    }
                                }
                                if (nFiles > 0 ) // single convert only && ( nFiles == 1 || CProgram.sbReqBool(title, "Merge " + nFiles + " files", ref bMergeFiles)))
                                {
                                    if (bUseStudyFolder)
                                    {
                                        copyStudy = "copy dvxS" + dvxStudyNR.ToString();
                                    }
                                    
                                    if (bMergeFiles)
                                    {
                                        _mbRunningConversion = true;
                                        CProgram.sSetBusy(true);

                                        string header = "Nonin7 import(" + copyStudy + ") merge " + nFiles.ToString();
                                        mbConvertNonin7File(header, firstFile, dvxStudyNR, files, DImportMethod.Manual, bUseStudyFolder, false);

                                        CProgram.sSetBusy(false);
                                        _mbRunningConversion = false;
                                    }
                                    else
                                    {
                                        _mbRunningConversion = true;
                                        CProgram.sSetBusy(true);
                                        UInt32 i = 0;

                                        foreach (String file in files)
                                        {
                                            ++i;
                                            string header = "Nonin7 convert(" + copyStudy + ") " + i.ToString() + " / " + nFiles.ToString();
                                            mbConvertNonin7File(header, file, dvxStudyNR, null, DImportMethod.Manual, bUseStudyFolder, false);
                                        }
                                        CProgram.sSetBusy(false);
                                        _mbRunningConversion = false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void mButtonDvxRevHea()
        {
            if (!mbCheckDaysLeft())
            {
                return;
            }
            if (_mbRunningConversion || _mbRunLoop)
            {
                CProgram.sLogLine("File conversion already running, press stop first!");
            }
            else
            {
                mSetRun(false);

                //openHeaFileDialog.RestoreDirectory = true;
                openFileDialogDat.InitialDirectory = _mHeaFromDir;
                if (false == mbIsRunNamePresent())
                {
                    openFileDialogDat.FileName = _mHeaFromDir;
                }
                else
                {
                    openFileDialogDat.FileName = Path.Combine(_mHeaFromDir, mGetRunFileName());
                }

                if (openFileDialogDat.ShowDialog() == DialogResult.OK)
                {
                    int nFiles = openFileDialogDat.FileNames == null ? 0 : openFileDialogDat.FileNames.Length;
                    if (nFiles > 0)
                    {
                        UInt16 nrBytes = 3;

                        if (CProgram.sbReqUInt16("Reverse HEA data files", "nr bytes", ref nrBytes, "", 1, 4))
                        {
                            _mbRunningConversion = true;
                            CProgram.sSetBusy(true);
                            string altFile = openFileDialogDat.FileNames[0];
                            foreach (String file in openFileDialogDat.FileNames)
                            {
                                mRevHeaFile(file, nrBytes, altFile);
                            }
                            CProgram.sSetBusy(false);
                            _mbRunningConversion = false;
                        }
                    }
                }
            }
        }

        private void importDVXSpO2ToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            mDoMenuImportDvxSpO2();
        }
        private void mDoMenuImportDvxSpO2()
        {
            if (!mbCheckDaysLeft())
            {
                return;
            }
            if (_mbRunningConversion || _mbRunLoop)
            {
                CProgram.sLogLine("File conversion already running, press stop first!");
            }
            else
            {
                mSetRun(false);

                UInt32 dvxStudyNR = 0;
                bool bMergeFiles = false;// AbMergeFiles;
                bool bUseStudyFolder = true;
                string baseFolder = "";
                string baseName = "";
                string sourceName = "DVX SpO2";
                string fileExtention = ".spo";
                string title = "Import " + sourceName + " (0=Import folder)";
                bool bOk = false;


                if (CProgram.sbReqUInt32(title, "DVX Study nr", ref dvxStudyNR, "", 0, Int32.MaxValue))
                {
                    if (dvxStudyNR == 0)
                    {
                        baseFolder = _mDVXImportDir == null || _mDVXImportDir.Length < 2 ? _mRecordingDir : _mDVXImportDir;
                        baseName = "Import";
                        bUseStudyFolder = false;
                        bOk = true;
                    }
                    else
                    {
                        bOk = CDvxEvtRec.sbGetStudyDeviceDataFolder(out baseFolder, dvxStudyNR, 0);
                        if (false == bOk)
                        {
                            CProgram.sAskWarning(title, "Unknown DVX Study " + dvxStudyNR.ToString());
                        }
                        else
                        {
                            baseName = "dvxStudy " + dvxStudyNR.ToString();
                        }
                    }
                    if (bOk)
                    {
                        title = "Select " + sourceName + " " + baseName;
                        if (bMergeFiles)
                        {
                            title += " (Merging)";
                        }
                        openFileDialogDVX.Title = title;
                        openFileDialogDVX.Filter = sourceName + "|*" + fileExtention + "|All|*.*";
                        openFileDialogDVX.FilterIndex = 1;
                        openFileDialogDVX.InitialDirectory = baseFolder;
                        if (false == mbIsRunNamePresent())
                        {
                            openFileDialogDVX.FileName = baseFolder;
                        }
                        else
                        {
                            string name = Path.ChangeExtension(mGetRunFileName(), fileExtention);
                            openFileDialogDVX.FileName = Path.Combine(baseFolder, name);
                        }


                        if (openFileDialogDVX.ShowDialog() == DialogResult.OK)
                        {
                            string[] files = openFileDialogDVX.FileNames;
                            int nFiles = files == null ? 0 : files.Length;

                            if (nFiles > 0)
                            {
                                string firstFile = files[0];
                                string copyStudy = "";

                                if (bUseStudyFolder)
                                {
                                    string s1 = baseFolder.ToLower();
                                    string s2 = firstFile.ToLower();
                                    bUseStudyFolder = s2.StartsWith(s1); // check if files are loaded from selected study
                                }
/*                                if (bUseStudyFolder)
                                {
                                    if (false == CProgram.sbReqBool("Import " + sourceName, "Keep Files in Study Folder (no Copy)", ref bUseStudyFolder))
                                    {
                                        nFiles = 0;
                                    }
                               }
*/                                bMergeFiles = nFiles > 1;
                                if (nFiles > 0 && (nFiles == 1 || CProgram.sbReqBool(title, "Merge " + nFiles + " files", ref bMergeFiles)))
                                {
                                    if (bUseStudyFolder)
                                    {
                                        copyStudy = "copy dvxS" + dvxStudyNR.ToString();
                                    }

                                    if (bMergeFiles)
                                    {
                                        _mbRunningConversion = true;
                                        CProgram.sSetBusy(true);

                                        string header = "DVX SpO2 import(" + copyStudy + ") merge " + nFiles.ToString();
                                        mbImportOneDVXSpO2File(header, firstFile, dvxStudyNR, files, DImportMethod.Manual, bUseStudyFolder, dvxStudyNR);

                                        CProgram.sSetBusy(false);
                                        _mbRunningConversion = false;
                                    }
                                    else
                                    {
                                        _mbRunningConversion = true;
                                        CProgram.sSetBusy(true);
                                        UInt32 i = 0;

                                        foreach (String file in files)
                                        {
                                            ++i;
                                            string header = "DVX SpO2 import(" + copyStudy + ") " + i.ToString() + " / " + nFiles.ToString();
                                            mbImportOneDVXSpO2File(header, file, dvxStudyNR, null, DImportMethod.Manual, bUseStudyFolder, dvxStudyNR);
                                        }
                                        CProgram.sSetBusy(false);
                                        _mbRunningConversion = false;
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }

        private void convertDVXSpO2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mDoMenuConvertDvxSpO2();
        }
        public bool mbConvertOneDVXSpO2File(string AHeader, string AFilePath, UInt32 AStudyNr, string[] AMergeFiles, DImportMethod AImportMethod, bool AbUseStudyFolder, bool AbCheckSkip)
        {
            bool bOk = false;
            bool bError = false;

            try
            {
                bool bWriteCsv = AMergeFiles == null; // CProgram.sbCtrlKeyPressed();
                bool bWriteMit16 = true; // CProgram.sbCtrlKeyPressed();
                float fillGapSec = 4.0F;

                CDvxPleth dvxSpO2 = new CDvxPleth(bWriteCsv, bWriteMit16);

                if (null != dvxSpO2)
                {
                    CRecordMit rec = null;

                    if (AMergeFiles != null)
                    {
                        rec = dvxSpO2.mReadMultipleFiles(AMergeFiles, fillGapSec);
                    }
                    else
                    {
                       rec = dvxSpO2.mReadOneFile(AFilePath);
                    }
                    

                    bOk = rec != null;

                    if (bOk && bWriteMit16)
                    {
                        dvxSpO2.mbWriteMit16(rec);
                    }

                    dvxSpO2.mClose();
                }
            }
            catch (Exception e)
            {
                CProgram.sLogException("Failed to convert DVX SpO2 file " + AFilePath, e);
                bError = true;
            }
            return bOk;
        }
            private void mDoMenuConvertDvxSpO2()
        {
            if (!mbCheckDaysLeft())
            {
                return;
            }
            if (_mbRunningConversion || _mbRunLoop)
            {
                CProgram.sLogLine("File conversion already running, press stop first!");
            }
            else
            {
                mSetRun(false);

                UInt32 dvxStudyNR = 0;
                bool bMergeFiles = false;// AbMergeFiles;
                bool bUseStudyFolder = true;
                string baseFolder = "";
                string baseName = "";
                string sourceName = "DVX SpO2";
                string fileExtention = ".spo";
                string title = "Convert " + sourceName + " (0=Import folder)";
                bool bOk = false;


                if (CProgram.sbReqUInt32(title, "DVX Study nr", ref dvxStudyNR, "", 0, Int32.MaxValue))
                {
                    if (dvxStudyNR == 0)
                    {
                        baseFolder = _mDVXImportDir == null || _mDVXImportDir.Length < 2 ? _mRecordingDir : _mDVXImportDir;
                        baseName = "Convert";
                        bUseStudyFolder = false;
                        bOk = true;
                    }
                    else
                    {
                        bOk = CDvxEvtRec.sbGetStudyDeviceDataFolder(out baseFolder, dvxStudyNR, 0);
                        if (false == bOk)
                        {
                            CProgram.sAskWarning(title, "Unknown DVX Study " + dvxStudyNR.ToString());
                        }
                        else
                        {
                            baseName = "dvxStudy " + dvxStudyNR.ToString();
                        }
                    }
                    if (bOk)
                    {
                        title = "Select " + sourceName + " " + baseName;
                        if (bMergeFiles)
                        {
                            title += " (Merging)";
                        }
                        openFileDialogDVX.Title = title;
                        openFileDialogDVX.Filter = sourceName + "|*" + fileExtention + "|All|*.*";
                        openFileDialogDVX.FilterIndex = 1;
                        openFileDialogDVX.InitialDirectory = baseFolder;
                        if (false == mbIsRunNamePresent())
                        {
                            openFileDialogDVX.FileName = baseFolder;
                        }
                        else
                        {
                            string name = Path.ChangeExtension(mGetRunFileName(), fileExtention);
                            openFileDialogDVX.FileName = Path.Combine(baseFolder, name);
                        }


                        if (openFileDialogDVX.ShowDialog() == DialogResult.OK)
                        {
                            string[] files = openFileDialogDVX.FileNames;
                            int nFiles = files == null ? 0 : files.Length;

                            if (nFiles > 0)
                            {
                                string firstFile = files[0];
                                string copyStudy = "";

                                if (bUseStudyFolder)
                                {
                                    string s1 = baseFolder.ToLower();
                                    string s2 = firstFile.ToLower();
                                    bUseStudyFolder = s2.StartsWith(s1); // check if files are loaded from selected study
                                }
                                if (bUseStudyFolder)
                                {
                                    if (false == CProgram.sbReqBool("Convert " + sourceName, "Keep Files in Study Folder (no Copy)", ref bUseStudyFolder))
                                    {
                                        nFiles = 0;
                                    }
                                }
                                bMergeFiles = nFiles > 0;
                                if (nFiles > 0 && (nFiles == 1 || CProgram.sbReqBool(title, "Merge " + nFiles + " files", ref bMergeFiles)))
                                {
                                    if (bUseStudyFolder)
                                    {
                                        copyStudy = "copy dvxS" + dvxStudyNR.ToString();
                                    }

                                    if (bMergeFiles)
                                    {
                                        _mbRunningConversion = true;
                                        CProgram.sSetBusy(true);

                                        string header = "DVX SpO2 convert(" + copyStudy + ") merge " + nFiles.ToString();
                                        mbConvertOneDVXSpO2File(header, firstFile, dvxStudyNR, files, DImportMethod.Manual, bUseStudyFolder, false);

                                        CProgram.sSetBusy(false);
                                        _mbRunningConversion = false;
                                    }
                                    else
                                    {
                                        _mbRunningConversion = true;
                                        CProgram.sSetBusy(true);
                                        UInt32 i = 0;

                                        foreach (String file in files)
                                        {
                                            ++i;
                                            string header = "DVX SpO2 convert(" + copyStudy + ") " + i.ToString() + " / " + nFiles.ToString();
                                            mbConvertOneDVXSpO2File(header, file, dvxStudyNR, null, DImportMethod.Manual, bUseStudyFolder, false);
                                        }
                                        CProgram.sSetBusy(false);
                                        _mbRunningConversion = false;
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }

        private void dVXTestToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private bool mRevHeaFile(string AFilePath, UInt16 ANrBytes, string AAltFile)
        {
            bool bOk = false;

            try
            {

                string toFileName = "rev_" + Path.GetFileName(AFilePath);
                string toFilePath = Path.Combine(Path.GetDirectoryName(AFilePath), toFileName);

                string fromHeaFilePath = Path.ChangeExtension(AFilePath, ".hea");
                if (false == File.Exists(fromHeaFilePath))
                {
                    fromHeaFilePath = Path.ChangeExtension(AAltFile, ".hea");
                }

                string toFileHea = Path.ChangeExtension(toFilePath, ".hea");
                // hea
                if (File.Exists(toFileHea))
                {
                    File.Delete(toFileHea);
                }

                string lines = File.ReadAllText(fromHeaFilePath);

                lines += "#Revers " + ANrBytes.ToString() + " bytes values\r\n";

                File.WriteAllText(toFileHea, lines);

                // dat

                if (File.Exists(toFilePath))
                {
                    File.Delete(toFilePath);
                }

                using (FileStream writerDat = File.Create(toFilePath))
                {
                    byte[] data = File.ReadAllBytes(AFilePath);
                    int n = data.Length;
                    int i = 0, j;

                    while (n - i >= ANrBytes)
                    {
                        for (j = ANrBytes; --j >= 0;)
                        {
                            writerDat.WriteByte(data[i + j]);
                        }
                        i += ANrBytes;
                    }
                    CProgram.sLogLine("Reversed to file:" + toFileName);
                    writerDat.Flush();
                    writerDat.Close();
                    bOk = true;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Reverse DAT", ex);
            }
            return bOk;
        }
        private void mCheckDbCollect()
        {
            string tested = "DB?";
            DateTime nowDT = DateTime.Now;

            try
            {
                string stateDB = "!"; ;

                if (mbTestDBaseConnection(out stateDB))
                {
                    tested = "DB " + stateDB;

                    CSqlDBaseConnection db = CDvtmsData.sGetDBaseConnection();
                    uint nLimit = db != null ? db.mGetSqlRowLimit() : 0;
                    CRecordMit searchRec = new CRecordMit(db);
                    List<CSqlDataTableRow> recList = null;

                    if (searchRec != null)
                    {
                        bool bError;
                        UInt64 searchMask = 0;
                        string receivedName = searchRec.mVarGetName((UInt16)DRecordMitVars.ReceivedUTC);
                        DateTime dt = DateTime.Now;
                        string whereStr = receivedName + "<" + searchRec.mSqlDateTimeSqlString(dt);

                        if (searchRec.mbDoSqlSelectList(out bError, out recList, searchRec.mMaskValid, searchMask, false, whereStr))
                        {
                            int n = recList != null ? n = recList.Count : 0;

                            tested += " collected " + n.ToString() + " records (limit " + nLimit.ToString() + ")";
                        }
                        else
                        {
                            tested += " search failed(limit " + nLimit.ToString() + ")";
                        }
                    }
                }
                else
                {
                    tested = "!DB! " + stateDB;
                }

                double sec = (DateTime.Now - nowDT).TotalSeconds;

                tested += " in " + sec.ToString("0.000") + "sec, ";

            }
            catch (Exception)
            {

            }
            CProgram.sLogLine(tested);
        }
        public bool mbImportOneDVXSpO2File(string AHeader, string AFilePath, UInt32 AStudyNr, string[] AMergeFiles, 
            DImportMethod AImportMethod, bool AbUseStudyFolder, UInt32 ADvxStudyNr)
        {
            bool bOk = false;
            bool bError = false;
            string resultText = "";
            DateTime startTime = DateTime.Now;
            string addName = AMergeFiles == null ? "" : "_N" + AMergeFiles.Length;
            string fileName = Path.GetFileNameWithoutExtension(AFilePath) + addName;
            string logHeader = AHeader + ": " + fileName;

            try
            {
                bool bWriteCsv = false; // CProgram.sbCtrlKeyPressed();
                bool bWriteMit16 = false; // CProgram.sbCtrlKeyPressed();
                float fillGapSec = 4.0F;
                bool bCopyFiles = false == AbUseStudyFolder || AImportMethod == DImportMethod.Manual;
                double dTDataRead = 0.0;

                CDvxPleth dvxSpO2 = new CDvxPleth(bWriteCsv, bWriteMit16);

                if (null != dvxSpO2)
                {
                    CRecordMit rec = null;

                    if (AMergeFiles != null)
                    {
                        rec = dvxSpO2.mReadMultipleFiles(AMergeFiles, fillGapSec);
                    }
                    else
                    {
                        rec = dvxSpO2.mReadOneFile(AFilePath);
                    }
                    dTDataRead = (DateTime.Now - startTime).TotalSeconds;
                    bOk = rec != null;

                    if( bOk && rec.mSignals != null )
                    {
                        CSignalData signal = rec.mSignals[0];

                        if( signal == null || signal.mMinValue >= signal.mMaxValue)
                        {
                            bOk = false;
                            resultText = "Empty SpO2 signal";
                        }
                    }
                    if (bOk && bWriteMit16)
                    {
                        dvxSpO2.mbWriteMit16(rec);
                    }

                    if (bOk)
                    {
                        string storeName = "SPO_" + fileName;

                        fileName += ".spo";
                        // * make recording folder
                        string toPath, toFile = "";
                        DateTime receiveUTC = DateTime.UtcNow;

                        if (mbChangeToDateDir(out toPath, _mRecordingDir, receiveUTC) == false)
                        {
                            bError = true;
                        }
                        else
                        {

                            storeName = CSqlDBaseConnection.sSqlLimitSqlString(storeName, CRecordMit.cStoredAtMaxLen);

                            toPath = Path.Combine(toPath, storeName);
                            toFile = Path.Combine(toPath, fileName);

                            try
                            {
                                if (mbCreateDirectory(toPath))
                                {
                                    if (bCopyFiles)
                                    {

                                        if (File.Exists(toFile))
                                        {
                                            bError = true;
                                            resultText = "already exists";
                                        }
                                        else
                                        {
                                            if (false == dvxSpO2.mbWriteSpo(toFile, rec))
                                            {
                                                bError = true;
                                                resultText = "Failed write file";
                                            }
                                        }
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                CProgram.sLogException("SPO Failed create Recording folder for " + storeName, e);
                                bError = true;
                            }
                        }
                        if (bError == false)
                        {
                            string addRem = "SpO @" + CDvxPleth._cNrFramesPerSecond + " sps";
                            rec.mImportConverter = (UInt16)DImportConverter.DvxSpO;
                            rec.mImportMethod = (UInt16)AImportMethod;
                            rec.mFileName = fileName;
                            rec.mStoredAt = storeName;
                            rec.mReceivedUTC = DateTime.UtcNow;
                            rec.mRecState = (UInt16)DRecState.Received;
                            rec.mDeviceID = "DVXSPO" + dvxSpO2._mDeviceNr.ToString("000000");

                            if (ADvxStudyNr > 0)
                            {
                                addRem += "\r\ndvxS" + ADvxStudyNr.ToString() + (bCopyFiles ? "c" : "");
                            }
                            if (addRem.Length > 0)
                            {
                                rec.mRecRemark.mAddLine(addRem);
                            }
                            int newIndex;
                            DateTime startDB = DateTime.Now;
                            bOk = mbAddSqlDbRow(out newIndex, rec, AFilePath);

                            resultText = "R" + newIndex;

                            double dbSec = (DateTime.Now - startDB).TotalSeconds;

                            DateTime startStudy = DateTime.Now;
                            double studySec = 0.0;
                            double dTPlot = 0.0;
                            if (bOk && DImportMethod.Automatic == AImportMethod)
                            {
                                mbFindStudy(rec);   // neads record number to update
                                studySec = (DateTime.Now - startStudy).TotalSeconds;
                                mbSendReceivedMail(AFilePath, rec);
                            }
                            mbCreateImages(toFile, rec, out dTPlot);  // plot later, needs invert mask from device
                            mbStoreGrid(rec);
                            TimeSpan dT = DateTime.Now - startTime;

                            CProgram.sLogLine("[" + newIndex.ToString() + "]Import DvxSpO " + fileName + " in " + dT.TotalSeconds.ToString("0.00") + " Sec"
                                                          + "(" + rec.mNrSignals.ToString() + "x" + rec.mGetNrSamples().ToString() + " " + rec.mGetSamplesTotalTime().ToString("0")
                                                          + "s) ld=" + dTDataRead.ToString("0.00") + " pl=" + dTPlot.ToString("0.00")
                                                          + " db=" + dbSec.ToString("0.00") + " st=" + studySec.ToString("0.00") + " S" + rec.mStudy_IX.ToString());
                        }
                    }

                    dvxSpO2.mClose();
                }
                if (bOk)
                {
                    TimeSpan dT = DateTime.Now - startTime;
                    CProgram.sLogLine(logHeader + " imported " + resultText + " in " + dT.TotalSeconds.ToString("0.00") + " Sec.");
                    mProcessSetInfo(logHeader + " " + resultText);
                    mIncreaseTotalCount();
                }
                else if (bError)
                {
                    CProgram.sLogLine(logHeader + " error " + resultText);
                    mProcessSetInfo(logHeader + " error " + resultText);

                    mIncreaseErrorCount();
                }
                else
                {
                    mProcessSetInfo(logHeader + " - " + resultText);

                    if (_mbLogExtra)
                    {
                        CProgram.sLogLine(logHeader + " - " + resultText);
                    }
                }
            }
            catch (Exception e)
            {
                CProgram.sLogException("Failed to import DVX SpO2 file " + AFilePath, e);
                bError = true;
            }
            return bOk;
        }

    }
}
