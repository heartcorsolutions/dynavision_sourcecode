@echo off
echo Copy I-Reader Version to Local: %1
set toPath=\\192.168.0.36\SoftwareReleases\suEventSystem\suIReader\
dir %toPath%
if "%1"=="" GOTO noParam
echo Creating I-Reader Local sub folder %1
@pause

mkDir %toPath%\%1
copy version.json %toPath%\%1
copy i-Reader.exe %toPath%\%1
mkDir %toPath%\%1\Content
mkDir "%toPath%\%1\Content\0. Release notes"
mkDir "%toPath%\%1\Content\9. Extra"
mkDir "%toPath%\%1\Content\9. Extra\I-Reader_Config"
mkDir "%toPath%\%1\Content\9. Extra\Eventboard_Config"

copy c:\Projects\EventSystem\EventboardReader\i-Reader\ChangeHistoryI-Reader.rtf "%toPath%\%1\Content\0. Release notes\"
copy "c:\Projects\EventSystem\EventboardReader\i-Reader\ireader_config.rtf" "%toPath%\%1\Content\9. Extra\I-Reader_Config\ireader_config.rtf" 
copy c:\Projects\EventSystem\EventboardReader\i-Reader\i-Reader\App.config "%toPath%\%1\Content\9. Extra\I-Reader_Config\i-Reader.exe.All_config" 
copy c:\Projects\EventSystem\EventboardReader\EventBoard\EventBoard\App.config "%toPath%\%1\Content\9. Extra\Eventboard_Config\Eventboard.exe.All_config" 
copy "c:\Projects\EventSystem\EventboardReader\EventBoard\eventboard_config.rtf" "%toPath%\%1\Content\9. Extra\Eventboard_Config\eventboard_config.rtf" 

dir %toPath%\%1
echo Done copy I-Reader to Local version %1
pause
rem start %toPath%\%1\
set toFile="%toPath%\%1\Content\0. Release notes\ChangeHistoryI-Reader.rtf"
echo toFile=%toFile%
dir %toFile%
%toFile%
echo Done show I-Reader Local version %1
pause
exit
:noParam
echo no parameter given
pause
