@echo off
echo Copy I-Reader Version to Upload: %1

echo use total commander to copy from development to release!
pause
exit

set toPath="q:\DEVELOPMENT\EVENTBOARD\SOFTWARE RELEASES\I-Reader" 
dir %toPath%
if "%1"=="" GOTO noParam
echo Creating I-Reader Upload sub folder %1
@pause

mkDir %toPath%\%1
copy version.json %toPath%\%1
copy i-Reader.exe %toPath%\%1
mkDir %toPath%\%1\Content
mkDir "%toPath%\%1\Content\0. Release notes"
copy c:\Projects\EventSystem\EventboardReader\i-Reader\ChangeHistoryI-Reader.rtf "%toPath%\%1\Content\0. Release notes\"

dir %toPath%\%1
echo Done copy I-Reader to Upload version %1
pause
start %toPath%\%1\
set toFile="%toPath%\%1\Content\0. Release notes\ChangeHistoryI-Reader.rtf"
echo toFile=%toFile%
dir %toFile%
%toFile%

exit
:noParam
echo no parameter given
pause
