﻿// LicKeyGen Program 
// by Simon Vlaar 
// can be used by Techmedic Development B.V
// created 16 August 2016
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Program_Base;

namespace LicKeyGen
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            CProgram.sSetProgLogConsole(DDebugFlags.FullDebug); // AllOff);
            CProgram.sSetProgLogFile(DDebugFlags.FullDebug); // StandardLog);  // enables file logging
            CProgram.sSetProgLogScreen(DDebugFlags.FullDebug ^ DDebugFlags.Class);
            CProgram.sSetPromptFlags(DDebugFlags.FullDebug); // StandardPrompt);

            if (CProgram.sbStartProgram("LicKeyGen", 62, 10, "Techmedic", "TMI1", 0x36C281A5, 0))
            {
                try
                {
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);
                    Application.Run(new Form1());
                }
                catch (Exception Ex)
                {
                    //                    CProgram.sPromptException(false, "MainForm", "Uncaught Exception", Ex);
                    CProgram.sLogException("Uncaught Exception", Ex);
                }

                CProgram.sEndProgram();
            }
        }
    }
}
