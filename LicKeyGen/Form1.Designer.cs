﻿namespace LicKeyGen
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.TextBox textBoxNrBytes;
            System.Windows.Forms.Label label15;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripTestProgNr = new System.Windows.Forms.ToolStripButton();
            this.toolStripLoadClipboard = new System.Windows.Forms.ToolStripButton();
            this.toolStripLoadFile = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripCreateKey = new System.Windows.Forms.ToolStripButton();
            this.toolStripExportKey = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripCenters = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonTZr = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLicSirona = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLicTZ = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLicDV2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripLicDVX = new System.Windows.Forms.ToolStripButton();
            this.toolStripOpenCenter = new System.Windows.Forms.ToolStripButton();
            this.toolStripListLicenses = new System.Windows.Forms.ToolStripButton();
            this.toolStripMemTest = new System.Windows.Forms.ToolStripButton();
            this.toolStripGenID = new System.Windows.Forms.ToolStripButton();
            this.toolStripProgID = new System.Windows.Forms.ToolStripButton();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel33 = new System.Windows.Forms.Panel();
            this.textBoxMailBody = new System.Windows.Forms.TextBox();
            this.panel34 = new System.Windows.Forms.Panel();
            this.checkBoxFileBody = new System.Windows.Forms.CheckBox();
            this.label27 = new System.Windows.Forms.Label();
            this.panel23 = new System.Windows.Forms.Panel();
            this.textBoxMailTitle = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.panel27 = new System.Windows.Forms.Panel();
            this.textBoxMailFrom = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.panel26 = new System.Windows.Forms.Panel();
            this.checkBoxMailAttach1 = new System.Windows.Forms.CheckBox();
            this.textBoxMailFile1 = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.panel25 = new System.Windows.Forms.Panel();
            this.checkBoxMailAttach2 = new System.Windows.Forms.CheckBox();
            this.textBoxMailFile2 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.panel24 = new System.Windows.Forms.Panel();
            this.textBoxMailTo = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.buttonTMI = new System.Windows.Forms.Button();
            this.textBoxSmtpPW = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxSmtpUser = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.checkBoxSmtpSsl = new System.Windows.Forms.CheckBox();
            this.panel28 = new System.Windows.Forms.Panel();
            this.buttonGMail = new System.Windows.Forms.Button();
            this.buttonZiggo = new System.Windows.Forms.Button();
            this.textBoxSmtpPort = new System.Windows.Forms.TextBox();
            this.labelSmtpPort = new System.Windows.Forms.Label();
            this.textBoxSmtpHost = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel36 = new System.Windows.Forms.Panel();
            this.labelSmtpError = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.panel35 = new System.Windows.Forms.Panel();
            this.labelSmtpOk = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.buttonKillCue = new System.Windows.Forms.Button();
            this.panel32 = new System.Windows.Forms.Panel();
            this.labelSmtpClearN = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.panel31 = new System.Windows.Forms.Panel();
            this.labelSmtpSendN = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.panel30 = new System.Windows.Forms.Panel();
            this.labelSmtpCueN = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.panel29 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.buttonMSendMail = new System.Windows.Forms.Button();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel21 = new System.Windows.Forms.Panel();
            this.textBoxEncrypted = new System.Windows.Forms.TextBox();
            this.panel22 = new System.Windows.Forms.Panel();
            this.buttonSmtpPW = new System.Windows.Forms.Button();
            this.buttonSqlPW = new System.Windows.Forms.Button();
            this.buttonDecrypt = new System.Windows.Forms.Button();
            this.panel20 = new System.Windows.Forms.Panel();
            this.textBoxPlainText = new System.Windows.Forms.TextBox();
            this.buttonEncrypt = new System.Windows.Forms.Button();
            this.panel18 = new System.Windows.Forms.Panel();
            this.textBoxItemName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.radioButtonInt = new System.Windows.Forms.RadioButton();
            this.radioButtonString = new System.Windows.Forms.RadioButton();
            this.radioButtonPassword = new System.Windows.Forms.RadioButton();
            this.textBoxBytes = new System.Windows.Forms.TextBox();
            this.panel15 = new System.Windows.Forms.Panel();
            this.checkBoxGenByteSwap = new System.Windows.Forms.CheckBox();
            this.textBoxOrgHash = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxLicKey = new System.Windows.Forms.TextBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.buttonEventboard = new System.Windows.Forms.Button();
            this.buttonIReader = new System.Windows.Forms.Button();
            this.textBoxProgLast = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.textBoxProgFirst = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.buttonPcIReader = new System.Windows.Forms.Button();
            this.buttonPcEventboard = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.buttonPcText = new System.Windows.Forms.Button();
            this.buttonXlsBackupClipboard = new System.Windows.Forms.Button();
            this.textBoxNrDays = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.buttonNrDays = new System.Windows.Forms.Button();
            this.textBoxEndDate = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel16 = new System.Windows.Forms.Panel();
            this.textBoxHwStr = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.checkBoxMultiPC = new System.Windows.Forms.CheckBox();
            this.checkBoxEbMpc = new System.Windows.Forms.CheckBox();
            this.buttonNextNr = new System.Windows.Forms.Button();
            this.textBoxDevNr = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.textBoxDevID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel19 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.checkBoxDemo = new System.Windows.Forms.CheckBox();
            this.buttonLastCenter = new System.Windows.Forms.Button();
            this.textBoxOrgNr = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.buttonAddCenter = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.comboBoxCenters = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonCheckHardwareKey = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxHwKey = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.TextBoxLog = new System.Windows.Forms.TextBox();
            this.openFileDialogTZr = new System.Windows.Forms.OpenFileDialog();
            this.timerProcessSmtp = new System.Windows.Forms.Timer(this.components);
            this.openFileDialogAttachment = new System.Windows.Forms.OpenFileDialog();
            textBoxNrBytes = new System.Windows.Forms.TextBox();
            label15 = new System.Windows.Forms.Label();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel33.SuspendLayout();
            this.panel34.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel26.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel36.SuspendLayout();
            this.panel35.SuspendLayout();
            this.panel32.SuspendLayout();
            this.panel31.SuspendLayout();
            this.panel30.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxNrBytes
            // 
            textBoxNrBytes.Dock = System.Windows.Forms.DockStyle.Left;
            textBoxNrBytes.Location = new System.Drawing.Point(212, 0);
            textBoxNrBytes.Margin = new System.Windows.Forms.Padding(2);
            textBoxNrBytes.Name = "textBoxNrBytes";
            textBoxNrBytes.Size = new System.Drawing.Size(83, 20);
            textBoxNrBytes.TabIndex = 7;
            // 
            // label15
            // 
            label15.AutoSize = true;
            label15.Dock = System.Windows.Forms.DockStyle.Left;
            label15.Location = new System.Drawing.Point(150, 0);
            label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            label15.Name = "label15";
            label15.Size = new System.Drawing.Size(62, 13);
            label15.TabIndex = 6;
            label15.Text = "  NrBytes = ";
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton2,
            this.toolStripSeparator1,
            this.toolStripTestProgNr,
            this.toolStripLoadClipboard,
            this.toolStripLoadFile,
            this.toolStripSeparator4,
            this.toolStripCreateKey,
            this.toolStripExportKey,
            this.toolStripButton3,
            this.toolStripButton5,
            this.toolStripCenters,
            this.toolStripButtonTZr,
            this.toolStripSeparator3,
            this.toolStripLicSirona,
            this.toolStripLicTZ,
            this.toolStripLicDV2,
            this.toolStripLicDVX,
            this.toolStripOpenCenter,
            this.toolStripListLicenses,
            this.toolStripMemTest,
            this.toolStripGenID,
            this.toolStripProgID});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1318, 39);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton1.Text = "toolStripTestGen";
            this.toolStripButton1.ToolTipText = "Gen Hardware key from this PC";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton2.Text = "toolStripButton2";
            this.toolStripButton2.ToolTipText = "Test This PC License key";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripTestProgNr
            // 
            this.toolStripTestProgNr.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripTestProgNr.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripTestProgNr.Image = ((System.Drawing.Image)(resources.GetObject("toolStripTestProgNr.Image")));
            this.toolStripTestProgNr.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripTestProgNr.Name = "toolStripTestProgNr";
            this.toolStripTestProgNr.Size = new System.Drawing.Size(49, 36);
            this.toolStripTestProgNr.Text = "ProgNr";
            this.toolStripTestProgNr.Click += new System.EventHandler(this.toolStripTestProgNr_Click);
            // 
            // toolStripLoadClipboard
            // 
            this.toolStripLoadClipboard.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripLoadClipboard.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripLoadClipboard.Image = ((System.Drawing.Image)(resources.GetObject("toolStripLoadClipboard.Image")));
            this.toolStripLoadClipboard.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripLoadClipboard.Name = "toolStripLoadClipboard";
            this.toolStripLoadClipboard.Size = new System.Drawing.Size(36, 36);
            this.toolStripLoadClipboard.Text = "Test Hardware Key";
            this.toolStripLoadClipboard.ToolTipText = "Test Hardware Key";
            this.toolStripLoadClipboard.Click += new System.EventHandler(this.toolStripLoadClipboard_Click);
            // 
            // toolStripLoadFile
            // 
            this.toolStripLoadFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripLoadFile.Image = ((System.Drawing.Image)(resources.GetObject("toolStripLoadFile.Image")));
            this.toolStripLoadFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripLoadFile.Name = "toolStripLoadFile";
            this.toolStripLoadFile.Size = new System.Drawing.Size(36, 36);
            this.toolStripLoadFile.Text = "Load File";
            this.toolStripLoadFile.ToolTipText = "Load Hardware Key from Clipboard";
            this.toolStripLoadFile.Click += new System.EventHandler(this.toolStripLoadFile_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripCreateKey
            // 
            this.toolStripCreateKey.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripCreateKey.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripCreateKey.Image = ((System.Drawing.Image)(resources.GetObject("toolStripCreateKey.Image")));
            this.toolStripCreateKey.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripCreateKey.Name = "toolStripCreateKey";
            this.toolStripCreateKey.Size = new System.Drawing.Size(36, 36);
            this.toolStripCreateKey.Text = "Create License Key";
            this.toolStripCreateKey.Click += new System.EventHandler(this.toolStripCreateKey_Click);
            // 
            // toolStripExportKey
            // 
            this.toolStripExportKey.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripExportKey.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripExportKey.Image = ((System.Drawing.Image)(resources.GetObject("toolStripExportKey.Image")));
            this.toolStripExportKey.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripExportKey.Name = "toolStripExportKey";
            this.toolStripExportKey.Size = new System.Drawing.Size(36, 36);
            this.toolStripExportKey.Text = "Export Key";
            this.toolStripExportKey.Visible = false;
            this.toolStripExportKey.Click += new System.EventHandler(this.toolStripExportKey_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton3.Text = "toolStripButton3";
            this.toolStripButton3.ToolTipText = "Test PC License";
            this.toolStripButton3.Visible = false;
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton5.Image")));
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton5.Text = "toolStripButton5";
            this.toolStripButton5.Visible = false;
            this.toolStripButton5.Click += new System.EventHandler(this.toolStripButton5_Click);
            // 
            // toolStripCenters
            // 
            this.toolStripCenters.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripCenters.Image = ((System.Drawing.Image)(resources.GetObject("toolStripCenters.Image")));
            this.toolStripCenters.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripCenters.Name = "toolStripCenters";
            this.toolStripCenters.Size = new System.Drawing.Size(36, 36);
            this.toolStripCenters.Text = "toolStripButton4";
            this.toolStripCenters.ToolTipText = "Create centers";
            this.toolStripCenters.Visible = false;
            this.toolStripCenters.Click += new System.EventHandler(this.toolStripCenters_Click);
            // 
            // toolStripButtonTZr
            // 
            this.toolStripButtonTZr.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonTZr.Checked = true;
            this.toolStripButtonTZr.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolStripButtonTZr.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonTZr.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonTZr.Image")));
            this.toolStripButtonTZr.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonTZr.Name = "toolStripButtonTZr";
            this.toolStripButtonTZr.Size = new System.Drawing.Size(28, 36);
            this.toolStripButtonTZr.Text = "TZr";
            this.toolStripButtonTZr.Visible = false;
            this.toolStripButtonTZr.Click += new System.EventHandler(this.toolStripButtonTZr_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripLicSirona
            // 
            this.toolStripLicSirona.Name = "toolStripLicSirona";
            this.toolStripLicSirona.Size = new System.Drawing.Size(82, 36);
            this.toolStripLicSirona.Text = "License Sirona";
            this.toolStripLicSirona.ToolTipText = "Create License key Sirona";
            this.toolStripLicSirona.Click += new System.EventHandler(this.toolStripLicSirona_Click);
            // 
            // toolStripLicTZ
            // 
            this.toolStripLicTZ.Name = "toolStripLicTZ";
            this.toolStripLicTZ.Size = new System.Drawing.Size(63, 36);
            this.toolStripLicTZ.Text = "Licence TZ";
            this.toolStripLicTZ.ToolTipText = "Create License key TZ";
            this.toolStripLicTZ.Click += new System.EventHandler(this.toolStripLicTZ_Click);
            // 
            // toolStripLicDV2
            // 
            this.toolStripLicDV2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripLicDV2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripLicDV2.Image")));
            this.toolStripLicDV2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripLicDV2.Name = "toolStripLicDV2";
            this.toolStripLicDV2.Size = new System.Drawing.Size(74, 36);
            this.toolStripLicDV2.Text = "License DV2";
            this.toolStripLicDV2.ToolTipText = "Create License key DV2";
            this.toolStripLicDV2.Click += new System.EventHandler(this.toolStripLicDV2_Click);
            // 
            // toolStripLicDVX
            // 
            this.toolStripLicDVX.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripLicDVX.Image = ((System.Drawing.Image)(resources.GetObject("toolStripLicDVX.Image")));
            this.toolStripLicDVX.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripLicDVX.Name = "toolStripLicDVX";
            this.toolStripLicDVX.Size = new System.Drawing.Size(75, 36);
            this.toolStripLicDVX.Text = "License DVX";
            this.toolStripLicDVX.Click += new System.EventHandler(this.toolStripLicDVX_Click);
            // 
            // toolStripOpenCenter
            // 
            this.toolStripOpenCenter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripOpenCenter.Image = ((System.Drawing.Image)(resources.GetObject("toolStripOpenCenter.Image")));
            this.toolStripOpenCenter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripOpenCenter.Name = "toolStripOpenCenter";
            this.toolStripOpenCenter.Size = new System.Drawing.Size(36, 36);
            this.toolStripOpenCenter.Text = "toolStripButton4";
            this.toolStripOpenCenter.ToolTipText = "Open Center Org. folder";
            this.toolStripOpenCenter.Click += new System.EventHandler(this.toolStripOpenCenter_Click);
            // 
            // toolStripListLicenses
            // 
            this.toolStripListLicenses.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripListLicenses.Image = ((System.Drawing.Image)(resources.GetObject("toolStripListLicenses.Image")));
            this.toolStripListLicenses.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripListLicenses.Name = "toolStripListLicenses";
            this.toolStripListLicenses.Size = new System.Drawing.Size(36, 36);
            this.toolStripListLicenses.Text = "toolStripButton4";
            this.toolStripListLicenses.ToolTipText = "List All Center devices counts";
            this.toolStripListLicenses.Click += new System.EventHandler(this.toolStripListLicenses_Click);
            // 
            // toolStripMemTest
            // 
            this.toolStripMemTest.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripMemTest.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripMemTest.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMemTest.Image")));
            this.toolStripMemTest.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripMemTest.Name = "toolStripMemTest";
            this.toolStripMemTest.Size = new System.Drawing.Size(58, 36);
            this.toolStripMemTest.Text = "Memtest";
            this.toolStripMemTest.Click += new System.EventHandler(this.toolStripMemTest_Click);
            // 
            // toolStripGenID
            // 
            this.toolStripGenID.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripGenID.Image = ((System.Drawing.Image)(resources.GetObject("toolStripGenID.Image")));
            this.toolStripGenID.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripGenID.Name = "toolStripGenID";
            this.toolStripGenID.Size = new System.Drawing.Size(36, 36);
            this.toolStripGenID.Text = "toolStripButton4";
            this.toolStripGenID.ToolTipText = "Generate sequence of serial numbers";
            this.toolStripGenID.Click += new System.EventHandler(this.toolStripGenID_Click);
            // 
            // toolStripProgID
            // 
            this.toolStripProgID.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripProgID.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripProgID.Image = ((System.Drawing.Image)(resources.GetObject("toolStripProgID.Image")));
            this.toolStripProgID.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripProgID.Name = "toolStripProgID";
            this.toolStripProgID.Size = new System.Drawing.Size(47, 36);
            this.toolStripProgID.Text = "ProgID";
            this.toolStripProgID.ToolTipText = "Decript ProgID to ProgNr, version";
            this.toolStripProgID.Click += new System.EventHandler(this.toolStripProgID_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 39);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(2);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel6);
            this.splitContainer1.Panel1.Controls.Add(this.panel14);
            this.splitContainer1.Panel1.Controls.Add(this.textBoxBytes);
            this.splitContainer1.Panel1.Controls.Add(this.panel15);
            this.splitContainer1.Panel1.Controls.Add(this.panel13);
            this.splitContainer1.Panel1.Controls.Add(this.panel7);
            this.splitContainer1.Panel1.Controls.Add(this.panel8);
            this.splitContainer1.Panel1.Controls.Add(this.panel17);
            this.splitContainer1.Panel1.Controls.Add(this.panel5);
            this.splitContainer1.Panel1.Controls.Add(this.panel2);
            this.splitContainer1.Panel1.Controls.Add(this.panel16);
            this.splitContainer1.Panel1.Controls.Add(this.panel4);
            this.splitContainer1.Panel1.Controls.Add(this.panel19);
            this.splitContainer1.Panel1.Controls.Add(this.panel3);
            this.splitContainer1.Panel1.Controls.Add(this.panel1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.TextBoxLog);
            this.splitContainer1.Panel2.Controls.Add(this.textBox1);
            this.splitContainer1.Size = new System.Drawing.Size(1318, 676);
            this.splitContainer1.SplitterDistance = 557;
            this.splitContainer1.SplitterWidth = 3;
            this.splitContainer1.TabIndex = 1;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.panel12);
            this.panel6.Controls.Add(this.panel11);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(0, 324);
            this.panel6.Margin = new System.Windows.Forms.Padding(2);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(557, 220);
            this.panel6.TabIndex = 7;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.panel33);
            this.panel12.Controls.Add(this.panel23);
            this.panel12.Controls.Add(this.panel27);
            this.panel12.Controls.Add(this.panel26);
            this.panel12.Controls.Add(this.panel25);
            this.panel12.Controls.Add(this.panel24);
            this.panel12.Controls.Add(this.panel9);
            this.panel12.Controls.Add(this.panel10);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel12.Location = new System.Drawing.Point(62, 0);
            this.panel12.Margin = new System.Windows.Forms.Padding(2);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(495, 220);
            this.panel12.TabIndex = 1;
            // 
            // panel33
            // 
            this.panel33.Controls.Add(this.textBoxMailBody);
            this.panel33.Controls.Add(this.panel34);
            this.panel33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel33.Location = new System.Drawing.Point(0, 123);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(495, 49);
            this.panel33.TabIndex = 19;
            // 
            // textBoxMailBody
            // 
            this.textBoxMailBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxMailBody.Location = new System.Drawing.Point(44, 0);
            this.textBoxMailBody.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxMailBody.Multiline = true;
            this.textBoxMailBody.Name = "textBoxMailBody";
            this.textBoxMailBody.Size = new System.Drawing.Size(451, 49);
            this.textBoxMailBody.TabIndex = 17;
            // 
            // panel34
            // 
            this.panel34.Controls.Add(this.checkBoxFileBody);
            this.panel34.Controls.Add(this.label27);
            this.panel34.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel34.Location = new System.Drawing.Point(0, 0);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(44, 49);
            this.panel34.TabIndex = 0;
            // 
            // checkBoxFileBody
            // 
            this.checkBoxFileBody.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.checkBoxFileBody.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkBoxFileBody.Location = new System.Drawing.Point(0, 14);
            this.checkBoxFileBody.Name = "checkBoxFileBody";
            this.checkBoxFileBody.Size = new System.Drawing.Size(44, 35);
            this.checkBoxFileBody.TabIndex = 0;
            this.checkBoxFileBody.Text = "File";
            this.checkBoxFileBody.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.checkBoxFileBody.UseVisualStyleBackColor = true;
            this.checkBoxFileBody.Visible = false;
            // 
            // label27
            // 
            this.label27.Dock = System.Windows.Forms.DockStyle.Top;
            this.label27.Location = new System.Drawing.Point(0, 0);
            this.label27.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(44, 14);
            this.label27.TabIndex = 9;
            this.label27.Text = "Body =";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel23
            // 
            this.panel23.Controls.Add(this.textBoxMailTitle);
            this.panel23.Controls.Add(this.label23);
            this.panel23.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel23.Location = new System.Drawing.Point(0, 98);
            this.panel23.Margin = new System.Windows.Forms.Padding(2);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(495, 25);
            this.panel23.TabIndex = 12;
            // 
            // textBoxMailTitle
            // 
            this.textBoxMailTitle.Dock = System.Windows.Forms.DockStyle.Left;
            this.textBoxMailTitle.Location = new System.Drawing.Point(41, 0);
            this.textBoxMailTitle.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxMailTitle.Name = "textBoxMailTitle";
            this.textBoxMailTitle.Size = new System.Drawing.Size(391, 20);
            this.textBoxMailTitle.TabIndex = 9;
            // 
            // label23
            // 
            this.label23.Dock = System.Windows.Forms.DockStyle.Left;
            this.label23.Location = new System.Drawing.Point(0, 0);
            this.label23.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(41, 25);
            this.label23.TabIndex = 8;
            this.label23.Text = "Title =";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel27
            // 
            this.panel27.Controls.Add(this.textBoxMailFrom);
            this.panel27.Controls.Add(this.label26);
            this.panel27.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel27.Location = new System.Drawing.Point(0, 74);
            this.panel27.Margin = new System.Windows.Forms.Padding(2);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(495, 24);
            this.panel27.TabIndex = 17;
            // 
            // textBoxMailFrom
            // 
            this.textBoxMailFrom.Dock = System.Windows.Forms.DockStyle.Left;
            this.textBoxMailFrom.Location = new System.Drawing.Point(41, 0);
            this.textBoxMailFrom.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxMailFrom.Name = "textBoxMailFrom";
            this.textBoxMailFrom.Size = new System.Drawing.Size(224, 20);
            this.textBoxMailFrom.TabIndex = 9;
            // 
            // label26
            // 
            this.label26.Dock = System.Windows.Forms.DockStyle.Left;
            this.label26.Location = new System.Drawing.Point(0, 0);
            this.label26.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(41, 24);
            this.label26.TabIndex = 8;
            this.label26.Text = "From =";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel26
            // 
            this.panel26.Controls.Add(this.checkBoxMailAttach1);
            this.panel26.Controls.Add(this.textBoxMailFile1);
            this.panel26.Controls.Add(this.label25);
            this.panel26.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel26.Location = new System.Drawing.Point(0, 172);
            this.panel26.Margin = new System.Windows.Forms.Padding(2);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(495, 24);
            this.panel26.TabIndex = 15;
            // 
            // checkBoxMailAttach1
            // 
            this.checkBoxMailAttach1.AutoSize = true;
            this.checkBoxMailAttach1.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxMailAttach1.Location = new System.Drawing.Point(365, 0);
            this.checkBoxMailAttach1.Margin = new System.Windows.Forms.Padding(2);
            this.checkBoxMailAttach1.Name = "checkBoxMailAttach1";
            this.checkBoxMailAttach1.Size = new System.Drawing.Size(57, 24);
            this.checkBoxMailAttach1.TabIndex = 10;
            this.checkBoxMailAttach1.Text = "Attach";
            this.checkBoxMailAttach1.UseVisualStyleBackColor = true;
            // 
            // textBoxMailFile1
            // 
            this.textBoxMailFile1.Dock = System.Windows.Forms.DockStyle.Left;
            this.textBoxMailFile1.Location = new System.Drawing.Point(44, 0);
            this.textBoxMailFile1.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxMailFile1.Name = "textBoxMailFile1";
            this.textBoxMailFile1.Size = new System.Drawing.Size(321, 20);
            this.textBoxMailFile1.TabIndex = 9;
            this.textBoxMailFile1.Text = "mailAttachment1.png";
            // 
            // label25
            // 
            this.label25.Dock = System.Windows.Forms.DockStyle.Left;
            this.label25.Location = new System.Drawing.Point(0, 0);
            this.label25.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(44, 24);
            this.label25.TabIndex = 8;
            this.label25.Text = "File1 =";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label25.Click += new System.EventHandler(this.label25_Click);
            // 
            // panel25
            // 
            this.panel25.Controls.Add(this.checkBoxMailAttach2);
            this.panel25.Controls.Add(this.textBoxMailFile2);
            this.panel25.Controls.Add(this.label24);
            this.panel25.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel25.Location = new System.Drawing.Point(0, 196);
            this.panel25.Margin = new System.Windows.Forms.Padding(2);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(495, 24);
            this.panel25.TabIndex = 14;
            // 
            // checkBoxMailAttach2
            // 
            this.checkBoxMailAttach2.AutoSize = true;
            this.checkBoxMailAttach2.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxMailAttach2.Location = new System.Drawing.Point(365, 0);
            this.checkBoxMailAttach2.Margin = new System.Windows.Forms.Padding(2);
            this.checkBoxMailAttach2.Name = "checkBoxMailAttach2";
            this.checkBoxMailAttach2.Size = new System.Drawing.Size(57, 24);
            this.checkBoxMailAttach2.TabIndex = 10;
            this.checkBoxMailAttach2.Text = "Attach";
            this.checkBoxMailAttach2.UseVisualStyleBackColor = true;
            // 
            // textBoxMailFile2
            // 
            this.textBoxMailFile2.Dock = System.Windows.Forms.DockStyle.Left;
            this.textBoxMailFile2.Location = new System.Drawing.Point(44, 0);
            this.textBoxMailFile2.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxMailFile2.Name = "textBoxMailFile2";
            this.textBoxMailFile2.Size = new System.Drawing.Size(321, 20);
            this.textBoxMailFile2.TabIndex = 9;
            this.textBoxMailFile2.Text = "mailAttachment2.png";
            // 
            // label24
            // 
            this.label24.Dock = System.Windows.Forms.DockStyle.Left;
            this.label24.Location = new System.Drawing.Point(0, 0);
            this.label24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(44, 24);
            this.label24.TabIndex = 8;
            this.label24.Text = "File 2 =";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label24.Click += new System.EventHandler(this.label24_Click);
            // 
            // panel24
            // 
            this.panel24.Controls.Add(this.textBoxMailTo);
            this.panel24.Controls.Add(this.label20);
            this.panel24.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel24.Location = new System.Drawing.Point(0, 50);
            this.panel24.Margin = new System.Windows.Forms.Padding(2);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(495, 24);
            this.panel24.TabIndex = 13;
            // 
            // textBoxMailTo
            // 
            this.textBoxMailTo.Dock = System.Windows.Forms.DockStyle.Left;
            this.textBoxMailTo.Location = new System.Drawing.Point(41, 0);
            this.textBoxMailTo.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxMailTo.Name = "textBoxMailTo";
            this.textBoxMailTo.Size = new System.Drawing.Size(224, 20);
            this.textBoxMailTo.TabIndex = 9;
            // 
            // label20
            // 
            this.label20.Dock = System.Windows.Forms.DockStyle.Left;
            this.label20.Location = new System.Drawing.Point(0, 0);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(41, 24);
            this.label20.TabIndex = 8;
            this.label20.Text = "  To =";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.buttonTMI);
            this.panel9.Controls.Add(this.textBoxSmtpPW);
            this.panel9.Controls.Add(this.label10);
            this.panel9.Controls.Add(this.textBoxSmtpUser);
            this.panel9.Controls.Add(this.label11);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 24);
            this.panel9.Margin = new System.Windows.Forms.Padding(2);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(495, 26);
            this.panel9.TabIndex = 11;
            // 
            // buttonTMI
            // 
            this.buttonTMI.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonTMI.Location = new System.Drawing.Point(445, 0);
            this.buttonTMI.Name = "buttonTMI";
            this.buttonTMI.Size = new System.Drawing.Size(50, 26);
            this.buttonTMI.TabIndex = 12;
            this.buttonTMI.Text = "TMI";
            this.buttonTMI.UseVisualStyleBackColor = true;
            this.buttonTMI.Click += new System.EventHandler(this.buttonTMI_Click);
            // 
            // textBoxSmtpPW
            // 
            this.textBoxSmtpPW.Dock = System.Windows.Forms.DockStyle.Left;
            this.textBoxSmtpPW.Location = new System.Drawing.Point(188, 0);
            this.textBoxSmtpPW.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxSmtpPW.Name = "textBoxSmtpPW";
            this.textBoxSmtpPW.Size = new System.Drawing.Size(150, 20);
            this.textBoxSmtpPW.TabIndex = 11;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Left;
            this.label10.Location = new System.Drawing.Point(149, 0);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(39, 13);
            this.label10.TabIndex = 10;
            this.label10.Text = "  pw = ";
            // 
            // textBoxSmtpUser
            // 
            this.textBoxSmtpUser.Dock = System.Windows.Forms.DockStyle.Left;
            this.textBoxSmtpUser.Location = new System.Drawing.Point(41, 0);
            this.textBoxSmtpUser.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxSmtpUser.Name = "textBoxSmtpUser";
            this.textBoxSmtpUser.Size = new System.Drawing.Size(108, 20);
            this.textBoxSmtpUser.TabIndex = 9;
            // 
            // label11
            // 
            this.label11.Dock = System.Windows.Forms.DockStyle.Left;
            this.label11.Location = new System.Drawing.Point(0, 0);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 26);
            this.label11.TabIndex = 8;
            this.label11.Text = " User =";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.checkBoxSmtpSsl);
            this.panel10.Controls.Add(this.panel28);
            this.panel10.Controls.Add(this.buttonGMail);
            this.panel10.Controls.Add(this.buttonZiggo);
            this.panel10.Controls.Add(this.textBoxSmtpPort);
            this.panel10.Controls.Add(this.labelSmtpPort);
            this.panel10.Controls.Add(this.textBoxSmtpHost);
            this.panel10.Controls.Add(this.label13);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Margin = new System.Windows.Forms.Padding(2);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(495, 24);
            this.panel10.TabIndex = 10;
            // 
            // checkBoxSmtpSsl
            // 
            this.checkBoxSmtpSsl.AutoSize = true;
            this.checkBoxSmtpSsl.Dock = System.Windows.Forms.DockStyle.Left;
            this.checkBoxSmtpSsl.Location = new System.Drawing.Point(282, 0);
            this.checkBoxSmtpSsl.Margin = new System.Windows.Forms.Padding(2);
            this.checkBoxSmtpSsl.Name = "checkBoxSmtpSsl";
            this.checkBoxSmtpSsl.Size = new System.Drawing.Size(46, 24);
            this.checkBoxSmtpSsl.TabIndex = 8;
            this.checkBoxSmtpSsl.Text = "SSL";
            this.checkBoxSmtpSsl.UseVisualStyleBackColor = true;
            // 
            // panel28
            // 
            this.panel28.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel28.Location = new System.Drawing.Point(274, 0);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(8, 24);
            this.panel28.TabIndex = 11;
            // 
            // buttonGMail
            // 
            this.buttonGMail.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonGMail.Location = new System.Drawing.Point(401, 0);
            this.buttonGMail.Name = "buttonGMail";
            this.buttonGMail.Size = new System.Drawing.Size(44, 24);
            this.buttonGMail.TabIndex = 10;
            this.buttonGMail.Text = "gmail";
            this.buttonGMail.UseVisualStyleBackColor = true;
            this.buttonGMail.Click += new System.EventHandler(this.buttonGMail_Click);
            // 
            // buttonZiggo
            // 
            this.buttonZiggo.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonZiggo.Location = new System.Drawing.Point(445, 0);
            this.buttonZiggo.Name = "buttonZiggo";
            this.buttonZiggo.Size = new System.Drawing.Size(50, 24);
            this.buttonZiggo.TabIndex = 9;
            this.buttonZiggo.Text = "Ziggo";
            this.buttonZiggo.UseVisualStyleBackColor = true;
            this.buttonZiggo.Click += new System.EventHandler(this.buttonZiggo_Click);
            // 
            // textBoxSmtpPort
            // 
            this.textBoxSmtpPort.Dock = System.Windows.Forms.DockStyle.Left;
            this.textBoxSmtpPort.Location = new System.Drawing.Point(229, 0);
            this.textBoxSmtpPort.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxSmtpPort.Name = "textBoxSmtpPort";
            this.textBoxSmtpPort.Size = new System.Drawing.Size(45, 20);
            this.textBoxSmtpPort.TabIndex = 7;
            // 
            // labelSmtpPort
            // 
            this.labelSmtpPort.AutoSize = true;
            this.labelSmtpPort.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSmtpPort.Location = new System.Drawing.Point(185, 0);
            this.labelSmtpPort.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelSmtpPort.Name = "labelSmtpPort";
            this.labelSmtpPort.Size = new System.Drawing.Size(44, 13);
            this.labelSmtpPort.TabIndex = 6;
            this.labelSmtpPort.Text = "  Port = ";
            // 
            // textBoxSmtpHost
            // 
            this.textBoxSmtpHost.Dock = System.Windows.Forms.DockStyle.Left;
            this.textBoxSmtpHost.Location = new System.Drawing.Point(41, 0);
            this.textBoxSmtpHost.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxSmtpHost.Name = "textBoxSmtpHost";
            this.textBoxSmtpHost.Size = new System.Drawing.Size(144, 20);
            this.textBoxSmtpHost.TabIndex = 5;
            // 
            // label13
            // 
            this.label13.Dock = System.Windows.Forms.DockStyle.Left;
            this.label13.Location = new System.Drawing.Point(0, 0);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 24);
            this.label13.TabIndex = 4;
            this.label13.Text = " Host = ";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.panel36);
            this.panel11.Controls.Add(this.panel35);
            this.panel11.Controls.Add(this.buttonKillCue);
            this.panel11.Controls.Add(this.panel32);
            this.panel11.Controls.Add(this.panel31);
            this.panel11.Controls.Add(this.panel30);
            this.panel11.Controls.Add(this.panel29);
            this.panel11.Controls.Add(this.label19);
            this.panel11.Controls.Add(this.label7);
            this.panel11.Controls.Add(this.buttonMSendMail);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel11.Location = new System.Drawing.Point(0, 0);
            this.panel11.Margin = new System.Windows.Forms.Padding(2);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(62, 220);
            this.panel11.TabIndex = 0;
            // 
            // panel36
            // 
            this.panel36.Controls.Add(this.labelSmtpError);
            this.panel36.Controls.Add(this.label33);
            this.panel36.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel36.Location = new System.Drawing.Point(0, 148);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(62, 19);
            this.panel36.TabIndex = 9;
            // 
            // labelSmtpError
            // 
            this.labelSmtpError.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSmtpError.Location = new System.Drawing.Point(41, 0);
            this.labelSmtpError.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelSmtpError.Name = "labelSmtpError";
            this.labelSmtpError.Size = new System.Drawing.Size(19, 19);
            this.labelSmtpError.TabIndex = 10;
            this.labelSmtpError.Text = "--";
            this.labelSmtpError.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label33
            // 
            this.label33.Dock = System.Windows.Forms.DockStyle.Left;
            this.label33.Location = new System.Drawing.Point(0, 0);
            this.label33.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(41, 19);
            this.label33.TabIndex = 9;
            this.label33.Text = "Error=";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel35
            // 
            this.panel35.Controls.Add(this.labelSmtpOk);
            this.panel35.Controls.Add(this.label30);
            this.panel35.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel35.Location = new System.Drawing.Point(0, 129);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(62, 19);
            this.panel35.TabIndex = 8;
            // 
            // labelSmtpOk
            // 
            this.labelSmtpOk.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSmtpOk.Location = new System.Drawing.Point(41, 0);
            this.labelSmtpOk.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelSmtpOk.Name = "labelSmtpOk";
            this.labelSmtpOk.Size = new System.Drawing.Size(19, 19);
            this.labelSmtpOk.TabIndex = 10;
            this.labelSmtpOk.Text = "--";
            this.labelSmtpOk.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label30
            // 
            this.label30.Dock = System.Windows.Forms.DockStyle.Left;
            this.label30.Location = new System.Drawing.Point(0, 0);
            this.label30.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(41, 19);
            this.label30.TabIndex = 9;
            this.label30.Text = "OK=";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // buttonKillCue
            // 
            this.buttonKillCue.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonKillCue.Location = new System.Drawing.Point(0, 103);
            this.buttonKillCue.Name = "buttonKillCue";
            this.buttonKillCue.Size = new System.Drawing.Size(62, 26);
            this.buttonKillCue.TabIndex = 7;
            this.buttonKillCue.Text = "Kill SMTP";
            this.buttonKillCue.UseVisualStyleBackColor = true;
            this.buttonKillCue.Click += new System.EventHandler(this.buttonKillCue_Click);
            // 
            // panel32
            // 
            this.panel32.Controls.Add(this.labelSmtpClearN);
            this.panel32.Controls.Add(this.label31);
            this.panel32.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel32.Location = new System.Drawing.Point(0, 86);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(62, 17);
            this.panel32.TabIndex = 6;
            // 
            // labelSmtpClearN
            // 
            this.labelSmtpClearN.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSmtpClearN.Location = new System.Drawing.Point(41, 0);
            this.labelSmtpClearN.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelSmtpClearN.Name = "labelSmtpClearN";
            this.labelSmtpClearN.Size = new System.Drawing.Size(19, 17);
            this.labelSmtpClearN.TabIndex = 10;
            this.labelSmtpClearN.Text = "--";
            this.labelSmtpClearN.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label31
            // 
            this.label31.Dock = System.Windows.Forms.DockStyle.Left;
            this.label31.Location = new System.Drawing.Point(0, 0);
            this.label31.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(41, 17);
            this.label31.TabIndex = 9;
            this.label31.Text = "clear=";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel31
            // 
            this.panel31.Controls.Add(this.labelSmtpSendN);
            this.panel31.Controls.Add(this.label29);
            this.panel31.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel31.Location = new System.Drawing.Point(0, 69);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(62, 17);
            this.panel31.TabIndex = 5;
            // 
            // labelSmtpSendN
            // 
            this.labelSmtpSendN.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSmtpSendN.Location = new System.Drawing.Point(41, 0);
            this.labelSmtpSendN.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelSmtpSendN.Name = "labelSmtpSendN";
            this.labelSmtpSendN.Size = new System.Drawing.Size(19, 17);
            this.labelSmtpSendN.TabIndex = 10;
            this.labelSmtpSendN.Text = "--";
            this.labelSmtpSendN.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label29
            // 
            this.label29.Dock = System.Windows.Forms.DockStyle.Left;
            this.label29.Location = new System.Drawing.Point(0, 0);
            this.label29.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(41, 17);
            this.label29.TabIndex = 9;
            this.label29.Text = "Send=";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel30
            // 
            this.panel30.Controls.Add(this.labelSmtpCueN);
            this.panel30.Controls.Add(this.label12);
            this.panel30.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel30.Location = new System.Drawing.Point(0, 50);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(62, 19);
            this.panel30.TabIndex = 4;
            // 
            // labelSmtpCueN
            // 
            this.labelSmtpCueN.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelSmtpCueN.Location = new System.Drawing.Point(41, 0);
            this.labelSmtpCueN.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelSmtpCueN.Name = "labelSmtpCueN";
            this.labelSmtpCueN.Size = new System.Drawing.Size(19, 19);
            this.labelSmtpCueN.TabIndex = 10;
            this.labelSmtpCueN.Text = "--";
            this.labelSmtpCueN.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.Dock = System.Windows.Forms.DockStyle.Left;
            this.label12.Location = new System.Drawing.Point(0, 0);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 19);
            this.label12.TabIndex = 9;
            this.label12.Text = "Cue=";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel29
            // 
            this.panel29.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel29.Location = new System.Drawing.Point(0, 40);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(62, 10);
            this.panel29.TabIndex = 3;
            // 
            // label19
            // 
            this.label19.Dock = System.Windows.Forms.DockStyle.Top;
            this.label19.Location = new System.Drawing.Point(0, 20);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(62, 20);
            this.label19.TabIndex = 2;
            this.label19.Text = "mail";
            // 
            // label7
            // 
            this.label7.Dock = System.Windows.Forms.DockStyle.Top;
            this.label7.Location = new System.Drawing.Point(0, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 20);
            this.label7.TabIndex = 1;
            this.label7.Text = "SMTP";
            // 
            // buttonMSendMail
            // 
            this.buttonMSendMail.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonMSendMail.Image = ((System.Drawing.Image)(resources.GetObject("buttonMSendMail.Image")));
            this.buttonMSendMail.Location = new System.Drawing.Point(0, 186);
            this.buttonMSendMail.Name = "buttonMSendMail";
            this.buttonMSendMail.Size = new System.Drawing.Size(62, 34);
            this.buttonMSendMail.TabIndex = 0;
            this.buttonMSendMail.UseVisualStyleBackColor = true;
            this.buttonMSendMail.Click += new System.EventHandler(this.buttonMSendMail_Click);
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.panel21);
            this.panel14.Controls.Add(this.panel20);
            this.panel14.Controls.Add(this.panel18);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel14.Location = new System.Drawing.Point(0, 544);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(557, 132);
            this.panel14.TabIndex = 19;
            // 
            // panel21
            // 
            this.panel21.Controls.Add(this.textBoxEncrypted);
            this.panel21.Controls.Add(this.panel22);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel21.Location = new System.Drawing.Point(0, 48);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(557, 95);
            this.panel21.TabIndex = 2;
            // 
            // textBoxEncrypted
            // 
            this.textBoxEncrypted.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxEncrypted.Location = new System.Drawing.Point(71, 0);
            this.textBoxEncrypted.Multiline = true;
            this.textBoxEncrypted.Name = "textBoxEncrypted";
            this.textBoxEncrypted.Size = new System.Drawing.Size(486, 95);
            this.textBoxEncrypted.TabIndex = 4;
            // 
            // panel22
            // 
            this.panel22.Controls.Add(this.buttonSmtpPW);
            this.panel22.Controls.Add(this.buttonSqlPW);
            this.panel22.Controls.Add(this.buttonDecrypt);
            this.panel22.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel22.Location = new System.Drawing.Point(0, 0);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(71, 95);
            this.panel22.TabIndex = 6;
            // 
            // buttonSmtpPW
            // 
            this.buttonSmtpPW.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonSmtpPW.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonSmtpPW.Location = new System.Drawing.Point(0, 54);
            this.buttonSmtpPW.Name = "buttonSmtpPW";
            this.buttonSmtpPW.Size = new System.Drawing.Size(71, 27);
            this.buttonSmtpPW.TabIndex = 6;
            this.buttonSmtpPW.Text = "=smtpPW";
            this.buttonSmtpPW.UseVisualStyleBackColor = false;
            this.buttonSmtpPW.Click += new System.EventHandler(this.buttonSmtpPW_Click);
            // 
            // buttonSqlPW
            // 
            this.buttonSqlPW.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.buttonSqlPW.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonSqlPW.Location = new System.Drawing.Point(0, 27);
            this.buttonSqlPW.Name = "buttonSqlPW";
            this.buttonSqlPW.Size = new System.Drawing.Size(71, 27);
            this.buttonSqlPW.TabIndex = 5;
            this.buttonSqlPW.Text = "=sqlPw";
            this.buttonSqlPW.UseVisualStyleBackColor = false;
            this.buttonSqlPW.Click += new System.EventHandler(this.buttonSqlPW_Click);
            // 
            // buttonDecrypt
            // 
            this.buttonDecrypt.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonDecrypt.Location = new System.Drawing.Point(0, 0);
            this.buttonDecrypt.Name = "buttonDecrypt";
            this.buttonDecrypt.Size = new System.Drawing.Size(71, 27);
            this.buttonDecrypt.TabIndex = 4;
            this.buttonDecrypt.Text = "Decrypt";
            this.buttonDecrypt.UseVisualStyleBackColor = true;
            this.buttonDecrypt.Click += new System.EventHandler(this.buttonDecrypt_Click);
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.textBoxPlainText);
            this.panel20.Controls.Add(this.buttonEncrypt);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel20.Location = new System.Drawing.Point(0, 24);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(557, 24);
            this.panel20.TabIndex = 1;
            // 
            // textBoxPlainText
            // 
            this.textBoxPlainText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxPlainText.Location = new System.Drawing.Point(71, 0);
            this.textBoxPlainText.Name = "textBoxPlainText";
            this.textBoxPlainText.Size = new System.Drawing.Size(486, 20);
            this.textBoxPlainText.TabIndex = 2;
            // 
            // buttonEncrypt
            // 
            this.buttonEncrypt.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonEncrypt.Location = new System.Drawing.Point(0, 0);
            this.buttonEncrypt.Name = "buttonEncrypt";
            this.buttonEncrypt.Size = new System.Drawing.Size(71, 24);
            this.buttonEncrypt.TabIndex = 0;
            this.buttonEncrypt.Text = "Encrypt";
            this.buttonEncrypt.UseVisualStyleBackColor = true;
            this.buttonEncrypt.Click += new System.EventHandler(this.buttonEncrypt_Click);
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.textBoxItemName);
            this.panel18.Controls.Add(this.label6);
            this.panel18.Controls.Add(this.radioButtonInt);
            this.panel18.Controls.Add(this.radioButtonString);
            this.panel18.Controls.Add(this.radioButtonPassword);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel18.Location = new System.Drawing.Point(0, 0);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(557, 24);
            this.panel18.TabIndex = 3;
            // 
            // textBoxItemName
            // 
            this.textBoxItemName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxItemName.Location = new System.Drawing.Point(206, 0);
            this.textBoxItemName.Name = "textBoxItemName";
            this.textBoxItemName.Size = new System.Drawing.Size(351, 20);
            this.textBoxItemName.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.Dock = System.Windows.Forms.DockStyle.Left;
            this.label6.Location = new System.Drawing.Point(160, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 24);
            this.label6.TabIndex = 0;
            this.label6.Text = " Item:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // radioButtonInt
            // 
            this.radioButtonInt.AutoSize = true;
            this.radioButtonInt.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioButtonInt.Enabled = false;
            this.radioButtonInt.Location = new System.Drawing.Point(123, 0);
            this.radioButtonInt.Name = "radioButtonInt";
            this.radioButtonInt.Size = new System.Drawing.Size(37, 24);
            this.radioButtonInt.TabIndex = 4;
            this.radioButtonInt.Text = "Int";
            this.radioButtonInt.UseVisualStyleBackColor = true;
            // 
            // radioButtonString
            // 
            this.radioButtonString.AutoSize = true;
            this.radioButtonString.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioButtonString.Enabled = false;
            this.radioButtonString.Location = new System.Drawing.Point(71, 0);
            this.radioButtonString.Name = "radioButtonString";
            this.radioButtonString.Size = new System.Drawing.Size(52, 24);
            this.radioButtonString.TabIndex = 3;
            this.radioButtonString.Text = "String";
            this.radioButtonString.UseVisualStyleBackColor = true;
            // 
            // radioButtonPassword
            // 
            this.radioButtonPassword.AutoSize = true;
            this.radioButtonPassword.Checked = true;
            this.radioButtonPassword.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioButtonPassword.Location = new System.Drawing.Point(0, 0);
            this.radioButtonPassword.Name = "radioButtonPassword";
            this.radioButtonPassword.Size = new System.Drawing.Size(71, 24);
            this.radioButtonPassword.TabIndex = 2;
            this.radioButtonPassword.TabStop = true;
            this.radioButtonPassword.Text = "Password";
            this.radioButtonPassword.UseVisualStyleBackColor = true;
            // 
            // textBoxBytes
            // 
            this.textBoxBytes.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBoxBytes.Font = new System.Drawing.Font("Lucida Console", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxBytes.Location = new System.Drawing.Point(0, 285);
            this.textBoxBytes.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxBytes.Multiline = true;
            this.textBoxBytes.Name = "textBoxBytes";
            this.textBoxBytes.Size = new System.Drawing.Size(557, 31);
            this.textBoxBytes.TabIndex = 13;
            this.textBoxBytes.Text = "00 01 02 03 04 05 06 07 08 09 0a 0b 0c 0d 0e 0f \r\n10\r\n20\r\n30\r\n40\r\n50\r\n60\r\n70\r\n80\r" +
    "\n90\r\na0\r\nb0\r\nc0\r\nd0\r\ne0\r\nf0 f1 f2 f3 f4 f5 f6 f7 f8 f9 fa fb fc fd fe ff";
            this.textBoxBytes.TextChanged += new System.EventHandler(this.textBoxBytes_TextChanged);
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.checkBoxGenByteSwap);
            this.panel15.Controls.Add(textBoxNrBytes);
            this.panel15.Controls.Add(label15);
            this.panel15.Controls.Add(this.textBoxOrgHash);
            this.panel15.Controls.Add(this.label14);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel15.Location = new System.Drawing.Point(0, 255);
            this.panel15.Margin = new System.Windows.Forms.Padding(2);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(557, 30);
            this.panel15.TabIndex = 12;
            // 
            // checkBoxGenByteSwap
            // 
            this.checkBoxGenByteSwap.AutoSize = true;
            this.checkBoxGenByteSwap.Dock = System.Windows.Forms.DockStyle.Right;
            this.checkBoxGenByteSwap.Location = new System.Drawing.Point(454, 0);
            this.checkBoxGenByteSwap.Margin = new System.Windows.Forms.Padding(2);
            this.checkBoxGenByteSwap.Name = "checkBoxGenByteSwap";
            this.checkBoxGenByteSwap.Size = new System.Drawing.Size(103, 30);
            this.checkBoxGenByteSwap.TabIndex = 8;
            this.checkBoxGenByteSwap.Text = "Gen. Byte Swap";
            this.checkBoxGenByteSwap.UseVisualStyleBackColor = true;
            // 
            // textBoxOrgHash
            // 
            this.textBoxOrgHash.Dock = System.Windows.Forms.DockStyle.Left;
            this.textBoxOrgHash.Location = new System.Drawing.Point(67, 0);
            this.textBoxOrgHash.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxOrgHash.Name = "textBoxOrgHash";
            this.textBoxOrgHash.Size = new System.Drawing.Size(83, 20);
            this.textBoxOrgHash.TabIndex = 5;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Dock = System.Windows.Forms.DockStyle.Left;
            this.label14.Location = new System.Drawing.Point(0, 0);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(67, 13);
            this.label14.TabIndex = 4;
            this.label14.Text = "  OrgHash = ";
            // 
            // panel13
            // 
            this.panel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel13.Location = new System.Drawing.Point(0, 245);
            this.panel13.Margin = new System.Windows.Forms.Padding(2);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(557, 10);
            this.panel13.TabIndex = 10;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.label9);
            this.panel7.Controls.Add(this.textBoxLicKey);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 203);
            this.panel7.Margin = new System.Windows.Forms.Padding(2);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(557, 42);
            this.panel7.TabIndex = 8;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label9.Location = new System.Drawing.Point(0, 3);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(45, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Lic-Key:";
            // 
            // textBoxLicKey
            // 
            this.textBoxLicKey.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textBoxLicKey.Location = new System.Drawing.Point(0, 16);
            this.textBoxLicKey.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxLicKey.Multiline = true;
            this.textBoxLicKey.Name = "textBoxLicKey";
            this.textBoxLicKey.Size = new System.Drawing.Size(557, 26);
            this.textBoxLicKey.TabIndex = 2;
            // 
            // panel8
            // 
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(0, 195);
            this.panel8.Margin = new System.Windows.Forms.Padding(2);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(557, 8);
            this.panel8.TabIndex = 9;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.buttonEventboard);
            this.panel17.Controls.Add(this.buttonIReader);
            this.panel17.Controls.Add(this.textBoxProgLast);
            this.panel17.Controls.Add(this.label21);
            this.panel17.Controls.Add(this.button4);
            this.panel17.Controls.Add(this.textBoxProgFirst);
            this.panel17.Controls.Add(this.label22);
            this.panel17.Controls.Add(this.buttonPcIReader);
            this.panel17.Controls.Add(this.buttonPcEventboard);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel17.Location = new System.Drawing.Point(0, 173);
            this.panel17.Margin = new System.Windows.Forms.Padding(2);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(557, 22);
            this.panel17.TabIndex = 16;
            // 
            // buttonEventboard
            // 
            this.buttonEventboard.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonEventboard.Location = new System.Drawing.Point(266, 0);
            this.buttonEventboard.Name = "buttonEventboard";
            this.buttonEventboard.Size = new System.Drawing.Size(85, 22);
            this.buttonEventboard.TabIndex = 6;
            this.buttonEventboard.Text = "Eventboard";
            this.buttonEventboard.UseVisualStyleBackColor = true;
            this.buttonEventboard.Click += new System.EventHandler(this.buttonEventboard_Click);
            // 
            // buttonIReader
            // 
            this.buttonIReader.BackColor = System.Drawing.SystemColors.Control;
            this.buttonIReader.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonIReader.Location = new System.Drawing.Point(206, 0);
            this.buttonIReader.Name = "buttonIReader";
            this.buttonIReader.Size = new System.Drawing.Size(60, 22);
            this.buttonIReader.TabIndex = 7;
            this.buttonIReader.Text = "i-Reader";
            this.buttonIReader.UseVisualStyleBackColor = true;
            this.buttonIReader.Click += new System.EventHandler(this.buttonIReader_Click);
            // 
            // textBoxProgLast
            // 
            this.textBoxProgLast.Dock = System.Windows.Forms.DockStyle.Left;
            this.textBoxProgLast.Location = new System.Drawing.Point(176, 0);
            this.textBoxProgLast.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxProgLast.Name = "textBoxProgLast";
            this.textBoxProgLast.Size = new System.Drawing.Size(30, 20);
            this.textBoxProgLast.TabIndex = 3;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Dock = System.Windows.Forms.DockStyle.Left;
            this.label21.Location = new System.Drawing.Point(131, 0);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(45, 13);
            this.label21.TabIndex = 2;
            this.label21.Text = "  Last = ";
            // 
            // button4
            // 
            this.button4.Dock = System.Windows.Forms.DockStyle.Left;
            this.button4.Font = new System.Drawing.Font("Webdings", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.button4.Location = new System.Drawing.Point(103, 0);
            this.button4.Margin = new System.Windows.Forms.Padding(2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(28, 22);
            this.button4.TabIndex = 4;
            this.button4.Text = "4";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // textBoxProgFirst
            // 
            this.textBoxProgFirst.Dock = System.Windows.Forms.DockStyle.Left;
            this.textBoxProgFirst.Location = new System.Drawing.Point(71, 0);
            this.textBoxProgFirst.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxProgFirst.Name = "textBoxProgFirst";
            this.textBoxProgFirst.Size = new System.Drawing.Size(32, 20);
            this.textBoxProgFirst.TabIndex = 1;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Dock = System.Windows.Forms.DockStyle.Left;
            this.label22.Location = new System.Drawing.Point(0, 0);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(71, 13);
            this.label22.TabIndex = 0;
            this.label22.Text = "ProgID first = ";
            // 
            // buttonPcIReader
            // 
            this.buttonPcIReader.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonPcIReader.Location = new System.Drawing.Point(361, 0);
            this.buttonPcIReader.Name = "buttonPcIReader";
            this.buttonPcIReader.Size = new System.Drawing.Size(98, 22);
            this.buttonPcIReader.TabIndex = 13;
            this.buttonPcIReader.Text = "PC+i-Reader";
            this.buttonPcIReader.UseVisualStyleBackColor = true;
            this.buttonPcIReader.Click += new System.EventHandler(this.buttonPcIReader_Click);
            // 
            // buttonPcEventboard
            // 
            this.buttonPcEventboard.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonPcEventboard.Location = new System.Drawing.Point(459, 0);
            this.buttonPcEventboard.Name = "buttonPcEventboard";
            this.buttonPcEventboard.Size = new System.Drawing.Size(98, 22);
            this.buttonPcEventboard.TabIndex = 11;
            this.buttonPcEventboard.Text = "PC+Eventboard";
            this.buttonPcEventboard.UseVisualStyleBackColor = true;
            this.buttonPcEventboard.Click += new System.EventHandler(this.buttonPcEventboard_Click);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.buttonPcText);
            this.panel5.Controls.Add(this.buttonXlsBackupClipboard);
            this.panel5.Controls.Add(this.textBoxNrDays);
            this.panel5.Controls.Add(this.label8);
            this.panel5.Controls.Add(this.buttonNrDays);
            this.panel5.Controls.Add(this.textBoxEndDate);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 147);
            this.panel5.Margin = new System.Windows.Forms.Padding(2);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(557, 26);
            this.panel5.TabIndex = 6;
            // 
            // buttonPcText
            // 
            this.buttonPcText.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonPcText.Location = new System.Drawing.Point(498, 0);
            this.buttonPcText.Name = "buttonPcText";
            this.buttonPcText.Size = new System.Drawing.Size(59, 26);
            this.buttonPcText.TabIndex = 11;
            this.buttonPcText.Text = "PcText";
            this.buttonPcText.UseVisualStyleBackColor = true;
            this.buttonPcText.Click += new System.EventHandler(this.buttonPcText_Click);
            // 
            // buttonXlsBackupClipboard
            // 
            this.buttonXlsBackupClipboard.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonXlsBackupClipboard.Location = new System.Drawing.Point(327, 0);
            this.buttonXlsBackupClipboard.Name = "buttonXlsBackupClipboard";
            this.buttonXlsBackupClipboard.Size = new System.Drawing.Size(140, 26);
            this.buttonXlsBackupClipboard.TabIndex = 9;
            this.buttonXlsBackupClipboard.Text = "XLS Backup + Clipboard";
            this.buttonXlsBackupClipboard.UseVisualStyleBackColor = true;
            this.buttonXlsBackupClipboard.Click += new System.EventHandler(this.buttonXlsBackupClipboard_Click);
            // 
            // textBoxNrDays
            // 
            this.textBoxNrDays.Dock = System.Windows.Forms.DockStyle.Left;
            this.textBoxNrDays.Location = new System.Drawing.Point(275, 0);
            this.textBoxNrDays.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxNrDays.Name = "textBoxNrDays";
            this.textBoxNrDays.Size = new System.Drawing.Size(52, 20);
            this.textBoxNrDays.TabIndex = 7;
            this.textBoxNrDays.TextChanged += new System.EventHandler(this.textBoxNrDays_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Left;
            this.label8.Location = new System.Drawing.Point(216, 0);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "  nr days = ";
            // 
            // buttonNrDays
            // 
            this.buttonNrDays.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonNrDays.Font = new System.Drawing.Font("Webdings", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.buttonNrDays.Location = new System.Drawing.Point(188, 0);
            this.buttonNrDays.Margin = new System.Windows.Forms.Padding(2);
            this.buttonNrDays.Name = "buttonNrDays";
            this.buttonNrDays.Size = new System.Drawing.Size(28, 26);
            this.buttonNrDays.TabIndex = 8;
            this.buttonNrDays.Text = "3";
            this.buttonNrDays.UseVisualStyleBackColor = true;
            this.buttonNrDays.Click += new System.EventHandler(this.buttonNrDays_Click);
            // 
            // textBoxEndDate
            // 
            this.textBoxEndDate.Dock = System.Windows.Forms.DockStyle.Left;
            this.textBoxEndDate.Location = new System.Drawing.Point(118, 0);
            this.textBoxEndDate.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxEndDate.Name = "textBoxEndDate";
            this.textBoxEndDate.Size = new System.Drawing.Size(70, 20);
            this.textBoxEndDate.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Left;
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(118, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "End date YYYYMMDD:";
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 136);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(557, 11);
            this.panel2.TabIndex = 4;
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.textBoxHwStr);
            this.panel16.Controls.Add(this.label17);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel16.Location = new System.Drawing.Point(0, 108);
            this.panel16.Margin = new System.Windows.Forms.Padding(2);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(557, 28);
            this.panel16.TabIndex = 14;
            // 
            // textBoxHwStr
            // 
            this.textBoxHwStr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxHwStr.Location = new System.Drawing.Point(29, 0);
            this.textBoxHwStr.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxHwStr.Name = "textBoxHwStr";
            this.textBoxHwStr.Size = new System.Drawing.Size(528, 20);
            this.textBoxHwStr.TabIndex = 3;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Dock = System.Windows.Forms.DockStyle.Left;
            this.label17.Location = new System.Drawing.Point(0, 0);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(29, 13);
            this.label17.TabIndex = 1;
            this.label17.Text = "Hw=";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.checkBoxMultiPC);
            this.panel4.Controls.Add(this.checkBoxEbMpc);
            this.panel4.Controls.Add(this.buttonNextNr);
            this.panel4.Controls.Add(this.textBoxDevNr);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.button1);
            this.panel4.Controls.Add(this.button2);
            this.panel4.Controls.Add(this.textBoxDevID);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 83);
            this.panel4.Margin = new System.Windows.Forms.Padding(2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(557, 25);
            this.panel4.TabIndex = 5;
            // 
            // checkBoxMultiPC
            // 
            this.checkBoxMultiPC.AutoSize = true;
            this.checkBoxMultiPC.Dock = System.Windows.Forms.DockStyle.Right;
            this.checkBoxMultiPC.Location = new System.Drawing.Point(495, 0);
            this.checkBoxMultiPC.Margin = new System.Windows.Forms.Padding(2);
            this.checkBoxMultiPC.Name = "checkBoxMultiPC";
            this.checkBoxMultiPC.Size = new System.Drawing.Size(62, 25);
            this.checkBoxMultiPC.TabIndex = 18;
            this.checkBoxMultiPC.Text = "MultiPC";
            this.checkBoxMultiPC.UseVisualStyleBackColor = true;
            // 
            // checkBoxEbMpc
            // 
            this.checkBoxEbMpc.AutoSize = true;
            this.checkBoxEbMpc.Checked = true;
            this.checkBoxEbMpc.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxEbMpc.Location = new System.Drawing.Point(364, 5);
            this.checkBoxEbMpc.Margin = new System.Windows.Forms.Padding(2);
            this.checkBoxEbMpc.Name = "checkBoxEbMpc";
            this.checkBoxEbMpc.Size = new System.Drawing.Size(120, 17);
            this.checkBoxEbMpc.TabIndex = 17;
            this.checkBoxEbMpc.Text = "MultiPC Eventboard";
            this.checkBoxEbMpc.UseVisualStyleBackColor = true;
            // 
            // buttonNextNr
            // 
            this.buttonNextNr.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonNextNr.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNextNr.Location = new System.Drawing.Point(318, 0);
            this.buttonNextNr.Name = "buttonNextNr";
            this.buttonNextNr.Size = new System.Drawing.Size(40, 25);
            this.buttonNextNr.TabIndex = 16;
            this.buttonNextNr.Text = "++";
            this.buttonNextNr.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.buttonNextNr.UseVisualStyleBackColor = true;
            this.buttonNextNr.Click += new System.EventHandler(this.buttonNextNr_Click);
            // 
            // textBoxDevNr
            // 
            this.textBoxDevNr.Dock = System.Windows.Forms.DockStyle.Left;
            this.textBoxDevNr.Location = new System.Drawing.Point(276, 0);
            this.textBoxDevNr.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxDevNr.Name = "textBoxDevNr";
            this.textBoxDevNr.Size = new System.Drawing.Size(42, 20);
            this.textBoxDevNr.TabIndex = 3;
            this.textBoxDevNr.TextChanged += new System.EventHandler(this.textBoxDevNr_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Left;
            this.label4.Location = new System.Drawing.Point(214, 0);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = " PC ID-nr = ";
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Left;
            this.button1.Font = new System.Drawing.Font("Webdings", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.button1.Location = new System.Drawing.Point(186, 0);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(28, 25);
            this.button1.TabIndex = 12;
            this.button1.Text = "4";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Dock = System.Windows.Forms.DockStyle.Left;
            this.button2.Font = new System.Drawing.Font("Webdings", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.button2.Location = new System.Drawing.Point(158, 0);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(28, 25);
            this.button2.TabIndex = 10;
            this.button2.Text = "3";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // textBoxDevID
            // 
            this.textBoxDevID.Dock = System.Windows.Forms.DockStyle.Left;
            this.textBoxDevID.Location = new System.Drawing.Point(84, 0);
            this.textBoxDevID.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxDevID.Name = "textBoxDevID";
            this.textBoxDevID.Size = new System.Drawing.Size(74, 20);
            this.textBoxDevID.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Left;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "PC Device-ID = ";
            // 
            // panel19
            // 
            this.panel19.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel19.Location = new System.Drawing.Point(0, 75);
            this.panel19.Margin = new System.Windows.Forms.Padding(2);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(557, 8);
            this.panel19.TabIndex = 18;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.checkBoxDemo);
            this.panel3.Controls.Add(this.buttonLastCenter);
            this.panel3.Controls.Add(this.textBoxOrgNr);
            this.panel3.Controls.Add(this.label18);
            this.panel3.Controls.Add(this.button3);
            this.panel3.Controls.Add(this.buttonAddCenter);
            this.panel3.Controls.Add(this.label16);
            this.panel3.Controls.Add(this.comboBoxCenters);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 50);
            this.panel3.Margin = new System.Windows.Forms.Padding(2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(557, 25);
            this.panel3.TabIndex = 15;
            // 
            // checkBoxDemo
            // 
            this.checkBoxDemo.AutoSize = true;
            this.checkBoxDemo.Dock = System.Windows.Forms.DockStyle.Right;
            this.checkBoxDemo.Location = new System.Drawing.Point(503, 0);
            this.checkBoxDemo.Margin = new System.Windows.Forms.Padding(2);
            this.checkBoxDemo.Name = "checkBoxDemo";
            this.checkBoxDemo.Size = new System.Drawing.Size(54, 25);
            this.checkBoxDemo.TabIndex = 15;
            this.checkBoxDemo.Text = "Demo";
            this.checkBoxDemo.UseVisualStyleBackColor = true;
            // 
            // buttonLastCenter
            // 
            this.buttonLastCenter.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonLastCenter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLastCenter.Location = new System.Drawing.Point(385, 0);
            this.buttonLastCenter.Name = "buttonLastCenter";
            this.buttonLastCenter.Size = new System.Drawing.Size(25, 25);
            this.buttonLastCenter.TabIndex = 14;
            this.buttonLastCenter.Text = "=";
            this.buttonLastCenter.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.buttonLastCenter.UseVisualStyleBackColor = true;
            this.buttonLastCenter.Click += new System.EventHandler(this.buttonLastCenter_Click);
            // 
            // textBoxOrgNr
            // 
            this.textBoxOrgNr.Dock = System.Windows.Forms.DockStyle.Left;
            this.textBoxOrgNr.Location = new System.Drawing.Point(340, 0);
            this.textBoxOrgNr.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxOrgNr.Name = "textBoxOrgNr";
            this.textBoxOrgNr.Size = new System.Drawing.Size(45, 20);
            this.textBoxOrgNr.TabIndex = 3;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Dock = System.Windows.Forms.DockStyle.Left;
            this.label18.Location = new System.Drawing.Point(291, 0);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(49, 13);
            this.label18.TabIndex = 10;
            this.label18.Text = " org-nr = ";
            // 
            // button3
            // 
            this.button3.Dock = System.Windows.Forms.DockStyle.Left;
            this.button3.Font = new System.Drawing.Font("Webdings", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.button3.Location = new System.Drawing.Point(263, 0);
            this.button3.Margin = new System.Windows.Forms.Padding(2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(28, 25);
            this.button3.TabIndex = 11;
            this.button3.Text = "4";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // buttonAddCenter
            // 
            this.buttonAddCenter.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonAddCenter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAddCenter.Location = new System.Drawing.Point(238, 0);
            this.buttonAddCenter.Name = "buttonAddCenter";
            this.buttonAddCenter.Size = new System.Drawing.Size(25, 25);
            this.buttonAddCenter.TabIndex = 13;
            this.buttonAddCenter.Text = "+";
            this.buttonAddCenter.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.buttonAddCenter.UseVisualStyleBackColor = true;
            this.buttonAddCenter.Click += new System.EventHandler(this.buttonAddCenter_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Dock = System.Windows.Forms.DockStyle.Left;
            this.label16.Location = new System.Drawing.Point(161, 0);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(77, 13);
            this.label16.TabIndex = 2;
            this.label16.Text = "CC1234NNNN";
            // 
            // comboBoxCenters
            // 
            this.comboBoxCenters.Dock = System.Windows.Forms.DockStyle.Left;
            this.comboBoxCenters.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCenters.FormattingEnabled = true;
            this.comboBoxCenters.Location = new System.Drawing.Point(62, 0);
            this.comboBoxCenters.Name = "comboBoxCenters";
            this.comboBoxCenters.Size = new System.Drawing.Size(99, 21);
            this.comboBoxCenters.TabIndex = 12;
            this.comboBoxCenters.SelectedIndexChanged += new System.EventHandler(this.comboBoxCenters_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Left;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "OrgLabel = ";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonCheckHardwareKey);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.textBoxHwKey);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(557, 50);
            this.panel1.TabIndex = 3;
            // 
            // buttonCheckHardwareKey
            // 
            this.buttonCheckHardwareKey.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonCheckHardwareKey.Image = ((System.Drawing.Image)(resources.GetObject("buttonCheckHardwareKey.Image")));
            this.buttonCheckHardwareKey.Location = new System.Drawing.Point(518, 13);
            this.buttonCheckHardwareKey.Name = "buttonCheckHardwareKey";
            this.buttonCheckHardwareKey.Size = new System.Drawing.Size(39, 37);
            this.buttonCheckHardwareKey.TabIndex = 3;
            this.buttonCheckHardwareKey.UseVisualStyleBackColor = true;
            this.buttonCheckHardwareKey.Click += new System.EventHandler(this.buttonCheckHardwareKey_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Hardware-Key:";
            // 
            // textBoxHwKey
            // 
            this.textBoxHwKey.Location = new System.Drawing.Point(0, 16);
            this.textBoxHwKey.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxHwKey.Multiline = true;
            this.textBoxHwKey.Name = "textBoxHwKey";
            this.textBoxHwKey.Size = new System.Drawing.Size(513, 34);
            this.textBoxHwKey.TabIndex = 2;
            // 
            // textBox1
            // 
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textBox1.Font = new System.Drawing.Font("Lucida Console", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(0, 646);
            this.textBox1.Margin = new System.Windows.Forms.Padding(2);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(758, 30);
            this.textBox1.TabIndex = 1;
            this.textBox1.Text = "00 01 02 03 04 05 06 07 08 09 0a 0b 0c 0d 0e 0f \r\n10\r\n20\r\n30\r\n40\r\n50\r\n60\r\n70\r\n80\r" +
    "\n90\r\na0\r\nb0\r\nc0\r\nd0\r\ne0\r\nf0 f1 f2 f3 f4 f5 f6 f7 f8 f9 fa fb fc fd fe ff";
            this.textBox1.Visible = false;
            // 
            // TextBoxLog
            // 
            this.TextBoxLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TextBoxLog.Location = new System.Drawing.Point(0, 0);
            this.TextBoxLog.Margin = new System.Windows.Forms.Padding(2);
            this.TextBoxLog.Multiline = true;
            this.TextBoxLog.Name = "TextBoxLog";
            this.TextBoxLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TextBoxLog.Size = new System.Drawing.Size(758, 646);
            this.TextBoxLog.TabIndex = 0;
            // 
            // openFileDialogTZr
            // 
            this.openFileDialogTZr.FileName = "openFileDialog1";
            this.openFileDialogTZr.Filter = "TzReport|*.tzr";
            // 
            // timerProcessSmtp
            // 
            this.timerProcessSmtp.Interval = 5000;
            this.timerProcessSmtp.Tick += new System.EventHandler(this.timerProcessSmtp_Tick);
            // 
            // openFileDialogAttachment
            // 
            this.openFileDialogAttachment.FileName = "openFileDialog";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1318, 715);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "License Key Generator";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel33.ResumeLayout(false);
            this.panel33.PerformLayout();
            this.panel34.ResumeLayout(false);
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            this.panel26.ResumeLayout(false);
            this.panel26.PerformLayout();
            this.panel25.ResumeLayout(false);
            this.panel25.PerformLayout();
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel36.ResumeLayout(false);
            this.panel35.ResumeLayout(false);
            this.panel32.ResumeLayout(false);
            this.panel31.ResumeLayout(false);
            this.panel30.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            this.panel22.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TextBox TextBoxLog;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripLoadClipboard;
        private System.Windows.Forms.ToolStripButton toolStripLoadFile;
        private System.Windows.Forms.ToolStripButton toolStripCreateKey;
        private System.Windows.Forms.ToolStripButton toolStripExportKey;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.TextBox textBoxBytes;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.TextBox textBoxOrgHash;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxLicKey;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.TextBox textBoxSmtpPW;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxSmtpUser;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.TextBox textBoxSmtpPort;
        private System.Windows.Forms.Label labelSmtpPort;
        private System.Windows.Forms.TextBox textBoxSmtpHost;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox textBoxNrDays;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxEndDate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox textBoxDevNr;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxDevID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxHwKey;
        private System.Windows.Forms.CheckBox checkBoxGenByteSwap;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.TextBox textBoxHwStr;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox textBoxOrgNr;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonNrDays;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.TextBox textBoxProgLast;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox textBoxProgFirst;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ToolStripButton toolStripCenters;
        private System.Windows.Forms.ToolStripButton toolStripButtonTZr;
        private System.Windows.Forms.OpenFileDialog openFileDialogTZr;
		private System.Windows.Forms.ToolStripLabel toolStripLicTZ;
		private System.Windows.Forms.ToolStripLabel toolStripLicSirona;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Button buttonAddCenter;
        private System.Windows.Forms.ComboBox comboBoxCenters;
        private System.Windows.Forms.ToolStripButton toolStripOpenCenter;
        private System.Windows.Forms.Button buttonIReader;
        private System.Windows.Forms.Button buttonEventboard;
        private System.Windows.Forms.Button buttonNextNr;
        private System.Windows.Forms.Button buttonLastCenter;
        private System.Windows.Forms.ToolStripButton toolStripLicDV2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
		private System.Windows.Forms.Panel panel14;
		private System.Windows.Forms.Panel panel21;
		private System.Windows.Forms.TextBox textBoxEncrypted;
		private System.Windows.Forms.Panel panel20;
		private System.Windows.Forms.TextBox textBoxPlainText;
		private System.Windows.Forms.Button buttonEncrypt;
		private System.Windows.Forms.Panel panel18;
		private System.Windows.Forms.TextBox textBoxItemName;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.RadioButton radioButtonString;
		private System.Windows.Forms.RadioButton radioButtonPassword;
		private System.Windows.Forms.Panel panel22;
		private System.Windows.Forms.Button buttonSmtpPW;
		private System.Windows.Forms.Button buttonSqlPW;
		private System.Windows.Forms.Button buttonDecrypt;
		private System.Windows.Forms.RadioButton radioButtonInt;
		private System.Windows.Forms.ToolStripButton toolStripListLicenses;
		private System.Windows.Forms.ToolStripButton toolStripMemTest;
		private System.Windows.Forms.Panel panel23;
		private System.Windows.Forms.TextBox textBoxMailTitle;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.Panel panel27;
		private System.Windows.Forms.TextBox textBoxMailFrom;
		private System.Windows.Forms.Label label26;
		private System.Windows.Forms.Panel panel26;
		private System.Windows.Forms.TextBox textBoxMailFile1;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.Panel panel25;
		private System.Windows.Forms.TextBox textBoxMailFile2;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.Panel panel24;
		private System.Windows.Forms.TextBox textBoxMailTo;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Button buttonGMail;
		private System.Windows.Forms.Button buttonZiggo;
		private System.Windows.Forms.CheckBox checkBoxSmtpSsl;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Button buttonMSendMail;
		private System.Windows.Forms.Button buttonTMI;
		private System.Windows.Forms.Panel panel28;
		private System.Windows.Forms.Timer timerProcessSmtp;
		private System.Windows.Forms.Panel panel32;
		private System.Windows.Forms.Label labelSmtpClearN;
		private System.Windows.Forms.Label label31;
		private System.Windows.Forms.Panel panel31;
		private System.Windows.Forms.Label labelSmtpSendN;
		private System.Windows.Forms.Label label29;
		private System.Windows.Forms.Panel panel30;
		private System.Windows.Forms.Label labelSmtpCueN;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Panel panel29;
		private System.Windows.Forms.Button buttonKillCue;
		private System.Windows.Forms.Panel panel33;
		private System.Windows.Forms.TextBox textBoxMailBody;
		private System.Windows.Forms.Panel panel34;
		private System.Windows.Forms.CheckBox checkBoxFileBody;
		private System.Windows.Forms.OpenFileDialog openFileDialogAttachment;
		private System.Windows.Forms.CheckBox checkBoxMailAttach1;
		private System.Windows.Forms.CheckBox checkBoxMailAttach2;
		private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Panel panel36;
        private System.Windows.Forms.Label labelSmtpError;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Panel panel35;
        private System.Windows.Forms.Label labelSmtpOk;
        private System.Windows.Forms.Label label30;
		private System.Windows.Forms.Button buttonXlsBackupClipboard;
		private System.Windows.Forms.Button buttonPcEventboard;
		private System.Windows.Forms.ToolStripButton toolStripGenID;
        private System.Windows.Forms.CheckBox checkBoxEbMpc;
        private System.Windows.Forms.CheckBox checkBoxDemo;
        private System.Windows.Forms.Button buttonPcIReader;
        private System.Windows.Forms.Button buttonPcText;
        private System.Windows.Forms.CheckBox checkBoxMultiPC;
        private System.Windows.Forms.ToolStripButton toolStripTestProgNr;
        private System.Windows.Forms.ToolStripButton toolStripProgID;
        private System.Windows.Forms.ToolStripButton toolStripLicDVX;
        private System.Windows.Forms.Button buttonCheckHardwareKey;
    }
}

