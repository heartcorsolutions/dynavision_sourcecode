﻿// LicKeyGen Form1
// by Simon Vlaar 
// can be used by Techmedic Development B.V
// created 28 August 2016
using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Program_Base;
using System.Diagnostics;

namespace LicKeyGen
{
	public partial class Form1 : Form
	{
		public string _mLicKeyDir = null;
		public List<string> _mCenterList = null;

		public string lastCenter = "";
		public UInt32 lastID = 0;

		string _mSqlUser = "", _mSqlServer = "", _mSqlPW="", _mSmtpUser="", _mStmpServer="", _mStmpPW="";

        UInt32 mUseEndDate = 201991231;

        public Form1()
		{
			try
			{
				InitializeComponent();

				CProgram.sSetProgLogScreen( TextBoxLog, 10000, 0 );

				CProgram.sSetProgTitleFormat( "$", true, DProgramVersionLevel.Alpha, DShowOrganisation.End, "<" );
				CProgram.sSetProgramMainForm( this, "Create License key" );
				//CProgram.sSetProgramTitle("Create license key", true);

				try
				{
					_mCenterList = new List<string>();
					_mLicKeyDir = Properties.Settings.Default.LickKeyCenters;
					if (String.IsNullOrEmpty( _mLicKeyDir ))
					{
						_mLicKeyDir = Path.GetDirectoryName( Application.ExecutablePath );
					}
					_mLicKeyDir = Path.Combine( _mLicKeyDir, "CenterList" );

					if (false == Directory.Exists( _mLicKeyDir ))
					{
						if (CProgram.sbAskOkCancel( "CenterList", "Create dir:" + _mLicKeyDir ))
						{
							Directory.CreateDirectory( _mLicKeyDir );
							string path = Path.GetFullPath( _mLicKeyDir );
							CProgram.sLogLine( "Created LicKeyDir= " + path );
						}
					}
					else
					{
						string path = Path.GetFullPath( _mLicKeyDir );
						CProgram.sLogLine( "LicKeyDir= " + path );
					}
                    CProgram.sSetProgramMainForm(this, "Create License key: " + _mLicKeyDir);



                    String[] centerList = Directory.GetDirectories( _mLicKeyDir );
					int n = centerList == null ? 0 : centerList.Length;
					string allCenters = "";
					string noCenter = "-- Select --";

					comboBoxCenters.Items.Add( noCenter );

					for (int i = 0; i < n; ++i)
					{
						string center = Path.GetFileName( centerList[i] );
						if (i > 0)
						{
							allCenters += ", ";
						}
						comboBoxCenters.Items.Add( center );

						allCenters += center;

						// add to select box
					}
					comboBoxCenters.Text = noCenter;
					CProgram.sLogLine( n.ToString() + " centers: " + allCenters );
				}
				catch (Exception ex)
				{
					CProgram.sLogException( "Failed to get CenterList", ex );
				}
			}
			catch (Exception ex)
			{
				CProgram.sLogException( "CreateAccessibilityInstance form1", ex );
			}

		}

		public bool mbCreateLicKey( bool AbSetLastCopyClipboard)
		{
			string errStr = "";
			string orgStr = "";
			string hwStr = textBoxHwStr.Text;
			UInt16 orgNr = 0;
			UInt32 devID = 0;
			UInt16 devNr = 0;
			UInt16 progFirst = 0, progLast = 0;
			UInt32 endDate = 0, year = 0, month = 0, day = 0;
			string orgNrStr = textBoxOrgNr.Text;
			string devIdStr = textBoxDevID.Text;
			string devNrStr = textBoxDevNr.Text;

			if (false == CLicKeyDev.sbFormatOrgLabel( out orgStr, comboBoxCenters.Text ))
			{
				errStr += " OrgLabel";
			}

			UInt16.TryParse( textBoxOrgNr.Text, out orgNr );
			if (orgNr == 0 || CLicKeyDev.sExtractNr( orgStr ) != orgNr) errStr += " OrgNr";

			if (UInt32.TryParse( textBoxDevID.Text, out devID )
				&& UInt16.TryParse( textBoxDevNr.Text, out devNr ))
			{
				int i = orgNr * CLicKeyDev._cNrDevPerOrg + devNr;
				if (devID != i || devNr == 0) errStr += " devID";
			}
			else
			{
				errStr += " devID";
			}
			if (UInt16.TryParse( textBoxProgFirst.Text, out progFirst )
				&& UInt16.TryParse( textBoxProgLast.Text, out progLast )
				&& progLast <= 63 && progFirst <= progLast)
			{

			}
			else
			{
				errStr += " progNr";
			}
			if (UInt32.TryParse( textBoxEndDate.Text, out endDate ))
			{
				if (endDate < 2000)
				{
				}
				else if (endDate < 10000)
				{
					endDate = endDate * 100 + 20000028;    // given year month
					textBoxEndDate.Text = endDate.ToString();
				}

				DateTime dt;
				CProgram.sbSplitYMD( endDate, out dt );

				int nrDays = endDate < 20180101 ? -1 : (int)((dt - DateTime.Now).TotalDays + 0.99);

				textBoxNrDays.Text = nrDays.ToString();
				if (nrDays < 0)
				{
					errStr += " date";
				}
			}
			else
			{
				errStr += " YMD";
			}
			if (errStr.Length > 0)
			{
				CProgram.sLogLine( "Create licKey error = " + errStr );
				textBoxLicKey.Text = "";
			}
			else
			{
				if (checkBoxDemo.Checked)
				{
					devID |= 0x80000000;    // set demo
				}
				string licKey = orgStr + "-" + devID.ToString( "X8" );
				UInt32 cs = CLicKeyDev.sCalcChecksum2( licKey + "-" + hwStr );
				UInt16 progNr = (UInt16)(progLast << 8 | progFirst);

				licKey += "-" + cs.ToString( "X8" ) + "-" + endDate.ToString( "00000000" ) + "-" + progNr.ToString( "X4" );

				cs = CLicKeyDev.sCalcChecksum1( licKey );
				licKey += "-" + cs.ToString( "X8" );

                textBoxLicKey.Text = licKey;

                if( AbSetLastCopyClipboard)
                {
                    CProgram.sLogLine("LicKey generated and copied to clipboard");

                    Clipboard.SetText(licKey+"\r\n");
                    lastCenter = orgStr;
                    lastID = devID;
                }
            }
			return errStr.Length == 0;
		}
		public bool mbCheckHardwareKey()
		{
			string hwKey = textBoxHwKey.Text == null ? "" : textBoxHwKey.Text.Trim();
			bool bOk = CLicKeyDev.sbSetCheckHardwareKey( hwKey );

			textBoxHwKey.BackColor = bOk ? Color.LightGreen : Color.Orange;

			textBoxOrgNr.Text = CLicKeyDev.sGetOrgNr().ToString();
			comboBoxCenters.Text = CLicKeyDev.sGetOrgLabel();
			UInt32 nr = CLicKeyDev.sGetDeviceID();
			checkBoxDemo.Checked = (nr & 0x80000000) != 0;
			nr &= 0x7FFFFFFF;
			textBoxDevID.Text = nr.ToString();
			textBoxDevNr.Text = (nr % CLicKeyDev._cNrDevPerOrg).ToString();
			textBoxHwStr.Text = CLicKeyDev.sGetHardwareStr();

			return bOk;
		}
		private void toolStripButton1_Click( object sender, EventArgs e )
		{
			CProgram.sLogLine( "Test pc..." );

			CProgram.sLogLine( "BaseID=" + HwScan.baseId() );
			CProgram.sLogLine( "BiosID=" + HwScan.biosId() );
			CProgram.sLogLine( "CpuID=" + HwScan.cpuId() );
			CProgram.sLogLine( "DiskID=" + HwScan.diskId() );
			CProgram.sLogLine( "MacID=" + HwScan.macId() );
			CProgram.sLogLine( "VideoID=" + HwScan.videoId() );
			CProgram.sLogLine( "Value=" + HwScan.Value() );

			string orgLabel = "";

			//if (CLicKeyDev.sbFormatOrgLabel( out orgLabel, comboBoxCenters.Text ))
			{
				UInt32 devId = 0;
				UInt32.TryParse( textBoxDevID.Text, out devId );
				if (orgLabel.Length < 2) orgLabel = "X";

				string hw = orgLabel + "-" + devId.ToString( "X8" ) + "-" + CLicKeyDev.sCalcHardwareID();
				UInt32 cs1 = CLicKeyDev.sCalcChecksum1( hw );
				UInt32 cs2 = CLicKeyDev.sCalcChecksum2( hw );
				CProgram.sLogLine( "Hardware=" + hw );
				CProgram.sLogLine( "checksum1=" + cs1.ToString( "X8" ) );
				CProgram.sLogLine( "checksum2=" + cs2.ToString( "X8" ) );

				textBoxHwKey.Text = hw + "-" + cs1.ToString( "X8" );

				mbCheckHardwareKey();
			}
		}

		private void textBoxBytes_TextChanged( object sender, EventArgs e )
		{

		}

		private void toolStripCreateKey_Click( object sender, EventArgs e )
		{
			mbCreateLicKey(true);
		}

		private void buttonNrDays_Click( object sender, EventArgs e )
		{
			int i;

			if (int.TryParse( textBoxNrDays.Text, out i ))
			{
				DateTime dt = DateTime.Now.AddDays( i );

				textBoxEndDate.Text = CProgram.sCalcYMD( dt ).ToString();
			}
		}

		private void button3_Click( object sender, EventArgs e )
		{
			string orgLabel = "";

			if (CLicKeyDev.sbFormatOrgLabel( out orgLabel, comboBoxCenters.Text ))
			{
				UInt16 i = CLicKeyDev.sExtractNr( orgLabel );

				//                textBoxOrgLabel.Text = orgStr;
				textBoxOrgNr.Text = i.ToString();
			}
		}

		private void button1_Click( object sender, EventArgs e )
		{
			UInt32 i = 0;

			if (UInt32.TryParse( textBoxDevID.Text, out i ))
			{
				textBoxDevNr.Text = (i % CLicKeyDev._cNrDevPerOrg).ToString();
			}
		}

		private void button2_Click( object sender, EventArgs e )
		{
			int orgNr = 0;
			int devNr = 0;

			if (int.TryParse( textBoxOrgNr.Text, out orgNr )
				&& int.TryParse( textBoxDevNr.Text, out devNr ))
			{
				textBoxDevID.Text = (orgNr * CLicKeyDev._cNrDevPerOrg + devNr).ToString();
			}
		}
		private void toolStripLoadFile_Click( object sender, EventArgs e )
		{
			string hwKey = Clipboard.GetText();

			textBoxHwKey.Text = hwKey == null ? "" : hwKey.Trim();

			mbCheckHardwareKey();
		}

		private void toolStripButton2_Click( object sender, EventArgs e )
		{
			string licKey = textBoxLicKey.Text;

			if (CLicKeyDev.sbSetCheckLicKey( licKey ))
			{
				int nrDays = CLicKeyDev.sGetDaysLeft();
				CProgram.sLogLine( "License key ok, " + nrDays.ToString() + " days left" );

				if (CLicKeyDev.sbCheckAgainstPc())
				{
					CProgram.sLogLine( "Checked, harware key is from this pc" );
				}
				else
				{
					CProgram.sLogLine( "Failed PC harware test!" );
				}
			}
			else
			{
				CProgram.sLogLine( "Lisense key not ok" );
			}
		}

		private void toolStripButton3_Click( object sender, EventArgs e )
		{
			CEncryptedString hardwareKey = new CEncryptedString( "LicHwKey", DEncryptLevel.L1_Program, 0 );
			CEncryptedString licenseKey = new CEncryptedString( "LicHwKey", DEncryptLevel.L1_Program, 0 );
			bool bLicChanged = false;

			bool bLicOk = false;

			if (hardwareKey != null && licenseKey != null)
			{
				bool bChanged;

				bLicChanged = hardwareKey.mbSetEncrypted( textBoxHwKey.Text == null ? "" : textBoxHwKey.Text.Trim() ); // Properties.Settings.Default.LicHwKey);
				bLicChanged |= licenseKey.mbSetEncrypted( textBoxLicKey.Text ); // Properties.Settings.Default.LicHwKey);

				bLicOk = CLicKeyDev.sbLicenseRequest( out bChanged, ref hardwareKey, ref licenseKey, 14, false );
				bLicChanged |= bChanged;
			}
			if (bLicChanged)
			{
				// store lic key in settings
				textBoxHwKey.Text = hardwareKey.mDecrypt();
				textBoxLicKey.Text = licenseKey.mDecrypt();
				mbCheckHardwareKey();
			}
			if (bLicOk)
			{
				CProgram.sLogLine( "Licesekey tested ok for device" + CProgram.sGetDeviceID().ToString() + "@" + CProgram.sGetOrganisationLabel() );
			}
		}

		private void button4_Click( object sender, EventArgs e )
		{
			textBoxProgLast.Text = textBoxProgFirst.Text;
		}

		private void buttonTestProgr_Click( object sender, EventArgs e )
		{
			int progNr = CProgram.sGetProgNr();

			textBoxProgFirst.Text = textBoxProgLast.Text = progNr.ToString();
		}

		private void toolStripLoadClipboard_Click( object sender, EventArgs e )
		{
			mbCheckHardwareKey();
		}

		private void toolStripCenters_Click( object sender, EventArgs e )
		{
			/*CServerCentersForm form = new CServerCentersForm( _mLicKeyDir);

            if( form != null )
            {
                form.Show();
            }
            */
		}

		private void toolStripButtonTZr_Click( object sender, EventArgs e )
		{
			/*           openFileDialogTZr.InitialDirectory = "D:\\Data\\TZ Aera\\";
                       if( DialogResult.OK == openFileDialogTZr.ShowDialog())
                       {
                           CTzReport tzReport = new CTzReport();

                           if( tzReport != null )
                           {
                               string filePath = openFileDialogTZr.FileName;
                               tzReport.mbReadOneFile(filePath);



                           }
                       }
              */
		}


		private void toolStripLicSirona_Click( object sender, EventArgs e )
		{
			try
			{
				string collection = "Sirona";
				UInt64 seed = 0x8393F6D3840A272B;

				mMakeListKey( collection, seed, mUseEndDate);
			}
			catch (Exception ex)
			{
				CProgram.sLogException( "LicSirona", ex );
			}

		}

		private void mMakeListKey( string ACollection, UInt64 ASeed, UInt32 AUseEndDate )
		{
			try
			{
                bool bIsNotProgrammer = false == CLicKeyDev.sbDeviceIsProgrammer(); // always true id = 0

				CItemKey itemList = new CItemKey( ACollection );
				CItemKey itemKey = new CItemKey( ACollection );
				CItemKey readKey = new CItemKey( ACollection );

				string centerName = "";

				if (false == CLicKeyDev.sbFormatOrgLabel( out centerName, comboBoxCenters.Text ))
				{
					CProgram.sAskWarning( "Make list " + ACollection, "Select valid center first" );
					return;
				}

				string centerPath = Path.Combine( _mLicKeyDir, centerName );
				UInt32 endDate = 0;
				DateTime dt = DateTime.Now;
				UInt32 curDate = (UInt32)(dt.Year * 10000 + dt.Month * 100 + dt.Day);
                UInt32 day0 = curDate;

                if (false == Directory.Exists( centerPath ))
				{
					CProgram.sLogError( "Center directory does not exist: " + centerPath );
				}
				else
				{
					string inFile = Path.Combine( centerPath, CItemKey.sMakeItemListFileName( ACollection ) );
					string outFile = Path.Combine( centerPath, CItemKey.sMakeItemKeyFileName( ACollection ) );

					bool bCheckSumOk = false;
					bool bCont = itemList.mbReadFromFile( out bCheckSumOk, inFile, ASeed, centerName, false );

					if (false == bCont && false == File.Exists( inFile ))
					{
						StreamWriter fs = new StreamWriter( inFile );

						if (fs != null)
						{
							CProgram.sLogLine( "Creating missing file: " + inFile );

							string line;

							line = CItemKey.sMakeParamLine( CItemKey._cParamCenter, centerName );
							fs.WriteLine( line );

							line = CItemKey.sMakeParamLine( CItemKey._cParamCollection, ACollection );
							fs.WriteLine( line );

							line = CItemKey.sMakeParamLine( CItemKey._cParamWritten, CProgram.sDateTimeToYMDHMS( DateTime.UtcNow ) );
							fs.WriteLine( line );

							line = CItemKey.sMakeParamLine( CItemKey._cParamDay0, CProgram.sCalcYMD( DateTime.UtcNow ).ToString() );
							fs.WriteLine( line );

							fs.Close();
						}
						return;
					}
					else if (false == bCont)
					{
						string center, group;
						string err = "";

						if (false == itemList.mbGetParamString( out center, "center" ))
						{
							err += "center not present. ";
						}
						else if (center != centerName)
						{
							err += "center " + center + "!=" + centerName + ". ";
						}
						if (false == itemList.mbGetParamString( out group, "collection" ))
						{
							if (err != null && err.Length > 0)
							{
								err += ", ";
							}
							err += "collection not present. ";
						}
						else if (group != ACollection)
						{
							if (err != null && err.Length > 0)
							{
								err += ", ";
							}
							err += "collection " + group + "!=" + ACollection + ". ";
						}
                        if( false == itemList.mbGetParamUInt32(out day0, CItemKey._cParamDay0) && false == bIsNotProgrammer)
                        {
                            if (err != null && err.Length > 0)
                            {
                                err += ", ";
                            }
                            err += "day0 not present. ";
                        }
                        if (false == bCheckSumOk && false == bIsNotProgrammer)
						{
							if (err != null && err.Length > 0)
							{
								err += ", ";
							}
							err += "checksum not ok";
						}
						CProgram.sLogError( ACollection + ": failed read item list: " + inFile );
						if (err != null && err.Length > 0)
						{
							CProgram.sLogError( err );
                            bCont = CProgram.sbAskOkCancel(ACollection, "error: " + err + ", Continue?");
                        }
                        else
                        {
                            bCont = true; // checksum not ok
                        }
                    }
					if (bCont)
					{
						List<string> linesList = itemList.mGetLinesList();
						List<string> newLinesList = itemKey.mGetLinesList();
						int nLines = linesList == null ? 0 : linesList.Count;

						bCont = false;
						if (nLines == 0)
						{
							CProgram.sLogError( ACollection + ": empty: " + inFile );
						}
						else if (newLinesList == null)
						{
							CProgram.sLogError( ACollection + ": itemKey not valid" );

						}
						else
						{
                            bool b = itemList.mbGetParamUInt32(out day0, CItemKey._cParamDay0);
                            endDate = day0 > AUseEndDate ? day0 : AUseEndDate;

							if (CProgram.sbReqUInt32( "LicKey " + ACollection, "endDate", ref endDate, "", curDate, 20991231 ))
							{
								char codeChar;
								string name, value;
								string newLine;
								int nOld = 0;
								int nActive = 0;
								int nParam = 0;

								foreach (string line in linesList)
								{
									if (itemList.mbIsNotReservedLine( line ))
									{
										if (itemList.mbSplitItem( line, out codeChar, out name, out value ))
										{
											if (codeChar == CItemKey._cCharParam)
											{
												newLine = CItemKey.sMakeParamLine( name, value );
												newLinesList.Add( newLine );
												++nParam;
											}
											else if (codeChar == CItemKey._cCharValid)
											{
												newLine = CItemKey.sMakeValidItemLine( true, name, value );
												newLinesList.Add( newLine );
												++nActive;
											}
											else if (codeChar == CItemKey._cCharOld)
											{
												newLine = CItemKey.sMakeValidItemLine( false, name, value );
												newLinesList.Add( newLine );
												++nOld;
											}
											else if (codeChar == '\0')
											{
												if (value == null || value.Length == 0)
												{
													value = "0";    // single name => "+<name>=0"
												}
												newLine = CItemKey.sMakeValidItemLine( true, name, value );
												newLinesList.Add( newLine );
												++nActive;
											}
										}
									}
								}
								CProgram.sLogError( ACollection + ": " + nParam.ToString() + " params, " + nOld.ToString() + " old, " + nActive.ToString() + " active" );
								bCont = itemKey.mbWriteToFile( outFile, ASeed, centerName, true, endDate );
								if (false == bCont)
								{
									CProgram.sLogError( ACollection + ": failed writing to: " + outFile );
								}
								else
								{
									CProgram.sLogLine( ACollection + ": succeded to write items: " + outFile );
									CProgram.sLogLine( "Copy " + Path.GetFileName( outFile ) + " to customers device directory." );
								}
							}
						}
					}
					if (bCont)
					{
						Int32 checkDay0 = -1;
						UInt32 checkAtDay = 2;
						UInt32 fileDay0 = 0;
						DateTime dtTest = DateTime.Now;

						bCheckSumOk = false;
						bCont = readKey.mbReadFromFile( out bCheckSumOk, outFile, ASeed, centerName, true );

						if (false == bCont)
						{
							string center, group;
							string err = "";

							if (false == readKey.mbGetParamString( out center, "center" ))
							{
								err += "center not present. ";
							}
							else if (center != centerName)
							{
								err += "center " + center + "!=" + centerName + ". ";
							}
							if (false == readKey.mbGetParamString( out group, "collection" ))
							{
								err += "collection not present. ";
							}
							else if (group != ACollection)
							{
								err += "collection " + group + "!=" + ACollection + ". ";
							}
							CProgram.sLogError( ACollection + ": failed read key list: " + outFile );
							if (err != null && err.Length > 0)
							{
								CProgram.sLogError( err );
							}
							bCont = CProgram.sbAskOkCancel( "Item list " + ACollection, "failed read key, Continue?" );
						}
						else
						{
							CProgram.sLogLine( ACollection + ": key file read: " + readKey.mGetLinesList().Count + " lines" );
						}
						if (bCont && false == bCheckSumOk)
						{
							CProgram.sLogError( ACollection + ": key file checksum not ok: " );
							bCont = CProgram.sbAskOkCancel( "ReadKey " + ACollection, "Failed checksum, Continue?" );
						}
						if (bCont)
						{
							bCont = readKey.mbGetParamUInt32( out fileDay0, CItemKey._cParamDay0 );

							if (false == bCont)
							{
								CProgram.sLogError( ACollection + ": file Day 0 missing " );
							}
							else
							{
								CProgram.sbSplitYMD( fileDay0, out dtTest );
								int nLeft = (int)(dtTest - DateTime.Now).TotalDays;

								CProgram.sLogLine( ACollection + ": file day 0 = " + fileDay0.ToString() + ", " + nLeft.ToString() + " days" );
							}
						}
						if (bCont)
						{
							bCont = bIsNotProgrammer
                                || CProgram.sbReqInt32( "LicKey device " + ACollection, "testDate", ref checkDay0, "", -1000, 1000 );

							if (bCont)
							{
								checkAtDay = 2;
								bCont = bIsNotProgrammer
                                    || CProgram.sbReqUInt32( "LicKey device " + ACollection, "testAtDate", ref checkAtDay, "", 0, 1000 );
							}
						}
						if (bCont)
						{
							dtTest = dtTest.AddDays( checkDay0 );
							checkDay0 = (Int32)CProgram.sCalcYMD( dtTest );
							CProgram.sLogLine( ACollection + ": curDate = " + curDate.ToString() + ", testDate= " + checkDay0.ToString()
									+ ", day0= " + fileDay0.ToString() + ", test also after " + checkAtDay.ToString() );

							UInt32 nAtDay = 0;
							List<string> strList = readKey.mGetValidItems( out nAtDay, dtTest, (UInt16)checkAtDay, fileDay0 );
							int n = strList == null ? 0 : strList.Count;

							CProgram.sLogLine( ACollection + ": active items n=" + n.ToString() );
							string strNames = "";
							string strDoubles = "";
							UInt16 nDoubles = 0;

							if (n > 0)
							{
								int iNames = 0;
								foreach (string s in strList)
								{
									if (iNames > 0) strNames += ", ";

									if( strNames.Contains( s ))
									{
										strDoubles += "!" + s;
										++nDoubles;
									}
									strNames += s;
									++iNames;
								}
							}
							CProgram.sLogLine( strNames );
							if( nDoubles > 0)
							{
								CProgram.sLogLine( nDoubles.ToString() + " doubles: " + strDoubles );
								CProgram.sPromptError( true, "Encrypt", nDoubles.ToString() + " double device snrs" );
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				CProgram.sLogException( "LicKey " + ACollection, ex );
			}

		}

		private void toolStripLicTZ_Click( object sender, EventArgs e )
		{
			try
			{
				string collection = "TZ";
				UInt64 seed = 0x4A2277CE8090D3E4;

				mMakeListKey( collection, seed , mUseEndDate);
			}
			catch (Exception ex)
			{
				CProgram.sLogException( "LicSirona", ex );
			}
		}

		private void buttonAddCenter_Click( object sender, EventArgs e )
		{
			string name = "";

			if (CProgram.sbReqLabel( "Add Center XX0000Yyyy", "center", ref name, "", false ))
			{
				string center;

				if (CLicKeyDev.sbFormatOrgLabel( out center, name ))
				{
					UInt16 iCenter = CLicKeyDev.sExtractNr( center );
					string strNr = iCenter.ToString( "D4" );
					bool bFound = false;
					string strFound = "";

					foreach (string s in _mCenterList)
					{
						if (s != null && s.Length > 2)
						{
							if (s.Substring( 2 ).StartsWith( strNr ))
							{
								bFound = true;
								strFound = s;
								break;
							}
						}
					}
					if (bFound)
					{
						CProgram.sLogError( "Center name number " + center + " already exists as " + strFound );
					}
					else
					{
						try
						{
							Directory.CreateDirectory( Path.Combine( _mLicKeyDir, center ) );
							CProgram.sLogLine( "Created center " + center );
							comboBoxCenters.Items.Insert( 1, center );
							comboBoxCenters.Text = center;
						}
						catch (Exception ex)
						{
							CProgram.sLogException( "Failed create center directory " + center, ex );
						}
					}
				}
				else
				{
					CProgram.sLogError( "Invalid center name " + name );
				}
			}
		}

		private void comboBoxCenters_SelectedIndexChanged( object sender, EventArgs e )
		{
			string center = comboBoxCenters.Text;
			UInt16 iCenter = CLicKeyDev.sExtractNr( center );

			textBoxOrgNr.Text = iCenter.ToString();
			textBoxDevID.Text = "";
			textBoxDevNr.Text = "";
		}

		private void toolStripOpenCenter_Click( object sender, EventArgs e )
		{
			string center;

			if (CLicKeyDev.sbFormatOrgLabel( out center, comboBoxCenters.Text ))
			{
				string centerPath = Path.Combine( _mLicKeyDir, center );

				if (false == Directory.Exists( centerPath ))
				{
					CProgram.sLogError( "Center directory does not exist: " + centerPath );
				}
				else
				{
					CProgram.sLogError( "Opening Center directory: " + centerPath );

					try
					{
						ProcessStartInfo startInfo = new ProcessStartInfo();
						startInfo.FileName = @"explorer";
						startInfo.Arguments = centerPath;
						Process.Start( startInfo );
					}
					catch (Exception e2)
					{
						CProgram.sLogException( "Failed open explorer", e2 );
					}
				}
			}
		}

		private void buttonIReader_Click( object sender, EventArgs e )
		{
			textBoxProgFirst.Text = "11";
			textBoxProgLast.Text = "11";

			mbCreateLicKey(true);
		}

		private void buttonEventboard_Click( object sender, EventArgs e )
		{
			textBoxProgFirst.Text = "21";
			textBoxProgLast.Text = "21";

			mbCreateLicKey(true);
		}

		private void buttonLastCenter_Click( object sender, EventArgs e )
		{
			string center;

			if (lastCenter != null && lastCenter.Length > 0
				&& false == CLicKeyDev.sbFormatOrgLabel( out center, comboBoxCenters.Text ))
			{
				comboBoxCenters.Text = lastCenter;

				UInt16 iCenter = CLicKeyDev.sExtractNr( center );
				UInt32 devID = 0;

                textBoxOrgNr.Text = iCenter.ToString();

				if (UInt32.TryParse( textBoxDevID.Text, out devID ))
				{
					if (devID > 0)
					{
						UInt32 i = devID / 10000;
						if (i != iCenter)
						{
							textBoxDevID.Text = "";
							textBoxDevNr.Text = "";
						}
					}
				}

			}
		}

		private void buttonNextNr_Click( object sender, EventArgs e )
		{
			string center;

			if (false == CLicKeyDev.sbFormatOrgLabel( out center, comboBoxCenters.Text ))
			{
				if (lastCenter != null && lastCenter.Length > 0)
				{
					comboBoxCenters.Text = lastCenter;

                    UInt16 iCenter = CLicKeyDev.sExtractNr(lastCenter);
 
                    textBoxOrgNr.Text = iCenter.ToString();

                    UInt32 devID = lastID;
					++devID;
					textBoxDevID.Text = devID.ToString();
					textBoxDevNr.Text = (devID % 10000).ToString();
                    CProgram.sLogLine("LastCenter and nextDevice " + lastCenter + " " + devID.ToString());
				}
			}
			else
			{
				UInt16 iCenter = CLicKeyDev.sExtractNr( center );
				UInt32 devID = 0;

                textBoxOrgNr.Text = iCenter.ToString();

                if (center != lastCenter)
                {
                    lastCenter = "";
                    lastID = 0;
                }
                if (UInt32.TryParse( textBoxDevID.Text, out devID ))
				{
					if (devID > 0)
					{
						UInt32 i = devID / 10000;
						if (i != iCenter)
						{
							textBoxDevID.Text = "";
							textBoxDevNr.Text = "";
                            CProgram.sLogLine("Device nr not ok" + center + " " + devID.ToString());
                        }
                        else
						{
							++devID;
							textBoxDevID.Text = devID.ToString();
							textBoxDevNr.Text = (devID % 10000).ToString();
                            CProgram.sLogLine("Next device " + center + " " + devID.ToString());

                        }
                    }
					else 
					{
                        CProgram.sLogLine("no device " + center + " " + devID.ToString());
                        /*     					devID = lastID == 0 ? (UInt32)(iCenter * 10000 + 101) : lastID + 1;

                                                textBoxDevID.Text = devID.ToString();
                                                textBoxDevNr.Text = (devID % 10000).ToString();
                        */
                    }
                }
				else
				{
                    CProgram.sLogLine("empty device " + center );
                    /*                   if (center != lastCenter)
                                       {
                                           lastCenter = "";
                                           lastID = 0;
                                       }
                                       devID = lastID == 0 ? (UInt32)(iCenter * 10000 + 101) : lastID + 1;
                                       textBoxDevID.Text = devID.ToString();
                                       textBoxDevNr.Text = (devID % 10000).ToString();
                   */
                }
            }
		}

		private void toolStripLicDV2_Click( object sender, EventArgs e )
		{
			try
			{
				string collection = "DV2";
				UInt64 seed = 0xC653DE78AC890923;

				mMakeListKey( collection, seed, mUseEndDate);
			}
			catch (Exception ex)
			{
				CProgram.sLogException( "LicDV2", ex );
			}

		}

		private void toolStripButton5_Click( object sender, EventArgs e )
		{

		}

		private void toolStripExportKey_Click( object sender, EventArgs e )
		{

		}

		private void buttonSqlPW_Click( object sender, EventArgs e )
		{
			//			string _mSqlUser, _mSqlServer, _mSqlPW, _mSmtpUser, _mStmpServer, _mStmpPW;

			try
			{
				string center;

				if (false == CLicKeyDev.sbFormatOrgLabel( out center, comboBoxCenters.Text ))
				{
					CProgram.sLogLine( "Select Center first!" );
				}
				else if (CProgram.sbReqLabel( "Create SQL Password", "Server", ref _mSqlServer, "", false )
					&& CProgram.sbReqLabel( "Create SQL Password", "User", ref _mSqlUser, "", false )
					&& CProgram.sbReqLabel( "Create SQL Password", "Password", ref _mSqlPW, "", false ))
				{
					string name = _mSqlUser.Trim() + "@" + _mSqlServer.Trim() + ":sqlPW";
					string plainText = _mSqlPW;

					textBoxItemName.Text = name;
					textBoxPlainText.Text = plainText;

					string encrypted, decrypted;
					CPasswordString pw = new CPasswordString( name, center );

					bool b = pw.mbEncrypt( plainText );

					encrypted = pw.mGetEncrypted();
					textBoxEncrypted.Text = encrypted;
					if (b == false)
					{
						CProgram.sLogError( "Encrypt SQL PW item " + name + " failed encrypt" );
					}
					else
					{
						decrypted = pw.mDecrypt();

						if (plainText == decrypted)
						{
							CProgram.sLogLine( "Encrypt SQL PW item " + name + " successful copied to Clipboard." );
							Clipboard.SetText( encrypted );
						}
						else
						{
							CProgram.sLogError( "Encrypt SQL PW item " + name + " failed reverse" );
						}
					}
				}
			}
			catch (Exception ex)
			{
				CProgram.sLogException( "Encrypt SQL PW", ex );
			}
		}

		private void buttonSmtpPW_Click( object sender, EventArgs e )
		{
			//			string _mSqlUser, _mSqlServer, _mSqlPW, _mSmtpUser, _mStmpServer, _mStmpPW;

			try
			{
				string center;

				if (false == CLicKeyDev.sbFormatOrgLabel( out center, comboBoxCenters.Text ))
				{
					CProgram.sLogLine( "Select Center first!" );
				}
				else if (CProgram.sbReqLabel( "Create SMTP Password", "Server", ref _mStmpServer, "", false )
					&& CProgram.sbReqLabel( "Create SMTP Password", "User", ref _mSmtpUser, "", false )
					&& CProgram.sbReqLabel( "Create SMTP Password", "Password", ref _mStmpPW, "", false ))
				{
					string name = _mSmtpUser.Trim() + "@" + _mStmpServer.Trim() + ":smtpPW";
					string plainText = _mStmpPW;

					textBoxItemName.Text = name;
					textBoxPlainText.Text = plainText;

					string encrypted, decrypted;
					CPasswordString pw = new CPasswordString( name, center );

					bool b = pw.mbEncrypt( plainText );

					encrypted = pw.mGetEncrypted();
					textBoxEncrypted.Text = encrypted;
					if (b == false)
					{
						CProgram.sLogError( "Encrypt SMTP PW item " + name + " failed encrypt" );
					}
					else
					{
						decrypted = pw.mDecrypt();

						if (plainText == decrypted)
						{
							CProgram.sLogLine( "Encrypt SMTP PW item " + name + " successful copied to Clipboard." );
							Clipboard.SetText( encrypted );
						}
						else
						{
							CProgram.sLogError( "Encrypt SMTP PW item " + name + " failed reverse" );
						}
					}
				}
			}
			catch (Exception ex)
			{
				CProgram.sLogException( "Encrypt SMTP PW", ex );
			}

		}

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            UInt32 startNr = 0, lastNr = 0;
            string startStr = "", frontStr, nrStr;

            if (CProgram.sbReqLabel("Generate serial numbers (last 4 are digits)", "Start snr", ref startStr, "", false))
            {
                if (startStr == null || startStr.Length < 6)
                {
                    CProgram.sbReqLabel("Generate serial numbers (last 4 are digits)", "Start snr", ref startStr, "", false);
                }
                else
                {
                    int len = startStr.Length - 4;
                    frontStr = startStr.Substring(0, len);

                    if (false == UInt32.TryParse(startStr.Substring(len), out startNr))
                    {
                        CProgram.sLogError("Invalid snr: " + startStr);
                    }
                    else
                    {
                        lastNr = startNr;

                        if (CProgram.sbReqUInt32("Generate serial numbers (last 4 digits)", "last number", ref lastNr, "", 0, 9999))
                        {
                            if (lastNr < startNr)
                            {
                                CProgram.sLogError("Last nr < start nr: " + startStr);

                            }
                            else
                            {
                                UInt32 n = 0;
                                UInt32 snr = startNr;
                                string lines = "";

                                while (snr <= lastNr)
                                {
                                    lines += frontStr + snr.ToString("D4") + "\r\n";
                                    ++n;
                                    ++snr;
                                }
                                Clipboard.SetText(lines);
                                CProgram.sLogLine("On clipboard " + n.ToString() + " snr from " + startStr + " to " + lastNr.ToString());
                            }
                        }
                    }
                }
            }
        }

        private void buttonCopyPC_Click(object sender, EventArgs e)
        {
 
            Clipboard.SetText("!invalid license!");
            if(mbCreateLicKey(false))
            {
                string line = "";
                string pcName = CLicKeyDev.sGetPcName();
                if ( pcName == null || pcName.Length < 2 )
                {
                    CProgram.sLogError("Invalid PC name");
                }
                else
                {
                    if( textBoxProgFirst.Text == "11")
                    {
                        line += "I-Reader ";
                    }
                    if (textBoxProgLast.Text == "21")
                    {
                        line += "Eventboard ";
                    }
                    line += "license for " + pcName + ":";
                    Clipboard.SetText("\r\n"+ line + "\r\n");
                    CProgram.sLogLine("Clipboard= " + line);
                }
            }
        }

        private void buttonTestProgr_Click_1(object sender, EventArgs e)
        {

        }

        private void buttonDate_Click(object sender, EventArgs e)
        {
            string line = "!invalid end date";
            string endDate = textBoxEndDate.Text;
            UInt32 iEndDate;

            if( endDate != null && endDate.Length == 8 
                && UInt32.TryParse( endDate, out iEndDate)
                && iEndDate > 20170101 && iEndDate < 21000000 )
            {
                line = CProgram.sCalcYMD(DateTime.Now) + "\t" + endDate;
                CProgram.sLogLine("Clipboard =" + line);                
            }
            else
            {
                CProgram.sLogLine("Invalid end date!");
            }
            Clipboard.SetText(line);

        }

        private void buttonEncrypt_Click( object sender, EventArgs e )
		{
			string center;
			string name = textBoxItemName.Text;

			if (false == CLicKeyDev.sbFormatOrgLabel( out center, comboBoxCenters.Text ))
			{
				CProgram.sLogLine( "Select Center first!" );
			}
			else if (name == null && name.Length < 4)
			{
				CProgram.sLogLine( "Item name not ok!" );
			}
			else { 
				name = name.Trim();

				string plainText = textBoxPlainText.Text;
				string encrypted, decrypted;
				Int32 value = 0;

				if (radioButtonPassword.Checked)
				{
					try
					{
						// password encryption
						CPasswordString pw = new CPasswordString( name, center );

						bool b = pw.mbEncrypt( plainText );

						encrypted = pw.mGetEncrypted();
						textBoxEncrypted.Text = encrypted;
						if (b == false)
						{
							CProgram.sLogError( "Encrypt PW item " + name + " failed encrypt" );
						}
						else
						{
							decrypted = pw.mDecrypt();

							if (plainText == decrypted)
							{
								CProgram.sLogLine( "Encrypt PW item " + name + " successful" );
							}
							else
							{
								CProgram.sLogError( "Encrypt PW item " + name + " failed reverse" );
							}
						}

					}
					catch ( Exception ex )
					{
						CProgram.sLogException( "Encrypt PW", ex );
					}
				}
				else if( radioButtonString.Checked)
				{
					// encrypted string
					try
					{
						CEncryptedString pw = new CEncryptedString();
						pw.mbEncrypt( plainText );

						encrypted = pw.mGetEncrypted();
						textBoxEncrypted.Text = encrypted;

						decrypted = pw.mDecrypt();

						if (encrypted == decrypted)
						{
							CProgram.sLogLine( "Encrypt String item " + name + " successful" );
						}
						else
						{
							CProgram.sLogError( "Encrypt String item " + name + " failed reverse" );
						}

					}
					catch (Exception ex)
					{
						CProgram.sLogException( "Encrypt String", ex );
					}
				}
				else if( false == Int32.TryParse(plainText, out value))
				{
					CProgram.sLogError( "Encrypt Int: no integer value to encrypt" );
				}
				else
				{
					// encrypted int
					try
					{
						CEncryptedInt pw = new CEncryptedInt();
						Int32 encryptedInt, decryptedInt;

						pw.mEncrypt( value );

						encryptedInt = pw.mGetEncryptedInt();
						textBoxEncrypted.Text = encryptedInt.ToString();

						decryptedInt = pw.mDecrypt();

						if (value == decryptedInt)
						{
							CProgram.sLogLine( "Encrypt Int item " + name + " successful" );
						}
						else
						{
							CProgram.sLogError( "Encrypt Int item " + name + " failed reverse" );
						}

					}
					catch (Exception ex)
					{
						CProgram.sLogException( "Encrypt Int", ex );
					}

				}
			}
		}

		private void buttonDecrypt_Click( object sender, EventArgs e )
		{
			string center;
			string name = textBoxItemName.Text;

			if (false == CLicKeyDev.sbFormatOrgLabel( out center, comboBoxCenters.Text ))
			{
				CProgram.sLogLine( "Select Center first!" );
			}
			else if (name == null && name.Length < 4)
			{
				CProgram.sLogLine( "Item name not ok!" );
			}
			else
			{
				name = name.Trim();

				string encrypted = textBoxEncrypted.Text;
				string decrypted;
				Int32 value = 0;

				if (radioButtonPassword.Checked)
				{
					try
					{
						// password encryption
						CPasswordString pw = new CPasswordString( name, center );


						bool b = pw.mbSetEncrypted( encrypted );
						if (b == false)
						{
							CProgram.sLogError( "Encrypt PW item " + name + " failed setEncrypted" );
						}
						else
						{
							decrypted = pw.mDecrypt();
							string old = textBoxPlainText.Text;
							textBoxPlainText.Text = decrypted;

							string result = "";
							if( old != null && old.Length > 0)
							{
								result = old == decrypted ? ", same als old plain." : ", different to old plain";
							}
							CProgram.sLogLine( "decrypt string done" + result );
						}

					}
					catch (Exception ex)
					{
						CProgram.sLogException( "Encrypt PW", ex );
					}
				}
				else if (radioButtonString.Checked)
				{
					// encrypted string
					try
					{
						CEncryptedString pw = new CEncryptedString();
						pw.mbSetEncrypted( encrypted );

	
						decrypted = pw.mDecrypt();
						textBoxPlainText.Text = decrypted;

					}
					catch (Exception ex)
					{
						CProgram.sLogException( "Encrypt String", ex );
					}
				}
				else if (false == Int32.TryParse( encrypted, out value ))
				{
					CProgram.sLogError( "Encrypt Int: no integer value to decrypt" );
				}
				else
				{
					// encrypted int
					try
					{
						CEncryptedInt pw = new CEncryptedInt();
						Int32 encryptedInt, decryptedInt;

						bool b = pw.mbSetEncrypted( value );

						decryptedInt = pw.mDecrypt();
						textBoxPlainText.Text = decryptedInt.ToString();

					}
					catch (Exception ex)
					{
						CProgram.sLogException( "Encrypt Int", ex );
					}
				}
			}
		}

		public string mGetCenterDeviceTest( string ACenterName, UInt32 ATestDate, out UInt32 ArNrSirona, out UInt32 ArNrTZ, out UInt32 ArNrDV2 )
		{
			string result = ACenterName;

			ArNrSirona = ArNrTZ = ArNrDV2 = 0;

			string collection = "Sirona";
			UInt64 seed = 0x8393F6D3840A272B;
			UInt32 nrDevices = 0;
			UInt32 day0 = 0;

			bool b = mbTestCenterListKey( out nrDevices, out day0, ACenterName, ATestDate, collection, seed );

			result += b ? "\t" + day0.ToString() + "\t" + nrDevices : "\tNA\t0";
			ArNrSirona = nrDevices;
			//
			collection = "TZ";
			seed = 0x4A2277CE8090D3E4;

			nrDevices = 0;
			day0 = 0;

			b = mbTestCenterListKey( out nrDevices, out day0, ACenterName, ATestDate, collection, seed );

			result += b ? "\t" + day0.ToString() + "\t" + nrDevices : "\tNA\t0";
			ArNrTZ = nrDevices;

			//
			collection = "DV2";
			seed = 0xC653DE78AC890923;
			nrDevices = 0;
			day0 = 0;

			b = mbTestCenterListKey( out nrDevices, out day0, ACenterName, ATestDate, collection, seed );

			result += b ? "\t" + day0.ToString() + "\t" + nrDevices : "\tNA\t0";
			ArNrDV2 = nrDevices;
			return result;
		}

		private bool mbTestCenterListKey( out UInt32 ArNrDevices, out UInt32 ArDay0, string ACenterName, UInt32 ATestDate, string ACollection, UInt64 ASeed )
		{
			bool bOk = false;
			UInt32 nrDevices = 0;
			UInt32 day0 = 0;

			try
			{
				CItemKey readKey = new CItemKey( ACollection );

				string centerPath = Path.Combine( _mLicKeyDir, ACenterName );

				if (false == Directory.Exists( centerPath ))
				{
					CProgram.sLogError( "Center directory does not exist: " + centerPath );
				}
				else
				{
					string inFile = Path.Combine( centerPath, CItemKey.sMakeItemKeyFileName( ACollection ) );
					bool bCheckSumOk;

					bOk = readKey.mbReadFromFile( out bCheckSumOk, inFile, ASeed, ACenterName, true );

					if (bOk)
					{
						bOk = bCheckSumOk;
					}
					if (bOk)
					{
						bOk = readKey.mbGetParamUInt32( out day0, CItemKey._cParamDay0 );
					}
					if (bOk)
					{
						UInt32 nAtDay = 0;
						DateTime dtTest = DateTime.Now;
						CProgram.sbSplitYMD( ATestDate, out dtTest );

						List<string> strList = readKey.mGetValidItems( out nAtDay, dtTest, 14, day0 );

						nrDevices = (UInt32)(strList == null ? 0 : strList.Count);
					}
				}
			}
			catch (Exception ex)
			{
				CProgram.sLogException( "LicKey " + ACollection, ex );
			}
			ArNrDevices = nrDevices;
			ArDay0 = day0;
			return bOk;
		}

		private void toolStripListLicenses_Click( object sender, EventArgs e )
		{
			try
			{
				int n = comboBoxCenters.Items.Count;
				int i;
				string center;
				string result, resultList = "";
				Int32 testDate = 0;
				UInt32 nrSirona, nrTZ, nrDV2;
				UInt32 totSirona, totTZ, totDV2;
				
				if ( n > 1)
				{
					if( CProgram.sbReqInt32("Center device list", "Test date (yyyymmdd or rel)", ref testDate, "", -100, 99999999))
					{
						totSirona = totTZ = totDV2 = 0;
						if (  testDate < 1000)
						{
							testDate += (Int32)CProgram.sCalcYMD(DateTime.Now);
						}
						else if( testDate < 10000)
						{
							testDate += (Int32)CProgram.sCalcYMD( (UInt16)DateTime.Now.Year, 0, 0 );
						}
						resultList = "\r\nCenter\tSirona\t\tTZ\t\tDV2\r\n";
						resultList += "Name\tEndDate\tdevices\tEndDate\tdevices\tEndDate\tdevices\r\n";
						for (i = 1; i < n; ++i)
						{
							center = comboBoxCenters.Items[i].ToString();

							result = mGetCenterDeviceTest( center, (UInt32)testDate, out nrSirona, out nrTZ, out nrDV2 );
							totSirona += nrSirona;
							totTZ += nrTZ;
							totDV2 += nrDV2;

							resultList += result + "\r\n";
						}
						resultList += "\r\n+Total\tSirona\t"+totSirona.ToString() + "\tTZ\t" + totTZ.ToString() + "\tDV2\t" + totDV2.ToString() + "\r\n";
						CProgram.sLogInfo( resultList );
						Clipboard.SetText( resultList );
						CProgram.sLogLine( "Device lict copied to clipboard.");
					}
				}
			}
			catch (Exception ex)
			{
				CProgram.sLogException( "List all center devices", ex );
			}
		}
	}
}
