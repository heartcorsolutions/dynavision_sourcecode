﻿using Event_Base;
using Program_Base;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 *  plethysmographic waveform (pulse waveform) from Nonin pulse oximeter infra-red signal. 
 *  stored in DVX as 16 bit at 75 samples per second
*/

namespace EventBoard.Event_Base
{
    public class CConvertValues
    {
        public Int32 _mLowFrom;
        public double _mLowTo;

        public Int32 _mHighFrom;
        public double _mHighTo;

        private double _mScaleFactor;

        public CConvertValues()
        {
            _mLowFrom = 0;
            _mLowTo = 0;

            _mHighFrom = 1000;
            _mHighTo = 1000;

            _mScaleFactor= 1;
        }
        public bool mbInit()
        {
            Int32 deltaFrom = _mHighFrom - _mLowFrom;
            double deltaTo = _mHighTo - _mLowTo;

            bool bOk = deltaFrom != 0;

            if (bOk)
            {
                _mScaleFactor = deltaTo / deltaFrom;
            }
            return bOk;
        }
        public bool mbSetup( Int32 ALowFrom, double ALowTo, Int32 AHighFrom, double AHighTo)
        {
            _mLowFrom = ALowFrom;
            _mLowTo = ALowTo;

            _mHighFrom = AHighFrom;
            _mHighTo = AHighTo;

            return mbInit();
        }
        public double mConvert( Int32 AValue)
        {
            return _mHighTo + (AValue - _mLowFrom) * _mScaleFactor; 
        }
    }


    public class CDvxPleth
    {
        public const UInt16 _cNrFramesPerSecond = 75;
        const double _cFrameUnitSec = 1.0 / _cNrFramesPerSecond;
        const string _cCsvTab = "\t";
        const UInt16 _cNrBytesInFrame = 2;

        // First file
        private string _mConvertSpO2Path = "";

        private DateTime _mFirstFileUTC = DateTime.MinValue;    // base date time
//        private string _mFirstFullName = "";
        private string _mFirstFileName = "";

        private bool _mbWriteCsv = false;
        private bool _mbWriteMit16 = false;
        private string _mCsvFullName = "";
        private StreamWriter _mCsvWriter = null;

        private bool _mbDebugRead = true;
        private string _mDebugReadLog = "";
        // Multiple files

        public UInt16 _mFileCount = 0;
        public string _mFileFullName;
        public string _mFileName;
        public DateTime _mFileUTC;
        public UInt32 _mFileSize = 0;
        public UInt32 _mFileBytesAdded = 0;
        public UInt32 _mDeviceNr = 0;

            
        public CDvxPleth(bool AbWrite2Csv, bool AbWriteMit16)
        {
            _mbWriteCsv = AbWrite2Csv;
            _mbWriteMit16 = AbWriteMit16;


        }
         ~CDvxPleth()
        {
            mClose();
        }
        public void mClose()
        {
            if( null != _mCsvWriter)
            {
                _mCsvWriter.Flush();
                _mCsvWriter.Close();
                _mCsvWriter = null;
            }
        }
        public bool mbCsvOpenFile(string APath, string AFileSpO2Name)
        {
            bool bOk = false;

            try
            {
                if (AFileSpO2Name != null && AFileSpO2Name.Length > 10)
                {
                    _mCsvFullName = Path.Combine( APath, AFileSpO2Name + "_" + CProgram.sDateTimeToYMDHMS( DateTime.Now ) + ".csv");
                    _mCsvWriter = new StreamWriter(_mCsvFullName);

                    string s = " _yyyMMddHHmmss " + _cCsvTab + "iFrame"
                        +_cCsvTab + "sec" + _cCsvTab + "Pleth";
                    _mCsvWriter.WriteLine(s);

                    bOk = true;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("DvxSpO2 open csv", ex);
            }
            return bOk;
        }
        public bool mbCsvWritePleth(UInt32 AIndex, UInt32 APleth)
        {
            bool bOk = false;

            try
            {
                if (null != _mCsvWriter)
                {
                    /*                   string s = "_yyyMMddHHmmss" + _cCsvTab + "iFrame" + _cCsvTab + "signal"
                                           + _cCsvTab + "sec" + "Pleth"
                                           + _cCsvTab + "Packet" + _cCsvTab + "seq" + _cCsvTab + "sync"
                                           + "Info" + _cCsvTab + "value";
                                           */
                    double t =AIndex  * _cFrameUnitSec;
                    DateTime frameUTC = _mFirstFileUTC.AddSeconds(t);

                    string s = "\"_" + CProgram.sDateTimeToYMDHMS(frameUTC) + "\""
                    + _cCsvTab + AIndex
                    + _cCsvTab + t.ToString("0.00") + _cCsvTab + APleth;
                    _mCsvWriter.WriteLine(s);
                    bOk = true;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("DvxSpO2 write csv", ex);
            }
            return bOk;
        }
        
        public bool mbTryParseDvxName(string AFileName)
        {
            bool bDvx = false;
            int len = AFileName == null ? 0 : AFileName.Length;
            int pos = len == 0 ? -1 : AFileName.IndexOf('_');
            // 123456_yyyyMMddHHmmss*******
            if( len >= 15 && pos >= 0)
            {
                string deviceName = AFileName.Substring(0, pos);
                string s = AFileName.Substring(pos+1);
                len = s.Length;
                if( len > 14 )
                {
                    s = s.Substring(0, 14);

                    UInt32.TryParse(deviceName, out _mDeviceNr);
                    bDvx = CProgram.sbParseYMDHMS(s, out _mFileUTC);
                }
            }
            return bDvx;
        }
        public bool mbWriteMit16(CRecordMit ARecord)
        {
            bool bOk = false;

            if( null != ARecord && _mConvertSpO2Path != null && _mConvertSpO2Path.Length > 2
                && ARecord.mNrSamples > 0)
            {
                string name = Path.GetFileNameWithoutExtension(_mFirstFileName)
                    + "_" + CProgram.sDateTimeToYMDHMS(DateTime.Now)
                    + (_mFileCount <= 1 ? "" : "N" + _mFileCount);

                string fullName = Path.Combine(_mConvertSpO2Path, name + ".hea");

                bOk = ARecord.mbSaveMIT(fullName, 16, true);
            }

            return bOk;
        }
        public CRecordMit mReadOneFile( string AFullName )
        {
            CRecordMit rec = null;

            try
            {
                if(AFullName != null && AFullName.Length > 10)
                {
                    _mFileFullName = AFullName;
                    _mFileName = Path.GetFileName(AFullName);
                    _mFileSize = 0;
                    _mFileBytesAdded = 0;
/*                    _mFramesCount = 0;
                    _mFramesOkCount = 0;
                    _mFramesSignalLow = 0;
                    _mFramesSignalMedium = 0;
                    _mFramesSignalHigh = 0;
*/                    _mFileUTC = DateTime.MinValue;
 
                    mbTryParseDvxName(_mFileName);// sets deviceNr and FileUTC

                    byte[] data = File.ReadAllBytes(AFullName);
                    Int32 nData = data == null ? 0 : data.Length;

                    if (_mbDebugRead)
                    {
                        Int32 nFrames = nData / _cNrBytesInFrame;
                        double t = nFrames * _cFrameUnitSec;
                        string s = _mFileCount + "\t" + _mFileName + "\t" + nData + "bytes\t"
                            + nFrames + " frames \t " + t.ToString( "0.00") +  " sec";
                        _mDebugReadLog += s + "\r\n";
                    }

                    if (nData > 0)
                    {
                        _mFileSize = (UInt32)nData;

                        if (++_mFileCount == 1)
                        {
                            _mFirstFileName = _mFileName;
                            _mFirstFileUTC = _mFileUTC;

                            if(_mbWriteCsv || _mbWriteMit16)
                            {
                                _mConvertSpO2Path = Path.Combine(Path.GetDirectoryName(AFullName), "convertSpO2");

                                CDvtmsData.sbCreateDir(_mConvertSpO2Path);
                            }
                            if (_mbWriteCsv)
                            {                             
                                mbCsvOpenFile(_mConvertSpO2Path, _mFirstFileName);
                            }
                        }

                        rec = new CRecordMit(CDvtmsData.sGetDBaseConnection());

                        if (rec.mbCreateSignals(1) && rec.mbCreateAllSignalData(_mFileSize / 2 + 100))
                        {
                            CSignalData signal = rec.mSignals[0];
                            UInt32 n = 0;

                            rec.mBaseUTC = DateTime.SpecifyKind( _mFileUTC, DateTimeKind.Utc );
                            rec.mSampleFrequency = _cNrFramesPerSecond;
                            rec.mSampleUnitT = (float)_cFrameUnitSec;
                            rec.mAdcGain = 1000;
                            rec.mSampleUnitA = 1.0F / rec.mAdcGain;
                            rec.mSignalLabels = "Pleth";
                            rec.mSignalUnits = "*";

                            for (UInt32 i = 0; i < nData; ++i)
                            {
                                byte lsb = data[i];

                                if (++i < nData)
                                {
                                    byte msb = data[i];

                                    UInt32 pleth = (((UInt32)(msb)) << 8) | ((UInt32)(lsb));

                                    signal.mAddValue((Int32)pleth);
                                    mbCsvWritePleth(++n, pleth);
                                }

                            }
                            rec.mNrSamples = signal.mNrValues;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("DvxSpO2 read one file " + AFullName, ex);
                rec = null;
            }
            return rec == null || rec.mGetNrSamples() == 0 ? null : rec;
        }

        public CRecordMit mReadMultipleFiles(string[] AMergeFullNames, float AFillGapSec)
        {
            CRecordMit recSum = null;
            Int32 nFiles = AMergeFullNames == null ? 0 : AMergeFullNames.Length;
            double fillGapSec = AFillGapSec < 0.01 ? 999999.999 : AFillGapSec;

            try
            {
                List<CRecordMit> recList = new List<CRecordMit>();
                Int32 nRecords = 0;
                bool bMergeRaw = false;

                if (nFiles > 0 && recList != null)
                {
                    for (Int32 iFile = 0; iFile < nFiles; ++iFile)
                    {
                        string fullName = AMergeFullNames[iFile];

                        if (fullName != null && fullName.Length > 1)
                        {
                            CRecordMit rec = mReadOneFile(fullName);

                            if( rec != null)
                            {
                                DateTime baseUtc = rec.mBaseUTC;

                                if( baseUtc == DateTime.MinValue )
                                {
                                    recList.Add(rec);
                                    ++nRecords;
                                    bMergeRaw = true;   // no time in file => add all file data without check for time gaps
                                }
                                else
                                {
                                    // add by time
                                    int insertPos = 0;

                                    while(insertPos < nRecords)
                                    {
                                        if(baseUtc < recList[insertPos].mBaseUTC)
                                        {
                                            break; // sort by base time
                                        }
                                        ++insertPos;
                                    }
                                    if(insertPos < nRecords)
                                    {
                                        recList.Insert(insertPos, rec); 
                                    }
                                    else
                                    {
                                        recList.Add(rec);
                                    }
                                    ++nRecords;                                   
                                }
                            }
                        }
                    }
                    recSum = new CRecordMit(CDvtmsData.sGetDBaseConnection());
                    if( nRecords > 0 && recSum != null)
                    {
                        if( bMergeRaw)
                        {
                            UInt32 nValues = 0;

                            for (int i = 0; i < nRecords; ++i) nValues += recList[i].mNrSamples;

                            if( recSum.mbCreateSignals( 1 ) && recSum.mbCreateAllSignalData(nValues + 1000))
                            {
                                CSignalData signal = recSum.mSignals[0];
                                Int32 vMin = Int32.MaxValue;
                                Int32 vMax = Int32.MinValue;
                                Int32 v;

                                recSum.mBaseUTC = recList[0].mBaseUTC;
                                recSum.mSampleFrequency = recList[0].mSampleFrequency;
                                recSum.mSampleUnitT = recList[0].mSampleUnitT;
                                recSum.mAdcGain = recList[0].mAdcGain;
                                recSum.mSampleUnitA = 1.0F / recSum.mAdcGain;
                                recSum.mSignalLabels = "Pleth";
                                recSum.mSignalUnits = "*";

                                for ( Int32 iRec = 0; iRec < nRecords; ++iRec)
                                {
                                    Int32[] values = recList[iRec].mSignals[0].mValues;
                                    UInt32 n = recList[iRec].mSignals[0].mNrValues;

                                    for ( int i = 0; i < n; ++i)
                                    {
                                        v = values[i];
                                        signal.mAddValue(v);
                                        if (v < vMin) vMin = v;
                                        if (v > vMax) vMax = v;
                                    }
                                }
                                signal.mMinValue = vMin;
                                signal.mMaxValue = vMax;
                                recSum.mNrSamples = signal.mNrValues;
                            }
                        }
                        else
                        {
                            Int32 iLast = nRecords - 1;
                            UInt32 sampleRate = recList[0].mSampleFrequency;
                            double unitT = 1.0 / sampleRate;
                            DateTime startUTC = recList[0].mBaseUTC;
                            DateTime endUTC = recList[iLast].mBaseUTC.AddSeconds(recList[iLast].mGetSamplesTotalTime());
                            DateTime curUTC = startUTC;

                            UInt32 nValues = (UInt32)((endUTC-startUTC).TotalSeconds * sampleRate + 0.5);
       
                            if (recSum.mbCreateSignals(1) && recSum.mbCreateAllSignalData(nValues + 1000))
                            {
                                CSignalData signal = recSum.mSignals[0];

                                recSum.mBaseUTC = recList[0].mBaseUTC;
                                recSum.mSampleFrequency = recList[0].mSampleFrequency;
                                recSum.mSampleUnitT = recList[0].mSampleUnitT;
                                recSum.mAdcGain = recList[0].mAdcGain;
                                recSum.mSampleUnitA = 1.0F / recSum.mAdcGain;
                                recSum.mSignalLabels = "Pleth";
                                recSum.mSignalUnits = "*";
                                nValues = 0;
                                Int32 vMin = Int32.MaxValue;
                                Int32 vMax = Int32.MinValue;
                                Int32 v;

                                for (Int32 iRec = 0; iRec < nRecords; ++iRec)
                                {
                                    Int32[] values = recList[iRec].mSignals[0].mValues;
                                    UInt32 n = recList[iRec].mSignals[0].mNrValues;
                                    DateTime baseUTC = recList[iRec].mBaseUTC;
                                    Int32 iStart = 0;
                                    double gap = (baseUTC - curUTC).TotalSeconds;
                                    double allowedGapPos = fillGapSec * (1.001 + 0.5 * iRec);
                                    double allowedGapNeg = fillGapSec * (1.001 + 0.05 * iRec);

                                    if( gap >= allowedGapPos)
                                    {
                                        Int32 nGap = (Int32)(gap * sampleRate);
                                        v = 0;

                                        for (int i = 0; i < nGap; ++i)
                                        {                                           
                                            signal.mAddValue(v);    // fill gap
                                        }
                                        if (v < vMin) vMin = v;
                                        if (v > vMax) vMax = v;

                                    }
                                    else if( gap < -allowedGapNeg)
                                    {
                                        iStart = (Int32)(gap * sampleRate); // skip overlap
                                    }
                                    for (int i = iStart; i < n; ++i)
                                    {
                                        v = values[i];
                                        signal.mAddValue(v);
                                        if (v < vMin) vMin = v;
                                        if (v > vMax) vMax = v;
                                    }
                                    nValues = signal.mNrValues;
                                    curUTC = startUTC.AddSeconds(nValues * unitT);
                                }
                                signal.mMinValue = vMin;
                                signal.mMaxValue = vMax;
                                recSum.mNrSamples = nValues;
                            }
                        }
                    }
                    //                            recSum.mNrSamples = signal.mNrValues;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("DvxSpO2 read multiple files " + _mFileFullName, ex);
                recSum = null;
            }
            return recSum == null || recSum.mGetNrSamples() == 0 ? null : recSum;
        }
        public bool mbWriteSpo( string AToFullName, CRecordMit ARecord )
        {
            bool bOk = AToFullName != null && AToFullName.Length > 3 && ARecord != null && ARecord.mSignals != null;

            try
            {
                CSignalData signal = ARecord.mSignals[0];
                Int32[] values = signal == null ? null : signal.mValues;

                FileStream fw = new FileStream(AToFullName, FileMode.Create);

                if( fw != null && values != null)
                {
                    UInt32 n = signal.mNrValues;

                    for (UInt32 i = 0; i < n; ++i)
                    {
                        Int32 v = values[i];

                        fw.WriteByte((byte)(v & 0x0FF));            //LSB
                        fw.WriteByte((byte)((v >> 8 ) & 0x0FF));    //MSB
                    }
                    bOk = true;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("DvxSpO2 write file " + AToFullName, ex);

            }
            return bOk;
        }

        public static bool sbLoadPlethData(out string ArResultText, out CRecordMit ArRecord, string ADvxSpo2FilePath,
            Int16 ADefaultTimeZoneMin, DMoveTimeZone AMoveTimeZone)
        {
            bool bOk = false;
            CRecordMit rec = null;

            ArResultText = "";
            try
            {
                CDvxPleth dvxSpO2 = new CDvxPleth(false, false);


                if (dvxSpO2 == null)
                {
                    ArResultText = "Failed create dvxSpO2";
                }
                else
                {
                    rec = dvxSpO2.mReadOneFile(ADvxSpo2FilePath);

                    if( rec == null )
                    {
                        ArResultText = "Failed load dvxSpO2";
                    }
                    else if(rec.mNrSamples == 0 )
                    {
                        ArResultText = "Empty dvxSpO2 file";

                    }
                    else
                    {
                        rec.mTimeZoneOffsetMin = ADefaultTimeZoneMin;

                        if (AMoveTimeZone == DMoveTimeZone.Move)
                        {
                            rec.mbMoveTimeZone((Int16)ADefaultTimeZoneMin);
                        }
                        bOk = true;
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("DvxSpO2 read file " + ADvxSpo2FilePath, ex);

            }
            ArRecord = rec;

            return bOk;
        }
    }
}
