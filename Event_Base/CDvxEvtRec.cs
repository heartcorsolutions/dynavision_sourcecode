﻿using Event_Base;
using EventBoard.Event_Base;
using Program_Base;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Event_Base
{
    public enum DDvxEvent_Priority  // lower is higher
    {                   // give room for alarm sort value
        Manual = 100,
        Pause = 200,
        AFib = 300,
        Brady = 400,
        Tachy = 500,
        Other = 600,
        Disregard = 999
    }

    public class CDvxEvtRec
    {
        public const UInt32 _cDefaultAmplGainPermV = 20913;

        public static CSqlEnumList _sEnumDvxAlarmType = null;   // enum list with 

        private static string _sDvxStudyRoot = null;
        private const UInt32 _cFileSeed = 0x8D3745A4;
        public const string _cEventExtension = ".evt";
        public const string _cEcgExtension = ".dat";
        public const string _cTrendExtension = ".tnd";
        public const string _cPlethExtension = ".spo";
        public const string _cEvtRecExtension = ".DvxEvtRec";
        public const string _cEvtErrorExtension = ".DvxEvtError";
        public const string _cEvtSkipExtension = ".DvxEvtSkip";
        public const char _cFileSetChar = '+';
        public const char _cMergeEvents = '+';
        public const char _cMergeNrEvents = '_'; // must be different to MergeEvents : AAA+BBB+CC_3 
        public const string _cEcgConfigFolder = "Config";
        public const string _cEcgConfigName = "ads1298r.config";
        public const string _cDvxDeviceFolder = "DvxDevice";

        public UInt32 _mDeviceSnr;          // device serial number 6 digits
        public UInt32 _mStudyNr;           // study index number

        public string _mFileName;   // origianal event file name (.dat or .dat.evt)
        public DateTime _mFileStartUTC;          // Date Time of start Primairy file
        public UInt32 _mEventSampleNr;          // sample number the event occured
        public string _mEventType;              // primairy event type
        public string _mEventAllFile;           // contains the evt file <evt type>(<sample nr>)+<evt type>(<sample nr>);
        public DateTime _mEventUTC;             // Date Time of start Primairy file
        public Int16 _mTimeZoneOffsetMin;

        public float _mSamplesPerSec;          // sample frequency
        public float _mAdjustSpsFactor;         // adjust the sample frequency to fit with time stamp factor sps = ((1-f) * SamplesPerSec + (f*SpcCalc)
        public Int32 _mAmplGainPermV;          // gain used for amplitude value
        public UInt16 _mHardwareScale;           // harware scaling used at ECG chip
        public UInt32 _mHardwareClockHz;        // sample frequency reference clock

        public UInt16 _mNrChannelsInFile;       // number of channels in file
        public bool _mbLeadOffInFile;           // first channel is leadoff channel in file
        public bool _mbLeadOffSkip;             // skip leadoff channel
        public bool _mbRespSkipFirstChannel;    // skip resporation (first) channel on 1705
        public UInt16 _mLoadChannelsMask;     // channels mask for loading file into a reacord

        public UInt16 _mNrBytesPerSample;       // number of bytes per sample (default 3: 24 bits)
        public bool _mbSignExtend;              // sign extend value to become 32 bit signed integer
        public bool _mbRevertBytesMSB;          // revert bytes (false = LSB(HEA), true=MSB), default true
        public bool _mbFillGaps;                // fill in the gaps with squeare 0.1mV at sps/4
        public bool _mbExactStrip = false;      // strip must be exact length starting at -_mPreEventSec and ending at _mPostEventSec

        public UInt32 _mPreEventSec;            // requested nr seconds before event
        public UInt32 _mPostEventSec;           // requested nr seconds after event

        public List<string> _mDatFiles;         // files to be included. // selected files or files calculated the first time containing the strip
        public bool _mbLoadFromStudy;           // load files from study (otherwise from recording folder)

        public string _mValuesOrigin;           // indicates origin of settings {created, cleared, default, device, file}
        public bool _mbManualImport;           // indicates origin of settings {created, cleared, default, device, file}
        public bool _mbReadCheckSumOK;           //file has not changed

        public UInt32 _mRecordIX;               // record number 
        public string _mSourceFilePath;         // import source file

        // Pleth
        public bool _mbPlethLoad;
        public List<string> _mPlethFiles;         // Pleth files to be included. // selected files or files calculated the first time containing the strip
        public CConvertValues _mPlethConvert;     // convert pleth file 16 bit to ECG Record mV (1 mV -> 100 mmHg)
        public float _mPlethSamplesPerSec;
        public float _mPlethMergeOffsetSec;         // merge Pleth into ecg record time offset;
        public UInt32 _mPlethPreEcgSec;            // load nr seconds before ecg (pleth and ecg do not have the same file duration)
        public UInt32 _mPlethPostEcgSec;           // load nr seconds after ecg  (merging pleth in ecg will remove overlap)


        public CDvxPleth _mPlethData;


        public CDvxEvtRec()
        {
            mClear();
            _mValuesOrigin = "Created";

            _mbPlethLoad = true;
            _mPlethConvert = new CConvertValues();     // convert pleth file 16 bit to ECG Record mV (1 mV -> 100 mmHg)

            _mPlethConvert.mbSetup(0, 0, UInt16.MaxValue, 200.0); // max 2 mv to be in range of ECG signals
        }

        public void mClearRecording(UInt32 ADeviceSnr)
        {
            _mDeviceSnr = ADeviceSnr;          // device serial number
            _mStudyNr = 0;                     // study index number

            _mFileName = "";            // origianal event file name (.dat or .dat.evt) and store file name (.DvxEvtRec)
            _mFileStartUTC = DateTime.MinValue;  // Date Time of start Primairy file
            _mEventSampleNr = 0;
            _mEventType = "";                   // primairy event type
            _mEventUTC = DateTime.MinValue;     // Date Time of start Primairy file
                                                //            _mTimeZoneOffsetMin = 0;
            _mEventAllFile = "";                // contains the evt file <evt type>(<sample nr>)+<evt type>(<sample nr>);


            _mRecordIX = 0;
            _mSourceFilePath = "";

            _mDatFiles = null;                  // files to be included. // selected files or files calculated the first time containing the strip


            // Pleth
            _mPlethFiles = null;         // Pleth files to be included. // selected files or files calculated the first time containing the strip
            _mPlethData = null;

        }
        public void mClear()
        {
            _mDeviceSnr = 0;                   // device serial number
            _mStudyNr = 0;                     // study index number

            _mFileName = "";            // origianal event file name (.dat or .dat.evt) and store file name (.DvxEvtRec)
            _mFileStartUTC = DateTime.MinValue;  // Date Time of start Primairy file
            _mEventSampleNr = 0;
            _mEventType = "";                   // primairy event type
            _mEventUTC = DateTime.MinValue;     // Date Time of start Primairy file
            _mEventAllFile = "";           // contains the evt file <evt type>(<sample nr>)+<evt type>(<sample nr>);

            _mTimeZoneOffsetMin = 0;

            _mSamplesPerSec = 1000;             // sample frequency
            _mAdjustSpsFactor = 0;         // adjust the sample frequency to fit with time stamp factor sps = ((1-f) * SamplesPerSec + (f*SpcCalc)

            _mAmplGainPermV = (Int32)_cDefaultAmplGainPermV;            // gain used for amplitude value (calibration value)
            _mHardwareScale = 6;                 // harware scaling used at ECG chip
            _mHardwareClockHz = 32000;          // sample frequency reference clock

            _mNrChannelsInFile = 4;             // number of channels in file
            _mbLeadOffInFile = true;           // first channel is leadoff channel in file
            _mbLeadOffSkip = true;             // skip leadoff channel
            _mbRespSkipFirstChannel = true;
            _mLoadChannelsMask = 12;           // channels mask for loading file into a reacord
                                               // B0=no lead off, B1=CH1, B2=CH2

            _mNrBytesPerSample = 3;             // number of bytes per sample (default 3: 24 bits)
            _mbSignExtend = true;               // sign extend value to a signed 32 bit integer
            _mbRevertBytesMSB = true;              // revert bytes (LSB-MSB), default true
            _mbFillGaps = true;                 // fill in the gaps with squeare 0.1mV at sps/4
                                                /*not implemented*/
            _mbExactStrip = false;              // strip must be exact length starting at -_mPreEventSec and ending at _mPostEventSec

            _mPreEventSec = 32;                 // requested nr seconds before event    (added 2 for rounding spread of time)
            _mPostEventSec = 32;                // requested nr seconds after event

            _mDatFiles = null;                  // files to be included. // selected files or files calculated the first time containing the strip
            _mbLoadFromStudy = true;            // load files from study (otherwise from recording folder)
            _mValuesOrigin = "cleared";
            _mbManualImport = true;
            _mbReadCheckSumOK = false;

            _mRecordIX = 0;
            _mSourceFilePath = "";

            // Pleth
            _mPlethFiles = null;         // Pleth files to be included. // selected files or files calculated the first time containing the strip
            _mPlethData = null;
            _mPlethSamplesPerSec = 75;
            _mPlethMergeOffsetSec = 0;
            _mPlethPreEcgSec = 100;            // load nr seconds before ecg (pleth and ecg do not have the same file duration
            _mPlethPostEcgSec = 100;           // load nr seconds after ecg
        }

        public static bool sbSetDvxStudyFolder(string ADvxStudyFolder, bool AbLoadAlarmTypes)
        {
            bool bExists = false;
            _sDvxStudyRoot = null;

            if (ADvxStudyFolder != null && ADvxStudyFolder.Length >= 3 && Directory.Exists(ADvxStudyFolder))
            {
                _sDvxStudyRoot = ADvxStudyFolder;
                CProgram.sLogLine("DvxStudyFolder=" + _sDvxStudyRoot);
                bExists = true;
            }
            if (AbLoadAlarmTypes)
            {
                sbLoadDvxAlarmType();
            }
            return bExists;
        }

        public static string sGetDeviceName(UInt32 ADeviceSNR)
        {
            return ADeviceSNR.ToString("000000");
        }

        public static bool sbGetStudyDeviceDataFolder(out string ArPath, UInt32 AStudyNr, UInt32 ADeviceNr)
        {
            bool bExists = false;
            string path = _sDvxStudyRoot;

            if (_sDvxStudyRoot != null && _sDvxStudyRoot.Length >= 3 && AStudyNr > 0)
            {
                uint dir1 = AStudyNr / 10000;
                uint dir2 = (AStudyNr - dir1 * 10000) / 100;
                uint dir3 = AStudyNr % 100;

                path = Path.Combine(path, dir1.ToString("00"), dir2.ToString("00"), dir3.ToString("00"), AStudyNr.ToString("000000"), "DVX");

                bExists = Directory.Exists(path);
                if (ADeviceNr > 0)
                {
                    path = Path.Combine(path, sGetDeviceName(ADeviceNr));

                    bExists = Directory.Exists(path);
                }

            }
            ArPath = path;
            return bExists;
        }
        public static bool sbGetStudyDeviceFromRemark(out string ArPath, out UInt32 ArDvxStudyIX, String ARemark, string ADeviceSNR)
        {
            bool bExists = false;
            string dvxPath = "";
            UInt32 dvxStudyIX = 0;

            int pos = ARemark == null ? 0 : ARemark.IndexOf("dvxS");

            if (pos >= 0)
            {
                string studyNr = "";

                int n = ARemark.Length;

                pos += 3;
                while (++pos < n)
                {
                    char c = ARemark[pos];

                    if (char.IsDigit(c))    // get dvxStudyIX
                    {
                        studyNr += c;
                    }
                    else
                    {
                        break;
                    }
                }
                if (UInt32.TryParse(studyNr, out dvxStudyIX) && dvxStudyIX > 0)
                {
                    string dvxStudyFolder;
                    if (CDvxEvtRec.sbGetStudyDeviceDataFolder(out dvxStudyFolder, dvxStudyIX, 0))
                    {
                        dvxPath = dvxStudyFolder;
                        bExists = true;

                        if (ADeviceSNR != null && ADeviceSNR.Length > 0)
                        {
                            dvxStudyFolder = Path.Combine(dvxStudyFolder, ADeviceSNR);

                            if (Directory.Exists(dvxStudyFolder))
                            {
                                dvxPath = dvxStudyFolder;
                            }
                        }
                    }
                }
            }

            ArPath = dvxPath;
            ArDvxStudyIX = dvxStudyIX;
            return bExists;
        }


        public bool mbGetStudyDeviceDataFolder(out string ArPath)
        {
            return sbGetStudyDeviceDataFolder(out ArPath, _mStudyNr, _mDeviceSnr);
        }

        public static bool sbParseFileName(string AFilePath, out UInt32 ArDeviceSNR, out DateTime ArUtc, out String ArExtension)
        {
            uint deviceSNR = 0;
            string deviceName = "";
            DateTime utc = DateTime.MinValue;
            string extension = "";
            bool bOk = false;


            if (AFilePath != null && AFilePath.Length > 0)
            {
                string fileName = Path.GetFileNameWithoutExtension(AFilePath);  // 123456_yyyyMMddHHmmss[.evt|.dat|DvxEvtRec]
                extension = Path.GetExtension(AFilePath);

                if (fileName.Length > 0)
                {
                    int pos = fileName.IndexOf('_');// should be 6
                    if (pos >= 1)
                    {
                        deviceName = fileName.Substring(0, pos);

                        UInt32.TryParse(deviceName, out deviceSNR);

                        string dateTime = fileName.Substring(pos + 1); // YYYYMMddHHmmss

                        if (dateTime.Length > 14)
                        {
                            dateTime = dateTime.Substring(0, 14);
                        }
                        bOk = CProgram.sbParseYMDHMS(dateTime, out utc);

                        // accept all snr incl 0                        bOk &= deviceSNR > 0;
                    }
                }
            }
            ArDeviceSNR = deviceSNR;
            ArUtc = DateTime.SpecifyKind(utc, DateTimeKind.Utc);
            ArExtension = extension;
            return bOk;
        }
        public static bool sbFindStudy(string AFilePath, out UInt32 ArStudyNr)
        {
            UInt32 studyNr = 0;
            bool bOk = false;

            if (AFilePath != null && AFilePath.Length > 0)
            {
                // DVX study folder ...\<studynr>\DVX\<snr>\<snr>_yyyyMMddHHmmss.*
                string parent1path = Path.GetDirectoryName(AFilePath);  // snr
                string parent2path = Path.GetDirectoryName(parent1path);    // DVX
                string parent2 = Path.GetFileName(parent2path).ToUpper();
                if (parent2 == "DVX")
                {
                    string parent3path = Path.GetDirectoryName(parent2path); // study nr
                    string parent3 = Path.GetFileName(parent3path);


                    bOk = UInt32.TryParse(parent3, out studyNr);

                    bOk &= studyNr > 0;
                }
            }
            ArStudyNr = studyNr;
            return bOk;
        }
        public bool mbSaveFile(string AFilePath)
        {
            bool bOk = false;

            try
            {
                CItemKey ik = new CItemKey("DvxEvtRec");

                if (ik != null)
                {
                    string useFileSet = sCombineUsedFiles(_mDatFiles, 65000);
                    string plethFileSet = sCombineUsedFiles(_mPlethFiles, 65000);

                    ik.mbAddItemUInt32("DeviceSNR", _mDeviceSnr);
                    ik.mbAddItemUInt32("StudyNR", _mStudyNr);
                    ik.mbAddItemString("PrimairyName", _mFileName);

                    ik.mbAddItemDateTime("PrimairyUTC", _mFileStartUTC);
                    ik.mbAddItemUInt32("EventSample", _mEventSampleNr);
                    ik.mbAddItemInt16("TimeZoneOffsetMin", _mTimeZoneOffsetMin);
                    ik.mbAddItemString("EventType", _mEventType);

                    ik.mbAddItemDateTime("EventUTC", _mEventUTC);
                    ik.mbAddItemString("EventAllFile", _mEventAllFile);

                    ik.mbAddItemFloat("SamplesPerSec", _mSamplesPerSec);
                    ik.mbAddItemFloat("AdjustSpsFactor", _mAdjustSpsFactor);


                    ik.mbAddItemInt32("AmplGainPermV", _mAmplGainPermV);
                    ik.mbAddItemUInt16("HarwareScale", _mHardwareScale);
                    ik.mbAddItemUInt32("HardwareClockHz", _mHardwareClockHz);
                    ik.mbAddItemUInt16("NrChannelsInFile", _mNrChannelsInFile);
                    ik.mbAddItemBoolText("LeadOffInFile", _mbLeadOffInFile);
                    ik.mbAddItemBoolText("LeadOffSkip", _mbLeadOffSkip);
                    ik.mbAddItemBoolText("RespSkipFirstChannel", _mbRespSkipFirstChannel);

                    ik.mbAddItemUInt16("LoadChannelsMask", _mLoadChannelsMask);

                    ik.mbAddItemUInt16("NrBytesPerSample", _mNrBytesPerSample);
                    ik.mbAddItemBoolText("SignExtend", _mbSignExtend);

                    ik.mbAddItemBoolText("RevertBytes", _mbRevertBytesMSB);
                    ik.mbAddItemBoolText("FillGaps", _mbFillGaps);
                    ik.mbAddItemBoolText("ExactStrip", _mbExactStrip);
                    ik.mbAddItemUInt32("PreEventSec", _mPreEventSec);
                    ik.mbAddItemUInt32("PostEventSec", _mPostEventSec);
                    ik.mbAddItemString("FilesUsed", useFileSet);
                    ik.mbAddItemBoolText("LoadFromStudy", _mbLoadFromStudy);
                    ik.mbAddItemString("ValuesOrigin", _mValuesOrigin);
                    ik.mbAddItemBoolText("ManualImport", _mbManualImport);

                    ik.mbAddItemUInt32("RecordIX", _mRecordIX);
                    ik.mbAddItemString("SourceFilePath", _mSourceFilePath);

                    // Pleth
                    ik.mbAddItemBoolText("PlethLoad", _mbPlethLoad);
                    ik.mbAddItemString("PlethFilesUsed", plethFileSet);
                    ik.mbAddItemFloat("PlethSamplesPerSec", _mPlethSamplesPerSec);
                    ik.mbAddItemFloat("PlethMergeOffsetSec", _mPlethMergeOffsetSec);


                    ik.mbAddItemUInt32("PlethPreEcgSec", _mPlethPreEcgSec);
                    ik.mbAddItemUInt32("PlethPostEcgSec", _mPlethPostEcgSec);

                    if (_mPlethConvert != null)
                    {
                        ik.mbAddItemInt32("PlethLowFrom", _mPlethConvert._mLowFrom);
                        ik.mbAddItemDouble("PlethLowTo", _mPlethConvert._mLowTo);
                        ik.mbAddItemInt32("PlethHighFrom", _mPlethConvert._mHighFrom);
                        ik.mbAddItemDouble("PlethHighTo", _mPlethConvert._mHighTo);
                    }

                    bOk = ik.mbWriteToFile(AFilePath, _cFileSeed, CProgram.sGetOrganisationLabel(), true, 0);
                }
            }
            catch (Exception ex)
            {
                bOk = false;
                CProgram.sLogException("Failed save " + AFilePath, ex);
            }
            return bOk;

        }

        private bool mbLoadFile(out bool ArbCheckSumOk, string AFilePath, bool AbCheckStrict)
        {
            bool bOk = false;
            bool bChecksum = false;

            try
            {
                CItemKey ik = new CItemKey("DvxEvtRec");

                if (ik != null)
                {
                    string useFileSet = "";
                    string plethFileSet = "";

                    bOk = ik.mbReadFromFile(out bChecksum, AFilePath, _cFileSeed, CProgram.sGetOrganisationLabel(), AbCheckStrict);

                    if (bOk)
                    {

                        bOk &= ik.mbGetItemUInt32(ref _mDeviceSnr, "DeviceSNR");
                        bOk &= ik.mbGetItemUInt32(ref _mStudyNr, "StudyNR");
                        bOk &= ik.mbGetItemString(ref _mFileName, "PrimairyName");
                        bOk &= ik.mbGetItemDateTime(ref _mFileStartUTC, "PrimairyUTC");
                        bOk &= ik.mbGetItemUInt32(ref _mEventSampleNr, "EventSample");
                        bOk &= ik.mbGetItemString(ref _mEventType, "EventType");
                        bOk &= ik.mbGetItemInt16(ref _mTimeZoneOffsetMin, "TimeZoneOffsetMin");
                        bOk &= ik.mbGetItemDateTime(ref _mEventUTC, "EventUTC");
                        /*new item*/
                        ik.mbGetItemString(ref _mEventAllFile, "EventAllFile");

                        bOk &= ik.mbGetItemFloat(ref _mSamplesPerSec, "SamplesPerSec");
                        bOk &= ik.mbGetItemFloat(ref _mAdjustSpsFactor, "AdjustSpsFactor");
                        bOk &= ik.mbGetItemInt32(ref _mAmplGainPermV, "AmplGainPermV");
                        bOk &= ik.mbGetItemUInt16(ref _mHardwareScale, "HarwareScale");
                        bOk &= ik.mbGetItemUInt32(ref _mHardwareClockHz, "HardwareClockHz");
                        bOk &= ik.mbGetItemUInt16(ref _mNrChannelsInFile, "NrChannelsInFile");
                        bOk &= ik.mbGetItemBool(ref _mbLeadOffInFile, "LeadOffInFile");
                        bOk &= ik.mbGetItemBool(ref _mbLeadOffSkip, "LeadOffSkip");
                        bOk &= ik.mbGetItemBool(ref _mbRespSkipFirstChannel, "RespSkipFirstChannel");

                        bOk &= ik.mbGetItemUInt16(ref _mLoadChannelsMask, "LoadChannelsMask");

                        bOk &= ik.mbGetItemUInt16(ref _mNrBytesPerSample, "NrBytesPerSample");
                        bOk &= ik.mbGetItemBool(ref _mbSignExtend, "SignExtend");

                        bOk &= ik.mbGetItemBool(ref _mbRevertBytesMSB, "RevertBytes");
                        bOk &= ik.mbGetItemBool(ref _mbFillGaps, "FillGaps");
                        bOk &= ik.mbGetItemBool(ref _mbExactStrip, "ExactStrip");
                        bOk &= ik.mbGetItemUInt32(ref _mPreEventSec, "PreEventSec");
                        bOk &= ik.mbGetItemUInt32(ref _mPostEventSec, "PostEventSec");
                        bOk &= ik.mbGetItemString(ref useFileSet, "FilesUsed");
                        bOk &= ik.mbGetItemBool(ref _mbLoadFromStudy, "LoadFromStudy");
                        bOk &= ik.mbGetItemString(ref _mValuesOrigin, "ValuesOrigin");
                        bOk &= ik.mbGetItemBool(ref _mbManualImport, "ManualImport");

                        bOk &= ik.mbGetItemUInt32(ref _mRecordIX, "RecordIX");
                        bOk &= ik.mbGetItemString(ref _mSourceFilePath, "SourceFilePath");


                        // Pleth
                        ik.mbGetItemBool(ref _mbPlethLoad, "PlethLoad");
                        ik.mbGetItemString(ref plethFileSet, "PlethFilesUsed");
                        ik.mbGetItemFloat(ref _mPlethSamplesPerSec, "PlethSamplesPerSec");
                        ik.mbGetItemFloat(ref _mPlethMergeOffsetSec, "PlethMergeOffsetSec");
                        ik.mbGetItemUInt32(ref _mPlethPreEcgSec, "PlethPreEcgSec");
                        ik.mbGetItemUInt32(ref _mPlethPostEcgSec, "PlethPostEcgSec");

                        if (_mPlethConvert != null)
                        {
                            ik.mbGetItemInt32(ref _mPlethConvert._mLowFrom, "PlethLowFrom");
                            ik.mbGetItemDouble(ref _mPlethConvert._mLowTo, "PlethLowTo");
                            ik.mbGetItemInt32(ref _mPlethConvert._mHighFrom, "PlethHighFrom");
                            ik.mbGetItemDouble(ref _mPlethConvert._mHighTo, "PlethHighTo");
                            _mPlethConvert.mbInit();
                        }
                    }
                    _mDatFiles = sSplitSortFileSet(useFileSet);
                    _mPlethFiles = sSplitSortFileSet(plethFileSet);

                }
            }
            catch (Exception ex)
            {
                bOk = false;
                CProgram.sLogException("Failed load " + AFilePath, ex);
            }
            _mbReadCheckSumOK = bChecksum;

            ArbCheckSumOk = bChecksum;
            return bOk;
        }

        public void mClearFileList()
        {
            if (_mDatFiles != null)
            {
                _mDatFiles.Clear();
            }
        }

        public UInt32 mGetNrDatFiles()
        {
            return (UInt32)(_mDatFiles == null ? 0 : _mDatFiles.Count);
        }
        public UInt32 mGetNrPlethFiles()
        {
            return (UInt32)(_mPlethFiles == null ? 0 : _mPlethFiles.Count);
        }

        public static bool sbAddFileToList(ref List<string> ArFileList, string AFileName)
        {
            bool bAdded = false;

            if (AFileName != null && AFileName.Length > 0)
            {
                string fileName = Path.GetFileNameWithoutExtension(AFileName);

                if (fileName != null && fileName.Length > 0)
                {
                    if (ArFileList == null)
                    {
                        ArFileList = new List<string>();
                    }
                    if (ArFileList != null)
                    {
                        int n = ArFileList.Count;

                        if (n == 0)
                        {
                            ArFileList.Add(fileName);
                            bAdded = true;
                        }
                        else
                        {
                            for (int iInsert = 0; iInsert <= n; ++iInsert)
                            {
                                if (iInsert == n)
                                {
                                    ArFileList.Add(fileName); // last
                                    bAdded = true;
                                    break;
                                }
                                else
                                {
                                    int cmp = fileName.CompareTo(ArFileList[iInsert]);

                                    if (cmp < 0)
                                    {
                                        ArFileList.Insert(iInsert, fileName); // insert before
                                        bAdded = true;
                                        break;
                                    }
                                    else if (cmp == 0)
                                    {
                                        // same
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return bAdded;
        }

        public bool mbAddFileToList(string AFileName)
        {
            return sbAddFileToList(ref _mDatFiles, AFileName);
        }
        public bool mbAddFileToPlethList(string AFileName)
        {
            return sbAddFileToList(ref _mPlethFiles, AFileName);
        }

        public static List<string> sSplitSortFileSet(string AUseFileSet)
        {
            List<string> fileList = null;

            if (AUseFileSet != null && AUseFileSet.Length > 0)
            {
                fileList = new List<string>();

                if (fileList != null)
                {
                    string[] parts = AUseFileSet.Split(_cFileSetChar);

                    if (parts != null)
                    {
                        int n = parts.Length;
                        for (int i = 0; i < n; ++i)
                        {
                            if (parts[i] != null && parts[i].Length > 0)
                            {
                                sbAddFileToList(ref fileList, parts[i]);
                            }
                        }
                    }
                }

            }
            return fileList;
        }

        public static string sCombineUsedFiles(string[] AFileArray, UInt16 AMaxListed)
        {
            string useSet = "";

            if (AFileArray != null)
            {
                int nList = AFileArray.Length;
                bool bAll = nList <= AMaxListed;
                int n = 0;
                foreach (string filePath in AFileArray)
                {
                    string fileName = Path.GetFileNameWithoutExtension(filePath);
                    if (fileName.Length > 0)
                    {
                        if (bAll || n < AMaxListed)
                        {
                            if (++n > 1)
                            {
                                useSet += _cFileSetChar;
                            }
                            useSet += fileName;
                        }
                        else if (n == AMaxListed)
                        {
                            if (++n > 1)
                            {
                                useSet += _cFileSetChar;
                            }
                            useSet += "..." + _cFileSetChar + fileName;

                        }
                    }
                }
            }
            return useSet;
        }
        public static string sCombineUsedFiles(List<string> AFileList, UInt16 AMaxListed)
        {
            string useSet = "";

            if (AFileList != null)
            {
                int nList = AFileList.Count;
                bool bAll = nList <= AMaxListed;
                int n = 0;
                foreach (string filePath in AFileList)
                {
                    string fileName = Path.GetFileNameWithoutExtension(filePath);
                    if (fileName.Length > 0)
                    {
                        if (bAll || n < AMaxListed)
                        {
                            if (++n > 1)
                            {
                                useSet += _cFileSetChar;
                            }
                            useSet += fileName;
                        }
                        else if (n == AMaxListed)
                        {
                            if (++n > 1)
                            {
                                useSet += _cFileSetChar;
                            }
                            useSet += "..." + _cFileSetChar + fileName;

                        }
                    }
                }
            }
            return useSet;
        }

        public static bool sbInUsedFilesNameOnly(List<string> AFileList, string AFileName)
        {
            bool bFound = false;

            if (AFileList != null)
            {
                string searchName = Path.GetFileNameWithoutExtension(AFileName);    // any name.* e.g. .dat, .evt. trn

                foreach (string filePath in AFileList)
                {
                    string fileName = Path.GetFileNameWithoutExtension(filePath);

                    if (fileName.Length > 0)
                    {
                        if (fileName == searchName)
                        {
                            bFound = true;
                            break;
                        }
                    }
                }
            }
            return bFound;
        }
        public static bool sbInUsedFilesNameExt(List<string> AFileList, string AFileExtension, string AFileName)
        {
            bool bFound = false;

            if (AFileList != null)
            {
                string searchName = Path.GetFileName(AFileName);    // only same file.ext e.g. *.spo

                foreach (string filePath in AFileList)
                {
                    string fileName = Path.GetFileName(filePath);

                    if (fileName.Length > 0)
                    {
                        if (false == fileName.EndsWith(AFileExtension))
                        {
                            fileName += AFileExtension;
                        }
                        if (fileName == searchName)
                        {
                            bFound = true;
                            break;
                        }
                    }
                }
            }
            return bFound;
        }

        public bool mbInUsedFiles(string AFileName)
        {
            return sbInUsedFilesNameOnly(_mDatFiles, AFileName);
        }

        public bool mbInUsedPlethFiles(string AFileName)
        {
            return sbInUsedFilesNameExt(_mPlethFiles, _cPlethExtension, AFileName);
        }

        private bool mbSplitConfigLine(out UInt16 ArValue, out string ArParam, string ALine)
        {
            UInt16 value = 0xFFFF;
            string param = "?";

            bool bOk = ALine != null && ALine.Length > 1 && ALine.StartsWith("0x");

            if (bOk)
            {
                int pos = ALine.IndexOf(':');

                bOk = pos > 4;

                if (bOk)
                {
                    string hex = ALine.Substring(2, 2);
                    param = ALine.Substring(pos + 1).Trim().ToUpper();

                    bOk = param.Length > 0
                        && UInt16.TryParse(hex, NumberStyles.HexNumber, CultureInfo.CurrentCulture, out value);
                }
            }
            ArValue = value;
            ArParam = param;
            return bOk;

        }
        private int mExtractGain(UInt16 AValue)
        {
            int gain = 0;   // 7=pwr dwn, 6:4=Gain, 2:0 ch imput

            if ((AValue & 0x0080) == 0)
            {
                int pgaGain = (AValue & (0x70) >> 4);

                switch (pgaGain)
                {
                    case 0: gain = 6; break;
                    case 1: gain = 1; break;
                    case 2: gain = 2; break;
                    case 3: gain = 3; break;
                    case 4: gain = 4; break;
                    case 5: gain = 8; break;
                    case 6: gain = 12; break;
                    case 7: gain = 0; break;
                }
            }
            return gain;
        }

        private bool mbLoadEcgSettingsFile(string ASettingsFilePath, string ACopyToPath, bool AbLogOK)
        {
            bool bOk = false;

            try
            {
                if (File.Exists(ASettingsFilePath))
                {
                    /* Config \ ads1298r.config)
                     * ADS1298RIZXG.pdf section 9.6 Register maps
    0x85 :CONFIG1       B8=High resolution, B2:0 =Data Rate
    0x10 :CONFIG2
    0xCC :CONFIG3
    0xF2 :LOFF
    0x00 :CH1SET        // 7=pwr dwn, 6:4=Gain, 2:0 ch imput
    0x00 :CH2SET
    0x00 :CH3SET
    0x11 :CH4SET
    0x11 :CH5SET
    0x11 :CH6SET
    0x11 :CH7SET
    0x11 :CH8SET
    0x02 :RLD_SENSP
    0x02 :RLD_SENSN
    0x00 :LOFF_SENSP
    0x00 :LOFF_SENSN
    0x00 :LOFF_FLIP
    0x0F :GPIO
    0x00 :PACE
    0x20 :RESP
    0x22 :CONFIG4
    0x03 :WCT1
    0x14 :WCT2
    0x03 :nr_of_ch
    0x00 :1705                  
                     */
                    string[] lines = File.ReadAllLines(ASettingsFilePath);

                    int n = lines == null ? 0 : lines.Length;
                    string param;
                    UInt16 value;
                    int[] gain = new int[16];
                    int nrChannels = 0;
                    bool b1705 = false;
                    UInt16 mask = 0;
                    UInt16 iMask = 0;

                    for (int i = 0; i < 16; ++i) gain[i] = 0;

                    for (int i = 0; i < n; ++i)
                    {
                        if (mbSplitConfigLine(out value, out param, lines[i]))
                        {
                            if (param == "CONFIG1")
                            {
                                bool bHR = (value & 0x0080) != 0;
                                UInt16 iDR = (UInt16)(value & 0x07);
                                int frDev = 1 << iDR;
                                uint fr = (bHR ? _mHardwareClockHz : _mHardwareClockHz / 2);
                                fr /= (uint)frDev;

                                _mSamplesPerSec = fr;
                            }
                            else if (param == "CH1SET") { gain[0] = mExtractGain(value); }
                            else if (param == "CH2SET") { gain[1] = mExtractGain(value); }
                            else if (param == "CH3SET") { gain[2] = mExtractGain(value); }
                            else if (param == "CH4SET") { gain[3] = mExtractGain(value); }
                            else if (param == "CH5SET") { gain[4] = mExtractGain(value); }
                            else if (param == "CH6SET") { gain[5] = mExtractGain(value); }
                            else if (param == "CH7SET") { gain[6] = mExtractGain(value); }
                            else if (param == "CH8SET") { gain[7] = mExtractGain(value); }
                            else if (param == "RESP")
                            {

                            }
                            else if (param == "NR_OF_CH")
                            {
                                nrChannels = value;
                            }
                            else if (param == "1705")
                            {
                                b1705 = value != 0;
                            }
                        }
                    }
                    int usedGain = 0;
                    int iChannel = 0;

                    _mNrChannelsInFile = (UInt16)nrChannels;
                    if (_mbLeadOffInFile)
                    {
                        ++_mNrChannelsInFile;
                        if (false == _mbLeadOffSkip)
                        {
                            mask = 1;   // leadoff in file and loaded
                        }
                        ++iMask;
                    }
                    if (_mbRespSkipFirstChannel && false == b1705)
                    {
                        ++iMask;    // skip resporatory channel (CH1)
                        ++iChannel;
                    }
                    for (; iChannel < nrChannels && iChannel < 16; ++iChannel)
                    {
                        mask |= (UInt16)(1L << iMask);
                        ++iMask;
                        if (usedGain == 0 && gain[iChannel] > 0)
                        {
                            usedGain = gain[iChannel];      // use the first gain as the gain used for all the channels
                            bOk = true;
                        }
                    }
                    _mLoadChannelsMask = mask;
                    if (usedGain > 0 && _mHardwareScale > 0 && usedGain != _mHardwareScale)
                    {
                        _mAmplGainPermV = (Int32)((_mAmplGainPermV * usedGain) / _mHardwareScale);
                        _mHardwareScale = (UInt16)usedGain;
                    }

                    if (bOk)
                    {
                        if (mask == 0)
                        {
                            CProgram.sLogError("DVX load mask = 0 in " + ASettingsFilePath);
                            bOk = false;
                        }
                        if (iMask != _mNrChannelsInFile)
                        {
                            CProgram.sLogError("DVX load mask = 0x" + mask.ToString("X")
                                + " " + iMask.ToString() + " not equal to file " + _mNrChannelsInFile.ToString() + "  in " + ASettingsFilePath);
                            bOk = false;
                        }
                        if (AbLogOK)
                        {
                            CProgram.sLogLine("DVX config file loaded " + ASettingsFilePath);
                        }
                    }
                    if (n > 0 && ACopyToPath != null && ACopyToPath.Length > 0)
                    {
                        string copyTo = Path.Combine(ACopyToPath, Path.GetFileName(ASettingsFilePath));
                        File.Copy(ASettingsFilePath, copyTo);
                    }
                }
            }
            catch (Exception ex)
            {
                bOk = false;
                CProgram.sLogException("Failed load DVX config " + ASettingsFilePath, ex);
            }
            return bOk;
        }
        public bool mbCheckLoadEcgConfig(out string ArEcgConfigFilePath, string AFilePath, bool AbLogOK)
        {
            bool bOk = false;
            bool bLoad = false;
            string folder = Path.GetDirectoryName(AFilePath);
            string configFile = Path.Combine(folder, _cEcgConfigFolder, _cEcgConfigName);

            if (File.Exists(configFile))
            {
                bLoad = true;
            }
            else
            {
                configFile = Path.Combine(folder, _cEcgConfigName);
                bLoad = File.Exists(configFile);
            }
            if (bLoad)
            {
                bOk = mbLoadEcgSettingsFile(configFile, null, AbLogOK);
                if (bOk)
                {
                    _mValuesOrigin += "-ecgConfig";
                }
            }
            ArEcgConfigFilePath = bOk ? configFile : "";
            return bOk;
        }

        private bool mbSplitEventLine(out string ArType, out UInt32 ArSample, string ALine)
        {
            UInt32 sample = 0;
            string eventType = "?";

            bool bOk = false;

            if (ALine != null && ALine.Length > 1)
            {
                int pos = ALine.IndexOf(':');
                if (pos > 1)
                {
                    eventType = ALine.Substring(0, pos).Trim();
                    string index = "";

                    while (++pos < ALine.Length)
                    {
                        char c = ALine[pos];

                        if (Char.IsDigit(c))
                        {
                            index += c;
                        }
                        else if (c == '.')
                        {
                            break;
                        }
                    }
                    bOk = eventType.Length > 0
                        && UInt32.TryParse(index, out sample);
                }
            }
            ArSample = sample;
            ArType = eventType;
            return bOk;

        }
        public string mGetChecksumChar()
        {
            return _mbReadCheckSumOK ? "~" : "";
        }

        private bool mbParseEventFile(out UInt16 ArEventLevel, out UInt32 ArNrEvents, string AEventFilePath, bool AbReport, bool AbLogLine) // *.evt
        {
            bool bOk = false;
            UInt16 eventLevel = (UInt16)DDvxEvent_Priority.Disregard;
            UInt32 nrEvents = 0;

            try
            {
                /* <snr>_yyyyMMddHHmmss.dat.evt)
                 * 
alarm 1: 5845
   alarm 1: 26070.
alarm 1: 4490                 
double_tap: 4979
                 */
                _mEventType = "";
                _mEventSampleNr = 0;
                _mEventUTC = _mFileStartUTC;

                _mEventAllFile = "";           // contains the evt file <evt type>(<sample nr>)+<evt type>(<sample nr>);

                if (File.Exists(AEventFilePath))
                {
                    string[] lines = File.ReadAllLines(AEventFilePath);
                    int n = lines == null ? 0 : lines.Length;
                    string eventType;
                    UInt32 sampleNr;
                    UInt32 lastSampleNr = UInt32.MaxValue;
                    string lastEventType = "";
                    List<CDvxEventLevel> eventList = null;

                    for (int i = 0; i < n; ++i)
                    {
                        if (mbSplitEventLine(out eventType, out sampleNr, lines[i]))
                        {
                            UInt16 level = (UInt16)DDvxEvent_Priority.Disregard;
                            string cmpStr = eventType.ToLower().Trim();

                            if (cmpStr == "double_tap")
                            {
                                level = (UInt16)DDvxEvent_Priority.Manual;
                                eventType = "Manual";
                            }
                            else if (cmpStr == "afib")
                            {
                                level = (UInt16)DDvxEvent_Priority.AFib;
                            }
                            else if (cmpStr == "afib_start")
                            {
                                level = (UInt16)DDvxEvent_Priority.AFib;
                                //                                eventType = "AFib";
                            }
                            else if (cmpStr == "afib_stop")
                            {
                                level = (UInt16)DDvxEvent_Priority.AFib;
                                //                                eventType = "AFib";
                            }
                            else if (cmpStr == "tachy")
                            {
                                level = (UInt16)DDvxEvent_Priority.Tachy;
                            }
                            else if (cmpStr == "brady")
                            {
                                level = (UInt16)DDvxEvent_Priority.Brady;
                            }
                            else if (cmpStr == "pause")
                            {
                                level = (UInt16)DDvxEvent_Priority.Pause;
                            }
                            else if (cmpStr == "manual")
                            {
                                level = (UInt16)DDvxEvent_Priority.Manual;
                            }
                            else if (cmpStr.Length > 0)
                            {
                                string alarmLabel;
                                UInt16 alarmLevel;

                                if (sbGetDvxAlarmType(cmpStr, out alarmLabel, out alarmLevel))
                                {
                                    eventType = alarmLabel;

                                    level = alarmLevel;
                                }
                                else
                                {
                                    level = (UInt16)DDvxEvent_Priority.Other;
                                }
                            }
                            CProgram.sAddText(ref _mEventAllFile, eventType + "(" + sampleNr + ")", "+");

                            if (level != (UInt16)DDvxEvent_Priority.Disregard)
                            {
                                if (sampleNr - lastSampleNr <= 2)
                                {
                                    string lowerLast = lastEventType.ToLower();
                                    string lower = eventType.ToLower();
                                    if (lowerLast == lower)
                                    {
                                        level = (UInt16)DDvxEvent_Priority.Disregard;   // disable double entries like Manual
                                    }
                                    else if (lowerLast.Contains("manual") && lower.Contains("manual"))
                                    {
                                        level = (UInt16)DDvxEvent_Priority.Disregard;   // disable double Manual from double tap and alarm
                                    }
                                }
                                if (level != (UInt16)DDvxEvent_Priority.Disregard)
                                {
                                    if (CDvxEventLevel.sbAddEvent(ref eventList, eventType, level))
                                    {
                                        ++nrEvents;
                                    }
                                    if (level < eventLevel || level == eventLevel && sampleNr < _mEventSampleNr)    // higher priority (<) and first occurence 
                                    {
                                        eventLevel = level;
                                        _mEventType = eventType;
                                        _mEventSampleNr = sampleNr;
                                        _mEventUTC = _mFileStartUTC.AddSeconds(_mSamplesPerSec > 0 ? sampleNr / (double)_mSamplesPerSec : 0);
                                    }
                                    lastSampleNr = sampleNr;
                                    lastEventType = eventType;
                                }
                            }
                        }
                    }
                    bOk = true;

                    if (eventLevel < (UInt16)DDvxEvent_Priority.Disregard) // event is present 
                    {
                        _mEventType = CDvxEventLevel.sGetEventsLabel(ref eventList, 9999, true);   // get all event labels
                        if (nrEvents > 1 || AbReport)
                        {
                            if (AbReport)
                            {
                                _mEventType = nrEvents.ToString() + "\t" + _mEventType;
                            }
                            else
                            {
                                _mEventType = _mEventType + _cMergeNrEvents + nrEvents.ToString();
                            }
                        }
                        if (AbLogLine)
                        {
                            CProgram.sLogLine("DVX event file loaded " + AEventFilePath
                            + " event " + _mEventType + "@" + CProgram.sDateTimeToYMDHMS(_mEventUTC));
                        }
                    }
                    else
                    {
                        if (AbLogLine)
                        {
                            CProgram.sLogLine("DVX event file disregarded " + AEventFilePath
                                + " event " + _mEventType + "@" + CProgram.sDateTimeToYMDHMS(_mEventUTC));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                bOk = false;
                CProgram.sLogException("Failed load DVX event " + AEventFilePath, ex);
            }
            ArEventLevel = eventLevel;
            ArNrEvents = nrEvents;
            return bOk;
        }

        public static string sRemoveNrEvents(string AEventType)
        {
            string s = AEventType;
            int pos = s == null || s.Length == 0 ? -1 : s.IndexOf(_cMergeNrEvents);

            if (pos >= 0)
            {
                s = s.Substring(0, pos);
            }
            return s;
        }
        public static bool sbDifferentOrManual(string ANewEventType, string APrevEventType)
        {
            bool bDifferent = false;

            if (ANewEventType != null && ANewEventType.Length > 0)
            {
                string s = ANewEventType.ToLower();

                if (s.Contains("manual"))
                {
                    bDifferent = true;
                }
                else if (APrevEventType == null || APrevEventType.Length == 0)
                {
                    bDifferent = true;

                }
                else
                {
                    string s2 = APrevEventType.ToLower();

                    bDifferent = s != s2;
                }
            }

            return bDifferent;
        }

        public bool mbCheckLoadEventFile(out UInt16 ArEventLevel, out UInt32 ArNrEvents, string AFilePath, bool AbReport, bool AbLogLine)
        {
            bool bOk = false;
            string eventFile = Path.ChangeExtension(AFilePath, CDvxEvtRec._cEventExtension);

            _mEventSampleNr = 0;
            _mEventUTC = _mFileStartUTC;
            if (mbParseEventFile(out ArEventLevel, out ArNrEvents, eventFile, AbReport, AbLogLine))
            {
                bOk = true;
            }

            return bOk;
        }

        public static CDvxEvtRec sLoadDvxEvtRecFile(out bool ArbCheckSumOk, string AFilePath, bool AbCheckStrict)
        {
            CDvxEvtRec dvxEvtRec = null;
            bool bChecksum = false;

            try
            {
                if (File.Exists(AFilePath))
                {
                    string ext = Path.GetExtension(AFilePath);

                    if (ext == _cEvtRecExtension)
                    {
                        CDvxEvtRec dvx = new CDvxEvtRec();

                        if (dvx != null)
                        {
                            if (dvx.mbLoadFile(out bChecksum, AFilePath, AbCheckStrict))
                            {
                                dvxEvtRec = dvx;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed load evtRec " + AFilePath, ex);
            }
            ArbCheckSumOk = bChecksum;
            return dvxEvtRec;

        }
        public static CDvxEvtRec sLoadDeviceFile(out bool ArbCheckSumOk, UInt32 ADeviceSNR, bool AbCheckStrict)
        {
            CDvxEvtRec dvxEvtRec = null;
            string deviceSNR = sGetDeviceName(ADeviceSNR);
            bool bChecksum = false;

            try
            {
                string devicePath;

                if (CDvtmsData.sbGetDeviceUnitDir(out devicePath, DDeviceType.DVX, deviceSNR, true))
                {
                    string filePath = Path.Combine(devicePath, deviceSNR + _cEvtRecExtension);

                    dvxEvtRec = sLoadDvxEvtRecFile(out bChecksum, filePath, AbCheckStrict);

                    if (dvxEvtRec != null)
                    {
                        dvxEvtRec._mValuesOrigin = "Device";
                    }
                }
                if (dvxEvtRec == null)
                {
                    if (CDvtmsData.sbGetDeviceTypeDir(out devicePath, DDeviceType.DVX))
                    {
                        string filePath = Path.Combine(devicePath, "defaults" + _cEvtRecExtension);

                        dvxEvtRec = sLoadDvxEvtRecFile(out bChecksum, filePath, AbCheckStrict);
                        if (dvxEvtRec != null)
                        {
                            dvxEvtRec._mValuesOrigin = "Defaults";
                        }
                    }
                }
                if (dvxEvtRec == null)
                {
                    dvxEvtRec = new CDvxEvtRec();   // created
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed load evtRec for DVX SNR " + deviceSNR, ex);
            }
            ArbCheckSumOk = bChecksum;
            return dvxEvtRec;
        }
        public static CDvxEvtRec sCheckDefaults()
        {
            CDvxEvtRec dvxEvtRec = null;

            try
            {
                string devicePath;

                if (CDvtmsData.sbGetDeviceTypeDir(out devicePath, DDeviceType.DVX))
                {
                    string filePath = Path.Combine(devicePath, "defaults" + _cEvtRecExtension);

                    if (false == File.Exists(filePath))
                    {
                        dvxEvtRec = new CDvxEvtRec();   // created

                        if (dvxEvtRec != null)
                        {
                            if (dvxEvtRec.mbSaveFile(filePath))
                            {
                                CProgram.sLogLine("DVX defaults created");
                            }
                            else
                            {
                                CProgram.sLogError("Failed create DVX defaults");
                            }
                        }
                    }
                    else
                    {
                        bool bChecksum = false;
                        bool bSave = false;

                        dvxEvtRec = sLoadDvxEvtRecFile(out bChecksum, filePath, false);

                        if (dvxEvtRec == null)
                        {
                            CProgram.sLogError("Failed read DVX defaults");
                            bSave = CProgram.sbAskYesNo("DVX Defaults corrupted", "Save defaults");
                            dvxEvtRec = new CDvxEvtRec();   // created
                        }
                        else if (false == bChecksum)
                        {
                            CProgram.sLogError("DVX defaults present but changed");
                            bSave = CProgram.sbAskYesNo("DVX Defaults changed", "Save defaults");
                        }
                        if (bSave && dvxEvtRec != null)
                        {
                            dvxEvtRec.mClearRecording(0);
                            if (dvxEvtRec.mbSaveFile(filePath))
                            {
                                CProgram.sLogLine("DVX defaults saved");
                            }
                            else
                            {
                                CProgram.sLogError("Failed save DVX defaults");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed check DVX defaults", ex);
            }
            return dvxEvtRec;
        }

        public bool mbScanAllFiles(ref string[] ArAllFilesInDir, string AFilePath)
        {
            if (ArAllFilesInDir == null)
            {
                string folder = Path.GetDirectoryName(AFilePath);

                try
                {
                    string[] allFiles = Directory.GetFiles(folder);

                    if (allFiles != null && allFiles.Length > 0)
                    {
                        ArAllFilesInDir = allFiles;
                    }

                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Failed scan folder " + folder, ex);
                }
            }
            return ArAllFilesInDir != null;
        }

        public bool mbDeterminFileRange(ref string[] ArAllFilesInDir, /*out string ArSourcePath, */string AFilePath,
            out DateTime ArLastUTC, out bool ArbFoundPrimairy)//
        {
            bool bOk = false;
            DateTime lastUTC = DateTime.MinValue;
            UInt32 nFiles = 0;
            DateTime fromStripUTC = _mFileStartUTC.AddSeconds(-_mPreEventSec - 0.2);  // keep it simple just file blocks 
            DateTime endStripUTC = _mFileStartUTC.AddSeconds(_mPostEventSec);
            bool bFoundPrimairy = false;
            string primairyName = Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(_mFileName));

            UInt32 nPlethFiles = 0;
            DateTime fromPlethUTC = fromStripUTC.AddSeconds(-_mPlethPreEcgSec);  // keep it simple just file blocks 
            DateTime endPlethUTC = endStripUTC.AddSeconds(_mPlethPostEcgSec);


            DateTime startUTC = DateTime.UtcNow;
            double tTotal = 0;
            double tList = 0;
            double tScan = 0;
            Int32 nList = 0;
            Int32 nPlethList = 0;

            // test files against pre post
            try
            {
                mClearFileList();

                if (mbScanAllFiles(ref ArAllFilesInDir, AFilePath))
                {
                    nList = ArAllFilesInDir == null ? 0 : ArAllFilesInDir.Length;
                    tTotal = (DateTime.UtcNow - startUTC).TotalSeconds;
                    tList = tTotal;

                    foreach (string fileWithPath in ArAllFilesInDir)
                    {
                        UInt32 deviceSNR;
                        DateTime utc;
                        string extension;

                        if (sbParseFileName(fileWithPath, out deviceSNR, out utc, out extension))
                        {
                            if (extension == _cEcgExtension) // dat ecg file
                            {
                                if (utc > lastUTC)
                                {
                                    lastUTC = utc;
                                }
                                if (utc >= fromStripUTC && utc < endStripUTC)
                                {
                                    mbAddFileToList(fileWithPath);
                                    ++nFiles;

                                    string fileName = Path.GetFileNameWithoutExtension(fileWithPath);

                                    if (fileName == primairyName)
                                    {
                                        bFoundPrimairy = true;
                                    }
                                }
                            }
                            else if (_mbPlethLoad && extension == _cPlethExtension)
                            {
                                if (utc >= fromPlethUTC && utc < endPlethUTC)
                                {
                                    mbAddFileToPlethList(fileWithPath);
                                    ++nPlethFiles;
                                }
                            }

                        }
                    }
                    if (false == bFoundPrimairy)
                    {
                        mbAddFileToList(primairyName);     // adding primairy ECG file that should contain the event, file can come in later
                        CProgram.sLogLine("DVX strip file list missing primary:  " + primairyName);
                    }
                    bOk = nFiles > 0;
                    if (false == bOk)
                    {
                        CProgram.sLogError("No DVX ECG files in range " + CProgram.sDateTimeToYMDHMS(fromStripUTC)
                            + " ... " + CProgram.sDateTimeToYMDHMS(endStripUTC) + " @ " + AFilePath);
                    }
                }
                else
                {
                    CProgram.sLogError("No DVX files in folder range " + AFilePath);

                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed determin file range " + AFilePath, ex);
            }
            tTotal = (DateTime.UtcNow - startUTC).TotalSeconds;
            tScan = tTotal - tList;
            if (tTotal > 2)
            {
                string s = "DVX Range: " + primairyName + " t=" + tTotal.ToString("0.00") + " tList[" + nList + "]=" + tList.ToString("0.00")
                        + " tScan=" + tScan.ToString("0.00") + "->" + nFiles;
                if (_mbPlethLoad)
                {
                    s += " Pleth " + nPlethFiles;
                }

                CProgram.sLogLine(s);
            }

            ArLastUTC = lastUTC;
            ArbFoundPrimairy = bFoundPrimairy;
            return bOk;
        }
        public bool mbDeterminPlethFileRange(ref string[] ArAllFilesInDir, string AFilePath,
            DateTime ArStartUTC, DateTime AEndUTC)//
        {
            bool bOk = false;
            DateTime lastUTC = DateTime.MinValue;

            DateTime fromStripUTC = ArStartUTC.AddSeconds(-_mPreEventSec - 0.2);  // keep it simple just file blocks 
            DateTime endStripUTC = AEndUTC.AddSeconds(_mPostEventSec);

            UInt32 nPlethFiles = 0;
            DateTime fromPlethUTC = fromStripUTC.AddSeconds(-_mPlethPreEcgSec);  // keep it simple just file blocks 
            DateTime endPlethUTC = endStripUTC.AddSeconds(_mPlethPostEcgSec);


            DateTime startUTC = DateTime.UtcNow;
            double tTotal = 0;
            double tList = 0;
            double tScan = 0;
            Int32 nList = 0;

            // test files against pre post
            try
            {
                if (mbScanAllFiles(ref ArAllFilesInDir, AFilePath))
                {
                    nList = ArAllFilesInDir == null ? 0 : ArAllFilesInDir.Length;
                    tTotal = (DateTime.UtcNow - startUTC).TotalSeconds;
                    tList = tTotal;

                    foreach (string fileWithPath in ArAllFilesInDir)
                    {
                        UInt32 deviceSNR;
                        DateTime utc;
                        string extension;

                        if (sbParseFileName(fileWithPath, out deviceSNR, out utc, out extension))
                        {
                            if (_mbPlethLoad && extension == _cPlethExtension)
                            {
                                if (utc >= fromPlethUTC && utc < endPlethUTC)
                                {
                                    mbAddFileToPlethList(fileWithPath);
                                    ++nPlethFiles;
                                }
                            }
                        }
                    }
                    bOk = nPlethFiles > 0;
                    if (false == bOk)
                    {
                        CProgram.sLogError("No DVX Pleth files in range " + CProgram.sDateTimeToYMDHMS(fromStripUTC)
                            + " ... " + CProgram.sDateTimeToYMDHMS(endStripUTC) + " @ " + AFilePath);
                    }
                }
                else
                {
                    CProgram.sLogError("No DVX files in folder range " + AFilePath);

                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed determin file range " + AFilePath, ex);
            }
            tTotal = (DateTime.UtcNow - startUTC).TotalSeconds;
            tScan = tTotal - tList;
            if (tTotal > 2)
            {
                string s = "DVX Pleth Range: " + AFilePath + " t=" + tTotal.ToString("0.00") + " tList[" + nList + "]=" + tList.ToString("0.00")
                        + " tScan=" + tScan.ToString("0.00") + "->" + nPlethFiles;
                CProgram.sLogLine(s);
            }
            return bOk;
        }

        public bool mbCopyFiles(ref string[] ArAllFilesInDir, string AFilePath, string AToPath)
        {
            bool bOk = false;
            string fromPath = Path.GetDirectoryName(AFilePath);
            string[] fromAllFiles = null;

            if (_mStudyNr > 0 && _mbLoadFromStudy)
            {
                bOk = mbGetStudyDeviceDataFolder(out fromPath);

                if (bOk)
                {
                    try
                    {
                        string[] allFiles = Directory.GetFiles(fromPath);

                        if (allFiles != null && allFiles.Length > 0)
                        {
                            fromAllFiles = allFiles;
                        }
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException("Failed scan study " + _mStudyNr.ToString() + " DVX " + sGetDeviceName(_mDeviceSnr), ex);
                        bOk = false;
                    }
                }
            }
            else
            {
                bOk = mbScanAllFiles(ref ArAllFilesInDir, AFilePath);

                fromAllFiles = ArAllFilesInDir;
            }
            if (bOk && fromAllFiles != null)
            {
                int n = fromAllFiles.Length;
                uint nFilesCopied = 0;
                uint nFailed = 0;
                uint nDatFiles = mGetNrDatFiles();
                uint nPlethFiles = mGetNrPlethFiles();

                if (n == 0)
                {
                    CProgram.sLogWarning("Source folder empty " + fromPath);
                    bOk = false;
                }
                else if (nDatFiles == 0)
                {
                    CProgram.sLogWarning("No dat files used" + AFilePath);
                    bOk = false;
                }
                else
                {
                    foreach (string fromFilePath in fromAllFiles)
                    {
                        if (mbInUsedFiles(fromFilePath) // copy all files with name.*
                            || mbInUsedPlethFiles(fromFilePath))    // and name.spo
                        {
                            string name = Path.GetFileName(fromFilePath);
                            string toFilePath = Path.Combine(AToPath, name);

                            try
                            {
                                if (name.Length > 0)
                                {
                                    File.Copy(fromFilePath, toFilePath, true);
                                    ++nFilesCopied;
                                }
                            }
                            catch (Exception ex)
                            {
                                CProgram.sLogException("Failed copy file " + name + " to " + toFilePath, ex);
                                ++nFailed;
                            }

                        }
                    }
                    bOk = /*nFailed == 0 && */nFilesCopied > 0;
                }

            }

            return bOk;
        }
        public bool mbCopyConfigFolder(string AFilePath, string AToPath)
        {
            bool bOk = false;

            string folder = Path.GetDirectoryName(AFilePath);
            string configFolder = Path.Combine(folder, _cEcgConfigFolder);
            string configFile = Path.Combine(folder, _cEcgConfigFolder, _cEcgConfigName);
            //string toFolder = Path.Combine(AToPath, _cEcgConfigFolder);

            try
            {
                if (Directory.Exists(configFolder))
                {
                    if (File.Exists(configFile))
                    {
                        string toFile = Path.Combine(AToPath, _cEcgConfigName);

                        File.Copy(configFile, toFile);
                        bOk = true;
                    }
                }
            }

            catch (Exception ex)
            {
                CProgram.sLogException("Failed copy file " + configFile + " to " + AToPath, ex);
            }

            return bOk;
        }

        public bool mbGetSourceFolder(out string ArSourceDir, string AFilePath)
        {
            bool bOk = false;
            string fromPath = Path.GetDirectoryName(AFilePath);

            if (_mStudyNr > 0 && _mbLoadFromStudy)
            {
                bOk = mbGetStudyDeviceDataFolder(out fromPath);

            }
            else
            {
                bOk = Directory.Exists(fromPath);
            }
            ArSourceDir = fromPath;

            return bOk;
        }


        public CRecordMit mLoadEcgData(out string ArResultText, string AFilePath, Int16 ADefaultTimeZoneMin, DMoveTimeZone AMoveTimeZone)
        {
            CRecordMit rec = null;
            string sourceDir = "";
            UInt32 nDatFiles = mGetNrDatFiles();
            string result = "";
            string addRemark = "";
            DateTime lastUTC;
            bool bFoundPrimairy = false;

            if (nDatFiles == 0)
            {
                string[] allFiles = null;
                string source = _mbLoadFromStudy ? _mSourceFilePath : AFilePath;

                if (mbDeterminFileRange(ref allFiles, source, out lastUTC, out bFoundPrimairy))
                {
                    double lastPostSec = (lastUTC - _mFileStartUTC).TotalSeconds;

                    string listFiles = sCombineUsedFiles(_mDatFiles, 65000);
                    addRemark = "range{" + (-_mPreEventSec).ToString()
                                    + "," + _mPostEventSec.ToString() + "}=" + mGetNrDatFiles().ToString() + "="
                                    + (bFoundPrimairy ? "" : "!") + listFiles;
                    if (lastPostSec < _mPostEventSec)
                    {
                        addRemark += "<";
                    }
                    nDatFiles = mGetNrDatFiles();
                }
                else
                {
                    addRemark = " in range";
                }
            }

            if (nDatFiles == 0)
            {
                result = "No files set" + addRemark;
            }
            else
            {
                if (mbGetSourceFolder(out sourceDir, AFilePath))
                {
                    uint nFilesRead = 0;
                    List<CDvxData> listDvxData = new List<CDvxData>();

                    foreach (string datFileName in _mDatFiles)
                    {
                        string datName = Path.ChangeExtension(datFileName, _cEcgExtension);
                        string datFilePath = Path.Combine(sourceDir, datName);

                        CDvxData dvxData = mLoadOneDvxData(datFilePath);

                        if (dvxData != null)
                        {
                            // add sorted by _mUTC
                            bool bAdd = true;
                            int pos = 0;

                            foreach (CDvxData node in listDvxData)
                            {
                                if (dvxData._mUtc < node._mUtc)
                                {
                                    listDvxData.Insert(pos, dvxData);
                                    ++nFilesRead;
                                    bAdd = false;
                                    break;
                                }
                                ++pos;
                            }
                            if (bAdd)
                            {
                                listDvxData.Add(dvxData);
                                ++nFilesRead;
                            }
                        }
                    }
                    if (nFilesRead == 0)
                    {
                        result = " No files read (" + nDatFiles.ToString() + ")";
                    }
                    else
                    {
                        // combine files into a record
                        rec = mMergeDvxData(out result, listDvxData);

                        if (nFilesRead != nDatFiles)
                        {
                            result += " (missing " + (nDatFiles - nFilesRead).ToString() + ")";
                        }
                        if (rec != null)
                        {
                            if (addRemark.Length > 0)
                            {
                                rec.mRecRemark.mAddLine(addRemark);
                            }

                            if (AMoveTimeZone == DMoveTimeZone.Move)
                            {
                                rec.mTimeZoneOffsetMin = ADefaultTimeZoneMin;
                                //                                rec.mbMoveTimeZone();
                            }

                            // load PlethFiles
                            if (_mbPlethLoad)
                            {
                                //                                mbPlethScanFiles(rec);
                                mbLoadMergePleth(rec, AFilePath);
                            }

                        }
                    }
                }
                else
                {
                    result = "No source folder";
                }
            }
            ArResultText = result;
            return rec;
        }
        public static bool sbLoadEcgData(out string ArResultText, out CDvxEvtRec ArDvxEvtRec, out CRecordMit ArRecord, string ADvxEvtRecFilePath,
            Int16 ADefaultTimeZoneMin, DMoveTimeZone AMoveTimeZone)
        {
            bool bOk = false;
            bool bChecksumOK;
            CRecordMit rec = null;
            CDvxEvtRec dvxEvtRec = sLoadDvxEvtRecFile(out bChecksumOK, ADvxEvtRecFilePath, false);

            if (dvxEvtRec == null)
            {
                ArResultText = "Failed load dvxEvtRec";
            }
            else
            {
                rec = dvxEvtRec.mLoadEcgData(out ArResultText, ADvxEvtRecFilePath, ADefaultTimeZoneMin, AMoveTimeZone);

                bOk = rec != null && rec.mNrSamples > 0;
            }
            ArDvxEvtRec = dvxEvtRec;
            ArRecord = rec;

            return bOk;
        }

        private CDvxData mLoadOneDvxData(string AdatFilePath)
        {
            CDvxData dvxData = null;

            try
            {
                DateTime utc;
                UInt32 deviceSNR;
                string fileExt;

                if (false == CDvxEvtRec.sbParseFileName(AdatFilePath, out deviceSNR, out utc, out fileExt))
                {
                    CProgram.sLogLine("Bad DVX ECG file name " + AdatFilePath);
                }
                else if (_mSamplesPerSec <= 0.1F)
                {
                    CProgram.sLogLine("Samples per sec = 0 " + AdatFilePath);
                }
                else if (_mNrChannelsInFile <= 0)
                {
                    CProgram.sLogLine("_mNrChannelsInFile = 0 " + AdatFilePath);
                }
                else if (_mNrBytesPerSample <= 0)
                {
                    CProgram.sLogLine("_mNrBytesPerSample = 0 " + AdatFilePath);
                }
                else if (_mLoadChannelsMask == 0)
                {
                    CProgram.sLogLine("LoadChannelsMask = 0 " + AdatFilePath);
                }
                else
                {
                    Byte[] byteData = File.ReadAllBytes(AdatFilePath);

                    if (byteData != null)
                    {
                        dvxData = new CDvxData();

                        if (dvxData != null)
                        {
                            dvxData._mData = byteData;
                            dvxData._mSizeData = (UInt32)byteData.Length;
                            dvxData._mNrSamples = (UInt32)(dvxData._mSizeData / (_mNrBytesPerSample * _mNrChannelsInFile));
                            dvxData._mFileName = AdatFilePath;
                            dvxData._mUtc = utc;
                            dvxData._mDurationSec = dvxData._mNrSamples / (double)_mSamplesPerSec;
                            dvxData._mDeviceSNR = deviceSNR;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed DVX load " + AdatFilePath, ex);
                dvxData = null;
            }
            return dvxData;
        }

        public UInt16 mGetLoadNrChannels()
        {
            UInt16 nrChannels = 0;
            UInt16 mask = 1;

            for (int i = 0; i < 16; ++i)
            {
                mask = (UInt16)(1 << i);
                if ((_mLoadChannelsMask & mask) != 0)
                {
                    nrChannels++;
                }
            }
            return nrChannels;
        }

        public bool mbCheckDoFillGap(double AGapSec, Int32 AIndex, UInt16 AMaxHalfBlockSize = 10, float ABlockDiffSec = 0.5F)
        {
            return CRecordMit.sbCheckDoFillGap(AGapSec, (UInt32)AIndex);

        }


        private CRecordMit mMergeDvxData(out string ArResult, List<CDvxData> AListDvxData)
        {
            CRecordMit rec = null;
            string result = "";
            int nDat = 0;

            try
            {
                nDat = AListDvxData.Count;

                if (nDat > 0 && _mSamplesPerSec > 0.1F)
                {
                    UInt32 nEstimatedSamples = 0;
                    UInt32 nSamples = 0;
                    Int32 maxNrSamples = 1 << 30;
                    int i = 0;
                    DateTime startUtc = AListDvxData[0]._mUtc;
                    DateTime endUtc = startUtc;
                    double durationSec = 0;
                    double fillDuration = 0;
                    UInt16 nrChannels = mGetLoadNrChannels();

                    DateTime prevStartUtc = DateTime.MinValue;
                    UInt32 prevN = 0;
                    double prevDurSec = 0;

                    double sumPeriodSec = 0;
                    UInt32 sumPeriodN = 0;
                    UInt32 countPeriods = 0;

                    float calcSps;
                    float oldSps = _mSamplesPerSec;


                    foreach (CDvxData node in AListDvxData)
                    {
                        DateTime dt = node._mUtc.AddSeconds(node._mDurationSec);

                        if (dt < node._mUtc) startUtc = node._mUtc;
                        if (dt > endUtc) endUtc = dt;
                        durationSec += node._mDurationSec;
                        nEstimatedSamples += node._mNrSamples;
                        if (nEstimatedSamples > maxNrSamples)
                        {
                            break;
                        }
                        if (i > 0)
                        {
                            // calc avarage sample rate
                            double prevDeltaSec = (node._mUtc - prevStartUtc).TotalSeconds;
                            double gap = (prevDeltaSec - prevDurSec);

                            if (gap >= -3.0 && gap <= 3.0)
                            {
                                // normal period
                                sumPeriodSec += prevDeltaSec;
                                sumPeriodN += prevN;
                                ++countPeriods;
                            }
                        }

                        ++i;

                        prevStartUtc = node._mUtc;
                        prevN = node._mNrSamples;
                        prevDurSec = prevN / (double)_mSamplesPerSec;
                    }
                    if (countPeriods > 0 & sumPeriodSec > 1)
                    {
                        calcSps = (float)(sumPeriodN / sumPeriodSec);
                        float calcDelta = _mSamplesPerSec - oldSps;

                        if (calcDelta < -0.4 || calcDelta > 0.4)
                        {
                            CProgram.sLogLine("DVX calc " + calcDelta.ToString("0.00") + " samples/sec " + calcSps.ToString("0.00")
                                + " using " + countPeriods.ToString() + " / " + i.ToString() + " files");
                        }

                        //                        if( _mAdjustSpsFactor > 0.01)
                        {
                            _mSamplesPerSec = (float)((1 - _mAdjustSpsFactor) * oldSps + _mAdjustSpsFactor * calcSps);

                            float delta = _mSamplesPerSec - oldSps;
                            if (delta < -2 || delta > 2)
                            {
                                CProgram.sLogLine("DVX adjust " + delta.ToString("0.00") + " samples/sec " + _mSamplesPerSec.ToString("0.00")
                                    + " using " + countPeriods.ToString() + " / " + i.ToString() + " files");
                            }
                        }
                    }
                    if (durationSec > 5 * 24 * 3600)
                    {
                        result = "total strip to long " + durationSec.ToString("0.0") + "sec " + nEstimatedSamples.ToString() + " samples";
                    }
                    else
                    {
                        if (_mbFillGaps)
                        {
                            fillDuration = durationSec;
                            durationSec = (endUtc - startUtc).TotalSeconds;
                            fillDuration = durationSec - fillDuration;
                            nEstimatedSamples = (UInt32)(durationSec * _mSamplesPerSec + 0.4);
                        }
                        if (nEstimatedSamples > maxNrSamples)
                        {
                            result = "to much samples " + durationSec.ToString("0.0") + "sec " + nEstimatedSamples.ToString()
                                + " samples" + (_mbFillGaps ? "fill " + fillDuration.ToString("0.0") + " sec" : "");

                        }
                        else
                        {
                            CRecordMit newRec = new CRecordMit(CDvtmsData.sGetDBaseConnection());

                            if (newRec != null && newRec.mbCreateSignals(nrChannels) && newRec.mbCreateAllSignalData(nEstimatedSamples + 1000))
                            {
                                DateTime currentUTC = startUtc;
                                double gap;
                                double gapFilled = 0;
                                i = 0;

                                foreach (CDvxData node in AListDvxData)
                                {
                                    if (i > 0 && _mbFillGaps)
                                    {
                                        gap = (node._mUtc - currentUTC).TotalSeconds;

                                        if (mbCheckDoFillGap(gap, i))
                                        {
                                            gapFilled += gap;
                                            mAddGapSamples(ref currentUTC, newRec, gap);
                                        }

                                    }
                                    mbAddDataSamples(ref currentUTC, newRec, node);
                                    ++i;
                                }
                                newRec.mDeviceID = sGetDeviceName(_mDeviceSnr);
                                newRec.mDeviceModel = "DVX";
                                newRec.mTimeZoneOffsetMin = _mTimeZoneOffsetMin;
                                newRec.mSampleFrequency = (UInt32)(_mSamplesPerSec + 0.5);
                                newRec.mAdcGain = _mAmplGainPermV;
                                newRec.mAdcBits = (UInt16)(_mNrBytesPerSample * 8);
                                newRec.mbActive = true;
                                newRec.mDataFormat = 0; // only used for MIT
                                //newRec.mNrSamplesPerSignal = maxNrSamples;

                                //mAdcZero = 0;                   // curently discarded

                                newRec.mSampleUnitT = 1.0F / (float)_mSamplesPerSec;              // 1 / mFrequency
                                newRec.mSampleUnitA = 1.0F / (float)_mAmplGainPermV;            // 1 / mAdcGain
                                                                                                //mNrSamples = 0;                // nr sample values in the file
                                newRec.mEventTypeString = _mEventType;
                                newRec.mRecDurationSec = newRec.mGetSamplesTotalTime();
                                newRec.mTransmitUTC = DateTime.UtcNow;
                                //            mTransmitLocalDT = DateTime.MinValue;
                                newRec.mBaseUTC = DateTime.SpecifyKind(startUtc, DateTimeKind.Utc);
                                newRec.mbSetEventUTC(_mEventUTC);


                                rec = newRec;
                                result = nDat.ToString() + " files ";
                                if (gapFilled > 1)
                                {
                                    result += gapFilled.ToString("0.0") + " sec gap ";
                                }
                                result += rec.mNrSignals.ToString() + "x " + rec.mGetNrSamples().ToString() + " samples " + rec.mGetSamplesTotalTime().ToString("0.000") + " sec";
                            }
                            else
                            {
                                result = "failed create signals " + durationSec.ToString("0.0") + "sec " + nEstimatedSamples.ToString() + " samples";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result = "Failed merge DVX " + nDat.ToString();
                CProgram.sLogException(result, ex);
                rec = null;
            }
            ArResult = result;
            return rec;
        }
        private void mAddGapSamples(ref DateTime ArCurrentUTC, CRecordMit ARecord, double AGapSec)
        {
            UInt32 addN = (UInt32)(AGapSec * _mSamplesPerSec);
            if (ARecord != null && addN > 1)
            {
                try
                {
                    Int32 blockAmpl = _mAmplGainPermV / 20;
                    UInt16 nrSignals = ARecord.mNrSignals;

                    for (int iChannel = 0; iChannel < nrSignals; ++iChannel)
                    {
                        CSignalData signal = ARecord.mSignals[iChannel];
                        if (signal != null && signal.mValues != null)
                        {
                            UInt32 currentSample = signal.mNrValues;
                            Int32 lastValue = currentSample == 0 ? 0 : signal.mValues[currentSample - 1];

                            for (int i = 0; i < addN; ++i)     // add block wave 0.1 mV at sample freq / 4
                            {
                                int v = (i & 2) == 0 ? lastValue + blockAmpl : lastValue - blockAmpl;
                                signal.mAddValue(v);
                            }
                        }
                    }
                    ARecord.mNrSamples += addN;
                    for (int i = 0; i < nrSignals; ++i)
                    {
                        CSignalData signal = ARecord.mSignals[i];
                        if (signal.mNrValues < ARecord.mNrSamples)
                        {
                            CProgram.sLogError("DVX fillGap<" + CProgram.sDateTimeToYMDHMS(ArCurrentUTC) + ">[" + i + "] n= " + signal.mNrValues + " -> " + ARecord.mNrSamples);
                            ARecord.mNrSamples = signal.mNrValues;
                        }
                    }
                    ArCurrentUTC = ArCurrentUTC.AddSeconds(addN / (double)_mSamplesPerSec);
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Add gap failed", ex);
                }
            }

        }

        private bool mbAddDataSamples(ref DateTime ArCurrentUTC, CRecordMit ARecord, CDvxData ADvxData)
        {
            bool bOk = false;

            switch (_mNrBytesPerSample)
            {
                case 1:
                    bOk = mbAddDataByte(ref ArCurrentUTC, ARecord, ADvxData);
                    break;
                case 2:
                    bOk = _mbRevertBytesMSB ? mbAddDataMSB2(ref ArCurrentUTC, ARecord, ADvxData)
                    : mbAddDataLSB2(ref ArCurrentUTC, ARecord, ADvxData);
                    break;
                case 3:
                    bOk = _mbRevertBytesMSB ? mbAddDataMSB3(ref ArCurrentUTC, ARecord, ADvxData)
                    : mbAddDataLSB3(ref ArCurrentUTC, ARecord, ADvxData);
                    break;
                case 4:
                    bOk = _mbRevertBytesMSB ? mbAddDataMSB4(ref ArCurrentUTC, ARecord, ADvxData)
                    : mbAddDataLSB4(ref ArCurrentUTC, ARecord, ADvxData);
                    break;
            }
            return bOk;
        }

        private bool mbAddDataMSB4(ref DateTime ArCurrentUTC, CRecordMit ARecord, CDvxData ADvxData)
        {
            // read data samples with each 4 bytes <B31..B24><B23..B16><B15..B8><B7..B0> 
            bool bOk = false;

            if (ARecord != null && ADvxData != null && ADvxData._mData != null && ADvxData._mNrSamples > 0)
            {
                try
                {
                    Int32 blockIndex = 0;
                    byte[] data = ADvxData._mData;
                    UInt32 addN = ADvxData._mNrSamples;
                    UInt16 nrSignals = ARecord.mNrSignals;
                    int v;
                    byte B31B24, B23B16, B15B8, B7B0;
                    UInt16 nrChannels = mGetLoadNrChannels();
                    Int32 mask = 1;

                    if (nrSignals > nrChannels)
                    {
                        nrSignals = nrChannels;
                    }

                    for (int iSample = 0; iSample < addN; ++iSample)
                    {
                        int iSignal = 0;

                        for (int j = 0; j < _mNrChannelsInFile; ++j)
                        {
                            mask = 1 << j;

                            B31B24 = data[blockIndex]; ++blockIndex;
                            B23B16 = data[blockIndex]; ++blockIndex;
                            B15B8 = data[blockIndex]; ++blockIndex;
                            B7B0 = data[blockIndex]; ++blockIndex;
                            v = (Int32)(((UInt32)B31B24 << 24) | ((UInt32)B23B16 << 16) | ((UInt32)B15B8 << 8) | ((UInt32)B7B0));

                            if ((_mLoadChannelsMask & mask) != 0)
                            {
                                if (iSignal < nrSignals)
                                {
                                    CSignalData signal = ARecord.mSignals[iSignal];
                                    if (signal != null && signal.mValues != null)
                                    {
                                        signal.mAddValue(v);
                                    }
                                    ++iSignal;
                                }
                            }
                        }
                    }
                    ARecord.mNrSamples += addN;
                    for (int i = 0; i < nrSignals; ++i)
                    {
                        CSignalData signal = ARecord.mSignals[i];
                        if (signal.mNrValues < ARecord.mNrSamples)
                        {
                            CProgram.sLogError("DVX AddData(" + ADvxData._mFileName + ")[" + i + "] n= " + signal.mNrValues + " -> " + ARecord.mNrSamples);
                            ARecord.mNrSamples = signal.mNrValues;
                        }
                    }
                    ArCurrentUTC = ArCurrentUTC.AddSeconds(addN / (double)_mSamplesPerSec);
                    bOk = true;
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Add data failed " + ADvxData._mFileName, ex);
                }
            }
            return bOk;
        }
        private bool mbAddDataLSB4(ref DateTime ArCurrentUTC, CRecordMit ARecord, CDvxData ADvxData)
        {
            // read data samples with each 4 bytes <B7..B0><B15..B8><B23..B16><B31..B24> 
            bool bOk = false;

            if (ARecord != null && ADvxData != null && ADvxData._mData != null && ADvxData._mNrSamples > 0)
            {
                try
                {
                    Int32 blockIndex = 0;
                    byte[] data = ADvxData._mData;
                    UInt32 addN = ADvxData._mNrSamples;
                    UInt16 nrSignals = ARecord.mNrSignals;
                    int v;
                    byte B31B24, B23B16, B15B8, B7B0;
                    UInt16 nrChannels = mGetLoadNrChannels();
                    Int32 mask = 1;

                    if (nrSignals > nrChannels)
                    {
                        nrSignals = nrChannels;
                    }

                    for (int iSample = 0; iSample < addN; ++iSample)
                    {
                        int iSignal = 0;

                        for (int j = 0; j < _mNrChannelsInFile; ++j)
                        {
                            mask = 1 << j;

                            B7B0 = data[blockIndex]; ++blockIndex;
                            B15B8 = data[blockIndex]; ++blockIndex;
                            B23B16 = data[blockIndex]; ++blockIndex;
                            B31B24 = data[blockIndex]; ++blockIndex;
                            v = (Int32)(((UInt32)B31B24 << 24) | ((UInt32)B23B16 << 16) | ((UInt32)B15B8 << 8) | ((UInt32)B7B0));

                            if ((_mLoadChannelsMask & mask) != 0)
                            {
                                if (iSignal < nrSignals)
                                {
                                    CSignalData signal = ARecord.mSignals[iSignal];
                                    if (signal != null && signal.mValues != null)
                                    {
                                        signal.mAddValue(v);
                                    }
                                    ++iSignal;
                                }
                            }
                        }
                    }
                    ARecord.mNrSamples += addN;
                    for (int i = 0; i < nrSignals; ++i)
                    {
                        CSignalData signal = ARecord.mSignals[i];
                        if (signal.mNrValues < ARecord.mNrSamples)
                        {
                            CProgram.sLogError("DVX AddData(" + ADvxData._mFileName + ")[" + i + "] n= " + signal.mNrValues + " -> " + ARecord.mNrSamples);
                            ARecord.mNrSamples = signal.mNrValues;
                        }
                    }
                    ArCurrentUTC = ArCurrentUTC.AddSeconds(addN / (double)_mSamplesPerSec);
                    bOk = true;
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Add data failed " + ADvxData._mFileName, ex);
                }
            }
            return bOk;
        }
        private bool mbAddDataMSB3(ref DateTime ArCurrentUTC, CRecordMit ARecord, CDvxData ADvxData)
        {
            // read data samples with each 3 bytes <B23..B16><B15..B8><B7..B0> 
            bool bOk = false;

            if (ARecord != null && ADvxData != null && ADvxData._mData != null && ADvxData._mNrSamples > 0)
            {
                try
                {
                    Int32 blockIndex = 0;
                    byte[] data = ADvxData._mData;
                    UInt32 addN = ADvxData._mNrSamples;
                    UInt16 nrSignals = ARecord.mNrSignals;
                    int v;
                    UInt32 sign = 0xFF000000;
                    byte B23B16, B15B8, B7B0;
                    UInt16 nrChannels = mGetLoadNrChannels();
                    Int32 mask = 1;

                    if (nrSignals > nrChannels)
                    {
                        nrSignals = nrChannels;
                    }

                    for (int iSample = 0; iSample < addN; ++iSample)
                    {
                        int iSignal = 0;

                        for (int j = 0; j < _mNrChannelsInFile; ++j)
                        {
                            mask = 1 << j;

                            B23B16 = data[blockIndex]; ++blockIndex;
                            B15B8 = data[blockIndex]; ++blockIndex;
                            B7B0 = data[blockIndex]; ++blockIndex;
                            if ((_mLoadChannelsMask & mask) != 0)
                            {
                                if (iSignal < nrSignals)
                                {
                                    v = (Int32)(((UInt32)B23B16 << 16) | ((UInt32)B15B8 << 8) | ((UInt32)B7B0));

                                    if (_mbSignExtend && (B23B16 & 0x80) != 0)
                                    {   // sign extend
                                        v |= (Int32)sign;
                                    }
                                    CSignalData signal = ARecord.mSignals[iSignal];
                                    if (signal != null && signal.mValues != null)
                                    {
                                        signal.mAddValue(v);
                                    }
                                    ++iSignal;
                                }
                            }
                        }
                    }
                    ARecord.mNrSamples += addN;
                    for (int i = 0; i < nrSignals; ++i)
                    {
                        CSignalData signal = ARecord.mSignals[i];
                        if (signal.mNrValues < ARecord.mNrSamples)
                        {
                            CProgram.sLogError("DVX AddData(" + ADvxData._mFileName + ")[" + i + "] n= " + signal.mNrValues + " -> " + ARecord.mNrSamples);
                            ARecord.mNrSamples = signal.mNrValues;
                        }
                    }
                    ArCurrentUTC = ArCurrentUTC.AddSeconds(addN / (double)_mSamplesPerSec);
                    bOk = true;
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Add data failed " + ADvxData._mFileName, ex);
                }
            }
            return bOk;
        }
        private bool mbAddDataLSB3(ref DateTime ArCurrentUTC, CRecordMit ARecord, CDvxData ADvxData)
        {
            // read data samples with each 3 bytes <B7..B0><B15..B8><B23..B16> 
            bool bOk = false;

            if (ARecord != null && ADvxData != null && ADvxData._mData != null && ADvxData._mNrSamples > 0)
            {
                try
                {
                    Int32 blockIndex = 0;
                    byte[] data = ADvxData._mData;
                    UInt32 addN = ADvxData._mNrSamples;
                    UInt16 nrSignals = ARecord.mNrSignals;
                    int v;
                    UInt32 sign = 0xFF000000;
                    byte B23B16, B15B8, B7B0;
                    UInt16 nrChannels = mGetLoadNrChannels();
                    Int32 mask = 1;

                    if (nrSignals > nrChannels)
                    {
                        nrSignals = nrChannels;
                    }
                    for (int iSample = 0; iSample < addN; ++iSample)
                    {
                        int iSignal = 0;

                        for (int j = 0; j < _mNrChannelsInFile; ++j)
                        {
                            mask = 1 << j;

                            B7B0 = data[blockIndex]; ++blockIndex;
                            B15B8 = data[blockIndex]; ++blockIndex;
                            B23B16 = data[blockIndex]; ++blockIndex;
                            if ((_mLoadChannelsMask & mask) != 0)
                            {
                                if (iSignal < nrSignals)
                                {
                                    v = (Int32)(((UInt32)B23B16 << 16) | ((UInt32)B15B8 << 8) | ((UInt32)B7B0));

                                    if (_mbSignExtend && (B23B16 & 0x80) != 0)
                                    {   // sign extend
                                        v |= (Int32)sign;
                                    }

                                    CSignalData signal = ARecord.mSignals[iSignal];
                                    if (signal != null && signal.mValues != null)
                                    {
                                        signal.mAddValue(v);
                                    }
                                    ++iSignal;
                                }
                            }
                        }
                    }
                    ARecord.mNrSamples += addN;
                    for (int i = 0; i < nrSignals; ++i)
                    {
                        CSignalData signal = ARecord.mSignals[i];
                        if (signal.mNrValues < ARecord.mNrSamples)
                        {
                            CProgram.sLogError("DVX AddData(" + ADvxData._mFileName + ")[" + i + "] n= " + signal.mNrValues + " -> " + ARecord.mNrSamples);
                            ARecord.mNrSamples = signal.mNrValues;
                        }
                    }


                    ArCurrentUTC = ArCurrentUTC.AddSeconds(addN / (double)_mSamplesPerSec);
                    bOk = true;
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Add data failed " + ADvxData._mFileName, ex);
                }
            }
            return bOk;
        }
        private bool mbAddDataMSB2(ref DateTime ArCurrentUTC, CRecordMit ARecord, CDvxData ADvxData)
        {
            // read data samples with each 2 bytes <B15..B8><B7..B0> 
            bool bOk = false;

            if (ARecord != null && ADvxData != null && ADvxData._mData != null && ADvxData._mNrSamples > 0)
            {
                try
                {
                    Int32 blockIndex = 0;
                    byte[] data = ADvxData._mData;
                    UInt32 addN = ADvxData._mNrSamples;
                    UInt16 nrSignals = ARecord.mNrSignals;
                    int v;
                    UInt32 sign = 0xFFFF0000;
                    byte B15B8, B7B0;
                    UInt16 nrChannels = mGetLoadNrChannels();
                    Int32 mask = 1;

                    if (nrSignals > nrChannels)
                    {
                        nrSignals = nrChannels;
                    }

                    for (int iSample = 0; iSample < addN; ++iSample)
                    {
                        int iSignal = 0;

                        for (int j = 0; j < _mNrChannelsInFile; ++j)
                        {
                            mask = 1 << j;

                            B15B8 = data[blockIndex]; ++blockIndex;
                            B7B0 = data[blockIndex]; ++blockIndex;
                            if ((_mLoadChannelsMask & mask) != 0)
                            {
                                if (iSignal < nrSignals)
                                {
                                    v = (Int32)(((UInt32)B15B8 << 8) | ((UInt32)B7B0));

                                    if (_mbSignExtend && (B15B8 & 0x80) != 0)
                                    {   // sign extend
                                        v |= (Int32)sign;
                                    }
                                    CSignalData signal = ARecord.mSignals[iSignal];
                                    if (signal != null && signal.mValues != null)
                                    {
                                        signal.mAddValue(v);
                                    }
                                    ++iSignal;
                                }
                            }
                        }
                    }
                    ARecord.mNrSamples += addN;
                    for (int i = 0; i < nrSignals; ++i)
                    {
                        CSignalData signal = ARecord.mSignals[i];
                        if (signal.mNrValues < ARecord.mNrSamples)
                        {
                            CProgram.sLogError("DVX AddData(" + ADvxData._mFileName + ")[" + i + "] n= " + signal.mNrValues + " -> " + ARecord.mNrSamples);
                            ARecord.mNrSamples = signal.mNrValues;
                        }
                    }
                    ArCurrentUTC = ArCurrentUTC.AddSeconds(addN / (double)_mSamplesPerSec);
                    bOk = true;
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Add data failed " + ADvxData._mFileName, ex);
                }
            }
            return bOk;
        }
        private bool mbAddDataLSB2(ref DateTime ArCurrentUTC, CRecordMit ARecord, CDvxData ADvxData)
        {
            // read data samples with each 2 bytes <B7..B0><B15..B8> 
            bool bOk = false;

            if (ARecord != null && ADvxData != null && ADvxData._mData != null && ADvxData._mNrSamples > 0)
            {
                try
                {
                    Int32 blockIndex = 0;
                    byte[] data = ADvxData._mData;
                    UInt32 addN = ADvxData._mNrSamples;
                    UInt16 nrSignals = ARecord.mNrSignals;
                    int v;
                    UInt32 sign = 0xFFFF0000;
                    byte B15B8, B7B0;
                    UInt16 nrChannels = mGetLoadNrChannels();
                    Int32 mask = 1;

                    if (nrSignals > nrChannels)
                    {
                        nrSignals = nrChannels;
                    }

                    for (int iSample = 0; iSample < addN; ++iSample)
                    {
                        int iSignal = 0;

                        for (int j = 0; j < _mNrChannelsInFile; ++j)
                        {
                            mask = 1 << j;

                            B7B0 = data[blockIndex]; ++blockIndex;
                            B15B8 = data[blockIndex]; ++blockIndex;
                            if ((_mLoadChannelsMask & mask) != 0)
                            {
                                if (iSignal < nrSignals)
                                {
                                    v = (Int32)(((UInt32)B15B8 << 8) | ((UInt32)B7B0));

                                    if (_mbSignExtend && (B15B8 & 0x80) != 0)
                                    {   // sign extend
                                        v |= (Int32)sign;
                                    }
                                    CSignalData signal = ARecord.mSignals[iSignal];
                                    if (signal != null && signal.mValues != null)
                                    {
                                        signal.mAddValue(v);
                                    }
                                    ++iSignal;
                                }
                            }
                        }
                    }
                    ARecord.mNrSamples += addN;
                    for (int i = 0; i < nrSignals; ++i)
                    {
                        CSignalData signal = ARecord.mSignals[i];
                        if (signal.mNrValues < ARecord.mNrSamples)
                        {
                            CProgram.sLogError("DVX AddData(" + ADvxData._mFileName + ")[" + i + "] n= " + signal.mNrValues + " -> " + ARecord.mNrSamples);
                            ARecord.mNrSamples = signal.mNrValues;
                        }
                    }
                    ArCurrentUTC = ArCurrentUTC.AddSeconds(addN / (double)_mSamplesPerSec);
                    bOk = true;
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Add data failed " + ADvxData._mFileName, ex);
                }
            }
            return bOk;
        }
        private bool mbAddDataByte(ref DateTime ArCurrentUTC, CRecordMit ARecord, CDvxData ADvxData)
        {
            // read data samples with each 2 bytes <B7..B0><B15..B8> 
            bool bOk = false;

            if (ARecord != null && ADvxData != null && ADvxData._mData != null && ADvxData._mNrSamples > 0)
            {
                try
                {
                    Int32 blockIndex = 0;
                    byte[] data = ADvxData._mData;
                    UInt32 addN = ADvxData._mNrSamples;
                    UInt16 nrSignals = ARecord.mNrSignals;
                    int v;
                    UInt32 sign = 0xFFFFFF00;
                    byte B7B0;
                    UInt16 nrChannels = mGetLoadNrChannels();
                    Int32 mask = 1;

                    if (nrSignals > nrChannels)
                    {
                        nrSignals = nrChannels;
                    }

                    for (int iSample = 0; iSample < addN; ++iSample)
                    {
                        int iSignal = 0;

                        for (int j = 0; j < _mNrChannelsInFile; ++j)
                        {
                            mask = 1 << j;

                            B7B0 = data[blockIndex]; ++blockIndex;
                            if ((_mLoadChannelsMask & mask) != 0)
                            {
                                if (iSignal < nrSignals)
                                {
                                    v = (Int32)((UInt32)B7B0);

                                    if (_mbSignExtend && (B7B0 & 0x80) != 0)
                                    {   // sign extend
                                        v |= (Int32)sign;
                                    }
                                    CSignalData signal = ARecord.mSignals[iSignal];
                                    if (signal != null && signal.mValues != null)
                                    {
                                        signal.mAddValue(v);
                                    }
                                    ++iSignal;
                                }
                            }
                        }
                    }
                    ARecord.mNrSamples += addN;
                    for (int i = 0; i < nrSignals; ++i)
                    {
                        CSignalData signal = ARecord.mSignals[i];
                        if (signal.mNrValues < ARecord.mNrSamples)
                        {
                            CProgram.sLogError("DVX AddData(" + ADvxData._mFileName + ")[" + i + "] n= " + signal.mNrValues + " -> " + ARecord.mNrSamples);
                            ARecord.mNrSamples = signal.mNrValues;
                        }
                    }
                    ArCurrentUTC = ArCurrentUTC.AddSeconds(addN / (double)_mSamplesPerSec);
                    bOk = true;
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Add data failed " + ADvxData._mFileName, ex);
                }
            }
            return bOk;
        }

        public static bool sbLoadDvxAlarmType()
        {
            bool bOk = CDvtmsData.sbLoadEnumListFile(ref _sEnumDvxAlarmType, true, "DvxAlarmType", 6, false);

            return bOk;
        }

        static bool sbGetDvxAlarmType(string AAlarmType, out string ArAlarmLabel, out UInt16 ArAlarmLevel)
        {
            bool bOk = false;

            string alarmLabel = AAlarmType;
            UInt16 alarmLevel = (UInt16)DDvxEvent_Priority.Disregard;

            if (AAlarmType != null && AAlarmType.Length > 0)
            {
                alarmLevel = (UInt16)DDvxEvent_Priority.Other;  // there is a string

                if (_sEnumDvxAlarmType != null)
                {
                    string alarm = AAlarmType.Trim().ToLower();

                    if (alarm.StartsWith("alarm"))
                    {
                        string code = alarm.Substring(5).Trim();

                        CSqlEnumRow row = _sEnumDvxAlarmType.mGetRow(code);

                        if (row != null)
                        {
                            alarmLabel = row._mLabel;
                            if (row.mbActive)
                            {
                                bOk = true;
                                if (row._mSort > 0)
                                {
                                    alarmLevel = (UInt16)row._mSort;    // level is the sort
                                }
                            }
                            else
                            {
                                alarmLevel = (UInt16)DDvxEvent_Priority.Disregard;  // alarm is off;
                            }
                        }

                    }
                }
            }
            ArAlarmLabel = alarmLabel;
            ArAlarmLevel = alarmLevel;
            return bOk;
        }


        public bool mbListDvxDatInfo(out string ArReportTextAll, out string ArReportTextShort, out string ArReportResult, UInt32 AStudyNr, string[] AMergeFiles, UInt32 ASamplesPerSec)
        {
            bool bOk = false;
            string reportTextAll = "";
            string reportTextShort = "";
            string reportResult = "";
            string tab = "\t";
            string eol = "\r\n";
            string header;
            double gapTest = 3.0;

            Int32 nrFiles = AMergeFiles == null ? 0 : AMergeFiles.Length;

            string fileName = "?";
            string fileExt;
            UInt32 deviceSNR = 0;
            DateTime baseUTC = DateTime.MinValue;
            UInt32 fileSize;
            DateTime fileWriteUTC = DateTime.MinValue;


            DateTime startRunUtc = DateTime.UtcNow;
            double scanTimeSec = 0;
            double logEverySec = 5;
            DateTime logNextUTC = startRunUtc.AddSeconds(logEverySec);
            DateTime startEcgUTC = DateTime.MinValue;
            DateTime endEcgUTC = DateTime.MinValue;

            string firstFilePath = "";


            try
            {
                uint nFilesRead = 0;
                List<CDvxData> listDvxData = new List<CDvxData>();

                if (nrFiles == 0)
                {
                    reportResult = "No files selected";
                }
                else
                {
                    string basePath = Path.GetDirectoryName(AMergeFiles[0]);

                    CProgram.sLogLine("listDVXDatInfo: scan " + nrFiles.ToString() + " files " + basePath);
                    // scan all files for base time and duration
                    for (int i = 0; i < nrFiles; ++i)
                    {
                        string filePath = AMergeFiles[i];

                        fileName = Path.GetFileNameWithoutExtension(filePath);

                        if (false == CDvxEvtRec.sbParseFileName(filePath, out deviceSNR, out baseUTC, out fileExt))
                        {
                            ArReportResult = "Bad DVX file name " + fileName;
                        }
                        else
                        {
                            fileSize = 0;
                            fileWriteUTC = DateTime.MinValue;

                            try
                            {
                                FileInfo fi = new FileInfo(filePath);
                                if (fi != null)
                                {
                                    fileSize = (UInt32)fi.Length;
                                    fileWriteUTC = fi.LastWriteTimeUtc;
                                }
                            }
                            catch (Exception ex)
                            {
                                CProgram.sLogException("DVX list: Failed to get file info: " + fileName, ex);
                            }
                            CDvxData dvxData = new CDvxData();

                            if (dvxData != null)
                            {

                                dvxData._mFileName = fileName;
                                dvxData._mSizeData = fileSize;
                                dvxData._mFileWriteUtc = fileWriteUTC;
                                dvxData._mNrSamples = (UInt32)(dvxData._mSizeData / (_mNrBytesPerSample * _mNrChannelsInFile));
                                dvxData._mUtc = baseUTC;
                                dvxData._mDurationSec = dvxData._mNrSamples / (double)ASamplesPerSec;
                                dvxData._mDeviceSNR = deviceSNR;

                                if (nFilesRead == 0)
                                {
                                    startEcgUTC = baseUTC;
                                    firstFilePath = filePath;
                                }
                                endEcgUTC = baseUTC.AddSeconds(dvxData._mDurationSec);
                                // add sorted by _mUTC
                                bool bAdd = true;
                                int pos = 0;

                                foreach (CDvxData node in listDvxData)
                                {
                                    if (dvxData._mUtc < node._mUtc)
                                    {
                                        listDvxData.Insert(pos, dvxData);
                                        ++nFilesRead;
                                        bAdd = false;
                                        break;
                                    }
                                    ++pos;
                                }
                                if (bAdd)
                                {
                                    listDvxData.Add(dvxData);
                                    ++nFilesRead;
                                }
                                Int32 rcvMin = dvxData.mFileReceiveDelayMin();
                                if (rcvMin <= 1)
                                {
                                    double rcvHour = rcvMin / 60.0;
                                }

                            }
                        }
                        if (DateTime.UtcNow >= logNextUTC)
                        {
                            logNextUTC = logNextUTC.AddSeconds(logEverySec);
                            CProgram.sLogLine("listDVXDatInfo: scan " + i + " /  " + nrFiles.ToString() + " files...");
                            Application.DoEvents();
                        }
                    }
                    scanTimeSec = (DateTime.UtcNow - startRunUtc).TotalSeconds;

                    if (nFilesRead == 0)
                    {
                        reportResult = " No files read (" + nrFiles.ToString() + ")";
                    }
                    else
                    {

                        // combine files into a report
                        int iNext = 0;
                        bool bEvtPresent;
                        UInt16 iEvtRec, iEvtError, iEvtSkip;
                        string filePath, evtFilePath, recFilePath, errFilePath, skipFilePath;
                        UInt16 eventLevel;
                        DateTime eventUTC;
                        string eventTime;
                        double difSec;
                        double gapSec;
                        double useSec;
                        double rcvMin;
                        UInt32 useN, nrEvents;
                        double useSps;
                        string evtInfo;
                        string line;

                        UInt64 totalSamples = 0;
                        double totalSec = 0;
                        double useTotalSec = 0;
                        UInt64 useTotalN = 0;
                        double useTotalSps = 0;
                        UInt64 totalEvents = 0;

                        double totalRcvMin = 0;
                        UInt32 countRcvMin = 0;

                        UInt32 nrUseFiles = 0;
                        UInt32 nrRecFiles = 0;
                        UInt32 nrEvtFiles = 0;
                        UInt32 nrErrFiles = 0;
                        UInt32 nrSkipFiles = 0;
                        UInt32 nrOkFiles = 0;


                        // make header

                        header = "Index" + tab + "File__Date____Time__" + tab + "N" + tab + "Sec"
                            + tab + "GapSec" + tab + "DifSec"
                            + tab + "useN" + tab + "UseSec" + tab + "UseSps"
                            + tab + "RcvHrs"
                            + tab + "Rec" + tab + "Err" + tab + "skip"
                            + tab + "AtYYMMddHHmmss" + tab + "Level" + tab + "Events" + tab + "EventType";

                        reportTextAll = header + eol;
                        reportTextShort = header + eol;

                        CProgram.sLogLine("listDVXDatInfo: read evt " + nrFiles.ToString() + " files");


                        foreach (CDvxData dvxData in listDvxData)
                        {
                            bool bLogLine = false || iNext == 0;     // log all or first
                            ++iNext;
                            fileName = dvxData._mFileName;
                            filePath = Path.Combine(basePath, dvxData._mFileName);
                            mClearRecording(deviceSNR);
                            rcvMin = dvxData.mFileReceiveDelayMin();

                            difSec = 0;
                            gapSec = 0;
                            useSec = 0;
                            useN = 0;
                            useSps = 0;
                            iEvtRec = 0;
                            iEvtError = 0;
                            iEvtSkip = 0;

                            if (iNext < nFilesRead)
                            {
                                CDvxData nextData = listDvxData[iNext];

                                if (nextData != null)
                                {
                                    difSec = (nextData._mUtc - dvxData._mUtc).TotalSeconds;
                                    gapSec = difSec - dvxData._mDurationSec;

                                    if (gapSec > -gapTest && gapSec < gapTest)
                                    {
                                        // gap is within limit to recalc 
                                        ++nrUseFiles;
                                        useN = dvxData._mNrSamples;
                                        useSec = difSec;
                                        useSps = difSec < 0.1 ? 0 : useN / difSec;
                                    }
                                    else
                                    {
                                        bLogLine = true;    // line is different
                                    }
                                }
                            }
                            else
                            {
                                bLogLine = true;
                            }

                            evtFilePath = filePath + _cEcgExtension + _cEventExtension;
                            nrEvents = 0;
                            eventLevel = 0;
                            evtInfo = "\t";
                            eventTime = "              ";

                            bEvtPresent = mbCheckLoadEventFile(out eventLevel, out nrEvents, evtFilePath, true, false);  // sets eventUTC and eventSample

                            if (bEvtPresent)
                            {
                                bLogLine = true;
                                ++nrEvtFiles;
                                evtInfo = _mEventType;
                                eventTime = CProgram.sDateTimeToYMDHMS(dvxData._mUtc.AddSeconds(_mEventSampleNr / (double)ASamplesPerSec));

                                if (eventLevel == (UInt16)DDvxEvent_Priority.Disregard)
                                {
                                    evtInfo += "-";
                                }
                                if (_mEventSampleNr > dvxData._mNrSamples)
                                {
                                    evtInfo += "\t!" + _mEventSampleNr;
                                }
                            }

                            recFilePath = filePath + _cEcgExtension + _cEvtRecExtension;  // *.dat.DvxEvtRec
                            if (File.Exists(recFilePath))
                            {
                                bLogLine = true;
                                iEvtRec = 1;
                                ++nrRecFiles;
                            }
                            errFilePath = filePath + _cEcgExtension + _cEvtErrorExtension; //.dat.DvxEvtError
                            if (File.Exists(errFilePath))
                            {
                                bLogLine = true;
                                iEvtError = 1;
                                ++nrErrFiles;
                            }
                            skipFilePath = filePath + _cEcgExtension + _cEvtSkipExtension; //.dat.DvxEvtSkip
                            if (File.Exists(skipFilePath))
                            {
                                bLogLine = true;
                                iEvtSkip = 1;
                                ++nrSkipFiles;
                            }
                            if (iEvtRec >= 1 && iEvtError <= 0 && iEvtSkip <= 0)
                            {
                                ++nrOkFiles;
                            }

                            string gapStr = gapSec > 999 ? ((int)(gapSec)).ToString() : gapSec.ToString("0.000");
                            string difStr = difSec > 999 ? ((int)(difSec)).ToString() : difSec.ToString("0.000");
                            double rcvHour = rcvMin / 60.0;
                            line = iNext.ToString() + tab + fileName + tab + dvxData._mNrSamples.ToString() + tab + dvxData._mDurationSec.ToString("0.000")
                                + tab + gapStr + tab + difStr
                                + tab + useN.ToString() + tab + useSec.ToString("0.0") + tab + useSps.ToString("0.0")
                                + tab + (rcvMin >= 0 ? rcvHour.ToString("0.0") : "")
                                + tab + iEvtRec.ToString() + tab + iEvtError.ToString() + tab + iEvtSkip
                                + tab + eventTime + tab + eventLevel + tab + evtInfo;

                            reportTextAll += line + eol;
                            if (bLogLine)
                            {
                                reportTextShort += line + eol;
                            }
                            // sum totals

                            totalSamples += dvxData._mNrSamples;
                            totalSec += dvxData._mDurationSec;
                            useTotalSec += useSec;
                            useTotalN += useN;
                            useTotalSps += useSps;
                            totalEvents += nrEvents;
                            if (rcvMin >= 0)
                            {
                                totalRcvMin += rcvMin;
                                ++countRcvMin;
                            }

                            if (DateTime.UtcNow >= logNextUTC)
                            {
                                logNextUTC = logNextUTC.AddSeconds(logEverySec);
                                CProgram.sLogLine("listDVXDatInfo: read evt " + iNext + " /  " + nrFiles.ToString() + " files: " + nrEvtFiles + " events...");
                                Application.DoEvents();
                            }
                        }

                        //  calc avarage
                        double calcSps = useTotalSec < 0.1 ? 0 : useTotalN / useTotalSec;
                        UInt32 avgN = (UInt32)(totalSamples / (UInt64)nFilesRead);
                        double avgSec = totalSec / (double)nFilesRead;
                        UInt32 avgUseN = nrUseFiles == 0 ? 0 : (UInt32)(useTotalN / (UInt64)nrUseFiles);
                        double avgUseSps = nrUseFiles == 0 ? 0 : useTotalSps / (double)nrUseFiles;
                        double avgUseSec = nrUseFiles == 0 ? 0 : useTotalSec / (double)nrUseFiles;
                        double avgRcvHrs = countRcvMin == 0 ? 0 : totalRcvMin / 60.0 / (double)countRcvMin;
                        double startEndSec = (endEcgUTC - startEcgUTC).TotalSeconds;
                        double missingSec = startEndSec - totalSec;
                        double avgNrEvents = nrEvtFiles == 0 ? 0 : totalEvents / (double)nrEvtFiles;

                        // report
                        string reportSummery = eol;

                        reportSummery += "FirstFilePath" + tab + "=" + firstFilePath + eol;
                        reportSummery += "DeviceSNR" + tab + "=" + tab + sGetDeviceName(_mDeviceSnr) + eol;
                        reportSummery += "DeviceSps" + tab + "=" + tab + _mSamplesPerSec.ToString("0.00") + eol;
                        reportSummery += "NrChannels" + tab + "=" + tab + _mNrChannelsInFile.ToString() + eol;
                        reportSummery += "TestSps" + tab + "=" + tab + ASamplesPerSec.ToString("0.00") + eol;
                        reportSummery += "StartECG" + tab + "=" + tab + CProgram.sDateTimeToYMDHMS(startEcgUTC) + eol;
                        reportSummery += "EndECG" + tab + "=" + tab + CProgram.sDateTimeToYMDHMS(endEcgUTC) + eol;
                        reportSummery += "durationEcgSec" + tab + "=" + tab + startEndSec.ToString() + tab + CProgram.sPrintTimeSpan_dhmSec(startEndSec, "") + eol;
                        reportSummery += "MissingSec" + tab + "=" + tab + missingSec.ToString("0.0") + tab + CProgram.sPrintTimeSpan_dhmSec(missingSec, "") + eol;

                        reportSummery += "NrFiles" + tab + "=" + tab + nFilesRead.ToString() + eol;
                        reportSummery += "TotalSamples" + tab + "=" + tab + totalSamples.ToString() + tab + "avg " + avgN.ToString() + eol;
                        reportSummery += "TotalSec" + tab + "=" + tab + totalSec.ToString() + tab + "avg " + avgSec.ToString("0.000") + eol;
                        reportSummery += "avgRcvHours " + tab + "=" + avgRcvHrs.ToString("0.00") + tab + "rcvN" + tab + "=" + tab + countRcvMin + eol;
                        reportSummery += "NrUseFiles" + tab + "=" + tab + nrUseFiles.ToString() + eol;
                        reportSummery += "useN" + tab + "=" + tab + useTotalN.ToString() + tab + "avg " + avgUseN.ToString("0.0") + eol;
                        reportSummery += "useSec" + tab + "=" + tab + useTotalSec.ToString() + tab + "avg " + avgUseSec.ToString("0.000") + eol;
                        reportSummery += "useSps" + tab + "=" + tab + avgUseSps.ToString("0.0") + eol;
                        reportSummery += "useSps" + tab + "=" + tab + avgUseSps.ToString("0.0") + eol;

                        reportSummery += "NrEvtRec" + tab + "=" + tab + nrRecFiles.ToString() + eol;
                        reportSummery += "NrError" + tab + "=" + tab + nrErrFiles.ToString() + eol;
                        reportSummery += "NrSkip" + tab + "=" + tab + nrSkipFiles.ToString() + eol;
                        reportSummery += "NrOk" + tab + "=" + tab + nrOkFiles.ToString() + eol;
                        reportSummery += "NrEvt" + tab + "=" + tab + nrEvtFiles.ToString() + tab + "avg " + avgNrEvents.ToString("0.0") + eol;
                        reportSummery += "Total Events" + tab + "=" + tab + totalEvents.ToString() + eol;
                        reportSummery += "Report" + tab + "=" + tab + CProgram.sDateTimeToYMDHMS(DateTime.Now) + tab + "in" + tab + (DateTime.UtcNow - startRunUtc).TotalSeconds.ToString("0.0")
                            + tab + "by" + tab + CProgram.sGetUserAtPcString() + eol;
                        reportSummery += eol;
                        reportSummery += "calcSps" + tab + "=" + tab + calcSps.ToString("0.000") + eol;


                        reportTextAll += reportSummery;
                        reportTextShort += reportSummery;
                        reportResult = "Device=" + _mSamplesPerSec.ToString("0.00") + ", Test=" + ASamplesPerSec.ToString("0.00") + ", calulated= " + calcSps.ToString("0.000") + " Sps" + eol;

                        double totalTimeSec = (DateTime.UtcNow - startRunUtc).TotalSeconds;
                        double readTimeSec = totalTimeSec - scanTimeSec;

                        CProgram.sLogLine("listDVXDatInfo: " + nrFiles.ToString() + " files: " + nrEvtFiles + " events, " + nrRecFiles + " imported,  calcSps= "
                            + calcSps.ToString("0.000") + " Sps in " + totalTimeSec.ToString("0.0") + " sec(scan " + scanTimeSec.ToString("0.0") + ", read " + readTimeSec.ToString("0.0") + " sec)");

                        bOk = true;
                    }




                }
            }
            catch (Exception e)
            {
                CProgram.sLogException("Failed to import DVX file " + fileName, e);
                reportResult = "Exception";
            }
            ArReportTextAll = reportTextAll;
            ArReportTextShort = reportTextShort;
            ArReportResult = reportResult;

            return bOk;
        }
        public bool mbLoadMergePleth(CRecordMit ARecord, string AFilePath)

        {
            bool bOk = false;

            if (ARecord != null && ARecord.mNrSamples > 0 && ARecord.mNrSignals > 0 && ARecord.mSignals != null)
            {
                if (_mbPlethLoad)
                {
                    UInt32 nPlethFiles = mGetNrPlethFiles();

                    if (nPlethFiles > 0)
                    {
                        float plethDuration = 0;
                        string firstFile = _mPlethFiles[0];
                        string result = "LoadMergePleth: " + firstFile + "[" + nPlethFiles + "] ";

                        try
                        {
                            string[] fileArray = new string[nPlethFiles];
                            string sourcePath = "?";

                            if (mbGetSourceFolder(out sourcePath, AFilePath))
                            {
                                for (int i = 0; i < nPlethFiles; ++i)
                                {

                                    string filePath = Path.Combine(sourcePath, _mPlethFiles[i] + _cPlethExtension);
                                    fileArray[i] = filePath;
                                }
                                _mPlethData = new CDvxPleth(false, false);

                                CRecordMit plethRec = _mPlethData.mReadMultipleFiles(fileArray, 2.0F);

                                if (plethRec != null)
                                {
                                    UInt32 nrSamples = ARecord.mSignals[0].mNrValues;

                                    plethDuration = plethRec.mGetSamplesTotalTime();

                                    result += CProgram.sDateTimeToYMDHMS(plethRec.mBaseUTC) + " " + plethRec.mGetSamplesTotalTime().ToString("0.000") + "sec " + plethRec.mNrSamples + "@" + _mPlethSamplesPerSec.ToString("0.0") + "->";
                                    result += CProgram.sDateTimeToYMDHMS(ARecord.mBaseUTC) + " " + ARecord.mGetSamplesTotalTime().ToString("0.000") + "sec " + nrSamples + "@" + ARecord.mSampleFrequency.ToString("0.0") + ": L";

                                    if (plethDuration > 1)
                                    {
                                        DateTime startUTC = ARecord.mBaseUTC;

                                        plethRec.mTimeZoneOffsetMin = ARecord.mTimeZoneOffsetMin;   // apply same time zone offset

                                        double deltaPre = (plethRec.mBaseUTC - ARecord.mBaseUTC).TotalSeconds;
                                        double deltaPost = plethRec.mGetSamplesTotalTime() + deltaPre - ARecord.mGetSamplesTotalTime();
                                        result += "<" + deltaPre.ToString("0.000") + ", " + deltaPost.ToString("0.000") + ">";

                                        CRecordMit resampledRec = plethRec.mResampleRecord(_mPlethSamplesPerSec, startUTC, nrSamples, ARecord.mSampleFrequency, 0);

                                        if (resampledRec != null && resampledRec.mNrSignals >= 1 && resampledRec.mSignals != null)
                                        {
                                            CSignalData plethSignal = resampledRec.mSignals[0];
                                            CConvertValues convert = new CConvertValues();

                                            result += "R";
                                            convert.mbSetup(_mPlethConvert._mLowFrom, _mPlethConvert._mLowTo * ARecord.mAdcGain,
                                                _mPlethConvert._mHighFrom, _mPlethConvert._mHighTo * ARecord.mAdcGain);

                                            // convert pleth 16 bit to an ECG mV values  
                                            Int32[] values = plethSignal.mValues;
                                            UInt32 nValues = plethSignal.mNrValues;
                                            Int32 minValue = Int32.MaxValue;
                                            Int32 maxValue = Int32.MinValue;
                                            Int32 v, vNoSignal = 0;

                                            // determin range without 0 = no signal
                                            for (UInt32 i = 0; i < nValues; ++i)
                                            {
                                                v = values[i];
                                                if (v != 0)
                                                {
                                                    if (v < minValue) minValue = v;
                                                    if (v > maxValue) maxValue = v;
                                                }
                                            }
                                            plethSignal.mMinValue = minValue;
                                            plethSignal.mMaxValue = maxValue;

                                            if (minValue <= maxValue)
                                            {
                                                vNoSignal = minValue - (maxValue - minValue) / 10;  // no signal = 10% below minValue
                                            }
                                            minValue = Int32.MaxValue;
                                            maxValue = Int32.MinValue;

                                            // convert to new range
                                            for (UInt32 i = 0; i < nValues; ++i)
                                            {
                                                v = values[i];
                                                if (v == 0)
                                                {
                                                    v = vNoSignal;
                                                }
                                                v = (Int32)convert.mConvert(v);
                                                values[i] = v;
                                                if (v < minValue) minValue = v;
                                                if (v > maxValue) maxValue = v;
                                            }
                                            plethSignal.mMinValue = minValue;
                                            plethSignal.mMaxValue = maxValue;
                                            result += "C";

                                            // add pleth signal to record
                                            bOk = ARecord.mbAddSignal(plethSignal);

                                            if (bOk)
                                            {
                                                result += "C" + ARecord.mNrSignals;
                                            }
                                        }
                                    }
                                }
                            }
                            CProgram.sLogLine(result);
                        }
                        catch (Exception e)
                        {
                            CProgram.sLogException("Failed to LoadMergePleth " + nPlethFiles + " " + result, e);
                        }
                    }
                }
            }
            return bOk;
        }
    }
    public class CDvxData
    {
        public Byte[] _mData;
        public UInt32 _mSizeData;
        public UInt32 _mNrSamples;
        public string _mFileName;
        public DateTime _mUtc;
        public DateTime _mFileWriteUtc;
        public double _mDurationSec;
        public UInt32 _mDeviceSNR;

        public CDvxData()
        {
            _mData = null;
            _mSizeData = 0;
            _mNrSamples = 0;
            _mFileName = "";
            _mUtc = DateTime.MinValue;
            _mFileWriteUtc = _mUtc;
            _mDurationSec = 0;
            _mDeviceSNR = 0;
        }

        public Int32 mFileReceiveDelayMin()
        {
            int rcvMin = -9;

            if (_mUtc != DateTime.MinValue && _mFileWriteUtc != DateTime.MinValue)
            {
                rcvMin = (Int32)((_mFileWriteUtc - _mUtc).TotalMinutes + 0.999);
            }
            return rcvMin;
        }
    }

    public class CDvxEventLevel
    {
        public UInt16 _mEventLevel;
        public string _mEventType;

        public CDvxEventLevel(string AEventType, UInt16 AEventLevel)
        {
            _mEventType = AEventType;
            _mEventLevel = AEventLevel;
        }

        public static bool sbAddEvent(ref List<CDvxEventLevel> ArEventList, string AEventType, UInt16 AEventLevel)
        {
            bool bOk = AEventLevel < (UInt16)DDvxEvent_Priority.Disregard && AEventType != null && AEventType.Length > 0;

            if (bOk)
            {
                try
                {
                    if (ArEventList == null)
                    {
                        ArEventList = new List<CDvxEventLevel>();
                    }

                    if (ArEventList != null)
                    {
                        int n = ArEventList.Count;
                        bool bAdd = true;

                        int pos = n;
                        int i = 0;

                        foreach (CDvxEventLevel node in ArEventList)
                        {
                            if (AEventLevel < node._mEventLevel)
                            {
                                pos = i;
                                break;
                            }
                            else if (AEventLevel == node._mEventLevel && AEventType == node._mEventType)
                            {
                                pos = i;
                                bAdd = false;// same -> do not add
                                break;
                            }
                            ++i;
                        }
                        if (bAdd)
                        {
                            if (pos == n)
                            {
                                ArEventList.Add(new CDvxEventLevel(AEventType, AEventLevel));
                            }
                            else
                            {
                                ArEventList.Insert(pos, new CDvxEventLevel(AEventType, AEventLevel));
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Add EventLevel failed " + AEventType, ex);
                }
            }
            return bOk;
        }
        public static string sGetEventsLabel(ref List<CDvxEventLevel> ArEventList, UInt32 AMaxNrEvents, bool AbAll)
        {
            string label = "";

            if (ArEventList != null)
            {
                int n = ArEventList.Count;
                int i = 0;

                if (n > AMaxNrEvents)
                {
                    label = n.ToString() + " ";
                }

                foreach (CDvxEventLevel node in ArEventList)
                {
                    if (AbAll || i < AMaxNrEvents)
                    {
                        if (i > 0) label += CDvxEvtRec._cMergeEvents;
                        label += node._mEventType;
                    }
                    ++i;
                }
            }
            return label;
        }
        public static UInt16 sGetEventsLevel(ref List<CDvxEventLevel> ArEventList)
        {
            UInt16 eventLevel = (UInt16)DDvxEvent_Priority.Disregard;

            if (ArEventList != null)
            {
                int n = ArEventList.Count;

                if (n > 0)
                {
                    eventLevel = ArEventList[0]._mEventLevel;
                }
            }
            return eventLevel;
        }
        public static UInt32 sGetEventsCount(ref List<CDvxEventLevel> ArEventList)
        {
            UInt32 n = 0;

            if (ArEventList != null)
            {
                n = (UInt32)ArEventList.Count;
            }
            return n;
        }
    }
}
